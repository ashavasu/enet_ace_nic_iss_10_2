/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnpvcli.c,v 1.16.22.2 2018/03/21 13:08:37 siva Exp $
 *
 * Description : Action routines for PVLAN set/get objects in
 *               fsmpvlan.mib, fsmsvlan.mib and fsmsbext.mib
 *
 *******************************************************************/

#ifndef __VLNPVCLI_C__
#define __VLNPVCLI_C__

/* SOURCE FILE HEADER :
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : vlancli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : ISS TEAM                                         |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : Vlan                                             |
 * |                                                                           |
 * |  MODULE NAME           : Vlan configuration                               |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */
#include "vlaninc.h"
#include "fsmsbrlw.h"

INT4
cli_process_pvlan_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[VLAN_MAX_ARGS];
    UINT1              *pu1Inst = NULL;
    INT1                argno = 0;
    UINT4               u4ErrCode;
    UINT4               u4IfIndex = 0;
    UINT1              *pu1VlanList = NULL;
    UINT1               u1VlanType = 0;
    tVlanId             VlanId;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4Val = 0;

    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);

    VLAN_LOCK ();

    if (VlanCliSelectContextOnMode
        (CliHandle, u4Command, &u4ContextId, &u2LocalPortId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }
    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);
    if (pu1Inst != 0)
    {
        u4IfIndex = CLI_PTR_TO_U4 (pu1Inst);
        if (VcmGetContextInfoFromIfIndex
            ((UINT4) u4IfIndex, &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%% Port not mapped to any of the context \r\n");
            i4RetStatus = CLI_FAILURE;
        }
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store 16 arguements at the max. This is because vlan commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == VLAN_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {

        case CLI_VLAN_PVLAN_TYPE:
            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            u1VlanType = (UINT1) (CLI_PTR_TO_U4 (args[0]));
            i4RetStatus =
                VlanCliSetPvlanType (CliHandle, u4ContextId, VlanId,
                                     u1VlanType);
            break;

        case CLI_VLAN_NO_PVLAN_TYPE:
            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            i4RetStatus = VlanCliSetPvlanType (CliHandle, u4ContextId, VlanId,
                                               L2IWF_NORMAL_VLAN);
            break;
        case CLI_VLAN_ADD_PVLAN_ASSOCIATION:
            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            pu1VlanList = UtlShMemAllocVlanList ();
            if (pu1VlanList == NULL)
            {
                CliPrintf (CliHandle, "\r%pu1VlanList:Memory Allocation "
                           "Failed\r\n");
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            CLI_MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);

            if (CliStrToPortList ((UINT1 *) args[0], pu1VlanList,
                                  VLAN_LIST_SIZE, CFA_L2VLAN) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Vlan List\r\n");
                UtlShMemFreeVlanList (pu1VlanList);
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);

                return CLI_FAILURE;
            }

            i4RetStatus = VlanCliSetPvlanAssociation (CliHandle, u4ContextId,
                                                      VlanId, VLAN_ADD,
                                                      pu1VlanList);
            UtlShMemFreeVlanList (pu1VlanList);
            break;
        case CLI_VLAN_REM_PVLAN_ASSOCIATION:
            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            pu1VlanList = UtlShMemAllocVlanList ();

            if (pu1VlanList == NULL)
            {
                CliPrintf (CliHandle, "\r%% pu1VlanList:Memory Allocation"
                           " Failed\r\n");
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            CLI_MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);

            if (CliStrToPortList ((UINT1 *) args[0], pu1VlanList,
                                  VLAN_LIST_SIZE, CFA_L2VLAN) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Vlan List\r\n");
                UtlShMemFreeVlanList (pu1VlanList);
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);

                return CLI_FAILURE;
            }

            i4RetStatus = VlanCliSetPvlanAssociation (CliHandle, u4ContextId,
                                                      VlanId, VLAN_DELETE,
                                                      pu1VlanList);
            UtlShMemFreeVlanList (pu1VlanList);
            break;
        case CLI_VLAN_OVERWRITE_PVLAN_ASSOCIATION:
            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            pu1VlanList = UtlShMemAllocVlanList ();

            if (pu1VlanList == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% pu1VlanList:Memory Allocation Failed\r\n");
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            CLI_MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);

            if (CliStrToPortList ((UINT1 *) args[0], pu1VlanList,
                                  VLAN_LIST_SIZE, CFA_L2VLAN) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Vlan List\r\n");
                UtlShMemFreeVlanList (pu1VlanList);
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);

                return CLI_FAILURE;
            }

            i4RetStatus = VlanCliSetPvlanAssociation (CliHandle, u4ContextId,
                                                      VlanId, VLAN_UPDATE,
                                                      pu1VlanList);
            UtlShMemFreeVlanList (pu1VlanList);
            break;
        case CLI_VLAN_NO_PVLAN_ASSOCIATION:
            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;
            i4RetStatus = VlanCliDeleteAllAssocSecVlans (CliHandle,
                                                         u4ContextId, VlanId);
            break;
        case CLI_VLAN_PVLAN_HOST_ASSOCIATION:

            i4RetStatus = VlanCliAssociateHostPort (CliHandle,
                                                    u2LocalPortId,
                                                    (tVlanId)
                                                    (*(args[0])),
                                                    (tVlanId) (*(args[1])));
            break;
        case CLI_VLAN_NO_PVLAN_HOST_ASSOCIATION:
            i4RetStatus = VlanCliDisAssociateHostPort (CliHandle,
                                                       u2LocalPortId);
            break;
        case CLI_VLAN_PVLAN_MAPPING:

            pu1VlanList = UtlShMemAllocVlanList ();

            if (pu1VlanList == NULL)
            {
                CliPrintf (CliHandle,
                           "\r%% pu1VlanList:Memory Allocation Failed\r\n");
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            CLI_MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);

            if (args[2] != NULL)
            {
                if (CliStrToPortList ((UINT1 *) args[2], pu1VlanList,
                                      VLAN_LIST_SIZE,
                                      CFA_L2VLAN) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Vlan List\r\n");
                    UtlShMemFreeVlanList (pu1VlanList);
                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    return CLI_FAILURE;
                }
            }
            i4RetStatus = VlanCliAssocPromiscuousPort (CliHandle,
                                                       u2LocalPortId,
                                                       (tVlanId)
                                                       (*(args[0])),
                                                       (UINT1)
                                                       CLI_PTR_TO_U4 (args[1]),
                                                       pu1VlanList);
            UtlShMemFreeVlanList (pu1VlanList);
            break;
        case CLI_VLAN_NO_PVLAN_MAPPING:
            i4RetStatus = VlanCliDisAssocPromicuousPort (CliHandle,
                                                         u2LocalPortId);
            break;
        default:
            /* Given command does not match with any SET command. */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    VlanReleaseContext ();

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_VLAN) &&
            (u4ErrCode < CLI_VLAN_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s",
                       VlanCliErrString[CLI_ERR_OFFSET_VLAN (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    VLAN_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

INT4
cli_process_pvlan_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[VLAN_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode;
    UINT4               u4IfIndex = 0;
    UINT4               u4Type = 0;
    INT4                i4RetStatus = 0;
    INT4                i4Inst = 0;
    UINT4               u4ContextId;
    UINT4               u4TempContextId = VLAN_CLI_INVALID_CONTEXT;
    UINT2               u2LocalPortId = 0;
    UINT1              *pu1ContextName = NULL;

    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);

    VLAN_LOCK ();
    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));
    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     * After the u4IfIndex. (ie) In all the cli commands we are passing 
     * IfIndex as the first argument in variable argument list. Like that 
     * as the second argument we have to pass context-name*/
    pu1ContextName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store 16 arguements at the max. This is because vlan commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == VLAN_MAX_ARGS)
            break;
    }
    va_end (ap);

    while (VlanCliGetContextForShowCmd
           (CliHandle, pu1ContextName, u4IfIndex,
            u4TempContextId, &u4ContextId, &u2LocalPortId) == VLAN_SUCCESS)
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            continue;
        }
        switch (u4Command)
        {
            case CLI_VLAN_SHOW_PVLAN_ALL_CTXT:
                /* args[0] - Context Id */
                /* args[1] - Type */
                u4Type = CLI_PTR_TO_U4 (args[0]);
                i4RetStatus =
                    VlanCliShowPrivateVlan (CliHandle, -1, (UINT1) u4Type);
                break;
            case CLI_VLAN_SHOW_PVLAN_IN_CTXT:
                /* args[0] - Context Id */
                /* args[1] - Type */
                u4Type = CLI_PTR_TO_U4 (args[0]);
                i4RetStatus =
                    VlanCliShowPrivateVlan (CliHandle, (INT4) u4ContextId,
                                            (UINT1) u4Type);
                break;

            default:
                /* Given command does not match with any of the SHOW commands */
                CliPrintf (CliHandle, "\r%% Unknown command \r\n");

                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
        }
        /* If SwitchName or Interface is given as input for show command 
         * then we have to come out of the Loop */
        if ((pu1ContextName != NULL) || (u4IfIndex != 0))
        {
            break;
        }
        u4TempContextId = u4ContextId;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_VLAN) &&
            (u4ErrCode < CLI_VLAN_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s",
                       VlanCliErrString[CLI_ERR_OFFSET_VLAN (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    VLAN_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliSetPvlanType                                  */
/*                                                                           */
/* Description        : This function is used to configure the private       */
/*                      vlan type.                                           */
/*                                                                           */
/* Input(s)           : CliHandle - Handle to the cli context.               */
/*                      u4ContextId - Context Id                             */
/*                      VlanId - Vlan Identifier                             */
/*                      u1Type - Type of the pvlan                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliSetPvlanType (tCliHandle CliHandle, UINT4 u4ContextId, tVlanId VlanId,
                     UINT1 u1Type)
{
    UINT4               u4ErrCode;
    INT4                i4RowStatus = 0;

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u1Type);

    if (nmhGetFsDot1qVlanStaticRowStatus (u4ContextId, VlanId,
                                          &i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (i4RowStatus == VLAN_ACTIVE)
    {
        /* Since the rowstatus of the vlan table is active, configure
         * to notInService and then configure the type */
        if (nmhSetDot1qVlanStaticRowStatus (VlanId,
                                            VLAN_NOT_IN_SERVICE)
            == SNMP_SUCCESS)
        {

            if (nmhTestv2Dot1qVlanStaticRowStatus (&u4ErrCode,
                                                   VlanId,
                                                   VLAN_ACTIVE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetDot1qVlanStaticRowStatus (VlanId,
                                                VLAN_ACTIVE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        return CLI_SUCCESS;
    }

    VlanReleaseContext ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliSetPvlanAssociation                           */
/*                                                                           */
/* Description        : This function is used to configure the private       */
/*                      vlan association with the secondary vlans.           */
/*                                                                           */
/* Input(s)           : CliHandle - Handle to the cli context.               */
/*                      u4ContextId - Context Identifier                     */
/*                      PrimaryVlanId - Primary Vlan Identifier              */
/*                      u1Flag - Action flag used to identify ADD / REMOVE   */
/*                               the secondary vlans associated with the     */
/*                               given primary vlan.                         */
/*                               ADD - Adds the given list of secondary vlan */
/*                                     to already existing vlans.            */
/*                               REMOVE - Deletes the given vlans in the list*/
/*                               If nothing is mentioned then the existing   */
/*                               vlans will be overwritten with the given    */
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   /*                               vlan list.                                  *//*                      pu1SecVlanList - Pointer to the secondary vlan list  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliSetPvlanAssociation (tCliHandle CliHandle, UINT4 u4ContextId,
                            tVlanId InPrimaryVlanId, UINT1 u1Flag,
                            UINT1 *pu1NewVlanList)
{
    tSNMP_OCTET_STRING_TYPE VlanListAll;
    UINT2               u2ByteIndex;
    UINT2               u2VlanFlag = 0;
    UINT2               u2BitIndex = 0;
    UINT1              *pu1TmpAll = NULL;

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (InPrimaryVlanId);
    UNUSED_PARAM (u1Flag);

    pu1TmpAll = UtlShMemAllocVlanList ();

    if (pu1TmpAll == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return CLI_FAILURE;
    }

    MEMSET (pu1TmpAll, 0, VLAN_LIST_SIZE);

    VlanListAll.pu1_OctetList = pu1TmpAll;
    VlanListAll.i4_Length = VLAN_LIST_SIZE;

    CLI_MEMCPY (VlanListAll.pu1_OctetList, pu1NewVlanList,
                VlanListAll.i4_Length);

    for (u2ByteIndex = 0; u2ByteIndex < VLAN_LIST_SIZE; u2ByteIndex++)
    {
        if (VlanListAll.pu1_OctetList[u2ByteIndex] != 0)
        {
            u2VlanFlag = VlanListAll.pu1_OctetList[u2ByteIndex];

            for (u2BitIndex = 0;
                 ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u2VlanFlag != 0));
                 u2BitIndex++)
            {
                u2VlanFlag = (UINT1) (u2VlanFlag << 1);
            }
        }
    }

    UtlShMemFreeVlanList (pu1TmpAll);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliSetPvlanAssociation                           */
/*                                                                           */
/* Description        : This function is used to delete the associated       */
/*                      secondary vlans from the given primary vlan.         */
/*                                                                           */
/* Input(s)           : CliHandle - Handle to the cli context.               */
/*                      InPrimaryVlanId - Primary Vlan Identifier            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliDeleteAllAssocSecVlans (tCliHandle CliHandle, UINT4 u4ContextId,
                               tVlanId InPrimaryVlanId)
{
    UINT2              *pu2SecVlan = NULL;
    UINT4               u4CurrVlanIndex = 0;
    UINT4               u4NextVlanIndex = 0;
    UINT4               u4NextContextId = 0;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4PrimaryVlanId = 0;
    UINT2               u2VlanIndex1 = 0;
    UINT2               u2VlanIndex2 = 0;

    UNUSED_PARAM (CliHandle);

    pu2SecVlan = MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);

    if (pu2SecVlan == NULL)
    {
        return CLI_FAILURE;
    }

    while (nmhGetNextIndexFsDot1qVlanStaticTable ((INT4) u4ContextId,
                                                  (INT4 *) &u4NextContextId,
                                                  u4CurrVlanIndex,
                                                  &u4NextVlanIndex)
           == SNMP_SUCCESS)
    {
        if (u4ContextId != u4NextContextId)
        {
            break;
        }
        u4CurrVlanIndex = u4NextVlanIndex;

        nmhGetFsMIDot1qFutureStVlanVid ((INT4) u4ContextId,
                                        u4CurrVlanIndex, &i4PrimaryVlanId);
        /* Since the context is released inside the above MI routines,
         * here we need to select the context */
        VlanSelectContext (u4ContextId);
        if (i4PrimaryVlanId == InPrimaryVlanId)
        {
            pu2SecVlan[u2VlanIndex1] = (UINT2) u4CurrVlanIndex;
            u2VlanIndex1++;
        }
    }

    /* Since the context is released inside the above MI routines,
     * here we need to select the context */
    VlanSelectContext (u4ContextId);
    for (u2VlanIndex2 = 0; u2VlanIndex2 < u2VlanIndex1; u2VlanIndex2++)
    {
        if (pu2SecVlan[u2VlanIndex2] != 0)
        {
            /* Dis-associate all the secondary vlans from the primary vlan */
            i4RetVal = CLI_FAILURE;
            break;
        }
    }
    if (i4RetVal == CLI_FAILURE)
    {
        /* If it is failed, then again configure the older association
         * and return. */
        for (u2VlanIndex2 = 0; u2VlanIndex2 < u2VlanIndex1; u2VlanIndex2++)
        {
            if (pu2SecVlan[u2VlanIndex2] != 0)
            {
                MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                    (UINT1 *) pu2SecVlan);
                pu2SecVlan = NULL;
                return CLI_FAILURE;
            }
        }
    }
    MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                        (UINT1 *) pu2SecVlan);
    pu2SecVlan = NULL;

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliShowPrivateVlan                               */
/*                                                                           */
/* Description        : This function is used to display the private vlan    */
/*                      configurations.                                      */
/*                                                                           */
/* Input(s)           : CliHandle - Handle to the cli context.               */
/*                      i4ContextId - Context identifier. It takes the value */
/*                                    as -1 or 0 to 255. -1 represents the   */
/*                                    invalid context.                       */
/*                      VlanId - Vlan Identifier                             */
/*                      u1Type - Type of the pvlan that configured by the    */
/*                               administrator. It takes the value as        */
/*                               L2IWF_PRIMARY_VLAN                          */
/*                               L2IWF_ISOLATED_VLAN                         */
/*                               L2IWF_COMMUNITY_VLAN                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliShowPrivateVlan (tCliHandle CliHandle, INT4 i4ContextId, UINT1 u1Type)
{

    if (i4ContextId == -1)
    {
        if (u1Type == L2IWF_INVALID_VLAN_TYPE)
        {
            VlanCliShowAllPrivateVlan (CliHandle, i4ContextId,
                                       VLAN_SHOW_ALL_CTXT);
        }
        else
        {
            VlanCliShowPvlansOnType (CliHandle, i4ContextId, u1Type,
                                     VLAN_SHOW_ALL_CTXT);
        }
    }
    else
    {
        if (u1Type == L2IWF_INVALID_VLAN_TYPE)
        {
            VlanCliShowAllPrivateVlan (CliHandle, i4ContextId,
                                       VLAN_SHOW_SINGLE_CTXT);
        }
        else
        {
            VlanCliShowPvlansOnType (CliHandle, i4ContextId, u1Type,
                                     VLAN_SHOW_SINGLE_CTXT);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliShowAllPrivateVlan                            */
/*                                                                           */
/* Description        : This function is used to display the private vlan    */
/*                      configurations from all the contexts.                */
/*                                                                           */
/* Input(s)           : NONE.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
VlanCliShowAllPrivateVlan (tCliHandle CliHandle, INT4 i4ContextId,
                           UINT1 u1ActionFlag)
{
    tSNMP_OCTET_STRING_TYPE PvlanPorts;
    tPortList          *pMemberPorts = NULL;
    UINT4               u4CurrContextId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4CurrVlanIndex = 0;
    UINT4               u4NextVlanIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4PrvIfIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4PrimaryVlanId = 0;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    INT4                i4Type = 0;
    INT4                i4PortType = 0;

    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);
    MEMSET (&PvlanPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    pMemberPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pMemberPorts == NULL)
    {
        return;
    }
    MEMSET ((*pMemberPorts), 0, sizeof (tPortList));

    if (u1ActionFlag == VLAN_SHOW_SINGLE_CTXT)
    {
        u4CurrContextId = (UINT4) i4ContextId;
    }
    VcmGetAliasName (u4NextContextId, au1ContextName);
    CliPrintf (CliHandle, "switch %s\r\n", au1ContextName);
    CliPrintf (CliHandle, "VlanId   Type    Primary VlanId  Ports\r\n");
    CliPrintf (CliHandle, "-------  ------  --------------  -----\r\n");
    while (nmhGetNextIndexFsDot1qVlanStaticTable ((INT4) u4CurrContextId,
                                                  (INT4 *) &u4NextContextId,
                                                  u4CurrVlanIndex,
                                                  &u4NextVlanIndex)
           == SNMP_SUCCESS)
    {
        if (u1ActionFlag == VLAN_SHOW_ALL_CTXT)
        {
            if (u4CurrContextId != u4NextContextId)
            {
                VcmGetAliasName (u4NextContextId, au1ContextName);
                CliPrintf (CliHandle, "switch %s\r\n", au1ContextName);
                CliPrintf (CliHandle, "VlanId   Type        Primary VlanId");
                CliPrintf (CliHandle, "   Ports\r\n");
                CliPrintf (CliHandle, "-------  -------     --------------");
                CliPrintf (CliHandle, "  ------\r\n");
            }
        }
        else
        {
            if (u4CurrContextId != u4NextContextId)
            {
                break;
            }
        }

        u4CurrContextId = u4NextContextId;
        u4CurrVlanIndex = u4NextVlanIndex;

        nmhGetFsMIDot1qFutureStVlanType ((INT4) u4CurrContextId,
                                         u4CurrVlanIndex, &i4Type);
        nmhGetFsMIDot1qFutureStVlanVid ((INT4) u4CurrContextId,
                                        u4CurrVlanIndex, &i4PrimaryVlanId);

        if (i4Type != L2IWF_NORMAL_VLAN)
        {
            CliPrintf (CliHandle, "%-9d", u4CurrVlanIndex);

            switch (i4Type)
            {
                case L2IWF_ISOLATED_VLAN:
                    CliPrintf (CliHandle, "isolated  ");
                    break;
                case L2IWF_COMMUNITY_VLAN:
                    CliPrintf (CliHandle, "community ");
                    break;
                case L2IWF_PRIMARY_VLAN:
                    CliPrintf (CliHandle, "primary   ");
                    break;
                default:
                    break;
            }
            if (i4PrimaryVlanId != 0)
            {
                CliPrintf (CliHandle, "%-9d", i4PrimaryVlanId);
            }
            else
            {
                CliPrintf (CliHandle, "%-9s", "-");
            }
            if (VlanGetFirstPortInContext (u4CurrContextId,
                                           &u4IfIndex) == VLAN_SUCCESS)
            {
                do
                {
                    if (nmhGetFsDot1qVlanStaticPort (u4CurrContextId,
                                                     u4CurrVlanIndex,
                                                     u4IfIndex,
                                                     &i4PortType)
                        == SNMP_SUCCESS)
                    {
                        if ((i4PortType == VLAN_ADD_TAGGED_PORT) ||
                            (i4PortType == VLAN_ADD_UNTAGGED_PORT))
                        {
                            VLAN_SET_MEMBER_PORT ((*pMemberPorts), u4IfIndex);
                        }
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInContext
                       ((UINT4) u4CurrContextId, u4PrvIfIndex,
                        &u4IfIndex) == VLAN_SUCCESS);
            }

            PvlanPorts.pu1_OctetList = (UINT1 *) pMemberPorts;
            PvlanPorts.i4_Length = sizeof (tPortList);

            CliPrintf (CliHandle, "%4s", " ");
            CliOctetToIfName (CliHandle,
                              NULL,
                              &PvlanPorts,
                              BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                              sizeof (tPortList), 32, &u4PagingStatus, 3);
        }
        MEMSET ((*pMemberPorts), 0, sizeof (tPortList));
    }
    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);

    return;
}

/*****************************************************************************/
/* Function Name      : VlanCliShowPvlansOnType                              */
/*                                                                           */
/* Description        : This function is used to display the private vlan    */
/*                      configurations from all the contexts based on the    */
/*                      given vlan type.                                     */
/*                                                                           */
/* Input(s)           : u1Type - Type of the pvlan that configured by the    */
/*                               administrator. It takes the value as        */
/*                               L2IWF_PRIMARY_VLAN                          */
/*                               L2IWF_ISOLATED_VLAN                         */
/*                               L2IWF_COMMUNITY_VLAN                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
VlanCliShowPvlansOnType (tCliHandle CliHandle, INT4 i4ContextId,
                         UINT1 u1PvlanType, UINT1 u1ActionFlag)
{
    tSNMP_OCTET_STRING_TYPE PvlanPorts;
    tPortList          *pMemberPorts = NULL;
    UINT4               u4CurrContextId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4CurrVlanIndex = 0;
    UINT4               u4NextVlanIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4IfIndex = 0;
    UINT4               u4PrvIfIndex = 0;
    INT4                i4PrimaryVlanId = 0;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    INT4                i4Type;
    INT4                i4PortType = 0;

    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);
    MEMSET (&PvlanPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    pMemberPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pMemberPorts == NULL)
    {
        return;
    }
    MEMSET ((*pMemberPorts), 0, sizeof (tPortList));
    if (u1ActionFlag == VLAN_SHOW_SINGLE_CTXT)
    {
        u4CurrContextId = (UINT4) i4ContextId;
    }
    VcmGetAliasName (u4NextContextId, au1ContextName);
    CliPrintf (CliHandle, "switch %s\r\n", au1ContextName);
    CliPrintf (CliHandle, "VlanId   Type    Primary VlanId  Ports\r\n");
    CliPrintf (CliHandle, "-------  ------  --------------  -----\r\n");
    while (nmhGetNextIndexFsDot1qVlanStaticTable ((INT4) u4CurrContextId,
                                                  (INT4 *) &u4NextContextId,
                                                  u4CurrVlanIndex,
                                                  &u4NextVlanIndex)
           == SNMP_SUCCESS)
    {
        if (u1ActionFlag == VLAN_SHOW_ALL_CTXT)
        {
            if (u4CurrContextId != u4NextContextId)
            {
                VcmGetAliasName (u4NextContextId, au1ContextName);
                CliPrintf (CliHandle, "switch %s\r\n", au1ContextName);
                CliPrintf (CliHandle, "VlanId   Type        Primary VlanId");
                CliPrintf (CliHandle, "  Ports\r\n");
                CliPrintf (CliHandle, "-------  -------     --------------");
                CliPrintf (CliHandle, " --------\r\n");
            }
        }
        else
        {
            if (u4CurrContextId != u4NextContextId)
            {
                break;
            }
        }
        u4CurrContextId = u4NextContextId;
        u4CurrVlanIndex = u4NextVlanIndex;

        nmhGetFsMIDot1qFutureStVlanType ((INT4) u4CurrContextId,
                                         u4CurrVlanIndex, &i4Type);
        nmhGetFsMIDot1qFutureStVlanVid ((INT4) u4CurrContextId,
                                        u4CurrVlanIndex,
                                        (INT4 *) &i4PrimaryVlanId);
        if (u1PvlanType == i4Type)
        {
            CliPrintf (CliHandle, "%-9d", u4CurrVlanIndex);

            switch (i4Type)
            {
                case L2IWF_ISOLATED_VLAN:
                    CliPrintf (CliHandle, "isolated  ");
                    break;
                case L2IWF_COMMUNITY_VLAN:
                    CliPrintf (CliHandle, "community ");
                    break;
                case L2IWF_PRIMARY_VLAN:
                    CliPrintf (CliHandle, "primary   ");
                    break;
                default:
                    break;
            }
            if (i4PrimaryVlanId != 0)
            {
                CliPrintf (CliHandle, "%-9d", i4PrimaryVlanId);
            }
            else
            {
                CliPrintf (CliHandle, "%-9s", " - ");
            }
            if (VlanGetFirstPortInContext (u4CurrContextId,
                                           &u4IfIndex) == VLAN_SUCCESS)
            {
                do
                {
                    if (nmhGetFsDot1qVlanStaticPort (u4CurrContextId,
                                                     u4CurrVlanIndex,
                                                     u4IfIndex,
                                                     &i4PortType)
                        == SNMP_SUCCESS)
                    {
                        if ((i4PortType == VLAN_ADD_TAGGED_PORT) ||
                            (i4PortType == VLAN_ADD_UNTAGGED_PORT))
                        {
                            VLAN_SET_MEMBER_PORT ((*pMemberPorts), u4IfIndex);
                        }
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInContext
                       ((UINT4) u4CurrContextId, u4PrvIfIndex,
                        &u4IfIndex) == VLAN_SUCCESS);
            }

            PvlanPorts.pu1_OctetList = (UINT1 *) pMemberPorts;
            PvlanPorts.i4_Length = sizeof (tPortList);

            CliPrintf (CliHandle, "%4s", " ");
            CliOctetToIfName (CliHandle,
                              NULL,
                              &PvlanPorts,
                              BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                              sizeof (tPortList), 32, &u4PagingStatus, 3);
        }
        MEMSET ((*pMemberPorts), 0, sizeof (tPortList));
    }
    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanCliAssociateHostPort                             */
/*                                                                           */
/* Description        : This function is used to configure the member port   */
/*                      for primary and secondary vlans. it will be invoked  */
/*                      when the host port association command is issued.    */
/*                                                                           */
/* Input(s)           : CliHandle - Cli handle.                              */
/*                      u2LocalPortId - Local port which is mapping to the   */
/*                               primary vlan as well as secondary vlan.     */
/*                      PrimaryVlanId - Primary vlan identifier.             */
/*                      SecondaryVlanId - Secondary vlan identifier.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliAssociateHostPort (tCliHandle CliHandle, UINT2 u2LocalPortId,
                          tVlanId PrimaryVlanId, tVlanId SecondaryVlanId)
{
    UINT4               u4ContextId = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4IsPrimaryVlanPort = VLAN_NON_MEMBER_PORT;
    INT4                i4IsSecondaryVlanPort = VLAN_NON_MEMBER_PORT;
    INT4                i4PvlanType = 0;
    INT4                i4PortType;

    UNUSED_PARAM (CliHandle);

    u4ContextId = VLAN_CURR_CONTEXT_ID ();

    nmhGetFsMIDot1qFutureVlanPortType (VLAN_GET_PHY_PORT (u2LocalPortId),
                                       &i4PortType);

    /* If the port type is not host port type then return failure */
    if (i4PortType != VLAN_HOST_PORT)
    {
        CliPrintf (CliHandle, "\r%%Port is not a host port\r\n");
        return CLI_FAILURE;
    }

    nmhGetFsMIDot1qFutureStVlanType (u4ContextId, PrimaryVlanId, &i4PvlanType);
    if (i4PvlanType != L2IWF_PRIMARY_VLAN)
    {
        CLI_SET_ERR (CLI_VLAN_PVLAN_PRIMARY_TYPE_ERR);
        return CLI_FAILURE;
    }

    nmhGetFsMIDot1qFutureStVlanType (u4ContextId, SecondaryVlanId,
                                     &i4PvlanType);
    if ((i4PvlanType != L2IWF_ISOLATED_VLAN)
        && (i4PvlanType != L2IWF_COMMUNITY_VLAN))
    {
        CLI_SET_ERR (CLI_VLAN_PVLAN_SECONDARY_TYPE_ERR);
        return CLI_FAILURE;
    }
    nmhGetFsDot1qVlanStaticPort (u4ContextId, PrimaryVlanId, u2LocalPortId,
                                 &i4IsPrimaryVlanPort);

    nmhGetFsDot1qVlanStaticPort (u4ContextId, PrimaryVlanId, u2LocalPortId,
                                 &i4IsSecondaryVlanPort);

    if ((i4IsPrimaryVlanPort == VLAN_NON_MEMBER_PORT) ||
        (i4IsPrimaryVlanPort == VLAN_ADD_FORBIDDEN_PORT))
    {
        if (nmhTestv2FsDot1qVlanStaticPort (&u4ErrCode,
                                            u4ContextId, PrimaryVlanId,
                                            u2LocalPortId,
                                            VLAN_ADD_UNTAGGED_PORT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDot1qVlanStaticPort (u4ContextId, PrimaryVlanId,
                                         u2LocalPortId,
                                         VLAN_ADD_UNTAGGED_PORT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if ((i4IsSecondaryVlanPort == VLAN_NON_MEMBER_PORT) ||
        (i4IsSecondaryVlanPort == VLAN_ADD_FORBIDDEN_PORT))
    {
        if (nmhTestv2FsDot1qVlanStaticPort (&u4ErrCode,
                                            u4ContextId, SecondaryVlanId,
                                            u2LocalPortId,
                                            VLAN_ADD_UNTAGGED_PORT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDot1qVlanStaticPort (u4ContextId, SecondaryVlanId,
                                         u2LocalPortId,
                                         VLAN_ADD_UNTAGGED_PORT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliDisAssociateHostPort                          */
/*                                                                           */
/* Description        : This function is used to remove the host member port */
/*                      from the primary and secondary vlans. it will be     */
/*                      invoked when the host port disassociation command is */
/*                      issued.                                              */
/*                                                                           */
/* Input(s)           : CliHandle - Cli handle.                              */
/*                      u2LocalPortId - Local port which is unmapping from   */
/*                                      the primary vlan as well as          */
/*                                      secondary vlan.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliDisAssociateHostPort (tCliHandle CliHandle, UINT2 u2LocalPortId)
{
    UINT2              *pu2VlanList = NULL;
    UINT4               u4ContextId;
    UINT4               u4CurrContextId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4CurrVlanIndex = 0;
    UINT4               u4NextVlanIndex = 0;
    UINT4               u4ErrCode;
    INT4                i4StaticPort;
    INT4                i4IsVlanMemberPort = VLAN_NON_MEMBER_PORT;
    UINT2               u2VlanIndex = 0;
    UINT2               u2VlanIndex2 = 0;
    INT4                i4VlanType = 0;

    UNUSED_PARAM (CliHandle);

    u4ContextId = VLAN_CURR_CONTEXT_ID ();

    pu2VlanList = MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);

    if (pu2VlanList == NULL)
    {
        return CLI_FAILURE;
    }

    while (nmhGetNextIndexFsDot1qVlanStaticTable ((INT4) u4ContextId,
                                                  (INT4 *) &u4NextContextId,
                                                  u4CurrVlanIndex,
                                                  &u4NextVlanIndex)
           == SNMP_SUCCESS)
    {
        if (u4ContextId != u4NextContextId)
        {
            break;
        }
        u4CurrVlanIndex = u4NextVlanIndex;

        nmhGetFsDot1qVlanStaticPort (u4ContextId, u4CurrVlanIndex,
                                     u2LocalPortId, &i4IsVlanMemberPort);

        if (i4IsVlanMemberPort != VLAN_NON_MEMBER_PORT)
        {
            nmhGetFsMIDot1qFutureStVlanType ((INT4) u4CurrContextId,
                                             u4CurrVlanIndex, &i4VlanType);
            /* Since the context is released inside the above MI routines,
             * here we need to select the context */
            VlanSelectContext (u4ContextId);
            if (i4VlanType == L2IWF_NORMAL_VLAN)
            {
                continue;
            }
            if (i4IsVlanMemberPort == VLAN_ADD_UNTAGGED_PORT)
            {
                i4StaticPort = VLAN_DEL_UNTAGGED_PORT;
            }
            else
            {
                i4StaticPort = VLAN_DEL_TAGGED_PORT;
            }
            if (nmhTestv2FsDot1qVlanStaticPort (&u4ErrCode, u4CurrContextId,
                                                u4CurrVlanIndex, u2LocalPortId,
                                                i4StaticPort) == SNMP_FAILURE)
            {
                MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                    (UINT1 *) pu2VlanList);
                pu2VlanList = NULL;
                return CLI_FAILURE;
            }
            /* Store the private vlans, in which that this port is
             * configured as member port */
            pu2VlanList[u2VlanIndex] = (UINT2) u4CurrVlanIndex;
            u2VlanIndex++;
        }
    }
    /* Loop the private vlans alone and remove the port */
    for (u2VlanIndex2 = 0; u2VlanIndex2 < u2VlanIndex; u2VlanIndex2++)
    {
        nmhGetFsDot1qVlanStaticPort (u4ContextId, pu2VlanList[u2VlanIndex2],
                                     u2LocalPortId, &i4IsVlanMemberPort);

        /* Since the context is released inside the above MI routines,
         * here we need to select the context */
        VlanSelectContext (u4ContextId);
        if (i4IsVlanMemberPort != VLAN_NON_MEMBER_PORT)
        {
            if (i4IsVlanMemberPort == VLAN_ADD_UNTAGGED_PORT)
            {
                i4StaticPort = VLAN_DEL_UNTAGGED_PORT;
            }
            else
            {
                i4StaticPort = VLAN_DEL_TAGGED_PORT;
            }
            if (nmhSetFsDot1qVlanStaticPort (u4CurrContextId,
                                             pu2VlanList[u2VlanIndex2],
                                             u2LocalPortId, i4StaticPort)
                == SNMP_FAILURE)
            {
                MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                    (UINT1 *) pu2VlanList);
                pu2VlanList = NULL;
                return CLI_FAILURE;
            }
        }
    }
    MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                        (UINT1 *) pu2VlanList);
    pu2VlanList = NULL;
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliAssocPromiscuousPort                          */
/*                                                                           */
/* Description        : This function is used to configure the promiscuous   */
/*                      member port to the primary and secondary vlans.      */
/*                                                                           */
/* Input(s)           : CliHandle - Cli handle.                              */
/*                      u2LocalPortId - Local port which is mapping to       */
/*                                      the primary vlan as well as          */
/*                                      secondary vlan.                      */
/*                      PrimaryVlanId - Primary vlan id.                     */
/*                      pu1VlanList - Secondary vlan list.                   */
/*                      u1ActionFlag - It will take the values as VLAN_ADD / */
/*                                     VLAN_DELETE / VLAN_UPDATE.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliAssocPromiscuousPort (tCliHandle CliHandle, UINT2 u2LocalPortId,
                             tVlanId PrimaryVlanId, UINT1 u1ActionFlag,
                             UINT1 *pu1VlanList)
{
    UINT4               u4ContextId;
    UINT4               u4NextContextId = 0;
    UINT4               u4CurrVlanIndex = 0;
    UINT4               u4NextVlanIndex = 0;
    UINT4               u4ErrCode;
    INT4                i4StaticPort = VLAN_NON_MEMBER_PORT;
    INT4                i4IsVlanMemberPort = VLAN_NON_MEMBER_PORT;
    UINT2               u2ByteInd = 0;
    UINT2               u2VlanIndex = 0;
    UINT2               u2VlanIndex2 = 0;
    UINT2              *pu2VlanList = NULL;
    INT4                i4PortType = 0;
    UINT1              *pu1LocalVlanList = NULL;
    UINT1              *pu1AddedVlans = NULL;
    UINT1              *pu1DeletedVlans = NULL;
    INT4                i4VlanType = 0;
    BOOL1               b1Result = VLAN_FALSE;

    UNUSED_PARAM (CliHandle);

    pu1LocalVlanList = UtlShMemAllocVlanList ();

    if (pu1LocalVlanList == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    MEMSET (pu1LocalVlanList, 0, VLAN_LIST_SIZE);

    u4ContextId = VLAN_CURR_CONTEXT_ID ();

    nmhGetFsMIDot1qFutureVlanPortType (VLAN_GET_PHY_PORT (u2LocalPortId),
                                       &i4PortType);

    /* If the given port is not of promoscuous port then return failure */
    if (i4PortType != VLAN_PROMISCOUS_PORT)
    {
        UtlShMemFreeVlanList (pu1LocalVlanList);
        return CLI_FAILURE;
    }

    if (u1ActionFlag == VLAN_ADD)
    {
        /* Add the given port to the primary vlan */
        if (nmhTestv2FsDot1qVlanStaticPort (&u4ErrCode,
                                            u4ContextId, PrimaryVlanId,
                                            u2LocalPortId,
                                            VLAN_ADD_UNTAGGED_PORT)
            == SNMP_FAILURE)
        {
            UtlShMemFreeVlanList (pu1LocalVlanList);
            return CLI_FAILURE;
        }

        if (nmhSetFsDot1qVlanStaticPort (u4ContextId, PrimaryVlanId,
                                         u2LocalPortId,
                                         VLAN_ADD_UNTAGGED_PORT)
            == SNMP_FAILURE)
        {
            UtlShMemFreeVlanList (pu1LocalVlanList);
            return CLI_FAILURE;
        }

        pu2VlanList =
            MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);

        if (pu2VlanList == NULL)
        {
            UtlShMemFreeVlanList (pu1LocalVlanList);
            return CLI_FAILURE;
        }

        /* Add the given port in to the list of secondary vlans */
        for (u2VlanIndex = 1; u2VlanIndex <= VLAN_MAX_VLAN_ID; u2VlanIndex++)
        {
            OSIX_BITLIST_IS_BIT_SET (pu1VlanList, u2VlanIndex, VLAN_LIST_SIZE,
                                     b1Result);

            if (b1Result == OSIX_TRUE)
            {
                if (nmhTestv2FsDot1qVlanStaticPort (&u4ErrCode,
                                                    u4ContextId, u2VlanIndex,
                                                    u2LocalPortId,
                                                    VLAN_ADD_UNTAGGED_PORT)
                    == SNMP_FAILURE)
                {
                    MemReleaseMemBlock
                        (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                         (UINT1 *) pu2VlanList);
                    pu2VlanList = NULL;
                    UtlShMemFreeVlanList (pu1LocalVlanList);
                    return CLI_FAILURE;
                }
                pu2VlanList[u2VlanIndex2] = u2VlanIndex;
                u2VlanIndex2++;
            }
        }
        for (u2VlanIndex = 0; u2VlanIndex < u2VlanIndex2; u2VlanIndex++)
        {

            if (nmhSetFsDot1qVlanStaticPort (u4ContextId,
                                             pu2VlanList[u2VlanIndex],
                                             u2LocalPortId,
                                             VLAN_ADD_UNTAGGED_PORT)
                == SNMP_FAILURE)
            {
                MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                    (UINT1 *) pu2VlanList);
                pu2VlanList = NULL;
                UtlShMemFreeVlanList (pu1LocalVlanList);
                return CLI_FAILURE;
            }
        }
        MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                            (UINT1 *) pu2VlanList);
        pu2VlanList = NULL;
    }
    else if (u1ActionFlag == VLAN_DELETE)
    {
        pu2VlanList =
            MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);

        if (pu2VlanList == NULL)
        {
            UtlShMemFreeVlanList (pu1LocalVlanList);
            return CLI_FAILURE;
        }
        /* Delete from the given vlan list */
        for (u2VlanIndex = 1; u2VlanIndex <= VLAN_MAX_VLAN_ID; u2VlanIndex++)
        {
            OSIX_BITLIST_IS_BIT_SET (pu1VlanList, u2VlanIndex, VLAN_LIST_SIZE,
                                     b1Result);
            if (b1Result == OSIX_TRUE)
            {
                nmhGetFsDot1qVlanStaticPort (u4ContextId, u2VlanIndex,
                                             u2LocalPortId,
                                             &i4IsVlanMemberPort);
                if (i4IsVlanMemberPort != VLAN_NON_MEMBER_PORT)
                {
                    if (i4IsVlanMemberPort == VLAN_ADD_UNTAGGED_PORT)
                    {
                        i4StaticPort = VLAN_DEL_UNTAGGED_PORT;
                    }
                    else
                    {
                        i4StaticPort = VLAN_DEL_TAGGED_PORT;
                    }
                    if (nmhTestv2FsDot1qVlanStaticPort (&u4ErrCode,
                                                        u4ContextId,
                                                        u2VlanIndex,
                                                        u2LocalPortId,
                                                        i4StaticPort) ==
                        SNMP_FAILURE)
                    {
                        MemReleaseMemBlock
                            (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                             (UINT1 *) pu2VlanList);
                        pu2VlanList = NULL;
                        UtlShMemFreeVlanList (pu1LocalVlanList);
                        return CLI_FAILURE;
                    }
                    pu2VlanList[u2VlanIndex2] = u2VlanIndex;
                    u2VlanIndex2++;
                }
            }
        }
        for (u2VlanIndex = 0; u2VlanIndex < u2VlanIndex2; u2VlanIndex++)
        {
            if (nmhSetFsDot1qVlanStaticPort (u4ContextId,
                                             pu2VlanList[u2VlanIndex],
                                             u2LocalPortId,
                                             i4StaticPort) == SNMP_FAILURE)
            {
                MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                    (UINT1 *) pu2VlanList);
                pu2VlanList = NULL;
                UtlShMemFreeVlanList (pu1LocalVlanList);
                return CLI_FAILURE;
            }
        }
        MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                            (UINT1 *) pu2VlanList);
        pu2VlanList = NULL;
    }
    else
    {
        pu2VlanList =
            MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);

        if (pu2VlanList == NULL)
        {
            UtlShMemFreeVlanList (pu1LocalVlanList);
            return CLI_FAILURE;
        }
        /* Delete from the existing vlan list */
        while (nmhGetNextIndexFsDot1qVlanStaticTable ((INT4) u4ContextId,
                                                      (INT4 *) &u4NextContextId,
                                                      u4CurrVlanIndex,
                                                      &u4NextVlanIndex)
               == SNMP_SUCCESS)
        {
            if (u4ContextId != u4NextContextId)
            {
                break;
            }
            u4CurrVlanIndex = u4NextVlanIndex;

            nmhGetFsDot1qVlanStaticPort (u4ContextId, u4CurrVlanIndex,
                                         u2LocalPortId, &i4IsVlanMemberPort);

            if (i4IsVlanMemberPort != VLAN_NON_MEMBER_PORT)
            {
                /* Form the vlan list, which the port is already member */
                OSIX_BITLIST_SET_BIT (pu1LocalVlanList, u4CurrVlanIndex,
                                      VLAN_LIST_SIZE);
            }
        }

        pu1AddedVlans = UtlShMemAllocVlanList ();
        if (pu1AddedVlans == NULL)
        {
            MemReleaseMemBlock
                (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                 (UINT1 *) pu2VlanList);
            UtlShMemFreeVlanList (pu1LocalVlanList);
            return CLI_FAILURE;
        }
        pu1DeletedVlans = UtlShMemAllocVlanList ();
        if (pu1DeletedVlans == NULL)
        {
            MemReleaseMemBlock
                (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                 (UINT1 *) pu2VlanList);
            UtlShMemFreeVlanList (pu1AddedVlans);
            UtlShMemFreeVlanList (pu1LocalVlanList);
            return CLI_FAILURE;
        }

        for (u2ByteInd = 0; u2ByteInd < VLAN_LIST_SIZE; u2ByteInd++)
        {
            pu1AddedVlans[u2ByteInd]
                =
                (UINT1) ((pu1VlanList[u2ByteInd]) &
                         ~(pu1LocalVlanList[u2ByteInd]));

            pu1DeletedVlans[u2ByteInd]
                =
                (UINT1) ((pu1LocalVlanList[u2ByteInd]) &
                         ~(pu1VlanList[u2ByteInd]));
        }
        for (u2VlanIndex = 1; u2VlanIndex <= VLAN_MAX_VLAN_ID; u2VlanIndex++)
        {
            OSIX_BITLIST_IS_BIT_SET (pu1DeletedVlans, u2VlanIndex,
                                     VLAN_LIST_SIZE, b1Result);
            if (b1Result == OSIX_TRUE)
            {
                nmhGetFsMIDot1qFutureStVlanType (u4ContextId,
                                                 u2VlanIndex, &i4VlanType);

                if (i4VlanType == L2IWF_NORMAL_VLAN)
                {
                    continue;
                }

                nmhGetFsDot1qVlanStaticPort (u4ContextId, u2VlanIndex,
                                             u2LocalPortId,
                                             &i4IsVlanMemberPort);

                if (i4IsVlanMemberPort == VLAN_ADD_UNTAGGED_PORT)
                {
                    i4StaticPort = VLAN_DEL_UNTAGGED_PORT;
                }
                else
                {
                    i4StaticPort = VLAN_DEL_TAGGED_PORT;
                }
                /* Since the context is released inside the above MI routines,
                 * here we need to select the context */
                VlanSelectContext (u4ContextId);
                if (nmhTestv2FsDot1qVlanStaticPort (&u4ErrCode,
                                                    u4ContextId, u2VlanIndex,
                                                    u2LocalPortId,
                                                    i4StaticPort)
                    == SNMP_FAILURE)
                {
                    MemReleaseMemBlock
                        (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                         (UINT1 *) pu2VlanList);
                    UtlShMemFreeVlanList (pu1AddedVlans);
                    UtlShMemFreeVlanList (pu1DeletedVlans);
                    pu2VlanList = NULL;
                    UtlShMemFreeVlanList (pu1LocalVlanList);
                    return CLI_FAILURE;
                }
                pu2VlanList[u2VlanIndex2] = (UINT2) u4CurrVlanIndex;
                u2VlanIndex2++;
            }
        }
        UtlShMemFreeVlanList (pu1DeletedVlans);

        /* Since the context is released inside the above MI routines,
         * here we need to select the context */
        VlanSelectContext (u4ContextId);
        for (u2VlanIndex = 0; u2VlanIndex < u2VlanIndex2; u2VlanIndex++)
        {
            if (nmhSetFsDot1qVlanStaticPort (u4ContextId,
                                             pu2VlanList[u2VlanIndex],
                                             u2LocalPortId,
                                             i4StaticPort) == SNMP_FAILURE)
            {
                MemReleaseMemBlock
                    (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                     (UINT1 *) pu2VlanList);
                UtlShMemFreeVlanList (pu1AddedVlans);
                pu2VlanList = NULL;
                UtlShMemFreeVlanList (pu1LocalVlanList);
                return CLI_FAILURE;
            }
        }

        /* Add that port to the new primary vlan */
        nmhGetFsDot1qVlanStaticPort (u4ContextId, PrimaryVlanId,
                                     u2LocalPortId, &i4IsVlanMemberPort);
        /* Since the context is released inside the above MI routines,
         * here we need to select the context */
        VlanSelectContext (u4ContextId);
        if (i4IsVlanMemberPort != VLAN_NON_MEMBER_PORT)
        {
            if (nmhTestv2FsDot1qVlanStaticPort (&u4ErrCode,
                                                u4ContextId, PrimaryVlanId,
                                                u2LocalPortId,
                                                VLAN_ADD_UNTAGGED_PORT)
                == SNMP_FAILURE)
            {
                MemReleaseMemBlock
                    (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                     (UINT1 *) pu2VlanList);
                UtlShMemFreeVlanList (pu1AddedVlans);
                pu2VlanList = NULL;
                UtlShMemFreeVlanList (pu1LocalVlanList);
                return CLI_FAILURE;
            }

            if (nmhSetFsDot1qVlanStaticPort (u4ContextId, PrimaryVlanId,
                                             u2LocalPortId,
                                             VLAN_ADD_UNTAGGED_PORT)
                == SNMP_FAILURE)
            {
                MemReleaseMemBlock
                    (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                     (UINT1 *) pu2VlanList);
                UtlShMemFreeVlanList (pu1AddedVlans);
                pu2VlanList = NULL;
                UtlShMemFreeVlanList (pu1LocalVlanList);
                return CLI_FAILURE;
            }
        }

        MEMSET (pu2VlanList, 0, (VLAN_MAX_COMMUNITY_VLANS + 1));
        /* Add the port in the added vlan list */
        for (u2VlanIndex = 1; u2VlanIndex <= VLAN_MAX_VLAN_ID; u2VlanIndex++)
        {
            OSIX_BITLIST_IS_BIT_SET (pu1AddedVlans, u2VlanIndex, VLAN_LIST_SIZE,
                                     b1Result);

            if (b1Result == OSIX_TRUE)
            {
                if (nmhTestv2FsDot1qVlanStaticPort (&u4ErrCode,
                                                    u4ContextId, u2VlanIndex,
                                                    u2LocalPortId,
                                                    VLAN_ADD_UNTAGGED_PORT)
                    == SNMP_FAILURE)
                {
                    MemReleaseMemBlock
                        (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                         (UINT1 *) pu2VlanList);
                    UtlShMemFreeVlanList (pu1AddedVlans);
                    pu2VlanList = NULL;
                    UtlShMemFreeVlanList (pu1LocalVlanList);
                    return CLI_FAILURE;
                }
                pu2VlanList[u2VlanIndex2] = u2VlanIndex;
                u2VlanIndex2++;
            }
        }
        UtlShMemFreeVlanList (pu1AddedVlans);

        for (u2VlanIndex = 0; u2VlanIndex < u2VlanIndex2; u2VlanIndex++)
        {
            if (nmhSetFsDot1qVlanStaticPort (u4ContextId,
                                             pu2VlanList[u2VlanIndex],
                                             u2LocalPortId,
                                             VLAN_ADD_UNTAGGED_PORT)
                == SNMP_FAILURE)
            {
                MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                    (UINT1 *) pu2VlanList);
                pu2VlanList = NULL;
                UtlShMemFreeVlanList (pu1LocalVlanList);
                return CLI_FAILURE;
            }
        }
        MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                            (UINT1 *) pu2VlanList);
        pu2VlanList = NULL;
    }
    UtlShMemFreeVlanList (pu1LocalVlanList);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanCliDisAssocPromicuousPort                        */
/*                                                                           */
/* Description        : This function is used to removes the promiscuous     */
/*                      member port to the primary and secondary vlans.      */
/*                                                                           */
/* Input(s)           : CliHandle - Cli handle.                              */
/*                      u2LocalPortId - Local port which is mapping to       */
/*                                      the primary vlan as well as          */
/*                                      secondary vlan.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCliDisAssocPromicuousPort (tCliHandle CliHandle, UINT2 u2LocalPortId)
{
    UINT2              *pu2SecVlan = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4CurrVlanIndex = 0;
    UINT4               u4NextVlanIndex = 0;
    INT4                i4StaticPort = VLAN_NON_MEMBER_PORT;
    INT4                i4PortType = VLAN_NON_MEMBER_PORT;
    INT4                i4VlanType = 0;
    UINT2               u2VlanIndex = 0;
    UINT2               u2VlanIndex1 = 0;
    UINT4               u4IfIndex = 0;

    UNUSED_PARAM (CliHandle);

    u4ContextId = VLAN_CURR_CONTEXT_ID ();

    pu2SecVlan = MemAllocMemBlk (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId);
    if (pu2SecVlan == NULL)
    {
        return CLI_FAILURE;
    }

    u4IfIndex = VLAN_GET_IFINDEX (u2LocalPortId);

    while (nmhGetNextIndexFsDot1qVlanStaticTable ((INT4) u4ContextId,
                                                  (INT4 *) &u4NextContextId,
                                                  u4CurrVlanIndex,
                                                  &u4NextVlanIndex)
           == SNMP_SUCCESS)
    {
        if (u4ContextId != u4NextContextId)
        {
            break;
        }
        u4CurrVlanIndex = u4NextVlanIndex;

        nmhGetFsMIDot1qFutureStVlanType (u4ContextId, u4CurrVlanIndex,
                                         &i4VlanType);
        if (i4VlanType == L2IWF_NORMAL_VLAN)
        {
            continue;
        }
        nmhGetFsDot1qVlanStaticPort (u4ContextId, u4CurrVlanIndex,
                                     u4IfIndex, &i4PortType);

        /* Since the context is released inside the above MI routines,
         * here we need to select the context */
        VlanSelectContext (u4ContextId);
        if (i4PortType != VLAN_NON_MEMBER_PORT)
        {
            if (i4PortType == VLAN_ADD_UNTAGGED_PORT)
            {
                i4StaticPort = VLAN_DEL_UNTAGGED_PORT;
            }
            else
            {
                i4StaticPort = VLAN_DEL_TAGGED_PORT;
            }
            if (nmhTestv2FsDot1qVlanStaticPort (&u4ErrCode,
                                                u4ContextId, u4CurrVlanIndex,
                                                u2LocalPortId,
                                                i4StaticPort) == SNMP_FAILURE)
            {
                MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                    (UINT1 *) pu2SecVlan);
                pu2SecVlan = NULL;
                return CLI_FAILURE;
            }
            pu2SecVlan[u2VlanIndex] = u4CurrVlanIndex;
            u2VlanIndex++;
        }
    }
    for (u2VlanIndex1 = 0; u2VlanIndex1 < u2VlanIndex; u2VlanIndex1++)
    {
        nmhGetFsDot1qVlanStaticPort (u4ContextId, pu2SecVlan[u2VlanIndex1],
                                     u4IfIndex, &i4PortType);

        if (i4PortType != VLAN_NON_MEMBER_PORT)
        {
            if (i4PortType == VLAN_ADD_UNTAGGED_PORT)
            {
                i4StaticPort = VLAN_DEL_UNTAGGED_PORT;
            }
            else
            {
                i4StaticPort = VLAN_DEL_TAGGED_PORT;
            }
            /* Since the context is released inside the above MI routines,
             * here we need to select the context */
            VlanSelectContext (u4ContextId);

            if (nmhSetFsDot1qVlanStaticPort (u4ContextId,
                                             pu2SecVlan[u2VlanIndex1],
                                             u2LocalPortId,
                                             i4StaticPort) == SNMP_FAILURE)
            {
                MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                                    (UINT1 *) pu2SecVlan);
                pu2SecVlan = NULL;
                return CLI_FAILURE;
            }
        }
    }
    MemReleaseMemBlock (gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId,
                        (UINT1 *) pu2SecVlan);
    pu2SecVlan = NULL;
    return CLI_SUCCESS;
}
#endif
