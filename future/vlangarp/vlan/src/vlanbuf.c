/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanbuf.c,v 1.37 2016/07/07 14:17:26 siva Exp $
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : vlanbuf.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains routines to allocate and      */
/*                          release buffer for VLAN module.                  */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#include "vlaninc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetBuf                                       */
/*                                                                           */
/*    Description         : This function allocates memory of the given      */
/*                          type.                                            */
/*                                                                           */
/*    Input(s)            : u1BufType - Type of memory needed.               */
/*                          u4Size    - Size in bytes                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanMem                                   */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the allocated buffer if SUCCESS        */
/*                         NULL if allocation fails                          */
/*                                                                           */
/*****************************************************************************/
UINT1              *
VlanGetBuf (UINT1 u1BufType, UINT4 u4Size)
{
    UINT1              *pu1Buf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    switch (u1BufType)
    {
        case VLAN_FID_ENTRY:

            VLAN_ALLOC_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.VlanFidPoolId,
                                  pu1Buf);

            break;
        case VLAN_PORT_ENTRY:

            VLAN_ALLOC_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.VlanPortPoolId,
                                  pu1Buf);

            break;
        case VLAN_CONTEXT_INFO:

            VLAN_ALLOC_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.
                                  VlanContextPoolId, pu1Buf);

            break;

        case VLAN_CURR_ENTRY:

            VLAN_ALLOC_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.VlanCurrPoolId,
                                  pu1Buf);

            break;

        case VLAN_FDB_ENTRY:
            /* Delibrate fall through. Same mempool is used for Fdb Enry and
             * for Fdb Info. If Fdb Entry is used then Fdb Info will not be
             * used. */
        case VLAN_FDB_INFO:

            VLAN_ALLOC_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.VlanFdbPoolId,
                                  pu1Buf);

            break;

        case VLAN_ST_UCAST_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanStUcastPoolId, pu1Buf);

            break;

        case VLAN_ST_MCAST_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanStMcastPoolId, pu1Buf);

            break;

        case VLAN_GROUP_ENTRY:

            VLAN_ALLOC_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.VlanGroupPoolId,
                                  pu1Buf);

            break;

        case VLAN_MAC_MAP_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanMacMapPoolId, pu1Buf);

            break;

        case VLAN_SUBNET_MAP_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanSubnetMapPoolId, pu1Buf);

            break;

        case VLAN_ST_VLAN_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanStEntriesPoolId, pu1Buf);

            break;
#ifndef NPAPI_WANTED
        case VLAN_PORT_STATS_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanPortStatsPoolId, pu1Buf);

            break;

        case VLAN_STATS_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanStatsPoolId, pu1Buf);

            break;
#endif /*NPAPI_WANTED */

        case VLAN_MAC_CONTROL_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanMacControlPoolId, pu1Buf);

            break;

        case VLAN_MESSAGE_BUFFER:

            pBuf = VLAN_ALLOCATE_CRU_BUF (u4Size, 0);

            pu1Buf = (UINT1 *) pBuf;

            break;

        case VLAN_PROTO_GROUP_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanProtoGroupPoolId, pu1Buf);

            break;

        case VLAN_PORT_PROTO_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanPortProtoPoolId, pu1Buf);

            break;

        case VLAN_QMSG_BUFF:

            VLAN_ALLOC_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.VlanQMsgPoolId,
                                  pu1Buf);
            break;

        case VLAN_Q_IN_PKT_BUFF:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanPktQPoolId, pu1Buf);

            break;

        case VLAN_WILD_CARD_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanWildCardPoolId, pu1Buf);

            break;

        case VLAN_MISC:

            VLAN_ALLOC_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.VlanMiscPoolId,
                                  pu1Buf);
            break;

        case VLAN_TEMP_PORTLIST_ENTRY:

            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanTmpPortListId, pu1Buf);

            break;

#ifdef L2RED_WANTED
        case VLAN_RED_GVRP_LEARNT_BUFF:

            VLAN_ALLOC_MEM_BLOCK (gVlanRedTaskInfo.VlanLrntPoolId, pu1Buf);
            break;

        case VLAN_RED_GMRP_LEARNT_BUFF:

            VLAN_ALLOC_MEM_BLOCK (gVlanRedTaskInfo.GrpLrntPoolId, pu1Buf);
            break;

        case VLAN_RED_GVRP_DEL_BUFF:

            VLAN_ALLOC_MEM_BLOCK (gVlanRedTaskInfo.VlanDelPoolId, pu1Buf);
            break;

        case VLAN_RED_GMRP_DEL_BUFF:

            VLAN_ALLOC_MEM_BLOCK (gVlanRedTaskInfo.GrpDelPoolId, pu1Buf);
            break;
#endif /* L2RED_WANTED */
        case VLAN_BASE_PORT_ENTRY:

            VLAN_ALLOC_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.VlanBasePortPoolId,
                                  pu1Buf);
            break;

        case VLAN_PORT_VLAN_MAP_ENTRY:
            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.PortVlanMapPoolId, pu1Buf);
            break;

        case VLAN_HW_STATS_ENTRY:

            VLAN_ALLOC_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.
                                  VlanHwStatsMapPoolId, pu1Buf);
            break;
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
        case VLAN_RED_CACHE_ENTRY:
            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanRedCachePoolId, pu1Buf);
            break;
        case VLAN_RED_ST_UCAST_CACHE_ENTRY:
            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanRedStUcastCachePoolId, pu1Buf);
            break;
        case VLAN_RED_MCAST_CACHE_ENTRY:
            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanRedStMcastCachePoolId, pu1Buf);
            break;

        case VLAN_RED_PROTO_VLAN_CACHE_ENTRY:
            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanRedProtoVlanCachePoolId, pu1Buf);
            break;
#endif /* defined L2RED_WANTED && NPAPI_WANTED */
        case VLAN_MCAG_QMSG_BUFF:
            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanMcagQMsgPoolId ,pu1Buf);
            break;
        case VLAN_MCAG_MAC_AGING_ENTRIES:
            VLAN_ALLOC_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanMcagMacAgingEntryPoolId ,pu1Buf);
            break;
        default:
            break;
    }

    return pu1Buf;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanReleaseBuf                                   */
/*                                                                           */
/*    Description         : This function releases the allocated memory to   */
/*                          the free pool specified by the buffer type.      */
/*                                                                           */
/*    Input(s)            : u1BufType - Type of memory.                      */
/*                          pu1Buf    - Buffer to be released.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanMem                                   */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanReleaseBuf (UINT1 u1BufType, UINT1 *pu1Buf)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    switch (u1BufType)
    {
        case VLAN_FID_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanFidPoolId, pu1Buf);
            break;

        case VLAN_PORT_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanPortPoolId, pu1Buf);
            break;

        case VLAN_CONTEXT_INFO:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanContextPoolId, pu1Buf);
            break;

        case VLAN_CURR_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanCurrPoolId, pu1Buf);
            break;

        case VLAN_FDB_ENTRY:
            /* Delibrate fall through. Same mempool is used for Fdb Enry and
             * for Fdb Info. If Fdb Entry is used then Fdb Info will not be
             * used. */
        case VLAN_FDB_INFO:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanFdbPoolId, pu1Buf);

            break;

        case VLAN_ST_UCAST_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanStUcastPoolId, pu1Buf);

            break;

        case VLAN_ST_MCAST_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanStMcastPoolId, pu1Buf);

            break;

        case VLAN_GROUP_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanGroupPoolId, pu1Buf);

            break;

        case VLAN_MAC_MAP_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanMacMapPoolId, pu1Buf);

            break;

        case VLAN_SUBNET_MAP_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanSubnetMapPoolId, pu1Buf);

            break;

        case VLAN_ST_VLAN_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanStEntriesPoolId, pu1Buf);

            break;
#ifndef NPAPI_WANTED
        case VLAN_PORT_STATS_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanPortStatsPoolId, pu1Buf);

            break;

        case VLAN_STATS_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanStatsPoolId, pu1Buf);

            break;
#endif /*NPAPI_WANTED */

        case VLAN_MAC_CONTROL_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanMacControlPoolId, pu1Buf);

            break;

        case VLAN_MESSAGE_BUFFER:

            pBuf = (tCRU_BUF_CHAIN_HEADER *) (VOID *) pu1Buf;

            VLAN_RELEASE_CRU_BUF (pBuf);

            break;

        case VLAN_PROTO_GROUP_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanProtoGroupPoolId, pu1Buf);

            break;

        case VLAN_PORT_PROTO_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanPortProtoPoolId, pu1Buf);

            break;

        case VLAN_QMSG_BUFF:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanQMsgPoolId, pu1Buf);
            break;

        case VLAN_Q_IN_PKT_BUFF:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanPktQPoolId, pu1Buf);
            break;

        case VLAN_WILD_CARD_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanWildCardPoolId, pu1Buf);

            break;

        case VLAN_MISC:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanMiscPoolId, pu1Buf);
            break;

        case VLAN_TEMP_PORTLIST_ENTRY:

            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanTmpPortListId, pu1Buf);
            break;

#ifdef L2RED_WANTED
        case VLAN_RED_GVRP_LEARNT_BUFF:

            VLAN_RELEASE_MEM_BLOCK (gVlanRedTaskInfo.VlanLrntPoolId, pu1Buf);
            break;

        case VLAN_RED_GMRP_LEARNT_BUFF:

            VLAN_RELEASE_MEM_BLOCK (gVlanRedTaskInfo.GrpLrntPoolId, pu1Buf);
            break;

        case VLAN_RED_GVRP_DEL_BUFF:

            VLAN_RELEASE_MEM_BLOCK (gVlanRedTaskInfo.VlanDelPoolId, pu1Buf);
            break;

        case VLAN_RED_GMRP_DEL_BUFF:

            VLAN_RELEASE_MEM_BLOCK (gVlanRedTaskInfo.GrpDelPoolId, pu1Buf);
            break;
#endif /* L2RED_WANTED */

        case VLAN_BASE_PORT_ENTRY:
            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanBasePortPoolId, pu1Buf);
            break;

        case VLAN_PORT_VLAN_MAP_ENTRY:
            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.PortVlanMapPoolId, pu1Buf);
            break;
#if (defined L2RED_WANTED) && (defined NPAPI_WANTED)
        case VLAN_RED_CACHE_ENTRY:
            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanRedCachePoolId, pu1Buf);
            break;
        case VLAN_RED_ST_UCAST_CACHE_ENTRY:
            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanRedStUcastCachePoolId, pu1Buf);
            break;
        case VLAN_RED_MCAST_CACHE_ENTRY:
            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanRedStMcastCachePoolId, pu1Buf);
            break;
        case VLAN_RED_PROTO_VLAN_CACHE_ENTRY:
            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanRedProtoVlanCachePoolId, pu1Buf);
            break;

#endif /*(defined L2RED_WANTED) && (defined NPAPI_WANTED) */

        case VLAN_HW_STATS_ENTRY:
            VLAN_RELEASE_MEM_BLOCK (gVlanTaskInfo.VlanPoolIds.
                                    VlanHwStatsMapPoolId, pu1Buf);
            break;

        case VLAN_MCAG_QMSG_BUFF:
            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanMcagQMsgPoolId ,pu1Buf);
            break;
         case VLAN_MCAG_MAC_AGING_ENTRIES:
            VLAN_RELEASE_MEM_BLOCK
                (gVlanTaskInfo.VlanPoolIds.VlanMcagMacAgingEntryPoolId ,pu1Buf);
            break;

        default:
            break;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDupBuf                                       */
/*                                                                           */
/*    Description         : This function takes care of duplicating the      */
/*                          specified chain buffer. This function allocates  */
/*                          a new chain buffer of the size equal to the size */
/*                          of the given buffer and copies the data to the   */
/*                          newly allocated buffer.                          */
/*                                                                           */
/*    Input(s)            : pSrc - Pointer to the source buffer.             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the allocated buffer OR NULL           */
/*                                                                           */
/*****************************************************************************/
tCRU_BUF_CHAIN_HEADER *
VlanDupBuf (tCRU_BUF_CHAIN_HEADER * pSrc)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    UINT4               u4PktSize = 0;

    u4PktSize = VLAN_GET_BUF_LEN (pSrc);

    pDupBuf = VLAN_ALLOCATE_CRU_BUF (u4PktSize, 0);

    if (pDupBuf == NULL)
    {
        return NULL;
    }

    VLAN_MEMSET (gua1VlanDupBuff, 0, VLAN_DEFAULT_PKT_SIZE);

    if (u4PktSize > VLAN_DEFAULT_PKT_SIZE)
    {
        UtlTrcLog (1, 1, "",
                   "Incoming packet size is greater than vlan default packet size,u4PktSize = %d VLAN_DEFAULT_PKT_SIZE = %d,u4PktSize,                      VLAN_DEFAULT_PKT_SIZE\n");
    
        VLAN_RELEASE_CRU_BUF (pDupBuf);
        return NULL;
    }
    if (VLAN_COPY_FROM_BUF (pSrc, gua1VlanDupBuff, 0, u4PktSize) == CRU_FAILURE)
    {
        VLAN_RELEASE_CRU_BUF (pDupBuf);
        return NULL;
    }

    if (VLAN_COPY_TO_BUF (pDupBuf, gua1VlanDupBuff, 0, u4PktSize) ==
        CRU_FAILURE)
    {
        VLAN_RELEASE_CRU_BUF (pDupBuf);
        return NULL;
    }

    return pDupBuf;
}

tStaticVlanEntry   *
VlanCustGetBuf (tVlanId vlanId, UINT1 u1BufType, UINT4 u4Size)
{
    if ((vlanId < 1) || (vlanId > VLAN_MAX_VLAN_ID))
    {
        return NULL;
    }

    gpVlanContextInfo->apStaticVlanInfo[vlanId - 1] =
        (tStaticVlanEntry *) (VOID *) VLAN_GET_BUF (u1BufType, u4Size);
    return gpVlanContextInfo->apStaticVlanInfo[vlanId - 1];
}

VOID
VlanCustReleaseBuf (tVlanId vlanId, UINT1 u1BufType, UINT1 *pu1Buf)
{
    VLAN_RELEASE_BUF (u1BufType, pu1Buf);
    if ((vlanId >= 1) && (vlanId <= VLAN_MAX_VLAN_ID))
    {
        gpVlanContextInfo->apStaticVlanInfo[vlanId - 1] = NULL;
    }
}
