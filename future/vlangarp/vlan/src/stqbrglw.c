/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stqbrglw.c,v 1.2 2013/12/07 11:09:15 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stqbrglw.h"
# include  "vlaninc.h"

/* LOW LEVEL Routines for Table : Ieee8021QBridgeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeTable (UINT4
                                              u4Ieee8021QBridgeComponentId)
{
    if (VlanSelectContext (u4Ieee8021QBridgeComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* release context info */
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeTable (UINT4 *pu4Ieee8021QBridgeComponentId)
{
    if (VlanGetFirstActiveContext (pu4Ieee8021QBridgeComponentId) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
                nextIeee8021QBridgeComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeTable (UINT4 u4Ieee8021QBridgeComponentId,
                                     UINT4 *pu4NextIeee8021QBridgeComponentId)
{
    if (VlanGetNextActiveContext (u4Ieee8021QBridgeComponentId,
                                  pu4NextIeee8021QBridgeComponentId)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanVersionNumber
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeVlanVersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanVersionNumber (UINT4 u4Ieee8021QBridgeComponentId,
                                        INT4
                                        *pi4RetValIeee8021QBridgeVlanVersionNumber)
{
    INT1                i1RetVal;

    i1RetVal =
        nmhGetFsDot1qVlanVersionNumber ((INTT4) u4Ieee8021QBridgeComponentId,
                                        pi4RetValIeee8021QBridgeVlanVersionNumber);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeMaxVlanId
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeMaxVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeMaxVlanId (UINT4 u4Ieee8021QBridgeComponentId,
                                INT4 *pi4RetValIeee8021QBridgeMaxVlanId)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetFsDot1qMaxVlanId ((INTT4) u4Ieee8021QBridgeComponentId,
                                       pi4RetValIeee8021QBridgeMaxVlanId);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeMaxSupportedVlans
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeMaxSupportedVlans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeMaxSupportedVlans (UINT4 u4Ieee8021QBridgeComponentId,
                                        UINT4
                                        *pu4RetValIeee8021QBridgeMaxSupportedVlans)
{
    INT1                i1RetVal;

    i1RetVal =
        nmhGetFsDot1qMaxSupportedVlans ((INT4) u4Ieee8021QBridgeComponentId,
                                        (UINT4 *) &i4MaxSupportedVlans);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeNumVlans
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeNumVlans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeNumVlans (UINT4 u4Ieee8021QBridgeComponentId,
                               UINT4 *pu4RetValIeee8021QBridgeNumVlans)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetFsDot1qNumVlans ((INT4) u4Ieee8021QBridgeComponentId,
                                      pu4RetValIeee8021QBridgeNumVlans);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeMvrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeMvrpEnabledStatus (UINT4 u4Ieee8021QBridgeComponentId,
                                        INT4
                                        *pi4RetValIeee8021QBridgeMvrpEnabledStatus)
{
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                setValIeee8021QBridgeMvrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeMvrpEnabledStatus (UINT4 u4Ieee8021QBridgeComponentId,
                                          INT4
                                          i4SetValIeee8021QBridgeMvrpEnabledStatus)
{
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                testValIeee8021QBridgeMvrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeMvrpEnabledStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Ieee8021QBridgeComponentId,
                                           INT4
                                           i4TestValIeee8021QBridgeMvrpEnabledStatus)
{
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeCVlanPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeCVlanPortTable (UINT4
                                                       u4Ieee8021QBridgeCVlanPortComponentId,
                                                       UINT4
                                                       u4Ieee8021QBridgeCVlanPortNumber)
{
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeCVlanPortTable (UINT4
                                               *pu4Ieee8021QBridgeCVlanPortComponentId,
                                               UINT4
                                               *pu4Ieee8021QBridgeCVlanPortNumber)
{
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                nextIeee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
                nextIeee8021QBridgeCVlanPortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeCVlanPortTable (UINT4
                                              u4Ieee8021QBridgeCVlanPortComponentId,
                                              UINT4
                                              *pu4NextIeee8021QBridgeCVlanPortComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeCVlanPortNumber,
                                              UINT4
                                              *pu4NextIeee8021QBridgeCVlanPortNumber)
{
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeCVlanPortRowStatus
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber

                The Object 
                retValIeee8021QBridgeCVlanPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeCVlanPortRowStatus (UINT4
                                         u4Ieee8021QBridgeCVlanPortComponentId,
                                         UINT4 u4Ieee8021QBridgeCVlanPortNumber,
                                         INT4
                                         *pi4RetValIeee8021QBridgeCVlanPortRowStatus)
{
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeCVlanPortRowStatus
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber

                The Object 
                setValIeee8021QBridgeCVlanPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeCVlanPortRowStatus (UINT4
                                           u4Ieee8021QBridgeCVlanPortComponentId,
                                           UINT4
                                           u4Ieee8021QBridgeCVlanPortNumber,
                                           INT4
                                           i4SetValIeee8021QBridgeCVlanPortRowStatus)
{
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeCVlanPortRowStatus
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber

                The Object 
                testValIeee8021QBridgeCVlanPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeCVlanPortRowStatus (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4Ieee8021QBridgeCVlanPortComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeCVlanPortNumber,
                                            INT4
                                            i4TestValIeee8021QBridgeCVlanPortRowStatus)
{
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeCVlanPortTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeFdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeFdbTable (UINT4
                                                 u4Ieee8021QBridgeFdbComponentId,
                                                 UINT4 u4Ieee8021QBridgeFdbId)
{
    UINT4               u4FidIndex = 0;
    if (VlanSelectContext (u4Ieee8021QBridgeFdbComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4Ieee8021QBridgeFdbId);

    /* Release the context */
    VlanReleaseContext ();
    if (VLAN_INVALID_FID_INDEX != u4FidIndex)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeFdbTable (UINT4
                                         *pu4Ieee8021QBridgeFdbComponentId,
                                         UINT4 *pu4Ieee8021QBridgeFdbId)
{

    UINT4               u4ContextId = 0;

    if (VlanGetFirstActiveContext (pu4Ieee8021QBridgeFdbComponentId) ==
        VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    do
    {
        if (VlanSelectContext (*pu4Ieee8021QBridgeFdbComponentId) !=
            VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (nmhGetFirstIndexDot1qFdbTable (pu4Ieee8021QBridgeFdbId) ==
            SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }

        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pu4Ieee8021QBridgeFdbComponentId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                nextIeee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                nextIeee8021QBridgeFdbId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeFdbTable (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                        UINT4
                                        *pu4NextIeee8021QBridgeFdbComponentId,
                                        UINT4 u4Ieee8021QBridgeFdbId,
                                        UINT4 *pu4NextIeee8021QBridgeFdbId)
{
    UINT4               u4ContextId = 0;

    if (VlanSelectContext (u4Ieee8021QBridgeFdbComponentId) != VLAN_SUCCESS)
    {
        if (nmhGetNextIndexDot1qFdbTable (u4Ieee8021QBridgeFdbId,
                                          pu4NextIeee8021QBridgeFdbId) ==
            SNMP_SUCCESS)
        {
            *pu4NextIeee8021QBridgeFdbComponentId =
                u4Ieee8021QBridgeFdbComponentId;

            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }

    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) u4Ieee8021QBridgeFdbComponentId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4Ieee8021QBridgeFdbComponentId = (INT4) u4ContextId;
        *pu4NextIeee8021QBridgeFdbComponentId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qFdbTable (pu4NextIeee8021QBridgeFdbId) !=
           SNMP_SUCCESS);

    /* Release the context */
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeFdbDynamicCount
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                retValIeee8021QBridgeFdbDynamicCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeFdbDynamicCount (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                      UINT4 u4Ieee8021QBridgeFdbId,
                                      UINT4
                                      *pu4RetValIeee8021QBridgeFdbDynamicCount)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext (u4Ieee8021QBridgeFdbComponentId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFdbDynamicCount (u4Ieee8021QBridgeFdbId,
                                           pu4RetValIeee8021QBridgeFdbDynamicCount);

    /* Release the context */
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeFdbLearnedEntryDiscards
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                retValIeee8021QBridgeFdbLearnedEntryDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeFdbLearnedEntryDiscards (UINT4
                                              u4Ieee8021QBridgeFdbComponentId,
                                              UINT4 u4Ieee8021QBridgeFdbId,
                                              tSNMP_COUNTER64_TYPE *
                                              pu8RetValIeee8021QBridgeFdbLearnedEntryDiscards)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT4               u4lsn = 0;
    UINT4               u4msn = 0;

    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    i1RetVal =
        nmhGetFsDot1dTpLearnedEntryDiscards ((INT4)
                                             u4Ieee8021QBridgeFdbComponentId,
                                             &u4lsn);

    pu8RetValIeee8021QBridgeFdbLearnedEntryDiscards->lsn = u4lsn;
    pu8RetValIeee8021QBridgeFdbLearnedEntryDiscards->msn = u4msn;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeFdbAgingTime
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                retValIeee8021QBridgeFdbAgingTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeFdbAgingTime (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                   UINT4 u4Ieee8021QBridgeFdbId,
                                   INT4 *pi4RetValIeee8021QBridgeFdbAgingTime)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    i1RetVal = nmhGetFsDot1dTpAgingTime ((INT4) u4Ieee8021QBridgeFdbComponentId,
                                         pi4RetValIeee8021QBridgeFdbAgingTime);

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeFdbAgingTime
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                setValIeee8021QBridgeFdbAgingTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeFdbAgingTime (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                     UINT4 u4Ieee8021QBridgeFdbId,
                                     INT4 i4SetValIeee8021QBridgeFdbAgingTime)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    i1RetVal =
        nmhSetExFsDot1dTpAgingTime ((INT4) u4Ieee8021QBridgeFdbComponentId,
                                    i4SetValIeee8021QBridgeFdbAgingTime);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeFdbAgingTime
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                testValIeee8021QBridgeFdbAgingTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeFdbAgingTime (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ieee8021QBridgeFdbComponentId,
                                      UINT4 u4Ieee8021QBridgeFdbId,
                                      INT4 i4TestValIeee8021QBridgeFdbAgingTime)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    i1RetVal = nmhTestv2FsDot1dTpAgingTime (pu4ErrorCode,
                                            (INT4)
                                            u4Ieee8021QBridgeFdbComponentId,
                                            i4SetValIeee8021QBridgeFdbAgingTime);

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeFdbTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeTpFdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeTpFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeTpFdbTable (UINT4
                                                   u4Ieee8021QBridgeFdbComponentId,
                                                   UINT4 u4Ieee8021QBridgeFdbId,
                                                   tMacAddr
                                                   Ieee8021QBridgeTpFdbAddress)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i1RetVal =
        nmhValidateIndexInstanceFsDot1qTpFdbTable ((INT4)
                                                   u4Ieee8021QBridgeFdbComponentId,
                                                   u4Ieee8021QBridgeFdbId,
                                                   Ieee8021QBridgeTpFdbAddress);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeTpFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeTpFdbTable (UINT4
                                           *pu4Ieee8021QBridgeFdbComponentId,
                                           UINT4 *pu4Ieee8021QBridgeFdbId,
                                           tMacAddr *
                                           pIeee8021QBridgeTpFdbAddress)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i1RetVal =
        nmhGetFirstIndexFsDot1qTpFdbTable ((INT4 *)
                                           pu4Ieee8021QBridgeFdbComponentId,
                                           pu4Ieee8021QBridgeFdbId,
                                           pIeee8021QBridgeTpFdbAddress);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeTpFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                nextIeee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                nextIeee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress
                nextIeee8021QBridgeTpFdbAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeTpFdbTable (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                          UINT4
                                          *pu4NextIeee8021QBridgeFdbComponentId,
                                          UINT4 u4Ieee8021QBridgeFdbId,
                                          UINT4 *pu4NextIeee8021QBridgeFdbId,
                                          tMacAddr Ieee8021QBridgeTpFdbAddress,
                                          tMacAddr *
                                          pNextIeee8021QBridgeTpFdbAddress)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i1RetVal =
        nmhGetNextIndexFsDot1qTpFdbTable ((INT4)
                                          u4Ieee8021QBridgeFdbComponentId,
                                          (INT4 *)
                                          pu4NextIeee8021QBridgeFdbComponentId,
                                          u4Ieee8021QBridgeFdbId,
                                          pu4NextIeee8021QBridgeFdbId,
                                          Ieee8021QBridgeTpFdbAddress,
                                          pNextIeee8021QBridgeTpFdbAddress);

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpFdbPort
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress

                The Object 
                retValIeee8021QBridgeTpFdbPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpFdbPort (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                UINT4 u4Ieee8021QBridgeFdbId,
                                tMacAddr Ieee8021QBridgeTpFdbAddress,
                                UINT4 *pu4RetValIeee8021QBridgeTpFdbPort)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i1RetVal = nmhGetFsDot1qTpFdbPort ((INT4) u4Ieee8021QBridgeFdbComponentId,
                                       u4Ieee8021QBridgeFdbId,
                                       Ieee8021QBridgeTpFdbAddress,
                                       pu4RetValIeee8021QBridgeTpFdbPort);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpFdbStatus
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress

                The Object 
                retValIeee8021QBridgeTpFdbStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpFdbStatus (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                  UINT4 u4Ieee8021QBridgeFdbId,
                                  tMacAddr Ieee8021QBridgeTpFdbAddress,
                                  INT4 *pi4RetValIeee8021QBridgeTpFdbStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i1RetVal = nmhGetFsDot1qTpFdbStatus ((INT4) u4Ieee8021QBridgeFdbComponentId,
                                         u4Ieee8021QBridgeFdbId,
                                         Ieee8021QBridgeTpFdbAddress,
                                         pi4RetValIeee8021QBridgeTpFdbStatus);

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanNumDeletes
 Input       :  The Indices

                The Object 
                retValIeee8021QBridgeVlanNumDeletes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanNumDeletes (tSNMP_COUNTER64_TYPE *
                                     pu8RetValIeee8021QBridgeVlanNumDeletes)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT4               u4ContextId = 0;
    UINT4               u4TempContextId = 0;
    UINT4              *pu4lsn = NULL;
    UINT4              *pu4msn = NULL;

    pu8RetValIeee8021QBridgeVlanNumDeletes->lsn = 0;
    pu8RetValIeee8021QBridgeVlanNumDeletes->msn = 0;

    pu4lsn = pu8RetValIeee8021QBridgeVlanNumDeletes->lsn;
    pu4msn = pu8RetValIeee8021QBridgeVlanNumDeletes->msn;

    if (VlanGetFirstActiveContext (&u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i1RetVal = nmhGetDot1qVlanNumDeletes (pu4lsn);
        if (i1RetVal == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }

        u4TempContextId = u4ContextId;
        VlanReleaseContext ();

    }
    while (VlanGetNextActiveContext
           (u4TempContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeVlanCurrentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable (UINT4
                                                         u4Ieee8021QBridgeVlanTimeMark,
                                                         UINT4
                                                         u4Ieee8021QBridgeVlanCurrentComponentId,
                                                         UINT4
                                                         u4Ieee8021QBridgeVlanIndex)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i1RetVal = nmhValidateIndexInstanceFsDot1qVlanCurrentTable ((INT4)
                                                                u4Ieee8021QBridgeVlanCurrentComponentId,
                                                                u4Ieee8021QBridgeVlanTimeMark,
                                                                u4Ieee8021QBridgeVlanIndex);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeVlanCurrentTable
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeVlanCurrentTable (UINT4
                                                 *pu4Ieee8021QBridgeVlanTimeMark,
                                                 UINT4
                                                 *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                 UINT4
                                                 *pu4Ieee8021QBridgeVlanIndex)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i1RetVal = nmhGetFirstIndexFsDot1qVlanCurrentTable ((INT4 *)
                                                        pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                        pu4Ieee8021QBridgeVlanTimeMark,
                                                        pu4Ieee8021QBridgeVlanIndex);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeVlanCurrentTable
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                nextIeee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeVlanCurrentTable (UINT4
                                                u4Ieee8021QBridgeVlanTimeMark,
                                                UINT4
                                                *pu4NextIeee8021QBridgeVlanTimeMark,
                                                UINT4
                                                u4Ieee8021QBridgeVlanCurrentComponentId,
                                                UINT4
                                                *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeVlanIndex,
                                                UINT4
                                                *pu4NextIeee8021QBridgeVlanIndex)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i1RetVal = nmhGetNextIndexFsDot1qVlanCurrentTable ((INT4)
                                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                                       (INT4 *)
                                                       pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                       u4Ieee8021QBridgeVlanTimeMark,
                                                       pu4NextIeee8021QBridgeVlanTimeMark,
                                                       u4Ieee8021QBridgeVlanIndex,
                                                       pu4NextIeee8021QBridgeVlanIndex);

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanFdbId
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanFdbId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanFdbId (UINT4 u4Ieee8021QBridgeVlanTimeMark,
                                UINT4 u4Ieee8021QBridgeVlanCurrentComponentId,
                                UINT4 u4Ieee8021QBridgeVlanIndex,
                                UINT4 *pu4RetValIeee8021QBridgeVlanFdbId)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i1RetVal =
        nmhGetFsDot1qVlanFdbId ((INT4) u4Ieee8021QBridgeVlanCurrentComponentId,
                                u4Ieee8021QBridgeVlanTimeMark,
                                u4Ieee8021QBridgeVlanIndex,
                                pu4RetValIeee8021QBridgeVlanFdbId);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanCurrentEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanCurrentEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanCurrentEgressPorts (UINT4
                                             u4Ieee8021QBridgeVlanTimeMark,
                                             UINT4
                                             u4Ieee8021QBridgeVlanCurrentComponentId,
                                             UINT4 u4Ieee8021QBridgeVlanIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pRetValIeee8021QBridgeVlanCurrentEgressPorts)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qVlanCurrentEgressPorts (u4Ieee8021QBridgeVlanTimeMark,
                                                  u4Ieee8021QBridgeVlanIndex,
                                                  pRetValIeee8021QBridgeVlanCurrentEgressPorts);

    /* release context info */
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanCurrentUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanCurrentUntaggedPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanCurrentUntaggedPorts (UINT4
                                               u4Ieee8021QBridgeVlanTimeMark,
                                               UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4 u4Ieee8021QBridgeVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeVlanCurrentUntaggedPorts)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qVlanCurrentUntaggedPorts (u4Ieee8021QBridgeVlanTimeMark,
                                             u4Ieee8021QBridgeVlanIndex,
                                             pRetValIeee8021QBridgeVlanCurrentUntaggedPorts);

    /* release context info */
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStatus (UINT4 u4Ieee8021QBridgeVlanTimeMark,
                                 UINT4 u4Ieee8021QBridgeVlanCurrentComponentId,
                                 UINT4 u4Ieee8021QBridgeVlanIndex,
                                 INT4 *pi4RetValIeee8021QBridgeVlanStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i1RetVal = nmhGetFsDot1qVlanStatus (u4Ieee8021QBridgeVlanCurrentComponentId,
                                        u4Ieee8021QBridgeVlanTimeMark,
                                        u4Ieee8021QBridgeVlanIndex,
                                        pi4RetValIeee8021QBridgeVlanStatus);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanCreationTime
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanCreationTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanCreationTime (UINT4 u4Ieee8021QBridgeVlanTimeMark,
                                       UINT4
                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                       UINT4 u4Ieee8021QBridgeVlanIndex,
                                       UINT4
                                       *pu4RetValIeee8021QBridgeVlanCreationTime)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    i1RetVal =
        nmhGetFsDot1qVlanCreationTime (u4Ieee8021QBridgeVlanCurrentComponentId,
                                       u4Ieee8021QBridgeVlanTimeMark,
                                       u4Ieee8021QBridgeVlanIndex,
                                       pu4RetValIeee8021QBridgeVlanCreationTime);

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeTpGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeTpGroupTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeTpGroupTable (UINT4
                                                     u4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeVlanIndex,
                                                     tMacAddr
                                                     Ieee8021QBridgeTpGroupAddress)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext (u4Ieee8021QBridgeVlanCurrentComponentId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1qTpGroupTable (u4Ieee8021QBridgeVlanIndex,
                                                   Ieee8021QBridgeTpGroupAddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeTpGroupTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeTpGroupTable (UINT4
                                             *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                             UINT4 *pu4Ieee8021QBridgeVlanIndex,
                                             tMacAddr *
                                             pIeee8021QBridgeTpGroupAddress)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeTpGroupTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress
                nextIeee8021QBridgeTpGroupAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeTpGroupTable (UINT4
                                            u4Ieee8021QBridgeVlanCurrentComponentId,
                                            UINT4
                                            *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                            UINT4 u4Ieee8021QBridgeVlanIndex,
                                            UINT4
                                            *pu4NextIeee8021QBridgeVlanIndex,
                                            tMacAddr
                                            Ieee8021QBridgeTpGroupAddress,
                                            tMacAddr *
                                            pNextIeee8021QBridgeTpGroupAddress)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpGroupEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress

                The Object 
                retValIeee8021QBridgeTpGroupEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpGroupEgressPorts (UINT4
                                         u4Ieee8021QBridgeVlanCurrentComponentId,
                                         UINT4 u4Ieee8021QBridgeVlanIndex,
                                         tMacAddr Ieee8021QBridgeTpGroupAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValIeee8021QBridgeTpGroupEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpGroupLearnt
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress

                The Object 
                retValIeee8021QBridgeTpGroupLearnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpGroupLearnt (UINT4
                                    u4Ieee8021QBridgeVlanCurrentComponentId,
                                    UINT4 u4Ieee8021QBridgeVlanIndex,
                                    tMacAddr Ieee8021QBridgeTpGroupAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValIeee8021QBridgeTpGroupLearnt)
{
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeForwardAllTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardAllVlanIndex)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeForwardAllTable (UINT4
                                                *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                UINT4
                                                *pu4Ieee8021QBridgeForwardAllVlanIndex)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
                nextIeee8021QBridgeForwardAllVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeForwardAllTable (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               UINT4
                                               *pu4NextIeee8021QBridgeForwardAllVlanIndex)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardAllPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                retValIeee8021QBridgeForwardAllPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardAllPorts (UINT4
                                      u4Ieee8021QBridgeVlanCurrentComponentId,
                                      UINT4
                                      u4Ieee8021QBridgeForwardAllVlanIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValIeee8021QBridgeForwardAllPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardAllStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                retValIeee8021QBridgeForwardAllStaticPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardAllStaticPorts (UINT4
                                            u4Ieee8021QBridgeVlanCurrentComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeForwardAllVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValIeee8021QBridgeForwardAllStaticPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardAllForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                retValIeee8021QBridgeForwardAllForbiddenPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardAllForbiddenPorts (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeForwardAllForbiddenPorts)
{
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeForwardAllStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                setValIeee8021QBridgeForwardAllStaticPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeForwardAllStaticPorts (UINT4
                                              u4Ieee8021QBridgeVlanCurrentComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeForwardAllVlanIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pSetValIeee8021QBridgeForwardAllStaticPorts)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeForwardAllForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                setValIeee8021QBridgeForwardAllForbiddenPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeForwardAllForbiddenPorts (UINT4
                                                 u4Ieee8021QBridgeVlanCurrentComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeForwardAllVlanIndex,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pSetValIeee8021QBridgeForwardAllForbiddenPorts)
{
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardAllStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                testValIeee8021QBridgeForwardAllStaticPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardAllStaticPorts (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValIeee8021QBridgeForwardAllStaticPorts)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardAllForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                testValIeee8021QBridgeForwardAllForbiddenPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardAllForbiddenPorts (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanCurrentComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeForwardAllVlanIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pTestValIeee8021QBridgeForwardAllForbiddenPorts)
{
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeForwardAllTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeForwardUnregisteredTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable (UINT4
                                                                 u4Ieee8021QBridgeVlanCurrentComponentId,
                                                                 UINT4
                                                                 u4Ieee8021QBridgeForwardUnregisteredVlanIndex)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeForwardUnregisteredTable (UINT4
                                                         *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                         UINT4
                                                         *pu4Ieee8021QBridgeForwardUnregisteredVlanIndex)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
                nextIeee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeForwardUnregisteredTable (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeForwardUnregisteredVlanIndex)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardUnregisteredPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                retValIeee8021QBridgeForwardUnregisteredPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardUnregisteredPorts (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeForwardUnregisteredPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardUnregisteredStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                retValIeee8021QBridgeForwardUnregisteredStaticPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardUnregisteredStaticPorts (UINT4
                                                     u4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pRetValIeee8021QBridgeForwardUnregisteredStaticPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                retValIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardUnregisteredForbiddenPorts (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pRetValIeee8021QBridgeForwardUnregisteredForbiddenPorts)
{
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeForwardUnregisteredStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                setValIeee8021QBridgeForwardUnregisteredStaticPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeForwardUnregisteredStaticPorts (UINT4
                                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                                       UINT4
                                                       u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pSetValIeee8021QBridgeForwardUnregisteredStaticPorts)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                setValIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeForwardUnregisteredForbiddenPorts (UINT4
                                                          u4Ieee8021QBridgeVlanCurrentComponentId,
                                                          UINT4
                                                          u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts)
{
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardUnregisteredStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                testValIeee8021QBridgeForwardUnregisteredStaticPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardUnregisteredStaticPorts (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pTestValIeee8021QBridgeForwardUnregisteredStaticPorts)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardUnregisteredForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                testValIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardUnregisteredForbiddenPorts (UINT4 *pu4ErrorCode,
                                                           UINT4
                                                           u4Ieee8021QBridgeVlanCurrentComponentId,
                                                           UINT4
                                                           u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pTestValIeee8021QBridgeForwardUnregisteredForbiddenPorts)
{
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeForwardUnregisteredTable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeStaticUnicastTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable (UINT4
                                                           u4Ieee8021QBridgeStaticUnicastComponentId,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                           tMacAddr
                                                           Ieee8021QBridgeStaticUnicastAddress,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastReceivePort)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeStaticUnicastTable (UINT4
                                                   *pu4Ieee8021QBridgeStaticUnicastComponentId,
                                                   UINT4
                                                   *pu4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                   tMacAddr *
                                                   pIeee8021QBridgeStaticUnicastAddress,
                                                   UINT4
                                                   *pu4Ieee8021QBridgeStaticUnicastReceivePort)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                nextIeee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                nextIeee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                nextIeee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
                nextIeee8021QBridgeStaticUnicastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeStaticUnicastTable (UINT4
                                                  u4Ieee8021QBridgeStaticUnicastComponentId,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeStaticUnicastComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeStaticUnicastVlanIndex,
                                                  tMacAddr
                                                  Ieee8021QBridgeStaticUnicastAddress,
                                                  tMacAddr *
                                                  pNextIeee8021QBridgeStaticUnicastAddress,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeStaticUnicastReceivePort)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastStaticEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastStaticEgressPorts (UINT4
                                                     u4Ieee8021QBridgeStaticUnicastComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                     tMacAddr
                                                     Ieee8021QBridgeStaticUnicastAddress,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pRetValIeee8021QBridgeStaticUnicastStaticEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastForbiddenEgressPorts (UINT4
                                                        u4Ieee8021QBridgeStaticUnicastComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                        tMacAddr
                                                        Ieee8021QBridgeStaticUnicastAddress,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pRetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastStorageType
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastStorageType (UINT4
                                               u4Ieee8021QBridgeStaticUnicastComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                               tMacAddr
                                               Ieee8021QBridgeStaticUnicastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastReceivePort,
                                               INT4
                                               *pi4RetValIeee8021QBridgeStaticUnicastStorageType)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastRowStatus (UINT4
                                             u4Ieee8021QBridgeStaticUnicastComponentId,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                             tMacAddr
                                             Ieee8021QBridgeStaticUnicastAddress,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastReceivePort,
                                             INT4
                                             *pi4RetValIeee8021QBridgeStaticUnicastRowStatus)
{
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeStaticUnicastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastStaticEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeStaticUnicastStaticEgressPorts (UINT4
                                                       u4Ieee8021QBridgeStaticUnicastComponentId,
                                                       UINT4
                                                       u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                       tMacAddr
                                                       Ieee8021QBridgeStaticUnicastAddress,
                                                       UINT4
                                                       u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeStaticUnicastForbiddenEgressPorts (UINT4
                                                          u4Ieee8021QBridgeStaticUnicastComponentId,
                                                          UINT4
                                                          u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                          tMacAddr
                                                          Ieee8021QBridgeStaticUnicastAddress,
                                                          UINT4
                                                          u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeStaticUnicastStorageType
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeStaticUnicastStorageType (UINT4
                                                 u4Ieee8021QBridgeStaticUnicastComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                 tMacAddr
                                                 Ieee8021QBridgeStaticUnicastAddress,
                                                 UINT4
                                                 u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                 INT4
                                                 i4SetValIeee8021QBridgeStaticUnicastStorageType)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeStaticUnicastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeStaticUnicastRowStatus (UINT4
                                               u4Ieee8021QBridgeStaticUnicastComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                               tMacAddr
                                               Ieee8021QBridgeStaticUnicastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastReceivePort,
                                               INT4
                                               i4SetValIeee8021QBridgeStaticUnicastRowStatus)
{
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastStaticEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastStaticEgressPorts (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                        tMacAddr
                                                        Ieee8021QBridgeStaticUnicastAddress,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pTestValIeee8021QBridgeStaticUnicastStaticEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastForbiddenEgressPorts (UINT4 *pu4ErrorCode,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastComponentId,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                           tMacAddr
                                                           Ieee8021QBridgeStaticUnicastAddress,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pTestValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastStorageType
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastStorageType (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                  tMacAddr
                                                  Ieee8021QBridgeStaticUnicastAddress,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                  INT4
                                                  i4TestValIeee8021QBridgeStaticUnicastStorageType)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastRowStatus (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021QBridgeStaticUnicastComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                tMacAddr
                                                Ieee8021QBridgeStaticUnicastAddress,
                                                UINT4
                                                u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                INT4
                                                i4TestValIeee8021QBridgeStaticUnicastRowStatus)
{
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeStaticUnicastTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeStaticMulticastTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable (UINT4
                                                             u4Ieee8021QBridgeVlanCurrentComponentId,
                                                             UINT4
                                                             u4Ieee8021QBridgeVlanIndex,
                                                             tMacAddr
                                                             Ieee8021QBridgeStaticMulticastAddress,
                                                             UINT4
                                                             u4Ieee8021QBridgeStaticMulticastReceivePort)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeStaticMulticastTable (UINT4
                                                     *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     *pu4Ieee8021QBridgeVlanIndex,
                                                     tMacAddr *
                                                     pIeee8021QBridgeStaticMulticastAddress,
                                                     UINT4
                                                     *pu4Ieee8021QBridgeStaticMulticastReceivePort)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                nextIeee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
                nextIeee8021QBridgeStaticMulticastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeStaticMulticastTable (UINT4
                                                    u4Ieee8021QBridgeVlanCurrentComponentId,
                                                    UINT4
                                                    *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                    UINT4
                                                    u4Ieee8021QBridgeVlanIndex,
                                                    UINT4
                                                    *pu4NextIeee8021QBridgeVlanIndex,
                                                    tMacAddr
                                                    Ieee8021QBridgeStaticMulticastAddress,
                                                    tMacAddr *
                                                    pNextIeee8021QBridgeStaticMulticastAddress,
                                                    UINT4
                                                    u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                    UINT4
                                                    *pu4NextIeee8021QBridgeStaticMulticastReceivePort)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastStaticEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastStaticEgressPorts (UINT4
                                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                                       UINT4
                                                       u4Ieee8021QBridgeVlanIndex,
                                                       tMacAddr
                                                       Ieee8021QBridgeStaticMulticastAddress,
                                                       UINT4
                                                       u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pRetValIeee8021QBridgeStaticMulticastStaticEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastForbiddenEgressPorts (UINT4
                                                          u4Ieee8021QBridgeVlanCurrentComponentId,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanIndex,
                                                          tMacAddr
                                                          Ieee8021QBridgeStaticMulticastAddress,
                                                          UINT4
                                                          u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pRetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastStorageType
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastStorageType (UINT4
                                                 u4Ieee8021QBridgeVlanCurrentComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanIndex,
                                                 tMacAddr
                                                 Ieee8021QBridgeStaticMulticastAddress,
                                                 UINT4
                                                 u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                 INT4
                                                 *pi4RetValIeee8021QBridgeStaticMulticastStorageType)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastRowStatus (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4 u4Ieee8021QBridgeVlanIndex,
                                               tMacAddr
                                               Ieee8021QBridgeStaticMulticastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticMulticastReceivePort,
                                               INT4
                                               *pi4RetValIeee8021QBridgeStaticMulticastRowStatus)
{
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeStaticMulticastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastStaticEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeStaticMulticastStaticEgressPorts (UINT4
                                                         u4Ieee8021QBridgeVlanCurrentComponentId,
                                                         UINT4
                                                         u4Ieee8021QBridgeVlanIndex,
                                                         tMacAddr
                                                         Ieee8021QBridgeStaticMulticastAddress,
                                                         UINT4
                                                         u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                         tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts)
{
}

/**************************************dot1qForwardAllEntry OBJECT-TYPE
    SYNTAX      Dot1qForwardAllEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "Forwarding information for a VLAN, specifying the set
        of ports to which all multicast should be forwarded,
        configured statically by management or dynamically by
        GMRP."
    INDEX   { dot1qVlanIndex }
    ::= { dot1qForwardAllTable 1 }
**************************************
 Function    :  nmhSetExIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeStaticMulticastForbiddenEgressPorts (UINT4
                                                            u4Ieee8021QBridgeVlanCurrentComponentId,
                                                            UINT4
                                                            u4Ieee8021QBridgeVlanIndex,
                                                            tMacAddr
                                                            Ieee8021QBridgeStaticMulticastAddress,
                                                            UINT4
                                                            u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                            tSNMP_OCTET_STRING_TYPE
                                                            *
                                                            pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeStaticMulticastStorageType
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeStaticMulticastStorageType (UINT4
                                                   u4Ieee8021QBridgeVlanCurrentComponentId,
                                                   UINT4
                                                   u4Ieee8021QBridgeVlanIndex,
                                                   tMacAddr
                                                   Ieee8021QBridgeStaticMulticastAddress,
                                                   UINT4
                                                   u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                   INT4
                                                   i4SetValIeee8021QBridgeStaticMulticastStorageType)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeStaticMulticastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeStaticMulticastRowStatus (UINT4
                                                 u4Ieee8021QBridgeVlanCurrentComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanIndex,
                                                 tMacAddr
                                                 Ieee8021QBridgeStaticMulticastAddress,
                                                 UINT4
                                                 u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                 INT4
                                                 i4SetValIeee8021QBridgeStaticMulticastRowStatus)
{
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastStaticEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastStaticEgressPorts (UINT4 *pu4ErrorCode,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanCurrentComponentId,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanIndex,
                                                          tMacAddr
                                                          Ieee8021QBridgeStaticMulticastAddress,
                                                          UINT4
                                                          u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pTestValIeee8021QBridgeStaticMulticastStaticEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastForbiddenEgressPorts (UINT4
                                                             *pu4ErrorCode,
                                                             UINT4
                                                             u4Ieee8021QBridgeVlanCurrentComponentId,
                                                             UINT4
                                                             u4Ieee8021QBridgeVlanIndex,
                                                             tMacAddr
                                                             Ieee8021QBridgeStaticMulticastAddress,
                                                             UINT4
                                                             u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                             tSNMP_OCTET_STRING_TYPE
                                                             *
                                                             pTestValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastStorageType
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastStorageType (UINT4 *pu4ErrorCode,
                                                    UINT4
                                                    u4Ieee8021QBridgeVlanCurrentComponentId,
                                                    UINT4
                                                    u4Ieee8021QBridgeVlanIndex,
                                                    tMacAddr
                                                    Ieee8021QBridgeStaticMulticastAddress,
                                                    UINT4
                                                    u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                    INT4
                                                    i4TestValIeee8021QBridgeStaticMulticastStorageType)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastRowStatus (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanCurrentComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanIndex,
                                                  tMacAddr
                                                  Ieee8021QBridgeStaticMulticastAddress,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                  INT4
                                                  i4TestValIeee8021QBridgeStaticMulticastRowStatus)
{
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeStaticMulticastTable (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeVlanStaticTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable (UINT4
                                                        u4Ieee8021QBridgeVlanStaticComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeVlanStaticVlanIndex)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeVlanStaticTable (UINT4
                                                *pu4Ieee8021QBridgeVlanStaticComponentId,
                                                UINT4
                                                *pu4Ieee8021QBridgeVlanStaticVlanIndex)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                nextIeee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
                nextIeee8021QBridgeVlanStaticVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeVlanStaticTable (UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               *pu4NextIeee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               UINT4
                                               *pu4NextIeee8021QBridgeVlanStaticVlanIndex)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticName
 Input       :  The Indices
                Ieee8021QBridgeVlanStatdot1qForwardAllEntry OBJECT-TYPE
    SYNTAX      Dot1qForwardAllEntry
    MAX-ACCESS  not-accessible
    STATUS      current
    DESCRIPTION
        "Forwarding information for a VLAN, specifying the set
        of ports to which all multicast should be forwarded,
        configured statically by management or dynamically by
        GMRP."
    INDEX   { dot1qVlanIndex }
    ::= { dot1qForwardAllTable 1 }
icComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticName (UINT4
                                     u4Ieee8021QBridgeVlanStaticComponentId,
                                     UINT4 u4Ieee8021QBridgeVlanStaticVlanIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValIeee8021QBridgeVlanStaticName)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticEgressPorts (UINT4
                                            u4Ieee8021QBridgeVlanStaticComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeVlanStaticVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValIeee8021QBridgeVlanStaticEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanForbiddenEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanForbiddenEgressPorts (UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeVlanForbiddenEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticUntaggedPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticUntaggedPorts (UINT4
                                              u4Ieee8021QBridgeVlanStaticComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeVlanStaticVlanIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValIeee8021QBridgeVlanStaticUntaggedPorts)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticRowStatus (UINT4
                                          u4Ieee8021QBridgeVlanStaticComponentId,
                                          UINT4
                                          u4Ieee8021QBridgeVlanStaticVlanIndex,
                                          INT4
                                          *pi4RetValIeee8021QBridgeVlanStaticRowStatus)
{
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeVlanStaticName
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeVlanStaticName (UINT4
                                       u4Ieee8021QBridgeVlanStaticComponentId,
                                       UINT4
                                       u4Ieee8021QBridgeVlanStaticVlanIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValIeee8021QBridgeVlanStaticName)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeVlanStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeVlanStaticEgressPorts (UINT4
                                              u4Ieee8021QBridgeVlanStaticComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeVlanStaticVlanIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pSetValIeee8021QBridgeVlanStaticEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeVlanForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanForbiddenEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeVlanForbiddenEgressPorts (UINT4
                                                 u4Ieee8021QBridgeVlanStaticComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pSetValIeee8021QBridgeVlanForbiddenEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeVlanStaticUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticUntaggedPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeVlanStaticUntaggedPorts (UINT4
                                                u4Ieee8021QBridgeVlanStaticComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pSetValIeee8021QBridgeVlanStaticUntaggedPorts)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeVlanStaticRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeVlanStaticRowStatus (UINT4
                                            u4Ieee8021QBridgeVlanStaticComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeVlanStaticVlanIndex,
                                            INT4
                                            i4SetValIeee8021QBridgeVlanStaticRowStatus)
{
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticName
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticName (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4Ieee8021QBridgeVlanStaticComponentId,
                                        UINT4
                                        u4Ieee8021QBridgeVlanStaticVlanIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValIeee8021QBridgeVlanStaticName)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticEgressPorts (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValIeee8021QBridgeVlanStaticEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanForbiddenEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanForbiddenEgressPorts (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanStaticComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pTestValIeee8021QBridgeVlanForbiddenEgressPorts)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticUntaggedPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticUntaggedPorts (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanStaticComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pTestValIeee8021QBridgeVlanStaticUntaggedPorts)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticRowStatus (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4Ieee8021QBridgeVlanStaticComponentId,
                                             UINT4
                                             u4Ieee8021QBridgeVlanStaticVlanIndex,
                                             INT4
                                             i4TestValIeee8021QBridgeVlanStaticRowStatus)
{
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeVlanStaticTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeNextFreeLocalVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeNextFreeLocalVlanTable
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeNextFreeLocalVlanTable (UINT4
                                                               u4Ieee8021QBridgeNextFreeLocalVlanComponentId)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeNextFreeLocalVlanTable
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeNextFreeLocalVlanTable (UINT4
                                                       *pu4Ieee8021QBridgeNextFreeLocalVlanComponentId)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeNextFreeLocalVlanTable
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId
                nextIeee8021QBridgeNextFreeLocalVlanComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeNextFreeLocalVlanTable (UINT4
                                                      u4Ieee8021QBridgeNextFreeLocalVlanComponentId,
                                                      UINT4
                                                      *pu4NextIeee8021QBridgeNextFreeLocalVlanComponentId)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeNextFreeLocalVlanIndex
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId

                The Object 
                retValIeee8021QBridgeNextFreeLocalVlanIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeNextFreeLocalVlanIndex (UINT4
                                             u4Ieee8021QBridgeNextFreeLocalVlanComponentId,
                                             UINT4
                                             *pu4RetValIeee8021QBridgeNextFreeLocalVlanIndex)
{
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgePortVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgePortVlanTable (UINT4
                                                      u4Ieee8021BridgeBasePortComponentId,
                                                      UINT4
                                                      u4Ieee8021BridgeBasePort)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgePortVlanTable (UINT4
                                              *pu4Ieee8021BridgeBasePortComponentId,
                                              UINT4 *pu4Ieee8021BridgeBasePort)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgePortVlanTable (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4
                                             *pu4NextIeee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             UINT4
                                             *pu4NextIeee8021BridgeBasePort)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePvid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePvid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePvid (UINT4 u4Ieee8021BridgeBasePortComponentId,
                           UINT4 u4Ieee8021BridgeBasePort,
                           UINT4 *pu4RetValIeee8021QBridgePvid)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortAcceptableFrameTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortAcceptableFrameTypes (UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               *pi4RetValIeee8021QBridgePortAcceptableFrameTypes)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortIngressFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortIngressFiltering (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           *pi4RetValIeee8021QBridgePortIngressFiltering)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortMvrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortMvrpEnabledStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            *pi4RetValIeee8021QBridgePortMvrpEnabledStatus)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortMvrpFailedRegistrations
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortMvrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortMvrpFailedRegistrations (UINT4
                                                  u4Ieee8021BridgeBasePortComponentId,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePort,
                                                  tSNMP_COUNTER64_TYPE *
                                                  pu8RetValIeee8021QBridgePortMvrpFailedRegistrations)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortMvrpLastPduOrigin
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortMvrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortMvrpLastPduOrigin (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            tMacAddr *
                                            pRetValIeee8021QBridgePortMvrpLastPduOrigin)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortRestrictedVlanRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortRestrictedVlanRegistration (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     INT4
                                                     *pi4RetValIeee8021QBridgePortRestrictedVlanRegistration)
{
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgePvid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePvid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgePvid (UINT4 u4Ieee8021BridgeBasePortComponentId,
                             UINT4 u4Ieee8021BridgeBasePort,
                             UINT4 u4SetValIeee8021QBridgePvid)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortAcceptableFrameTypes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgePortAcceptableFrameTypes (UINT4
                                                 u4Ieee8021BridgeBasePortComponentId,
                                                 UINT4 u4Ieee8021BridgeBasePort,
                                                 INT4
                                                 i4SetValIeee8021QBridgePortAcceptableFrameTypes)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgePortIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortIngressFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgePortIngressFiltering (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4
                                             i4SetValIeee8021QBridgePortIngressFiltering)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgePortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortMvrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgePortMvrpEnabledStatus (UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4SetValIeee8021QBridgePortMvrpEnabledStatus)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgePortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortRestrictedVlanRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgePortRestrictedVlanRegistration (UINT4
                                                       u4Ieee8021BridgeBasePortComponentId,
                                                       UINT4
                                                       u4Ieee8021BridgeBasePort,
                                                       INT4
                                                       i4SetValIeee8021QBridgePortRestrictedVlanRegistration)
{
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePvid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePvid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePvid (UINT4 *pu4ErrorCode,
                              UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              UINT4 u4TestValIeee8021QBridgePvid)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortAcceptableFrameTypes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortAcceptableFrameTypes (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePortComponentId,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePort,
                                                  INT4
                                                  i4TestValIeee8021QBridgePortAcceptableFrameTypes)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortIngressFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortIngressFiltering (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4TestValIeee8021QBridgePortIngressFiltering)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortMvrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortMvrpEnabledStatus (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4TestValIeee8021QBridgePortMvrpEnabledStatus)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortRestrictedVlanRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortRestrictedVlanRegistration (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePort,
                                                        INT4
                                                        i4TestValIeee8021QBridgePortRestrictedVlanRegistration)
{
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgePortVlanTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgePortVlanStatisticsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable (UINT4
                                                                u4Ieee8021BridgeBasePortComponentId,
                                                                UINT4
                                                                u4Ieee8021BridgeBasePort,
                                                                UINT4
                                                                u4Ieee8021QBridgeVlanIndex)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgePortVlanStatisticsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgePortVlanStatisticsTable (UINT4
                                                        *pu4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        *pu4Ieee8021BridgeBasePort,
                                                        UINT4
                                                        *pu4Ieee8021QBridgeVlanIndex)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgePortVlanStatisticsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgePortVlanStatisticsTable (UINT4
                                                       u4Ieee8021BridgeBasePortComponentId,
                                                       UINT4
                                                       *pu4NextIeee8021BridgeBasePortComponentId,
                                                       UINT4
                                                       u4Ieee8021BridgeBasePort,
                                                       UINT4
                                                       *pu4NextIeee8021BridgeBasePort,
                                                       UINT4
                                                       u4Ieee8021QBridgeVlanIndex,
                                                       UINT4
                                                       *pu4NextIeee8021QBridgeVlanIndex)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpVlanPortInFrames
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeTpVlanPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpVlanPortInFrames (UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         UINT4 u4Ieee8021QBridgeVlanIndex,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValIeee8021QBridgeTpVlanPortInFrames)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpVlanPortOutFrames
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeTpVlanPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpVlanPortOutFrames (UINT4
                                          u4Ieee8021BridgeBasePortComponentId,
                                          UINT4 u4Ieee8021BridgeBasePort,
                                          UINT4 u4Ieee8021QBridgeVlanIndex,
                                          tSNMP_COUNTER64_TYPE *
                                          pu8RetValIeee8021QBridgeTpVlanPortOutFrames)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpVlanPortInDiscards
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeTpVlanPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpVlanPortInDiscards (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           UINT4 u4Ieee8021QBridgeVlanIndex,
                                           tSNMP_COUNTER64_TYPE *
                                           pu8RetValIeee8021QBridgeTpVlanPortInDiscards)
{
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeLearningConstraintsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintsTable (UINT4
                                                                 u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                                 UINT4
                                                                 u4Ieee8021QBridgeLearningConstraintsVlan,
                                                                 INT4
                                                                 i4Ieee8021QBridgeLearningConstraintsSet)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeLearningConstraintsTable (UINT4
                                                         *pu4Ieee8021QBridgeLearningConstraintsComponentId,
                                                         UINT4
                                                         *pu4Ieee8021QBridgeLearningConstraintsVlan,
                                                         INT4
                                                         *pi4Ieee8021QBridgeLearningConstraintsSet)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                nextIeee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                nextIeee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
                nextIeee8021QBridgeLearningConstraintsSet
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeLearningConstraintsTable (UINT4
                                                        u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeLearningConstraintsComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeLearningConstraintsVlan,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeLearningConstraintsVlan,
                                                        INT4
                                                        i4Ieee8021QBridgeLearningConstraintsSet,
                                                        INT4
                                                        *pi4NextIeee8021QBridgeLearningConstraintsSet)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                retValIeee8021QBridgeLearningConstraintsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintsType (UINT4
                                              u4Ieee8021QBridgeLearningConstraintsComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeLearningConstraintsVlan,
                                              INT4
                                              i4Ieee8021QBridgeLearningConstraintsSet,
                                              INT4
                                              *pi4RetValIeee8021QBridgeLearningConstraintsType)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintsStatus
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                retValIeee8021QBridgeLearningConstraintsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintsStatus (UINT4
                                                u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeLearningConstraintsVlan,
                                                INT4
                                                i4Ieee8021QBridgeLearningConstraintsSet,
                                                INT4
                                                *pi4RetValIeee8021QBridgeLearningConstraintsStatus)
{
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeLearningConstraintsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                setValIeee8021QBridgeLearningConstraintsType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeLearningConstraintsType (UINT4
                                                u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeLearningConstraintsVlan,
                                                INT4
                                                i4Ieee8021QBridgeLearningConstraintsSet,
                                                INT4
                                                i4SetValIeee8021QBridgeLearningConstraintsType)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeLearningConstraintsStatus
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                setValIeee8021QBridgeLearningConstraintsStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeLearningConstraintsStatus (UINT4
                                                  u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeLearningConstraintsVlan,
                                                  INT4
                                                  i4Ieee8021QBridgeLearningConstraintsSet,
                                                  INT4
                                                  i4SetValIeee8021QBridgeLearningConstraintsStatus)
{
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                testValIeee8021QBridgeLearningConstraintsType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintsType (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeLearningConstraintsVlan,
                                                 INT4
                                                 i4Ieee8021QBridgeLearningConstraintsSet,
                                                 INT4
                                                 i4TestValIeee8021QBridgeLearningConstraintsType)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintsStatus
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                testValIeee8021QBridgeLearningConstraintsStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintsStatus (UINT4 *pu4ErrorCode,
                                                   UINT4
                                                   u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                   UINT4
                                                   u4Ieee8021QBridgeLearningConstraintsVlan,
                                                   INT4
                                                   i4Ieee8021QBridgeLearningConstraintsSet,
                                                   INT4
                                                   i4TestValIeee8021QBridgeLearningConstraintsStatus)
{
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeLearningConstraintsTable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeLearningConstraintDefaultsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintDefaultsTable (UINT4
                                                                        u4Ieee8021QBridgeLearningConstraintDefaultsComponentId)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeLearningConstraintDefaultsTable (UINT4
                                                                *pu4Ieee8021QBridgeLearningConstraintDefaultsComponentId)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
                nextIeee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable (UINT4
                                                               u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                               UINT4
                                                               *pu4NextIeee8021QBridgeLearningConstraintDefaultsComponentId)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintDefaultsSet
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                retValIeee8021QBridgeLearningConstraintDefaultsSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintDefaultsSet (UINT4
                                                    u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                    INT4
                                                    *pi4RetValIeee8021QBridgeLearningConstraintDefaultsSet)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintDefaultsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                retValIeee8021QBridgeLearningConstraintDefaultsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintDefaultsType (UINT4
                                                     u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                     INT4
                                                     *pi4RetValIeee8021QBridgeLearningConstraintDefaultsType)
{
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeLearningConstraintDefaultsSet
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                setValIeee8021QBridgeLearningConstraintDefaultsSet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeLearningConstraintDefaultsSet (UINT4
                                                      u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                      INT4
                                                      i4SetValIeee8021QBridgeLearningConstraintDefaultsSet)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeLearningConstraintDefaultsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                setValIeee8021QBridgeLearningConstraintDefaultsType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeLearningConstraintDefaultsType (UINT4
                                                       u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                       INT4
                                                       i4SetValIeee8021QBridgeLearningConstraintDefaultsType)
{
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsSet
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                testValIeee8021QBridgeLearningConstraintDefaultsSet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsSet (UINT4 *pu4ErrorCode,
                                                       UINT4
                                                       u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                       INT4
                                                       i4TestValIeee8021QBridgeLearningConstraintDefaultsSet)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                testValIeee8021QBridgeLearningConstraintDefaultsType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsType (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                        INT4
                                                        i4TestValIeee8021QBridgeLearningConstraintDefaultsType)
{
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeLearningConstraintDefaultsTable (UINT4 *pu4ErrorCode,
                                                        tSnmpIndexList *
                                                        pSnmpIndexList,
                                                        tSNMP_VAR_BIND *
                                                        pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeProtocolGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeProtocolGroupTable (UINT4
                                                           u4Ieee8021QBridgeProtocolGroupComponentId,
                                                           INT4
                                                           i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pIeee8021QBridgeProtocolTemplateProtocolValue)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeProtocolGroupTable (UINT4
                                                   *pu4Ieee8021QBridgeProtocolGroupComponentId,
                                                   INT4
                                                   *pi4Ieee8021QBridgeProtocolTemplateFrameType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pIeee8021QBridgeProtocolTemplateProtocolValue)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                nextIeee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                nextIeee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
                nextIeee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeProtocolGroupTable (UINT4
                                                  u4Ieee8021QBridgeProtocolGroupComponentId,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeProtocolGroupComponentId,
                                                  INT4
                                                  i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                  INT4
                                                  *pi4NextIeee8021QBridgeProtocolTemplateFrameType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pIeee8021QBridgeProtocolTemplateProtocolValue,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pNextIeee8021QBridgeProtocolTemplateProtocolValue)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolGroupId
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                retValIeee8021QBridgeProtocolGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolGroupId (UINT4
                                      u4Ieee8021QBridgeProtocolGroupComponentId,
                                      INT4
                                      i4Ieee8021QBridgeProtocolTemplateFrameType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pIeee8021QBridgeProtocolTemplateProtocolValue,
                                      INT4
                                      *pi4RetValIeee8021QBridgeProtocolGroupId)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolGroupRowStatus
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                retValIeee8021QBridgeProtocolGroupRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolGroupRowStatus (UINT4
                                             u4Ieee8021QBridgeProtocolGroupComponentId,
                                             INT4
                                             i4Ieee8021QBridgeProtocolTemplateFrameType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pIeee8021QBridgeProtocolTemplateProtocolValue,
                                             INT4
                                             *pi4RetValIeee8021QBridgeProtocolGroupRowStatus)
{
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeProtocolGroupId
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                setValIeee8021QBridgeProtocolGroupId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeProtocolGroupId (UINT4
                                        u4Ieee8021QBridgeProtocolGroupComponentId,
                                        INT4
                                        i4Ieee8021QBridgeProtocolTemplateFrameType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pIeee8021QBridgeProtocolTemplateProtocolValue,
                                        INT4
                                        i4SetValIeee8021QBridgeProtocolGroupId)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeProtocolGroupRowStatus
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                setValIeee8021QBridgeProtocolGroupRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeProtocolGroupRowStatus (UINT4
                                               u4Ieee8021QBridgeProtocolGroupComponentId,
                                               INT4
                                               i4Ieee8021QBridgeProtocolTemplateFrameType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pIeee8021QBridgeProtocolTemplateProtocolValue,
                                               INT4
                                               i4SetValIeee8021QBridgeProtocolGroupRowStatus)
{
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolGroupId
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                testValIeee8021QBridgeProtocolGroupId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolGroupId (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4Ieee8021QBridgeProtocolGroupComponentId,
                                         INT4
                                         i4Ieee8021QBridgeProtocolTemplateFrameType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pIeee8021QBridgeProtocolTemplateProtocolValue,
                                         INT4
                                         i4TestValIeee8021QBridgeProtocolGroupId)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolGroupRowStatus
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                testValIeee8021QBridgeProtocolGroupRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolGroupRowStatus (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021QBridgeProtocolGroupComponentId,
                                                INT4
                                                i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pIeee8021QBridgeProtocolTemplateProtocolValue,
                                                INT4
                                                i4TestValIeee8021QBridgeProtocolGroupRowStatus)
{
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeProtocolGroupTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeProtocolPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeProtocolPortTable (UINT4
                                                          u4Ieee8021BridgeBasePortComponentId,
                                                          UINT4
                                                          u4Ieee8021BridgeBasePort,
                                                          INT4
                                                          i4Ieee8021QBridgeProtocolPortGroupId)
{
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeProtocolPortTable (UINT4
                                                  *pu4Ieee8021BridgeBasePortComponentId,
                                                  UINT4
                                                  *pu4Ieee8021BridgeBasePort,
                                                  INT4
                                                  *pi4Ieee8021QBridgeProtocolPortGroupId)
{
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
                nextIeee8021QBridgeProtocolPortGroupId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeProtocolPortTable (UINT4
                                                 u4Ieee8021BridgeBasePortComponentId,
                                                 UINT4
                                                 *pu4NextIeee8021BridgeBasePortComponentId,
                                                 UINT4 u4Ieee8021BridgeBasePort,
                                                 UINT4
                                                 *pu4NextIeee8021BridgeBasePort,
                                                 INT4
                                                 i4Ieee8021QBridgeProtocolPortGroupId,
                                                 INT4
                                                 *pi4NextIeee8021QBridgeProtocolPortGroupId)
{
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolPortGroupVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                retValIeee8021QBridgeProtocolPortGroupVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolPortGroupVid (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4Ieee8021QBridgeProtocolPortGroupId,
                                           INT4
                                           *pi4RetValIeee8021QBridgeProtocolPortGroupVid)
{
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                retValIeee8021QBridgeProtocolPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolPortRowStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4Ieee8021QBridgeProtocolPortGroupId,
                                            INT4
                                            *pi4RetValIeee8021QBridgeProtocolPortRowStatus)
{
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeProtocolPortGroupVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                setValIeee8021QBridgeProtocolPortGroupVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeProtocolPortGroupVid (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4
                                             i4Ieee8021QBridgeProtocolPortGroupId,
                                             INT4
                                             i4SetValIeee8021QBridgeProtocolPortGroupVid)
{
}

/****************************************************************************
 Function    :  nmhSetExIeee8021QBridgeProtocolPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                setValIeee8021QBridgeProtocolPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetExIeee8021QBridgeProtocolPortRowStatus (UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4Ieee8021QBridgeProtocolPortGroupId,
                                              INT4
                                              i4SetValIeee8021QBridgeProtocolPortRowStatus)
{
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolPortGroupVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                testValIeee8021QBridgeProtocolPortGroupVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolPortGroupVid (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4Ieee8021QBridgeProtocolPortGroupId,
                                              INT4
                                              i4TestValIeee8021QBridgeProtocolPortGroupVid)
{
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                testValIeee8021QBridgeProtocolPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolPortRowStatus (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4Ieee8021QBridgeProtocolPortGroupId,
                                               INT4
                                               i4TestValIeee8021QBridgeProtocolPortRowStatus)
{
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeProtocolPortTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
