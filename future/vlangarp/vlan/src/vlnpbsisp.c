/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: vlnpbsisp.c,v 1.3 2013/05/28 14:50:30 siva Exp $                     */
/*                                                                           */
/*  FILE NAME             : vlnpbsisp.c                                      */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 16 Feb 2009                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains PB and SISP related routines  */
/*****************************************************************************/

#include "vlaninc.h"

/***************************************************************************/
/* Function Name    : VlanSispCopyIngressEtherTypeToSispPorts              */
/*                                                                         */
/* Description      : This function used to copy the ingress ether type    */
/*                    of a physical or port channel port to all of its     */
/*                    logical ports.                                       */
/*                                                                         */
/* Input(s)         : u4PhyIfIndex  - Physical or Port channel ID.         */
/*                    u2IngressEtherType - Ingress ether type of a phyiscal*/
/*                                         port to be copied to all of its */
/*                                         logical ports.                  */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanSispCopyIngressEtherTypeToSispPorts (UINT4 u4PhyIfIndex,
                                         UINT2 u2IngressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               au2SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4Ctx = 0;
    UINT4               u4PortCnt;
    UINT4               u4TempPortCnt = 0;

    /* !!!!!!!!!!!!!!!!!!!!!!!!!IMPORTANT!!!!!!!!!!!!!!!!!!!!!!
     * This routine will switch contexts for all the sisp interfaces running
     * over this SISP enabled physical interface. The assumption being after
     * invoking this function caller will take care of restoring to the primary
     * context.
     * */

    VLAN_MEMSET (au2SispPorts, VLAN_INIT_VAL, sizeof (au2SispPorts));

    if (VlanVcmSispGetSispPortsInfoOfPhysicalPort (u4PhyIfIndex, VCM_TRUE,
                                                   (VOID *) au2SispPorts,
                                                   &u4PortCnt) == VCM_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, MGMT_TRC,
                         VLAN_NAME, "No logical port are existing for physical"
                         " port %u\n", u4PhyIfIndex);
        return VLAN_SUCCESS;
    }

    /*Scan the logical interface array list */
    for (; u4Ctx < VLAN_SIZING_CONTEXT_COUNT && u4TempPortCnt < u4PortCnt;
         u4Ctx++)
    {
        if (au2SispPorts[u4Ctx] == 0)
        {
            continue;
        }

        u4TempPortCnt++;

        VlanSelectContext (u4Ctx);

        pVlanPortEntry = VLAN_GET_PORT_ENTRY (au2SispPorts[u4Ctx]);

        if (pVlanPortEntry == NULL)
        {
            /* Exceptional case */
            continue;
        }

        if (pVlanPortEntry->u2IngressEtherType != u2IngressEtherType)
        {
            /* This involves programming properties of 
             * (u4PhyIfIndex, u4Ctx) := SISP index. Hence using 
             * u4PhyIfIndex to program the hardware
             * */
            if (VlanHwSetPortIngressEtherType (VLAN_CURR_CONTEXT_ID (),
                                               u4PhyIfIndex,
                                               u2IngressEtherType)
                != VLAN_SUCCESS)
            {
                VlanReleaseContext ();
                return VLAN_FAILURE;
            }

            pVlanPortEntry->u2IngressEtherType = u2IngressEtherType;
        }

        /* Notify ECFM about the change in Ingress Ether Type */
        VlanEcfmEtherTypeChngInd (VLAN_CURR_CONTEXT_ID (),
                                  pVlanPortEntry->u4IfIndex,
                                  ECFM_VLAN_INGRESS_ETHER_TYPE,
                                  u2IngressEtherType);

        VlanReleaseContext ();
    }

    return VLAN_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanSispCopyEgressEtherTypeToSispPorts               */
/*                                                                         */
/* Description      : This function used to copy the egress ether type     */
/*                    of a physical or port channel port to all of its     */
/*                    logical ports.                                       */
/*                                                                         */
/* Input(s)         : u4PhyIfIndex  - Physical or Port channel ID.         */
/*                    u2EgressEtherType - Egress ether type of a phyiscal  */
/*                                         port to be copied to all of its */
/*                                         logical ports.                  */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanSispCopyEgressEtherTypeToSispPorts (UINT4 u4PhyIfIndex,
                                        UINT2 u2EgressEtherType)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               au2SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4Ctx = 0;
    UINT4               u4PortCnt;
    UINT4               u4TempPortCnt = 0;

    VLAN_MEMSET (au2SispPorts, VLAN_INIT_VAL, sizeof (au2SispPorts));

    if (VlanVcmSispGetSispPortsInfoOfPhysicalPort (u4PhyIfIndex, VCM_TRUE,
                                                   (VOID *) au2SispPorts,
                                                   &u4PortCnt) == VCM_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, MGMT_TRC,
                         VLAN_NAME, "No logical port are existing for physical"
                         " port %u\n", u4PhyIfIndex);
        return VLAN_SUCCESS;
    }

    /*Scan the logical interface array list */
    for (; u4Ctx < VLAN_SIZING_CONTEXT_COUNT && u4TempPortCnt < u4PortCnt;
         u4Ctx++)
    {
        if (au2SispPorts[u4Ctx] == 0)
        {
            continue;
        }

        u4TempPortCnt++;

        VlanSelectContext (u4Ctx);
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (au2SispPorts[u4Ctx]);

        if (pVlanPortEntry == NULL)
        {
            /* Exceptional case */
            continue;
        }

        if (pVlanPortEntry->u2EgressEtherType != u2EgressEtherType)
        {
            if (VlanHwSetPortEgressEtherType (VLAN_CURR_CONTEXT_ID (),
                                              u4PhyIfIndex,
                                              u2EgressEtherType)
                != VLAN_SUCCESS)
            {
                VlanReleaseContext ();
                continue;
            }

            pVlanPortEntry->u2EgressEtherType = u2EgressEtherType;
        }

        VlanReleaseContext ();
    }

    return VLAN_SUCCESS;
}
