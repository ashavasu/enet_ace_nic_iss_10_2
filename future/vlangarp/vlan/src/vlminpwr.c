/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *  $Id: vlminpwr.c,v 1.38 2016/03/03 10:19:51 siva Exp $
 *
 * Description: All functions with the port list have the one level of wrapper
                protocol call Function with FsMiWrXXX, the portlist will be 
                converted to array of ports and FsMiXXX call will be made to
                Network Processor API functions
 *******************************************************************/
#include "vlaninc.h"

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwSetAllGroupsPorts                    */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All Groups behavior  */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId                                 */
/*                          PortBmp - PortList                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwSetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                               tLocalPortList PortBmp)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS * sizeof (UINT4));
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
            VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                               (sizeof (UINT4) * VLAN_MAX_PORTS)));
    }
    i4RetVal =
        VlanFsMiVlanHwSetAllGroupsPorts (u4ContextId, VlanId, &HwPortArray);
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwResetAllGroupsPorts                  */
/*                                                                           */
/*    Description         : This function removes the given Ports from       */
/*                          Forward All Group Port list for the specified    */
/*                          Vlan.                                            */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId                                 */
/*                          PortBmp - PortList                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwResetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                 tLocalPortList PortBmp)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
            VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                               (sizeof (UINT4) * VLAN_MAX_PORTS)));
    }
    i4RetVal =
        VlanFsMiVlanHwResetAllGroupsPorts (u4ContextId, VlanId, &HwPortArray);
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwSetUnRegGroupsPorts                      */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All unreg groups     */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : VlanId - VlanId                                  */
/*                          PortBmp  - PortList.                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwSetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                 tLocalPortList PortBmp)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
            VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                               (sizeof (UINT4) * VLAN_MAX_PORTS)));
    }
    i4RetVal =
        VlanFsMiVlanHwSetUnRegGroupsPorts (u4ContextId, VlanId, &HwPortArray);
    /* if Memory allocated then free it */
    if (HwPortArray.pu4PortArray != NULL)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwResetUnRegGroupsPorts                    */
/*                                                                           */
/*    Description         : This function removes the given Ports from       */
/*                          Forward UnReg Group Port list for the specified  */
/*                          Vlan.                                            */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId                                 */
/*                          PortBmp - PortList                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwResetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                   tLocalPortList PortBmp)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
            VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                               (sizeof (UINT4) * VLAN_MAX_PORTS)));
    }
    i4RetVal =
        VlanFsMiVlanHwResetUnRegGroupsPorts (u4ContextId, VlanId, &HwPortArray);
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwAddMcastEntry                            */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Multicast table.                                 */
/*                          This function can be called, when the Multicast  */
/*                          entry is already present in the Hardware. In     */
/*                          which case the Old Multicast member ports        */
/*                          must be removed and the new ports ('PortBmp')    */
/*                          must be added in the Hardware.                   */
/*                                                                           */
/*    Input(s)            : VlanId   - VlanId                                */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                          PortBmp - List of Ports.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwAddMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                           tLocalPortList PortBmp)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
            VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                               (sizeof (UINT4) * VLAN_MAX_PORTS)));
    }
    i4RetVal =
        VlanFsMiVlanHwAddMcastEntry (u4ContextId, VlanId, MacAddr,
                                     &HwPortArray);
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwAddStMcastEntry                      */
/*                                                                           */
/*    Description         : This function adds a static entry to the         */
/*                          hardware Multicast table                         */
/*                                                                           */
/*    Input(s)            : VlanId    - VlanId                               */
/*                          pMacAddr  - Pointer to the Mac Address.          */
/*                          i4RcvPort - Received port                        */
/*                          PortBmp   - List of Ports.                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwAddStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                             tMacAddr MacAddr, INT4 i4RcvPort,
                             tLocalPortList PortBmp)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    UNUSED_PARAM (i4RcvPort);

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray =
            (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                               (sizeof (UINT4) * VLAN_MAX_PORTS)));
    }
    i4RetVal = VlanFsMiVlanHwAddMcastEntry (u4ContextId, VlanId, MacAddr,
                                            &HwPortArray);

    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwAddVlanEntry                             */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Vlan table.                                      */
/*                                                                           */
/*    Input(s)            : VlanId     - VlanId                              */
/*                          PortBmp - List of Ports.                         */
/*                          UnTagPortBmp - List of Untagged Ports.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwAddVlanEntry (UINT4 u4ContextId, tVlanId VlanId,
                          tLocalPortList PortBmp, tLocalPortList UnTagPortBmp)
{
    tHwPortArray        HwPortArray;
    tHwPortArray        HwUnTagPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    UINT2               u2NumUntagPorts = 0;
    INT4                i4RetVal = FNP_FAILURE;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));
    MEMSET (&HwUnTagPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        if (u2NumPorts != 0)
        {
            HwPortArray.i4Length = u2NumPorts;
            HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
                VLAN_GET_BUF (VLAN_MISC, u4Size);
            if (HwPortArray.pu4PortArray == NULL)
            {
                return FNP_FAILURE;
            }
            MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                    MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                                   (sizeof (UINT4) * VLAN_MAX_PORTS)));
        }
    }
    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    if (MEMCMP (gNullPortList, UnTagPortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwUnTagPortArray.i4Length = u2NumUntagPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (UnTagPortBmp, au4PortArray,
                                         &u2NumUntagPorts);
        if (u2NumUntagPorts != 0)
        {
            HwUnTagPortArray.i4Length = u2NumUntagPorts;
            HwUnTagPortArray.pu4PortArray = (UINT4 *) (VOID *)
                VLAN_GET_BUF (VLAN_MISC, u4Size);

            if (HwUnTagPortArray.pu4PortArray == NULL)
            {
                if (u2NumPorts != 0)
                {
                    VLAN_RELEASE_BUF (VLAN_MISC,
                                      (UINT1 *) HwPortArray.pu4PortArray);
                }

                return FNP_FAILURE;
            }
            MEMCPY (HwUnTagPortArray.pu4PortArray, au4PortArray,
                    MEM_MAX_BYTES ((sizeof (UINT4) * u2NumUntagPorts),
                                   (sizeof (UINT4) * VLAN_MAX_PORTS)));
        }
    }
    /* Check for VPLS_VLAN. If the vlan is VPLS_VLAN, Vlan Entry should not taken place */
    if (VlanId <= VLAN_VFI_MIN_ID)
    {
        i4RetVal =
            VlanFsMiVlanHwAddVlanEntry (u4ContextId, VlanId, &HwPortArray,
                                        &HwUnTagPortArray);
    }
    else if (VLAN_VFI_MAX_ID == VLAN_VFI_MIN_ID)
    {
        /* No VPLS VLAN exist */
        i4RetVal = FNP_FAILURE;
    }
    else
    {
        /* VLAN ID greater than VLAN_VFI_MIN_ID is for VPLS VLAN.
           So returning success */
        i4RetVal = FNP_SUCCESS;
    }

    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    if (u2NumUntagPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwUnTagPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwAddStaticUcastEntry                  */
/*                                                                           */
/*    Description         : This function adds a static unicast entry to the */
/*                          hardware table                         */
/*                                                                           */
/*    Input(s)            : VlanId    - VlanId                               */
/*                          pMacAddr  - Pointer to the Mac Address.          */
/*                          i4RcvPort - Received port                        */
/*                          PortBmp   - List of Ports.                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwAddStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                 tMacAddr MacAddr, UINT4 u4Port,
                                 tLocalPortList AllowedToGoPortBmp,
                                 UINT1 u1Status, tMacAddr ConnectionId)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;
    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, AllowedToGoPortBmp,
                sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (AllowedToGoPortBmp,
                                         au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
            VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                               (sizeof (UINT4) * VLAN_MAX_PORTS)));
    }
    if (VLAN_ARE_MAC_ADDR_EQUAL (ConnectionId, gNullMacAddress) == VLAN_TRUE)
    {
        i4RetVal = VlanFsMiVlanHwAddStaticUcastEntry (u4ContextId, u4Fid,
                                                      MacAddr, u4Port,
                                                      &HwPortArray, u1Status);
    }
    else
    {
        i4RetVal = VlanFsMiVlanHwAddStaticUcastEntryEx (u4ContextId, u4Fid,
                                                        MacAddr, u4Port,
                                                        &HwPortArray, u1Status,
                                                        ConnectionId);
    }
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : FsMiWrVlanHwMacLearningStatus                        */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a VLAN.                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanId  - VLAN identifier.                           */
/*                      u2Fdbid  - FDB identifier.                           */
/*                      PortBmp - Static Member port list                    */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiWrVlanHwMacLearningStatus (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2FdbId,
                               tLocalPortList PortBmp, UINT1 u1Status)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;
    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /*convert the port list in to array of ports */
    VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

    HwPortArray.i4Length = u2NumPorts;
    HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
        VLAN_GET_BUF (VLAN_MISC, u4Size);
    if (HwPortArray.pu4PortArray == NULL)
    {
        return FNP_FAILURE;
    }
    MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
            MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                           (sizeof (UINT4) * VLAN_MAX_PORTS)));

    i4RetVal = VlanFsMiVlanHwMacLearningStatus (u4ContextId, VlanId, u2FdbId,
                                                &HwPortArray, u1Status);

    VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);

    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwGetVlanInfo                          */
/*                                                                           */
/*    Description         : This function returns the VLAN membership of the */
/*                          given VLAN in the hardware if it is present.     */
/*                          The VLAN membership returned should not include  */
/*                          the physical ports that are part of aggregation  */
/*                          group. i.e. if P1, P2 and P3 are members of      */
/*                          agg. group P26, then the port list returned      */
/*                          should contain P26 and not P1, P2 and P3.        */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN id for which the info needs to be  */
/*                          retrieved.                                       */
/*                                                                           */
/*    Output(s)           : pHwEntry - VLAN info pointer.                    */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwGetVlanInfo (UINT4 u4ContextId, tVlanId VlanId,
                         tHwMiVlanEntry * pHwEntry)
{
    tHwPortArray        HwMemberPortArray;
    tHwPortArray        HwUntagPortArray;
    tHwPortArray        HwFwdAllPortArray;
    tHwPortArray        HwFwdUnregPortArray;
    tHwVlanPortArray    HwVlanPortArray;
    INT4                i4RetVal = FNP_FAILURE;
    UINT4               u4Size = 0;

    UNUSED_PARAM (u4ContextId);

    u4Size = VLAN_MISC_SIZE;

    MEMSET (&HwMemberPortArray, 0, sizeof (tHwPortArray));
    MEMSET (&HwUntagPortArray, 0, sizeof (tHwPortArray));
    MEMSET (&HwFwdAllPortArray, 0, sizeof (tHwPortArray));
    MEMSET (&HwFwdUnregPortArray, 0, sizeof (tHwPortArray));
    MEMSET (&HwVlanPortArray, 0, sizeof (tHwVlanPortArray));

    HwMemberPortArray.i4Length = VLAN_MAX_PORTS;
    HwMemberPortArray.pu4PortArray = (UINT4 *) (VOID *)
        VLAN_GET_BUF (VLAN_MISC, u4Size);
    if (HwMemberPortArray.pu4PortArray == NULL)
    {
        return FNP_FAILURE;
    }
    VLAN_MEMSET (HwMemberPortArray.pu4PortArray, 0, u4Size);
    HwUntagPortArray.i4Length = VLAN_MAX_PORTS;
    HwUntagPortArray.pu4PortArray = (UINT4 *) (VOID *)
        VLAN_GET_BUF (VLAN_MISC, u4Size);
    if (HwUntagPortArray.pu4PortArray == NULL)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwMemberPortArray.pu4PortArray);
        return FNP_FAILURE;
    }
    VLAN_MEMSET (HwUntagPortArray.pu4PortArray, 0, u4Size);
    HwFwdAllPortArray.i4Length = VLAN_MAX_PORTS;
    HwFwdAllPortArray.pu4PortArray = (UINT4 *) (VOID *)
        VLAN_GET_BUF (VLAN_MISC, u4Size);
    if (HwFwdAllPortArray.pu4PortArray == NULL)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwMemberPortArray.pu4PortArray);
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwUntagPortArray.pu4PortArray);
        return FNP_FAILURE;
    }
    VLAN_MEMSET (HwFwdAllPortArray.pu4PortArray, 0, u4Size);
    HwFwdUnregPortArray.i4Length = VLAN_MAX_PORTS;
    HwFwdUnregPortArray.pu4PortArray = (UINT4 *) (VOID *)
        VLAN_GET_BUF (VLAN_MISC, u4Size);
    if (HwFwdUnregPortArray.pu4PortArray == NULL)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwMemberPortArray.pu4PortArray);
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwUntagPortArray.pu4PortArray);
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwFwdAllPortArray.pu4PortArray);
        return FNP_FAILURE;
    }
    VLAN_MEMSET (HwFwdUnregPortArray.pu4PortArray, 0, u4Size);
    HwVlanPortArray.pHwMemberPortArray = &HwMemberPortArray;
    HwVlanPortArray.pHwUntagPortArray = &HwUntagPortArray;
    HwVlanPortArray.pHwFwdAllPortArray = &HwFwdAllPortArray;
    HwVlanPortArray.pHwFwdUnregPortArray = &HwFwdUnregPortArray;

    i4RetVal =
        VlanFsMiVlanHwGetVlanInfo (u4ContextId, VlanId, &HwVlanPortArray);

    if (i4RetVal == FNP_SUCCESS)
    {
        if (VlanConvertPortArrayToLocalPortList
            (HwMemberPortArray.pu4PortArray, HwMemberPortArray.i4Length,
             pHwEntry->HwMemberPbmp) != VLAN_SUCCESS)
        {
            i4RetVal = FNP_FAILURE;
        }

        if (VlanConvertPortArrayToLocalPortList
            (HwUntagPortArray.pu4PortArray, HwUntagPortArray.i4Length,
             pHwEntry->HwUntagPbmp) != VLAN_SUCCESS)
        {
            i4RetVal = FNP_FAILURE;
        }

        if (VlanConvertPortArrayToLocalPortList
            (HwFwdAllPortArray.pu4PortArray, HwFwdAllPortArray.i4Length,
             pHwEntry->HwFwdAllPbmp) != VLAN_SUCCESS)
        {
            i4RetVal = FNP_FAILURE;
        }

        if (VlanConvertPortArrayToLocalPortList
            (HwFwdUnregPortArray.pu4PortArray, HwFwdUnregPortArray.i4Length,
             pHwEntry->HwFwdUnregPbmp) != VLAN_SUCCESS)
        {
            i4RetVal = FNP_FAILURE;
        }
    }

    /* Free the allocated Memory */
    VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwMemberPortArray.pu4PortArray);
    VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwUntagPortArray.pu4PortArray);
    VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwFwdAllPortArray.pu4PortArray);
    VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwFwdUnregPortArray.pu4PortArray);

    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwGetMcastEntry                        */
/*                                                                           */
/*    Description         : This function is used to get the                 */
/*                          multicast entry in the hardware table.           */
/*                                                                           */
/*    Input(s)            : ContextId - Context Identifier                   */
/*                          VlanId    - VLAN ID                              */
/*                          MacAddr   - MAC Address                          */
/*                          HwPortList - PortList                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwGetMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                           tLocalPortList HwPortList)
{
    tHwPortArray        HwPortArray;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    HwPortArray.i4Length = VLAN_MAX_PORTS;
    HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
        VLAN_GET_BUF (VLAN_MISC, u4Size);
    if (HwPortArray.pu4PortArray == NULL)
    {
        return FNP_FAILURE;
    }

    i4RetVal = VlanFsMiVlanHwGetMcastEntry (u4ContextId, VlanId,
                                            MacAddr, &HwPortArray);

    if (i4RetVal == FNP_SUCCESS)
    {
        if (VlanConvertPortArrayToLocalPortList
            (HwPortArray.pu4PortArray, HwPortArray.i4Length,
             HwPortList) != VLAN_SUCCESS)
        {
            i4RetVal = FNP_FAILURE;
        }
    }
    /* Free the allocated Memory */
    VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwSetFidPortLearningStatus             */
/*                                                                           */
/*    Description         : This function removes the given Ports from       */
/*                          Forward All Group Port list for the specified    */
/*                          Vlan.                                            */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId                                 */
/*                          PortBmp - PortList                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid,
                                      tLocalPortList PortBmp, UINT4 u4IfIndex,
                                      UINT1 u1Action)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
            VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                               (sizeof (UINT4) * VLAN_MAX_PORTS)));
    }

    i4RetVal = (VlanFsMiVlanHwSetFidPortLearningStatus
                (u4ContextId, u4Fid, HwPortArray, u4IfIndex, u1Action));

    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwGetStMcastEntry                      */
/*                                                                           */
/*    Description         : This function is used to get the static          */
/*                          multicast entry in the hardware table.           */
/*                                                                           */
/*    Input(s)            : ContextId - Context Identifier                   */
/*                          VlanId    - VLAN ID                              */
/*                          MacAddr   - MAC Address                          */
/*                          u4RcvPort - Receive port                         */
/*                          HwPortList - PortList                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwGetStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                             tMacAddr MacAddr, UINT4 u4RcvPort,
                             tLocalPortList HwPortList)
{
    tHwPortArray        HwPortArray;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    HwPortArray.i4Length = VLAN_MAX_PORTS;
    HwPortArray.pu4PortArray =
        (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC, u4Size);
    if (HwPortArray.pu4PortArray == NULL)
    {
        return FNP_FAILURE;
    }

    i4RetVal = VlanFsMiVlanHwGetStMcastEntry (u4ContextId, VlanId,
                                              MacAddr, u4RcvPort, &HwPortArray);

    if (i4RetVal == FNP_SUCCESS)
    {
        if (VlanConvertPortArrayToLocalPortList
            (HwPortArray.pu4PortArray, HwPortArray.i4Length,
             HwPortList) != VLAN_SUCCESS)
        {
            i4RetVal = FNP_FAILURE;
        }
    }
    /* Free the allocated Memory */
    VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwGetStMcastEntry                      */
/*                                                                           */
/*    Description         : This function is used to get the static          */
/*                          multicast entry in the hardware table.           */
/*                                                                           */
/*    Input(s)            : ContextId - Context Identifier                   */
/*                          u4Fid     - FDB ID                               */
/*                          MacAddr   - MAC Address                          */
/*                          u4Port    - Receive port                         */
/*                          HwPortList - PortList                            */
/*                          pu1Status - Status.                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwGetStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                 tMacAddr MacAddr, UINT4 u4Port,
                                 tLocalPortList AllowedToGoPortBmp,
                                 UINT1 *pu1Status)
{
    tHwPortArray        HwPortArray;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    HwPortArray.i4Length = VLAN_MAX_PORTS;
    HwPortArray.pu4PortArray =
        (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC, u4Size);
    if (HwPortArray.pu4PortArray == NULL)
    {
        return FNP_FAILURE;
    }

    i4RetVal = VlanFsMiVlanHwGetStaticUcastEntry (u4ContextId, u4Fid,
                                                  MacAddr, u4Port,
                                                  &HwPortArray, pu1Status);
    if (i4RetVal == FNP_SUCCESS)
    {
        if (VlanConvertPortArrayToLocalPortList
            (HwPortArray.pu4PortArray, HwPortArray.i4Length,
             AllowedToGoPortBmp) != VLAN_SUCCESS)
        {
            i4RetVal = FNP_FAILURE;
        }
    }
    /* Free the allocated Memory */
    VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwGetStUcastEntryEx                    */
/*                                                                           */
/*    Description         : This function is used to get the static          */
/*                          multicast entry in the hardware table.           */
/*                                                                           */
/*    Input(s)            : ContextId - Context Identifier                   */
/*                          u4Fid     - FDB ID                               */
/*                          MacAddr   - MAC Address                          */
/*                          u4Port    - Receive port                         */
/*                          HwPortList - PortList                            */
/*                          pu1Status - Status.         
 *                          ConnectionId - Back Bone Mac Address             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwGetStaticUcastEntryEx (UINT4 u4ContextId, UINT4 u4Fid,
                                   tMacAddr MacAddr, UINT4 u4Port,
                                   tLocalPortList AllowedToGoPortBmp,
                                   UINT1 *pu1Status, tMacAddr ConnectionId)
{
    tHwPortArray        HwPortArray;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    HwPortArray.i4Length = VLAN_MAX_PORTS;
    HwPortArray.pu4PortArray =
        (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC, u4Size);
    if (HwPortArray.pu4PortArray == NULL)
    {
        return FNP_FAILURE;
    }

    i4RetVal = VlanFsMiVlanHwGetStaticUcastEntryEx (u4ContextId, u4Fid,
                                                    MacAddr, u4Port,
                                                    &HwPortArray, pu1Status,
                                                    ConnectionId);
    if (i4RetVal == FNP_SUCCESS)
    {
        if (VlanConvertPortArrayToLocalPortList
            (HwPortArray.pu4PortArray, HwPortArray.i4Length,
             AllowedToGoPortBmp) != VLAN_SUCCESS)
        {
            i4RetVal = FNP_FAILURE;
        }
    }
    /* Free the allocated Memory */
    VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    return i4RetVal;
}

#endif
#ifdef MBSM_WANTED

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwAddVlanEntry                     */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Vlan table.                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - Vlan Index                         */
/*                          PortBmp     - Egress Portlist                    */
/*                          UnTagPortBmp- Untagged Portlist                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwAddVlanEntry (UINT4 u4ContextId, tVlanId VlanId,
                              tLocalPortList PortBmp,
                              tLocalPortList UnTagPortBmp,
                              tMbsmSlotInfo * pSlotInfo)
{
    tHwPortArray        HwPortArray;
    tHwPortArray        HwUnTagPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    UINT2               u2NumUntagPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;
    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));
    MEMSET (&HwUnTagPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray =
            (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                sizeof (UINT4) * u2NumPorts);
    }
    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    if (MEMCMP (gNullPortList, UnTagPortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwUnTagPortArray.i4Length = u2NumUntagPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (UnTagPortBmp, au4PortArray,
                                         &u2NumUntagPorts);

        HwUnTagPortArray.i4Length = u2NumUntagPorts;
        HwUnTagPortArray.pu4PortArray =
            (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwUnTagPortArray.pu4PortArray == NULL)
        {
            if (u2NumPorts != 0)
            {
                VLAN_RELEASE_BUF (VLAN_MISC,
                                  (UINT1 *) HwPortArray.pu4PortArray);
            }

            return FNP_FAILURE;
        }
        MEMCPY (HwUnTagPortArray.pu4PortArray, au4PortArray,
                sizeof (UINT4) * u2NumUntagPorts);
    }
    i4RetVal =
        VlanFsMiVlanMbsmHwAddVlanEntry (u4ContextId, VlanId, &HwPortArray,
                                        &HwUnTagPortArray, pSlotInfo);
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    if (u2NumUntagPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwUnTagPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwAddStaticUcastEntry              */
/*                                                                           */
/*    Description         : This function adds a static unicast entry to the */
/*                          hardware table                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId        - Context Identifier          */
/*                          u4Fid              - FID                         */
/*                          MacAddr            - Mac Address.                */
/*                          u4Port             - Received port               */
/*                          AllowedToGoPortBmp - List of Ports.              */
/*                          u1Status           - Status of entry takes       */
/*                          values like  permanent, deleteOnReset and        */
/*                          deleteOnTimeout.                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwAddStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid,
                                     tMacAddr MacAddr, UINT4 u4Port,
                                     tLocalPortList AllowedToGoPortBmp,
                                     UINT1 u1Status, tMbsmSlotInfo * pSlotInfo,
                                     tMacAddr ConnectionId)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, AllowedToGoPortBmp,
                sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (AllowedToGoPortBmp,
                                         au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray =
            (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                sizeof (UINT4) * u2NumPorts);
    }

    if (VLAN_ARE_MAC_ADDR_EQUAL (ConnectionId, gNullMacAddress) == VLAN_TRUE)
    {
        i4RetVal = VlanFsMiVlanMbsmHwAddStaticUcastEntry
            (u4ContextId, u4Fid, MacAddr, u4Port,
             &HwPortArray, u1Status, pSlotInfo);
    }
    else
    {
        i4RetVal = VlanFsMiVlanMbsmHwAddStaticUcastEntryEx
            (u4ContextId, u4Fid, MacAddr, u4Port,
             &HwPortArray, u1Status, ConnectionId, pSlotInfo);
    }
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwAddStMcastEntry                  */
/*                                                                           */
/*    Description         : This function adds a static entry to the         */
/*                          hardware Multicast table                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          MacAddr     - Mac Address.                       */
/*                          i4RcvPort   - Received port                      */
/*                          PortBmp     - List of Ports.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwAddStMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                                 tMacAddr MacAddr, INT4 i4RcvPort,
                                 tLocalPortList PortBmp,
                                 tMbsmSlotInfo * pSlotInfo)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray =
            (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                sizeof (UINT4) * u2NumPorts);
    }
    i4RetVal = VlanFsMiVlanMbsmHwAddStMcastEntry (u4ContextId, VlanId, MacAddr,
                                                  i4RcvPort, &HwPortArray,
                                                  pSlotInfo);
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwAddMcastEntry                    */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Multicast table.                                 */
/*                          This function can be called, when the Multicast  */
/*                          entry is already present in the Hardware. In     */
/*                          which case the Old Multicast member ports        */
/*                          must be removed and the new ports ('PortBmp')    */
/*                          must be added in the Hardware.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          MacAddr     - Mac Address.                       */
/*                          PortBmp     - List of Ports.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwAddMcastEntry (UINT4 u4ContextId, tVlanId VlanId,
                               tMacAddr MacAddr, tLocalPortList PortBmp,
                               tMbsmSlotInfo * pSlotInfo)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray =
            (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                sizeof (UINT4) * u2NumPorts);
    }
    i4RetVal =
        VlanFsMiVlanMbsmHwAddMcastEntry (u4ContextId, VlanId, MacAddr,
                                         &HwPortArray, pSlotInfo);
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwSetAllGroupsPorts                */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All Groups behavior  */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          PortBmp     - PortList                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwSetAllGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                   tLocalPortList PortBmp,
                                   tMbsmSlotInfo * pSlotInfo)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS * sizeof (UINT4));
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray =
            (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                sizeof (UINT4) * u2NumPorts);
    }
    i4RetVal = VlanFsMiVlanMbsmHwSetAllGroupsPorts (u4ContextId, VlanId,
                                                    &HwPortArray, pSlotInfo);
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanMbsmHwSetUnRegGroupsPorts              */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All unreg groups     */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          VlanId      - VlanId                             */
/*                          PortBmp     - PortList.                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwSetUnRegGroupsPorts (UINT4 u4ContextId, tVlanId VlanId,
                                     tLocalPortList PortBmp,
                                     tMbsmSlotInfo * pSlotInfo)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray =
            (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                sizeof (UINT4) * u2NumPorts);
    }
    i4RetVal =
        VlanFsMiVlanMbsmHwSetUnRegGroupsPorts (u4ContextId, VlanId,
                                               &HwPortArray, pSlotInfo);
    /* if Memory allocated then free it */
    if (HwPortArray.pu4PortArray != NULL)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : FsMiWrVlanMbsmHwEvbConfigSChIface                  */
/*                                                                           */
/* Description        : This function is called when the S-Channel is        */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : pVlanEvbHwConfigInfo - Vlan EVB hardware             */
/*                                             configuration information     */
/*                      pSlotInfo            - MBSM information              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On success                             */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiWrVlanMbsmHwEvbConfigSChIface (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo,
                                     tMbsmSlotInfo * pSlotInfo)
{
    return(VlanFsMiVlanMbsmHwEvbConfigSChIface(pVlanEvbHwConfigInfo,
                                      pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiWrVlanMbsmHwSetBridgePortType                      */
/*                                                                           */
/* Description        : This function is called when the Bridge Port
                        Type is configured for a port.                       */
/*                                                                           */
/* Input(s)           : tVlanHwPortInfo - UAP Port Info                      */
/*                        tMbsmSlotInfo  - Mbsm slot Info                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
FsMiWrVlanMbsmHwSetBridgePortType (tVlanHwPortInfo *pVlanHwPortInfo,
                                  tMbsmSlotInfo * pSlotInfo)
{
    return (VlanFsMiVlanMbsmHwSetBridgePortType(pVlanHwPortInfo,
                                                pSlotInfo));
}


#endif

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwSetUnRegGroupsPortsForSisp           */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All unreg groups     */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : VlanId - VlanId                                  */
/*                          PortBmp  - PortList.                             */
/*                          u4PortId - Destination port id                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                           */
/*****************************************************************************/
INT4
FsMiWrVlanHwSetUnRegGroupsPortsForSisp (UINT4 u4ContextId, tVlanId VlanId,
                                        tLocalPortList PortBmp, UINT4 u4PortId)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS);
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        /*Add the port of the primary context */
        au4PortArray[u2NumPorts] = u4PortId;
        u2NumPorts++;

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
            VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                               (sizeof (UINT4) * VLAN_MAX_PORTS)));
    }
    i4RetVal =
        VlanFsMiVlanHwSetUnRegGroupsPorts (u4ContextId, VlanId, &HwPortArray);
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwSetAllGroupsPortsForSisp             */
/*                                                                           */
/*    Description         : This function takes care of updating the         */
/*                          hardware table for  Forward All Groups behavior  */
/*                          for the specified VLAN ID.                       */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId                                 */
/*                          PortBmp - PortList                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : FNP_SUCCESS / FNP_FAILURE                         */
/*****************************************************************************/
INT4
FsMiWrVlanHwSetAllGroupsPortsForSisp (UINT4 u4ContextId, tVlanId VlanId,
                                      tLocalPortList PortBmp, UINT4 u4PortId)
{
    tHwPortArray        HwPortArray;
    UINT4               au4PortArray[VLAN_MAX_PORTS];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal;
    UINT4               u4Size = 0;

    u4Size = VLAN_MISC_SIZE;

    MEMSET (au4PortArray, 0, VLAN_MAX_PORTS * sizeof (UINT4));
    MEMSET (&HwPortArray, 0, sizeof (tHwPortArray));

    /* if the port list is NULL, no need to convert */
    if (MEMCMP (gNullPortList, PortBmp, sizeof (tLocalPortList)) == 0)
    {
        HwPortArray.i4Length = u2NumPorts;
    }
    else
    {
        /*convert the port list in to array of ports */
        VlanConvertLocalPortListToArray (PortBmp, au4PortArray, &u2NumPorts);

        /*Add the port of the primary context */
        au4PortArray[u2NumPorts] = u4PortId;
        u2NumPorts++;

        HwPortArray.i4Length = u2NumPorts;
        HwPortArray.pu4PortArray = (UINT4 *) (VOID *)
            VLAN_GET_BUF (VLAN_MISC, u4Size);
        if (HwPortArray.pu4PortArray == NULL)
        {
            return FNP_FAILURE;
        }
        MEMCPY (HwPortArray.pu4PortArray, au4PortArray,
                MEM_MAX_BYTES ((sizeof (UINT4) * u2NumPorts),
                               (sizeof (UINT4) * VLAN_MAX_PORTS)));
    }
    i4RetVal =
        VlanFsMiVlanHwSetAllGroupsPorts (u4ContextId, VlanId, &HwPortArray);
    /* if Memory allocated then free it */
    if (u2NumPorts != 0)
    {
        VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FsMiWrVlanHwResetDefGroupInfoForPort             */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          reset the default group information.             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual Switch Context Identfier   */
/*                          VlanId - Vlan identifier number                  */
/*                          u4IfIndex - Interface Index                      */
/*                          u1Type  - All Groups / Unreg Groups              */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : FNP_SUCCESS / FNP_FAILURE                   */
/*                                                                           */
/*****************************************************************************/

INT4
FsMiWrVlanHwResetDefGroupInfoForPort (UINT4 u4ContextId, tVlanId VlanId,
                                      UINT4 u4IfIndex, UINT1 u1Type)
{
    tHwPortArray        HwPortArray;
    INT4                i4RetVal = FNP_SUCCESS;

    HwPortArray.i4Length = 1;
    HwPortArray.pu4PortArray = (UINT4 *) (VOID *) VLAN_GET_BUF (VLAN_MISC,
                                                                VLAN_MISC_SIZE);

    if (HwPortArray.pu4PortArray == NULL)
    {
        return FNP_FAILURE;
    }
    MEMSET (HwPortArray.pu4PortArray, 0, VLAN_MISC_SIZE);
    HwPortArray.pu4PortArray[0] = u4IfIndex;

    if (u1Type == VLAN_ALL_GROUPS)
    {

        if (VlanFsMiVlanHwResetAllGroupsPorts (u4ContextId,
                                               VlanId,
                                               &HwPortArray) != FNP_SUCCESS)
        {
            i4RetVal = FNP_FAILURE;
        }

    }
    else
    {
        if (VlanFsMiVlanHwResetUnRegGroupsPorts (u4ContextId,
                                                 VlanId, &HwPortArray)
            != FNP_SUCCESS)
        {
            i4RetVal = FNP_FAILURE;
        }
    }

    VLAN_RELEASE_BUF (VLAN_MISC, (UINT1 *) HwPortArray.pu4PortArray);

    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : FsMiWrVlanHwConfigSChInterface                         */
/*                                                                           */
/* Description        : This function is called when the S-Channel is        */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : pVlanEvbHwConfigInfo - Vlan EVB hardware             */
/*                                             configuration information     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiWrVlanHwEvbConfigSChIface (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo)
{
    return (VlanFsMiVlanHwEvbConfigSChIface(pVlanEvbHwConfigInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiWrVlanHwSetBridgePortType                          */
/*                                                                           */
/* Description        : This function is called when the Bridge Port 
                        Type is configured for a port.                       */
/*                                                                           */
/* Input(s)           : tVlanHwPortInfo - UAP Port Info                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
FsMiWrVlanHwSetBridgePortType (tVlanHwPortInfo *pVlanHwPortInfo)
{
     return (VlanFsMiVlanHwSetBridgePortType(pVlanHwPortInfo));
}


