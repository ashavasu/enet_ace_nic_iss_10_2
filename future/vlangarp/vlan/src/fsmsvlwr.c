
/* $Id: fsmsvlwr.c,v 1.8 2011/10/25 10:39:08 siva Exp $*/
# include  "lr.h"
# include  "vlaninc.h"
# include  "fssnmp.h"
# include  "fsmsvlwr.h"
# include  "fsmsvldb.h"

INT4
GetNextIndexFsDot1qBaseTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qBaseTable (&i4FsDot1qVlanContextId) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qBaseTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1qVlanContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

VOID
RegisterFSMSVL ()
{
    SNMPRegisterMib (&fsmsvlOID, &fsmsvlEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsmsvlOID, (const UINT1 *) "fsmsvlan");
}

VOID
UnRegisterFSMSVL ()
{
    SNMPUnRegisterMib (&fsmsvlOID, &fsmsvlEntry);
    SNMPDelSysorEntry (&fsmsvlOID, (const UINT1 *) "fsmsvlan");
}

INT4
FsDot1qVlanVersionNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qBaseTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qVlanVersionNumber (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qMaxVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qBaseTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1qMaxVlanId (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qMaxSupportedVlansGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qBaseTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qMaxSupportedVlans (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qNumVlansGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qBaseTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1qNumVlans (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qGvrpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qBaseTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    GARP_LOCK ();

    i4RetVal = nmhGetFsDot1qGvrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1qGvrpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhSetFsDot1qGvrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1qGvrpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2FsDot1qGvrpStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1qBaseTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qBaseTable (pu4Error, pSnmpIndexList,
                                         pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qFdbTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qFdbId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qFdbTable (&i4FsDot1qVlanContextId,
                                             &u4FsDot1qFdbId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qFdbTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4FsDot1qFdbId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qFdbId;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qFdbDynamicCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qFdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qFdbDynamicCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
GetNextIndexFsDot1qTpFdbTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qFdbId;
    tMacAddr           *pFsDot1qTpFdbAddress;

    pFsDot1qTpFdbAddress = (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList;    /* not-accessible */

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qTpFdbTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qFdbId,
             pFsDot1qTpFdbAddress) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qTpFdbTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qFdbId,
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList, pFsDot1qTpFdbAddress) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qFdbId;
    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qTpFdbPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceFsDot1qTpFdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1qTpFdbPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       (*(tMacAddr *) pMultiIndex->pIndex[2].
                                        pOctetStrValue->pu1_OctetList),
                                       &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qTpFdbStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceFsDot1qTpFdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1qTpFdbStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         (*(tMacAddr *) pMultiIndex->pIndex[2].
                                          pOctetStrValue->pu1_OctetList),
                                         &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qTpFdbPwGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsDot1qTpFdbTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1qTpFdbPw (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     (*(tMacAddr *) pMultiIndex->pIndex[2].
                                      pOctetStrValue->pu1_OctetList),
                                     &(pMultiData->u4_ULongValue));
    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1qTpGroupTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;
    INT4                i4FsDot1qTpPort;
    tMacAddr           *pFsDot1qTpGroupAddress;

    pFsDot1qTpGroupAddress = (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList;    /* not-accessible */

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qTpGroupTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qVlanIndex,
             pFsDot1qTpGroupAddress, &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qTpGroupTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qVlanIndex,
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList, pFsDot1qTpGroupAddress,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;
    pNextMultiIndex->pIndex[3].i4_SLongValue = i4FsDot1qTpPort;
    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qTpGroupIsLearntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceFsDot1qTpGroupTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qTpGroupIsLearnt (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[2].
                                       pOctetStrValue->pu1_OctetList),
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
GetNextIndexFsDot1qForwardAllLearntPortTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;
    INT4                i4FsDot1qTpPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qForwardAllLearntPortTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qVlanIndex,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qForwardAllLearntPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qVlanIndex,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4FsDot1qTpPort;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qForwardAllIsLearntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qForwardAllLearntPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qForwardAllIsLearnt (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
GetNextIndexFsDot1qForwardAllStatusTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;

    VLAN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qForwardAllStatusTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qForwardAllStatusTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4FsDot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qForwardAllRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsDot1qForwardAllStatusTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qForwardAllRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qForwardAllRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qForwardAllRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qForwardAllRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qForwardAllRowStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qForwardAllStatusTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qForwardAllStatusTable (pu4Error, pSnmpIndexList,
                                                     pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qForwardAllPortConfigTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;
    INT4                i4FsDot1qTpPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qForwardAllPortConfigTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qVlanIndex,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qForwardAllPortConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qVlanIndex,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4FsDot1qTpPort;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qForwardAllPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();
    if (nmhValidateIndexInstanceFsDot1qForwardAllPortConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qForwardAllPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qForwardAllPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qForwardAllPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qForwardAllPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qForwardAllPort (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qForwardAllPortConfigTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qForwardAllPortConfigTable (pu4Error,
                                                         pSnmpIndexList,
                                                         pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qForwardUnregLearntPortTable (tSnmpIndex * pFirstMultiIndex,
                                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;
    INT4                i4FsDot1qTpPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qForwardUnregLearntPortTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qVlanIndex,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qForwardUnregLearntPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qVlanIndex,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4FsDot1qTpPort;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qForwardUnregIsLearntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qForwardUnregLearntPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qForwardUnregIsLearnt (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
GetNextIndexFsDot1qForwardUnregStatusTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qForwardUnregStatusTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qForwardUnregStatusTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4FsDot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qForwardUnregRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qForwardUnregStatusTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qForwardUnregRowStatus (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qForwardUnregRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qForwardUnregRowStatus (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qForwardUnregRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qForwardUnregRowStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qForwardUnregStatusTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qForwardUnregStatusTable (pu4Error,
                                                       pSnmpIndexList,
                                                       pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qForwardUnregPortConfigTable (tSnmpIndex * pFirstMultiIndex,
                                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;
    INT4                i4FsDot1qTpPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qForwardUnregPortConfigTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qVlanIndex,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qForwardUnregPortConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qVlanIndex,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4FsDot1qTpPort;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qForwardUnregPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qForwardUnregPortConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qForwardUnregPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qForwardUnregPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qForwardUnregPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qForwardUnregPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qForwardUnregPort (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qForwardUnregPortConfigTableDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qForwardUnregPortConfigTable (pu4Error,
                                                           pSnmpIndexList,
                                                           pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qStaticUnicastTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qFdbId;
    tMacAddr           *pFsDot1qStaticUnicastAddress;
    INT4                i4FsDot1qStaticUnicastReceivePort;

    pFsDot1qStaticUnicastAddress = (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList;    /* not-accessible */

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qStaticUnicastTable (&i4FsDot1qVlanContextId,
                                                       &u4FsDot1qFdbId,
                                                       pFsDot1qStaticUnicastAddress,
                                                       &i4FsDot1qStaticUnicastReceivePort)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qStaticUnicastTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qFdbId,
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList, pFsDot1qStaticUnicastAddress,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &i4FsDot1qStaticUnicastReceivePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qFdbId;
    pNextMultiIndex->pIndex[3].i4_SLongValue =
        i4FsDot1qStaticUnicastReceivePort;
    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qStaticUnicastRowStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qStaticUnicastTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qStaticUnicastRowStatus (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             (*(tMacAddr *) pMultiIndex->
                                              pIndex[2].pOctetStrValue->
                                              pu1_OctetList),
                                             pMultiIndex->pIndex[3].
                                             i4_SLongValue,
                                             &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticUnicastStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qStaticUnicastTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qStaticUnicastStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[2].
                                           pOctetStrValue->pu1_OctetList),
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticUnicastRowStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qStaticUnicastRowStatus (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             (*(tMacAddr *) pMultiIndex->
                                              pIndex[2].pOctetStrValue->
                                              pu1_OctetList),
                                             pMultiIndex->pIndex[3].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticUnicastStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qStaticUnicastStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[2].
                                           pOctetStrValue->pu1_OctetList),
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticUnicastRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qStaticUnicastRowStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       (*(tMacAddr *)
                                                        pMultiIndex->pIndex[2].
                                                        pOctetStrValue->
                                                        pu1_OctetList),
                                                       pMultiIndex->pIndex[3].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticUnicastStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qStaticUnicastStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    (*(tMacAddr *) pMultiIndex->
                                                     pIndex[2].pOctetStrValue->
                                                     pu1_OctetList),
                                                    pMultiIndex->pIndex[3].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticUnicastTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qStaticUnicastTable (pu4Error, pSnmpIndexList,
                                                  pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qStaticAllowedToGoTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qFdbId;
    INT4                i4FsDot1qStaticUnicastReceivePort;
    INT4                i4FsDot1qTpPort;
    tMacAddr           *pFsDot1qStaticUnicastAddress;

    pFsDot1qStaticUnicastAddress = (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList;    /* not-accessible */

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qStaticAllowedToGoTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qFdbId,
             pFsDot1qStaticUnicastAddress, &i4FsDot1qStaticUnicastReceivePort,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qStaticAllowedToGoTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qFdbId,
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList, pFsDot1qStaticUnicastAddress,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &i4FsDot1qStaticUnicastReceivePort,
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qFdbId;
    pNextMultiIndex->pIndex[3].i4_SLongValue =
        i4FsDot1qStaticUnicastReceivePort;
    pNextMultiIndex->pIndex[4].i4_SLongValue = i4FsDot1qTpPort;
    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qStaticAllowedIsMemberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qStaticAllowedToGoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qStaticAllowedIsMember (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            (*(tMacAddr *) pMultiIndex->
                                             pIndex[2].pOctetStrValue->
                                             pu1_OctetList),
                                            pMultiIndex->pIndex[3].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[4].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticAllowedIsMemberSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qStaticAllowedIsMember (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            (*(tMacAddr *) pMultiIndex->
                                             pIndex[2].pOctetStrValue->
                                             pu1_OctetList),
                                            pMultiIndex->pIndex[3].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[4].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticAllowedIsMemberTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qStaticAllowedIsMember (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      (*(tMacAddr *)
                                                       pMultiIndex->pIndex[2].
                                                       pOctetStrValue->
                                                       pu1_OctetList),
                                                      pMultiIndex->pIndex[3].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[4].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticAllowedToGoTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qStaticAllowedToGoTable (pu4Error, pSnmpIndexList,
                                                      pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qStaticMulticastTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;
    INT4                i4FsDot1qStaticMulticastReceivePort;
    tMacAddr           *pFsDot1qStaticMulticastAddress;

    pFsDot1qStaticMulticastAddress = (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList;    /* not-accessible */

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qStaticMulticastTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qVlanIndex,
             pFsDot1qStaticMulticastAddress,
             &i4FsDot1qStaticMulticastReceivePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qStaticMulticastTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qVlanIndex,
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList, pFsDot1qStaticMulticastAddress,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &i4FsDot1qStaticMulticastReceivePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;
    pNextMultiIndex->pIndex[3].i4_SLongValue =
        i4FsDot1qStaticMulticastReceivePort;
    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qStaticMulticastRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qStaticMulticastTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qStaticMulticastRowStatus (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               (*(tMacAddr *) pMultiIndex->
                                                pIndex[2].pOctetStrValue->
                                                pu1_OctetList),
                                               pMultiIndex->pIndex[3].
                                               i4_SLongValue,
                                               &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticMulticastStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qStaticMulticastTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qStaticMulticastStatus (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            (*(tMacAddr *) pMultiIndex->
                                             pIndex[2].pOctetStrValue->
                                             pu1_OctetList),
                                            pMultiIndex->pIndex[3].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticMulticastRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qStaticMulticastRowStatus (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               (*(tMacAddr *) pMultiIndex->
                                                pIndex[2].pOctetStrValue->
                                                pu1_OctetList),
                                               pMultiIndex->pIndex[3].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticMulticastStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qStaticMulticastStatus (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            (*(tMacAddr *) pMultiIndex->
                                             pIndex[2].pOctetStrValue->
                                             pu1_OctetList),
                                            pMultiIndex->pIndex[3].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qStaticMulticastRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qStaticMulticastRowStatus (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         (*(tMacAddr *)
                                                          pMultiIndex->
                                                          pIndex[2].
                                                          pOctetStrValue->
                                                          pu1_OctetList),
                                                         pMultiIndex->pIndex[3].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qStaticMulticastStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qStaticMulticastStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      (*(tMacAddr *)
                                                       pMultiIndex->pIndex[2].
                                                       pOctetStrValue->
                                                       pu1_OctetList),
                                                      pMultiIndex->pIndex[3].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qStaticMulticastTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qStaticMulticastTable (pu4Error, pSnmpIndexList,
                                                    pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qStaticMcastPortTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;
    INT4                i4FsDot1qStaticMulticastReceivePort;
    INT4                i4FsDot1qTpPort;
    tMacAddr           *pFsDot1qStaticMulticastAddress;

    pFsDot1qStaticMulticastAddress = (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList;    /* not-accessible */

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qStaticMcastPortTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qVlanIndex,
             pFsDot1qStaticMulticastAddress,
             &i4FsDot1qStaticMulticastReceivePort,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qStaticMcastPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qVlanIndex,
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList, pFsDot1qStaticMulticastAddress,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &i4FsDot1qStaticMulticastReceivePort,
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;
    pNextMultiIndex->pIndex[3].i4_SLongValue =
        i4FsDot1qStaticMulticastReceivePort;
    pNextMultiIndex->pIndex[4].i4_SLongValue = i4FsDot1qTpPort;
    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qStaticMcastPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qStaticMcastPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qStaticMcastPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[2].
                                       pOctetStrValue->pu1_OctetList),
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticMcastPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qStaticMcastPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[2].
                                       pOctetStrValue->pu1_OctetList),
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qStaticMcastPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qStaticMcastPort (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                (*(tMacAddr *) pMultiIndex->
                                                 pIndex[2].pOctetStrValue->
                                                 pu1_OctetList),
                                                pMultiIndex->pIndex[3].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[4].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qStaticMcastPortTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qStaticMcastPortTable (pu4Error, pSnmpIndexList,
                                                    pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qVlanNumDeletesTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qVlanNumDeletesTable (&i4FsDot1qVlanContextId)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qVlanNumDeletesTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1qVlanContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qVlanNumDeletesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qVlanNumDeletesTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qVlanNumDeletes (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
GetNextIndexFsDot1qVlanCurrentTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanTimeMark;
    UINT4               u4FsDot1qVlanIndex;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qVlanCurrentTable (&i4FsDot1qVlanContextId,
                                                     &u4FsDot1qVlanTimeMark,
                                                     &u4FsDot1qVlanIndex) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qVlanCurrentTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qVlanTimeMark,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &u4FsDot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanTimeMark;
    pNextMultiIndex->pIndex[2].u4_ULongValue = u4FsDot1qVlanIndex;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qVlanFdbIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qVlanCurrentTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1qVlanFdbId (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qVlanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qVlanCurrentTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1qVlanStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qVlanCreationTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qVlanCurrentTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qVlanCreationTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qVlanEgressPortTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanTimeMark;
    UINT4               u4FsDot1qVlanIndex;
    INT4                i4FsDot1qTpPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qVlanEgressPortTable (&i4FsDot1qVlanContextId,
                                                        &u4FsDot1qVlanTimeMark,
                                                        &u4FsDot1qVlanIndex,
                                                        &i4FsDot1qTpPort) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qVlanEgressPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qVlanTimeMark,
             pFirstMultiIndex->pIndex[2].u4_ULongValue, &u4FsDot1qVlanIndex,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanTimeMark;
    pNextMultiIndex->pIndex[2].u4_ULongValue = u4FsDot1qVlanIndex;
    pNextMultiIndex->pIndex[3].i4_SLongValue = i4FsDot1qTpPort;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qVlanCurrentEgressPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qVlanEgressPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qVlanCurrentEgressPort (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[3].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qVlanStaticTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qVlanStaticTable (&i4FsDot1qVlanContextId,
                                                    &u4FsDot1qVlanIndex) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qVlanStaticTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4FsDot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qVlanStaticNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qVlanStaticTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qVlanStaticName (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qVlanStaticRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qVlanStaticTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qVlanStaticRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qVlanStaticNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qVlanStaticName (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qVlanStaticRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qVlanStaticRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qVlanStaticNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qVlanStaticName (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qVlanStaticRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qVlanStaticRowStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qVlanStaticTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qVlanStaticTable (pu4Error, pSnmpIndexList,
                                               pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qVlanStaticPortConfigTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qVlanIndex;
    INT4                i4FsDot1qTpPort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qVlanStaticPortConfigTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qVlanIndex,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qVlanStaticPortConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4FsDot1qVlanIndex,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4FsDot1qTpPort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4FsDot1qTpPort;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qVlanStaticPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qVlanStaticPortConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qVlanStaticPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qVlanStaticPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qVlanStaticPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qVlanStaticPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qVlanStaticPort (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qVlanStaticPortConfigTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qVlanStaticPortConfigTable (pu4Error,
                                                         pSnmpIndexList,
                                                         pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qNextFreeLocalVlanIndexTable (tSnmpIndex * pFirstMultiIndex,
                                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qNextFreeLocalVlanIndexTable
            (&i4FsDot1qVlanContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qNextFreeLocalVlanIndexTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1qVlanContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qNextFreeLocalVlanIndexGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qNextFreeLocalVlanIndexTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qNextFreeLocalVlanIndex (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
GetNextIndexFsDot1qPortVlanTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qPortVlanTable (&i4FsDot1dBasePort) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qPortVlanTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dBasePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qPvidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsDot1qPvid (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qPortAcceptableFrameTypesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qPortAcceptableFrameTypes (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qPortIngressFilteringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qPortIngressFiltering (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qPortGvrpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    GARP_LOCK ();

    i4RetVal =
        nmhGetFsDot1qPortGvrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1qPortGvrpFailedRegistrationsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    GARP_LOCK ();

    i4RetVal =
        nmhGetFsDot1qPortGvrpFailedRegistrations (pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  &(pMultiData->u4_ULongValue));

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1qPortGvrpLastPduOriginGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    VLAN_UNLOCK ();

    GARP_LOCK ();

    i4RetVal =
        nmhGetFsDot1qPortGvrpLastPduOrigin (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            (tMacAddr *) pMultiData->
                                            pOctetStrValue->pu1_OctetList);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1qPortRestrictedVlanRegistrationGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    GARP_LOCK ();

    i4RetVal =
        nmhGetFsDot1qPortRestrictedVlanRegistration (pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     &(pMultiData->
                                                       i4_SLongValue));

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1qPvidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetFsDot1qPvid (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qPortAcceptableFrameTypesSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qPortAcceptableFrameTypes (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qPortIngressFilteringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qPortIngressFiltering (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qPortGvrpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal =
        nmhSetFsDot1qPortGvrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1qPortRestrictedVlanRegistrationSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal =
        nmhSetFsDot1qPortRestrictedVlanRegistration (pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1qPvidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qPvid (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qPortAcceptableFrameTypesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qPortAcceptableFrameTypes (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qPortIngressFilteringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qPortIngressFiltering (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qPortGvrpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2FsDot1qPortGvrpStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1qPortRestrictedVlanRegistrationTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2FsDot1qPortRestrictedVlanRegistration (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue);

    GARP_UNLOCK ();
    return i4RetVal;
}

INT4
FsDot1qPortVlanTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qPortVlanTable (pu4Error, pSnmpIndexList,
                                             pSnmpvarbinds);

    VLAN_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsDot1qPortVlanStatisticsTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;
    UINT4               u4FsDot1qVlanIndex;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qPortVlanStatisticsTable (&i4FsDot1dBasePort,
                                                            &u4FsDot1qVlanIndex)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qPortVlanStatisticsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dBasePort,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4FsDot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qTpVlanPortInFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qTpVlanPortInFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qTpVlanPortOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qTpVlanPortOutFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qTpVlanPortInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qTpVlanPortInDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qTpVlanPortInOverflowFramesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qTpVlanPortInOverflowFrames (pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qTpVlanPortOutOverflowFramesGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qTpVlanPortOutOverflowFrames (pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  &(pMultiData->u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qTpVlanPortInOverflowDiscardsGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qTpVlanPortInOverflowDiscards (pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   &(pMultiData->
                                                     u4_ULongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qPortVlanHCStatisticsTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1dBasePort;
    UINT4               u4FsDot1qVlanIndex;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qPortVlanHCStatisticsTable
            (&i4FsDot1dBasePort, &u4FsDot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qPortVlanHCStatisticsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1dBasePort,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4FsDot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1dBasePort;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qVlanIndex;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qTpVlanPortHCInFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanHCStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qTpVlanPortHCInFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           &(pMultiData->u8_Counter64Value));

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
FsDot1qTpVlanPortHCOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanHCStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qTpVlanPortHCOutFrames (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            &(pMultiData->u8_Counter64Value));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qTpVlanPortHCInDiscardsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qPortVlanHCStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qTpVlanPortHCInDiscards (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             &(pMultiData->u8_Counter64Value));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qLearningConstraintsTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    UINT4               u4FsDot1qConstraintVlan;
    INT4                i4FsDot1qConstraintSet;

    VLAN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qLearningConstraintsTable
            (&i4FsDot1qVlanContextId, &u4FsDot1qConstraintVlan,
             &i4FsDot1qConstraintSet) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qLearningConstraintsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4FsDot1qConstraintVlan,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4FsDot1qConstraintSet) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4FsDot1qConstraintVlan;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4FsDot1qConstraintSet;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qConstraintTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qLearningConstraintsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qConstraintType (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qConstraintStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qLearningConstraintsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qConstraintStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qConstraintTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qConstraintType (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qConstraintStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qConstraintStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qConstraintTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qConstraintType (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qConstraintStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qConstraintStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qLearningConstraintsTableDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qLearningConstraintsTable (pu4Error,
                                                        pSnmpIndexList,
                                                        pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1qConstraintDefaultTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1qConstraintDefaultTable
            (&i4FsDot1qVlanContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1qConstraintDefaultTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1qVlanContextId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1qConstraintSetDefaultGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qConstraintDefaultTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qConstraintSetDefault (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qConstraintTypeDefaultGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1qConstraintDefaultTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1qConstraintTypeDefault (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qConstraintSetDefaultSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qConstraintSetDefault (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qConstraintTypeDefaultSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1qConstraintTypeDefault (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qConstraintSetDefaultTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qConstraintSetDefault (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qConstraintTypeDefaultTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1qConstraintTypeDefault (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1qConstraintDefaultTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1qConstraintDefaultTable (pu4Error, pSnmpIndexList,
                                                      pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1vProtocolGroupTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1qVlanContextId;
    INT4                i4FsDot1vProtocolTemplateFrameType;
    tSNMP_OCTET_STRING_TYPE *pFsDot1vProtocolTemplateProtocolValue;

    VLAN_LOCK ();

    pFsDot1vProtocolTemplateProtocolValue = pNextMultiIndex->pIndex[2].pOctetStrValue;    /* not-accessible */

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1vProtocolGroupTable (&i4FsDot1qVlanContextId,
                                                       &i4FsDot1vProtocolTemplateFrameType,
                                                       pFsDot1vProtocolTemplateProtocolValue)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1vProtocolGroupTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4FsDot1qVlanContextId,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4FsDot1vProtocolTemplateFrameType,
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pFsDot1vProtocolTemplateProtocolValue) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1qVlanContextId;
    pNextMultiIndex->pIndex[1].i4_SLongValue =
        i4FsDot1vProtocolTemplateFrameType;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1vProtocolGroupIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1vProtocolGroupTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1vProtocolGroupId (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolGroupRowStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1vProtocolGroupTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1vProtocolGroupRowStatus (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[2].
                                             pOctetStrValue,
                                             &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolGroupIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1vProtocolGroupId (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolGroupRowStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1vProtocolGroupRowStatus (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[2].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolGroupIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1vProtocolGroupId (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[2].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolGroupRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1vProtocolGroupRowStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[1].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[2].
                                                       pOctetStrValue,
                                                       pMultiData->
                                                       i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolGroupTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1vProtocolGroupTable (pu4Error, pSnmpIndexList,
                                                  pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexFsDot1vProtocolPortTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsDot1vPort;
    INT4                i4FsDot1vProtocolPortGroupId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot1vProtocolPortTable (&i4FsDot1vPort,
                                                      &i4FsDot1vProtocolPortGroupId)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot1vProtocolPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsDot1vPort,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4FsDot1vProtocolPortGroupId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsDot1vPort;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4FsDot1vProtocolPortGroupId;

    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

INT4
FsDot1vProtocolPortGroupVidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1vProtocolPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1vProtocolPortGroupVid (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolPortRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceFsDot1vProtocolPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsDot1vProtocolPortRowStatus (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolPortGroupVidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1vProtocolPortGroupVid (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolPortRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetFsDot1vProtocolPortRowStatus (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolPortGroupVidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1vProtocolPortGroupVid (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolPortRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2FsDot1vProtocolPortRowStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
FsDot1vProtocolPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2FsDot1vProtocolPortTable (pu4Error, pSnmpIndexList,
                                                 pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}
