
/* $Id: stdvlawr.c,v 1.12 2011/10/25 10:39:08 siva Exp $*/
#include "lr.h"
#include "vlaninc.h"
#include "fssnmp.h"
#include "stdvlawr.h"
#include "stdvladb.h"

VOID
RegisterSTDVLA ()
{
    SNMPRegisterMib (&stdvlaOID, &stdvlaEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&stdvlaOID, (const UINT1 *) "qBridgeMIB");
}

INT4
Dot1qVlanVersionNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetDot1qVlanVersionNumber (&pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qMaxVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetDot1qMaxVlanId (&pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qMaxSupportedVlansGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetDot1qMaxSupportedVlans (&pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qNumVlansGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetDot1qNumVlans (&pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qGvrpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhTestv2Dot1qGvrpStatus (pu4Error, pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qGvrpStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhDepv2Dot1qGvrpStatus (pu4Error, pSnmpIndexList,
                                        pSnmpvarbinds);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qGvrpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhSetDot1qGvrpStatus (pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qGvrpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetDot1qGvrpStatus (&pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qFdbDynamicCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qFdbDynamicCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qTpFdbPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceDot1qTpFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpFdbPort (pMultiIndex->pIndex[0].u4_ULongValue,
                                     *(tMacAddr *) pMultiIndex->pIndex[1].
                                     pOctetStrValue->pu1_OctetList,
                                     &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qTpFdbStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceDot1qTpFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpFdbStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       *(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList,
                                       &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qTpGroupEgressPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceDot1qTpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpGroupEgressPorts
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
         pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qTpGroupLearntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceDot1qTpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpGroupLearnt
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qForwardAllPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qForwardAllTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qForwardAllPorts (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardAllStaticPortsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qForwardAllStaticPorts (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardAllStaticPortsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qForwardAllStaticPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardAllStaticPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qForwardAllTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qForwardAllStaticPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardAllForbiddenPortsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qForwardAllForbiddenPorts (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardAllTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qForwardAllTable (pu4Error, pSnmpIndexList,
                                             pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardAllForbiddenPortsSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qForwardAllForbiddenPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardAllForbiddenPortsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qForwardAllTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qForwardAllForbiddenPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardUnregisteredPortsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qForwardUnregisteredTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qForwardUnregisteredPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardUnregisteredStaticPortsTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qForwardUnregisteredStaticPorts (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardUnregisteredStaticPortsSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qForwardUnregisteredStaticPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardUnregisteredStaticPortsGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qForwardUnregisteredTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qForwardUnregisteredStaticPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardUnregisteredForbiddenPortsTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qForwardUnregisteredForbiddenPorts (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardUnregisteredTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qForwardUnregisteredTable (pu4Error,
                                                      pSnmpIndexList,
                                                      pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardUnregisteredForbiddenPortsSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qForwardUnregisteredForbiddenPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qForwardUnregisteredForbiddenPortsGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qForwardUnregisteredTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qForwardUnregisteredForbiddenPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qStaticUnicastAllowedToGoToTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhTestv2Dot1qStaticUnicastAllowedToGoTo (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         *(tMacAddr *)
                                                         pMultiIndex->pIndex[1].
                                                         pOctetStrValue->
                                                         pu1_OctetList,
                                                         pMultiIndex->pIndex[2].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qStaticUnicastAllowedToGoToSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhSetDot1qStaticUnicastAllowedToGoTo
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList, pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qStaticUnicastAllowedToGoToGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceDot1qStaticUnicastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qStaticUnicastAllowedToGoTo
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList, pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qStaticUnicastStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhTestv2Dot1qStaticUnicastStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  *(tMacAddr *) pMultiIndex->
                                                  pIndex[1].pOctetStrValue->
                                                  pu1_OctetList,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qStaticUnicastTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qStaticUnicastTable (pu4Error, pSnmpIndexList,
                                                pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qStaticUnicastStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhSetDot1qStaticUnicastStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList, pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qStaticUnicastStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceDot1qStaticUnicastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qStaticUnicastStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList, pMultiIndex->pIndex[2].i4_SLongValue,
         &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qStaticMulticastStaticEgressPortsTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhTestv2Dot1qStaticMulticastStaticEgressPorts (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               u4_ULongValue,
                                                               *(tMacAddr *)
                                                               pMultiIndex->
                                                               pIndex[1].
                                                               pOctetStrValue->
                                                               pu1_OctetList,
                                                               pMultiIndex->
                                                               pIndex[2].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qStaticMulticastStaticEgressPortsSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhSetDot1qStaticMulticastStaticEgressPorts
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList, pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qStaticMulticastStaticEgressPortsGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceDot1qStaticMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qStaticMulticastStaticEgressPorts
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList, pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qStaticMulticastForbiddenEgressPortsTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhTestv2Dot1qStaticMulticastForbiddenEgressPorts (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  u4_ULongValue,
                                                                  *(tMacAddr *)
                                                                  pMultiIndex->
                                                                  pIndex[1].
                                                                  pOctetStrValue->
                                                                  pu1_OctetList,
                                                                  pMultiIndex->
                                                                  pIndex[2].
                                                                  i4_SLongValue,
                                                                  pMultiData->
                                                                  pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qStaticMulticastForbiddenEgressPortsSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qStaticMulticastForbiddenEgressPorts
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList, pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qStaticMulticastForbiddenEgressPortsGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceDot1qStaticMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qStaticMulticastForbiddenEgressPorts
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList, pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qStaticMulticastStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhTestv2Dot1qStaticMulticastStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    *(tMacAddr *) pMultiIndex->
                                                    pIndex[1].pOctetStrValue->
                                                    pu1_OctetList,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qStaticMulticastTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qStaticMulticastTable (pu4Error, pSnmpIndexList,
                                                  pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qStaticMulticastStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhSetDot1qStaticMulticastStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList, pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qStaticMulticastStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceDot1qStaticMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qStaticMulticastStatus
        (pMultiIndex->pIndex[0].u4_ULongValue,
         *(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
         pu1_OctetList, pMultiIndex->pIndex[2].i4_SLongValue,
         &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qVlanNumDeletesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetDot1qVlanNumDeletes (&pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanFdbIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qVlanFdbId (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanCurrentEgressPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qVlanCurrentEgressPorts
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanCurrentUntaggedPortsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qVlanCurrentUntaggedPorts
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qVlanStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanCreationTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qVlanCreationTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qVlanStaticName (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qVlanStaticName (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qVlanStaticName (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticEgressPortsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qVlanStaticEgressPorts (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticEgressPortsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qVlanStaticEgressPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticEgressPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qVlanStaticEgressPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanForbiddenEgressPortsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qVlanForbiddenEgressPorts (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanForbiddenEgressPortsSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qVlanForbiddenEgressPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanForbiddenEgressPortsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qVlanForbiddenEgressPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticUntaggedPortsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qVlanStaticUntaggedPorts (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticUntaggedPortsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qVlanStaticUntaggedPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticUntaggedPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qVlanStaticUntaggedPorts
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qVlanStaticRowStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qVlanStaticTable (pu4Error, pSnmpIndexList,
                                             pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qVlanStaticRowStatus
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qVlanStaticRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qVlanStaticRowStatus
        (pMultiIndex->pIndex[0].u4_ULongValue, &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qNextFreeLocalVlanIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetDot1qNextFreeLocalVlanIndex (&pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPvidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qPvid (pu4Error,
                                   pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPvidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qPvid (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPvidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qPvid (pMultiIndex->pIndex[0].i4_SLongValue,
                                &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortAcceptableFrameTypesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qPortAcceptableFrameTypes (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortAcceptableFrameTypesSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qPortAcceptableFrameTypes
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortAcceptableFrameTypesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qPortAcceptableFrameTypes
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortIngressFilteringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qPortIngressFiltering (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortIngressFilteringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qPortIngressFiltering
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortIngressFilteringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qPortIngressFiltering
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortGvrpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{

    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2Dot1qPortGvrpStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortGvrpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhSetDot1qPortGvrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortGvrpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

    GARP_LOCK ();

    i4RetVal = nmhGetDot1qPortGvrpStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortGvrpFailedRegistrationsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

    GARP_LOCK ();

    i4RetVal = nmhGetDot1qPortGvrpFailedRegistrations
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue);

    GARP_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortGvrpLastPduOriginGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    pMultiData->pOctetStrValue->i4_Length = 6;
    if (nmhValidateIndexInstanceDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

    GARP_LOCK ();

    i4RetVal = nmhGetDot1qPortGvrpLastPduOrigin
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList);

    GARP_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qPortRestrictedVlanRegistrationTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhTestv2Dot1qPortRestrictedVlanRegistration (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             i4_SLongValue,
                                                             pMultiData->
                                                             i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qPortVlanTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qPortVlanTable (pu4Error, pSnmpIndexList,
                                           pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qPortRestrictedVlanRegistrationSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    GARP_LOCK ();

    i4RetVal = nmhSetDot1qPortRestrictedVlanRegistration
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    GARP_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qPortRestrictedVlanRegistrationGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }

    VLAN_UNLOCK ();

    GARP_LOCK ();

    i4RetVal = nmhGetDot1qPortRestrictedVlanRegistration
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    GARP_UNLOCK ();

    return i4RetVal;

}

INT4
Dot1qTpVlanPortInFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qTpVlanPortInFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qTpVlanPortOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpVlanPortOutFrames
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qTpVlanPortInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpVlanPortInDiscards
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qTpVlanPortInOverflowFramesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpVlanPortInOverflowFrames
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qTpVlanPortOutOverflowFramesGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpVlanPortOutOverflowFrames
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qTpVlanPortInOverflowDiscardsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpVlanPortInOverflowDiscards
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->u4_ULongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qTpVlanPortHCInFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanHCStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpVlanPortHCInFrames
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->u8_Counter64Value);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qTpVlanPortHCOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanHCStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpVlanPortHCOutFrames
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->u8_Counter64Value);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qTpVlanPortHCInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qPortVlanHCStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qTpVlanPortHCInDiscards
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->u8_Counter64Value);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qConstraintType (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1qConstraintType (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qLearningConstraintsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1qConstraintType (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1qConstraintStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qLearningConstraintsTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qLearningConstraintsTable (pu4Error,
                                                      pSnmpIndexList,
                                                      pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal =
        nmhSetDot1qConstraintStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1qLearningConstraintsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetDot1qConstraintStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintSetDefaultTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhTestv2Dot1qConstraintSetDefault
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintSetDefaultSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhSetDot1qConstraintSetDefault (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintSetDefaultGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetDot1qConstraintSetDefault (&pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintTypeDefaultTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhTestv2Dot1qConstraintTypeDefault
        (pu4Error, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintSetDefaultDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qConstraintSetDefault (pu4Error, pSnmpIndexList,
                                                  pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintTypeDefaultDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1qConstraintTypeDefault (pu4Error, pSnmpIndexList,
                                                   pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintTypeDefaultSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhSetDot1qConstraintTypeDefault (pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1qConstraintTypeDefaultGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetDot1qConstraintTypeDefault (&pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexDot1qLearningConstraintsTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4dot1qConstraintVlan;
    INT4                i4dot1qConstraintSet;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qLearningConstraintsTable
            (&u4dot1qConstraintVlan, &i4dot1qConstraintSet) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qLearningConstraintsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4dot1qConstraintVlan,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4dot1qConstraintSet) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4dot1qConstraintVlan;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4dot1qConstraintSet;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qPortVlanHCStatisticsTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dBasePort;
    UINT4               u4dot1qVlanIndex;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qPortVlanHCStatisticsTable (&i4dot1dBasePort,
                                                            &u4dot1qVlanIndex)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qPortVlanHCStatisticsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4dot1dBasePort,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4dot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dBasePort;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4dot1qVlanIndex;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qPortVlanStatisticsTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dBasePort;
    UINT4               u4dot1qVlanIndex;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qPortVlanStatisticsTable (&i4dot1dBasePort,
                                                          &u4dot1qVlanIndex)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qPortVlanStatisticsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4dot1dBasePort,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4dot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dBasePort;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4dot1qVlanIndex;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qPortVlanTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dBasePort;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qPortVlanTable (&i4dot1dBasePort)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qPortVlanTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4dot1dBasePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dBasePort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qVlanStaticTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4dot1qVlanIndex;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qVlanStaticTable (&u4dot1qVlanIndex)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qVlanStaticTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &u4dot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4dot1qVlanIndex;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qVlanCurrentTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4dot1qVlanTimeMark;
    UINT4               u4dot1qVlanIndex;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qVlanCurrentTable (&u4dot1qVlanTimeMark,
                                                   &u4dot1qVlanIndex)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qVlanCurrentTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4dot1qVlanTimeMark,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4dot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4dot1qVlanTimeMark;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4dot1qVlanIndex;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qStaticMulticastTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4dot1qVlanIndex;
    tMacAddr           *p1dot1qStaticMulticastAddress;
    INT4                i4dot1qStaticMulticastReceivePort;
    p1dot1qStaticMulticastAddress = (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList;    /* not-accessible */

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qStaticMulticastTable (&u4dot1qVlanIndex,
                                                       p1dot1qStaticMulticastAddress,
                                                       &i4dot1qStaticMulticastReceivePort)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        pFirstMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
        if (nmhGetNextIndexDot1qStaticMulticastTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4dot1qVlanIndex,
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList, p1dot1qStaticMulticastAddress,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4dot1qStaticMulticastReceivePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }

    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4dot1qVlanIndex;
    pNextMultiIndex->pIndex[2].i4_SLongValue =
        i4dot1qStaticMulticastReceivePort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qStaticUnicastTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4dot1qFdbId;
    tMacAddr           *p1dot1qStaticUnicastAddress;
    INT4                i4dot1qStaticUnicastReceivePort;
    p1dot1qStaticUnicastAddress = (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList;    /* not-accessible */

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qStaticUnicastTable (&u4dot1qFdbId,
                                                     p1dot1qStaticUnicastAddress,
                                                     &i4dot1qStaticUnicastReceivePort)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        pFirstMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
        if (nmhGetNextIndexDot1qStaticUnicastTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4dot1qFdbId,
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList, p1dot1qStaticUnicastAddress,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &i4dot1qStaticUnicastReceivePort) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }

    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4dot1qFdbId;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4dot1qStaticUnicastReceivePort;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qForwardUnregisteredTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4dot1qVlanIndex;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qForwardUnregisteredTable (&u4dot1qVlanIndex)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qForwardUnregisteredTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &u4dot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4dot1qVlanIndex;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qForwardAllTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4dot1qVlanIndex;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qForwardAllTable (&u4dot1qVlanIndex)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qForwardAllTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &u4dot1qVlanIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4dot1qVlanIndex;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qTpGroupTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4dot1qVlanIndex;
    tMacAddr           *p1dot1qTpGroupAddress;
    p1dot1qTpGroupAddress = (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList;    /* not-accessible */

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qTpGroupTable (&u4dot1qVlanIndex,
                                               p1dot1qTpGroupAddress)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        pFirstMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
        if (nmhGetNextIndexDot1qTpGroupTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4dot1qVlanIndex,
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList, p1dot1qTpGroupAddress) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }

    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4dot1qVlanIndex;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qTpFdbTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4dot1qFdbId;
    tMacAddr           *p1dot1qTpFdbAddress;
    p1dot1qTpFdbAddress = (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList;    /* not-accessible */

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qTpFdbTable (&u4dot1qFdbId,
                                             p1dot1qTpFdbAddress)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qTpFdbTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4dot1qFdbId,
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList, p1dot1qTpFdbAddress) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }

    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4dot1qFdbId;

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;

    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1qFdbTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4dot1qFdbId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1qFdbTable (&u4dot1qFdbId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1qFdbTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &u4dot1qFdbId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4dot1qFdbId;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1vProtocolGroupIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1vProtocolGroupId (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolGroupIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1vProtocolGroupId (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolGroupIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1vProtocolGroupTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1vProtocolGroupId (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolGroupRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1vProtocolGroupRowStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     pOctetStrValue,
                                                     pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolGroupTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1vProtocolGroupTable (pu4Error, pSnmpIndexList,
                                                pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolGroupRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1vProtocolGroupRowStatus
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolGroupRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1vProtocolGroupTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1vProtocolGroupRowStatus
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue, &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolPortGroupVidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1vProtocolPortGroupVid (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolPortGroupVidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1vProtocolPortGroupVid
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolPortGroupVidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1vProtocolPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1vProtocolPortGroupVid
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolPortRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhTestv2Dot1vProtocolPortRowStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhDepv2Dot1vProtocolPortTable (pu4Error, pSnmpIndexList,
                                               pSnmpvarbinds);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolPortRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VLAN_LOCK ();

    i4RetVal = nmhSetDot1vProtocolPortRowStatus
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
Dot1vProtocolPortRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;

    VLAN_LOCK ();

    if (nmhValidateIndexInstanceDot1vProtocolPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1vProtocolPortRowStatus
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->i4_SLongValue);

    VLAN_UNLOCK ();

    return i4RetVal;
}

INT4
GetNextIndexDot1vProtocolPortTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1dBasePort;
    INT4                i4dot1vProtocolPortGroupId;

    VLAN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1vProtocolPortTable (&i4dot1dBasePort,
                                                    &i4dot1vProtocolPortGroupId)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1vProtocolPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4dot1dBasePort,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4dot1vProtocolPortGroupId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1dBasePort;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4dot1vProtocolPortGroupId;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexDot1vProtocolGroupTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    INT4                i4dot1vProtocolTemplateFrameType;
    tSNMP_OCTET_STRING_TYPE *p1dot1vProtocolTemplateProtocolValue;

    VLAN_LOCK ();

    p1dot1vProtocolTemplateProtocolValue = pNextMultiIndex->pIndex[1].pOctetStrValue;    /* not-accessible */
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1vProtocolGroupTable
            (&i4dot1vProtocolTemplateFrameType,
             p1dot1vProtocolTemplateProtocolValue) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1vProtocolGroupTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4dot1vProtocolTemplateFrameType,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             p1dot1vProtocolTemplateProtocolValue) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4dot1vProtocolTemplateFrameType;
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}
