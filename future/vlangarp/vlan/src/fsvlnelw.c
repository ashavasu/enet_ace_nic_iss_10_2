/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvlnelw.c,v 1.55 2016/02/18 07:58:09 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "vlaninc.h"
# include  "fsmvlecli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVlanBridgeMode
 Input       :  The Indices

                The Object 
                retValFsVlanBridgeMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanBridgeMode (INT4 *pi4RetValFsVlanBridgeMode)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        /* VLAN module is shutdown...return default bridge mode */
        *pi4RetValFsVlanBridgeMode = (INT4) VLAN_INVALID_BRIDGE_MODE;
        return SNMP_SUCCESS;
    }

    *pi4RetValFsVlanBridgeMode = (INT4) VLAN_BRIDGE_MODE ();
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVlanBridgeMode
 Input       :  The Indices

                The Object 
                setValFsVlanBridgeMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanBridgeMode (INT4 i4SetValFsVlanBridgeMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4SetValFsVlanBridgeMode == (INT4) VLAN_BRIDGE_MODE ())
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    VlanSetOrSyncBrgModeFlag (VLAN_CURR_CONTEXT_ID (), VLAN_TRUE);
#endif

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (VLAN_CURR_CONTEXT_ID ()) ==
        VLAN_SNMP_FALSE)
    {
        /*VLAN Module was not shutdown, So Shutting down */
        /*Shutting down the VLAN module */
#ifdef NPAPI_WANTED
        if ((i4RetVal = VlanFsMiVlanHwVlanDisable (VLAN_CURR_CONTEXT_ID ())) ==
            FNP_FAILURE)
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanBridgeMode, u4SeqNum,
                                  FALSE, VlanLock, VlanUnLock, 1, SNMP_FAILURE);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                              i4SetValFsVlanBridgeMode));
            VlanSelectContext (u4CurrContextId);
            return SNMP_FAILURE;
        }

        if (i4RetVal == FNP_VLAN_DISABLED_BUT_NOT_DELETED)
        {
            VlanHwDelStaticInfo ();
        }
#endif

        VlanDeInit ();

        VlanL2IwfDeleteAllVlans (VLAN_CURR_CONTEXT_ID ());
        VlanNotifyProtocolShutdownStatus (VLAN_CURR_CONTEXT_ID ());

    }

    /*Updating the L2IWF with the configured Bridge Mode */

    VlanL2IwfSetBridgeMode (VLAN_CURR_CONTEXT_ID (),
                            (UINT4) i4SetValFsVlanBridgeMode);

    /* Whenever the Bridge mode is configured, Even thought the VLAN 
     * Module was shutdown, it will be started without the knowledge 
     * of the Administrator.*/

    /* Starting the VLAN Module */

    if (VlanInit () == VLAN_FAILURE)
    {
#ifdef NPAPI_WANTED
        VlanSetOrSyncBrgModeFlag (VLAN_CURR_CONTEXT_ID (), VLAN_FALSE);
#endif
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        /* Set Notify is added, before notify configuration */
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanBridgeMode, u4SeqNum,
                              FALSE, VlanLock, VlanUnLock, 1, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                          i4SetValFsVlanBridgeMode));
        VlanSelectContext (u4CurrContextId);
        return SNMP_FAILURE;
    }

    /* Triggering the Bridge Mode Change to L2IWF */
    if (VlanL2IwfChangeBridgeModeChange (VLAN_CURR_CONTEXT_ID ())
        == VLAN_FAILURE)
    {
#ifdef NPAPI_WANTED
        VlanSetOrSyncBrgModeFlag (VLAN_CURR_CONTEXT_ID (), VLAN_FALSE);
#endif
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
        /* Set Notify is added, before notify configuration */
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanBridgeMode, u4SeqNum,
                              FALSE, VlanLock, VlanUnLock, 1, SNMP_FAILURE);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                          i4SetValFsVlanBridgeMode));
        VlanSelectContext (u4CurrContextId);

        return SNMP_FAILURE;
    }
    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanBridgeMode, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValFsVlanBridgeMode));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVlanBridgeMode
 Input       :  The Indices

                The Object 
                testValFsVlanBridgeMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanBridgeMode (UINT4 *pu4ErrorCode, INT4 i4TestValFsVlanBridgeMode)
{
    UINT4               u4Retval = 0;
    if (VlanPbValidateBridgeMode (i4TestValFsVlanBridgeMode,
                                  pu4ErrorCode) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () == (UINT4) i4TestValFsVlanBridgeMode)
    {
        return SNMP_SUCCESS;
    }

    u4Retval = (UINT4) VlanL2IwfIsBrgModeChangeAllowed (VLAN_CURR_CONTEXT_ID (),
                                                        (UINT4)
                                                        i4TestValFsVlanBridgeMode);
    if (u4Retval == L2IWF_FALSE)
    {
        CLI_SET_ERR (CLI_PB_BRIDGE_MODE_DEP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else if (u4Retval == L2IWF_PBB_FALSE)
    {

        CLI_SET_ERR (CLI_PB_BRIDGE_MODE_SHUT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVlanBridgeMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanBridgeMode (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelBpduPri
 Input       :  The Indices

                The Object 
                retValFsVlanTunnelBpduPri
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelBpduPri (INT4 *pi4RetValFsVlanTunnelBpduPri)
{
    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        *pi4RetValFsVlanTunnelBpduPri = (INT4) VLAN_TUNNEL_BPDU_PRIORITY ();
        return SNMP_SUCCESS;

    }

    *pi4RetValFsVlanTunnelBpduPri = (INT4) VLAN_DEF_TUNNEL_BPDU_PRIORITY;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelStpAddress
 Input       :  The Indices

                The Object 
                retValFsVlanTunnelStpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelStpAddress (tMacAddr * pRetValFsVlanTunnelStpAddress)
{

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelStpAddress, VLAN_TUNNEL_STP_ADDR (),
                     VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelStpAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelLacpAddress
 Input       :  The Indices

                The Object 
                retValFsVlanTunnelLacpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelLacpAddress (tMacAddr * pRetValFsVlanTunnelLacpAddress)
{

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelLacpAddress,
                     VLAN_TUNNEL_LACP_ADDR (), VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelLacpAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelDot1xAddress
 Input       :  The Indices

                The Object 
                retValFsVlanTunnelDot1xAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelDot1xAddress (tMacAddr * pRetValFsVlanTunnelDot1xAddress)
{

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelDot1xAddress,
                     VLAN_TUNNEL_DOT1X_ADDR (), VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelDot1xAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelGvrpAddress
 Input       :  The Indices

                The Object 
                retValFsVlanTunnelGvrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelGvrpAddress (tMacAddr * pRetValFsVlanTunnelGvrpAddress)
{

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelGvrpAddress,
                     VLAN_TUNNEL_GVRP_ADDR (), VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelGvrpAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelMvrpAddress
 Input       :  The Indices

                The Object
                retValFsVlanTunnelMvrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelMvrpAddress (tMacAddr * pRetValFsVlanTunnelMvrpAddress)
{

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelMvrpAddress,
                     VLAN_TUNNEL_MVRP_ADDR (), VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelMvrpAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelGmrpAddress
 Input       :  The Indices

                The Object 
                retValFsVlanTunnelGmrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelGmrpAddress (tMacAddr * pRetValFsVlanTunnelGmrpAddress)
{

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelGmrpAddress,
                     VLAN_TUNNEL_GMRP_ADDR (), VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelGmrpAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelGmrpAddress
 Input       :  The Indices

                The Object
                retValFsVlanTunnelGmrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelMmrpAddress (tMacAddr * pRetValFsVlanTunnelMmrpAddress)
{

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelMmrpAddress,
                     VLAN_TUNNEL_MMRP_ADDR (), VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelMmrpAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelElmiAddress
 Input       :  The Indices

                The Object 
                retValFsVlanTunnelElmiAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelElmiAddress (tMacAddr * pRetValFsVlanTunnelElmiAddress)
{
    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelElmiAddress,
                     VLAN_TUNNEL_ELMI_ADDR (), VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelElmiAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelLldpAddress
 Input       :  The Indices

                The Object 
                retValFsVlanTunnelLldpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelLldpAddress (tMacAddr * pRetValFsVlanTunnelLldpAddress)
{
    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelLldpAddress,
                     VLAN_TUNNEL_LLDP_ADDR (), VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelLldpAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelEcfmAddress
 Input       :  The Indices

                The Object
                retValFsVlanTunnelEcfmAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelEcfmAddress (tMacAddr * pRetValFsVlanTunnelEcfmAddress)
{
    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelEcfmAddress,
                     VLAN_TUNNEL_ECFM_ADDR (), VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelEcfmAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelEoamAddress
 Input       :  The Indices

                The Object
                retValFsVlanTunnelEoamAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelEoamAddress (tMacAddr * pRetValFsVlanTunnelEoamAddress)
{
    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelEoamAddress,
                     VLAN_TUNNEL_EOAM_ADDR (), VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelEoamAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelIgmpAddress
 Input       :  The Indices

                The Object
                retValFsVlanTunnelIgmpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsVlanTunnelIgmpAddress(tMacAddr * pRetValFsVlanTunnelIgmpAddress)
{
    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
    {
        VLAN_MEMCPY (pRetValFsVlanTunnelIgmpAddress,
                     VLAN_TUNNEL_IGMP_ADDR (), VLAN_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }

    VLAN_MEMSET (pRetValFsVlanTunnelIgmpAddress, 0, VLAN_MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelBpduPri
 Input       :  The Indices

                The Object 
                setValFsVlanTunnelBpduPri
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelBpduPri (INT4 i4SetValFsVlanTunnelBpduPri)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4SetValFsVlanTunnelBpduPri == VLAN_TUNNEL_BPDU_PRIORITY ())
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_TUNNEL_BPDU_PRIORITY () = (UINT1) i4SetValFsVlanTunnelBpduPri;

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelBpduPri, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_CURR_CONTEXT_ID (),
                      i4SetValFsVlanTunnelBpduPri));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelStpAddress
 Input       :  The Indices

                The Object 
                setValFsVlanTunnelStpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelStpAddress (tMacAddr SetValFsVlanTunnelStpAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelStpAddress,
                                   VLAN_NP_STP_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_STP_ADDR (), SetValFsVlanTunnelStpAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelStpAddress, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelStpAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelLacpAddress
 Input       :  The Indices

                The Object 
                setValFsVlanTunnelLacpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelLacpAddress (tMacAddr SetValFsVlanTunnelLacpAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelLacpAddress,
                                   VLAN_NP_LACP_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_LACP_ADDR (), SetValFsVlanTunnelLacpAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelLacpAddress, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelLacpAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelDot1xAddress
 Input       :  The Indices

                The Object 
                setValFsVlanTunnelDot1xAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelDot1xAddress (tMacAddr SetValFsVlanTunnelDot1xAddress)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelDot1xAddress,
                                   VLAN_NP_DOT1X_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_DOT1X_ADDR (), SetValFsVlanTunnelDot1xAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelDot1xAddress, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelDot1xAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelGvrpAddress
 Input       :  The Indices

                The Object 
                setValFsVlanTunnelGvrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelGvrpAddress (tMacAddr SetValFsVlanTunnelGvrpAddress)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelGvrpAddress,
                                   VLAN_NP_GVRP_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_GVRP_ADDR (), SetValFsVlanTunnelGvrpAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelGvrpAddress, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelGvrpAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsVlanTunnelMvrpAddress
 Input       :  The Indices

                The Object
                setValFsVlanTunnelMvrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelMvrpAddress (tMacAddr SetValFsVlanTunnelMvrpAddress)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelMvrpAddress,
                                   VLAN_NP_MVRP_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_MVRP_ADDR (), SetValFsVlanTunnelMvrpAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIVlanTunnelMvrpAddress,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelMvrpAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelGmrpAddress
 Input       :  The Indices

                The Object 
                setValFsVlanTunnelGmrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelGmrpAddress (tMacAddr SetValFsVlanTunnelGmrpAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelGmrpAddress,
                                   VLAN_NP_GMRP_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_GMRP_ADDR (), SetValFsVlanTunnelGmrpAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelGmrpAddress, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelGmrpAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelMmrpAddress
 Input       :  The Indices

                The Object
                setValFsVlanTunnelMmrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelMmrpAddress (tMacAddr SetValFsVlanTunnelMmrpAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelMmrpAddress,
                                   VLAN_NP_MMRP_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_MMRP_ADDR (), SetValFsVlanTunnelMmrpAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIVlanTunnelMmrpAddress,
                          u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelMmrpAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelElmiAddress
 Input       :  The Indices

                The Object 
                setValFsVlanTunnelElmiAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelElmiAddress (tMacAddr SetValFsVlanTunnelElmiAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelElmiAddress,
                                   VLAN_NP_ELMI_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_ELMI_ADDR (), SetValFsVlanTunnelElmiAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelElmiAddress, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelElmiAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelLldpAddress
 Input       :  The Indices

                The Object 
                setValFsVlanTunnelLldpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelLldpAddress (tMacAddr SetValFsVlanTunnelLldpAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelLldpAddress,
                                   VLAN_NP_LLDP_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_LLDP_ADDR (), SetValFsVlanTunnelLldpAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelLldpAddress, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelLldpAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelEcfmAddress
 Input       :  The Indices

                The Object
                setValFsVlanTunnelEcfmAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelEcfmAddress (tMacAddr SetValFsVlanTunnelEcfmAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelEcfmAddress,
                                   VLAN_NP_ECFM_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_ECFM_ADDR (), SetValFsVlanTunnelEcfmAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelEcfmAddress, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelEcfmAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelEoamAddress
 Input       :  The Indices

                The Object
                setValFsVlanTunnelEoamAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelEoamAddress (tMacAddr SetValFsVlanTunnelEoamAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelEoamAddress,
                                   VLAN_NP_EOAM_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_EOAM_ADDR (), SetValFsVlanTunnelEoamAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelEoamAddress, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelEoamAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelIgmpAddress
 Input       :  The Indices

                The Object
                setValFsVlanTunnelIgmpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsVlanTunnelIgmpAddress(tMacAddr SetValFsVlanTunnelIgmpAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                   SetValFsVlanTunnelIgmpAddress,
                                   VLAN_NP_IGMP_PROTO_ID) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    VLAN_MEMCPY (VLAN_TUNNEL_IGMP_ADDR (), SetValFsVlanTunnelIgmpAddress,
                 VLAN_MAC_ADDR_LEN);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelIgmpAddress, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", VLAN_CURR_CONTEXT_ID (),
                      SetValFsVlanTunnelIgmpAddress));
    VlanSelectContext (u4CurrContextId);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelBpduPri
 Input       :  The Indices

                The Object 
                testValFsVlanTunnelBpduPri
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelBpduPri (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsVlanTunnelBpduPri)
{

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelBpduPri < VLAN_DEF_USER_PRIORITY) ||
        (i4TestValFsVlanTunnelBpduPri > VLAN_HIGHEST_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelStpAddress
 Input       :  The Indices

                The Object 
                testValFsVlanTunnelStpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelStpAddress (UINT4 *pu4ErrorCode,
                                 tMacAddr TestValFsVlanTunnelStpAddress)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_STP_ADDR (), TestValFsVlanTunnelStpAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelStpAddress) == VLAN_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_ADDRESS_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_MEMCMP (TestValFsVlanTunnelStpAddress, VLAN_TUNNEL_DOT1X_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelStpAddress, VLAN_TUNNEL_LACP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelStpAddress, VLAN_TUNNEL_GVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelStpAddress, VLAN_TUNNEL_GMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelStpAddress, VLAN_TUNNEL_MVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelStpAddress, VLAN_TUNNEL_MMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelStpAddress, VLAN_TUNNEL_ELMI_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelStpAddress, VLAN_TUNNEL_LLDP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelStpAddress, VLAN_TUNNEL_ECFM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelStpAddress, VLAN_TUNNEL_EOAM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelStpAddress, VLAN_TUNNEL_IGMP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))

    {
        CLI_SET_ERR (CLI_PB_STP_ADDR_ALREADY_USE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelLacpAddress
 Input       :  The Indices

                The Object 
                testValFsVlanTunnelLacpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelLacpAddress (UINT4 *pu4ErrorCode,
                                  tMacAddr TestValFsVlanTunnelLacpAddress)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_LACP_ADDR (), TestValFsVlanTunnelLacpAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelLacpAddress) == VLAN_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_ADDRESS_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_MEMCMP (TestValFsVlanTunnelLacpAddress, VLAN_TUNNEL_DOT1X_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLacpAddress, VLAN_TUNNEL_STP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLacpAddress, VLAN_TUNNEL_GVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLacpAddress, VLAN_TUNNEL_GMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLacpAddress, VLAN_TUNNEL_MVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLacpAddress, VLAN_TUNNEL_MMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLacpAddress, VLAN_TUNNEL_ELMI_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLacpAddress, VLAN_TUNNEL_LLDP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLacpAddress, VLAN_TUNNEL_ECFM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLacpAddress, VLAN_TUNNEL_EOAM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLacpAddress, VLAN_TUNNEL_IGMP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))

    {
        CLI_SET_ERR (CLI_PB_LACP_ADDR_ALREADY_USE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelDot1xAddress
 Input       :  The Indices

                The Object 
                testValFsVlanTunnelDot1xAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelDot1xAddress (UINT4 *pu4ErrorCode,
                                   tMacAddr TestValFsVlanTunnelDot1xAddress)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_DOT1X_ADDR (), TestValFsVlanTunnelDot1xAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelDot1xAddress) == VLAN_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_ADDRESS_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_MEMCMP (TestValFsVlanTunnelDot1xAddress, VLAN_TUNNEL_LACP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelDot1xAddress, VLAN_TUNNEL_STP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelDot1xAddress, VLAN_TUNNEL_GVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelDot1xAddress, VLAN_TUNNEL_GMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelDot1xAddress, VLAN_TUNNEL_MVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelDot1xAddress, VLAN_TUNNEL_MMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelDot1xAddress, VLAN_TUNNEL_ELMI_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelDot1xAddress, VLAN_TUNNEL_LLDP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelDot1xAddress, VLAN_TUNNEL_ECFM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelDot1xAddress, VLAN_TUNNEL_EOAM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelDot1xAddress, VLAN_TUNNEL_IGMP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))

    {
        CLI_SET_ERR (CLI_PB_DOT1X_ADDR_ALREADY_USE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelGvrpAddress
 Input       :  The Indices

                The Object 
                testValFsVlanTunnelGvrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelGvrpAddress (UINT4 *pu4ErrorCode,
                                  tMacAddr TestValFsVlanTunnelGvrpAddress)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_GVRP_ADDR (), TestValFsVlanTunnelGvrpAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelGvrpAddress) == VLAN_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_ADDRESS_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_MEMCMP (TestValFsVlanTunnelGvrpAddress, VLAN_TUNNEL_DOT1X_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGvrpAddress, VLAN_TUNNEL_LACP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGvrpAddress, VLAN_TUNNEL_STP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGvrpAddress, VLAN_TUNNEL_GMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGvrpAddress, VLAN_TUNNEL_MVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGvrpAddress, VLAN_TUNNEL_MMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGvrpAddress, VLAN_TUNNEL_ELMI_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGvrpAddress, VLAN_TUNNEL_LLDP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGvrpAddress, VLAN_TUNNEL_ECFM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGvrpAddress, VLAN_TUNNEL_EOAM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGvrpAddress, VLAN_TUNNEL_IGMP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))

    {
        CLI_SET_ERR (CLI_PB_GVRP_ADDR_ALREADY_USE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelMvrpAddress
 Input       :  The Indices

                The Object
                testValFsVlanTunnelMvrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelMvrpAddress (UINT4 *pu4ErrorCode,
                                  tMacAddr TestValFsVlanTunnelMvrpAddress)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_MVRP_ADDR (), TestValFsVlanTunnelMvrpAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelMvrpAddress) == VLAN_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_ADDRESS_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_MEMCMP (TestValFsVlanTunnelMvrpAddress, VLAN_TUNNEL_DOT1X_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMvrpAddress, VLAN_TUNNEL_LACP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMvrpAddress, VLAN_TUNNEL_STP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMvrpAddress, VLAN_TUNNEL_GMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMvrpAddress, VLAN_TUNNEL_GVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMvrpAddress, VLAN_TUNNEL_MMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMvrpAddress, VLAN_TUNNEL_ELMI_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMvrpAddress, VLAN_TUNNEL_LLDP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMvrpAddress, VLAN_TUNNEL_ECFM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMvrpAddress, VLAN_TUNNEL_EOAM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMvrpAddress, VLAN_TUNNEL_IGMP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))

    {
        CLI_SET_ERR (CLI_PB_MVRP_ADDR_ALREADY_USE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelGmrpAddress
 Input       :  The Indices

                The Object 
                testValFsVlanTunnelGmrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelGmrpAddress (UINT4 *pu4ErrorCode,
                                  tMacAddr TestValFsVlanTunnelGmrpAddress)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_GMRP_ADDR (), TestValFsVlanTunnelGmrpAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelGmrpAddress) == VLAN_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_ADDRESS_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_MEMCMP (TestValFsVlanTunnelGmrpAddress, VLAN_TUNNEL_DOT1X_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGmrpAddress, VLAN_TUNNEL_LACP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGmrpAddress, VLAN_TUNNEL_STP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGmrpAddress, VLAN_TUNNEL_GVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGmrpAddress, VLAN_TUNNEL_MMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGmrpAddress, VLAN_TUNNEL_MVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGmrpAddress, VLAN_TUNNEL_ELMI_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGmrpAddress, VLAN_TUNNEL_LLDP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGmrpAddress, VLAN_TUNNEL_ECFM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGmrpAddress, VLAN_TUNNEL_EOAM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelGmrpAddress, VLAN_TUNNEL_IGMP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))

    {
        CLI_SET_ERR (CLI_PB_GMRP_ADDR_ALREADY_USE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelMmrpAddress
 Input       :  The Indices

                The Object
                testValFsVlanTunnelMmrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelMmrpAddress (UINT4 *pu4ErrorCode,
                                  tMacAddr TestValFsVlanTunnelMmrpAddress)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_MMRP_ADDR (), TestValFsVlanTunnelMmrpAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelMmrpAddress) == VLAN_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_ADDRESS_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_MEMCMP (TestValFsVlanTunnelMmrpAddress, VLAN_TUNNEL_DOT1X_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMmrpAddress, VLAN_TUNNEL_LACP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMmrpAddress, VLAN_TUNNEL_STP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMmrpAddress, VLAN_TUNNEL_GVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMmrpAddress, VLAN_TUNNEL_GMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMmrpAddress, VLAN_TUNNEL_MVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMmrpAddress, VLAN_TUNNEL_ELMI_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMmrpAddress, VLAN_TUNNEL_LLDP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMmrpAddress, VLAN_TUNNEL_ECFM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMmrpAddress, VLAN_TUNNEL_EOAM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelMmrpAddress, VLAN_TUNNEL_IGMP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))

    {
        CLI_SET_ERR (CLI_PB_MMRP_ADDR_ALREADY_USE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelElmiAddress
 Input       :  The Indices

                The Object 
                testValFsVlanTunnelElmiAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelElmiAddress (UINT4 *pu4ErrorCode,
                                  tMacAddr TestValFsVlanTunnelElmiAddress)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_ELMI_ADDR (), TestValFsVlanTunnelElmiAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelElmiAddress) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((VLAN_MEMCMP (TestValFsVlanTunnelElmiAddress, VLAN_TUNNEL_DOT1X_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelElmiAddress, VLAN_TUNNEL_LACP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelElmiAddress, VLAN_TUNNEL_STP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelElmiAddress, VLAN_TUNNEL_GVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelElmiAddress, VLAN_TUNNEL_GMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelElmiAddress, VLAN_TUNNEL_MVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelElmiAddress, VLAN_TUNNEL_MMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelElmiAddress, VLAN_TUNNEL_LLDP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelElmiAddress, VLAN_TUNNEL_ECFM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelElmiAddress, VLAN_TUNNEL_EOAM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelElmiAddress, VLAN_TUNNEL_IGMP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))
    {
        CLI_SET_ERR (CLI_PB_ELMI_ADDR_ALREADY_USE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelLldpAddress
 Input       :  The Indices

                The Object 
                testValFsVlanTunnelLldpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelLldpAddress (UINT4 *pu4ErrorCode,
                                  tMacAddr TestValFsVlanTunnelLldpAddress)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_LLDP_ADDR (), TestValFsVlanTunnelLldpAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelLldpAddress) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((VLAN_MEMCMP (TestValFsVlanTunnelLldpAddress, VLAN_TUNNEL_DOT1X_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLldpAddress, VLAN_TUNNEL_LACP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLldpAddress, VLAN_TUNNEL_STP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLldpAddress, VLAN_TUNNEL_GVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLldpAddress, VLAN_TUNNEL_GMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLldpAddress, VLAN_TUNNEL_MVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLldpAddress, VLAN_TUNNEL_MMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLldpAddress, VLAN_TUNNEL_ELMI_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLldpAddress, VLAN_TUNNEL_ECFM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLldpAddress, VLAN_TUNNEL_EOAM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelLldpAddress, VLAN_TUNNEL_IGMP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))
    {
        CLI_SET_ERR (CLI_PB_LLDP_ADDR_ALREADY_USE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelEcfmAddress
 Input       :  The Indices

                The Object
                testValFsVlanTunnelEcfmAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelEcfmAddress (UINT4 *pu4ErrorCode,
                                  tMacAddr TestValFsVlanTunnelEcfmAddress)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_ECFM_ADDR (), TestValFsVlanTunnelEcfmAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelEcfmAddress) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((VLAN_MEMCMP (TestValFsVlanTunnelEcfmAddress, VLAN_TUNNEL_DOT1X_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEcfmAddress, VLAN_TUNNEL_LACP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEcfmAddress, VLAN_TUNNEL_STP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEcfmAddress, VLAN_TUNNEL_GVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEcfmAddress, VLAN_TUNNEL_GMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEcfmAddress, VLAN_TUNNEL_MVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEcfmAddress, VLAN_TUNNEL_MMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEcfmAddress, VLAN_TUNNEL_ELMI_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEcfmAddress, VLAN_TUNNEL_LLDP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEcfmAddress, VLAN_TUNNEL_EOAM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEcfmAddress, VLAN_TUNNEL_IGMP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))
    {
        CLI_SET_ERR (CLI_PB_ECFM_ADDR_ALREADY_USE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelEoamAddress
 Input       :  The Indices

                The Object
                testValFsVlanTunnelEoamAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelEoamAddress (UINT4 *pu4ErrorCode,
                                  tMacAddr TestValFsVlanTunnelEoamAddress)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_EOAM_ADDR (), TestValFsVlanTunnelEoamAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelEoamAddress) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((VLAN_MEMCMP (TestValFsVlanTunnelEoamAddress, VLAN_TUNNEL_DOT1X_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEoamAddress, VLAN_TUNNEL_LACP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEoamAddress, VLAN_TUNNEL_STP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEoamAddress, VLAN_TUNNEL_GVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEoamAddress, VLAN_TUNNEL_GMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEoamAddress, VLAN_TUNNEL_MVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEoamAddress, VLAN_TUNNEL_MMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEoamAddress, VLAN_TUNNEL_ELMI_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEoamAddress, VLAN_TUNNEL_LLDP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEoamAddress, VLAN_TUNNEL_ECFM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelEoamAddress, VLAN_TUNNEL_IGMP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelIgmpAddress
 Input       :  The Indices

                The Object
                testValFsVlanTunnelIgmpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsVlanTunnelIgmpAddress(UINT4 *pu4ErrorCode , tMacAddr TestValFsVlanTunnelIgmpAddress)
{

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_MEMCMP (VLAN_TUNNEL_IGMP_ADDR (), TestValFsVlanTunnelIgmpAddress,
                     VLAN_MAC_ADDR_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    if (VlanIsValidMcastAddr (TestValFsVlanTunnelIgmpAddress) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_MEMCMP (TestValFsVlanTunnelIgmpAddress, VLAN_TUNNEL_DOT1X_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelIgmpAddress, VLAN_TUNNEL_LACP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelIgmpAddress, VLAN_TUNNEL_STP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelIgmpAddress, VLAN_TUNNEL_GVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelIgmpAddress, VLAN_TUNNEL_GMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelIgmpAddress, VLAN_TUNNEL_MVRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelIgmpAddress, VLAN_TUNNEL_MMRP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelIgmpAddress, VLAN_TUNNEL_ELMI_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelIgmpAddress, VLAN_TUNNEL_LLDP_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelIgmpAddress, VLAN_TUNNEL_ECFM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0) ||
        (VLAN_MEMCMP (TestValFsVlanTunnelIgmpAddress, VLAN_TUNNEL_EOAM_ADDR (),
                      VLAN_MAC_ADDR_LEN) == 0))
    {
        CLI_SET_ERR (CLI_PB_EOAM_ADDR_ALREADY_USE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelBpduPri
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelBpduPri (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelStpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelStpAddress (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelLacpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelLacpAddress (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelDot1xAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelDot1xAddress (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelGvrpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelGvrpAddress (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelGvrpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelMvrpAddress (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/**************************************************************************** 
 Function    :  nmhDepv2FsVlanTunnelGmrpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelGmrpAddress (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelMmrpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelMmrpAddress (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelElmiAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelElmiAddress (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelLldpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelLldpAddress (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelEcfmAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelEcfmAddress (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelEoamAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelEoamAddress (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelIgmpAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsVlanTunnelIgmpAddress(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVlanTunnelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVlanTunnelTable
 Input       :  The Indices
                FsVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVlanTunnelTable (INT4 i4FsVlanPort)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVlanTunnelTable
 Input       :  The Indices
                FsVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVlanTunnelTable (INT4 *pi4FsVlanPort)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsVlanTunnelTable (0, pi4FsVlanPort));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVlanTunnelTable
 Input       :  The Indices
                FsVlanPort
                nextFsVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVlanTunnelTable (INT4 i4FsVlanPort, INT4 *pi4NextFsVlanPort)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    if (i4FsVlanPort < 0 || i4FsVlanPort > VLAN_MAX_PORTS)
    {
        return SNMP_FAILURE;
    }

    for (u2Port = (UINT2) (i4FsVlanPort + 1);
         u2Port <= VLAN_MAX_PORTS; u2Port++)
    {
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pVlanPortEntry != NULL)
        {
            *pi4NextFsVlanPort = (INT4) u2Port;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelStatus
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelStatus (INT4 i4FsVlanPort, INT4 *pi4RetValFsVlanTunnelStatus)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
    {
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsVlanTunnelStatus = (INT4) VLAN_TUNNEL_STATUS (pVlanPortEntry);

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelStatus
 Input       :  The Indices
                FsVlanPort

                The Object 
                setValFsVlanTunnelStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelStatus (INT4 i4FsVlanPort, INT4 i4SetValFsVlanTunnelStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT2               u2AggPort;
    INT1                i1SetStatus;
    UINT1               u1TunnelStatus;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2AggPort = (UINT2) i4FsVlanPort;

    u1TunnelStatus = (UINT1) i4SetValFsVlanTunnelStatus;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2AggPort);
    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (u1TunnelStatus == VLAN_TUNNEL_STATUS (pVlanPortEntry))
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    i1SetStatus = VlanSetFsVlanTunnelStatus (i4FsVlanPort,
                                             i4SetValFsVlanTunnelStatus);

    /* Sending Trigger to MSR */

    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelStatus, u4SeqNum,
                          FALSE, VlanLock, VlanUnLock, 1, i1SetStatus);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", VLAN_GET_IFINDEX (i4FsVlanPort),
                      i4SetValFsVlanTunnelStatus));

    VlanSelectContext (u4CurrContextId);
    if (i1SetStatus == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  VlanSetFsVlanTunnelStatus
 Input       :  The Indices
                FsVlanPort

                The Object 
                setValFsVlanTunnelStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
VlanSetFsVlanTunnelStatus (INT4 i4FsVlanPort, INT4 i4SetValFsVlanTunnelStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT1               u1TunnelStatus;
    UINT2               u2Port;
    INT4                i4RetVal;
    UINT4               u4Mode;
    UINT4               u4Port;
    u2Port = (UINT2) i4FsVlanPort;
    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        return SNMP_FAILURE;
    }

    u1TunnelStatus = (UINT1) i4SetValFsVlanTunnelStatus;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (VLAN_TUNNEL_STATUS (pVlanPortEntry) ==
        (UINT1) i4SetValFsVlanTunnelStatus)
    {
        return SNMP_SUCCESS;
    }
    u4Port = VLAN_GET_IFINDEX (u2Port);
    if (u1TunnelStatus == VLAN_ENABLED)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME,
                       "Enabling tunneling on Port  - %d \n", u4Port);

        u4Mode = VLAN_TUNNEL_EXTERNAL;
    }
    else
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME,
                       "Disabling tunneling on Port  - %d \n", u4Port);

        u4Mode = VLAN_TUNNEL_INTERNAL;
    }

    i4RetVal =
        VlanHwSetPortTunnelMode (VLAN_CURR_CONTEXT_ID (), u2Port, u4Mode);

    if (i4RetVal == VLAN_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                       "Setting Tunnel mode as EXTERNAL failed for Port - %d \n",
                       u4Port);
        return SNMP_FAILURE;
    }

    VLAN_TUNNEL_STATUS (pVlanPortEntry) = u1TunnelStatus;

    if (u1TunnelStatus == VLAN_ENABLED)
    {
        VlanL2IwfSetPortVlanTunnelStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                          OSIX_TRUE);
    }
    else
    {
        VlanL2IwfSetPortVlanTunnelStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                          OSIX_FALSE);
    }

    /* Tunnelling is disabled on that port. Reset STP BPDU tunnelling too */

    /* If the default tunnel status of this tunnel port for any protocol below is modified, it needs
     * to be taken care on the global variable gau1PbDefPortProtoTunnelStatus also in CBP row*/
    VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
    VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;

    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_STP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_GVRP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_GMRP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_IGMP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_LACP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_DOT1X,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_MVRP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_MMRP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_ELMI,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_LLDP,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_EOAM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                            u2Port, L2_PROTO_ECFM,
                                            VLAN_TUNNEL_PROTOCOL_PEER);
    /*Clearning Tunnel Rx Counters for all protocols */
    VLAN_TUNNEL_DOT1X_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_LACP_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_STP_BPDUS_RX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_GVRP_PDUS_RX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_GMRP_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_IGMP_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_MVRP_PDUS_RX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_MMRP_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_EOAM_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_ECFM_PKTS_RX (pVlanPortEntry) = 0;

    /*Clearning Tunnel Tx Counters for all protocols */
    VLAN_TUNNEL_DOT1X_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_LACP_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_STP_BPDUS_TX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_GVRP_PDUS_TX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_GMRP_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_IGMP_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_MVRP_PDUS_TX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_MMRP_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_EOAM_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_TUNNEL_ECFM_PKTS_TX (pVlanPortEntry) = 0;

    /*Clearing Discard Rx Counters for all Protocols */
    VLAN_DISCARD_DOT1X_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_DISCARD_LACP_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_DISCARD_STP_BPDUS_RX (pVlanPortEntry) = 0;
    VLAN_DISCARD_GVRP_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_DISCARD_GMRP_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_DISCARD_IGMP_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_DISCARD_MVRP_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_DISCARD_MMRP_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_DISCARD_EOAM_PKTS_RX (pVlanPortEntry) = 0;
    VLAN_DISCARD_ECFM_PKTS_RX (pVlanPortEntry) = 0;

    /*Clearing Discard Tx Counters for all Protocols */
    VLAN_DISCARD_DOT1X_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_DISCARD_LACP_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_DISCARD_STP_BPDUS_TX (pVlanPortEntry) = 0;
    VLAN_DISCARD_GVRP_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_DISCARD_GMRP_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_DISCARD_IGMP_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_DISCARD_MVRP_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_DISCARD_MMRP_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_DISCARD_EOAM_PKTS_TX (pVlanPortEntry) = 0;
    VLAN_DISCARD_ECFM_PKTS_TX (pVlanPortEntry) = 0;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelStatus
 Input       :  The Indices
                FsVlanPort

                The Object 
                testValFsVlanTunnelStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelStatus (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                             INT4 i4TestValFsVlanTunnelStatus)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;
    UINT1               u1Status;

    if ((i4TestValFsVlanTunnelStatus != VLAN_ENABLED) &&
        (i4TestValFsVlanTunnelStatus != VLAN_DISABLED))
    {
        /* The variable cannot take this value */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        /* Can assume this value but inconsistent at this time */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (i4TestValFsVlanTunnelStatus == VLAN_ENABLED)
    {

        if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_BRIDGE_MODE)
        {
            CLI_SET_ERR (CLI_PB_PROVIDERBRIDGE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (pVlanPortEntry->u1PortType != VLAN_ACCESS_PORT)
        {
            /* Tunnel ports should be access ports */
            CLI_SET_ERR (CLI_PB_INCONSISTENT_PORT_TYPE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (pVlanPortEntry->u1AccpFrmTypes ==
            VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
        {
            /* Tunnel ports should not be set to accept only tagged frames */

            CLI_SET_ERR (CLI_PB_INCONSISTENT_FRAME_TYPE_ERROR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
#ifdef RSTP_WANTED
        if ((VlanL2IwfGetProtocolEnabledStatusOnPort
             ((UINT2) (VLAN_GET_IFINDEX (u2Port)), L2_PROTO_STP,
              &u1Status) == L2IWF_SUCCESS) && (u1Status == OSIX_ENABLED))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_PB_STP_PORT_MODE_ERR);
            return SNMP_FAILURE;
        }
#endif
        if ((VlanL2IwfGetProtocolEnabledStatusOnPort
             ((UINT2) (VLAN_GET_IFINDEX (u2Port)), L2_PROTO_GMRP,
              &u1Status) == L2IWF_SUCCESS) && (u1Status == OSIX_ENABLED))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_PB_GMRP_PORT_MODE_ERR);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelTable
 Input       :  The Indices
                FsVlanPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVlanTunnelProtocolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVlanTunnelProtocolTable
 Input       :  The Indices
                FsVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVlanTunnelProtocolTable (INT4 i4FsVlanPort)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVlanTunnelProtocolTable
 Input       :  The Indices
                FsVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVlanTunnelProtocolTable (INT4 *pi4FsVlanPort)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsVlanTunnelProtocolTable (0, pi4FsVlanPort));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVlanTunnelProtocolTable
 Input       :  The Indices
                FsVlanPort
                nextFsVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVlanTunnelProtocolTable (INT4 i4FsVlanPort,
                                          INT4 *pi4NextFsVlanPort)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (i4FsVlanPort < 0 || i4FsVlanPort > VLAN_MAX_PORTS)
    {
        return SNMP_FAILURE;
    }

    for (u2Port = (UINT2) (i4FsVlanPort + 1); u2Port <= VLAN_MAX_PORTS;
         u2Port++)
    {
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pVlanPortEntry != NULL)
        {
            /* In case of Customer bridges, all ports will be considered.
             * In case of other bridge modes, only tunnel ports will be
             * considered */
            if ((VLAN_BRIDGE_MODE () != VLAN_CUSTOMER_BRIDGE_MODE) &&
                (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
            {
                continue;
            }
            *pi4NextFsVlanPort = (INT4) u2Port;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolDot1x
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolDot1x
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolDot1x (INT4 i4FsVlanPort,
                                 INT4 *pi4RetValFsVlanTunnelProtocolDot1x)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolDot1x = VLAN_DOT1X_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolLacp
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolLacp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolLacp (INT4 i4FsVlanPort,
                                INT4 *pi4RetValFsVlanTunnelProtocolLacp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolLacp = VLAN_LACP_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolStp
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolStp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolStp (INT4 i4FsVlanPort,
                               INT4 *pi4RetValFsVlanTunnelProtocolStp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolStp = VLAN_STP_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolGvrp
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolGvrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolGvrp (INT4 i4FsVlanPort,
                                INT4 *pi4RetValFsVlanTunnelProtocolGvrp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolGvrp = VLAN_GVRP_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsVlanTunnelProtocolMvrp
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanTunnelProtocolMvrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolMvrp (INT4 i4FsVlanPort,
                                INT4 *pi4RetValFsVlanTunnelProtocolMvrp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolMvrp = VLAN_MVRP_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolGmrp
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolGmrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolGmrp (INT4 i4FsVlanPort,
                                INT4 *pi4RetValFsVlanTunnelProtocolGmrp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolGmrp = VLAN_GMRP_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolMmrp
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanTunnelProtocolMmrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolMmrp (INT4 i4FsVlanPort,
                                INT4 *pi4RetValFsVlanTunnelProtocolMmrp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolMmrp = VLAN_MMRP_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolElmi
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolElmi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolElmi (INT4 i4FsVlanPort,
                                INT4 *pi4RetValFsVlanTunnelProtocolElmi)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolElmi = VLAN_ELMI_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolLldp
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolLldp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolLldp (INT4 i4FsVlanPort,
                                INT4 *pi4RetValFsVlanTunnelProtocolLldp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolLldp = VLAN_LLDP_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolIgmp
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolIgmp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolIgmp (INT4 i4FsVlanPort,
                                INT4 *pi4RetValFsVlanTunnelProtocolIgmp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolIgmp = VLAN_IGMP_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolEcfm
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolIgmp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolEcfm (INT4 i4FsVlanPort,
                                INT4 *pi4RetValFsVlanTunnelProtocolEcfm)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolEcfm = VLAN_ECFM_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelOverrideOption
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanTunnelOverrideOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelOverrideOption (INT4 i4FsVlanPort,
                                  INT4 *pi4RetValFsVlanTunnelOverrideOption)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port = VLAN_ZERO;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelOverrideOption =
            (INT4) pVlanPortEntry->u4OverrideOption;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolEoam
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanTunnelProtocolEoam
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolEoam (INT4 i4FsVlanPort,
                                INT4 *pi4RetValFsVlanTunnelProtocolEoam)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    if (pVlanPortEntry != NULL)
    {
        *pi4RetValFsVlanTunnelProtocolEoam = VLAN_EOAM_TUNNEL_STATUS
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects */

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelProtocolDot1x
 Input       :  The Indices
                FsVlanPort

                The Object 
                setValFsVlanTunnelProtocolDot1x
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolDot1x (INT4 i4FsVlanPort,
                                 INT4 i4SetValFsVlanTunnelProtocolDot1x)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolDot1x ==
            VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }
        RM_GET_SEQ_NUM (&u4SeqNum);
        VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolDot1x;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_DOT1X,
                                                (UINT1)
                                                i4SetValFsVlanTunnelProtocolDot1x);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_DOT1X_PROTO_ID,
                                             i4SetValFsVlanTunnelProtocolDot1x);
        /*Clearing DOT1X tunnel counters */
        VLAN_DISCARD_DOT1X_PKTS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_DOT1X_PKTS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_DOT1X_PKTS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_DOT1X_PKTS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelProtocolDot1x,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolDot1x));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelProtocolLacp
 Input       :  The Indices
                FsVlanPort

                The Object 
                setValFsVlanTunnelProtocolLacp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolLacp (INT4 i4FsVlanPort,
                                INT4 i4SetValFsVlanTunnelProtocolLacp)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolLacp ==
            VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);

        VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolLacp;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                L2_PROTO_LACP, (UINT1)
                                                i4SetValFsVlanTunnelProtocolLacp);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_LACP_PROTO_ID,
                                             i4SetValFsVlanTunnelProtocolLacp);
        /*Clearing LACP tunnel counters */
        VLAN_DISCARD_LACP_PKTS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_LACP_PKTS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_LACP_PKTS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_LACP_PKTS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelProtocolLacp,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolLacp));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelProtocolStp
 Input       :  The Indices
                FsVlanPort

                The Object 
                setValFsVlanTunnelProtocolStp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolStp (INT4 i4FsVlanPort,
                               INT4 i4SetValFsVlanTunnelProtocolStp)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolStp ==
            VLAN_STP_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);

        VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolStp;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                L2_PROTO_STP, (UINT1)
                                                i4SetValFsVlanTunnelProtocolStp);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_STP_PROTO_ID,
                                             i4SetValFsVlanTunnelProtocolStp);
        /*Clearing STP tunnel counters */
        VLAN_DISCARD_STP_BPDUS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_STP_BPDUS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_STP_BPDUS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_STP_BPDUS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelProtocolStp,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolStp));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelProtocolGvrp
 Input       :  The Indices
                FsVlanPort

                The Object 
                setValFsVlanTunnelProtocolGvrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolGvrp (INT4 i4FsVlanPort,
                                INT4 i4SetValFsVlanTunnelProtocolGvrp)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolGvrp ==
            VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);

        VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolGvrp;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                L2_PROTO_GVRP, (UINT1)
                                                i4SetValFsVlanTunnelProtocolGvrp);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_GVRP_PROTO_ID,
                                             i4SetValFsVlanTunnelProtocolGvrp);
        /*Clearing GVRP tunnel counters */
        VLAN_DISCARD_GVRP_PKTS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_GVRP_PDUS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_GVRP_PDUS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_GVRP_PKTS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelProtocolGvrp,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolGvrp));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelProtocolMvrp
 Input       :  The Indices
                FsVlanPort

                The Object
                setValFsVlanTunnelProtocolMvrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolMvrp (INT4 i4FsVlanPort,
                                INT4 i4SetValFsVlanTunnelProtocolMvrp)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolMvrp ==
            VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);

        VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolMvrp;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                L2_PROTO_MVRP, (UINT1)
                                                i4SetValFsVlanTunnelProtocolMvrp);
        /*Clearing MVRP tunnel counters */
        VLAN_DISCARD_MVRP_PKTS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_MVRP_PDUS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_MVRP_PDUS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_MVRP_PKTS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIVlanTunnelProtocolMvrp,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolMvrp));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelProtocolGmrp
 Input       :  The Indices
                FsVlanPort

                The Object 
                setValFsVlanTunnelProtocolGmrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolGmrp (INT4 i4FsVlanPort,
                                INT4 i4SetValFsVlanTunnelProtocolGmrp)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolGmrp ==
            VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);

        VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolGmrp;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                L2_PROTO_GMRP, (UINT1)
                                                i4SetValFsVlanTunnelProtocolGmrp);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_GMRP_PROTO_ID,
                                             i4SetValFsVlanTunnelProtocolGmrp);
        /*Clearing GMRP tunnel counters */
        VLAN_DISCARD_GMRP_PKTS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_GMRP_PKTS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_GMRP_PKTS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_GMRP_PKTS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelProtocolGmrp,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolGmrp));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhSetFsVlanTunnelProtocolMmrp
 Input       :  The Indices
                FsVlanPort

                The Object
                setValFsVlanTunnelProtocolMmrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolMmrp (INT4 i4FsVlanPort,
                                INT4 i4SetValFsVlanTunnelProtocolMmrp)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolMmrp ==
            VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);

        VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolMmrp;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                L2_PROTO_MMRP, (UINT1)
                                                i4SetValFsVlanTunnelProtocolMmrp);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_MMRP_PROTO_ID,
                                             i4SetValFsVlanTunnelProtocolMmrp);

        /*Clearing MMRP tunnel counters */
        VLAN_DISCARD_MMRP_PKTS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_MMRP_PKTS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_MMRP_PKTS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_MMRP_PKTS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIVlanTunnelProtocolMmrp,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolMmrp));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelProtocolElmi
 Input       :  The Indices
                FsVlanPort

                The Object 
                setValFsVlanTunnelProtocolElmi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolElmi (INT4 i4FsVlanPort,
                                INT4 i4SetValFsVlanTunnelProtocolElmi)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolElmi ==
            VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }
        RM_GET_SEQ_NUM (&u4SeqNum);
        VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolElmi;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_ELMI,
                                                (UINT1)
                                                i4SetValFsVlanTunnelProtocolElmi);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_ELMI_PROTO_ID,
                                             i4SetValFsVlanTunnelProtocolElmi);

        /*Clearing ELMI tunnel counters */
        VLAN_DISCARD_ELMI_PKTS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_ELMI_PKTS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_ELMI_PKTS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_ELMI_PKTS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelProtocolElmi,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolElmi));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelProtocolLldp
 Input       :  The Indices
                FsVlanPort

                The Object 
                setValFsVlanTunnelProtocolLldp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolLldp (INT4 i4FsVlanPort,
                                INT4 i4SetValFsVlanTunnelProtocolLldp)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolLldp ==
            VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }
        RM_GET_SEQ_NUM (&u4SeqNum);
        VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolLldp;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_LLDP,
                                                (UINT1)
                                                i4SetValFsVlanTunnelProtocolLldp);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_LLDP_PROTO_ID,
                                             i4SetValFsVlanTunnelProtocolLldp);

        /*Clearing LLDP tunnel counters */
        VLAN_DISCARD_LLDP_PKTS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_LLDP_PKTS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_LLDP_PKTS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_LLDP_PKTS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelProtocolLldp,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolLldp));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelProtocolIgmp
 Input       :  The Indices
                FsVlanPort

                The Object 
                setValFsVlanTunnelProtocolIgmp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolIgmp (INT4 i4FsVlanPort,
                                INT4 i4SetValFsVlanTunnelProtocolIgmp)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolIgmp ==
            VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);

        VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolIgmp;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                L2_PROTO_IGMP, (UINT1)
                                                i4SetValFsVlanTunnelProtocolIgmp);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_IGMP_PROTO_ID,
                                             i4SetValFsVlanTunnelProtocolIgmp);
        /*Clearing IGMP tunnel counters */
        VLAN_DISCARD_IGMP_PKTS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_IGMP_PKTS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_IGMP_PKTS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_IGMP_PKTS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelProtocolIgmp,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolIgmp));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelProtocolEcfm
 Input       :  The Indices
                FsVlanPort

                The Object 
                setValFsVlanTunnelProtocolEcfm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolEcfm (INT4 i4FsVlanPort,
                                INT4 i4SetValFsVlanTunnelProtocolEcfm)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolEcfm ==
            VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);

        VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolEcfm;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                L2_PROTO_ECFM, (UINT1)
                                                i4SetValFsVlanTunnelProtocolEcfm);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_ECFM_PROTO_ID,
                                             i4SetValFsVlanTunnelProtocolEcfm);
        /*Clearing ECFM tunnel counters */
        VLAN_DISCARD_ECFM_PKTS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_ECFM_PKTS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_ECFM_PKTS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_ECFM_PKTS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelProtocolEcfm,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolEcfm));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelOverrideOption
 Input       :  The Indices
                FsVlanPort

                The Object
                setValFsVlanTunnelOverrideOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelOverrideOption (INT4 i4FsVlanPort,
                                  INT4 i4SetValFsVlanTunnelOverrideOption)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port = VLAN_ZERO;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        pVlanPortEntry->u4OverrideOption =
            (UINT4) i4SetValFsVlanTunnelOverrideOption;

        VlanL2IwfSetOverrideOption (u2Port,
                                    (UINT1) i4SetValFsVlanTunnelOverrideOption);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsVlanTunnelProtocolEoam
 Input       :  The Indices
                FsVlanPort

                The Object
                setValFsVlanTunnelProtocolEoam
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVlanTunnelProtocolEoam (INT4 i4FsVlanPort,
                                INT4 i4SetValFsVlanTunnelProtocolEoam)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        if (i4SetValFsVlanTunnelProtocolEoam ==
            VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry))
        {
            return SNMP_SUCCESS;
        }

        RM_GET_SEQ_NUM (&u4SeqNum);

        VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry) =
            (UINT1) i4SetValFsVlanTunnelProtocolEoam;
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                L2_PROTO_EOAM, (UINT1)
                                                i4SetValFsVlanTunnelProtocolEoam);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_EOAM_PROTO_ID,
                                             i4SetValFsVlanTunnelProtocolEoam);
        /*Clearing EOAM tunnel counters */
        VLAN_DISCARD_EOAM_PKTS_RX (pVlanPortEntry) = 0;
        VLAN_TUNNEL_EOAM_PKTS_RX (pVlanPortEntry) = 0;

        VLAN_TUNNEL_EOAM_PKTS_TX (pVlanPortEntry) = 0;
        VLAN_DISCARD_EOAM_PKTS_TX (pVlanPortEntry) = 0;

        /* Sending Trigger to MSR */

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIVlanTunnelProtocolEoam,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (i4FsVlanPort),
                          i4SetValFsVlanTunnelProtocolEoam));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolDot1x
 Input       :  The Indices
                FsVlanPort

                The Object 
                testValFsVlanTunnelProtocolDot1x
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolDot1x (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                    INT4 i4TestValFsVlanTunnelProtocolDot1x)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolDot1x != VLAN_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsVlanTunnelProtocolDot1x != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolDot1x != VLAN_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* In case of Q-in-Q Bridge mode, tunnel status must be enabled
     * for configuring the protocol tunnel status even though it
     * is peer/tunnel/discard*/
    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) &&
        (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
    {
        CLI_SET_ERR (CLI_PB_DOT1X_PORT_TYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_DOT1X_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->u1IfType == VLAN_LAGG_INTERFACE_TYPE)
    {
        CLI_SET_ERR (CLI_PB_DOT1X_LAGG_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Tunnel/Discard options are not allowed when the protocol status
     * is enabled on a port.
     */
    if ((i4TestValFsVlanTunnelProtocolDot1x == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||
        (i4TestValFsVlanTunnelProtocolDot1x == VLAN_TUNNEL_PROTOCOL_DISCARD))
    {
        if (VlanPnacGetPnacEnableStatus () == PNAC_SUCCESS)
        {
            CLI_SET_ERR (CLI_PB_TUNNEL_PROTOCOL_ENA_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {

        if ((i4TestValFsVlanTunnelProtocolDot1x == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (VLAN_PB_PORT_TYPE (u2Port) == VLAN_CUSTOMER_EDGE_PORT))
        {
            /*
             * Protocol tunneling should not be allowed on CEPs supporting
             * more than one PEP.
             */
            if (VlanPbIsTunnelingValidOnCep (u2Port) == VLAN_FALSE)
            {
                CLI_SET_ERR (CLI_PB_TUNNEL_PEP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolLacp
 Input       :  The Indices
                FsVlanPort

                The Object 
                testValFsVlanTunnelProtocolLacp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolLacp (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                   INT4 i4TestValFsVlanTunnelProtocolLacp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolLacp != VLAN_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsVlanTunnelProtocolLacp != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolLacp != VLAN_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* In case of Q-in-Q Bridge mode, tunnel status must be enabled
     * for configuring the protocol tunnel status even though it
     * is peer/tunnel/discard*/
    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) &&
        (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
    {
        CLI_SET_ERR (CLI_PB_LACP_PORT_TYPE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_LACP_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pVlanPortEntry->u1IfType == VLAN_LAGG_INTERFACE_TYPE)
    {
        CLI_SET_ERR (CLI_PB_LACP_LAGG_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry) ==
        i4TestValFsVlanTunnelProtocolLacp)
    {
        return SNMP_SUCCESS;
    }

    if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {
        if ((i4TestValFsVlanTunnelProtocolLacp == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (VLAN_PB_PORT_TYPE (u2Port) == VLAN_CUSTOMER_EDGE_PORT))
        {
            /*
             * Protocol tunneling should not be allowed on CEPs supporting
             * more than one PEP.
             */
            if (VlanPbIsTunnelingValidOnCep (u2Port) == VLAN_FALSE)
            {
                CLI_SET_ERR (CLI_PB_TUNNEL_PEP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolStp
 Input       :  The Indices
                FsVlanPort

                The Object 
                testValFsVlanTunnelProtocolStp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolStp (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                  INT4 i4TestValFsVlanTunnelProtocolStp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;
    UINT1               u1Status;

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolStp != VLAN_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsVlanTunnelProtocolStp != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolStp != VLAN_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* In case of Q-in-Q Bridge mode, tunnel status must be enabled
     * for configuring the protocol tunnel status even though it
     * is peer/tunnel/discard*/
    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) &&
        (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_STP_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_STP_TUNNEL_STATUS (pVlanPortEntry) ==
        i4TestValFsVlanTunnelProtocolStp)
    {
        return SNMP_SUCCESS;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () != VLAN_TRUE)
    {
        /* Tunnel/Discard options are not allowed when the protocol status is 
         * enabled on a port. Only peer option is allowed when the protocol 
         * status is enabled on a port.
         */
        if ((i4TestValFsVlanTunnelProtocolStp == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||
            (i4TestValFsVlanTunnelProtocolStp == VLAN_TUNNEL_PROTOCOL_DISCARD))
        {
            if ((VlanL2IwfGetProtocolEnabledStatusOnPort
                 ((UINT2) (VLAN_GET_IFINDEX (u2Port)), L2_PROTO_STP, &u1Status)
                 == L2IWF_SUCCESS) && (u1Status == OSIX_ENABLED))
            {
                CLI_SET_ERR (CLI_PB_TUNNEL_PROTOCOL_ENA_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

    if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {
        if ((i4TestValFsVlanTunnelProtocolStp == VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
            (VLAN_PB_PORT_TYPE (u2Port) == VLAN_CUSTOMER_EDGE_PORT))
        {
            if (VlanPbIsTunnelingValidOnCep (u2Port) == VLAN_FALSE)
            {
                /*
                 * Protocol tunneling should not be allowed on CEPs supporting
                 * more than one PEP.
                 */
                CLI_SET_ERR (CLI_PB_TUNNEL_PEP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        /* Customer STP BPDU peering is allowed only on customer edge 
         * ports.
         */

        if ((i4TestValFsVlanTunnelProtocolStp == VLAN_TUNNEL_PROTOCOL_PEER) &&
            (VLAN_PB_PORT_TYPE (u2Port) != VLAN_CUSTOMER_EDGE_PORT))
        {
            CLI_SET_ERR (CLI_PB_STP_PEER_ERROR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolGvrp
 Input       :  The Indices
                FsVlanPort

                The Object 
                testValFsVlanTunnelProtocolGvrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolGvrp (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                   INT4 i4TestValFsVlanTunnelProtocolGvrp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;
    UINT1               u1Status;

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolGvrp != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolGvrp != VLAN_TUNNEL_PROTOCOL_DISCARD) &&
        (i4TestValFsVlanTunnelProtocolGvrp != VLAN_TUNNEL_PROTOCOL_PEER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* In case of Q-in-Q Bridge mode, tunnel status must be enabled
     * for configuring the protocol tunnel status even though it
     * is peer/tunnel/discard*/
    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) &&
        (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_GVRP_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry) ==
        i4TestValFsVlanTunnelProtocolGvrp)
    {
        return SNMP_SUCCESS;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () != VLAN_TRUE)
    {
        /* Tunnel/Discard options are not allowed when the protocol status is 
         * enabled on a port. Only peer option is allowed when the protocol 
         * status is enabled on a port.
         */
        if ((VlanL2IwfGetProtocolEnabledStatusOnPort
             ((UINT2) (VLAN_GET_IFINDEX (u2Port)), L2_PROTO_GVRP, &u1Status) ==
             L2IWF_SUCCESS) && (u1Status == OSIX_ENABLED))
        {
            CLI_SET_ERR (CLI_PB_TUNNEL_PROTOCOL_ENA_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {
        /* Customer Gvrp peering is not supported. */
        if (i4TestValFsVlanTunnelProtocolGvrp == VLAN_TUNNEL_PROTOCOL_PEER)
        {
            CLI_SET_ERR (CLI_PB_GVRP_PEER_ERROR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if ((i4TestValFsVlanTunnelProtocolGvrp == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (VLAN_PB_PORT_TYPE (u2Port) == VLAN_CUSTOMER_EDGE_PORT))
        {
            if (VlanPbIsTunnelingValidOnCep (u2Port) == VLAN_FALSE)
            {
                /*
                 * Protocol tunneling should not be allowed on CEPs supporting
                 * more than one PEP.
                 */
                CLI_SET_ERR (CLI_PB_TUNNEL_PEP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolMvrp
 Input       :  The Indices
                FsVlanPort

                The Object
                testValFsVlanTunnelProtocolMvrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolMvrp (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                   INT4 i4TestValFsVlanTunnelProtocolMvrp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;
    UINT1               u1Status;

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolMvrp != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolMvrp != VLAN_TUNNEL_PROTOCOL_DISCARD) &&
        (i4TestValFsVlanTunnelProtocolMvrp != VLAN_TUNNEL_PROTOCOL_PEER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* In case of Q-in-Q Bridge mode, tunnel status must be enabled
     * for configuring the protocol tunnel status even though it
     * is peer/tunnel/discard*/
    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) &&
        (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_MVRP_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry) ==
        i4TestValFsVlanTunnelProtocolMvrp)
    {
        return SNMP_SUCCESS;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () != VLAN_TRUE)
    {
        /* Tunnel/Discard options are not allowed when the protocol status is
         * enabled on a port. Only peer option is allowed when the protocol
         * status is enabled on a port.
         */
        if ((VlanL2IwfGetProtocolEnabledStatusOnPort
             ((UINT2) (VLAN_GET_IFINDEX (u2Port)), L2_PROTO_MVRP, &u1Status) ==
             L2IWF_SUCCESS) && (u1Status == OSIX_ENABLED))
        {
            CLI_SET_ERR (CLI_PB_TUNNEL_PROTOCOL_ENA_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {
        /* Customer Mvrp peering is not supported. */
        if (i4TestValFsVlanTunnelProtocolMvrp == VLAN_TUNNEL_PROTOCOL_PEER)
        {
            CLI_SET_ERR (CLI_PB_MVRP_PEER_ERROR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if ((i4TestValFsVlanTunnelProtocolMvrp == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (VLAN_PB_PORT_TYPE (u2Port) == VLAN_CUSTOMER_EDGE_PORT))
        {
            if (VlanPbIsTunnelingValidOnCep (u2Port) == VLAN_FALSE)
            {
                /*
                 * Protocol tunneling should not be allowed on CEPs supporting
                 * more than one PEP.
                 */
                CLI_SET_ERR (CLI_PB_TUNNEL_PEP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolGmrp
 Input       :  The Indices
                FsVlanPort

                The Object 
                testValFsVlanTunnelProtocolGmrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolGmrp (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                   INT4 i4TestValFsVlanTunnelProtocolGmrp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;
    UINT1               u1Status;

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolGmrp != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolGmrp != VLAN_TUNNEL_PROTOCOL_DISCARD) &&
        (i4TestValFsVlanTunnelProtocolGmrp != VLAN_TUNNEL_PROTOCOL_PEER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* In case of Q-in-Q Bridge mode, tunnel status must be enabled
     * for configuring the protocol tunnel status even though it
     * is peer/tunnel/discard*/
    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) &&
        (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_GMRP_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry) ==
        i4TestValFsVlanTunnelProtocolGmrp)
    {
        return SNMP_SUCCESS;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () != VLAN_TRUE)
    {
        /* Tunnel/Discard options are not allowed when the protocol status is 
         * enabled on a port. Only peer option is allowed when the protocol 
         * status is enabled on a port.
         */
        if ((VlanL2IwfGetProtocolEnabledStatusOnPort
             ((UINT2) (VLAN_GET_IFINDEX (u2Port)), L2_PROTO_GMRP, &u1Status) ==
             L2IWF_SUCCESS) && (u1Status == OSIX_ENABLED))

        {
            CLI_SET_ERR (CLI_PB_TUNNEL_PROTOCOL_ENA_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {
        /* Customer Gmrp peering is not supported. */
        if (i4TestValFsVlanTunnelProtocolGmrp == VLAN_TUNNEL_PROTOCOL_PEER)
        {
            CLI_SET_ERR (CLI_PB_GMRP_PEER_ERROR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if ((i4TestValFsVlanTunnelProtocolGmrp == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (VLAN_PB_PORT_TYPE (u2Port) == VLAN_CUSTOMER_EDGE_PORT))
        {
            if (VlanPbIsTunnelingValidOnCep (u2Port) == VLAN_FALSE)
            {
                /*
                 * Protocol tunneling should not be allowed on CEPs supporting
                 * more than one PEP.
                 */
                CLI_SET_ERR (CLI_PB_TUNNEL_PEP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolMmrp
 Input       :  The Indices
                FsVlanPort

                The Object
                testValFsVlanTunnelProtocolMmrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolMmrp (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                   INT4 i4TestValFsVlanTunnelProtocolMmrp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;
    UINT1               u1Status;

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolMmrp != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolMmrp != VLAN_TUNNEL_PROTOCOL_DISCARD) &&
        (i4TestValFsVlanTunnelProtocolMmrp != VLAN_TUNNEL_PROTOCOL_PEER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* In case of Q-in-Q Bridge mode, tunnel status must be enabled
     * for configuring the protocol tunnel status even though it
     * is peer/tunnel/discard*/
    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) &&
        (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_MMRP_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry) ==
        i4TestValFsVlanTunnelProtocolMmrp)
    {
        return SNMP_SUCCESS;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () != VLAN_TRUE)
    {
        /* Tunnel/Discard options are not allowed when the protocol status is
         * enabled on a port. Only peer option is allowed when the protocol
         * status is enabled on a port.
         */
        if ((VlanL2IwfGetProtocolEnabledStatusOnPort
             ((UINT2) (VLAN_GET_IFINDEX (u2Port)), L2_PROTO_MMRP, &u1Status) ==
             L2IWF_SUCCESS) && (u1Status == OSIX_ENABLED))

        {
            CLI_SET_ERR (CLI_PB_TUNNEL_PROTOCOL_ENA_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {
        /* Customer Mmrp peering is not supported. */
        if (i4TestValFsVlanTunnelProtocolMmrp == VLAN_TUNNEL_PROTOCOL_PEER)
        {
            CLI_SET_ERR (CLI_PB_MMRP_PEER_ERROR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if ((i4TestValFsVlanTunnelProtocolMmrp == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (VLAN_PB_PORT_TYPE (u2Port) == VLAN_CUSTOMER_EDGE_PORT))
        {
            if (VlanPbIsTunnelingValidOnCep (u2Port) == VLAN_FALSE)
            {
                /*
                 * Protocol tunneling should not be allowed on CEPs supporting
                 * more than one PEP.
                 */
                CLI_SET_ERR (CLI_PB_TUNNEL_PEP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolElmi
 Input       :  The Indices
                FsVlanPort

                The Object 
                testValFsVlanTunnelProtocolElmi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolElmi (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                   INT4 i4TestValFsVlanTunnelProtocolElmi)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pPortPbEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;
    pPortPbEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolElmi != VLAN_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsVlanTunnelProtocolElmi != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolElmi != VLAN_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* In case of Q-in-Q Bridge mode, tunnel status must be enabled
     * for configuring the protocol tunnel status even though it
     * is peer/tunnel/discard*/
    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) &&
        (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_ELMI_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pPortPbEntry != NULL)
    {
        if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
            (((pPortPbEntry->u1PbPortType) == VLAN_PROVIDER_NETWORK_PORT) ||
             ((pPortPbEntry->u1PbPortType) == VLAN_CNP_TAGGED_PORT)))
        {
            /*In case of 1AD Bridges, Protocol tunnel status are not
             * allowed to configure on network ports*/
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry) ==
        i4TestValFsVlanTunnelProtocolElmi)
    {
        return SNMP_SUCCESS;
    }

    if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {
        if (pPortPbEntry != NULL)
        {
            if ((i4TestValFsVlanTunnelProtocolElmi ==
                 VLAN_TUNNEL_PROTOCOL_TUNNEL)
                && ((pPortPbEntry->u1PbPortType) == VLAN_CUSTOMER_EDGE_PORT))
            {
                /*
                 * Protocol tunneling should not be allowed on CEPs supporting
                 * more than one PEP.
                 */
                if (VlanPbIsTunnelingValidOnCep (u2Port) == VLAN_FALSE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolLldp
 Input       :  The Indices
                FsVlanPort

                The Object 
                testValFsVlanTunnelProtocolLldp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolLldp (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                   INT4 i4TestValFsVlanTunnelProtocolLldp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPbPortEntry   *pPortPbEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;
    pPortPbEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolLldp != VLAN_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsVlanTunnelProtocolLldp != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolLldp != VLAN_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* In case of Q-in-Q Bridge mode, tunnel status must be enabled
     * for configuring the protocol tunnel status even though it
     * is peer/tunnel/discard*/
    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) &&
        (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_LLDP_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pPortPbEntry != NULL)
    {
        if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
            (((pPortPbEntry->u1PbPortType) == VLAN_PROVIDER_NETWORK_PORT) ||
             ((pPortPbEntry->u1PbPortType) == VLAN_CNP_TAGGED_PORT)))
        {
            /*In case of 1AD Bridges, Protocol tunnel status are not
             * allowed to configure on network ports*/
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry) ==
        i4TestValFsVlanTunnelProtocolLldp)
    {
        return SNMP_SUCCESS;
    }

    if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {
        if (pPortPbEntry != NULL)
        {
            if ((i4TestValFsVlanTunnelProtocolLldp ==
                 VLAN_TUNNEL_PROTOCOL_TUNNEL)
                && ((pPortPbEntry->u1PbPortType) == VLAN_CUSTOMER_EDGE_PORT))
            {
                /*
                 * Protocol tunneling should not be allowed on CEPs supporting
                 * more than one PEP.
                 */
                if (VlanPbIsTunnelingValidOnCep (u2Port) == VLAN_FALSE)
                {
                    CLI_SET_ERR (CLI_PB_TUNNEL_PEP_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
        }
    }
     if (pVlanPortEntry->u1IfType == VLAN_LAGG_INTERFACE_TYPE)
    {
        CLI_SET_ERR (CLI_PB_LLDP_LAGG_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolIgmp
 Input       :  The Indices
                FsVlanPort

                The Object 
                testValFsVlanTunnelProtocolIgmp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolIgmp (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                   INT4 i4TestValFsVlanTunnelProtocolIgmp)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PB_VLAN_NOT_ACTIVE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolIgmp != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolIgmp != VLAN_TUNNEL_PROTOCOL_DISCARD) &&
        (i4TestValFsVlanTunnelProtocolIgmp != VLAN_TUNNEL_PROTOCOL_PEER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        CLI_SET_ERR (CLI_PB_INVALID_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* Protocol Tunnel/Peer/Discard options are not allowed for 
     * - Provider Network Port
     * - Proprietary provider network port
     * - Customer network port supporting S-Tagged service
     */

    /* In case of Q-in-Q Bridge mode, tunnel status must be enabled
     * for configuring the protocol tunnel status even though it
     * is peer/tunnel/discard*/
    if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE) &&
        (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
    {
        CLI_SET_ERR (CLI_PB_INVALID_TUNNEL_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_IGMP_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry) ==
        i4TestValFsVlanTunnelProtocolIgmp)
    {
        return SNMP_SUCCESS;
    }

    if (VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ())
        == SNOOP_ENABLED)
    {
        CLI_SET_ERR (CLI_PB_IGMP_TUNNEL_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {
        if (i4TestValFsVlanTunnelProtocolIgmp == VLAN_TUNNEL_PROTOCOL_PEER)
        {
            CLI_SET_ERR (CLI_PB_IGMP_PEER_ERROR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if ((i4TestValFsVlanTunnelProtocolIgmp == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (VLAN_PB_PORT_TYPE (u2Port) == VLAN_CUSTOMER_EDGE_PORT))
        {
            if (VlanPbIsTunnelingValidOnCep (u2Port) == VLAN_FALSE)
            {
                /*
                 * Protocol tunneling should not be allowed on CEPs supporting
                 * more than one PEP.
                 */
                CLI_SET_ERR (CLI_PB_TUNNEL_PEP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolEcfm
 Input       :  The Indices
                FsVlanPort

                The Object 
                testValFsVlanTunnelProtocolEcfm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolEcfm (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                   INT4 i4TestValFsVlanTunnelProtocolEcfm)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolEcfm != VLAN_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsVlanTunnelProtocolEcfm != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolEcfm != VLAN_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_ECFM_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
   
    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry) ==
        i4TestValFsVlanTunnelProtocolEcfm)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelOverrideOption
 Input       :  The Indices
                FsVlanPort

                The Object
                testValFsVlanTunnelOverrideOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelOverrideOption (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                     INT4 i4TestValFsVlanTunnelOverrideOption)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port = VLAN_ZERO;

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelOverrideOption != VLAN_OVERRIDE_OPTION_ENABLE) &&
        (i4TestValFsVlanTunnelOverrideOption != VLAN_OVERRIDE_OPTION_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVlanTunnelProtocolEoam
 Input       :  The Indices
                FsVlanPort

                The Object
                testValFsVlanTunnelProtocolEoam
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVlanTunnelProtocolEoam (UINT4 *pu4ErrorCode, INT4 i4FsVlanPort,
                                   INT4 i4TestValFsVlanTunnelProtocolEoam)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVlanTunnelProtocolEoam != VLAN_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsVlanTunnelProtocolEoam != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsVlanTunnelProtocolEoam != VLAN_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
        (VLAN_PB_PROTO_TUNNEL_PORT (u2Port) != VLAN_TRUE))
    {
        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to configure on network ports*/
        CLI_SET_ERR (CLI_PB_EOAM_NETWORK_PORT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    
    if (pVlanPortEntry->u1IfType == VLAN_LAGG_INTERFACE_TYPE)
    {
        CLI_SET_ERR (CLI_PB_EOAM_LAGG_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }          


    if (VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry) ==
        i4TestValFsVlanTunnelProtocolEoam)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVlanTunnelProtocolTable
 Input       :  The Indices
                FsVlanPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVlanTunnelProtocolTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVlanTunnelProtocolStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVlanTunnelProtocolStatsTable
 Input       :  The Indices
                FsVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVlanTunnelProtocolStatsTable (INT4 i4FsVlanPort)
{
    INT1                i1ReVal;

    i1ReVal = nmhValidateIndexInstanceFsVlanTunnelProtocolTable (i4FsVlanPort);

    return i1ReVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVlanTunnelProtocolStatsTable
 Input       :  The Indices
                FsVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVlanTunnelProtocolStatsTable (INT4 *pi4FsVlanPort)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsVlanTunnelProtocolTable (0, pi4FsVlanPort));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVlanTunnelProtocolStatsTable
 Input       :  The Indices
                FsVlanPort
                nextFsVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVlanTunnelProtocolStatsTable (INT4 i4FsVlanPort,
                                               INT4 *pi4NextFsVlanPort)
{

    return (nmhGetNextIndexFsVlanTunnelProtocolTable
            (i4FsVlanPort, pi4NextFsVlanPort));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolDot1xPktsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolDot1xPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolDot1xPktsRecvd (INT4 i4FsVlanPort,
                                          UINT4
                                          *pu4RetValFsVlanTunnelProtocolDot1xPktsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolDot1xPktsRecvd =
            VLAN_TUNNEL_DOT1X_PKTS_RX (pVlanPortEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolDot1xPktsSent
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolDot1xPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolDot1xPktsSent (INT4 i4FsVlanPort,
                                         UINT4
                                         *pu4RetValFsVlanTunnelProtocolDot1xPktsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolDot1xPktsSent = VLAN_TUNNEL_DOT1X_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolLacpPktsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolLacpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolLacpPktsRecvd (INT4 i4FsVlanPort,
                                         UINT4
                                         *pu4RetValFsVlanTunnelProtocolLacpPktsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolLacpPktsRecvd = VLAN_TUNNEL_LACP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolLacpPktsSent
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolLacpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolLacpPktsSent (INT4 i4FsVlanPort,
                                        UINT4
                                        *pu4RetValFsVlanTunnelProtocolLacpPktsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolLacpPktsSent = VLAN_TUNNEL_LACP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolStpPDUsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolStpPDUsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolStpPDUsRecvd (INT4 i4FsVlanPort,
                                        UINT4
                                        *pu4RetValFsVlanTunnelProtocolStpPDUsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;

    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolStpPDUsRecvd = VLAN_TUNNEL_STP_BPDUS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolStpPDUsSent
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolStpPDUsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolStpPDUsSent (INT4 i4FsVlanPort,
                                       UINT4
                                       *pu4RetValFsVlanTunnelProtocolStpPDUsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolStpPDUsSent = VLAN_TUNNEL_STP_BPDUS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolGvrpPDUsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolGvrpPDUsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolGvrpPDUsRecvd (INT4 i4FsVlanPort,
                                         UINT4
                                         *pu4RetValFsVlanTunnelProtocolGvrpPDUsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolGvrpPDUsRecvd = VLAN_TUNNEL_GVRP_PDUS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolMvrpPktsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanTunnelProtocolMvrpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolMvrpPktsRecvd (INT4 i4FsVlanPort,
                                         UINT4
                                         *pu4RetValFsVlanTunnelProtocolMvrpPktsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolMvrpPktsRecvd = VLAN_TUNNEL_MVRP_PDUS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolGvrpPDUsSent
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolGvrpPDUsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolGvrpPDUsSent (INT4 i4FsVlanPort,
                                        UINT4
                                        *pu4RetValFsVlanTunnelProtocolGvrpPDUsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;

    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolGvrpPDUsSent = VLAN_TUNNEL_GVRP_PDUS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function     :  nmhGetFsVlanTunnelProtocolMvrpPktsSent
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanTunnelProtocolMvrpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolMvrpPktsSent (INT4 i4FsVlanPort,
                                        UINT4
                                        *pu4RetValFsVlanTunnelProtocolMvrpPktsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;

    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolMvrpPktsSent = VLAN_TUNNEL_MVRP_PDUS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/**************************************************************************** 
 Function    :  nmhGetFsVlanTunnelProtocolGmrpPktsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolGmrpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolGmrpPktsRecvd (INT4 i4FsVlanPort,
                                         UINT4
                                         *pu4RetValFsVlanTunnelProtocolGmrpPktsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolGmrpPktsRecvd = VLAN_TUNNEL_GMRP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolMmrpPktsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanTunnelProtocolMmrpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolMmrpPktsRecvd (INT4 i4FsVlanPort,
                                         UINT4
                                         *pu4RetValFsVlanTunnelProtocolMmrpPktsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolMmrpPktsRecvd = VLAN_TUNNEL_MMRP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolGmrpPktsSent
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolGmrpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolGmrpPktsSent (INT4 i4FsVlanPort,
                                        UINT4
                                        *pu4RetValFsVlanTunnelProtocolGmrpPktsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolGmrpPktsSent = VLAN_TUNNEL_GMRP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolMmrpPktsSent
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanTunnelProtocolMmrpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolMmrpPktsSent (INT4 i4FsVlanPort,
                                        UINT4
                                        *pu4RetValFsVlanTunnelProtocolMmrpPktsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolMmrpPktsSent = VLAN_TUNNEL_MMRP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/**************************************************************************** 
 Function    :  nmhGetFsVlanTunnelProtocolIgmpPktsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolIgmpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolIgmpPktsRecvd (INT4 i4FsVlanPort,
                                         UINT4
                                         *pu4RetValFsVlanTunnelProtocolIgmpPktsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolIgmpPktsRecvd = VLAN_TUNNEL_IGMP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolIgmpPktsSent
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolIgmpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolIgmpPktsSent (INT4 i4FsVlanPort,
                                        UINT4
                                        *pu4RetValFsVlanTunnelProtocolIgmpPktsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolIgmpPktsSent = VLAN_TUNNEL_IGMP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/**************************************************************************** 
 Function    :  nmhGetFsVlanTunnelProtocolElmiPktsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolElmiPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolElmiPktsRecvd (INT4 i4FsVlanPort,
                                         UINT4
                                         *pu4RetValFsVlanTunnelProtocolElmiPktsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolElmiPktsRecvd = VLAN_TUNNEL_ELMI_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolElmiPktsSent
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolElmiPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolElmiPktsSent (INT4 i4FsVlanPort,
                                        UINT4
                                        *pu4RetValFsVlanTunnelProtocolElmiPktsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolElmiPktsSent = VLAN_TUNNEL_ELMI_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/**************************************************************************** 
 Function    :  nmhGetFsVlanTunnelProtocolLldpPktsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolLldpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolLldpPktsRecvd (INT4 i4FsVlanPort,
                                         UINT4
                                         *pu4RetValFsVlanTunnelProtocolLldpPktsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolLldpPktsRecvd = VLAN_TUNNEL_LLDP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolLldpPktsSent
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolLldpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolLldpPktsSent (INT4 i4FsVlanPort,
                                        UINT4
                                        *pu4RetValFsVlanTunnelProtocolLldpPktsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolLldpPktsSent = VLAN_TUNNEL_LLDP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/**************************************************************************** 
 Function    :  nmhGetFsVlanTunnelProtocolEcfmPktsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolEcfmPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolEcfmPktsRecvd (INT4 i4FsVlanPort,
                                         UINT4
                                         *pu4RetValFsVlanTunnelProtocolEcfmPktsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolEcfmPktsRecvd = VLAN_TUNNEL_ECFM_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolEcfmPktsSent
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanTunnelProtocolEcfmPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolEcfmPktsSent (INT4 i4FsVlanPort,
                                        UINT4
                                        *pu4RetValFsVlanTunnelProtocolEcfmPktsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolEcfmPktsSent = VLAN_TUNNEL_ECFM_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolEoamPktsRecvd
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanTunnelProtocolEoamPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolEoamPktsRecvd (INT4 i4FsVlanPort,
                                         UINT4
                                         *pu4RetValFsVlanTunnelProtocolEoamPktsRecvd)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolEoamPktsRecvd = VLAN_TUNNEL_EOAM_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsVlanTunnelProtocolEoamPktsSent
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanTunnelProtocolEoamPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanTunnelProtocolEoamPktsSent (INT4 i4FsVlanPort,
                                        UINT4
                                        *pu4RetValFsVlanTunnelProtocolEoamPktsSent)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Here the VLAN bridge mode is not checked under the assumption
     * that pVlanPbPortEntry will not even be initialised in
     * Customer or Provider bridge mode.
     */

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanTunnelProtocolEoamPktsSent = VLAN_TUNNEL_EOAM_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsVlanDiscardStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVlanDiscardStatsTable
 Input       :  The Indices
                FsVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVlanDiscardStatsTable (INT4 i4FsVlanPort)
{
    UINT2               u2Port;
    tVlanPortEntry     *pVlanPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) i4FsVlanPort;

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* In case of Customer bridges, all ports will be considered.
     * In case of other bridge modes, only tunnel ports will be
     * considered */
    if ((VLAN_BRIDGE_MODE () != VLAN_CUSTOMER_BRIDGE_MODE) &&
        (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVlanDiscardStatsTable
 Input       :  The Indices
                FsVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVlanDiscardStatsTable (INT4 *pi4FsVlanPort)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexFsVlanDiscardStatsTable (0, pi4FsVlanPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVlanDiscardStatsTable
 Input       :  The Indices
                FsVlanPort
                nextFsVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVlanDiscardStatsTable (INT4 i4FsVlanPort,
                                        INT4 *pi4NextFsVlanPort)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (i4FsVlanPort < 0 || i4FsVlanPort > VLAN_MAX_PORTS)
    {
        return SNMP_FAILURE;
    }

    for (u2Port = (UINT2) (i4FsVlanPort + 1); u2Port <= VLAN_MAX_PORTS;
         u2Port++)
    {
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pVlanPortEntry != NULL)
        {
            /* In case of Customer bridges, all ports will be considered.
             * In case of other bridge modes, only tunnel ports will be
             * considered */
            if ((VLAN_BRIDGE_MODE () != VLAN_CUSTOMER_BRIDGE_MODE) &&
                (VLAN_TUNNEL_STATUS (pVlanPortEntry) != VLAN_ENABLED))
            {
                continue;
            }
            *pi4NextFsVlanPort = (INT4) u2Port;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardDot1xPktsRx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardDot1xPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardDot1xPktsRx (INT4 i4FsVlanPort,
                                UINT4 *pu4RetValFsVlanDiscardDot1xPktsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardDot1xPktsRx = VLAN_DISCARD_DOT1X_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardDot1xPktsTx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardDot1xPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardDot1xPktsTx (INT4 i4FsVlanPort,
                                UINT4 *pu4RetValFsVlanDiscardDot1xPktsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardDot1xPktsTx = VLAN_DISCARD_DOT1X_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardLacpPktsRx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardLacpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardLacpPktsRx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardLacpPktsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardLacpPktsRx = VLAN_DISCARD_LACP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardLacpPktsTx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardLacpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardLacpPktsTx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardLacpPktsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardLacpPktsTx = VLAN_DISCARD_LACP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardStpPDUsRx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardStpPDUsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardStpPDUsRx (INT4 i4FsVlanPort,
                              UINT4 *pu4RetValFsVlanDiscardStpPDUsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardStpPDUsRx = VLAN_DISCARD_STP_BPDUS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardStpPDUsTx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardStpPDUsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardStpPDUsTx (INT4 i4FsVlanPort,
                              UINT4 *pu4RetValFsVlanDiscardStpPDUsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardStpPDUsTx = VLAN_DISCARD_STP_BPDUS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardGvrpPktsRx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardGvrpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardGvrpPktsRx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardGvrpPktsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardGvrpPktsRx = VLAN_DISCARD_GVRP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardMvrpPktsRx
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanDiscardMvrpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardMvrpPktsRx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardMvrpPktsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardMvrpPktsRx = VLAN_DISCARD_MVRP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/**************************************************************************** 
 Function    :  nmhGetFsVlanDiscardGvrpPktsTx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardGvrpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardGvrpPktsTx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardGvrpPktsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardGvrpPktsTx = VLAN_DISCARD_GVRP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardMvrpPktsTx
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanDiscardMvrpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardMvrpPktsTx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardMvrpPktsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardMvrpPktsTx = VLAN_DISCARD_MVRP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardGmrpPktsRx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardGmrpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardGmrpPktsRx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardGmrpPktsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardGmrpPktsRx = VLAN_DISCARD_GMRP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsVlanDiscardMmrpPktsRx
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanDiscardMmrpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardMmrpPktsRx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardMmrpPktsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardMmrpPktsRx = VLAN_DISCARD_MMRP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/**************************************************************************** 
 Function    :  nmhGetFsVlanDiscardGmrpPktsTx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardGmrpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardGmrpPktsTx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardGmrpPktsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardGmrpPktsTx = VLAN_DISCARD_GMRP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsVlanDiscardMmrpPktsTx
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanDiscardMmrpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardMmrpPktsTx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardMmrpPktsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardMmrpPktsTx = VLAN_DISCARD_MMRP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/**************************************************************************** 
 Function    :  nmhGetFsVlanDiscardIgmpPktsRx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardIgmpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardIgmpPktsRx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardIgmpPktsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardIgmpPktsRx = VLAN_DISCARD_IGMP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardIgmpPktsTx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardIgmpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardIgmpPktsTx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardIgmpPktsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardIgmpPktsTx = VLAN_DISCARD_IGMP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/**************************************************************************** 
 Function    :  nmhGetFsVlanDiscardElmiPktsRx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardElmiPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardElmiPktsRx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardElmiPktsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardElmiPktsRx = VLAN_DISCARD_ELMI_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardElmiPktsTx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardElmiPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardElmiPktsTx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardElmiPktsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardElmiPktsTx = VLAN_DISCARD_ELMI_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/**************************************************************************** 
 Function    :  nmhGetFsVlanDiscardLldpPktsRx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardLldpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardLldpPktsRx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardLldpPktsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardLldpPktsRx = VLAN_DISCARD_LLDP_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardLldpPktsTx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardLldpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardLldpPktsTx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardLldpPktsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardLldpPktsTx = VLAN_DISCARD_LLDP_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/**************************************************************************** 
 Function    :  nmhGetFsVlanDiscardEcfmPktsRx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardEcfmPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardEcfmPktsRx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardEcfmPktsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardEcfmPktsRx = VLAN_DISCARD_ECFM_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardEcfmPktsTx
 Input       :  The Indices
                FsVlanPort

                The Object 
                retValFsVlanDiscardEcfmPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardEcfmPktsTx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardEcfmPktsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardEcfmPktsTx = VLAN_DISCARD_ECFM_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardEoamPktsRx
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanDiscardEoamPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardEoamPktsRx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardEoamPktsRx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardEoamPktsRx = VLAN_DISCARD_EOAM_PKTS_RX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsVlanDiscardEoamPktsTx
 Input       :  The Indices
                FsVlanPort

                The Object
                retValFsVlanDiscardEoamPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVlanDiscardEoamPktsTx (INT4 i4FsVlanPort,
                               UINT4 *pu4RetValFsVlanDiscardEoamPktsTx)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT2               u2Port;

    u2Port = (UINT2) i4FsVlanPort;

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pVlanPortEntry != NULL)
    {
        *pu4RetValFsVlanDiscardEoamPktsTx = VLAN_DISCARD_EOAM_PKTS_TX
            (pVlanPortEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsServiceVlanTunnelProtocolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsServiceVlanTunnelProtocolTable
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsServiceVlanTunnelProtocolTable (INT4
                                                          i4FsServiceVlanId,
                                                          INT4
                                                          i4FsServiceProtocolEnum)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = SNMP_FAILURE;
    UINT2               u2Protocol = SNMP_FAILURE;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u2Protocol = (UINT2) i4FsServiceProtocolEnum;

    if (u2Protocol > L2_PROTO_MAX)
    {
        return SNMP_FAILURE;
    }
    else if (L2_PROTO_CHECK (u2Protocol) == L2IWF_TRUE)
    {
        if (!(u2Protocol > L2_PROTO_OTHER) && (u2Protocol < L2_PROTO_MAX))
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsServiceVlanTunnelProtocolTable
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsServiceVlanTunnelProtocolTable (INT4 *pi4FsServiceVlanId,
                                                  INT4
                                                  *pi4FsServiceProtocolEnum)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               VlanStartId = VLAN_ZERO;

    VlanStartId = VLAN_START_VLAN_INDEX ();
    pVlanCurrEntry = VlanGetVlanEntry (VlanStartId);

    if (pVlanCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4FsServiceVlanId = pVlanCurrEntry->VlanId;
    *pi4FsServiceProtocolEnum = L2_PROTO_DOT1X;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsServiceVlanTunnelProtocolTable
 Input       :  The Indices
                FsServiceVlanId
                nextFsServiceVlanId
                FsServiceProtocolEnum
                nextFsServiceProtocolEnum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsServiceVlanTunnelProtocolTable (INT4 i4FsServiceVlanId,
                                                 INT4 *pi4NextFsServiceVlanId,
                                                 INT4 i4FsServiceProtocolEnum,
                                                 INT4
                                                 *pi4NextFsServiceProtocolEnum)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2NextVlanId = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);
    if (pVlanCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FsServiceProtocolEnum != L2_PROTO_PTP)
    {
        *pi4NextFsServiceVlanId = pVlanCurrEntry->VlanId;

        if (i4FsServiceProtocolEnum == L2_PROTO_DOT1X)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
        else if (i4FsServiceProtocolEnum == L2_PROTO_LACP)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
        else if (i4FsServiceProtocolEnum == L2_PROTO_STP)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
        else if (i4FsServiceProtocolEnum == L2_PROTO_GVRP)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
        else if (i4FsServiceProtocolEnum == L2_PROTO_GMRP)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
        else if (i4FsServiceProtocolEnum == L2_PROTO_IGMP)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
        else if (i4FsServiceProtocolEnum == L2_PROTO_MVRP)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
        else if (i4FsServiceProtocolEnum == L2_PROTO_MMRP)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
        else if (i4FsServiceProtocolEnum == L2_PROTO_EOAM)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
        else if (i4FsServiceProtocolEnum == L2_PROTO_ELMI)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
        else if (i4FsServiceProtocolEnum == L2_PROTO_LLDP)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
        else if (i4FsServiceProtocolEnum == L2_PROTO_ECFM)
        {
            *pi4NextFsServiceProtocolEnum = i4FsServiceProtocolEnum + 1;
            return SNMP_SUCCESS;
        }
    }
    else
    {
        u2NextVlanId = VLAN_GET_NEXT_VLAN_INDEX (u2VlanId);

        pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2NextVlanId);
        if (pVlanCurrEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        *pi4NextFsServiceVlanId = pVlanCurrEntry->VlanId;
        *pi4NextFsServiceProtocolEnum = L2_PROTO_DOT1X;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsServiceVlanRsvdMacaddress
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                retValFsServiceVlanRsvdMacaddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsServiceVlanRsvdMacaddress (INT4 i4FsServiceVlanId,
                                   INT4 i4FsServiceProtocolEnum,
                                   tMacAddr *
                                   pRetValFsServiceVlanRsvdMacaddress)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);
    if (pVlanCurrEntry != NULL)
    {
        u2Protocol = (UINT2) i4FsServiceProtocolEnum;
        switch (u2Protocol)
        {
            case L2_PROTO_DOT1X:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_DOT1X_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_LACP:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_LACP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_STP:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_STP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_GVRP:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_GVRP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_GMRP:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_GMRP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_IGMP:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_IGMP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_MVRP:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_MVRP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_MMRP:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_MMRP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_ELMI:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_ELMI_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_LLDP:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_LLDP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_ECFM:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_ECFM_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_EOAM:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_EOAM_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_PTP:
                MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                        VLAN_RSVD_PTP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            default:
                break;
        }
        if ((u2Protocol >= L2_PROTO_OTHER) && (u2Protocol <= L2_PROTO_MAX))
        {
            MEMCPY (*pRetValFsServiceVlanRsvdMacaddress,
                    VLAN_RSVD_OTHER_ADDR_PER_VLAN (pVlanCurrEntry,
                                                   (u2Protocol -
                                                    L2_PROTO_OTHER)),
                    ETHERNET_ADDR_SIZE);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsServiceVlanTunnelMacaddress
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                retValFsServiceVlanTunnelMacaddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsServiceVlanTunnelMacaddress (INT4 i4FsServiceVlanId,
                                     INT4 i4FsServiceProtocolEnum,
                                     tMacAddr *
                                     pRetValFsServiceVlanTunnelMacaddress)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);
    if (pVlanCurrEntry != NULL)
    {
        u2Protocol = (UINT2) i4FsServiceProtocolEnum;
        switch (u2Protocol)
        {
            case L2_PROTO_DOT1X:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_DOT1X_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_LACP:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_LACP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_STP:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_STP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_GVRP:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_GVRP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_GMRP:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_GMRP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_IGMP:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_IGMP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_MVRP:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_MVRP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_MMRP:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_MMRP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_ELMI:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_ELMI_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_LLDP:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_LLDP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_ECFM:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_ECFM_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_EOAM:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_EOAM_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            case L2_PROTO_PTP:
                MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                        VLAN_TUNNEL_PTP_ADDR_PER_VLAN (pVlanCurrEntry),
                        ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            default:
                break;
        }
        if ((u2Protocol >= L2_PROTO_OTHER) && (u2Protocol <= L2_PROTO_MAX))
        {
            MEMCPY (*pRetValFsServiceVlanTunnelMacaddress,
                    VLAN_TUNNEL_OTHER_ADDR_PER_VLAN (pVlanCurrEntry,
                                                     (u2Protocol -
                                                      L2_PROTO_OTHER)),
                    ETHERNET_ADDR_SIZE);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsServiceVlanTunnelProtocolStatus
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                retValFsServiceVlanTunnelProtocolStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsServiceVlanTunnelProtocolStatus (INT4 i4FsServiceVlanId,
                                         INT4 i4FsServiceProtocolEnum,
                                         INT4
                                         *pi4RetValFsServiceVlanTunnelProtocolStatus)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry != NULL)
    {
        u2Protocol = (UINT2) i4FsServiceProtocolEnum;

        switch (u2Protocol)
        {
            case L2_PROTO_ELMI:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_ELMI_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_LLDP:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_LLDP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_DOT1X:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_DOT1X_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_LACP:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_LACP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_STP:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_STP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_GVRP:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_GVRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_GMRP:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_GMRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_MVRP:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_MVRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_MMRP:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_MMRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_IGMP:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_IGMP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_ECFM:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_ECFM_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_EOAM:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_EOAM_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_PTP:
                *pi4RetValFsServiceVlanTunnelProtocolStatus =
                    VLAN_PTP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            default:
                break;
        }

        if ((u2Protocol >= L2_PROTO_OTHER) && (u2Protocol <= L2_PROTO_MAX))
        {
            *pi4RetValFsServiceVlanTunnelProtocolStatus =
                VLAN_OTHER_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry,
                                                   (u2Protocol -
                                                    L2_PROTO_OTHER));
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsServiceVlanTunnelPktsRecvd
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                retValFsServiceVlanTunnelPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsServiceVlanTunnelPktsRecvd (INT4 i4FsServiceVlanId,
                                    INT4 i4FsServiceProtocolEnum,
                                    UINT4
                                    *pu4RetValFsServiceVlanTunnelPktsRecvd)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry != NULL)
    {
        u2Protocol = (UINT2) i4FsServiceProtocolEnum;

        switch (u2Protocol)
        {
            case L2_PROTO_ELMI:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_ELMI_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_LLDP:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_LLDP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_DOT1X:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_DOT1X_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_LACP:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_LACP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_STP:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_STP_BPDUS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_GVRP:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_GVRP_PDUS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_GMRP:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_GMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_MVRP:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_MVRP_PDUS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_MMRP:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_MMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_IGMP:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_IGMP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_ECFM:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_ECFM_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_EOAM:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_EOAM_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_PTP:
                *pu4RetValFsServiceVlanTunnelPktsRecvd =
                    VLAN_TUNNEL_PTP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            default:
                break;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsServiceVlanTunnelPktsSent
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                retValFsServiceVlanTunnelPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsServiceVlanTunnelPktsSent (INT4 i4FsServiceVlanId,
                                   INT4 i4FsServiceProtocolEnum,
                                   UINT4 *pu4RetValFsServiceVlanTunnelPktsSent)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry != NULL)
    {
        u2Protocol = (UINT2) i4FsServiceProtocolEnum;

        switch (u2Protocol)
        {
            case L2_PROTO_ELMI:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_DOT1X_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_LLDP:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_LLDP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_DOT1X:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_DOT1X_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_LACP:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_LACP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_STP:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_STP_BPDUS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_GVRP:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_GVRP_PDUS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_GMRP:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_GMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_MVRP:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_MVRP_PDUS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_MMRP:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_MMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_IGMP:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_IGMP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_ECFM:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_ECFM_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_EOAM:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_EOAM_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_PTP:
                *pu4RetValFsServiceVlanTunnelPktsSent =
                    VLAN_TUNNEL_PTP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            default:
                break;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsServiceVlanDiscardPktsRx
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                retValFsServiceVlanDiscardPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsServiceVlanDiscardPktsRx (INT4 i4FsServiceVlanId,
                                  INT4 i4FsServiceProtocolEnum,
                                  UINT4 *pu4RetValFsServiceVlanDiscardPktsRx)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry != NULL)
    {
        u2Protocol = (UINT2) i4FsServiceProtocolEnum;

        switch (u2Protocol)
        {
            case L2_PROTO_ELMI:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_ELMI_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_LLDP:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_LLDP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_DOT1X:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_DOT1X_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_LACP:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_LACP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_STP:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_STP_BPDUS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_GVRP:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_GVRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_GMRP:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_GMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_MVRP:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_MVRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_MMRP:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_MMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_IGMP:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_IGMP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_ECFM:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_ECFM_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_EOAM:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_EOAM_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_PTP:
                *pu4RetValFsServiceVlanDiscardPktsRx =
                    VLAN_DISCARD_PTP_PKTS_RX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            default:
                break;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsServiceVlanDiscardPktsTx
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                retValFsServiceVlanDiscardPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsServiceVlanDiscardPktsTx (INT4 i4FsServiceVlanId,
                                  INT4 i4FsServiceProtocolEnum,
                                  UINT4 *pu4RetValFsServiceVlanDiscardPktsTx)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry != NULL)
    {
        u2Protocol = (UINT2) i4FsServiceProtocolEnum;

        switch (u2Protocol)
        {
            case L2_PROTO_ELMI:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_ELMI_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_LLDP:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_LLDP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_DOT1X:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_DOT1X_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_LACP:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_LACP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_STP:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_STP_BPDUS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_GVRP:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_GVRP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_GMRP:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_GMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_MVRP:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_MVRP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_MMRP:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_MMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_IGMP:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_IGMP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_ECFM:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_ECFM_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_EOAM:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_EOAM_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            case L2_PROTO_PTP:
                *pu4RetValFsServiceVlanDiscardPktsTx =
                    VLAN_DISCARD_PTP_PKTS_TX_PER_VLAN (pVlanCurrEntry);
                return SNMP_SUCCESS;
            default:
                break;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsServiceVlanRsvdMacaddress
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                setValFsServiceVlanRsvdMacaddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsServiceVlanRsvdMacaddress (INT4 i4FsServiceVlanId,
                                   INT4 i4FsServiceProtocolEnum,
                                   tMacAddr SetValFsServiceVlanRsvdMacaddress)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry != NULL)
    {

        if (VLAN_IS_MCASTADDR (SetValFsServiceVlanRsvdMacaddress) != VLAN_TRUE)
        {
            return SNMP_FAILURE;
        }
        else
        {
            u2Protocol = (UINT2) i4FsServiceProtocolEnum;
            if ((u2Protocol >= L2_PROTO_OTHER) && (u2Protocol <= L2_PROTO_MAX))
            {
                MEMCPY (VLAN_RSVD_OTHER_ADDR_PER_VLAN (pVlanCurrEntry,
                                                       (u2Protocol -
                                                        L2_PROTO_OTHER)),
                        SetValFsServiceVlanRsvdMacaddress, ETHERNET_ADDR_SIZE);
            }
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsServiceVlanTunnelMacaddress
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                setValFsServiceVlanTunnelMacaddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsServiceVlanTunnelMacaddress (INT4 i4FsServiceVlanId,
                                     INT4 i4FsServiceProtocolEnum,
                                     tMacAddr
                                     SetValFsServiceVlanTunnelMacaddress)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry != NULL)
    {

        if (VLAN_IS_MCASTADDR (SetValFsServiceVlanTunnelMacaddress) !=
            VLAN_TRUE)
        {
            return SNMP_FAILURE;
        }
        else
        {
            u2Protocol = (UINT2) i4FsServiceProtocolEnum;

            if ((u2Protocol >= L2_PROTO_OTHER) && (u2Protocol <= L2_PROTO_MAX))
            {
                MEMCPY (VLAN_TUNNEL_OTHER_ADDR_PER_VLAN (pVlanCurrEntry,
                                                         (u2Protocol -
                                                          L2_PROTO_OTHER)),
                        SetValFsServiceVlanTunnelMacaddress,
                        ETHERNET_ADDR_SIZE);
            }
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsServiceVlanTunnelProtocolStatus
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                setValFsServiceVlanTunnelProtocolStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsServiceVlanTunnelProtocolStatus (INT4 i4FsServiceVlanId,
                                         INT4 i4FsServiceProtocolEnum,
                                         INT4
                                         i4SetValFsServiceVlanTunnelProtocolStatus)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;
    UINT1               u1TunnelStatus = VLAN_ZERO;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry != NULL)
    {
        /* Set the Tunnel status (Enable/Disable) based on the protocol tunnel status (Tunnel/Peer/Discard) */
        if ((i4SetValFsServiceVlanTunnelProtocolStatus ==
             VLAN_TUNNEL_PROTOCOL_TUNNEL)
            || (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_TUNNEL_PROTOCOL_DISCARD))
        {
            u1TunnelStatus = VLAN_ENABLED;
            VLAN_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) = VLAN_ENABLED;
        }
        else if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                 VLAN_TUNNEL_PROTOCOL_PEER)
        {
            u1TunnelStatus = VLAN_DISABLED;
            VLAN_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) = VLAN_DISABLED;
        }

        VlanL2IwfSetTunnelStatusPerVlan (VLAN_CURR_CONTEXT_ID (),
                                         (UINT4) u2VlanId, u1TunnelStatus);

        u2Protocol = (UINT2) i4FsServiceProtocolEnum;

        if (u2Protocol == L2_PROTO_DOT1X)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_DOT1X_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_DOT1X_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_LACP)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_LACP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_LACP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_STP)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_STP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_STP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_GVRP)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_GVRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_GVRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_GMRP)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_GMRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_GMRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_IGMP)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_IGMP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_IGMP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_MVRP)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_MVRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_MVRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_MMRP)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_MMRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_MMRP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_ELMI)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_ELMI_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_ELMI_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_LLDP)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_LLDP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_LLDP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_ECFM)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_ECFM_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_ECFM_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_EOAM)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_EOAM_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_EOAM_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if (u2Protocol == L2_PROTO_PTP)
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_PTP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry))
            {
                return SNMP_SUCCESS;
            }
            VLAN_PTP_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        else if ((u2Protocol >= L2_PROTO_OTHER) && (u2Protocol <= L2_PROTO_MAX))
        {
            if (i4SetValFsServiceVlanTunnelProtocolStatus ==
                VLAN_OTHER_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry,
                                                   (u2Protocol -
                                                    L2_PROTO_OTHER)))
            {
                return SNMP_SUCCESS;
            }
            VLAN_OTHER_TUNNEL_STATUS_PER_VLAN (pVlanCurrEntry,
                                               (u2Protocol - L2_PROTO_OTHER)) =
                (UINT1) i4SetValFsServiceVlanTunnelProtocolStatus;
        }
        VlanL2IwfSetProtocolTunnelStatusPerVlan (VLAN_CURR_CONTEXT_ID (),
                                                 u2VlanId, u2Protocol,
                                                 (UINT1)
                                                 i4SetValFsServiceVlanTunnelProtocolStatus);
        /* Sending Trigger to MSR */

        RM_GET_SEQ_NUM (&u4SeqNum);

        u4CurrContextId = VLAN_CURR_CONTEXT_ID ();
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIServiceVlanTunnelProtocolStatus, u4SeqNum,
                              FALSE, VlanLock, VlanUnLock, 3, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                          VLAN_CURR_CONTEXT_ID (),
                          i4FsServiceVlanId,
                          i4FsServiceProtocolEnum,
                          i4SetValFsServiceVlanTunnelProtocolStatus));
        VlanSelectContext (u4CurrContextId);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsServiceVlanRsvdMacaddress
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                testValFsServiceVlanRsvdMacaddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsServiceVlanRsvdMacaddress (UINT4 *pu4ErrorCode,
                                      INT4 i4FsServiceVlanId,
                                      INT4 i4FsServiceProtocolEnum,
                                      tMacAddr
                                      TestValFsServiceVlanRsvdMacaddress)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u2Protocol = (UINT2) i4FsServiceProtocolEnum;

    if ((u2Protocol < L2_PROTO_OTHER) || (u2Protocol > L2_PROTO_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_MCASTADDR (TestValFsServiceVlanRsvdMacaddress) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsServiceVlanTunnelMacaddress
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                testValFsServiceVlanTunnelMacaddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsServiceVlanTunnelMacaddress (UINT4 *pu4ErrorCode,
                                        INT4 i4FsServiceVlanId,
                                        INT4 i4FsServiceProtocolEnum,
                                        tMacAddr
                                        TestValFsServiceVlanTunnelMacaddress)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u2Protocol = (UINT2) i4FsServiceProtocolEnum;

    if ((u2Protocol < L2_PROTO_OTHER) || (u2Protocol > L2_PROTO_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
        return SNMP_FAILURE;
    }

    if (VLAN_IS_MCASTADDR (TestValFsServiceVlanTunnelMacaddress) != VLAN_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsServiceVlanTunnelProtocolStatus
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum

                The Object
                testValFsServiceVlanTunnelProtocolStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsServiceVlanTunnelProtocolStatus (UINT4 *pu4ErrorCode,
                                            INT4 i4FsServiceVlanId,
                                            INT4 i4FsServiceProtocolEnum,
                                            INT4
                                            i4TestValFsServiceVlanTunnelProtocolStatus)
{
    tVlanCurrEntry     *pVlanCurrEntry = NULL;
    UINT2               u2VlanId = VLAN_ZERO;
    UINT2               u2Protocol = VLAN_ZERO;
    UINT2               u2TunnelStatus = VLAN_ZERO;

    u2VlanId = (UINT2) i4FsServiceVlanId;

    pVlanCurrEntry = VLAN_GET_CURR_ENTRY (u2VlanId);

    if (pVlanCurrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u2Protocol = (UINT2) i4FsServiceProtocolEnum;

    if (u2Protocol > L2_PROTO_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Protocols that can be validated on Per Vlan Functionality is alone taken,
     * other protocols will return Error code while trying to set Tunnel status */
    else if ((u2Protocol != L2_PROTO_GVRP) && (u2Protocol != L2_PROTO_GMRP) &&
             (u2Protocol != L2_PROTO_IGMP) && (u2Protocol != L2_PROTO_MVRP) &&
             (u2Protocol != L2_PROTO_MMRP) && (u2Protocol != L2_PROTO_ECFM) &&
             (u2Protocol != L2_PROTO_OTHER))
    {
        if (!(u2Protocol > L2_PROTO_OTHER) && (u2Protocol < L2_PROTO_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
    {
        CLI_SET_ERR (CLI_PB_1AD_BRIDGE_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u2TunnelStatus = (UINT2) i4TestValFsServiceVlanTunnelProtocolStatus;

    if ((u2Protocol == L2_PROTO_GVRP)
        && (u2TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER))
    {
        CLI_SET_ERR (CLI_PB_GVRP_PEER_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u2Protocol == L2_PROTO_GMRP)
        && (u2TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER))
    {
        CLI_SET_ERR (CLI_PB_GMRP_PEER_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u2Protocol == L2_PROTO_MVRP)
        && (u2TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER))
    {
        CLI_SET_ERR (CLI_PB_MVRP_PEER_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u2Protocol == L2_PROTO_MMRP)
        && (u2TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER))
    {
        CLI_SET_ERR (CLI_PB_MMRP_PEER_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u2Protocol == L2_PROTO_IGMP)
        && (u2TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER))
    {
        CLI_SET_ERR (CLI_PB_IGMP_PEER_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u2TunnelStatus != VLAN_TUNNEL_PROTOCOL_PEER) &&
        (u2TunnelStatus != VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
        (u2TunnelStatus != VLAN_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsServiceVlanTunnelProtocolTable
 Input       :  The Indices
                FsServiceVlanId
                FsServiceProtocolEnum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsServiceVlanTunnelProtocolTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
