/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnpbred.c,v 1.18 2014/05/06 13:09:36 siva Exp $
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : vlnpbred.c                                      */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 20 Dec 2006                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains VLAN PB redundancy related    */
/*                          routines.                                        */
/*****************************************************************************/
#ifdef L2RED_WANTED
#include "vlaninc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRedSyncPbPortProperties                    */
/*                                                                           */
/*    Description         : This function syncronizes provider bridge port   */
/*                          properties in hardware and software              */
/*                                                                           */
/*    Input(s)            : u2Port - port to be syncronised                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/

VOID
VlanPbRedSyncPbPortProperties (UINT4 u4Context, UINT2 u2Port)
{
    tHwVlanPbPcpInfo    HwVlanPbPcpInfo;
    tVlanPbPortEntry   *pSrcPbPortEntry = NULL;
    tVlanPortEntry     *pSrcPortEntry = NULL;
    UINT2               u2Pcp;
    UINT1               u1Priority;
    UINT1               u1SVlanTranslationStatus = OSIX_DISABLED;
    UINT1               u1DropEligible;
    UINT4               u4Port;

    VLAN_MEMSET (&HwVlanPbPcpInfo, 0, sizeof (tHwVlanPbPcpInfo));

    VLAN_LOCK ();

    if (VLAN_RED_AUDIT_FLAG () != VLAN_RED_AUDIT_START)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
    {
        VLAN_UNLOCK ();
        return;
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () != VLAN_TRUE)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return;
    }

    pSrcPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (pSrcPbPortEntry == NULL)
    {
        VlanReleaseContext ();
        VLAN_UNLOCK ();

        return;
    }

    pSrcPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    u4Port = VLAN_GET_IFINDEX (u2Port);

    if (VlanHwSetProviderBridgePortType (VLAN_CURR_CONTEXT_ID (),
                                         VLAN_GET_PHY_PORT
                                         (u2Port),
                                         pSrcPbPortEntry->u1PbPortType) !=
        VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "AUDIT Setting Bridge Port type on port failed "
                       "for Port %d \n", u4Port);

    }

    if (VlanHwSetPortIngressEtherType (VLAN_CURR_CONTEXT_ID (),
                                       VLAN_GET_PHY_PORT (u2Port),
                                       pSrcPortEntry->
                                       u2IngressEtherType) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "AUDIT Setting Port Ingress Ether Type on port failed "
                       "for Port %d \n", u4Port);

    }

    if (VlanHwSetPortEgressEtherType (VLAN_CURR_CONTEXT_ID (),
                                      VLAN_GET_PHY_PORT (u2Port),
                                      pSrcPortEntry->
                                      u2EgressEtherType) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "AUDIT Setting Port Egress EtherType on port failed "
                       "for Port %d \n", u4Port);

    }

    if (VlanL2IwfPbGetVidTransStatus (VLAN_CURR_CONTEXT_ID (),
                                      u2Port, &u1SVlanTranslationStatus)
        != L2IWF_FAILURE)
    {

        if (VlanHwSetPortSVlanTranslationStatus (VLAN_CURR_CONTEXT_ID (),
                                                 VLAN_GET_PHY_PORT (u2Port),
                                                 u1SVlanTranslationStatus) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting SVlan Translation Status on port failed "
                           "for Port %d \n", u4Port);

        }

    }
    if (VlanHwSetPortEtherTypeSwapStatus (VLAN_CURR_CONTEXT_ID (),
                                          VLAN_GET_PHY_PORT (u2Port),
                                          pSrcPbPortEntry->
                                          u1EtherTypeSwapStatus) !=
        VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "AUDIT Setting Port EtherType Swap Status on port failed "
                       "for Port %d \n", u4Port);

    }

    if (pSrcPbPortEntry->u1DefCVlanClassify == VLAN_ENABLED)
    {
        if (VlanHwSetPortCustomerVlan (VLAN_CURR_CONTEXT_ID (),
                                       (UINT2)
                                       (VLAN_GET_PHY_PORT (u2Port)),
                                       pSrcPbPortEntry->CVlanId) !=
            VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting Port Customer Vlan on port failed "
                           "for Port %d \n", u4Port);

        }
    }

    if (VlanHwSetPortSVlanClassifyMethod (VLAN_CURR_CONTEXT_ID (),
                                          VLAN_GET_PHY_PORT (u2Port),
                                          (UINT1) pSrcPbPortEntry->
                                          u1SVlanTableType) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "AUDIT Setting Port Svlan Classify Method on port failed "
                       "for Port %d \n", u4Port);

    }

    if (VlanHwPortMacLearningStatus (VLAN_CURR_CONTEXT_ID (),
                                     VLAN_GET_PHY_PORT (u2Port),
                                     pSrcPbPortEntry->
                                     u1MacLearningStatus) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "AUDIT Setting Port Mac Learning Status on port failed "
                       "for Port %d \n", u4Port);

    }

    if (VlanHwPortMacLearningLimit (VLAN_CURR_CONTEXT_ID (),
                                    VLAN_GET_PHY_PORT (u2Port),
                                    pSrcPbPortEntry->
                                    u4MacLearningLimit) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "AUDIT Setting Port Mac Learning Limit on port failed "
                       "for Port %d \n", u4Port);

    }

    if (VlanHwSetPortPcpSelection (VLAN_CURR_CONTEXT_ID (),
                                   VLAN_GET_PHY_PORT (u2Port),
                                   pSrcPbPortEntry->u1PcpSelRow) !=
        VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "AUDIT Setting Port Pcp Row Selection on port failed "
                       "for Port %d \n", u4Port);

    }

    if (VlanHwSetPortReqDropEncoding (VLAN_CURR_CONTEXT_ID (),
                                      VLAN_GET_PHY_PORT (u2Port),
                                      pSrcPbPortEntry->u1ReqDropEncoding) !=
        VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "AUDIT Setting Port Req Drop Encoding on port failed "
                       "for Port %d \n", u4Port);

    }

    if (VlanHwSetPortUseDei (VLAN_CURR_CONTEXT_ID (),
                             VLAN_GET_PHY_PORT (u2Port),
                             pSrcPbPortEntry->u1UseDei) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "AUDIT Setting Port UseDei on port failed "
                       "for Port %d \n", u4Port);

    }

    HwVlanPbPcpInfo.u2PcpSelRow = pSrcPbPortEntry->u1PcpSelRow;

    for (u2Pcp = 0; u2Pcp < VLAN_PB_MAX_PCP; u2Pcp++)
    {
        HwVlanPbPcpInfo.u2PcpValue = u2Pcp;
        HwVlanPbPcpInfo.u2Priority
            = VLAN_PB_PCP_DECODE_PRIORITY
            (pSrcPbPortEntry, pSrcPbPortEntry->u1PcpSelRow, u2Pcp);
        HwVlanPbPcpInfo.u1DropEligible
            = VLAN_PB_PCP_DECODE_DROP_ELIGIBLE
            (pSrcPbPortEntry, pSrcPbPortEntry->u1PcpSelRow, u2Pcp);

        if (VlanHwSetPcpDecodTbl (VLAN_CURR_CONTEXT_ID (),
                                  VLAN_GET_PHY_PORT (u2Port),
                                  HwVlanPbPcpInfo) != VLAN_SUCCESS)
        {

            VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "AUDIT Setting Pcp Decoding Table on port failed "
                           "for Port %d \n", u4Port);

        }
    }

    for (u1Priority = 0; u1Priority < VLAN_PB_MAX_PRIORITY; u1Priority++)
    {
        for (u1DropEligible = 0; u1DropEligible < VLAN_PB_MAX_DROP_ELIGIBLE;
             u1DropEligible++)
        {
            HwVlanPbPcpInfo.u2Priority = u1Priority;
            HwVlanPbPcpInfo.u1DropEligible = u1DropEligible;
            HwVlanPbPcpInfo.u2PcpValue
                = VLAN_PB_PCP_ENCODE_PCP (pSrcPbPortEntry,
                                          pSrcPbPortEntry->u1PcpSelRow,
                                          u1Priority, u1DropEligible);

            if (VlanHwSetPcpEncodTbl (VLAN_CURR_CONTEXT_ID (),
                                      VLAN_GET_PHY_PORT (u2Port),
                                      HwVlanPbPcpInfo) != VLAN_SUCCESS)
            {
                VLAN_TRC_ARG1 (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "AUDIT Setting Pcp Encoding Table on port failed "
                               "for Port %d \n", u4Port);

            }
        }
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    VlanPbRedSyncCvidEtries (u4Context, u2Port);

    VlanPbRedSyncVidTranslationEntries (u4Context, u2Port);

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRedSyncCvidEtries                          */
/*                                                                           */
/*    Description         : This function syncronizes provider bridge CVID   */
/*                          Registration table in  hardware and software     */
/*                                                                           */
/*    Input(s)            : u2Port - port to be syncronised                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/

VOID
VlanPbRedSyncCvidEtries (UINT4 u4Context, UINT2 u2Port)
{
    tVlanPbLogicalPortEntry VlanPbLogicalPortEntry;
    tVlanCVlanSVlanEntry CVlanSVlanTable;
    tHwVlanPbPepInfo    HwVlanPbPepInfo;
    tVlanSVlanMap       HwVlanSVlanMap;
    tVlanSVlanMap       VlanSVlanMap;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;
    tVlanId             CVlanId = 0;
    UINT4               u4TblIndex;
    INT4                i4AuditFlag = VLAN_RED_ENTRY_NOT_PRESENT_IN_BOTH;
    INT4                i4RetVal;
    INT4                i4RegenPriority;
    UINT1               u1Priority;

    VLAN_MEMSET (&HwVlanSVlanMap, 0, sizeof (tVlanSVlanMap));

    u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);

    /*Get the next valid entry from Sw and Next Valid entry from Hw
     * Compare CVlan Id,
     * if entry exist but equal CvlanId: Entry is present in Sw and Hw
     * if entry exist but Sw CVlanId is less: Entry is present only in Sw
     * if entry exist but Hw CVlanId is less: Entry is present only in Hw
     * */
    do
    {

        VLAN_LOCK ();

        if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
        {
            VLAN_UNLOCK ();
            return;
        }

        pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
        if (pVlanPbPortEntry == NULL)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return;
        }

        if ((pVlanPbPortEntry->u1PbPortType) != VLAN_CUSTOMER_EDGE_PORT)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return;
        }

        /*Initialise i4AuditFlag */
        i4AuditFlag = VLAN_RED_ENTRY_NOT_PRESENT_IN_BOTH;

        VLAN_MEMSET (&CVlanSVlanTable, 0, sizeof (CVlanSVlanTable));

        CVlanSVlanTable.u2Port = u2Port;
        CVlanSVlanTable.CVlanId = CVlanId;

        /*Get the next entry from software */
        pCVlanSVlanEntry =
            (tVlanCVlanSVlanEntry *) RBTreeGetNext
            (VLAN_CURR_CONTEXT_PTR ()->apVlanSVlanTable[u4TblIndex],
             (tRBElem *) & CVlanSVlanTable, NULL);

        VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

        VlanSVlanMap.u2Port = (UINT2) (VLAN_GET_IFINDEX (u2Port));
        VlanSVlanMap.CVlanId = CVlanId;
        VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_TYPE;

        /*Get the next entry from hardware */
        i4RetVal = VlanHwGetNextSVlanMap (VLAN_CURR_CONTEXT_ID (),
                                          VlanSVlanMap, &HwVlanSVlanMap);

        if (pCVlanSVlanEntry != NULL)
        {
            if (((pCVlanSVlanEntry->u2Port != u2Port)) &&
                ((i4RetVal == VLAN_FAILURE) ||
                 (HwVlanSVlanMap.u2Port != u2Port)))
            {
                /*no entry is available for this port, return */
                VlanReleaseContext ();
                VLAN_UNLOCK ();

                return;

            }
            else if (((pCVlanSVlanEntry->u2Port != u2Port)) &&
                     ((i4RetVal == VLAN_SUCCESS) &&
                      (HwVlanSVlanMap.u2Port == u2Port)))
            {
                /*entry is available only Hw for this port */
                pCVlanSVlanEntry = NULL;
            }
            else if ((pCVlanSVlanEntry->u2Port == u2Port) &&
                     (HwVlanSVlanMap.u2Port != u2Port))
            {
                /*entry is availble only in Sw */
                i4RetVal = VLAN_FAILURE;
            }
        }

        /*if entries exist in both and CVlanId is same,
         * update CVlanId for new CVlanId*/
        if ((i4RetVal == VLAN_SUCCESS) &&
            ((pCVlanSVlanEntry != NULL) &&
             (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE) &&
             (pCVlanSVlanEntry->CVlanId == HwVlanSVlanMap.CVlanId)))
        {
            CVlanId = pCVlanSVlanEntry->CVlanId;
            i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_BOTH;
        }

        /*if entries exist in both and Hw CVlanId is lesser than Sw CVlanId,
         * or There is no entry left in Sw, update CVlanId for hw CVlanId*/
        else if ((i4RetVal == VLAN_SUCCESS) &&
                 (((pCVlanSVlanEntry != NULL) &&
                   (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE) &&
                   (HwVlanSVlanMap.CVlanId < pCVlanSVlanEntry->CVlanId)) ||
                  (pCVlanSVlanEntry == NULL)))
        {
            CVlanId = HwVlanSVlanMap.CVlanId;
            i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_HW_ONLY;
        }

        /*if entries exist in both and Sw CVlanId is lesser than Hw CVlanId,
         * or There is no entry left in Hw, update CVlanId for Sw CVlanId*/
        else if ((pCVlanSVlanEntry != NULL) &&
                 (((i4RetVal == VLAN_SUCCESS) &&
                   (pCVlanSVlanEntry->u1RowStatus == VLAN_ACTIVE) &&
                   (pCVlanSVlanEntry->CVlanId < HwVlanSVlanMap.CVlanId)) ||
                  (i4RetVal == VLAN_FAILURE)))

        {
            CVlanId = pCVlanSVlanEntry->CVlanId;
            i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_SW_ONLY;
        }

        switch (i4AuditFlag)
        {
            case VLAN_RED_ENTRY_PRESENT_IN_HW_ONLY:

                VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "AUDIT : CVID entry for vlan  %d Present only in Hardware. "
                               " \n", CVlanId);

                /*delete PEP ports entry from hw for this CVID entry */

                VlanHwDelProviderEdgePort (VLAN_CURR_CONTEXT_ID (),
                                           VLAN_GET_PHY_PORT (u2Port),
                                           HwVlanSVlanMap.SVlanId);

                if (VlanHwDeleteSVlanMap
                    (VLAN_CURR_CONTEXT_ID (), HwVlanSVlanMap) != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    VLAN_UNLOCK ();
                    return;
                }
                break;

            case VLAN_RED_ENTRY_PRESENT_IN_SW_ONLY:

                VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "AUDIT : CVID entry for vlan  %d Present only in Software. "
                               " \n", CVlanId);

                /*Get PEP port for this CVID entry and write in to Hw */

                VLAN_MEMSET (&VlanPbLogicalPortEntry, 0,
                             sizeof (tVlanPbLogicalPortEntry));

                VlanPbLogicalPortEntry.u2CepPort = u2Port;
                VlanPbLogicalPortEntry.SVlanId =
                    (tVlanId) pCVlanSVlanEntry->SVlanId;

                pVlanPbLogicalPortEntry = RBTreeGet
                    (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
                     &VlanPbLogicalPortEntry);

                if ((pVlanPbLogicalPortEntry != NULL) &&
                    (pVlanPbLogicalPortEntry->u1OperStatus == VLAN_OPER_UP))
                {

                    VLAN_MEMSET (&VlanSVlanMap, 0, sizeof (tVlanSVlanMap));

                    VlanSVlanMap.u2Port =
                        (UINT2) (VLAN_GET_IFINDEX (pCVlanSVlanEntry->u2Port));
                    VlanSVlanMap.SVlanId = pCVlanSVlanEntry->SVlanId;
                    VlanSVlanMap.CVlanId = pCVlanSVlanEntry->CVlanId;
                    VlanSVlanMap.u1PepUntag = pCVlanSVlanEntry->u1PepUntag;
                    VlanSVlanMap.u1CepUntag = pCVlanSVlanEntry->u1CepUntag;

                    VlanSVlanMap.u4TableType = VLAN_SVLAN_PORT_CVLAN_TYPE;

                    /*Program Hw for this CVID entry */

                    VlanHwAddSVlanMap (VLAN_CURR_CONTEXT_ID (), VlanSVlanMap);

                    HwVlanPbPepInfo.i4DefUserPri =
                        pVlanPbLogicalPortEntry->u1DefUserPriority;
                    HwVlanPbPepInfo.Cpvid = pVlanPbLogicalPortEntry->Cpvid;
                    HwVlanPbPepInfo.u1AccptFrameType =
                        pVlanPbLogicalPortEntry->u1AccptFrameType;
                    HwVlanPbPepInfo.u1IngFiltering =
                        pVlanPbLogicalPortEntry->u1IngressFiltering;

                    if (VlanHwCreateProviderEdgePort
                        (VLAN_CURR_CONTEXT_ID (),
                         (UINT2) (VLAN_GET_PHY_PORT (u2Port)),
                         pCVlanSVlanEntry->SVlanId,
                         HwVlanPbPepInfo) == VLAN_SUCCESS)
                    {

                        for (u1Priority = 0;
                             u1Priority < VLAN_PB_MAX_PRIORITY; u1Priority++)
                        {
                            i4RegenPriority =
                                pVlanPbLogicalPortEntry->
                                au1RegenPriority[u1Priority];

                            VlanHwSetServicePriRegenEntry
                                (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_GET_PHY_PORT (u2Port),
                                 pCVlanSVlanEntry->SVlanId,
                                 (INT4) u1Priority, i4RegenPriority);
                        }

                    }

                    /*Notify Ast about new Hw pep creation */
                    VlanL2IwfHwPbPepCreate (VLAN_GET_IFINDEX (u2Port),
                                            pCVlanSVlanEntry->SVlanId);

                }

                break;
            case VLAN_RED_ENTRY_PRESENT_IN_BOTH:

                /*Get PEP port prop for this CVID entry and write in to Hw */

                VLAN_MEMSET (&VlanPbLogicalPortEntry, 0,
                             sizeof (tVlanPbLogicalPortEntry));

                VlanPbLogicalPortEntry.u2CepPort = u2Port;
                VlanPbLogicalPortEntry.SVlanId =
                    (tVlanId) pCVlanSVlanEntry->SVlanId;

                pVlanPbLogicalPortEntry = RBTreeGet
                    (VLAN_CURR_CONTEXT_PTR ()->pVlanLogicalPortTable,
                     &VlanPbLogicalPortEntry);

                if (pVlanPbLogicalPortEntry != NULL)
                {

                    if (VlanHwSetPepPvid (VLAN_CURR_CONTEXT_ID (),
                                          VLAN_GET_PHY_PORT (u2Port),
                                          pCVlanSVlanEntry->SVlanId,
                                          pVlanPbLogicalPortEntry->Cpvid)
                        != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        VLAN_UNLOCK ();
                        return;
                    }

                    VlanHwSetPepAccFrameType
                        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2Port),
                         pCVlanSVlanEntry->SVlanId,
                         pVlanPbLogicalPortEntry->u1AccptFrameType);

                    VlanHwSetPepDefUserPriority
                        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2Port),
                         pCVlanSVlanEntry->SVlanId,
                         pVlanPbLogicalPortEntry->u1DefUserPriority);

                    VlanHwSetPepIngFiltering
                        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2Port),
                         pCVlanSVlanEntry->SVlanId,
                         pVlanPbLogicalPortEntry->u1IngressFiltering);

                    for (u1Priority = 0;
                         u1Priority < VLAN_PB_MAX_PRIORITY; u1Priority++)
                    {
                        i4RegenPriority =
                            pVlanPbLogicalPortEntry->
                            au1RegenPriority[u1Priority];

                        VlanHwSetServicePriRegenEntry
                            (VLAN_CURR_CONTEXT_ID (),
                             VLAN_GET_PHY_PORT (u2Port),
                             pCVlanSVlanEntry->SVlanId,
                             (INT4) u1Priority, i4RegenPriority);
                    }

                }

                break;

            default:
                break;
        }

        VlanReleaseContext ();
        VLAN_UNLOCK ();

    }
    while ((i4RetVal == VLAN_SUCCESS) || (pCVlanSVlanEntry != NULL));

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRedSyncVidTranslationEntries               */
/*                                                                           */
/*    Description         : This function syncronizes provider bridge VID    */
/*                          Translation table in  hardware and software      */
/*                                                                           */
/*    Input(s)            : u2Port - port to be syncronised                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/

VOID
VlanPbRedSyncVidTranslationEntries (UINT4 u4Context, UINT2 u2Port)
{
    tVidTransEntryInfo  VidTransEntryInfo;
    tVidTransEntryInfo  HwVidTransEntryInfo;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanId             LocalVid = 0;
    INT4                i4RetVal1;
    INT4                i4RetVal2;
    INT4                i4AuditFlag = VLAN_RED_ENTRY_NOT_PRESENT_IN_BOTH;

    do
    {

        VLAN_LOCK ();

        if (VlanSelectContext (u4Context) != VLAN_SUCCESS)
        {
            VLAN_UNLOCK ();
            return;
        }
        pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
        if (pVlanPbPortEntry == NULL)
        {
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            return;
        }

        if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_CORE_BRIDGE_MODE) ||
            (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE))
        {
            if (VLAN_IS_VID_TRANS_VALID_ONPORT (u2Port) == VLAN_FALSE)
            {
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return;
            }
        }

        if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
        {
            if (((pVlanPbPortEntry->u1PbPortType) ==
                 VLAN_PROVIDER_INSTANCE_PORT) ||
                ((pVlanPbPortEntry->u1PbPortType) ==
                 VLAN_CUSTOMER_BACKBONE_PORT))

            {
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                return;
            }
        }

        /*Initialise i4AuditFlag */
        i4AuditFlag = VLAN_RED_ENTRY_NOT_PRESENT_IN_BOTH;

        VLAN_MEMSET (&VidTransEntryInfo, 0, sizeof (VidTransEntryInfo));

        /*Search with Local Vid */
        i4RetVal1 = VlanL2IwfPbGetNextVidTransTblEntry (VLAN_CURR_CONTEXT_ID (),
                                                        u2Port, LocalVid,
                                                        &VidTransEntryInfo);

        /* Entries with zero local or relay vids can be present in
         *  the hw and those entries are not useful for audit*/

        do
        {
            VLAN_MEMSET (&HwVidTransEntryInfo, 0, sizeof (HwVidTransEntryInfo));

            i4RetVal2 = VlanHwGetNextSVlanTranslationEntry
                (VLAN_CURR_CONTEXT_ID (), (UINT2) VLAN_GET_PHY_PORT (u2Port),
                 LocalVid, &HwVidTransEntryInfo);

            if (HwVidTransEntryInfo.u2Port != (VLAN_GET_IFINDEX (u2Port)))
            {
                /* There is no VID Translation entry present for this port. */
                i4RetVal2 = VLAN_FAILURE;
                break;
            }
            if ((i4RetVal2 != VLAN_FAILURE)
                && (HwVidTransEntryInfo.RelayVid == 0))
            {
                /*Skip zero relay vid entry */
                LocalVid = HwVidTransEntryInfo.LocalVid;
            }

        }
        while ((i4RetVal2 != VLAN_FAILURE) &&
               ((HwVidTransEntryInfo.LocalVid == 0) ||
                (HwVidTransEntryInfo.RelayVid == 0)));

        if (i4RetVal1 != L2IWF_FAILURE)
        {
            if (((VidTransEntryInfo.u2Port != u2Port)) &&
                ((i4RetVal2 != VLAN_FAILURE) ||
                 (HwVidTransEntryInfo.u2Port != u2Port)))
            {
                /*no entry is available for this port, return */
                VlanReleaseContext ();
                VLAN_UNLOCK ();

                return;

            }
            else if (((VidTransEntryInfo.u2Port != u2Port)) &&
                     ((i4RetVal2 == VLAN_SUCCESS) &&
                      HwVidTransEntryInfo.u2Port == u2Port))
            {
                /*entry is available only Hw for this port */
                i4RetVal1 = L2IWF_FAILURE;
            }
            else if (((VidTransEntryInfo.u2Port == u2Port)) &&
                     (HwVidTransEntryInfo.u2Port != u2Port))
            {
                /*entry is availble only in Sw */
                i4RetVal2 = VLAN_FAILURE;
            }
        }

        /*If entry is available both in Hw & Sw, return LocalVid will be same
         *and Update the loop for this new LocalVid*/
        if (((i4RetVal1 == L2IWF_SUCCESS) &&
             (VidTransEntryInfo.u1RowStatus == VLAN_ACTIVE)) &&
            (i4RetVal2 == VLAN_SUCCESS) &&
            VidTransEntryInfo.LocalVid == HwVidTransEntryInfo.LocalVid)
        {
            LocalVid = VidTransEntryInfo.LocalVid;
            i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_BOTH;
        }
        /*If entry is available in Sw but not in Hw, return LocalVid of
         * Sw will be less and update the loop for lesser LocalVid or
         * There is no entry in Hw*/
        else if (((i4RetVal1 == L2IWF_SUCCESS) &&
                  (VidTransEntryInfo.u1RowStatus == VLAN_ACTIVE)) &&
                 (((i4RetVal2 == VLAN_SUCCESS) &&
                   (VidTransEntryInfo.LocalVid <
                    HwVidTransEntryInfo.LocalVid)) ||
                  (i4RetVal2 == VLAN_FAILURE)))
        {
            LocalVid = VidTransEntryInfo.LocalVid;
            i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_SW_ONLY;
        }
        /*If entry is available in Hw not in Sw, return LocalVid of Hw
         * will be less and update the loop for lesser LocalVid or
         * There is no entry left in Sw*/
        else if ((i4RetVal2 == VLAN_SUCCESS) &&
                 ((((i4RetVal1 == L2IWF_SUCCESS) &&
                    (VidTransEntryInfo.u1RowStatus == VLAN_ACTIVE)) &&
                   (HwVidTransEntryInfo.LocalVid <
                    VidTransEntryInfo.LocalVid)) ||
                  (i4RetVal1 == L2IWF_FAILURE)))
        {
            LocalVid = HwVidTransEntryInfo.LocalVid;
            i4AuditFlag = VLAN_RED_ENTRY_PRESENT_IN_HW_ONLY;
        }

        switch (i4AuditFlag)
        {
            case VLAN_RED_ENTRY_PRESENT_IN_HW_ONLY:

                VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "AUDIT : VID Trans entry for vlan  %d "
                               "Present only in Hardware. " " \n", LocalVid);

                VlanHwDelSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                                (UINT2)
                                                VLAN_GET_PHY_PORT (u2Port),
                                                HwVidTransEntryInfo.LocalVid,
                                                HwVidTransEntryInfo.RelayVid);

                break;
            case VLAN_RED_ENTRY_PRESENT_IN_SW_ONLY:

                VlanHwAddSVlanTranslationEntry (VLAN_CURR_CONTEXT_ID (),
                                                (UINT2)
                                                VLAN_GET_PHY_PORT (u2Port),
                                                VidTransEntryInfo.LocalVid,
                                                VidTransEntryInfo.RelayVid);

                VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "AUDIT : VID Trans entry for vlan  %d "
                               "Present only in Software. " " \n", LocalVid);

                break;
            case VLAN_RED_ENTRY_PRESENT_IN_BOTH:
                VLAN_TRC_ARG1 (VLAN_RED_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "AUDIT : VID Trans entry for vlan  %d "
                               "Present in Sw & Hw " " \n", LocalVid);
                break;

        }

        VlanReleaseContext ();
        VLAN_UNLOCK ();

    }
    while ((i4RetVal1 != L2IWF_FAILURE) || (i4RetVal2 != VLAN_FAILURE));
    /*break the loop, if there is no entry */
}

#endif
