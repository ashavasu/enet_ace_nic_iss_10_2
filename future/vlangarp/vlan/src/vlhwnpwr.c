/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: vlhwnpwr.c,v 1.17 2016/06/20 10:00:09 siva Exp $
 *
 * Description:This file contains the wrapper for
 *             for Hardware API's w.r.t VLAN
 *             <Part of dual node stacking model>
 *
 *******************************************************************/
#ifndef __VLHWNPWR_C__
#define __VLHWNPWR_C__

#include "vlaninc.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : VlanNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tVlanNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
VlanNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tVlanNpModInfo     *pVlanNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pVlanNpModInfo = &(pFsHwNp->VlanNpModInfo);

    if (NULL == pVlanNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_MI_VLAN_HW_INIT:
        {
            tVlanNpWrFsMiVlanHwInit *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiVlanHwInit (pEntry->u4ContextId);
            break;
        }
        case FS_MI_VLAN_HW_DE_INIT:
        {
            tVlanNpWrFsMiVlanHwDeInit *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiVlanHwDeInit (pEntry->u4ContextId);
            break;
        }
        case FS_MI_VLAN_HW_SET_ALL_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwSetAllGroupsPorts *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetAllGroupsPorts;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetAllGroupsPorts (pEntry->u4ContextId,
                                             pEntry->VlanId,
                                             pEntry->pHwAllGroupPorts);
            break;
        }
        case FS_MI_VLAN_HW_RESET_ALL_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwResetAllGroupsPorts *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetAllGroupsPorts;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwResetAllGroupsPorts (pEntry->u4ContextId,
                                               pEntry->VlanId,
                                               pEntry->pHwResetAllGroupPorts);
            break;
        }
        case FS_MI_VLAN_HW_RESET_UN_REG_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwResetUnRegGroupsPorts *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetUnRegGroupsPorts;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwResetUnRegGroupsPorts (pEntry->u4ContextId,
                                                 pEntry->VlanId,
                                                 pEntry->
                                                 pHwResetUnRegGroupPorts);
            break;
        }
        case FS_MI_VLAN_HW_SET_UN_REG_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanHwSetUnRegGroupsPorts *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetUnRegGroupsPorts;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetUnRegGroupsPorts (pEntry->u4ContextId,
                                               pEntry->VlanId,
                                               pEntry->pHwUnRegPorts);
            break;
        }
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddStaticUcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStaticUcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAddStaticUcastEntry (pEntry->u4ContextId,
                                               pEntry->u4Fid, pEntry->MacAddr,
                                               pEntry->u4Port,
                                               pEntry->pHwAllowedToGoPorts,
                                               pEntry->u1Status);
            break;
        }
        case FS_MI_VLAN_HW_DEL_STATIC_UCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelStaticUcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelStaticUcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDelStaticUcastEntry (pEntry->u4ContextId,
                                               pEntry->u4Fid, pEntry->MacAddr,
                                               pEntry->u4Port);
            break;
        }
        case FS_MI_VLAN_HW_GET_FDB_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetFdbEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetFdbEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetFdbEntry (pEntry->u4ContextId, pEntry->u4FdbId,
                                       pEntry->MacAddr, pEntry->pEntry);
            break;
        }
        case FS_NP_HW_GET_PORT_FROM_FDB:
        {
            tVlanNpWrFsNpHwGetPortFromFdb *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsNpHwGetPortFromFdb;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetPortFromFdb (pEntry->u2VlanId, pEntry->i1pHwAddr,
                                          pEntry->pu4Port);
            break;
        }
#ifndef SW_LEARNING
        case FS_MI_VLAN_HW_GET_FDB_COUNT:
        {
            tVlanNpWrFsMiVlanHwGetFdbCount *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetFdbCount;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetFdbCount (pEntry->u4ContextId, pEntry->u4FdbId,
                                       pEntry->pu4Count);
            break;
        }
        case FS_MI_VLAN_HW_GET_FIRST_TP_FDB_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetFirstTpFdbEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetFirstTpFdbEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetFirstTpFdbEntry (pEntry->u4ContextId,
                                              pEntry->pu4FdbId,
                                              pEntry->MacAddr);
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_TP_FDB_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetNextTpFdbEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextTpFdbEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetNextTpFdbEntry (pEntry->u4ContextId,
                                             pEntry->u4FdbId, pEntry->MacAddr,
                                             pEntry->pu4NextContextId,
                                             pEntry->pu4NextFdbId,
                                             pEntry->NextMacAddr);
            break;
        }
#endif /* SW_LEARNING */
        case FS_MI_VLAN_HW_ADD_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddMcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddMcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAddMcastEntry (pEntry->u4ContextId, pEntry->VlanId,
                                         pEntry->MacAddr,
                                         pEntry->pHwMcastPorts);
            break;
        }
        case FS_MI_VLAN_HW_ADD_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddStMcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStMcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAddStMcastEntry (pEntry->u4ContextId, pEntry->VlanId,
                                           pEntry->MacAddr, pEntry->i4RcvPort,
                                           pEntry->pHwMcastPorts);
            break;
        }
        case FS_MI_VLAN_HW_SET_MCAST_PORT:
        {
            tVlanNpWrFsMiVlanHwSetMcastPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMcastPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetMcastPort (pEntry->u4ContextId, pEntry->VlanId,
                                        pEntry->MacAddr, pEntry->u4IfIndex);
            break;
        }
        case FS_MI_VLAN_HW_RESET_MCAST_PORT:
        {
            tVlanNpWrFsMiVlanHwResetMcastPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetMcastPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwResetMcastPort (pEntry->u4ContextId, pEntry->VlanId,
                                          pEntry->MacAddr, pEntry->u4IfIndex);
            break;
        }
        case FS_MI_VLAN_HW_DEL_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelMcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelMcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDelMcastEntry (pEntry->u4ContextId, pEntry->VlanId,
                                         pEntry->MacAddr);
            break;
        }
        case FS_MI_VLAN_HW_DEL_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelStMcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelStMcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDelStMcastEntry (pEntry->u4ContextId, pEntry->VlanId,
                                           pEntry->MacAddr, pEntry->i4RcvPort);
            break;
        }
        case FS_MI_VLAN_HW_ADD_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAddVlanEntry (pEntry->u4ContextId, pEntry->VlanId,
                                        pEntry->pHwEgressPorts,
                                        pEntry->pHwUnTagPorts);
            break;
        }
        case FS_MI_VLAN_HW_DEL_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDelVlanEntry (pEntry->u4ContextId, pEntry->VlanId);
            break;
        }
        case FS_MI_VLAN_HW_SET_VLAN_MEMBER_PORT:
        {
            tVlanNpWrFsMiVlanHwSetVlanMemberPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetVlanMemberPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetVlanMemberPort (pEntry->u4ContextId,
                                             pEntry->VlanId, pEntry->u4IfIndex,
                                             pEntry->u1IsTagged);
            break;
        }
        case FS_MI_VLAN_HW_RESET_VLAN_MEMBER_PORT:
        {
            tVlanNpWrFsMiVlanHwResetVlanMemberPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetVlanMemberPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwResetVlanMemberPort (pEntry->u4ContextId,
                                               pEntry->VlanId,
                                               pEntry->u4IfIndex);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PVID:
        {
            tVlanNpWrFsMiVlanHwSetPortPvid *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortPvid;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortPvid (pEntry->u4ContextId, pEntry->u4IfIndex,
                                       pEntry->VlanId);
            break;
        }
        case FS_MI_VLAN_HW_SET_DEFAULT_VLAN_ID:
        {
            tVlanNpWrFsMiVlanHwSetDefaultVlanId *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetDefaultVlanId;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetDefaultVlanId (pEntry->u4ContextId,
                                            pEntry->VlanId);
            break;
        }
#ifdef L2RED_WANTED
        case FS_MI_VLAN_HW_SYNC_DEFAULT_VLAN_ID:
        {
            tVlanNpWrFsMiVlanHwSyncDefaultVlanId *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSyncDefaultVlanId;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSyncDefaultVlanId (pEntry->u4ContextId,
                                             pEntry->SwVlanId);
            break;
        }
        case FS_MI_VLAN_RED_HW_UPDATE_D_B_FOR_DEFAULT_VLAN_ID:
        {
            tVlanNpWrFsMiVlanRedHwUpdateDBForDefaultVlanId *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanRedHwUpdateDBForDefaultVlanId;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanRedHwUpdateDBForDefaultVlanId (pEntry->u4ContextId,
                                                       pEntry->VlanId);
            break;
        }
        case FS_MI_VLAN_HW_SCAN_PROTOCOL_VLAN_TBL:
        {
            tVlanNpWrFsMiVlanHwScanProtocolVlanTbl *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwScanProtocolVlanTbl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwScanProtocolVlanTbl (pEntry->u4ContextId,
                                               pEntry->u4Port,
                                               pEntry->ProtoVlanCallBack);
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_PROTOCOL_MAP:
        {
            tVlanNpWrFsMiVlanHwGetVlanProtocolMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanProtocolMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetVlanProtocolMap (pEntry->u4ContextId,
                                              pEntry->u4IfIndex,
                                              pEntry->u4GroupId,
                                              pEntry->pProtoTemplate,
                                              pEntry->pVlanId);
            break;
        }
        case FS_MI_VLAN_HW_SCAN_MULTICAST_TBL:
        {
            tVlanNpWrFsMiVlanHwScanMulticastTbl *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwScanMulticastTbl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwScanMulticastTbl (pEntry->u4ContextId,
                                            pEntry->McastCallBack);
            break;
        }
        case FS_MI_VLAN_HW_GET_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetStMcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStMcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetStMcastEntry (pEntry->u4ContextId, pEntry->VlanId,
                                           pEntry->MacAddr, pEntry->u4RcvPort,
                                           pEntry->pHwMcastPorts);
            break;
        }
        case FS_MI_VLAN_HW_SCAN_UNICAST_TBL:
        {
            tVlanNpWrFsMiVlanHwScanUnicastTbl *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwScanUnicastTbl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwScanUnicastTbl (pEntry->u4ContextId,
                                          pEntry->UcastCallBack);
            break;
        }
        case FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetStaticUcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStaticUcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetStaticUcastEntry (pEntry->u4ContextId,
                                               pEntry->u4Fid, pEntry->MacAddr,
                                               pEntry->u4Port,
                                               pEntry->pAllowedToGoPorts,
                                               pEntry->pu1Status);
            break;
        }
        case FS_MI_VLAN_HW_GET_SYNCED_TNL_PROTOCOL_MAC_ADDR:
        {
            tVlanNpWrFsMiVlanHwGetSyncedTnlProtocolMacAddr *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanHwGetSyncedTnlProtocolMacAddr;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetSyncedTnlProtocolMacAddr (pEntry->u4ContextId,
                                                       pEntry->u2Protocol,
                                                       pEntry->MacAddr);
            break;
        }
#endif /* L2RED_WANTED */
        case FS_MI_VLAN_HW_SET_PORT_ACC_FRAME_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPortAccFrameType *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortAccFrameType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortAccFrameType (pEntry->u4ContextId,
                                               pEntry->u4IfIndex,
                                               pEntry->u1AccFrameType);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_ING_FILTERING:
        {
            tVlanNpWrFsMiVlanHwSetPortIngFiltering *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortIngFiltering;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortIngFiltering (pEntry->u4ContextId,
                                               pEntry->u4IfIndex,
                                               pEntry->u1IngFilterEnable);
            break;
        }
        case FS_MI_VLAN_HW_VLAN_ENABLE:
        {
            tVlanNpWrFsMiVlanHwVlanEnable *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwVlanEnable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiVlanHwVlanEnable (pEntry->u4ContextId);
            break;
        }
        case FS_MI_VLAN_HW_VLAN_DISABLE:
        {
            tVlanNpWrFsMiVlanHwVlanDisable *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwVlanDisable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiVlanHwVlanDisable (pEntry->u4ContextId);
            break;
        }
        case FS_MI_VLAN_HW_SET_VLAN_LEARNING_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetVlanLearningType *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetVlanLearningType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetVlanLearningType (pEntry->u4ContextId,
                                               pEntry->u1LearningType);
            break;
        }
        case FS_MI_VLAN_HW_SET_MAC_BASED_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwSetMacBasedStatusOnPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMacBasedStatusOnPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetMacBasedStatusOnPort (pEntry->u4ContextId,
                                                   pEntry->u4IfIndex,
                                                   pEntry->
                                                   u1MacBasedVlanEnable);
            break;
        }
        case FS_MI_VLAN_HW_SET_SUBNET_BASED_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwSetSubnetBasedStatusOnPort *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetSubnetBasedStatusOnPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetSubnetBasedStatusOnPort (pEntry->u4ContextId,
                                                      pEntry->u4IfIndex,
                                                      pEntry->
                                                      u1SubnetBasedVlanEnable);
            break;
        }
        case FS_MI_VLAN_HW_ENABLE_PROTO_VLAN_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwEnableProtoVlanOnPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwEnableProtoVlanOnPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwEnableProtoVlanOnPort (pEntry->u4ContextId,
                                                 pEntry->u4IfIndex,
                                                 pEntry->u1VlanProtoEnable);
            break;
        }
        case FS_MI_VLAN_HW_SET_DEF_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanHwSetDefUserPriority *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetDefUserPriority;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetDefUserPriority (pEntry->u4ContextId,
                                              pEntry->u4IfIndex,
                                              pEntry->i4DefPriority);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_NUM_TRAF_CLASSES:
        {
            tVlanNpWrFsMiVlanHwSetPortNumTrafClasses *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortNumTrafClasses;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortNumTrafClasses (pEntry->u4ContextId,
                                                 pEntry->u4IfIndex,
                                                 pEntry->i4NumTraffClass);
            break;
        }
        case FS_MI_VLAN_HW_SET_REGEN_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanHwSetRegenUserPriority *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetRegenUserPriority;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetRegenUserPriority (pEntry->u4ContextId,
                                                pEntry->u4IfIndex,
                                                pEntry->i4UserPriority,
                                                pEntry->i4RegenPriority);
            break;
        }
        case FS_MI_VLAN_HW_SET_TRAFF_CLASS_MAP:
        {
            tVlanNpWrFsMiVlanHwSetTraffClassMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetTraffClassMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetTraffClassMap (pEntry->u4ContextId,
                                            pEntry->u4IfIndex,
                                            pEntry->i4UserPriority,
                                            pEntry->i4TraffClass);
            break;
        }
        case FS_MI_VLAN_HW_GMRP_ENABLE:
        {
            tVlanNpWrFsMiVlanHwGmrpEnable *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGmrpEnable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiVlanHwGmrpEnable (pEntry->u4ContextId);
            break;
        }
        case FS_MI_VLAN_HW_GMRP_DISABLE:
        {
            tVlanNpWrFsMiVlanHwGmrpDisable *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGmrpDisable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiVlanHwGmrpDisable (pEntry->u4ContextId);
            break;
        }
        case FS_MI_VLAN_HW_GVRP_ENABLE:
        {
            tVlanNpWrFsMiVlanHwGvrpEnable *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGvrpEnable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiVlanHwGvrpEnable (pEntry->u4ContextId);
            break;
        }
        case FS_MI_VLAN_HW_GVRP_DISABLE:
        {
            tVlanNpWrFsMiVlanHwGvrpDisable *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGvrpDisable;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiVlanHwGvrpDisable (pEntry->u4ContextId);
            break;
        }
        case FS_MI_NP_DELETE_ALL_FDB_ENTRIES:
        {
            tVlanNpWrFsMiNpDeleteAllFdbEntries *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiNpDeleteAllFdbEntries;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiNpDeleteAllFdbEntries (pEntry->u4ContextId);
            break;
        }
        case FS_MI_VLAN_HW_ADD_VLAN_PROTOCOL_MAP:
        {
            tVlanNpWrFsMiVlanHwAddVlanProtocolMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddVlanProtocolMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAddVlanProtocolMap (pEntry->u4ContextId,
                                              pEntry->u4IfIndex,
                                              pEntry->u4GroupId,
                                              pEntry->pProtoTemplate,
                                              pEntry->VlanId);
            break;
        }
        case FS_MI_VLAN_HW_DEL_VLAN_PROTOCOL_MAP:
        {
            tVlanNpWrFsMiVlanHwDelVlanProtocolMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelVlanProtocolMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDelVlanProtocolMap (pEntry->u4ContextId,
                                              pEntry->u4IfIndex,
                                              pEntry->u4GroupId,
                                              pEntry->pProtoTemplate);
            break;
        }
        case FS_MI_VLAN_HW_GET_PORT_STATS:
        {
            tVlanNpWrFsMiVlanHwGetPortStats *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetPortStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetPortStats (pEntry->u4ContextId, pEntry->u4Port,
                                        pEntry->VlanId, pEntry->u1StatsType,
                                        pEntry->pu4PortStatsValue);
            break;
        }
        case FS_MI_VLAN_HW_GET_PORT_STATS64:
        {
            tVlanNpWrFsMiVlanHwGetPortStats64 *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetPortStats64;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetPortStats64 (pEntry->u4ContextId, pEntry->u4Port,
                                          pEntry->VlanId, pEntry->u1StatsType,
                                          pEntry->pValue);
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_STATS:
        {
            tVlanNpWrFsMiVlanHwGetVlanStats *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetVlanStats (pEntry->u4ContextId, pEntry->VlanId,
                                        pEntry->u1StatsType,
                                        pEntry->pu4VlanStatsValue);
            break;
        }
        case FS_MI_VLAN_HW_RESET_VLAN_STATS:
        {
            tVlanNpWrFsMiVlanHwResetVlanStats *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetVlanStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwResetVlanStats (pEntry->u4ContextId, pEntry->VlanId);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_TUNNEL_MODE:
        {
            tVlanNpWrFsMiVlanHwSetPortTunnelMode *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortTunnelMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortTunnelMode (pEntry->u4ContextId,
                                             pEntry->u4IfIndex, pEntry->u4Mode);
            break;
        }
        case FS_MI_VLAN_HW_SET_TUNNEL_FILTER:
        {
            tVlanNpWrFsMiVlanHwSetTunnelFilter *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetTunnelFilter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetTunnelFilter (pEntry->u4ContextId,
                                           pEntry->i4BridgeMode);
            break;
        }
        case FS_MI_VLAN_HW_CHECK_TAG_AT_EGRESS:
        {
            tVlanNpWrFsMiVlanHwCheckTagAtEgress *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwCheckTagAtEgress;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwCheckTagAtEgress (pEntry->u4ContextId,
                                            pEntry->pu1TagSet);
            break;
        }
        case FS_MI_VLAN_HW_CHECK_TAG_AT_INGRESS:
        {
            tVlanNpWrFsMiVlanHwCheckTagAtIngress *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwCheckTagAtIngress;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwCheckTagAtIngress (pEntry->u4ContextId,
                                             pEntry->pu1TagSet);
            break;
        }
        case FS_MI_VLAN_HW_CREATE_FDB_ID:
        {
            tVlanNpWrFsMiVlanHwCreateFdbId *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwCreateFdbId;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwCreateFdbId (pEntry->u4ContextId, pEntry->u4Fid);
            break;
        }
        case FS_MI_VLAN_HW_DELETE_FDB_ID:
        {
            tVlanNpWrFsMiVlanHwDeleteFdbId *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeleteFdbId;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDeleteFdbId (pEntry->u4ContextId, pEntry->u4Fid);
            break;
        }
        case FS_MI_VLAN_HW_ASSOCIATE_VLAN_FDB:
        {
            tVlanNpWrFsMiVlanHwAssociateVlanFdb *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAssociateVlanFdb;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAssociateVlanFdb (pEntry->u4ContextId, pEntry->u4Fid,
                                            pEntry->VlanId);
            break;
        }
        case FS_MI_VLAN_HW_DISASSOCIATE_VLAN_FDB:
        {
            tVlanNpWrFsMiVlanHwDisassociateVlanFdb *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDisassociateVlanFdb;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDisassociateVlanFdb (pEntry->u4ContextId,
                                               pEntry->u4Fid, pEntry->VlanId);
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_ID:
        {
            tVlanNpWrFsMiVlanHwFlushPortFdbId *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPortFdbId;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwFlushPortFdbId (pEntry->u4ContextId, pEntry->u4Port,
                                          pEntry->u4Fid,
                                          pEntry->i4OptimizeFlag);
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT_FDB_LIST:
        {
            tVlanNpWrFsMiVlanHwFlushPortFdbList *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPortFdbList;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwFlushPortFdbList (pEntry->u4ContextId,
                                            pEntry->pVlanFlushInfo);
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_PORT:
        {
            tVlanNpWrFsMiVlanHwFlushPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwFlushPort (pEntry->u4ContextId, pEntry->u4IfIndex,
                                     pEntry->i4OptimizeFlag);
            break;
        }
        case FS_MI_VLAN_HW_FLUSH_FDB_ID:
        {
            tVlanNpWrFsMiVlanHwFlushFdbId *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwFlushFdbId;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwFlushFdbId (pEntry->u4ContextId, pEntry->u4Fid);
            break;
        }
        case FS_MI_VLAN_HW_SET_SHORT_AGEOUT:
        {
            tVlanNpWrFsMiVlanHwSetShortAgeout *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetShortAgeout;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetShortAgeout (pEntry->u4ContextId, pEntry->u4Port,
                                          pEntry->i4AgingTime);
            break;
        }
        case FS_MI_VLAN_HW_RESET_SHORT_AGEOUT:
        {
            tVlanNpWrFsMiVlanHwResetShortAgeout *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetShortAgeout;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwResetShortAgeout (pEntry->u4ContextId, pEntry->u4Port,
                                            pEntry->i4LongAgeout);
            break;
        }
        case FS_MI_VLAN_HW_GET_VLAN_INFO:
        {
            tVlanNpWrFsMiVlanHwGetVlanInfo *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetVlanInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetVlanInfo (pEntry->u4ContextId, pEntry->VlanId,
                                       pEntry->pHwEntry);
            break;
        }
        case FS_MI_VLAN_HW_GET_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetMcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetMcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetMcastEntry (pEntry->u4ContextId, pEntry->VlanId,
                                         pEntry->MacAddr,
                                         pEntry->pHwMcastPorts);
            break;
        }
        case FS_MI_VLAN_HW_TRAFF_CLASS_MAP_INIT:
        {
            tVlanNpWrFsMiVlanHwTraffClassMapInit *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwTraffClassMapInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwTraffClassMapInit (pEntry->u4ContextId,
                                             pEntry->u1Priority,
                                             pEntry->i4CosqValue);
            break;
        }
#ifndef BRIDGE_WANTED
        case FS_MI_BRG_SET_AGING_TIME:
        {
            tVlanNpWrFsMiBrgSetAgingTime *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiBrgSetAgingTime;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiBrgSetAgingTime (pEntry->u4ContextId, pEntry->i4AgingTime);
            break;
        }
#endif /* BRIDGE_WANTED */
        case FS_MI_VLAN_HW_SET_BRG_MODE:
        {
            tVlanNpWrFsMiVlanHwSetBrgMode *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetBrgMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetBrgMode (pEntry->u4ContextId,
                                      pEntry->u4BridgeMode);
            break;
        }
        case FS_MI_VLAN_HW_SET_BASE_BRIDGE_MODE:
        {
            tVlanNpWrFsMiVlanHwSetBaseBridgeMode *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetBaseBridgeMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetBaseBridgeMode (pEntry->u4IfIndex, pEntry->u4Mode);
            break;
        }
        case FS_MI_VLAN_HW_ADD_PORT_MAC_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddPortMacVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddPortMacVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAddPortMacVlanEntry (pEntry->u4ContextId,
                                               pEntry->u4IfIndex,
                                               pEntry->MacAddr, pEntry->VlanId,
                                               pEntry->bSuppressOption);
            break;
        }
        case FS_MI_VLAN_HW_DELETE_PORT_MAC_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDeletePortMacVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeletePortMacVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDeletePortMacVlanEntry (pEntry->u4ContextId,
                                                  pEntry->u4IfIndex,
                                                  pEntry->MacAddr);
            break;
        }
        case FS_MI_VLAN_HW_ADD_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddPortSubnetVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddPortSubnetVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAddPortSubnetVlanEntry (pEntry->u4ContextId,
                                                  pEntry->u4IfIndex,
                                                  pEntry->SubnetAddr,
                                                  pEntry->VlanId,
                                                  pEntry->bARPOption);
            break;
        }
        case FS_MI_VLAN_HW_DELETE_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDeletePortSubnetVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeletePortSubnetVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDeletePortSubnetVlanEntry (pEntry->u4ContextId,
                                                     pEntry->u4IfIndex,
                                                     pEntry->SubnetAddr);
            break;
        }
        case FS_MI_VLAN_HW_UPDATE_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwUpdatePortSubnetVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwUpdatePortSubnetVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwUpdatePortSubnetVlanEntry (pEntry->u4ContextId,
                                                     pEntry->u4IfIndex,
                                                     pEntry->u4SubnetAddr,
                                                     pEntry->u4SubnetMask,
                                                     pEntry->VlanId,
                                                     pEntry->bARPOption,
                                                     pEntry->u1Action);
            break;
        }
        case FS_MI_VLAN_NP_HW_RUN_MAC_AGEING:
        {
            tVlanNpWrFsMiVlanNpHwRunMacAgeing *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanNpHwRunMacAgeing;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiVlanNpHwRunMacAgeing (pEntry->u4ContextId);
            break;
        }
        case FS_MI_VLAN_HW_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanHwMacLearningLimit *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwMacLearningLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwMacLearningLimit (pEntry->u4ContextId, pEntry->VlanId,
                                            pEntry->u2FdbId,
                                            pEntry->u4MacLimit);
            break;
        }
        case FS_MI_VLAN_HW_SWITCH_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanHwSwitchMacLearningLimit *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSwitchMacLearningLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSwitchMacLearningLimit (pEntry->u4ContextId,
                                                  pEntry->u4MacLimit);
            break;
        }
        case FS_MI_VLAN_HW_MAC_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanHwMacLearningStatus *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwMacLearningStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwMacLearningStatus (pEntry->u4ContextId,
                                             pEntry->VlanId, pEntry->u2FdbId,
                                             pEntry->pHwEgressPorts,
                                             pEntry->u1Status);
            break;
        }
        case FS_MI_VLAN_HW_SET_FID_PORT_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetFidPortLearningStatus *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetFidPortLearningStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetFidPortLearningStatus (pEntry->u4ContextId,
                                                    pEntry->u4Fid,
                                                    pEntry->HwPortList,
                                                    pEntry->u4Port,
                                                    pEntry->u1Action);
            break;
        }
        case FS_MI_VLAN_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanHwSetProtocolTunnelStatusOnPort *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetProtocolTunnelStatusOnPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetProtocolTunnelStatusOnPort (pEntry->u4ContextId,
                                                         pEntry->u4IfIndex,
                                                         pEntry->ProtocolId,
                                                         pEntry->
                                                         u4TunnelStatus);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PROTECTED_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetPortProtectedStatus *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortProtectedStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortProtectedStatus (pEntry->u4ContextId,
                                                  pEntry->u4IfIndex,
                                                  pEntry->i4ProtectedStatus);
            break;
        }
        case FS_MI_VLAN_HW_ADD_STATIC_UCAST_ENTRY_EX:
        {
            tVlanNpWrFsMiVlanHwAddStaticUcastEntryEx *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddStaticUcastEntryEx;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAddStaticUcastEntryEx (pEntry->u4ContextId,
                                                 pEntry->u4Fid, pEntry->MacAddr,
                                                 pEntry->u4Port,
                                                 pEntry->pHwAllowedToGoPorts,
                                                 pEntry->u1Status,
                                                 pEntry->ConnectionId);
            break;
        }
        case FS_MI_VLAN_HW_GET_STATIC_UCAST_ENTRY_EX:
        {
            tVlanNpWrFsMiVlanHwGetStaticUcastEntryEx *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetStaticUcastEntryEx;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetStaticUcastEntryEx (pEntry->u4ContextId,
                                                 pEntry->u4Fid, pEntry->MacAddr,
                                                 pEntry->u4Port,
                                                 pEntry->pAllowedToGoPorts,
                                                 pEntry->pu1Status,
                                                 pEntry->ConnectionId);
            break;
        }
        case FS_VLAN_HW_FORWARD_PKT_ON_PORTS:
        {
            tVlanNpWrFsVlanHwForwardPktOnPorts *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsVlanHwForwardPktOnPorts;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsVlanHwForwardPktOnPorts (pEntry->pu1Packet,
                                           pEntry->u2PacketLen,
                                           pEntry->pVlanFwdInfo);
            break;
        }
        case FS_MI_VLAN_HW_PORT_MAC_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanHwPortMacLearningStatus *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwPortMacLearningStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwPortMacLearningStatus (pEntry->u4ContextId,
                                                 pEntry->u4IfIndex,
                                                 pEntry->u1Status);
            break;
        }
        case FS_VLAN_HW_GET_MAC_LEARNING_MODE:
        {
            tVlanNpWrFsVlanHwGetMacLearningMode *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsVlanHwGetMacLearningMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsVlanHwGetMacLearningMode (pEntry->pu4LearningMode);
            break;
        }
        case FS_MI_VLAN_HW_SET_MCAST_INDEX:
        {
            tVlanNpWrFsMiVlanHwSetMcastIndex *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetMcastIndex;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMiVlanHwSetMcastIndex (pEntry->HwMcastIndexInfo,
                                                pEntry->pu4McastIndex);
            break;
        }
#ifdef MBSM_WANTED
#ifdef PB_WANTED
        case FS_MI_VLAN_MBSM_HW_SET_PROVIDER_BRIDGE_PORT_TYPE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetProviderBridgePortType *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetProviderBridgePortType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetProviderBridgePortType (pEntry->u4ContextId,
                                                         pEntry->u4IfIndex,
                                                         pEntry->u4PortType,
                                                         pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_INGRESS_ETHER_TYPE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortIngressEtherType *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortIngressEtherType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortIngressEtherType (pEntry->u4ContextId,
                                                       pEntry->u4IfIndex,
                                                       pEntry->u2EtherType,
                                                       pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_EGRESS_ETHER_TYPE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortEgressEtherType *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortEgressEtherType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortEgressEtherType (pEntry->u4ContextId,
                                                      pEntry->u4IfIndex,
                                                      pEntry->u2EtherType,
                                                      pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortSVlanTranslationStatus *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->
                VlanNpFsMiVlanMbsmHwSetPortSVlanTranslationStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortSVlanTranslationStatus (pEntry->
                                                             u4ContextId,
                                                             pEntry->u4IfIndex,
                                                             pEntry->u1Status,
                                                             pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddSVlanTranslationEntry *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddSVlanTranslationEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwAddSVlanTranslationEntry (pEntry->u4ContextId,
                                                        pEntry->u4IfIndex,
                                                        pEntry->u2LocalSVlan,
                                                        pEntry->u2RelaySVlan,
                                                        pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortEtherTypeSwapStatus *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortEtherTypeSwapStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortEtherTypeSwapStatus (pEntry->u4ContextId,
                                                          pEntry->u4IfIndex,
                                                          pEntry->u1Status,
                                                          pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_ETHER_TYPE_SWAP_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddEtherTypeSwapEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddEtherTypeSwapEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwAddEtherTypeSwapEntry (pEntry->u4ContextId,
                                                     pEntry->u4IfIndex,
                                                     pEntry->u2LocalEtherType,
                                                     pEntry->u2RelayEtherType,
                                                     pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanMbsmHwAddSVlanMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddSVlanMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwAddSVlanMap (pEntry->u4ContextId,
                                           pEntry->VlanSVlanMap,
                                           pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortSVlanClassifyMethod *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortSVlanClassifyMethod;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortSVlanClassifyMethod (pEntry->u4ContextId,
                                                          pEntry->u4IfIndex,
                                                          pEntry->u1TableType,
                                                          pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanMbsmHwPortMacLearningLimit *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwPortMacLearningLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwPortMacLearningLimit (pEntry->u4ContextId,
                                                    pEntry->u4IfIndex,
                                                    pEntry->u4MacLimit,
                                                    pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_CUSTOMER_VLAN:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortCustomerVlan *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortCustomerVlan;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortCustomerVlan (pEntry->u4ContextId,
                                                   pEntry->u4IfIndex,
                                                   pEntry->CVlanId,
                                                   pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_CREATE_PROVIDER_EDGE_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwCreateProviderEdgePort *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwCreateProviderEdgePort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwCreateProviderEdgePort (pEntry->u4ContextId,
                                                      pEntry->u4IfIndex,
                                                      pEntry->SVlanId,
                                                      pEntry->PepConfig,
                                                      pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PCP_ENCOD_TBL:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPcpEncodTbl *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPcpEncodTbl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPcpEncodTbl (pEntry->u4ContextId,
                                              pEntry->u4IfIndex,
                                              pEntry->NpPbVlanPcpInfo,
                                              pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PCP_DECOD_TBL:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPcpDecodTbl *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPcpDecodTbl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPcpDecodTbl (pEntry->u4ContextId,
                                              pEntry->u4IfIndex,
                                              pEntry->NpPbVlanPcpInfo,
                                              pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_USE_DEI:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortUseDei *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortUseDei;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortUseDei (pEntry->u4ContextId,
                                             pEntry->u4IfIndex,
                                             pEntry->u1UseDei,
                                             pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_REQ_DROP_ENCODING:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortReqDropEncoding *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortReqDropEncoding;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortReqDropEncoding (pEntry->u4ContextId,
                                                      pEntry->u4IfIndex,
                                                      pEntry->u1ReqDrpEncoding,
                                                      pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PCP_SELECTION:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortPcpSelection *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortPcpSelection;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortPcpSelection (pEntry->u4ContextId,
                                                   pEntry->u4IfIndex,
                                                   pEntry->u2PcpSelection,
                                                   pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_SERVICE_PRI_REGEN_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwSetServicePriRegenEntry *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetServicePriRegenEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetServicePriRegenEntry (pEntry->u4ContextId,
                                                       pEntry->u4IfIndex,
                                                       pEntry->SVlanId,
                                                       pEntry->i4RecvPriority,
                                                       pEntry->i4RegenPriority,
                                                       pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_MULTICAST_MAC_TABLE_LIMIT:
        {
            tVlanNpWrFsMiVlanMbsmHwMulticastMacTableLimit *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwMulticastMacTableLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwMulticastMacTableLimit (pEntry->u4ContextId,
                                                      pEntry->u4MacLimit,
                                                      pEntry->pSlotInfo);
            break;
        }
#endif /* PB_WANTED */
        case FS_MI_VLAN_MBSM_HW_SET_BRG_MODE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetBrgMode *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetBrgMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetBrgMode (pEntry->u4ContextId,
                                          pEntry->u4BridgeMode,
                                          pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PROPERTY:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortProperty *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortProperty;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortProperty (pEntry->u4ContextId,
                                               pEntry->u4IfIndex,
                                               pEntry->VlanPortProperty,
                                               pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_INIT:
        {
            tVlanNpWrFsMiVlanMbsmHwInit *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwInit (pEntry->u4ContextId, pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_DEFAULT_VLAN_ID:
        {
            tVlanNpWrFsMiVlanMbsmHwSetDefaultVlanId *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetDefaultVlanId;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetDefaultVlanId (pEntry->u4ContextId,
                                                pEntry->VlanId,
                                                pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddStaticUcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddStaticUcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwAddStaticUcastEntry (pEntry->u4ContextId,
                                                   pEntry->u4FdbId,
                                                   pEntry->MacAddr,
                                                   pEntry->u4RcvPort,
                                                   pEntry->pHwAllowedToGoPorts,
                                                   pEntry->u1Status,
                                                   pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_UCAST_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwSetUcastPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetUcastPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetUcastPort (pEntry->u4ContextId,
                                            pEntry->u4FdbId, pEntry->MacAddr,
                                            pEntry->u4RcvPort,
                                            pEntry->u4AllowedToGoPort,
                                            pEntry->u1Status,
                                            pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_RESET_UCAST_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwResetUcastPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwResetUcastPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwResetUcastPort (pEntry->u4ContextId,
                                              pEntry->u4FdbId, pEntry->MacAddr,
                                              pEntry->u4RcvPort,
                                              pEntry->u4AllowedToGoPort,
                                              pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_DEL_STATIC_UCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwDelStaticUcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwDelStaticUcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwDelStaticUcastEntry (pEntry->u4ContextId,
                                                   pEntry->u4Fid,
                                                   pEntry->MacAddr,
                                                   pEntry->u4RcvPort,
                                                   pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddMcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddMcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwAddMcastEntry (pEntry->u4ContextId,
                                             pEntry->VlanId, pEntry->MacAddr,
                                             pEntry->pHwMcastPorts,
                                             pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_MCAST_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwSetMcastPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetMcastPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetMcastPort (pEntry->u4ContextId, pEntry->VlanId,
                                            pEntry->MacAddr, pEntry->u4Port,
                                            pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_RESET_MCAST_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwResetMcastPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwResetMcastPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwResetMcastPort (pEntry->u4ContextId,
                                              pEntry->VlanId, pEntry->MacAddr,
                                              pEntry->u4Port,
                                              pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddVlanEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwAddVlanEntry (pEntry->u4ContextId, pEntry->VlanId,
                                            pEntry->pHwEgressPorts,
                                            pEntry->pHwUnTagPorts,
                                            pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_VLAN_MEMBER_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwSetVlanMemberPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetVlanMemberPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetVlanMemberPort (pEntry->u4ContextId,
                                                 pEntry->VlanId, pEntry->u4Port,
                                                 pEntry->u1IsTagged,
                                                 pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_RESET_VLAN_MEMBER_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwResetVlanMemberPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwResetVlanMemberPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwResetVlanMemberPort (pEntry->u4ContextId,
                                                   pEntry->VlanId,
                                                   pEntry->u4Port,
                                                   pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PVID:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortPvid *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortPvid;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortPvid (pEntry->u4ContextId, pEntry->u4Port,
                                           pEntry->VlanId, pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ACC_FRAME_TYPE:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortAccFrameType *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortAccFrameType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortAccFrameType (pEntry->u4ContextId,
                                                   pEntry->u4Port,
                                                   pEntry->u1AccFrameType,
                                                   pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_ING_FILTERING:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortIngFiltering *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortIngFiltering;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortIngFiltering (pEntry->u4ContextId,
                                                   pEntry->u4Port,
                                                   pEntry->u1IngFilterEnable,
                                                   pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_DEF_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanMbsmHwSetDefUserPriority *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetDefUserPriority;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetDefUserPriority (pEntry->u4ContextId,
                                                  pEntry->u4Port,
                                                  pEntry->i4DefPriority,
                                                  pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_TRAFF_CLASS_MAP_INIT:
        {
            tVlanNpWrFsMiVlanMbsmHwTraffClassMapInit *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwTraffClassMapInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwTraffClassMapInit (pEntry->u4ContextId,
                                                 pEntry->u1Priority,
                                                 pEntry->i4CosqValue,
                                                 pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_TRAFF_CLASS_MAP:
        {
            tVlanNpWrFsMiVlanMbsmHwSetTraffClassMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetTraffClassMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetTraffClassMap (pEntry->u4ContextId,
                                                pEntry->u4Port,
                                                pEntry->i4UserPriority,
                                                pEntry->i4TraffClass,
                                                pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddStMcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddStMcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwAddStMcastEntry (pEntry->u4ContextId,
                                               pEntry->VlanId,
                                               pEntry->McastAddr,
                                               pEntry->i4RcvPort,
                                               pEntry->pHwMcastPorts,
                                               pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_DEL_ST_MCAST_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwDelStMcastEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwDelStMcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwDelStMcastEntry (pEntry->u4ContextId,
                                               pEntry->VlanId, pEntry->MacAddr,
                                               pEntry->u4RcvPort,
                                               pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_NUM_TRAF_CLASSES:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortNumTrafClasses *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortNumTrafClasses;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortNumTrafClasses (pEntry->u4ContextId,
                                                     pEntry->u4Port,
                                                     pEntry->i4NumTraffClass,
                                                     pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_REGEN_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanMbsmHwSetRegenUserPriority *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetRegenUserPriority;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetRegenUserPriority (pEntry->u4ContextId,
                                                    pEntry->u4Port,
                                                    pEntry->i4UserPriority,
                                                    pEntry->i4RegenPriority,
                                                    pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_SUBNET_BASED_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwSetSubnetBasedStatusOnPort *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetSubnetBasedStatusOnPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetSubnetBasedStatusOnPort (pEntry->u4ContextId,
                                                          pEntry->u4Port,
                                                          pEntry->
                                                          u1VlanSubnetEnable,
                                                          pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ENABLE_PROTO_VLAN_ON_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwEnableProtoVlanOnPort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwEnableProtoVlanOnPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwEnableProtoVlanOnPort (pEntry->u4ContextId,
                                                     pEntry->u4Port,
                                                     pEntry->u1VlanProtoEnable,
                                                     pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_ALL_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetAllGroupsPorts *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetAllGroupsPorts;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetAllGroupsPorts (pEntry->u4ContextId,
                                                 pEntry->VlanId,
                                                 pEntry->pHwAllGroupPorts,
                                                 pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_UN_REG_GROUPS_PORTS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetUnRegGroupsPorts *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetUnRegGroupsPorts;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetUnRegGroupsPorts (pEntry->u4ContextId,
                                                   pEntry->VlanId,
                                                   pEntry->pHwUnRegPorts,
                                                   pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_TUNNEL_FILTER:
        {
            tVlanNpWrFsMiVlanMbsmHwSetTunnelFilter *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetTunnelFilter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetTunnelFilter (pEntry->u4ContextId,
                                               pEntry->i4BridgeMode,
                                               pEntry->MacAddr,
                                               pEntry->u2ProtocolId,
                                               pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_PORT_SUBNET_VLAN_ENTRY:
        {
            tVlanNpWrFsMiVlanMbsmHwAddPortSubnetVlanEntry *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddPortSubnetVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwAddPortSubnetVlanEntry (pEntry->u4ContextId,
                                                      pEntry->u4Port,
                                                      pEntry->u4SubnetAddr,
                                                      pEntry->VlanId,
                                                      pEntry->u1ArpOption,
                                                      pEntry->pSlotInfo);
            break;
        }
#ifndef BRIDGE_WANTED
        case FS_MI_BRG_MBSM_SET_AGING_TIME:
        {
            tVlanNpWrFsMiBrgMbsmSetAgingTime *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiBrgMbsmSetAgingTime;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiBrgMbsmSetAgingTime (pEntry->u4ContextId,
                                         pEntry->i4AgingTime,
                                         pEntry->pSlotInfo);
            break;
        }
#endif /* BRIDGE_WANTED */
        case FS_MI_VLAN_MBSM_HW_SET_FID_PORT_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetFidPortLearningStatus *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetFidPortLearningStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetFidPortLearningStatus (pEntry->u4ContextId,
                                                        pEntry->u4Fid,
                                                        pEntry->u4Port,
                                                        pEntry->u1Action,
                                                        pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PROTOCOL_TUNNEL_STATUS_ON_PORT:
        {
            tVlanNpWrFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->
                VlanNpFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetProtocolTunnelStatusOnPort (pEntry->
                                                             u4ContextId,
                                                             pEntry->u4IfIndex,
                                                             pEntry->ProtocolId,
                                                             pEntry->
                                                             u4TunnelStatus,
                                                             pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SET_PORT_PROTECTED_STATUS:
        {
            tVlanNpWrFsMiVlanMbsmHwSetPortProtectedStatus *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetPortProtectedStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSetPortProtectedStatus (pEntry->u4ContextId,
                                                      pEntry->u4IfIndex,
                                                      pEntry->u4ProtectedStatus,
                                                      pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_SWITCH_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanMbsmHwSwitchMacLearningLimit *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSwitchMacLearningLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwSwitchMacLearningLimit (pEntry->u4ContextId,
                                                      pEntry->u4MacLimit,
                                                      pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_PORT_MAC_LEARNING_STATUS:
        {
            tVlanNpWrFsMiVlanMbsmHwPortMacLearningStatus *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwPortMacLearningStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwPortMacLearningStatus (pEntry->u4ContextId,
                                                     pEntry->u4IfIndex,
                                                     pEntry->u1Status,
                                                     pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanMbsmHwMacLearningLimit *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwMacLearningLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwMacLearningLimit (pEntry->u4ContextId,
                                                pEntry->VlanId, pEntry->u2FdbId,
                                                pEntry->u4MacLimit,
                                                pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_HW_ADD_STATIC_UCAST_ENTRY_EX:
        {
            tVlanNpWrFsMiVlanMbsmHwAddStaticUcastEntryEx *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwAddStaticUcastEntryEx;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanMbsmHwAddStaticUcastEntryEx (pEntry->u4ContextId,
                                                     pEntry->u4FdbId,
                                                     pEntry->MacAddr,
                                                     pEntry->u4RcvPort,
                                                     pEntry->
                                                     pHwAllowedToGoPorts,
                                                     pEntry->u1Status,
                                                     pEntry->ConnectionId,
                                                     pEntry->pSlotInfo);
            break;
        }
        case FS_MI_VLAN_MBSM_SYNC_F_D_B_INFO:
        {
            /* No NPAPI to be invoked. Since the function invoked
             * with this opcode has to be always executed in the remote
             * unit only. Invocation in local unit not required. */
            tVlanNpWrFsMiVlanMbsmSyncFDBInfo *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmSyncFDBInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            break;
        }

        case FS_MI_VLAN_MBSM_HW_EVB_CONFIG_S_CH_IFACE:
        {
            tVlanNpWrFsMiVlanMbsmHwEvbConfigSChIface *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwEvbConfigSChIface;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }

            u1RetVal = FsMiVlanHwEvbConfigSChIface (&pEntry->VlanEvbHwConfigInfo);

            break;
        }


        case FS_MI_VLAN_MBSM_HW_SET_BRIDGE_PORT_TYPE:
        {
               tVlanNpWrFsMiVlanMbsmHwSetBridgePortType *pEntry = NULL;
               pEntry = &pVlanNpModInfo->VlanNpFsMiVlanMbsmHwSetBridgePortType;
               if(NULL == pEntry)
               {
                   u1RetVal = FNP_FAILURE;
                   break;
               }
               u1RetVal =
                   FsMiVlanHwSetBridgePortType (&pEntry->VlanHwPortInfo);
               break;
        }

#endif /* MBSM_WANTED */
        case FS_MI_VLAN_HW_SET_PORT_INGRESS_ETHER_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPortIngressEtherType *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortIngressEtherType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortIngressEtherType (pEntry->u4ContextId,
                                                   pEntry->u4IfIndex,
                                                   pEntry->u2EtherType);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_EGRESS_ETHER_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPortEgressEtherType *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortEgressEtherType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortEgressEtherType (pEntry->u4ContextId,
                                                  pEntry->u4IfIndex,
                                                  pEntry->u2EtherType);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PROPERTY:
        {
            tVlanNpWrFsMiVlanHwSetPortProperty *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortProperty;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortProperty (pEntry->u4ContextId,
                                           pEntry->u4IfIndex,
                                           pEntry->VlanPortProperty);
            break;
        }

        case FS_MI_VLAN_HW_SET_LOOPBACK_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetVlanLoopbackStatus *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetVlanLoopbackStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetVlanLoopbackStatus (pEntry->u4ContextId,
                                                 pEntry->VlanId,
                                                 pEntry->i4LoopbackStatus);
            break;
        }

#ifdef PB_WANTED
        case FS_MI_VLAN_HW_SET_PROVIDER_BRIDGE_PORT_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetProviderBridgePortType *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetProviderBridgePortType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetProviderBridgePortType (pEntry->u4ContextId,
                                                     pEntry->u4IfIndex,
                                                     pEntry->u4PortType);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_S_VLAN_TRANSLATION_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetPortSVlanTranslationStatus *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortSVlanTranslationStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortSVlanTranslationStatus (pEntry->u4ContextId,
                                                         pEntry->u4IfIndex,
                                                         pEntry->u1Status);
            break;
        }
        case FS_MI_VLAN_HW_ADD_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddSVlanTranslationEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddSVlanTranslationEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAddSVlanTranslationEntry (pEntry->u4ContextId,
                                                    pEntry->u4IfIndex,
                                                    pEntry->u2LocalSVlan,
                                                    pEntry->u2RelaySVlan);
            break;
        }
        case FS_MI_VLAN_HW_DEL_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelSVlanTranslationEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelSVlanTranslationEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDelSVlanTranslationEntry (pEntry->u4ContextId,
                                                    pEntry->u4IfIndex,
                                                    pEntry->u2LocalSVlan,
                                                    pEntry->u2RelaySVlan);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_ETHER_TYPE_SWAP_STATUS:
        {
            tVlanNpWrFsMiVlanHwSetPortEtherTypeSwapStatus *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortEtherTypeSwapStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortEtherTypeSwapStatus (pEntry->u4ContextId,
                                                      pEntry->u4IfIndex,
                                                      pEntry->u1Status);
            break;
        }
        case FS_MI_VLAN_HW_ADD_ETHER_TYPE_SWAP_ENTRY:
        {
            tVlanNpWrFsMiVlanHwAddEtherTypeSwapEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddEtherTypeSwapEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAddEtherTypeSwapEntry (pEntry->u4ContextId,
                                                 pEntry->u4IfIndex,
                                                 pEntry->u2LocalEtherType,
                                                 pEntry->u2RelayEtherType);
            break;
        }
        case FS_MI_VLAN_HW_DEL_ETHER_TYPE_SWAP_ENTRY:
        {
            tVlanNpWrFsMiVlanHwDelEtherTypeSwapEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelEtherTypeSwapEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDelEtherTypeSwapEntry (pEntry->u4ContextId,
                                                 pEntry->u4IfIndex,
                                                 pEntry->u2LocalEtherType,
                                                 pEntry->u2RelayEtherType);
            break;
        }
        case FS_MI_VLAN_HW_ADD_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwAddSVlanMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwAddSVlanMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwAddSVlanMap (pEntry->u4ContextId,
                                       pEntry->VlanSVlanMap);
            break;
        }
        case FS_MI_VLAN_HW_DELETE_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwDeleteSVlanMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDeleteSVlanMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDeleteSVlanMap (pEntry->u4ContextId,
                                          pEntry->VlanSVlanMap);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_S_VLAN_CLASSIFY_METHOD:
        {
            tVlanNpWrFsMiVlanHwSetPortSVlanClassifyMethod *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortSVlanClassifyMethod;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortSVlanClassifyMethod (pEntry->u4ContextId,
                                                      pEntry->u4IfIndex,
                                                      pEntry->u1TableType);
            break;
        }
        case FS_MI_VLAN_HW_PORT_MAC_LEARNING_LIMIT:
        {
            tVlanNpWrFsMiVlanHwPortMacLearningLimit *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwPortMacLearningLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwPortMacLearningLimit (pEntry->u4ContextId,
                                                pEntry->u4IfIndex,
                                                pEntry->u4MacLimit);
            break;
        }
        case FS_MI_VLAN_HW_PORT_UNICAST_MAC_SEC_TYPE:
        {
            tVlanNpWrFsMiVlanHwPortUnicastMacSecType *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwPortUnicastMacSecType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwPortUnicastMacSecType (pEntry->u4ContextId,
                                                pEntry->u4IfIndex,
                                                pEntry->i4MacSecType);
            break;
        }

        case FS_MI_VLAN_HW_MULTICAST_MAC_TABLE_LIMIT:
        {
            tVlanNpWrFsMiVlanHwMulticastMacTableLimit *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwMulticastMacTableLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwMulticastMacTableLimit (pEntry->u4ContextId,
                                                  pEntry->u4MacLimit);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_CUSTOMER_VLAN:
        {
            tVlanNpWrFsMiVlanHwSetPortCustomerVlan *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortCustomerVlan;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortCustomerVlan (pEntry->u4ContextId,
                                               pEntry->u4IfIndex,
                                               pEntry->CVlanId);
            break;
        }
        case FS_MI_VLAN_HW_RESET_PORT_CUSTOMER_VLAN:
        {
            tVlanNpWrFsMiVlanHwResetPortCustomerVlan *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwResetPortCustomerVlan;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwResetPortCustomerVlan (pEntry->u4ContextId,
                                                 pEntry->u4IfIndex);
            break;
        }
        case FS_MI_VLAN_HW_CREATE_PROVIDER_EDGE_PORT:
        {
            tVlanNpWrFsMiVlanHwCreateProviderEdgePort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwCreateProviderEdgePort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwCreateProviderEdgePort (pEntry->u4ContextId,
                                                  pEntry->u4IfIndex,
                                                  pEntry->SVlanId,
                                                  pEntry->PepConfig);
            break;
        }
        case FS_MI_VLAN_HW_DEL_PROVIDER_EDGE_PORT:
        {
            tVlanNpWrFsMiVlanHwDelProviderEdgePort *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwDelProviderEdgePort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwDelProviderEdgePort (pEntry->u4ContextId,
                                               pEntry->u4IfIndex,
                                               pEntry->SVlanId);
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_PVID:
        {
            tVlanNpWrFsMiVlanHwSetPepPvid *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepPvid;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPepPvid (pEntry->u4ContextId, pEntry->u4IfIndex,
                                      pEntry->SVlanId, pEntry->Pvid);
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_ACC_FRAME_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetPepAccFrameType *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepAccFrameType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPepAccFrameType (pEntry->u4ContextId,
                                              pEntry->u4IfIndex,
                                              pEntry->SVlanId,
                                              pEntry->u1AccepFrameType);
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_DEF_USER_PRIORITY:
        {
            tVlanNpWrFsMiVlanHwSetPepDefUserPriority *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepDefUserPriority;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPepDefUserPriority (pEntry->u4ContextId,
                                                 pEntry->u4IfIndex,
                                                 pEntry->SVlanId,
                                                 pEntry->i4DefUsrPri);
            break;
        }
        case FS_MI_VLAN_HW_SET_PEP_ING_FILTERING:
        {
            tVlanNpWrFsMiVlanHwSetPepIngFiltering *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPepIngFiltering;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPepIngFiltering (pEntry->u4ContextId,
                                              pEntry->u4IfIndex,
                                              pEntry->SVlanId,
                                              pEntry->u1IngFilterEnable);
            break;
        }
        case FS_MI_VLAN_HW_SET_CVID_UNTAG_PEP:
        {
            tVlanNpWrFsMiVlanHwSetCvidUntagPep *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCvidUntagPep;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetCvidUntagPep (pEntry->u4ContextId,
                                           pEntry->VlanSVlanMap);
            break;
        }
        case FS_MI_VLAN_HW_SET_CVID_UNTAG_CEP:
        {
            tVlanNpWrFsMiVlanHwSetCvidUntagCep *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCvidUntagCep;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetCvidUntagCep (pEntry->u4ContextId,
                                           pEntry->VlanSVlanMap);
            break;
        }
        case FS_MI_VLAN_HW_SET_PCP_ENCOD_TBL:
        {
            tVlanNpWrFsMiVlanHwSetPcpEncodTbl *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPcpEncodTbl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPcpEncodTbl (pEntry->u4ContextId,
                                          pEntry->u4IfIndex,
                                          pEntry->NpPbVlanPcpInfo);
            break;
        }
        case FS_MI_VLAN_HW_SET_PCP_DECOD_TBL:
        {
            tVlanNpWrFsMiVlanHwSetPcpDecodTbl *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPcpDecodTbl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPcpDecodTbl (pEntry->u4ContextId,
                                          pEntry->u4IfIndex,
                                          pEntry->NpPbVlanPcpInfo);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_USE_DEI:
        {
            tVlanNpWrFsMiVlanHwSetPortUseDei *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortUseDei;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortUseDei (pEntry->u4ContextId, pEntry->u4IfIndex,
                                         pEntry->u1UseDei);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_REQ_DROP_ENCODING:
        {
            tVlanNpWrFsMiVlanHwSetPortReqDropEncoding *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortReqDropEncoding;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortReqDropEncoding (pEntry->u4ContextId,
                                                  pEntry->u4IfIndex,
                                                  pEntry->u1ReqDrpEncoding);
            break;
        }
        case FS_MI_VLAN_HW_SET_PORT_PCP_SELECTION:
        {
            tVlanNpWrFsMiVlanHwSetPortPcpSelection *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetPortPcpSelection;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetPortPcpSelection (pEntry->u4ContextId,
                                               pEntry->u4IfIndex,
                                               pEntry->u2PcpSelection);
            break;
        }
        case FS_MI_VLAN_HW_SET_SERVICE_PRI_REGEN_ENTRY:
        {
            tVlanNpWrFsMiVlanHwSetServicePriRegenEntry *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetServicePriRegenEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetServicePriRegenEntry (pEntry->u4ContextId,
                                                   pEntry->u4IfIndex,
                                                   pEntry->SVlanId,
                                                   pEntry->i4RecvPriority,
                                                   pEntry->i4RegenPriority);
            break;
        }
        case FS_MI_VLAN_HW_SET_TUNNEL_MAC_ADDRESS:
        {
            tVlanNpWrFsMiVlanHwSetTunnelMacAddress *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetTunnelMacAddress;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetTunnelMacAddress (pEntry->u4ContextId,
                                               pEntry->MacAddr,
                                               pEntry->u2Protocol);
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_S_VLAN_MAP:
        {
            tVlanNpWrFsMiVlanHwGetNextSVlanMap *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextSVlanMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetNextSVlanMap (pEntry->u4ContextId,
                                           pEntry->VlanSVlanMap,
                                           pEntry->pRetVlanSVlanMap);
            break;
        }
        case FS_MI_VLAN_HW_GET_NEXT_S_VLAN_TRANSLATION_ENTRY:
        {
            tVlanNpWrFsMiVlanHwGetNextSVlanTranslationEntry *pEntry = NULL;
            pEntry =
                &pVlanNpModInfo->VlanNpFsMiVlanHwGetNextSVlanTranslationEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetNextSVlanTranslationEntry (pEntry->u4ContextId,
                                                        pEntry->u4IfIndex,
                                                        pEntry->u2LocalSVlan,
                                                        pEntry->
                                                        pVidTransEntryInfo);
            break;
        }

        case FS_MI_VLAN_HW_SET_EVC_ATTRIBUTE:
        {
            tVlanNpWrFsMiVlanHwSetEvcAttribute *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetEvcAttribute;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetEvcAttribute (pEntry->i4FsEvcContextId,
                                           pEntry->u4Action,
                                           pEntry->pIssEvcInfo);
            break;
        }
        
        case FS_MI_VLAN_HW_SET_CVLAN_STAT:
        {
            tVlanNpWrFsMiVlanHwSetCVlanStat *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetCVlanStat;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetCVlanStat (pEntry->VlanStat);
            break;
        }

        case FS_MI_VLAN_HW_GET_CVLAN_STAT:
        {
            tVlanNpWrFsMiVlanHwGetCVlanStat *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwGetCVlanStat;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwGetCVlanStat (   pEntry->u2Port,
                                           pEntry->u2CVlanId,
                                           pEntry->u1StatsType,
                                           (pEntry->pu4VlanStatsValue));
            break;
        }


        case FS_MI_VLAN_HW_CLEAR_CVLAN_STAT:
        {
            tVlanNpWrFsMiVlanHwClearCVlanStat *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwClearCVlanStat;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwClearCVlanStat ( pEntry->u2Port,
                                           pEntry->u2CVlanId);
            break;
        }
      
#endif /* PB_WANTED */

        case FS_MI_VLAN_HW_PORT_PKT_REFLECT_STATUS:
        {
            tVlanNpWrFsMiVlanHwPortPktReflectStatus *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwPortPktReflectStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwPortPktReflectStatus (pEntry->pPortReflectEntry);
            break;
        }

        case FS_MI_VLAN_HW_EVB_CONFIG_S_CH_IFACE:
        {
            tVlanNpWrFsMiVlanHwEvbConfigSChIface *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwEvbConfigSChIface;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwEvbConfigSChIface (&pEntry->VlanEvbHwConfigInfo);
            break;
        }

   case FS_MI_VLAN_HW_SET_BRIDGE_PORT_TYPE:
        {
            tVlanNpWrFsMiVlanHwSetBridgePortType *pEntry = NULL;
            pEntry = &pVlanNpModInfo->VlanNpFsMiVlanHwSetBridgePortType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiVlanHwSetBridgePortType (&pEntry->VlanHwPortInfo);
            break;
        }

        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __VLHWNPWR_C__ */
