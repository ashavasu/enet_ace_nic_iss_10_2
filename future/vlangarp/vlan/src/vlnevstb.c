/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevstb.c,v 1.3 2016/06/01 09:50:58 siva Exp $
 *
 * Description: This file contains EVB Stub functions. 
 *
 *******************************************************************/
#ifndef __VLNEVSTB_C__
#define __VLNEVSTB_C__

#include "vlaninc.h"

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfUpdateLocalPort                          */
/*                                                                           */
/* Description        : This function updates the local port number for the  */
/*                      S-Channel interface. Local port number is derived    */
/*                      from VCM whereas at the time of creating of S-Channel*/ 
/*                      interface in EVB, it is not created in VCM and hence */
/*                      the local port would be set as zero initially.After  */ 
/*                      it s created in CFA & VCM, the port create indication*/
/*                      for S-Channel interface would be coming from L2IWF to*/
/*                      VLAN and here the local port is updated.             */
/*                                                                           */
/* Input(s)           : u4SChIfIndex - SBP interface index                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4 
VlanEvbSChIfUpdateLocalPort (UINT4 u4SChIfIndex)
{
    UNUSED_PARAM (u4SChIfIndex);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfSetDefProperties                         */
/*                                                                           */
/* Description        : This function sets the following port properties for */
/*                      the given UAP port in tVlanPortEntry.                */
/*                      -Admit all frames.                                   */
/*                      -PVID parameter equal to the default S-chan SVID-1   */
/*                      -Should be included in the member set and un-tagged  */
/*                      set for the default S-channel S-VID-1. This          */
/*                      operation helps to transmit out default S-Channel    */
/*                      packets out of UAP without S-tag.                    */
/*                      -Default TPID for the port as 0x88a8                 */
/*                                                                           */
/* Input(s)           : pPortEntry - VLAN port entry                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanEvbUapIfSetDefProperties (tVlanPortEntry *pVlanPortEntry)
{
    UNUSED_PARAM(pVlanPortEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfWrAddEntry                               */
/*                                                                           */
/* Description        : This routine adds a UAP interface entry in           */
/*                      gEvbGlobalInfo.EvbUapIfTree if EVB is enabled.       */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP interface index                   */
/*                                                                           */
/* Output(s)          : ppEvbUapIfEntry - pointer to pEvbUapIfEntry          */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbUapIfWrAddEntry (UINT4 u4UapIfIndex)
{
    UNUSED_PARAM(u4UapIfIndex);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfSetAdminStatus                           */
/*                                                                           */
/* Description        : This function sets the admins status of the S-Channel*/
/*                      interface. Before that it does the following actions.*/
/*                      When the status is UP, S-Channel interface is        */
/*                      programmed in the hardware.When the status is DOWN,  */
/*                      -Deletes the S-Channelinterface entry fromthehardware*/
/*                      -Does not remove this (SCID, SVID) pair from CDCP TLV*/
/*                      that are already in progress. This utility gives the */
/*                      provision to the administrator to make a particular  */
/*                      S-Channel UP or DOWN when all other S-Channels are   */
/*                      active in an UAP.Though the argument is said as      */
/*                      u1OperStatus, since the S-Channel interfaceisalogical*/
/*                      interface; it actually holds the admin status.Whereas*/
/*                      the real operational status for the SChannelinterface*/
/*                      would depend on the underlying UAP. operationalstatus*/
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP interface index                   */
/*                      u4SVId - SVID                                        */
/*                                                                           */
/* Output(s)          : u4IfIndex - Unique interface index if available      */
/*                      zero - otherwise                                     */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
INT4
VlanEvbSChSetAdminStatus (UINT4 u4SChIfIndex, UINT1 u1Status)
{
    UNUSED_PARAM(u4SChIfIndex);
    UNUSED_PARAM(u1Status);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbUapSetOperStatus                              */
/*                                                                           */
/* Description        : This function updates the S-Channel Handler and      */
/*                      CDCP handler about the operational status change.    */
/*                      It sets the UAP port's CDCP running status and       */
/*                      remote UAP port's CDCP status.                       */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP Interface Index                   */
/*                      u1OperStatus - CFA_IF_UP/CFA_IF_DOWN.                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbUapSetOperStatus (UINT4 u4UapIfIndex, UINT1 u1OperStatus)
{
        UNUSED_PARAM(u4UapIfIndex);
        UNUSED_PARAM(u1OperStatus);
        return VLAN_SUCCESS;
}


/***************************************************************************
 *  FUNCTION NAME : VlanEvbCdcpProcessTlv
 *
 *  DESCRIPTION   : This function is used to process CDCP TLV received on the
 *                  UAP and take actions as following when EVB is enabled.
 *  INPUT         : pMsg - Queue message received from VLAN queue.  
 *  OUTPUT        : None                                                     
 *  RETURNS       : VLAN_SUCCESS/VLAN_FAILURE 
 * **************************************************************************/
INT4
VlanEvbCdcpProcessTlv (tEvbQueMsg * pMsg) 
{
    UNUSED_PARAM(pMsg);
    return VLAN_SUCCESS;
}                                                         

/*****************************************************************************/
/* Function Name      : VlanEvbUapIfDelEntry                                 */
/* Description        : This routine deletes a UAP If Entry and also deletes */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP If Index.                         */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbUapIfDelEntry (UINT4 u4UapIfIndex)
{
    UNUSED_PARAM(u4UapIfIndex);
    return VLAN_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : VlanEvbGetSystemStatus                               */
/*                                                                           */
/* Description        : This function is used to call from VLAN API          */
/*                      to get the system status of EVB. this will be used   */
/*                      when other modules try to check EVB status.          */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_TRUE if EVB is running on the context           */
/*                      VLAN_FALSE otherwise                                 */
/*****************************************************************************/
UINT1
VlanEvbGetSystemStatus (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return VLAN_FALSE;
}
/*****************************************************************************/
/* Function Name      : VlanEvbStart                                         */
/*                                                                           */
/* Description        :This function does the following tasks:               */
/*                     - creates the context data base for u4ContextId.      */
/*                     For the first context creation in EVB, the below tasks*/
/*                     - Initializes the global variables.                   */
/*                     - Registers EVB MIBs with SNMP module.                */
/*                     - Creates memory for EVB context info table, UAP      */
/*                       interface table and S-Channel interface tables.     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS if initialization is successful         */
/*                      VLAN_FAILURE otherwise                               */
/*****************************************************************************/
INT4
VlanEvbStart (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbShutdown                                      */
/*                                                                           */
/* Description        :This function does the following tasks:               */
/*                     - deletes the context data base for u4ContextId.      */
/*                     For the last context creation in EVB, the below tasks */
/*                     - Initializes the global variables.                   */
/*                     - De-Registers EVB MIBs with SNMP module.             */
/*                     - Deletes memory for EVB context info table, UAP      */
/*                       interface table and S-Channel interface tables.     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
VlanEvbShutdown (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return;
}
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanEvbPostCdcpMsg                               */
/*                                                                           */
/*    Description         : Posts the CDCP message to Vlan Config Q for the  */
/*                          processing.                                      */
/*                                                                           */
/*    Input(s)            : pLldpAppTlv - CDCP TLV info filled.              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanEvbPostCdcpMsg (tLldpAppTlv *pLldpAppTlv)
{
    UNUSED_PARAM (pLldpAppTlv);
    return;
}
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanEvbGetSbpPortsOnUap                          */
/*                                                                           */
/*    Description         : This function retrieves the SBP ports present on */
/*                          the UAP.                                         */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                                                                           */
/*    Output(s)           : pSbpArray    - SBP ports.                        */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
VOID
VlanEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex, tVlanEvbSbpArray  *pSbpArray)
{
    UNUSED_PARAM (u4UapIfIndex);
    UNUSED_PARAM (pSbpArray);
    return;
}
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetSChIfIndex                                */
/*                                                                           */
/*    Description         : This function provides the S-Channel IfIndex for */
/*                          the given SVID and UAP IfIndex.                  */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                          u2SVID       - u2SVID                            */
/*                                                                           */
/*    Output(s)           : pu4SChIfIndex- S-Channel IfIndex                 */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetSChIfIndex (UINT4 u4UapIfIndex, UINT4 u4SVID, UINT4 *pu4SChIfIndex)
{
    UNUSED_PARAM (u4UapIfIndex);
    UNUSED_PARAM (u4SVID);
    UNUSED_PARAM (pu4SChIfIndex);
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetSChInfoFromSChIfIndex                     */
/*                                                                           */
/*    Description         : This function provides the UAP IfIndex and SVID  */
/*                          for the given SCh IfIndex.                       */
/*                                                                           */
/*    Input(s)            : u4SChIfIndex - S-Channel IfIndex.                */
/*                          u4ContextId  - Context Identifier                */
/*                                                                           */
/*    Output(s)           : pu4UapIfIndex- UAP IfIndex                       */
/*                          pu4SVID      - SVID                              */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetSChInfoFromSChIfIndex (UINT4 u4SChIfIndex, UINT4 *pu4UapIfIndex,
                                  UINT2 *pu2SVID)
{
    UNUSED_PARAM (u4SChIfIndex);
    UNUSED_PARAM (pu4UapIfIndex);
    UNUSED_PARAM (pu2SVID);
    return VLAN_FAILURE;
}
/****************************************************************************
 *
 *    FUNCTION NAME    : VlanEvbRedGetMsgLen
 *
 *    DESCRIPTION      : This function returns the message length based on
 *                       message type
 *
 *    INPUT            : u1MsgType - Message type
 *                       pu2MsgLen - pointer to message length
 *                       pSyncUpInfo - pointer to sync up info
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
VlanEvbRedGetMsgLen (UINT1 u1MsgType, UINT2 *pu2MsgLen)
{
    UNUSED_PARAM(u1MsgType);
    UNUSED_PARAM(pu2MsgLen);
    return;
}
/****************************************************************************
 *
 *    FUNCTION NAME    : VlanEvbRedProcessCdcpTlvSyncUp
 *
 *    DESCRIPTION      : This function process the TLV change sync up
 *                       message received from the Active node and updates
 *                       the EVB data base at standby node.
 *
 *    INPUT            : pRmCtrlMsg - RM control message
 *                       pu2Offset  - pointer to number of bytes added in
 *                                    RM message buffer in calling function
 *
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
INT4
VlanEvbRedProcessCdcpTlvSyncUp (tRmMsg * pRmMsg, UINT2 *pu2Offset)
{
    UNUSED_PARAM(pRmMsg);
    UNUSED_PARAM(pu2Offset);
    return VLAN_SUCCESS;
}
/****************************************************************************
 *
 *    FUNCTION NAME    : VlanEvbRedProcessRemoteRoleSyncUp
 *
 *    DESCRIPTION      : This function process the RemoteRole sync up
 *                       message received from the Active node and updates
 *                       the EVB data base at standby node.
 *
 *    INPUT            : pRmCtrlMsg - RM control message
 *                       pu2Offset  - pointer to number of bytes added in
 *                                    RM message buffer in calling function
 *
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
INT4
VlanEvbRedProcessRemoteRoleSyncUp (tRmMsg * pRmMsg, UINT2 *pu2Offset)
{
    UNUSED_PARAM(pRmMsg);
    UNUSED_PARAM(pu2Offset);
    return VLAN_SUCCESS;
}
/****************************************************************************
 *
 *    FUNCTION NAME    : VlanEvbRedProcessSbpSyncUp
 *
 *    DESCRIPTION      : This function process the SBP add sync up
 *                       message received from the Active node and updates
 *                       the EVB data base at standby node.
 *
 *    INPUT            : pRmMsg - RM control message
 *                       pu2Offset - pointer to number of bytes added in
 *                                   RM message buffer in calling function
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
INT4
VlanEvbRedProcessSbpSyncUp (tRmMsg * pRmMsg, UINT1 u1MsgType, UINT2 *pu2Offset)
{
    UNUSED_PARAM(pRmMsg);
    UNUSED_PARAM(u1MsgType);
    UNUSED_PARAM(pu2Offset);
    return VLAN_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : VlanEvbRedSendCdcpTlvBulkSyncup                      */
/*                                                                           */
/* Description        : This function sync  up the CDCP TLV change           */
/*                      Bulk Request message.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanEvbRedSendCdcpTlvBulkSyncup ()
{
return;
}

/*****************************************************************************/
/* Function Name      : VlanEvbRedSendRemoteRoleBulkSyncup                   */
/*                                                                           */
/* Description        : This function sync  up the RemoteRole change         */
/*                      Bulk Request message.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanEvbRedSendRemoteRoleBulkSyncup ()
{
return;
}
/*****************************************************************************/
/* Function Name      : VlanEvbRedSendSbpBulkSyncup                          */
/*                                                                           */
/* Description        : This function sync  up the SBP                       */
/*                      Bulk Request message.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanEvbRedSendSbpBulkSyncup ()
{
return;
}
/*****************************************************************************/
/* Function Name      :      VlanGetSChFilterStatus                          */
/*                                                                           */
/* Description        : This function sync  up the SBP                       */
/*                      Bulk Request message.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
VlanGetSChFilterStatus (UINT4 u4UapIfIndex, UINT4 u4SVID)
{
   UNUSED_PARAM(u4UapIfIndex);
   UNUSED_PARAM(u4SVID);
   return VLAN_SUCCESS;
}
/*****************************************************************************/
/* Function Name      :      VlanSetSChFilterStatus                          */
/*                                                                           */
/* Description        : This function sync  up the SBP                       */
/*                      Bulk Request message.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
VlanSetSChFilterStatus (UINT4 u4UapIfIndex, UINT4 u4SVID,
                          INT4 i4RetValFsMIEvbSChannelFilterStatus)
{
   UNUSED_PARAM(u4UapIfIndex);
   UNUSED_PARAM(u4SVID);
   UNUSED_PARAM(i4RetValFsMIEvbSChannelFilterStatus);
   return VLAN_SUCCESS;
}
/******************************************************************************/
/* Function Name      : VlanEvbSchIfDelDynamicEntry                           */
/*                                                                            */
/* Description        : This routine deletes a tEvbSChIfEntry from the        */
/*                      gEvbGlobalInfo.EvbSchIfTree and                       */
/*                      gEvbGlobalInfo.EvbSchScidTree.                        */
/*                      Either SVID or SCID must not be zero so that it will  */
/*                      be used to delete from both RBTrees &releases memory. */
/*                      for Dynamic Mode when CDCP is Enable                  */
/*                                                                            */
/* Input(s)           : u4SbpIfIndex - CFA interface index for S-Channel      */
/*                                                                            */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                             */
/******************************************************************************/
INT4
VlanEvbSchIfDelDynamicEntry (UINT4 u4SbpIfIndex)
{
    UNUSED_PARAM(u4SbpIfIndex);
    return VLAN_SUCCESS;
}
#endif /* __VLNEVSTB_C__ */
