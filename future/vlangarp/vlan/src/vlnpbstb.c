/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnpbstb.c,v 1.49 2016/03/23 12:27:02 siva Exp $
 *
 * Description: This file contains utility routines used in VLANmodule
 *              for provider bridges
 *
 *******************************************************************/
#ifndef _VLNPBSTB_C
#define _VLNPBSTB_C
#include "vlaninc.h"
#include "vlnpbstb.h"
#include "cli.h"
/*****************************************************************************/
/*   Function Name        : RegisterFSPB ()                                  */
/*                                                                           */
/*    Description         : Calls function to Register FsPb Mib              */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
RegisterFSPB ()
{
}

/*****************************************************************************/
/*                                                                           */
/*   Function Name        : RegisterFSMPB ()                                  */
/*                                                                           */
/*    Description         : Calls function to Register FsmPb Mib             */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
RegisterFSMPB ()
{
}

/*****************************************************************************/
/*                                                                           */
/*   Function Name        : RegisterFSMVLE ()                                */
/*                                                                           */
/*    Description         : Calls function to Register FsMVLE Mib            */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/

VOID
RegisterFSMVLE ()
{
}

/*****************************************************************************/
/*                                                                           */
/*   Function Name        : RegisterSTDDOT()                                 */
/*                                                                           */
/*    Description         : Calls function to Register STDDOT1AD Mib         */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/

VOID
RegisterSTDDOT ()
{
}

/*****************************************************************************/
/*                                                                           */
/*   Function Name        : RegisterFSVLNE ()                                */
/*                                                                           */
/*    Description         : Calls function to Register FsVLNE Mib            */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/

VOID
RegisterFSVLNE ()
{
}

/*****************************************************************************/
/*                                                                           */
/*   Function Name        : RegisterFSDOT1 ()                                */
/*                                                                           */
/*    Description         : Calls function to Register Fsdot1ad Mib          */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/

VOID
RegisterFSDOT1 ()
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbInit                                       */
/*                                                                           */
/*    Description         : Calls functions to initialise S-VLAN             */
/*                          classification tables, Ether type swap table,    */
/*                          VID translation table, FID control and Initialise*/
/*                          Memory Pools                                     */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbInit (VOID)
{

    return VLAN_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbDeInit                                     */
/*                                                                           */
/*    Description         : Calls function to delete data structures         */
/*                          initialised for S-VLAN classification tables,    */
/*                          Ether type swap table, VID translation table,    */
/*                          FID control and delete memory pools.             */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbDeInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdateSVlanClassifyParams                     */
/*                                                                           */
/*    Description         : Updates the all available S-VLAN classification  */
/*                          table index values from the frame.               */
/*                                                                           */
/*    Input(s)            : pFrame - The incoming frame buffer               */
/*                                                                           */
/*    Output(s)           : pVlanSVlanIndex - The S-VLAN classification      */
/*                          table indices updated in the pointer             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : gau1VlanIpHdr                              */
/*                                gau1VlanArpHdr                             */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbUpdateSVlanClassifyParams (tCRU_BUF_CHAIN_DESC * pFrame,
                                 tVlanSVlanClassificationParams *
                                 pVlanSVlanIndex)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (pVlanSVlanIndex);
    return;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetTunnelMacAddress                            */
/*                                                                           */
/* Description        : This function creates filters for receiving the      */
/*                      packets destined to proprietary tunnel MAC           */
/*                      address configured for different L2 protocols.       */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      MacAddr - MAC address used for protocol tunneling.   */
/*                      u2Protocol - L2 protocol for which the tunnel MAC    */
/*                                   address is configured for tunneling.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetTunnelMacAddress (UINT4 u4ContextId, tMacAddr MacAddr,
                           UINT2 u2Protocol)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u2Protocol);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetServiceVlanId                             */
/*                                                                           */
/*    Description         : This function gets the service VLAN Id for a     */
/*                          port from the port's associated table type.      */
/*                                                                           */
/*    Input(s)            : SVlanClassificationParams - Updated with all     */
/*                                                      possible index       */
/*                                                      values from the      */
/*                                                       frame               */
/*                                                                           */
/*    Output(s)           : pVlanId - Updated with the classified S- VLAN    */
/*                                                                           */
/*    Global Variables Referred : gapVlanSVlanTable                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbGetServiceVlanId (tVlanSVlanClassificationParams
                        SVlanClassificationParams, tVlanId * pSVlanId)
{
    UNUSED_PARAM (SVlanClassificationParams);
    UNUSED_PARAM (pSVlanId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfEtherSwapEntry                           */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          enabled for adding ether type swap info on per port*/
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbConfEtherSwapEntry (VOID)
{
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfSVlanTranslationTable                    */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          enabled for adding SVlan swap info on per port   */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index/PortChannel Index            */
/*                          u2DstPort - Used to refer physical port in case  */
/*                          of LA scenario                                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbConfSVlanTranslationTable (UINT2 u2Port, UINT2 u2DstPort)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u2DstPort);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbConfHwSVlanClassificationTable               */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          enabled for adding SVlan Classification table entries*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbConfHwSVlanClassificationTable (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveHwSVlanClassificationTable             */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                        disabled for deleting SVlan Classification table entries*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbRemoveHwSVlanClassificationTable (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemovePortCVidEntries                      */
/*                                                                           */
/*    Description         : This function updates the Port CVID Registration */
/*                          table entries and PEP Entries                    */
/*                                                                           */
/*    Input(s)            : u2Port - CEP port number                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_FAILURE/VLAN_SUCCESS                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbRemovePortCVidEntries (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return VLAN_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveHwSVlanTranslationTable                */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan swap entries         */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index/PortChannel Index            */
/*                          u2DstPort - Used to refer physical port in case  */
/*                          of LA scenario                                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbRemoveHwSVlanTranslationTable (UINT2 u2Port, UINT2 u2DstPort)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u2DstPort);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveHwSVlanEtherTypeSwapTable              */
/*                                                                           */
/*    Description         : This function is called when the VLAN module is  */
/*                          disabled for deleting SVlan ether type swap entries*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbRemoveHwSVlanEtherTypeSwapTable (VOID)
{
    return;
}
#endif
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveSVlanClassificationEntriesForPort      */
/*                                                                           */
/*    Description         : This function is called when the port is deleted */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbRemoveSVlanClassificationEntriesForPort (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveSVlanTranslationEntriesForPort         */
/*                                                                           */
/*    Description         : This function is called when the Port type is    */
/*                          Changed/ Port is deleted                         */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbRemoveSVlanTranslationEntriesForPort (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRemoveEtherTypeSwapEntriesForPort            */
/*                                                                           */
/*    Description         : This function is called when the Port type is    */
/*                          Changed/ Port is deleted                         */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbRemoveEtherTypeSwapEntriesForPort (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHandleResetPbPortPropertiesToHw            */
/*                                                                           */
/*    Description         : This function is used to reset the PB port       */
/*                          properties in pSrcPortEntry to physical port     */
/*                          u2DstPort in hardware. This function will be     */
/*                          called when a port is Deleted in the Vlan.       */
/*                                                                           */
/*    Input(s)            : u2Port - Physical Port Index                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbHandleResetPbPortPropertiesToHw (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbProcessFrame                               */
/*                                                                           */
/*    Description         : This function is called when the Packet ingressed*/
/*                                                                           */
/*    Input(s)            : pFrame - Ingressed Frame                         */
/*                          pVlanIf - Message related to the frame like len  */
/*                                                                           */
/*    Output(s)           :                                                  */
/*                          pCustomerVlan - Customer VLAN in packet OR zero  */
/*                          pServiceVlan  - Service VLAN in packet OR zero   */
/*                          pu1Action  - VLAN_DROP/VLAN_FORWARD              */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/ VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/

UINT1
VlanPbProcessFrame (UINT2 u2Port, tCRU_BUF_CHAIN_DESC * pFrame,
                    tVlanIfMsg * pVlanMsg, tVlanId * pCustomerVlan,
                    tVlanId * pServiceVlan, UINT1 *pu1Action)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (pVlanMsg);
    UNUSED_PARAM (pCustomerVlan);
    UNUSED_PARAM (pServiceVlan);
    UNUSED_PARAM (pu1Action);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdateTagInFrame                           */
/*                                                                           */
/*    Description         : This function is used for updating the VLAN tag  */
/*                          after VID referring the VID translation table.   */
/*                                                                           */
/*    Input(s)            : pFrame - Ingressed Frame                         */
/*                          pOutPortEntry - Egress Port entry structure.     */
/*                          SVlanId - Service VLAN to be put in the packet   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbUpdateTagInFrame (tCRU_BUF_CHAIN_DESC * pFrame,
                        tVlanPortEntry * pOutPortEntry, tVlanId SVlanId)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (pOutPortEntry);
    UNUSED_PARAM (SVlanId);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : cli_process_pb_cmd                               */
/*                                                                           */
/*    Description         : This function is called from CLI frame work to   */
/*                          handle Provider bridge related commands          */
/*                                                                           */
/*    Input(s)            : CliHandle - Cli Context Identifier.              */
/*                          u4Command - Command Identifier                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_pb_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Command);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanShowProtocolTunnelStatus                     */
/*                                                                           */
/*    Description         : This function is used for printing the protocol  */
/*                          tunnel status in case of q-in-q/customer bridge. */
/*                                                                           */
/*    Input(s)            : CliHandle - CLI Context Identifier               */
/*                          i4PortId  - Port Index                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanShowProtocolTunnelStatus (tCliHandle CliHandle, INT4 i4PortId)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4PortId);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbSetDefPbPortInfo                             */
/*                                                                           */
/*    Description         : This function is used to set the default PB port */
/*                          values. The calling function should ensure that  */
/*                          pPortEntry is not NULL.                          */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbSetDefPbPortInfo (tVlanPortEntry * pPortEntry)
{
    UNUSED_PARAM (pPortEntry);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHandleCopyPbPortPropertiesToHw               */
/*                                                                           */
/*    Description         : This function is used to set the PB port         */
/*                          properties in pSrcPortEntry to physical port     */
/*                          u2DstPort in hardware. This function will be     */
/*                          called when a port is created or a physical port */
/*                          is added to a port channel.                      */
/*                                                                           */
/*    Input(s)            : u2DstPort - Physical Port Index                  */
/*                          pSrcPortEntry - Port Entry structure whose       */
/*                          properties need to be propgrammed in physical    */
/*                          u2DstPort port                                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbHandleCopyPbPortPropertiesToHw (UINT2 u2DstPort, UINT2 u2SrcPort,
                                      UINT1 u1InterfaceType)
{
    UNUSED_PARAM (u2DstPort);
    UNUSED_PARAM (u2SrcPort);
    UNUSED_PARAM (u1InterfaceType);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbTagFrame                                   */
/*                                                                           */
/*    Description         : This function takes care of adding the VLAN tag  */
/*                          to the given frame.                              */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          VlanId - VlanID to be tagged                     */
/*                          u1Priority - Priority to be tagged.              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbTagFrame (tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId, UINT1 u1Priority)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1Priority);

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningConfigInterfaceDetails            */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for a particular interface                         */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        u1PbPortType - Port type                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4LocalPort,
                                         UINT1 u1PbPortType, UINT1 *pu1HeadFlag)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4LocalPort);
    UNUSED_PARAM (u1PbPortType);
    UNUSED_PARAM (pu1HeadFlag);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetTranslatedVlanBasedOnRelaySVlan           */
/*                                                                           */
/*    Description         : This function is an utility to get the           */
/*                          translated Local VLAN based on Relay SVLAN       */
/*                          from the VID Translation table (RBTREE)          */
/*                                                                           */
/*    Input(s)            : u2Port - Port Index                              */
/*                          u2RelayServiceVlan - SVLAN used in forwarding    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : Local Service VLAN                              */
/*                                                                           */
/*****************************************************************************/
tVlanId
VlanPbGetTranslatedVlanBasedOnRelaySVlan (UINT2 u2Port, tVlanId u2RelaySVlan)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u2RelaySVlan);
    return VLAN_DEF_VLAN_ID;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbConfHwFidProperties                           */
/*                                                                          */
/*     DESCRIPTION      : This function will be called when the bridge mode */
/*                        is changed to provider bridge mode. This function */
/*                        will configure fid mac learning limit and status and*/
/*                        also multicast mac limit                          */
/*                        Information                                       */
/*                                                                          */
/*     INPUT            : None                                              */
/*                                                                          */
/*     OUTPUT           : None                                              */
/*                                                                          */
/*     RETURNS          : None.                                             */
/*                                                                          */
/****************************************************************************/

VOID
VlanPbConfHwFidProperties (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsPortMacLearnLimitExceeded                */
/*                                                                           */
/*    Description         : This function checks whether port MAC Learning   */
/*                          limit has been exceeded or not                   */
/*                                                                           */
/*    Input(s)            : pVlanPbPortEntry - Port Entry                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*                                                                           */
/*    Returns            : VLAN_TRUE - if Limit exceeds                      */
/*                       : VLAN_FALSE - if Limit doesn't exceed              */
/*                                                                           */
/*****************************************************************************/

UINT1
VlanPbIsPortMacLearnLimitExceeded (tVlanPbPortEntry * pVlanPortEntry)
{
    UNUSED_PARAM (pVlanPortEntry);

    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetPortMacLearningStatus                   */
/*                                                                           */
/*    Description         : This function returns port MAC learning status   */
/*                                                                           */
/*    Input(s)            : pVlanPbPortEntry - Port Entry                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*                                                                           */
/*    Returns            : VLAN_ENABLED/VLAN_DISABLED                        */
/*                                                                           */
/*****************************************************************************/
UINT1
VlanPbGetPortMacLearningStatus (tVlanPbPortEntry * pVlanPortEntry)
{
    UNUSED_PARAM (pVlanPortEntry);

    return VLAN_ENABLED;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsMulticastMacLimitExceeded                */
/*                                                                           */
/*    Description         : This function check multicast mac limit          */
/*                          limit                                            */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*                                                                           */
/*    Returns            : VLAN_TRUE - if Limit exceeds                      */
/*                       : VLAN_FALSE - if Limit doesn't exceed              */
/*                                                                           */
/*****************************************************************************/

UINT1
VlanPbIsMulticastMacLimitExceeded (VOID)
{
 
    /* Check static multicast limit */
    if ((VLAN_ST_MCAST_TABLE_COUNT + 1) > VLAN_DEV_MAX_MCAST_TABLE_SIZE)
    {
     return VLAN_TRUE;
    }
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdateCurrentMulticastMacCount             */
/*                                                                           */
/*    Description         : This function updateds multicast mac count       */
/*                                                                           */
/*    Input(s)            : u1flag - ADD/DELETE option for count             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbUpdateCurrentMulticastMacCount (UINT1 u1Flag)
{
    /* Increment or Decrement based on Multicast Mac */
    if (u1Flag == VLAN_ADD)
    {
        VLAN_ST_MCAST_TABLE_COUNT++;
    }
    else
    {
        VLAN_ST_MCAST_TABLE_COUNT--;
    }

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdateTagEtherType()                         */
/*                                                                           */
/*    Description         : This function updates the Rthertype field of the */
/*                          tagged frames.                                   */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          u2EtherType  - EtherType value to be filled      */
/*                          in the frame.                                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanPbUpdateTagEtherType (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2EtherType)
{

    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u2EtherType);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHandleNoSVlanIfCep                         */
/*                                                                           */
/*    Description         : This function verifies port type for CUSTOMER    */
/*                          EDGE PORT.                                       */
/*                                                                           */
/*    Input(s)            : pPortEntry - Pointer to PortType                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*****************************************************************************/

UINT1
VlanPbHandleNoSVlanIfCep (tVlanPortEntry * pPortEntry)
{
    UNUSED_PARAM (pPortEntry);
    return VLAN_FALSE;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningConfigScalars                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays current Provider Bridge     */
/*                        configuration for scalars objects                  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 *pu1HeadFlag)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu1HeadFlag);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbNotifySVlanStatusForPep                    */
/*                                                                           */
/*    Description         : This function updates the PEP oper status        */
/*                          based on the row status of SVLAN. This would     */
/*                          be called whenever the row status of any static  */
/*                          VLAN changes.                                    */
/*                                                                           */
/*    Input(s)            : SVlanId     - Service VLAN ID                    */
/*                          u2RowStatus - SVLAN row status                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbNotifySVlanStatusForPep (tVlanId SVlanId, UINT1 u1RowStatus)
{
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1RowStatus);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbNotifyCepOperStatusForPep                  */
/*                                                                           */
/*    Description         : This function updates the PEP oper status        */
/*                          based on the oper status of CEP.This would be    */
/*                          called whenever the CEP oper status changes.     */
/*                                                                           */
/*    Input(s)            : u2Port       - Customer Edge Port                */
/*                          u1OperStatus - CEP oper status                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbNotifyCepOperStatusForPep (UINT2 u2Port, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1OperStatus);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbSendCustBpduOnPep                          */
/*                                                                           */
/*    Description         : This function posts an event to VLAN task for    */
/*                          packets handling on PEP ports.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId  - Context ID                        */
/*                          pBuf         - Pointer to the incoming frame     */
/*                          u4CepIfIndex - CEP associated with the PEP       */
/*                          SVlanId      - Service VLAN ID                   */
/*                          u4PktSize    - Packet size                       */
/*    Output(s)           : None                                             */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbSendCustBpduOnPep (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4CepIfIndex,
                         tVlanId SVlanId)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u4CepIfIndex);
    UNUSED_PARAM (SVlanId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHandlePktOnPep                             */
/*                                                                           */
/*    Description         : This function processes the packets received     */
/*                          on PEP ports.                                    */
/*                                                                           */
/*    Input(s)            : pFrame       - Pointer to the incoming frame     */
/*                          pVlanIfMsg   - Pointer to the VLAN if message    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbHandlePktOnPep (tCRU_BUF_CHAIN_DESC * pFrame, tVlanIfMsg * pVlanIf)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (pVlanIf);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbValidateBridgeMode                         */
/*                                                                           */
/*    Description         : This function is used to validate the bridge     */
/*                          mode configured in the switch.                   */
/*    Input (s)           : u2BridgeMode - Bridge mode                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbValidateBridgeMode (INT4 i4BridgeMode, UINT4 *pu4ErrorCode)
{
    if ((i4BridgeMode != VLAN_CUSTOMER_BRIDGE_MODE) &&
        (i4BridgeMode != VLAN_PROVIDER_BRIDGE_MODE))
    {
        /* Bridge mode should be customer or provider or provider core or
         * provider edge
         */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlanIdList                               */
/*                                                                           */
/*    Description         : This function returns the Customer VLAN list     */
/*                          for the given Port, Service VLAN in the CVID     */
/*                          Registration table.                              */
/*                                                                           */
/*    Input(s)            : u4IfIndex    - Interface Index                   */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : CVlanList    - Customer VLAN List                */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetCVlanIdList (UINT4 u4IfIndex, tVlanId SVlanId, tVlanList CVlanList)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (CVlanList);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlanInfoOnCustomerPort                   */
/*                                                                           */
/*    Description         : This function returns the tagged Customer VLAN   */
/*                          list and untagged customer VLAN list for the     */
/*                          given Port, Service VLAN in the CVID             */
/*                          Registration table.                              */
/*                                                                           */
/*    Input(s)            : u4IfIndex    - Interface Index                   */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : pCVlanInfo - Customer VLAN Information           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetCVlanInfoOnCustomerPort (UINT4 u4IfIndex, tVlanId SVlanId,
                                tCVlanInfo * pCVlanInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (pCVlanInfo);
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHandlePktRxOnCep                           */
/*                                                                           */
/*    Description         : This function handles frame received on customer */
/*                          edge port.                                       */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pFrame    - Incoming frame                       */
/*                          u2Port    - Port number                          */
/*                          u1PktType - Packet type                          */
/*                          CVlanId   - C-VLAN ID                            */
/*                          pVlanTag  - S-VLAN tag information               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbHandlePktRxOnCep (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                        tVlanTag * pVlanTag)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (pVlanTag);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbReleaseMemBlock                            */
/*                                                                           */
/*    Description         : This function allocates memory from the          */
/*                          requested memory pool.                           */
/*                                                                           */
/*    Input(s)            : u1BlockType - Indicates the memory pool.         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanPbPoolInfo                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the allocated memory if SUCCESS        */
/*                         NULL if allocation fails                          */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbReleaseMemBlock (UINT1 u1BlockType, UINT1 *pu1Buf)
{
    UNUSED_PARAM (u1BlockType);
    UNUSED_PARAM (pu1Buf);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbHandlePktTxOnCep                           */
/*                                                                           */
/*    Description         : This function handles packet transmission on     */
/*                          customer edge ports.                             */
/*                                                                           */
/*    Input(s)            : pFrame    - Outgoing frame                       */
/*                          u2Port    - Port number                          */
/*                          SVlanId   - Service VLAN ID                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbHandlePktTxOnCep (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                        tVlanId SVlanId, tVlanIfMsg * pVlanIf,
                        UINT1 u1IsCustBpdu)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (pVlanIf);
    UNUSED_PARAM (u1IsCustBpdu);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdatePepOperStatusToAst                   */
/*                                                                           */
/*    Description         : This function sets PEP oper status to Down state */
/*                          in AST when VLAN module is disabled.             */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbUpdatePepOperStatusToAst ()
{
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetMemBlock                                */
/*                                                                           */
/*    Description         : This function allocates memory from the          */
/*                          requested memory pool.                           */
/*                                                                           */
/*    Input(s)            : u1BlockType - Indicates the memory pool.         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanPbPoolInfo                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the allocated memory if SUCCESS        */
/*                         NULL if allocation fails                          */
/*                                                                           */
/*****************************************************************************/
UINT1              *
VlanPbGetMemBlock (UINT1 u1BlockType)
{
    UNUSED_PARAM (u1BlockType);
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbSetDot1adPortType                          */
/*                                                                           */
/*    Description         : This function sets the given port type for the   */
/*                          given port and the corresponding port properties.*/
/*                                                                           */
/*    Input(s)            : pVlanPortEntry - Pointer to port entry.          */
/*                          u1PbPortType - Port type to be changed.          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
INT1
VlanPbSetDot1adPortType (tVlanPortEntry * pVlanPortEntry, UINT1 u1PbPortType)
{
    UNUSED_PARAM (pVlanPortEntry);
    UNUSED_PARAM (u1PbPortType);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbInitPcpValues                              */
/*                                                                           */
/*    Description         : This function initialises the PCP decoding and   */
/*                          encoding tables for physical ports with default  */
/*                          values.                                          */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u2Port         - Port number                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbInitPcpValues (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbEtherTypeSwap                              */
/*                                                                           */
/*    Description         : This function checks whether the incoming frame  */
/*                          is tagged based on the entries configured in     */
/*                          ether type swap table.                           */
/*                                                                           */
/*    Input(s)            : pFrame      - Incoming frame                     */
/*                          u2Port      - Receive port                       */
/*                          u2EtherType - Ethertype in the incoming frame    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TAGGED/VLAN_UNTAGGED                  */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbEtherTypeSwap (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                     UINT2 u2EtherType)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u2EtherType);
    return VLAN_UNTAGGED;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbCheckEtherTypeSwap                         */
/*                                                                           */
/*    Description         : This function checks whether the incoming frame  */
/*                          is tagged based on the entries configured in     */
/*                          ether type swap table.                           */
/*                                                                           */
/*    Input(s)            : u2Port      - Receive port                       */
/*                          u2EtherType - Ethertype in the incoming frame    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE/VLAN_FALSE                       */
/*                                                                           */
/*****************************************************************************/
BOOL1
VlanPbCheckEtherTypeSwap (UINT2 u2Port, UINT2 u2EtherType)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u2EtherType);
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetPrimaryContextEtherType                   */
/*                                                                           */
/*    Description         : This function used to get the ethertype(Ingress  */
/*                          or Egress) of given physical port.               */
/*                                                                           */
/*    Input(s)            : u4IfIndex     - IfIndex of the port              */
/*                          pu2EtherValue - Returned Ether typeming frame    */
/*                          u1EtherType   - Flag to identify Ingress or      */
/*                                          Egress ethertype.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
#ifdef PB_WANTED
INT4
VlanGetPrimaryContextEtherType (UINT4 u4IfIndex, UINT2 *pu2EtherValue,
                                UINT1 u1EtherType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu2EtherValue);
    UNUSED_PARAM (u1EtherType);
    return VLAN_SUCCESS;
}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetPcpDecodeVal                            */
/*                                                                           */
/*    Description         : This function returns the priority and drop      */
/*                          eligible values associated with the given PCP.   */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u2Port       - Port number                       */
/*                          u1Pcp        - PCP value                         */
/*                                                                           */
/*    Output(s)           : pu1Priority  - Priority associated with the      */
/*                                         incoming frame                    */
/*                          u1DropEligible - Drop eligible value associated  */
/*                                           with the incoming frame.        */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbGetPcpDecodeVal (UINT2 u2Port, UINT1 u1InPcp, UINT1 *pu1Priority,
                       UINT1 *pu1DropEligible)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1InPcp);
    UNUSED_PARAM (pu1Priority);
    UNUSED_PARAM (pu1DropEligible);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsCreateIvrValid                           */
/*                                                                           */
/*    Description         : This function checks whether an IVR interface    */
/*                          can be created for the given vlan. This function */
/*                          calls another function that does the exact       */
/*                          funtionality by passing the default context id.  */
/*                                                                           */
/*    Input(s)            : u2IfIvrVlanId - Vlan id over which ivr is being  */
/*                                          created.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if IVR can be created for the  */
/*                                given vlan, otherwise VLAN_FALSE           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbIsCreateIvrValid (UINT2 u2IfIvrVlanId)
{
    UNUSED_PARAM (u2IfIvrVlanId);
    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbClassifySVlan                              */
/*                                                                           */
/*    Description         : This function associates service VLAN ID for the */
/*                          incoming frame for PCEP or PCNP based on the     */
/*                          proprietary S-VLAN classification mechanisms.    */
/*                          Otherwise port PVID will be assumed to be the    */
/*                          service VLAN ID                                  */
/*                                                                           */
/*    Input(s)            : u2Port       - Port number                       */
/*                          pFrame       - Incoming frame                    */
/*                          u1PktType    - single or double tagged.          */
/*                          CVlanId      - Customer VLAN ID associated       */
/*                                         with the incoming frame           */
/*                                                                           */
/*    Output(s)           : pSVlanId  - Service VLAN ID associated with      */
/*                                      the incoming frame                   */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbClassifySVlan (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                     UINT1 u1PktType, tVlanId CVlanId, tVlanId * pSVlanId)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1PktType);
    UNUSED_PARAM (CVlanId);
    UNUSED_PARAM (pSVlanId);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsPortTypePresentInPortList                */
/*                                                                           */
/*    Description         : This function checks whether the given port      */
/*                          type is present in the given port list.          */
/*                                                                           */
/*    Input(s)            : pu1Ports - Port bit map containing the list of   */
/*                                     untagged ports.                       */
/*                          i4Len - Length of the given port bit map.        */
/*                          u1PbPortType - Port type to be checked.          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if the given bit map contains  */
/*                                a port of given type, otherwise VLAN_FALSE */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbIsPortTypePresentInPortList (UINT1 *pu1Ports, INT4 i4Len,
                                   UINT1 u1PbPortType)
{
    UNUSED_PARAM (pu1Ports);
    UNUSED_PARAM (i4Len);
    UNUSED_PARAM (u1PbPortType);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsThisInterfaceVlan                          */
/*                                                                           */
/*    Description         : This function is a wrapper function for          */
/*                          CfaIsThisInterfaceVlan. Once Cfa is instantiated */
/*                          this function will get the current context id    */
/*                          and then will pass that to cfa as one more       */
/*                          parameter.                                       */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan id.                                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if IVR interface is present    */
/*                                for given vlan, otherwise VLAN_FALSE       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsThisInterfaceVlan (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsAnyCustomerPortPresent                   */
/*                                                                           */
/*    Description         : This function checks whether any customer port   */
/*                          is present in the given port list.               */
/*                                                                           */
/*    Input(s)            : pu1Ports - Port bit map containing the list of   */
/*                                     untagged ports.                       */
/*                          i4Len - Length of the given port bit map.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TRUE - if the given bit map contains  */
/*                                a customer port, otherwise VLAN_FALSE      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbIsAnyCustomerPortPresent (UINT1 *pu1Ports, INT4 i4Len)
{
    UNUSED_PARAM (pu1Ports);
    UNUSED_PARAM (i4Len);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbCheckVlanServiceType                       */
/*                                                                           */
/*    Description         : This function checks the VLAN service type and   */
/*                          number of ports getting configured for a VLAN.   */
/*                          If the service type is E-Line and the number of  */
/*                          ports is greater than two, failure is returned.  */
/*                                                                           */
/*    Input(s)            : VlanId  - VLAN ID                                */
/*                          InPorts - Member ports getting configured for    */
/*                                    the VLAN.                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
INT4
VlanPbCheckVlanServiceType (tVlanId VlanId, tLocalPortList InPorts)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (InPorts);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbNpUpdateCvidAndPepInHw                     */
/*                                                                           */
/*    Description         : This function creates the logical port entry and */
/*                          CVID registration table entries in HW.           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbNpUpdateCvidAndPepInHw (UINT2 u2Port, tVlanId SVlanId, UINT2 u2DstPort,
                              UINT1 u1Flag)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1Flag);
    UNUSED_PARAM (u2DstPort);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandlePbRemovePortTablesFromHw               */
/*                                                                           */
/*    Description         : This function is used to remove the PB port      */
/*                          properties in pSrcPortEntry from physical port   */
/*                          u2DstPort in hardware. This function will be     */
/*                          called when a port comes out of the port channel */
/*                                                                           */
/*    Input(s)            : u2DstPort - Physical Port Index                  */
/*                          u2SrcPort - Port channel properties need to be   */
/*                          propgrammed in physical                          */
/*                          u2DstPort port                                   */
/*                          pVlanQMsg - Message received by VLAN             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHandlePbRemovePortTablesFromHw (tVlanQMsg * pVlanQMsg, UINT2 u2SrcPort,
                                    UINT2 u2DstPort)
{
    UNUSED_PARAM (pVlanQMsg);
    UNUSED_PARAM (u2SrcPort);
    UNUSED_PARAM (u2DstPort);
    return VLAN_SUCCESS;
}

#ifdef CLI_WANTED
/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbCliVlanDisplay                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the VLAN service type       */
/*                       & Fdb Mac learning Status configuration.This        */
/*                       function is called from show VLAN Current database  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4VlanId - Specified Vlan id                       */
/*                                   for configuration                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbCliVlanDisplay (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4VlanId)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VlanId);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbCliShowL2ProtocolTunnel                      */
/*                                                                           */
/*     DESCRIPTION      : This function show entries in the Tunnel protocol  */
/*                        table.                                             */
/*                                                                           */
/*     INPUT            : CliHandle - Context in which the CLI               */
/*                                      command is processed                 */
/*                                                                           */
/*                        u4Port       - Port which is to be displayed       */
/*                                                                           */
/*                        u1Summary    - Summary Option Present / Not        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully         */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                  */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbCliShowL2ProtocolTunnel (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4Port, UINT1 u1Summary)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Summary);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : DisplayPbServiceVlanInfo                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the VLAN  service type      */
/*                        configuration                                      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4VlanId - Specified Vlan id                       */
/*                                   for configuration                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
DisplayPbServiceVlanInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4VlanId, UINT1 *pu1isActiveVlan)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VlanId);
    UNUSED_PARAM (pu1isActiveVlan);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningConfigPerVlanTable                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the VLAN  service type      */
/*                        configuration                                      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4CurrentContextId -  Virtual context Id           */
/*                        i4CurrentVlanId    -  Vlan Id                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbShowRunningConfigPerVlanTable (tCliHandle CliHandle,
                                     UINT4 u4CurrentContextId,
                                     UINT4 i4CurrentVlanId, UINT1 *pu1HeadFlag)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4CurrentContextId);
    UNUSED_PARAM (i4CurrentVlanId);
    UNUSED_PARAM (pu1HeadFlag);
}

#endif /*CLI_WANTED */

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetPcpAndLocalVid                            */
/*                                                                           */
/*    Description         : This function returns the PCP and local VLAN ID  */
/*                          to be used in the outgoing frame tag.            */
/*                                                                           */
/*    Input(s)            : VlanTag  - VLAN tag filled with VLAN-ID,         */
/*                                     priority and drop-eligible.           */
/*                        : pVlanPortEntry - Pointer to port entry.          */
/*                                                                           */
/*    Output(s)           : pLocalVid - Local VLAN ID to be used in the      */
/*                                       outgoing frame.                     */
/*                          pu2Pcp    - The PCP to be used in the outgoing   */
/*                                       frame.                              */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/

INT4
VlanGetPcpAndLocalVid (tVlanTag VlanTag, tVlanPortEntry * pVlanPortEntry,
                       tVlanId * pLocalVid, UINT1 *pu1Pcp)
{
    UNUSED_PARAM (VlanTag);
    UNUSED_PARAM (pVlanPortEntry);
    UNUSED_PARAM (pLocalVid);
    UNUSED_PARAM (pu1Pcp);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*   Function Name        : RegisterFSM1AD ()                                */
/*                                                                           */
/*    Description         : Calls function to Register FsmPb Mib             */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/

VOID
RegisterFSM1AD ()
{
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbIsMacLearningAllowed                       */
/*                                                                           */
/*    Description         : This function checks whether a mac has to be     */
/*                          learned or not on a port or an fid.This function */
/*                          is called from VlanLearn                         */
/*                                                                           */
/*    Input(s)            : pFdbEntry - Learned Entry                        */
/*                          pVlanPortEntry - Port on which Mac is learned    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_TRUE / VLAN_FALSE                           */
/*****************************************************************************/

INT4
VlanPbIsMacLearningAllowed (tVlanFdbEntry * pFdbEntry,
                            tVlanPortEntry * pVlanPortEntry)
{
    UNUSED_PARAM (pFdbEntry);
    UNUSED_PARAM (pVlanPortEntry);
    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbUpdateLearntMacCount                       */
/*                                                                           */
/*    Description         : This function updates the Mac Count learnt on the*/
/*                          port and vlan.                                   */
/*                                                                           */
/*    Input(s)            : pFdbEntry - Learned Entry                        */
/*                          pVlanRec  - Vlan Entry                           */
/*                          pVlanPortEntry - Port on which Mac is learned    */
/*                          pFidEntry - FID Entry                            */
/*                          u1Flag - VLAN_ADD/VLAN_DELETE/VLAN_UPDATE        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VlanPbUpdateLearntMacCount (tVlanPortEntry * pVlanPortEntry,
                            tVlanFdbEntry * pFdbEntry, UINT1 u1Flag)
{
    UNUSED_PARAM (pVlanPortEntry);
    UNUSED_PARAM (pFdbEntry);
    UNUSED_PARAM (u1Flag);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetBridgeMode                              */
/*                                                                           */
/*    Description         : This function is used to get the Bridge mode     */
/*                                                                           */
/*    Input(s)            : pi4BridgeMode - Bridge mode                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*****************************************************************************/
INT4
VlanPbGetBridgeMode (INT4 i4Context, INT4 *pi4BridgeMode)
{
    UNUSED_PARAM (i4Context);
    *pi4BridgeMode = (INT4) VLAN_CUSTOMER_BRIDGE_MODE;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbValidatePortType                           */
/*                                                                           */
/*    Description         : This function tests whether given port type can  */
/*                          set for a port in the given bridge.              */
/*                                                                           */
/*    Input(s)            : u1PbPortType - PbPortType to be validated.       */
/*                          u2LocalPort - Local Port                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE / VLAN_FALSE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbValidatePortType (UINT2 u2LocalPort, UINT1 u1PbPortType)
{
    UNUSED_PARAM (u2LocalPort);
    if ((u1PbPortType != VLAN_CUSTOMER_BRIDGE_PORT) &&
        (u1PbPortType != VLAN_UPLINK_ACCESS_PORT))
    {
        return VLAN_FALSE;
    }

    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbRedSyncPbPortProperties                    */
/*                                                                           */
/*    Description         : This function syncronizes provider bridge port   */
/*                          properties in hardware and software              */
/*                                                                           */
/*    Input(s)            : u2Port - port to be syncronised                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/

VOID
VlanPbRedSyncPbPortProperties (UINT4 u4Context, UINT2 u2Port)
{
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (u2Port);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlansFromCVlanSVlanTable                 */
/*                                                                           */
/*    Description         : This function returns the Customer VLAN list     */
/*                          for the given Port, Service VLAN in the CVID     */
/*                          Registration table.                              */
/*                                                                           */
/*    Input(s)            : u4TblIndex   - Index to identify the S-VLAN Table*/
/*                          u2Port       - Local Port Number                 */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : CVlanList    - Customer VLAN List                */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
VlanGetCVlansFromCVlanSVlanTable (UINT4 u4TblIndex, UINT2 u2Port,
                                  tVlanId SVlanId, tCVlanInfo * pCVlanInfo)
{
    UNUSED_PARAM (u4TblIndex);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (pCVlanInfo);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbGetSVlanTableIndex                           */
/*                                                                           */
/*    Description         : This function returns the index of the S-VLAN    */
/*                          classification table in the global array.        */
/*                                                                           */
/*    Input(s)            : i4TableType - Indicates the Table Type           */
/*                                        Port, C-VLAN and the likes.        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanSVlanTableInfo                        */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : The array index of the gVlanSVlanTableInfo        */
/*                                                                           */
/*****************************************************************************/
UINT4
VlanPbGetSVlanTableIndex (INT4 i4TableType)
{
    UNUSED_PARAM (i4TableType);
    return VLAN_FALSE;
}

/*****************************************************************************/
/* Function Name      : VlanGetSVlanId                                       */
/*                                                                           */
/* Description        : This function is used to get SVlanId                 */
/*                                                                           */
/* Input (s)          : pFrame     - Pointer to the incoming packet          */
/*                      u2Port     - Port Number                             */
/*                      u4ContextId - Context ID                             */
/*                                                                           */
/* Output (s)         : SVlanId   - VlanId                                   */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetSVlanId (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port, UINT4 u4ContextId,
                tVlanId * SVlanId)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM(SVlanId);
    return VLAN_SUCCESS;
}

#ifdef MBSM_WANTED
/*****************************************************************************/
/* Function Name      : VlanMbsmUpdtPbPortPropertiesToHw                     */
/*                                                                           */
/* Description        : This function Updates Pb port properties for         */
/*                      instrted slot ports                                  */
/*                                                                           */
/* Input(s)           : pSlotInfo - Information about inserted slot          */
/*                      u2PortNum - Port Number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdtPbPortPropertiesToHw (tMbsmSlotInfo * pSlotInfo, UINT2 u2PortNum)
{
    UNUSED_PARAM (pSlotInfo);
    UNUSED_PARAM (u2PortNum);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanMbsmUpdtPepStatusToHw                            */
/*                                                                           */
/* Description        : This function Updates Peb port status for            */
/*                      instrted slot ports                                  */
/*                                                                           */
/* Input(s)           : pSlotInfo - Information about inserted slot          */
/*                      u2Port    - Local Port  of current context           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdtPepStatusToHw (UINT2 u2Port, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (pSlotInfo);

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanMbsmUpdateCvidEntriesToHw                        */
/*                                                                           */
/* Description        : This function Updates Cvid Entried for               */
/*                      instrted slot ports                                  */
/*                                                                           */
/* Input(s)           : pSlotInfo - Information about inserted slot          */
/*                      u4SVlanId - Service vlan Ip for port                 */
/*                      u2Port    - Local Port  of current context           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdateCvidEntriesToHw (UINT2 u2Port, UINT4 u4SVlanId,
                               tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4SVlanId);
    UNUSED_PARAM (pSlotInfo);

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanMbsmUpdateVlanSwapTableToHw                      */
/*                                                                           */
/* Description        : This function Updates Vlan Swap Table for            */
/*                      instrted slot ports                                  */
/*                                                                           */
/* Input(s)           : pSlotInfo - Information about inserted slot          */
/*                      u2Port    - Local Port  of current context           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdateVlanSwapTableToHw (UINT2 u2Port, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (pSlotInfo);

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanMbsmUpdtMcastMacLimitToHw                        */
/*                                                                           */
/* Description        : This function Updates Pb Multicast Mac limit for     */
/*                      instrted slots                                       */
/*                                                                           */
/* Input(s)          :  pSlotInfo - Information about inserted slot          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables   : None                                                 */
/*                                                                           */
/* Global Variables   : None.                                                */
/*                                                                           */
/* Returns            : MBSM_SUCCESS on success                              */
/*                      MBSM_FAILURE on failure                              */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMbsmUpdtMcastMacLimitToHw (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);

    return MBSM_SUCCESS;
}
#endif
/*****************************************************************************/
/*                                                                           */
/*     Function Name    : VlanPbClearEvcL2TunnelCounters                     */
/*                                                                           */
/*     Description      : This function Clears the L2Protocol Tunnel Counters*/
/*                        ,which is invoked from MEF.                        */
/*                                                                           */
/*     Input (s)        : tCliHandle  CliHandle - Context in which the CLI   */
/*                                                command is processed       */
/*                        u4ContextId           - Context ID                 */
/*                        tVlanId VlanId        - Vlan ID                    */
/*                                                                           */
/*     Output (s)       : None                                               */
/*                                                                           */
/*     Global Variables Referred  : gpVlanContextInfo                        */
/*                                                                           */
/*     Global Variables Modified  : None                                     */
/*                                                                           */
/*     RETURNS          : VLAN_SUCCESS/VLAN_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbClearEvcL2TunnelCounters (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return VLAN_SUCCESS;
}
#endif /*VLNPBSTB*/
