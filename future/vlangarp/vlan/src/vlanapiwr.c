/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2004                        */
/*                                                                           */
/*  FILE NAME             : vlanapiwr.c                                      */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 07 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains the wrapper routines for other*/
/*                          modules apis.                                    */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Date(DD/MM/YYYY)      : 22/07/2004                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Modified for L2MI_PHS2                           */
/*****************************************************************************/
#ifdef VLAN_WANTED

#include "vlaninc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dTpHCPortTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1dTpHCPortTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

INT1
nmhValidateIndexInstanceDot1dTpHCPortTable (INT4 i4Dot1dTpPort)
{
    tVlanPortEntry     *pPortEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (i4Dot1dTpPort) == VLAN_FALSE)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (i4Dot1dTpPort);

    if (pPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dTpPortOverflowTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1dTpPortOverflowTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

INT1
nmhValidateIndexInstanceDot1dTpPortOverflowTable (INT4 i4Dot1dTpPort)
{
    return (nmhValidateIndexInstanceDot1dTpHCPortTable (i4Dot1dTpPort));
}

/****************************************************************************
 Function    :  nmhGetDot1dTpHCPortInFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpHCPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpHCPortInFrames ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

INT1
nmhGetDot1dTpHCPortInFrames (INT4 i4Dot1dTpPort,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValDot1dTpHCPortInFrames)
{
    UNUSED_PARAM (i4Dot1dTpPort);

    VLAN_MEMSET (pu8RetValDot1dTpHCPortInFrames, 0,
                 sizeof (tSNMP_COUNTER64_TYPE));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpHCPortInDiscards
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpHCPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpHCPortInDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

INT1
nmhGetDot1dTpHCPortInDiscards (INT4 i4Dot1dTpPort, tSNMP_COUNTER64_TYPE *
                               pu8RetValDot1dTpHCPortInDiscards)
{
    UNUSED_PARAM (i4Dot1dTpPort);

    VLAN_MEMSET (pu8RetValDot1dTpHCPortInDiscards, 0,
                 sizeof (tSNMP_COUNTER64_TYPE));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpHCPortOutFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpHCPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpHCPortOutFrames ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

INT1
nmhGetDot1dTpHCPortOutFrames (INT4 i4Dot1dTpPort,
                              tSNMP_COUNTER64_TYPE *
                              pu8RetValDot1dTpHCPortOutFrames)
{
    UNUSED_PARAM (i4Dot1dTpPort);

    VLAN_MEMSET (pu8RetValDot1dTpHCPortOutFrames, 0,
                 sizeof (tSNMP_COUNTER64_TYPE));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortInOverflowFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortInOverflowFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpPortInOverflowFrames ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

INT1
nmhGetDot1dTpPortInOverflowFrames (INT4 i4Dot1dTpPort,
                                   UINT4 *pu4RetValDot1dTpPortInOverflowFrames)
{
    UNUSED_PARAM (i4Dot1dTpPort);

    *pu4RetValDot1dTpPortInOverflowFrames = 0;
    return SNMP_SUCCESS;
}

 /****************************************************************************
 Function    :  nmhGetDot1dTpPortOutOverflowFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortOutOverflowFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpPortOutOverflowFrames ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

INT1
nmhGetDot1dTpPortOutOverflowFrames (INT4 i4Dot1dTpPort, UINT4
                                    *pu4RetValDot1dTpPortOutOverflowFrames)
{
    UNUSED_PARAM (i4Dot1dTpPort);

    *pu4RetValDot1dTpPortOutOverflowFrames = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortInOverflowDiscards
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortInOverflowDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpPortInOverflowDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDot1dTpPortInOverflowDiscards (INT4 i4Dot1dTpPort,
                                     UINT4
                                     *pu4RetValDot1dTpPortInOverflowDiscards)
{
    UNUSED_PARAM (i4Dot1dTpPort);

    *pu4RetValDot1dTpPortInOverflowDiscards = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dTpPortOverflowTable
 Input       :  The Indices
                Dot1dTpPort
                nextDot1dTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1dTpPortOverflowTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexDot1dTpPortOverflowTable (INT4 i4Dot1dTpPort,
                                         INT4 *pi4NextDot1dTpPort)
{
    return (nmhGetNextIndexDot1dTpHCPortTable (i4Dot1dTpPort,
                                               pi4NextDot1dTpPort));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dTpPortOverflowTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1dTpPortOverflowTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexDot1dTpPortOverflowTable (INT4 *pi4Dot1dTpPort)
{
    return (nmhGetFirstIndexDot1dTpHCPortTable (pi4Dot1dTpPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dTpHCPortTable
 Input       :  The Indices
                Dot1dTpPort
                nextDot1dTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1dTpHCPortTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexDot1dTpHCPortTable (INT4 i4Dot1dTpPort, INT4 *pi4NextDot1dTpPort)
{
    UINT2               u2Port;
    tVlanPortEntry     *pPortEntry;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {

        return SNMP_FAILURE;
    }

    if (i4Dot1dTpPort < 0)
    {
        return SNMP_FAILURE;
    }

    for (u2Port = (UINT2) (i4Dot1dTpPort + 1);
         u2Port <= VLAN_MAX_PORTS; u2Port++)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        if (pPortEntry == NULL)
        {
            continue;
        }

        if (pPortEntry != NULL)
        {
            *pi4NextDot1dTpPort = (INT4) u2Port;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dTpHCPortTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1dTpHCPortTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexDot1dTpHCPortTable (INT4 *pi4Dot1dTpPort)
{
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1dTpHCPortTable (0, pi4Dot1dTpPort));
}
#endif
