
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevsch.c,v 1.13 2016/07/22 06:46:32 siva Exp $
 *
 * Description: This file contains UAP,S-Channel utilities 
                for Edge Virtual Bridging
 *
 *******************************************************************/
#ifndef __VLNEVSCH_C__
#define __VLNEVSCH_C__

#include "vlaninc.h"

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfTblCreate                                */
/*                                                                           */
/* Description        : This routine creates gEvbGlobalInfo.EvbSchIfTree and */
/*                      gEvbGlobalInfo.EvbSchScidTree.                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbSChIfTblCreate (VOID)
{
    /* This function is called from VlanEvbStart */
    gEvbGlobalInfo.EvbSchIfTree = RBTreeCreateEmbedded (FSAP_OFFSETOF 
                   (tEvbSChIfEntry, SChIfNode), VlanEvbSChIfTblCmp);

    if (gEvbGlobalInfo.EvbSchIfTree == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanEvbSChIfTblCreate: RBTree creation for " 
     		      "gEvbGlobalInfo.EvbSchIfTree failed\r\n");
	    return VLAN_FAILURE;
    }

    gEvbGlobalInfo.EvbSchScidTree = RBTreeCreateEmbedded (FSAP_OFFSETOF
                    (tEvbSChIfEntry, SChScidNode), VlanEvbSChScidIfTblCmp);

    if (gEvbGlobalInfo.EvbSchScidTree == NULL)
    {
        VLAN_TRC (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanEvbSChIfTblCreate: RB Tree creation for "
                  "gEvbGlobalInfo.EvbSchScidTree failed\r\n");
	    return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfTblDelete                                */
/*                                                                           */
/* Description        : This routine deletes gEvbGlobalInfo.EvbSchIfTree and */
/*                      gEvbGlobalInfo.EvbSchScidTree.                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanEvbSChIfTblDelete (VOID)
{
    /* This function is called from VlanEvbShutdown */

    /* Delete all the S-Channel entries before deleting the RBTree.
     * It takes care deleting the entries from  both 
     * gEvbGlobalInfo.EvbSchIfTree and  gEvbGlobalInfo.EvbSchScidTree
     * RBTrees. */

    VlanEvbSChIfDelAllEntries ();

    if (gEvbGlobalInfo.EvbSchScidTree != NULL)
    {
        RBTreeDelete (gEvbGlobalInfo.EvbSchScidTree);
        gEvbGlobalInfo.EvbSchScidTree = NULL;
    }

    if (gEvbGlobalInfo.EvbSchIfTree != NULL)
    {
        RBTreeDelete (gEvbGlobalInfo.EvbSchIfTree);
        gEvbGlobalInfo.EvbSchIfTree = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfTblCmp                                   */
/*                                                                           */
/* Description        : This function is used to compare the RBTree nodes of */
/*                      gEvbGlobalInfo.EvbSchIfTree                          */
/*                      -	u4UapIfIndex - Primary index                     */
/*  	                -	u4SVId       - secondary index                   */
/*                                                                           */
/* Input(s)           : pNode        - Node from RBTree                      */
/*                      pNodeIn      - Input node                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
VlanEvbSChIfTblCmp (tRBElem *pNode,
                    tRBElem *pNodeIn)
{
    tEvbSChIfEntry    *pSChNode = (tEvbSChIfEntry *)pNode;
    tEvbSChIfEntry    *pSChNodeIn = (tEvbSChIfEntry *)pNodeIn;

    /* key 1 --> Interface index.
     * key 2 --> SVID
     */
    if (pSChNode->u4UapIfIndex < pSChNodeIn->u4UapIfIndex)
    {
	    return (VLAN_RBTREE_KEY_LESSER);
    }
    else if (pSChNode->u4UapIfIndex > pSChNodeIn->u4UapIfIndex)
    {
	    return (VLAN_RBTREE_KEY_GREATER);
    }
    else if (pSChNode->u4SVId < pSChNodeIn->u4SVId)
    {
	    return (VLAN_RBTREE_KEY_LESSER);
    }
    else if (pSChNode->u4SVId > pSChNodeIn->u4SVId)
    {
	    return (VLAN_RBTREE_KEY_GREATER);
    }
    return (VLAN_RBTREE_KEY_EQUAL);
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChScidIfTblCmp                               */
/*                                                                           */
/* Description        : This function is used to compare the RBTree nodes of */
/*                      gEvbGlobalInfo.EvbSchScidTree.                       */
/*                      -	u4UapIfIndex - Primary index                     */
/*  	                -	u4SCID       - secondary index                   */
/*                                                                           */
/* Input(s)           : pNode        - Node from RBTree                      */
/*                      pNodeIn      - Input node                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
VlanEvbSChScidIfTblCmp (tRBElem *pNode,
                        tRBElem *pNodeIn)
{
    tEvbSChIfEntry    *pSChNode = (tEvbSChIfEntry *)pNode;
    tEvbSChIfEntry    *pSChNodeIn = (tEvbSChIfEntry *)pNodeIn;

    /* key 1 --> Interface index.
     * key 2 --> S-Channel Identifier
     */
    if (pSChNode->u4UapIfIndex < pSChNodeIn->u4UapIfIndex)
    {
	    return (VLAN_RBTREE_KEY_LESSER);
    }
    else if (pSChNode->u4UapIfIndex > pSChNodeIn->u4UapIfIndex)
    {
	    return (VLAN_RBTREE_KEY_GREATER);
    }
    else if (pSChNode->u4SChId < pSChNodeIn->u4SChId)
    {
	    return (VLAN_RBTREE_KEY_LESSER);
    }
    else if (pSChNode->u4SChId > pSChNodeIn->u4SChId)
    {
	    return (VLAN_RBTREE_KEY_GREATER);
    }
    return (VLAN_RBTREE_KEY_EQUAL);
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfAddEntry                                 */
/*                                                                           */
/* Description        : This function allocates memory for S-Channel entry & */
/*                      adds the S-Channel entry  either in                  */
/*                      gEvbGlobalInfo.EvbSchIfTree or in                    */
/*                      gEvbGlobalInfo.EvbSchScidTree.                       */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - CFA interface index for UAP           */
/*                      u4Mode - VLAN_EVB_SCH_DYNAMIC                        */
/*                      u4SVId - SVID from 1-4094                            */
/*                      u4SCID - S-Channel Identifier                        */
/*                                                                           */
/* Output(s)          : ppEvbSChIfEntry  - Pointer which has the memory of   */
/*                                         S-Channel interface entry         */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                          */
/*****************************************************************************/
INT4
VlanEvbSChIfAddEntry (UINT4 u4UapIfIndex, UINT4 u4SVId, UINT1 u1Mode, 
                          UINT4 u4SChId, tEvbSChIfEntry **ppEvbSChIfEntry)
{
    UINT1   u1OperStatus = 0;
    UINT4  u4SChIfIndex = 0 ; /* simply used for macro typecsting purpose */
    INT4   i4SchIndex = 0;
    /* this function is called from VlanEvbUapIfAddEntry for (1,1) pair.
     * It is also called from nmhSetIeee8021BridgeEvbCAPRowStatus  for static 
     * and CDCP hdlr for dynamic .*/
    
    tEvbUapIfEntry   *pUapIfEntry    = NULL;
    (*ppEvbSChIfEntry) = NULL;

    if (u1Mode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
    {
        if (((*ppEvbSChIfEntry) = VlanEvbSChScidIfGetEntry (u4UapIfIndex, 
                u4SChId)) != NULL)
        {
            VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbSChIfAddEntry : Dynamic S-Channel entry is "
                    "already present for UAP Port %d, SVID %d, SCID %d\r\n",
                   (*ppEvbSChIfEntry)->u4UapIfIndex, (*ppEvbSChIfEntry)->u4SVId,
                     (*ppEvbSChIfEntry)->u4SChId);
            /* NOTE: Check the return value whether success or failure */
            return VLAN_FAILURE;
        }

        /* Fetching free interface index for SBP port creation */
        VlanEvbGetFreeDynSChIndexForUap (u4UapIfIndex,
                                      &i4SchIndex);
        if (i4SchIndex == 0)
        {
            VLAN_TRC_ARG3 (VLAN_EVB_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                    VLAN_NAME, "VlanEvbSChIfAddEntry: Free SBP index is not "
                    "available for UAP port in dynamic mode %d, SVID %d\r,SCID %d\r\n",u4UapIfIndex, u4SVId, u4SChId);
            return VLAN_FAILURE;
        }

    }
    else
    {
        if (((*ppEvbSChIfEntry) = VlanEvbSChIfGetEntry (u4UapIfIndex, u4SVId)) 
                != NULL)
        {
            VLAN_TRC_ARG3 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                    "VlanEvbSChIfAddEntry : Static S-Channel entry is "
                    "already present for UAP Port %d, SVID %d, SCID %d\r\n",
                   (*ppEvbSChIfEntry)->u4UapIfIndex, (*ppEvbSChIfEntry)->u4SVId,
                     (*ppEvbSChIfEntry)->u4SChId);
            return VLAN_SUCCESS;
        }
    }

    (*ppEvbSChIfEntry) = (tEvbSChIfEntry *) (VOID *) VLAN_EVB_GET_BUF
                            (VLAN_EVB_SCH_ENTRY, sizeof (tEvbSChIfEntry));
    if ((*ppEvbSChIfEntry) == NULL)
    {
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC, 
                       VLAN_NAME, "VlanEvbSChIfAddEntry: Memory allocation "
                        "for UAP port %d, SVID %d, SCID %d to create "
                        "tEvbSChIfEntry failed\r\n", u4UapIfIndex, u4SVId, 
                        u4SChId);
        return VLAN_FAILURE;
    }

    MEMSET ((*ppEvbSChIfEntry), 0, sizeof (tEvbSChIfEntry));

    VlanEvbUapGetOperStatus (u4UapIfIndex, &u1OperStatus);

    /* Setting default values for other variables in S-Channel entry */
    /* u4SChIfPort is filled when SBP port create indication
     * comes from the CFA -> L2IWF -> VLAN */
 
    (*ppEvbSChIfEntry)->i4SChIfRowStatus = NOT_IN_SERVICE;
    (*ppEvbSChIfEntry)->u1AdminStatus = CFA_IF_UP;
     
	(*ppEvbSChIfEntry)->u1OperStatus = u1OperStatus;
	/* Nego Status is only used in hybrid mode. */
	(*ppEvbSChIfEntry)->i4NegoStatus = VLAN_EVB_SCH_FREE;

    if (u1Mode != VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
    {
        /* Nego Status is only used in hybrid mode. */
        (*ppEvbSChIfEntry)->i4NegoStatus = VLAN_EVB_SCH_FREE;

        /* SChId = 0 indicates that S-Channel mode is VLAN_EVB_SCH_MODE_AUTO.
         * So, getting next available S-Channel identifier. */

        /* check VLAN_EVB_SCH_MODE_AUTO is not required below.*/

        if ((u4SChId == 0) && ((VLAN_EVB_CURR_CONTEXT_PTR()->i4EvbSysSChMode) 
                                == VLAN_EVB_SCH_MODE_AUTO))
        {   
            if ((u4SChId = VlanEvbSChGetNextSChannelId (u4UapIfIndex)) == 0)
            {
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanEvbSChIfAddEntry: S-Channel Id is not available "
                      "for UAP Port %d and SVID %d\r\n", u4UapIfIndex, u4SVId);
                return VLAN_FAILURE;
            }
        } /* End of u4SChId == 0 */
    } /* End of u1Mode != VLAN_EVB_UAP_SCH_MODE_DYNAMIC */

    /* Setting RBTree Indices and adding in respective RBTree. */
    (*ppEvbSChIfEntry)->u4UapIfIndex = u4UapIfIndex;
    (*ppEvbSChIfEntry)->u4SVId = u4SVId;
    (*ppEvbSChIfEntry)->u4SChId = u4SChId;

    /* Adding in RBTree 1 - keys UAP port + SVID */
    if (RBTreeAdd (gEvbGlobalInfo.EvbSchIfTree, (tRBElem *) (VOID *)
                   (*ppEvbSChIfEntry)) == RB_FAILURE)
    {
        VLAN_EVB_RELEASE_BUF (VLAN_EVB_SCH_ENTRY, (VOID *)(*ppEvbSChIfEntry));
        (*ppEvbSChIfEntry) = NULL;

        VLAN_TRC_ARG3 (VLAN_EVB_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                VLAN_NAME, "VlanEvbSChIfAddEntry: EvbSchIfTree addition"
                "for UAP port %d, SVID %d, SCID %d failed in static\r\n",
                u4UapIfIndex, u4SVId, u4SChId);
        return VLAN_FAILURE;
    }
    if (u4SChId != 0)
    { 
        /* Adding in RBTree 1 - keys UAP port + SCID */
        if (RBTreeAdd (gEvbGlobalInfo.EvbSchScidTree, (tRBElem *) (VOID *)
                    (*ppEvbSChIfEntry)) == RB_FAILURE)
        {
            if (RBTreeRemove (gEvbGlobalInfo.EvbSchIfTree, (tRBElem *) (VOID *)
                    (*ppEvbSChIfEntry)) == RB_FAILURE)
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "VlanEvbSChIfAddEntry: RBTree remove for UAP port %d, "
                           "SVID %d, SCID %d for EvbSchIfTree failed\r\n",
                           u4UapIfIndex, u4SVId, u4SChId);
            }

            VLAN_EVB_RELEASE_BUF (VLAN_EVB_SCH_ENTRY, (VOID *)(*ppEvbSChIfEntry));
            (*ppEvbSChIfEntry) = NULL;

            VLAN_TRC_ARG3 (VLAN_EVB_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                    VLAN_NAME, "VlanEvbSChIfAddEntry: EvbSchScidTree addition"
                    "for UAP port %d, SVID %d, SCID %d failed in dynamic\r\n",
                    u4UapIfIndex, u4SVId, u4SChId);
            return VLAN_FAILURE;
        }
    }

	pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex);

	if (pUapIfEntry == NULL)
	{
        VLAN_TRC_ARG1 (VLAN_EVB_TRC,  ALL_FAILURE_TRC,
                VLAN_NAME, "VlanEvbSChIfAddEntry: UAP %d not exists\r\n",
                u4UapIfIndex);
		return VLAN_FAILURE;
	}



    if (u4SVId != VLAN_EVB_DEF_SVID)
    {
        (*ppEvbSChIfEntry)->i4SChIfRowStatus = CREATE_AND_WAIT;
    }
    else
    {
        /*Default S-Channel entry is created in CFA brg port type change itself.
         */
        CFA_GET_DEFAULT_SCHANNEL_IFINDEX (u4UapIfIndex, u4SChIfIndex); 
        (*ppEvbSChIfEntry)->i4SChIfIndex = (INT4) u4SChIfIndex;

        if (VlanEvbHwConfigSChInterface ((*ppEvbSChIfEntry),
                        VLAN_EVB_HW_SCH_IF_CREATE) == VLAN_FAILURE)
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                        VLAN_NAME, "VlanEvbSChIfAddEntry: "
                        "Creating HW entry for SCID-SVID (%d - %d) failed on "
                        "UAP port %d\r\n", (*ppEvbSChIfEntry)->u4SChId,
                        (*ppEvbSChIfEntry)->u4SVId, 
                        (*ppEvbSChIfEntry)->u4UapIfIndex);
                /* Sending Default S-Channel entry CREATE FAIL Trap */
                if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_CREATE_FAIL_TRAP,
                      (*ppEvbSChIfEntry)->u4UapIfIndex, 
                      (*ppEvbSChIfEntry)->u4SVId)  == VLAN_FAILURE)
                {
                    VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                            VLAN_NAME, "VlanEvbSChIfAddEntry: "
                            "EVB S-Channel creation fail Trap generation failed"
                            "for SCID-SVID (%d - %d) on"
                            "UAP port %d\r\n", (*ppEvbSChIfEntry)->u4SChId,
                            (*ppEvbSChIfEntry)->u4SVId, 
                            (*ppEvbSChIfEntry)->u4UapIfIndex);

                }
#ifdef SYSLOG_WANTED
                SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4)gi4VlanSysLogId,
                            "SCID-SVID (%d - %d) creation in hardware failed on"
                            " the UAP port %d", (*ppEvbSChIfEntry)->u4SChId, 
                            (*ppEvbSChIfEntry)->u4SVId,
                            (*ppEvbSChIfEntry)->u4UapIfIndex));
#endif
                return VLAN_FAILURE;
            }
        /* Sending Default S-Channel entry CREATE Trap */
		if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_CREATE_TRAP,
					(*ppEvbSChIfEntry)->u4UapIfIndex, 
					(*ppEvbSChIfEntry)->u4SVId)  == VLAN_FAILURE)
		{
			VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
					VLAN_NAME, "VlanEvbSChIfAddEntry: "
					"EVB S-Channel creation  Trap generation failed"
					"for SCID-SVID (%d - %d) on"
					"UAP port %d\r\n", (*ppEvbSChIfEntry)->u4SChId,
					(*ppEvbSChIfEntry)->u4SVId, 
					(*ppEvbSChIfEntry)->u4UapIfIndex);

		}
            /* UAP oper status or S-Channel oper status are same here */
            if (u1OperStatus == CFA_IF_UP)
            {
                if (VlanEvbHwConfigSChInterface ((*ppEvbSChIfEntry),
                            VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD) == VLAN_FAILURE)
                {
                    VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC,
                            VLAN_NAME, "VlanEvbSChIfAddEntry: "
                            "Traffic fwd setting on HW entry for SCID-SVID(%d - %d)"
                            " failed on  UAP port %d\r\n",
                            (*ppEvbSChIfEntry)->u4SChId, (*ppEvbSChIfEntry)->u4SVId,
                            (*ppEvbSChIfEntry)->u4UapIfIndex);

                    return VLAN_FAILURE;
                }
               if (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID)
               {
					(*ppEvbSChIfEntry)->i4NegoStatus = VLAN_EVB_SCH_CONFIRMED;
               }
            }
            else
            {
                (*ppEvbSChIfEntry)->i4NegoStatus = VLAN_EVB_SCH_FREE;
            }

        /* Default S-Channel will be confirmed state always since it 
         * is transmitted in all the pairs.*/
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4) gi4VlanSysLogId,
                        "SCID-SVID (%d - %d) is created in hardware on the "
                        " UAP port %d",
                        (*ppEvbSChIfEntry)->u4SChId, (*ppEvbSChIfEntry)->u4SVId,
                        (*ppEvbSChIfEntry)->u4UapIfIndex));
            
#endif
        (*ppEvbSChIfEntry)->i4SChIfRowStatus = ACTIVE;
    }
    if (u1Mode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
    {
        (*ppEvbSChIfEntry)->i4SChIfIndex = i4SchIndex;
        /* Indication to create SBP port */
        if (VLAN_EVB_MODULE_STATUS (pUapIfEntry->u4UapIfCompId) 
                                       == VLAN_EVB_MODULE_ENABLE)
        {
            if (VlanPortCfaCreateDynamicSChannelInterface (u4UapIfIndex, 
                        (UINT4)((*ppEvbSChIfEntry)->i4SChIfIndex)) == VLAN_FAILURE)
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                        VLAN_NAME, "VlanEvbSChIfAddEntry: SBP port can not be "
                        "created for UAP port in dynamic mode %d, SVID %d,SCID %d \r\n",
                        u4UapIfIndex, u4SVId, u4SChId);
                /* Create fail trap for all the s-channel interface
                 * Dynamic scenario */ 
                if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_CREATE_FAIL_TRAP,
                      (*ppEvbSChIfEntry)->u4UapIfIndex, 
                      (*ppEvbSChIfEntry)->u4SVId)  == VLAN_FAILURE)
                {
                    VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                            VLAN_NAME, "VlanEvbSChIfAddEntry: "
                            "EVB S-Channel creation fail Trap generation failed"
                            "for SCID-SVID (%d - %d) on"
                            "UAP port %d\r\n", u4SChId,
                             u4SVId , 
                             u4UapIfIndex);

                }
                return VLAN_FAILURE;
            }
        }
        (*ppEvbSChIfEntry)->i4SChIfRowStatus = ACTIVE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfGetEntry                                 */
/*                                                                           */
/* Description        : This routine gets a tEvbSChIfEntry  for the          */
/*                      given u4UapIfIndex + u4SVId.                         */
/*                                                                           */
/* Input(s)           : u4SVId         - SVID                                */
/*                      u4UapIfIndex   - UAP Interface Index                 */
/*                                                                           */
/* Output(s)          : pSChIfNode  - pointer to the tEvbSChIfEntry if entry */
/*                                    is present otherwise NULL.             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
tEvbSChIfEntry *
VlanEvbSChIfGetEntry (UINT4 u4UapIfIndex, UINT4 u4SVId)
{
    tEvbSChIfEntry  *pSChIfNode = NULL;
    tEvbSChIfEntry   SChIfNode;

    MEMSET (&SChIfNode, 0, sizeof (tEvbSChIfEntry));

    SChIfNode.u4UapIfIndex = u4UapIfIndex;
    SChIfNode.u4SVId = u4SVId;

    pSChIfNode  = (tEvbSChIfEntry *) (VOID *) RBTreeGet
        ((gEvbGlobalInfo.EvbSchIfTree), (tRBElem *) (VOID *) &SChIfNode);

    return pSChIfNode;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChScidIfGetEntry                             */
/*                                                                           */
/* Description        : This routine gets a tEvbSChIfEntry  for the          */
/*                      given u4UapIfIndex + u4SChId.                        */
/*                                                                           */
/* Input(s)           : u4SChId     - S-Channel Id                           */
/*                      u4UapIfIndex- UAP Interface Index.                   */
/*                                                                           */
/* Output(s)          : pSChIfNode  - pointer to the tEvbSChIfEntry if entry */
/*                                    is present otherwise NULL.             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
tEvbSChIfEntry *
VlanEvbSChScidIfGetEntry (UINT4 u4UapIfIndex, UINT4 u4SChId)
{
    tEvbSChIfEntry  *pSChIfNode = NULL;
    tEvbSChIfEntry   SChIfNode;

    MEMSET (&SChIfNode, 0, sizeof (tEvbSChIfEntry));

    SChIfNode.u4UapIfIndex = u4UapIfIndex;
    SChIfNode.u4SChId = u4SChId;

    pSChIfNode = (tEvbSChIfEntry *) (VOID *) RBTreeGet
        ((gEvbGlobalInfo.EvbSchScidTree), (tRBElem *) (VOID *) &SChIfNode);
    return pSChIfNode;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfGetFirstEntry                            */
/*                                                                           */
/* Description        : This routine is used to get first tEvbSChIfEntry     */
/*                      from gEvbGlobalInfo.EvbSchIfTree.                    */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : pSChIfNode - pointer to the tEvbSChIfEntry           */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
tEvbSChIfEntry  *
VlanEvbSChIfGetFirstEntry (VOID)
{
    tEvbSChIfEntry    *pSChIfNode = NULL;

    pSChIfNode = (tEvbSChIfEntry *) (VOID *) RBTreeGetFirst
                     (gEvbGlobalInfo.EvbSchIfTree);
    return pSChIfNode;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChScidIfGetFirstEntry                        */
/*                                                                           */
/* Description        : This routine is used to get first tEvbSChIfEntry     */
/*                      from gEvbGlobalInfo.EvbSchScidTree.                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : pSChIfNode - pointer to the tEvbSChIfEntry           */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
tEvbSChIfEntry  *
VlanEvbSChScidIfGetFirstEntry (VOID)
{
    tEvbSChIfEntry    *pSChScidIfNode = NULL;

    pSChScidIfNode = (tEvbSChIfEntry *) (VOID *) RBTreeGetFirst
                         (gEvbGlobalInfo.EvbSchScidTree);
    return pSChScidIfNode;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfGetNextEntry                             */
/*                                                                           */
/* Description        : This routine is used to get the next                 */
/*                      tEvbSChIfEntry from gEvbGlobalInfo.EvbSchIfTree for  */
/*                      the given UAP If Index and SVID.                     */
/*                                                                           */
/* Input(s)           : u4SVId      - SVID                                   */
/*                      u4UapIfIndex- UAP Interface Index.                   */
/*                                                                           */
/* Output(s)          : pSChIfNode - pointer to the tEvbSChIfEntry           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
tEvbSChIfEntry *
VlanEvbSChIfGetNextEntry (UINT4 u4UapIfIndex, UINT4 u4SVId)
{
    tEvbSChIfEntry  *pSChIfNode = NULL;
    tEvbSChIfEntry   SChIfNode;

    MEMSET (&SChIfNode, 0, sizeof (tEvbSChIfEntry));

    SChIfNode.u4UapIfIndex  = u4UapIfIndex;
    SChIfNode.u4SVId = u4SVId;

    pSChIfNode  = (tEvbSChIfEntry *) (VOID *) RBTreeGetNext
          ((gEvbGlobalInfo.EvbSchIfTree), (tRBElem *) (VOID *)&SChIfNode, NULL);
    return pSChIfNode;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChScidIfGetNextEntry                         */
/*                                                                           */
/* Description        : This routine is used to get the next                 */
/*                      tEvbSChIfEntry from gEvbGlobalInfo.EvbSchScidTree for*/
/*                      the given UAP If Index and S-Channel Id.             */
/*                                                                           */
/* Input(s)           : u4SChId     - S-Channel Id                           */
/*                      u4UapIfIndex- UAP Interface Index.                   */
/*                                                                           */
/* Output(s)          : pSChIfNode - pointer to the tEvbSChIfEntry           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
tEvbSChIfEntry *
VlanEvbSChScidIfGetNextEntry (UINT4 u4UapIfIndex, UINT4 u4SChId)
{
    tEvbSChIfEntry  *pSChIfNode = NULL;
    tEvbSChIfEntry   SChIfNode;

    MEMSET (&SChIfNode, 0, sizeof (tEvbSChIfEntry));

    SChIfNode.u4UapIfIndex  = u4UapIfIndex;
    SChIfNode.u4SChId   = u4SChId;

    pSChIfNode  = (tEvbSChIfEntry *) (VOID *) RBTreeGetNext
        ((gEvbGlobalInfo.EvbSchScidTree), (tRBElem *) (VOID *)&SChIfNode, NULL);
    return pSChIfNode;
}

/******************************************************************************/
/* Function Name      : VlanEvbSChIfDelEntry                                  */
/*                                                                            */
/* Description        : This routine deletes a tEvbSChIfEntry from the        */
/*                      gEvbGlobalInfo.EvbSchIfTree and                       */
/*                      gEvbGlobalInfo.EvbSchScidTree.                        */
/*                      Either SVID or SCID must not be zero so that it will  */
/*                      be used to delete from both RBTrees &releases memory. */
/*                                                                            */
/* Input(s)           : u4UapIfIndex - CFA interface index for UAP            */
/*                      u4SVId - SVID from 1-4094                             */
/*                      u4SCID - S-Channel Identifier                         */
/*                                                                            */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                             */
/******************************************************************************/
INT4
VlanEvbSChIfDelEntry (UINT4 u4UapIfIndex, UINT4 u4SVId, UINT4 u4SChId)
{
    tEvbSChIfEntry   *pSChIfEntry = NULL;
    tEvbSChIfEntry   *pSChScidEntry = NULL;
    tEvbUapIfEntry   *pUapIfEntry    = NULL;

    pSChIfEntry = VlanEvbSChIfGetEntry (u4UapIfIndex, u4SVId);

    if (pSChIfEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                   "VlanEvbSChIfDelEntry: Entry for UAP Port %d, SVID %d "
                   "not exists\r\n", u4UapIfIndex, u4SVId);
        /* CHECK THE RETURN TYPE */
        return VLAN_SUCCESS;
    }

    if (u4SChId == 0) 
    {
        u4SChId = pSChIfEntry->u4SChId;
    }

    pSChScidEntry = VlanEvbSChScidIfGetEntry (u4UapIfIndex, u4SChId);

    if (pSChScidEntry == NULL)
    {
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "VlanEvbSChIfDelEntry: Entry for UAP Port %d, SVID %d "
                       ",SCID %d not exists\r\n", u4UapIfIndex, u4SVId,
                        u4SChId);
        /* CHECK THE RETURN TYPE */
        return VLAN_SUCCESS;
    }
    pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex);
    if (pUapIfEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "VlanEvbSChIfDelEntry: Entry for UAP Port %d not exists "
                       "\r\n", u4UapIfIndex);
        return VLAN_SUCCESS;
    }
   
    if (VLAN_EVB_MODULE_STATUS (pUapIfEntry->u4UapIfCompId)
                                  == VLAN_EVB_MODULE_ENABLE)
    {
	    if ((pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)&&
            (pUapIfEntry->i4UapIfSchCdcpAdminEnable == VLAN_EVB_UAP_CDCP_ENABLE))
	    {
		    if (VlanPortCfaDeleteDynamicSChannelInterface
				    ((UINT4)(pSChIfEntry->i4SChIfIndex)) == VLAN_FAILURE)
		    {
			    VLAN_TRC_ARG3 (VLAN_EVB_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
					    VLAN_NAME, "VlanEvbSChIfDelEntry: SBP port deletion is "
					    "failed for UAP port %d, SVID %d, SCID %d \r\n",
					    u4UapIfIndex, pSChIfEntry->u4SVId, pSChIfEntry->u4SChId);

                /* Sending Delete S-channel interface Fail Trap for DYNAMIC
                 * scenario */ 
                if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_DELETE_FAIL_TRAP, 
                            pSChIfEntry->u4UapIfIndex, pSChIfEntry->u4SVId) 
                        == VLAN_FAILURE)
                {
                    VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                            VLAN_NAME, "VlanEvbSChIfDelEntry: "
                            "EVB S-Channel deletion fail Trap generation failed"
                            "for SCID-SVID (%d - %d) on"
                            "the UAP port %d\r\n",pSChIfEntry->u4SChId, 
                            pSChIfEntry->u4SVId,
                            u4UapIfIndex);
                }
			    return VLAN_FAILURE;
		    }
            if (VlanEvbHwConfigSChInterface (pSChIfEntry,
                        VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK) == VLAN_FAILURE)
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                        VLAN_NAME, "VlanEvbSChIfDelEntry: "
                        "Traffic Block HW entry for SCID-SVID (%d - %d) failed on "
                        "UAP port %d\r\n", pSChIfEntry->u4SChId, 
                        pSChIfEntry->u4SVId, pSChIfEntry->u4UapIfIndex);
            }
	    }
	    /* Indication to delete SBP port */
        else
	    {
		    if(pSChIfEntry->i4SChIfIndex != 0)
		    {
			    if (VlanPortCfaDeleteSChannelInterface((UINT4)(pSChIfEntry->i4SChIfIndex))
					    == VLAN_FAILURE)
			    {
				    VLAN_TRC_ARG3 (VLAN_EVB_TRC, OS_RESOURCE_TRC|ALL_FAILURE_TRC,
						    VLAN_NAME, "VlanEvbSChIfDelEntry: SBP port deletion"
						    " is failed for UAP port %d, SVID %d, SCID %d \r\n",
						    u4UapIfIndex, pSChIfEntry->u4SVId,
                             pSChIfEntry->u4SChId);
					if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_DELETE_FAIL_TRAP, 
								pSChIfEntry->u4UapIfIndex, pSChIfEntry->u4SVId) 
							== VLAN_FAILURE)
					{
						VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
								VLAN_NAME, "VlanEvbSChIfDelEntry: "
								"EVB S-Channel deletion fail Trap generation failed"
								"for SCID-SVID (%d - %d) on"
								"the UAP port %d\r\n", pSChIfEntry->u4SChId, 
								pSChIfEntry->u4SVId,
								u4UapIfIndex);
					}
				    return VLAN_FAILURE;
			    }
		    }
	    }
    }

    /* Deleting the underlying hardware S-Channel entry */
    if(pSChIfEntry->i4SChIfRowStatus == ACTIVE)
    {
        if (pSChIfEntry->i4NegoStatus != VLAN_EVB_SCH_FREE)
        {    
            /* Making Negotiation status as free becuase the pair
             * should not be updated in CDCP-TLV */
            pSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_FREE;
            VlanEvbCdcpConstructTlv (pUapIfEntry);
            /* Once changed from UAP  to CBP,
             * DeletePortInfo will be Called , where 
             * LLDPAPP Info will be Flushed, so to 
             * Avoid  Stale Entries for created 
             * S-channels VLAN_FAILURE is removed */   
            VlanEvbPortPostMsgToCdcp (pUapIfEntry,
                        VLAN_EVB_CDCP_LLDP_PORT_UPDATE);
        }
        if (((pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
            && (pUapIfEntry->i4UapIfCdcpOperState == VLAN_EVB_UAP_CDCP_NOT_RUNNING))
            || (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID))
        {
            if (VlanEvbHwConfigSChInterface (pSChIfEntry,
                        VLAN_EVB_HW_SCH_IF_DELETE) == VLAN_FAILURE)
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                        VLAN_NAME, "VlanEvbSChIfDelEntry: "
                        "Deleting HW entry for SCID-SVID (%d - %d) failed on "
                        "UAP port %d\r\n", pSChIfEntry->u4SChId, 
                        pSChIfEntry->u4SVId, pSChIfEntry->u4UapIfIndex);

                if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_DELETE_FAIL_TRAP, 
                            pSChIfEntry->u4UapIfIndex, pSChIfEntry->u4SVId) 
                        == VLAN_FAILURE)
                {
                    VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                            VLAN_NAME, "VlanEvbSChIfDelEntry: "
                            "EVB S-Channel deletion fail Trap generation failed"
                            "for SCID-SVID (%d - %d) on"
                            "the UAP port %d\r\n", pSChIfEntry->u4SChId, 
                            pSChIfEntry->u4SVId,
                            pSChIfEntry->u4UapIfIndex);
                }
#ifdef SYSLOG_WANTED
                SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4)gi4VlanSysLogId,
                            "SCID-SVID (%d - %d) deletion is failed in hardware  "
                            "on the UAP port %d",
                            pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                            pSChIfEntry->u4UapIfIndex));

#endif
                return VLAN_FAILURE;
            }
        }
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4) gi4VlanSysLogId,
                        "SCID-SVID (%d - %d) is deleted in hardware on the UAP "
                        "port %d",
                        pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                        pSChIfEntry->u4UapIfIndex));
#endif
    }

    if (((pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
                && (pUapIfEntry->i4UapIfCdcpOperState == VLAN_EVB_UAP_CDCP_NOT_RUNNING))
            || (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID))
    {
        /* Removing from RBTree1 - Keys UAP port + SVID */
        if (RBTreeRemove ((gEvbGlobalInfo.EvbSchIfTree),
                    (tRBElem *)(VOID *) pSChIfEntry) == RB_FAILURE)
        {
            VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbSChIfDelEntry: RBTree remove for UAP port %d, "
                    "SVID %d, SCID %d for EvbSchIfTree failed\r\n", 
                    u4UapIfIndex, u4SVId, u4SChId);
            /* Do not return here since have to remove from another RBTree */
        }

        if (pSChScidEntry != NULL)
        {
            /* Removing from RBTree2 - Keys UAP port + SCID */
            if (RBTreeRemove ((gEvbGlobalInfo.EvbSchScidTree),
                        (tRBElem *)(VOID *) pSChIfEntry) == RB_FAILURE)
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                        "VlanEvbSChIfDelEntry: RBTree remove for UAP port %d, "
                        "SVID %d, SCID %d for EvbSchScidTree failed\r\n", 
                        u4UapIfIndex, u4SVId, u4SChId);
                /* Need not return here since memory release to be done */
            }
        }
        /* Send the notification to LLDP  regarding a new static s-channel has 
         *   been deleted */
		if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_DELETE_TRAP,
					u4UapIfIndex,u4SVId) == VLAN_FAILURE)
		{
			VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
					VLAN_NAME, "VlanEvbSChIfDelEntry: "
					"EVB S-Channel deletion success Trap generation failed"
					"for SCID-SVID (%d - %d) on"
					"the UAP port %d\r\n", pSChIfEntry->u4SChId, 
                    pSChIfEntry->u4SVId,
					pSChIfEntry->u4UapIfIndex);
		}
        if ((pUapIfEntry->pEvbLldpSChannelIfNotify != NULL) && 
                (u4SVId != VLAN_EVB_DEF_SVID))
        {
            pUapIfEntry->pEvbLldpSChannelIfNotify
                (u4UapIfIndex,u4SVId,VLAN_EVB_SVID_DELETE);
        }
        VLAN_EVB_RELEASE_BUF (VLAN_EVB_SCH_ENTRY, (VOID *)(pSChIfEntry));
        pSChIfEntry = NULL;
        UNUSED_PARAM(pSChIfEntry);
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfDelAllEntries                            */
/*                                                                           */
/* Description        : This function deletes all the tEvbSChIfEntry         */
/*                      entries of gEvbGlobalInfo.EvbSchIfTree and           */
/*                      gEvbGlobalInfo.EvbSchScidTree and releases memory    */
/*                      which are present in ALL the UAP ports.              */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
VlanEvbSChIfDelAllEntries (VOID)
{
    tEvbSChIfEntry   *pSChIfNode = NULL;
    tEvbSChIfEntry   *pNextSchIfNode = NULL;

    pSChIfNode = VlanEvbSChIfGetFirstEntry ();

    while (pSChIfNode != NULL)
    {
        /* Get the next entry */
        pNextSchIfNode = VlanEvbSChIfGetNextEntry (pSChIfNode->u4UapIfIndex,
                                                   pSChIfNode->u4SVId);

        /* Deletes the entry from EvbSchIfTree and EvbSchScidTree
         * and releases the memory of pSChIfNode. */
        VlanEvbSChIfDelEntry (pSChIfNode->u4UapIfIndex, pSChIfNode->u4SVId, 
                              pSChIfNode->u4SChId);
        pSChIfNode = pNextSchIfNode;
    }
    return; 
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfDelAllEntriesOnUap                       */
/*                                                                           */
/* Description        : This function deletes all the tEvbSChIfEntry         */
/*                      entries from gEvbGlobalInfo.EvbSchIfTree and         */
/*                      gEvbGlobalInfo.EvbSchScidTree and releases memory    */
/*                      which are present in the given UAP port.             */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP Interface Index.                  */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID 
VlanEvbSChIfDelAllEntriesOnUap (UINT4 u4UapIfIndex)
{
    tEvbSChIfEntry   *pSChIfNode = NULL;
    tEvbSChIfEntry   *pNextSchIfNode = NULL;

    /* To retrieve the first S-Ch entry present in this UAP, 
     * SVID is passed as zero */
    pSChIfNode = VlanEvbSChIfGetNextEntry (u4UapIfIndex, VLAN_EVB_DEF_SVID);

    while (pSChIfNode != NULL)
    {
        if (u4UapIfIndex != pSChIfNode->u4UapIfIndex)
        {
            /* All the S-Ch entries on this UAP port is deleted. */
            break;
        }
        /* Get the next entry */
        pNextSchIfNode = VlanEvbSChIfGetNextEntry (pSChIfNode->u4UapIfIndex,
                                                   pSChIfNode->u4SVId);

        /* Deletes the entry from EvbSchIfTree and EvbSchScidTree
         * and releases the memory of pSChIfNode. */
        VlanEvbSChIfDelEntry (pSChIfNode->u4UapIfIndex, pSChIfNode->u4SVId, 
                              pSChIfNode->u4SChId);
        pSChIfNode = pNextSchIfNode;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : VlanEvbGetFreeSChIfIndex                             */
/*                                                                           */
/* Description        :This function provides the unique interface index for */
/*                     the  S-Channel interface for the given UAP Port.      */
/*                     Using this interface index, SBP port is               */
/*                     created in CFA and in the respective layer 2 modules  */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP interface index                   */
/*                                                                           */
/* Output(s)          : u4IfIndex - Unique interface index if available      */
/*                      zero - otherwise                                     */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
UINT4 
VlanEvbGetFreeSChIfIndex (UINT4 u4UapIfIndex)
{
     /* It is called from VlanEvbSChIfAddEntry function  */
    UINT4 u4IfIndex = 0;
    
    if (VlanPortCfaGetFreeSChIndexForUap (u4UapIfIndex, &u4IfIndex) 
                == VLAN_FAILURE) 
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
           VLAN_NAME,"VlanEvbGetFreeSChIfIndex: No free SBP ports for UAP port "
           "%d is available\r\n", u4UapIfIndex);
        return 0;
    }
    return u4IfIndex;
}

/****************************************************************************/
/* Function Name      : VlanEvbIsSChannelIdAvailable                        */
/* Description        : This function checks whether the given S-Channel    */
/*                      identifier is available on this UAP.                */
/*                      This is required when the user configures the S-ch  */
/*                      identifier in case of  i4EvbSysSChMode  is          */
/*                       VLAN_EVB_SCH_MODE_MANUAL                           */
/*                                                                          */
/* Input(s)           : u4UapIfIndex - UAP interface index                  */
/*                      u4SVId - SVID                                       */
/*                      u4SChId - S-Channel Identifier                      */
/*                                                                          */
/* Output(s)          : VLAN_TRUE -if the S-Channel identifier is available */
/*                      VLAN_FALSE -if the S-Channel identifier is already  */
/*                                  in use                                  */
/* Return Value(s)    : NONE                                                */
/****************************************************************************/
UINT1 
VlanEvbIsSChannelIdAvailable (tEvbUapIfEntry *pUapIfEntry, UINT4 u4SVId,
                              UINT4 u4SChId)
{
    /* this function is be called from SNMP */
    tEvbSChIfEntry *pSChIfEntry = NULL;

    if(VLAN_EVB_CURR_CONTEXT_PTR()->i4EvbSysSChMode != VLAN_EVB_SCH_MODE_MANUAL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanEvbIsSChannelIdAvailable: Mode is auto, cannot configure"
                  " S-Channel Id in this context %d\r\n", 
                  VLAN_EVB_CURR_CONTEXT_PTR()->u4EvbSysCxtId);
        return VLAN_FALSE;
    }

    pSChIfEntry = VlanEvbSChIfGetEntry  (pUapIfEntry->u4UapIfIndex, u4SVId);
    if (pSChIfEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "VlanEvbIsSChannelIdAvailable: Mode is manual:"
                  " S-Channel Id is available in this context %d\r\n", 
                  VLAN_EVB_CURR_CONTEXT_PTR()->u4EvbSysCxtId);
        return VLAN_TRUE;
    }

    /* Configured S-Channel entry must not configured with any SCID */
    if ((pSChIfEntry != NULL) && (pSChIfEntry->u4SChId == 0))
    {
        /* The SCID may be part of some other SVId. So the below validation */
        pSChIfEntry = VlanEvbSChScidIfGetEntry (pUapIfEntry->u4UapIfIndex, 
                            u4SChId);
        if (pSChIfEntry == NULL)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                    "VlanEvbIsSChannelIdAvailable: Mode is manual:"
                    " S-Channel Id is available in this context %d\r\n", 
                    VLAN_EVB_CURR_CONTEXT_PTR()->u4EvbSysCxtId);
            return VLAN_TRUE;
        }
    }
    VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
            "VlanEvbIsSChannelIdAvailable: Mode is manual:"
            " S-Channel Id is not available in this context %d\r\n", 
            VLAN_EVB_CURR_CONTEXT_PTR()->u4EvbSysCxtId);
    return VLAN_FALSE;        
}

/****************************************************************************/
/* Function Name      : VlanEvbSChGetNextSChannelId                         */
/* Description        : This function checks whether the SCID is avialable  */
/*                      on this UAP, This function should be called only in */
/*                       VLAN_EVB_SCH_MODE_AUTO                             */
/*                                                                          */
/* Input(s)           : u4UapIfIndex - UAP interface index                  */
/*                      u4SVId - SVID                                       */
/*                                                                          */
/* Output(s)          : u4Scid -if the S-Channel identifier is available    */
/*                      0      - if the S-Channel identifier is already     */
/*                                  in use                                  */
/* Return Value(s)    : NONE                                                */
/****************************************************************************/
UINT4 
VlanEvbSChGetNextSChannelId (UINT4 u4UapIfIndex)
{
    tEvbSChIfEntry *pSChIfEntry      = NULL;
    tEvbUapIfEntry *pUapIfEntry      = NULL;
    UINT4           u4SChId          = VLAN_EVB_DEF_SCID;
    UINT4           u4LocUapIndex    = u4UapIfIndex;

    if(VLAN_EVB_CURR_CONTEXT_PTR()->i4EvbSysSChMode != VLAN_EVB_SCH_MODE_AUTO)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanEvbSChGetNextSChannelId: Mode is manual, user must give"
                  " S-Channel Id in this context %d\r\n", 
                  VLAN_EVB_CURR_CONTEXT_PTR()->u4EvbSysCxtId);
        return 0;
    }

    if ((pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex)) == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanEvbSChGetNextSChannelId: Fetching UAP entry for UAP port"
                  " %d failed\r\n", u4UapIfIndex);
        return 0;
    }

    while ((pSChIfEntry = VlanEvbSChScidIfGetNextEntry (u4LocUapIndex,
                            u4SChId)) != NULL)
    {
        if (u4UapIfIndex != pSChIfEntry->u4UapIfIndex)
        {
            break;
        }
        u4LocUapIndex = pSChIfEntry->u4UapIfIndex;
        u4SChId = pSChIfEntry->u4SChId;
    }

    /* Assigning to the next available index */
    u4SChId = u4SChId + 1;

    /* NOTE: Is the following check required? */
    if (u4SChId <= (UINT4) pUapIfEntry->i4UapIfSchAdminCdcpChanCap)
    {
        return u4SChId; 
    }
    VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
            "VlanEvbSChGetNextSChannelId: SChId is not available for UAP "
            " %d\r\n", u4UapIfIndex);
    return 0;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfUpdateLocalPort                          */
/*                                                                           */
/* Description        : This function updates the local port number for the  */
/*                      S-Channel interface. Local port number is derived    */
/*                      from VCM whereas at the time of creating of S-Channel*/ 
/*                      interface in EVB, it is not created in VCM and hence */
/*                      the local port would be set as zero initially.After  */ 
/*                      it s created in CFA & VCM, the port create indication*/
/*                      for S-Channel interface would be coming from L2IWF to*/
/*                      VLAN and here the local port is updated.             */
/*                                                                           */
/* Input(s)           : u4SChIfIndex - SBP interface index                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4 
VlanEvbSChIfUpdateLocalPort (UINT4 u4SChIfIndex)
{
    /* This function is called from VlanHandleCreatePort for SBP */
    tEvbSChIfEntry *pSChIfEntry     = NULL;
    tEvbUapIfEntry *pUapIfEntry     = NULL;
    UINT4          u4UapIfIndex     = 0;
    UINT4          u4SVId           = 0;
    UINT4	       u4ContextId      = 0;
    UINT2	       u2LocalPortId    = 0;

    VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC,
            VLAN_NAME,"VlanEvbSChIfUpdateLocalPort "
            "Local port number update hit for SChIfIndex %d \n", 
            u4SChIfIndex);

    if (VlanVcmGetContextInfoFromIfIndex (u4SChIfIndex, &u4ContextId,
            &u2LocalPortId) != VCM_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
            VLAN_NAME,"VlanEvbSChIfUpdateLocalPort "
            "Could not update local port number for SChIfIndex %d \n", 
            u4SChIfIndex);
        return VLAN_FAILURE;
    }
    while ((pSChIfEntry = VlanEvbSChIfGetNextEntry (u4UapIfIndex, u4SVId))
            != NULL)
    {
        if (pSChIfEntry->i4SChIfIndex == ((INT4)u4SChIfIndex))
        {
            /* Setting the local port for the SBP */
            pSChIfEntry->u4SChIfPort = u2LocalPortId;
            VLAN_TRC_ARG2 (VLAN_EVB_TRC, CONTROL_PLANE_TRC,
                    VLAN_NAME,"VlanEvbSChIfUpdateLocalPort "
                    "Updated local port number %d for SChIfIndex %d \n", 
                    u2LocalPortId, u4SChIfIndex);

            pUapIfEntry = VlanEvbUapIfGetEntry(pSChIfEntry->u4UapIfIndex); 

            /* When CDCP is Running and Mode is Dynamic alone S-channel's 
             * should be Created */
            if  ((pUapIfEntry != NULL ) &&
                 (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
                 && (pUapIfEntry->i4UapIfCdcpOperState == VLAN_EVB_UAP_CDCP_RUNNING))
            {
                if (VlanEvbSChHwIndication(pSChIfEntry,
                            VLAN_EVB_HW_SCH_IF_CREATE) == VLAN_FAILURE)
                {
                    VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                            VLAN_NAME, "VlanEvbSChIfUpdateLocalPort: "
                            " HW entry Creation for SCID-SVID (%d - %d) failed on "
                            "UAP port %d\r\n", pSChIfEntry->u4SChId, 
                            pSChIfEntry->u4SVId, pSChIfEntry->u4UapIfIndex);
                    /* Create FAIL TRAP EVB-DYNAMIC Case*/
					if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_CREATE_FAIL_TRAP,
								pSChIfEntry->u4UapIfIndex, pSChIfEntry->u4SVId)
							== VLAN_FAILURE)
					{

						VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
								VLAN_NAME, "VlanEvbSChIfUpdateLocalPort: "
								"EVB S-Channel creation fail Trap generation failed"
								"for SCID-SVID (%d - %d) on"
								"the UAP port %d\r\n", pSChIfEntry->u4SChId, 
								pSChIfEntry->u4SVId, pSChIfEntry->u4UapIfIndex);
					}
                    return VLAN_FAILURE;
                }
                /* Create Success Trap EVB-DYNAMIC Case*/
				if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_CREATE_TRAP,
							pSChIfEntry->u4UapIfIndex, pSChIfEntry->u4SVId)
						== VLAN_FAILURE)
				{

					VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
							VLAN_NAME, "VlanEvbSChIfUpdateLocalPort: "
							"EVB S-Channel creation fail Trap generation failed"
							"for SCID-SVID (%d - %d) on"
							"the UAP port %d\r\n", pSChIfEntry->u4SChId, 
							pSChIfEntry->u4SVId, pSChIfEntry->u4UapIfIndex);
				}
                  
            }
            return VLAN_SUCCESS;
        }
        u4UapIfIndex = pSChIfEntry->u4UapIfIndex;
        u4SVId = pSChIfEntry->u4SVId;
    }
    return VLAN_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChSetStaticEntriesOperUp                     */
/*                                                                           */
/* Description        : This function sets the operational status of all     */
/*                      the S-Channel entries present in the UAP port and    */
/*                      programs those entries in the hardware based on its  */
/*                      admin status is UP.                                  */
/*                                                                           */
/* Input(s)           : pUapIfEntry  - UAP interface entry.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS if operational status is set            */
/*                      successfully VLAN_FAILURE otherwise                  */
/*****************************************************************************/
INT4
VlanEvbSChSetStaticEntriesOperUp (tEvbUapIfEntry *pUapIfEntry)
{
	tEvbSChIfEntry  *pSChIfEntry = NULL;
    UINT4            u4UapIfIndex = pUapIfEntry->u4UapIfIndex;
    UINT4            u4SVId = 0;
    INT4             i4RetVal = VLAN_SUCCESS;

    while ((pSChIfEntry = VlanEvbSChIfGetNextEntry (u4UapIfIndex, u4SVId))
                            != NULL)
    {
        if (pSChIfEntry->u4UapIfIndex != pUapIfEntry->u4UapIfIndex)
        {
            /* Configured all the S-Channel entries on this UAP port.
             * Take a breath by breaking */
            break;
        }
        if (VlanEvbSChSetOperUp (pSChIfEntry) == VLAN_FAILURE)
        {
            i4RetVal = VLAN_FAILURE;
            VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                    VLAN_NAME,"VlanEvbSChSetStaticEntriesOperUp : "
                    "Failed to set operational status for UAP %d SVID %d "
                    "SChIfIndex %d\n",pSChIfEntry->u4UapIfIndex, u4SVId,
                    pSChIfEntry->i4SChIfIndex);

            /* No return here since to program other entries */
        }
        u4UapIfIndex = pSChIfEntry->u4UapIfIndex;
        u4SVId = pSChIfEntry->u4SVId;
    }
    /* VLAN_FAILURE may occur if one or many or all the S-Channel entries
     * programming is/are failed. */
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChSetOperUp                                  */
/*                                                                           */
/* Description        : This function sets the operational status of         */
/*                      the S-Channel entry provided for the UAP port and    */
/*                      programs the entry in the hardware if its admin      */
/*                      status is UP.                                        */
/*                                                                           */
/* Input(s)           : pSChIfEntry  - S-Channel Entry.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS if operational status is set            */
/*                      successfully VLAN_FAILURE otherwise                  */
/*****************************************************************************/
INT4
VlanEvbSChSetOperUp (tEvbSChIfEntry *pSChIfEntry)
{
    /* This is called when CDCP negotiation completes and default pair up case*/
    if (pSChIfEntry->u1AdminStatus == CFA_IF_UP)
    {
        if (VlanEvbHwConfigSChInterface (pSChIfEntry,
                    VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                    VLAN_NAME, "VlanEvbSChSetOperUp: "
                    "Programming for SCID-SVID (%d - %d) failed on "
                    "UAP port %d\r\n", pSChIfEntry->u4SChId, pSChIfEntry->
                    u4SVId, pSChIfEntry->u4UapIfIndex);
            return VLAN_FAILURE;
        }
    }
    else /* CFA_IF_DOWN */
    {
        /* NOTE: is this correct? Oper is setting up though admin is down */
        VLAN_TRC_ARG4 (VLAN_EVB_TRC, DATA_PATH_TRC, VLAN_NAME, 
                  "VlanEvbSChSetOperUp: "
                  "Programming for SCID-SVID (%d - %d) on UAP port %d can not "
                  "be done since the respective SBP port %d is administratively"
                   " down\r\n", pSChIfEntry->u4SChId, pSChIfEntry->u4SVId, 
                   pSChIfEntry->u4UapIfIndex, pSChIfEntry->i4SChIfIndex);
    }
    /* Setting oper up in the S-Channel entry */
    pSChIfEntry->u1OperStatus = CFA_IF_UP;
#ifdef  SYSLOG_WANTED  
    SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4) gi4VlanSysLogId,
            "SBP oper Status is [UP] for SCID-SVID (%d - %d) on the UAP port %d",
            pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
            pSChIfEntry->u4UapIfIndex));
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChSetStaticEntriesOperDown                   */
/*                                                                           */
/* Description        : This function sets the operational status of all     */
/*                      the S-Channel entries present in the UAP port and    */
/*                      deletes the entries from  the hardware based on its  */
/*                      admin status is UP. If admin is down, then the       */
/*                      entries are already deleted from the hardware.       */
/*                                                                           */
/* Input(s)           : pUapIfEntry  - UAP interface entry.                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS if operational status is set            */
/*                      successfully VLAN_FAILURE otherwise                  */
/*****************************************************************************/
INT4
VlanEvbSChSetStaticEntriesOperDown (tEvbUapIfEntry *pUapIfEntry, UINT1 u1Ageout)
{
	tEvbSChIfEntry  *pSChIfEntry = NULL;
    UINT4            u4UapIfIndex = pUapIfEntry->u4UapIfIndex;
    UINT4            u4SVId = 0;
    INT4             i4RetVal = VLAN_SUCCESS;

    if (u1Ageout == OSIX_TRUE)
    {
        u4SVId = VLAN_EVB_DEF_SVID;
    }

    while ((pSChIfEntry = VlanEvbSChIfGetNextEntry (u4UapIfIndex, u4SVId))
                            != NULL)
    {
        if (pSChIfEntry->u4UapIfIndex != pUapIfEntry->u4UapIfIndex)
        {
            /* Deleted all the S-Channel entries on this UAP port.
             * Take a breath by breaking */
            break;
        }
        if (pSChIfEntry->u1OperStatus == CFA_IF_DOWN) 
        {
            u4UapIfIndex = pSChIfEntry->u4UapIfIndex;
            u4SVId = pSChIfEntry->u4SVId;
            continue;
        }
        if (pSChIfEntry->u1AdminStatus == CFA_IF_UP)
        {
            if (VlanEvbHwConfigSChInterface (pSChIfEntry,
                        VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK) == VLAN_FAILURE)
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                        VLAN_NAME, "VlanEvbSChSetOperDown: "
                        "traffic block HW entry for SCID-SVID (%d - %d) failed "
                        "on UAP port %d\r\n", pSChIfEntry->u4SChId, 
                        pSChIfEntry->u4SVId, pSChIfEntry->u4UapIfIndex);

#ifdef SYSLOG_WANTED
                SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4)gi4VlanSysLogId,
                            "SBP oper Status is [DOWN] for SCID-SVID (%d - %d) "
                            "on the UAP port %d",
                            pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                            pSChIfEntry->u4UapIfIndex));
#endif
                i4RetVal = VLAN_FAILURE;
            }
            pSChIfEntry->u1OperStatus = CFA_IF_DOWN;
        }
        if (u1Ageout == OSIX_TRUE)
        {
            VlanPortCfaUpdateSChannelOperStatus (((UINT4)pSChIfEntry->i4SChIfIndex),
                                                 CFA_IF_DOWN);
        }
        pSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_FREE;
#ifdef L2RED_WANTED
        if (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID)
        {
            /* Updating the S-Channel state information
             *  to STANDBY node  */
            if (VlanEvbRedSendDynamicUpdates (
                        (UINT1) VLAN_EVB_RED_SBP_ADD_SYNC_UP,
                        (VOID *) pSChIfEntry) == VLAN_FAILURE)
            {
                VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                        "VlanEvbSChSetStaticEntriesOperDown: S-Channel"
                        " Negotiation status sync up "
                        "failed on the standby\n");
            }
        }
#endif
        u4UapIfIndex = pSChIfEntry->u4UapIfIndex;
        u4SVId = pSChIfEntry->u4SVId;
        /* Fix for bug 12233:
         * TAGLLDP : Tagged LLDP PDUs are sent though EVB module is disabled
         */
		if ((pUapIfEntry->pEvbLldpSChannelIfNotify != NULL) &&
				(u4SVId != VLAN_EVB_DEF_SVID))
		{
			pUapIfEntry->pEvbLldpSChannelIfNotify
				(u4UapIfIndex,u4SVId,VLAN_EVB_SVID_DELETE);
		}

    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChSetOperDown                                */
/*                                                                           */
/* Description        : This function sets the operational status of         */
/*                      the S-Channel entry provided for the UAP port and    */
/*                      deletes the entry from the hardware if its admin     */
/*                      status is UP. If the admin status is down, then the  */
/*                      entry is already deleted from the hardware.          */
/*                                                                           */
/* Input(s)           : pSChIfEntry  - S-Channel Entry.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS if operational status is set            */
/*                      successfully VLAN_FAILURE otherwise                  */
/*****************************************************************************/
INT4
VlanEvbSChSetOperDown (tEvbSChIfEntry *pSChIfEntry)
{
    /* This is called when CDCP negotiation deletes and default pair down */
    if (pSChIfEntry->u1AdminStatus == CFA_IF_UP)
    {
        if (VlanEvbHwConfigSChInterface (pSChIfEntry,
                    VLAN_EVB_HW_SCH_IF_DELETE) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                    VLAN_NAME, "VlanEvbSChSetOperDown: "
                    "Deleting HW entry for SCID-SVID (%d - %d) failed on "
                    "UAP port %d\r\n", pSChIfEntry->u4SChId, 
                    pSChIfEntry->u4SVId, pSChIfEntry->u4UapIfIndex);
            if (VlanEvbSendSChannelTrap (VLAN_EVB_SBP_DELETE_FAIL_TRAP, 
                pSChIfEntry->u4UapIfIndex, pSChIfEntry->u4SVId) == VLAN_FAILURE)
            {

                VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                        VLAN_NAME, "VlanEvbSChSetOperDown: "
                        "EVB S-Channel deletion fail Trap generation failed"
                        "for SCID-SVID (%d - %d) on"
                        "the UAP port %d\r\n", pSChIfEntry->u4SChId, 
                        pSChIfEntry->u4SVId,
                        pSChIfEntry->u4UapIfIndex);

            }
#ifdef  SYSLOG_WANTED  
            SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4) gi4VlanSysLogId,
                        "SCID-SVID (%d - %d) deletion from hardware is failed "
                        "on the UAP port %d",
                        pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                        pSChIfEntry->u4UapIfIndex));
#endif
            return VLAN_FAILURE;
        }
    }
    else /* CFA_IF_DOWN */
    {

        /* NOTE: is this correct? Oper is setting up though admin is down.
         * Expectation: HW entry is deleted when admin down was done.*/
        /* Setting oper down in the S-Channel entry */
        VLAN_TRC_ARG4 (VLAN_EVB_TRC, DATA_PATH_TRC, VLAN_NAME, 
                  "VlanEvbSChSetOperDown: "
                  "HW entry is already deleted for SCID-SVID (%d - %d) on UAP "
                  "port %d when its respective SBP port %d was made "
                  "administratively down\r\n",
                   pSChIfEntry->u4SChId, pSChIfEntry->u4SVId, 
                   pSChIfEntry->u4UapIfIndex, pSChIfEntry->i4SChIfIndex);
    }
    /* Setting oper down in the S-Channel entry */
    pSChIfEntry->u1OperStatus = CFA_IF_DOWN;

    if (VlanEvbSendSChannelStatusTrap (pSChIfEntry) == VLAN_FAILURE)
    {
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                VLAN_NAME, "VlanEvbSChSetOperDown: "
                "EVB S-Channel status Trap generation failed"
                "for SCID-SVID (%d - %d) on"
                "the UAP port %d\r\n", pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                pSChIfEntry->u4UapIfIndex);
    }
#ifdef  SYSLOG_WANTED  
    SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL, (UINT4)gi4VlanSysLogId,
            "SBP oper Status is [DOWN] for SCID-SVID (%d - %d) on the UAP port %d",
            pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
            pSChIfEntry->u4UapIfIndex));

    SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL, (UINT4)gi4VlanSysLogId,
            "SCID-SVID (%d - %d) is deleted from hardware on the UAP port %d",
            pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
            pSChIfEntry->u4UapIfIndex));
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChSetStaticEntriesOperStatus                 */
/*                                                                           */
/* Description        : This function is the superset function where it      */
/*                      decides to set the operational status of the         */
/*                      S-Channel entries for the UAP port which has static  */
/*                      S-Channel entries.                                   */
/*                      - For static, the oper status is set for all the     */
/*                        underlying S-Channel entries.                      */
/*                                                                           */
/* Input(s)           : pUapIfEntry  - UAP Interface Entry                   */
/*                      u1OperStatus - operational status                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS if operational status is set            */
/*                      successfully VLAN_FAILURE otherwise                  */
/*****************************************************************************/
INT4
VlanEvbSChSetStaticEntriesOperStatus (tEvbUapIfEntry *pUapIfEntry,
                                      UINT1 u1OperStatus)
{
    if (u1OperStatus == CFA_IF_UP)
    {
        return (VlanEvbSChSetStaticEntriesOperUp (pUapIfEntry));
    }
    else /* CFA_IF_DOWN */
    {
        return (VlanEvbSChSetStaticEntriesOperDown (pUapIfEntry, OSIX_FALSE));
    }
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChSetDynamicEntryOperStatus                  */
/*                                                                           */
/* Description        : This function is the superset function where it      */
/*                      decides to set the operational status of the         */
/*                      S-Channel entry for the UAP port which is in dynamic */
/*                      or hybrid mode.                                      */
/*                      - For dynamic/hybrid - the oper status is set for the*/
/*                        default S-Channel entry present on the UAP.        */
/*                      - CDCP is enabled - the oper status is respectively  */
/*                         set based on the S-Channel establishment.         */
/*                                                                           */
/* Input(s)           : pUapIfEntry  - UAP Interface Entry                   */
/*                      u1OperStatus - operational status                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS if operational status is set            */
/*                      successfully VLAN_FAILURE otherwise                  */
/*****************************************************************************/
INT4
VlanEvbSChSetDynamicEntryOperStatus (tEvbSChIfEntry *pSChIfEntry,
                                     UINT1 u1OperStatus)
{
    if (u1OperStatus == CFA_IF_UP)
    {
        return (VlanEvbSChSetOperUp (pSChIfEntry));
    }
    else /* CFA_IF_DOWN */
    {
        return (VlanEvbSChSetOperDown (pSChIfEntry));
    }
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChSetAllEntriesOperStatus                    */
/*                                                                           */
/* Description        : This function is the superset function where it      */
/*                      decides to set the operational status of the         */
/*                      S-Channel entr(y)(ies) based on the UAP port mode    */
/*                      (static/dynamic/hybrid).                             */
/*                      - For static, the oper status is set for all the     */
/*                        underlying S-Channel entries.                      */
/*                      - For dynamic/hybrid - the oper status is set for the*/
/*                        default S-Channel entry present on the UAP.        */
/*                                                                           */
/* Input(s)           : pUapIfEntry  - UAP Interface Entry                   */
/*                      u1OperStatus - operational status                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS if operational status is set            */
/*                      successfully VLAN_FAILURE otherwise                  */
/*****************************************************************************/
INT4
VlanEvbSChSetAllEntriesOperStatus (tEvbUapIfEntry *pUapIfEntry, 
                                   UINT1 u1OperStatus)
{
    tEvbSChIfEntry *pSChIfEntry = NULL;
    if (pUapIfEntry->i4UapIfSchCdcpAdminEnable == VLAN_EVB_UAP_CDCP_DISABLE) 
    {
        return (VlanEvbSChSetStaticEntriesOperStatus (pUapIfEntry, 
                                                      u1OperStatus));
    }
    else /* VLAN_EVB_UAP_SCH_MODE_HYBRID/VLAN_EVB_UAP_SCH_MODE_DYNAMIC */
    {
        /* DYNAMIC MODE TO BE TAKEN CARE */
        if ((pSChIfEntry =VlanEvbSChIfGetEntry (pUapIfEntry->u4UapIfIndex, 
                                                VLAN_EVB_DEF_SVID)) == NULL)
        {
            VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
                    VLAN_NAME,"VlanEvbSChSetAllEntriesOperStatus: "
                    "Retrieving default S-Channel entry for UAP port "
                    " %d failed\r\n", pUapIfEntry->u4UapIfIndex);
            return VLAN_FAILURE;
        }
        if ((u1OperStatus == CFA_IF_UP) && 
            (pSChIfEntry->u1AdminStatus == CFA_IF_UP))
        {    
            if (VlanEvbHwConfigSChInterface (pSChIfEntry,
                        VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD) == VLAN_FAILURE)
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                        VLAN_NAME, "VlanEvbSChSetAllEntriesOperStatus: "
                        "Setting Traffic Forward of HW entry for SCID-SVID "
                        "(%d - %d) failed on UAP port %d\r\n", 
                        pSChIfEntry->u4SChId, pSChIfEntry->u4SVId, 
                        pSChIfEntry->u4UapIfIndex);

                return VLAN_FAILURE;
             }
#ifdef SYSLOG_WANTED
            SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4) gi4VlanSysLogId,
                        "SBP oper Status is [UP] for SCID-SVID (%d - %d) on the"
                        " UAP port %d",
                        pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                        pSChIfEntry->u4UapIfIndex));
#endif
            if (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID)
            {
                pSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_CONFIRMED;
            }
            pSChIfEntry->u1OperStatus = CFA_IF_UP;
        } 
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbSChIfSetAdminStatus                           */
/*                                                                           */
/* Description        : This function sets the admins status of the S-Channel*/
/*                      interface. Before that it does the following actions.*/
/*                      When the status is UP, S-Channel interface is        */
/*                      programmed in the hardware.When the status is DOWN,  */
/*                      -Deletes the S-Channelinterface entry fromthehardware*/
/*                      -Does not remove this (SCID, SVID) pair from CDCP TLV*/ 
/*                      that are already in progress. This utility gives the */
/*                      provision to the administrator to make a particular  */
/*                      S-Channel UP or DOWN when all other S-Channels are   */
/*                      active in an UAP.Though the argument is said as      */
/*                      u1OperStatus, since the S-Channel interfaceisalogical*/
/*                      interface; it actually holds the admin status.Whereas*/ 
/*                      the real operational status for the SChannelinterface*/ 
/*                      would depend on the underlying UAP. operationalstatus*/ 
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP interface index                   */
/*                      u4SVId - SVID                                        */
/*                                                                           */
/* Output(s)          : u4IfIndex - Unique interface index if available      */
/*                      zero - otherwise                                     */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
INT4 
VlanEvbSChSetAdminStatus (UINT4 u4SChIfIndex, UINT1 u1Status)
{
    /* This function is called from the VlanHandlePortOperInd */
    tEvbSChIfEntry *pSChIfEntry     = NULL;
    tEvbUapIfEntry *pUapIfEntry     = NULL;
    UINT4           u4UapIfIndex    = 0;
    UINT4           u4SVId          = 0;

    VLAN_TRC_ARG2 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, 
            VLAN_NAME,"VlanEvbSChSetAdminStatus: Hit for "
            "UAP Index %d Status %d\r\n", u4UapIfIndex, u1Status);

    while ((pSChIfEntry = VlanEvbSChIfGetNextEntry (u4UapIfIndex, u4SVId))
                        != NULL)
    {
        u4UapIfIndex = pSChIfEntry->u4UapIfIndex;
        u4SVId = pSChIfEntry->u4SVId;
        if (pSChIfEntry->i4SChIfIndex == (INT4)u4SChIfIndex)
        {
            break;
        }
    }
    pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex);
    if (pUapIfEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
                   VLAN_NAME,"VlanEvbSChSetAdminStatus: "
                   "UAP Entry %d is not present\r\n",
                   u4UapIfIndex);
        return VLAN_FAILURE;
    }
    if (pSChIfEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
                   VLAN_NAME,"VlanEvbSChSetAdminStatus: "
                   "S-Channel entry for SBP port %d is not present\r\n",
                   u4SChIfIndex);
        return VLAN_FAILURE;
    }

    if (u1Status == VLAN_OPER_UP)
    {
       if ((pSChIfEntry->i4NegoStatus != VLAN_EVB_SCH_CONFIRMED) && 
           (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID) &&
           (pSChIfEntry->u4SVId != VLAN_EVB_DEF_SVID))
        { 
            VlanPortCfaUpdateSChannelOperStatus ((UINT4) pSChIfEntry->i4SChIfIndex,
                                                 CFA_IF_DOWN);
            pSChIfEntry->u1OperStatus = CFA_IF_DOWN;
            return VLAN_SUCCESS;
        }
        if (pSChIfEntry->u1OperStatus == CFA_IF_UP)
        {
            if (VlanEvbHwConfigSChInterface (pSChIfEntry,
                        VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD) == VLAN_FAILURE)
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                        VLAN_NAME, "VlanEvbSChSetAdminStatus: "
                        "traffic fwd HW entry for SCID-SVID (%d - %d) failed on "
                        "UAP port %d\r\n", pSChIfEntry->u4SChId, pSChIfEntry->
                        u4SVId, pSChIfEntry->u4UapIfIndex);
#ifdef  SYSLOG_WANTED  
                SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4)gi4VlanSysLogId,
                            "SBP Admin Status is [%s] for SCID-SVID (%d - %d) "
                            "on the UAP port %d",
                            ((pSChIfEntry->u1AdminStatus ==
                              CFA_IF_UP) ? "UP" : "DOWN"),
                            pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                            pSChIfEntry->u4UapIfIndex));
#endif
                return VLAN_FAILURE;
            }
        } 
        /* Setting Admin status UP  in the following cases.
         * 1. On successful HW entry addition 
         * 2. Though oper status is down (dont program hardware), 
         *     set the status alone.*/
        pSChIfEntry->u1AdminStatus = CFA_IF_UP;

        if ((pSChIfEntry->u4SVId == VLAN_EVB_DEF_SVID) &&
            (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID))
        {
            pSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_CONFIRMED;
        }

        if ((pUapIfEntry->pEvbLldpSChannelIfNotify != NULL) && 
            (pSChIfEntry->u4SVId != VLAN_EVB_DEF_SVID))
        {
             pUapIfEntry->pEvbLldpSChannelIfNotify
                         (pSChIfEntry->u4UapIfIndex,
                          pSChIfEntry->u4SVId,VLAN_EVB_SVID_UPDATE);
        }
    }
    else /* VLAN_OPER_DOWN */
    {
        if (pSChIfEntry->u1OperStatus == CFA_IF_UP)
        {
            if (VlanEvbHwConfigSChInterface (pSChIfEntry,
                        VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK) == VLAN_FAILURE)
            {
                VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                        VLAN_NAME, "VlanEvbSChSetAdminStatus: "
                        "Traffic block HW entry for SCID-SVID (%d - %d) failed "
                        "on UAP port %d\r\n", pSChIfEntry->u4SChId,pSChIfEntry->
                        u4SVId, pSChIfEntry->u4UapIfIndex);
#ifdef  SYSLOG_WANTED  
                SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4) gi4VlanSysLogId,
                            "SBP Admin Status is [%s] for SCID-SVID (%d - %d) "
                            " on the UAP port %d",
                            ((pSChIfEntry->u1AdminStatus ==
                              CFA_IF_UP) ? "UP" : "DOWN"),
                            pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                            pSChIfEntry->u4UapIfIndex));
#endif
                return VLAN_FAILURE;
            }
        } 
        /* Setting Admin status DOWN in the following cases.
         * 1. On successful HW entry deletion, 
         * 2. Since the oper status is down, the entry would have removed 
         *    when its oper status is made down, so simply setting the status
         *    down.
         */
        pSChIfEntry->u1AdminStatus = CFA_IF_DOWN;
    }
#ifdef L2RED_WANTED
    /* Updating the S-Channel state information
     *  to STANDBY node  */
    if (VlanEvbRedSendDynamicUpdates (
                (UINT1) VLAN_EVB_RED_SBP_ADD_SYNC_UP,
                (VOID *) pSChIfEntry) == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbSChSetAdminStatus: S-Channel"
                " Negotiation status sync up "
                "failed on the standby\n");
    }
#endif
    if (VlanEvbSendSChannelStatusTrap (pSChIfEntry) == VLAN_FAILURE)
    {
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                VLAN_NAME, "VlanEvbSChSetAdminStatus: "
                "EVB S-Channel status Trap generation failed"
                "for SCID-SVID (%d - %d) on"
                "the UAP port %d\r\n", pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                pSChIfEntry->u4UapIfIndex);
    }
#ifdef  SYSLOG_WANTED  
    SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4)gi4VlanSysLogId,
            "SBP Admin Status is [%s] for SCID-SVID (%d - %d) on the UAP port %d",
            ((pSChIfEntry->u1AdminStatus ==
              CFA_IF_UP) ? "UP" : "DOWN"),
            pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
            pSChIfEntry->u4UapIfIndex));
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanEvbHwConfigSChInterface                          */
/*                                                                           */
/* Description        : This function creates/deletes the S-Channel interface*/ 
/*                      (virtual port) in the hardware                       */
/*                                                                           */
/* Input(s)           : pSChIfEntry  - S-Channel Entry.                      */
/*                      u1OpCode     - VLAN_EVB_HW_SCH_IF_CREATE/            */
/*                                     VLAN_EVB_HW_SCH_IF_DELETE/            */
/*                                     VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK      */
/*                                     VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD    */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4 
VlanEvbHwConfigSChInterface (tEvbSChIfEntry *pSChIfEntry, UINT1 u1OpCode)
{
    tVlanEvbHwConfigInfo VlanEvbHwInfo;
    tVlanPortEntry       *pVlanPortEntry = NULL;
    UINT4                u4ContextId   = 0;
    UINT2                u2LocalPortId = 0;

 
    MEMSET (&VlanEvbHwInfo, 0, sizeof (tVlanEvbHwConfigInfo));

    /* Two reasons why AccFrameType and PVID setting are not required.
     * 1. Really they are not required when going to delete the Virtual
     *    interface from the hardware.
     * 2. S-Channel interface will be deleted in CFA and in L2 modules, 
     *    so VlanVcmGetContextInfoFromIfIndex or VLAN_GET_PORT_ENTRY
     *    would be failed. This blocks in hardware deletion. Hence to 
     *    bypass it..
     */
    if (VLAN_EVB_HW_SCH_IF_DELETE != u1OpCode)
    {
        if (VlanVcmGetContextInfoFromIfIndex (
                    (UINT4) pSChIfEntry->i4SChIfIndex, 
                    &u4ContextId, &u2LocalPortId) == VCM_SUCCESS)
        {
            VLAN_TRC_ARG3 (VLAN_EVB_TRC, CONTROL_PLANE_TRC | DATA_PATH_TRC,
                    VLAN_NAME, "VlanEvbHwConfigSChInterface: "
                    "Getting local port is success for ifindex %d : Localport %d"
                    " context Id : %d\n",
                    pSChIfEntry->i4SChIfIndex, u2LocalPortId, u4ContextId);

            pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

            if (NULL == pVlanPortEntry)
            {
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC,
                        VLAN_NAME, "VlanEvbHwConfigSChInterface: tVlanPortEntry not "
                        " Present for S-Ch Index : %d and LocalPort Id : %d \r\n",
                        pSChIfEntry->i4SChIfIndex, u2LocalPortId);
                return VLAN_FAILURE;
            }
            VlanEvbHwInfo.u1AccFrameType =  pVlanPortEntry->u1AccpFrmTypes;
            VlanEvbHwInfo.u2Pvid         =  pVlanPortEntry->Pvid;
        }
        else
        {
            /* when setting the bridge port type as UAP, the default s-channel 
             * will not be created in VLAN. But still we need to set PVID and 
             * acceptable frame type in the hardware.
             * Hence passing the default values here.
             *
             * FYI: WHEN CHANGING THE PORT TYPE AS UAP, the flow will be:-
             * ------------------------------------------------------------
             * 1. Customer bridge port deletion :
             *     CFA -> L2IWF -> VLAN -> 
             *
             * 2. UAP port creation: 
             *     CFA -> L2IWF -> VLAN -> EVB -> UAP creation -> Def S-Ch creation
             *        -> Def S-Channel Creation in HW
             *
             * 3. UAP port status setting:
             *    CFA -> L2IWF -> VLAN -> EVB -> All S-Channels (as of now only
             *    def S-Channel) are set as FORWARD/BLOCK in HW.
             *
             * 4. Default S-Channel creation:
             *    CFA -> L2IWF -> VLAN -> VLAN port creation -> EVB -> 
             *    UpdateLocalPortInfo (since already it is created in step 2) 
             */
            if (pSChIfEntry->u4SVId == VLAN_EVB_DEF_SVID)
            {
                VlanEvbHwInfo.u1AccFrameType = VLAN_ADMIT_ALL_FRAMES; 
                VlanEvbHwInfo.u2Pvid         = VLAN_EVB_DEF_SVID;
            }
        }
    }

    VlanEvbHwInfo.u4UapIfIndex  =  pSChIfEntry->u4UapIfIndex; 
    VlanEvbHwInfo.u4SChIfIndex  =  (UINT4) pSChIfEntry->i4SChIfIndex;
    VlanEvbHwInfo.u4SChId       =  pSChIfEntry->u4SChId;
    VlanEvbHwInfo.u2SVId        =  (UINT2)pSChIfEntry->u4SVId;
    VlanEvbHwInfo.u1OpCode      =  u1OpCode;
   
    VLAN_TRC_ARG6 (VLAN_EVB_TRC, DATA_PATH_TRC,VLAN_NAME,
                  "VlanEvbHwConfigSChInterface: Hw Programming for "
                  "UAP : %d, SVID: %d, SChIf: %d OPCODE: %d "
                  "ACC-TYPE: %d, PVID: %d\n", VlanEvbHwInfo.u4UapIfIndex,
                  VlanEvbHwInfo.u2SVId,VlanEvbHwInfo.u4SChIfIndex,
                  VlanEvbHwInfo.u1OpCode,
                  VlanEvbHwInfo.u1AccFrameType, VlanEvbHwInfo.u2Pvid);

    if (VlanHwEvbConfigSChIface(&VlanEvbHwInfo) == VLAN_FAILURE)
    {
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                       VLAN_NAME, "VlanEvbHwConfigSChInterface: "
                       "HW programming for SCID-SVID (%d - %d) failed on "
                       "UAP port %d\r\n", pSChIfEntry->u4SChId, pSChIfEntry->
                        u4SVId, pSChIfEntry->u4UapIfIndex);
        return VLAN_FAILURE;
    }

    VLAN_TRC_ARG7 (VLAN_EVB_TRC, DATA_PATH_TRC,VLAN_NAME,
                  "VlanEvbHwConfigSChInterface: Hw Programming Done for "
                  "UAP : %d, SVID: %d, SChIf: %d OPCODE: %d "
                  "ACC-TYPE: %d, PVID: %d VPIndex: %d\n", 
                  VlanEvbHwInfo.u4UapIfIndex, VlanEvbHwInfo.u2SVId,
                  VlanEvbHwInfo.u4SChIfIndex, VlanEvbHwInfo.u1OpCode,
                  VlanEvbHwInfo.u1AccFrameType, VlanEvbHwInfo.u2Pvid,
                  VlanEvbHwInfo.u4VpHwIfIndex);

    if ((u1OpCode == VLAN_EVB_HW_SCH_IF_CREATE) ||
        (u1OpCode == VLAN_EVB_HW_SCH_IF_DELETE))
    {
        pSChIfEntry->u4SChHwIfIndex = (VlanEvbHwInfo.u4VpHwIfIndex);
    }
    if ((u1OpCode == VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK) ||
        (u1OpCode == VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD))
    {
        pSChIfEntry->i4RxFilterEntry = (VlanEvbHwInfo.i4RxFilterEntry);
    }
    /* IMPORTANT: This has to be done only for CREATE and DELETE?? */
    return VLAN_SUCCESS; 
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSetSChFilterStatus                           */
/*                                                                           */
/*    Description         : This function provides the S-Channel IfIndex and */
/*                          the ACL update Filter Status form                */
/*                          the given SVID and UAP IfIndex.                  */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                          u2SVID       - u2SVID                            */
/*                                                                           */
/*    Output(s)           : pu4SChIfIndex- S-Channel IfIndex                 */
/*                          pi4RetValFsMIEvbSChannelFilterStatus             */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetSChFilterStatus (UINT4 u4UapIfIndex, UINT4 u4SVID,
                        INT4 i4RetValFsMIEvbSChannelFilterStatus)
{
 tEvbSChIfEntry      *pSChIfEntry   =   NULL;
    INT4                i4SChIfIndex   = 0;

    pSChIfEntry = VlanEvbSChIfGetEntry (u4UapIfIndex, u4SVID);

    if (pSChIfEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                VLAN_NAME, "VlanGetSChIfIndex: S-Channel index is"
                "not present for UAP port %d and SVID %d\r\n",
                u4UapIfIndex, u4SVID);
        return VLAN_FAILURE;
    }
    i4SChIfIndex = pSChIfEntry->i4SChIfIndex;


    if(i4RetValFsMIEvbSChannelFilterStatus == VLAN_EVB_SCH_L2_FILTER_STATUS_ENABLE)
    {
#ifdef BCMX_WANTED
        if (OSIX_SUCCESS !=  (IssAclVlanSetSChFilterStatus (
                        (UINT4) i4SChIfIndex,
                        i4RetValFsMIEvbSChannelFilterStatus)))
        {
            VLAN_TRC_ARG2 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    VLAN_NAME, "IssAclVlanSetSChFilterStatus :Creating Filter Entry for S-Channel"
                    "failed for the given UAP port %d and SVID %d\r\n",
                    u4UapIfIndex, u4SVID);
            return VLAN_FAILURE;
        }
#else
        UNUSED_PARAM(i4SChIfIndex);
#endif

        pSChIfEntry->i4SChFilterStatus = i4RetValFsMIEvbSChannelFilterStatus;

    }

    return VLAN_SUCCESS;
}


/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetSChFilterStatus                           */
/*                                                                           */
/*    Description         : This function retrieves filter status of
                            the S-Channel IfIndex                            */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                          u2SVID       - u2SVID                            */
/*                                                                           */
/*    Output(s)           : u4UapIfIndex - UAP IfIndex                       */
/*                          u4SVID - SVLAN ID                                */
/*    Returns             : S-Channel FilterStatus                           */
/*                                                                           */
/*****************************************************************************/

INT4 
VlanGetSChFilterStatus (UINT4 u4UapIfIndex, UINT4 u4SVID)
{
    tEvbSChIfEntry      *pSChIfEntry   =   NULL;

    pSChIfEntry = VlanEvbSChIfGetEntry (u4UapIfIndex, u4SVID);
    if (pSChIfEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                VLAN_NAME, "VlanGetSChIfIndex: S-Channel index is"
                "not present for UAP port %d and SVID %d\r\n",
                u4UapIfIndex, u4SVID);
        return VLAN_FAILURE;
    }
    return (pSChIfEntry->i4SChFilterStatus);
}


/*****************************************************************************
 *
 *    Function Name       : VlanEvbGetFreeDynSChIndexForUap
 *
 *    Description         : This function returns the first available
 *                          free interface index from SBP interface pool for 
 *                          the provided UAP interface index in dynamic mode.
 *
 *    Input(s)            : u1UapIfIndex - UAP Interface Index.         
 *
 *    Output(s)           : pu4Index - Pointer to interface index
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Returns            : VLAN_SUCCESS if free index is available
 *                         otherwise VLAN_FAILURE.
 *
 *****************************************************************************/
INT4
VlanEvbGetFreeDynSChIndexForUap (UINT4 u4UapIfIndex, INT4 *pi4Index)
{
    INT4                i4SchIndex       = 0;
    INT4                i4IndexMin       = 0;
    INT4                i4IndexMax       = 0;
    UINT4               u4SVId           = 0;
    INT4                i4SBPIndex       = 0;
    tEvbSChIfEntry      *pSChIfEntry     = NULL;
    UINT1                u1EntryExist    = VLAN_FALSE;

    /*____________________________________________
     *|             |              |              |
     *| UAP1        | UAP2         | UAP3         |
     *|_____________|______________|______________|
     *|             |              |              |
     *|SBP1,SBP2,etc| SBP1,SBP2,etc|SBP1,SBP2,etc |
     *|_____________|______________|______________|
     */

    i4IndexMin = (INT4)((CFA_MIN_EVB_SBP_INDEX + (VLAN_EVB_MAX_SBP_PER_UAP * 
                (u4UapIfIndex - 1))));

    i4IndexMax = i4IndexMin + VLAN_EVB_MAX_SBP_PER_UAP - 1;
    i4SBPIndex = i4IndexMin;
    /* Assigning S-channel Min Index to this Local Variable. */
    /* Flexible ifIndex changes ===
       to support free ifindex being returned within the
       required range */
	for ( ; i4SBPIndex <= i4IndexMax ; i4SBPIndex++ )
	{
        /* Scanning From MININDEX to MAXINDEX for a particular UAP */
        u1EntryExist = VLAN_FALSE;
        /* Initially Entry Exist Flag is Set to False for all INDEX within that UAP*/
		while ((pSChIfEntry = VlanEvbSChIfGetNextEntry (u4UapIfIndex, u4SVId))
				!= NULL)
		{
            /* Next S-channel Entry IN RB tree is fetched */
			if (pSChIfEntry->u4UapIfIndex != u4UapIfIndex)
			{
            /* If S-channel entry from another UAP Index is fetched 
             * then break the loop*/             
				break;
			}
            if (i4SBPIndex ==  pSChIfEntry->i4SChIfIndex)
            {
             /* If SBP Index and S-channel Entry SchIndex are same
              *  then that Value is Already assigned so Set 
              *  EntryExist as VLAN_TRUE and assigning SVID 
              *  as Zero , then only VlanEvbSChIfGetNextEntry 
              *  gets executed for all the available SVID's 
              *  in RB Tree */      
              u1EntryExist = VLAN_TRUE;
              u4SVId = 0;
              break;
            }
            /* Assinging assigning NextEntry pSChIfEntry->u4SVId to SVID */
			u4SVId = pSChIfEntry->u4SVId;
		}/*End of While*/
        if (u1EntryExist == VLAN_FALSE)
        {
          /* If the SBPIndex Value is not assigned to any (SCID-SVID)
           * break from the for loop */ 
          break;
        }
	}/*End of for */
    if (u1EntryExist == VLAN_FALSE)
    {
        /* Assingning SBPINDEX value to i4SchIndex*/
		i4SchIndex = i4SBPIndex ;
    }
    if ((i4SchIndex > i4IndexMin) && (i4SchIndex <= i4IndexMax))
    {
        /* Checking whether the i4SchIndex is less than the 
         * IndexMin and IndexMax of particular UAP*/ 
        *pi4Index = i4SchIndex;
        return (VLAN_SUCCESS);
    }
    else
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                VLAN_NAME, "VlanEvbGetFreeDynSChIndexForUap: S-Channel index is"
                "not available for UAP port %d \r\n",
                u4UapIfIndex);
        *pi4Index = 0;
        return (VLAN_FAILURE);
    }
}

/******************************************************************************/
/* Function Name      : VlanEvbSchIfDelDynamicEntry                           */
/*                                                                            */
/* Description        : This routine deletes a tEvbSChIfEntry from the        */
/*                      gEvbGlobalInfo.EvbSchIfTree and                       */
/*                      gEvbGlobalInfo.EvbSchScidTree.                        */
/*                      Either SVID or SCID must not be zero so that it will  */
/*                      be used to delete from both RBTrees &releases memory. */
/*                      for Dynamic Mode when CDCP is Enable                  */
/*                                                                            */
/* Input(s)           : u4SbpIfIndex - CFA interface index for S-Channel      */
/*                                                                            */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                             */
/******************************************************************************/
INT4
VlanEvbSchIfDelDynamicEntry (UINT4 u4SbpIfIndex)
{
    tEvbSChIfEntry   *pSChIfEntry     = NULL;
    tEvbSChIfEntry   *pSChScidEntry   = NULL;
    tEvbSChIfEntry   *pNextSChIfEntry = NULL;
    tEvbUapIfEntry   *pUapIfEntry     = NULL;
    UINT4             u4UapIfIndex    = 0;
    UINT2             u2SVId          = 0;
    UINT4             u4SChId         = 0;
    
    if (VlanGetSChInfoFromSChIfIndex(u4SbpIfIndex,&u4UapIfIndex,&u2SVId) == VLAN_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                VLAN_NAME, "VlanEvbSchIfDelDynamicEntry: S-Channel index is"
                "not present :%d\r\n", u4SbpIfIndex);
        return VLAN_FAILURE;
    }

    VLAN_TRC_ARG3 (VLAN_EVB_TRC, CONTROL_PLANE_TRC,
            VLAN_NAME, "VlanEvbSchIfDelDynamicEntry: FOR"
            "SBP Index: %d UAP is :%d SVID is :%d\r\n",
            u4SbpIfIndex,u4UapIfIndex,u2SVId);

    pUapIfEntry = VlanEvbUapIfGetEntry (u4UapIfIndex);

    if (pUapIfEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                VLAN_NAME, "VlanEvbSchIfDelDynamicEntry: UAP is"
                "not present for Index: %d SBP Index: %d\r\n",
                u4UapIfIndex, u4SbpIfIndex);
        return VLAN_FAILURE;
    }

    if (((pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
        && (pUapIfEntry->i4UapIfCdcpOperState == VLAN_EVB_UAP_CDCP_NOT_RUNNING))
            || (pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID))
    {
        VLAN_TRC_ARG1 (VLAN_EVB_TRC, CONTROL_PLANE_TRC,
                VLAN_NAME, "VlanEvbSchIfDelDynamicEntry:Mode is "
                "not Dynamic or CDCP is Disable for UAP%d\r\n",u4UapIfIndex);
        return VLAN_SUCCESS;
    }
    pSChIfEntry = VlanEvbSChIfGetEntry (u4UapIfIndex,(UINT4)u2SVId);
    
    if (pSChIfEntry == NULL)
    {
        VLAN_TRC_ARG2 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                "VlanEvbSchIfDelDynamicEntry: Entry for UAP Port %d, SVID %d "
                " not exists\r\n", u4UapIfIndex, u2SVId);
        return VLAN_SUCCESS;
    }
    u4SChId = pSChIfEntry->u4SChId;

    pSChScidEntry = VlanEvbSChScidIfGetEntry (u4UapIfIndex, u4SChId);

    if (pSChScidEntry == NULL)
    {
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                "VlanEvbSchIfDelDynamicEntry: Entry for UAP Port %d, SVID %d "
                ",SCID %d not exists\r\n", u4UapIfIndex, u2SVId,
                u4SChId);
        return VLAN_SUCCESS;
    }
    /* Deleting the underlying hardware S-Channel entry */
    if (VlanEvbSChHwIndication(pSChIfEntry,
                VLAN_EVB_HW_SCH_IF_DELETE) == VLAN_FAILURE)
    {
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                VLAN_NAME, "VlanEvbSchIfDelDynamicEntry: "
                " HW entry Deletion for SCID-SVID (%d - %d) failed on "
                "UAP port %d\r\n", pSChIfEntry->u4SChId, 
                pSChIfEntry->u4SVId, pSChIfEntry->u4UapIfIndex);
        return VLAN_FAILURE;
    }
    pSChIfEntry->u1OperStatus = CFA_IF_DOWN;
    /* Removing from RBTree1 - Keys UAP port + SVID */
    if (RBTreeRemove ((gEvbGlobalInfo.EvbSchIfTree),
                (tRBElem *)(VOID *) pSChIfEntry) == RB_FAILURE)
    {
        VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                "VlanEvbSchIfDelDynamicEntry: RBTree remove for UAP port %d, "
                "SVID %d, SCID %d for EvbSchIfTree failed\r\n", 
                u4UapIfIndex, u2SVId, u4SChId);
        /* Do not return here since have to remove from another RBTree */
    }

    if (pSChScidEntry != NULL)
    {
        /* Removing from RBTree2 - Keys UAP port + SCID */
        if (RBTreeRemove ((gEvbGlobalInfo.EvbSchScidTree),
                    (tRBElem *)(VOID *) pSChIfEntry) == RB_FAILURE)
        {
            VLAN_TRC_ARG3 (VLAN_EVB_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                    "VlanEvbSchIfDelDynamicEntry: RBTree remove for UAP port %d, "
                    "SVID %d, SCID %d for EvbSchScidTree failed\r\n", 
                    u4UapIfIndex, u2SVId, u4SChId);
            /* Need not return here since memory release to be done */
        }
    }
    /* Send the notification to LLDP  regarding a new static s-channel has 
     *   been deleted */
    if ((pUapIfEntry->pEvbLldpSChannelIfNotify != NULL) && 
            (u2SVId != VLAN_EVB_DEF_SVID))
    {
        pUapIfEntry->pEvbLldpSChannelIfNotify
            (u4UapIfIndex,(UINT4)u2SVId,VLAN_EVB_SVID_DELETE);
    }

    /* Checking Whether the Next Entry is present , 
     * if not setting the UAP OPER STATE as NOT_RUNNING
     * or if next entry is present NextSChEntry UAP 
     * Index and u4UapIfIndex not equal then 
     * set cdcp as not-running for that uap */
    pNextSChIfEntry = VlanEvbSChIfGetNextEntry (u4UapIfIndex,VLAN_EVB_DEF_SVID);
    if ((pNextSChIfEntry == NULL) ||
            ((pNextSChIfEntry != NULL ) && 
             (pNextSChIfEntry->u4UapIfIndex != u4UapIfIndex)))
    {
        pUapIfEntry->i4UapIfCdcpOperState = VLAN_EVB_UAP_CDCP_NOT_RUNNING;
    }

    VLAN_EVB_RELEASE_BUF (VLAN_EVB_SCH_ENTRY, (VOID *)(pSChIfEntry));
    pSChIfEntry = NULL;
    UNUSED_PARAM(pSChIfEntry);
    return VLAN_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : VlanEvbSbpHwIndication                               */
/*                                                                           */
/* Description        : This function indicates to Create/Delete S-Channel   */
/*                      entry in Hardware.                                   */ 
/*                                                                           */
/* Input(s)           : pSChIfEntry  - S-Channel Entry.                      */
/*                      u1OpCode     - VLAN_EVB_HW_SCH_IF_CREATE/            */
/*                                     VLAN_EVB_HW_SCH_IF_DELETE             */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanEvbSChHwIndication (tEvbSChIfEntry  *pSChIfEntry,UINT1 u1OpCode)
{
    UINT1                u1Trap         = 0;

    if (u1OpCode == VLAN_EVB_HW_SCH_IF_CREATE)
    {
        u1Trap = VLAN_EVB_SBP_CREATE_FAIL_TRAP;
    }
    else 
    {
        u1Trap = VLAN_EVB_SBP_DELETE_FAIL_TRAP;
    }
    if (VlanEvbHwConfigSChInterface (pSChIfEntry,
                u1OpCode) == VLAN_FAILURE)
    {
        VLAN_TRC_ARG4 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, 
                VLAN_NAME, "VlanEvbSChHwIndication: "
                " HW entry for Opcode:%d for SCID-SVID (%d - %d) failed on "
                "UAP port %d\r\n",u1OpCode, pSChIfEntry->u4SChId, 
                pSChIfEntry->u4SVId, pSChIfEntry->u4UapIfIndex);

        if (VlanEvbSendSChannelTrap (u1Trap, 
                    pSChIfEntry->u4UapIfIndex, pSChIfEntry->u4SVId) 
                == VLAN_FAILURE)
        {
            VLAN_TRC_ARG4 (VLAN_EVB_TRC, ALL_FAILURE_TRC,
                    VLAN_NAME, "VlanEvbSChHwIndication: "
                    "EVB S-Channel for Trap Value:%d fail Trap generation failed"
                    "for SCID-SVID (%d - %d) on"
                    "the UAP port %d\r\n",u1Trap,pSChIfEntry->u4SChId, 
                    pSChIfEntry->u4SVId,
                    pSChIfEntry->u4UapIfIndex);
        }
#ifdef SYSLOG_WANTED
        SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL, (UINT4)gi4VlanSysLogId,
                    "SCID-SVID (%d - %d) for opcode:%d is failed in hardware  "
                    "on the UAP port %d",
                    pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                    u1OpCode,pSChIfEntry->u4UapIfIndex));

#endif
        return VLAN_FAILURE ;
    }
#ifdef SYSLOG_WANTED
    SYS_LOG_MSG (((UINT4)SYSLOG_INFO_LEVEL,(UINT4)gi4VlanSysLogId,
                "SCID-SVID (%d - %d) for Opcode :%d is success in hardware "
                "on the UAP port %d",
                pSChIfEntry->u4SChId, pSChIfEntry->u4SVId,
                u1OpCode,pSChIfEntry->u4UapIfIndex));
#endif
    return VLAN_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : VlanEvbSChUpdateCfaOperStatus                        */
/*                                                                           */
/* Description        : This function is Called only from EVB Enable/Disable */
/*                      alone.Makes all the S-channels same oper status as   */
/*                      Received for Static Mode , Only Default S-channels   */
/*                      Oper State only be moved as Received Oper State      */
/*                      for Hybrid/Dynamic.                                  */
/* Input(s)           : pUapIfEntry  - UAP Interface Entry                   */
/*                      u1OperStatus - operational status                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanEvbSChUpdateCfaOperStatus (tEvbUapIfEntry *pUapIfEntry,
                                      UINT1 u1OperStatus)
{
	tEvbSChIfEntry  *pSChIfEntry  = NULL;
    UINT4            u4UapIfIndex = pUapIfEntry->u4UapIfIndex;
    UINT4            u4SVId       = 0;

    if ((pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_DYNAMIC)
            && (pUapIfEntry->i4UapIfSchCdcpAdminEnable == VLAN_EVB_UAP_CDCP_DISABLE))
    {
        /* For Static Mode , all S-channels oper status are moved to the 
         * Oper State Received */
        while ((pSChIfEntry = VlanEvbSChIfGetNextEntry (u4UapIfIndex, u4SVId))
                != NULL)
        {
            if (pSChIfEntry->u4UapIfIndex != pUapIfEntry->u4UapIfIndex)
            {
                /* Configured all the S-Channel entries on this UAP port.
                 * Take a breath by breaking */
                break;
            }

            VlanPortCfaUpdateSChannelOperStatus ((UINT4) 
                    pSChIfEntry->i4SChIfIndex,
                    u1OperStatus);
            u4UapIfIndex = pSChIfEntry->u4UapIfIndex;
            u4SVId = pSChIfEntry->u4SVId;
        }
    }
    else
    {
        /* Only Default S-channels Oper State only be moved as Received Oper State      
         * for Hybrid/Dynamic. */
        u4SVId = VLAN_EVB_DEF_SVID; 
        pSChIfEntry = VlanEvbSChIfGetEntry (u4UapIfIndex, u4SVId);
		if (pSChIfEntry != NULL)
		{
			VlanPortCfaUpdateSChannelOperStatus ((UINT4) 
                    pSChIfEntry->i4SChIfIndex,
					u1OperStatus);
			if(pUapIfEntry->i4UapIfSChMode == VLAN_EVB_UAP_SCH_MODE_HYBRID)
			{ 
				if	(u1OperStatus == CFA_IF_UP)
				{
					pSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_CONFIRMED;
				}
				else
				{
					pSChIfEntry->i4NegoStatus = VLAN_EVB_SCH_FREE;
				}
			}
		}
    }
    return ;
}
#endif  /* __VLNEVSCH_C__ */
