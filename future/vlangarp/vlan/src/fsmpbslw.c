/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbslw.c,v 1.13 2013/12/05 12:44:53 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "vlaninc.h"
# include  "fssnmp.h"

/* LOW LEVEL Routines for Table : Dot1adMIPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1adMIPortTable
 Input       :  The Indices
                Dot1adMIPortNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adMIPortTable (INT4 i4Dot1adMIPortNum)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1adPortTable (u2LocalPort);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1adMIPortTable
 Input       :  The Indices
                Dot1adMIPortNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adMIPortTable (INT4 *pi4Dot1adMIPortNum)
{

    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4CurrPort = 0;

    i1RetVal =
        nmhGetNextIndexDot1adMIPortTable (i4CurrPort, pi4Dot1adMIPortNum);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1adMIPortTable
 Input       :  The Indices
                Dot1adMIPortNum
                nextDot1adMIPortNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adMIPortTable (INT4 i4Dot1adMIPortNum, INT4 *pi4NextPortNum)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4Dot1adMIPortNum < 0)
    {
        i4Dot1adMIPortNum = 0;
    }

    while (VlanGetNextPortInSystem (i4Dot1adMIPortNum, &u4IfIndex) ==
           VLAN_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE &&
                    VLAN_PB_802_1AD_AH_BRIDGE () != VLAN_FALSE)
                {
                    *pi4NextPortNum = (INT4) u4IfIndex;
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }

                VlanReleaseContext ();
            }
        }

        i4Dot1adMIPortNum = u4IfIndex;
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1adMIPortPcpSelectionRow
 Input       :  The Indices
                Dot1adMIPortNum

                The Object 
                retValDot1adMIPortPcpSelectionRow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPortPcpSelectionRow (INT4 i4Dot1adMIPortNum,
                                   INT4 *pi4PortPcpSelectionRow)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1adPortPcpSelectionRow (u2LocalPort,
                                                pi4PortPcpSelectionRow);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMIPortUseDei
 Input       :  The Indices
                Dot1adMIPortNum

                The Object 
                retValDot1adMIPortUseDei
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPortUseDei (INT4 i4Dot1adMIPortNum, INT4 *pi4RetValPortUseDei)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1adPortUseDei (u2LocalPort, pi4RetValPortUseDei);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMIPortReqDropEncoding
 Input       :  The Indices
                Dot1adMIPortNum

                The Object 
                retValDot1adMIPortReqDropEncoding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPortReqDropEncoding (INT4 i4Dot1adMIPortNum,
                                   INT4 *pi4PortReqDropEncoding)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1adPortReqDropEncoding (u2LocalPort,
                                                pi4PortReqDropEncoding);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMIPortSVlanPriorityType
 Input       :  The Indices
                Dot1adMIPortNum

                The Object
                
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPortSVlanPriorityType (INT4 i4Dot1adMIPortNum,
                                     INT4 *pi4PortSVlanPriorityType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1adPortSVlanPriorityType (u2LocalPort,
                                                  pi4PortSVlanPriorityType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMIPortSVlanPriority
 Input       :  The Indices
                Dot1adMIPortNum

                The Object

 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPortSVlanPriority (INT4 i4Dot1adMIPortNum,
                                 INT4 *pi4PortSVlanPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1adPortSVlanPriority (u2LocalPort,
                                              pi4PortSVlanPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1adMIPortPcpSelectionRow
 Input       :  The Indices
                Dot1adMIPortNum

                The Object 
                setValDot1adMIPortPcpSelectionRow
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPortPcpSelectionRow (INT4 i4Dot1adMIPortNum,
                                   INT4 i4PortPcpSelectionRow)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1adPortPcpSelectionRow (u2LocalPort,
                                                i4PortPcpSelectionRow);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMIPortUseDei
 Input       :  The Indices
                Dot1adMIPortNum

                The Object 
                setValDot1adMIPortUseDei
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPortUseDei (INT4 i4Dot1adMIPortNum, INT4 i4SetValPortUseDei)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1adPortUseDei (u2LocalPort, i4SetValPortUseDei);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMIPortReqDropEncoding
 Input       :  The Indices
                Dot1adMIPortNum

                The Object 
                setValDot1adMIPortReqDropEncoding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPortReqDropEncoding (INT4 i4Dot1adMIPortNum,
                                   INT4 i4PortReqDropEncoding)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1adPortReqDropEncoding (u2LocalPort,
                                                i4PortReqDropEncoding);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMIPortSVlanPriorityType
 Input       :  The Indices
                Dot1adMIPortNum

                The Object
                setValDot1adMIPortSVlanPriorityType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPortSVlanPriorityType (INT4 i4Dot1adMIPortNum,
                                     INT4 i4PortSVlanPriorityType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1adPortSVlanPriorityType (u2LocalPort,
                                                  i4PortSVlanPriorityType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMIPortSVlanPriority
 Input       :  The Indices
                Dot1adMIPortNum

                The Object
                setValDot1adMIPortSVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPortSVlanPriority (INT4 i4Dot1adMIPortNum,
                                 INT4 i4PortSVlanPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1adPortSVlanPriority (u2LocalPort, i4PortSVlanPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPortPcpSelectionRow
 Input       :  The Indices
                Dot1adMIPortNum

                The Object 
                testValDot1adMIPortPcpSelectionRow
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPortPcpSelectionRow (UINT4 *pu4ErrorCode,
                                      INT4 i4Dot1adMIPortNum,
                                      INT4 i4PortPcpSelectionRow)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPortPcpSelectionRow (pu4ErrorCode,
                                                   u2LocalPort,
                                                   i4PortPcpSelectionRow);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPortUseDei
 Input       :  The Indices
                Dot1adMIPortNum

                The Object 
                testValDot1adMIPortUseDei
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPortUseDei (UINT4 *pu4ErrorCode, INT4 i4Dot1adMIPortNum,
                             INT4 i4PortUseDei)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPortUseDei (pu4ErrorCode,
                                          u2LocalPort, i4PortUseDei);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPortReqDropEncoding
 Input       :  The Indices
                Dot1adMIPortNum

                The Object 
                testValDot1adMIPortReqDropEncoding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPortReqDropEncoding (UINT4 *pu4ErrorCode,
                                      INT4 i4Dot1adMIPortNum,
                                      INT4 i4PortReqDropEncoding)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPortReqDropEncoding (pu4ErrorCode,
                                                   u2LocalPort,
                                                   i4PortReqDropEncoding);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPortSVlanPriorityType
 Input       :  The Indices
                Dot1adMIPortNum

                The Object
                testValDot1adMIPortSVlanPriorityType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPortSVlanPriorityType (UINT4 *pu4ErrorCode,
                                        INT4 i4Dot1adMIPortNum,
                                        INT4 i4PortSVlanPriorityType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPortSVlanPriorityType (pu4ErrorCode,
                                                     u2LocalPort,
                                                     i4PortSVlanPriorityType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPortSVlanPriority
 Input       :  The Indices
                Dot1adMIPortNum

                The Object
                testValDot1adMIPortSVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPortSVlanPriority (UINT4 *pu4ErrorCode,
                                    INT4 i4Dot1adMIPortNum,
                                    INT4 i4PortSVlanPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPortSVlanPriority (pu4ErrorCode,
                                                 u2LocalPort,
                                                 i4PortSVlanPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhDepv2Dot1adMIPortTable
 Input       :  The Indices
                Dot1adMIPortNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1adMIPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adMIVidTranslationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1adMIVidTranslationTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIVidTranslationLocalVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adMIVidTranslationTable (INT4 i4Dot1adMIPortNum,
                                                     INT4
                                                     i4VidTranslationLocalVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1adVidTranslationTable (u2LocalPort,
                                                           i4VidTranslationLocalVid);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1adMIVidTranslationTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIVidTranslationLocalVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adMIVidTranslationTable (INT4 *pi4Dot1adMIPortNum,
                                             INT4 *pi4TranslationLocalVid)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    INT4                i4LocalVid = 0;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1adMIVidTranslationTable (u4IfIndex,
                                                        pi4Dot1adMIPortNum,
                                                        i4LocalVid,
                                                        pi4TranslationLocalVid));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1adMIVidTranslationTable
 Input       :  The Indices
                Dot1adMIPortNum
                nextDot1adMIPortNum
                Dot1adMIVidTranslationLocalVid
                nextDot1adMIVidTranslationLocalVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adMIVidTranslationTable (INT4 i4Dot1adMIPortNum,
                                            INT4 *pi4NextPortNum,
                                            INT4 i4TranslationLocalVid,
                                            INT4 *pi4NextVidTranslationLocalVid)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4Dot1adMIPortNum < 0)
    {
        i4Dot1adMIPortNum = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4Dot1adMIPortNum;

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexDot1adVidTranslationTable
                    ((INT4) u2LocalPort, pi4NextPortNum, i4TranslationLocalVid,
                     pi4NextVidTranslationLocalVid) == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextPortNum);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextPortNum = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }
        i4TranslationLocalVid = 0;
        u4IfIndex = u4CurrIfIndex;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1adMIVidTranslationRelayVid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIVidTranslationLocalVid

                The Object 
                retValDot1adMIVidTranslationRelayVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIVidTranslationRelayVid (INT4 i4Dot1adMIPortNum,
                                      INT4 i4VidTranslationLocalVid,
                                      INT4 *pi4RetValVidTranslationRelayVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adVidTranslationRelayVid (u2LocalPort,
                                            i4VidTranslationLocalVid,
                                            pi4RetValVidTranslationRelayVid);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMIVidTranslationRowStatus
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIVidTranslationLocalVid

                The Object 
                retValDot1adMIVidTranslationRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIVidTranslationRowStatus (INT4 i4Dot1adMIPortNum,
                                       INT4 i4VidTranslationLocalVid,
                                       INT4 *pi4RetValVidTranslationRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adVidTranslationRowStatus (u2LocalPort,
                                             i4VidTranslationLocalVid,
                                             pi4RetValVidTranslationRowStatus);
    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1adMIVidTranslationRelayVid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIVidTranslationLocalVid

                The Object 
                setValDot1adMIVidTranslationRelayVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIVidTranslationRelayVid (INT4 i4Dot1adMIPortNum,
                                      INT4 i4VidTranslationLocalVid,
                                      INT4 i4SetValVidTranslationRelayVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adVidTranslationRelayVid (u2LocalPort,
                                            i4VidTranslationLocalVid,
                                            i4SetValVidTranslationRelayVid);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMIVidTranslationRowStatus
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIVidTranslationLocalVid

                The Object 
                setValDot1adMIVidTranslationRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIVidTranslationRowStatus (INT4 i4Dot1adMIPortNum,
                                       INT4 i4VidTranslationLocalVid,
                                       INT4 i4SetValVidTranslationRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adVidTranslationRowStatus (u2LocalPort,
                                             i4VidTranslationLocalVid,
                                             i4SetValVidTranslationRowStatus);
    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIVidTranslationRelayVid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIVidTranslationLocalVid

                The Object 
                testValDot1adMIVidTranslationRelayVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIVidTranslationRelayVid (UINT4 *pu4ErrorCode,
                                         INT4 i4Dot1adMIPortNum,
                                         INT4 i4VidTranslationLocalVid,
                                         INT4 i4TestValVidTranslationRelayVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1adVidTranslationRelayVid (pu4ErrorCode,
                                               (INT4) u2LocalPort,
                                               i4VidTranslationLocalVid,
                                               i4TestValVidTranslationRelayVid);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIVidTranslationRowStatus
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIVidTranslationLocalVid

                The Object 
                testValDot1adMIVidTranslationRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIVidTranslationRowStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4Dot1adMIPortNum,
                                          INT4 i4VidTranslationLocalVid,
                                          INT4 i4TestValVidTranslationRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1adVidTranslationRowStatus (pu4ErrorCode,
                                                (INT4) u2LocalPort,
                                                i4VidTranslationLocalVid,
                                                i4TestValVidTranslationRowStatus);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhDepv2Dot1adMIVidTranslationTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIVidTranslationLocalVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1adMIVidTranslationTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adMICVidRegistrationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1adMICVidRegistrationTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adMICVidRegistrationTable (INT4 i4Dot1adMIPortNum,
                                                       INT4
                                                       i4CVidRegistrationCVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1adCVidRegistrationTable (u2LocalPort,
                                                             i4CVidRegistrationCVid);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1adMICVidRegistrationTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adMICVidRegistrationTable (INT4 *pi4Dot1adMIPortNum,
                                               INT4 *pi4CVidRegistrationCVid)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    INT4                i4CVidRegistrationCVid = 0;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1adMICVidRegistrationTable (u4IfIndex,
                                                          pi4Dot1adMIPortNum,
                                                          i4CVidRegistrationCVid,
                                                          pi4CVidRegistrationCVid));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1adMICVidRegistrationTable
 Input       :  The Indices
                Dot1adMIPortNum
                nextDot1adMIPortNum
                Dot1adMICVidRegistrationCVid
                nextDot1adMICVidRegistrationCVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adMICVidRegistrationTable (INT4 i4Dot1adMIPortNum,
                                              INT4 *pi4NextPortNum,
                                              INT4 i4CVidRegistrationCVid,
                                              INT4 *pi4NextCVidRegistrationCVid)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4Dot1adMIPortNum < 0)
    {
        i4Dot1adMIPortNum = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4Dot1adMIPortNum;

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexDot1adCVidRegistrationTable
                    ((INT4) u2LocalPort, pi4NextPortNum, i4CVidRegistrationCVid,
                     pi4NextCVidRegistrationCVid) == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextPortNum);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextPortNum = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }
        i4CVidRegistrationCVid = 0;
        u4IfIndex = u4CurrIfIndex;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1adMICVidRegistrationSVid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                retValDot1adMICVidRegistrationSVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMICVidRegistrationSVid (INT4 i4Dot1adMIPortNum,
                                    INT4 i4CVidRegistrationCVid,
                                    INT4 *pi4CVidRegistrationSVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adCVidRegistrationSVid (u2LocalPort,
                                          i4CVidRegistrationCVid,
                                          pi4CVidRegistrationSVid);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMICVidRegistrationUntaggedPep
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                retValDot1adMICVidRegistrationUntaggedPep
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMICVidRegistrationUntaggedPep (INT4 i4Dot1adMIPortNum,
                                           INT4 i4CVidRegistrationCVid,
                                           INT4 *pi4CVidRegistrationUntaggedPep)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adCVidRegistrationUntaggedPep (u2LocalPort,
                                                 i4CVidRegistrationCVid,
                                                 pi4CVidRegistrationUntaggedPep);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMICVidRegistrationUntaggedCep
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                retValDot1adMICVidRegistrationUntaggedCep
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMICVidRegistrationUntaggedCep (INT4 i4Dot1adMIPortNum,
                                           INT4 i4CVidRegistrationCVid,
                                           INT4 *pi4CVidRegistrationUntaggedCep)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adCVidRegistrationUntaggedCep (u2LocalPort,
                                                 i4CVidRegistrationCVid,
                                                 pi4CVidRegistrationUntaggedCep);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMICVidRegistrationRowStatus
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                retValDot1adMICVidRegistrationRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMICVidRegistrationRowStatus (INT4 i4Dot1adMIPortNum,
                                         INT4 i4CVidRegistrationCVid,
                                         INT4 *pi4CVidRegistrationRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adCVidRegistrationRowStatus (u2LocalPort,
                                               i4CVidRegistrationCVid,
                                               pi4CVidRegistrationRowStatus);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMICVidRegistrationRelayCVid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                retValDot1adMICVidRegistrationRelayCVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMICVidRegistrationRelayCVid (INT4 i4Dot1adMIPortNum,
                                         INT4 i4CVidRegistrationCVid,
                                         INT4 *pi4CVidRegistrationRelayCVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adCVIdRegistrationRelayCVid (u2LocalPort,
                                               i4CVidRegistrationCVid,
                                               pi4CVidRegistrationRelayCVid);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMICVidRegistrationSVlanPriorityType
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object
                retValDot1adMICVidRegistrationSVlanPriorityType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMICVidRegistrationSVlanPriorityType (INT4 i4Dot1adMIPortNum,
                                                 INT4 i4CVidRegistrationCVid,
                                                 INT4
                                                 *pi4CVidRegistrationSVlanPriorityType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adCVidRegistrationSVlanPriorityType (u2LocalPort,
                                                       i4CVidRegistrationCVid,
                                                       pi4CVidRegistrationSVlanPriorityType);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMICVidRegistrationSVlanPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object
                retValDot1adMICVidRegistrationSVlanPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMICVidRegistrationSVlanPriority (INT4 i4Dot1adMIPortNum,
                                             INT4 i4CVidRegistrationCVid,
                                             INT4
                                             *pi4CVidRegistrationSVlanPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adCVidRegistrationSVlanPriority (u2LocalPort,
                                                   i4CVidRegistrationCVid,
                                                   pi4CVidRegistrationSVlanPriority);
    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1adMICVidRegistrationSVid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                setValDot1adMICVidRegistrationSVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMICVidRegistrationSVid (INT4 i4Dot1adMIPortNum,
                                    INT4 i4CVidRegistrationCVid,
                                    INT4 i4SetValCVidRegistrationSVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adCVidRegistrationSVid (u2LocalPort,
                                          i4CVidRegistrationCVid,
                                          i4SetValCVidRegistrationSVid);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMICVidRegistrationUntaggedPep
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                setValDot1adMICVidRegistrationUntaggedPep
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMICVidRegistrationUntaggedPep (INT4 i4Dot1adMIPortNum,
                                           INT4 i4CVidRegistrationCVid,
                                           INT4 i4CVidRegistrationUntaggedPep)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adCVidRegistrationUntaggedPep (u2LocalPort,
                                                 i4CVidRegistrationCVid,
                                                 i4CVidRegistrationUntaggedPep);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMICVidRegistrationUntaggedCep
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                setValDot1adMICVidRegistrationUntaggedCep
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMICVidRegistrationUntaggedCep (INT4 i4Dot1adMIPortNum,
                                           INT4 i4CVidRegistrationCVid,
                                           INT4 i4CVidRegistrationUntaggedCep)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adCVidRegistrationUntaggedCep (u2LocalPort,
                                                 i4CVidRegistrationCVid,
                                                 i4CVidRegistrationUntaggedCep);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMICVidRegistrationRowStatus
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                setValDot1adMICVidRegistrationRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMICVidRegistrationRowStatus (INT4 i4Dot1adMIPortNum,
                                         INT4 i4CVidRegistrationCVid,
                                         INT4 i4CVidRegistrationRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adCVidRegistrationRowStatus (u2LocalPort,
                                               i4CVidRegistrationCVid,
                                               i4CVidRegistrationRowStatus);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMICVidRegistrationRelayCVid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                setValDot1adMICVidRegistrationRelayCVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMICVidRegistrationRelayCVid (INT4 i4Dot1adMIPortNum,
                                         INT4 i4CVidRegistrationCVid,
                                         INT4 i4SetValCVidRegistrationRelayCVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adCVIdRegistrationRelayCVid (u2LocalPort,
                                               i4CVidRegistrationCVid,
                                               i4SetValCVidRegistrationRelayCVid);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMICVidRegistrationSVlanPriorityType
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object
                setValDot1adMICVidRegistrationSVlanPriorityType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMICVidRegistrationSVlanPriorityType (INT4 i4Dot1adMIPortNum,
                                                 INT4 i4CVidRegistrationCVid,
                                                 INT4
                                                 i4SetValCVidRegistrationSVlanPriorityType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adCVidRegistrationSVlanPriorityType (u2LocalPort,
                                                       i4CVidRegistrationCVid,
                                                       i4SetValCVidRegistrationSVlanPriorityType);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMICVidRegistrationSVlanPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object
                setValDot1adMICVidRegistrationSVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMICVidRegistrationSVlanPriority (INT4 i4Dot1adMIPortNum,
                                             INT4 i4CVidRegistrationCVid,
                                             INT4
                                             i4SetValCVidRegistrationSVlanPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adCVidRegistrationSVlanPriority (u2LocalPort,
                                                   i4CVidRegistrationCVid,
                                                   i4SetValCVidRegistrationSVlanPriority);
    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1adMICVidRegistrationSVid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                testValDot1adMICVidRegistrationSVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMICVidRegistrationSVid (UINT4 *pu4ErrorCode,
                                       INT4 i4Dot1adMIPortNum,
                                       INT4 i4CVidRegistrationCVid,
                                       INT4 i4CVidRegistrationSVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1adCVidRegistrationSVid (pu4ErrorCode,
                                             (INT4) u2LocalPort,
                                             i4CVidRegistrationCVid,
                                             i4CVidRegistrationSVid);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMICVidRegistrationUntaggedPep
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                testValDot1adMICVidRegistrationUntaggedPep
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMICVidRegistrationUntaggedPep (UINT4 *pu4ErrorCode,
                                              INT4 i4Dot1adMIPortNum,
                                              INT4 i4CVidRegistrationCVid,
                                              INT4
                                              i4CVidRegistrationUntaggedPep)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1adCVidRegistrationUntaggedPep (pu4ErrorCode,
                                                    (INT4) u2LocalPort,
                                                    i4CVidRegistrationCVid,
                                                    i4CVidRegistrationUntaggedPep);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMICVidRegistrationUntaggedCep
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                testValDot1adMICVidRegistrationUntaggedCep
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMICVidRegistrationUntaggedCep (UINT4 *pu4ErrorCode,
                                              INT4 i4Dot1adMIPortNum,
                                              INT4 i4CVidRegistrationCVid,
                                              INT4
                                              i4CVidRegistrationUntaggedCep)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1adCVidRegistrationUntaggedCep (pu4ErrorCode,
                                                    (INT4) u2LocalPort,
                                                    i4CVidRegistrationCVid,
                                                    i4CVidRegistrationUntaggedCep);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMICVidRegistrationRowStatus
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                testValDot1adMICVidRegistrationRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMICVidRegistrationRowStatus (UINT4 *pu4ErrorCode,
                                            INT4 i4Dot1adMIPortNum,
                                            INT4 i4CVidRegistrationCVid,
                                            INT4 i4CVidRegistrationRowStatus)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1adCVidRegistrationRowStatus (pu4ErrorCode,
                                                  (INT4) u2LocalPort,
                                                  i4CVidRegistrationCVid,
                                                  i4CVidRegistrationRowStatus);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMICVidRegistrationRelayCVid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object 
                testValDot1adMICVidRegistrationRelayCVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMICVidRegistrationRelayCVid (UINT4 *pu4ErrorCode,
                                            INT4 i4Dot1adMIPortNum,
                                            INT4 i4CVidRegistrationCVid,
                                            INT4 i4CVidRegistrationRelayCVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1adCVIdRegistrationRelayCVid (pu4ErrorCode,
                                                  (INT4) u2LocalPort,
                                                  i4CVidRegistrationCVid,
                                                  i4CVidRegistrationRelayCVid);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMICVidRegistrationSVlanPriorityType
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object
                testValDot1adMICVidRegistrationSVlanPriorityType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMICVidRegistrationSVlanPriorityType (UINT4 *pu4ErrorCode,
                                                    INT4 i4Dot1adMIPortNum,
                                                    INT4 i4CVidRegistrationCVid,
                                                    INT4
                                                    i4CVidRegistrationSVlanPriorityType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1adCVidRegistrationSVlanPriorityType (pu4ErrorCode,
                                                          (INT4) u2LocalPort,
                                                          i4CVidRegistrationCVid,
                                                          i4CVidRegistrationSVlanPriorityType);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMICVidRegistrationSVlanPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid

                The Object
                testValDot1adMICVidRegistrationSVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMICVidRegistrationSVlanPriority (UINT4 *pu4ErrorCode,
                                                INT4 i4Dot1adMIPortNum,
                                                INT4 i4CVidRegistrationCVid,
                                                INT4
                                                i4CVidRegistrationSVlanPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1adCVidRegistrationSVlanPriority (pu4ErrorCode,
                                                      (INT4) u2LocalPort,
                                                      i4CVidRegistrationCVid,
                                                      i4CVidRegistrationSVlanPriority);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhDepv2Dot1adMICVidRegistrationTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationCVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1adMICVidRegistrationTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adMIPepTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1adMIPepTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adMIPepTable (INT4 i4Dot1adMIPortNum,
                                          INT4 i4CVidRegistrationSVid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1adPepTable (u2LocalPort,
                                                i4CVidRegistrationSVid);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1adMIPepTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adMIPepTable (INT4 *pi4Dot1adMIPortNum,
                                  INT4 *pi4CVidRegistrationSVid)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    INT4                i4CVidRegistrationSVid = 0;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1adMIPepTable (u4IfIndex,
                                             pi4Dot1adMIPortNum,
                                             i4CVidRegistrationSVid,
                                             pi4CVidRegistrationSVid));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1adMIPepTable
 Input       :  The Indices
                Dot1adMIPortNum
                nextDot1adMIPortNum
                Dot1adMICVidRegistrationSVid
                nextDot1adMICVidRegistrationSVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adMIPepTable (INT4 i4Dot1adMIPortNum, INT4 *pi4NextPortNum,
                                 INT4 i4CVidRegistrationSVid,
                                 INT4 *pi4NextCVidRegistrationSVid)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4Dot1adMIPortNum < 0)
    {
        i4Dot1adMIPortNum = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4Dot1adMIPortNum;

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexDot1adPepTable ((INT4) u2LocalPort,
                                                   pi4NextPortNum,
                                                   i4CVidRegistrationSVid,
                                                   pi4NextCVidRegistrationSVid)
                    == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextPortNum);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextPortNum = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }
        i4CVidRegistrationSVid = 0;
        u4IfIndex = u4CurrIfIndex;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1adMIPepPvid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                retValDot1adMIPepPvid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPepPvid (INT4 i4Dot1adMIPortNum, INT4 i4CVidRegistrationSVid,
                       INT4 *pi4RetValPepPvid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1adPepPvid (u2LocalPort,
                                    i4CVidRegistrationSVid, pi4RetValPepPvid);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMIPepDefaultUserPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                retValDot1adMIPepDefaultUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPepDefaultUserPriority (INT4 i4Dot1adMIPortNum,
                                      INT4 i4CVidRegistrationSVid,
                                      INT4 *pi4PepDefaultUserPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adPepDefaultUserPriority (u2LocalPort,
                                            i4CVidRegistrationSVid,
                                            pi4PepDefaultUserPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMIPepAccptableFrameTypes
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                retValDot1adMIPepAccptableFrameTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPepAccptableFrameTypes (INT4 i4Dot1adMIPortNum,
                                      INT4 i4CVidRegistrationSVid,
                                      INT4 *pi4PepAccptableFrameTypes)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adPepAccptableFrameTypes (u2LocalPort,
                                            i4CVidRegistrationSVid,
                                            pi4PepAccptableFrameTypes);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMIPepIngressFiltering
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                retValDot1adMIPepIngressFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPepIngressFiltering (INT4 i4Dot1adMIPortNum,
                                   INT4 i4CVidRegistrationSVid,
                                   INT4 *pi4PepIngressFiltering)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1adPepIngressFiltering (u2LocalPort,
                                                i4CVidRegistrationSVid,
                                                pi4PepIngressFiltering);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1adMIPepPvid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                setValDot1adMIPepPvid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPepPvid (INT4 i4Dot1adMIPortNum, INT4 i4CVidRegistrationSVid,
                       INT4 i4SetValPepPvid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1adPepPvid (u2LocalPort,
                                    i4CVidRegistrationSVid, i4SetValPepPvid);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMIPepDefaultUserPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                setValDot1adMIPepDefaultUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPepDefaultUserPriority (INT4 i4Dot1adMIPortNum,
                                      INT4 i4CVidRegistrationSVid,
                                      INT4 i4PepDefaultUserPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1adPepDefaultUserPriority (u2LocalPort,
                                                   i4CVidRegistrationSVid,
                                                   i4PepDefaultUserPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMIPepAccptableFrameTypes
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                setValDot1adMIPepAccptableFrameTypes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPepAccptableFrameTypes (INT4 i4Dot1adMIPortNum,
                                      INT4 i4CVidRegistrationSVid,
                                      INT4 i4PepAccptableFrameTypes)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1adPepAccptableFrameTypes (u2LocalPort,
                                                   i4CVidRegistrationSVid,
                                                   i4PepAccptableFrameTypes);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMIPepIngressFiltering
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                setValDot1adMIPepIngressFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPepIngressFiltering (INT4 i4Dot1adMIPortNum,
                                   INT4 i4CVidRegistrationSVid,
                                   INT4 i4PepIngressFiltering)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1adPepIngressFiltering (u2LocalPort,
                                                i4CVidRegistrationSVid,
                                                i4PepIngressFiltering);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPepPvid
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                testValDot1adMIPepPvid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPepPvid (UINT4 *pu4ErrorCode, INT4 i4Dot1adMIPortNum,
                          INT4 i4CVidRegistrationSVid, INT4 i4TestValPepPvid)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPepPvid (pu4ErrorCode,
                                       (INT4) u2LocalPort,
                                       i4CVidRegistrationSVid,
                                       i4TestValPepPvid);
    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPepDefaultUserPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                testValDot1adMIPepDefaultUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPepDefaultUserPriority (UINT4 *pu4ErrorCode,
                                         INT4 i4Dot1adMIPortNum,
                                         INT4 i4CVidRegistrationSVid,
                                         INT4 i4PepDefaultUserPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPepDefaultUserPriority (pu4ErrorCode,
                                                      (INT4) u2LocalPort,
                                                      i4CVidRegistrationSVid,
                                                      i4PepDefaultUserPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPepAccptableFrameTypes
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                testValDot1adMIPepAccptableFrameTypes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPepAccptableFrameTypes (UINT4 *pu4ErrorCode,
                                         INT4 i4Dot1adMIPortNum,
                                         INT4 i4CVidRegistrationSVid,
                                         INT4 i4PepAccptableFrameTypes)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPepAccptableFrameTypes (pu4ErrorCode,
                                                      (INT4) u2LocalPort,
                                                      i4CVidRegistrationSVid,
                                                      i4PepAccptableFrameTypes);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPepIngressFiltering
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid

                The Object 
                testValDot1adMIPepIngressFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPepIngressFiltering (UINT4 *pu4ErrorCode,
                                      INT4 i4Dot1adMIPortNum,
                                      INT4 i4CVidRegistrationSVid,
                                      INT4 i4PepIngressFiltering)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPepIngressFiltering (pu4ErrorCode,
                                                   (INT4) u2LocalPort,
                                                   i4CVidRegistrationSVid,
                                                   i4PepIngressFiltering);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhDepv2Dot1adMIPepTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1adMIPepTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adMIServicePriorityRegenerationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1adMIServicePriorityRegenerationTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
                Dot1adMIServicePriorityRegenReceivedPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceDot1adMIServicePriorityRegenerationTable
    (INT4 i4Dot1adMIPortNum, INT4 i4CVidRegistrationSVid,
     INT4 i4RegenReceivedPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1adServicePriorityRegenerationTable
        (u2LocalPort, i4CVidRegistrationSVid, i4RegenReceivedPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1adMIServicePriorityRegenerationTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
                Dot1adMIServicePriorityRegenReceivedPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexDot1adMIServicePriorityRegenerationTable
    (INT4 *pi4Dot1adMIPortNum, INT4 *piCVidRegistrationSVid,
     INT4 *pi4RegenReceivedPriority)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    INT4                i4CVidRegistrationSVid = 0;
    INT4                i4RegenReceivedPriority = 0;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1adMIServicePriorityRegenerationTable
            (u4IfIndex, pi4Dot1adMIPortNum, i4CVidRegistrationSVid,
             piCVidRegistrationSVid, i4RegenReceivedPriority,
             pi4RegenReceivedPriority));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1adMIServicePriorityRegenerationTable
 Input       :  The Indices
                Dot1adMIPortNum
                nextDot1adMIPortNum
                Dot1adMICVidRegistrationSVid
                nextDot1adMICVidRegistrationSVid
                Dot1adMIServicePriorityRegenReceivedPriority
                nextDot1adMIServicePriorityRegenReceivedPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexDot1adMIServicePriorityRegenerationTable
    (INT4 i4Dot1adMIPortNum, INT4 *pi4NextPortNum,
     INT4 i4CVidRegistrationSVid, INT4 *pi4NextCVidRegistrationSVid,
     INT4 i4RegenReceivedPriority, INT4 *pi4NextRegenReceivedPriority)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4Dot1adMIPortNum < 0)
    {
        i4Dot1adMIPortNum = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4Dot1adMIPortNum;

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexDot1adServicePriorityRegenerationTable
                    ((INT4) u2LocalPort, pi4NextPortNum, i4CVidRegistrationSVid,
                     pi4NextCVidRegistrationSVid, i4RegenReceivedPriority,
                     pi4NextRegenReceivedPriority) == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextPortNum);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextPortNum = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }
        i4CVidRegistrationSVid = 0;
        i4RegenReceivedPriority = 0;
        u4IfIndex = u4CurrIfIndex;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1adMIServicePriorityRegenRegeneratedPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
                Dot1adMIServicePriorityRegenReceivedPriority

                The Object 
                retValDot1adMIServicePriorityRegenRegeneratedPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetDot1adMIServicePriorityRegenRegeneratedPriority
    (INT4 i4Dot1adMIPortNum, INT4 i4CVidRegistrationSVid,
     INT4 i4RegenReceivedPriority, INT4 *pi4RegenRegeneratedPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adServicePriorityRegenRegeneratedPriority
        (u2LocalPort, i4CVidRegistrationSVid,
         i4RegenReceivedPriority, pi4RegenRegeneratedPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1adMIServicePriorityRegenRegeneratedPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
                Dot1adMIServicePriorityRegenReceivedPriority

                The Object 
                setValDot1adMIServicePriorityRegenRegeneratedPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIServicePriorityRegenRegeneratedPriority (INT4 i4Dot1adMIPortNum,
                                                       INT4
                                                       i4CVidRegistrationSVid,
                                                       INT4
                                                       i4RegenReceivedPriority,
                                                       INT4
                                                       i4RegenRegeneratedPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adServicePriorityRegenRegeneratedPriority
        (u2LocalPort, i4CVidRegistrationSVid,
         i4RegenReceivedPriority, i4RegenRegeneratedPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIServicePriorityRegenRegeneratedPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
                Dot1adMIServicePriorityRegenReceivedPriority

                The Object 
                testValDot1adMIServicePriorityRegenRegeneratedPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIServicePriorityRegenRegeneratedPriority (UINT4 *pu4ErrorCode,
                                                          INT4
                                                          i4Dot1adMIPortNum,
                                                          INT4
                                                          i4CVidRegistrationSVid,
                                                          INT4
                                                          i4RegenReceivedPriority,
                                                          INT4
                                                          i4RegenRegeneratedPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2Dot1adServicePriorityRegenRegeneratedPriority
        (pu4ErrorCode, (INT4) u2LocalPort, i4CVidRegistrationSVid,
         i4RegenReceivedPriority, i4RegenRegeneratedPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhDepv2Dot1adMIServicePriorityRegenerationTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMICVidRegistrationSVid
                Dot1adMIServicePriorityRegenReceivedPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1adMIServicePriorityRegenerationTable (UINT4 *pu4ErrorCode,
                                                  tSnmpIndexList *
                                                  pSnmpIndexList,
                                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adMIPcpDecodingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1adMIPcpDecodingTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpDecodingPcpSelRow
                Dot1adMIPcpDecodingPcpValue
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adMIPcpDecodingTable (INT4 i4Dot1adMIPortNum,
                                                  INT4 i4PcpDecodingPcpSelRow,
                                                  INT4 i4PcpDecodingPcpValue)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1adPcpDecodingTable (u2LocalPort,
                                                        i4PcpDecodingPcpSelRow,
                                                        i4PcpDecodingPcpValue);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1adMIPcpDecodingTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpDecodingPcpSelRow
                Dot1adMIPcpDecodingPcpValue
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adMIPcpDecodingTable (INT4 *pi4Dot1adMIPortNum,
                                          INT4 *pi4PcpDecodingPcpSelRow,
                                          INT4 *pi4PcpDecodingPcpValue)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    INT4                i4PcpDecodingPcpSelRow = 0;
    INT4                i4PcpDecodingPcpValue = 0;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1adMIPcpDecodingTable (u4IfIndex,
                                                     pi4Dot1adMIPortNum,
                                                     i4PcpDecodingPcpSelRow,
                                                     pi4PcpDecodingPcpSelRow,
                                                     i4PcpDecodingPcpValue,
                                                     pi4PcpDecodingPcpValue));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1adMIPcpDecodingTable
 Input       :  The Indices
                Dot1adMIPortNum
                nextDot1adMIPortNum
                Dot1adMIPcpDecodingPcpSelRow
                nextDot1adMIPcpDecodingPcpSelRow
                Dot1adMIPcpDecodingPcpValue
                nextDot1adMIPcpDecodingPcpValue
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adMIPcpDecodingTable (INT4 i4Dot1adMIPortNum,
                                         INT4 *pi4NextPortNum,
                                         INT4 i4PcpDecodingPcpSelRow,
                                         INT4 *pi4NextPcpDecodingPcpSelRow,
                                         INT4 i4PcpDecodingPcpValue,
                                         INT4 *pi4NextPcpDecodingPcpValue)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4Dot1adMIPortNum < 0)
    {
        i4Dot1adMIPortNum = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4Dot1adMIPortNum;

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexDot1adPcpDecodingTable ((INT4) u2LocalPort,
                                                           pi4NextPortNum,
                                                           i4PcpDecodingPcpSelRow,
                                                           pi4NextPcpDecodingPcpSelRow,
                                                           i4PcpDecodingPcpValue,
                                                           pi4NextPcpDecodingPcpValue)
                    == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextPortNum);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextPortNum = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }
        i4PcpDecodingPcpSelRow = 0;
        i4PcpDecodingPcpValue = 0;
        u4IfIndex = u4CurrIfIndex;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1adMIPcpDecodingPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpDecodingPcpSelRow
                Dot1adMIPcpDecodingPcpValue

                The Object 
                retValDot1adMIPcpDecodingPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPcpDecodingPriority (INT4 i4Dot1adMIPortNum,
                                   INT4 i4PcpDecodingPcpSelRow,
                                   INT4 i4PcpDecodingPcpValue,
                                   INT4 *pi4PcpDecodingPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1adPcpDecodingPriority (u2LocalPort,
                                                i4PcpDecodingPcpSelRow,
                                                i4PcpDecodingPcpValue,
                                                pi4PcpDecodingPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetDot1adMIPcpDecodingDropEligible
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpDecodingPcpSelRow
                Dot1adMIPcpDecodingPcpValue

                The Object 
                retValDot1adMIPcpDecodingDropEligible
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPcpDecodingDropEligible (INT4 i4Dot1adMIPortNum,
                                       INT4 i4PcpDecodingPcpSelRow,
                                       INT4 i4PcpDecodingPcpValue,
                                       INT4 *pi4PcpDecodingDropEligible)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adPcpDecodingDropEligible (u2LocalPort,
                                             i4PcpDecodingPcpSelRow,
                                             i4PcpDecodingPcpValue,
                                             pi4PcpDecodingDropEligible);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1adMIPcpDecodingPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpDecodingPcpSelRow
                Dot1adMIPcpDecodingPcpValue

                The Object 
                setValDot1adMIPcpDecodingPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPcpDecodingPriority (INT4 i4Dot1adMIPortNum,
                                   INT4 i4PcpDecodingPcpSelRow,
                                   INT4 i4PcpDecodingPcpValue,
                                   INT4 i4PcpDecodingPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhSetDot1adPcpDecodingPriority (u2LocalPort,
                                                i4PcpDecodingPcpSelRow,
                                                i4PcpDecodingPcpValue,
                                                i4PcpDecodingPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetDot1adMIPcpDecodingDropEligible
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpDecodingPcpSelRow
                Dot1adMIPcpDecodingPcpValue

                The Object 
                setValDot1adMIPcpDecodingDropEligible
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPcpDecodingDropEligible (INT4 i4Dot1adMIPortNum,
                                       INT4 i4PcpDecodingPcpSelRow,
                                       INT4 i4PcpDecodingPcpValue,
                                       INT4 i4PcpDecodingDropEligible)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adPcpDecodingDropEligible (u2LocalPort,
                                             i4PcpDecodingPcpSelRow,
                                             i4PcpDecodingPcpValue,
                                             i4PcpDecodingDropEligible);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPcpDecodingPriority
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpDecodingPcpSelRow
                Dot1adMIPcpDecodingPcpValue

                The Object 
                testValDot1adMIPcpDecodingPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPcpDecodingPriority (UINT4 *pu4ErrorCode,
                                      INT4 i4Dot1adMIPortNum,
                                      INT4 i4PcpDecodingPcpSelRow,
                                      INT4 i4PcpDecodingPcpValue,
                                      INT4 i4PcpDecodingPriority)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPcpDecodingPriority (pu4ErrorCode,
                                                   (INT4) u2LocalPort,
                                                   i4PcpDecodingPcpSelRow,
                                                   i4PcpDecodingPcpValue,
                                                   i4PcpDecodingPriority);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPcpDecodingDropEligible
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpDecodingPcpSelRow
                Dot1adMIPcpDecodingPcpValue

                The Object 
                testValDot1adMIPcpDecodingDropEligible
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPcpDecodingDropEligible (UINT4 *pu4ErrorCode,
                                          INT4 i4Dot1adMIPortNum,
                                          INT4 i4PcpDecodingPcpSelRow,
                                          INT4 i4PcpDecodingPcpValue,
                                          INT4 i4PcpDecodingDropEligible)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPcpDecodingDropEligible (pu4ErrorCode,
                                                       (INT4) u2LocalPort,
                                                       i4PcpDecodingPcpSelRow,
                                                       i4PcpDecodingPcpValue,
                                                       i4PcpDecodingDropEligible);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhDepv2Dot1adMIPcpDecodingTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpDecodingPcpSelRow
                Dot1adMIPcpDecodingPcpValue
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1adMIPcpDecodingTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1adMIPcpEncodingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1adMIPcpEncodingTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpEncodingPcpSelRow
                Dot1adMIPcpEncodingPriority
                Dot1adMIPcpEncodingDropEligible
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1adMIPcpEncodingTable (INT4 i4Dot1adMIPortNum,
                                                  INT4 i4PcpEncodingPcpSelRow,
                                                  INT4 i4PcpEncodingPriority,
                                                  INT4
                                                  i4PcpEncodingDropEligible)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1adPcpEncodingTable (u2LocalPort,
                                                        i4PcpEncodingPcpSelRow,
                                                        i4PcpEncodingPriority,
                                                        i4PcpEncodingDropEligible);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1adMIPcpEncodingTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpEncodingPcpSelRow
                Dot1adMIPcpEncodingPriority
                Dot1adMIPcpEncodingDropEligible
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDot1adMIPcpEncodingTable (INT4 *pi4Dot1adMIPortNum,
                                          INT4 *pi4PcpEncodingPcpSelRow,
                                          INT4 *pi4PcpEncodingPriority,
                                          INT4 *pi4PcpEncodingDropEligible)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    INT4                i4PcpEncodingPcpSelRow = -1;
    INT4                i4PcpEncodingPriority = -1;
    INT4                i4PcpEncodingDropEligible = -1;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1adMIPcpEncodingTable (u4IfIndex,
                                                     pi4Dot1adMIPortNum,
                                                     i4PcpEncodingPcpSelRow,
                                                     pi4PcpEncodingPcpSelRow,
                                                     i4PcpEncodingPriority,
                                                     pi4PcpEncodingPriority,
                                                     i4PcpEncodingDropEligible,
                                                     pi4PcpEncodingDropEligible));

}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1adMIPcpEncodingTable
 Input       :  The Indices
                Dot1adMIPortNum
                nextDot1adMIPortNum
                Dot1adMIPcpEncodingPcpSelRow
                nextDot1adMIPcpEncodingPcpSelRow
                Dot1adMIPcpEncodingPriority
                nextDot1adMIPcpEncodingPriority
                Dot1adMIPcpEncodingDropEligible
                nextDot1adMIPcpEncodingDropEligible
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1adMIPcpEncodingTable (INT4 i4Dot1adMIPortNum,
                                         INT4 *pi4NextPortNum,
                                         INT4 i4PcpEncodingPcpSelRow,
                                         INT4 *pi4NextPcpEncodingPcpSelRow,
                                         INT4 i4PcpEncodingPriority,
                                         INT4 *pi4NextPcpEncodingPriority,
                                         INT4 i4PcpEncodingDropEligible,
                                         INT4 *pi4NextPcpEncodingDropEligible)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4CurrIfIndex = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (i4Dot1adMIPortNum < 0)
    {
        i4Dot1adMIPortNum = 0;
    }

    /* u4CurrIfIndex is the Current Port Index */

    u4CurrIfIndex = (UINT4) i4Dot1adMIPortNum;

    do
    {
        if (VlanGetContextInfoFromIfIndex (u4CurrIfIndex,
                                           &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (nmhGetNextIndexDot1adPcpEncodingTable ((INT4) u2LocalPort,
                                                           pi4NextPortNum,
                                                           i4PcpEncodingPcpSelRow,
                                                           pi4NextPcpEncodingPcpSelRow,
                                                           i4PcpEncodingPriority,
                                                           pi4NextPcpEncodingPriority,
                                                           i4PcpEncodingDropEligible,
                                                           pi4NextPcpEncodingDropEligible)
                    == SNMP_SUCCESS)
                {
                    pVlanPortEntry = VLAN_GET_PORT_ENTRY (*pi4NextPortNum);

                    /* Successful return here if the entry in table has
                     * port index same as the Current port index */

                    if (pVlanPortEntry->u4IfIndex == u4CurrIfIndex)
                    {
                        *pi4NextPortNum = (INT4) pVlanPortEntry->u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }
                VlanReleaseContext ();
            }
        }

        i4PcpEncodingPcpSelRow = -1;
        i4PcpEncodingPriority = -1;
        i4PcpEncodingDropEligible = -1;

        u4IfIndex = u4CurrIfIndex;

    }
    while (VlanGetNextPortInSystem (u4IfIndex, &u4CurrIfIndex) == VLAN_SUCCESS);

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1adMIPcpEncodingPcpValue
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpEncodingPcpSelRow
                Dot1adMIPcpEncodingPriority
                Dot1adMIPcpEncodingDropEligible

                The Object 
                retValDot1adMIPcpEncodingPcpValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1adMIPcpEncodingPcpValue (INT4 i4Dot1adMIPortNum,
                                   INT4 i4PcpEncodingPcpSelRow,
                                   INT4 i4PcpEncodingPriority,
                                   INT4 i4PcpEncodingDropEligible,
                                   INT4 *pi4PcpEncodingPcpValue)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetDot1adPcpEncodingPcpValue (u2LocalPort,
                                         i4PcpEncodingPcpSelRow,
                                         i4PcpEncodingPriority,
                                         i4PcpEncodingDropEligible,
                                         pi4PcpEncodingPcpValue);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1adMIPcpEncodingPcpValue
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpEncodingPcpSelRow
                Dot1adMIPcpEncodingPriority
                Dot1adMIPcpEncodingDropEligible

                The Object 
                setValDot1adMIPcpEncodingPcpValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1adMIPcpEncodingPcpValue (INT4 i4Dot1adMIPortNum,
                                   INT4 i4PcpEncodingPcpSelRow,
                                   INT4 i4PcpEncodingPriority,
                                   INT4 i4PcpEncodingDropEligible,
                                   INT4 i4PcpEncodingPcpValue)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum,
                                       &u4ContextId, &u2LocalPort)
        == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhSetDot1adPcpEncodingPcpValue (u2LocalPort,
                                         i4PcpEncodingPcpSelRow,
                                         i4PcpEncodingPriority,
                                         i4PcpEncodingDropEligible,
                                         i4PcpEncodingPcpValue);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1adMIPcpEncodingPcpValue
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpEncodingPcpSelRow
                Dot1adMIPcpEncodingPriority
                Dot1adMIPcpEncodingDropEligible

                The Object 
                testValDot1adMIPcpEncodingPcpValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1adMIPcpEncodingPcpValue (UINT4 *pu4ErrorCode,
                                      INT4 i4Dot1adMIPortNum,
                                      INT4 i4PcpEncodingPcpSelRow,
                                      INT4 i4PcpEncodingPriority,
                                      INT4 i4PcpEncodingDropEligible,
                                      INT4 i4PcpEncodingPcpValue)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4Dot1adMIPortNum < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4Dot1adMIPortNum, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1adPcpEncodingPcpValue (pu4ErrorCode,
                                                   (INT4) u2LocalPort,
                                                   i4PcpEncodingPcpSelRow,
                                                   i4PcpEncodingPriority,
                                                   i4PcpEncodingDropEligible,
                                                   i4PcpEncodingPcpValue);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhDepv2Dot1adMIPcpEncodingTable
 Input       :  The Indices
                Dot1adMIPortNum
                Dot1adMIPcpEncodingPcpSelRow
                Dot1adMIPcpEncodingPriority
                Dot1adMIPcpEncodingDropEligible
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1adMIPcpEncodingTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
