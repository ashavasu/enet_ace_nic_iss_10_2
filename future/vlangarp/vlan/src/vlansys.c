/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlansys.c,v 1.400 2017/09/26 13:14:58 siva Exp $
 *
 * Description: This file contains VLAN related routines.
 *
 *******************************************************************/

#include "vlaninc.h"
#ifndef PB_WANTED
#include "vlnpbglb.h"
#endif
#include "vlanglob.h"
#include "pnacnp.h"
#ifdef SNMP_3_WANTED
#include "fsvlanmib.h"
#endif
#include "fsmsvlcli.h"
#ifdef EVB_WANTED
#include "vlnevglb.h"
#endif
tVlanId             gVlanDefVlanId;
#ifdef SNMP_3_WANTED
static INT1         ai1TempBuffer[257];
#endif
extern UINT1       *EoidGetEnterpriseOid (void);
extern UINT4        FsMIDot1qFutureVlanPortIngressEtherType[12];
extern UINT4        FsMIDot1qFutureVlanPortEgressEtherType[12];
extern UINT4        FsMIDot1qFutureVlanPortType[12];
#ifdef MPLS_WANTED
#ifdef HVPLS_WANTED
extern INT4         L2VpnIsPwVpws (UINT4 u4PwVcIndex);
#endif
#endif
UINT4               gau4PortArrayEntry[VLAN_PORT_LIST_SIZE];
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPostCfgMessage                               */
/*                                                                           */
/*    Description         : Posts the message to Vlan Config Q.              */
/*                                                                           */
/*    Input(s)            : u1MsgType -  Meesage Type                        */
/*                          u2Port    -  Port Index.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPostCfgMessage (UINT2 u2MsgType, UINT4 u4Port)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPort;
    tVlanQMsg          *pVlanQMsg = NULL;

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        return VLAN_FAILURE;
    }
    if (VlanVcmGetContextInfoFromIfIndex (u4Port, &u4ContextId, &u2LocalPort) ==
        VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }
    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "Config Message Type %u \r\n", u2MsgType);

        return VLAN_FAILURE;
    }

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME, " Message Allocation failed for Vlan "
                         "Config Message Type %u \r\n", u2MsgType);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = u2MsgType;
    pVlanQMsg->u4ContextId = u4ContextId;
    pVlanQMsg->u4Port = u4Port;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME,
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", u2MsgType);

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPostPortCreateMessage                        */
/*                                                                           */
/*    Description         : Posts the message to Vlan Config Q.              */
/*                                                                           */
/*    Input(s)            : u4ContextId -  Context Identifier                */
/*                          u4IfIndex   -  Interface index (physical)        */
/*                          u2Port    -  Port Index (local port number).     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPostPortCreateMessage (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2Port,
                           UINT2 u2MsgType)
{
    tVlanQMsg          *pVlanQMsg = NULL;

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        return VLAN_FAILURE;
    }
    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "port create message  \r\n");

        return VLAN_FAILURE;
    }

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME, " Message Allocation failed for Vlan "
                         "port create message  \r\n");
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = u2MsgType;
    pVlanQMsg->u4ContextId = u4ContextId;
    pVlanQMsg->u4Port = u4IfIndex;
    pVlanQMsg->u2LocalPort = u2Port;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME,
                         " Send To Q failed for  Vlan port create message "
                         "Type \r\n");

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);

    return VLAN_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPostIntfChangeMessage                        */
/*                                                                           */
/*    Description         : Posts the message to Vlan Config Q.              */
/*                                                                           */
/*    Input(s)            : u4ContextId -  Context Identifier                */
/*                          u1IntfType  -  Interface Type                    */
/*                          u2MsgType  -  Message Type                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPostIntfChangeMessage (UINT4 u4ContextId, UINT1 u1IntfType, UINT2 u2MsgType)
{
    tVlanQMsg          *pVlanQMsg = NULL;

    if (VLAN_IS_MODULE_INITIALISED () != VLAN_TRUE)
    {
        return VLAN_FAILURE;
    }
    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                         "VLAN module is shutdown. Cannot Post Vlan "
                         "port create message  \r\n");

        return VLAN_FAILURE;
    }

    pVlanQMsg =
        (tVlanQMsg *) (VOID *) VLAN_GET_BUF (VLAN_QMSG_BUFF,
                                             sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME, " Message Allocation failed for Vlan "
                         "port create message  \r\n");
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = u2MsgType;
    pVlanQMsg->u4ContextId = u4ContextId;
    pVlanQMsg->u1IntfType = u1IntfType;

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         VLAN_NAME,
                         " Send To Q failed for  Vlan port create message "
                         "Type \r\n");

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return VLAN_FAILURE;
    }

    VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleProcessPkt                             */
/*                                                                           */
/*    Description         : It process the incoming data packet. this        */
/*                          function should not relese the context, which    */
/*                          will be done out side this fucntion              */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          pVlanIf  - Pointer to the Vlan If Msg.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleProcessPkt (tCRU_BUF_CHAIN_DESC * pFrame, tVlanIfMsg * pVlanIf)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4Pvid = 0;
    tVlanCurrEntry     *pVlanRec = NULL;
    tVlanPortEntry     *pPortEntry;
    tMacAddr            DestAddr;
    tMacAddr            SrcAddr;
    tVlanTag            VlanTag;
    tVlanId             VlanId = 0;
    UINT4               u4ContextId;
    INT4                i4Result;
    UINT2               u2InPort;
    UINT1               u1DefPriority;
    BOOL1               bFlag = VLAN_FALSE;
#ifdef MRVLLS
    tMacAddr            FdbAddr;
#endif
#ifndef NPAPI_WANTED
    tVlanPortInfoPerVlan *pPortInfoPerVlan = NULL;
    tVlanStats         *pVlanCounters = NULL;
#endif /*NPAPI_WANTED */
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanWildCardEntry *pWildCardEntry = NULL;
    if (VLAN_IS_PKT_RCVD_FROM_MPLS (pVlanIf) == VLAN_TRUE)
    {
#ifdef MPLS_WANTED
#ifdef HVPLS_WANTED
        CRU_BUF_Copy_FromBufChain (pFrame, (UINT1 *) &(pVlanIf->u4PwVcIndex), 0,
                                   sizeof (UINT4));
        if (pVlanIf->u4PwVcIndex < MAX_L2VPN_PW_VC_ENTRIES)
        {
            CRU_BUF_Move_ValidOffset (pFrame, sizeof (UINT4));
        }
        else
        {
            pVlanIf->u4PwVcIndex = 0;
        }
        if (L2VpnIsPwVpws (pVlanIf->u4PwVcIndex) == VLAN_TRUE)
        {
            pVlanIf->u4PwVcIndex = 0;
        }
#endif
#endif
        /* In MPLS slow path Raw Mode switching u2LocalPort 
         * will be used as a outgoing port */
        /* get context from interface on which the packet received 
         * only if port if index passed from MPLS L2VPN module is valid */
        if (pVlanIf->u2LocalPort != 0)
        {
            i4Result =
                VlanGetContextInfoFromIfIndex ((UINT4) pVlanIf->u2LocalPort,
                                               &u4ContextId,
                                               &(pVlanIf->u2LocalPort));
            if (i4Result == VLAN_FAILURE)
            {
                return;
            }
        }
        /*If Packet recvd from MPLS interface, go through MPLS
         * related processing and Fwd on Ethernet Ports*/
        VlanL2VpnHandleMplsPktFwdOnPorts (pFrame, pVlanIf);
#ifdef HVPLS_WANTED
        if (pVlanIf->u4PwVcIndex == 0)
        {
            return;
        }
#else
        return;
#endif

    }

    /* get context from interface on which the packet received */
    i4Result = VlanGetContextInfoFromIfIndex (pVlanIf->u4IfIndex,
                                              &u4ContextId, &u2InPort);
    if (i4Result == VLAN_FAILURE)
    {
        return;
    }
    /* update the local port in pVlanIf */
    pVlanIf->u2LocalPort = u2InPort;
    /* switch to the context, here no need to restore the context, restoring is 
       done after returning from this function  */
    i4Result = VlanSelectContext (u4ContextId);
    if (i4Result == VLAN_FAILURE)
    {
        return;
    }
    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VLAN module is not enabled. Dropping the incoming "
                       "packet received on port %d.\r\n", pVlanIf->u4IfIndex);

        return;
    }

    if (VLAN_IS_PORT_VALID (u2InPort) == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Data Packet received on Invalid Port \n");

        return;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2InPort);

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                  "Data Packet received on Invalid Port \n");

        return;
    }

    if (pPortEntry->u1OperStatus == VLAN_OPER_DOWN)
    {
        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC,
                  VLAN_NAME, "Port Oper Status DOWN. Data Packet received\n");

        return;
    }

    MEMSET (&VlanTag, 0, sizeof (VlanTag));

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2InPort);
        if (pVlanPbPortEntry == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC,
                      VLAN_NAME,
                      "802.1ad Bridge. Packet received on invalid port.\n");
            return;
        }
        if (pVlanPbPortEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT)
        {
            if (pVlanIf->SVlanId != VLAN_NULL_VLAN_ID)
            {
                /* BPDU coming out of a PEP and is entering internal customer
                 * network port. */
                if (VlanPbHandlePktOnPep (pFrame, pVlanIf) == VLAN_FAILURE)

                {
                    VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC,
                              VLAN_NAME,
                              "802.1ad Bridge. Processing packets from "
                              " provider edge port failed .\n");
                    return;
                }
                /* BPDU coming out of a PEP is successfully transmitted on 
                   internal customer network port. */
                return;
            }
        }
    }

    VLAN_MEMCPY (DestAddr, VLAN_IFMSG_DST_ADDR (pVlanIf), ETHERNET_ADDR_SIZE);

    if (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_BRIDGE_MODE)
    {
        if (VLAN_TUNNEL_STATUS (pPortEntry) == VLAN_ENABLED)
        {
            if ((VLAN_DESTADDR_IS_STPADDR (DestAddr) == VLAN_TRUE) ||
                (VLAN_DESTADDR_IS_GVRPADDR (DestAddr) == VLAN_TRUE) ||
                (VLAN_DESTADDR_IS_MVRPADDR (DestAddr) == VLAN_TRUE))
            {
                /* Destination address is STP/GVRP address...
                 * Packet is at the ingress of the tunnel...
                 * use configured priority */
                u1DefPriority = VLAN_TUNNEL_BPDU_PRIORITY ();
            }
            else
            {
                u1DefPriority = pPortEntry->u1DefUserPriority;
            }

            if (VLAN_HW_ING_TAG_SUPPORT () == VLAN_FALSE)
            {
                VlanAddTagToFrame (pFrame, pPortEntry->Pvid, u1DefPriority,
                                   VLAN_DE_FALSE, pPortEntry);
                VLAN_IFMSG_LEN (pVlanIf) += VLAN_TAG_PID_LEN;
            }
            else
            {
                if ((VLAN_DESTADDR_IS_STPADDR (DestAddr) == VLAN_TRUE) ||
                    (VLAN_DESTADDR_IS_GVRPADDR (DestAddr) == VLAN_TRUE) ||
                    (VLAN_DESTADDR_IS_MVRPADDR (DestAddr) == VLAN_TRUE))
                {
                    VlanUpdatePriorityInFrame (pFrame, (UINT2) u1DefPriority);
                }
            }
        }
    }

    VLAN_MEMCPY (SrcAddr, VLAN_IFMSG_SRC_ADDR (pVlanIf), ETHERNET_ADDR_SIZE);

    VlanId = VlanGetVlanId (pFrame, SrcAddr, u2InPort, &VlanTag);

    /* Handling All Types of Tagged Packets for 802.1d Mode. In 802.1d Mode 
     * tagged packets are also considered as untagged pkts. So, change 
     * the tag information to Default Vlan, since 802.1D bridges has only 
     * Default Vlan present */
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanTag.OuterVlanTag.u2VlanId = VLAN_DEF_VLAN_ID;
        VlanId = VLAN_DEF_VLAN_ID;
    }

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    VlanCfaGetIfInfo (pVlanIf->u4IfIndex, &IfInfo);

    if ((VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE) &&
        (IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
        (IfInfo.u1IfSubType == CFA_SUBTYPE_AC_INTERFACE))
    {
        VlanUtilGetPvid (pVlanIf->u4IfIndex, &u4Pvid);
        VlanId = (UINT2) u4Pvid;
        VlanTag.OuterVlanTag.u2VlanId = VlanId;
        if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
        {
            return;
        }
    }

    if (VlanId == VLAN_NULL_VLAN_ID)
    {
        VlanL2IwfIncrFilterInDiscards (u2InPort);
        return;
    }

    pVlanRec = VlanGetVlanEntry (VlanId);

    if (pVlanRec == NULL)
    {
        /* If VlanId does not exist, But Wildcard vlan present for DestMAc,
         * then forward the frames to wildcard egress ports.    
         */
        pWildCardEntry = VlanGetWildCardEntry (DestAddr);
        if (pWildCardEntry != NULL)
        {
            VlanFwdOnPorts (pFrame, pVlanIf, pWildCardEntry->EgressPorts,
                            NULL, &VlanTag, BRG_DATA_FRAME);
        }
        else
        {

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                           "VLAN 0x%x associated with the data packet "
                           "is unknown \n", VlanId);

            VlanL2IwfIncrFilterInDiscards (u2InPort);
        }
        return;
    }
#ifdef MRVLLS

    if (VLAN_IS_BCASTADDR (DestAddr) == VLAN_TRUE)
    {
        VLAN_MEMSET (FdbAddr, 0, sizeof (tMacAddr));
        VLAN_MEMCPY (FdbAddr, SrcAddr, ETHERNET_ADDR_SIZE);
        VlanHwFdbTableAdd (VLAN_GET_PHY_PORT (u2InPort), SrcAddr, VlanId,
                           VLAN_FDB_LEARNT);
    }
#endif

    if ((VLAN_IS_MCASTADDR (DestAddr) == VLAN_TRUE) ||
        (VLAN_IS_BCASTADDR (DestAddr) == VLAN_TRUE))
    {
        if (VlanCheckReservedGroup (DestAddr) == VLAN_FALSE)
        {
            if (VlanGetMacMapMcastBcastFilterOption (u2InPort, SrcAddr)
                == VLAN_MCAST_DROP)
            {
#ifndef NPAPI_WANTED
                if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
                {
                    pPortInfoPerVlan =
                        VlanGetPortStatsEntry (pVlanRec, u2InPort);
                    if (NULL == pPortInfoPerVlan)
                    {
                        return;
                    }

                    VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
                }
#endif
                return;
            }
        }
    }

    if ((VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE) &&
        (pVlanPbPortEntry != NULL) &&
        ((pVlanPbPortEntry->u1PbPortType) == VLAN_CUSTOMER_EDGE_PORT))
    {
        if (VlanPbHandlePktRxOnCep (pFrame, u2InPort, &VlanTag) == VLAN_FAILURE)
        {
            return;
        }
    }
#ifndef NPAPI_WANTED
    if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
    {
        /* 
         * Port stats entry will be there in all vlan entries for all active
         * ports 
         */
        pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2InPort);
        if (NULL == pPortInfoPerVlan)
        {
            return;
        }
        VLAN_INC_IN_FRAMES (pPortInfoPerVlan);

        pVlanCounters = pVlanRec->pVlanStats;
        if (pVlanCounters != NULL)
        {
            if ((VLAN_IS_BCASTADDR (DestAddr) == VLAN_TRUE) ||
                (VLAN_IS_MCASTADDR (DestAddr) == VLAN_TRUE))
            {
                VLAN_INC_IN_MCAST_BCAST (pVlanCounters);
            }
            else
            {
                /*Unicast frame is received */
                VLAN_INC_IN_UCAST (pVlanCounters);
            }
        }
    }
#endif /*NPAPI_WANTED */

    i4Result = VlanIngressFiltering (pVlanRec, u2InPort);

    if (i4Result == VLAN_NO_FORWARD)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Data packet received on port %d is discarded due"
                       " to Ingress Filtering.\n", pVlanIf->u4IfIndex);

#ifndef NPAPI_WANTED

        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
        }
#endif
        VlanL2IwfIncrFilterInDiscards (u2InPort);

        return;
    }

#ifndef NPAPI_WANTED
    if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
    {
        bFlag = pPortInfoPerVlan->b1LckStatus;

    }
#endif

    if (bFlag == VLAN_TRUE)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Data packet received on port %d is discarded due"
                       " to Lck Condition Set\n", pVlanIf->u4IfIndex);
#ifndef NPAPI_WANTED
        VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
#endif

        VlanL2IwfIncrFilterInDiscards (u2InPort);

        return;
    }

#ifdef NPAPI_WANTED
    if (IssCPUControlledLearning (u2InPort) == OSIX_SUCCESS)
    {
        VlanLearn (SrcAddr, u2InPort, pVlanRec);
    }
#else
    VlanLearn (SrcAddr, u2InPort, pVlanRec);
#endif
    if (VLAN_IS_MCASTADDR (DestAddr) == VLAN_TRUE)
    {
        /* packet is destined for a multicast or broadcast packet */
        VlanProcessMcastDataPkt (pFrame, pVlanIf, pVlanRec, &VlanTag);
    }
    else
    {
        VlanProcessUcastDataPkt (pFrame, pVlanIf, pVlanRec, &VlanTag);
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanProcessCfgEvent                              */
/*                                                                           */
/*    Description         : Function Process the received configuration event*/
/*                                                                           */
/*    Input(s)            : pVlanQMsg - Pointer to the Q Message             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanProcessCfgEvent (tVlanQMsg * pVlanQMsg)
{
#ifdef SW_LEARNING
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
#endif
    INT4                i4RetVal = 0;
#if (defined SW_AGE_TIMER) || (!defined NPAPI_WANTED)
    INT4                i4AgingTime;
#endif
#if(!defined NPAPI_WANTED)
    INT4                i4Result = OSIX_FALSE;
    UINT4               u4FdbId = 0;
#endif
    UINT4               u4ContextId;
    UINT4               u4PoContextId;
    UINT4               u4InterfaceType = VLAN_INVALID_INTERFACE_TYPE;
    UINT2               u2LocalPort;
    UINT2               u2LocalDstPort;
#ifdef MBSM_WANTED
    INT4                i4ProtoId = 0;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
#endif /* MBSM_WANTED */
#ifdef MRVLLS
    tLocalPortList      AllowedToGoPortList;
    tMacAddr            ConnectionId;
    UINT2               u2PnacMacAuthStatus = 0;
    UINT2               u2PortAuthControl = 0;
    UINT2               u2PortAuthMode = 0;
    UINT1               u1VlanStatus = 0;
#endif
#ifdef ICCH_WANTED
#ifdef SW_LEARNING
    tVlanFdbInfo       *pVlanFdbInfo = NULL;
#endif
    tVlanIcchUcastFdbSync VlanIcchUcastFdb;
    MEMSET (&VlanIcchUcastFdb, 0, sizeof (tVlanIcchUcastFdbSync));
#endif
#ifdef SW_LEARNING
    VLAN_MEMSET (au1String, 0, VLAN_CLI_MAX_MAC_STRING_SIZE);
#endif
    switch (VLAN_QMSG_TYPE (pVlanQMsg))
    {
        case VLAN_CREATE_CONTEXT_MSG:

            i4RetVal = VlanHandleCreateContext (pVlanQMsg->u4ContextId);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                                 ALL_FAILURE_TRC,
                                 "VLAN context %d creation failed  %d \n",
                                 pVlanQMsg->u4ContextId);
            }
            L2MI_SYNC_GIVE_SEM ();
            break;

        case VLAN_DELETE_CONTEXT_MSG:

            i4RetVal = VlanHandleDeleteContext (pVlanQMsg->u4ContextId);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                                 ALL_FAILURE_TRC,
                                 "VLAN context %d deletion failed  %d \n",
                                 pVlanQMsg->u4ContextId);
            }

            L2MI_SYNC_GIVE_SEM ();
            break;

        case VLAN_UPDATE_CONTEXT_NAME:

            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }

            i4RetVal = VlanHandleUpdateContextName (pVlanQMsg->u4ContextId);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                                 ALL_FAILURE_TRC,
                                 "VLAN context %d name updation failed  %d \n",
                                 pVlanQMsg->u4ContextId);
            }
            break;

        case VLAN_PORT_MAP_MSG:

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                L2MI_SYNC_GIVE_SEM ();
                break;
            }

            i4RetVal = VlanHandleCreatePort (pVlanQMsg->u4ContextId,
                                             pVlanQMsg->u4Port,
                                             pVlanQMsg->u2LocalPort);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VLAN Port map failed for Port %d \n",
                               pVlanQMsg->u4Port);
            }

            L2MI_SYNC_GIVE_SEM ();
            break;

        case VLAN_PORT_UNMAP_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                L2MI_SYNC_GIVE_SEM ();
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                L2MI_SYNC_GIVE_SEM ();
                break;
            }
            i4RetVal = VlanHandleDeletePort (u2LocalPort);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VLAN Port unmap failed for Port %d \n",
                               pVlanQMsg->u4Port);
            }

            L2MI_SYNC_GIVE_SEM ();
            break;

        case VLAN_PORT_CREATE_MSG:

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }

            i4RetVal = VlanHandleCreatePort (pVlanQMsg->u4ContextId,
                                             pVlanQMsg->u4Port,
                                             pVlanQMsg->u2LocalPort);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VLAN Port Creation failed for Port %d \n",
                               pVlanQMsg->u4Port);
            }

            L2_SYNC_GIVE_SEM ();

            break;

        case VLAN_PORT_DELETE_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }
            i4RetVal = VlanHandleDeletePort (u2LocalPort);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VLAN Port Deletion failed for Port %d \n",
                               pVlanQMsg->u4Port);
            }

            L2_SYNC_GIVE_SEM ();
            break;

        case VLAN_UPDATE_INTF_TYPE_MSG:

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }
            i4RetVal = VlanPbbUpdateInterfaceType (pVlanQMsg->u1IntfType);

            if (i4RetVal == VLAN_FALSE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VLAN Interface Type updation failed "
                               "for Context %d \n", pVlanQMsg->u4ContextId);
            }

            L2_SYNC_GIVE_SEM ();
            break;

        case VLAN_PORT_DEL_COPY_PROPERTIES_MSG:
            /*Context info of a port channel is obtained from VCM
               as in case port channel is of the type PIP, vlan has no
               information regarding it as vlan is PIP unaware. */

            if (VlanVcmGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                                  &u4PoContextId,
                                                  &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4DstPort,
                                               &u4ContextId,
                                               &u2LocalDstPort) == VLAN_FAILURE)
            {
                break;
            }

            /* switch context. Both physical port and port-channel are in same context */
            if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
            {
                break;
            }
            /*If the Port channel is of the type PIP, then this function would
               only delete the u2LocalDstPort */
            i4RetVal =
                VlanHandleDelPortAndCopyProperties (u2LocalDstPort,
                                                    u2LocalPort);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VLAN Port Deletion failed for Port %d \n",
                               pVlanQMsg->u4Port);
            }

#ifdef NPAPI_WANTED
#ifndef LA_HW_TRUNK_SUPPORTED

            /*Program logical interface properties in Hw */
            VlanSispHandleDelAndCopySispPortProp (pVlanQMsg->u4DstPort,
                                                  pVlanQMsg->u4Port,
                                                  VLAN_PORT_DEL_COPY_PROPERTIES_MSG);
#endif
#endif
            break;

        case VLAN_PORT_OPER_UP_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }

            i4RetVal = VlanHandlePortOperInd (u2LocalPort, VLAN_OPER_UP);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VLAN Port Oper Ind failed for Port %d \n",
                               pVlanQMsg->u4Port);
                L2_SYNC_GIVE_SEM ();
                break;
            }
            i4RetVal = VlanRedSyncPortOperStatus (VLAN_CURR_CONTEXT_ID (),
                                                  u2LocalPort, VLAN_OPER_UP);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VLAN - RED: Failed to sync oper up "
                               "indication for Port %d with standby node\n",
                               pVlanQMsg->u4Port);
            }
            L2_SYNC_GIVE_SEM ();
            break;

        case VLAN_PORT_OPER_DOWN_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }
            i4RetVal = VlanHandlePortOperInd (u2LocalPort, VLAN_OPER_DOWN);
            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VLAN Port Oper Ind failed for Port %d \n",
                               pVlanQMsg->u4Port);
                L2_SYNC_GIVE_SEM ();
                break;
            }

            i4RetVal = VlanRedSyncPortOperStatus (VLAN_CURR_CONTEXT_ID (),
                                                  u2LocalPort, VLAN_OPER_DOWN);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VLAN - RED: Failed to sync oper down "
                               "indication for Port %d with standby node\n",
                               pVlanQMsg->u4Port);
            }
            L2_SYNC_GIVE_SEM ();
            break;
        case VLAN_PORT_P2P_UPDATE_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }

            if (VLAN_IS_ENH_FILTERING_ENABLED () == VLAN_TRUE)
            {
                VlanUpdateEnhanceFiltering (u2LocalPort);
            }

            break;

        case VLAN_PORT_STP_STATE_UPDATE_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }

            if (VLAN_IS_ENH_FILTERING_ENABLED () == VLAN_TRUE)
            {
                VlanUpdateEnhanceFiltering (VLAN_ALL_PORTS);
            }

            break;

#ifndef NPAPI_WANTED
        case VLAN_DELETE_FDB_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }
            VlanHandleDeleteFdbEntries (u2LocalPort);

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Delete Fdb Entries called for port  %d \n",
                           pVlanQMsg->u4Port);
            break;

        case VLAN_FLUSH_FDB_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Flush Fdb Entries called for port  %d \n",
                           pVlanQMsg->u4Port);

            VlanHandleFlushFdbEntries (u2LocalPort, pVlanQMsg->u4FdbId,
                                       pVlanQMsg->u1ModType);
            break;

        case VLAN_FLUSH_FDB_LIST_MSG:

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Flush Fdb Entries called for port  %d \n",
                           pVlanQMsg->u2LocalPort);
            for (u4FdbId = 1; u4FdbId <= VLAN_DEV_MAX_VLAN_ID; u4FdbId++)
            {
                OSIX_BITLIST_IS_BIT_SET (pVlanQMsg->au1FdbList, u4FdbId,
                                         VLAN_DEV_VLAN_LIST_SIZE, i4Result);

                if (i4Result == OSIX_TRUE)
                {
                    VlanHandleFlushFdbEntries (pVlanQMsg->u2LocalPort, u4FdbId,
                                               pVlanQMsg->u1ModType);
                }
            }

            break;

        case VLAN_FLUSH_FDBID_MSG:
            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "Flush Fdb Entries called for FdbId  %d \n",
                           pVlanQMsg->u4FdbId);

            VlanHandleFlushFdbId (pVlanQMsg->u4FdbId);
            break;
#endif
#if (defined SW_AGE_TIMER) || (!defined NPAPI_WANTED)
        case VLAN_SHORT_AGEOUT_MSG:

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }
            /* Need not set Ageout time for the switch again when 
             * it has already been set by some other port */
            if (gpVlanContextInfo->VlanInfo.i4RapidAgingNumPorts == 0)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                          "Short ageout set\n");

                gpVlanContextInfo->VlanInfo.i4ShortAgingTime =
                    pVlanQMsg->i4AgingTime;

                gpVlanContextInfo->VlanInfo.bUseShortAgingTime = VLAN_TRUE;

                /* Restart the Age out timer */
                VLAN_START_TIMER (VLAN_AGEOUT_TIMER, pVlanQMsg->i4AgingTime);
            }

            gpVlanContextInfo->VlanInfo.i4RapidAgingNumPorts++;

            break;

        case VLAN_LONG_AGEOUT_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }
            gpVlanContextInfo->VlanInfo.i4RapidAgingNumPorts--;

            /* Do not revert to the long timeout if any port 
             * still requires it's entries to be aged out rapidly */
            if (gpVlanContextInfo->VlanInfo.i4RapidAgingNumPorts == 0)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                          "Short ageout reset\n");

                i4AgingTime = (INT4) VlanL2IwfGetAgeoutInt ();

                gpVlanContextInfo->VlanInfo.bUseShortAgingTime = VLAN_FALSE;

                i4AgingTime = VLAN_SPLIT_AGEOUT_TIME (i4AgingTime);

                /* Restart the Age out timer */
                VLAN_START_TIMER (VLAN_AGEOUT_TIMER, i4AgingTime);
            }

            break;
#endif

        case VLAN_ADD_L2VPN_INFO:
            if (VlanSelectContext (pVlanQMsg->L2VpnData.u4ContextId) ==
                VLAN_FAILURE)
            {
                break;
            }

            VlanL2VpnHandleAddL2VpnInfo (&pVlanQMsg->L2VpnData);

            break;

        case VLAN_DEL_L2VPN_INFO:
            if (VlanSelectContext (pVlanQMsg->L2VpnData.u4ContextId) ==
                VLAN_FAILURE)
            {
                break;
            }

            VlanL2VpnHandleDelL2VpnInfo (&pVlanQMsg->L2VpnData);

            break;

#ifdef NPAPI_WANTED
        case VLAN_COPY_PORT_INFO_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4DstPort,
                                               &u4ContextId,
                                               &u2LocalDstPort) == VLAN_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }
            /* we presume that both the port belongs to same context */
            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }

            VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "VlanCopyPortPropertiesToHw called for Destination Port  %d"
                           "and SrcPort \n", pVlanQMsg->u4DstPort,
                           pVlanQMsg->u4Port);

            VlanL2IwfGetInterfaceType (pVlanQMsg->u4ContextId,
                                       (UINT1 *) &u4InterfaceType);

            VlanHandleCopyPortPropertiesToHw (u2LocalDstPort, u2LocalPort,
                                              (UINT1) u4InterfaceType);

            /*Program logical interface properties in Hw */
            VlanSispHandleDelAndCopySispPortProp (pVlanQMsg->u4DstPort,
                                                  pVlanQMsg->u4Port,
                                                  VLAN_COPY_PORT_INFO_MSG);
            L2_SYNC_GIVE_SEM ();
            break;

        case VLAN_REMOVE_PORT_INFO_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            /* the port was part of a portchannel, hence, obtain the
             * context id and local port number from vcm */
            if (VlanVcmGetContextInfoFromIfIndex (pVlanQMsg->u4DstPort,
                                                  &u4ContextId,
                                                  &u2LocalDstPort)
                == VLAN_FAILURE)
            {
                break;
            }
            /* we presume that both the port belongs to same context */
            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }

            VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "VlanRemoveyPortPropertiesToHw called for DstPort"
                           "%d and SrcPort %d \n", pVlanQMsg->u4DstPort,
                           pVlanQMsg->u4Port);

            VlanHandleRemovePortPropertiesFromHw (pVlanQMsg->u4DstPort,
                                                  u2LocalPort, VLAN_TRUE);

            /* Removing the PB Related Tables for the port going out of the aggregation */
            VlanHandlePbRemovePortTablesFromHw (pVlanQMsg, u2LocalPort,
                                                u2LocalDstPort);

            /*Program logical interface properties in Hw */
            VlanSispHandleDelAndCopySispPortProp (pVlanQMsg->u4DstPort,
                                                  pVlanQMsg->u4Port,
                                                  VLAN_REMOVE_PORT_INFO_MSG);

            break;

        case VLAN_COPY_PORT_MCAST_INFO_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "VlanCopyPortMcastPropertiesToHw called for"
                           "Port %d\n", pVlanQMsg->u4Port);

            VlanHandleCopyPortMcastPropertiesToHw (u2LocalPort);
            /*Program logical interface properties in Hw */
            VlanSispHandleDelAndCopySispPortProp (pVlanQMsg->u4Port,
                                                  pVlanQMsg->u4Port,
                                                  VLAN_COPY_PORT_MCAST_INFO_MSG);

            break;

        case VLAN_REMOVE_PORT_MCAST_INFO_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "VlanRemovePortMcastPropertiesFromHw called for"
                           "Port %d\n", pVlanQMsg->u4Port);
            VlanHandleRemovePortMcastPropertiesFromHw (u2LocalPort);

            break;

        case VLAN_COPY_PORT_UCAST_INFO_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "VlanHandleCopyPortUcastPropertiesToHw called for"
                           "Port %d\n", pVlanQMsg->u4Port);

            VlanHandleCopyPortUcastPropertiesToHw (u2LocalPort);

            /*Program logical interface properties in Hw */
            VlanSispHandleDelAndCopySispPortProp (pVlanQMsg->u4Port,
                                                  pVlanQMsg->u4Port,
                                                  VLAN_COPY_PORT_UCAST_INFO_MSG);

            break;

        case VLAN_REMOVE_PORT_UCAST_INFO_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "VlanHandleRemovePortUcastPropertiesFromHw called for"
                           "Port %d\n", pVlanQMsg->u4Port);
            VlanHandleRemovePortUcastPropertiesFromHw (u2LocalPort);

            break;

#else
            UNUSED_PARAM (u4InterfaceType);

#endif

#ifdef MBSM_WANTED
        case MBSM_MSG_CARD_INSERT:

            VLAN_MEMSET (&gMbsmSlotInfo, 0, sizeof (tMbsmSlotInfo));
            VLAN_MEMSET (&gMbsmPortInfo, 0, sizeof (tMbsmPortInfo));

            i4ProtoId = pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg->i4ProtoCookie;
            gi4ProtoId = i4ProtoId;
            pSlotInfo =
                &(pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmSlotInfo);
            VLAN_MEMCPY (&gMbsmSlotInfo, pSlotInfo, sizeof (tMbsmSlotInfo));
            pPortInfo =
                &(pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmPortInfo);
            VLAN_MEMCPY (&gMbsmPortInfo, pPortInfo, sizeof (tMbsmPortInfo));
            i4RetVal = VlanMbsmUpdateOnCardInsertion (pPortInfo, pSlotInfo);
            /* Do not send ack to MBSM here */
            /* It will be done inside VlanMbsmUpdateOnCardInsertion */
            MEM_FREE (pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg);
            break;

        case MBSM_MSG_CARD_REMOVE:

            i4ProtoId = pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg->i4ProtoCookie;
            pSlotInfo =
                &(pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmSlotInfo);
            pPortInfo =
                &(pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmPortInfo);

            i4RetVal = VlanMbsmUpdateOnCardRemoval (pPortInfo, pSlotInfo);

            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
            MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
            MbsmProtoAckMsg.i4RetStatus = i4RetVal;
            MbsmSendAckFromProto (&MbsmProtoAckMsg);
            MEM_FREE (pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg);
            break;

        case MBSM_MSG_LOAD_SHARING_ENABLE:
            VlanMbsmUpdtVlanTblForLoadSharing (MBSM_LOAD_SHARING_ENABLE);
            VlanMbsmUpdtL2McForLoadSharing (MBSM_LOAD_SHARING_ENABLE);
            break;

        case MBSM_MSG_LOAD_SHARING_DISABLE:
            VlanMbsmUpdtVlanTblForLoadSharing (MBSM_LOAD_SHARING_DISABLE);
            VlanMbsmUpdtL2McForLoadSharing (MBSM_LOAD_SHARING_DISABLE);
            break;
#endif

#ifdef L2RED_WANTED
        case VLAN_RM_MSG:

            VlanRedProcessUpdateMsg (pVlanQMsg);
            break;

        case VLAN_DYN_DATA_APPLIED_MSG:

            VlanRedProcessDataAppliedMsg ();
            break;
#endif /* L2RED_WANTED */
#ifdef ICCH_WANTED
        case VLAN_ICCH_MSG:
            VlanIcchProcessUpdateMsg (pVlanQMsg);
            break;
#endif /* ICCH_WANTED */

        case VLAN_MCLAG_ENABLE_STATUS:
        case VLAN_MCLAG_DISABLE_STATUS:
            VlanProcessMclagStatus (pVlanQMsg);
            break;

        case VLAN_FDBENTRY_ADD_MSG:

            if (VlanGetContextInfoFromIfIndex (pVlanQMsg->u4Port,
                                               &u4ContextId,
                                               &u2LocalPort) == VLAN_FAILURE)
            {
                break;
            }

            if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
            {
                break;
            }
            if (pVlanQMsg->u1EntryType == VLAN_STATIC_ADDR)
            {
                VLAN_TRC_ARG2 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "Dropping indication received for statically configured"
                               "Mac entry on Port %d for Vlan %d. \n",
                               pVlanQMsg->u4Port, pVlanQMsg->FdbData.VlanId);
                break;
            }

            if (L2IwfMiIsVlanActive (u4ContextId,
                                     pVlanQMsg->FdbData.VlanId) == OSIX_FALSE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "Vlan is not active in the context %d\n",
                               u4ContextId);
                break;
            }

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "VlanFdbTableAddInCtxt called for context%d\n",
                           u4ContextId);
#ifdef SW_LEARNING

            /* Check If the Entry is Already present in SW_LEARNING Database
             * and break if it is there. 
             */
            if (VLAN_SUCCESS == VlanGetFdbEntryWithPort
                (pVlanQMsg->FdbData.au1MacAddr,
                 pVlanQMsg->FdbData.VlanId, u2LocalPort))
            {
                PrintMacAddress (pVlanQMsg->FdbData.au1MacAddr, au1String);
                VLAN_TRC_ARG3 (VLAN_MOD_TRC,
                               CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                               VLAN_NAME,
                               "FDB Entry Mac:%s VlanId:%d Port:%d Already"
                               "present in SW Database. \n", au1String,
                               pVlanQMsg->FdbData.VlanId, u2LocalPort);
                break;
            }

            VlanFdbTableAddInCtxt (u2LocalPort, pVlanQMsg->FdbData.au1MacAddr,
                                   pVlanQMsg->FdbData.VlanId,
                                   pVlanQMsg->u1EntryType,
                                   pVlanQMsg->FdbData.au1ConnectionId, FNP_ZERO,
                                   FNP_ZERO, pVlanQMsg->u4PwIndex);

            VlanPvlanFdbTableAdd (u2LocalPort,
                                  pVlanQMsg->FdbData.au1MacAddr,
                                  pVlanQMsg->FdbData.VlanId,
                                  VLAN_FDB_MGMT,
                                  pVlanQMsg->FdbData.au1ConnectionId);
#ifdef ICCH_WANTED
            VlanCfaGetInterfaceNameFromIndex (pVlanQMsg->u4Port,
                                              VlanIcchUcastFdb.au1IfName);
            VLAN_CPY_MAC_ADDR (VlanIcchUcastFdb.UcastAddr,
                               pVlanQMsg->FdbData.au1MacAddr);
            VLAN_CPY_MAC_ADDR (VlanIcchUcastFdb.ConnectionId,
                               pVlanQMsg->FdbData.au1ConnectionId);
            VlanIcchUcastFdb.u4IfIndex = pVlanQMsg->u4Port;
            VlanIcchUcastFdb.u4Context = u4ContextId;
            VlanIcchUcastFdb.VlanId = pVlanQMsg->FdbData.VlanId;
            VlanIcchUcastFdb.u2Flag = VLAN_ADD;
            VlanIcchUcastFdb.u4PwIndex = pVlanQMsg->u4PwIndex;
            VlanIcchSyncUcastAddress (&VlanIcchUcastFdb);
#endif
#else
#ifdef MRVLLS
            if (AclCheckDenyRule ((UINT2) pVlanQMsg->FdbData.VlanId,
                                  pVlanQMsg->FdbData.au1MacAddr,
                                  SRC_MAC_BASED_ACL) == ISS_DROP)
            {
                return;
            }
            u1VlanStatus = VLAN_OTHER;
#ifdef PNAC_WANTED
            if (PnacPnacHwGetAuthStatus
                (u2LocalPort, pVlanQMsg->FdbData.au1MacAddr, 0,
                 &u2PnacMacAuthStatus, NULL) == FNP_FAILURE)
            {
                return;
            }
            if ((u2PnacMacAuthStatus == PNAC_MAC_SESS_DEL) ||
                (u2PnacMacAuthStatus == PNAC_PORTSTATUS_UNAUTHORIZED))

            {
                /* TODO: Send indication to PNAC - First Data Frame */
                return;
            }

            u2PnacMacAuthStatus = 0;

            if (L2IwfGetPortPnacAuthControl (u2LocalPort, &u2PortAuthControl)
                == L2IWF_SUCCESS)
            {
                if (L2IwfGetPortPnacAuthMode (u2LocalPort, &u2PortAuthMode)
                    == L2IWF_SUCCESS)
                {
                    if ((u2PortAuthControl == PNAC_PORTCNTRL_AUTO) &&
                        (u2PortAuthMode == PNAC_PORT_AUTHMODE_MACBASED))
                    {
                        /*if port auth control is auto and auth mode is
                         *mac based,then the vlan status should be Permanent*/
                        u1VlanStatus = VLAN_PERMANENT;
                    }
                }
            }
#endif

            MEMSET (AllowedToGoPortList, 0, sizeof (AllowedToGoPortList));
            MEMSET (ConnectionId, 0, sizeof (ConnectionId));

            /* Program the hardware */
            VLAN_SET_MEMBER_PORT (AllowedToGoPortList, u2LocalPort);

            if (VlanHwAddStaticUcastEntry (pVlanQMsg->FdbData.VlanId,
                                           pVlanQMsg->FdbData.au1MacAddr, 0,
                                           AllowedToGoPortList, u1VlanStatus,
                                           ConnectionId) == VLAN_FAILURE)
            {
                return;
            }
#endif
#endif
            break;

        case VLAN_FDBENTRY_DELETE_MSG:

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }
            if (pVlanQMsg->u1EntryType == VLAN_STATIC_ADDR)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                               "Dropping indication received for statically configured"
                               "Mac entry on Vlan %d. \n",
                               pVlanQMsg->FdbData.VlanId);
                break;
            }

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "VlanHandleDeleteFdbEntry called for context%d\n",
                           pVlanQMsg->u4ContextId);

#ifdef SW_LEARNING
#ifdef ICCH_WANTED
            VLAN_CPY_MAC_ADDR (VlanIcchUcastFdb.UcastAddr,
                               pVlanQMsg->FdbData.au1MacAddr);
            VLAN_CPY_MAC_ADDR (VlanIcchUcastFdb.ConnectionId,
                               pVlanQMsg->FdbData.au1ConnectionId);
            VlanIcchUcastFdb.u4IfIndex = pVlanQMsg->u4Port;
            VlanIcchUcastFdb.u4Context = VLAN_DEFAULT_CONTEXT_ID;
            VlanIcchUcastFdb.VlanId = pVlanQMsg->FdbData.VlanId;
            VlanIcchUcastFdb.u2Flag = VLAN_DELETE;

            /* Syncing the peer about the FDB entry deletion is not
             * required if the entry is not present on the SW_LEARNING
             * database.
             */

            pVlanFdbInfo = VlanGetFdbEntry ((UINT4) VlanIcchUcastFdb.VlanId,
                                            VlanIcchUcastFdb.UcastAddr);
            if (NULL != pVlanFdbInfo)
            {
                if (pVlanFdbInfo->u2RemoteId != VLAN_REMOTE_FDB)
                {
                    VlanIcchSyncUcastAddress (&VlanIcchUcastFdb);
                }
            }
#endif
#endif

            VlanHandleDeleteFdbEntry (pVlanQMsg->FdbData.VlanId,
                                      pVlanQMsg->FdbData.au1MacAddr,
                                      VLAN_LOCAL_FDB);

            break;

        case VLAN_FDBENTRY_ALL_DELETE_MSG:

            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "VlanHandleDeleteAllFdbEntry called for context%d\n",
                           pVlanQMsg->u4ContextId);

            VlanHandleDeleteAllFdbEntry ();

            break;

        case VLAN_STP_STATUS_CHG_MSG:
            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
                break;
            }
            if (VlanUpdateTunnelStatusforSTP
                (pVlanQMsg->u4Port,
                 pVlanQMsg->bTunnelSTPStatus) == VLAN_FAILURE)
            {
                VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                          "failure while setting VlanUpdateTunnelStatusforSTP");
                return;
            }
            break;

        case VLAN_EVB_CDCP_LLDP_MSG:
            /* switch context */
            if (VlanSelectContext (pVlanQMsg->u4ContextId) == VLAN_FAILURE)
            {
#ifdef EVB_WANTED
                VLAN_EVB_RELEASE_BUF (VLAN_EVB_QUE_ENTRY,
                                      pVlanQMsg->pEvbQueMsg);
                break;
#endif
                /* DO NOT Return here.Since pEvbQueMsg to be released */
            }
            if (VlanEvbCdcpProcessTlv (pVlanQMsg->pEvbQueMsg) == VLAN_FAILURE)
            {
                VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                          "Failure in processing CDCP TLV");

            }
            break;

        default:
            break;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleCreatePort                             */
/*                                                                           */
/*    Description         : Creates a Port in VLAN module                    */
/*                                                                           */
/*    Input(s)            : u2Port    - The Number of the Port               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gau1PriTrfClassMap                         */
/*                                                                           */
/*    Global Variables Modified : gpVlanContextInfo->u2VlanStartPortInd,     */
/*                                gpVlanContextInfo->u2VlanLastPortInd       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanHandleCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPort)
{
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanBasePortEntry *pBasePortEntry = NULL;
    tVlanPortEntry     *pLastPortEntry = NULL;
#ifdef NPAPI_WANTED
    tFsNpVlanPortReflectEntry FsNpVlanPortReflectEntry;
#endif
    tCfaIfInfo          CfaIfInfo;
    tIssUpdtPortIsolation PortIsolation;
    UINT4               u4ComponentId = u4ContextId;
    UINT4               u4IcclIfIndex = 0;

#ifdef NPAPI_WANTED
    VLAN_MEMSET (&FsNpVlanPortReflectEntry, 0,
                 sizeof (tFsNpVlanPortReflectEntry));
    tVlanHwPortInfo     VlanHwPortInfo;
    MEMSET (&VlanHwPortInfo, 0, sizeof (tVlanHwPortInfo));
#endif

    VLAN_CONVERT_CTXT_ID_TO_COMP_ID (u4ComponentId);

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Create Port failed as module is shutdown\n");
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2LocalPort) == VLAN_FALSE)
    {
        /* Port number exceeds the allowed range */
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Create Port for invalid port number \n");

        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    /* Check whether this port is already active */

    if (pPortEntry != NULL)
    {
        /* this port is already active */
        return VLAN_SUCCESS;
    }
    if (VlanCfaGetIfInfo (u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  " Failed to get Cfa Interface Parameters. \r\n");
        return VLAN_FAILURE;
    }

    /* If the PIP Port create Indication is NOT given to VLAN then
     * due to synchronisation issues between VLAN and PBB, Bridge Port 
     * type may be wrongly set in hardware. To avoid this PIP create 
     * message is posted to VLAN but VLAN ignores it.
     */

    if (CfaIfInfo.u1BrgPortType == VLAN_PROVIDER_INSTANCE_PORT)
    {
        return VLAN_SUCCESS;
    }

    /*create new port entry */
    pPortEntry = (tVlanPortEntry *) (VOID *) VLAN_GET_BUF (VLAN_PORT_ENTRY,
                                                           sizeof
                                                           (tVlanPortEntry));

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  VLAN_NAME, "No Memory for Port Entry creation\n");

        return VLAN_FAILURE;
    }

    pPortEntry->u2Port = u2LocalPort;
    pPortEntry->u4IfIndex = u4IfIndex;
    pPortEntry->u4ContextId = u4ContextId;
    pPortEntry->u1OperStatus = VLAN_OPER_DOWN;
    pPortEntry->u2FiltUtilityCriteria = VLAN_DEFAULT_FILTERING_CRITERIA;
    pPortEntry->u1MacLearningStatus = VLAN_PORT_MAC_LEARNING_ENABLED;
    pPortEntry->u1EgressTPIDType = VLAN_PORT_EGRESS_PORTBASED;
    pPortEntry->u2AllowableTPID1 = VLAN_PORT_DEFAULT_ALLOWABLE_TPID;
    pPortEntry->u2AllowableTPID2 = VLAN_PORT_DEFAULT_ALLOWABLE_TPID;
    pPortEntry->u2AllowableTPID3 = VLAN_PORT_DEFAULT_ALLOWABLE_TPID;
    pPortEntry->i4ReflectionStatus = VLAN_DISABLED;

#ifdef NPAPI_WANTED
    FsNpVlanPortReflectEntry.u4IfIndex = u4IfIndex;
    FsNpVlanPortReflectEntry.u4Status = VLAN_DISABLED;
    FsNpVlanPortReflectEntry.u4ContextId = u4ContextId;

    if (FNP_FAILURE ==
        VlanFsMiVlanHwPortPktReflectStatus (&FsNpVlanPortReflectEntry))
    {

        VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                         "Unable to set port reflection for the port "
                         "%d in hardware \r\n", u4IfIndex);

    }

#endif

    if (CfaIfInfo.u1BrgPortType == CFA_UPLINK_ACCESS_PORT)
    {
        if (VlanEvbUapIfWrAddEntry (u4IfIndex) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           VLAN_NAME, "UAP port %d creation failed\n",
                           u4IfIndex);
            VLAN_RELEASE_BUF (VLAN_PORT_ENTRY, (UINT1 *) pPortEntry);
            VLAN_GET_PORT_ENTRY (u2LocalPort) = NULL;
            return VLAN_FAILURE;
        }
        VlanEvbUapIfSetDefProperties (pPortEntry);
    }
    /* The following row status is added regarding the implementation of 
     * ieee8021BridgeDot1dPortTable. When creating the default port entries
     * the status would be ACTIVE. But when creating the entries through 
     * management routines, this u1RowStatus object would be over written 
     * in nmh handling function as CREATE_AND_WAIT, CREATE_AND_GO, ACTIVE,
     * DESTROY, NOT_IN_SERVICE or NOT_READY.
     */
    pPortEntry->u1RowStatus = ACTIVE;

#ifndef NPAPI_WANTED
    if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
    {
        if (VlanAddPortStatsForAllVlans (u2LocalPort) != VLAN_SUCCESS)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "Unable to add port stats for all VLANs \n");
            VLAN_RELEASE_BUF (VLAN_PORT_ENTRY, (UINT1 *) pPortEntry);
            VLAN_GET_PORT_ENTRY (u2LocalPort) = NULL;
            return VLAN_FAILURE;
        }
    }
#endif

    pPortEntry->u2NextPortInd = VLAN_INVALID_PORT_INDEX;

    VLAN_GET_PORT_ENTRY (u2LocalPort) = pPortEntry;

    pBasePortEntry = VLAN_GET_BASE_PORT_ENTRY (u2LocalPort);

    if (pBasePortEntry == NULL)
    {
        pBasePortEntry = (tVlanBasePortEntry *) (VOID *) VLAN_GET_BUF
            (VLAN_BASE_PORT_ENTRY, sizeof (tVlanBasePortEntry));
        if (pBasePortEntry == NULL)
        {
            VLAN_RELEASE_BUF (VLAN_PORT_ENTRY, (UINT1 *) pPortEntry);
            VLAN_GET_PORT_ENTRY (u2LocalPort) = NULL;
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME, "No Memory for Base Port Entry creation\n");

            return VLAN_FAILURE;
        }
        pBasePortEntry->u4ContextId = u4ContextId;
        pBasePortEntry->u4IfIndex = u4IfIndex;
        pBasePortEntry->u4ComponentId = u4ComponentId;
        pBasePortEntry->u4BasePort = u2LocalPort;
        pBasePortEntry->u1IfType = CFA_GI_ENET;
        pBasePortEntry->u1RowStatus = ACTIVE;
        VLAN_GET_BASE_PORT_ENTRY (u2LocalPort) = pBasePortEntry;
    }

    if (VlanAddPortToIfIndexTable (pPortEntry) == VLAN_FAILURE)
    {
        VLAN_RELEASE_BUF (VLAN_PORT_ENTRY, (UINT1 *) pPortEntry);
        VLAN_GET_PORT_ENTRY (u2LocalPort) = NULL;
        return VLAN_FAILURE;
    }

#ifdef SYS_HC_WANTED
    if (CfaIfInfo.u4IfSpeed > VLAN_HC_PORT_SPEED)
    {
        pPortEntry->u1IsHCPort = VLAN_TRUE;
    }
    else
    {
        pPortEntry->u1IsHCPort = VLAN_FALSE;
    }
#endif

    pPortEntry->u1IfType = CfaIfInfo.u1IfType;

    /* In case of 1AD bridge, alloate mem for PbPortInfo. */
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        pPortEntry->pVlanPbPortEntry =
            (tVlanPbPortEntry *) (VOID *)
            VlanPbGetMemBlock (VLAN_PB_PORT_ENTRY_TYPE);

        if (pPortEntry->pVlanPbPortEntry == NULL)
        {
            VLAN_RELEASE_BUF (VLAN_PORT_ENTRY, (UINT1 *) pPortEntry);
            VLAN_GET_PORT_ENTRY (u2LocalPort) = NULL;
            return VLAN_FAILURE;
        }

        VLAN_MEMSET (pPortEntry->pVlanPbPortEntry, 0,
                     sizeof (tVlanPbPortEntry));
    }

    if (gpVlanContextInfo->u2VlanStartPortInd == VLAN_INVALID_PORT_INDEX)
    {
        /* 
         * This is the first port to be created. Set the start 
         * port index to this port 
         */
        gpVlanContextInfo->u2VlanStartPortInd = u2LocalPort;
    }
    else
    {
        /* Some ports exist before this */
        pLastPortEntry =
            VLAN_GET_PORT_ENTRY (gpVlanContextInfo->u2VlanLastPortInd);
        pLastPortEntry->u2NextPortInd = u2LocalPort;
    }
    gpVlanContextInfo->u2VlanLastPortInd = u2LocalPort;

    /* To set the default port parameters
     * for the port 
     */
    VlanSetDefPortInfo (u2LocalPort);
    /* Calling this function here because after setting DefPortInfo 
     * only we will get PVID AND ACCEPTABLE Frame Type for 
     * S-Channel Created */
    if (CfaIfInfo.u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        /* SBP port entry is already created for EVB module. Setting 
         * its local port alone has to be done. */
        if (VlanEvbSChIfUpdateLocalPort (u4IfIndex) == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                           VLAN_NAME, "Setting SBP %d local port failed\n",
                           u4IfIndex);
            VLAN_RELEASE_BUF (VLAN_PORT_ENTRY, (UINT1 *) pPortEntry);
            VLAN_GET_PORT_ENTRY (u2LocalPort) = NULL;
            return VLAN_FAILURE;
        }
    }
    /* Add a new port-isolation rule to allow traffic received on ICCL
     * interface to get forwarded on this newly created port.
     */

#ifdef NPAPI_WANTED
    VlanHwPortInfo.u4ContextId = u4ContextId;
    VlanHwPortInfo.u4IfIndex = u4IfIndex;

    if (CfaIfInfo.u1BrgPortType == CFA_UPLINK_ACCESS_PORT)
    {
        VlanHwPortInfo.u1BrgPortType = CFA_UPLINK_ACCESS_PORT;
        if (VlanHwSetBridgePortType (&VlanHwPortInfo) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC,
                           VLAN_NAME, "UAP port %d setting failed\n",
                           u4IfIndex);
            VLAN_RELEASE_BUF (VLAN_PORT_ENTRY, (UINT1 *) pPortEntry);
            VLAN_GET_PORT_ENTRY (u2LocalPort) = NULL;
            return VLAN_FAILURE;

        }
    }
    else if (CfaIfInfo.u1BrgPortType == CFA_CUSTOMER_BRIDGE_PORT)
    {
        VlanHwPortInfo.u1BrgPortType = CFA_CUSTOMER_BRIDGE_PORT;
        if (VlanHwSetBridgePortType (&VlanHwPortInfo) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC,
                           VLAN_NAME, "CBP port %d setting failed\n",
                           u4IfIndex);
            VLAN_RELEASE_BUF (VLAN_PORT_ENTRY, (UINT1 *) pPortEntry);
            VLAN_GET_PORT_ENTRY (u2LocalPort) = NULL;
            return VLAN_FAILURE;

        }
    }
#endif
    if ((VlanLaGetLaEnableStatus () == LA_ENABLED) &&
        (LA_MCLAG_ENABLED == VlanLaGetMCLAGSystemStatus ()))
    {
        MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));
        VlanIcchGetIcclIfIndex (&u4IcclIfIndex);

        if (u4IcclIfIndex != 0)
        {
            PortIsolation.InVlanId = VLAN_ZERO;
            PortIsolation.u4IngressPort = u4IcclIfIndex;
            PortIsolation.u2NumEgressPorts = 1;
            if (u4IfIndex != u4IcclIfIndex)
            {
                /* Add the port as a valid egress port for ICCL */
                gau4PortArrayEntry[0] = u4IfIndex;
                PortIsolation.pu4EgressPorts = gau4PortArrayEntry;
                PortIsolation.u1Action = ISS_PI_ADD;
                VlanIssApiUpdtPortIsolationEntry (&PortIsolation);
            }
        }
    }

    VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC | CONTROL_PLANE_TRC,
                   VLAN_NAME, "Port %d created successfully \n", u4IfIndex);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleDeletePort                             */
/*                                                                           */
/*    Description         : Deletes the specified port from VLAN Module.     */
/*                          by management module.                            */
/*                                                                           */
/*    Input(s)            : u2Port - The number of the port that is deleted. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHandleDeletePort (UINT2 u2Port)
{
    INT4                i4RetVal;
    UINT1               u1LldpFlag = VLAN_LLDP_PORT_NOTIFY_TRUE;
    UINT4               u4IfIndex = 0;
    INT4                i4IfBrgPortType = 0;

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleDeletePort function failed as module is shutdown \n");
        return VLAN_FAILURE;
    }

    i4RetVal = VlanDeletePortConfigurations (u2Port, u1LldpFlag);
    if (i4RetVal == VLAN_SUCCESS)
    {
        u4IfIndex = VLAN_GET_PORT_ENTRY (u2Port)->u4IfIndex;
        if (VlanCfaGetInterfaceBrgPortType (u4IfIndex,
                                            &i4IfBrgPortType) == VLAN_SUCCESS)
        {
            if (i4IfBrgPortType == CFA_UPLINK_ACCESS_PORT)
            {
                i4RetVal = VlanEvbUapIfDelEntry (u4IfIndex);
            }
            else if (i4IfBrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
            {
                i4RetVal = VlanEvbSchIfDelDynamicEntry (u4IfIndex);
            }
        }
        i4RetVal = VlanDeletePortTables (u2Port);
    }
    return (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeletePortTables                             */
/*                                                                           */
/*    Description         : Deletes the specified porttables from VLAN Module*/
/*                                                                           */
/*    Input(s)            : u2Port - The number of the port that is deleted. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : gpVlanContextInfo->u2VlanStartPortInd,     */
/*                                gpVlanContextInfo->u2VlanLastPortInd       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDeletePortTables (UINT2 u2Port)
{
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanBasePortEntry *pBasePortEntry = NULL;
    tIssUpdtPortIsolation PortIsolation;
    UINT4               u4IcclIfIndex = 0;
    UINT4               au4PortArray[VLAN_PORT_LIST_SIZE];
    BOOL1               bPortFound = VLAN_FALSE;
    UINT2               u2PortIndex = 0;
    UINT2               u2PrePortIndex = VLAN_INVALID_PORT_INDEX;

    MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    /* Set the gpVlanContextInfo->u2VlanStartPortInd and 
       gpVlanContextInfo->u2VlanLastPortInd to appropriate values */

    if (u2Port == gpVlanContextInfo->u2VlanStartPortInd)
    {

        gpVlanContextInfo->u2VlanStartPortInd = pPortEntry->u2NextPortInd;

        /* for only one entry in the port table.
         * make last port index to start port index which is an invalid
         * index */

        if (u2Port == gpVlanContextInfo->u2VlanLastPortInd)
        {

            gpVlanContextInfo->u2VlanLastPortInd =
                gpVlanContextInfo->u2VlanStartPortInd;
        }
    }
    else
    {
        /* This port is either in the last or in the middle */
        VLAN_SCAN_PORT_TABLE (u2PortIndex)
        {
            if (u2PortIndex == u2Port)
            {
                bPortFound = VLAN_TRUE;
                break;
            }
            else
            {
                u2PrePortIndex = u2PortIndex;
            }
        }

        if (bPortFound == VLAN_FALSE)
        {
            return VLAN_FAILURE;
        }

        if (u2PrePortIndex >= VLAN_MAX_PORTS + 1)
        {
            return VLAN_FAILURE;
        }
        VLAN_GET_NEXT_PORT_INDEX (u2PrePortIndex) = pPortEntry->u2NextPortInd;

        if (u2Port == gpVlanContextInfo->u2VlanLastPortInd)
        {
            gpVlanContextInfo->u2VlanLastPortInd = u2PrePortIndex;
        }
    }

    if ((VlanLaGetLaEnableStatus () == LA_ENABLED) &&
        (LA_MCLAG_ENABLED == VlanLaGetMCLAGSystemStatus ()))
    {
        MEMSET (&PortIsolation, 0, sizeof (tIssUpdtPortIsolation));
        VlanIcchGetIcclIfIndex (&u4IcclIfIndex);

        PortIsolation.InVlanId = VLAN_ZERO;
        PortIsolation.u4IngressPort = u4IcclIfIndex;
        PortIsolation.u2NumEgressPorts = 1;
        au4PortArray[0] = VLAN_GET_IFINDEX (u2Port);
        PortIsolation.pu4EgressPorts = au4PortArray;
        PortIsolation.u1Action = ISS_PI_DELETE;
        VlanIssApiUpdtPortIsolationEntry (&PortIsolation);
    }

    pPortEntry->u2NextPortInd = VLAN_INVALID_PORT_INDEX;

    VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC | CONTROL_PLANE_TRC,
                   VLAN_NAME, "Port %d deleted successfully \n",
                   pPortEntry->u4IfIndex);

    VlanResetPortTnlStatusAndRelPbPortMem (u2Port);

    VlanRemovePortFromIfIndexTable (pPortEntry);
    VlanDeletePortInfo (u2Port);

    if (u2Port < VLAN_MAX_PORTS + 1)
    {
        pBasePortEntry = VLAN_GET_BASE_PORT_ENTRY (u2Port);

        if (pBasePortEntry != NULL)
        {
            VLAN_RELEASE_BUF (VLAN_BASE_PORT_ENTRY, (UINT1 *) pBasePortEntry);
            VLAN_GET_BASE_PORT_ENTRY (u2Port) = NULL;
        }
    }
    VLAN_RELEASE_BUF (VLAN_PORT_ENTRY, (UINT1 *) pPortEntry);
    VLAN_GET_PORT_ENTRY (u2Port) = NULL;

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeletePortConfigurations                     */
/*                                                                           */
/*    Description         : Deletes the Port Related Configurations from VLAN */
/*                                                                           */
/*    Input(s)            : u2Port - The number of the port that is deleted. */
/*                          u1LldpFlg - LLDP notification flag               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDeletePortConfigurations (UINT2 u2Port, UINT1 u1LldpFlg)
{
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
#ifdef NPAPI_WANTED
    UINT1               u1HwDelDefVlan = VLAN_TRUE;
#endif
    tHwVlanPortProperty VlanPortProperty;
    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        /* Port number exceeds the allowed range */

        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "DeletePortConfigurations for invalid port number \n");

        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {

        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Delete Port for invalid port number \n");

        return VLAN_SUCCESS;
    }

#ifndef NPAPI_WANTED
    if (VLAN_IS_PRIORITY_ENABLED () == VLAN_TRUE)
    {
        VlanFreePriorityQ (pPortEntry);
    }
#endif

    /*
     * Check whether this port is a trunk port and
     * delete the updated information for the VLAN
     */
    if (pPortEntry->u1PortType == VLAN_TRUNK_PORT)
    {
        VlanDeleteTrunkPortFromExistingVlan (u2Port);
        /* Sending the trigger to MSR to set the default port type 
         * for the PBB ports */
        if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
        {
            nmhSetDot1qFutureVlanPortType (u2Port, VLAN_HYBRID_PORT);
        }
    }
    /*
     * Setting the port type to default, to avoid reflection
     * of it during updation of VLAN information
     */
    pPortEntry->u1PortType = VLAN_DEFAULT_PORT;

    if ((VlanCfaGetIvrStatus () == CFA_ENABLED) &&
        (pPortEntry->u1OperStatus == VLAN_OPER_UP))
    {
        VlanIvrUpdateVlanIfOperStatus (u2Port, VLAN_OPER_DOWN);
    }

#ifdef NPAPI_WANTED
    if (VLAN_IS_PORT_CHANNEL (pPortEntry) != VLAN_TRUE)
    {
        VlanHandleRemovePortPropertiesFromHw (VLAN_GET_PHY_PORT (u2Port),
                                              u2Port, u1HwDelDefVlan);
        VlanHandleRemovePortUcastPropertiesFromHw (u2Port);
        VlanHandleRemovePortMcastPropertiesFromHw (u2Port);
    }
#endif /* NPAPI_WANTED */

    if (VlanHwSetPortIngressEtherType (VLAN_CURR_CONTEXT_ID (),
                                       VLAN_GET_PHY_PORT (u2Port),
                                       VLAN_PROTOCOL_ID) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                       VLAN_NAME, "Setting Ingress ethertype failed."
                       "Port %d\n", u2Port);
        return VLAN_FAILURE;
    }

    if (VlanHwSetPortEgressEtherType (VLAN_CURR_CONTEXT_ID (),
                                      VLAN_GET_PHY_PORT (u2Port),
                                      VLAN_PROTOCOL_ID) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                       VLAN_NAME, "Setting Egress ethertype failed."
                       "Port %d\n", u2Port);
        return VLAN_FAILURE;
    }

    /* Set Egress TPID to default when Bridge Port-type is changed */
    VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_EGRESS_TPID_TYPE;
    VlanPortProperty.u1EgressTPIDType = VLAN_PORT_EGRESS_PORTBASED;
    if (VlanHwSetPortProperty
        (VLAN_CURR_CONTEXT_ID (), VLAN_GET_PHY_PORT (u2Port),
         VlanPortProperty) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanRemovePortPropertiesFromHw (): "
                       "Setting Egress TPID Type failed for Port %d \n",
                       u2Port);
        return VLAN_FAILURE;
    }

    /* The following 3 TPID settings are assigned to ZERO because 
     * no h/w call is necessary as setting the IngressEthertype will
     * reset all the configured TPIDs in h/w */
    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));

    VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID1;
    VlanPortProperty.u2Flag = VLAN_DELETE;
    VlanPortProperty.u2AllowableTPID1 = 0;
    if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                               VLAN_GET_PHY_PORT (u2Port),
                               VlanPortProperty) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanRemovePortPropertiesFromHw (): "
                       "Deleting Allowable TPID1 failed for Port %d \n",
                       u2Port);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));

    VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID2;
    VlanPortProperty.u2Flag = VLAN_DELETE;
    VlanPortProperty.u2AllowableTPID2 = 0;
    if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                               VLAN_GET_PHY_PORT (u2Port),
                               VlanPortProperty) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanRemovePortPropertiesFromHw (): "
                       "Deleting Allowable TPID2 failed for Port %d \n",
                       u2Port);
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));

    VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_ALLOWABLE_TPID3;
    VlanPortProperty.u2Flag = VLAN_DELETE;
    VlanPortProperty.u2AllowableTPID3 = 0;
    if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                               VLAN_GET_PHY_PORT (u2Port),
                               VlanPortProperty) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanRemovePortPropertiesFromHw (): "
                       "Deleting Allowable TPID1 failed for Port %d \n",
                       u2Port);
        return VLAN_FAILURE;
    }

    /* Release the vid set entries in the port
     */
    VLAN_SLL_SCAN (&pPortEntry->VlanVidSet,
                   pVlanPortVidSetEntry, tVlanPortVidSet *)
    {

        VLAN_SLL_DEL (&pPortEntry->VlanVidSet,
                      (tTMO_SLL_NODE *) & (pVlanPortVidSetEntry->NextNode));
        VLAN_RELEASE_BUF (VLAN_PORT_PROTO_ENTRY,
                          (UINT1 *) pVlanPortVidSetEntry);
        pVlanPortVidSetEntry = NULL;
    }
    VLAN_SLL_INIT (&pPortEntry->VlanVidSet);

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        /* Apart from CVID registration table entries, remove 
         * the other SVLAN classification entries from HW.
         */
        VlanPbRemoveSVlanClassificationEntriesForPort (u2Port);

        if (pPortEntry->u1OperStatus == VLAN_OPER_UP)
        {
            /*This function removes PEP and CVID ENtries from H/w */
            VlanPbNpUpdateCvidAndPepInHw (u2Port, 0, 0, VLAN_DELETE);
        }
        /*This function removes PEP and CVID ENtries from S/w and indicate STP
         * to delete PEP*/
        VlanPbRemovePortCVidEntries (u2Port);
        VlanPbRemoveSVlanTranslationEntriesForPort (u2Port);
        VlanPbRemoveEtherTypeSwapEntriesForPort (u2Port);
        /* This function Resets the PB related Port Properties in 
         * Hardware. 
         */
        VlanPbHandleResetPbPortPropertiesToHw (u2Port);
        /* Reset all the Protocol Tunnel Status to Peer for the Port 
         */

        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                             u2Port, VLAN_NP_DOT1X_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_LACP_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_STP_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_GVRP_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_GMRP_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_IGMP_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_MVRP_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_MMRP_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_ELMI_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_LLDP_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_ECFM_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);
        VlanHwSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                             VLAN_NP_EOAM_PROTO_ID,
                                             VLAN_TUNNEL_PROTOCOL_PEER);

        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_DOT1X,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_LACP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_STP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_GVRP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_GMRP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_IGMP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
    }

    pPortEntry->u1OperStatus = VLAN_OPER_DOWN;

    VlanDeleteMacMapEntryOnPort ((UINT4) u2Port);

    VlanDeleteSubnetMapEntryOnPort ((UINT4) u2Port);

#ifndef NPAPI_WANTED
    if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
    {
        VlanDelPortStatsFromAllVlans (u2Port);
    }
#endif

    /* Port and Mac based support for port is disabled
     * and the memory is freed if the port protocol list is present
     */
    VLAN_PORT_PORT_PROTOCOL_BASED (u2Port) = VLAN_SNMP_FALSE;
    VLAN_PORT_MAC_BASED (u2Port) = VLAN_DISABLED;
    VLAN_SET_PORT_SUBNET_BASED (u2Port, VLAN_DISABLED);
    if (u1LldpFlg == VLAN_LLDP_PORT_NOTIFY_TRUE)
    {
        /* Send the notification to LLDP to notify 
         *  the status change on the port.*/
        VlanLldpApiNotifyProtoVlanStatus (u2Port, VLAN_SNMP_FALSE);
    }

    /* Delete all the Port-Vlan-Context Entries associated for this
     * port */
    VlanVcmSispDeleteAllPortVlanMapEntriesForPort (VLAN_GET_IFINDEX (u2Port));

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanResetPortTnlStatusAndRelPbPortMem            */
/*                                                                           */
/*    Description         : Resets the port tunnel status in L2Iwf and then  */
/*                          release the PbPort entry memory.                 */
/*                                                                           */
/*    Input(s)            : u2Port - Number of the port.                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
VlanResetPortTnlStatusAndRelPbPortMem (UINT2 u2Port)
{
    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        return;
    }
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        if (VlanL2IwfIsPortInPortChannel (VLAN_GET_IFINDEX (u2Port))
            == L2IWF_SUCCESS)
        {
            /* When a port is set part of a port-channel, then 
               the port will be deleted from vlan, AST. But the 
               port will be in L2Iwf, La and Pnac. So set the LACP,
               dot1x tunnel status to be peer on this port.
               Othewise dot1x, LACP will not be peered on this port.
             */
            VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                    u2Port, L2_PROTO_DOT1X,
                                                    VLAN_TUNNEL_PROTOCOL_PEER);
            VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                    u2Port, L2_PROTO_LACP,
                                                    VLAN_TUNNEL_PROTOCOL_PEER);
        }

        VlanPbReleaseMemBlock (VLAN_PB_PORT_ENTRY_TYPE,
                               (UINT1 *) VLAN_GET_PB_PORT_ENTRY (u2Port));
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleDelPortAndCopyProperties               */
/*                                                                           */
/*    Description         : Deletes the specified port from VLAN Module.     */
/*                          and program the port-channel properties for that */
/*                          port in H/W.                                     */
/*                                                                           */
/*    Input(s)            : u2Port - The number of the port that is deleted. */
/*                          u2PoIndex - PortChannel Index                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHandleDelPortAndCopyProperties (UINT2 u2Port, UINT2 u2PoIndex)
{

#ifdef NPAPI_WANTED
    tFsNpVlanPortReflectEntry FsNpVlanPortReflectEntry;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ReflectionStatus = VLAN_DISABLED;
    UINT4               u4IfIndex = 0;
#endif
    INT4                i4RetVal;
    UINT1               u1LldpFlag = VLAN_LLDP_PORT_NOTIFY_TRUE;
    UINT1               u1InterfaceType = VLAN_INVALID_INTERFACE_TYPE;

#ifdef NPAPI_WANTED
    VLAN_MEMSET (&FsNpVlanPortReflectEntry, 0,
                 sizeof (tFsNpVlanPortReflectEntry));
#endif

    if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleDelPortAndCopyProperties function"
                  "failed as module is shutdown \n");
        return VLAN_FAILURE;
    }

    i4RetVal = VlanDeletePortConfigurations (u2Port, u1LldpFlag);

#ifdef NPAPI_WANTED
    /*Get the VLAN port entry for the port channel */
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2PoIndex);

    u4IfIndex = (UINT4) VLAN_GET_PHY_PORT (u2Port);

    if (pVlanPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Unable to fetch port entry for the port " "%d \r\n");

    }
    /* Get the reflection status of the port-channel */
    else
    {
        u4ReflectionStatus = (UINT4) pVlanPortEntry->i4ReflectionStatus;
    }

    /*If a port is added to a port-channel,then set the
       reflection status of the port similar to the port-channel */

    FsNpVlanPortReflectEntry.u4IfIndex = u4IfIndex;
    FsNpVlanPortReflectEntry.u4Status = u4ReflectionStatus;
    FsNpVlanPortReflectEntry.u4ContextId = VLAN_DEFAULT_CONTEXT_ID;

    if (FNP_FAILURE ==
        VlanFsMiVlanHwPortPktReflectStatus (&FsNpVlanPortReflectEntry))
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Unable to set port reflection for the port "
                  "%d in hardware \r\n");

    }

#endif

    if (i4RetVal == VLAN_SUCCESS)
    {
#ifdef NPAPI_WANTED
#ifndef LA_HW_TRUNK_SUPPORTED
        VlanL2IwfGetInterfaceType (VLAN_CURR_CONTEXT_ID (), &u1InterfaceType);

        i4RetVal = VlanHandleCopyPortPropertiesToHw (u2Port, u2PoIndex,
                                                     u1InterfaceType);
#else
        UNUSED_PARAM (u2PoIndex);
        UNUSED_PARAM (u1InterfaceType);
#endif
#else
        UNUSED_PARAM (u2PoIndex);
        UNUSED_PARAM (u1InterfaceType);
#endif
        i4RetVal = VlanDeletePortTables (u2Port);
    }

    return (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandlePortOperInd()                          */
/*                                                                           */
/*    Description         : Invoked  whenever Port Oper Status change Msg is */
/*                                                                           */
/*    Input(s)            : u2Port    - The Number of the Port               */
/*                          u1OperStatus - VLAN_OPER_UP / VLAN_OPER_DOWN     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHandlePortOperInd (UINT2 u2Port, UINT1 u1OperStatus)
{
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanPbPortEntry   *pPortPbEntry = NULL;
#ifdef NPAPI_WANTED
    UINT4               u4IcclIfIndex = 0;
#endif
#if (defined NPAPI_WANTED) && (defined RSTP_WANTED)
    UINT1               u1Status;
#endif
#ifdef ICCH_WANTED
    UINT1               u1IsMclagEnabled = OSIX_FALSE;

    UNUSED_PARAM (u1IsMclagEnabled);
#endif
    UINT4               u4IfIndex = 0;
    INT4                i4IfBrgPortType = 0;

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        return VLAN_FAILURE;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Port Oper Indication for invalid port. \n");

        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        /* Port not created */
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Port not created. \n");
        return VLAN_FAILURE;
    }

    if (pPortEntry->u1OperStatus == u1OperStatus)
    {
        /* No change in oper status...ignore it */
        return VLAN_SUCCESS;
    }

    if (u1OperStatus == VLAN_OPER_UP)
    {
        /*
         * GARP Application specific port entries wud have been created 
         * (if applications are enabled) if and if the port is operationally UP.
         * So propagate the static registrations on this port to GVRP/GMRP.
         */
        VlanPropagateNewPortInfo (u2Port);
    }

    pPortEntry->u1OperStatus = u1OperStatus;

    if (VlanCfaGetIvrStatus () == VLAN_ENABLED)
    {
        VlanIvrUpdateVlanIfOperStatus (u2Port, u1OperStatus);
    }
    pPortPbEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    if (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        if (pPortPbEntry != NULL)
        {
            if (pPortPbEntry->u1PbPortType == VLAN_CUSTOMER_EDGE_PORT)
            {
                /* Update the PEP oper status based on the oper status 
                 * of CEP in Provider Edge Bridges
                 */
                VlanPbNotifyCepOperStatusForPep (u2Port, u1OperStatus);
            }
        }
    }

    if (VLAN_IS_PORT_CHANNEL (pPortEntry) == VLAN_TRUE)
    {
        if (u1OperStatus == VLAN_OPER_UP)
        {
            VlanHandleCopyPortUcastPropertiesToHw (u2Port);
            VlanHandleCopyPortMcastPropertiesToHw (u2Port);
        }
        else
        {
            VlanHandleRemovePortUcastPropertiesFromHw (u2Port);
            VlanHandleRemovePortMcastPropertiesFromHw (u2Port);
        }
    }

#ifdef NPAPI_WANTED

#ifndef RSTP_WANTED
    /* Flush all the fdb Entries learnt in this port */
    if (u1OperStatus == VLAN_OPER_DOWN)
    {
#ifdef ICCH_WANTED
        /* 
         * Dont Delete the FDB entries for MC-LAG PortChannel since this will 
         * be seperately taken care when MC-LAG is enabled and PEER is 
         * available. 
         */
        if (VLAN_IS_PORT_CHANNEL (pPortEntry) == VLAN_TRUE)
        {
            LaApiIsMclagInterface (pPortEntry->u4IfIndex, &u1IsMclagEnabled);
            if ((u1IsMclagEnabled == OSIX_TRUE) &&
                (IcchGetPeerNodeState () == ICCH_PEER_UP))
            {
                return VLAN_SUCCESS;
            }
        }

        VlanIcchGetIcclIfIndex (&u4IcclIfIndex);
#endif
        if (VLAN_GET_PHY_PORT (u2Port) != u4IcclIfIndex)
        {
            VlanHwFlushPort (VLAN_CURR_CONTEXT_ID (), u2Port, VLAN_OPTIMIZE);
        }
    }
#else
    if ((IssGetAutoPortCreateFlag () == ISS_DISABLE))
    {
        if ((VlanL2IwfGetProtocolEnabledStatusOnPort
             ((UINT2) (VLAN_GET_IFINDEX (u2Port)), L2_PROTO_STP,
              &u1Status) == L2IWF_SUCCESS) && (u1Status == OSIX_DISABLED))
        {
            VlanHwFlushPort (VLAN_CURR_CONTEXT_ID (), u2Port, VLAN_OPTIMIZE);
        }
    }
#endif
#endif
    u4IfIndex = VLAN_GET_PORT_ENTRY (u2Port)->u4IfIndex;

    if (VlanCfaGetInterfaceBrgPortType (u4IfIndex, &i4IfBrgPortType)
        == VLAN_SUCCESS)
    {
        if (i4IfBrgPortType == CFA_UPLINK_ACCESS_PORT)
        {
            if (VlanEvbUapSetOperStatus (u4IfIndex, u1OperStatus)
                == VLAN_FAILURE)
            {
                VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "Port Oper setting %d for UAP port %d is failed. \n",
                               u1OperStatus, u4IfIndex);
                /* No failure */
            }
        }
        else if (i4IfBrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
        {
            /* There is no oper status for SBP and admin status only
             * can be set from CFA. It is applied to the SBP port in 
             * EVB */
            if (VlanEvbSChSetAdminStatus (u4IfIndex, u1OperStatus)
                == VLAN_FAILURE)
            {
                VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "Port Status setting %d for SBP port %d is failed. \n",
                               u1OperStatus, u4IfIndex);
                /* No failure */
            }
        }
    }

#ifdef NPAPI_WANTED
    UNUSED_PARAM (u4IcclIfIndex);
#endif

    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name    : VlanTaskInit                                      */
/*                                                                      */
/* Description      : Creates Vlan Task Queue, Config Queue and Sema4   */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : VLAN_SUCCESS / VLAN_FAILURE                       */
/************************************************************************/
INT4
VlanTaskInit (VOID)
{

    VLAN_MEMSET (&(gVlanTaskInfo), 0, sizeof (tVlanTaskInfo));

    VLAN_MEMSET (&gFsVlanSizingInfo, 0, sizeof (tFsModSizingInfo));
    VLAN_MEMCPY (gFsVlanSizingInfo.ModName, "VLAN", STRLEN ("VLAN"));
    gFsVlanSizingInfo.u4ModMemPreAllocated = 0;
    gFsVlanSizingInfo.ModSizingParams = gFsVlanSizingParams;

    FsUtlSzCalculateModulePreAllocatedMemory (&gFsVlanSizingInfo);

    if (VLAN_CREATE_SEM (VLAN_SEM_NAME, 1, 0, &(VLAN_SEM_ID)) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC),
                         "MSG: Failed to Create VLAN Mutual Exclusion Sema4 \r\n");
        return VLAN_FAILURE;
    }

    if (VLAN_CREATE_QUEUE (VLAN_TASK_QUEUE, OSIX_MAX_Q_MSG_LEN, VLAN_Q_DEPTH,
                           &(VLAN_TASK_QUEUE_ID)) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC),
                         "MSG: Failed to create queue for VLAN Task\r\n");

        VLAN_DELETE_SEM (VLAN_SEM_ID);
        return VLAN_FAILURE;
    }

    if (VLAN_CREATE_QUEUE (VLAN_CFG_QUEUE, OSIX_MAX_Q_MSG_LEN,
                           VLAN_CFG_Q_DEPTH,
                           &(VLAN_CFG_QUEUE_ID)) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC),
                         "MSG: Failed to create queue for VLAN Config\r\n");

        VLAN_DELETE_QUEUE (VLAN_TASK_QUEUE_ID);
        VLAN_DELETE_SEM (VLAN_SEM_ID);
        return VLAN_FAILURE;
    }
    /*Creating all MemPools in VLAN module */
    if (VlanSizingMemCreateMemPools () != OSIX_SUCCESS)
    {

        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, INIT_SHUT_TRC,
                         "Mem Pool creation Failed \n");
        VLAN_DELETE_QUEUE (VLAN_TASK_QUEUE_ID);
        VLAN_DELETE_QUEUE (VLAN_CFG_QUEUE_ID);
        VLAN_DELETE_SEM (VLAN_SEM_ID);
        VlanSizingMemDeleteMemPools ();
        return VLAN_FAILURE;
    }
#ifdef L2RED_WANTED
    /*Creating all MemPools in VLAN module */
    if (VlanredSizingMemCreateMemPools () != OSIX_SUCCESS)
    {

        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, INIT_SHUT_TRC,
                         "Mem Pool creation Failed \n");
        VLAN_DELETE_QUEUE (VLAN_TASK_QUEUE_ID);
        VLAN_DELETE_QUEUE (VLAN_CFG_QUEUE_ID);
        VLAN_DELETE_SEM (VLAN_SEM_ID);
        VlanSizingMemDeleteMemPools ();
        return VLAN_FAILURE;
    }
#endif /* L2RED_WANTED */
    /*Assigning Mempool Ids */
    VlanAssignMempoolIds ();

    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name    : VlanMainDeInit                                    */
/*                                                                      */
/* Description      : Deletes Vlan Task Queue, Config Queue, Sema4,     */
/*                    Memory pools and global informations              */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None.                                             */
/************************************************************************/
VOID
VlanMainDeInit (VOID)
{
    /* Since the default context alone is created at the start up , 
     * this is to be deleted. */
    VlanEvbShutdown (VLAN_DEF_CONTEXT_ID);
    gu1IsVlanInitialised = VLAN_FALSE;
    VlanDeInitGlobalInfo ();
    VlanSizingMemDeleteMemPools ();
#ifdef L2RED_WANTED
    VlanredSizingMemDeleteMemPools ();
#endif /* L2RED_WANTED */
    VLAN_DELETE_QUEUE (VLAN_CFG_QUEUE_ID);
    VLAN_DELETE_QUEUE (VLAN_TASK_QUEUE_ID);
    VLAN_DELETE_SEM (VLAN_SEM_ID);
    if (gVlanTaskInfo.VlanTmrListId != 0)
    {
        VLAN_DELETE_TMR_LIST (gVlanTaskInfo.VlanTmrListId);
    }
#ifdef SW_LEARNING
    VLAN_DELETE_SEM (VLAN_SHADOW_TBL_SEM_ID);
#endif
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanInit                                         */
/*                                                                           */
/*    Description         : Initialises the vlan tables and creates memory   */
/*                          pool for all the tables.                         */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanInit ()
{
    UINT4               u4AgeOutInt;
    INT4                i4RetVal;
    UINT2               u2Index;
    UINT4               u4BridgeMode = VLAN_INVALID_BRIDGE_MODE;
    UINT1               u1Priority;
    UINT1               u1ProtocolId;
    UINT2               u2InterfaceType = VLAN_INVALID_INTERFACE_TYPE;
    UINT1               u1NodeStatus = VLAN_NODE_ACTIVE;
    tMacAddr            MacAddr;

    if (VLAN_MODULE_STATUS () == VLAN_ENABLED)
    {
        return VLAN_SUCCESS;
    }

    /* Initialise gpVlanContextInfo->VlanInfo structure */

    MEMSET (&gpVlanContextInfo->VlanInfo, 0, sizeof (tVlanInfo));

    /*Bridge Mode is get from the L2IWF module. In SI case the Bridge
     * Mode cannot be invalid. whereas in MI case the VLAN module
     * shall not be initialised without configuring the Bridge Mode in
     * L2IWF*/
    if (VlanL2IwfGetBridgeMode (VLAN_CURR_CONTEXT_ID (), &u4BridgeMode) !=
        L2IWF_SUCCESS)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "Getting the Bridge Mode from L2IWF Failed \n");
        return VLAN_FAILURE;
    }

    VLAN_BRIDGE_MODE () = u4BridgeMode;

    /*In SI/MI, here Bridge Mode should not be invalid */
    if (VLAN_BRIDGE_MODE () == VLAN_INVALID_BRIDGE_MODE)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "Bridge Mode is Invalid, so Initialisation of VLAN"
                  "Module failed !!!!!!!\r\n");
        return VLAN_FAILURE;
    }

    /* Starting VLAN ageing timer */
    u4AgeOutInt = VlanL2IwfGetAgeoutInt ();

    VlanL2IwfStartHwAgeTimer (VLAN_CURR_CONTEXT_ID (), u4AgeOutInt);
    /* Starting VLAN ageing timer - VLAN and GARP are enabled by default */

    u4AgeOutInt = VLAN_SPLIT_AGEOUT_TIME (u4AgeOutInt);

    VLAN_START_TIMER (VLAN_AGEOUT_TIMER, u4AgeOutInt);

    i4RetVal = VlanHwInit (VLAN_CURR_CONTEXT_ID ());

    if (i4RetVal == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "Hardware Initialisation Failed \n");

        VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
        return VLAN_FAILURE;
    }

    /* Cfa has already read the Default Vlan Id read from issnvram.txt
     * and updated L2Iwf.Vlan gets the default vlan id from L2Iwf.
     */
    L2IwfGetDefaultVlanId (&gVlanDefVlanId);

    i4RetVal = VlanHwSetBrgMode (VLAN_CURR_CONTEXT_ID (), VLAN_BRIDGE_MODE ());

    /* Also we do the following:
     * - update the np global variable (which the npapi routines will refer 
     *   for default vlan id) with the default vlan id read from L2Iwf.
     * - Update the hw with the proper default vlan id.
     */
    VlanHwSetDefaultVlanId (VLAN_CURR_CONTEXT_ID (), VLAN_DEF_VLAN_ID);

    if (i4RetVal == VLAN_FAILURE)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "Configuration of Bridge Mode failed \n");
        VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
        return VLAN_FAILURE;
    }

    /* Set the Administrator Interface Type Flag as FALSE 
     * This Flag will be set to TRUE when the operator
     * sets a port type of any port in I COmponent
     */
    VlanL2IwfSetAdminIntfTypeFlag (VLAN_CURR_CONTEXT_ID (), CFA_FALSE);

    if (u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE)
    {
        u2InterfaceType = VLAN_S_INTERFACE_TYPE;
    }

    /* Set the Interface Type in L2IWF */
    VlanL2IwfSetInterfaceType (VLAN_CURR_CONTEXT_ID (), u2InterfaceType);

    /* set the default traffic class to priority mapping */
    for (u1Priority = 0; u1Priority < VLAN_MAX_PRIORITY; u1Priority++)
    {
        i4RetVal = VlanHwTraffClassMapInit
            (VLAN_CURR_CONTEXT_ID (), u1Priority,
             gau1PriTrfClassMap[u1Priority][VLAN_MAX_TRAFF_CLASS - 1]);

        if (i4RetVal == VLAN_FAILURE)
        {
            VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                      "default traffic class map initialization failed \n");

            VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
            return VLAN_FAILURE;
        }
    }
    gpVlanContextInfo->VlanStaticUnicastInfo =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanStUcastEntry, RBNode),
                              VlanStaticUnicastTableCmp);

    if (gpVlanContextInfo->VlanStaticUnicastInfo == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "VLAN Static Unicast RB Tree creation Failed \n");
        VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
        return VLAN_FAILURE;
    }

#ifdef SW_LEARNING
    gpVlanContextInfo->VlanFdbInfo =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanFdbInfo, RBNode),
                              VlanFdbTableCmp);

    if (gpVlanContextInfo->VlanFdbInfo == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "VLAN FDB RB Tree creation Failed \n");
        VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
        return VLAN_FAILURE;
    }
#ifdef MBSM_WANTED
    /* This RB Tree is used in standby to hold the already learnt FDB
     * entries sent from Active (when standby is down) during initialization.
     * Once initialization is completed, the RB Tree entries will be 
     * updated in VLAN mac address table */
    gVlanFDBInfo =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanFdbInfo, RBNode),
                              VlanFdbTableCmp);

    if (gVlanFDBInfo == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "VLAN FDB Info RB Tree creation Failed \n");
        VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
        return VLAN_FAILURE;
    }
#endif /* MBSM_WANTED */
#endif /* SW_LEARNING */

    /* Init VLAN Current table */
    MEMSET (gpVlanContextInfo->apVlanCurrTable, 0,
            sizeof (gpVlanContextInfo->apVlanCurrTable));

    VLAN_START_VLAN_INDEX () = VLAN_INVALID_VLAN_INDEX;

    MEMSET (gpVlanContextInfo->apVlanFidTable, 0,
            sizeof (tVlanFidEntry *) * VLAN_MAX_FID_ENTRIES);

    /*
     * Initialise Port table first since other functions can access it.
     * e.g VlanEnablePriorityModule will access the VLAN port table.
     */
    i4RetVal = VlanPortTblInit ();

    VLAN_MODULE_ADMIN_STATUS () = VLAN_ENABLED;    /* Admin status of
                                                   the VLAN module enabled
                                                   by default */
    VLAN_MODULE_STATUS () = VLAN_DISABLED;

    VLAN_GVRP_STATUS () =
        (UINT1) VlanGvrpIsGvrpEnabledInContext (VLAN_CURR_CONTEXT_ID ());
    VLAN_GMRP_STATUS () =
        (UINT1) VlanGmrpIsGmrpEnabledInContext (VLAN_CURR_CONTEXT_ID ());

    gpVlanContextInfo->VlanInfo.u4NextFreeFdbId = VLAN_SHARED_DEF_FDBID;
    gpVlanContextInfo->VlanInfo.u1DefConstType = VLAN_DEF_CONSTRAINT_TYPE;
    gpVlanContextInfo->VlanInfo.u1VlanVersion = VLAN_VERSION;
    gpVlanContextInfo->VlanInfo.MaxVlanId = VLAN_MAX_VLAN_ID;
    gpVlanContextInfo->VlanInfo.u4MaxVlans = VLAN_DEV_MAX_NUM_VLAN;

    gpVlanContextInfo->VlanInfo.u1VlanLearningType = VLAN_INDEP_LEARNING;

    VlanL2IwfSetVlanLearningType (VLAN_CURR_CONTEXT_ID (), VLAN_INDEP_LEARNING);

    gpVlanContextInfo->VlanInfo.u4SwitchMulticastLimit =
        VLAN_DEV_MAX_MCAST_TABLE_SIZE;

    gpVlanContextInfo->VlanInfo.u4SwitchStaticUnicastCount = VLAN_INIT_VAL;
    gpVlanContextInfo->bRemoteFdbFlush = VLAN_FALSE;

    /* Enable enhanced filtering in the switch context and update l2iwf */
    VLAN_CURR_CONTEXT_PTR ()->u1ApplyEnhancedFilteringCriteria = VLAN_SNMP_TRUE;
    VlanL2IwfUpdateEnhFilterStatus (VLAN_CURR_CONTEXT_ID (), VLAN_SNMP_TRUE);

    VLAN_DYNAMIC_UNICAST_SIZE = VLAN_DEF_UNICAST_LEARNING_LIMIT;

    gau1VlanShutDownStatus[gpVlanContextInfo->u4ContextId] = VLAN_SNMP_FALSE;

    VLAN_PORT_PROTO_BASED = VLAN_SNMP_TRUE;
    VlanLldpApiNotifyProtoVlanStatus (0, VLAN_SNMP_TRUE);
    VLAN_MAC_BASED = VLAN_SNMP_FALSE;
    VLAN_SUBNET_BASED = VLAN_SNMP_FALSE;

    /* Initialize the Fdb Id for VLAN's in L2Iwf module. */
    /*By default the learing mode is taken as IVL and all
       vlans are mapped accordingly in L2Iwf module */
    for (u2Index = 1; u2Index <= VLAN_MAX_VLAN_ID; u2Index++)
    {
        VlanL2IwfSetVlanFdbId (VLAN_CURR_CONTEXT_ID (), u2Index, u2Index);
    }

    VLAN_TUNNEL_BPDU_PRIORITY () = VLAN_DEF_TUNNEL_BPDU_PRIORITY;

    gpVlanContextInfo->pStaticVlanTable = NULL;

    VLAN_SLL_INIT (&gpVlanContextInfo->WildCardTable);

    MEMSET (gNullPortList, 0, sizeof (tLocalPortList));

    /* Initialise all Hash Tables */
    /* Creating the hash table for the protocol group table */
    VLAN_PROTOCOL_GROUP_TBL =
        VLAN_HASH_CREATE_TABLE (VLAN_MAX_BUCKETS, NULL, FALSE);

    if (VLAN_PROTOCOL_GROUP_TBL == NULL)
    {
        /* mem pool creation failed */
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC,
                  VLAN_NAME, "Creation of Protocol Group Table Failed \n");
        VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
        return VLAN_FAILURE;
    }
    VLAN_INIT_HASH_TABLE (gpVlanContextInfo->VlanMacMapTable);

    gpVlanContextInfo->VlanSubnetMapTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanSubnetMapEntry, RBNode),
                              VlanSubnetMapTableCmp);

    if (gpVlanContextInfo->VlanSubnetMapTable == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "VLAN SUBNET RB Tree creation Failed \n");
        VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
        return VLAN_FAILURE;
    }

    gpVlanContextInfo->PortVlanMapTable =
        RBTreeCreateEmbedded (0, PortVlanMapTblCmp);
    if (gpVlanContextInfo->PortVlanMapTable == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC, VLAN_NAME,
                  "Port Vlan Map Table - RB Tree Creation Failed \n");
        VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
        return VLAN_FAILURE;
    }

    VLAN_MEMCPY (VLAN_TUNNEL_DOT1X_ADDR (), gVlanProviderDot1xAddr,
                 ETHERNET_ADDR_SIZE);
    VLAN_MEMCPY (VLAN_TUNNEL_LACP_ADDR (), gVlanProviderLacpAddr,
                 ETHERNET_ADDR_SIZE);
    VLAN_MEMCPY (VLAN_TUNNEL_STP_ADDR (), gVlanProviderStpAddr,
                 ETHERNET_ADDR_SIZE);
    VLAN_MEMCPY (VLAN_TUNNEL_GVRP_ADDR (), gVlanProviderGvrpAddr,
                 ETHERNET_ADDR_SIZE);
    VLAN_MEMCPY (VLAN_TUNNEL_GMRP_ADDR (), gVlanProviderGmrpAddr,
                 ETHERNET_ADDR_SIZE);
    VLAN_MEMCPY (VLAN_TUNNEL_MVRP_ADDR (), gVlanProviderMvrpAddr,
                 ETHERNET_ADDR_SIZE);
    VLAN_MEMCPY (VLAN_TUNNEL_MMRP_ADDR (), gVlanProviderMmrpAddr,
                 ETHERNET_ADDR_SIZE);
    VLAN_MEMCPY (VLAN_TUNNEL_ELMI_ADDR (), gVlanProviderElmiAddr,
                 ETHERNET_ADDR_SIZE);
    VLAN_MEMCPY (VLAN_TUNNEL_LLDP_ADDR (), gVlanProviderLldpAddr,
                 ETHERNET_ADDR_SIZE);
    VLAN_MEMCPY (VLAN_TUNNEL_ECFM_ADDR (), gVlanProviderEcfmAddr,
                 ETHERNET_ADDR_SIZE);
    VLAN_MEMCPY (VLAN_TUNNEL_EOAM_ADDR (), gVlanProviderEoamAddr,
                 ETHERNET_ADDR_SIZE);
    VLAN_MEMCPY (VLAN_TUNNEL_IGMP_ADDR (), gVlanProviderIgmpAddr,
                 ETHERNET_ADDR_SIZE);

    for (u1ProtocolId = 1; u1ProtocolId < VLAN_MAX_PROT_ID; u1ProtocolId++)
    {
        VLAN_MEMSET (MacAddr, 0, sizeof (tMacAddr));

        VlanTunnelProtocolMac (u1ProtocolId, MacAddr);

        i4RetVal = VlanHwSetTunnelMacAddress (VLAN_CURR_CONTEXT_ID (),
                                              MacAddr, u1ProtocolId);
        if (i4RetVal == VLAN_FAILURE)
        {
            VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME, "Setting Tunnel mac filter failed \n");
            VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
            return VLAN_FAILURE;
        }

    }

    if (VLAN_BRIDGE_MODE () != VLAN_CUSTOMER_BRIDGE_MODE)
    {
        VlanHwCheckTagAtIngress (VLAN_CURR_CONTEXT_ID (),
                                 &(gpVlanContextInfo->VlanInfo.
                                   u1HwIngTagSupport));
        VlanHwCheckTagAtEgress (VLAN_CURR_CONTEXT_ID (),
                                &(gpVlanContextInfo->VlanInfo.
                                  u1HwEgUnTagSupport));
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        if (VlanPbInit () != VLAN_SUCCESS)
        {
            VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC,
                      VLAN_NAME,
                      "Provider Bridge mode initialization failed \n");
            VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
            return VLAN_FAILURE;
        }
    }

    /* Restores previous configuration stored in FLASH */

    VlanRestoreConfigInfo ();

    /* 
     * Enables the previous configuration, restored from the Permanent 
     * Database. Indicates GARP, GVRP and GMRP incase if it has some 
     * VLAN entry or some group entry.
     */

    VlanEnablePreConfigInfo ();

    /* Add entry for default VLAN ID in the VLAN current table */

    /* Once all the task get created we have to allow the creation of
     * default VLAN */

    /* Also, when base bridge mode is saved as dot1d and increamental save is
     * disabled, create default Vlan while restoring base bridge mode. This is
     * because default VLAN will not get saved in iss.conf because of 
     * base bridge mode check in nmhGetFirst and nmhGetNext functions*/

#ifdef L2RED_WANTED
    u1NodeStatus = VLAN_NODE_STATUS ();
#endif

    if (u1NodeStatus == VLAN_NODE_ACTIVE)
    {
        if ((LrGetRestoreDefConfigFlag () == OSIX_TRUE) ||
            ((LrGetRestoreDefConfigFlag () == OSIX_FALSE) &&
             (IssGetIncrementalSaveStatus () == ISS_FALSE) &&
             (VLAN_BASE_BRIDGE_MODE () == DOT_1D_IN_PROGRESS_MODE)))
        {
            /* this function returns only success */
            VlanCreateDefaultVlanEntry ();
        }
    }
    else if (u1NodeStatus == VLAN_NODE_STANDBY)
    {
        VlanCreateDefaultVlanEntry ();
    }

    VlanEnablePriorityModule ();

#ifndef L2RED_WANTED
    /* Set the VLAN Learning Type in H/W */
    if (VlanHwSetVlanLearningType
        (VLAN_CURR_CONTEXT_ID (),
         gpVlanContextInfo->VlanInfo.u1VlanLearningType) == VLAN_FAILURE)
    {
        VlanDisablePriorityModule ();
        VLAN_MODULE_STATUS () = VLAN_DISABLED;

        VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC,
                  VLAN_NAME, "Setting the VLAN Learning Type Failed \n");
        VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
        return VLAN_FAILURE;
    }
#endif /* L2RED_WANTED */

    /* Once all the task get created we have to activate the default VLAN */

    if (((u1NodeStatus == VLAN_NODE_ACTIVE) &&
         ((LrGetRestoreDefConfigFlag () == OSIX_TRUE) ||
          ((LrGetRestoreDefConfigFlag () == OSIX_FALSE) &&
           (IssGetIncrementalSaveStatus () == ISS_FALSE) &&
           (VLAN_BASE_BRIDGE_MODE () == DOT_1D_IN_PROGRESS_MODE)))) ||
        ((u1NodeStatus == VLAN_NODE_STANDBY)))
    {
        if (VlanActivateDefaultVlanEntry () == VLAN_FAILURE)
        {
            VlanDisablePriorityModule ();
            VLAN_MODULE_STATUS () = VLAN_DISABLED;

            VLAN_TRC (VLAN_MOD_TRC, INIT_SHUT_TRC,
                      VLAN_NAME, "Activating Default VLAN entry Failed \n");

            VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
            return VLAN_FAILURE;
        }
    }

    if (VLAN_NODE_STATUS () != VLAN_NODE_IDLE)
    {
        i4RetVal = VlanEnableVlan ();

        if (i4RetVal != VLAN_SUCCESS)
        {
            VlanDisablePriorityModule ();

            VLAN_MODULE_STATUS () = VLAN_DISABLED;
            VlanMstMiDeleteAllVlans (VLAN_CURR_CONTEXT_ID ());
            VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
            return VLAN_FAILURE;
        }
    }
    else
    {
        /* 
         * create ports in VLAN even if VLAN node is not active else
         * msr configuration restore will fail.
         */
        VlanGetAllPorts ();
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeInit                                       */
/*                                                                           */
/*    Description         : This function deletes all the tables entries.    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : gpVlanContextInfo->pVlanPortTable                            */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
VlanDeInit ()
{
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4RetVal = VLAN_SUCCESS;
    UINT1               u1LldpFlag = VLAN_LLDP_PORT_NOTIFY_FALSE;
    UINT2               u2Port, u2NextPort;

    gau1VlanShutDownStatus[VLAN_CURR_CONTEXT_ID ()] = VLAN_SNMP_TRUE;

    VLAN_STOP_TIMER (VLAN_AGEOUT_TIMER);
#ifndef NPAPI_WANTED
    VLAN_STOP_TIMER (VLAN_PRIORITY_TIMER);
#endif
#ifdef L2RED_WANTED
    VLAN_STOP_TIMER (VLAN_RED_RELEARN_TIMER);
    VLAN_STOP_TIMER (VLAN_RED_PERIODIC_TIMER);
#endif
    /* Remove the Port related configurations from vlan as well
     * as from L2Iwf. */
    for (u2Port = (VLAN_CURR_CONTEXT_PTR ())->u2VlanStartPortInd;
         u2Port != VLAN_INVALID_PORT_INDEX; u2Port = u2NextPort)
    {
        if (VLAN_GET_PORT_ENTRY (u2Port) == NULL)
        {
            break;
        }
        u2NextPort = VLAN_GET_NEXT_PORT_INDEX (u2Port);

        i4RetVal = VlanDeletePortConfigurations (u2Port, u1LldpFlag);
        if (i4RetVal == VLAN_SUCCESS)
        {
            /* Reset the tunnel status of this port in L2Iwf and
             * release Pb port entry memory. */
            VlanResetPortTnlStatusAndRelPbPortMem (u2Port);
        }
    }

    if (gpVlanContextInfo->VlanStaticUnicastInfo != NULL)
    {
        /* delete the RBTree */
        RBTreeDelete (gpVlanContextInfo->VlanStaticUnicastInfo);
        gpVlanContextInfo->VlanStaticUnicastInfo = NULL;
    }

#ifdef SW_LEARNING
    if (gpVlanContextInfo->VlanFdbInfo != NULL)
    {
        /* delete the RBTree */
        RBTreeDelete (gpVlanContextInfo->VlanFdbInfo);
        gpVlanContextInfo->VlanFdbInfo = NULL;
    }
#ifdef MBSM_WANTED
    if (gVlanFDBInfo != NULL)
    {
        /* delete the RBTree */
        RBTreeDelete (gVlanFDBInfo);
        gVlanFDBInfo = NULL;
    }
#endif /* MBSM_WANTED */
#endif /* SW_LEARNING */

    /* 
     * This function stores the running configuration to permanent database.
     */
    VlanSaveConfigInfo ();

    if (VLAN_IS_PRIORITY_ENABLED () == VLAN_TRUE)
    {
        VlanDisablePriorityModule ();
    }

    /* This function traverses all VLAN ID created and
     * deletes the counter statistics */
    VlanDeleteStats ();

    /* No need to withdraw the propagate the infoformation 
     * Garp is already shutdown */

    /* 
     * This function takes care of deleting all static unicast table
     * and FDB table 
     */

    VlanDeleteFidTable ();

    VlanDeleteStVlanTable ();

    /*
     * This function takes care of deleting group table and static
     * multicast table.
     */

    VlanDeleteCurrTable ();

    VlanDeleteConstraintsTable ();

    VlanDeleteMacMapTable ();

    VlanDeleteSubnetMapTable ();
    VlanDeletePortVlanMapTable ();
    /* To release the allocated entries to the pool */
    VlanReleaseProtocolGrpTbl ();
    VlanDeleteWildCardTable ();

#ifdef SW_LEARNING
    if (gpVlanContextInfo->VlanFdbInfo != NULL)
    {
        /* delete the RBTree */
        RBTreeDelete (gpVlanContextInfo->VlanFdbInfo);
        /* Set Pointer as NULL */
        gpVlanContextInfo->VlanFdbInfo = NULL;
    }
#endif

    for (u2Port = (VLAN_CURR_CONTEXT_PTR ())->u2VlanStartPortInd;
         u2Port != VLAN_INVALID_PORT_INDEX; u2Port = u2NextPort)
    {

        if (VLAN_GET_PORT_ENTRY (u2Port) == NULL)
        {
            break;
        }
        VLAN_PORT_MAC_BASED (u2Port) = VLAN_SNMP_FALSE;
        VLAN_SET_PORT_SUBNET_BASED (u2Port, VLAN_SNMP_FALSE);
        u2NextPort = VLAN_GET_NEXT_PORT_INDEX (u2Port);
        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

        VlanRemovePortFromIfIndexTable (pPortEntry);
        VLAN_RELEASE_BUF (VLAN_PORT_ENTRY, (UINT1 *) pPortEntry);
        VLAN_GET_PORT_ENTRY (u2Port) = NULL;
    }

    VLAN_MODULE_STATUS () = VLAN_DISABLED;

    VLAN_PORT_PROTO_BASED = VLAN_SNMP_FALSE;
    VlanLldpApiNotifyProtoVlanStatus (0, VLAN_SNMP_FALSE);

    VLAN_MAC_BASED = VLAN_SNMP_FALSE;
    VLAN_SUBNET_BASED = VLAN_SNMP_FALSE;
    VLAN_START_VLAN_INDEX () = VLAN_INVALID_VLAN_INDEX;

    VlanSetVlanLearningMode (VLAN_INDEP_LEARNING);

    VlanL2IwfSetVlanLearningType (VLAN_CURR_CONTEXT_ID (), VLAN_INDEP_LEARNING);

    gpVlanContextInfo->VlanInfo.u1TrfClassEnabled = VLAN_SNMP_FALSE;

    /* DeInit the 1ad bridge related tables only when the existing
     * bridge mode is 1ad bridge mode. */
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        VlanPbDeInit ();
    }

    VLAN_BRIDGE_MODE () = VLAN_INVALID_BRIDGE_MODE;
    VlanHwDeInit (VLAN_CURR_CONTEXT_ID ());

    return;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteFidTable                               */
/*                                                                           */
/*    Description         : This function deletes all the entries in the Fid */
/*                          Table.                                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteFidTable (VOID)
{
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    tVlanStUcastEntry  *pNextStUcastEntry = NULL;
    UINT4               u4FidIndex = 0;
    tVlanId             VlanStartId = 0;
    tVlanId             VlanId = 0;
    tVlanCurrEntry     *pCurrEntry = NULL;

#ifndef NPAPI_WANTED
    tVlanFdbEntry      *pFdbEntry = NULL;
    tVlanFdbEntry      *pNextFdbEntry = NULL;
    UINT2               u2HashIndex;
#endif
    tVlanFidEntry      *pFidEntry = NULL;

    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry != NULL)
        {

            /* Deleting all static entries */
            pStUcastEntry = pFidEntry->pStUcastTbl;

            while (pStUcastEntry != NULL)
            {
                pNextStUcastEntry = pStUcastEntry->pNextNode;

                /* Delete any unicast entries configured staticaly in hardware */

                VlanHwDelStaticUcastEntry (pFidEntry->u4Fid,
                                           pStUcastEntry->MacAddr,
                                           pStUcastEntry->u2RcvPort);

                if (pStUcastEntry != NULL)
                {
                    RBTreeRemove (VLAN_CURR_CONTEXT_PTR ()->
                                  VlanStaticUnicastInfo,
                                  (tRBElem *) pStUcastEntry);
                }

                VLAN_RELEASE_BUF (VLAN_ST_UCAST_ENTRY, (UINT1 *) pStUcastEntry);
                VlanUpdateCurrentStaticUcastMacCount (VLAN_DELETE);

                pStUcastEntry = pNextStUcastEntry;
            }

            pFidEntry->pStUcastTbl = NULL;

#ifndef NPAPI_WANTED
            /* Deleting dynamic entries in the FDB table */
            for (u2HashIndex = 0; u2HashIndex < VLAN_MAX_BUCKETS; u2HashIndex++)
            {

                pFdbEntry = pFidEntry->UcastHashTbl[u2HashIndex];

                while (pFdbEntry != NULL)
                {

                    pNextFdbEntry = pFdbEntry->pNextHashNode;

                    VLAN_RELEASE_BUF (VLAN_FDB_ENTRY, (UINT1 *) pFdbEntry);

                    pFdbEntry = pNextFdbEntry;
                }

                pFidEntry->UcastHashTbl[u2HashIndex] = NULL;
            }
#endif
            if (gpVlanContextInfo->VlanInfo.u4NextFreeFdbId > pFidEntry->u4Fid)
            {
                gpVlanContextInfo->VlanInfo.u4NextFreeFdbId = pFidEntry->u4Fid;
            }
            /* Flush all the dynamically learnt entries
             * from the fdb Id and delete the fdb table */
            VlanHwFlushFdbId (VLAN_CURR_CONTEXT_ID (), pFidEntry->u4Fid);

            VlanHwDeleteFdbId (VLAN_CURR_CONTEXT_ID (), pFidEntry->u4Fid);

            MEMSET (pFidEntry, 0, sizeof (tVlanFidEntry));
            VLAN_RELEASE_BUF (VLAN_FID_ENTRY, (UINT1 *) pFidEntry);
            VLAN_GET_FID_ENTRY (u4FidIndex) = NULL;

        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteCurrTable                              */
/*                                                                           */
/*    Description         : This function deletes all the entries in the     */
/*                          Vlan Current table.                              */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteCurrTable (VOID)
{
    tVlanId             VlanId = VLAN_NULL_VLAN_ID;
    tVlanId             NextVlanId = VLAN_NULL_VLAN_ID;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanStMcastEntry  *pNextStMcastEntry = NULL;
    tVlanGroupEntry    *pGroupEntry = NULL;
    tVlanGroupEntry    *pNextGroupEntry = NULL;
    tVlanMacControl    *pVlanMacControlEntry = NULL;
#ifndef NPAPI_WANTED
    tVlanPortInfoPerVlan *pPortInfoPerVlanEntry = NULL;
    tVlanPortInfoPerVlan *pNextPortStatsEntry = NULL;
    tVlanStats         *pVlanStatsEntry;
#endif

    /* 
     * Deleting all the current table entries done based on
     * the active VLAN list 
     */

    for (VlanId = VLAN_START_VLAN_INDEX ();
         VlanId != VLAN_INVALID_VLAN_INDEX; VlanId = NextVlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            break;
        }
        NextVlanId = VLAN_GET_NEXT_VLAN_INDEX (VlanId);

        /* Deleting static Mcast entries for this VLAN */
        pStMcastEntry = pCurrEntry->pStMcastTable;
        pCurrEntry->pStMcastTable = NULL;

        while (pStMcastEntry != NULL)
        {
            pNextStMcastEntry = pStMcastEntry->pNextNode;

            VlanHwDelStMcastEntry (VlanId, pStMcastEntry->MacAddr,
                                   pStMcastEntry->u2RcvPort);

            VLAN_RELEASE_BUF (VLAN_ST_MCAST_ENTRY, (UINT1 *) pStMcastEntry);

            pStMcastEntry = pNextStMcastEntry;
        }

        /* Deleting Group Table */
        pGroupEntry = pCurrEntry->pGroupTable;
        pCurrEntry->pGroupTable = NULL;

        while (pGroupEntry != NULL)
        {
            pNextGroupEntry = pGroupEntry->pNextNode;

            if (pGroupEntry->u1StRefCount == 0)
            {
                VlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                     pGroupEntry->MacAddr);
            }

            VLAN_RELEASE_BUF (VLAN_GROUP_ENTRY, (UINT1 *) pGroupEntry);

            pGroupEntry = pNextGroupEntry;
        }

#ifndef NPAPI_WANTED
        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            /* Deleting port stats table */
            pPortInfoPerVlanEntry = pCurrEntry->pPortInfoTable;
            pCurrEntry->pPortInfoTable = NULL;

            while (pPortInfoPerVlanEntry != NULL)
            {

                pNextPortStatsEntry = pPortInfoPerVlanEntry->pNextNode;

                VLAN_RELEASE_BUF (VLAN_PORT_STATS_ENTRY,
                                  (UINT1 *) pPortInfoPerVlanEntry);
                pPortInfoPerVlanEntry = pNextPortStatsEntry;
            }
            pVlanStatsEntry = pCurrEntry->pVlanStats;
            pCurrEntry->pVlanStats = NULL;

            if (pVlanStatsEntry != NULL)
            {
                VLAN_RELEASE_BUF (VLAN_STATS_ENTRY, (UINT1 *) pVlanStatsEntry);
            }
        }
#endif

        /* Deleting the entries present in the unicast MAC control table */
        pVlanMacControlEntry = pCurrEntry->pVlanMacControl;

        if (pVlanMacControlEntry != NULL)
        {
            VLAN_RELEASE_BUF (VLAN_MAC_CONTROL_ENTRY,
                              (UINT1 *) pVlanMacControlEntry);
        }
        pCurrEntry->pVlanMacControl = NULL;

        /* Remove the VLAN from Hardware as well */
        VlanHwDelVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId);

        /* Remove the VLAN from the VLAN active list */
        VlanRemoveVlanFromCurrentTable (pCurrEntry->VlanId);
        VLAN_RELEASE_BUF (VLAN_CURR_ENTRY, (UINT1 *) pCurrEntry);
    }
    gpVlanContextInfo->VlanInfo.u4NumActiveVlans = 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteStVlanTable                            */
/*                                                                           */
/*    Description         : This function deletes all the entries in the     */
/*                          Static Vlan table.                               */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : gpVlanContextInfo->pStaticVlanTable                          */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteStVlanTable (VOID)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tStaticVlanEntry   *pNextStVlanEntry = NULL;

    pStVlanEntry = gpVlanContextInfo->pStaticVlanTable;
    gpVlanContextInfo->pStaticVlanTable = NULL;

    while (pStVlanEntry != NULL)
    {

        pNextStVlanEntry = pStVlanEntry->pNextNode;

        VlanCustReleaseBuf (pStVlanEntry->VlanId,
                            VLAN_ST_VLAN_ENTRY, (UINT1 *) pStVlanEntry);
        pStVlanEntry = pNextStVlanEntry;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleDeleteAllDynamicVlanInfo               */
/*                                                                           */
/*    Description         : Removes all the dynamic vlan information         */
/*                          from the Current table.                          */
/*                                                                           */
/*    Input(s)            : u2Port - Port Number of the input port.          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleDeleteAllDynamicVlanInfo (UINT4 u4ContextId, UINT2 u2Port)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT1              *pu1LocalPortList = NULL;
    UINT1              *pu1TempLocalPortList = NULL;
    INT4                i4RetVal;
    tMacAddr            McastAddr;
    tPortList          *pPortList = NULL;
    tVlanId             VlanId = VLAN_NULL_VLAN_ID;
    tVlanId             NextVlanId = VLAN_NULL_VLAN_ID;
    UINT1               u1DeleteAllPort;
    UINT1               u1Result = VLAN_TRUE;
    UINT4               u4Port;

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        return;
    }

    if (u2Port == VLAN_INVALID_PORT)
    {
        u1DeleteAllPort = VLAN_TRUE;

        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "Deleting Dynamic VLAN information Learnt on ALL ports\n");
    }
    else
    {
        if (u2Port >= VLAN_MAX_PORTS + 1)
        {
            VlanReleaseContext ();
            return;
        }
        if (VLAN_GET_PORT_ENTRY (u2Port) == NULL)
        {
            VlanReleaseContext ();
            return;
        }
        u1DeleteAllPort = VLAN_FALSE;

        u4Port = VLAN_GET_IFINDEX (u2Port);

        VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "Deleting Dynamic VLAN information learnt on Port %d\n",
                       u4Port);
    }

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Error in Allocating memory for bitlist\n");
        return;
    }

    /* 
     * VLAN_SCAN_VLAN_TABLE not used here since the entry could
     * be deleted inside the loop.
     */
    for (VlanId = VLAN_START_VLAN_INDEX ();
         VlanId != VLAN_INVALID_VLAN_INDEX; VlanId = NextVlanId)
    {
        pVlanEntry = VlanGetVlanEntry (VlanId);

        if (NULL == pVlanEntry)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "Error in getting node from VLAN_TABLE\n");
            break;
        }
        NextVlanId = VLAN_GET_NEXT_VLAN_INDEX (VlanId);

        if (u1DeleteAllPort == VLAN_TRUE)
        {
            if (pVlanEntry->pStVlanEntry == NULL)
            {
                i4RetVal = VlanHwDelDynVlanEntry (pVlanEntry->VlanId,
                                                  VLAN_INVALID_PORT);

                if (i4RetVal == VLAN_FAILURE)
                {
                    continue;
                }

                VlanDelVlanEntry (pVlanEntry);
            }
            else
            {
                VLAN_IS_NO_CURR_LEARNT_PORTS_PRESENT (pVlanEntry, u1Result);
                if (u1Result == VLAN_TRUE)
                {
                    continue;
                }

                pu1LocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    break;
                }
                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_CURR_LEARNT_PORTS (pVlanEntry, pu1LocalPortList);
                /* Remove Learn entries */
                VLAN_RESET_CURR_EGRESS_PORTS (pVlanEntry, pu1LocalPortList);

                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_UNTAGGED_PORTS (pVlanEntry->pStVlanEntry,
                                         pu1LocalPortList);

                pu1TempLocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1TempLocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    UtilPlstReleaseLocalPortList (pu1LocalPortList);
                    break;
                }
                MEMSET (pu1TempLocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_CURR_EGRESS_PORTS (pVlanEntry, pu1TempLocalPortList);

                i4RetVal =
                    VlanHwAddVlanEntry (pVlanEntry->VlanId,
                                        pu1TempLocalPortList, pu1LocalPortList);

                UtilPlstReleaseLocalPortList (pu1LocalPortList);

                if (i4RetVal == VLAN_FAILURE)
                {
                    UtilPlstReleaseLocalPortList (pu1TempLocalPortList);
                    continue;
                }

                if (SNOOP_ENABLED ==
                    VlanSnoopIsIgmpSnoopingEnabled (u4ContextId)
                    || SNOOP_ENABLED ==
                    VlanSnoopIsMldSnoopingEnabled (u4ContextId))
                {
                    MEMSET (McastAddr, 0, sizeof (tMacAddr));

                    /* For MI the port list to the externa modules should be 
                       physical port list so convert and pass it */
                    MEMSET (pPortList, 0, sizeof (tPortList));
                    VLAN_CONVERT_CURR_LEARNT_TO_IFPORTLIST (pVlanEntry,
                                                            (*pPortList));

                    VlanSnoopMiUpdatePortList (u4ContextId, pVlanEntry->VlanId,
                                               McastAddr, gNullIfPortList,
                                               *pPortList,
                                               VLAN_UPDT_VLAN_PORTS);
                }

                MEMSET (pu1TempLocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_CURR_LEARNT_PORTS (pVlanEntry, pu1TempLocalPortList);
                VlanNotifyMembersChangeToL2Iwf (pVlanEntry->VlanId,
                                                gNullPortList,
                                                pu1TempLocalPortList);

                VlanSispUpdatePortVlanTable (u4ContextId,
                                             pVlanEntry->VlanId, gNullPortList,
                                             pu1TempLocalPortList);

                UtilPlstReleaseLocalPortList (pu1TempLocalPortList);
                VLAN_RESET_ALL_CURR_LEARNT_PORT (pVlanEntry);

                if (VlanCfaGetIvrStatus () == CFA_ENABLED)
                {
                    VlanIvrComputeVlanMclagIfOperStatus (pVlanEntry);
                    VlanIvrComputeVlanIfOperStatus (pVlanEntry);
                }

                VLAN_GET_TIMESTAMP (&pVlanEntry->u4TimeStamp);
            }
        }
        else
        {
            /* 
             * Check whether the port membership to be deleted is 
             * a member of learnt ports.
             */
            VLAN_IS_CURR_LEARNT_PORT (pVlanEntry, u2Port, u1Result);

            if (u1Result == VLAN_FALSE)
            {
                /* nothing to be deleted */
                continue;
            }

            if (pVlanEntry->pStVlanEntry != NULL)
            {
                VLAN_IS_MEMBER_PORT (pVlanEntry->pStVlanEntry->EgressPorts,
                                     u2Port, u1Result);
                if (u1Result == VLAN_TRUE)
                {
                    /* Port is a staic port. Cant be deleted */
                    continue;
                }
            }

            VLAN_RESET_SINGLE_CURR_LEARNT_PORT (pVlanEntry, u2Port);
            VLAN_IS_NO_CURR_LEARNT_PORTS_PRESENT (pVlanEntry, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                if (pVlanEntry->pStVlanEntry == NULL)
                {
                    /* last member leave */
                    i4RetVal = VlanHwDelDynVlanEntry (pVlanEntry->VlanId,
                                                      u2Port);

                    if (i4RetVal == VLAN_FAILURE)
                    {
                        continue;
                    }
                    VlanDelVlanEntry (pVlanEntry);
                }
                else
                {
                    VlanUpdtVlanEgressPorts (pVlanEntry);

                    i4RetVal =
                        VlanHwResetVlanMemberPort (pVlanEntry->VlanId, u2Port);
                    if (i4RetVal == VLAN_FAILURE)
                    {
                        continue;
                    }

                    if (SNOOP_ENABLED ==
                        VlanSnoopIsIgmpSnoopingEnabled (u4ContextId)
                        || SNOOP_ENABLED ==
                        VlanSnoopIsMldSnoopingEnabled (u4ContextId))
                    {
                        MEMSET (McastAddr, 0, sizeof (tMacAddr));
                        MEMSET ((*pPortList), 0, sizeof (tPortList));

                        VLAN_SET_MEMBER_PORT ((*pPortList),
                                              VLAN_GET_IFINDEX (u2Port));

                        VlanSnoopMiUpdatePortList (u4ContextId, VlanId,
                                                   McastAddr, gNullIfPortList,
                                                   *pPortList,
                                                   VLAN_UPDT_VLAN_PORTS);
                    }

                    VlanNotifyResetVlanMemberToL2IwfAndEnhFltCriteria
                        (pVlanEntry->VlanId, u2Port);

                    /* 
                     * Need to update trunk ports changes for the updated
                     * VLAN 
                     */
                    VlanAddTrunkPortsToNewVlan (pVlanEntry->VlanId);

                    VLAN_GET_TIMESTAMP (&pVlanEntry->u4TimeStamp);
                }
            }
            else
            {
                VlanUpdtVlanEgressPorts (pVlanEntry);

                i4RetVal =
                    VlanHwResetVlanMemberPort (pVlanEntry->VlanId, u2Port);
                if (i4RetVal == VLAN_FAILURE)
                {
                    continue;
                }

                if (SNOOP_ENABLED ==
                    VlanSnoopIsIgmpSnoopingEnabled (u4ContextId)
                    || SNOOP_ENABLED ==
                    VlanSnoopIsMldSnoopingEnabled (u4ContextId))
                {
                    MEMSET (McastAddr, 0, sizeof (tMacAddr));
                    MEMSET ((*pPortList), 0, sizeof (tPortList));

                    VLAN_SET_MEMBER_PORT ((*pPortList),
                                          VLAN_GET_IFINDEX (u2Port));

                    VlanSnoopMiUpdatePortList (u4ContextId, VlanId, McastAddr,
                                               gNullIfPortList, *pPortList,
                                               VLAN_UPDT_VLAN_PORTS);
                }

                VlanNotifyResetVlanMemberToL2IwfAndEnhFltCriteria
                    (pVlanEntry->VlanId, u2Port);

                /* 
                 * Need to update trunk ports changes for the updated
                 * VLAN 
                 */
                VlanAddTrunkPortsToNewVlan (pVlanEntry->VlanId);

                VLAN_GET_TIMESTAMP (&pVlanEntry->u4TimeStamp);
            }
        }

    }

    VlanReleaseContext ();

    FsUtilReleaseBitList ((UINT1 *) pPortList);
    VlanRedSendDelAllDynInfoUpdate (VLAN_RED_GVRP_INFO, (UINT4) u2Port);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleDeleteAllDynamicMcastInfo              */
/*                                                                           */
/*    Description         : This function removes all the dynamic multicast  */
/*                          information from the table and static multicast  */
/*                          informaion from hardware if vlan is disabled.    */
/*                          This function deletes the multicast addresses    */
/*                          when called from IGS, MLDS and GARP. When called */
/*                          from MRP, this function deletes both unicast and */
/*                          multicast addresses.                             */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u2Port - Port Number of the input port           */
/*                          u1DisableVlan - VLAN_TRUE when called via        */
/*                          u1MacAddrType - Mac Address type of the dynamic  */
/*                          mcast entries to be deleted (IPV4 or IPV6 mac    */
/*                          address type or both)                            */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleDeleteAllDynamicMcastInfo (UINT4 u4Port, UINT1 u1DisableVlan,
                                     UINT1 u1MacAddrType)
{
    UINT1               u1DeleteAllPort;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanGroupEntry    *pGroupEntry = NULL;
    tVlanGroupEntry    *pNextGroupEntry = NULL;
    tVlanId             VlanId;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1DeleteMacType = VLAN_FALSE;
    UINT4               u4TempContext;
    UINT2               u2LocalPort;
    UINT2               u2HwAddr = 0;
    UINT1               u1MacMcastResult = 0;

    if (u4Port != VLAN_INVALID_PORT)
    {
        if (VlanGetContextInfoFromIfIndex (u4Port, &u4TempContext, &u2LocalPort)
            != VLAN_SUCCESS)
        {
            return;
        }
    }
    else
    {
        u2LocalPort = (UINT2) u4Port;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        return;
    }

    if (u2LocalPort == VLAN_INVALID_PORT)
    {

        u1DeleteAllPort = VLAN_TRUE;

        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "Deleting Dynamic Multicast information for ALL ports \n");
    }
    else
    {

        u1DeleteAllPort = VLAN_FALSE;

        VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "Deleting Dynamic Multicast information about "
                       "Port %d \n", u4Port);
    }

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pVlanEntry = VlanGetVlanEntry (VlanId);
        if (NULL == pVlanEntry)
        {
            continue;
        }

        pGroupEntry = pVlanEntry->pGroupTable;
        while (pGroupEntry != NULL)
        {
            pNextGroupEntry = pGroupEntry->pNextNode;
            /* ipv4 entries alone are deleted if the passed mac-address
             * type is IPVX_ADDR_FMLY_IPV4. ipv6 entries alone are deleted if
             * the mac-address type is IPVX_ADDR_FMLY_IPV6. This check takes
             * care that only ipv4 entries are deleted when called from
             * IGS and only ipv6 entries are deleted when called from MLDS
             * module. 
             * When called from GARP and MRP, both ipv4 and ipv6 entries
             * need to be deleted. Hence the mac-address type is passed as 
             * VLAN_ADDR_FMLY_ALL.
             */

            MEMCPY (&u2HwAddr, pGroupEntry->MacAddr, sizeof (UINT2));
            if ((u2HwAddr & 0xffff) == 0x3333)
                u1MacMcastResult = OSIX_TRUE;
            else
                u1MacMcastResult = OSIX_FALSE;

            if (((u1MacAddrType == VLAN_ADDR_FMLY_IPV4) &&
                 (CFA_IS_ENET_MAC_MCAST (pGroupEntry->MacAddr))) ||
                ((u1MacAddrType == VLAN_ADDR_FMLY_IPV6) &&
                 (u1MacMcastResult == OSIX_TRUE)) ||
                (u1MacAddrType == VLAN_ADDR_FMLY_ALL))
            {
                u1DeleteMacType = VLAN_TRUE;
            }
            else
            {
                u1DeleteMacType = VLAN_FALSE;
            }

            if (u1DeleteAllPort == VLAN_TRUE)
            {
                if (pGroupEntry->u1StRefCount == 0)
                {

                    /* Delete the entry from the hardware */
                    if (u1DeleteMacType == VLAN_TRUE)
                    {
                        VlanHwDelDynMcastEntry (pVlanEntry->VlanId,
                                                pGroupEntry->MacAddr,
                                                VLAN_INVALID_PORT);
                        VlanDeleteGroupEntry (pVlanEntry, pGroupEntry);
                        VlanPbUpdateCurrentMulticastMacCount (VLAN_DELETE);
#ifdef SW_LEARNING
                        VlanUpdateCurrentMulticastDynamicMacCount (VlanId,
                                                                   VLAN_DELETE);
#endif

                    }
                }
                else
                {
                    /* If VlanDisable Flag is True, Del Mcast Entry */
                    if (u1DisableVlan == VLAN_TRUE)
                    {
                        /* Delete Mcast entry in h/w */
                        VlanHwDelStMcastEntry (pVlanEntry->VlanId,
                                               pGroupEntry->MacAddr,
                                               VLAN_INVALID_PORT);
                        MEMSET (pGroupEntry->LearntPorts, 0,
                                sizeof (tLocalPortList));
                    }
                    else
                    {            /* vlan disable flag is set to FALSE */
                        if (u1DeleteMacType == VLAN_TRUE)
                        {
                            VlanDelMcastLearntPorts (pVlanEntry,
                                                     pGroupEntry->MacAddr);
                            MEMSET (pGroupEntry->LearntPorts, 0,
                                    sizeof (tLocalPortList));
                        }
                    }
                }
            }
            else
            {
                /* Delete only dynamically learnt ports into the H/W */
                u1Result = VLAN_FALSE;

                VLAN_IS_MEMBER_PORT (pGroupEntry->LearntPorts, u2LocalPort,
                                     u1Result);
                if (u1Result == VLAN_TRUE)
                {
                    VlanHwResetMcastPort (pVlanEntry->VlanId,
                                          pGroupEntry->MacAddr, u2LocalPort);
                }

                VlanDeleteGroupPortInfo (pVlanEntry, pGroupEntry, u2LocalPort);
            }

            pGroupEntry = pNextGroupEntry;
        }

        if (u1DeleteAllPort == VLAN_TRUE)
        {
            pVlanEntry->u1IsGrpChanged = VLAN_FALSE;
        }
    }

    VlanRedSendDelAllDynInfoUpdate (VLAN_RED_GMRP_INFO, (UINT4) u2LocalPort);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleDelDynamicMcastInfoForVlan             */
/*                                                                           */
/*    Description         : This function removes all the dynamic multicast  */
/*                          information from the table for the given vlan    */
/*                                                                           */
/*    Input(s)            : VlanId - VlanId for which the multicast entries  */
/*                                   are deleted                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleDelDynamicMcastInfoForVlan (tVlanId VlanId)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanGroupEntry    *pGroupEntry = NULL;
    tVlanGroupEntry    *pNextGroupEntry = NULL;

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        return;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);
    if (pVlanEntry == NULL)
    {
        return;
    }

    pGroupEntry = pVlanEntry->pGroupTable;

    while (pGroupEntry != NULL)
    {
        pNextGroupEntry = pGroupEntry->pNextNode;

        if (pGroupEntry->u1StRefCount == 0)
        {
            /* Delete the entry from the hardware */
            VlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                 pVlanEntry->VlanId, pGroupEntry->MacAddr);
            VlanDeleteGroupEntry (pVlanEntry, pGroupEntry);

            VlanPbUpdateCurrentMulticastMacCount (VLAN_DELETE);
#ifdef SW_LEARNING
            VlanUpdateCurrentMulticastDynamicMacCount (VlanId, VLAN_DELETE);
#endif

        }
        else
        {
            VlanDelMcastLearntPorts (pVlanEntry, pGroupEntry->MacAddr);
            MEMSET (pGroupEntry->LearntPorts, 0, sizeof (tLocalPortList));
        }

        pGroupEntry = pNextGroupEntry;
    }

    return;
}

#ifdef VLAN_EXTENDED_FILTER
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleDelDynamicDefGroupInfoForVlan          */
/*                                                                           */
/*    Description         : This function removes all the dynamic def group  */
/*                          information from the table for the given vlan    */
/*                                                                           */
/*    Input(s)            : VlanId - VlanId for which the dynamic def group  */
/*                                   is to be deleted                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleDelDynamicDefGroupInfoForVlan (tVlanId VlanId)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT1              *pHwPortList = NULL;

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        return;
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        return;
    }

    MEMSET (pVlanEntry->AllGrps.Ports, 0, sizeof (tLocalPortList));

    MEMCPY (pVlanEntry->AllGrps.Ports, pVlanEntry->AllGrps.StaticPorts,
            sizeof (tLocalPortList));
    pHwPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pHwPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleDelDynamicDefGroupInfoForVlan: Error in allocating memory "
                  "for pHwPortList \r\n");
        return;
    }

    MEMSET (pHwPortList, 0, sizeof (tLocalPortList));
    MEMCPY (pHwPortList, pVlanEntry->AllGrps.Ports, sizeof (tLocalPortList));

    VlanHwSetDefGroupInfo (VLAN_CURR_CONTEXT_ID (), VlanId, VLAN_ALL_GROUPS,
                           pHwPortList);

    MEMSET (pVlanEntry->UnRegGrps.Ports, 0, sizeof (tLocalPortList));

    MEMCPY (pVlanEntry->UnRegGrps.Ports, pVlanEntry->UnRegGrps.StaticPorts,
            sizeof (tLocalPortList));

    MEMSET (pHwPortList, 0, sizeof (tLocalPortList));
    MEMCPY (pHwPortList, pVlanEntry->UnRegGrps.Ports, sizeof (tLocalPortList));

    VlanHwSetDefGroupInfo (VLAN_CURR_CONTEXT_ID (), VlanId,
                           VLAN_UNREG_GROUPS, pHwPortList);
    UtilPlstReleaseLocalPortList (pHwPortList);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleDeleteAllDynamicDefGroupInfo           */
/*                                                                           */
/*    Description         : This function removes all the dynamic service req*/
/*                          information from the table.                      */
/*                                                                           */
/*    Input(s)            : u2Port - This is currently called from GARP, so  */
/*                          the port is local port                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleDeleteAllDynamicDefGroupInfo (UINT4 u4ContextId, UINT2 u2Port)
{
    tVlanCurrEntry     *pVlanEntry = NULL;
    tVlanId             VlanId;
    UINT1               u1DeleteAllPort;
    UINT1               u1Result = VLAN_FALSE;
    UINT4               u4Port;

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return;
    }

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        VlanReleaseContext ();
        return;
    }

    if (u2Port == VLAN_INVALID_PORT)
    {
        u1DeleteAllPort = VLAN_TRUE;

        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                  "Deleting Dynamic Service Requirement information"
                  "for ALL ports \n");
    }
    else
    {
        if (u2Port >= VLAN_MAX_PORTS + 1)
        {
            VlanReleaseContext ();
            return;
        }
        if (VLAN_GET_PORT_ENTRY (u2Port) == NULL)
        {
            VlanReleaseContext ();
            return;
        }

        u1DeleteAllPort = VLAN_FALSE;

        u4Port = VLAN_GET_IFINDEX (u2Port);
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                       "Deleting Dynamic Service Requirement information for "
                       "Port %d \n", u4Port);
    }

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pVlanEntry = VlanGetVlanEntry (VlanId);

        if (NULL == pVlanEntry)
        {
            continue;
        }
        if (u1DeleteAllPort == VLAN_TRUE)
        {

            MEMCPY (pVlanEntry->AllGrps.Ports,
                    pVlanEntry->AllGrps.StaticPorts, sizeof (tLocalPortList));

            MEMCPY (pVlanEntry->UnRegGrps.Ports,
                    pVlanEntry->UnRegGrps.StaticPorts, sizeof (tLocalPortList));

            VlanHwSetDefGroupInfo (u4ContextId, pVlanEntry->VlanId,
                                   VLAN_ALL_GROUPS,
                                   pVlanEntry->AllGrps.StaticPorts);

            VlanHwSetDefGroupInfo (u4ContextId, pVlanEntry->VlanId,
                                   VLAN_UNREG_GROUPS,
                                   pVlanEntry->UnRegGrps.StaticPorts);

        }
        else
        {

            VLAN_IS_MEMBER_PORT (pVlanEntry->AllGrps.StaticPorts, u2Port,
                                 u1Result);

            /* Check whether the port is a member port and 
             * if not member delete 
             */

            if (u1Result == VLAN_FALSE)
            {
                VLAN_RESET_MEMBER_PORT (pVlanEntry->AllGrps.Ports, u2Port);

                VlanHwSetDefGroupInfo (u4ContextId, pVlanEntry->VlanId,
                                       VLAN_ALL_GROUPS,
                                       pVlanEntry->AllGrps.Ports);
            }
            VLAN_IS_MEMBER_PORT (pVlanEntry->UnRegGrps.StaticPorts, u2Port,
                                 u1Result);
            if (u1Result == VLAN_FALSE)
            {
                VLAN_RESET_MEMBER_PORT (pVlanEntry->UnRegGrps.Ports, u2Port);
                VlanHwSetDefGroupInfo (u4ContextId, pVlanEntry->VlanId,
                                       VLAN_UNREG_GROUPS,
                                       pVlanEntry->UnRegGrps.Ports);
            }

        }
    }

    VlanReleaseContext ();
    return;
}
#endif /*VLAN_EXTENDED_FILTER */

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleDeleteFdbEntry                         */
/*                                                                           */
/*    Description         : This function is called to delete an FDB entry   */
/*                          from FDB table                                   */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan ID of the entry                    */
/*                          MacAddress - MAC address of the entry            */
/*                          u2RemoteId - InstanceId                        */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : NONE                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleDeleteFdbEntry (tVlanId VlanId, tMacAddr MacAddress, UINT2 u2RemoteId)
{
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
    VlanFdbTableRemoveInCtxt (VlanId, MacAddress, u2RemoteId);
    VlanPvlanFdbTableRem (VlanId, MacAddress);
#else
#ifdef KERNEL_WANTED
    VlanFdbRemoveEntryFromKernel (VlanId, MacAddress);
#else /*KERNEL_WANTED */
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddress);
#endif /*KERNEL_WANTED */
#endif /*SW_LEARNING */
#else /*NPAPI_WANTED */
    VlanFdbRemoveEntryFromHashTable (VlanId, MacAddress);
#endif /*NPAPI_WANTED */
    UNUSED_PARAM (u2RemoteId);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleDeleteAllFdbEntry                      */
/*                                                                           */
/*    Description         : This function is called to delete the entire FDB */
/*                          table from our software - either FDB table used  */
/*                          when only software forwarding is present (or) FDB*/
/*                          maintained to have a shadow of the Hw table      */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : NONE                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleDeleteAllFdbEntry (VOID)
{
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
    VlanFdbTableRemoveAllInCtxt ();
#endif /*SW_LEARNING */
#endif /*NPAPI_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteConstraintsTable                       */
/*                                                                           */
/*    Description         : This function deletes all the entries in the     */
/*                          Vlan Constraints Table.                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : gpVlanContextInfo->aVlanConstraintTable                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteConstraintsTable (VOID)
{
    UINT2               u2Index;

    /* Initialise VLAN constraints table */
    for (u2Index = 0; u2Index < VLAN_MAX_CONSTRAINTS; u2Index++)
    {

        gpVlanContextInfo->aVlanConstraintTable[u2Index].u1RowStatus =
            VLAN_DESTROY;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteMacMapTable                            */
/*                                                                           */
/*    Description         : This function deletes all the entries in the     */
/*                          Vlan MacMap Table.                               */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteMacMapTable (VOID)
{
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanMacMapEntry   *pMacMapEntry = NULL;
    tVlanMacMapEntry   *pNextMacMapEntry = NULL;
    UINT2               u2HashIndex = 0;

    for (u2HashIndex = 0; u2HashIndex < VLAN_MAX_MAC_BUCKETS; u2HashIndex++)
    {

        pMacMapEntry = VLAN_GET_MAC_MAP_ENTRY (u2HashIndex);
        VLAN_GET_MAC_MAP_ENTRY (u2HashIndex) = NULL;

        while (pMacMapEntry != NULL)
        {

            pNextMacMapEntry = pMacMapEntry->pNextHashNode;
            VlanHwDeletePortMacVlanEntry
                (VLAN_CURR_CONTEXT_ID (),
                 (UINT2) (VLAN_GET_PHY_PORT ((UINT2) pMacMapEntry->u4Port)),
                 pMacMapEntry->MacAddr);

            if (VLAN_IS_PORT_VALID (pMacMapEntry->u4Port) == VLAN_TRUE)
            {
                pPortEntry = VLAN_GET_PORT_ENTRY (pMacMapEntry->u4Port);

                (pPortEntry->u2MacVlanEntryCount)--;
            }

            VLAN_RELEASE_BUF (VLAN_MAC_MAP_ENTRY, (UINT1 *) pMacMapEntry);

            pMacMapEntry = pNextMacMapEntry;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteSubnetMapTable                         */
/*                                                                           */
/*    Description         : This function deletes all the entries in the     */
/*                          Vlan Subnet Map Table.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteSubnetMapTable (VOID)
{
#ifndef BCMX_WANTED
    tVlanPortEntry     *pPortEntry = NULL;
#endif
    tVlanSubnetMapEntry *pSubnetMapEntry = NULL;
    tVlanSubnetMapIndex CurrSubnetMapIndex;
    tVlanSubnetMapIndex NextSubnetMapIndex;
    UINT4               u4Port = VLAN_INIT_VAL;
    UINT4               u4SubnetAddr = VLAN_INIT_VAL;
    UINT4               u4SubnetMask = VLAN_INIT_VAL;

    if (VlanSubnetMapTableGetFirstEntry (&u4Port, &u4SubnetAddr,
                                         &u4SubnetMask) == VLAN_SUCCESS)
    {
        NextSubnetMapIndex.u4Port = u4Port;
        NextSubnetMapIndex.SubnetAddr = u4SubnetAddr;
        NextSubnetMapIndex.SubnetMask = u4SubnetMask;

        do
        {
            pSubnetMapEntry =
                VlanGetSubnetMapEntry (NextSubnetMapIndex.SubnetAddr,
                                       NextSubnetMapIndex.u4Port,
                                       NextSubnetMapIndex.SubnetMask);

            if (pSubnetMapEntry == NULL)
            {
                break;
            }
            if (VLAN_PORT_SUBNET_BASED (pSubnetMapEntry->u4Port) ==
                VLAN_ENABLED)
            {
                if (pSubnetMapEntry->u1RowStatus == VLAN_ACTIVE)
                {
                    VlanHwDeletePortSubnetVlanEntry (VLAN_CURR_CONTEXT_ID (),
                                                     (UINT2) (VLAN_GET_PHY_PORT
                                                              ((UINT2)
                                                               pSubnetMapEntry->
                                                               u4Port)),
                                                     pSubnetMapEntry->
                                                     SubnetAddr,
                                                     pSubnetMapEntry->
                                                     SubnetMask);
                }
            }

#ifndef BCMX_WANTED
            if (VLAN_IS_PORT_VALID (u4Port) == VLAN_TRUE)
            {
                pPortEntry = VLAN_GET_PORT_ENTRY (pSubnetMapEntry->u4Port);
                (pPortEntry->u2SubnetVlanEntryCount)--;
            }
#endif

            gu2VlanSubnetMapCount--;

            VLAN_RELEASE_BUF (VLAN_SUBNET_MAP_ENTRY, (UINT1 *) pSubnetMapEntry);

            CurrSubnetMapIndex.u4Port = NextSubnetMapIndex.u4Port;
            CurrSubnetMapIndex.SubnetAddr = NextSubnetMapIndex.SubnetAddr;
            CurrSubnetMapIndex.SubnetMask = NextSubnetMapIndex.SubnetMask;
        }
        while (VlanSubnetMapTableGetNextEntry (&CurrSubnetMapIndex,
                                               &NextSubnetMapIndex) ==
               VLAN_SUCCESS);
    }

    if (gpVlanContextInfo->VlanSubnetMapTable != NULL)
    {
        /* delete the RBTree */
        RBTreeDelete (gpVlanContextInfo->VlanSubnetMapTable);
    }

    gpVlanContextInfo->VlanSubnetMapTable = NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeletePortVlanMapTable                       */
/*                                                                           */
/*    Description         : This function deletes all the entries in the     */
/*                          Port vlan map Table.                             */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeletePortVlanMapTable (VOID)
{
    tPortVlanMapEntry  *pPortVlanMapEntry = NULL;
    tPortVlanMapEntry  *pNextEntry = NULL;

    pPortVlanMapEntry = VlanPortVlanMapTblGetFirstEntry ();

    while (pPortVlanMapEntry != NULL)
    {
        /* Delete all the nodes */
        pNextEntry = VlanPortVlanMapTblGetNextEntry
            (pPortVlanMapEntry->u2VlanId, pPortVlanMapEntry->u2Port);

        if (VlanDeletePortVlanMapEntry (pPortVlanMapEntry) == VLAN_FAILURE)
        {
            return;
        }
        pPortVlanMapEntry = pNextEntry;
    }

    if (gpVlanContextInfo->PortVlanMapTable != NULL)
    {
        /* delete the RBTree */
        RBTreeDelete (gpVlanContextInfo->PortVlanMapTable);
    }

    gpVlanContextInfo->PortVlanMapTable = NULL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPortTblInit                                  */
/*                                                                           */
/*    Description         : This function initialise the Vlan Port Table.    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : gpVlanContextInfo->pVlanPortTable          */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS if the initialization success        */
/*                         VLAN_FAILURE if the initialization failed         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPortTblInit (VOID)
{

    /* Inititialise array of pointers */
    MEMSET (gpVlanContextInfo->apVlanPortEntry, 0,
            (sizeof (tVlanPortEntry *) * (VLAN_MAX_PORTS + 1)));

    /* Initialise the start port index */
    gpVlanContextInfo->u2VlanStartPortInd = VLAN_INVALID_PORT_INDEX;
    gpVlanContextInfo->u2VlanLastPortInd = VLAN_INVALID_PORT_INDEX;

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPropagateStaticMacInfo ()                    */
/*                                                                           */
/*    Description         : This function is invoked whenever Vlan is        */
/*                          enabled. All Static Multicast / Unicast          */
/*                          information will                                 */
/*                          be propagated to other Vlan bridges.             */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanCurrTable                             */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPropagateStaticMacInfo (VOID)
{
    UINT1              *pu1LocalPortList = NULL;
    UINT1              *pFwdUnRegPortList = NULL;
    tVlanId             VlanId;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    tVlanFidEntry      *pFidEntry = NULL;
    VOID               *pAttrRpQMsg = NULL;
    INT4                i4RetVal = VLAN_SUCCESS;

    if ((VlanGmrpIsGmrpEnabledInContext (VLAN_CURR_CONTEXT_ID ()) == GMRP_FALSE)
        && (VlanMrpIsMmrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_FALSE))
    {
        return;
    }

    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "Propagating Static Multicast information \n ");
    pFwdUnRegPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pFwdUnRegPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanPropagateStaticMacInfo: Error in allocating memory "
                  "for pFwdUnRegPortList \r\n");
        return;
    }

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        /* If the VLAN is mapped to ESP VLAN list then should not 
         * be given to MRP/GARP */
        if (VlanPbbTeVidIsEspVid (VLAN_CURR_CONTEXT_ID (), VlanId) == VLAN_TRUE)
        {
            continue;
        }

        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (pCurrEntry == NULL)
        {
            continue;
        }
        pStMcastEntry = pCurrEntry->pStMcastTable;

        pFidEntry = VLAN_GET_FID_ENTRY (pCurrEntry->u4FidIndex);
        pStUcastEntry = pFidEntry->pStUcastTbl;

        while (pStMcastEntry != NULL)
        {
            if ((MEMCMP (pStMcastEntry->ForbiddenPorts, gNullPortList,
                         sizeof (tLocalPortList))) != 0)
            {
                if ((i4RetVal =
                     VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                                &pAttrRpQMsg,
                                                VLAN_SET_MCAST_FORBID_MSG,
                                                pCurrEntry->VlanId, 0,
                                                pStMcastEntry->ForbiddenPorts,
                                                (UINT1 *) pStMcastEntry->
                                                MacAddr)) == VLAN_FAILURE)
                {
                    break;
                }
            }

            if ((MEMCMP (pStMcastEntry->EgressPorts, gNullPortList,
                         sizeof (tLocalPortList))) != 0)
            {

                if ((i4RetVal =
                     VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                                &pAttrRpQMsg,
                                                VLAN_PROP_MAC_INFO_MSG,
                                                pCurrEntry->VlanId, 0,
                                                pStMcastEntry->EgressPorts,
                                                (UINT1 *) pStMcastEntry->
                                                MacAddr)) == VLAN_FAILURE)
                {
                    break;
                }
            }

            pStMcastEntry = pStMcastEntry->pNextNode;
        }

        if (i4RetVal == VLAN_FAILURE)
        {
            break;
        }

        if (VlanMrpIsMmrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_TRUE)
        {
            while (pStUcastEntry != NULL)
            {
                if ((MEMCMP (pStUcastEntry->AllowedToGo, gNullPortList,
                             sizeof (tLocalPortList))) != 0)
                {
                    if ((i4RetVal =
                         VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                                    &pAttrRpQMsg,
                                                    VLAN_PROP_MAC_INFO_MSG,
                                                    pCurrEntry->VlanId, 0,
                                                    pStUcastEntry->AllowedToGo,
                                                    (UINT1 *) pStUcastEntry->
                                                    MacAddr)) == VLAN_FAILURE)
                    {
                        break;
                    }
                }
                pStUcastEntry = pStUcastEntry->pNextNode;
            }
            if (i4RetVal == VLAN_FAILURE)
            {
                break;
            }
        }

#ifdef VLAN_EXTENDED_FILTER

        if ((MEMCMP (pCurrEntry->AllGrps.ForbiddenPorts, gNullPortList,
                     sizeof (tLocalPortList))) != 0)
        {

            if ((i4RetVal =
                 VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                            &pAttrRpQMsg,
                                            VLAN_SET_FWDALL_FORBID_MSG,
                                            pCurrEntry->VlanId, 0,
                                            pCurrEntry->AllGrps.ForbiddenPorts,
                                            NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }

        if ((MEMCMP (pCurrEntry->AllGrps.StaticPorts, gNullPortList,
                     sizeof (tLocalPortList))) != 0)
        {
            if ((i4RetVal =
                 VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                            &pAttrRpQMsg,
                                            VLAN_PROP_FWDALL_INFO_MSG,
                                            pCurrEntry->VlanId, 0,
                                            pCurrEntry->AllGrps.StaticPorts,
                                            NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }

        if ((MEMCMP (pCurrEntry->UnRegGrps.ForbiddenPorts, gNullPortList,
                     sizeof (tLocalPortList))) != 0)
        {

            if ((i4RetVal =
                 VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                            &pAttrRpQMsg,
                                            VLAN_SET_FWDUNREG_FORBID_MSG,
                                            pCurrEntry->VlanId, 0,
                                            pCurrEntry->UnRegGrps.
                                            ForbiddenPorts,
                                            NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }

        if ((MEMCMP (pCurrEntry->UnRegGrps.StaticPorts, gNullPortList,
                     sizeof (tLocalPortList))) != 0)
        {
            if (pCurrEntry->pStVlanEntry != NULL)
            {
                MEMCPY (pFwdUnRegPortList, pCurrEntry->UnRegGrps.StaticPorts,
                        sizeof (tLocalPortList));

                pu1LocalPortList = UtilPlstAllocLocalPortList
                    (sizeof (tLocalPortList));
                if (pu1LocalPortList == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Memory allocation failed for tLocalPortList\n");
                    UtilPlstReleaseLocalPortList (pFwdUnRegPortList);
                    return;
                }
                MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
                VLAN_GET_CURR_EGRESS_PORTS (pCurrEntry, pu1LocalPortList);

                VLAN_AND_PORT_LIST (pFwdUnRegPortList, pu1LocalPortList);
                UtilPlstReleaseLocalPortList (pu1LocalPortList);
            }
            else
            {
                MEMCPY (pFwdUnRegPortList, pCurrEntry->UnRegGrps.StaticPorts,
                        sizeof (tLocalPortList));
            }

            if ((i4RetVal =
                 VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                            &pAttrRpQMsg,
                                            VLAN_PROP_FWDUNREG_INFO_MSG,
                                            pCurrEntry->VlanId, 0,
                                            pFwdUnRegPortList,
                                            NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }
#else
        UNUSED_PARAM (pFwdUnRegPortList);
        UNUSED_PARAM (pu1LocalPortList);
#endif
    }
    UtilPlstReleaseLocalPortList (pFwdUnRegPortList);

    if ((pAttrRpQMsg != NULL) && (i4RetVal == VLAN_SUCCESS))
    {
        VlanAttrRPPostBulkCfgMessage (VLAN_CURR_CONTEXT_ID (), pAttrRpQMsg);
        pAttrRpQMsg = NULL;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPropStaticMacInfoForPort                     */
/*                                                                           */
/*    Description         : This function is invoked whenever Vlan is        */
/*                          enabled. All Static Multicast / Unicast          */
/*                          information will                                 */
/*                          be propagated to other Vlan bridges.             */
/*                                                                           */
/*    Input(s)            : u2Port - LocalPort Identifier                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanCurrTable                             */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPropStaticMacInfoForPort (UINT2 u2Port)
{
    tVlanId             VlanId;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    tVlanFidEntry      *pFidEntry = NULL;
    VOID               *pAttrRpQMsg = NULL;
    INT4                i4RetVal = VLAN_SUCCESS;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanMrpIsMmrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_FALSE)
    {
        return;
    }

    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "Propagating Static Multicast information \n ");

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        /* If the VLAN is mapped to ESP VLAN list then should not 
         * be given to MRP/GARP */
        if (VlanPbbTeVidIsEspVid (VLAN_CURR_CONTEXT_ID (), VlanId) == VLAN_TRUE)
        {
            continue;
        }
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (NULL == pCurrEntry)
        {
            continue;
        }
        pStMcastEntry = pCurrEntry->pStMcastTable;

        pFidEntry = VLAN_GET_FID_ENTRY (pCurrEntry->u4FidIndex);
        pStUcastEntry = pFidEntry->pStUcastTbl;

        while (pStMcastEntry != NULL)
        {
            VLAN_IS_MEMBER_PORT (pStMcastEntry->ForbiddenPorts, u2Port,
                                 u1Result);
            if (u1Result == VLAN_TRUE)
            {
                if ((i4RetVal = VlanAttrRPFillBulkMessage
                     (VLAN_CURR_CONTEXT_ID (), &pAttrRpQMsg,
                      VLAN_SET_FWDALL_FORBID_MSG, pCurrEntry->VlanId,
                      u2Port, gNullPortList, (UINT1 *) pStMcastEntry->MacAddr))
                    == VLAN_FAILURE)
                {
                    break;
                }
                pStMcastEntry = pStMcastEntry->pNextNode;
            }

            VLAN_IS_MEMBER_PORT (pStMcastEntry->EgressPorts, u2Port, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                if ((i4RetVal = VlanAttrRPFillBulkMessage
                     (VLAN_CURR_CONTEXT_ID (), &pAttrRpQMsg,
                      VLAN_PROP_MAC_INFO_MSG, pCurrEntry->VlanId, u2Port,
                      gNullPortList,
                      (UINT1 *) pStMcastEntry->MacAddr)) == VLAN_FAILURE)
                {
                    break;
                }
            }
            pStMcastEntry = pStMcastEntry->pNextNode;
        }

        if (i4RetVal == VLAN_FAILURE)
        {
            break;
        }

        while (pStUcastEntry != NULL)
        {
            VLAN_IS_MEMBER_PORT (pStUcastEntry->AllowedToGo, u2Port, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                if ((i4RetVal = VlanAttrRPFillBulkMessage
                     (VLAN_CURR_CONTEXT_ID (), &pAttrRpQMsg,
                      VLAN_PROP_MAC_INFO_MSG, pCurrEntry->VlanId, u2Port,
                      gNullPortList,
                      (UINT1 *) pStUcastEntry->MacAddr)) == VLAN_FAILURE)
                {
                    break;
                }
            }
            pStUcastEntry = pStUcastEntry->pNextNode;
        }
        if (i4RetVal == VLAN_FAILURE)
        {
            break;
        }

#ifdef VLAN_EXTENDED_FILTER
        VLAN_IS_MEMBER_PORT (pCurrEntry->AllGrps.ForbiddenPorts,
                             u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {

            if ((i4RetVal = VlanAttrRPFillBulkMessage
                 (VLAN_CURR_CONTEXT_ID (), &pAttrRpQMsg,
                  VLAN_SET_FWDALL_FORBID_MSG, pCurrEntry->VlanId, u2Port,
                  gNullPortList, NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }

        VLAN_IS_MEMBER_PORT (pCurrEntry->AllGrps.StaticPorts, u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            if ((i4RetVal = VlanAttrRPFillBulkMessage
                 (VLAN_CURR_CONTEXT_ID (), &pAttrRpQMsg,
                  VLAN_PROP_FWDALL_INFO_MSG, pCurrEntry->VlanId, u2Port,
                  gNullPortList, NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }

        VLAN_IS_MEMBER_PORT (pCurrEntry->UnRegGrps.ForbiddenPorts,
                             u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            if ((i4RetVal = VlanAttrRPFillBulkMessage
                 (VLAN_CURR_CONTEXT_ID (), &pAttrRpQMsg,
                  VLAN_PROP_FWDUNREG_INFO_MSG, pCurrEntry->VlanId,
                  u2Port, gNullPortList, NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }

        VLAN_IS_MEMBER_PORT (pCurrEntry->UnRegGrps.StaticPorts,
                             u2Port, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            if ((i4RetVal = VlanAttrRPFillBulkMessage
                 (VLAN_CURR_CONTEXT_ID (), &pAttrRpQMsg,
                  VLAN_PROP_FWDUNREG_INFO_MSG, pCurrEntry->VlanId,
                  u2Port, gNullPortList, NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }
#endif
    }

    if ((pAttrRpQMsg != NULL) && (i4RetVal == VLAN_SUCCESS))
    {
        VlanAttrRPPostBulkCfgMessage (VLAN_CURR_CONTEXT_ID (), pAttrRpQMsg);
        pAttrRpQMsg = NULL;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPropagateStaticVlanInfo                      */
/*                                                                           */
/*    Description         : This function is invoked whenever Vlan is        */
/*                          enabled. All Static VLAN information will        */
/*                          be propagated to other Vlan bridges.             */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->pStaticVlanTable                          */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPropagateStaticVlanInfo (VOID)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT1              *pu1LocalPortList = NULL;
    VOID               *pAttrRpQMsg = NULL;
    INT4                i4RetVal = VLAN_SUCCESS;
    UINT1               u1Result = VLAN_FALSE;

    if ((VlanGvrpIsGvrpEnabledInContext (VLAN_CURR_CONTEXT_ID ()) == GVRP_FALSE)
        && (VlanMrpIsMvrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_FALSE))
    {
        return;
    }

    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
              "Propagating Static VLAN information. \n ");
    pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1LocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Memory allocation failed for tLocalPortList\n");
        return;
    }

    pStVlanEntry = gpVlanContextInfo->pStaticVlanTable;

    while (pStVlanEntry != NULL)
    {
        /* If the VLAN is mapped to ESP VLAN list then should not 
         * be given to MRP/GARP */
        if (VlanPbbTeVidIsEspVid (VLAN_CURR_CONTEXT_ID (),
                                  pStVlanEntry->VlanId) == VLAN_TRUE)
        {
            pStVlanEntry = pStVlanEntry->pNextNode;
            continue;
        }
        VLAN_IS_NO_EGRESS_PORTS_PRESENT (pStVlanEntry, u1Result)
            if (u1Result == VLAN_FALSE)
        {
            MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
            VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

            if ((i4RetVal =
                 VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                            &pAttrRpQMsg,
                                            VLAN_PROP_VLAN_INFO_MSG,
                                            pStVlanEntry->VlanId, 0,
                                            pu1LocalPortList,
                                            NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }

        VLAN_IS_NO_FORBIDDEN_PORTS_PRESENT (pStVlanEntry, u1Result);
        if (u1Result == VLAN_FALSE)
        {
            MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
            VLAN_GET_FORBIDDEN_PORTS (pStVlanEntry, pu1LocalPortList);
            if ((i4RetVal =
                 VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                            &pAttrRpQMsg,
                                            VLAN_SET_VLAN_FORBID_MSG,
                                            pStVlanEntry->VlanId, 0,
                                            pu1LocalPortList,
                                            NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }

        pStVlanEntry = pStVlanEntry->pNextNode;
    }

    if ((pAttrRpQMsg != NULL) && (i4RetVal == VLAN_SUCCESS))
    {
        VlanAttrRPPostBulkCfgMessage (VLAN_CURR_CONTEXT_ID (), pAttrRpQMsg);
        pAttrRpQMsg = NULL;
    }
    UtilPlstReleaseLocalPortList (pu1LocalPortList);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPropStaticVlanInfoForPort                    */
/*                                                                           */
/*    Description         : This function is invoked whenever Vlan is        */
/*                          enabled. All Static VLAN information on this     */
/*                          port will be propagated to other Vlan bridges.   */
/*                                                                           */
/*    Input(s)            : u2Port - LocalPort Identifier                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->pStaticVlanTable        */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPropStaticVlanInfoForPort (UINT2 u2Port)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    VOID               *pAttrRpQMsg = NULL;
    INT4                i4RetVal = VLAN_SUCCESS;
    UINT1               u1Result = OSIX_FALSE;

    if (VlanMrpIsMvrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_FALSE)
    {
        return;
    }
    VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                   "Propagating Static VLAN info for port %d. \n ", u2Port);

    pStVlanEntry = gpVlanContextInfo->pStaticVlanTable;

    while (pStVlanEntry != NULL)
    {
        /* If the VLAN is mapped to ESP VLAN list then should not 
         * be given to MRP/GARP */
        if (VlanPbbTeVidIsEspVid (VLAN_CURR_CONTEXT_ID (),
                                  pStVlanEntry->VlanId) == VLAN_TRUE)
        {
            pStVlanEntry = pStVlanEntry->pNextNode;
            continue;
        }
        VLAN_IS_EGRESS_PORT (pStVlanEntry, u2Port, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            if ((i4RetVal =
                 VlanAttrRPFillBulkMessage
                 (VLAN_CURR_CONTEXT_ID (), &pAttrRpQMsg,
                  VLAN_PROP_VLAN_INFO_MSG, pStVlanEntry->VlanId,
                  u2Port, gNullPortList, NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }

        VLAN_IS_FORBIDDEN_PORT (pStVlanEntry, u2Port, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            if ((i4RetVal =
                 VlanAttrRPFillBulkMessage
                 (VLAN_CURR_CONTEXT_ID (), &pAttrRpQMsg,
                  VLAN_SET_VLAN_FORBID_MSG, pStVlanEntry->VlanId,
                  u2Port, gNullPortList, NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }

        pStVlanEntry = pStVlanEntry->pNextNode;
    }

    if ((pAttrRpQMsg != NULL) && (i4RetVal == VLAN_SUCCESS))
    {
        VlanAttrRPPostBulkCfgMessage (VLAN_CURR_CONTEXT_ID (), pAttrRpQMsg);
        pAttrRpQMsg = NULL;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPropagateNewPortInfo ()                      */
/*                                                                           */
/*    Description         : This function is invoked whenever a port is      */
/*                          created. The port membership is propagated to    */
/*                          other Bridges, if the port belongs to the        */
/*                          static ports of Static Vlan/Static Multicast/    */
/*                          Static All Groups/Static Unreg Groups entry.     */
/*                                                                           */
/*    Input(s)            : u2Port                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->pStaticVlanTable, gpVlanContextInfo->apVlanCurrTable.       */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPropagateNewPortInfo (UINT2 u2Port)
{
    VlanPropagateVlanInfoOnPort (u2Port);
    VlanPropagateMACInfoOnPort (u2Port);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPropagateVlanInfoOnPort ()                   */
/*                                                                           */
/*    Description         : This function is invoked whenever a port is      */
/*                          created. The port membership is propagated to    */
/*                          other Bridges, if the port belongs to the        */
/*                          static ports of Static Vlan                      */
/*                                                                           */
/*    Input(s)            : u2Port                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->pStaticVlanTable,       */
/*                                gpVlanContextInfo->apVlanCurrTable.        */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPropagateVlanInfoOnPort (UINT2 u2Port)
{
    UINT1              *pPortList = NULL;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    VOID               *pAttrRpQMsg = NULL;
    UINT1               u1Result = 0;
    INT4                i4RetVal = VLAN_SUCCESS;

    if ((VlanGvrpIsGvrpEnabledInContext (VLAN_CURR_CONTEXT_ID ()) == GVRP_FALSE)
        && (VlanMrpIsMvrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_FALSE))
    {
        return;
    }
    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanPropagateVlanInfoOnPort: Error in allocating memory "
                  "for pPortList\r\n");
        return;
    }

    MEMSET (pPortList, 0, sizeof (tLocalPortList));

    VLAN_SET_MEMBER_PORT (pPortList, u2Port);

    pStVlanEntry = gpVlanContextInfo->pStaticVlanTable;

    while (pStVlanEntry != NULL)
    {
        /* If the VLAN is mapped to ESP VLAN list then should not 
         * be given to MRP/GARP */
        if (VlanPbbTeVidIsEspVid (VLAN_CURR_CONTEXT_ID (),
                                  pStVlanEntry->VlanId) == VLAN_TRUE)
        {
            pStVlanEntry = pStVlanEntry->pNextNode;
            continue;
        }
        VLAN_IS_EGRESS_PORT (pStVlanEntry, u2Port, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            if ((i4RetVal =
                 VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                            &pAttrRpQMsg,
                                            VLAN_PROP_VLAN_INFO_MSG,
                                            pStVlanEntry->VlanId, 0,
                                            pPortList, NULL)) == VLAN_FAILURE)
            {
                break;
            }

        }
        else
        {
            VLAN_IS_FORBIDDEN_PORT (pStVlanEntry, u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                if ((i4RetVal =
                     VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                                &pAttrRpQMsg,
                                                VLAN_SET_VLAN_FORBID_MSG,
                                                pStVlanEntry->VlanId, 0,
                                                pPortList, NULL))
                    == VLAN_FAILURE)
                {
                    break;
                }

            }
        }

        pStVlanEntry = pStVlanEntry->pNextNode;
    }
    UtilPlstReleaseLocalPortList (pPortList);
    if ((pAttrRpQMsg != NULL) && (i4RetVal == VLAN_SUCCESS))
    {
        VlanAttrRPPostBulkCfgMessage (VLAN_CURR_CONTEXT_ID (), pAttrRpQMsg);
        pAttrRpQMsg = NULL;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPropagateMACInfoOnPort ()                    */
/*                                                                           */
/*    Description         : This function is invoked whenever a port is      */
/*                          created. The port membership is propagated to    */
/*                          other Bridges, if the port belongs to the        */
/*                          static ports of Static Multicast/                */
/*                          Static All Groups/Static Unreg Groups entry.     */
/*                                                                           */
/*    Input(s)            : u2Port                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->pStaticVlanTable,       */
/*                                gpVlanContextInfo->apVlanCurrTable.        */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPropagateMACInfoOnPort (UINT2 u2Port)
{
    UINT1              *pPortList = NULL;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    tVlanFidEntry      *pFidEntry = NULL;
    VOID               *pAttrRpQMsg = NULL;
    tVlanId             VlanId;
    UINT1               u1Result = VLAN_TRUE;
    INT4                i4RetVal = VLAN_SUCCESS;

    if ((VlanGmrpIsGmrpEnabledInContext (VLAN_CURR_CONTEXT_ID ()) == GARP_FALSE)
        && (VlanMrpIsMmrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_FALSE))
    {
        return;
    }
    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanPropagateMACInfoOnPort: Error in allocating memory "
                  "for pPortList\r\n");
        return;
    }

    MEMSET (pPortList, 0, sizeof (tLocalPortList));

    VLAN_SET_MEMBER_PORT (pPortList, u2Port);

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);

        /* If the VLAN is mapped to ESP VLAN list then should not 
         * be given to MRP/GARP */
        if (VlanPbbTeVidIsEspVid (VLAN_CURR_CONTEXT_ID (), VlanId) == VLAN_TRUE)
        {
            continue;
        }
        if (NULL == pCurrEntry)
        {
            continue;
        }

#ifdef VLAN_EXTENDED_FILTER

        VLAN_IS_MEMBER_PORT (pCurrEntry->AllGrps.ForbiddenPorts,
                             u2Port, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            if ((i4RetVal =
                 VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                            &pAttrRpQMsg,
                                            VLAN_SET_FWDALL_FORBID_MSG,
                                            pCurrEntry->VlanId, 0,
                                            pPortList, NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }
        else
        {
            VLAN_IS_MEMBER_PORT (pCurrEntry->AllGrps.StaticPorts,
                                 u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                if ((i4RetVal =
                     VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                                &pAttrRpQMsg,
                                                VLAN_PROP_FWDALL_INFO_MSG,
                                                pCurrEntry->VlanId, 0,
                                                pPortList, NULL))
                    == VLAN_FAILURE)
                {
                    break;
                }
            }
        }

        VLAN_IS_MEMBER_PORT (pCurrEntry->UnRegGrps.ForbiddenPorts,
                             u2Port, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            if ((i4RetVal =
                 VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                            &pAttrRpQMsg,
                                            VLAN_SET_FWDUNREG_FORBID_MSG,
                                            pCurrEntry->VlanId, 0,
                                            pPortList, NULL)) == VLAN_FAILURE)
            {
                break;
            }
        }
        else
        {
            VLAN_IS_MEMBER_PORT (pCurrEntry->UnRegGrps.StaticPorts,
                                 u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                if ((i4RetVal =
                     VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                                &pAttrRpQMsg,
                                                VLAN_PROP_FWDUNREG_INFO_MSG,
                                                pCurrEntry->VlanId, 0,
                                                pPortList, NULL))
                    == VLAN_FAILURE)
                {
                    break;
                }
            }
        }
#endif

        pStMcastEntry = pCurrEntry->pStMcastTable;

        while (pStMcastEntry != NULL)
        {
            VLAN_IS_MEMBER_PORT (pStMcastEntry->ForbiddenPorts,
                                 u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                if ((i4RetVal =
                     VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                                &pAttrRpQMsg,
                                                VLAN_SET_MCAST_FORBID_MSG,
                                                pCurrEntry->VlanId, 0,
                                                pPortList,
                                                (UINT1 *) pStMcastEntry->
                                                MacAddr)) == VLAN_FAILURE)
                {
                    break;
                }
            }
            else
            {
                VLAN_IS_MEMBER_PORT (pStMcastEntry->EgressPorts,
                                     u2Port, u1Result)
                    if (u1Result == VLAN_TRUE)
                {
                    if ((i4RetVal =
                         VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                                    &pAttrRpQMsg,
                                                    VLAN_PROP_MAC_INFO_MSG,
                                                    pCurrEntry->VlanId, 0,
                                                    pPortList,
                                                    (UINT1 *) pStMcastEntry->
                                                    MacAddr)) == VLAN_FAILURE)
                    {
                        break;
                    }
                }
            }

            pStMcastEntry = pStMcastEntry->pNextNode;
        }

        /* Send the Unicast Information to MMRP */
        if (VlanMrpIsMmrpEnabled (VLAN_CURR_CONTEXT_ID ()) == OSIX_TRUE)
        {
            pFidEntry = VLAN_GET_FID_ENTRY (pCurrEntry->u4FidIndex);
            pStUcastEntry = pFidEntry->pStUcastTbl;

            while (pStUcastEntry != NULL)
            {
                VLAN_IS_MEMBER_PORT (pStUcastEntry->AllowedToGo,
                                     u2Port, u1Result);

                if (u1Result == VLAN_TRUE)
                {
                    if ((i4RetVal =
                         VlanAttrRPFillBulkMessage (VLAN_CURR_CONTEXT_ID (),
                                                    &pAttrRpQMsg,
                                                    VLAN_PROP_MAC_INFO_MSG,
                                                    pCurrEntry->VlanId, 0,
                                                    pPortList,
                                                    (UINT1 *) pStUcastEntry->
                                                    MacAddr)) == VLAN_FAILURE)
                    {
                        break;
                    }
                }

                pStUcastEntry = pStUcastEntry->pNextNode;
            }
        }
    }
    UtilPlstReleaseLocalPortList (pPortList);
    if ((pAttrRpQMsg != NULL) && (i4RetVal == VLAN_SUCCESS))
    {
        VlanAttrRPPostBulkCfgMessage (VLAN_CURR_CONTEXT_ID (), pAttrRpQMsg);
        pAttrRpQMsg = NULL;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIngressFiltering                             */
/*                                                                           */
/*    Description         : Applies the ingress rules on the received packet.*/
/*                                                                           */
/*    Input(s)            : pVlanRec - Pointer to the vlan record.           */
/*                          pVlanTag - Pointer to the vlan tag.              */
/*                          u2InPort - Port on which the packet is received. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FORWARD on success - The packet has to be    */
/*                                                   forwarded.              */
/*                         VLAN_NO_FORWARD on failure - The packte has to be */
/*                                                   filtered.               */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIngressFiltering (tVlanCurrEntry * pVlanRec, UINT2 u2InPort)
{
    UINT1               u1Result = VLAN_FALSE;
    tVlanPortEntry     *pPortRec = NULL;
    UINT4               u4Port;

    if (u2InPort >= VLAN_MAX_PORTS + 1)
    {
        return VLAN_NO_FORWARD;
    }
    pPortRec = VLAN_GET_PORT_ENTRY (u2InPort);

    if (pPortRec->u1IngFiltering == VLAN_SNMP_TRUE)
    {
        VLAN_IS_CURR_EGRESS_PORT (pVlanRec, u2InPort, u1Result);

        if (u1Result != VLAN_TRUE)
        {
            u4Port = VLAN_GET_IFINDEX (u2InPort);
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                           "Frame received on port %d for Vlan %d filtered due "
                           "to Ingress filtering. \n",
                           u4Port, pVlanRec->VlanId);

            return VLAN_NO_FORWARD;
        }
    }

    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanEgressFiltering                              */
/*                                                                           */
/*    Description         : This function checks whether the given port is   */
/*                          the member port of the VLAN entry. This function */
/*                          also checks whether the given port is valid or   */
/*                          not.                                             */
/*                                                                           */
/*    Input(s)            : u2Port - Port on which the frame is going to be  */
/*                          transmitted.                                     */
/*                          pVlanEntry - Pointer to the VLAN entry.          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FORWARD / VLAN_NO_FORWARD                    */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanEgressFiltering (UINT2 u2Port, tVlanCurrEntry * pVlanEntry,
                     tMacAddr DestAddr)
{
    tVlanPortEntry     *pPortEntry;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1PortState;
    UINT4               u4Port;

    UNUSED_PARAM (DestAddr);

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return VLAN_NO_FORWARD;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {

        return VLAN_NO_FORWARD;
    }

    if (pVlanEntry == NULL)
    {
        return VLAN_NO_FORWARD;
    }
    /* Check added to ensure that it is not forwarded
     * on the higher member of the aggregated port number */
    if (pPortEntry->u1OperStatus == VLAN_OPER_DOWN)
    {
        return VLAN_NO_FORWARD;
    }

    /* Egress Filtering */

    VLAN_IS_CURR_EGRESS_PORT (pVlanEntry, u2Port, u1Result);

    if (u1Result != VLAN_TRUE)
    {
        /* 
         * EGRESS FILTERING - Out Port should be in the member set
         * for this VLAN ID.
         */

        u4Port = VLAN_GET_IFINDEX (u2Port);
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Outbound port %d is NOT a member port of Vlan %d."
                       "Frame filtered. \n", u4Port, pVlanEntry->VlanId);

        return VLAN_NO_FORWARD;
    }

    /* Forward frames only if the outbound port is in forwarding state. */
    u1PortState = VlanL2IwfGetVlanPortState (pVlanEntry->VlanId,
                                             (UINT2) pPortEntry->u4IfIndex);

    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);
    if (u1PortState != AST_PORT_STATE_FORWARDING)
    {
        if (pVlanPbPortEntry != NULL)
        {
            if ((VLAN_DESTADDR_IS_STPADDR (DestAddr) == VLAN_TRUE) &&
                (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE) &&
                ((pVlanPbPortEntry->u1PbPortType) == VLAN_CUSTOMER_EDGE_PORT) &&
                (VLAN_STP_TUNNEL_STATUS (pPortEntry) ==
                 VLAN_TUNNEL_PROTOCOL_PEER))
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                               "C-BPDU's destined for PEP ports are not filtered "
                               "when CEP is in discarding state\n",
                               pPortEntry->u4IfIndex);
                return VLAN_FORWARD;
            }
        }

        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Outbound Port %d is NOT in Forwarding state. "
                       "Frame Filtered. \n", pPortEntry->u4IfIndex);

        return VLAN_NO_FORWARD;
    }

    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanProcessUcastDataPkt                          */
/*                                                                           */
/*    Description         : Handles the received unicast data packet.        */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet.       */
/*                          pVlanIf   - Pointer to the bridge IF message.     */
/*                          pVlanRec - Pointer to the vlan record.           */
/*                          pVlanTag - Pointer to the vlan tag.              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
VlanProcessUcastDataPkt (tCRU_BUF_CHAIN_DESC * pFrame, tVlanIfMsg * pVlanIf,
                         tVlanCurrEntry * pVlanRec, tVlanTag * pVlanTag)
{
#ifndef NPAPI_WANTED
    tVlanPortInfoPerVlan *pPortInfoPerVlan = NULL;
    UINT2               u2InPort;
    tMacAddr            DestAddr;
    UINT1               u1PortState;
#endif
    BOOL1               bFlag = VLAN_FALSE;

    /* All unicast packet will be switched by H/W */
#ifdef NPAPI_WANTED
#ifdef VXLAN_WANTED
    if (VxlanIsIngressReplicaEnabledForVlan ((INT4) pVlanRec->VlanId) ==
        VXLAN_SUCCESS)
    {
        VlanFwdUnicastDataPacket (pFrame, pVlanIf, pVlanRec, pVlanTag);
    }
    UNUSED_PARAM (bFlag);
#else
    UNUSED_PARAM (pVlanRec);
    UNUSED_PARAM (pVlanIf);
    UNUSED_PARAM (pVlanTag);
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (bFlag);

    return;
#endif
#else
    u2InPort = VLAN_IFMSG_PORT (pVlanIf);
    VLAN_MEMCPY (DestAddr, &VLAN_IFMSG_DST_ADDR (pVlanIf), ETHERNET_ADDR_SIZE);

    /* Forward frames only if the reception port is in forwarding state. 
     * pass IfIndex to L2Iwf call */
    u1PortState = VlanL2IwfGetVlanPortState (pVlanRec->VlanId,
                                             (UINT2) pVlanIf->u4IfIndex);

    if (u1PortState != AST_PORT_STATE_FORWARDING)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Reception Port %d is NOT in Forwarding state. "
                       "Unicast Frame dropped.\n", pVlanIf->u4IfIndex);

        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2InPort);

            if (pPortInfoPerVlan != NULL)
            {
                VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
            }
        }
        VlanL2IwfIncrFilterInDiscards (u2InPort);

        return;
    }

    if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
    {
        pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2InPort);

        if (pPortInfoPerVlan != NULL)
        {
            bFlag = pPortInfoPerVlan->b1LckStatus;
        }
    }

    if (bFlag == VLAN_TRUE)
    {
        if (pPortInfoPerVlan != NULL)
        {
            VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
        }
        VlanL2IwfIncrFilterInDiscards (u2InPort);
        return;
    }
    VlanFwdUnicastDataPacket (pFrame, pVlanIf, pVlanRec, pVlanTag);
#endif /* End of !NPAPI_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanLearn                                        */
/*                                                                           */
/*    Description         : Learns the mac vlan association from Vlan MacMap */
/*                          table.                                           */
/*                                                                           */
/*    Input(s)            : pMacAddr - Pointer to the Mac Address.           */
/*                          u2InPort - The port on which the frame           */
/*                                     is received.                          */
/*                          pVlanRec - Pointer to the vlan record.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->VlanInfo                                  */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanLearn (tMacAddr MacAddr, UINT2 u2InPort, tVlanCurrEntry * pVlanRec)
{
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanFdbEntry      *pFdbEntry = NULL;
    tVlanWildCardEntry *pWildCardEntry = NULL;
    tVlanSrcRelearnTrap VlanSrcRelearnTrap;
    UINT4               u4FidIndex = pVlanRec->u4FidIndex;
    UINT1               u1PortState;
    UINT1               u1Result = VLAN_FALSE;
#ifdef NPAPI_WANTED
    tLocalPortList      AllowedToGoPortList;
    tMacAddr            ConnectionId;
    UINT4               u4TempContext = 0;
    UINT2               u2LocalPort = 0;
#endif
#ifdef EVPN_VXLAN_WANTED
    tVlanBasicInfo      VlanBasicInfo;
    UINT4               u4IfIndex = 0;
    tCfaIfInfo          IfInfo;
#endif

#ifdef EVPN_WANTED
    MEMSET (&VlanBasicInfo, 0, sizeof (tVlanBasicInfo));
#endif
    MEMSET (&VlanSrcRelearnTrap, 0, sizeof (tVlanSrcRelearnTrap));
    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2InPort);

    if (pVlanPortEntry == NULL)
    {
        return;
    }

    /* Check if the source address is Multicast address */
    if (VLAN_IS_MCASTADDR (MacAddr))
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC,
                       VLAN_NAME,
                       "No Learning of multicast Address in port %d \n",
                       pVlanPortEntry->u4IfIndex);
        return;

    }

    if (u2InPort >= VLAN_MAX_PORTS + 1)
    {
        return;
    }
    u1PortState = VlanL2IwfGetVlanPortState (pVlanRec->VlanId,
                                             VLAN_GET_IFINDEX (u2InPort));

    if ((u1PortState == AST_PORT_STATE_LEARNING) ||
        (u1PortState == AST_PORT_STATE_FORWARDING))
    {

        if (VLAN_CONTROL_OPER_MAC_LEARNING_STATUS (pVlanRec->VlanId)
            == VLAN_DISABLED)
        {
            return;
        }

        pFdbEntry = VlanGetUcastEntry (u4FidIndex, MacAddr);

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);
        /*If Enhance filtering utility enabled for u2InPort, check if
         * learning is enable for this port.
         */
        if (pVlanPortEntry->u2FiltUtilityCriteria ==
            VLAN_ENHANCE_FILTERING_CRITERIA)
        {
            VLAN_IS_MAC_LEARN_PORT (pFidEntry, u2InPort, u1Result);

            if (u1Result == VLAN_FALSE)
            {
                return;
            }
        }
        else
        {
            /*If Default filtering is enable, check if vlan has at least one
             * member port
             */
            VLAN_IS_NO_CURR_EGRESS_PORTS_PRESENT (pVlanRec, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                return;
            }
        }
        /* if Wildcard entry present for this Mac, do not learn
         */
        pWildCardEntry = VlanGetWildCardEntry (MacAddr);
        if (pWildCardEntry != NULL)
        {
            return;
        }

        /*if mac learning status of this port is disabled, then do not learn */
        if (pVlanPortEntry->u1MacLearningStatus == VLAN_DISABLED)
        {

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "Mac Learning is disabled on port %d.So No Learning "
                           "in FDB Entry on this port \n",
                           pVlanPortEntry->u4IfIndex);
            return;
        }

        if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
        {
            if (VlanPbIsMacLearningAllowed (pFdbEntry, pVlanPortEntry)
                != VLAN_TRUE)
            {
                return;
            }
        }

        if (pFdbEntry == NULL)
        {
            if (VLAN_DYNAMIC_UNICAST_COUNT == VLAN_DYNAMIC_UNICAST_SIZE)
            {
                if (VlanSendSwitchMacThresholdTrap () == VLAN_FAILURE)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC |
                              ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "Switch Mac Threshold Trap generation failed\n");
                    return;
                }
                VLAN_DYNAMIC_UNICAST_COUNT++;
                return;
            }

            if (VLAN_DYNAMIC_UNICAST_COUNT >= VLAN_DYNAMIC_UNICAST_SIZE)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC |
                          ALL_FAILURE_TRC,
                          VLAN_NAME, "Learning Limit exceeded for Switch\n");
                return;
            }

            if (pFidEntry->u4DynamicUnicastCount ==
                VLAN_CONTROL_MAC_LEARNING_LIMIT (pVlanRec->VlanId))
            {
                if (VlanSendMacThresholdTrap (VLAN_CURR_CONTEXT_ID (),
                                              pVlanRec->VlanId) == VLAN_FAILURE)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC |
                              ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "Mac Threshold Trap generation failed\n");
                    return;
                }
                pFidEntry->u4DynamicUnicastCount++;

                return;
            }

            if (pFidEntry->u4DynamicUnicastCount >=
                VLAN_CONTROL_MAC_LEARNING_LIMIT (pVlanRec->VlanId))
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC |
                          ALL_FAILURE_TRC,
                          VLAN_NAME, "Learning Limit exceeded for Vlan\n");
                return;
            }

            pFdbEntry = (tVlanFdbEntry *) (VOID *) VLAN_GET_BUF (VLAN_FDB_ENTRY,
                                                                 sizeof
                                                                 (tVlanFdbEntry));

            if (pFdbEntry == NULL)
            {

                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME, "No Free FDB Entry \n");
            }
            else
            {

                MEMSET (pFdbEntry, 0, sizeof (tVlanFdbEntry));
                VLAN_CPY_MAC_ADDR (pFdbEntry->MacAddr, MacAddr);

                VLAN_GET_TIMESTAMP (&pFdbEntry->u4TimeStamp);
                pFdbEntry->u2Port = u2InPort;
                pFdbEntry->u1Status = VLAN_FDB_LEARNT;
                pFdbEntry->VlanId = pVlanRec->VlanId;

#ifdef NPAPI_WANTED
                if (IssCPUControlledLearning (u2InPort) == OSIX_SUCCESS)
                {
                    MEMSET (AllowedToGoPortList, 0, sizeof (tLocalPortList));
                    MEMSET (ConnectionId, 0, sizeof (tMacAddr));

                    if (u2InPort != VLAN_INVALID_PORT)
                    {
                        if (VlanGetContextInfoFromIfIndex
                            (u2InPort, &u4TempContext,
                             &u2LocalPort) != VLAN_SUCCESS)
                        {
                            VLAN_RELEASE_BUF (VLAN_FDB_ENTRY,
                                              (UINT1 *) pFdbEntry);
                            return;
                        }
                    }
                    else
                    {
                        u2LocalPort = (UINT2) u2InPort;
                    }
                    /* Program the hardware */
                    VLAN_SET_MEMBER_PORT (AllowedToGoPortList, u2LocalPort);

                    if (VlanHwAddStaticUcastEntry (pFidEntry->u4Fid,
                                                   MacAddr,
                                                   VLAN_DEF_RECVPORT,
                                                   AllowedToGoPortList,
                                                   VLAN_DELETE_ON_TIMEOUT,
                                                   ConnectionId) ==
                        VLAN_FAILURE)
                    {
                        VLAN_RELEASE_BUF (VLAN_FDB_ENTRY, (UINT1 *) pFdbEntry);
                        VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                                  "Addition to NP failed for software learnt MAC entry\n");
                        return;
                    }
                }
#endif

                VlanAddFdbEntry (u4FidIndex, pFdbEntry);

                if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
                {
                    VlanPbUpdateLearntMacCount (pVlanPortEntry, NULL, VLAN_ADD);
                }

                VlanUpdateVlanMacCount (pVlanRec, VLAN_ADD);
            }
        }
        else
        {
            if (VLAN_CONTROL_OPER_MAC_LEARNING_STATUS
                (pVlanRec->VlanId) == VLAN_DISABLED)
            {
                return;
            }

            if (pFdbEntry->u2Port != u2InPort)
            {
                /* Source relearning has occurred
                 * Send a trap and event for syslog
                 * */

                VlanSrcRelearnTrap.u4OldPort = pFdbEntry->u2Port;
                VlanSrcRelearnTrap.u4NewPort = u2InPort;
                VlanSrcRelearnTrap.u4FidIndex = u4FidIndex;
                VlanSrcRelearnTrap.u2VlanId = pVlanRec->VlanId;
                VLAN_CPY_MAC_ADDR (VlanSrcRelearnTrap.MacAddr, MacAddr);

#ifdef SYSLOG_WANTED
                VlanSendSrcRelearnSyslog (&VlanSrcRelearnTrap);
#endif

                if (VlanSendSrcRelearnTrap (&VlanSrcRelearnTrap)
                    == VLAN_FAILURE)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC |
                              ALL_FAILURE_TRC, VLAN_NAME,
                              "Source Relearning Trap Generation Failed\n");
                    /* not returning, since a return statement here won't allow
                     * updation of the new mac-address in the database
                     * */
                }
#ifdef EVPN_VXLAN_WANTED
                pFdbEntry->VlanId = pVlanRec->VlanId;
                pFdbEntry->u2Port = u2InPort;

                if (gaVlanRegTbl[VLAN_VXLAN_EVPN_PROTO_ID].pVlanBasicInfo !=
                    NULL)
                {
                    VlanBasicInfo.u2VlanId = pFdbEntry->VlanId;
                    VlanBasicInfo.u2Port = pFdbEntry->u2Port;
                    VlanBasicInfo.u1EntryStatus = pFdbEntry->u1Status;
                    if (VlanVcmGetIfIndexFromLocalPort
                        (0, pFdbEntry->u2Port, &u4IfIndex) != VCM_FAILURE)
                    {
                        MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
                        VlanCfaGetIfInfo (u4IfIndex, &IfInfo);
                        if (IfInfo.u1IfType == CFA_VXLAN_NVE)
                        {
                            VlanBasicInfo.u2Port = (UINT2) u4IfIndex;
                        }

                        MEMCPY (VlanBasicInfo.MacAddr, pFdbEntry->MacAddr,
                                sizeof (tMacAddr));

                        VlanBasicInfo.u1Action = VLAN_CB_UPDATE_MAC;

                        (gaVlanRegTbl[VLAN_VXLAN_EVPN_PROTO_ID].
                         pVlanBasicInfo) (&VlanBasicInfo);
                    }
                }
#endif

            }
            pFdbEntry->VlanId = pVlanRec->VlanId;
            pFdbEntry->u2Port = u2InPort;
            VLAN_GET_TIMESTAMP (&pFdbEntry->u4TimeStamp);

            if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
            {
                VlanPbUpdateLearntMacCount (pVlanPortEntry,
                                            pFdbEntry, VLAN_UPDATE);
            }
        }
        return;
    }

    VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                   "Reception Port %d is neither in Learning State nor in"
                   " Forwarding State. Source Learning NOT done. \n",
                   pVlanPortEntry->u4IfIndex);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanProcessMcastDataPkt                          */
/*                                                                           */
/*    Description         : Handles the received multicast data packet.      */
/*                                                                           */
/*    Input(s)            : pBrgIf   - Pointer to the bridge IF message.     */
/*                          pFrame   - Pointer to the incoming packet.       */
/*                          pVlanRec - Pointer to the vlan record.           */
/*                          pVlanTag - Pointer to the vlan tag.              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanProcessMcastDataPkt (tCRU_BUF_CHAIN_DESC * pFrame, tVlanIfMsg * pVlanIf,
                         tVlanCurrEntry * pVlanRec, tVlanTag * pVlanTag)
{
    UINT1              *pFwdPortList = NULL;
    UINT2               u2InPort;
    tMacAddr            DestAddr;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    UINT1               u1PortState;
#ifndef NPAPI_WANTED
    tVlanPortInfoPerVlan *pPortInfoPerVlan = NULL;
    tVlanStats         *pVlanCounters;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2Port;
    UINT1               u1PortFlag;
#endif
    INT4                i4RetVal = VLAN_NO_FORWARD;
    tMacAddr            OutDestAddr;
#ifdef NPAPI_WANTED
    INT1                i1ResvAddr;
    UINT1               u1Result = VLAN_FALSE;
#endif
#ifdef MRVLLS
    UINT1               u1Temp;
#endif
    UINT4               u4ContextId = 0;
    tVlanId             SVlanId = 0;

    u2InPort = VLAN_IFMSG_PORT (pVlanIf);
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2InPort);

    u4ContextId = VLAN_CURR_CONTEXT_ID ();

    VLAN_MEMCPY (DestAddr, &VLAN_IFMSG_DST_ADDR (pVlanIf), ETHERNET_ADDR_SIZE);

    u1PortState = VlanL2IwfGetVlanPortState (pVlanRec->VlanId,
                                             (UINT2) pVlanIf->u4IfIndex);

    if (u1PortState != AST_PORT_STATE_FORWARDING)
    {

        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Reception Port %d is NOT in Forwarding state. "
                       "Multicast Frame dropped.\n", pVlanIf->u4IfIndex);

#ifndef NPAPI_WANTED
        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2InPort);
            if (pPortInfoPerVlan != NULL)
            {
                VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
            }
        }
#endif

        VlanL2IwfIncrFilterInDiscards (u2InPort);

        return;
    }

    VLAN_MEMCPY (OutDestAddr, DestAddr, sizeof (tMacAddr));

    if (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        VlanGetSVlanId (pFrame, u2InPort, u4ContextId, &SVlanId);
    }

    i4RetVal = VlanChangeDestAddrAtIngress (pFrame, DestAddr, u2InPort,
                                            OutDestAddr);

    if (i4RetVal == VLAN_FORWARD)
    {
        if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
            && (pVlanPbPortEntry != NULL)
            &&
            ((((pVlanPbPortEntry->u1PbPortType) == VLAN_CUSTOMER_EDGE_PORT)
              || ((pVlanPbPortEntry->u1PbPortType) ==
                  VLAN_PROP_CUSTOMER_EDGE_PORT)
              || ((pVlanPbPortEntry->u1PbPortType) ==
                  VLAN_PROP_CUSTOMER_NETWORK_PORT)
              || ((pVlanPbPortEntry->u1PbPortType) ==
                  VLAN_CNP_PORTBASED_PORT))))
        {
            if (MEMCMP (OutDestAddr, DestAddr, sizeof (tMacAddr)) != 0)
            {
                pVlanTag->OuterVlanTag.u1Priority =
                    VLAN_TUNNEL_BPDU_PRIORITY ();
            }
        }
    }

    if (i4RetVal == VLAN_TUNNEL_OVERRIDE)
    {
        VlanReleaseContext ();
        i4RetVal =
            VlanChangeDestAddrAtIngressPerVlan (pFrame, u4ContextId, SVlanId,
                                                u2InPort, DestAddr,
                                                OutDestAddr);
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return;
        }
    }

    if (i4RetVal == VLAN_NO_FORWARD)
    {

#ifndef NPAPI_WANTED
        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanRec, u2InPort);
            if (pPortInfoPerVlan != NULL)
            {
                VLAN_INC_IN_DISCARDS (pPortInfoPerVlan);
            }
        }
#endif

        VlanL2IwfIncrFilterInDiscards (u2InPort);
        return;
    }

    if ((i4RetVal == VLAN_FORWARD) &&
        (MEMCMP (OutDestAddr, DestAddr, sizeof (tMacAddr)) != 0))
    {
        if ((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE) &&
            (VLAN_PB_PORT_TYPE (u2InPort) == VLAN_PROVIDER_NETWORK_PORT))
        {
            if (MEMCMP (DestAddr, VLAN_TUNNEL_STP_ADDR (), ETHERNET_ADDR_SIZE)
                == 0)
            {
                VLAN_MEMCPY (&VLAN_IFMSG_DST_ADDR (pVlanIf), OutDestAddr,
                             ETHERNET_ADDR_SIZE);
            }
        }
    }

#ifdef NPAPI_WANTED

#ifdef MRVLLS
    if (VLAN_IS_BCASTADDR (DestAddr) == VLAN_TRUE)
    {
#ifdef DHCP_RLY_WANTED
        /* check DCHP relay.. (and suitable packet) */
        if (DhcpIsRelayEnabled ())
        {
            VlanIsDhcpControlPacket (pFrame, u2InPort, &u1Temp);
            if (u1Temp == VLAN_TRUE)
            {
                return;
            }
        }
#endif
    }
    else
#endif
    {
        /* 
         * This multicast packet would have been switched by hardware.
         * In case if GMRP/GVRP is disabled then we need to switch only those
         * GMRP/GVRP packets. Other unknown multicast packets can be dropped.
         */
        /* If tunnel is set, destination mac address would have changed,
         * to provide software path for tunnelling packet, check for
         * changed mac address.
         * If destination mac is not changed OutDestAddr will be same as
         * DestAddr.
         */

        i1ResvAddr = (INT1) VlanCheckReservedGroup (OutDestAddr);

        if ((i1ResvAddr == VLAN_FALSE) &&
            (VLAN_IS_MCAST_RESERVED_ADDR (OutDestAddr) == VLAN_FALSE)
#ifdef VXLAN_WANTED
            && (VxlanIsIngressReplicaEnabledForVlan ((INT4) pVlanRec->VlanId) ==
                VXLAN_FAILURE)
#endif
            )

        {
            VlanIsIgmpControlPacket (pFrame, u2InPort, &u1Result);

            if (u1Result == VLAN_FALSE)
            {
                return;
            }
        }
    }

#endif /* NPAPI_WANTED */
    pFwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pFwdPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanProcessMcastDataPkt: Error in allocating memory "
                  "for pFwdPortList\r\n");
        return;
    }

    MEMSET (pFwdPortList, 0, sizeof (tLocalPortList));

    VlanGetMcastFwdPortList (OutDestAddr, u2InPort, pVlanRec, pFwdPortList);

    VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                   "Multicast Data Packet received on Port %d is to be "
                   "forwarded on Ports ", pVlanIf->u4IfIndex);

    VLAN_PRINT_PORT_LIST (VLAN_MOD_TRC, pFwdPortList, u2InPort);

    if (MEMCMP (pFwdPortList, gNullPortList, sizeof (tLocalPortList)) != 0)
    {

#ifndef NPAPI_WANTED
        if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
        {
            if (VLAN_IS_BCASTADDR (DestAddr) == VLAN_TRUE)
            {
                pVlanCounters = pVlanRec->pVlanStats;
                if (pVlanCounters != NULL)
                {
                    VLAN_INC_OUT_BCAST (pVlanCounters);
                }
            }
            for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
            {
                if (pFwdPortList[u2ByteInd] != 0)
                {
                    u1PortFlag = pFwdPortList[u2ByteInd];
                    for (u2BitIndex = 0;
                         ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                          && (u1PortFlag != 0)); u2BitIndex++)
                    {
                        if ((u1PortFlag & VLAN_BIT8) != 0)
                        {
                            u2Port =
                                (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                         u2BitIndex + 1);

#ifndef NPAPI_WANTED
                            pPortInfoPerVlan =
                                VlanGetPortStatsEntry (pVlanRec, u2Port);

                            if (pPortInfoPerVlan != NULL)
                            {
                                if (pPortInfoPerVlan->b1LckStatus == VLAN_TRUE)
                                {
                                    VLAN_RESET_MEMBER_PORT (pFwdPortList,
                                                            u2Port);
                                }
                            }
                            else
#endif
                            {
                                VLAN_RESET_MEMBER_PORT (pFwdPortList, u2Port);
                            }
                        }
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                    }
                }
            }
        }
#endif
        if (VlanL2VpnPktFloodOnMplsPort (pFrame, pVlanIf, pVlanRec, pVlanTag)
            != VLAN_FORWARD)
        {
            VlanFwdOnPorts (pFrame, pVlanIf,
                            pFwdPortList, pVlanRec, pVlanTag, VLAN_MCAST_FRAME);
        }
    }

    UtilPlstReleaseLocalPortList (pFwdPortList);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFwdOnSinglePort                              */
/*                                                                           */
/*    Description         : The packet is forwarded on given single port.    */
/*                          This function will not take care of adding the   */
/*                          Vlan Id to the priority tagged frames.           */
/*                                                                           */
/*    Input(s)            : pFrame     - Pointer to the incoming packet.     */
/*                          pBrgIf     - Pointer to the bridge IF message.   */
/*                          u2Port     - The Port on which the frame has to  */
/*                                       forwarded                           */
/*                          pVlanEntry - Pointer to the vlan entry in current*/
/*                                       record                              */
/*                          pInVlanTag - Pointer to the vlan tag associated  */
/*                                       with the packet.                    */
/*                          u1FrameType - BRG_CNTL_FRAME/BRG_DATA_FRAME/     */
/*                                        BRG_MCAST_FRAME                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanFwdOnSinglePort (tCRU_BUF_CHAIN_DESC * pFrame, tVlanIfMsg * pVlanIf,
                     UINT2 u2Port, tVlanCurrEntry * pVlanEntry,
                     tVlanTag * pInVlanTag, UINT1 u1FrameType)
{
#ifndef NPAPI_WANTED
    tVlanPortInfoPerVlan *pPortInfoPerVlan = NULL;
#endif
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanPortEntry     *pOutPortEntry = NULL;
    tVlanPbPortEntry   *pVlanPbPortEntry = NULL;
    tVlanOutIfMsg       VlanOutIfMsg;
    tVlanTag            InVlanTag;
    tMacAddr            DestAddr;
    INT4                i4RetVal = VLAN_NO_FORWARD;
    UINT2               u2InPort;
    UINT1               u1TagOut = VLAN_TRUE;
    UINT1               u1RegenPri = 0;
    UINT1               u1DefPriority = 0;
    UINT1               u1IsCustBpdu = VLAN_FALSE;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1IsTunnelEnabled = VLAN_FALSE;
#ifndef NPAPI_WANTED
    UINT1               u1TrfClass;
#endif
    UINT4               u4ContextId = 0;
    tVlanId             SVlanId = 0;

    u2InPort = VLAN_IFMSG_PORT (pVlanIf);

    u4ContextId = VLAN_CURR_CONTEXT_ID ();

    VLAN_MEMSET (&InVlanTag, 0, sizeof (tVlanTag));
    VLAN_MEMSET (&VlanOutIfMsg, 0, sizeof (tVlanOutIfMsg));
    VLAN_MEMSET (&DestAddr, 0, sizeof (DestAddr));

    VLAN_MEMCPY (&InVlanTag, pInVlanTag, sizeof (tVlanTag));
    VLAN_MEMCPY (DestAddr, VLAN_IFMSG_DST_ADDR (pVlanIf), ETHERNET_ADDR_SIZE);

    /* MPLS L2VPN Tagged mode in port will be zero
     * as it is taken care by MPLS */
    if (u2InPort >= VLAN_MAX_PORTS + 1)
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return;
    }

    if (u2InPort != 0)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2InPort);
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {
        return;
    }

    pOutPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (((pPortEntry != NULL) &&
         (pPortEntry->u1PortProtected == VLAN_ENABLED)) &&
        (pOutPortEntry->u1PortProtected == VLAN_ENABLED))
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
        return;
    }

    if (VLAN_DESTADDR_IS_STPADDR (DestAddr) == VLAN_TRUE)
    {
        u1IsCustBpdu = VLAN_TRUE;

    }

    if ((u1IsCustBpdu == VLAN_TRUE) ||
        (VLAN_DESTADDR_IS_GVRPADDR (DestAddr) == VLAN_TRUE) ||
        (VLAN_DESTADDR_IS_MVRPADDR (DestAddr) == VLAN_TRUE))
    {
        /* This is for higher layer control frames. So assign higher priority. */
        u1RegenPri = VLAN_HIGHEST_PRIORITY;
        u1DefPriority = VLAN_HIGHEST_PRIORITY;
    }
    else
    {
        if (VLAN_IS_PKT_RCVD_FROM_MPLS (pVlanIf) == VLAN_TRUE)
        {
            u1DefPriority = pVlanIf->u1Priority;
            if (VLAN_IS_PRIORITY_ENABLED () == VLAN_TRUE)
            {
                u1RegenPri = pVlanIf->u1Priority;
            }
        }
        else
        {
            if (u1FrameType == VLAN_CFM_FRAME)
            {
                u1RegenPri = InVlanTag.OuterVlanTag.u1Priority;
            }
            else
            {
                if (pPortEntry != NULL)
                {
                    u1DefPriority = pPortEntry->u1DefUserPriority;

                    if (VLAN_IS_PRIORITY_ENABLED () == VLAN_TRUE)
                    {
                        u1RegenPri =
                            VlanRegenUserPriority (u2InPort, u1DefPriority);

                        u1RegenPri =
                            VlanQoSRegenerateVlanPriority
                            (u2InPort,
                             InVlanTag.OuterVlanTag.u2VlanId,
                             InVlanTag.OuterVlanTag.u1Priority);

                    }
                }
            }
        }
    }

    /* 
     * Check whether the u2Port is a member of UnTagPorts if
     * a static vlan entry is present for the VLAN 
     */

    if (pVlanEntry != NULL)
    {
        if (pVlanEntry->pStVlanEntry != NULL)
        {
            VLAN_IS_UNTAGGED_PORT (pVlanEntry->pStVlanEntry, u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                u1TagOut = VLAN_FALSE;
            }
        }
    }

    pOutPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    pVlanPbPortEntry = VLAN_GET_PB_PORT_ENTRY (u2Port);

    /* When L2VPN PW is used to transport layer 2 PDU's in RAW mode,
     * the Vlan Entry will not be present. In this case, handling of 
     * Vlan Tags should not be done. */
    if ((VLAN_BASE_BRIDGE_MODE () == DOT_1Q_VLAN_MODE) &&
        (VLAN_HW_EG_UNTAG_SUPPORT () == VLAN_FALSE) && (pVlanEntry != NULL))
    {
        if (VlanHandleTagOutFrame (pFrame, u2Port, pVlanIf, u1TagOut,
                                   &InVlanTag) == VLAN_NO_FORWARD)
        {
            VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
            return;
        }
    }

    if (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        VlanGetSVlanId (pFrame, u2Port, u4ContextId, &SVlanId);
    }

    /* Applying port isolation rule for packets ingressing on ICCL */
    if (pPortEntry != NULL)
    {

        if (VlanGetPortIsoFwdStatus (pPortEntry->u4IfIndex,
                                     pOutPortEntry->u4IfIndex,
                                     InVlanTag.OuterVlanTag.u2VlanId)
            == VLAN_FAILURE)
        {
            /* Egress port is not present in port isolation table.
             * So, do not forward the packet to egress port*/
            VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);

            VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC, VLAN_NAME,
                           "VlanFwdOnSinglePort: Egress port %d is "
                           "not present in port isolation table "
                           "So, do not forward the packet to egress port\r\n",
                           pOutPortEntry->u4IfIndex);
            return;
        }
    }

    /* Revert the DA to STP/GVRP address if the outgoing port is
     * a tunnel port */

    if ((u1FrameType == VLAN_MCAST_FRAME) || (u1FrameType == VLAN_CFM_FRAME))
    {
        u1IsTunnelEnabled = VLAN_IS_TUNNEL_ENABLED (pOutPortEntry);

        if ((u1IsTunnelEnabled == VLAN_TRUE) ||
            ((pVlanPbPortEntry != NULL) &&
             (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE) &&
             (pVlanPbPortEntry->u1PbPortType) ==
             VLAN_PROP_PROVIDER_NETWORK_PORT))
        {
            /* Revert the destination address in the packet */
            i4RetVal = VlanChangeDestAddrAtEgress (pFrame, u2Port);

            if (i4RetVal == VLAN_TUNNEL_OVERRIDE)
            {
                VlanReleaseContext ();
                i4RetVal =
                    VlanChangeDestAddrAtEgressPerVlan (pFrame, u4ContextId,
                                                       SVlanId, u2Port);
                if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
                {
                    return;
                }
            }

            if (i4RetVal == VLAN_NO_FORWARD)
            {
                VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pFrame);
                return;
            }
        }
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        if (pVlanPbPortEntry != NULL)
        {
            if ((pVlanPbPortEntry->u1PbPortType) == VLAN_CUSTOMER_EDGE_PORT)
            {
                /* Outgoing port is CEP. Check the UntaggedCEP status
                 * from CVID registration table and handle the frame 
                 * accordingly.
                 */
                i4RetVal = VlanPbHandlePktTxOnCep
                    (pFrame, u2Port,
                     (tVlanId) InVlanTag.OuterVlanTag.u2VlanId, pVlanIf,
                     u1IsCustBpdu);
                if (i4RetVal == VLAN_NO_FORWARD)
                {
                    /* No need to free the buffer here, as VlanPbHandlePktTxOnCep
                     * takes care of free the cru buffer in case of failures. */
                    return;
                }
            }
        }
    }

    VlanOutIfMsg.u2Port = u2Port;
    VlanOutIfMsg.u2Length = pVlanIf->u2Length;
    VlanOutIfMsg.u1FrameType = u1FrameType;
    VlanOutIfMsg.u4TimeStamp = VLAN_IFMSG_TIME_STAMP (pVlanIf);

#ifndef NPAPI_WANTED
    if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
    {
        if (pVlanEntry != NULL)
        {
            pPortInfoPerVlan = VlanGetPortStatsEntry (pVlanEntry, u2Port);
            if (pPortInfoPerVlan != NULL)
            {
                VLAN_INC_OUT_FRAMES (pPortInfoPerVlan);
            }
        }
    }

    /* Send the buffer here */
    if (VLAN_IS_PRIORITY_ENABLED () == VLAN_TRUE)
    {
        u1TrfClass = VLAN_GET_TRF_CLASS (u2Port, u1RegenPri);
        VlanSendToPriorityQ (pFrame, &VlanOutIfMsg, u1TrfClass);
    }
    else
#endif /* !NPAPI_WANTED */
    {
        UNUSED_PARAM (u1RegenPri);
        VlanHandleOutgoingPkt (pFrame, &VlanOutIfMsg);
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFwdOnPorts                                   */
/*                                                                           */
/*    Description         : The given packte is forwarded on all the ports   */
/*                          in the PortList.                                 */
/*                                                                           */
/*    Input(s)            : pFrame     - Pointer to the incoming packet      */
/*                          pVlanIf     - Pointer to the bridge IF message   */
/*                          fwdPortList - Forwarding Port List               */
/*                          pVlanEntry - Pointer to the vlan entry           */
/*                          pInVlanTag - Pointer to the vlan tag.            */
/*                          u1FrameType - BRG_CNTL_FRAME/BRG_DATA_FRAME/     */
/*                                        BRG_MCAST_FRAME                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanFwdOnPorts (tCRU_BUF_CHAIN_DESC * pFrame, tVlanIfMsg * pVlanIf,
                tLocalPortList FwdPortList, tVlanCurrEntry * pVlanEntry,
                tVlanTag * pInVlanTag, UINT1 u1FrameType)
{
    UINT1              *pPortList = NULL;
    UINT2               u2InPort;
    UINT2               u2Port;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    INT4                i4RetVal = VLAN_NO_FORWARD;
    tCRU_BUF_CHAIN_DESC *pDupBuf = NULL;
    tMacAddr            DestAddr;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT1               u1PortFlag = 0;
    UINT1               u1PortState;
    UINT1               u1Result = VLAN_FALSE;
    UINT2               u2Len = VLAN_INIT_VAL;
    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFwdOnPorts: Error in allocating memory "
                  "for pPortList\r\n");
        return;
    }
    MEMSET (pPortList, 0, sizeof (tLocalPortList));

    MEMCPY (pPortList, FwdPortList, sizeof (tLocalPortList));

    u2InPort = VLAN_IFMSG_PORT (pVlanIf);

    if (u2InPort != VLAN_INVALID_PORT)
    {

        VLAN_RESET_MEMBER_PORT (pPortList, u2InPort);
    }

    VLAN_MEMCPY (DestAddr, VLAN_IFMSG_DST_ADDR (pVlanIf), ETHERNET_ADDR_SIZE);

    /* Copy the original length of the packet which needs to forwarded 
     * on the other valid port of the VLAN in a local variable */
    u2Len = pVlanIf->u2Length;

    for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
    {

        if (pPortList[u2ByteInd] != 0)
        {

            u1PortFlag = pPortList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {

                if ((u1PortFlag & VLAN_BIT8) != 0)
                {

                    u2Port =
                        (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                 u2BitIndex + 1);

                    u2Port = MEM_MAX_BYTES (u2Port, VLAN_MAX_PORTS);
                    i4RetVal =
                        VlanEgressFiltering (u2Port, pVlanEntry, DestAddr);

                    /*if (i4RetVal == no forward), and if the port is not
                     * in vlan egress ports, then the port is from wild card
                     * entry. So check the CIST port state for this port and
                     * forward.
                     */
                    u1Result = VLAN_FALSE;
                    if (pVlanEntry != NULL)
                    {
                        VLAN_IS_CURR_EGRESS_PORT (pVlanEntry, u2Port, u1Result);
                    }

                    if ((i4RetVal == VLAN_NO_FORWARD) &&
                        (u1Result == VLAN_FALSE))
                    {
                        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

                        if (pPortEntry != NULL)
                        {
                            u1PortState = L2IwfGetInstPortState
                                (MST_CIST_CONTEXT,
                                 (UINT2) pPortEntry->u4IfIndex);

                            if (u1PortState == AST_PORT_STATE_FORWARDING)
                            {
                                i4RetVal = VLAN_FORWARD;
                            }
                        }
                    }

                    if (i4RetVal == VLAN_FORWARD)
                    {

                        pDupBuf = VlanDupBuf (pFrame);

                        if (pDupBuf == NULL)
                        {
                            UtilPlstReleaseLocalPortList (pPortList);
                            return;
                        }

                        VlanFwdOnSinglePort (pDupBuf, pVlanIf, u2Port,
                                             pVlanEntry, pInVlanTag,
                                             u1FrameType);
                        /* In the above function the length of the packet 
                         * in pVlanIf will be updated based on the 
                         * tagged/untagged properties of the outgoing 
                         * port. Hence update the original length 
                         * of packet again in the pVlanIf */
                        pVlanIf->u2Length = u2Len;
                    }
                }

                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }
    UtilPlstReleaseLocalPortList (pPortList);
}

/*****************************************************************************/
/* Function Name      : VlanHandleOutgoingPkt                                */
/*                                                                           */
/*                                                                           */
/* Description        : This function checks for TransitDelay and drop the   */
/*                      packet if TransitDelay exceeds otherwise Tx the      */
/*                      packet                                               */
/*                                                                           */
/*                                                                           */
/* Input(s)           :  pBuf       - Pointer to the Outgoing frame          */
/*                       pVlanOutIf - Pointer to Interface info containing   */
/*                                    outgoing information                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanHandleOutgoingPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tVlanOutIfMsg * pVlanOutIf)
{
    UINT4               u4CurSysTime = 0;
    UINT4               u4TimeStamp;
    UINT2               u2Port;
    UINT4               u4IfIndex;
    UINT2               u2Len;
    UINT1               u1FrameType;

    u4TimeStamp = pVlanOutIf->u4TimeStamp;
    u2Port = pVlanOutIf->u2Port;
    u2Len = pVlanOutIf->u2Length;
    u1FrameType = pVlanOutIf->u1FrameType;

    if ((u1FrameType == VLAN_DATA_FRAME) || (u1FrameType == VLAN_MCAST_FRAME))
    {

        /* Check if Transit Delay Time is exceeded
         * discard the frame if the tansit delay is more than 
         * the Maximum defined bridge  transit delay 
         * Send Out the packet otherwise
         */

        VLAN_GET_TIMESTAMP (&u4CurSysTime);

        if ((u4CurSysTime - u4TimeStamp) > VLAN_TRANSIT_DELAY_TICKS)
        {
            /* discard the frame */
            VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);

            VLAN_TRC (VLAN_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "Transit Delay Exceeded discarding Packet.\r\n");

            return;
        }
    }
    if ((u2Port >= VLAN_MAX_PORTS + 1) ||
        (VLAN_GET_PORT_ENTRY (u2Port) == NULL))
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
        return;
    }

    /* convert the local port to physical port and pass the same to L2Iwf */
    u4IfIndex = VLAN_GET_PHY_PORT (u2Port);
    if ((VLAN_NODE_STATUS () == VLAN_NODE_ACTIVE)
        || (VLAN_NODE_STATUS () == VLAN_NODE_SWITCHOVER_IN_PROGRESS))
    {
        if (u1FrameType == VLAN_CFM_FRAME)
        {
            VlanL2IwfHandleOutgoingPktOnPort (pBuf, u4IfIndex, u2Len,
                                              CFA_PROT_BPDU, CFA_ENCAP_NONE);
        }
        else
        {
            VlanL2IwfHandleOutgoingPktOnPort (pBuf, u4IfIndex, u2Len,
                                              0, CFA_ENCAP_NONE);
        }
    }
    else
    {
        VLAN_RELEASE_BUF (VLAN_MESSAGE_BUFFER, (UINT1 *) pBuf);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetAllPorts                                  */
/*                                                                           */
/*    Description         : This function scans through the L2IWF port table */
/*                          and issues create port indications to VLAN       */
/*                          module.                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanGetAllPorts (VOID)
{
    tVlanPortEntry     *pPortEntry = NULL;
    INT4                i4RetVal;
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPort;
    UINT2               u2PrevPort;
    UINT1               u1OperStatus;
    UINT1               u1PbPortType = VLAN_INVALID_PROVIDER_PORT;
    UINT1               u1InterfaceType;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4BridgeMode = VLAN_BRIDGE_MODE ();
    UINT2               u2AggId;
    UINT1               u1PhyPortBridgePortType;
    UINT1               u1LagBridgePortType;

    u2PrevPort = 0;
    u4ContextId = VLAN_CURR_CONTEXT_ID ();

    while (VLAN_L2IWF_GETNEXT_VALID_PORT (u4ContextId,
                                          u2PrevPort, &u2LocalPort,
                                          &u4IfIndex) == L2IWF_SUCCESS)
    {
        if (VLAN_NODE_STATUS () == VLAN_NODE_ACTIVE)
        {
            /* Setting Max Ports per port channel to default value
             * in case of Port Channels */

            if ((u4IfIndex > BRG_MAX_PHY_PORTS) &&
                (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
            {
                VlanLaSetDefaultPropForAggregator (u4IfIndex);
            }
        }
        if (VlanL2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
        {

            L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId);

            VlanCfaGetIfInfo (u4IfIndex, &CfaIfInfo);
            u1PhyPortBridgePortType = CfaIfInfo.u1BrgPortType;

            /* Get the port channel port info from CFA to compare the bridge
             * port type with physical port bridge port type */
            CfaGetIfInfo ((UINT4) u2AggId, &CfaIfInfo);
            u1LagBridgePortType = CfaIfInfo.u1BrgPortType;

            if ((VlanL2iwfValidatePortType (u4BridgeMode,
                                            CfaIfInfo.u1BrgPortType) !=
                 L2IWF_TRUE)
                || (u1PhyPortBridgePortType != u1LagBridgePortType))
            {
                /* Reset the bridge port-type for the ports which
                 * are part of the port channel if the current port
                 * type is not valid for the bridge mode and not
                 * not corrseponding to the port channel port type */

                VlanL2IwfPbSetDefBrgPortTypeInCfa (u4ContextId, u4IfIndex,
                                                   L2IWF_TRUE, L2IWF_TRUE);
            }

            u2PrevPort = u2LocalPort;
            continue;
        }
        /* In Stnandby node, after receiving CONFIG_RESTORE COMPLETE EVENT
         * Vlan module is enabled. Port types will be set in CFA before 
         * CONFIG_RESTORE_COMPLETE_EVENT. As PIP ports will not be known to Vlan 
         * ignore PIP ports if the bridge mode is I-Component (otherwise PIP 
         * port type will be overwritten when standby event is received).
         * In case of bridge mode change, for all ports in that component 
         * (including PIP ports) default port type applicable for new bridge 
         * mode has to be set. 
         */

        if (VLAN_BRIDGE_MODE () == VLAN_PBB_ICOMPONENT_BRIDGE_MODE)
        {
            VlanL2IwfGetPbPortType (u4IfIndex, &u1PbPortType);

            if (u1PbPortType == VLAN_PROVIDER_INSTANCE_PORT)
            {
                u2PrevPort = u2LocalPort;
                continue;
            }
        }
        pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

        if (pPortEntry == NULL)
        {

            /* For ports that are newly created in Vlan, set the default
             * PB port type. */

            /* Set the default bridge port type for this bridge in CFA.
             * As Vlan Create port will call Cfa to get the bridge port type,
             * we have to update Cfa before that. */

            /* If the Bridge Mode is PBB_I_COMP, then increment the count
               of the CNP ports in L2IWF */
            VlanL2IwfPbSetDefBrgPortTypeInCfa (u4ContextId, u4IfIndex,
                                               L2IWF_TRUE, L2IWF_TRUE);

            i4RetVal = VlanHandleCreatePort (u4ContextId, u4IfIndex,
                                             u2LocalPort);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                               VLAN_NAME,
                               "VLAN Port Creation failed for Port %d \n",
                               u4IfIndex);

                u2PrevPort = u2LocalPort;
                continue;
            }
        }

        VlanCfaGetIfInfo (u4IfIndex, &CfaIfInfo);
        if (CfaIfInfo.u1IfType == CFA_LAGG)
        {
            VlanL2IwfGetInterfaceType (VLAN_CURR_CONTEXT_ID (),
                                       &u1InterfaceType);

            if (VlanHandleCopyPortPropertiesToHw (u2LocalPort, u2LocalPort,
                                                  u1InterfaceType) !=
                VLAN_SUCCESS)
            {

                return;
            }
        }

        VlanL2IwfGetPortOperStatus (VLAN_MODULE, u4IfIndex, &u1OperStatus);

        if (u1OperStatus == CFA_IF_UP)
        {
            VlanHandlePortOperInd (u2LocalPort, VLAN_OPER_UP);
        }
        else
        {
            VlanHandlePortOperInd (u2LocalPort, VLAN_OPER_DOWN);
        }

        u2PrevPort = u2LocalPort;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanEnableVlan                                   */
/*                                                                           */
/*    Description         : This function enables the Vlan,                  */
/*                          1.When GARP is enabled propagates Static         */
/*                           Multicast, Vlan Information                     */
/*                          2.When IGS enabled,deletes all the Dynamic Info. */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FAILURE / VLAN_SUCCESS                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanEnableVlan (VOID)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    tVlanCurrEntry     *pCurrVlanEntry = NULL;
    tVlanId             VlanId;

    VLAN_MODULE_ADMIN_STATUS () = VLAN_ENABLED;

    VLAN_MODULE_STATUS () = VLAN_ENABLED;

    if (VlanCfaGetIvrStatus () == CFA_ENABLED)
    {
        VLAN_SCAN_VLAN_TABLE (VlanId)
        {
            pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

            if (pStVlanEntry != NULL)
            {
                pCurrVlanEntry = pStVlanEntry->pCurrEntry;
                if (pCurrVlanEntry != NULL)
                {
                    if (pCurrVlanEntry->u2OperPortsCount > 0)
                    {
                        VlanCfaIvrNotifyVlanIfOperStatus (VlanId, CFA_IF_UP);
                    }
                }
            }
        }
    }

    VlanGetAllPorts ();

    if (VlanHwVlanEnable (VLAN_CURR_CONTEXT_ID ()) == VLAN_FAILURE)
    {
        VLAN_MODULE_ADMIN_STATUS () = VLAN_DISABLED;

        VLAN_MODULE_STATUS () = VLAN_DISABLED;

        return VLAN_FAILURE;
    }

    VlanHwAddStaticInfo ();

    VlanPropagateStaticMacInfo ();
    VlanPropagateStaticVlanInfo ();

    if (VLAN_NODE_STATUS () != VLAN_NODE_IDLE)
    {
        if ((SNOOP_ENABLED ==
             VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()))
            || (SNOOP_ENABLED ==
                VlanSnoopIsMldSnoopingEnabled (VLAN_CURR_CONTEXT_ID ())))
        {
            VlanSnoopUpdateVlanStatus (VLAN_ENABLED);
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDisableVlan                                  */
/*                                                                           */
/*    Description         : This function disables the Vlan,                 */
/*                          1.When GARP is enabled withdraws  Static         */
/*                           Multicast, Vlan Information                     */
/*                          2.When IGS enabled,deletes all the Dynamic Info. */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FAILURE / VLAN_SUCCESS                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanDisableVlan (VOID)
{

    UINT4               u4IfIndex;
    UINT1               u1DisableVlan = VLAN_TRUE;
    tVlanId             VlanId;
    tCfaIfInfo          CfaIfInfo;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT4               u4FidIndex = 0;
    tVlanId             VlanStartId = 0;
    tVlanCurrEntry     *pCurrEntry = NULL;

  /***************************************************************************/
    /* ASSUMPTIONS:
     *  1) Since there is a restirction to disable GVRP and GMRP before 
     *     disabling VLAN. All Dynamic Vlan, Multicast and Ser Req info
     *     will be deleted when disabling GVRP and GMRP itself.
     *  
     *  2) There is no need to withdraw the static information given to 
     *     GVRP and GMRP. because these modules are already disabled.
     *
     *  When the restriction is removed . All Dynamic info should be 
     *  deleted and should call GVRP/GMRP to withdraw static information.
     *
     ***************************************************************************/

    /* should delete dynamic Mcast info alone because it can be learnt through 
     * IGS also*/

    VlanHandleDeleteAllDynamicMcastInfo (VLAN_INVALID_PORT, VLAN_TRUE,
                                         VLAN_ADDR_FMLY_IPV4);

    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        /* Flush all the Fid entries */
        VlanDelFidEntry (u4FidIndex, u1DisableVlan);
    }

    if ((SNOOP_ENABLED ==
         VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()))
        || (SNOOP_ENABLED ==
            VlanSnoopIsMldSnoopingEnabled (VLAN_CURR_CONTEXT_ID ())))
    {
        VlanSnoopUpdateVlanStatus (VLAN_DISABLED);
    }

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pStVlanEntry = VlanGetStaticVlanEntry (VlanId);

        if (pStVlanEntry != NULL)
        {

            if (VlanCfaGetIvrStatus () == CFA_ENABLED)

            {
                u4IfIndex =
                    (UINT2)
                    VlanCfaGetVlanInterfaceIndexInCxt (VLAN_CURR_CONTEXT_ID (),
                                                       VlanId);

                if (u4IfIndex != CFA_INVALID_INDEX)
                {

                    MEMSET (&CfaIfInfo, 0, sizeof (CfaIfInfo));

                    CfaIfInfo.u1IfOperStatus = CFA_IF_DOWN;

                    VlanCfaSetIfInfo (IF_OPER_STATUS, u4IfIndex, &CfaIfInfo);

                }
            }
        }
    }

    if (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        VlanPbUpdatePepOperStatusToAst ();
    }

    VlanL2IwfDeleteAllVlans (VLAN_CURR_CONTEXT_ID ());

    VLAN_MODULE_STATUS () = VLAN_DISABLED;

    VLAN_MODULE_ADMIN_STATUS () = VLAN_DISABLED;

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSetDefPortInfo                               */
/*                                                                           */
/*    Description         : Function called for setting the default port     */
/*                          parameter for VLAN.This function will be called  */
/*                          only during creation of the port.                */
/*                                                                           */
/*    Input(s)            : u2Port    - The Number of the Port               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified :  None                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanSetDefPortInfo (UINT2 u2Port)
{
    UINT1              *pPorts = NULL;
    UINT1              *pUnTagPorts = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT2               u2PriIndex;
    UINT2               u2NumTrfClasses;
    UINT1               u1IfType;
    tSNMP_OCTET_STRING_TYPE EgressPorts;
    tSNMP_OCTET_STRING_TYPE UnTagEgressPorts;
#ifdef VLAN_EXTENDED_FILTER
    UINT1              *pFwdUnRegPorts = NULL;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tSNMP_OCTET_STRING_TYPE FwdUnRegOctetString;
    tSNMP_OCTET_STRING_TYPE *pFwdUnRegOctetString = &FwdUnRegOctetString;
#endif
    INT1                i1RetVal = SNMP_SUCCESS;
    INT4                i4RowStatus;
    UINT4               u4Err;
    UINT1               u1Result;
    UINT1               u1IsDefVlanPresent = 1;
    UINT1               u1DeleteDefVlan = 0;
    UINT1               u1PbPortType = VLAN_PROVIDER_NETWORK_PORT;
    UINT1               u1InterfaceType = VLAN_S_INTERFACE_TYPE;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId;
    UINT4               u4SeqNum = 0;
    UINT4               u4IfIndex = 0;
    u4CurrContextId = VLAN_CURR_CONTEXT_ID ();

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
    if (u2Port >= VLAN_MAX_PORTS + 1)
    {
        return;
    }

    if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (u2Port)) == VCM_TRUE)
    {
        VlanVcmSispGetPhysicalPortOfSispPort (VLAN_GET_IFINDEX (u2Port),
                                              &(pPortEntry->u4PhyIfIndex));
    }
    else
    {
        pPortEntry->u4PhyIfIndex = pPortEntry->u4IfIndex;
    }

    VlanCfaGetIfInfo (VLAN_GET_PHY_PORT (u2Port), &CfaIfInfo);

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        /* Set the default port type (Access/Trunk/Hybrid). Based on the
         * PB Port type, this will be modified.*/
        pPortEntry->u1PortType = VLAN_DEFAULT_PORT;
        pPortEntry->Pvid = VLAN_DEFAULT_PORT_VID;

        /* Get the 1AD bridge port type from L2Iwf and set for the port. */
        if (VlanL2IwfGetPbPortType (VLAN_GET_IFINDEX (u2Port), &u1PbPortType)
            == L2IWF_FAILURE)
        {
            if (VLAN_IS_802_1AH_BRIDGE () == VLAN_TRUE)
            {
                /* In I Comp, VLAN will never receive the Internal Port as PIP,
                   Internal ports will be for VIP */
                if (VLAN_BRIDGE_MODE () == VLAN_PBB_ICOMPONENT_BRIDGE_MODE)
                {
                    if (CfaIfInfo.i4IsInternalPort == CFA_TRUE)
                    {
                        u1PbPortType = VLAN_VIRTUAL_INSTANCE_PORT;
                    }
                    else        /* External Ports */
                    {
                        u1PbPortType = VLAN_CNP_TAGGED_PORT;
                    }
                }
                else            /* VLAN_PBB_BCOMPONENT_BRIDGE_MODE */
                {
                    if (CfaIfInfo.i4IsInternalPort == CFA_TRUE)
                    {
                        u1PbPortType = VLAN_CUSTOMER_BACKBONE_PORT;
                    }
                    else
                    {
                        u1PbPortType = VLAN_PROVIDER_NETWORK_PORT;
                    }
                }
            }
            else
            {
                u1PbPortType = VLAN_PROVIDER_NETWORK_PORT;
            }
        }

        /* Set the 1AD bridge port type. */
        VlanPbSetDot1adPortType (pPortEntry, u1PbPortType);

        VlanSetHwProtocolTunnelStatus (pPortEntry, u2Port);

        /* Sending Trigger to MSR to store  corresponding Ingress 
         * and Egress ethertype, Acceptable Frame types,
         * and Port-type for that particular Bridge port-type*/
        if (VcmGetIfIndexFromLocalPort (u4CurrContextId, u2Port, &u4IfIndex) !=
            VCM_SUCCESS)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanSetDefPortInfo: Failure in retrieving IfIndex "
                      "form Local Port\r\n");
            return;
        }

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsDot1qPvid,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                          u4IfIndex, pPortEntry->Pvid));

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIDot1qFutureVlanPortIngressEtherType,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          u4IfIndex, pPortEntry->u2IngressEtherType));

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIDot1qFutureVlanPortEgressEtherType,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          u4IfIndex, pPortEntry->u2EgressEtherType));

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsDot1qPortAcceptableFrameTypes,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          u4IfIndex, pPortEntry->u1AccpFrmTypes));

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsMIDot1qFutureVlanPortType,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          u4IfIndex, pPortEntry->u1PortType));

        VlanSelectContext (u4CurrContextId);

    }
    else
    {
        /* Not an 1ad bridge. */
        /* Initialise tunnel info */
        VLAN_TUNNEL_STATUS (pPortEntry) = VLAN_DISABLED;
        VLAN_STP_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_GVRP_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_GMRP_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_MVRP_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_MMRP_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_IGMP_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_LACP_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_DOT1X_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_ELMI_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_LLDP_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_ECFM_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;
        VLAN_EOAM_TUNNEL_STATUS (pPortEntry) = VLAN_TUNNEL_PROTOCOL_PEER;

        if (CfaIfInfo.u1BrgPortType != CFA_UPLINK_ACCESS_PORT)
        {
            /* Ether type for UAP are set in VlanEvbUapIfSetDefProperties */
            pPortEntry->u2IngressEtherType = VLAN_PROTOCOL_ID;
            pPortEntry->u2EgressEtherType = VLAN_PROTOCOL_ID;
        }

        /*Initialize the Tunnel status maintained in L2IWF */

        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_DOT1X,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_LACP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_STP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_GVRP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_GMRP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_MVRP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_MMRP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_IGMP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_ELMI,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_LLDP,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_ECFM,
                                                VLAN_TUNNEL_PROTOCOL_PEER);
        VlanL2IwfSetProtocolTunnelStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                u2Port, L2_PROTO_EOAM,
                                                VLAN_TUNNEL_PROTOCOL_PEER);

        VlanSetHwProtocolTunnelStatus (pPortEntry, u2Port);
        pPortEntry->Pvid = VLAN_DEFAULT_PORT_VID;

        /* Sending Trigger to MSR to store default Pvid Value */

        /* Filling the Sequence number is not required as we sending
         * ** indication only to MSR */
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsDot1qPvid,
                              0, FALSE, VlanLock, VlanUnLock, 1, SNMP_SUCCESS);
        SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
                              (INT2) VLAN_GET_IFINDEX (u2Port),
                              pPortEntry->Pvid));

        VlanSelectContext (u4CurrContextId);

        pPortEntry->u1PortType = VLAN_DEFAULT_PORT;

        if (SISP_IS_LOGICAL_PORT (VLAN_GET_IFINDEX (u2Port)) == VCM_TRUE)
        {
            /* Acceptable frame type for the logical ports can be only
             * accept VLAN tagged frames. */
            pPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES;

            /* Ingress filtering will be always enabled on the logical
             * ports */
            pPortEntry->u1IngFiltering = VLAN_SNMP_TRUE;

            L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                         pPortEntry->u1AccpFrmTypes);

            VLAN_PORT_PORT_PROTOCOL_BASED (u2Port) = VLAN_SNMP_FALSE;
            VLAN_PORT_MAC_BASED (u2Port) = VLAN_SNMP_FALSE;
        }
        else
        {
            pPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

            if (pPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "VlanSetDefPortInfo: Error in allocating memory "
                          "for pPorts\r\n");
                return;
            }
            MEMSET (pPorts, 0, sizeof (tLocalPortList));

            pUnTagPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

            if (pUnTagPorts == NULL)
            {
                VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          VLAN_NAME,
                          "VlanSetDefPortInfo: Error in allocating memory "
                          "for pUnTagPorts\r\n");
                UtilPlstReleaseLocalPortList (pPorts);
                return;
            }
            MEMSET (pUnTagPorts, 0, sizeof (tLocalPortList));

            pPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ALL_FRAMES;
            pPortEntry->u1IngFiltering = VLAN_SNMP_FALSE;

            /* Port and Mac based support for port is configured 
             * based on the global configuration
             */
            VLAN_PORT_PORT_PROTOCOL_BASED (u2Port) = VLAN_PORT_PROTO_BASED;
            VlanLldpApiNotifyProtoVlanStatus (u2Port, VLAN_PORT_PROTO_BASED);

            VLAN_PORT_MAC_BASED (u2Port) = VLAN_MAC_BASED;

            L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                         pPortEntry->u1AccpFrmTypes);

            EgressPorts.pu1_OctetList = pPorts;
            EgressPorts.i4_Length = sizeof (tLocalPortList);

            UnTagEgressPorts.pu1_OctetList = pUnTagPorts;
            UnTagEgressPorts.i4_Length = sizeof (tLocalPortList);

            if (nmhGetDot1qVlanStaticRowStatus (VLAN_DEF_VLAN_ID, &i4RowStatus)
                != SNMP_SUCCESS)
            {
                i1RetVal = SNMP_FAILURE;
            }
            else
            {
                nmhGetDot1qVlanStaticEgressPorts (VLAN_DEF_VLAN_ID,
                                                  &EgressPorts);
            }

            /* Pseudo wire and attachment circuit interaces should only be added
             * in default VLAN only when they are configured for default VLAN */
            if ((i1RetVal == SNMP_SUCCESS) &&
                (!((CfaIfInfo.u1IfType == CFA_PSEUDO_WIRE) ||
                   ((CfaIfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                    (CfaIfInfo.u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)) ||
                   (CfaIfInfo.u1IfType == CFA_VXLAN_NVE) ||
                   (CfaIfInfo.u1IfType == CFA_CAPWAP_DOT11_BSS) ||
                   (CfaIfInfo.u1BrgPortType == CFA_UPLINK_ACCESS_PORT) ||
                   (CfaIfInfo.u1BrgPortType ==
                    CFA_STATION_FACING_BRIDGE_PORT))))
            {
                VLAN_IS_MEMBER_PORT (pPorts, u2Port, u1Result);

                if (u1Result == VLAN_FALSE)
                {
                    VLAN_SET_MEMBER_PORT (pPorts, u2Port);
                    if (VlanAstIsPvrstStartedInContext (VLAN_CURR_CONTEXT_ID ())
                        != AST_TRUE)
                    {
                        i1RetVal = VlanTestDot1qVlanStaticEgressPorts (&u4Err,
                                                                       VLAN_DEF_VLAN_ID,
                                                                       &EgressPorts);
                        i1RetVal = (i1RetVal == VLAN_SUCCESS) ?
                            SNMP_SUCCESS : SNMP_FAILURE;
                    }
                    else
                    {
                        i1RetVal = SNMP_SUCCESS;
                    }

                    if (i1RetVal == SNMP_SUCCESS)
                    {
                        nmhGetDot1qVlanStaticUntaggedPorts (VLAN_DEF_VLAN_ID,
                                                            &UnTagEgressPorts);

                        if (nmhSetDot1qVlanStaticEgressPorts (VLAN_DEF_VLAN_ID,
                                                              &EgressPorts)
                            == SNMP_SUCCESS)
                        {
                            VLAN_IS_MEMBER_PORT (pUnTagPorts, u2Port, u1Result);

                            if (u1Result == VLAN_FALSE)
                            {
                                VLAN_SET_MEMBER_PORT (pUnTagPorts, u2Port);
                                if (VlanAstIsPvrstStartedInContext
                                    (VLAN_CURR_CONTEXT_ID ()) != AST_TRUE)
                                {
                                    i1RetVal =
                                        VlanTestDot1qVlanStaticUntaggedPorts
                                        (&u4Err, VLAN_DEF_VLAN_ID,
                                         &UnTagEgressPorts);
                                    i1RetVal = (i1RetVal == VLAN_SUCCESS) ?
                                        SNMP_SUCCESS : SNMP_FAILURE;
                                }
                                else
                                {
                                    i1RetVal = SNMP_SUCCESS;
                                }

                                if (i1RetVal == SNMP_SUCCESS)
                                {
                                    if (nmhSetDot1qVlanStaticUntaggedPorts
                                        (VLAN_DEF_VLAN_ID,
                                         &UnTagEgressPorts) != SNMP_SUCCESS)
                                    {
                                        u1DeleteDefVlan = 1;
                                    }
                                }
                                else
                                {
                                    u1DeleteDefVlan = 1;
                                }
                            }
                        }
                        else
                        {
                            u1DeleteDefVlan = 1;
                        }
                    }
                    else
                    {
                        u1DeleteDefVlan = 1;
                    }

                    /* If Default Vlan had no ports previously it would have been
                     * put to NOT-READY state. Change it back to ACTIVE now */
                    if ((u1IsDefVlanPresent == 0)
                        || (i4RowStatus == VLAN_NOT_READY))
                    {
                        nmhSetDot1qVlanStaticRowStatus (VLAN_DEF_VLAN_ID,
                                                        VLAN_ACTIVE);
                    }

#ifdef VLAN_EXTENDED_FILTER
                    if (u1DeleteDefVlan == 0)
                    {
                        /* 
                         * Port addition to the Defaul Vlan is successful.
                         * Add the Port to the Forward All Ports also.
                         */
                        pFwdUnRegPorts =
                            UtilPlstAllocLocalPortList (sizeof
                                                        (tLocalPortList));

                        if (pFwdUnRegPorts == NULL)
                        {
                            VLAN_TRC (VLAN_MOD_TRC,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      VLAN_NAME,
                                      "VlanSetDefPortInfo: Error in allocating memory "
                                      "for pFwdUnRegPorts\r\n");
                            UtilPlstReleaseLocalPortList (pPorts);
                            UtilPlstReleaseLocalPortList (pUnTagPorts);
                            return;
                        }
                        MEMSET (pFwdUnRegPorts, 0, sizeof (tLocalPortList));
                        FwdUnRegOctetString.pu1_OctetList = pFwdUnRegPorts;
                        FwdUnRegOctetString.i4_Length = sizeof (tLocalPortList);

                        pCurrEntry = VlanGetVlanEntry (VLAN_DEF_VLAN_ID);
                        if (pCurrEntry != NULL)
                        {

                            VLAN_ADD_PORT_LIST (FwdUnRegOctetString.
                                                pu1_OctetList,
                                                pCurrEntry->UnRegGrps.
                                                StaticPorts);

                            FwdUnRegOctetString.i4_Length = VLAN_PORT_LIST_SIZE;

                        }

                        VLAN_IS_MEMBER_PORT (pFwdUnRegPorts, u2Port, u1Result);

                        if (u1Result == VLAN_FALSE)
                        {
                            VLAN_SET_MEMBER_PORT (pFwdUnRegPorts, u2Port);
                        }
                        if (pCurrEntry != NULL)
                        {
                            /* Here nmhSet is called instead of calling 
                             * nmhSet because we need to send trigger to 
                             * MSR for the Port membership of 
                             * default vlan as this flow is a result of 
                             * "map switch default" CLI command.*/

                            nmhSetDot1qForwardUnregisteredStaticPorts
                                (VLAN_DEF_VLAN_ID, pFwdUnRegOctetString);
                        }
                        UtilPlstReleaseLocalPortList (pFwdUnRegPorts);
                    }
#endif /* VLAN_EXTENDED_FILTER */
                }
            }
            UtilPlstReleaseLocalPortList (pPorts);
            UtilPlstReleaseLocalPortList (pUnTagPorts);
        }                        /*Not a logical port */

        /* Sending trigger to MSR to store AcceptableFrameTypes 
         * because the Frame type varies for differebt PB ports 
         * by default when compared to customer bridge */
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsDot1qPortAcceptableFrameTypes,
                              u4SeqNum, FALSE, VlanLock, VlanUnLock, 1,
                              SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                          VLAN_GET_IFINDEX (u2Port),
                          pPortEntry->u1AccpFrmTypes));
        VlanSelectContext (u4CurrContextId);

    }

    pPortEntry->u1PortProtected = VLAN_SNMP_FALSE;
    pPortEntry->u1DefUserPriority = VLAN_DEF_USER_PRIORITY;
    pPortEntry->u1NumTrfClass = VLAN_MAX_TRAFF_CLASS;
    pPortEntry->u4NoFilterInDiscard = 0;
    pPortEntry->u4FilterInDiscardOverflow = 0;
    pPortEntry->u4IfSpeed = CfaIfInfo.u4IfSpeed;
    pPortEntry->u4OverrideOption = VLAN_OVERRIDE_OPTION_DISABLE;

    /* Override option (Enable/Disable) to get Tunnel Status based on Per Port/Per Vlan */
    VlanL2IwfSetOverrideOption (u2Port, VLAN_OVERRIDE_OPTION_DISABLE);

    /* Initialize the tunnel counters. */
    VlanInitializeTunnelCounters (pPortEntry);
    /* Initialize the discard counters. */
    VlanInitializeDiscardCounters (pPortEntry);

    /* Port and Mac based support for port is configured 
     * based on the global configuration
     */
    VLAN_PORT_PORT_PROTOCOL_BASED (u2Port) = VLAN_PORT_PROTO_BASED;
    VlanLldpApiNotifyProtoVlanStatus (u2Port, VLAN_PORT_PROTO_BASED);

    VLAN_PORT_MAC_BASED (u2Port) = VLAN_MAC_BASED;
    VLAN_SET_PORT_SUBNET_BASED (u2Port, VLAN_SUBNET_BASED);
    VLAN_SLL_INIT (&pPortEntry->VlanVidSet);

    u1IfType = pPortEntry->u1IfType;
    if ((u1IfType != VLAN_ETHERNET_INTERFACE_TYPE)
        && (u1IfType != VLAN_LAGG_INTERFACE_TYPE))
    {
        /* 
         * Initialise priority regeneration table. This table is valid 
         * only for media that support native user priority.
         */
        for (u2PriIndex = 0; u2PriIndex < VLAN_MAX_PRIORITY; u2PriIndex++)
        {
            pPortEntry->au1PriorityRegen[u2PriIndex] = (UINT1) u2PriIndex;
        }
    }

    u2NumTrfClasses = VLAN_MAX_TRAFF_CLASS - 1;

    for (u2PriIndex = 0; u2PriIndex < VLAN_MAX_PRIORITY; u2PriIndex++)
    {
        /* Traffic class mapping table initialization */
        pPortEntry->au1TrfClassMap[u2PriIndex] =
            gau1PriTrfClassMap[u2PriIndex][u2NumTrfClasses];
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
        /* 
         * In 802.1ad, initialise the PCP encoding and decoding 
         * tables.
         */
        VlanPbInitPcpValues (u2Port);
    }
#ifndef NPAPI_WANTED
    for (u2PriIndex = 0; u2PriIndex < VLAN_MAX_TRAFF_CLASS; u2PriIndex++)
    {
        pPortEntry->VlanPriQ[u2PriIndex].pFirstBuf = NULL;
        pPortEntry->VlanPriQ[u2PriIndex].pLastBuf = NULL;
    }
#endif /* !NPAPI_WANTED */
    VlanL2IwfSetVlanPortType (VLAN_CURR_CONTEXT_ID (), u2Port,
                              pPortEntry->u1PortType);
    VlanL2IwfSetVlanPortPvid (VLAN_CURR_CONTEXT_ID (), u2Port,
                              pPortEntry->Pvid);

    if (VLAN_TUNNEL_STATUS (pPortEntry) == VLAN_ENABLED)
    {
        VlanL2IwfSetPortVlanTunnelStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                          OSIX_TRUE);
    }
    else
    {
        VlanL2IwfSetPortVlanTunnelStatus (VLAN_CURR_CONTEXT_ID (), u2Port,
                                          OSIX_FALSE);
    }

    if (CfaIfInfo.u1IfType != CFA_LAGG)
    {
        VlanL2IwfGetInterfaceType (VLAN_CURR_CONTEXT_ID (), &u1InterfaceType);

        if (VlanHandleCopyPortPropertiesToHw (u2Port, u2Port,
                                              u1InterfaceType) != VLAN_SUCCESS)
        {

            return;
        }
    }

    VLAN_TRC_ARG1 (VLAN_MOD_TRC, INIT_SHUT_TRC | CONTROL_PLANE_TRC,
                   VLAN_NAME, "Setting default Port %d successfully \n",
                   pPortEntry->u4IfIndex);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanNotifyMembersChangeToL2Iwf                   */
/*                                                                           */
/*    Description         : This function notifies the addition/deletion of  */
/*                          VLAN member ports to L2IWF                       */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN for which membership has been      */
/*                          changed.                                         */
/*                          AddedPorts - Added members                       */
/*                          DeletedPorts - Deleted members                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanNotifyMembersChangeToL2Iwf (tVlanId VlanId, tLocalPortList AddedPorts,
                                tLocalPortList DeletedPorts)
{
    tVlanCurrEntry     *pVlanEntry = NULL;

    VlanL2IwfUpdtVlanEgressPortList (VLAN_CURR_CONTEXT_ID (), VlanId,
                                     AddedPorts, DeletedPorts);

    if (VLAN_IS_ENH_FILTERING_ENABLED () == VLAN_TRUE)
    {
        /*Re-evaluate Enhance filtering criteria for u4FidIndex */
        pVlanEntry = VlanGetVlanEntry (VlanId);
        if (pVlanEntry != NULL)
        {
            VlanUpdateFidEnhanceFiltering (pVlanEntry->u4FidIndex, 0);
        }
    }

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanNotifyUntagMembersChangeToL2Iwf              */
/*                                                                           */
/*    Description         : This function notifies the addition/deletion of  */
/*                          VLAN untagged member ports to L2IWF              */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN for which membership has been      */
/*                          changed.                                         */
/*                          AddedPorts - Untag member ports list             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanNotifyUntagMembersChangeToL2Iwf (tVlanId VlanId, tLocalPortList AddedPorts)
{
    VlanL2IwfUpdtVlanUntagPortList (VLAN_CURR_CONTEXT_ID (), VlanId,
                                    AddedPorts);
}

#ifndef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleDeleteFdbEntries                       */
/*                                                                           */
/*    Description         : This function flushes the FDB entries in FDB  Tbl*/
/*                          learned on this port.                            */
/*                           This function will be called                    */
/*                          1) By RSTP during topology change in the port    */
/*                          2) By PNAC module during port down indication    */
/*                          3) By VLAN module during port deletion           */
/*    Input(s)            : u2Port - port number                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleDeleteFdbEntries (UINT2 u2Port)
{
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanFdbEntry      *pFdbEntry = NULL;
    tVlanFdbEntry      *pNextFdbEntry = NULL;
    UINT4               u4FidIndex = 0;
    UINT2               u2HashIndex = 0;
    tVlanId             VlanStartId = 0;
    tVlanId             VlanId = 0;
    tVlanCurrEntry     *pCurrEntry;

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        /* VLAN module is DISABLED */
        return;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {

        /* Port number exceeds the allowed range */
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Flushing based on Port called for invalid port \n");
        return;
    }

    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        /* Irrespective of the learned entries in the different 
         * FDB table, flushing is done based on the port applicable
         * for all learning types
         */

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry == NULL)
        {
            if (VLAN_LEARNING_MODE () == VLAN_SHARED_LEARNING)
            {
                return;
            }
            continue;
        }

        if (pFidEntry->u4Count == 0)
        {
            if (VLAN_LEARNING_MODE () == VLAN_SHARED_LEARNING)
            {
                return;
            }
            continue;
        }

        for (u2HashIndex = 0; u2HashIndex < VLAN_MAX_BUCKETS; u2HashIndex++)
        {
            pFdbEntry = VLAN_GET_FDB_ENTRY (u4FidIndex, u2HashIndex);

            while (pFdbEntry != NULL)
            {

                pNextFdbEntry = pFdbEntry->pNextHashNode;

                /* Delete the FDB entry only for port
                 * where it is learned
                 */
                if (pFdbEntry->u2Port == u2Port)
                {
                    if (pFdbEntry->u1StRefCount == 0)
                    {
                        /* No static entries present */
                        if (pFdbEntry->u1Status == VLAN_FDB_LEARNT)
                        {

                            VlanDeleteFdbEntry (u4FidIndex, pFdbEntry);
                            pFdbEntry = NULL;
                        }
                    }

                    if (pFdbEntry != NULL)
                    {
                        pFdbEntry->u2Port = VLAN_INVALID_PORT;
                    }

                }

                pFdbEntry = pNextFdbEntry;
            }

        }

        if (VLAN_LEARNING_MODE () == VLAN_SHARED_LEARNING)
        {
            return;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleFlushFdbEntries                        */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires in Fdb     */
/*                          table based on port and VLAN ID                  */
/*                                                                           */
/*    Input(s)            : u2Port - Port number                             */
/*                          FdbId  - FdbId                                   */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleFlushFdbEntries (UINT2 u2Port, UINT4 u4FdbId, UINT1 u1ModId)
{
    tVlanFdbEntry      *pFdbEntry = NULL;
    tVlanFdbEntry      *pNextFdbEntry = NULL;
    tVlanCurrEntry     *pVlanEntry;
    UINT4               u4FidIndex;
    UINT2               u2HashIndex;
    tVlanId             VlanId;
    UINT1               u1Result = VLAN_FALSE;

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        /* VLAN module is DISABLED */
        return;
    }

    if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
    {

        /* Port number is not in the allowed range */
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Flushing based on Port-FdbId called for invalid port \n");

        return;
    }

    if (VLAN_IS_FDB_ID_VALID (u4FdbId) == VLAN_FALSE)
    {
        /*Fdb Id is not in the  allowed range */

        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Flushing based on Port-FdbId called for invalid port \n");

        return;
    }

    if (VlanMrpIsMvrpEnabled (VLAN_CURR_CONTEXT_ID ()) == MRP_ENABLED)
    {
        if (VLAN_LEARNING_MODE () == VLAN_INDEP_LEARNING)
        {
            /* VLAN id is one-to-one mapped with FDB Id in case of 
             * Indepenant VLAN learning.*/
            VlanId = (tVlanId) u4FdbId;

            pVlanEntry = VlanGetVlanEntry (VlanId);

            if (pVlanEntry == NULL)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "Vlan %d Not Exist. Flushing Failed\n", VlanId);
                return;
            }

            if ((pVlanEntry->pStVlanEntry == NULL) && (u1ModId == STP_MODULE))
            {
                VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                          "Flushing on Port-FdbId cannot to done. As Mrp "
                          "module will flush\n");
                return;
            }

            if (pVlanEntry->pStVlanEntry != NULL)
            {
                VLAN_IS_EGRESS_PORT (pVlanEntry->pStVlanEntry,
                                     u2Port, u1Result);
            }

            if ((u1ModId == STP_MODULE) && (u1Result == VLAN_FALSE))
            {
                VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                          "Flushing on Port-FdbId cannot to done. As MRP "
                          "module will flush\n");
                return;
            }
            else if ((u1ModId == MRP_MODULE) && (u1Result == VLAN_TRUE))
            {
                VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                          "Flushing on Port-FdbId cannot to done. As STP "
                          "module will flush\n");
                return;
            }
        }
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FdbId);

    if (u4FidIndex >= (VLAN_INVALID_FID_INDEX - 1))
    {
        return;
    }

    for (u2HashIndex = 0; u2HashIndex < VLAN_MAX_BUCKETS; u2HashIndex++)
    {
        pFdbEntry = VLAN_GET_FDB_ENTRY (u4FidIndex, u2HashIndex);

        while (pFdbEntry != NULL)
        {
            pNextFdbEntry = pFdbEntry->pNextHashNode;

            /* 
             * Check for the port number and delete the learned 
             * FDB entry 
             */
            if (pFdbEntry->u2Port == u2Port)
            {
                if (pFdbEntry->u1StRefCount == 0)
                {
                    if (pFdbEntry->u1Status == VLAN_FDB_LEARNT)
                    {
                        /* 
                         * If no static entries is associated with 
                         * learned FDB. So learned FDB entry can be deleted
                         */
                        VlanDeleteFdbEntry (u4FidIndex, pFdbEntry);
                        pFdbEntry = NULL;
                    }
                }
                if (pFdbEntry != NULL)
                {
                    pFdbEntry->u2Port = VLAN_INVALID_PORT;
                }
            }
            pFdbEntry = pNextFdbEntry;
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleFlushFdbId                             */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires in Fdb     */
/*                          table                                            */
/*                                                                           */
/*    Input(s)            : FdbId  - FdbId                                   */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleFlushFdbId (UINT4 u4FdbId)
{
    tVlanFdbEntry      *pFdbEntry = NULL;
    tVlanFdbEntry      *pNextFdbEntry = NULL;
    UINT2               u2HashIndex;
    UINT4               u4FidIndex;

    if (VLAN_MODULE_STATUS () == VLAN_DISABLED)
    {
        /* VLAN module is DISABLED */
        return;
    }

    if (VLAN_IS_FDB_ID_VALID (u4FdbId) == VLAN_FALSE)
    {
        /*Fdb Id is not in the  allowed range */

        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Flushing based on Port-FdbId called for invalid port \n");

        return;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FdbId);

    if (u4FidIndex >= (VLAN_INVALID_FID_INDEX - 1))
    {
        return;
    }

    for (u2HashIndex = 0; u2HashIndex < VLAN_MAX_BUCKETS; u2HashIndex++)
    {
        pFdbEntry = VLAN_GET_FDB_ENTRY (u4FidIndex, u2HashIndex);

        while (pFdbEntry != NULL)
        {
            pNextFdbEntry = pFdbEntry->pNextHashNode;

            if (pFdbEntry->u1StRefCount == 0)
            {
                /* 
                 * If no static entries is associated with 
                 * learned FDB. So learned FDB entry can be deleted
                 */
                VlanDeleteFdbEntry (u4FidIndex, pFdbEntry);
            }
            pFdbEntry = pNextFdbEntry;
        }
    }

    return;
}
#endif /* NPAPI_WANTED */

/***************************************************************************/
/* Function Name    : VlanHandleCopyPortPropertiesToHw ()                  */
/*                                                                         */
/* Description      : This function programs the hardware with all         */
/*                    the Port properties for the port "u2DstPort".        */
/*                    The properties to be copied to "u2DstPort" is        */
/*                    obtained from "u2Port".                              */
/*                    "u2SrcPort" must have been created in Vlan. This     */
/*                    function does not program the Dynamic Vlan and Mcast */
/*                    membership and hence must not be called for ports    */
/*                    that are in OPER UP state. Dynamic Learning is NOT   */
/*                    done when the port is in OPER DOWN state.            */
/*                                                                         */
/* Input(s)         : u2DstPort - The Port whose properties have to be     */
/*                                programmed in the Hardware.              */
/*                                                                         */
/*                  : u2SrcPort - The Port from where the port properties  */
/*                                have to be obtained.                     */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanHandleCopyPortPropertiesToHw (UINT2 u2DstPort, UINT2 u2SrcPort,
                                  UINT1 u1InterfaceType)
{
    UINT1              *pPorts = NULL;
    UINT1              *pUnTagPorts = NULL;
    tVlanPortEntry     *pSrcPortEntry;
    tVlanCurrEntry     *pCurrEntry;
    tVlanPortVidSet    *pPortVidSetEntry;
    tHwVlanPortProperty VlanPortProperty;
    UINT2               u2PriIndex;
    tVlanId             VlanId;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1IsTagged;
    UINT4               u4Mode;
    UINT4               u4DstPort;
    UINT4               u4SrcPort;

    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));
    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanCopyPortPropertiesToHw (): Vlan is Shut down.\n");

        return VLAN_FAILURE;
    }

    if (VLAN_MODULE_STATUS () != VLAN_ENABLED)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanCopyPortPropertiesToHw (): Vlan is disabled.\n");

        return VLAN_FAILURE;
    }

    pSrcPortEntry = VLAN_GET_PORT_ENTRY (u2SrcPort);

    if (pSrcPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanCopyPortPropertiesToHw (): Invalid Src Port \n");

        return VLAN_FAILURE;
    }

    u4SrcPort = VLAN_GET_IFINDEX (u2SrcPort);
    u4DstPort = VLAN_GET_IFINDEX (u2DstPort);

    /* Here we want to copy all the Mac Vlan entries existing on the trunk
     * to its corresponding members in the hardware. Here u2SrcPort is the 
     * port from where the port properties have to be obtained;so it identifies 
     * the LA trunk. In VlanHandleMacBasedOnPort we will pass u2SrcPort.
     * Inside VlanHandleMacBasedOnPort the NPAPI will be called which will 
     * loop for all the physical ports which are part of that trunk and copy the 
     * Mac Vlan entries there.
     * */
    pPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleCopyPortPropertiesToHw: Error in allocating memory "
                  "for pPorts\r\n");
        return VLAN_FAILURE;
    }

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (NULL == pCurrEntry)
        {
            continue;
        }

        VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2SrcPort, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            VLAN_RESET_SINGLE_CURR_EGRESS_PORT (pCurrEntry, u2SrcPort);

            VLAN_IS_NO_CURR_EGRESS_PORTS_PRESENT (pCurrEntry, u1Result);
            if ((u2SrcPort == u2DstPort) && (u1Result == VLAN_TRUE))
            {
                /* u2SrcPort is the only member */
                VLAN_SET_SINGLE_CURR_EGRESS_PORT (pCurrEntry, u2SrcPort);

                pUnTagPorts =
                    UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

                if (pUnTagPorts == NULL)
                {
                    VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME,
                              "VlanHandleCopyPortPropertiesToHw: Error in allocating memory "
                              "for pUnTagPorts\r\n");
                    UtilPlstReleaseLocalPortList (pPorts);
                    return VLAN_FAILURE;
                }
                VLAN_GET_CURR_EGRESS_PORTS (pCurrEntry, pPorts);

                if (pCurrEntry->pStVlanEntry == NULL)
                {
                    VLAN_MEMSET (pUnTagPorts, 0, sizeof (tLocalPortList));
                }
                else
                {
                    VLAN_GET_UNTAGGED_PORTS (pCurrEntry->pStVlanEntry,
                                             pUnTagPorts);
                }

                if (VlanHwAddVlanEntry (pCurrEntry->VlanId,
                                        pPorts, pUnTagPorts) != VLAN_SUCCESS)
                {
                    VLAN_TRC_ARG3 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                   "VlanCopyPortPropertiesToHw (): Unable to"
                                   " program the Vlan %d. Src Port %d -"
                                   "Dst Port %d.\n", pCurrEntry->VlanId,
                                   u4DstPort, u4SrcPort);

                    UtilPlstReleaseLocalPortList (pPorts);
                    UtilPlstReleaseLocalPortList (pUnTagPorts);
                    return VLAN_FAILURE;
                }
                UtilPlstReleaseLocalPortList (pUnTagPorts);
            }
            else
            {
                /* u2SrcPort is NOT the only Vlan member */
                VLAN_SET_SINGLE_CURR_EGRESS_PORT (pCurrEntry, u2SrcPort);

                if (pCurrEntry->pStVlanEntry == NULL)
                {
                    u1IsTagged = VLAN_TRUE;
                }
                else
                {
                    VLAN_IS_UNTAGGED_PORT (pCurrEntry->pStVlanEntry,
                                           u2SrcPort, u1Result);

                    if (u1Result == VLAN_TRUE)
                    {
                        u1IsTagged = VLAN_FALSE;
                    }
                    else
                    {
                        u1IsTagged = VLAN_TRUE;
                    }
                }

                if (VlanHwSetVlanMemberPort (pCurrEntry->VlanId,
                                             u2DstPort,
                                             u1IsTagged) != VLAN_SUCCESS)
                {
                    VLAN_TRC_ARG3 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                   "VlanCopyPortPropertiesToHw (): Unable to"
                                   " add the Port to the Vlan %d. Src Port"
                                   " %d - Destination Port %d\n",
                                   pCurrEntry->VlanId, u4DstPort, u4SrcPort);

                    UtilPlstReleaseLocalPortList (pPorts);
                    return VLAN_FAILURE;
                }
            }

        }                        /* u2SrcPort is member of VLAN */

#ifdef VLAN_EXTENDED_FILTER
        VLAN_IS_MEMBER_PORT (pCurrEntry->AllGrps.Ports, u2SrcPort, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            VLAN_MEMSET (pPorts, 0, sizeof (tLocalPortList));

            VLAN_ADD_PORT_LIST (pPorts, pCurrEntry->AllGrps.Ports);

            VLAN_SET_MEMBER_PORT (pPorts, u2DstPort);

            if (VlanHwSetDefGroupInfo (VLAN_CURR_CONTEXT_ID (),
                                       pCurrEntry->VlanId, VLAN_ALL_GROUPS,
                                       pPorts) != VLAN_SUCCESS)
            {
                VLAN_TRC_ARG3 (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                               VLAN_NAME,
                               "VlanCopyPortPropertiesToHw (): "
                               "Unable to program the All Groups "
                               "membership for the Vlan %d. Src Port %d"
                               ", Destination Port %d\n", pCurrEntry->VlanId,
                               u4SrcPort, u4DstPort);

                UtilPlstReleaseLocalPortList (pPorts);
                return VLAN_FAILURE;
            }
        }

        VLAN_IS_MEMBER_PORT (pCurrEntry->UnRegGrps.Ports, u2SrcPort, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            VLAN_MEMSET (pPorts, 0, sizeof (tLocalPortList));

            VLAN_ADD_PORT_LIST (pPorts, pCurrEntry->UnRegGrps.Ports);

            VLAN_SET_MEMBER_PORT (pPorts, u2DstPort);

            if (VlanHwSetDefGroupInfo (VLAN_CURR_CONTEXT_ID (),
                                       pCurrEntry->VlanId, VLAN_UNREG_GROUPS,
                                       pPorts) != VLAN_SUCCESS)
            {
                VLAN_TRC_ARG3 (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                               VLAN_NAME,
                               "VlanCopyPortPropertiesToHw (): "
                               "Unable to program the UnReg Groups "
                               "membership for the Vlan %d. Src Port %d, "
                               "Destination Port %d.\n", pCurrEntry->VlanId,
                               u4SrcPort, u4DstPort);

                UtilPlstReleaseLocalPortList (pPorts);
                return VLAN_FAILURE;
            }
        }
#endif
    }
    UtilPlstReleaseLocalPortList (pPorts);

    if (L2IwfMiIsVlanActive (VLAN_CURR_CONTEXT_ID (),
                             pSrcPortEntry->Pvid) == OSIX_TRUE)
    {
        /* we program the pvid in hardware only if the vlan is active */
        if (VlanHwSetPortPvid (VLAN_CURR_CONTEXT_ID (),
                               u2DstPort, pSrcPortEntry->Pvid) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                           "VlanCopyPortPropertiesToHw (): "
                           "Unable to program Pvid. Src Port %d, Destination Port %d\n",
                           u4SrcPort, u4DstPort);

            return VLAN_FAILURE;
        }
    }

    if (VlanHwSetPortAccFrameType (VLAN_CURR_CONTEXT_ID (),
                                   u2DstPort,
                                   pSrcPortEntry->u1AccpFrmTypes) !=
        VLAN_SUCCESS)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanCopyPortPropertiesToHw (): Unable to program "
                       "Acceptable Frame Types. Src Port %d, Destination Port\n",
                       u4SrcPort, u4DstPort);

        return VLAN_FAILURE;
    }

    if (VlanHwSetPortIngFiltering (VLAN_CURR_CONTEXT_ID (), u2DstPort,
                                   pSrcPortEntry->u1IngFiltering) !=
        VLAN_SUCCESS)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanCopyPortPropertiesToHw (): Unable to program "
                       "Ingress Filtering. Src Port %d, Destination Port %d.\n",
                       u4SrcPort, u4DstPort);

        return VLAN_FAILURE;
    }

    if (VlanHwSetDefUserPriority (VLAN_CURR_CONTEXT_ID (), u2DstPort,
                                  pSrcPortEntry->u1DefUserPriority) !=
        VLAN_SUCCESS)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanCopyPortPropertiesToHw (): Unable to program "
                       "Default User Priority. Src Port %d, Destination Port %d\n",
                       u4SrcPort, u4DstPort);

        return VLAN_FAILURE;
    }

    if (VlanHwSetPortNumTrafClasses (VLAN_CURR_CONTEXT_ID (), u2DstPort,
                                     pSrcPortEntry->u1NumTrfClass) !=
        VLAN_SUCCESS)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanCopyPortPropertiesToHw (): Unable to program "
                       "Number of Traffic Classes. Src Port %d, Destination Port %d\n",
                       u4SrcPort, u4DstPort);

        return VLAN_FAILURE;
    }

    for (u2PriIndex = 0; u2PriIndex < VLAN_MAX_PRIORITY; u2PriIndex++)
    {
        if ((pSrcPortEntry->u1IfType != VLAN_ETHERNET_INTERFACE_TYPE) &&
            (pSrcPortEntry->u1IfType != VLAN_LAGG_INTERFACE_TYPE))
        {
            /* Priority regen table valid only for non-ethernet interfaces.
             * Since LAGG interfaces are also formed only over ethernet      
             * interfaces, priority regen table not valid for LAGG interfaces
             * also. */
            if (VlanHwSetRegenUserPriority (VLAN_CURR_CONTEXT_ID (),
                                            u2DstPort, (INT4) u2PriIndex,
                                            (INT4) pSrcPortEntry->
                                            au1PriorityRegen[u2PriIndex])
                != VLAN_SUCCESS)
            {
                VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC |
                               INIT_SHUT_TRC, VLAN_NAME,
                               "VlanCopyPortPropertiesToHw (): Setting Regen "
                               "user priority table failed. Src Port %d, Destination "
                               "Port %d.\n", u4SrcPort, u4DstPort);

                return VLAN_FAILURE;
            }
        }

        if (VlanHwSetTraffClassMap (VLAN_CURR_CONTEXT_ID (), u2DstPort,
                                    (INT4) u2PriIndex, (INT4) pSrcPortEntry->
                                    au1TrfClassMap[u2PriIndex]) != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC | INIT_SHUT_TRC,
                           VLAN_NAME,
                           "VlanCopyPortPropertiesToHw (): Setting Traffic "
                           "Class mapping table failed. Src Port %d, Destination Port "
                           "%d.\n", u4SrcPort, u4DstPort);

            return VLAN_FAILURE;
        }
    }

    VLAN_SLL_SCAN (&pSrcPortEntry->VlanVidSet, pPortVidSetEntry,
                   tVlanPortVidSet *)
    {
        if (pPortVidSetEntry->u1RowStatus == VLAN_ACTIVE)
        {
            VlanUpdateGroupVlanMapInfoInHw ((UINT4) u2DstPort,
                                            (UINT4) pPortVidSetEntry->
                                            u4ProtGrpId,
                                            pPortVidSetEntry->VlanId, VLAN_ADD);
        }
    }

    if (VlanHwSetMacBasedStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                       u2DstPort,
                                       VLAN_PORT_MAC_BASED (u2SrcPort)) !=
        VLAN_SUCCESS)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanCopyPortPropertiesToHw (): Setting Mac Based "
                       "Status in Hardware Failed. Src Port %d, Destination Port %d.\n",
                       u4SrcPort, u4DstPort);

        return VLAN_FAILURE;
    }

    if (VlanHwSetSubnetBasedStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                          u2DstPort,
                                          (UINT1)
                                          VLAN_PORT_SUBNET_BASED (u2SrcPort)) !=
        VLAN_SUCCESS)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanCopyPortPropertiesToHw (): Setting Subnet Based "
                       "Status in Hardware Failed. Src Port %d, Destination Port %d.\n",
                       u4SrcPort, u4DstPort);

        return VLAN_FAILURE;
    }

    if (VlanHwEnableProtoVlanOnPort (VLAN_CURR_CONTEXT_ID (), u2DstPort,
                                     VLAN_PORT_PORT_PROTOCOL_BASED (u2SrcPort))
        != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                       "VlanCopyPortPropertiesToHw (): Setting Port Proto "
                       "Based status in Hardware Failed. Src Port %d, Destination Port "
                       "%d.\n", u4SrcPort, u4DstPort);

        return VLAN_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
    {
        u4Mode = VLAN_NO_TUNNEL_PORT;
    }
    else
    {
        if (VLAN_TUNNEL_STATUS (pSrcPortEntry) == VLAN_ENABLED)
        {
            u4Mode = VLAN_TUNNEL_EXTERNAL;

            /* Tunnelling of packets for PCEP and CNP-portbased 
             * ports are not required,as they should only allow
             * the Priority tagged and Vlan Tagged packets*/
            if ((VLAN_PB_PORT_TYPE (u2SrcPort) == VLAN_PROP_CUSTOMER_EDGE_PORT)
                || (VLAN_PB_PORT_TYPE (u2SrcPort) == VLAN_CNP_PORTBASED_PORT))
            {
                u4Mode = VLAN_TUNNEL_INTERNAL;
            }
        }
        else
        {
            /* Bridge is running in Provider network. 
             * By default tunnelling is disabled
             * on all ports. */
            u4Mode = VLAN_TUNNEL_INTERNAL;    /* Indicates this port is 
                                             * connected to another provider 
                                             * bridge */
        }
    }

    if (VlanHwSetPortTunnelMode (VLAN_CURR_CONTEXT_ID (),
                                 u2DstPort, u4Mode) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                       VLAN_NAME, "Setting tunnel mode fails."
                       "Port %d creation failed.\n", u4DstPort);
        return VLAN_FAILURE;
    }

/* Set Egress TPID to default when Bridge Port-type is changed */
    VlanPortProperty.u2OpCode = VLAN_PORT_PROPERTY_CONF_EGRESS_TPID_TYPE;
    VlanPortProperty.u1EgressTPIDType = pSrcPortEntry->u1EgressTPIDType;

    if (VlanHwSetPortProperty (VLAN_CURR_CONTEXT_ID (),
                               u2DstPort, VlanPortProperty) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                       VLAN_NAME, "Setting Egress TPID Type failed."
                       "Port %d creation failed.\n", u4DstPort);

        return SNMP_FAILURE;
    }
#ifndef PB_WANTED
    if (VlanHwSetPortIngressEtherType (VLAN_CURR_CONTEXT_ID (),
                                       u4DstPort,
                                       pSrcPortEntry->
                                       u2IngressEtherType) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                       VLAN_NAME, "Setting Ingress ethertype failed."
                       "Port %d ", u4DstPort);
        return VLAN_FAILURE;
    }
    if (VlanHwSetPortEgressEtherType (VLAN_CURR_CONTEXT_ID (),
                                      u4DstPort,
                                      pSrcPortEntry->
                                      u2EgressEtherType) != VLAN_SUCCESS)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                       VLAN_NAME, "Setting Egress ethertype failed."
                       "Port %d \n", u4DstPort);
        return VLAN_FAILURE;
    }
#endif

#ifdef NPAPI_WANTED
    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {

        if (VlanPbHandleCopyPbPortPropertiesToHw (u2DstPort, u2SrcPort,
                                                  u1InterfaceType)
            != VLAN_SUCCESS)
        {
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC |
                           CONTROL_PLANE_TRC, VLAN_NAME,
                           "VlanHandlePbPortPropertiesToHw failed."
                           "Source Port %d, Destination Port %d.\n",
                           u4SrcPort, u4DstPort);
        }
    }
#else
    UNUSED_PARAM (u1InterfaceType);
#endif

    return VLAN_SUCCESS;
}

#ifdef NPAPI_WANTED
/***************************************************************************/
/* Function Name    : VlanRemovePortPropertiesFromHw ()                    */
/*                                                                         */
/* Description      : This function removes from the Hardware all the      */
/*                    port properties of "u2DstPort". The exact properties */
/*                    which need to be removed is obtained from the        */
/*                    port "u2SrcPort". This function is typically called  */
/*                    when a physical port is removed from a Port Channel. */
/*                    When the physical port is removed from a Port        */
/*                    Channel, the Port Channel properties must be removed */
/*                    from the physical port. This function will be        */
/*                    invoked with "u2DstPort" as the Physical Port Id and */
/*                    "u2SrcPort" as the Port Channel Id.                  */
/*                    "u2SrcPort" must have been created in VLAN.          */
/*                                                                         */
/*                    This function will also be invoked whenever a Port   */
/*                    is deleted in VLAN. In which, u2SrcPort and          */
/*                    u2DstPort will be same.                              */
/*                                                                         */
/* Input(s)         : u4DstIfIndex - Port whose properties have to be      */
/*                                   removed from the Hardware.            */
/*                                                                         */
/*                    u2SrcPort - Port from where the properties to be     */
/*                                removed, have to be obtained.            */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanHandleRemovePortPropertiesFromHw (UINT4 u4DstIfIndex, UINT2 u2SrcPort,
                                      UINT1 u1HwDelDefVlan)
{
    tVlanPortEntry     *pSrcPortEntry = NULL;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tHwVlanPortProperty VlanPortProperty;
    tLocalPortList      Ports;
    UINT4               u4SrcPort;
    tVlanId             VlanId;
    UINT1               u1Result;

    UNUSED_PARAM (u1HwDelDefVlan);
    VLAN_MEMSET (&VlanPortProperty, 0, sizeof (tHwVlanPortProperty));

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRemovePortPropertiesFromHw (): Vlan is Shut down.\n");

        return VLAN_FAILURE;
    }

    pSrcPortEntry = VLAN_GET_PORT_ENTRY (u2SrcPort);

    if (pSrcPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRemovePortPropertiesFromHw (): Invalid Src " "Port \n");

        return VLAN_FAILURE;
    }

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () != VLAN_TRUE)
    {
        return VLAN_SUCCESS;
    }
    u4SrcPort = VLAN_GET_IFINDEX (u2SrcPort);

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {

        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (pCurrEntry == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "VlanRemovePortPropertiesFromHw (): Invalid Vlan Entry \n");

            return VLAN_FAILURE;
        }

        VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2SrcPort, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            /* Delete the Default VLAN from the Hw only when u1HwDelDefVlan
             * is true */
            if (VlanMiHwResetVlanMemberPort (VLAN_CURR_CONTEXT_ID (),
                                             pCurrEntry->VlanId,
                                             u4DstIfIndex) != VLAN_SUCCESS)
            {
                VLAN_TRC_ARG3 (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                               VLAN_NAME,
                               "VlanRemovePortPropertiesFromHw (): "
                               "Unable to remove the Port from the "
                               "Vlan %d. Src Port %d, Destination Port %d\n",
                               pCurrEntry->VlanId, u4SrcPort, u4DstIfIndex);
            }
        }
#ifdef VLAN_EXTENDED_FILTER
        VLAN_IS_MEMBER_PORT (pCurrEntry->AllGrps.Ports, u2SrcPort, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            VLAN_MEMSET (Ports, 0, sizeof (Ports));

            VLAN_SET_MEMBER_PORT (Ports, u2SrcPort);

            if (VlanHwResetDefGroupInfo (VLAN_CURR_CONTEXT_ID (),
                                         pCurrEntry->VlanId,
                                         VLAN_ALL_GROUPS, Ports)
                != VLAN_SUCCESS)
            {
                VLAN_TRC_ARG3 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VlanRemovePortPropertiesFromHw (): "
                               "Unable to remove the All Groups "
                               "membership for the Vlan %d. Src Port %d,"
                               "Destination Port %d.\n", pCurrEntry->VlanId,
                               u4SrcPort, u4DstIfIndex);
            }
        }

        VLAN_IS_MEMBER_PORT (pCurrEntry->UnRegGrps.Ports, u2SrcPort, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            if (VlanHwResetDefGroupInfoForPort (VLAN_CURR_CONTEXT_ID (),
                                                pCurrEntry->VlanId,
                                                u4DstIfIndex,
                                                VLAN_UNREG_GROUPS) !=
                VLAN_SUCCESS)
            {
                VLAN_TRC_ARG3 (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                               "VlanRemovePortPropertiesFromHw (): "
                               "Unable to remove the UnReg Groups "
                               "membership for the Vlan %d. Src Port %d,"
                               "Destination Port %d.\n", pCurrEntry->VlanId,
                               u4SrcPort, u4DstIfIndex);
            }
        }
#else
        UNUSED_PARAM (Ports);
#endif
    }

    VlanHandleRemoveMacBasedOnPort (u4DstIfIndex, (UINT4) u2SrcPort);

    VlanDeleteGroupVlanMapInfoInHw (u4DstIfIndex, u2SrcPort);

    return VLAN_SUCCESS;
}

#endif
/***************************************************************************/
/* Function Name    : VlanHandleCopyPortUcastPropertiesToHw ()             */
/*                                                                         */
/* Description      : This function programs the Unicast entries in the    */
/*                    Hardware. It is called whenever a Port Channel       */
/*                    moves to OPER UP state. This is required because     */
/*                    Unicast membership of Port Channel is not            */
/*                    programmed if the Port Channel is in OPER_DOWN state.*/
/*                                                                         */
/* Input(s)         : u2Port - The Port whose properties have to be        */
/*                             programmed in the Hardware.                 */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanHandleCopyPortUcastPropertiesToHw (UINT2 u2Port)
{
    tVlanPortEntry     *pPortEntry;
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanStUcastEntry  *pStUcastEntry;
    UINT1               u1Result = VLAN_FALSE;
    UINT4               u4Port;
    UINT4               u4FidIndex = 0;
    tVlanId             VlanStartId = 0;
    tVlanId             VlanId = 0;
    tVlanCurrEntry     *pCurrEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleCopyPortUcastPropertiesToHw  (): Vlan is Shut down.\n");

        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleCopyPortUcastPropertiesToHw  (): Invalid Port\n");

        return VLAN_FAILURE;
    }
    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry != NULL)
        {
            pStUcastEntry = pFidEntry->pStUcastTbl;

            while (pStUcastEntry != NULL)
            {
                VLAN_IS_MEMBER_PORT (pStUcastEntry->AllowedToGo,
                                     u2Port, u1Result);

                if (u1Result == VLAN_TRUE)
                {
                    if (VlanHwAddStaticUcastEntry
                        (pFidEntry->u4Fid,
                         pStUcastEntry->MacAddr,
                         pStUcastEntry->u2RcvPort,
                         pStUcastEntry->AllowedToGo,
                         pStUcastEntry->u1Status,
                         pStUcastEntry->ConnectionId) != VLAN_SUCCESS)
                    {
                        u4Port = VLAN_GET_IFINDEX (u2Port);
                        VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                                       VLAN_NAME,
                                       "VlanHandleCopyPortUcastPropertiesToHw  (): "
                                       "Unable to program Static Unicast "
                                       "Entry for the Fid %d. Port %d\n",
                                       pFidEntry->u4Fid, u4Port);

                        return VLAN_FAILURE;
                    }
                }

                pStUcastEntry = pStUcastEntry->pNextNode;
            }
        }
    }

    return VLAN_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanHandleRemovePortUcastPropertiesFromHw ()         */
/*                                                                         */
/* Description      : This function removes the Unicast entries from the   */
/*                    Hardware. It is called whenever a Port Channel       */
/*                    moves to OPER DOWN state.                            */
/*                                                                         */
/* Input(s)         : u2Port - The Port whose properties have to be        */
/*                             removed from the Hardware.                  */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanHandleRemovePortUcastPropertiesFromHw (UINT2 u2Port)
{
    UINT1              *pPorts = NULL;
    tVlanPortEntry     *pPortEntry;
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanStUcastEntry  *pStUcastEntry;
    INT4                i4HwResult;
    UINT1               u1Result = VLAN_FALSE;
    UINT4               u4Port;
    UINT4               u4FidIndex = 0;
    tVlanId             VlanStartId = 0;
    tVlanId             VlanId = 0;
    tVlanCurrEntry     *pCurrEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleRemovePortUcastPropertiesFromHw (): "
                  "Vlan is Shut down.\n");

        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleRemovePortUcastPropertiesFromHw (): Invalid "
                  "Port\n");

        return VLAN_FAILURE;
    }
    VlanStartId = VLAN_START_VLAN_INDEX ();
    pPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleRemovePortUcastPropertiesFromHw: Error in allocating memory "
                  "for pPorts\r\n");
        return VLAN_FAILURE;
    }
    MEMSET (pPorts, 0, sizeof (tLocalPortList));

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry != NULL)
        {
            pStUcastEntry = pFidEntry->pStUcastTbl;

            while (pStUcastEntry != NULL)
            {
                VLAN_IS_MEMBER_PORT (pStUcastEntry->AllowedToGo,
                                     u2Port, u1Result);

                if (u1Result == VLAN_TRUE)
                {
                    VLAN_MEMCPY (pPorts,
                                 pStUcastEntry->AllowedToGo,
                                 sizeof (tLocalPortList));

                    VLAN_RESET_MEMBER_PORT (pPorts, u2Port);

                    if (MEMCMP (pPorts, gNullPortList, sizeof (tLocalPortList))
                        == 0)
                    {
                        i4HwResult =
                            VlanHwDelStaticUcastEntry (pFidEntry->u4Fid,
                                                       pStUcastEntry->MacAddr,
                                                       pStUcastEntry->
                                                       u2RcvPort);
                    }
                    else
                    {
                        i4HwResult =
                            VlanHwAddStaticUcastEntry (pFidEntry->u4Fid,
                                                       pStUcastEntry->MacAddr,
                                                       pStUcastEntry->u2RcvPort,
                                                       pPorts,
                                                       pStUcastEntry->u1Status,
                                                       pStUcastEntry->
                                                       ConnectionId);
                    }

                    if (i4HwResult != VLAN_SUCCESS)
                    {
                        u4Port = VLAN_GET_IFINDEX (u2Port);
                        VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC,
                                       VLAN_NAME,
                                       "VlanHandleRemovePortUcastPropertiesFromHw "
                                       "(): Unable to remove the Port "
                                       "from Static Unicast Entry. Fid "
                                       "%d, Src Port %d, Destination Port "
                                       "%d.\n", pFidEntry->u4Fid, u4Port);
                    }
                }

                pStUcastEntry = pStUcastEntry->pNextNode;
            }
        }
    }
    UtilPlstReleaseLocalPortList (pPorts);

    return VLAN_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanHandleCopyPortMcastPropertiesToHw ()             */
/*                                                                         */
/* Description      : This function programs the Multicast entries in the  */
/*                    Hardware. It is called whenever a Port Channel       */
/*                    moves to OPER UP state or when a member link is      */
/*                    added/removed from a Port Channel.                   */
/*                                                                         */
/*                    This function must called only for Port Channel      */
/*                                                                         */
/* Input(s)         : u2Port - Port Channel Port Id.                       */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanHandleCopyPortMcastPropertiesToHw (UINT2 u2Port)
{
    UINT1              *pHwPorts = NULL;
    tVlanPortEntry     *pPortEntry;
    tVlanCurrEntry     *pCurrEntry;
    tVlanStMcastEntry  *pStMcastEntry;
    tVlanGroupEntry    *pGroupEntry;
    tVlanId             VlanId;
    UINT1               u1Result = VLAN_FALSE;
    UINT4               u4Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanCopyPortMcastPropertiesToHw (): Vlan is Shut down.\n");

        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanCopyPortMcastPropertiesToHw (): Invalid " "Port \n");

        return VLAN_FAILURE;
    }
    pHwPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pHwPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleCopyPortMcastPropertiesToHw: Error in allocating memory "
                  "for pHwPorts\r\n");
        return VLAN_FAILURE;
    }
    u4Port = VLAN_GET_IFINDEX (u2Port);

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (NULL == pCurrEntry)
        {
            continue;
        }
        /* Static Multicast entry processing - START */
        pStMcastEntry = pCurrEntry->pStMcastTable;

        while (pStMcastEntry != NULL)
        {
            MEMCPY (pHwPorts, pStMcastEntry->EgressPorts,
                    sizeof (tLocalPortList));

            if (pStMcastEntry->pGroupEntry != NULL)
            {
                VLAN_ADD_PORT_LIST (pHwPorts,
                                    pStMcastEntry->pGroupEntry->LearntPorts);
            }

            VLAN_IS_MEMBER_PORT (pHwPorts, u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                VLAN_IS_PORT_ONLY_MEMBER (pHwPorts, u2Port, u1Result);

                if (u1Result == VLAN_TRUE)
                {
                    if (VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                             pCurrEntry->VlanId,
                                             pStMcastEntry->MacAddr,
                                             pHwPorts) != VLAN_SUCCESS)
                    {
                        VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                       ALL_FAILURE_TRC,
                                       VLAN_NAME,
                                       "VlanCopyPortMcastPropertiesToHw"
                                       " (): Unable to Add Multicast "
                                       "entry. Port = %d\n", u4Port);

                        UtilPlstReleaseLocalPortList (pHwPorts);
                        return VLAN_FAILURE;
                    }
                }
                else
                {
                    if (VlanHwSetMcastPort (pCurrEntry->VlanId,
                                            pStMcastEntry->MacAddr,
                                            u2Port) != VLAN_SUCCESS)
                    {
                        VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                       ALL_FAILURE_TRC,
                                       VLAN_NAME,
                                       "VlanCopyPortMcastPropertiesToHw (): "
                                       "Unable to program the Multicast "
                                       "membership. Port = %d\n", u4Port);

                        UtilPlstReleaseLocalPortList (pHwPorts);
                        return VLAN_FAILURE;
                    }
                }
            }

            pStMcastEntry = pStMcastEntry->pNextNode;
        }
        /* Static Multicast entry processing - END */

        /* Group entry processing - START */
        pGroupEntry = pCurrEntry->pGroupTable;

        while (pGroupEntry != NULL)
        {
            /*
             * Program only the Group Entries having u1StRefCount set to 0,
             * since the Static Multicast entries were programmed before this.
             */
            if (pGroupEntry->u1StRefCount == 0)
            {
                VLAN_IS_MEMBER_PORT (pGroupEntry->LearntPorts,
                                     u2Port, u1Result);

                if (u1Result == VLAN_TRUE)
                {
                    VLAN_IS_PORT_ONLY_MEMBER (pGroupEntry->LearntPorts,
                                              u2Port, u1Result);

                    if (u1Result == VLAN_TRUE)
                    {
                        if (VlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                                 pCurrEntry->VlanId,
                                                 pGroupEntry->MacAddr,
                                                 pGroupEntry->LearntPorts)
                            != VLAN_SUCCESS)
                        {
                            VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                           ALL_FAILURE_TRC,
                                           VLAN_NAME,
                                           "VlanCopyPortMcastPropertiesToHw"
                                           " (): Unable to Add Group entry. "
                                           "Port = %d\n", u4Port);

                            UtilPlstReleaseLocalPortList (pHwPorts);
                            return VLAN_FAILURE;
                        }
                    }
                    else
                    {
                        if (VlanHwSetMcastPort (pCurrEntry->VlanId,
                                                pGroupEntry->MacAddr,
                                                u2Port) != VLAN_SUCCESS)
                        {
                            VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                           ALL_FAILURE_TRC,
                                           VLAN_NAME,
                                           "VlanCopyPortMcastPropertiesToHw()"
                                           ": Unable to program the Group "
                                           "membership. Port = %d\n", u4Port);

                            UtilPlstReleaseLocalPortList (pHwPorts);
                            return VLAN_FAILURE;
                        }
                    }
                }
            }

            pGroupEntry = pGroupEntry->pNextNode;
        }
        /* Group entry processing - END */
    }

    UtilPlstReleaseLocalPortList (pHwPorts);
    return VLAN_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanHandleRemovePortMcastPropertiesFromHw ()         */
/*                                                                         */
/* Description      : This function removes the Multicast entries from the */
/*                    Hardware. It is called whenever a Port Channel       */
/*                    moves to OPER DOWN state or a member link is         */
/*                    added/removed from a Port Channel.                   */
/*                                                                         */
/*                    This function must be called only for Port Channel.  */
/*                                                                         */
/* Input(s)         : u2Port - Port Channel Port Id.                       */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanHandleRemovePortMcastPropertiesFromHw (UINT2 u2Port)
{
    UINT1              *pHwPorts = NULL;
    tVlanPortEntry     *pPortEntry;
    tVlanCurrEntry     *pCurrEntry;
    tVlanStMcastEntry  *pStMcastEntry;
    tVlanGroupEntry    *pGroupEntry;
    tVlanId             VlanId;
    UINT1               u1Result = VLAN_FALSE;
    UINT4               u4Port;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRemovePortMcastPropertiesFromHw (): "
                  "Vlan is Shut down.\n");

        return VLAN_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

    if (pPortEntry == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanRemovePortMcastPropertiesFromHw (): Invalid " "Port \n");

        return VLAN_FAILURE;
    }
    pHwPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pHwPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanHandleRemovePortMcastPropertiesFromHw: Error in allocating memory "
                  "for pHwPorts\r\n");
        return VLAN_FAILURE;
    }

    u4Port = VLAN_GET_IFINDEX (u2Port);

    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (NULL == pCurrEntry)
        {
            continue;
        }
        /* Static Multicast entry processing - START */
        pStMcastEntry = pCurrEntry->pStMcastTable;

        while (pStMcastEntry != NULL)
        {
            MEMCPY (pHwPorts, pStMcastEntry->EgressPorts,
                    sizeof (tLocalPortList));

            if (pStMcastEntry->pGroupEntry != NULL)
            {
                VLAN_ADD_PORT_LIST (pHwPorts,
                                    pStMcastEntry->pGroupEntry->LearntPorts);
            }

            VLAN_IS_MEMBER_PORT (pHwPorts, u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                VLAN_IS_PORT_ONLY_MEMBER (pHwPorts, u2Port, u1Result);

                if (u1Result == VLAN_TRUE)
                {
                    if (VlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                             pCurrEntry->VlanId,
                                             pStMcastEntry->MacAddr)
                        != VLAN_SUCCESS)
                    {
                        VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                       ALL_FAILURE_TRC,
                                       VLAN_NAME,
                                       "VlanRemovePortMcastPropertiesFromHw"
                                       " (): Unable to Remove Multicast "
                                       "entry. Port = %d\n", u4Port);
                    }
                }
                else
                {
                    if (VlanHwResetMcastPort (pCurrEntry->VlanId,
                                              pStMcastEntry->MacAddr,
                                              u2Port) != VLAN_SUCCESS)
                    {
                        VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                       ALL_FAILURE_TRC,
                                       VLAN_NAME,
                                       "VlanRemovePortMcastPropertiesFromHw "
                                       "(): Unable to remove the Multicast "
                                       "membership. Port = %d\n", u4Port);
                    }
                }
            }

            pStMcastEntry = pStMcastEntry->pNextNode;
        }
        /* Static Multicast entry processing - END */

        /* Group entry processing - START */
        pGroupEntry = pCurrEntry->pGroupTable;

        while (pGroupEntry != NULL)
        {
            /*
             * Program only the Group Entries having u1StRefCount set to 0,
             * since the Static Multicast entries were programmed before this.
             */
            if (pGroupEntry->u1StRefCount == 0)
            {
                VLAN_IS_MEMBER_PORT (pGroupEntry->LearntPorts,
                                     u2Port, u1Result);

                if (u1Result == VLAN_TRUE)
                {
                    VLAN_IS_PORT_ONLY_MEMBER (pGroupEntry->LearntPorts,
                                              u2Port, u1Result);

                    if (u1Result == VLAN_TRUE)
                    {
                        if (VlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                                 pCurrEntry->VlanId,
                                                 pGroupEntry->MacAddr)
                            != VLAN_SUCCESS)
                        {
                            VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                           ALL_FAILURE_TRC,
                                           VLAN_NAME,
                                           "VlanRemovePortMcastPropertiesFromHw"
                                           " (): Unable to Delete Group entry."
                                           " Port = %d\n", u4Port);
                        }
                    }
                    else
                    {
                        if (VlanHwResetMcastPort (pCurrEntry->VlanId,
                                                  pGroupEntry->MacAddr,
                                                  u2Port) != VLAN_SUCCESS)
                        {
                            VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                                           ALL_FAILURE_TRC,
                                           VLAN_NAME,
                                           "VlanRemovePortMcastPropertiesFromHw()"
                                           ": Unable to remove the Group "
                                           "membership. Port = %d\n", u4Port);
                        }
                    }
                }
            }

            pGroupEntry = pGroupEntry->pNextNode;
        }
        /* Group entry processing - END */
    }
    UtilPlstReleaseLocalPortList (pHwPorts);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwDelStaticInfo                              */
/*                                                                           */
/*    Description         : This function will be called to delete the       */
/*                          configured static Vlan information in the        */
/*                          hardware                                         */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified :  None                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion    : None.                                            */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHwDelStaticInfo (VOID)
{
    UINT4               u4FidIndex = 0;
    UINT2               u2HashIndex;
    tVlanId             VlanId;
    UINT2               u2PortInd;
    INT4                i4HwRetVal;
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry;
    tStaticVlanEntry   *pStVlanEntry;
    tVlanProtGrpEntry  *pVlanProtGrpEntry;
    tVlanPortVidSet    *pVlanPortVidSet;
    tVlanStUcastEntry  *pStUcastEntry;
    tVlanId             VlanStartId = 0;

#ifdef NPAPI_WANTED
    UINT4               u4Port;
#endif

    /* Deleting all the Static unicast entries in the database */
    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry != NULL)
        {

            pStUcastEntry = pFidEntry->pStUcastTbl;
            while (pStUcastEntry != NULL)
            {
                VlanHwDelStaticUcastEntry (pFidEntry->u4Fid,
                                           pStUcastEntry->MacAddr,
                                           pStUcastEntry->u2RcvPort);

                pStUcastEntry = pStUcastEntry->pNextNode;
            }
        }
    }
    /* The below loop is for deleting the programmed 
     * static multicast entries in the hardware 
     */
    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (NULL == pCurrEntry)
        {
            continue;
        }
        pStMcastEntry = pCurrEntry->pStMcastTable;

        while (pStMcastEntry != NULL)
        {
            VlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                 pCurrEntry->VlanId, pStMcastEntry->MacAddr);
            pStMcastEntry = pStMcastEntry->pNextNode;
        }
    }

    for (u2HashIndex = 0; u2HashIndex < VLAN_MAX_BUCKETS; u2HashIndex++)
    {
        /* Scanning through the list of group entries in
         * the group table database 
         */
        VLAN_HASH_SCAN_BUCKET (VLAN_PROTOCOL_GROUP_TBL,
                               u2HashIndex, pVlanProtGrpEntry,
                               tVlanProtGrpEntry *)
        {
            /* Check whether the group entry exists in the port
             * VID set.If true delete it from the hardware.
             */
            for (u2PortInd = gpVlanContextInfo->u2VlanStartPortInd;
                 u2PortInd != VLAN_INVALID_PORT_INDEX;
                 u2PortInd = VLAN_GET_NEXT_PORT_INDEX (u2PortInd))
            {
                pVlanPortVidSet = VlanGetPortProtoVidSetEntry
                    (u2PortInd, pVlanProtGrpEntry->u4ProtGrpId);

                if (pVlanPortVidSet == NULL)
                {
                    continue;
                }
                i4HwRetVal = VlanHwDelVlanProtocolMap
                    (VLAN_CURR_CONTEXT_ID (), u2PortInd,
                     pVlanProtGrpEntry->u4ProtGrpId,
                     &pVlanProtGrpEntry->VlanProtoTemplate);

                if (i4HwRetVal == VLAN_FAILURE)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Protocol Vlan Map updation in hardware failed \n");
                }
            }
        }
    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
#ifdef NPAPI_WANTED
        /*Remove the SVLAN classification table entries for all tables */
        VlanPbRemoveHwSVlanClassificationTable ();

        VlanPbRemoveHwSVlanTranslationTable (0, 0);

        VlanPbRemoveHwSVlanEtherTypeSwapTable ();
        VlanPbNpUpdateCvidAndPepInHw (0, 0, 0, VLAN_DELETE);

        /* Reset the EtherType values to default bridge Mode */
        for (u2PortInd = gpVlanContextInfo->u2VlanStartPortInd;
             u2PortInd < VLAN_INVALID_PORT_INDEX;
             u2PortInd = VLAN_GET_NEXT_PORT_INDEX (u2PortInd))
        {
            u4Port = VLAN_GET_IFINDEX (u2PortInd);
            if (VlanHwSetPortIngressEtherType (VLAN_CURR_CONTEXT_ID (),
                                               VLAN_GET_PHY_PORT (u2PortInd),
                                               VLAN_PROTOCOL_ID) !=
                VLAN_SUCCESS)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | CONTROL_PLANE_TRC, VLAN_NAME,
                               "Setting Ingress ethertype failed."
                               "for Port %d.\n", u4Port);
            }

            if (VlanHwSetPortEgressEtherType (VLAN_CURR_CONTEXT_ID (),
                                              u4Port,
                                              VLAN_PROTOCOL_ID) != VLAN_SUCCESS)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | CONTROL_PLANE_TRC, VLAN_NAME,
                               "Setting Egress ethertype failed."
                               "for Port %d.\n", u4Port);
            }
        }
#endif
    }
    /* The below loop is for removing the static vlan entries from the 
     * hardware
     */

    pStVlanEntry = gpVlanContextInfo->pStaticVlanTable;

    while (pStVlanEntry != NULL)
    {

        VlanHwDelVlanEntry (VLAN_CURR_CONTEXT_ID (), pStVlanEntry->VlanId);
        pStVlanEntry = pStVlanEntry->pNextNode;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwAddStaticInfo                              */
/*                                                                           */
/*    Description         : This function will be called to add the          */
/*                          configured static Vlan information in the        */
/*                          hardware                                         */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified :  None                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion    : None.                                            */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHwAddStaticInfo (VOID)
{
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry;
    tStaticVlanEntry   *pStVlanEntry;
    tVlanProtGrpEntry  *pVlanProtGrpEntry;
    tVlanPortVidSet    *pVlanPortVidSet;
    tVlanStUcastEntry  *pStUcastEntry;
    tVlanPortEntry     *pPortEntry;
    UINT1              *pu1LocalPortList = NULL;
    UINT1              *pu1UntagLocalPortList = NULL;
    UINT4               u4FidIndex = 0;
    UINT2               u2HashIndex;
    tVlanId             VlanId;
    UINT2               u2PortInd;
    UINT2               u2Port;
    UINT4               u4Mode;
    UINT4               u4DstIfIndex;
    INT4                i4HwRetVal;
    INT4                i4RetVal;
    UINT1               u1InterfaceType = VLAN_INVALID_INTERFACE_TYPE;
    UINT4               u4Port;
    UINT4               u4PhyPort = 0;
    tVlanId             VlanStartId = 0;

    pStVlanEntry = gpVlanContextInfo->pStaticVlanTable;

    pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1LocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Memory allocation failed for tLocalPortList\n");
        return;
    }
    pu1UntagLocalPortList = UtilPlstAllocLocalPortList
        (sizeof (tLocalPortList));
    if (pu1UntagLocalPortList == NULL)
    {
        UtilPlstReleaseLocalPortList (pu1LocalPortList);
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Memory allocation failed for tLocalPortList\n");
        return;
    }

    /* The below loop is for adding the static vlan entries into the 
     * hardware
     */
    while (pStVlanEntry != NULL)
    {
        MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
        VLAN_GET_EGRESS_PORTS (pStVlanEntry, pu1LocalPortList);

        MEMSET (pu1UntagLocalPortList, 0, sizeof (tLocalPortList));
        VLAN_GET_UNTAGGED_PORTS (pStVlanEntry, pu1UntagLocalPortList);

        if (VlanHwAddVlanEntry (pStVlanEntry->VlanId, pu1LocalPortList,
                                pu1UntagLocalPortList) != VLAN_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pu1LocalPortList);
            UtilPlstReleaseLocalPortList (pu1UntagLocalPortList);
            return;
        }

#ifdef NPAPI_WANTED
        if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
        {
            VlanConfHwLearningProperties (pStVlanEntry->VlanId);
        }
#endif

        pStVlanEntry = pStVlanEntry->pNextNode;
    }
    UtilPlstReleaseLocalPortList (pu1LocalPortList);
    UtilPlstReleaseLocalPortList (pu1UntagLocalPortList);

    /* For each individual port which had been created when the 
     * Vlan is disabled,we need to configure the h/w for the port*/
    for (u2Port = gpVlanContextInfo->u2VlanStartPortInd;
         u2Port < VLAN_INVALID_PORT_INDEX;
         u2Port = VLAN_GET_NEXT_PORT_INDEX (u2Port))
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);
        u4Port = VLAN_GET_IFINDEX (u2Port);

        if (pPortEntry == NULL)
        {
            continue;
        }

        if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
        {
            i4RetVal = VlanHwSetPortTunnelMode (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                VLAN_NO_TUNNEL_PORT);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | CONTROL_PLANE_TRC, VLAN_NAME,
                               "Setting tunnel mode fails." "for Port %d.\n",
                               u4Port);
            }
        }
        else
        {
            /* Bridge is running in Provider network. 
             * By default tunnelling is disabled on all ports. */
            if (VLAN_TUNNEL_STATUS (pPortEntry) == VLAN_ENABLED)
            {
                u4Mode = VLAN_TUNNEL_EXTERNAL;
            }
            else
            {
                u4Mode = VLAN_TUNNEL_INTERNAL;
            }

            i4RetVal = VlanHwSetPortTunnelMode (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                u4Mode);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                               ALL_FAILURE_TRC | CONTROL_PLANE_TRC, VLAN_NAME,
                               "Setting tunnel mode fails." "for Port %d.\n",
                               u4Port);

                VLAN_TUNNEL_STATUS (pPortEntry) = VLAN_DISABLED;
            }
#ifdef NPAPI_WANTED
            if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
            {
                VlanL2IwfGetInterfaceType (VLAN_CURR_CONTEXT_ID (),
                                           &u1InterfaceType);

                u4DstIfIndex = VLAN_GET_PHY_PORT (u2Port);
                if (VlanPbHandleCopyPbPortPropertiesToHw (u4DstIfIndex, u2Port,
                                                          u1InterfaceType)
                    == VLAN_FAILURE)
                {
                    VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC
                                   | INIT_SHUT_TRC, VLAN_NAME,
                                   "Setting provider bridge port related "
                                   " properties failed"
                                   " for Port %d. \n", u4Port);

                }
            }
#else
            UNUSED_PARAM (u4DstIfIndex);
            UNUSED_PARAM (u1InterfaceType);
#endif
        }

        if (L2IwfMiIsVlanActive (VLAN_CURR_CONTEXT_ID (),
                                 pPortEntry->Pvid) == OSIX_TRUE)
        {
            i4RetVal = VlanHwSetPortPvid (VLAN_CURR_CONTEXT_ID (),
                                          u2Port, pPortEntry->Pvid);

            if (i4RetVal == VLAN_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | INIT_SHUT_TRC,
                               VLAN_NAME,
                               "Setting default PVID failed for Port %d. \n",
                               u4Port);

                pPortEntry->Pvid = VLAN_DEFAULT_PORT_VID;
                L2IwfSetVlanPortPvid (VLAN_CURR_CONTEXT_ID (), u2Port,
                                      pPortEntry->Pvid);
            }
        }
        else
        {
            VLAN_TRC_ARG2 (VLAN_MOD_TRC, ALL_FAILURE_TRC | INIT_SHUT_TRC,
                           VLAN_NAME,
                           "Setting default PVID failed - Vlan %d not activefor Port %d. \n",
                           pPortEntry->Pvid, u4Port);
            pPortEntry->Pvid = VLAN_DEFAULT_PORT_VID;
            L2IwfSetVlanPortPvid (VLAN_CURR_CONTEXT_ID (), u2Port,
                                  pPortEntry->Pvid);
        }

        i4RetVal =
            VlanHwSetPortAccFrameType (VLAN_CURR_CONTEXT_ID (),
                                       u2Port, pPortEntry->u1AccpFrmTypes);

        if (i4RetVal == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | INIT_SHUT_TRC,
                           VLAN_NAME,
                           "Setting acceptable frame type failed for Port %d.\n",
                           u4Port);
            pPortEntry->u1AccpFrmTypes = VLAN_ADMIT_ALL_FRAMES;

            L2IwfSetVlanPortAccpFrmType (VLAN_CURR_CONTEXT_ID (), u2Port,
                                         pPortEntry->u1AccpFrmTypes);
        }

        i4RetVal =
            VlanHwSetPortIngFiltering (VLAN_CURR_CONTEXT_ID (), u2Port,
                                       pPortEntry->u1IngFiltering);

        if (i4RetVal == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | INIT_SHUT_TRC,
                           VLAN_NAME,
                           "Setting ingress filtering failed for Port %d.\n",
                           u4Port);
            pPortEntry->u1IngFiltering = VLAN_DISABLED;
        }

        if (i4RetVal == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | INIT_SHUT_TRC,
                           VLAN_NAME,
                           "Setting port type failed for Port %d. \n", u4Port);
            pPortEntry->u1PortType = VLAN_HYBRID_PORT;
        }

        i4RetVal =
            VlanHwSetDefUserPriority (VLAN_CURR_CONTEXT_ID (),
                                      u2Port, pPortEntry->u1DefUserPriority);

        if (i4RetVal == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | INIT_SHUT_TRC,
                           VLAN_NAME,
                           "Setting default user priority failed for Port %d. \n",
                           u4Port);

            pPortEntry->u1DefUserPriority = VLAN_DEF_USER_PRIORITY;
        }

        i4RetVal = VlanHwSetMacBasedStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                  u2Port,
                                                  VLAN_PORT_MAC_BASED (u2Port));

        if (i4RetVal == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | INIT_SHUT_TRC,
                           VLAN_NAME,
                           "Setting MAC based VLAN status failed for Port %d. \n",
                           u4Port);
            VLAN_PORT_MAC_BASED (u2Port) = VLAN_MAC_BASED;
        }

        i4RetVal = VlanHwSetSubnetBasedStatusOnPort (VLAN_CURR_CONTEXT_ID (),
                                                     u2Port,
                                                     (UINT1)
                                                     VLAN_PORT_SUBNET_BASED
                                                     (u2Port));

        if (i4RetVal == VLAN_FAILURE)
        {
            u4PhyPort = VLAN_GET_PHY_PORT (u2Port);
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | INIT_SHUT_TRC,
                           VLAN_NAME,
                           "Setting Subnet based VLAN status failed for Port %d\n",
                           u4PhyPort);
            VLAN_SET_PORT_SUBNET_BASED (u2Port, VLAN_SUBNET_BASED);
        }

        i4RetVal = VlanHwEnableProtoVlanOnPort (VLAN_CURR_CONTEXT_ID (), u2Port,
                                                VLAN_PORT_PORT_PROTOCOL_BASED
                                                (u2Port));

        if (i4RetVal == VLAN_FAILURE)
        {
            VLAN_TRC_ARG1 (VLAN_MOD_TRC, ALL_FAILURE_TRC | INIT_SHUT_TRC,
                           VLAN_NAME,
                           "Setting Protocol based VLAN status failed for "
                           "Port %d.\n", u4Port);
            if (VLAN_PORT_PORT_PROTOCOL_BASED (u2Port) != VLAN_PORT_PROTO_BASED)
            {
                VLAN_PORT_PORT_PROTOCOL_BASED (u2Port) = VLAN_PORT_PROTO_BASED;
                /* Only if there is a change in both the values notification 
                 * need to be sent */
                VlanLldpApiNotifyProtoVlanStatus (u2Port,
                                                  VLAN_PORT_PROTO_BASED);
            }
        }

    }

    if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
    {
#ifdef NPAPI_WANTED
        VlanPbConfEtherSwapEntry ();
        VlanPbConfSVlanTranslationTable (0, 0);
        VlanPbConfHwSVlanClassificationTable ();
#endif
    }
    /* program all port based 802.1p parameters in hardware */
    VlanHwEnablePriorityModule ();

    /* The below loop is for
     * (i) adding the programmed static multicast entries in the hardware 
     * (ii) Initializing the default group information for this VLAN
     */
    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (NULL == pCurrEntry)
        {
            continue;
        }

        VlanInitializeDefGroupInfo (pCurrEntry);
        pStMcastEntry = pCurrEntry->pStMcastTable;

        while (pStMcastEntry != NULL)
        {
            VlanHwAddStMcastEntry (pCurrEntry->VlanId,
                                   pStMcastEntry->MacAddr,
                                   pStMcastEntry->u2RcvPort,
                                   pStMcastEntry->EgressPorts);

            pStMcastEntry = pStMcastEntry->pNextNode;
        }
    }

    /* Adding all the Static unicast entries in the database */
    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if (pCurrEntry == NULL)
        {
            continue;
        }

        u4FidIndex = pCurrEntry->u4FidIndex;

        pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

        if (pFidEntry != NULL)
        {
            pStUcastEntry = pFidEntry->pStUcastTbl;

            while (pStUcastEntry != NULL)
            {
                if (VlanHwAddStaticUcastEntry (pFidEntry->u4Fid,
                                               pStUcastEntry->MacAddr,
                                               pStUcastEntry->u2RcvPort,
                                               pStUcastEntry->AllowedToGo,
                                               pStUcastEntry->u1Status,
                                               pStUcastEntry->ConnectionId)
                    != VLAN_SUCCESS)
                {
                    return;
                }

                pStUcastEntry = pStUcastEntry->pNextNode;
            }
        }
    }

    for (u2HashIndex = 0; u2HashIndex < VLAN_MAX_BUCKETS; u2HashIndex++)
    {
        /* Scanning through the list of group entries in
         * the group table database 
         */
        VLAN_HASH_SCAN_BUCKET (VLAN_PROTOCOL_GROUP_TBL,
                               u2HashIndex, pVlanProtGrpEntry,
                               tVlanProtGrpEntry *)
        {
            /* Check whether the group entry exists in the port
             * VID set.If true add it to the hardware.
             */
            for (u2PortInd = gpVlanContextInfo->u2VlanStartPortInd;
                 u2PortInd != VLAN_INVALID_PORT_INDEX;
                 u2PortInd = VLAN_GET_NEXT_PORT_INDEX (u2PortInd))
            {
                if ((pVlanPortVidSet = VlanGetPortProtoVidSetEntry
                     (u2PortInd, pVlanProtGrpEntry->u4ProtGrpId)) == NULL)
                {
                    continue;
                }

                i4HwRetVal =
                    VlanHwAddVlanProtocolMap (VLAN_CURR_CONTEXT_ID (),
                                              u2PortInd,
                                              pVlanProtGrpEntry->u4ProtGrpId,
                                              &pVlanProtGrpEntry->
                                              VlanProtoTemplate,
                                              pVlanPortVidSet->VlanId);

                if (i4HwRetVal == VLAN_FAILURE)
                {
                    VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                              "Protocol Vlan Map updation in hardware failed \n");

                }
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetMacMapMcastBcastFilterOption              */
/*                                                                           */
/*    Description         : This function is used to get the Bcast option    */
/*                          of the MacMap table                              */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u2InPort - Port in which the option is to be     */
/*                                     viewed                                */
/*                          SrcAddr  - Src Mac for which the option is       */
/*                                     viewed                                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified :  None                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion    : None.                                            */
/*                                                                           */
/*    Returns             : return i4McastOption/return FAILURE              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetMacMapMcastBcastFilterOption (UINT2 u2InPort, tMacAddr SrcAddr)
{
    tVlanMacMapEntry   *pVlanMacMapEntry = NULL;

    if (VLAN_PORT_MAC_BASED (u2InPort) != VLAN_ENABLED)
    {
        return VLAN_FAILURE;
    }

    pVlanMacMapEntry = VlanGetMacMapEntry (SrcAddr, (UINT4) u2InPort);

    if (pVlanMacMapEntry != NULL)
    {
        return ((INT4) pVlanMacMapEntry->u1McastOption);
    }

    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetSubnetMapARPFilterOption                  */
/*                                                                           */
/*    Description         : This function is used to get the ARP option      */
/*                          of the Subnet Map table                          */
/*                                                                           */
/*    Input(s)            : u2InPort - Port in which the option is to be     */
/*                                     viewed                                */
/*                          SubnetAddr  - Src Subnet for which the option    */
/*                                        is viewed                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified :  None                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion    : None.                                            */
/*                                                                           */
/*    Returns             : return i4ARPOption/return FAILURE                */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetSubnetMapArpFilterOption (UINT2 u2InPort, UINT4 SubnetAddr)
{
    tVlanSubnetMapEntry *pVlanSubnetMapEntry = NULL;
    UINT4               u4DefMask = VLAN_INIT_VAL;

    if (VLAN_PORT_SUBNET_BASED (u2InPort) != VLAN_ENABLED)
    {
        return VLAN_FAILURE;
    }

    u4DefMask = VlanGetDefMaskFromSrcIPAddr (SubnetAddr);

    pVlanSubnetMapEntry = VlanGetSubnetMapEntry (SubnetAddr, (UINT4) u2InPort,
                                                 u4DefMask);

    if (pVlanSubnetMapEntry != NULL)
    {
        return ((INT4) pVlanSubnetMapEntry->u1ArpOption);
    }

    return VLAN_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : VlanFwdUnicastDataPacket                         */
/*    Description         : This function will be called to forward unicast  */
/*                          Packet recvd on MPLS Port/ Ethernet Port to      */
/*                          Ethernet Port / MPLS or Ethernet Port respectively*/
/*                                                                           */
/*    Input(s)            : pFrame - Pkt recvd on MPLS Port/ Eth Port        */
/*                          pVlanIf - Message filled with Packet             */
/*                          pVlanRec - Vlan Current Entry                    */
/*                          pVlanTag - packet vlan tag information           */
/*    Output(s)           : None                                             */
/*    Returns             : None                                             */
/*****************************************************************************/

VOID
VlanFwdUnicastDataPacket (tCRU_BUF_CHAIN_HEADER * pFrame, tVlanIfMsg * pVlanIf,
                          tVlanCurrEntry * pVlanRec, tVlanTag * pVlanTag)
{
    UINT1              *pu1LocalPortList = NULL;
    UINT1              *pPortList = NULL;
    tVlanStUcastEntry  *pStUcastEntry = NULL;
    tVlanFdbEntry      *pUcastEntry = NULL;
    tCRU_BUF_CHAIN_DESC *pDupBuf = NULL;
#ifndef NPAPI_WANTED
    tVlanStats         *pVlanCounters = NULL;
#endif
    UINT4               u4FidIndex;
    INT4                i4RetVal;
    tVlanWildCardEntry *pWildCardEntry = NULL;
    UINT2               u2InPort;
    UINT4               u4Port;
#ifdef MPLS_WANTED
#ifdef HVPLS_WANTED
    if (pVlanRec != NULL)
#endif
#endif
    {
        u4FidIndex = pVlanRec->u4FidIndex;
    }
#ifdef MPLS_WANTED
#ifdef HVPLS_WANTED
    else
    {
        u4FidIndex = VLAN_L2VPN_DEF_PORT_VID;
    }
#endif
#endif
    u2InPort = VLAN_IFMSG_PORT (pVlanIf);

    pStUcastEntry = VlanGetStaticUcastEntry (u4FidIndex, pVlanIf->DestAddr,
                                             u2InPort);

    if (pStUcastEntry != NULL)
    {
        /* 
         * Static forwarding information is present for the given 
         * destination mac address and the given receive port.
         * So forward the frame on the Allowed To Go Port list
         */

        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Unicast Data Packet received on Port %d is to be "
                       "forwarded on Static UCAST Egress Ports ",
                       pVlanIf->u4IfIndex);

        VLAN_PRINT_PORT_LIST (VLAN_MOD_TRC, pStUcastEntry->AllowedToGo,
                              u2InPort);

#ifndef NPAPI_WANTED
        if ((pVlanRec != NULL) && (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE))
        {
            pVlanCounters = pVlanRec->pVlanStats;
            if (pVlanCounters != NULL)
            {
                VLAN_INC_OUT_UCAST (pVlanCounters);
            }
        }
#endif

        VlanFwdOnPorts (pFrame, pVlanIf, pStUcastEntry->AllowedToGo,
                        pVlanRec, pVlanTag, BRG_DATA_FRAME);

        /* Static ucast Entry will be fwd only on Allowed to go port
         * list and not on MPLS Port. Once MPLS Port configuration in
         * static ucast entry is supported, this will fwd for MPLS port
         * too*/
        return;
    }

    /* 
     * No Static information is present. This could bo one of the 2 cases :
     * 1. No Static entry present for this destination mac address.
     * 2. Static entry is present for this destination mac address, but
     *    the receive port does not match with the input port.
     */

    pUcastEntry = VlanGetUcastEntry (u4FidIndex, pVlanIf->DestAddr);
    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "VlanFwdUnicastDataPacket: Error in allocating memory "
                  "for pPortList\r\n");
        return;
    }
    MEMSET (pPortList, 0, sizeof (tLocalPortList));

    if (pUcastEntry != NULL)
    {
        u4Port = VLAN_GET_IFINDEX (pUcastEntry->u2Port);
        if (pUcastEntry->u2Port != VLAN_INVALID_PORT)
        {

            /* Dynamic unicast entry is present */

            i4RetVal = VlanEgressFiltering (pUcastEntry->u2Port, pVlanRec,
                                            pVlanIf->DestAddr);

            if ((u2InPort != pUcastEntry->u2Port) && (i4RetVal == VLAN_FORWARD))
            {
                if (pUcastEntry->u2Port >= VLAN_MAX_PORTS + 1)
                {
                    UtilPlstReleaseLocalPortList (pPortList);
                    return;
                }

                VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                               "Unicast Data Packet received on Port %d is to "
                               "be forwarded on Port %d \n", u2InPort, u4Port);

                pDupBuf = VlanDupBuf (pFrame);

                if (pDupBuf == NULL)
                {
                    UtilPlstReleaseLocalPortList (pPortList);
                    return;
                }

#ifndef NPAPI_WANTED
                if ((pVlanRec != NULL)
                    && (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE))
                {
                    pVlanCounters = pVlanRec->pVlanStats;
                    if (pVlanCounters != NULL)
                    {
                        VLAN_INC_OUT_UCAST (pVlanCounters);
                    }
                }
#endif

                VlanFwdOnSinglePort (pDupBuf, pVlanIf, pUcastEntry->u2Port,
                                     pVlanRec, pVlanTag, BRG_DATA_FRAME);
            }
            else
            {

                /* 
                 * Either the Egress filtering rules have filtered the frame
                 * or the reception port matches the out bound port.
                 * So discard the frame.
                 */
                if (VLAN_IS_PKT_RCVD_FROM_MPLS (pVlanIf) != VLAN_TRUE)
                {
                    /*For the Packet received from MPLS will have 
                     * receive port as zero*/
                    VlanL2IwfIncrFilterInDiscards (u2InPort);
                }

            }
        }
        else
        {
            if (pUcastEntry->u1Status == VLAN_FDB_LEARNT)
            {
                VLAN_TRC_ARG2 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                               "Unicast Data Packet received on Port %d is to "
                               "be forwarded on MPLS Port %d \n", u2InPort,
                               u4Port);

                /*Mac Learned on MPLS Interface, So Fwd to MPLS Module */
                VlanL2VpnFwdLearntPktOnMplsPort (pFrame, pVlanIf, pVlanRec,
                                                 pVlanTag,
                                                 pUcastEntry->u4PwVcIndex);
            }
            else
            {
                VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST (pVlanRec, pPortList);

                /*Add WildCard Ports also in forward Port List */
                pWildCardEntry = VlanGetWildCardEntry (pVlanIf->DestAddr);

                if (pWildCardEntry != NULL)
                {
                    VLAN_ADD_PORT_LIST (pPortList, pWildCardEntry->EgressPorts);
                }

                VlanFwdOnPorts (pFrame, pVlanIf, pPortList, pVlanRec, pVlanTag,
                                BRG_DATA_FRAME);
            }

        }
    }
    else
    {
        /* pUcastEntry is NULL here */

        /* 
         * Neither Static nor Dynamic forwarding information present. So
         * flood the frame on all the member ports of the Vlan.
         */
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, DATA_PATH_TRC, VLAN_NAME,
                       "Unicast Data Packet received on Port %d is to be "
                       "forwarded on VLAN Egress Ports ", pVlanIf->u4IfIndex);

        pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
        if (pu1LocalPortList == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                      "Memory allocation failed for tLocalPortList\n");
            UtilPlstReleaseLocalPortList (pPortList);
            return;
        }
        if (pVlanRec != NULL)
        {
            MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
            VLAN_GET_CURR_EGRESS_PORTS (pVlanRec, pu1LocalPortList);

            VLAN_PRINT_PORT_LIST (VLAN_MOD_TRC, pu1LocalPortList, u2InPort);
            UtilPlstReleaseLocalPortList (pu1LocalPortList);

#ifndef NPAPI_WANTED
            if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
            {
                pVlanCounters = pVlanRec->pVlanStats;
                if (pVlanCounters != NULL)
                {
                    VLAN_INC_OUT_UNKNOWN_UCAST (pVlanCounters);
                }
            }
#endif

            VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST (pVlanRec, pPortList);
        }
        else
        {
            UtilPlstReleaseLocalPortList (pu1LocalPortList);
        }

        /*Add WildCard Ports also in forward Port List */
        pWildCardEntry = VlanGetWildCardEntry (pVlanIf->DestAddr);

        if (pWildCardEntry != NULL)
        {
            VLAN_ADD_PORT_LIST (pPortList, pWildCardEntry->EgressPorts);
        }
        if (VlanL2VpnPktFloodOnMplsPort (pFrame, pVlanIf, pVlanRec, pVlanTag)
            != VLAN_FORWARD)
        {
            VlanFwdOnPorts (pFrame, pVlanIf, pPortList, pVlanRec, pVlanTag,
                            BRG_DATA_FRAME);
        }

    }

    UtilPlstReleaseLocalPortList (pPortList);
    return;

}

/*****************************************************************************/
/*    Function Name       : VlanGetMcastFwdPortList                          */
/*    Description         : This function will be called while processing    */
/*                          mcast frame. This function will return the ports */
/*                          to which the mcast packet has to be forwarded    */
/*                                                                           */
/*    Input(s)            : OutDestAddr- Mcast Address of the packet         */
/*                          u2LocalPort- Port on which mcast pkt is rcvd     */
/*                          pVlanRec - Vlan Current Entry                    */
/*    Output(s)           : FwdPortList - list of ports to which mcast pkt   */
/*                          has to be forwarded.                             */
/*    Returns             : None                                             */
/*****************************************************************************/

VOID
VlanGetMcastFwdPortList (tMacAddr OutDestAddr, UINT2 u2LocalPort,
                         tVlanCurrEntry * pVlanRec, tLocalPortList FwdPortList)
{
    UINT1              *pu1LocalPortList = NULL;
    tVlanGroupEntry    *pGroupEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanWildCardEntry *pWildCardEntry = NULL;
    UINT1               u1FloodStatus = VLAN_ENABLED;

    pGroupEntry = VlanGetGroupEntry (OutDestAddr, pVlanRec);

    if (pGroupEntry != NULL)
    {
        if (MEMCMP (pGroupEntry->LearntPorts,
                    gNullPortList, sizeof (tLocalPortList)) == 0)
        {
            /* In Transparent Bridging, forward multicast packets only to
             * Allowed ports in the Static Multicast Table */
            if (VLAN_BASE_BRIDGE_MODE () == DOT_1Q_VLAN_MODE)
            {
#ifdef VLAN_EXTENDED_FILTER
                /* Use unregister group ports if no dynamic info is present */
                MEMCPY (FwdPortList, pVlanRec->UnRegGrps.Ports,
                        sizeof (tLocalPortList));
#endif
            }
        }
        else
        {
            MEMCPY (FwdPortList, pGroupEntry->LearntPorts,
                    sizeof (tLocalPortList));
        }

        if (pGroupEntry->u1StRefCount != 0)
        {
            /* some static entry exist for this mcast address */
            pStMcastEntry =
                VlanGetStMcastEntry (OutDestAddr, u2LocalPort, pVlanRec);

            if (pStMcastEntry != NULL)
            {
                MEMSET (FwdPortList, 0, sizeof (tLocalPortList));
                MEMCPY (FwdPortList, pGroupEntry->LearntPorts, sizeof
                        (tLocalPortList));
            }
        }
    }
    else
    {
#ifdef VLAN_EXTENDED_FILTER

        if (VLAN_IS_BCASTADDR (OutDestAddr) == VLAN_FALSE)
        {
            MEMCPY (FwdPortList, pVlanRec->UnRegGrps.Ports,
                    sizeof (tLocalPortList));
        }
        else
        {
            VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST (pVlanRec, FwdPortList);
        }

#else
        VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST (pVlanRec, FwdPortList);
#endif

    }

#ifdef VLAN_EXTENDED_FILTER
    VLAN_ADD_PORT_LIST (FwdPortList, pVlanRec->AllGrps.Ports);
#endif

    pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1LocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Memory allocation failed for tLocalPortList\n");
        return;
    }
    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
    VLAN_GET_CURR_EGRESS_PORTS (pVlanRec, pu1LocalPortList);

    VLAN_AND_PORT_LIST (FwdPortList, pu1LocalPortList);
    UtilPlstReleaseLocalPortList (pu1LocalPortList);
    VlanL2IwfGetVlanFloodingStatus (VLAN_CURR_CONTEXT_ID (),
                                    pVlanRec->VlanId, &u1FloodStatus);

    if (pStMcastEntry != NULL)
    {
        VLAN_ADD_PORT_LIST (FwdPortList, pStMcastEntry->EgressPorts);
        VLAN_RESET_PORT_LIST (FwdPortList, pStMcastEntry->ForbiddenPorts);
    }
    else
    {
        if (u1FloodStatus == VLAN_DISABLED)
        {
            /* If flooding is disabled then we should NOT return any ports
             * if no group entry or static entry is found for the DA */
            return;
        }
        /* If Static entry doest not exist add, wildcard port list in
         * FwdPortList
         */
        pWildCardEntry = VlanGetWildCardEntry (OutDestAddr);
        if (pWildCardEntry != NULL)
        {
            VLAN_ADD_PORT_LIST (FwdPortList, pWildCardEntry->EgressPorts);
        }
    }

    return;

}

/************************************************************************/
/* Function Name    : VlanInitGlobalInfo                                */
/*                                                                      */
/* Description      : Initializes Global Task Info                      */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
INT4
VlanInitGlobalInfo (VOID)
{
    UINT4               u4Context;

    for (u4Context = 0; u4Context < VLAN_SIZING_CONTEXT_COUNT; u4Context++)
    {
        gau1VlanShutDownStatus[u4Context] = VLAN_SNMP_TRUE;
        gau1VlanStatus[u4Context] = VLAN_DISABLED;
    }

    /* Intialise global IfIndex based RBTree (tVlanPortEntry) */
    /* get proper offset and pass  */

    VLAN_GLOBAL_PORT_RBTREE () =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVlanPortEntry, RbNode),
                              VlanGlobalPortTblCmp);
    if (VLAN_GLOBAL_PORT_RBTREE () == NULL)
    {
        return VLAN_FAILURE;
    }

    /* Initialise all protocol related memory pools with default values. 
     * Memory type will be determined by the Mode. In case of SI it will be 
     * default type and for MI it is heap type. */

    VLAN_CURR_CONTEXT_PTR () = (tVlanContextInfo *) (VOID *)
        VLAN_GET_BUF (VLAN_CONTEXT_INFO, sizeof (tVlanContextInfo));
    if (VLAN_CURR_CONTEXT_PTR () == NULL)
    {
        return VLAN_FAILURE;
    }

    VLAN_MEMSET (VLAN_CURR_CONTEXT_PTR (), 0, sizeof (tVlanContextInfo));

    VLAN_CONTEXT_PTR (VLAN_DEF_CONTEXT_ID) = VLAN_CURR_CONTEXT_PTR ();

    /*Initialise per context global variables */
    VLAN_LOCAL_PORT_RBTREE () = RBTreeCreateEmbedded (0, VlanGlobalPortTblCmp);

    VLAN_CURR_CONTEXT_PTR ()->VlanStartVlanInd = VLAN_INVALID_VLAN_INDEX;
    VLAN_CURR_CONTEXT_PTR ()->u2VlanStartPortInd = VLAN_INVALID_PORT_INDEX;
    VLAN_CURR_CONTEXT_PTR ()->u2VlanLastPortInd = VLAN_INVALID_PORT_INDEX;
    VLAN_CURR_CONTEXT_PTR ()->u4VlanDbg = 0;
    VLAN_CURR_CONTEXT_PTR ()->u4ContextId = VLAN_DEF_CONTEXT_ID;
    VLAN_CURR_CONTEXT_PTR ()->u4BaseBridgeMode = DOT_1Q_VLAN_MODE;
    VLAN_CURR_CONTEXT_PTR ()->u1GlobalMacLearningStatus = VLAN_ENABLED;
    VLAN_CURR_CONTEXT_PTR ()->i4BridgeBaseRowStatus = VLAN_ACTIVE;
    VlanCfaGetSysMacAddress (VLAN_CURR_CONTEXT_PTR ()->VlanSysMacAddress);
    VLAN_CURR_CONTEXT_PTR ()->u2UserDefinedTPID = 0;
#ifndef NPAPI_WANTED
    VLAN_CURR_CONTEXT_PTR ()->VlanInfo.bUseShortAgingTime = VLAN_FALSE;
    VLAN_CURR_CONTEXT_PTR ()->VlanInfo.i4RapidAgingNumPorts = 0;
#endif
    VLAN_CURR_CONTEXT_PTR ()->u4AgeOutTime = VLAN_NORMAL_AGEOUT_TIME;

    VLAN_MEMSET (VLAN_CURR_CONTEXT_STR (), 0, VLAN_CONTEXT_ALIAS_LEN + 1);

    if (VlanVcmGetSystemModeExt (VLANMOD_PROTOCOL_ID) == VCM_MI_MODE)
    {
        VlanVcmGetAliasName (VLAN_CURR_CONTEXT_ID (), VLAN_CURR_CONTEXT_STR ());
        STRCAT (VLAN_CURR_CONTEXT_STR (), ":");
    }

    VLAN_SLL_INIT (&gVlanTempPortList);

    VLAN_SLL_INIT (&gVlanMcagEntries);

    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name    : VlanDeInitGlobalInfo                              */
/*                                                                      */
/* Description      : Deinitializes Global Task Info                    */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
INT4
VlanDeInitGlobalInfo (VOID)
{
    UINT4               u4Context;

    for (u4Context = 0; u4Context < VLAN_SIZING_CONTEXT_COUNT; u4Context++)
    {
        gau1VlanShutDownStatus[u4Context] = VLAN_SNMP_TRUE;
        gau1VlanStatus[u4Context] = VLAN_DISABLED;
    }

    VlanHandleDeleteContext (VLAN_DEF_CONTEXT_ID);

    /* destroy global IfIndex based RBTree (tVlanPortEntry) */
    if (VLAN_GLOBAL_PORT_RBTREE () != NULL)
    {
        RBTreeDestroy (VLAN_GLOBAL_PORT_RBTREE (),
                       (tRBKeyFreeFn) VlanGlobalPortFreeFunc, 0);
        VLAN_GLOBAL_PORT_RBTREE () = NULL;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSelectContext                                */
/*                                                                           */
/*    Description         : This function switches to given context          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR (), VLAN_CONTEXT_PTR.*/
/*                                                                           */
/*    Global Variables Modified : VLAN_CURR_CONTEXT_PTR ().                  */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanSelectContext (UINT4 u4ContextId)
{
    if (u4ContextId >= VLAN_SIZING_CONTEXT_COUNT)
    {
        return VLAN_FAILURE;
    }
    VLAN_CURR_CONTEXT_PTR () = VLAN_CONTEXT_PTR (u4ContextId);

    if (VLAN_CURR_CONTEXT_PTR () == NULL)
    {
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanReleaseContext                               */
/*                                                                           */
/*    Description         : This function makes the switched context to NULL */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : VLAN_CURR_CONTEXT_PTR ().                  */
/*                                                                           */
/*    Global Variables Modified : VLAN_CURR_CONTEXT_PTR ().                  */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                       .                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanReleaseContext (VOID)
{
    VLAN_CURR_CONTEXT_PTR () = NULL;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleCreateContext                          */
/*                                                                           */
/*    Description         : This function creates context, and initalises the*/
/*                          per context information                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanHandleCreateContext (UINT4 u4ContextId)
{
    /* allocate context pointer from memory pool, and store it in the 
       context pointers array */
    VLAN_CURR_CONTEXT_PTR () = (tVlanContextInfo *) (VOID *)
        VLAN_GET_BUF (VLAN_CONTEXT_INFO, sizeof (tVlanContextInfo));
    if (VLAN_CURR_CONTEXT_PTR () == NULL)
    {
        return VLAN_FAILURE;
    }
    VLAN_CONTEXT_PTR (u4ContextId) = VLAN_CURR_CONTEXT_PTR ();

    /*Initialise per context global variables */
    VLAN_LOCAL_PORT_RBTREE () = RBTreeCreateEmbedded (0, VlanGlobalPortTblCmp);
    if (VLAN_LOCAL_PORT_RBTREE () == NULL)
    {
        VLAN_RELEASE_BUF (VLAN_CONTEXT_INFO,
                          (UINT1 *) VLAN_CURR_CONTEXT_PTR ());
        VLAN_CURR_CONTEXT_PTR () = NULL;
        VLAN_CONTEXT_PTR (u4ContextId) = VLAN_CURR_CONTEXT_PTR ();
        return VLAN_FAILURE;
    }

    VLAN_CURR_CONTEXT_PTR ()->VlanStartVlanInd = VLAN_INVALID_VLAN_INDEX;
    VLAN_CURR_CONTEXT_PTR ()->u2VlanStartPortInd = VLAN_INVALID_PORT_INDEX;
    VLAN_CURR_CONTEXT_PTR ()->u2VlanLastPortInd = VLAN_INVALID_PORT_INDEX;
    VLAN_CURR_CONTEXT_PTR ()->u4VlanDbg = 0;
    VLAN_CURR_CONTEXT_PTR ()->u4ContextId = u4ContextId;
    VLAN_CURR_CONTEXT_PTR ()->u4BaseBridgeMode = DOT_1Q_VLAN_MODE;
    VLAN_CURR_CONTEXT_PTR ()->i4BridgeBaseRowStatus = VLAN_ACTIVE;
#ifndef NPAPI_WANTED
    VLAN_CURR_CONTEXT_PTR ()->VlanInfo.bUseShortAgingTime = VLAN_FALSE;
    VLAN_CURR_CONTEXT_PTR ()->VlanInfo.i4RapidAgingNumPorts = 0;
#endif
    VLAN_CURR_CONTEXT_PTR ()->u4AgeOutTime = VLAN_NORMAL_AGEOUT_TIME;
    VLAN_CURR_CONTEXT_PTR ()->u1GlobalMacLearningStatus = VLAN_ENABLED;

    VLAN_MEMSET (VLAN_CURR_CONTEXT_STR (), 0, VLAN_CONTEXT_ALIAS_LEN + 1);
    VlanVcmGetAliasName (VLAN_CURR_CONTEXT_ID (), VLAN_CURR_CONTEXT_STR ());
    STRCAT (VLAN_CURR_CONTEXT_STR (), ":");

    if (VlanL2IwfGetBridgeMode (u4ContextId, &(VLAN_BRIDGE_MODE ())) !=
        L2IWF_SUCCESS)
    {
        RBTreeDelete (VLAN_CURR_CONTEXT_PTR ()->VlanPortList);
        VLAN_CURR_CONTEXT_PTR ()->VlanPortList = NULL;
        VLAN_RELEASE_BUF (VLAN_CONTEXT_INFO,
                          (UINT1 *) VLAN_CURR_CONTEXT_PTR ());
        VLAN_CURR_CONTEXT_PTR () = NULL;
        VLAN_CONTEXT_PTR (u4ContextId) = VLAN_CURR_CONTEXT_PTR ();
        return VLAN_FAILURE;
    }

    if (VLAN_BRIDGE_MODE () == VLAN_INVALID_BRIDGE_MODE)
    {
        return VLAN_SUCCESS;
    }

    if (VlanInit () != VLAN_SUCCESS)
    {
        RBTreeDelete (VLAN_CURR_CONTEXT_PTR ()->VlanPortList);
        VLAN_CURR_CONTEXT_PTR ()->VlanPortList = NULL;
        VLAN_RELEASE_BUF (VLAN_CONTEXT_INFO,
                          (UINT1 *) VLAN_CURR_CONTEXT_PTR ());
        VLAN_CURR_CONTEXT_PTR () = NULL;
        VLAN_CONTEXT_PTR (u4ContextId) = VLAN_CURR_CONTEXT_PTR ();
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleDeleteContext                          */
/*                                                                           */
/*    Description         : This function deletes given context              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanHandleDeleteContext (UINT4 u4ContextId)
{
    INT4                i4RetVal;

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }
    i4RetVal = VlanHwVlanDisable (u4ContextId);
    if (i4RetVal == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }
    if (i4RetVal == VLAN_DISABLED_BUT_NOT_DELETED)
    {
        /* Hw supports Vlan disable But, the configured entries are not
         * deleted, do delete  all static configuration */
        VlanHwDelStaticInfo ();
    }

    VlanDeInit ();

    /* update L2Iwf data base */
    VlanL2IwfDeleteAllVlans (VLAN_CURR_CONTEXT_ID ());
    RBTreeDestroy (VLAN_LOCAL_PORT_RBTREE (),
                   (tRBKeyFreeFn) VlanGlobalPortFreeFunc, 0);
    MEMSET (VLAN_CURR_CONTEXT_PTR (), 0, sizeof (tVlanContextInfo));
    VLAN_RELEASE_BUF (VLAN_CONTEXT_INFO, VLAN_CURR_CONTEXT_PTR ());
    VLAN_CURR_CONTEXT_PTR () = NULL;
    VLAN_CONTEXT_PTR (u4ContextId) = VLAN_CURR_CONTEXT_PTR ();

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleUpdateContextName                      */
/*                                                                           */
/*    Description         : This function updated the context name of the    */
/*                          given context.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanHandleUpdateContextName (UINT4 u4ContextId)
{
    MEMSET (VLAN_CURR_CONTEXT_STR (), 0, VLAN_CONTEXT_ALIAS_LEN + 1);

    if (VlanVcmGetAliasName (u4ContextId,
                             VLAN_CURR_CONTEXT_STR ()) != VCM_SUCCESS)
    {
        return VLAN_FAILURE;
    }
    STRCAT (VLAN_CURR_CONTEXT_STR (), ":");

    return VLAN_SUCCESS;
}

/************************************************************************/
/* Function Name    : VlanGlobalPortFreeFunc ()                         */
/*                                                                      */
/* Description      : Releases the Port Entry to the free pool          */
/*                                                                      */
/* Input(s)         : Pointer to the Port entry to be released.         */
/*                                                                      */
/* Output(s)        : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None.                                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None.                                             */
/*                                                                      */
/* Use of Recursion : None.                                             */
/*                                                                      */
/* Returns          : VOID                                              */
/************************************************************************/
INT4
VlanGlobalPortFreeFunc (tRBElem * pElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    VLAN_RELEASE_BUF (VLAN_PORT_ENTRY, (tVlanPortEntry *) pElem);
    return 1;
}

/*****************************************************************************/
/* Function Name      : VlanSendMacThresholdTrap                             */
/*                                                                           */
/* Description        : This function will send an trap to the administrator */
/*                      when the mac address limit of a vlan exceeds         */
/*                                                                           */
/* Input(s)           : u4ContextId   - Virtual context Id                   */
/*                      u4VlanId      - Vlan Id whose MAC limit exceeded     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanSendMacThresholdTrap (UINT4 u4ContextId, UINT4 u4VlanId)
{
#ifdef  SNMP_3_WANTED
    UNUSED_PARAM (u4ContextId);

    if (VlanVcmGetSystemModeExt (VLANMOD_PROTOCOL_ID) == VCM_SI_MODE)
    {
        return (VlanSISendMacThresholdTrap (u4VlanId));
    }
    else
    {
        return (VlanMISendMacThresholdTrap (u4VlanId));
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VlanId);

    return VLAN_SUCCESS;
#endif /* SNMP_2_WANTED */
}

/*****************************************************************************/
/* Function Name      : VlanSendSwitchMacThresholdTrap                       */
/*                                                                           */
/* Description        : This function will send an trap to the administrator */
/*                      when the mac address limit of a switch exceeds       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanSendSwitchMacThresholdTrap (VOID)
{
#ifdef  SNMP_3_WANTED
    if (VlanVcmGetSystemModeExt (VLANMOD_PROTOCOL_ID) == VCM_SI_MODE)
    {
        return (VlanSISendSwitchMacThresholdTrap ());
    }
    else
    {
        return (VlanMISendSwitchMacThresholdTrap ());
    }
#else
    return VLAN_SUCCESS;
#endif /* SNMP_2_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteWildCardTable                          */
/*                                                                           */
/*    Description         : This function deletes all the entries in the     */
/*                          Wild card table.                                */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : gpVlanContextInfo->pWildCardTable          */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteWildCardTable (VOID)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;

    VLAN_SLL_SCAN (&gpVlanContextInfo->WildCardTable,
                   pWildCardEntry, tVlanWildCardEntry *)
    {

        VLAN_SLL_DEL (&gpVlanContextInfo->WildCardTable,
                      (tTMO_SLL_NODE *) & (pWildCardEntry->NextNode));
        VLAN_RELEASE_BUF (VLAN_WILD_CARD_ENTRY, (UINT1 *) pWildCardEntry);
        pWildCardEntry = NULL;
    }

    VLAN_SLL_INIT (&gpVlanContextInfo->WildCardTable);

    return;
}

/*****************************************************************************/
/* Function Name      : VlanSendSrcRelearnTrap                               */
/*                                                                           */
/* Description        : This function will send an trap to the administrator */
/*                      when there is a source relearning event happend      */
/*                                                                           */
/* Input(s)           : pVlanSrcRelearnTrap   - pointer to structure         */
/*                                              containing the               */
/*                                              information to generate trap */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanSendSrcRelearnTrap (tVlanSrcRelearnTrap * pVlanSrcRelearnTrap)
{
#ifdef  SNMP_3_WANTED
    if (VlanVcmGetSystemModeExt (VLANMOD_PROTOCOL_ID) == VCM_SI_MODE)
    {
        return (VlanSISendSrcRelearnTrap (pVlanSrcRelearnTrap));
    }
    else
    {
        return (VlanMISendSrcRelearnTrap (pVlanSrcRelearnTrap));
    }
#else
    UNUSED_PARAM (pVlanSrcRelearnTrap);
    return VLAN_SUCCESS;
#endif
}

#ifdef  SYSLOG_WANTED
/*****************************************************************************/
/* Function Name      : VlanSendSrcRelearnSyslog                             */
/*                                                                           */
/* Description        : This function will send an  sysLog                   */
/*                      hen there is a source  relearning event happening    */
/*                                                                           */
/* Input(s)           : pVlanSrcRelearnTrap   - pointer to structure         */
/*                                              containing the               */
/*                                              information to generate trap */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanSendSrcRelearnSyslog (tVlanSrcRelearnTrap * pVlanSrcRelearnTrap)
{
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];

    VLAN_MEMSET (au1String, 0, VLAN_CLI_MAX_MAC_STRING_SIZE);

    PrintMacAddress (pVlanSrcRelearnTrap->MacAddr, au1String);

    if ((VLAN_GET_PORT_ENTRY (pVlanSrcRelearnTrap->u4OldPort) != NULL) &&
        (VLAN_GET_PORT_ENTRY (pVlanSrcRelearnTrap->u4NewPort) != NULL))
    {
        /* Sending an event to syslog */

        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VlanSysLogId,
                      "VLAN: Source relearning has Occurred for Mac Address %s"
                      "from the port %d to the port :%d ", au1String,
                      (VLAN_GET_IFINDEX (pVlanSrcRelearnTrap->u4OldPort)),
                      (VLAN_GET_IFINDEX (pVlanSrcRelearnTrap->u4NewPort))));
    }
}
#endif

/******************************************************************************
 * Function : VlanParseSubIdNew
 * Input    : ppu1TempPtr
 * Output   : value of ppu1TempPtr
 * Returns  : RST_SUCCESS or RST_FAILURE
 *******************************************************************************/
INT4
VlanParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp;
    INT4                i4RetVal = VLAN_SUCCESS;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = VLAN_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : VlanMakeObjIdFromDotNew                              */
/*                                                                           */
/* Description        : This function will return the OID from name of the   */
/*                      object                                               */
/*                                                                           */
/* Input(s)           : pi1TextStr - Object Name                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pOidPtr  or NULL                                     */
/*****************************************************************************/
tSNMP_OID_TYPE     *
VlanMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
#ifdef SNMP_3_WANTED
    /* TRAP_ADDITON */
    INT1               *pi1TempPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = VLAN_INIT_VAL;
    UINT2               u2DotCount = VLAN_INIT_VAL;
    UINT1              *pu1TempPtr = NULL;
    UINT2               u2Len = 0;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0; ((pi1TempPtr < pi1DotPtr) && (u2Index < 256));
             u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0;
             ((u2Index <
               (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
              && (orig_mib_oid_table[u2Index].pName != NULL)); u2Index++)
        {
            if ((STRCMP
                 (orig_mib_oid_table[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer,
                         orig_mib_oid_table[u2Index].pNumber,
                         VLAN_MIN (sizeof (ai1TempBuffer),
                                   STRLEN (orig_mib_oid_table[u2Index].
                                           pNumber)));
                ai1TempBuffer[sizeof (ai1TempBuffer) - 1] = '\0';
                break;
            }
        }

        if (u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            if (orig_mib_oid_table[u2Index].pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        u2Len =
            (UINT2) ((sizeof (ai1TempBuffer) - STRLEN (ai1TempBuffer) - 1) <
                     STRLEN ((INT1 *) pi1DotPtr) ?
                     (sizeof (ai1TempBuffer) - STRLEN (ai1TempBuffer) -
                      1) : STRLEN ((INT1 *) pi1DotPtr));

        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr, u2Len);
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0; ai1TempBuffer[u2Index] != '\0'; u2Index++)
    {
        if (u2Index >= 256)
        {
            break;
        }
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    /* The object specified may be either Tabular or Scalar
     * Object. Tabular Objects needs index to be added at the
     * end. So, allocating for Maximum OID Length
     * */
    if ((pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        return (NULL);
    }
    pOidPtr->u4_Length = (UINT4) u2DotCount + 1;

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (u2Index >= 256)
        {
            break;
        }
        if (VlanParseSubIdNew
            ((&(pu1TempPtr)), &(pOidPtr->pu4_OidList[u2Index])) == RST_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */
    return (pOidPtr);
#else
    UNUSED_PARAM (pi1TextStr);
    return (pOidPtr);
#endif
}

#ifdef SNMP_3_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanRegisterMIB                                  */
/*                                                                           */
/*    Description         : This function registers the corresponding MIB's. */
/*                          In case of SI mode, register SI mibs. Register   */
/*                          MI mibs in case of SI as well as in MI mode.     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified :                                            */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
VlanRegisterMIB (VOID)
{
    RegisterFSMSBR ();
    RegisterFSMSVL ();
    RegisterFSMPVL ();
    RegisterFSMSBE ();
    RegisterFSMPB ();
    RegisterFSM1AD ();
    RegisterFSMVLE ();
    RegisterSTDDOT ();
    RegisterSTD1D1 ();
    RegisterSTD1Q1 ();
    VlanL2IwfRegisterSTDBRI ();

    /* Registering SI mibs in case of SI alone. We can't register SI mibs 
     * in MI since Port/Portlist conversion is required before 
     * calling SI nmh*/
    if (VlanVcmGetSystemModeExt (VLANMOD_PROTOCOL_ID) == VCM_SI_MODE)
    {
        RegisterFSVLAN ();
        RegisterSTDVLA ();
        RegisterSTDBRG ();
        RegisterFSPB ();
        RegisterFSDOT1 ();
        RegisterFSVLNE ();
    }
}

/*****************************************************************************/
/* Function Name      : VlanSISendMacThresholdTrap                           */
/*                                                                           */
/* Description        : This function will send an trap to the administrator */
/*                      when the switch runs on SI mode.                     */
/*                                                                           */
/* Input(s)           : u4VlanId      - Vlan Id whose MAC limit exceeded     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanSISendMacThresholdTrap (UINT4 u4VlanId)
{
    UINT4               u4MacLimit;
    tVlanCurrEntry     *pVlanEntry;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid;
    UINT1               au1Buf[VLAN_OBJ_NAME_LEN];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    INT1                VLAN_TRAPS_OID[SNMP_MAX_OID_LENGTH];

    MEMSET (VLAN_TRAPS_OID, '\0', SNMP_MAX_OID_LENGTH);

    pVlanEntry = VLAN_GET_CURR_ENTRY (u4VlanId);

    if (pVlanEntry == NULL)
    {
        return VLAN_FAILURE;
    }
    u4MacLimit = VLAN_CONTROL_MAC_LEARNING_LIMIT (u4VlanId);

    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = 1;

    /* In place of 2076, passing commonly used macro whose change will 
     * effected every where. */
    SPRINTF ((CHR1 *) VLAN_TRAPS_OID, "1.3.6.1.4.1.%s.65.3.0",
             EoidGetEnterpriseOid ());
    pEnterpriseOid = SNMP_AGT_GetOidFromString (VLAN_TRAPS_OID);
    if (pEnterpriseOid == NULL)
    {
        return VLAN_FAILURE;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    SPRINTF ((char *) au1Buf, "dot1qFutureVlanIndex");
    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) u4VlanId, NULL, NULL, SnmpCnt64Type);

    if (pVbList == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pStartVb = pVbList;

    SPRINTF ((char *) au1Buf, "dot1qFutureVlanUnicastMacLimit");
    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) u4MacLimit, NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb, NULL);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanMISendMacThresholdTrap                           */
/*                                                                           */
/* Description        : This function will send an trap to the administrator */
/*                      when the switch runs on MI mode.                     */
/*                                                                           */
/* Input(s)           : u4VlanId      - Vlan Id whose MAC limit exceeded     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanMISendMacThresholdTrap (UINT4 u4VlanId)
{
    UINT4               u4MacLimit;
    tVlanCurrEntry     *pVlanEntry;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid;
    UINT1               au1Buf[VLAN_OBJ_NAME_LEN];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    INT1                VLAN_TRAPS_OID[SNMP_MAX_OID_LENGTH];
    tSNMP_OCTET_STRING_TYPE *pOstring, *pContextName = NULL;
    UINT1              *pu1ContextName;
    UINT1               au1TempContextName[VLAN_SWITCH_ALIAS_LEN];

    MEMSET (VLAN_TRAPS_OID, '\0', SNMP_MAX_OID_LENGTH);
    /* In place of 2076, passing commonly used macro whose change will 
     * effected every where. */
    SPRINTF ((CHR1 *) VLAN_TRAPS_OID, "1.3.6.1.4.1.%s.120.3.0",
             EoidGetEnterpriseOid ());
    pVlanEntry = VLAN_GET_CURR_ENTRY (u4VlanId);

    if (pVlanEntry == NULL)
    {
        return VLAN_FAILURE;
    }
    u4MacLimit = VLAN_CONTROL_MAC_LEARNING_LIMIT (u4VlanId);

    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = 1;

    pEnterpriseOid = SNMP_AGT_GetOidFromString (VLAN_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        return VLAN_FAILURE;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    SPRINTF ((char *) au1Buf, "fsMIDot1qFutureVlanContextName");

    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    if (VlanVcmGetAliasName (VLAN_CURR_CONTEXT_ID (),
                             au1TempContextName) == VCM_FAILURE)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }
    pu1ContextName = au1TempContextName;

    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) pu1ContextName,
                                  (INT4) (VLAN_SWITCH_ALIAS_LEN - 1));
    pContextName = pOstring;
    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                              0, pOstring, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pStartVb = pVbList;

    SPRINTF ((char *) au1Buf, "fsMIDot1qFutureVlanIndex");
    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) u4VlanId, NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((char *) au1Buf, "fsMIDot1qFutureVlanUnicastMacLimit");
    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) u4MacLimit, NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }
    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb, pContextName);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanSISendSrcRelearnTrap                             */
/* Description        : This function will send an trap to the administrator */
/*                      when there is a source relearning event happend      */
/*                                                                           */
/* Input(s)           : pVlanSrcRelearnTrap   - pointer to structure         */
/*                                              containing the               */
/*                                              information to generate trap */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanSISendSrcRelearnTrap (tVlanSrcRelearnTrap * pVlanSrcRelearnTrap)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT4               u4GenTrapType = VLAN_INIT_VAL;
    UINT4               u4SpecTrapType = VLAN_INIT_VAL;
    UINT4               u4MacLen = VLAN_INIT_VAL;
    UINT2               u2Count = 0;
    UINT1               au1Buf[VLAN_OBJ_NAME_LEN];
    INT1                VLAN_TRAPS_OID[SNMP_MAX_OID_LENGTH];

    MEMSET (VLAN_TRAPS_OID, '\0', SNMP_MAX_OID_LENGTH);
    /* In place of 2076, passing commonly used macro whose change will 
     * effected every where. */
    SPRINTF ((CHR1 *) VLAN_TRAPS_OID, "1.3.6.1.4.1.%s.65.3.0",
             EoidGetEnterpriseOid ());

    if (pVlanSrcRelearnTrap == NULL)
    {
        return VLAN_FAILURE;
    }

    if ((VLAN_GET_PORT_ENTRY (pVlanSrcRelearnTrap->u4OldPort) == NULL) ||
        (VLAN_GET_PORT_ENTRY (pVlanSrcRelearnTrap->u4NewPort) == NULL))
    {
        return VLAN_FAILURE;
    }

    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = VLAN_SRC_RELEARN_TRAP_TYPE;

    pEnterpriseOid = SNMP_AGT_GetOidFromString (VLAN_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        return VLAN_FAILURE;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    /* Object :: dot1qFutureVlanFid.VlanIndex
     * */
    VLAN_MEMSET (au1Buf, VLAN_INIT_VAL, VLAN_OBJ_NAME_LEN);
    MEMCPY (au1Buf, "dot1qFutureVlanFid", sizeof ("dot1qFutureVlanFid"));

    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }
    /* Tabular object.
     * Append the index here
     * */
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length] = pVlanSrcRelearnTrap->u2VlanId;
        pOid->u4_Length++;
    }

    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) (pVlanSrcRelearnTrap->u4FidIndex), NULL,
                              NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }
    pStartVb = pVbList;

    /* Object :: dot1qTpOldFdbPort.dot1qFdbId.dot1qTpFdbAddress
     * */
    VLAN_MEMSET (au1Buf, VLAN_INIT_VAL, VLAN_OBJ_NAME_LEN);
    MEMCPY (au1Buf, "dot1qTpOldFdbPort", sizeof ("dot1qTpOldFdbPort"));

    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }
    /* Append the indices
     * Going to append 1) FidIndex &
     *                 2) MacAddr
     * Check whether there is enough space left for the objects to be appended
     * i.e (VLAN_MAC_ADDR_LEN + 1)
     * */
    u4MacLen = VLAN_MAC_ADDR_LEN;
    if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - (u4MacLen + 1)))
    {
        pOid->pu4_OidList[pOid->u4_Length] = pVlanSrcRelearnTrap->u4FidIndex;
        pOid->u4_Length++;

        for (u2Count = 0; u2Count <= (u4MacLen - 1); u2Count++)
        {
            pOid->pu4_OidList[(pOid->u4_Length)]
                = pVlanSrcRelearnTrap->MacAddr[u2Count];

            pOid->u4_Length++;
        }
        /* Reinitialise the value of u4MacLen as it will be used again */
        u4MacLen = VLAN_INIT_VAL;
    }

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) (pVlanSrcRelearnTrap->u4OldPort), NULL,
                              NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    /* Object :: dot1qTpFdbPort.dot1qFdbId.dot1qTpFdbAddress
     * */
    VLAN_MEMSET (au1Buf, VLAN_INIT_VAL, VLAN_OBJ_NAME_LEN);
    MEMCPY (au1Buf, "dot1qTpFdbPort", sizeof ("dot1qTpFdbPort"));
    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    /* Append the indices
     * Going to append 1) FidIndex &
     *                 2) MacAddr
     * Check whether there is enough space left for the objects to be appended
     * i.e (VLAN_MAC_ADDR_LEN + 1)
     * */
    u4MacLen = VLAN_MAC_ADDR_LEN;
    if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - (u4MacLen + 1)))
    {
        pOid->pu4_OidList[pOid->u4_Length] = pVlanSrcRelearnTrap->u4FidIndex;
        pOid->u4_Length++;

        for (u2Count = 0; u2Count <= (u4MacLen - 1); u2Count++)
        {
            pOid->pu4_OidList[(pOid->u4_Length)]
                = pVlanSrcRelearnTrap->MacAddr[u2Count];
            pOid->u4_Length++;
        }
    }

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) (pVlanSrcRelearnTrap->u4NewPort), NULL,
                              NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb, NULL);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanMISendSrcRelearnTrap                             */
/*                                                                           */
/* Description        : This function will send an trap to the administrator */
/*                      when there is a source relearning event happend      */
/*                                                                           */
/* Input(s)           : pVlanSrcRelearnTrap   - pointer to structure         */
/*                                              containing the               */
/*                                              information to generate trap */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanMISendSrcRelearnTrap (tVlanSrcRelearnTrap * pVlanSrcRelearnTrap)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    UINT1              *pu1ContextName;
    UINT1               au1TempContextName[VLAN_SWITCH_ALIAS_LEN];
    UINT1               au1Buf[VLAN_OBJ_NAME_LEN];
    INT1                VLAN_TRAPS_OID[SNMP_MAX_OID_LENGTH];
    UINT2               u2Count = 0;
    UINT4               u4GenTrapType = VLAN_INIT_VAL;
    UINT4               u4SpecTrapType = VLAN_INIT_VAL;
    UINT4               u4MacLen = VLAN_INIT_VAL;

    MEMSET (VLAN_TRAPS_OID, '\0', SNMP_MAX_OID_LENGTH);
    /* In place of 2076, passing commonly used macro whose change will 
     * effected every where. */
    SPRINTF ((CHR1 *) VLAN_TRAPS_OID, "1.3.6.1.4.1.%s.120.3.0",
             EoidGetEnterpriseOid ());
    if (pVlanSrcRelearnTrap == NULL)
    {
        return VLAN_FAILURE;
    }

    if ((VLAN_GET_PORT_ENTRY (pVlanSrcRelearnTrap->u4OldPort) == NULL) ||
        (VLAN_GET_PORT_ENTRY (pVlanSrcRelearnTrap->u4NewPort) == NULL))
    {
        return VLAN_FAILURE;
    }

    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = VLAN_SRC_RELEARN_TRAP_TYPE;

    pEnterpriseOid = SNMP_AGT_GetOidFromString (VLAN_TRAPS_OID);
    if (pEnterpriseOid == NULL)
    {
        return VLAN_FAILURE;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    VLAN_MEMSET (au1Buf, VLAN_INIT_VAL, VLAN_OBJ_NAME_LEN);

    MEMCPY (au1Buf, "fsMIDot1qFutureVlanContextName",
            sizeof ("fsMIDot1qFutureVlanContextName"));

    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }
    /* Tabular object.
     * Append the index here
     * fsMIDot1qFutureVlanContextName.fsMIDot1qFutureVlanContextId
     * */
    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length] = VLAN_CURR_CONTEXT_ID ();
        pOid->u4_Length++;
    }

    if (VcmGetAliasName (VLAN_CURR_CONTEXT_ID (), au1TempContextName) ==
        VCM_FAILURE)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }
    pu1ContextName = au1TempContextName;

    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) pu1ContextName,
                                  (INT4) (VLAN_SWITCH_ALIAS_LEN - 1));
    pContextName = pOstring;

    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                              0, pOstring, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }
    pStartVb = pVbList;

    VLAN_MEMSET (au1Buf, VLAN_INIT_VAL, VLAN_OBJ_NAME_LEN);
    MEMCPY (au1Buf, "fsMIDot1qFutureVlanFid",
            sizeof ("fsMIDot1qFutureVlanFid"));

    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    /* Append the indices
     * fsMIDot1qFutureVlanFid.fsMIDot1qFutureVlanContextId.fsMIDot1qFutureVlanIndex
     * */

    u4MacLen = STRLEN (pVlanSrcRelearnTrap->MacAddr);
    if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - (u4MacLen + 1)))
    {
        pOid->pu4_OidList[pOid->u4_Length] = VLAN_CURR_CONTEXT_ID ();
        pOid->u4_Length++;
        pOid->pu4_OidList[pOid->u4_Length] = pVlanSrcRelearnTrap->u2VlanId;
        pOid->u4_Length++;

    }

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) (pVlanSrcRelearnTrap->u4FidIndex), NULL,
                              NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    VLAN_MEMSET (au1Buf, VLAN_INIT_VAL, VLAN_OBJ_NAME_LEN);
    MEMCPY (au1Buf, "fsDot1qTpFdbPort", sizeof ("fsDot1qTpFdbPort"));
    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    /* Append the indices
     * Going to append 1) u4ContextId
     *                 2) FidIndex &
     *                 3) MacAddr
     * Check whether there is enough space left for the objects to be appended
     * i.e (VLAN_MAC_ADDR_LEN + 1 + 1)
     * */

    u4MacLen = VLAN_MAC_ADDR_LEN;
    if (pOid->u4_Length <= (SNMP_MAX_OID_LENGTH - (u4MacLen + 2)))
    {
        pOid->pu4_OidList[pOid->u4_Length] = VLAN_CURR_CONTEXT_ID ();
        pOid->u4_Length++;

        pOid->pu4_OidList[pOid->u4_Length] = pVlanSrcRelearnTrap->u4FidIndex;
        pOid->u4_Length++;

        for (u2Count = 0; u2Count <= (u4MacLen - 1); u2Count++)
        {
            pOid->pu4_OidList[(pOid->u4_Length)]
                = pVlanSrcRelearnTrap->MacAddr[u2Count];

            pOid->u4_Length++;
        }
    }
    /* Reinitialise the value of u4MacLen as it will be used again */
    u4MacLen = VLAN_INIT_VAL;

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) (VLAN_GET_IFINDEX
                                      (pVlanSrcRelearnTrap->u4NewPort)), NULL,
                              NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    VLAN_MEMSET (au1Buf, VLAN_INIT_VAL, VLAN_OBJ_NAME_LEN);

    MEMCPY (au1Buf, "fsMIDot1qFutureVlanOldTpFdbPort",
            sizeof ("fsMIDot1qFutureVlanOldTpFdbPort"));

    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    /* Append the indices
     * Going to append 1) u4ContextId
     *                 2) FidIndex &
     *                 3) MacAddr
     * Check whether there is enough space left for the objects to be appended
     * i.e (VLAN_MAC_ADDR_LEN + 1 + 1)
     * */

    u4MacLen = VLAN_MAC_ADDR_LEN;
    if (pOid->u4_Length <= (SNMP_MAX_OID_LENGTH - (u4MacLen + 2)))
    {
        pOid->pu4_OidList[pOid->u4_Length] = VLAN_CURR_CONTEXT_ID ();
        pOid->u4_Length++;

        pOid->pu4_OidList[pOid->u4_Length] = pVlanSrcRelearnTrap->u4FidIndex;
        pOid->u4_Length++;

        for (u2Count = 0; u2Count <= (u4MacLen - 1); u2Count++)
        {
            pOid->pu4_OidList[(pOid->u4_Length)]
                = pVlanSrcRelearnTrap->MacAddr[u2Count];

            pOid->u4_Length++;
        }
    }
    /* Reinitialise the value of u4MacLen as it will be used again */
    u4MacLen = VLAN_INIT_VAL;

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) (VLAN_GET_IFINDEX
                                      (pVlanSrcRelearnTrap->u4OldPort)), NULL,
                              NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb, pContextName);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanSISendSwitchMacThresholdTrap                     */
/*                                                                           */
/* Description        : This function will send an trap to the administrator */
/*                      when the switch runs on SI mode.                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanSISendSwitchMacThresholdTrap (VOID)
{
    UINT4               u4SwitchMacLimit;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid;
    UINT1               au1Buf[VLAN_OBJ_NAME_LEN];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    INT1                VLAN_TRAPS_OID[SNMP_MAX_OID_LENGTH];

    u4SwitchMacLimit = VLAN_DYNAMIC_UNICAST_SIZE;

    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = VLAN_SWITCH_MAC_LIMIT_TRAP_TYPE;

    MEMSET (VLAN_TRAPS_OID, '\0', SNMP_MAX_OID_LENGTH);
    /* In place of 2076, passing commonly used macro whose change will 
     * effected every where. */
    SPRINTF ((CHR1 *) VLAN_TRAPS_OID, "1.3.6.1.4.1.%s.65.3.0",
             EoidGetEnterpriseOid ());

    pEnterpriseOid = SNMP_AGT_GetOidFromString (VLAN_TRAPS_OID);
    if (pEnterpriseOid == NULL)
    {
        return VLAN_FAILURE;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    SPRINTF ((char *) au1Buf, "dot1qFutureUnicastMacLearningLimit");
    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) u4SwitchMacLimit,
                              NULL, NULL, SnmpCnt64Type);

    if (pVbList == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pStartVb = pVbList;

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb, NULL);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanMISendMacThresholdTrap                           */
/*                                                                           */
/* Description        : This function will send an trap to the administrator */
/*                      when the switch runs on MI mode.                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanMISendSwitchMacThresholdTrap (VOID)
{
    UINT4               u4SwitchMacLimit;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid;
    UINT1               au1Buf[VLAN_OBJ_NAME_LEN];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    INT1                VLAN_TRAPS_OID[SNMP_MAX_OID_LENGTH];
    tSNMP_OCTET_STRING_TYPE *pOstring, *pContextName = NULL;
    UINT1              *pu1ContextName;
    UINT1               au1TempContextName[VLAN_SWITCH_ALIAS_LEN];

    u4SwitchMacLimit = VLAN_DYNAMIC_UNICAST_SIZE;

    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = VLAN_SWITCH_MAC_LIMIT_TRAP_TYPE;

    MEMSET (VLAN_TRAPS_OID, '\0', SNMP_MAX_OID_LENGTH);
    /* In place of 2076, passing commonly used macro whose change will 
     * effected every where. */
    SPRINTF ((CHR1 *) VLAN_TRAPS_OID, "1.3.6.1.4.1.%s.120.3.0",
             EoidGetEnterpriseOid ());
    pEnterpriseOid = SNMP_AGT_GetOidFromString (VLAN_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        return VLAN_FAILURE;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    SPRINTF ((char *) au1Buf, "fsMIDot1qFutureVlanContextName");

    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    /* Appending the Index for the tabular object 
     * Object: fsMIDot1qFutureVlanContextName 
     * Index : fsMIDot1qFutureVlanContextId */

    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length] = VLAN_CURR_CONTEXT_ID ();
        pOid->u4_Length++;
    }

    if (VlanVcmGetAliasName (VLAN_CURR_CONTEXT_ID (),
                             au1TempContextName) == VCM_FAILURE)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }
    pu1ContextName = au1TempContextName;

    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) pu1ContextName,
                                  (INT4) (VLAN_SWITCH_ALIAS_LEN - 1));
    pContextName = pOstring;
    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                              0, pOstring, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    pStartVb = pVbList;

    SPRINTF ((char *) au1Buf, "fsMIDot1qFutureUnicastMacLearningLimit");
    pOid = VlanMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }

    /* Appending the Index for the tabular object 
     * Object: fsMIDot1qFutureUnicastMacLearningLimit 
     * Index : fsMIDot1qFutureVlanContextId */

    if (pOid->u4_Length < SNMP_MAX_OID_LENGTH)
    {
        pOid->pu4_OidList[pOid->u4_Length] = VLAN_CURR_CONTEXT_ID ();
        pOid->u4_Length++;
    }

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) u4SwitchMacLimit,
                              NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return VLAN_FAILURE;
    }
    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_Context_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb, pContextName);

    return VLAN_SUCCESS;
}
#endif /* SNMP_2_WANTED */

#ifdef KERNEL_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFdbRemoveEntryFromKernel                     */
/*                                                                           */
/*    Description         : This function is called to delete an FDB entry   */
/*                          from Kernel RBTREE                               */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan ID of the entry                    */
/*                          MacAddress - MAC address of the entry            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : NONE                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanFdbRemoveEntryFromKernel (tVlanId VlanId, tMacAddr MacAddress)
{
    tVlanFdbEntryParams VlanFdbEntryParams;

    VlanFdbEntryParams.u4Action = VLAN_KERN_REMOVE;
    VlanFdbEntryParams.VlanId = VlanId;
    MEMCPY (VlanFdbEntryParams.MacAddress, MacAddress, CFA_ENET_ADDR_LEN);
    KAPIUpdateInfo (VLAN_FDB_IOCTL, (tKernCmdParam *) & VlanFdbEntryParams);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanRemoveFdbEntryFromKernel                     */
/*                                                                           */
/*    Description         : This function is called to delete all FDB entry  */
/*                          from Kernel RBTREE                               */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : NONE                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRemoveFdbEntryFromKernel ()
{
    tVlanFdbParams      VlanFdbParams;

    VlanFdbParams.u4Action = VLAN_KERN_FDB_REMOVE;
    KAPIUpdateInfo (VLAN_FDB_REMOVE_IOCTL, (tKernCmdParam *) & VlanFdbParams);
}
#endif /*KERNEL_WANTED */

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFdbRemoveEntryFromHashTable                  */
/*                                                                           */
/*    Description         : This function is called to delete an FDB entry   */
/*                          from Hash table of FDB Entries                   */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan ID of the entry                    */
/*                          MacAddress - MAC address of the entry            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : NONE                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanFdbRemoveEntryFromHashTable (tVlanId VlanId, tMacAddr MacAddr)
{

    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanFidEntry      *pFidEntry = NULL;
    tVlanFdbEntry      *pFdbEntry = NULL;
    UINT4               u4HashIndex;
    INT4                i4Result;

    pCurrEntry = VlanGetVlanEntry (VlanId);

    if (pCurrEntry == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       VLAN_NAME, "No Vlan entry for vlan ID %d\r\n", VlanId);
        return;
    }
    if ((pFidEntry = VLAN_GET_FID_ENTRY (pCurrEntry->u4FidIndex)) == NULL)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       VLAN_NAME, "No FID entry for FID %d\r\n",
                       pCurrEntry->u4FidIndex);
        return;
    }

    for (u4HashIndex = 0; u4HashIndex < VLAN_MAX_BUCKETS; u4HashIndex++)
    {
        pFdbEntry = (pFidEntry->UcastHashTbl)[u4HashIndex];

        if ((pFdbEntry != NULL) && (pFdbEntry->u1Status == VLAN_FDB_LEARNT))
        {
            i4Result = VlanCmpMacAddr (MacAddr, pFdbEntry->MacAddr);
            if (i4Result == VLAN_EQUAL)
            {
                VlanDeleteFdbEntry (pCurrEntry->u4FidIndex, pFdbEntry);
                return;
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : VlanHandleStartModule                                */
/*                                                                           */
/* Description        : This function will start VLAN module                 */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanHandleStartModule (VOID)
{
    INT4                i4RetVal = 0;
    /* The structure tVlanNpPortArray and tVlanFdbInfo  is declared here locally
     * to ensure that the sizing parameter structure is visible in debugger.
     * This will be removed in the next subsequent changes in the sizing parameters.*/
    tVlanNpPortArray   *pVlanNpPortArray = NULL;
    tVlanFdbInfo       *pVlanFdbInfoPtr = NULL;
    UNUSED_PARAM (pVlanNpPortArray);
    UNUSED_PARAM (pVlanFdbInfoPtr);

    if (VLAN_CREATE_TMR_LIST (VLAN_TASK, VLAN_TIMER_EXP_EVENT,
                              NULL, &(gVlanTaskInfo.VlanTmrListId))
        != TMR_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, INIT_SHUT_TRC,
                         "VLAN Timer creation Failed \n");
        return VLAN_FAILURE;
    }

#ifdef SW_LEARNING
    if (VLAN_CREATE_SEM (VLAN_SHADOW_TBL_SEM_NAME, 1, 0,
                         &(VLAN_SHADOW_TBL_SEM_ID)) != OSIX_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC,
                         (OS_RESOURCE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC),
                         "MSG: Failed to Create VLAN shadow Table Mutual Exclusion "
                         "Sema4 \r\n");
        return VLAN_FAILURE;
    }
#endif

    if (VlanInitGlobalInfo () == VLAN_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, INIT_SHUT_TRC,
                         "Global information initialization failed \n");
        return VLAN_FAILURE;
    }
    gu1IsVlanInitialised = VLAN_TRUE;

    VLAN_NODE_STATUS () = VlanRedGetNodeStatus ();

    i4RetVal = VlanRedInitGlobalInfo ();

    if (i4RetVal != VLAN_SUCCESS)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, INIT_SHUT_TRC,
                         "RED Global information initialization failed \n");
        return VLAN_FAILURE;
    }
    i4RetVal = VlanRedInitRedundancyInfo ();

    i4RetVal = VlanRedRegisterWithRM ();

#ifdef ICCH_WANTED
    /* Register vlan with ICCH module */
    i4RetVal = VlanIcchRegisterWithICCH ();
#endif

    if (i4RetVal == VLAN_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, INIT_SHUT_TRC,
                         "Registration with RM failed \n");
        return VLAN_FAILURE;
    }

    if (VlanSelectContext (VLAN_DEF_CONTEXT_ID) == VLAN_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, INIT_SHUT_TRC,
                         "Default context selection failed \n");
        return VLAN_FAILURE;
    }

    if (VlanInit () == VLAN_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, INIT_SHUT_TRC,
                         "VLAN initialization failed \n");
    }

    /*L2VPN initialization is not specific to context.
     * Even though, the failure of L2VPN initialization should not
     * initialise ISS.exe */
    if (VlanL2VpnInit () == VLAN_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, INIT_SHUT_TRC,
                         "L2VPN initialization failed \n");
    }
    if (VlanEvbStart (VLAN_DEF_CONTEXT_ID) == VLAN_FAILURE)
    {
        VLAN_GLOBAL_TRC (VLAN_INVALID_CONTEXT, VLAN_MOD_TRC, INIT_SHUT_TRC,
                         "EVB Initialization for default context failed \n");
    }
    /* release context info */
    VlanReleaseContext ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanAssignMempoolIds                                 */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's                                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
VlanAssignMempoolIds (VOID)
{
    /*tVlanQMsg */
    gVlanTaskInfo.VlanPoolIds.VlanQMsgPoolId =
        VLANMemPoolIds[MAX_VLAN_CONFIG_Q_MESG_SIZING_ID];
    /*tVlanQInPkt */
    gVlanTaskInfo.VlanPoolIds.VlanPktQPoolId =
        VLANMemPoolIds[MAX_VLAN_PKTS_IN_QUE_SIZING_ID];
    /*tVlanPvlanEntry */
    gVlanTaskInfo.VlanPoolIds.VlanPvlanPoolId =
        VLANMemPoolIds[MAX_VLAN_PVLAN_ENTRIES_SIZING_ID];
    /*tVlanContextInfo */
    gVlanTaskInfo.VlanPoolIds.VlanContextPoolId =
        VLANMemPoolIds[MAX_VLAN_CONTEXT_INFO_SIZING_ID];
    /*tVlanNpPortArray */
    gVlanTaskInfo.VlanPoolIds.VlanMiscPoolId =
        VLANMemPoolIds[MAX_VLAN_NUM_PORT_ARRARY_SIZING_ID];
    /*tVlanTempPortList */
    gVlanTaskInfo.VlanPoolIds.VlanTmpPortListId =
        VLANMemPoolIds[MAX_VLAN_TEMP_PORT_LIST_COUNT_SIZING_ID];
    /*tVlanFidEntry */
    gVlanTaskInfo.VlanPoolIds.VlanFidPoolId =
        VLANMemPoolIds[MAX_VLAN_FID_ENTRIES_SIZING_ID];
    /*tVlanPortEntry */
    gVlanTaskInfo.VlanPoolIds.VlanPortPoolId =
        VLANMemPoolIds[MAX_VLAN_PORT_ENTRIES_SIZING_ID];
    /*tVlanCurrEntry */
    gVlanTaskInfo.VlanPoolIds.VlanCurrPoolId =
        VLANMemPoolIds[MAX_VLAN_CURR_ENTRIES_SIZING_ID];
    /*tVlanFdbInfo */
    gVlanTaskInfo.VlanPoolIds.VlanFdbPoolId =
        VLANMemPoolIds[MAX_VLAN_FDB_INFO_SIZING_ID];
    /*VlanStUcastEntry */
    gVlanTaskInfo.VlanPoolIds.VlanStUcastPoolId =
        VLANMemPoolIds[MAX_VLAN_ST_UCAST_ENTRIES_SIZING_ID];
    /*tVlanStMcastEntry */
    gVlanTaskInfo.VlanPoolIds.VlanStMcastPoolId =
        VLANMemPoolIds[MAX_VLAN_ST_MCAST_ENTRIES_SIZING_ID];
    /*tVlanGroupEntry */
    gVlanTaskInfo.VlanPoolIds.VlanGroupPoolId =
        VLANMemPoolIds[MAX_VLAN_GROUP_ENTRIES_SIZING_ID];
    /*tVlanMacMapEntry */
    gVlanTaskInfo.VlanPoolIds.VlanMacMapPoolId =
        VLANMemPoolIds[MAX_VLAN_MAC_MAP_ENTRIES_SIZING_ID];
    /*tVlanSubnetMapEntry */
    gVlanTaskInfo.VlanPoolIds.VlanSubnetMapPoolId =
        VLANMemPoolIds[MAX_VLAN_SUBNET_MAP_ENTRIES_SIZING_ID];
    /*tStaticVlanEntry */
    gVlanTaskInfo.VlanPoolIds.VlanStEntriesPoolId =
        VLANMemPoolIds[MAX_VLAN_STATIC_ENTRIES_SIZING_ID];
    /*tVlanPortInfoPerVlan */
    gVlanTaskInfo.VlanPoolIds.VlanPortStatsPoolId =
        VLANMemPoolIds[MAX_VLAN_PER_PORT_PER_VLAN_STATS_SIZING_ID];
    /*tVlanStats */
    gVlanTaskInfo.VlanPoolIds.VlanStatsPoolId =
        VLANMemPoolIds[MAX_VLAN_STATS_ENTRIES_SIZING_ID];
    /*tVlanProtGrpEntry */
    gVlanTaskInfo.VlanPoolIds.VlanProtoGroupPoolId =
        VLANMemPoolIds[MAX_VLAN_PROTO_GROUP_ENTRIES_SIZING_ID];
    /*tVlanPortVidSet */
    gVlanTaskInfo.VlanPoolIds.VlanPortProtoPoolId =
        VLANMemPoolIds[MAX_VLAN_PORT_PROTOCOL_ENTRIES_SIZING_ID];
    /*tVlanMacControl */
    gVlanTaskInfo.VlanPoolIds.VlanMacControlPoolId =
        VLANMemPoolIds[MAX_VLAN_MAC_CONTROL_ENTRIES_SIZING_ID];
    /*tVlanWildCardEntry */
    gVlanTaskInfo.VlanPoolIds.VlanWildCardPoolId =
        VLANMemPoolIds[MAX_VLAN_WILD_CARD_ENTRIES_SIZING_ID];
    /*tVlanL2VpnMap */
    gVlanTaskInfo.VlanPoolIds.VlanL2VpnMapPoolId =
        VLANMemPoolIds[MAX_VLAN_L2VPN_MAP_ENTRIES_SIZING_ID];
    /* tPortVlanMapEntry */
    gVlanTaskInfo.VlanPoolIds.PortVlanMapPoolId =
        VLANMemPoolIds[MAX_VLAN_PORT_VLAN_MAP_ENTRIES_SIZING_ID];
    /*tVlanPbPortEntry */
    gVlanPbPoolInfo.PbPortTblPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_PORT_ENTRIES_SIZING_ID];
    /*tVlanCVlanSVlanEntry */
    gVlanPbPoolInfo.VlanCVlanPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_CVID_ENTRIES_SIZING_ID];
    /*tVlanCVlanSrcMacSVlanEntry */
    gVlanPbPoolInfo.VlanCVlanSrcMacPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_CVLAN_SRCMAC_SVLAN_ENTRIES_SIZING_ID];
    /*tVlanCVlanDstMacSVlanEntry */
    gVlanPbPoolInfo.VlanCVlanDstMacPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_CVLAN_DSTMAC_SVLAN_ENTRIES_SIZING_ID];
    /*tVlanCVlanDscpSVlanEntry */
    gVlanPbPoolInfo.VlanCVlanDscpPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_CVLAN_DSCP_SVLAN_ENTRIES_SIZING_ID];
    /*tVlanCVlanDstIpSVlanEntry */
    gVlanPbPoolInfo.VlanCVlanDstIpPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_CVLAN_DSTIP_SVLAN_ENTRIES_SIZING_ID];
    /*tVlanSrcMacSVlanEntry */
    gVlanPbPoolInfo.VlanSrcMacPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_SRCMAC_SVLAN_ENTRIES_SIZING_ID];
    /*tVlanDstMacSVlanEntry */
    gVlanPbPoolInfo.VlanDstMacPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_DSTMAC_SVLAN_ENTRIES_SIZING_ID];
    /*tVlanDscpSVlanEntry */
    gVlanPbPoolInfo.VlanDscpPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_DSCP_SVLAN_ENTRIES_SIZING_ID];
    /*tVlanSrcIpSVlanEntry */
    gVlanPbPoolInfo.VlanSrcIpPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_SRCIP_SVLAN_ENTRIES_SIZING_ID];
    /*tVlanDstIpSVlanEntry */
    gVlanPbPoolInfo.VlanDstIpPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_DSTIP_SVLAN_ENTRIES_SIZING_ID];
    /*tVlanSrcDstIpSVlanEntry */
    gVlanPbPoolInfo.VlanSrcDstIpPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_SRC_DSTIP_SVLAN_ENTRIES_SIZING_ID];
    /*tEtherTypeSwapEntry */
    gVlanPbPoolInfo.EtherTypeSwapPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_ETHER_TYPE_SWAP_ENTRIES_SIZING_ID];
    /*tVlanPbLogicalPortEntry */
    gVlanPbPoolInfo.LogicalPortTblPoolId =
        VLANMemPoolIds[MAX_VLAN_PB_PEP_ENTRIES_SIZING_ID];

    gVlanTaskInfo.VlanPoolIds.VlanBasePortPoolId =
        VLANMemPoolIds[MAX_VLAN_BASE_PORT_ENTRIES_SIZING_ID];
    /* VOID * VlanHwStatsEntry */
    gVlanTaskInfo.VlanPoolIds.VlanHwStatsMapPoolId =
        VLANMemPoolIds[MAX_VLAN_HW_STATS_ENTRIES_SIZING_ID];
#ifdef L2RED_WANTED
    gVlanTaskInfo.VlanPoolIds.VlanRedCachePoolId =
        VLANREDMemPoolIds[MAX_VLAN_RED_CACHE_ENTRIES_SIZING_ID];
    gVlanTaskInfo.VlanPoolIds.VlanRedStUcastCachePoolId =
        VLANREDMemPoolIds[MAX_VLAN_RED_ST_UCAST_CACHE_ENTRIES_SIZING_ID];
    gVlanTaskInfo.VlanPoolIds.VlanRedStMcastCachePoolId =
        VLANREDMemPoolIds[MAX_VLAN_RED_MCAST_CACHE_ENTRIES_SIZING_ID];
    gVlanTaskInfo.VlanPoolIds.VlanRedProtoVlanCachePoolId =
        VLANREDMemPoolIds[MAX_VLAN_RED_PROTO_CACHE_ENTRIES_SIZING_ID];
#endif /* L2RED_WANTED */
    gVlanTaskInfo.VlanPoolIds.VlanMcagQMsgPoolId =
        VLANMemPoolIds[MAX_VLAN_MCAG_CONFIG_Q_MESG_SIZING_ID];
    gVlanTaskInfo.VlanPoolIds.VlanMcagMacAgingEntryPoolId =
        VLANMemPoolIds[MAX_VLAN_MCAG_MAC_AGING_ENTRIES_SIZING_ID];
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteStats                                  */
/*                                                                           */
/*    Description         : This function traverses all VLAN ID created and  */
/*                          deletes the counter statistics                   */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteStats (VOID)
{
    tVlanId             VlanStartId = 0;
    tVlanId             VlanId = 0;
    tVlanCurrEntry     *pCurrEntry = NULL;
    UINT1               u1RetVal = SNMP_FAILURE;

    VlanStartId = VLAN_START_VLAN_INDEX ();

    VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        if ((pCurrEntry != NULL) && (pCurrEntry->pVlanHwStatsEntry != NULL))
        {
            u1RetVal = VlanHwGetVlanStats ((INT4) VLAN_CURR_CONTEXT_ID (),
                                           (tVlanId) VlanId,
                                           VLAN_DISABLED,
                                           pCurrEntry->pVlanHwStatsEntry);
            if (u1RetVal != VLAN_SUCCESS)
            {
                VLAN_RELEASE_BUF (VLAN_HW_STATS_ENTRY,
                                  (UINT1 *) pCurrEntry->pVlanHwStatsEntry);
                pCurrEntry->i4VlanCounterStatus = VLAN_DISABLED;
            }
            VLAN_RELEASE_BUF (VLAN_HW_STATS_ENTRY,
                              (UINT1 *) pCurrEntry->pVlanHwStatsEntry);
        }
    }
}
