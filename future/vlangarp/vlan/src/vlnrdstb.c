/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : vlnrdstb.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Redundancy                                  */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains VLAN redundancy related       */
/*                          stub routines which will get invoked if          */
/*                          L2RED_WANTED is undefined.                       */
/*****************************************************************************/
#ifndef L2RED_WANTED

#include "vlaninc.h"

/*****************************************************************************/
/* Function Name      : VlanRedInitGlobalInfo                                */
/*                                                                           */
/* Description        : Initializes redundancy global variables.             */
/*                      This function will get invoked during BOOTUP.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                          */
/*****************************************************************************/
INT4
VlanRedInitGlobalInfo (VOID)
{
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedRegisterWithRM                                */
/*                                                                           */
/* Description        : Registers VLAN module with RM to send and receive    */
/*                      update messages.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then VLAN_SUCCESS         */
/*                      Otherwise VLAN_FAILURE                               */
/*****************************************************************************/
INT4
VlanRedRegisterWithRM (VOID)
{
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedGetNodeStatus                                 */
/*                                                                           */
/* Description        : This function returns the node status after getting  */
/*                      it from the RM module.                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : tVlanNodeStatus                                      */
/*****************************************************************************/
tVlanNodeStatus
VlanRedGetNodeStatus (VOID)
{
    /* Node active always */
    return VLAN_NODE_ACTIVE;
}

/*****************************************************************************/
/* Function Name      : VlanRedInitRedundancyInfo                            */
/*                                                                           */
/* Description        : This function initialises the required information   */
/*                      to support redundancy based on the current node      */
/*                      status. This function will be invoked when the VLAN  */
/*                      module is started afresh.                            */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE                          */
/*****************************************************************************/
INT4
VlanRedInitRedundancyInfo (VOID)
{
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedDeInitRedundancyInfo                          */
/*                                                                           */
/* Description        : This function deinitialises all the information      */
/*                      allocated to support redundancy based on the node    */
/*                      status. This function will be invoked when the VLAN  */
/*                      module is shutdown.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedDeInitRedundancyInfo (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : VlanRedSyncUcastAddressDeletion                      */
/*                                                                           */
/* Description        : This function will send unicast address deletion     */
/*                      syncup message on time out to standby node.          */
/*                                                                           */
/* Input(s)           : u4Context   - Virtual ContextId                      */
/*                      u4FidIndex  - Fid Index                              */
/*                      UcastAddr   - Unicast address which has been deleted */
/*                      u4RcvPortId - Port Index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanRedSyncUcastAddressDeletion (UINT4 u4Context, UINT4 u4FidIndex,
                                 tMacAddr UcastAddr, UINT4 u4RcvPortId)
{
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (u4FidIndex);
    UNUSED_PARAM (UcastAddr);
    UNUSED_PARAM (u4RcvPortId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedSyncMcastAddressDeletion                      */
/*                                                                           */
/* Description        : This function will send multicast address deletion   */
/*                      sync up message on time out to standby node.         */
/*                                                                           */
/* Input(s)           : u4Context - Virtual ContextId                        */
/*                      u2VlanId  - Vlan Id.                                 */
/*                      McastAddr - Multicast address which has been deleted */
/*                      u4RcvPortId - Port Index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanRedSyncMcastAddressDeletion (UINT4 u4Context, UINT2 u2VlanId,
                                 tMacAddr McastAddr, UINT4 u4RcvPortId)
{
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (McastAddr);
    UNUSED_PARAM (u4RcvPortId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedSyncPortOperStatus                            */
/*                                                                           */
/* Description        : This function will send port oper status change to   */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Id                     */
/*                      u4Port   - Port number.                              */
/*                      u1Status - Port Oper status.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS / VLAN_FAILURE.                         */
/*****************************************************************************/
INT4
VlanRedSyncPortOperStatus (UINT4 u4ContextId, UINT4 u4Port, UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Status);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanRedSetGroupChangedFlag                           */
/*                                                                           */
/* Description        : This function sets the changed flag in the VLAN      */
/*                      Group entry.                                         */
/*                                                                           */
/* Input(s)           : pVlanEntry - Pointer to the VLAN entry               */
/*                      pGroupEntry - Pointer to the Group entry             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedSetGroupChangedFlag (tVlanCurrEntry * pVlanEntry,
                            tVlanGroupEntry * pGroupEntry)
{
    UNUSED_PARAM (pVlanEntry);
    UNUSED_PARAM (pGroupEntry);

    return;
}

/*****************************************************************************/
/* Function Name      : VlanRedSetVlanChangedFlag                            */
/*                                                                           */
/* Description        : This function sets the changed flag in the VLAN      */
/*                      current entry.                                       */
/*                                                                           */
/* Input(s)           : pVlanEntry - Pointer to the VLAN entry               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedSetVlanChangedFlag (tVlanCurrEntry * pVlanEntry)
{
    UNUSED_PARAM (pVlanEntry);

    return;
}

/*****************************************************************************/
/* Function Name      : VlanRedAddGroupDeletedEntry                          */
/*                                                                           */
/* Description        : This function adds a GROOUP deleted entry into GROUP */
/*                      deleted table. This function is invoked whenever a   */
/*                      GROUP entry is deleted as a result of last member    */
/*                      leaving the multicast group.                         */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which the entry needs to be     */
/*                               added.                                      */
/*                      McastAddr - Multicast address for which the entry    */
/*                               needs to be added.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedAddGroupDeletedEntry (tVlanId VlanId, tMacAddr McastAddr)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (McastAddr);

    return;
}

/*****************************************************************************/
/* Function Name      : VlanRedDelGroupDeletedEntry                          */
/*                                                                           */
/* Description        : This function deletes the GROUP deleted entry from   */
/*                      the GROUP Deleted table.                             */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which the entry needs to be     */
/*                               added.                                      */
/*                      McastAddr - Multicast mac address.                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDelGroupDeletedEntry (tVlanId VlanId, tMacAddr McastAddr)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (McastAddr);

    return;
}

/*****************************************************************************/
/* Function Name      : VlanRedAddVlanDeletedEntry                           */
/*                                                                           */
/* Description        : This function adds a VLAN deleted entry into VLAN    */
/*                      deleted table. This function is invoked whenever a   */
/*                      VLAN entry is deleted as a result of last member     */
/*                      leaving the group.                                   */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which the entry needs to be     */
/*                               added.                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedAddVlanDeletedEntry (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);

    return;
}

/*****************************************************************************/
/* Function Name      : VlanRedDelVlanDeletedEntry                           */
/*                                                                           */
/* Description        : This function deletes the VLAN deleted entry from    */
/*                      the VLAN deleted table.                              */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Id for which the entry needs to be     */
/*                               deleted.                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VlanRedDelVlanDeletedEntry (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);

    return;
}

/******************************************************************************/
/*  Function Name   : VlanRedIsVlanPresentInLrntTable                         */
/*                                                                            */
/*  Description     : This function will be checks whether the given VLAN     */
/*                    is present in GVRP learnt ports table or not.           */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS or VLAN_FAILURE                            */
/******************************************************************************/
INT4
VlanRedIsVlanPresentInLrntTable (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);

    return VLAN_FAILURE;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedIsRelearningInProgress                           */
/*                                                                            */
/*  Description     : This function will be invoked to check if relearning    */
/*                    is completed.                                           */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
INT4
VlanRedIsRelearningInProgress (VOID)
{
    return VLAN_FALSE;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanRedIsMcastRelrningInProgress                        */
/*                                                                            */
/*  Description     : This function will be invoked to check if multicast     */
/*                    relearning is completed.                                */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
INT4
VlanRedIsMcastRelrningInProgress (VOID)
{
    return VLAN_FALSE;
}

/*****************************************************************************/
/* Function Name      : VlanRedSendDelAllDynInfoUpdate                       */
/*                                                                           */
/* Description        : This function sends delete all dynamic information   */
/*                      update message to the standby node. This function    */
/*                      will be invoked whenever GVRP/GMRP is disabled       */
/*                      globally or on a port or if the port becomes         */
/*                      operationally down.                                  */
/*                                                                           */
/* Input(s)           : u1Flag - Indicates whether VLAN or GROUP information.*/
/*                      u4Port - Indicates port number for which information */
/*                               needs to be deleted. Will be 0 for all ports*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VlanRedSendDelAllDynInfoUpdate (UINT1 u1Flag, UINT4 u4Port)
{
    UNUSED_PARAM (u1Flag);
    UNUSED_PARAM (u4Port);

    return;
}

#ifdef NPAPI_WANTED
/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanSetOrSyncBrgModeFlag                                */
/*                                                                            */
/*  Description     : This function will be invoked when the Bridge Mode is   */
/*                    Changed in Active Node and when the corresponding nmh   */
/*                    sync-up is received at the standby node. In Standby node*/
/*                    it resets the flag while the active node sends the      */
/*                    sync-up which would have actually set the same flag.    */
/*                    This Flag is used during H/w Audit                      */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context Identifier                        */
/*                    u1Flag - Flag indicating if Failover happened when      */
/*                             setting the Bridge Mode.                       */
/*                             VLAN_TRUE/VLAN_FALSE                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanSetOrSyncBrgModeFlag (UINT4 u4ContextId, UINT1 u1Flag)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Flag);
    return;
}

#endif
#endif /* !L2RED_WANTED */
