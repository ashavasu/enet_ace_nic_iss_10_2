/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmvlelw.c,v 1.16 2015/06/05 09:40:38 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include "vlaninc.h"
# include "vcm.h"
# include  "fssnmp.h"

/* LOW LEVEL Routines for Table : FsMIVlanBridgeInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIVlanBridgeInfoTable
 Input       :  The Indices
                FsMIVlanContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIVlanBridgeInfoTable (INT4 i4FsMIVlanContextId)
{
    if (VLAN_IS_VC_VALID (i4FsMIVlanContextId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIVlanBridgeInfoTable
 Input       :  The Indices
                FsMIVlanContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIVlanBridgeInfoTable (INT4 *pi4FsMIVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIVlanBridgeInfoTable
 Input       :  The Indices
                FsMIVlanContextId
                nextFsMIVlanContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIVlanBridgeInfoTable (INT4 i4FsMIVlanContextId,
                                        INT4 *pi4NextFsMIVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetNextActiveContext ((UINT4) i4FsMIVlanContextId,
                                  &u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIVlanBridgeMode
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                retValFsMIVlanBridgeMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanBridgeMode (INT4 i4FsMIVlanContextId,
                          INT4 *pi4RetValFsMIVlanBridgeMode)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanBridgeMode (pi4RetValFsMIVlanBridgeMode);

    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIVlanBridgeMode
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                setValFsMIVlanBridgeMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanBridgeMode (INT4 i4FsMIVlanContextId,
                          INT4 i4SetValFsMIVlanBridgeMode)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanBridgeMode (i4SetValFsMIVlanBridgeMode);

    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanBridgeMode
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                testValFsMIVlanBridgeMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanBridgeMode (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanContextId,
                             INT4 i4TestValFsMIVlanBridgeMode)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsVlanBridgeMode (pu4ErrorCode,
                                          i4TestValFsMIVlanBridgeMode);

    VlanReleaseContext ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIVlanBridgeInfoTable
 Input       :  The Indices
                FsMIVlanContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIVlanBridgeInfoTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIVlanTunnelContextInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIVlanTunnelContextInfoTable
 Input       :  The Indices
                FsMIVlanContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIVlanTunnelContextInfoTable (INT4
                                                        i4FsMIVlanContextId)
{
    if (VLAN_IS_VC_VALID (i4FsMIVlanContextId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIVlanTunnelContextInfoTable
 Input       :  The Indices
                FsMIVlanContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIVlanTunnelContextInfoTable (INT4 *pi4FsMIVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIVlanTunnelContextInfoTable
 Input       :  The Indices
                FsMIVlanContextId
                nextFsMIVlanContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIVlanTunnelContextInfoTable (INT4 i4FsMIVlanContextId,
                                               INT4 *pi4NextFsMIVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetNextActiveContext ((UINT4) i4FsMIVlanContextId,
                                  &u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelBpduPri
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                retValFsMIVlanTunnelBpduPri
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelBpduPri (INT4 i4FsMIVlanContextId,
                             INT4 *pi4RetValFsMIVlanTunnelBpduPri)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelBpduPri (pi4RetValFsMIVlanTunnelBpduPri);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelStpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                retValFsMIVlanTunnelStpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelStpAddress (INT4 i4FsMIVlanContextId,
                                tMacAddr * pRetValFsMIVlanTunnelStpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelStpAddress (pRetValFsMIVlanTunnelStpAddress);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelLacpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                retValFsMIVlanTunnelLacpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelLacpAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr * pRetValFsMIVlanTunnelLacpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelLacpAddress (pRetValFsMIVlanTunnelLacpAddress);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelDot1xAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                retValFsMIVlanTunnelDot1xAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelDot1xAddress (INT4 i4FsMIVlanContextId,
                                  tMacAddr * pRetValFsMIVlanTunnelDot1xAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelDot1xAddress (pRetValFsMIVlanTunnelDot1xAddress);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelGvrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                retValFsMIVlanTunnelGvrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelGvrpAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr * pRetValFsMIVlanTunnelGvrpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelGvrpAddress (pRetValFsMIVlanTunnelGvrpAddress);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelMvrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                retValFsMIVlanTunnelMvrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelMvrpAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr * pRetValFsMIVlanTunnelMvrpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetFsVlanTunnelMvrpAddress (pRetValFsMIVlanTunnelMvrpAddress);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelGmrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                retValFsMIVlanTunnelGmrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelGmrpAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr * pRetValFsMIVlanTunnelGmrpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelGmrpAddress (pRetValFsMIVlanTunnelGmrpAddress);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelMmrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                retValFsMIVlanTunnelMmrpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelMmrpAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr * pRetValFsMIVlanTunnelMmrpAddress)
{

    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelMmrpAddress (pRetValFsMIVlanTunnelMmrpAddress);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelElmiAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                retValFsMIVlanTunnelElmiAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelElmiAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr * pRetValFsMIVlanTunnelElmiAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelElmiAddress (pRetValFsMIVlanTunnelElmiAddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelLldpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                retValFsMIVlanTunnelLldpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelLldpAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr * pRetValFsMIVlanTunnelLldpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelLldpAddress (pRetValFsMIVlanTunnelLldpAddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelEcfmAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                retValFsMIVlanTunnelEcfmAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelEcfmAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr * pRetValFsMIVlanTunnelEcfmAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelEcfmAddress (pRetValFsMIVlanTunnelEcfmAddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelEoamAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                retValFsMIVlanTunnelEoamAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelEoamAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr * pRetValFsMIVlanTunnelEoamAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelEoamAddress (pRetValFsMIVlanTunnelEoamAddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelIgmpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                retValFsMIVlanTunnelIgmpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIVlanTunnelIgmpAddress(INT4 i4FsMIVlanContextId , tMacAddr * pRetValFsMIVlanTunnelIgmpAddress)
{

    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelIgmpAddress (pRetValFsMIVlanTunnelIgmpAddress);

    VlanReleaseContext ();
    return i1RetVal;
}


/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelBpduPri
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                setValFsMIVlanTunnelBpduPri
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelBpduPri (INT4 i4FsMIVlanContextId,
                             INT4 i4SetValFsMIVlanTunnelBpduPri)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelBpduPri (i4SetValFsMIVlanTunnelBpduPri);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelStpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                setValFsMIVlanTunnelStpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelStpAddress (INT4 i4FsMIVlanContextId,
                                tMacAddr SetValFsMIVlanTunnelStpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelStpAddress (SetValFsMIVlanTunnelStpAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelLacpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                setValFsMIVlanTunnelLacpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelLacpAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr SetValFsMIVlanTunnelLacpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelLacpAddress (SetValFsMIVlanTunnelLacpAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelDot1xAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                setValFsMIVlanTunnelDot1xAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelDot1xAddress (INT4 i4FsMIVlanContextId,
                                  tMacAddr SetValFsMIVlanTunnelDot1xAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelDot1xAddress (SetValFsMIVlanTunnelDot1xAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelGvrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                setValFsMIVlanTunnelGvrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelGvrpAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr SetValFsMIVlanTunnelGvrpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelGvrpAddress (SetValFsMIVlanTunnelGvrpAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhSetFsMIVlanTunnelMvrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                setValFsMIVlanTunnelMvrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelMvrpAddress (INT4 i4FsMIVlanContextId, tMacAddr
                                 SetValFsMIVlanTunnelMvrpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelMvrpAddress (SetValFsMIVlanTunnelMvrpAddress);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelGmrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                setValFsMIVlanTunnelGmrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelGmrpAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr SetValFsMIVlanTunnelGmrpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelGmrpAddress (SetValFsMIVlanTunnelGmrpAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelMmrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                setValFsMIVlanTunnelMmrpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelMmrpAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr SetValFsMIVlanTunnelMmrpAddress)
{

    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelMmrpAddress (SetValFsMIVlanTunnelMmrpAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelElmiAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                setValFsMIVlanTunnelElmiAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelElmiAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr SetValFsMIVlanTunnelElmiAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelElmiAddress (SetValFsMIVlanTunnelElmiAddress);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelLldpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                setValFsMIVlanTunnelLldpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelLldpAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr SetValFsMIVlanTunnelLldpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelLldpAddress (SetValFsMIVlanTunnelLldpAddress);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelEcfmAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                setValFsMIVlanTunnelEcfmAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelEcfmAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr SetValFsMIVlanTunnelEcfmAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelEcfmAddress (SetValFsMIVlanTunnelEcfmAddress);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelEoamAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                setValFsMIVlanTunnelEoamAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelEoamAddress (INT4 i4FsMIVlanContextId,
                                 tMacAddr SetValFsMIVlanTunnelEoamAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelEoamAddress (SetValFsMIVlanTunnelEoamAddress);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelIgmpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                setValFsMIVlanTunnelIgmpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIVlanTunnelIgmpAddress(INT4 i4FsMIVlanContextId , tMacAddr SetValFsMIVlanTunnelIgmpAddress)
{

    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelIgmpAddress (SetValFsMIVlanTunnelIgmpAddress);

    VlanReleaseContext ();

    return i1RetVal;
}


/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelBpduPri
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                testValFsMIVlanTunnelBpduPri
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelBpduPri (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanContextId,
                                INT4 i4TestValFsMIVlanTunnelBpduPri)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsVlanTunnelBpduPri (pu4ErrorCode,
                                             i4TestValFsMIVlanTunnelBpduPri);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelStpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                testValFsMIVlanTunnelStpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelStpAddress (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIVlanContextId,
                                   tMacAddr TestValFsMIVlanTunnelStpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelStpAddress (pu4ErrorCode,
                                         TestValFsMIVlanTunnelStpAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelLacpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                testValFsMIVlanTunnelLacpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelLacpAddress (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIVlanContextId,
                                    tMacAddr TestValFsMIVlanTunnelLacpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelLacpAddress (pu4ErrorCode,
                                          TestValFsMIVlanTunnelLacpAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelDot1xAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                testValFsMIVlanTunnelDot1xAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelDot1xAddress (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIVlanContextId,
                                     tMacAddr TestValFsMIVlanTunnelDot1xAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelDot1xAddress (pu4ErrorCode,
                                           TestValFsMIVlanTunnelDot1xAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelGvrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                testValFsMIVlanTunnelGvrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelGvrpAddress (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIVlanContextId,
                                    tMacAddr TestValFsMIVlanTunnelGvrpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelGvrpAddress (pu4ErrorCode,
                                          TestValFsMIVlanTunnelGvrpAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhTestv2FsMIVlanTunnelMvrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                testValFsMIVlanTunnelMvrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelMvrpAddress (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIVlanContextId,
                                    tMacAddr TestValFsMIVlanTunnelMvrpAddress)
{

    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelMvrpAddress (pu4ErrorCode,
                                          TestValFsMIVlanTunnelMvrpAddress);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelGmrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                testValFsMIVlanTunnelGmrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelGmrpAddress (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIVlanContextId,
                                    tMacAddr TestValFsMIVlanTunnelGmrpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelGmrpAddress (pu4ErrorCode,
                                          TestValFsMIVlanTunnelGmrpAddress);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelMmrpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                testValFsMIVlanTunnelMmrpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelMmrpAddress (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIVlanContextId,
                                    tMacAddr TestValFsMIVlanTunnelMmrpAddress)
{

    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelMmrpAddress (pu4ErrorCode,
                                          TestValFsMIVlanTunnelMmrpAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelElmiAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                testValFsMIVlanTunnelElmiAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelElmiAddress (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIVlanContextId,
                                    tMacAddr TestValFsMIVlanTunnelElmiAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelElmiAddress (pu4ErrorCode,
                                          TestValFsMIVlanTunnelElmiAddress);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelLldpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object 
                testValFsMIVlanTunnelLldpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelLldpAddress (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIVlanContextId,
                                    tMacAddr TestValFsMIVlanTunnelLldpAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelLldpAddress (pu4ErrorCode,
                                          TestValFsMIVlanTunnelLldpAddress);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelEcfmAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                testValFsMIVlanTunnelEcfmAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned                                                                      SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelEcfmAddress (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIVlanContextId,
                                    tMacAddr TestValFsMIVlanTunnelEcfmAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelEcfmAddress (pu4ErrorCode,
                                          TestValFsMIVlanTunnelEcfmAddress);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelEoamAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                testValFsMIVlanTunnelEoamAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelEoamAddress (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIVlanContextId,
                                    tMacAddr TestValFsMIVlanTunnelEoamAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelEoamAddress (pu4ErrorCode,
                                          TestValFsMIVlanTunnelEoamAddress);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelIgmpAddress
 Input       :  The Indices
                FsMIVlanContextId

                The Object
                testValFsMIVlanTunnelIgmpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIVlanTunnelIgmpAddress(UINT4 *pu4ErrorCode , INT4 i4FsMIVlanContextId , tMacAddr TestValFsMIVlanTunnelIgmpAddress)
{

    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelIgmpAddress (pu4ErrorCode,
                                          TestValFsMIVlanTunnelIgmpAddress);

    VlanReleaseContext ();

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIVlanTunnelContextInfoTable
 Input       :  The Indices
                FsMIVlanContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIVlanTunnelContextInfoTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIVlan */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIVlanTunnelTable
 Input       :  The Indices
                FsMIVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIVlanTunnelTable (INT4 i4FsMIVlanPort)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsVlanTunnelTable (u2LocalPort);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIVlanTunnelTable
 Input       :  The Indices
                FsMIVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIVlanTunnelTable (INT4 *pi4FsMIVlanPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsMIVlanTunnelTable (0, pi4FsMIVlanPort);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIVlanTunnelTable
 Input       :  The Indices
                FsMIVlanPort
                nextFsMIVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIVlanTunnelTable (INT4 i4FsMIVlanPort,
                                    INT4 *pi4NextFsMIVlanPort)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    tVlanPortEntry     *pPortEntry = NULL;

    if (i4FsMIVlanPort < 0)
    {
        i4FsMIVlanPort = 0;
    }

    while (VlanGetNextPortInSystem (i4FsMIVlanPort, &u4IfIndex) == VLAN_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
                {
                    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

                    if (pPortEntry != NULL)
                    {
                        if (VLAN_TUNNEL_STATUS (pPortEntry) == VLAN_ENABLED)
                        {
                            *pi4NextFsMIVlanPort = (INT4) u4IfIndex;
                            VlanReleaseContext ();
                            return SNMP_SUCCESS;
                        }
                    }
                }

                VlanReleaseContext ();
            }
        }

        i4FsMIVlanPort = (INT4) u4IfIndex;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelStatus
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelStatus (INT4 i4FsMIVlanPort,
                            INT4 *pi4RetValFsMIVlanTunnelStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVlanTunnelStatus ((INT4) u2LocalPortId,
                                         pi4RetValFsMIVlanTunnelStatus);

    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelStatus
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                setValFsMIVlanTunnelStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelStatus (INT4 i4FsMIVlanPort,
                            INT4 i4SetValFsMIVlanTunnelStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsVlanTunnelStatus ((INT4) u2LocalPortId,
                                         i4SetValFsMIVlanTunnelStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelStatus
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                testValFsMIVlanTunnelStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanPort,
                               INT4 i4TestValFsMIVlanTunnelStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsVlanTunnelStatus (pu4ErrorCode,
                                            (INT4) u2LocalPortId,
                                            i4TestValFsMIVlanTunnelStatus);

    VlanReleaseContext ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIVlanTunnelTable
 Input       :  The Indices
                FsMIVlanPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIVlanTunnelTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIVlanTunnelProtocolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIVlanTunnelProtocolTable (INT4 i4FsMIVlanPort)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFsVlanTunnelProtocolTable (u2LocalPort);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIVlanTunnelProtocolTable (INT4 *pi4FsMIVlanPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsMIVlanTunnelProtocolTable (0, pi4FsMIVlanPort);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIVlanPort
                nextFsMIVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIVlanTunnelProtocolTable (INT4 i4FsMIVlanPort,
                                            INT4 *pi4NextFsMIVlanPort)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT4               u4IfIndex = VLAN_INVALID_PORT_INDEX;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    tVlanPortEntry     *pPortEntry = NULL;

    if (i4FsMIVlanPort < 0)
    {
        i4FsMIVlanPort = 0;
    }

    while (VlanGetNextPortInSystem (i4FsMIVlanPort, &u4IfIndex) == VLAN_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                           &u2LocalPort) == VLAN_SUCCESS)
        {
            if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
            {
                if (VLAN_SYSTEM_CONTROL () != VLAN_SNMP_TRUE)
                {
                    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

                    if (pPortEntry != NULL)
                    {
                        *pi4NextFsMIVlanPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                }

                VlanReleaseContext ();
            }
        }

        i4FsMIVlanPort = (INT4) u4IfIndex;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolDot1x
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolDot1x
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolDot1x (INT4 i4FsMIVlanPort,
                                   INT4 *pi4RetValFsMIVlanTunnelProtocolDot1x)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolDot1x ((INT4) u2LocalPort,
                                         pi4RetValFsMIVlanTunnelProtocolDot1x);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolLacp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolLacp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolLacp (INT4 i4FsMIVlanPort,
                                  INT4 *pi4RetValFsMIVlanTunnelProtocolLacp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolLacp ((INT4) u2LocalPort,
                                        pi4RetValFsMIVlanTunnelProtocolLacp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolStp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolStp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolStp (INT4 i4FsMIVlanPort,
                                 INT4 *pi4RetValFsMIVlanTunnelProtocolStp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolStp ((INT4) u2LocalPort,
                                       pi4RetValFsMIVlanTunnelProtocolStp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolGvrp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolGvrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolGvrp (INT4 i4FsMIVlanPort,
                                  INT4 *pi4RetValFsMIVlanTunnelProtocolGvrp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolGvrp ((INT4) u2LocalPort,
                                        pi4RetValFsMIVlanTunnelProtocolGvrp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolMvrp
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolMvrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolMvrp (INT4 i4FsMIVlanPort,
                                  INT4 *pi4RetValFsMIVlanTunnelProtocolMvrp)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetFsVlanTunnelProtocolMvrp ((INT4) u2LocalPort,
                                        pi4RetValFsMIVlanTunnelProtocolMvrp);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolGmrp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolGmrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolGmrp (INT4 i4FsMIVlanPort,
                                  INT4 *pi4RetValFsMIVlanTunnelProtocolGmrp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolGmrp ((INT4) u2LocalPort,
                                        pi4RetValFsMIVlanTunnelProtocolGmrp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolMmrp
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolMmrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolMmrp (INT4 i4FsMIVlanPort,
                                  INT4 *pi4RetValFsMIVlanTunnelProtocolMmrp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolMmrp ((INT4) u2LocalPort,
                                        pi4RetValFsMIVlanTunnelProtocolMmrp);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolElmi
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolElmi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolElmi (INT4 i4FsMIVlanPort,
                                  INT4 *pi4RetValFsMIVlanTunnelProtocolElmi)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolElmi ((INT4) u2LocalPort,
                                        pi4RetValFsMIVlanTunnelProtocolElmi);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolLldp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolLldp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolLldp (INT4 i4FsMIVlanPort,
                                  INT4 *pi4RetValFsMIVlanTunnelProtocolLldp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolLldp ((INT4) u2LocalPort,
                                        pi4RetValFsMIVlanTunnelProtocolLldp);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolIgmp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolIgmp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolIgmp (INT4 i4FsMIVlanPort,
                                  INT4 *pi4RetValFsMIVlanTunnelProtocolIgmp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolIgmp ((INT4) u2LocalPort,
                                        pi4RetValFsMIVlanTunnelProtocolIgmp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolEcfm                                                                   Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolEcfm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolEcfm (INT4 i4FsMIVlanPort,
                                  INT4 *pi4RetValFsMIVlanTunnelProtocolEcfm)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolEcfm ((INT4) u2LocalPort,
                                        pi4RetValFsMIVlanTunnelProtocolEcfm);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolEoam
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolEoam
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolEoam (INT4 i4FsMIVlanPort,
                                  INT4 *pi4RetValFsMIVlanTunnelProtocolEoam)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolEoam ((INT4) u2LocalPort,
                                        pi4RetValFsMIVlanTunnelProtocolEoam);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelOverrideOption
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelOverrideOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelOverrideOption (INT4 i4FsMIVlanPort,
                                    INT4 *pi4RetValFsMIVlanTunnelOverrideOption)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelOverrideOption ((INT4) u2LocalPort,
                                          pi4RetValFsMIVlanTunnelOverrideOption);

    VlanReleaseContext ();

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolDot1x
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                setValFsMIVlanTunnelProtocolDot1x
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolDot1x (INT4 i4FsMIVlanPort,
                                   INT4 i4SetValFsMIVlanTunnelProtocolDot1x)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolDot1x ((INT4) u2LocalPort,
                                         i4SetValFsMIVlanTunnelProtocolDot1x);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolLacp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                setValFsMIVlanTunnelProtocolLacp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolLacp (INT4 i4FsMIVlanPort,
                                  INT4 i4SetValFsMIVlanTunnelProtocolLacp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolLacp ((INT4) u2LocalPort,
                                        i4SetValFsMIVlanTunnelProtocolLacp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolStp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                setValFsMIVlanTunnelProtocolStp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolStp (INT4 i4FsMIVlanPort,
                                 INT4 i4SetValFsMIVlanTunnelProtocolStp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolStp ((INT4) u2LocalPort,
                                       i4SetValFsMIVlanTunnelProtocolStp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolGvrp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                setValFsMIVlanTunnelProtocolGvrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolGvrp (INT4 i4FsMIVlanPort,
                                  INT4 i4SetValFsMIVlanTunnelProtocolGvrp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolGvrp ((INT4) u2LocalPort,
                                        i4SetValFsMIVlanTunnelProtocolGvrp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolMvrp
 Input       :  The Indices
                FsMIVlanPort

                The Object
                setValFsMIVlanTunnelProtocolMvrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolMvrp (INT4 i4FsMIVlanPort,
                                  INT4 i4SetValFsMIVlanTunnelProtocolMvrp)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolMvrp ((INT4) u2LocalPort,
                                        i4SetValFsMIVlanTunnelProtocolMvrp);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolGmrp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                setValFsMIVlanTunnelProtocolGmrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolGmrp (INT4 i4FsMIVlanPort,
                                  INT4 i4SetValFsMIVlanTunnelProtocolGmrp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolGmrp ((INT4) u2LocalPort,
                                        i4SetValFsMIVlanTunnelProtocolGmrp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolMmrp
 Input       :  The Indices
                FsMIVlanPort

                The Object
                setValFsMIVlanTunnelProtocolMmrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolMmrp (INT4 i4FsMIVlanPort,
                                  INT4 i4SetValFsMIVlanTunnelProtocolMmrp)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolMmrp ((INT4) u2LocalPort,
                                        i4SetValFsMIVlanTunnelProtocolMmrp);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolElmi
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                setValFsMIVlanTunnelProtocolElmi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolElmi (INT4 i4FsMIVlanPort,
                                  INT4 i4SetValFsMIVlanTunnelProtocolElmi)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolElmi ((INT4) u2LocalPort,
                                        i4SetValFsMIVlanTunnelProtocolElmi);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolLldp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                setValFsMIVlanTunnelProtocolLldp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolLldp (INT4 i4FsMIVlanPort,
                                  INT4 i4SetValFsMIVlanTunnelProtocolLldp)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolLldp ((INT4) u2LocalPort,
                                        i4SetValFsMIVlanTunnelProtocolLldp);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolIgmp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                setValFsMIVlanTunnelProtocolIgmp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolIgmp (INT4 i4FsMIVlanPort,
                                  INT4 i4SetValFsMIVlanTunnelProtocolIgmp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolIgmp ((INT4) u2LocalPort,
                                        i4SetValFsMIVlanTunnelProtocolIgmp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolEcfm
 Input       :  The Indices
                FsMIVlanPort

                The Object
                setValFsMIVlanTunnelProtocolEcfm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolEcfm (INT4 i4FsMIVlanPort,
                                  INT4 i4SetValFsMIVlanTunnelProtocolEcfm)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolEcfm ((INT4) u2LocalPort,
                                        i4SetValFsMIVlanTunnelProtocolEcfm);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelOverrideOption
 Input       :  The Indices
                FsMIVlanPort

                The Object
                setValFsMIVlanTunnelOverrideOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelOverrideOption (INT4 i4FsMIVlanPort,
                                    INT4 i4SetValFsMIVlanTunnelOverrideOption)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelOverrideOption ((INT4) u2LocalPort,
                                          i4SetValFsMIVlanTunnelOverrideOption);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIVlanTunnelProtocolEoam
 Input       :  The Indices
                FsMIVlanPort

                The Object
                setValFsMIVlanTunnelProtocolEoam
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIVlanTunnelProtocolEoam (INT4 i4FsMIVlanPort,
                                  INT4 i4SetValFsMIVlanTunnelProtocolEoam)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhSetFsVlanTunnelProtocolEoam ((INT4) u2LocalPort,
                                        i4SetValFsMIVlanTunnelProtocolEoam);

    VlanReleaseContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelProtocolDot1x
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                testValFsMIVlanTunnelProtocolDot1x
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolDot1x (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanPort,
                                      INT4 i4TestValFsMIVlanTunnelProtocolDot1x)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolDot1x (pu4ErrorCode, (INT4) u2LocalPort,
                                            i4TestValFsMIVlanTunnelProtocolDot1x);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelProtocolLacp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                testValFsMIVlanTunnelProtocolLacp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolLacp (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanPort,
                                     INT4 i4TestValFsMIVlanTunnelProtocolLacp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolLacp (pu4ErrorCode, (INT4) u2LocalPort,
                                           i4TestValFsMIVlanTunnelProtocolLacp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelProtocolStp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                testValFsMIVlanTunnelProtocolStp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolStp (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanPort,
                                    INT4 i4TestValFsMIVlanTunnelProtocolStp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolStp (pu4ErrorCode, (INT4) u2LocalPort,
                                          i4TestValFsMIVlanTunnelProtocolStp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelProtocolGvrp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                testValFsMIVlanTunnelProtocolGvrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolGvrp (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanPort,
                                     INT4 i4TestValFsMIVlanTunnelProtocolGvrp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolGvrp (pu4ErrorCode, (INT4) u2LocalPort,
                                           i4TestValFsMIVlanTunnelProtocolGvrp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelProtocolMvrp
 Input       :  The Indices
                FsMIVlanPort

                The Object
                testValFsMIVlanTunnelProtocolMvrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolMvrp (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIVlanPort,
                                     INT4 i4TestValFsMIVlanTunnelProtocolMvrp)
{

    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolMvrp (pu4ErrorCode, (INT4) u2LocalPort,
                                           i4TestValFsMIVlanTunnelProtocolMvrp);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelProtocolGmrp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                testValFsMIVlanTunnelProtocolGmrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolGmrp (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanPort,
                                     INT4 i4TestValFsMIVlanTunnelProtocolGmrp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolGmrp (pu4ErrorCode, (INT4) u2LocalPort,
                                           i4TestValFsMIVlanTunnelProtocolGmrp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhTestv2FsMIVlanTunnelProtocolMmrp
 Input       :  The Indices
                FsMIVlanPort

                The Object
                testValFsMIVlanTunnelProtocolMmrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolMmrp (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIVlanPort,
                                     INT4 i4TestValFsMIVlanTunnelProtocolMmrp)
{

    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolMmrp (pu4ErrorCode, (INT4) u2LocalPort,
                                           i4TestValFsMIVlanTunnelProtocolMmrp);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelProtocolElmi
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                testValFsMIVlanTunnelProtocolElmi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolElmi (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIVlanPort,
                                     INT4 i4TestValFsMIVlanTunnelProtocolElmi)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolElmi (pu4ErrorCode, (INT4) u2LocalPort,
                                           i4TestValFsMIVlanTunnelProtocolElmi);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelProtocolLldp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                testValFsMIVlanTunnelProtocolLldp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolLldp (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIVlanPort,
                                     INT4 i4TestValFsMIVlanTunnelProtocolLldp)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolLldp (pu4ErrorCode, (INT4) u2LocalPort,
                                           i4TestValFsMIVlanTunnelProtocolLldp);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelProtocolIgmp
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                testValFsMIVlanTunnelProtocolIgmp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolIgmp (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanPort,
                                     INT4 i4TestValFsMIVlanTunnelProtocolIgmp)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolIgmp (pu4ErrorCode, (INT4) u2LocalPort,
                                           i4TestValFsMIVlanTunnelProtocolIgmp);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelProtocolEcfm
 Input       :  The Indices
                FsMIVlanPort

                The Object
                testValFsMIVlanTunnelProtocolEcfm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolEcfm (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanPort,
                                     INT4 i4TestValFsMIVlanTunnelProtocolEcfm)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolEcfm (pu4ErrorCode, (INT4) u2LocalPort,
                                           i4TestValFsMIVlanTunnelProtocolEcfm);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelOverrideOption
 Input       :  The Indices
                FsMIVlanPort

                The Object
                testValFsMIVlanTunnelOverrideOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelOverrideOption (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanPort,
                                       INT4
                                       i4TestValFsMIVlanTunnelOverrideOption)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelOverrideOption (pu4ErrorCode, (INT4) u2LocalPort,
                                             i4TestValFsMIVlanTunnelOverrideOption);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIVlanTunnelProtocolEoam
 Input       :  The Indices
                FsMIVlanPort

                The Object
                testValFsMIVlanTunnelProtocolEoam
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIVlanTunnelProtocolEoam (UINT4 *pu4ErrorCode, INT4 i4FsMIVlanPort,
                                     INT4 i4TestValFsMIVlanTunnelProtocolEoam)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4FsMIVlanPort < 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhTestv2FsVlanTunnelProtocolEoam (pu4ErrorCode, (INT4) u2LocalPort,
                                           i4TestValFsMIVlanTunnelProtocolEoam);

    VlanReleaseContext ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIVlanPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIVlanTunnelProtocolTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIVlanTunnelProtocolStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIVlanTunnelProtocolStatsTable
 Input       :  The Indices
                FsMIVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIVlanTunnelProtocolStatsTable (INT4 i4FsMIVlanPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsMIVlanTunnelProtocolTable (i4FsMIVlanPort);
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIVlanTunnelProtocolStatsTable
 Input       :  The Indices
                FsMIVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIVlanTunnelProtocolStatsTable (INT4 *pi4FsMIVlanPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsMIVlanTunnelProtocolTable (0, pi4FsMIVlanPort);

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIVlanTunnelProtocolStatsTable
 Input       :  The Indices
                FsMIVlanPort
                nextFsMIVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIVlanTunnelProtocolStatsTable (INT4 i4FsMIVlanPort,
                                                 INT4 *pi4NextFsMIVlanPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsMIVlanTunnelProtocolTable (i4FsMIVlanPort,
                                                           pi4NextFsMIVlanPort);

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolDot1xPktsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolDot1xPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolDot1xPktsRecvd (INT4 i4FsMIVlanPort,
                                            UINT4
                                            *pu4RetValFsMIVlanTunnelProtocolDot1xPktsRecvd)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolDot1xPktsRecvd ((INT4) u2LocalPort,
                                                  pu4RetValFsMIVlanTunnelProtocolDot1xPktsRecvd);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolDot1xPktsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolDot1xPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolDot1xPktsSent (INT4 i4FsMIVlanPort,
                                           UINT4
                                           *pu4RetValFsMIVlanTunnelProtocolDot1xPktsSent)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolDot1xPktsSent ((INT4) u2LocalPort,
                                                 pu4RetValFsMIVlanTunnelProtocolDot1xPktsSent);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolLacpPktsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolLacpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolLacpPktsRecvd (INT4 i4FsMIVlanPort,
                                           UINT4
                                           *pu4RetValFsMIVlanTunnelProtocolLacpPktsRecvd)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolLacpPktsRecvd ((INT4) u2LocalPort,
                                                 pu4RetValFsMIVlanTunnelProtocolLacpPktsRecvd);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolLacpPktsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolLacpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolLacpPktsSent (INT4 i4FsMIVlanPort,
                                          UINT4
                                          *pu4RetValFsMIVlanTunnelProtocolLacpPktsSent)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolLacpPktsSent ((INT4) u2LocalPort,
                                                pu4RetValFsMIVlanTunnelProtocolLacpPktsSent);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolStpPDUsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolStpPDUsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolStpPDUsRecvd (INT4 i4FsMIVlanPort,
                                          UINT4
                                          *pu4RetValFsMIVlanTunnelProtocolStpPDUsRecvd)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolStpPDUsRecvd ((INT4) u2LocalPort,
                                                pu4RetValFsMIVlanTunnelProtocolStpPDUsRecvd);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolStpPDUsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolStpPDUsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolStpPDUsSent (INT4 i4FsMIVlanPort,
                                         UINT4
                                         *pu4RetValFsMIVlanTunnelProtocolStpPDUsSent)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolStpPDUsSent ((INT4) u2LocalPort,
                                               pu4RetValFsMIVlanTunnelProtocolStpPDUsSent);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolGvrpPDUsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolGvrpPDUsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolGvrpPDUsRecvd (INT4 i4FsMIVlanPort,
                                           UINT4
                                           *pu4RetValFsMIVlanTunnelProtocolGvrpPDUsRecvd)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolGvrpPDUsRecvd ((INT4) u2LocalPort,
                                                 pu4RetValFsMIVlanTunnelProtocolGvrpPDUsRecvd);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIVlanTunnelProtocolMvrpPktsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolMvrpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolMvrpPktsRecvd (INT4 i4FsMIVlanPort,
                                           UINT4
                                           *pu4RetValFsMIVlanTunnelProtocolMvrpPktsRecvd)
{

    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetVal =
        nmhGetFsVlanTunnelProtocolMvrpPktsRecvd ((INT4) u2LocalPort,
                                                 pu4RetValFsMIVlanTunnelProtocolMvrpPktsRecvd);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolGvrpPDUsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolGvrpPDUsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolGvrpPDUsSent (INT4 i4FsMIVlanPort,
                                          UINT4
                                          *pu4RetValFsMIVlanTunnelProtocolGvrpPDUsSent)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolGvrpPDUsSent ((INT4) u2LocalPort,
                                                pu4RetValFsMIVlanTunnelProtocolGvrpPDUsSent);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIVlanTunnelProtocolMvrpPktsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolMvrpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolMvrpPktsSent (INT4 i4FsMIVlanPort,
                                          UINT4
                                          *pu4RetValFsMIVlanTunnelProtocolMvrpPktsSent)
{

    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolMvrpPktsSent ((INT4) u2LocalPort,
                                                pu4RetValFsMIVlanTunnelProtocolMvrpPktsSent);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolGmrpPktsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolGmrpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolGmrpPktsRecvd (INT4 i4FsMIVlanPort,
                                           UINT4
                                           *pu4RetValFsMIVlanTunnelProtocolGmrpPktsRecvd)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolGmrpPktsRecvd ((INT4) u2LocalPort,
                                                 pu4RetValFsMIVlanTunnelProtocolGmrpPktsRecvd);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolGmrpPktsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolGmrpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolGmrpPktsSent (INT4 i4FsMIVlanPort,
                                          UINT4
                                          *pu4RetValFsMIVlanTunnelProtocolGmrpPktsSent)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolGmrpPktsSent ((INT4) u2LocalPort,
                                                pu4RetValFsMIVlanTunnelProtocolGmrpPktsSent);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolMmrpPktsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolMmrpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolMmrpPktsRecvd (INT4 i4FsMIVlanPort,
                                           UINT4
                                           *pu4RetValFsMIVlanTunnelProtocolMmrpPktsRecvd)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolMmrpPktsRecvd ((INT4) u2LocalPort,
                                                 pu4RetValFsMIVlanTunnelProtocolMmrpPktsRecvd);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolMmrpPktsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolMmrpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolMmrpPktsSent (INT4 i4FsMIVlanPort,
                                          UINT4
                                          *pu4RetValFsMIVlanTunnelProtocolMmrpPktsSent)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolMmrpPktsSent ((INT4) u2LocalPort,
                                                pu4RetValFsMIVlanTunnelProtocolMmrpPktsSent);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolIgmpPktsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolIgmpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolIgmpPktsRecvd (INT4 i4FsMIVlanPort,
                                           UINT4
                                           *pu4RetValFsMIVlanTunnelProtocolIgmpPktsRecvd)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolIgmpPktsRecvd ((INT4) u2LocalPort,
                                                 pu4RetValFsMIVlanTunnelProtocolIgmpPktsRecvd);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolIgmpPktsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanTunnelProtocolIgmpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolIgmpPktsSent (INT4 i4FsMIVlanPort,
                                          UINT4
                                          *pu4RetValFsMIVlanTunnelProtocolIgmpPktsSent)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolIgmpPktsSent ((INT4) u2LocalPort,
                                                pu4RetValFsMIVlanTunnelProtocolIgmpPktsSent);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolElmiPktsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolElmiPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolElmiPktsRecvd (INT4 i4FsMIVlanPort,
                                           UINT4
                                           *pu4RetValFsMIVlanTunnelProtocolElmiPktsRecvd)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolElmiPktsRecvd ((INT4) u2LocalPort,
                                                 pu4RetValFsMIVlanTunnelProtocolElmiPktsRecvd);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolElmiPktsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolElmiPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolElmiPktsSent (INT4 i4FsMIVlanPort,
                                          UINT4
                                          *pu4RetValFsMIVlanTunnelProtocolElmiPktsSent)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolElmiPktsSent ((INT4) u2LocalPort,
                                                pu4RetValFsMIVlanTunnelProtocolElmiPktsSent);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolLldpPktsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolLldpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolLldpPktsRecvd (INT4 i4FsMIVlanPort,
                                           UINT4
                                           *pu4RetValFsMIVlanTunnelProtocolLldpPktsRecvd)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolLldpPktsRecvd ((INT4) u2LocalPort,
                                                 pu4RetValFsMIVlanTunnelProtocolLldpPktsRecvd);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolLldpPktsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolLldpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolLldpPktsSent (INT4 i4FsMIVlanPort,
                                          UINT4
                                          *pu4RetValFsMIVlanTunnelProtocolLldpPktsSent)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolLldpPktsSent ((INT4) u2LocalPort,
                                                pu4RetValFsMIVlanTunnelProtocolLldpPktsSent);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolEcfmPktsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolEcfmPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolEcfmPktsRecvd (INT4 i4FsMIVlanPort,
                                           UINT4
                                           *pu4RetValFsMIVlanTunnelProtocolEcfmPktsRecvd)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolEcfmPktsRecvd ((INT4) u2LocalPort,
                                                 pu4RetValFsMIVlanTunnelProtocolEcfmPktsRecvd);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolEcfmPktsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolEcfmPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolEcfmPktsSent (INT4 i4FsMIVlanPort,
                                          UINT4
                                          *pu4RetValFsMIVlanTunnelProtocolEcfmPktsSent)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolEcfmPktsSent ((INT4) u2LocalPort,
                                                pu4RetValFsMIVlanTunnelProtocolEcfmPktsSent);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolEoamPktsRecvd
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolEoamPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolEoamPktsRecvd (INT4 i4FsMIVlanPort,
                                           UINT4
                                           *pu4RetValFsMIVlanTunnelProtocolEoamPktsRecvd)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolEoamPktsRecvd ((INT4) u2LocalPort,
                                                 pu4RetValFsMIVlanTunnelProtocolEoamPktsRecvd);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanTunnelProtocolEoamPktsSent
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanTunnelProtocolEoamPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanTunnelProtocolEoamPktsSent (INT4 i4FsMIVlanPort,
                                          UINT4
                                          *pu4RetValFsMIVlanTunnelProtocolEoamPktsSent)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanTunnelProtocolEoamPktsSent ((INT4) u2LocalPort,
                                                pu4RetValFsMIVlanTunnelProtocolEoamPktsSent);

    VlanReleaseContext ();

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIVlanDiscardStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIVlanDiscardStatsTable
 Input       :  The Indices
                FsMIVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIVlanDiscardStatsTable (INT4 i4FsMIVlanPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal =
        nmhValidateIndexInstanceFsMIVlanTunnelProtocolTable (i4FsMIVlanPort);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIVlanDiscardStatsTable
 Input       :  The Indices
                FsMIVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIVlanDiscardStatsTable (INT4 *pi4FsMIVlanPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsMIVlanTunnelProtocolTable (0, pi4FsMIVlanPort);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIVlanDiscardStatsTable
 Input       :  The Indices
                FsMIVlanPort
                nextFsMIVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIVlanDiscardStatsTable (INT4 i4FsMIVlanPort,
                                          INT4 *pi4NextFsMIVlanPort)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexFsMIVlanTunnelProtocolTable (i4FsMIVlanPort,
                                                           pi4NextFsMIVlanPort);
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardDot1xPktsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardDot1xPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardDot1xPktsRx (INT4 i4FsMIVlanPort,
                                  UINT4 *pu4RetValFsMIVlanDiscardDot1xPktsRx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardDot1xPktsRx ((INT4) u2LocalPort,
                                        pu4RetValFsMIVlanDiscardDot1xPktsRx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardDot1xPktsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardDot1xPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardDot1xPktsTx (INT4 i4FsMIVlanPort,
                                  UINT4 *pu4RetValFsMIVlanDiscardDot1xPktsTx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardDot1xPktsTx ((INT4) u2LocalPort,
                                        pu4RetValFsMIVlanDiscardDot1xPktsTx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardLacpPktsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardLacpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardLacpPktsRx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardLacpPktsRx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardLacpPktsRx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardLacpPktsRx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardLacpPktsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardLacpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardLacpPktsTx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardLacpPktsTx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardLacpPktsTx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardLacpPktsTx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardStpPDUsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardStpPDUsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardStpPDUsRx (INT4 i4FsMIVlanPort,
                                UINT4 *pu4RetValFsMIVlanDiscardStpPDUsRx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardStpPDUsRx ((INT4) u2LocalPort,
                                      pu4RetValFsMIVlanDiscardStpPDUsRx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardStpPDUsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardStpPDUsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardStpPDUsTx (INT4 i4FsMIVlanPort,
                                UINT4 *pu4RetValFsMIVlanDiscardStpPDUsTx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardStpPDUsTx ((INT4) u2LocalPort,
                                      pu4RetValFsMIVlanDiscardStpPDUsTx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardGvrpPktsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardGvrpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardGvrpPktsRx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardGvrpPktsRx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardGvrpPktsRx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardGvrpPktsRx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardGvrpPktsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardGvrpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardGvrpPktsTx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardGvrpPktsTx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardGvrpPktsTx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardGvrpPktsTx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardMvrpPktsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardMvrpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardMvrpPktsRx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardMvrpPktsRx)
{

    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardMvrpPktsRx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardMvrpPktsRx);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardMvrpPktsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardMvrpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardMvrpPktsTx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardMvrpPktsTx)
{

    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardMvrpPktsTx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardMvrpPktsTx);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardGmrpPktsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardGmrpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardGmrpPktsRx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardGmrpPktsRx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardGmrpPktsRx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardGmrpPktsRx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardGmrpPktsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardGmrpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardGmrpPktsTx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardGmrpPktsTx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardGmrpPktsTx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardGmrpPktsTx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
Function    :  nmhGetFsMIVlanDiscardMmrpPktsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardMmrpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardMmrpPktsRx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardMmrpPktsRx)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardMmrpPktsRx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardMmrpPktsRx);
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardMmrpPktsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardMmrpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardMmrpPktsTx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardMmrpPktsTx)
{

    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardMmrpPktsTx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardMmrpPktsTx);
    VlanReleaseContext ();
    return i1RetVal;
}

/**************************************************************************** 
 Function    :  nmhGetFsMIVlanDiscardIgmpPktsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardIgmpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardIgmpPktsRx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardIgmpPktsRx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardIgmpPktsRx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardIgmpPktsRx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardIgmpPktsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object 
                retValFsMIVlanDiscardIgmpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardIgmpPktsTx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardIgmpPktsTx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardIgmpPktsTx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardIgmpPktsTx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardElmiPktsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardElmiPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardElmiPktsRx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardElmiPktsRx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardElmiPktsRx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardElmiPktsRx);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardElmiPktsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardElmiPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardElmiPktsTx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardElmiPktsTx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardElmiPktsTx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardElmiPktsTx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardLldpPktsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardLldpPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardLldpPktsRx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardLldpPktsRx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardLldpPktsRx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardLldpPktsRx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardLldpPktsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardLldpPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardLldpPktsTx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardLldpPktsTx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardLldpPktsTx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardLldpPktsTx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardEcfmPktsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardEcfmPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardEcfmPktsRx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardEcfmPktsRx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardEcfmPktsRx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardEcfmPktsRx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardEcfmPktsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardEcfmPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardEcfmPktsTx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardEcfmPktsTx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardEcfmPktsTx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardEcfmPktsTx);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardEoamPktsRx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardEoamPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardEoamPktsRx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardEoamPktsRx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardEoamPktsRx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardEoamPktsRx);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIVlanDiscardEoamPktsTx
 Input       :  The Indices
                FsMIVlanPort

                The Object
                retValFsMIVlanDiscardEoamPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIVlanDiscardEoamPktsTx (INT4 i4FsMIVlanPort,
                                 UINT4 *pu4RetValFsMIVlanDiscardEoamPktsTx)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIVlanPort, &u4ContextId,
                                       &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetFsVlanDiscardEoamPktsTx ((INT4) u2LocalPort,
                                       pu4RetValFsMIVlanDiscardEoamPktsTx);

    VlanReleaseContext ();

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIServiceVlanTunnelProtocolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIServiceVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIServiceVlanTunnelProtocolTable (INT4
                                                            i4FsMIVlanContextId,
                                                            INT4
                                                            i4FsMIServiceVlanId,
                                                            INT4
                                                            i4FsMIServiceProtocolEnum)
{
    if (VLAN_IS_VC_VALID (i4FsMIVlanContextId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_SUCCESS)
    {
        if (nmhValidateIndexInstanceFsServiceVlanTunnelProtocolTable
            (i4FsMIServiceVlanId, i4FsMIServiceProtocolEnum) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIServiceVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIServiceVlanTunnelProtocolTable (INT4 *pi4FsMIVlanContextId,
                                                    INT4 *pi4FsMIServiceVlanId,
                                                    INT4
                                                    *pi4FsMIServiceProtocolEnum)
{
    UINT4               u4ContextId = VLAN_ZERO;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            *pi4FsMIVlanContextId = (INT4) u4ContextId;
            continue;
        }
        *pi4FsMIVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexFsServiceVlanTunnelProtocolTable
            (pi4FsMIServiceVlanId, pi4FsMIServiceProtocolEnum) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }

        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           ((UINT4) *pi4FsMIVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIServiceVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIVlanContextId
                nextFsMIVlanContextId
                FsMIServiceVlanId
                nextFsMIServiceVlanId
                FsMIServiceProtocolEnum
                nextFsMIServiceProtocolEnum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIServiceVlanTunnelProtocolTable (INT4 i4FsMIVlanContextId,
                                                   INT4
                                                   *pi4NextFsMIVlanContextId,
                                                   INT4 i4FsMIServiceVlanId,
                                                   INT4
                                                   *pi4NextFsMIServiceVlanId,
                                                   INT4
                                                   i4FsMIServiceProtocolEnum,
                                                   INT4
                                                   *pi4NextFsMIServiceProtocolEnum)
{
    UINT4               u4ContextId = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_SUCCESS)
    {

        if (nmhGetNextIndexFsServiceVlanTunnelProtocolTable
            (i4FsMIServiceVlanId, pi4NextFsMIServiceVlanId,
             i4FsMIServiceProtocolEnum,
             pi4NextFsMIServiceProtocolEnum) == SNMP_SUCCESS)
        {
            *pi4NextFsMIVlanContextId = i4FsMIVlanContextId;

            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsMIVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsMIVlanContextId = (INT4) u4ContextId;
        *pi4NextFsMIVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexFsServiceVlanTunnelProtocolTable
           (pi4NextFsMIServiceVlanId,
            pi4NextFsMIServiceProtocolEnum) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIServiceVlanRsvdMacaddress
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                retValFsMIServiceVlanRsvdMacaddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIServiceVlanRsvdMacaddress (INT4 i4FsMIVlanContextId,
                                     INT4 i4FsMIServiceVlanId,
                                     INT4 i4FsMIServiceProtocolEnum,
                                     tMacAddr *
                                     pRetValFsMIServiceVlanRsvdMacaddress)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsServiceVlanRsvdMacaddress (i4FsMIServiceVlanId,
                                                  i4FsMIServiceProtocolEnum,
                                                  pRetValFsMIServiceVlanRsvdMacaddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIServiceVlanTunnelMacaddress
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                retValFsMIServiceVlanTunnelMacaddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIServiceVlanTunnelMacaddress (INT4 i4FsMIVlanContextId,
                                       INT4 i4FsMIServiceVlanId,
                                       INT4 i4FsMIServiceProtocolEnum,
                                       tMacAddr *
                                       pRetValFsMIServiceVlanTunnelMacaddress)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsServiceVlanTunnelMacaddress (i4FsMIServiceVlanId,
                                                    i4FsMIServiceProtocolEnum,
                                                    pRetValFsMIServiceVlanTunnelMacaddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIServiceVlanTunnelProtocolStatus
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                retValFsMIServiceVlanTunnelProtocolStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIServiceVlanTunnelProtocolStatus (INT4 i4FsMIVlanContextId,
                                           INT4 i4FsMIServiceVlanId,
                                           INT4 i4FsMIServiceProtocolEnum,
                                           INT4
                                           *pi4RetValFsMIServiceVlanTunnelProtocolStatus)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsServiceVlanTunnelProtocolStatus (i4FsMIServiceVlanId,
                                                        i4FsMIServiceProtocolEnum,
                                                        pi4RetValFsMIServiceVlanTunnelProtocolStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIServiceVlanTunnelPktsRecvd
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                retValFsMIServiceVlanTunnelPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIServiceVlanTunnelPktsRecvd (INT4 i4FsMIVlanContextId,
                                      INT4 i4FsMIServiceVlanId,
                                      INT4 i4FsMIServiceProtocolEnum,
                                      UINT4
                                      *pu4RetValFsMIServiceVlanTunnelPktsRecvd)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsServiceVlanTunnelPktsRecvd (i4FsMIServiceVlanId,
                                                   i4FsMIServiceProtocolEnum,
                                                   pu4RetValFsMIServiceVlanTunnelPktsRecvd);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIServiceVlanTunnelPktsSent
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                retValFsMIServiceVlanTunnelPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIServiceVlanTunnelPktsSent (INT4 i4FsMIVlanContextId,
                                     INT4 i4FsMIServiceVlanId,
                                     INT4 i4FsMIServiceProtocolEnum,
                                     UINT4
                                     *pu4RetValFsMIServiceVlanTunnelPktsSent)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsServiceVlanTunnelPktsSent (i4FsMIServiceVlanId,
                                                  i4FsMIServiceProtocolEnum,
                                                  pu4RetValFsMIServiceVlanTunnelPktsSent);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIServiceVlanDiscardPktsRx
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                retValFsMIServiceVlanDiscardPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIServiceVlanDiscardPktsRx (INT4 i4FsMIVlanContextId,
                                    INT4 i4FsMIServiceVlanId,
                                    INT4 i4FsMIServiceProtocolEnum,
                                    UINT4
                                    *pu4RetValFsMIServiceVlanDiscardPktsRx)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsServiceVlanDiscardPktsRx (i4FsMIServiceVlanId,
                                                 i4FsMIServiceProtocolEnum,
                                                 pu4RetValFsMIServiceVlanDiscardPktsRx);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIServiceVlanDiscardPktsTx
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                retValFsMIServiceVlanDiscardPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIServiceVlanDiscardPktsTx (INT4 i4FsMIVlanContextId,
                                    INT4 i4FsMIServiceVlanId,
                                    INT4 i4FsMIServiceProtocolEnum,
                                    UINT4
                                    *pu4RetValFsMIServiceVlanDiscardPktsTx)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsServiceVlanDiscardPktsTx (i4FsMIServiceVlanId,
                                                 i4FsMIServiceProtocolEnum,
                                                 pu4RetValFsMIServiceVlanDiscardPktsTx);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIServiceVlanRsvdMacaddress
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                setValFsMIServiceVlanRsvdMacaddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIServiceVlanRsvdMacaddress (INT4 i4FsMIVlanContextId,
                                     INT4 i4FsMIServiceVlanId,
                                     INT4 i4FsMIServiceProtocolEnum,
                                     tMacAddr
                                     SetValFsMIServiceVlanRsvdMacaddress)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsServiceVlanRsvdMacaddress (i4FsMIServiceVlanId,
                                                  i4FsMIServiceProtocolEnum,
                                                  SetValFsMIServiceVlanRsvdMacaddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIServiceVlanTunnelMacaddress
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                setValFsMIServiceVlanTunnelMacaddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIServiceVlanTunnelMacaddress (INT4 i4FsMIVlanContextId,
                                       INT4 i4FsMIServiceVlanId,
                                       INT4 i4FsMIServiceProtocolEnum,
                                       tMacAddr
                                       SetValFsMIServiceVlanTunnelMacaddress)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsServiceVlanTunnelMacaddress (i4FsMIServiceVlanId,
                                                    i4FsMIServiceProtocolEnum,
                                                    SetValFsMIServiceVlanTunnelMacaddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIServiceVlanTunnelProtocolStatus
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                setValFsMIServiceVlanTunnelProtocolStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIServiceVlanTunnelProtocolStatus (INT4 i4FsMIVlanContextId,
                                           INT4 i4FsMIServiceVlanId,
                                           INT4 i4FsMIServiceProtocolEnum,
                                           INT4
                                           i4SetValFsMIServiceVlanTunnelProtocolStatus)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetFsServiceVlanTunnelProtocolStatus (i4FsMIServiceVlanId,
                                                        i4FsMIServiceProtocolEnum,
                                                        i4SetValFsMIServiceVlanTunnelProtocolStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIServiceVlanRsvdMacaddress
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                testValFsMIServiceVlanRsvdMacaddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIServiceVlanRsvdMacaddress (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIVlanContextId,
                                        INT4 i4FsMIServiceVlanId,
                                        INT4 i4FsMIServiceProtocolEnum,
                                        tMacAddr
                                        TestValFsMIServiceVlanRsvdMacaddress)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsServiceVlanRsvdMacaddress (pu4ErrorCode,
                                                     i4FsMIServiceVlanId,
                                                     i4FsMIServiceProtocolEnum,
                                                     TestValFsMIServiceVlanRsvdMacaddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIServiceVlanTunnelMacaddress
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                testValFsMIServiceVlanTunnelMacaddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIServiceVlanTunnelMacaddress (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIVlanContextId,
                                          INT4 i4FsMIServiceVlanId,
                                          INT4 i4FsMIServiceProtocolEnum,
                                          tMacAddr
                                          TestValFsMIServiceVlanTunnelMacaddress)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsServiceVlanTunnelMacaddress (pu4ErrorCode,
                                                       i4FsMIServiceVlanId,
                                                       i4FsMIServiceProtocolEnum,
                                                       TestValFsMIServiceVlanTunnelMacaddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIServiceVlanTunnelProtocolStatus
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum

                The Object
                testValFsMIServiceVlanTunnelProtocolStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIServiceVlanTunnelProtocolStatus (UINT4 *pu4ErrorCode,
                                              INT4 i4FsMIVlanContextId,
                                              INT4 i4FsMIServiceVlanId,
                                              INT4 i4FsMIServiceProtocolEnum,
                                              INT4
                                              i4TestValFsMIServiceVlanTunnelProtocolStatus)
{
    INT1                i1RetVal = VLAN_ZERO;

    if (VlanSelectContext ((UINT4) i4FsMIVlanContextId) != VLAN_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2FsServiceVlanTunnelProtocolStatus (pu4ErrorCode,
                                                           i4FsMIServiceVlanId,
                                                           i4FsMIServiceProtocolEnum,
                                                           i4TestValFsMIServiceVlanTunnelProtocolStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIServiceVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIVlanContextId
                FsMIServiceVlanId
                FsMIServiceProtocolEnum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIServiceVlanTunnelProtocolTable (UINT4 *pu4ErrorCode,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
