/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanmbsm.c,v 1.65 2016/07/16 11:15:04 siva Exp $
 *
 * Description: This file contains hardware related routines used in VLAN module.
 *
 *******************************************************************/

#ifdef MBSM_WANTED
#include  "vlaninc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmUpdateOnCardInsertion                    */
/*                                                                           */
/*    Description         : This function walks all the vlan tables and      */
/*                          programs the Hardware on Line card insertion     */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdateOnCardInsertion (tMbsmPortInfo * pPortInfo,
                               tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4ContextId;
    UINT4               u4PrevContextId;
    INT4                i4RetVal;
    tLocalPortList      SlotLocalPorts;
    tLocalPortList      NullPortList;
    UINT1               u1Priority;

    VLAN_MEMSET (NullPortList, 0, sizeof (tLocalPortList));

    if (VlanGetFirstActiveContext (&u4ContextId) == VLAN_SUCCESS)
    {
        gu4ContextIdProcessed = u4ContextId;
        do
        {
            u4PrevContextId = u4ContextId;

            if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
            {
                continue;
            }

            if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
            {
                continue;
            }

            VLAN_MEMSET (SlotLocalPorts, 0, sizeof (tLocalPortList));

            /* SlotLocalPorts - tLocalPortList of the slot that is inserted
             * now and mapped to the current context
             */
            VlanMbsmGetPortListForCurrContext (pPortInfo, SlotLocalPorts);

            VlanGetPortChannelPortListForCurrContext (pPortInfo,
                                                      SlotLocalPorts);

            if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
            {

                if (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL)
                {
                    if (VLAN_MODULE_ADMIN_STATUS () == VLAN_ENABLED)
                    {
                        i4RetVal = VlanEnableVlan ();
                        if (i4RetVal != VLAN_SUCCESS)
                        {
                            VLAN_TRC (VLAN_RED_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                                      "VLAN Node to ACTIVE state failed when enabling "
                                      "VLAN module.\n");
                        }
                    }
                }

                if (VlanFsMiVlanMbsmHwSetBrgMode (VLAN_CURR_CONTEXT_ID (),
                                                  VLAN_BRIDGE_MODE (),
                                                  pSlotInfo) == FNP_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                              "Card Insertion: VlanFsMiVlanMbsmHwSetBrgMode Failed \n");

                    VlanSendAckToMbsm (MBSM_FAILURE);
                    VlanReleaseContext ();
                    return MBSM_FAILURE;
                }

                if (VlanFsMiVlanMbsmHwInit (VLAN_CURR_CONTEXT_ID (),
                                            pSlotInfo) == FNP_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                              "Card Insertion: VlanFsMiVlanMbsmHwInit Failed \n");

                    VlanSendAckToMbsm (MBSM_FAILURE);
                    VlanReleaseContext ();
                    return MBSM_FAILURE;
                }
                if (VlanFsMiVlanMbsmHwSetDefaultVlanId (VLAN_CURR_CONTEXT_ID (),
                                                        VLAN_DEF_VLAN_ID,
                                                        pSlotInfo) ==
                    FNP_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                              "Card Insertion: VlanFsMiVlanMbsmHwSetDefaultVlanId Failed \n");

                    VlanSendAckToMbsm (MBSM_FAILURE);
                    VlanReleaseContext ();
                    return MBSM_FAILURE;
                }

                /* set the FDB Aging time */
                if (VlanFsMiBrgMbsmSetAgingTime (VLAN_CURR_CONTEXT_ID (),
                                                 VlanGetAgeOutTime (),
                                                 pSlotInfo) == FNP_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                              "Card Insertion: VlanFsMiBrgMbsmSetAgingTime Failed \n");

                    VlanSendAckToMbsm (MBSM_FAILURE);
                    VlanReleaseContext ();
                    return MBSM_FAILURE;
                }

                /* set the default traffic class to priority mapping */
                for (u1Priority = 0; u1Priority < VLAN_MAX_PRIORITY;
                     u1Priority++)
                {
                    i4RetVal = VlanFsMiVlanMbsmHwTraffClassMapInit
                        (VLAN_CURR_CONTEXT_ID (), u1Priority,
                         gau1PriTrfClassMap[u1Priority]
                         [VLAN_MAX_TRAFF_CLASS - 1], pSlotInfo);

                    if (i4RetVal == FNP_FAILURE)
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                                  "default traffic class map initialization failed \n");

                        VlanSendAckToMbsm (MBSM_FAILURE);
                        VlanReleaseContext ();
                        return MBSM_FAILURE;
                    }
                }
            }                    /*end !MBSM_SLOT_INFO_ISPORTMSG(pSlotInfo) */

#ifdef EVB_WANTED
            if (VlanMbsmUpdtEvbConfig (pPortInfo,  pSlotInfo) == MBSM_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "Card Insertion: "
                        "VlanMbsmUpdtEvbConfig Failed \n");
                VlanSendAckToMbsm (MBSM_FAILURE);
                VlanReleaseContext ();
                return MBSM_FAILURE;
            }
#endif

            if (VlanMbsmUpdtVlanEntriesToHw (pPortInfo, pSlotInfo,
                                             SlotLocalPorts) == MBSM_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "Card Insertion: "
                          "VlanMbsmUpdtVlanEntriesToHw Failed \n");

                VlanSendAckToMbsm (MBSM_FAILURE);
                VlanReleaseContext ();
                return MBSM_FAILURE;
            }

            if (VlanMbsmUpdtMcastTblToHw (pPortInfo, pSlotInfo,
                                          SlotLocalPorts) == MBSM_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "Card Insertion: "
                          "VlanMbsmUpdtMcastTblToHw Failed \n");

                VlanSendAckToMbsm (MBSM_FAILURE);
                VlanReleaseContext ();
                return MBSM_FAILURE;
            }
            if (VlanMbsmUpdtWildCardTblToHw (pPortInfo, pSlotInfo,
                                             SlotLocalPorts) == MBSM_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "Card Insertion: "
                          "VlanMbsmUpdtWildCardTblToHw Failed \n");

                VlanSendAckToMbsm (MBSM_FAILURE);
                VlanReleaseContext ();
                return MBSM_FAILURE;
            }
            if (MEMCMP (SlotLocalPorts,
                        NullPortList, sizeof (tLocalPortList)) != 0)
            {
                if (VlanMbsmUpdtStUcastTblToHw
                    (pPortInfo, pSlotInfo, SlotLocalPorts) == MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "Card Insertion: "
                              "VlanMbsmUpdtStUcastTblToHw Failed \n");

                    VlanSendAckToMbsm (MBSM_FAILURE);
                    VlanReleaseContext ();
                    return MBSM_FAILURE;
                }
            }
            VlanReleaseContext ();
        }
        while (VlanGetNextActiveContext (u4PrevContextId,
                                         &u4ContextId) == VLAN_SUCCESS);
    }
    return VlanMbsmUpdtPortPropertiesToHw (pPortInfo, pSlotInfo);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmUpdtVlanEntriesToHw                      */
/*                                                                           */
/*    Description         : This function walks the vlan current table and   */
/*                          programs the Hardware with all the Static/Dynamic*/
/*                          Vlans.                                           */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                          SlotLocalPorts - Local Portlist of slot which    */
/*                          is inserted now and mapped to current context    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdtVlanEntriesToHw (tMbsmPortInfo * pPortInfo,
                             tMbsmSlotInfo * pSlotInfo,
                             tLocalPortList SlotLocalPorts)
{
    tVlanCurrEntry     *pVlanEntry;
    tHwVlanPortProperty VlanPortProperty;
    tLocalPortList      UnTagPorts;
    tLocalPortList      EgressPorts;
    tLocalPortList      NullPorts;
    tVlanId             VlanId;
    UINT4               u4FdbId;
    UINT4               u4IfIndex = 0;

    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (SlotLocalPorts);

    MEMSET (NullPorts, 0, sizeof (tLocalPortList));

    /* 
     *   Scan the Vlan current database, program each vlan entry
     *   into the newly inserted card.
     *   For Dynamic entry the vlan entry will be programmed
     *   with null port list.
     */
    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pVlanEntry = VlanGetVlanEntry (VlanId);

        if (pVlanEntry == NULL)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtVlanEntriesToHw : "
  	  	                          "VlanGetVlanEntry Failed \n");
            return MBSM_FAILURE;
        }
        if (pVlanEntry->pStVlanEntry != NULL)
        {
            VLAN_GET_UNTAGGED_PORTS (pVlanEntry->pStVlanEntry, UnTagPorts);
        }
        else
        {
            if (FsMiWrVlanMbsmHwAddVlanEntry
                (VLAN_CURR_CONTEXT_ID (), pVlanEntry->VlanId, NullPorts,
                 NullPorts, pSlotInfo) == FNP_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtVlanEntriesToHw : "
  	  	                          "FsMiWrVlanMbsmHwAddVlanEntry Failed \n");
                return MBSM_FAILURE;
            }

            /* We have added a Dynamic entry with Null port list.
             * All the information for this Vlan will be learned dynamically
             * So nothing to do further for this vlan. Just continue with 
             * next Vlan
             */
            continue;
        }

        VLAN_GET_CURR_EGRESS_PORTS (pVlanEntry, EgressPorts);

        if (FsMiWrVlanMbsmHwAddVlanEntry (VLAN_CURR_CONTEXT_ID (),
                                          pVlanEntry->VlanId, EgressPorts,
                                          UnTagPorts, pSlotInfo) == FNP_FAILURE)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtVlanEntriesToHw : "
  	  	                          "FsMiWrVlanMbsmHwAddVlanEntry Failed \n");
            return MBSM_FAILURE;
        }

        if (VlanGetFdbIdFromVlanId (pVlanEntry->VlanId,
                                    &u4FdbId) == VLAN_FAILURE)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtVlanEntriesToHw : "
  	  	                          "VlanGetFdbIdFromVlanId Failed \n");
            return MBSM_FAILURE;
        }

        if (VlanFsMiVlanMbsmHwMacLearningLimit (VLAN_CURR_CONTEXT_ID (),
                                                pVlanEntry->VlanId,
                                                (UINT2) u4FdbId,
                                                VLAN_CONTROL_MAC_LEARNING_LIMIT
                                                (pVlanEntry->VlanId),
                                                pSlotInfo) != FNP_SUCCESS)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtVlanEntriesToHw : "
                                          "VlanFsMiVlanMbsmHwMacLearningLimit Failed \n");

            return MBSM_FAILURE;
        }
        if (VlanFsMiVlanMbsmHwSetPortProperty (VLAN_CURR_CONTEXT_ID (), u4IfIndex,
                                               VlanPortProperty,
                                               pSlotInfo) != FNP_SUCCESS)
        {
            return MBSM_FAILURE;
        }
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmUpdtStUcastTblToHw                       */
/*                                                                           */
/*    Description         : This function walks the vlan fdb table and       */
/*                          programs the Hardware with all the Static        */
/*                          unicast FDB entries                              */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                          SlotLocalPorts - Local Portlist of slot which    */
/*                          is inserted now and mapped to current context    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdtStUcastTblToHw (tMbsmPortInfo * pPortInfo,
                            tMbsmSlotInfo * pSlotInfo,
                            tLocalPortList SlotLocalPorts)
{
    tLocalPortList      AllowedToGoTo;
    tLocalPortList      NullPortList;
    tVlanFidEntry      *pFidEntry;
    tVlanStUcastEntry  *pStUcastEntry;
    tMbsmSlotInfo       ProgSlotInfo;
    tVlanPortEntry     *pPortEntry;
    UINT4               u4Fid;
    INT4                i4RcvPortSlotId = -1;
    UINT4               u4RcvPhyPort = 0;
    UINT2               u2PortIndex = 0;
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;

    UNUSED_PARAM (pPortInfo);

    MEMSET (NullPortList, 0, sizeof (tLocalPortList));

    /*
     *  Scan the Fid Table and scan the Static unicast table in
     *  each fid entry and program the static unicast entry 
     *  in the inserted card
     */

    for (u4Fid = 0; u4Fid < VLAN_MAX_FID_ENTRIES; u4Fid++)
    {
        pFidEntry = VLAN_GET_FID_ENTRY (u4Fid);
        if (pFidEntry == NULL)
        {
            continue;
        }

        pStUcastEntry = pFidEntry->pStUcastTbl;

        while (pStUcastEntry != NULL)
        {
            MEMCPY (AllowedToGoTo, pStUcastEntry->AllowedToGo,
                    sizeof (tLocalPortList));

            if (pStUcastEntry->u2RcvPort != 0)
            {
                u4RcvPhyPort = VLAN_GET_PHY_PORT (pStUcastEntry->u2RcvPort);

                if (MbsmGetSlotFromPort (u4RcvPhyPort, &i4RcvPortSlotId)
                    == MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtStUcastTblToHw : "
  	  	                              "MbsmGetSlotFromPort Failed \n");
                    return MBSM_FAILURE;
                }
            }
            else
            {
                u4RcvPhyPort = 0;
            }

            if ((u4RcvPhyPort == 0) ||
                ((u4RcvPhyPort != 0) &&
                 (i4RcvPortSlotId == MBSM_SLOT_INFO_SLOT_ID (pSlotInfo))))
            {
                if (FsMiWrVlanMbsmHwAddStaticUcastEntry
                    (VLAN_CURR_CONTEXT_ID (), pFidEntry->u4Fid,
                     pStUcastEntry->MacAddr, u4RcvPhyPort, AllowedToGoTo,
                     pStUcastEntry->u1Status, pSlotInfo,
                     pStUcastEntry->ConnectionId) == FNP_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtStUcastTblToHw : "
  	  	                              "FsMiWrVlanMbsmHwAddStaticUcastEntry Failed \n");
                    return MBSM_FAILURE;
                }
            }

            if ((u4RcvPhyPort != 0) &&
                (i4RcvPortSlotId == MBSM_SLOT_INFO_SLOT_ID (pSlotInfo)))
            {
                /* Proceed to program next Ucast entry */
                pStUcastEntry = pStUcastEntry->pNextNode;
                continue;
            }

            /* Masking out the ports of other slots */
            VLAN_AND_PORT_LIST (AllowedToGoTo, SlotLocalPorts);

            if (MEMCMP (AllowedToGoTo, NullPortList,
                        sizeof (tLocalPortList)) == 0)
            {
                /* Proceed to program next Ucast entry */
                pStUcastEntry = pStUcastEntry->pNextNode;
                continue;
            }

            if ((u4RcvPhyPort != 0) &&
                (i4RcvPortSlotId != MBSM_SLOT_INFO_SLOT_ID (pSlotInfo)))
            {
                if (FsMiWrVlanMbsmHwAddStaticUcastEntry
                    (VLAN_CURR_CONTEXT_ID (), pFidEntry->u4Fid,
                     pStUcastEntry->MacAddr, u4RcvPhyPort, AllowedToGoTo,
                     pStUcastEntry->u1Status, pSlotInfo,
                     pStUcastEntry->ConnectionId) == FNP_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtStUcastTblToHw : "
  	  	                              "FsMiWrVlanMbsmHwAddStaticUcastEntry Failed \n");
                    return MBSM_FAILURE;
                }
            }

            MEMCPY (&ProgSlotInfo, pSlotInfo, sizeof (tMbsmSlotInfo));

            if (u4RcvPhyPort != 0)
            {
                ProgSlotInfo.i4SlotId = i4RcvPortSlotId;
            }
            else
            {
                ProgSlotInfo.i4SlotId = MBSM_SLOT_ALL;
            }

            for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
            {
                u1PortFlag = AllowedToGoTo[u2ByteInd];
                for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                                      && (u1PortFlag != 0)); u2BitIndex++)
                {
                    if ((u1PortFlag & VLAN_BIT8) != 0)
                    {
                        u2PortIndex =
                            (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);
                        pPortEntry = VLAN_GET_PORT_ENTRY (u2PortIndex);

                        if (pPortEntry != NULL)
                        {
                            if (VlanFsMiVlanMbsmHwSetUcastPort
                                (VLAN_CURR_CONTEXT_ID (), pFidEntry->u4Fid,
                                 pStUcastEntry->MacAddr, u4RcvPhyPort,
                                 VLAN_GET_PHY_PORT (u2PortIndex),
                                 pStUcastEntry->u1Status,
                                 &ProgSlotInfo) == FNP_FAILURE)
                            {
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtStUcastTblToHw : "
  	  	                                 "VlanFsMiVlanMbsmHwSetUcastPort Failed \n");
                                return MBSM_FAILURE;
                            }
                        }
                    }
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                }
            }

            pStUcastEntry = pStUcastEntry->pNextNode;
        }
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmUpdtMcastTblToHw                         */
/*                                                                           */
/*    Description         : This function walks the vlan current table and   */
/*                          programs the Hardware with all the Static  and   */
/*                          Dynamic Multicast group entries                  */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                          SlotLocalPorts - Local Portlist of slot which    */
/*                          is inserted now and mapped to current context    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdtMcastTblToHw (tMbsmPortInfo * pMBSMPortInfo,
                          tMbsmSlotInfo * pSlotInfo,
                          tLocalPortList SlotLocalPorts)
{
    tLocalPortList      HwPorts;
    tLocalPortList      NullPortList;
#ifdef VLAN_EXTENDED_FILTER
    tLocalPortList      AllGrpPorts;
    tLocalPortList      UnregGrpPorts;
#endif
    tMbsmSlotInfo       ProgSlotInfo;
    tVlanId             VlanId;
    tVlanCurrEntry     *pVlanEntry;
    tVlanStMcastEntry  *pStMcastEntry;
    tVlanGroupEntry    *pGroupEntry;
    tVlanPortEntry     *pPortEntry;
    INT4                i4RcvPortSlotId = -1;
    UINT4               u4RcvPhyPort = 0;
    UINT2               u2PortIndex = 0;
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    UNUSED_PARAM (pMBSMPortInfo);

    MEMSET (NullPortList, 0, sizeof (tLocalPortList));

    /*
     *   Scan the Vlan Current Database and program 
     *   the static multicast entry and Dynamic multicast
     *   entries for each vlan into the inserted card.
     *   The Dynamic Multicast entries will be programmed
     *   with Null egress portlist.
     *
     *   Also the forward all, forward unreg entry for each
     *   vlan will be programmed in the inserted card. 
     *
     */
    VLAN_SCAN_VLAN_TABLE (VlanId)
    {

        pVlanEntry = VlanGetVlanEntry (VlanId);

        if (pVlanEntry == NULL)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtMcastTblToHw : "
  	  	                          "VlanGetVlanEntry Failed \n");
            return MBSM_FAILURE;
        }
        pStMcastEntry = pVlanEntry->pStMcastTable;

        while (pStMcastEntry != NULL)
        {
            MEMCPY (HwPorts, pStMcastEntry->EgressPorts,
                    sizeof (tLocalPortList));

            if (pStMcastEntry->u2RcvPort != 0)
            {
                u4RcvPhyPort = VLAN_GET_PHY_PORT (pStMcastEntry->u2RcvPort);

                if (MbsmGetSlotFromPort (u4RcvPhyPort, &i4RcvPortSlotId)
                    == MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtMcastTblToHw : "
  	  	                          "MbsmGetSlotFromPort Failed \n");
                    return MBSM_FAILURE;
                }
            }
            else
            {
                u4RcvPhyPort = 0;
            }

            if ((u4RcvPhyPort == 0) ||
                ((u4RcvPhyPort != 0) &&
                 (i4RcvPortSlotId == MBSM_SLOT_INFO_SLOT_ID (pSlotInfo))))
            {
                if (FsMiWrVlanMbsmHwAddStMcastEntry
                    (VLAN_CURR_CONTEXT_ID (), pVlanEntry->VlanId,
                     pStMcastEntry->MacAddr, u4RcvPhyPort,
                     HwPorts, pSlotInfo) == FNP_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtMcastTblToHw : "
  	  	                          "FsMiWrVlanMbsmHwAddStMcastEntry Failed \n");
                    return MBSM_FAILURE;
                }
            }

            if ((u4RcvPhyPort != 0) &&
                (i4RcvPortSlotId == MBSM_SLOT_INFO_SLOT_ID (pSlotInfo)))
            {
                /* Proceed to program next static Mcast entry */
                pStMcastEntry = pStMcastEntry->pNextNode;
                continue;
            }

            /* Masking out the ports of other slots */
            VLAN_AND_PORT_LIST (HwPorts, SlotLocalPorts);

            if (MEMCMP (HwPorts, NullPortList, sizeof (tLocalPortList)) == 0)
            {
                /* Proceed to program next static Mcast entry */
                pStMcastEntry = pStMcastEntry->pNextNode;
                continue;
            }

            if ((u4RcvPhyPort != 0) &&
                (i4RcvPortSlotId != MBSM_SLOT_INFO_SLOT_ID (pSlotInfo)))
            {
                if (FsMiWrVlanMbsmHwAddStMcastEntry
                    (VLAN_CURR_CONTEXT_ID (), pVlanEntry->VlanId,
                     pStMcastEntry->MacAddr, u4RcvPhyPort,
                     HwPorts, pSlotInfo) == FNP_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtMcastTblToHw : "
  	  	                          "FsMiWrVlanMbsmHwAddStMcastEntry Failed \n");
                    return MBSM_FAILURE;
                }
            }

            MEMCPY (&ProgSlotInfo, pSlotInfo, sizeof (tMbsmSlotInfo));

            if (u4RcvPhyPort != 0)
            {
                ProgSlotInfo.i4SlotId = i4RcvPortSlotId;
            }
            else
            {
                ProgSlotInfo.i4SlotId = MBSM_SLOT_ALL;
            }

            for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
            {
                u1PortFlag = HwPorts[u2ByteInd];
                for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                                      && (u1PortFlag != 0)); u2BitIndex++)
                {
                    if ((u1PortFlag & VLAN_BIT8) != 0)
                    {
                        u2PortIndex =
                            (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);
                        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS
                                                 (pMBSMPortInfo), u2PortIndex,
                                                 sizeof (tPortList),
                                                 u1IsSetInPortListStatus);
                        if (OSIX_FALSE == u1IsSetInPortListStatus)
                        {
                            pPortEntry = VLAN_GET_PORT_ENTRY (u2PortIndex);

                            if (pPortEntry != NULL)
                            {
                                VlanFsMiVlanMbsmHwSetMcastPort
                                    (VLAN_CURR_CONTEXT_ID (),
                                     pVlanEntry->VlanId,
                                     pStMcastEntry->MacAddr,
                                     VLAN_GET_PHY_PORT (u2PortIndex),
                                     &ProgSlotInfo);
                            }
                        }
                    }
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                }
            }

            pStMcastEntry = pStMcastEntry->pNextNode;
        }

        pGroupEntry = pVlanEntry->pGroupTable;

        while (pGroupEntry != NULL)
        {
            /*Generally the forwarding database will not be having any remote
             * ports learnt in it, before the remote line card gets attached.
             * In this scenario, when MBSM line card attach event is received,
             * Forwarding database is programmed in remote line card with
             * the Null portlist.
             * But when a remote card's port is configured as mrouter port,
             * it will automatically gets added in the forwarding database
             *In this scenario, when MBSM line card attach event is received,
             *Forwarding database is programmed in NP with the remote ports. */

            MEMCPY (HwPorts, NullPortList, sizeof (tLocalPortList));
            MEMCPY (HwPorts, pGroupEntry->LearntPorts, sizeof (tLocalPortList));
            VLAN_AND_PORT_LIST (HwPorts, SlotLocalPorts);

            if (pGroupEntry->u1StRefCount == 0)
            {
                if (FsMiWrVlanMbsmHwAddMcastEntry
                    (VLAN_CURR_CONTEXT_ID (), pVlanEntry->VlanId,
                     pGroupEntry->MacAddr, HwPorts, pSlotInfo) == FNP_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtMcastTblToHw : "
  	  	                          "FsMiWrVlanMbsmHwAddMcastEntry Failed \n");
                    return MBSM_FAILURE;
                }
            }
            pGroupEntry = pGroupEntry->pNextNode;
        }

#ifdef VLAN_EXTENDED_FILTER
        MEMCPY (AllGrpPorts, pVlanEntry->AllGrps.Ports,
                sizeof (tLocalPortList));

        if (FsMiWrVlanMbsmHwSetAllGroupsPorts
            (VLAN_CURR_CONTEXT_ID (), pVlanEntry->VlanId, AllGrpPorts,
             pSlotInfo) == FNP_FAILURE)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtMcastTblToHw : "
  	  	                          "FsMiWrVlanMbsmHwSetAllGroupsPorts Failed \n");
            return MBSM_FAILURE;
        }
        MEMCPY (UnregGrpPorts, pVlanEntry->UnRegGrps.Ports,
                sizeof (tLocalPortList));
        if (FsMiWrVlanMbsmHwSetUnRegGroupsPorts
            (VLAN_CURR_CONTEXT_ID (), pVlanEntry->VlanId, UnregGrpPorts,
             pSlotInfo) == FNP_FAILURE)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtMcastTblToHw : "
  	  	                          "FsMiWrVlanMbsmHwSetUnRegGroupsPorts Failed \n");
            return MBSM_FAILURE;
        }
#endif

    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmUpdateOnCardRemoval                      */
/*                                                                           */
/*    Description         : This function walks all the vlan tables and      */
/*                          resets the removed ports from the hw tables      */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdateOnCardRemoval (tMbsmPortInfo * pPortInfo,
                             tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4ContextId;
    UINT4               u4PrevContextId;
#ifdef NPAPI_WANTED
    UINT4               u4IfIndex = 0;
    tVlanFlushInfo      VlanFlushInfo;
#endif
    tLocalPortList      ContextLocalPorts;
    tLocalPortList      NullPortList;

#ifdef NPAPI_WANTED
    VLAN_MEMSET (&VlanFlushInfo, 0, sizeof (tVlanFlushInfo));
#endif
    VLAN_MEMSET (NullPortList, 0, sizeof (tLocalPortList));

    if (VlanGetFirstActiveContext (&u4ContextId) == VLAN_SUCCESS)
    {
        do
        {
            u4PrevContextId = u4ContextId;

            if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
            {
                continue;
            }

            if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
            {
                continue;
            }
#ifdef NPAPI_WANTED
            for (u4IfIndex = pPortInfo->u4StartIfIndex;
                 u4IfIndex <
                 (pPortInfo->u4StartIfIndex + pPortInfo->u4PortCount);
                 u4IfIndex++)
            {
                VlanFlushInfo.u4IfIndex = u4IfIndex;
                /* Flush the MAC Learned on the correspoding ports from Hardware */
                if (VlanHwFlushFdbList (u4ContextId, &VlanFlushInfo) ==
                    VLAN_FAILURE)
                {
                    return MBSM_FAILURE;
                }
                VLAN_MEMSET (&VlanFlushInfo, 0, sizeof (tVlanFlushInfo));

            }
#endif
            VLAN_MEMSET (ContextLocalPorts, 0, sizeof (tLocalPortList));

            VlanGetPortListForCurrContext (MBSM_PORT_INFO_PORTLIST_STATUS
                                           (pPortInfo), ContextLocalPorts);

            if (((pSlotInfo->u1SwFabricType == MBSM_FALSE) &&
                 (MEMCMP
                  (ContextLocalPorts, NullPortList,
                   sizeof (tLocalPortList))) != 0)
                || (pSlotInfo->u1SwFabricType == MBSM_TRUE))
            {
                if (VlanMbsmResetVlanTbl (pPortInfo, pSlotInfo,
                                          ContextLocalPorts) == MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                              "Card Removal: VlanMbsmResetVlanTbl Failed \n");

                    VlanReleaseContext ();
                    return MBSM_FAILURE;
                }
                if (VlanMbsmResetMcastTbl (pPortInfo, pSlotInfo,
                                           ContextLocalPorts) == MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                              "Card Removal: VlanMbsmResetMcastTbl Failed \n");

                    VlanReleaseContext ();
                    return MBSM_FAILURE;
                }
            }

            if (MEMCMP (ContextLocalPorts,
                        NullPortList, sizeof (tLocalPortList)) != 0)
            {
                if (VlanMbsmResetStUcastTbl (pPortInfo, pSlotInfo,
                                             ContextLocalPorts) == MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                              "Card Removal: VlanMbsmResetStUcastTbl Failed \n");
                    VlanReleaseContext ();
                    return MBSM_FAILURE;
                }
                if (VlanMbsmResetWildCardTbl (pPortInfo, pSlotInfo,
                                              ContextLocalPorts) ==
                    MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                              "Card Removal: VlanMbsmResetWildCardTbl Failed \n");
                    VlanReleaseContext ();
                    return MBSM_FAILURE;
                }
            }

            VlanReleaseContext ();

        }
        while (VlanGetNextActiveContext (u4PrevContextId,
                                         &u4ContextId) == VLAN_SUCCESS);
    }

    return MBSM_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmResetVlanTbl                             */
/*                                                                           */
/*    Description         : This function walks the static vlan table and    */
/*                          resets the removed ports from the hw tables      */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                          LocalPorts - Local Portlist of current context   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmResetVlanTbl (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo,
                      tLocalPortList LocalPorts)
{
    tVlanCurrEntry     *pVlanEntry;
    tVlanPortEntry     *pPortEntry = NULL;
    tLocalPortList      EgressPorts;
    tVlanId             VlanId;
    tMbsmSlotInfo       ProgSlotInfo;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2Port;
    UINT1               u1PortFlag;

    UNUSED_PARAM (pPortInfo);

    MEMSET (EgressPorts, 0, sizeof (tLocalPortList));

    MEMCPY (&ProgSlotInfo, pSlotInfo, sizeof (tMbsmSlotInfo));
    ProgSlotInfo.i4SlotId = MBSM_SLOT_ALL;
    /*
     * Scan the Vlan Current Table and for each vlan
     * entry present, reset the ports present in the 
     * removed slot from the vlan entry in the hardware.
     *
     */
    VLAN_SCAN_VLAN_TABLE (VlanId)
    {

        pVlanEntry = VlanGetVlanEntry (VlanId);

        if (pVlanEntry->pStVlanEntry != NULL)
        {
            VLAN_GET_EGRESS_PORTS (pVlanEntry->pStVlanEntry, EgressPorts);
            VLAN_AND_PORT_LIST (EgressPorts, LocalPorts);
            for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
            {
                u1PortFlag = EgressPorts[u2ByteInd];
                for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                                      && (u1PortFlag != 0)); u2BitIndex++)
                {
                    if ((u1PortFlag & VLAN_BIT8) != 0)
                    {
                        u2Port =
                            (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);

                        pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

                        if (pPortEntry != NULL)
                        {
                            VlanFsMiVlanMbsmHwResetVlanMemberPort
                                (VLAN_CURR_CONTEXT_ID (),
                                 pVlanEntry->VlanId,
                                 VLAN_GET_PHY_PORT (u2Port), &ProgSlotInfo);
                        }
                    }
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                }
            }
        }
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmResetStUcastTbl                          */
/*                                                                           */
/*    Description         : This function walks the static unicast table and */
/*                          removes the entry from the hardware              */
/*                          In distributed data plane case the entry is      */
/*                          removed from all the other slots.                */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                          LocalPorts - Local Portlist of current context   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmResetStUcastTbl (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo,
                         tLocalPortList LocalPorts)
{
    tLocalPortList      AllowedToGoTo;
    tLocalPortList      NullPortList;
    tVlanFidEntry      *pFidEntry;
    tVlanStUcastEntry  *pStUcastEntry;
    tVlanPortEntry     *pPortEntry = NULL;
    tMbsmSlotInfo       ProgSlotInfo;
    UINT4               u4Fid;
    INT4                i4RcvPortSlotId = -1;
    UINT4               u4RcvPhyPort = 0;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2Port;
    UINT1               u1PortFlag;

    UNUSED_PARAM (pPortInfo);

    MEMSET (NullPortList, 0, sizeof (tLocalPortList));

    MEMCPY (&ProgSlotInfo, pSlotInfo, sizeof (tMbsmSlotInfo));

    /*
     *   Scan the Fid Table and for each entry present
     *   in the static unicast table in each Fid Entry
     *   if the AllowedToGoTo PortList contains only
     *   the ports present in the removed slot then
     *   the Static Unicast entry is removed from all
     *   the other cards.
     */
    for (u4Fid = 0; u4Fid < VLAN_MAX_FID_ENTRIES; u4Fid++)
    {
        pFidEntry = VLAN_GET_FID_ENTRY (u4Fid);
        if (pFidEntry == NULL)
        {
            continue;
        }

        pStUcastEntry = pFidEntry->pStUcastTbl;

        while (pStUcastEntry != NULL)
        {
            MEMSET (AllowedToGoTo, 0, sizeof (tLocalPortList));
            MEMCPY (AllowedToGoTo, pStUcastEntry->AllowedToGo,
                    sizeof (tLocalPortList));

            if (pStUcastEntry->u2RcvPort != 0)
            {
                u4RcvPhyPort = VLAN_GET_PHY_PORT (pStUcastEntry->u2RcvPort);

                if (MbsmGetSlotFromPort (u4RcvPhyPort, &i4RcvPortSlotId)
                    == MBSM_FAILURE)
                {
                    return MBSM_FAILURE;
                }
            }
            else
            {
                u4RcvPhyPort = 0;
            }

            if ((u4RcvPhyPort != 0) &&
                (i4RcvPortSlotId == MBSM_SLOT_INFO_SLOT_ID (pSlotInfo)))
            {
                ProgSlotInfo.i4SlotId = MBSM_SLOT_ALL;

                if (VlanFsMiVlanMbsmHwDelStaticUcastEntry
                    (VLAN_CURR_CONTEXT_ID (), pFidEntry->u4Fid,
                     pStUcastEntry->MacAddr, u4RcvPhyPort,
                     &ProgSlotInfo) == FNP_FAILURE)
                {
                    return MBSM_FAILURE;
                }
            }

            VLAN_AND_PORT_LIST (AllowedToGoTo, LocalPorts);
            if (MEMCMP (AllowedToGoTo, NullPortList,
                        sizeof (tLocalPortList)) == 0)
            {
                /*Proceed to program next Ucast entry */
                pStUcastEntry = pStUcastEntry->pNextNode;
                continue;
            }

            if ((u4RcvPhyPort == 0) ||
                ((u4RcvPhyPort != 0) &&
                 (i4RcvPortSlotId != MBSM_SLOT_INFO_SLOT_ID (pSlotInfo))))
            {
                if (u4RcvPhyPort == 0)
                {
                    ProgSlotInfo.i4SlotId = MBSM_SLOT_ALL;
                }

                if ((u4RcvPhyPort != 0) &&
                    (i4RcvPortSlotId != MBSM_SLOT_INFO_SLOT_ID (pSlotInfo)))
                {
                    ProgSlotInfo.i4SlotId = i4RcvPortSlotId;
                }

                for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE;
                     u2ByteInd++)
                {
                    u1PortFlag = AllowedToGoTo[u2ByteInd];
                    for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                                          && (u1PortFlag != 0)); u2BitIndex++)
                    {
                        if ((u1PortFlag & VLAN_BIT8) != 0)
                        {
                            u2Port =
                                (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                         u2BitIndex + 1);

                            pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

                            if (pPortEntry != NULL)
                            {
                                VlanFsMiVlanMbsmHwResetUcastPort
                                    (VLAN_CURR_CONTEXT_ID (), pFidEntry->u4Fid,
                                     pStUcastEntry->MacAddr, u4RcvPhyPort,
                                     VLAN_GET_PHY_PORT (u2Port), &ProgSlotInfo);
                            }
                        }
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                    }
                }
            }

            pStUcastEntry = pStUcastEntry->pNextNode;
        }
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmResetMcastTbl                            */
/*                                                                           */
/*    Description         : This function walks the static multicast         */
/*                          table and resets the port that are removed.      */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                          LocalPorts - Local Portlist of current context   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmResetMcastTbl (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo,
                       tLocalPortList LocalPorts)
{
    tLocalPortList      HwPorts;
    tLocalPortList      NullPortList;
    tVlanCurrEntry     *pVlanEntry;
    tVlanStMcastEntry  *pStMcastEntry;
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanId             VlanId;
    tMbsmSlotInfo       ProgSlotInfo;
    INT4                i4RcvPortSlotId = -1;
    UINT4               u4RcvPhyPort;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2Port;
    UINT1               u1PortFlag;

    UNUSED_PARAM (pPortInfo);

    MEMSET (NullPortList, 0, sizeof (tLocalPortList));
    MEMCPY (&ProgSlotInfo, pSlotInfo, sizeof (tMbsmSlotInfo));

    /*
     * Scan the vlan current table and for each multicast
     * entry present in the static and multicast tables in
     * each vlan entry, reset the ports from the egress port
     * list of the multicast entries if the port is present 
     * in the removed card.
     */
    VLAN_SCAN_VLAN_TABLE (VlanId)
    {
        pVlanEntry = VlanGetVlanEntry (VlanId);

        if (pVlanEntry == NULL)
        {
            return MBSM_FAILURE;
        }

        pStMcastEntry = pVlanEntry->pStMcastTable;

        while (pStMcastEntry != NULL)
        {
            MEMCPY (HwPorts, pStMcastEntry->EgressPorts,
                    sizeof (tLocalPortList));

            if (pStMcastEntry->u2RcvPort != 0)
            {
                u4RcvPhyPort = VLAN_GET_PHY_PORT (pStMcastEntry->u2RcvPort);

                if (MbsmGetSlotFromPort (u4RcvPhyPort, &i4RcvPortSlotId)
                    == MBSM_FAILURE)
                {
                    return MBSM_FAILURE;
                }
            }
            else
            {
                u4RcvPhyPort = 0;
            }

            if ((u4RcvPhyPort != 0) &&
                (i4RcvPortSlotId == MBSM_SLOT_INFO_SLOT_ID (pSlotInfo)))
            {
                ProgSlotInfo.i4SlotId = MBSM_SLOT_ALL;

                if (VlanFsMiVlanMbsmHwDelStMcastEntry
                    (VLAN_CURR_CONTEXT_ID (), pVlanEntry->VlanId,
                     pStMcastEntry->MacAddr, u4RcvPhyPort,
                     &ProgSlotInfo) == FNP_FAILURE)
                {
                    return MBSM_FAILURE;
                }
            }

            VLAN_AND_PORT_LIST (HwPorts, LocalPorts);

            if (MEMCMP (HwPorts, NullPortList, sizeof (tLocalPortList)) == 0)
            {
                /*Proceed to program next Mcast entry */
                pStMcastEntry = pStMcastEntry->pNextNode;
                continue;
            }

            if ((u4RcvPhyPort == 0) ||
                ((u4RcvPhyPort != 0) &&
                 (i4RcvPortSlotId != MBSM_SLOT_INFO_SLOT_ID (pSlotInfo))))
            {
                if (u4RcvPhyPort == 0)
                {
                    ProgSlotInfo.i4SlotId = MBSM_SLOT_ALL;
                }

                if ((u4RcvPhyPort != 0) &&
                    (i4RcvPortSlotId != MBSM_SLOT_INFO_SLOT_ID (pSlotInfo)))
                {
                    ProgSlotInfo.i4SlotId = i4RcvPortSlotId;
                }

                for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE;
                     u2ByteInd++)
                {
                    u1PortFlag = HwPorts[u2ByteInd];
                    for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                                          && (u1PortFlag != 0)); u2BitIndex++)
                    {
                        if ((u1PortFlag & VLAN_BIT8) != 0)
                        {
                            u2Port =
                                (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                         u2BitIndex + 1);

                            pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

                            if (pPortEntry != NULL)
                            {
                                VlanFsMiVlanMbsmHwResetMcastPort
                                    (VLAN_CURR_CONTEXT_ID (),
                                     pVlanEntry->VlanId,
                                     pStMcastEntry->MacAddr,
                                     VLAN_GET_PHY_PORT (u2Port), &ProgSlotInfo);
                            }
                        }
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                    }
                }
            }

            pStMcastEntry = pStMcastEntry->pNextNode;
        }
    }

    return MBSM_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanMbsmPostMessage ()                               */
/*                                                                         */
/* Description      : This function is an API for mbsm module to enqueue   */
/*                    packets when a new LC is inserted or removed         */
/*                                                                         */
/* Input(s)         : pProtoMsg - Queue message.                           */
/*                                                                         */
/*                    i4Event   - Event type.                              */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{

    tVlanQMsg          *pVlanQMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (VLAN_IS_MODULE_INITIALISED () == VLAN_FALSE)
    {
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                              "VlanMbsmPostMessage enqueued packets and returns success \n");
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    pVlanQMsg = (tVlanQMsg *) VLAN_GET_BUF (VLAN_QMSG_BUFF, sizeof (tVlanQMsg));

    if (NULL == pVlanQMsg)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                       (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                       VLAN_NAME, " Message Allocation failed for Vlan "
                       "Config Message Type %u\r\n", VLAN_REMOVE_PORT_INFO_MSG);
        return MBSM_FAILURE;
    }

    VLAN_MEMSET (pVlanQMsg, 0, sizeof (tVlanQMsg));

    pVlanQMsg->u2MsgType = (UINT2) i4Event;

    if (pProtoMsg != NULL)
    {
        if (!(pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg =
              MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)))
        {
            VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);
            return MBSM_FAILURE;
        }

        MEMCPY (pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg, pProtoMsg,
                sizeof (tMbsmProtoMsg));
    }

    if (VLAN_SEND_TO_QUEUE (VLAN_CFG_QUEUE_ID,
                            (UINT1 *) &pVlanQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                       (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                       VLAN_NAME, " Send To Q failed for  Vlan Config Message "
                       "Type %u \r\n", VLAN_REMOVE_PORT_INFO_MSG);

        if (pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg != NULL)
        {
            MEM_FREE (pVlanQMsg->MbsmCardUpdate.pMbsmProtoMsg);
        }

        VLAN_RELEASE_BUF (VLAN_QMSG_BUFF, pVlanQMsg);

        return MBSM_FAILURE;
    }

    if (VLAN_SEND_EVENT (VLAN_TASK_ID, VLAN_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        VLAN_TRC_ARG1 (VLAN_MOD_TRC,
                       (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                       VLAN_NAME, " Send Event failed for  Vlan Config Message "
                       "Type %u \r\n", VLAN_REMOVE_PORT_INFO_MSG);

        /* No Need to Free Message Sice it is already Queued */
        return MBSM_FAILURE;
    }
    
    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                              "VlanMbsmPostMessage enqueued packets and returns success \n");

    return MBSM_SUCCESS;
}

/*****************************************************************************
 * Function Name : VlanMbsmUpdtVlanTblForLoadSharing 
 * Description   : Function to update the port bitmaps of Vlan table.
 *                 HiGig pbmp of ports connecting to Slave CFM is removed 
 *                 in Vlan table to avoid looping of packets when load-sharing
 *                 is enabled.
 * Input(s)      : u1Flag - Load sharing flag 
 * Output(s)     : None  
 * Returns       : MBSM_SUCCESS/MBSM_FAILURE 
 *****************************************************************************/
INT4
VlanMbsmUpdtVlanTblForLoadSharing (UINT1 u1Flag)
{
    tVlanId             VlanId;
    tVlanId             NextVlanId;

    UNUSED_PARAM (u1Flag);

    /* Scan thru' s/w vlan tbl to get the vlan ids of each
     * vlan entry. */
    for (VlanId = VLAN_START_VLAN_INDEX ();
         VlanId != VLAN_INVALID_VLAN_INDEX; VlanId = NextVlanId)
    {
        NextVlanId = VLAN_GET_NEXT_VLAN_INDEX (VlanId);
    }

    return (MBSM_SUCCESS);
}

/*****************************************************************************
 * Function Name : VlanMbsmUpdtL2McForLoadSharing 
 * Description   : Function to update the port bitmaps of L2 multicast table,
 *                 when Load Sharing is enabled.
 *                 HiGig pbmp of ports connecting to Active CFM is removed 
 *                 in L2MC table to avoid looping of packets when load-sharing
 *                 is enabled.
 * Input(s)      : u1Flag - LoadSharingFlag 
 * Output(s)     : None  
 * Returns       : MBSM_SUCCESS/MBSM_FAILURE 
 *****************************************************************************/
INT4
VlanMbsmUpdtL2McForLoadSharing (UINT1 u1Flag)
{
    tVlanId             VlanId;
    tVlanId             NextVlanId;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanStMcastEntry  *pStMcastEntry = NULL;
    tVlanStMcastEntry  *pNextStMcastEntry = NULL;
    tVlanGroupEntry    *pGroupEntry = NULL;
    tVlanGroupEntry    *pNextGroupEntry = NULL;

    UNUSED_PARAM (u1Flag);

    for (VlanId = VLAN_START_VLAN_INDEX ();
         VlanId != VLAN_INVALID_VLAN_INDEX; VlanId = NextVlanId)
    {
        pCurrEntry = VlanGetVlanEntry (VlanId);
        NextVlanId = VLAN_GET_NEXT_VLAN_INDEX (VlanId);

        /* Get the static Mcast entries for this VLAN  and
         * update the member ports for load-sharing */
        pStMcastEntry = pCurrEntry->pStMcastTable;

        while (pStMcastEntry != NULL)
        {
            pNextStMcastEntry = pStMcastEntry->pNextNode;

            pStMcastEntry = pNextStMcastEntry;
        }

        /* Get the Group table entries for this VLAN  and
         * update the member ports for load-sharing */
        pGroupEntry = pCurrEntry->pGroupTable;

        while (pGroupEntry != NULL)
        {
            pNextGroupEntry = pGroupEntry->pNextNode;

            pGroupEntry = pNextGroupEntry;
        }
    }
    return (MBSM_SUCCESS);
}

INT4
VlanMbsmSetPortTunnelModeToHw (tMbsmPortInfo * pPortInfo,
                               tLocalPortList LocalPorts)
{
    UINT2               u2ActPortNo;
    INT4                i4RetVal;
    UINT4               u4TunnelMode;
    UINT4               u4Port;
    UINT4               u4ContextId;
    UINT2               u2LaAggIndex;
    UINT1               u1Result;

    UNUSED_PARAM (pPortInfo);

    if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
    {
        u4TunnelMode = VLAN_NO_TUNNEL_PORT;
    }
    else
    {
        VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC, VLAN_NAME,
                  "Increase system MTU by 4 bytes to support tunneling \n");
        u4TunnelMode = VLAN_TUNNEL_INTERNAL;
    }

    VLAN_SCAN_PORT_TABLE (u4Port)
    {
        VLAN_IS_MEMBER_PORT (LocalPorts, u4Port, u1Result);

        if (u1Result == VLAN_TRUE)
        {
            if (L2IWF_SUCCESS == VlanL2IwfIsPortInPortChannel
                (VLAN_GET_PHY_PORT (u4Port)))
            {
                VlanL2IwfGetPortChannelForPort (VLAN_GET_PHY_PORT (u4Port),
                                                &u2LaAggIndex);
                /* Get The Local Port Number for Portchannel */
                VlanGetContextInfoFromIfIndex ((UINT4) u2LaAggIndex,
                                               &u4ContextId, &u2ActPortNo);
            }
            else
            {
                u2ActPortNo = (UINT2) u4Port;
            }

            if (VLAN_BRIDGE_MODE () != VLAN_CUSTOMER_BRIDGE_MODE)
            {
                if (VLAN_TUNNEL_STATUS (VLAN_GET_PORT_ENTRY (u2ActPortNo))
                    == VLAN_ENABLED)
                {
                    u4TunnelMode = VLAN_TUNNEL_EXTERNAL;
                }
                else
                {
                    /* Bridge is running in Provider network. 
                     * By default tunnelling is disabled
                     * on all ports. */
                    u4TunnelMode = VLAN_TUNNEL_INTERNAL;
                }
            }

            i4RetVal = VlanFsMiVlanHwSetPortTunnelMode
                (VLAN_CURR_CONTEXT_ID (),
                 VLAN_GET_PHY_PORT (u2ActPortNo), u4TunnelMode);

            if (i4RetVal == FNP_FAILURE)
            {
                VLAN_TRC_ARG1 (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                               VLAN_NAME,
                               "Setting Tunnel mode failed for Port - %d \n",
                               u2ActPortNo);
                return MBSM_FAILURE;
            }
        }
    }
    return MBSM_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanMbsmUpdatePortLearningStatus                     */
/*                                                                         */
/* Description      : This function configures Port Learning status in hw  */
/*                                                                         */
/* Input(s)         : u2PortId - Port Index                                */
/*                                                                         */
/*                    i4VlanFilteringUtilityCriteria - Filtering criteria  */
/*                    to be configured.                                    */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanMbsmUpdatePortLearningStatus (tMbsmSlotInfo * pSlotInfo,
                                  UINT2 u2PortId,
                                  INT4 i4VlanFilteringUtilityCriteria)
{
    tVlanFidEntry      *pFidEntry = NULL;
    UINT4               u4FidIndex;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT1               u1VlanLearningType;
    UINT1               u1Result = VLAN_FALSE;

    u1VlanLearningType = VlanGetVlanLearningMode ();

    if (u1VlanLearningType == VLAN_SHARED_LEARNING)
    {
        pFidEntry = VLAN_GET_FID_ENTRY (VLAN_DEF_FID_INDEX);

        VLAN_IS_MAC_LEARN_PORT (pFidEntry, u2PortId, u1Result);

        if (i4VlanFilteringUtilityCriteria == VLAN_ENHANCE_FILTERING_CRITERIA)
        {
            if (u1Result == VLAN_TRUE)
            {
                i4RetVal = VlanFsMiVlanMbsmHwSetFidPortLearningStatus
                    (VLAN_CURR_CONTEXT_ID (), VLAN_DEF_FID_INDEX,
                     VLAN_GET_PHY_PORT (u2PortId), VLAN_ENABLED, pSlotInfo);
            }
            else
            {
                i4RetVal = VlanFsMiVlanMbsmHwSetFidPortLearningStatus
                    (VLAN_CURR_CONTEXT_ID (), VLAN_DEF_FID_INDEX,
                     VLAN_GET_PHY_PORT (u2PortId), VLAN_DISABLED, pSlotInfo);

            }
        }
    }
    else
    {
        for (u4FidIndex = 0; u4FidIndex < VLAN_MAX_FID_ENTRIES; u4FidIndex++)
        {
            pFidEntry = VLAN_GET_FID_ENTRY (u4FidIndex);

            if (pFidEntry != NULL)
            {
                VLAN_IS_MAC_LEARN_PORT (pFidEntry, u2PortId, u1Result);
                if (i4VlanFilteringUtilityCriteria
                    == VLAN_ENHANCE_FILTERING_CRITERIA)
                {
                    if (u1Result == VLAN_TRUE)
                    {
                        i4RetVal = VlanFsMiVlanMbsmHwSetFidPortLearningStatus
                            (VLAN_CURR_CONTEXT_ID (), u4FidIndex,
                             VLAN_GET_PHY_PORT (u2PortId), VLAN_ENABLED,
                             pSlotInfo);
                    }
                    else
                    {
                        i4RetVal = VlanFsMiVlanMbsmHwSetFidPortLearningStatus
                            (VLAN_CURR_CONTEXT_ID (), u4FidIndex,
                             VLAN_GET_PHY_PORT (u2PortId), VLAN_DISABLED,
                             pSlotInfo);

                    }
                }
            }
        }

    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmUpdtWildCardTblToHw                      */
/*                                                                           */
/*    Description         : This function walks the wildd card table and     */
/*                          programs the Hardware with entries present for   */
/*                          given card slot                                  */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                          SlotLocalPorts - Local Portlist of slot which    */
/*                          is inserted now and mapped to current context    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdtWildCardTblToHw (tMbsmPortInfo * pMBSMPortInfo,
                             tMbsmSlotInfo * pSlotInfo,
                             tLocalPortList SlotLocalPorts)
{
    tVlanPortEntry     *pPortEntry;
    tVlanWildCardEntry *pWildCardEntry = NULL;
    tLocalPortList      HwPorts;
    tLocalPortList      NullPortList;
    tMbsmSlotInfo       ProgSlotInfo;
    tMacAddr            ConnectionId;
    UINT2               u2PortIndex = 0;
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    UNUSED_PARAM (pMBSMPortInfo);

    VLAN_MEMSET (NullPortList, 0, sizeof (tLocalPortList));

    VLAN_SLL_SCAN (&gpVlanContextInfo->WildCardTable, pWildCardEntry,
                   tVlanWildCardEntry *)
    {
        MEMSET (HwPorts, 0, sizeof (tLocalPortList));
        MEMSET (ConnectionId, 0, sizeof (tMacAddr));
        MEMCPY (HwPorts, pWildCardEntry->EgressPorts, sizeof (tLocalPortList));

        if (VLAN_IS_MCASTADDR (pWildCardEntry->MacAddr) == VLAN_TRUE)
        {
            if (FsMiWrVlanMbsmHwAddStMcastEntry
                (VLAN_CURR_CONTEXT_ID (), VLAN_WILD_CARD_ID,
                 pWildCardEntry->MacAddr, VLAN_DEF_RECVPORT,
                 HwPorts, pSlotInfo) == FNP_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtWildCardTblToHw : "
  	  	                              "FsMiWrVlanMbsmHwAddStMcastEntry Failed \n");
                return MBSM_FAILURE;
            }

        }
        else
        {
            if (FsMiWrVlanMbsmHwAddStaticUcastEntry
                (VLAN_CURR_CONTEXT_ID (), VLAN_WILD_CARD_ID,
                 pWildCardEntry->MacAddr, VLAN_DEF_RECVPORT,
                 HwPorts, VLAN_PERMANENT, pSlotInfo,
                 ConnectionId) == FNP_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtWildCardTblToHw : "
  	  	                              "FsMiWrVlanMbsmHwAddStaticUcastEntry Failed \n");
                return MBSM_FAILURE;
            }
        }

        /* Masking out the ports of other slots */
        VLAN_AND_PORT_LIST (HwPorts, SlotLocalPorts);

        if (MEMCMP (HwPorts, NullPortList, sizeof (tLocalPortList)) == 0)
        {
            continue;
        }

        MEMCPY (&ProgSlotInfo, pSlotInfo, sizeof (tMbsmSlotInfo));

        ProgSlotInfo.i4SlotId = MBSM_SLOT_ALL;

        for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
        {
            u1PortFlag = HwPorts[u2ByteInd];

            for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & VLAN_BIT8) != 0)
                {
                    u2PortIndex =
                        (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                 u2BitIndex + 1);
                    OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS
                                             (pMBSMPortInfo), u2PortIndex,
                                             sizeof (tPortList),
                                             u1IsSetInPortListStatus);

                    if (OSIX_FALSE == u1IsSetInPortListStatus)
                    {
                        pPortEntry = VLAN_GET_PORT_ENTRY (u2PortIndex);

                        if (pPortEntry != NULL)
                        {
                            if (VLAN_IS_MCASTADDR (pWildCardEntry->MacAddr)
                                == VLAN_TRUE)
                            {
                                if (VlanFsMiVlanMbsmHwSetMcastPort
                                    (VLAN_CURR_CONTEXT_ID (), VLAN_WILD_CARD_ID,
                                     pWildCardEntry->MacAddr,
                                     VLAN_GET_PHY_PORT (u2PortIndex),
                                     &ProgSlotInfo) == FNP_FAILURE)
                                {
                                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtWildCardTblToHw : "
  	  	                                      "VlanFsMiVlanMbsmHwSetMcastPort Failed \n");
                                    return MBSM_FAILURE;
                                }
                            }
                            else
                            {
                                if (VlanFsMiVlanMbsmHwSetUcastPort
                                    (VLAN_CURR_CONTEXT_ID (), VLAN_WILD_CARD_ID,
                                     pWildCardEntry->MacAddr, VLAN_DEF_RECVPORT,
                                     VLAN_GET_PHY_PORT (u2PortIndex),
                                     VLAN_PERMANENT,
                                     &ProgSlotInfo) == FNP_FAILURE)
                                {
                                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtWildCardTblToHw : "
  	  	                                      "VlanFsMiVlanMbsmHwSetUcastPort Failed \n");
                                    return MBSM_FAILURE;
                                }

                            }
                        }
                    }            /* end MBSM_FALSE == u1RetPortListStatus */
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }

    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmResetWildCardTbl                         */
/*                                                                           */
/*    Description         : This function walks the Wild card table and      */
/*                          removes the entry from the hardware              */
/*                          In distributed data plane case the entry is      */
/*                          removed from all the other slots.                */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                          LocalPorts - Local Portlist of current context   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmResetWildCardTbl (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo,
                          tLocalPortList LocalPorts)
{
    tLocalPortList      NullPortList;
    tLocalPortList      HwPorts;
    tVlanWildCardEntry *pWildCardEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    tMbsmSlotInfo       ProgSlotInfo;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2Port;
    UINT1               u1PortFlag;

    UNUSED_PARAM (pPortInfo);

    MEMSET (NullPortList, 0, sizeof (tLocalPortList));

    MEMCPY (&ProgSlotInfo, pSlotInfo, sizeof (tMbsmSlotInfo));

    VLAN_SLL_SCAN (&gpVlanContextInfo->WildCardTable, pWildCardEntry,
                   tVlanWildCardEntry *)
    {
        MEMSET (HwPorts, 0, sizeof (tLocalPortList));
        MEMCPY (HwPorts, pWildCardEntry->EgressPorts, sizeof (tLocalPortList));

        VLAN_AND_PORT_LIST (HwPorts, LocalPorts);

        if (MEMCMP (HwPorts, NullPortList, sizeof (tLocalPortList)) == 0)
        {
            continue;
        }

        ProgSlotInfo.i4SlotId = MBSM_SLOT_ALL;

        for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
        {
            u1PortFlag = HwPorts[u2ByteInd];
            for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & VLAN_BIT8) != 0)
                {
                    u2Port =
                        (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                 u2BitIndex + 1);

                    pPortEntry = VLAN_GET_PORT_ENTRY (u2Port);

                    if (pPortEntry != NULL)
                    {
                        if (VLAN_IS_MCASTADDR (pWildCardEntry->MacAddr)
                            == VLAN_TRUE)
                        {
                            VlanFsMiVlanMbsmHwResetMcastPort
                                (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_WILD_CARD_ID,
                                 pWildCardEntry->MacAddr,
                                 VLAN_GET_PHY_PORT (u2Port), &ProgSlotInfo);
                        }
                        else
                        {

                            VlanFsMiVlanMbsmHwResetUcastPort
                                (VLAN_CURR_CONTEXT_ID (), VLAN_WILD_CARD_ID,
                                 pWildCardEntry->MacAddr, VLAN_DEF_RECVPORT,
                                 VLAN_GET_PHY_PORT (u2Port), &ProgSlotInfo);
                        }
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }

    return MBSM_SUCCESS;
}

INT4
VlanMbsmGetProtocolTunnelStatus (UINT1 u1ProtocolId, UINT2 u2PortNum,
                                 UINT1 *pu1Tunnelstatus)
{
    tVlanPortEntry     *pPortEntry = NULL;

    /*NULL chech for this port entry is not needed here.
       since it was already checked before calling this function */
    pPortEntry = VLAN_GET_PORT_ENTRY (u2PortNum);

    switch (u1ProtocolId)
    {
        case VLAN_NP_GMRP_PROTO_ID:

            *pu1Tunnelstatus = VLAN_GMRP_TUNNEL_STATUS (pPortEntry);
            break;
        case VLAN_NP_GVRP_PROTO_ID:

            *pu1Tunnelstatus = VLAN_GVRP_TUNNEL_STATUS (pPortEntry);
            break;
        case VLAN_NP_LACP_PROTO_ID:

            *pu1Tunnelstatus = VLAN_LACP_TUNNEL_STATUS (pPortEntry);
            break;
        case VLAN_NP_STP_PROTO_ID:

            *pu1Tunnelstatus = VLAN_STP_TUNNEL_STATUS (pPortEntry);
            break;
        case VLAN_NP_DOT1X_PROTO_ID:

            *pu1Tunnelstatus = VLAN_DOT1X_TUNNEL_STATUS (pPortEntry);
            break;
        case VLAN_NP_IGMP_PROTO_ID:

            *pu1Tunnelstatus = VLAN_IGMP_TUNNEL_STATUS (pPortEntry);
            break;
        case VLAN_NP_MMRP_PROTO_ID:

            *pu1Tunnelstatus = VLAN_MMRP_TUNNEL_STATUS (pPortEntry);
            break;
        case VLAN_NP_MVRP_PROTO_ID:

            *pu1Tunnelstatus = VLAN_MVRP_TUNNEL_STATUS (pPortEntry);
            break;

        default:
            return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/**************************************************************************/
/* Function Name       : VlanMbsmGetPortListForCurrContext                */
/*                                                                        */
/* Description         : This function extracts the Portlist (local) from */
/*                       the given Portlist (IfIndex based) for the       */
/*                       current context.                                 */
/*                                                                        */
/* Input(s)            : IfPortList    - IfIndex based Portlist           */
/*                                                                        */
/* Output(s)           : LocalPortList - Local port list                  */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/
VOID
VlanMbsmGetPortListForCurrContext (tMbsmPortInfo * pPortInfo,
                                   tLocalPortList LocalPortList)
{
    UINT2               au2SispPorts[SYS_DEF_MAX_NUM_CONTEXTS];
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT4               u4Port = VLAN_INIT_VAL;
    UINT4               u4ByteIndex = VLAN_INIT_VAL;
    UINT4               u4PortCnt = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    UINT2               u2BitIndex = VLAN_INIT_VAL;
    UINT1               u1PortFlag = VLAN_INIT_VAL;

    /* The local port list should be set in any one of the following two
     * conditions
     * 1) Port present in the attached slot is mapped to this context
     * 2) Any SISP logical interface running over this port is present in
     *    this context.
     * */

    VLAN_MEMSET (au2SispPorts, VLAN_INIT_VAL, sizeof (au2SispPorts));

    for (u4ByteIndex = 0; u4ByteIndex < BRG_PORT_LIST_SIZE; u4ByteIndex++)
    {
        u1PortFlag = pPortInfo->PortList[u4ByteIndex];

        if (u1PortFlag == 0)
        {
            continue;
        }

        for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) != 0)
            {
                u4Port = (UINT4) ((u4ByteIndex * VLAN_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                if (VlanGetContextInfoFromIfIndex (u4Port,
                                                   &u4ContextId,
                                                   &u2LocalPortId) ==
                    VLAN_SUCCESS)
                {
                    if (u4ContextId == VLAN_CURR_CONTEXT_ID ())
                    {
                        VLAN_SET_MEMBER_PORT (LocalPortList, u2LocalPortId);
                    }
                }

                if (VlanVcmSispGetSispPortsInfoOfPhysicalPort (u4Port, VCM_TRUE,
                                                               (VOID *)
                                                               au2SispPorts,
                                                               &u4PortCnt) ==
                    VCM_SUCCESS)
                {
                    if (au2SispPorts[VLAN_CURR_CONTEXT_ID ()] != 0)
                    {
                        VLAN_SET_MEMBER_PORT (LocalPortList,
                                              au2SispPorts[VLAN_CURR_CONTEXT_ID
                                                           ()]);
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);

        }                        /*Bit Index */

    }                            /* ByteIndex */
}

/**************************************************************************/
/* Function Name       : VlanMbsmProgramDynamicFdbEntry                   */
/*                                                                        */
/* Description         : This function programs the dynamically learnt    */
/*                       FDB entry on receiving the MBSM Card insert event*/
/*                                                                        */
/*    Input(s)            : pSlotInfo - Information about inserted slot   */
/*                                                                        */
/*    Output(s)           : None                                          */
/*                                                                        */
/*    Global Variables Referred : None                                    */
/*                                                                        */
/*    Global Variables Modified : None.                                   */
/*                                                                        */
/*    Exceptions or Operating                                             */
/*    System Error Handling    : None.                                    */
/*                                                                        */
/*    Use of Recursion        : None.                                     */
/*                                                                        */
/*    Returns            : MBSM_SUCCESS on success                        */
/*                         MBSM_FAILURE on failure                        */
/**************************************************************************/
INT4
VlanMbsmProgramDynamicFdbEntry (tMbsmSlotInfo * pSlotInfo)
{
    tVlanFdbInfo        VlanFdbInfo;
    tFDBInfoArray       FDBInfoArray;
    INT4                i4FdbCount = 0;
#ifdef SW_LEARNING
    tVlanFdbInfo       *pVlanFdbInfo = NULL;
#else
    UINT4               u4NextContextId = 0;
    UINT4               u4CurrFdbId = 0;
    UINT4               u4NextFdbId = 0;
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    tVlanTempFdbInfo    VlanTempFdbInfo;
#endif

    MEMSET (&VlanFdbInfo, 0, sizeof (tVlanFdbInfo));
    MEMSET (&FDBInfoArray, 0, sizeof (tFDBInfoArray));
    if (gpVlanContextInfo == NULL)
    {
        return MBSM_SUCCESS;
    }

#ifdef SW_LEARNING
    while ((pVlanFdbInfo =
            (tVlanFdbInfo *) RBTreeGetNext (gpVlanContextInfo->VlanFdbInfo,
                                            (tRBElem *) & VlanFdbInfo,
                                            NULL)) != NULL)
    {
        MEMCPY (&VlanFdbInfo, pVlanFdbInfo, sizeof (tVlanFdbInfo));
        VLAN_CPY_MAC_ADDR (FDBInfoArray.aFDBInfoArray[i4FdbCount].MacAddr,
                           VlanFdbInfo.MacAddr);
        FDBInfoArray.aFDBInfoArray[i4FdbCount].u4FdbId = VlanFdbInfo.u4FdbId;
        FDBInfoArray.aFDBInfoArray[i4FdbCount].u4ContextId =
            VLAN_CURR_CONTEXT_ID ();
        FDBInfoArray.aFDBInfoArray[i4FdbCount].u4Port =
            (UINT4) VLAN_GET_IFINDEX (VlanFdbInfo.u2Port);
        FDBInfoArray.aFDBInfoArray[i4FdbCount].u1EntryType =
            VlanFdbInfo.u1EntryType;

        if (VlanFdbInfo.u1EntryType != VLAN_FDB_LEARNT)
        {
            continue;
        }

        i4FdbCount++;
        if (i4FdbCount == MAX_FDB_INFO)
        {
            FDBInfoArray.u4FdbEntryCount = i4FdbCount;
            /* Copied the MAX FDB INFO, hence invoke the MBSM NPAPI */
            if (VlanFsMiVlanMbsmSyncFDBInfo (&FDBInfoArray, pSlotInfo) ==
                FNP_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                          "Card Insertion: "
                          "VlanFsMiVlanMbsmSyncFDBInfo Failed \n");
                return MBSM_FAILURE;
            }
            i4FdbCount = 0;
            MEMSET (&FDBInfoArray, 0, sizeof (tFDBInfoArray));
        }
    }
#else
    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));

    if (VlanHwGetNextTpFdbEntry (VLAN_CURR_CONTEXT_ID (), u4CurrFdbId,
                                 CurrMacAddr, &u4NextContextId,
                                 &u4NextFdbId, NextMacAddr) != VLAN_SUCCESS)
    {
        /* There are no entries */
        return MBSM_SUCCESS;
    }

    do
    {
        u4CurrFdbId = u4NextFdbId;
        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));

        MEMSET (&VlanTempFdbInfo, 0, sizeof (tVlanTempFdbInfo));

        if (VlanGetFdbInfo (u4CurrFdbId, CurrMacAddr,
                            &VlanTempFdbInfo) == VLAN_FAILURE)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                      "Card Insertion: " "VlanGetFdbInfo Failed \n");
            continue;
        }

        VLAN_CPY_MAC_ADDR (FDBInfoArray.aFDBInfoArray[i4FdbCount].MacAddr,
                           NextMacAddr);
        FDBInfoArray.aFDBInfoArray[i4FdbCount].u4ContextId =
            VLAN_CURR_CONTEXT_ID ();
        FDBInfoArray.aFDBInfoArray[i4FdbCount].u4FdbId = u4NextFdbId;
        FDBInfoArray.aFDBInfoArray[i4FdbCount].u4Port =
            (UINT4) VLAN_GET_IFINDEX (VlanTempFdbInfo.u2Port);
        FDBInfoArray.aFDBInfoArray[i4FdbCount].u1EntryType =
            VlanTempFdbInfo.u1Status;

        i4FdbCount++;
        if (i4FdbCount == MAX_FDB_INFO)
        {
            FDBInfoArray.u4FdbEntryCount = i4FdbCount;
            /* Copied the MAX FDB INFO, hence invoke the MBSM NPAPI */
            if (VlanFsMiVlanMbsmSyncFDBInfo (&FDBInfoArray, pSlotInfo) ==
                FNP_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                          "Card Insertion: "
                          "VlanFsMiVlanMbsmSyncFDBInfo Failed \n");
                return MBSM_FAILURE;
            }
            i4FdbCount = 0;
            MEMSET (&FDBInfoArray, 0, sizeof (tFDBInfoArray));
        }

    }
    while (VlanHwGetNextTpFdbEntry (VLAN_CURR_CONTEXT_ID (), u4CurrFdbId,
                                    CurrMacAddr, &u4NextContextId, &u4NextFdbId,
                                    NextMacAddr) == VLAN_SUCCESS);
#endif
    if (i4FdbCount != 0)
    {
        FDBInfoArray.u4FdbEntryCount = (UINT4) i4FdbCount;
        if (VlanFsMiVlanMbsmSyncFDBInfo (&FDBInfoArray, pSlotInfo) ==
            FNP_FAILURE)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                      "Card Insertion: "
                      "VlanFsMiVlanMbsmSyncFDBInfo Failed \n");
            return MBSM_FAILURE;
        }
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSendAckToMbsm                                */
/*                                                                           */
/*    Description         : This function is used to send Ack to MBSM on     */
/*                          processing card attach event                     */
/*                                                                           */
/*    Input(s)            : i4RetStatus - Return value on processing         */
/*                                        the card attach event              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS / MBSM_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSendAckToMbsm (INT4 i4RetStatus)
{
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    VLAN_MEMSET (&MbsmProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));
    MbsmProtoAckMsg.i4ProtoCookie = gi4ProtoId;
    MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (&gMbsmSlotInfo);
    MbsmProtoAckMsg.i4RetStatus = i4RetStatus;
    if (i4RetStatus == MBSM_SUCCESS)
    {
    	MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
        	                      "VlanSendAckToMbsm sent Ack to MBSM and returns success \n");
    }
    return MbsmSendAckFromProto (&MbsmProtoAckMsg);

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmUpdtPortPropertiesToHw                   */
/*                                                                           */
/*    Description         : This function walks the vlan port table and      */
/*                          programs the Hardware with all the vlan port     */
/*                          properties like pvid, acceptable frame types etc */
/*                                                                           */
/*                          Batching is applied to this function .           */
/*                          This function configures all port properties     */
/*                          in loop and it takes more than 15 seconds for    */
/*                          completion. Vlan Lock is aquired for 15 seconds. */
/*                          This is causing huge delay in  ping to CPU as    */
/*                          CFA waits for VLAN Lock. So loop executes only   */
/*                          for 2 ports at a time . 10 ms timer is started   */
/*                          before configuring next set of ports */
/*                                                                           */
/*    Input(s)            : pMbsmPortInfo - Inserted port list               */
/*                          pSlotInfo - Information about inserted slot      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdtPortPropertiesToHw (tMbsmPortInfo * pMbsmPortInfo,
                                tMbsmSlotInfo * pSlotInfo)
{
    tFsNpVlanPortReflectEntry  PortReflectEntry;
    tLocalPortList      PortList;
    tVlanPortEntry     *pPortEntry = NULL;
    tVlanPortVidSet    *pPortVidSetEntry;
    tVlanSubnetMapEntry *pSubnetMapEntry;
    tVlanSubnetMapIndex CurrIndex;
    tLocalPortList      NullPortList;
    tMacAddr            MacAddr;
    tVlanHwPortInfo     VlanHwPortInfo;
    INT4                i4BridgeMode;
    INT4                i4BrgPortType = 0;
    UINT4               u4ContextId;
    UINT4               u4Count = 0;
    UINT4               u4PrevContextId = 0;
    UINT2               u2ByteIndex;
    UINT2               u2BitIndex;
    UINT2               u2Port;
    UINT2               u2PriIndex;
    UINT1               u1PortFlag;
    UINT1               u1Tunnelstatus;
    UINT1               u1ProtocolId;
    UINT2               u2PortNum;
    UINT2               u2LaAggIndex;
    UINT1               u1HwEnable;

    VLAN_MEMSET (NullPortList, 0, sizeof (tLocalPortList));
    VLAN_MEMSET (PortList, 0, sizeof (tLocalPortList));
    VLAN_MEMSET (&PortReflectEntry,0,sizeof (tFsNpVlanPortReflectEntry));

    if ((VlanSelectContext (gu4ContextIdProcessed) != VLAN_SUCCESS) ||
        (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE))
    {
        u4PrevContextId = gu4ContextIdProcessed;
        if (VlanGetNextActiveContext (u4PrevContextId,
                                      &gu4ContextIdProcessed) == VLAN_SUCCESS)
        {
            gu2ByteIndex = 0;
            gu2BitIndex = 0;
            gu1PortFlag = 0;
            gu1IsBatchingStarted = OSIX_FALSE;
            if (VlanSelectContext (gu4ContextIdProcessed) != VLAN_SUCCESS)
            {
                VlanSendAckToMbsm (MBSM_FAILURE);
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                              "VlanSelectContext Failed \n");
                return MBSM_FAILURE;
            }
            VLAN_START_TIMER (VLAN_MBSM_TIMER, VLAN_MBSM_STAGERRING_TIME);
        }
        else
        {
            VlanSendAckToMbsm (MBSM_SUCCESS);
        }
        VlanReleaseContext ();
        return MBSM_SUCCESS;
    }

    VlanMbsmGetPortListForCurrContext (pMbsmPortInfo, PortList);
    VlanGetPortChannelPortListForCurrContext (pMbsmPortInfo, PortList);

    /*
     *   Scan the vlan port table and program all the port
     *   properties for each port into the inserted card
     */

    if (MEMCMP (PortList, NullPortList, sizeof (tLocalPortList)) != 0)
    {
            for (u2ByteIndex = gu2ByteIndex; u2ByteIndex < VLAN_PORT_LIST_SIZE;
                 u2ByteIndex++)
            {
                if (gu1IsBatchingStarted == OSIX_FALSE)
                {
                    u1PortFlag = PortList[u2ByteIndex];
                    gu2BitIndex = 0;
                }
                else if ((gu2BitIndex == VLAN_PORTS_PER_BYTE) && (gu1PortFlag == 0))
                {
                    u1PortFlag = PortList[u2ByteIndex];
                    gu2BitIndex = 0;
                }
                else
                {
                    u1PortFlag = gu1PortFlag;
                }
                for (u2BitIndex = gu2BitIndex;
                     (u2BitIndex < VLAN_PORTS_PER_BYTE);
                     u2BitIndex++)
                {
                    if (u1PortFlag != 0)
                    {
                        if ((u1PortFlag & VLAN_BIT8) != 0)
                        {
                            u2Port = (UINT2) ((u2ByteIndex * VLAN_PORTS_PER_BYTE) +
                                              u2BitIndex + 1);
                            if ((VLAN_GET_PORT_ENTRY (u2Port) != NULL)
                                && (L2IWF_SUCCESS ==
                                    VlanL2IwfIsPortInPortChannel (VLAN_GET_PHY_PORT
                                                                  (u2Port))))
                            {
                                VlanL2IwfGetPortChannelForPort (VLAN_GET_PHY_PORT
                                                                (u2Port),
                                                                &u2LaAggIndex);
                                /* Get The Local Port Number for Portchannel */
                                VlanGetContextInfoFromIfIndex ((UINT4) u2LaAggIndex,
                                                               &u4ContextId,
                                                               &u2PortNum);
                            }
                            else
                            {
                                u2PortNum = u2Port;
                            }

                            pPortEntry = VLAN_GET_PORT_ENTRY (u2PortNum);

                            if (pPortEntry == NULL)
                            {
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                                gu1PortFlag = u1PortFlag;
                                continue;
                            }
                            
                            PortReflectEntry.u4Status=(UINT4)pPortEntry->i4ReflectionStatus;
                            PortReflectEntry.u4IfIndex =(UINT4) VLAN_GET_PHY_PORT (u2PortNum);
                            PortReflectEntry.u4ContextId = (UINT4) VLAN_CURR_CONTEXT_ID ();
                             
                                
                            if (VlanFsMiVlanMbsmHwSetPortPvid
                                (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_GET_PHY_PORT (u2PortNum), pPortEntry->Pvid,
                                 pSlotInfo) == FNP_FAILURE)
                            {
                                VlanSendAckToMbsm (MBSM_FAILURE);
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                              "VlanFsMiVlanMbsmHwSetPortPvid Failed \n");
                                return MBSM_FAILURE;
                            }
                            for (u1ProtocolId = 1; u1ProtocolId < VLAN_MAX_PROT_ID;
                                 u1ProtocolId++)
                            {
                                VlanMbsmGetProtocolTunnelStatus (u1ProtocolId,
                                                                 u2PortNum,
                                                                 &u1Tunnelstatus);

                                if (VlanFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort
                                    (VLAN_CURR_CONTEXT_ID (),
                                     VLAN_GET_PHY_PORT (u2PortNum), u1ProtocolId,
                                     u1Tunnelstatus, pSlotInfo) == FNP_FAILURE)
                                {
                                    VlanSendAckToMbsm (MBSM_FAILURE);
                                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                              "VlanFsMiVlanMbsmHwSetProtocolTunnelStatusOnPort Failed \n");
                                    return MBSM_FAILURE;
                                }
                            }
                            if (VlanFsMiVlanMbsmHwSetPortProtectedStatus
                                (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_GET_PHY_PORT (u2PortNum),
                                 pPortEntry->u1PortProtected,
                                 pSlotInfo) == FNP_FAILURE)
                            {
                                VlanSendAckToMbsm (MBSM_FAILURE);
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                              "VlanFsMiVlanMbsmHwSetPortProtectedStatus Failed \n");
                                return MBSM_FAILURE;
                            }
                            if (VlanFsMiVlanMbsmHwSetPortAccFrameType
                                (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_GET_PHY_PORT (u2PortNum),
                                 pPortEntry->u1AccpFrmTypes,
                                 pSlotInfo) == FNP_FAILURE)
                            {
                                VlanSendAckToMbsm (MBSM_FAILURE);
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                              "VlanFsMiVlanMbsmHwSetPortAccFrameType Failed \n");
                                return MBSM_FAILURE;
                            }
                            u1HwEnable =
                                (pPortEntry->u1IngFiltering ==
                                 VLAN_SNMP_TRUE) ? FNP_TRUE : FNP_FALSE;
                            if (VlanFsMiVlanMbsmHwSetPortIngFiltering
                                (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_GET_PHY_PORT (u2PortNum), u1HwEnable,
                                 pSlotInfo) == FNP_FAILURE)
                            {
                                VlanSendAckToMbsm (MBSM_FAILURE);
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                              "VlanFsMiVlanMbsmHwSetPortIngFiltering Failed \n");
                                return MBSM_FAILURE;
                            }
                            if (VlanFsMiVlanMbsmHwSetDefUserPriority
                                (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_GET_PHY_PORT (u2PortNum),
                                 pPortEntry->u1DefUserPriority,
                                 pSlotInfo) == FNP_FAILURE)
                            {
                                VlanSendAckToMbsm (MBSM_FAILURE);
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                              "VlanFsMiVlanMbsmHwSetDefUserPriority Failed \n");
                                return MBSM_FAILURE;
                            }
                            if (VlanFsMiVlanMbsmHwSetPortNumTrafClasses
                                (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_GET_PHY_PORT (u2PortNum),
                                 pPortEntry->u1NumTrfClass,
                                 pSlotInfo) == FNP_FAILURE)
                            {
                                VlanSendAckToMbsm (MBSM_FAILURE);
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                              "VlanFsMiVlanMbsmHwSetPortNumTrafClasses Failed \n");
                                return MBSM_FAILURE;
                            }
                            for (u2PriIndex = 0; u2PriIndex < VLAN_MAX_PRIORITY;
                                 u2PriIndex++)
                            {
                                if ((pPortEntry->u1IfType !=
                                     VLAN_ETHERNET_INTERFACE_TYPE)
                                    && (pPortEntry->u1IfType !=
                                        VLAN_LAGG_INTERFACE_TYPE))
                                {
                                    if (VlanFsMiVlanMbsmHwSetRegenUserPriority
                                        (VLAN_CURR_CONTEXT_ID (),
                                         VLAN_GET_PHY_PORT (u2PortNum),
                                         (INT4) u2PriIndex,
                                         (INT4) pPortEntry->
                                         au1PriorityRegen[u2PriIndex],
                                         pSlotInfo) == FNP_FAILURE)
                                    {
                                        VlanSendAckToMbsm (MBSM_FAILURE);
                                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                               "VlanFsMiVlanMbsmHwSetRegenUserPriority Failed \n");
                                        return MBSM_FAILURE;
                                    }
                                }
                                if (VlanFsMiVlanMbsmHwSetTraffClassMap
                                    (VLAN_CURR_CONTEXT_ID (),
                                     VLAN_GET_PHY_PORT (u2PortNum),
                                     (INT4) u2PriIndex,
                                     (INT4) pPortEntry->au1TrfClassMap[u2PriIndex],
                                     pSlotInfo) == FNP_FAILURE)
                                {
                                    VlanSendAckToMbsm (MBSM_FAILURE);
                                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                               "VlanFsMiVlanMbsmHwSetTraffClassMap Failed \n");
                                    return MBSM_FAILURE;
                                }
                            }
                            VLAN_SLL_SCAN (&pPortEntry->VlanVidSet,
                                           pPortVidSetEntry, tVlanPortVidSet *)
                            {
                                if (pPortVidSetEntry->u1RowStatus == VLAN_ACTIVE)
                                {
                                    VlanUpdateGroupVlanMapInfoInHw ((UINT4)
                                                                    u2PortNum,
                                                                    (UINT4)
                                                                    pPortVidSetEntry->
                                                                    u4ProtGrpId,
                                                                    pPortVidSetEntry->
                                                                    VlanId,
                                                                    VLAN_ADD);
                                }
                            }

                            if (VLAN_FAILURE ==
                                VlanUpdatePortMacVlanInfoInHw (VLAN_CURR_CONTEXT_ID
                                                               (), u2PortNum))
                            {
                                VlanSendAckToMbsm (MBSM_FAILURE);
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                               "VlanUpdatePortMacVlanInfoInHw Failed \n");
                                return MBSM_FAILURE;
                            }

                            /* Enable Subnet based VLAN classification in hardware */
                            if (VLAN_PORT_SUBNET_BASED (u2PortNum) == VLAN_ENABLED)
                            {
                                u1HwEnable = FNP_TRUE;
                            }
                            else
                            {
                                u1HwEnable = FNP_FALSE;
                            }

                            if (VlanFsMiVlanMbsmHwSetSubnetBasedStatusOnPort
                                (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_GET_PHY_PORT (u2PortNum), u1HwEnable,
                                 pSlotInfo) != FNP_SUCCESS)
                            {
                                VlanSendAckToMbsm (MBSM_FAILURE);
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                               "VlanFsMiVlanMbsmHwSetSubnetBasedStatusOnPort Failed \n");
                                return MBSM_FAILURE;
                            }

                            /* Program Subnet based VLAN classification in hardware */
                            pSubnetMapEntry = VlanSubnetMapTableGetFirstEntryOnPort
                                (VLAN_GET_PHY_PORT (u2PortNum));

                            while (pSubnetMapEntry != NULL)
                            {
                                if (pSubnetMapEntry->u4Port ==
                                    VLAN_GET_PHY_PORT (u2PortNum))
                                {
                                    if (VlanFsMiVlanMbsmHwAddPortSubnetVlanEntry
                                        (VLAN_CURR_CONTEXT_ID (),
                                         VLAN_GET_PHY_PORT (u2PortNum),
                                         pSubnetMapEntry->SubnetAddr,
                                         pSubnetMapEntry->VlanId,
                                         pSubnetMapEntry->u1ArpOption,
                                         pSlotInfo) != FNP_SUCCESS)
                                    {
                                        VlanSendAckToMbsm (MBSM_FAILURE);
                                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                                     "VlanFsMiVlanMbsmHwAddPortSubnetVlanEntry Failed \n");
                                        return MBSM_FAILURE;
                                    }
                                }

                                CurrIndex.u4Port = pSubnetMapEntry->u4Port;
                                CurrIndex.SubnetAddr = pSubnetMapEntry->SubnetAddr;

                                pSubnetMapEntry =
                                    VlanSubnetMapTableGetNextEntryOnPort
                                    (CurrIndex);
                            }

                            if (VLAN_PORT_PORT_PROTOCOL_BASED (u2PortNum) ==
                                VLAN_ENABLED)
                            {
                                u1HwEnable = FNP_TRUE;
                            }
                            else
                            {
                                u1HwEnable = FNP_FALSE;
                            }

                            if (VlanFsMiVlanMbsmHwEnableProtoVlanOnPort
                                (VLAN_CURR_CONTEXT_ID (),
                                 VLAN_GET_PHY_PORT (u2PortNum), u1HwEnable,
                                 pSlotInfo) != FNP_SUCCESS)
                            {
                                VlanSendAckToMbsm (MBSM_FAILURE);
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                                    "VlanFsMiVlanMbsmHwEnableProtoVlanOnPort Failed \n");
                                return MBSM_FAILURE;
                            }
                            if (VlanMbsmUpdatePortLearningStatus
                                (pSlotInfo, u2PortNum,
                                 pPortEntry->u2FiltUtilityCriteria) != FNP_SUCCESS)
                            {
                                VlanSendAckToMbsm (MBSM_FAILURE);
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                                    "VlanMbsmUpdatePortLearningStatus Failed \n");
                                return MBSM_FAILURE;
                            }
                            
                            if (VlanFsMiVlanMbsmHwPortPktReflectStatus
                                 (&PortReflectEntry) != FNP_SUCCESS)
                            {
                                VlanSendAckToMbsm (MBSM_FAILURE);
                                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanFsMiVlanMbsmHwPortPktReflectStatus: "
                                            "VlanFsMiVlanMbsmHwPortPktReflectStatus Failed \n");
                                return MBSM_FAILURE;

                            } 
  
                            if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
                            {
                                if (VlanMbsmUpdtPbPortPropertiesToHw
                                    (pSlotInfo, u2PortNum) != MBSM_SUCCESS)
                                {
                                    VlanSendAckToMbsm (MBSM_FAILURE);
                                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, 
                                    "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                            "VlanMbsmUpdtPbPortPropertiesToHw Failed\n");
                                    return MBSM_FAILURE;
                                }
                            }
                            VlanCfaGetInterfaceBrgPortType (VLAN_GET_PHY_PORT (u2PortNum) , 
                                                            &i4BrgPortType);
                            if (i4BrgPortType == CFA_UPLINK_ACCESS_PORT)
                            {
                                VlanHwPortInfo.u4IfIndex = VLAN_GET_PHY_PORT (u2PortNum);
                                VlanHwPortInfo.u1BrgPortType = CFA_UPLINK_ACCESS_PORT;

                                /* VlanFsMiVlanMbsmHwSetBridgePortType will be invoked only for RemoteNPPorts */
                                if (VlanFsMiVlanMbsmHwSetBridgePortType (&VlanHwPortInfo, pSlotInfo)  
                                        == FNP_FAILURE)
                                {
                                    VlanSendAckToMbsm (MBSM_FAILURE);
                                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, 
                                    "VlanMbsmUpdtPortPropertiesToHw: "
  	  	                            "VlanHwSetBridgePortType Failed\n");
                                    return MBSM_FAILURE;
                                }
                            }

                        }
                        u4Count++;
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                        gu1PortFlag = u1PortFlag;
                        /* If number of ports configured reaches Max count,
                           start a timer and return from the function */

                        /* This is done to avoid VLAN lock being acquired for 
                           long time */
                        if (u4Count == VLAN_MBSM_MAX_PORTS_CFG)
                        {
                            /* This function manipulates portlist byte by byte */
                            /* Before returning from function , store the 
                               bits and bytes  processed till now . */
                            gu1IsBatchingStarted = OSIX_TRUE;
                            u4Count = 0;
                            if (u2BitIndex == (VLAN_PORTS_PER_BYTE - 1))
                            {
                                u2ByteIndex++;
                                gu2BitIndex = 0;
                                gu1PortFlag = PortList[u2ByteIndex];
                            }
                            else
                            {
                                u2BitIndex++;
                                gu2BitIndex = u2BitIndex;
                            }
                            gu2ByteIndex = u2ByteIndex;
                            VLAN_START_TIMER (VLAN_MBSM_TIMER,
                                              VLAN_MBSM_STAGERRING_TIME);
                            return MBSM_SUCCESS;
                        }
                    }
                }
                gu2BitIndex = u2BitIndex;
            }
        if (VlanMbsmSetPortTunnelModeToHw
            (pMbsmPortInfo, PortList) == MBSM_FAILURE)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw : "
				"VlanMbsmSetPortTunnelModeToHw Failed \n");
            VlanReleaseContext ();
            VlanSendAckToMbsm (MBSM_FAILURE);
            return MBSM_FAILURE;
        }
    }

    i4BridgeMode = VLAN_BRIDGE_MODE ();
    if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {

        for (u1ProtocolId = 1; u1ProtocolId < VLAN_MAX_PROT_ID; u1ProtocolId++)
        {
            VLAN_MEMSET (MacAddr, 0, sizeof (tMacAddr));

            VlanTunnelProtocolMac (u1ProtocolId, MacAddr);

            if (VlanFsMiVlanMbsmHwSetTunnelFilter
                (VLAN_CURR_CONTEXT_ID (), i4BridgeMode,
                 MacAddr, u1ProtocolId, pSlotInfo) != FNP_SUCCESS)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw : "
					"VlanFsMiVlanMbsmHwSetTunnelFilter Failed \n");
                VlanReleaseContext ();
                VlanSendAckToMbsm (MBSM_FAILURE);
                return MBSM_FAILURE;

            }

        }

        if (VlanFsMiVlanMbsmHwSwitchMacLearningLimit
            (VLAN_CURR_CONTEXT_ID (), VLAN_DYNAMIC_UNICAST_SIZE,
             pSlotInfo) != FNP_SUCCESS)
        {
             MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, "VlanMbsmUpdtPortPropertiesToHw : "
                      "VlanFsMiVlanMbsmHwSwitchMacLearningLimit Failed \n");

            VlanReleaseContext ();
            VlanSendAckToMbsm (MBSM_FAILURE);
            return MBSM_FAILURE;
        }

        if (VLAN_PB_802_1AD_AH_BRIDGE () == VLAN_TRUE)
        {
            if (VlanMbsmUpdtMcastMacLimitToHw (pSlotInfo) == MBSM_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                          "VlanMbsmUpdtPortPropertiesToHw : "
                          "VlanMbsmUpdtMcastMacLimitToHw Failed \n");
                VlanReleaseContext ();
                VlanSendAckToMbsm (MBSM_FAILURE);
                return MBSM_FAILURE;
            }
        }
    }
    if (VlanMbsmProgramDynamicFdbEntry (pSlotInfo) == MBSM_FAILURE)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                   "VlanMbsmUpdtPortPropertiesToHw : "
                  "VlanMbsmProgramDynamicFdbEntry Failed \n");
        VlanReleaseContext ();
        VlanSendAckToMbsm (MBSM_FAILURE);
        return MBSM_FAILURE;
    }
    VlanReleaseContext ();
    u4PrevContextId = gu4ContextIdProcessed;
    if (VlanGetNextActiveContext (u4PrevContextId,
                                  &gu4ContextIdProcessed) == VLAN_SUCCESS)
    {
        gu2ByteIndex = 0;
        gu2BitIndex = 0;
        gu1PortFlag = 0;
        gu1IsBatchingStarted = OSIX_FALSE;
        if (VlanSelectContext (gu4ContextIdProcessed) != VLAN_SUCCESS)
        {
            VlanSendAckToMbsm (MBSM_FAILURE);
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
 	  	                  "VlanMbsmUpdtPortPropertiesToHw : "
  	  	                  "VlanSelectContext Failed \n");
            return MBSM_FAILURE;
        }
        VLAN_START_TIMER (VLAN_MBSM_TIMER, VLAN_MBSM_STAGERRING_TIME);
        VlanReleaseContext ();
        return MBSM_SUCCESS;
    }
    gu2ByteIndex = 0;
    gu2BitIndex = 0;
    gu1PortFlag = 0;
    gu1IsBatchingStarted = OSIX_FALSE;
    VlanSendAckToMbsm (MBSM_SUCCESS);
    return MBSM_SUCCESS;
}
#ifdef EVB_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMbsmUpdtEvbConfig		                     */
/*                                                                           */
/*    Description         : This function updates the EVB UAP port configs   */
/*			    and S-Channel creation configurations.                       */
/*                                                                           */
/*    Input(s)            : pMbsmPortInfo - Inserted port list               */
/*                          pSlotInfo - Information about inserted slot      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanMbsmUpdtEvbConfig (tMbsmPortInfo * pMbsmPortInfo,
                       tMbsmSlotInfo * pSlotInfo)
{

    tVlanPortEntry       *pVlanPortEntry = NULL;
    tVlanHwPortInfo       VlanHwPortInfo;
    tVlanEvbHwConfigInfo  VlanEvbHwConfigInfo;
    tEvbUapIfEntry       *pEvbUapIfEntry = NULL;
    tEvbSChIfEntry       *pEvbSChIfEntry = NULL;
    UINT4                 u4UapIfIndex = 0;
    UINT4                 u4SVID = 0;
    UNUSED_PARAM(pMbsmPortInfo);
    VLAN_MEMSET(&VlanHwPortInfo,0,sizeof(tVlanHwPortInfo));
    VLAN_MEMSET(&VlanEvbHwConfigInfo, 0, sizeof(tVlanEvbHwConfigInfo));

    while ((pEvbUapIfEntry = 
                VlanEvbUapIfGetNextEntry(u4UapIfIndex)) != NULL)
    {
        u4UapIfIndex = pEvbUapIfEntry->u4UapIfIndex;

        VlanHwPortInfo.u4ContextId = 
            pEvbUapIfEntry->u4UapIfCompId;
        VlanHwPortInfo.u4IfIndex   =  
            u4UapIfIndex;
        VlanHwPortInfo.u1BrgPortType = 
            CFA_UPLINK_ACCESS_PORT;
        /* VlanFsMiVlanMbsmHwSetBridgePortType will be invoked only for RemoteNPPorts */
        if(VlanFsMiVlanMbsmHwSetBridgePortType (
                    &VlanHwPortInfo,
                    pSlotInfo)  == FNP_FAILURE)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, 
                    "VlanMbsmUpdtEvbConfig: "
                    "VlanFsMiVlanMbsmHwSetBridgePortType"
                    "Failed \n");
            return MBSM_FAILURE;
        }

        u4SVID = 0;
        while ((pEvbSChIfEntry = VlanEvbSChIfGetNextEntry (
                        u4UapIfIndex, u4SVID)) != NULL)
        {
            /* Now creating the S-channel in hardware 
             * Hence u1OpCode is updated as 
             * VLAN_EVB_HW_SCH_IF_CREATE */
            if(pEvbSChIfEntry->u4UapIfIndex != u4UapIfIndex)
            {
                break;
            }
            VlanEvbHwConfigInfo.u4ContextId =
                pEvbUapIfEntry->u4UapIfCompId;
            VlanEvbHwConfigInfo.u4UapIfIndex = 
                u4UapIfIndex;

            VlanEvbHwConfigInfo.u4SChIfIndex =
               (UINT4) pEvbSChIfEntry->i4SChIfIndex;
            VlanEvbHwConfigInfo.u4SChId =
                pEvbSChIfEntry->u4SChId;
            VlanEvbHwConfigInfo.u2SVId =
                (UINT2)pEvbSChIfEntry->u4SVId;
            VlanEvbHwConfigInfo.u1OpCode =
                VLAN_EVB_HW_SCH_IF_CREATE;
            VlanEvbHwConfigInfo.u4VpHwIfIndex = 0; 
            VlanEvbHwConfigInfo.i4RxFilterEntry = 0; 

            pVlanPortEntry = VLAN_GET_PORT_ENTRY (pEvbSChIfEntry->u4SChIfPort);

            if (NULL == pVlanPortEntry)
            {
                VLAN_TRC_ARG2 (VLAN_EVB_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC,
                        VLAN_NAME, "VlanMbsmUpdtEvbConfig: tVlanPortEntry not "
                        " Present for S-Ch Index : %d and LocalPort Id : %d \r\n",
                        pEvbSChIfEntry->i4SChIfIndex, pEvbSChIfEntry->u4SChIfPort);
                return MBSM_FAILURE;
            }
#if 1
            /* should not  be merged  in ICM CODEBASE*/
            VlanEvbHwConfigInfo.u1AccFrameType =  pVlanPortEntry->u1AccpFrmTypes;
            VlanEvbHwConfigInfo.u2Pvid         =  pVlanPortEntry->Pvid;
#endif
            if(VlanFsMiVlanMbsmHwEvbConfigSChIface(
                        &VlanEvbHwConfigInfo,
                        pSlotInfo) == FNP_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN, 
                        "VlanMbsmUpdtEvbConfig: "
                        "VlanFsMiVlanMbsmHwEvbConfigSChIface "
                        "Failed \n");
                return MBSM_FAILURE;
            }

            pEvbSChIfEntry->u4SChHwIfIndex = 
                (VlanEvbHwConfigInfo.u4VpHwIfIndex);
            pEvbSChIfEntry->i4RxFilterEntry = 
                (VlanEvbHwConfigInfo.i4RxFilterEntry);

            /* Now verifying the S-channel Operstatus and 
             * based on that updating the status as 
             * VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD
             * or  VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK */ 
            if(pEvbSChIfEntry->u1OperStatus == CFA_IF_UP)
            {
                VlanEvbHwConfigInfo.u1OpCode = 
                    VLAN_EVB_HW_SCH_IF_TRAFFIC_FORWARD;
            }
            else
            {
                VlanEvbHwConfigInfo.u1OpCode = 
                    VLAN_EVB_HW_SCH_IF_TRAFFIC_BLOCK;
            }

            if(VlanFsMiVlanMbsmHwEvbConfigSChIface(
                        &VlanEvbHwConfigInfo,
                        pSlotInfo) == FNP_FAILURE)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_VLAN,
                        "VlanMbsmUpdtEvbConfig: "
                        "VlanFsMiVlanMbsmHwEvbConfigSChIface"
                        " Failed \n");
                return MBSM_FAILURE;
            }
            u4SVID = pEvbSChIfEntry->u4SVId;
            u4UapIfIndex = pEvbSChIfEntry->u4UapIfIndex;
        } /* SChannel Loop */
    } /* UAP loop */
    return MBSM_SUCCESS;
}
#endif /* EVB_WANTED */
#endif /* MBSM_WANTED */
