/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlmigfgn.c,v 1.35 2014/08/14 10:33:02 siva Exp $
 *
 * Description: This file contains the GetFirst and GetNext
 *              routines of the VLAN table.
 *
 *******************************************************************/

#include "vlaninc.h"
#include "vcm.h"

PRIVATE INT4
 
 
 
 VlanGetStaticUnicastAllowedToGoPorts (UINT4 u4FdbId, tMacAddr MacAddr,
                                       UINT2 u2RcvPort, tLocalPortList Ports);

PRIVATE INT4
 
 
 
 VlanGetNextPortForStUcastEntry (UINT4 u4FdbId, tMacAddr UcastAddress,
                                 UINT2 u2RcvPort, INT4 i4Port,
                                 INT4 *pi4NextPort);

PRIVATE INT4
 
 
 
 VlanValidateIfIndex (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 *pu2LocalPort);

PRIVATE INT4
 
 
 
 VlanGetNextPortInWildCardEntry (tMacAddr WildCardMacAddress, INT4 i4Port,
                                 INT4 *pi4NextPort);

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qBaseTable
 Input       :  The Indices
                FsDot1qVlanContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qBaseTable (INT4 *pi4FsDot1qVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qBaseTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qBaseTable (INT4 i4FsDot1qVlanContextId,
                                 INT4 *pi4NextFsDot1qVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                  &u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qFdbTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qFdbTable (INT4 *pi4FsDot1qVlanContextId,
                                 UINT4 *pu4FsDot1qFdbId)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qFdbTable (pu4FsDot1qFdbId) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qFdbTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qFdbId
                nextFsDot1qFdbId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qFdbTable (INT4 i4FsDot1qVlanContextId,
                                INT4 *pi4NextFsDot1qVlanContextId,
                                UINT4 u4FsDot1qFdbId,
                                UINT4 *pu4NextFsDot1qFdbId)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qFdbTable (u4FsDot1qFdbId,
                                          pu4NextFsDot1qFdbId) == SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;

            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qFdbTable (pu4NextFsDot1qFdbId) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qTpFdbTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qTpFdbAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qTpFdbTable (INT4 *pi4FsDot1qVlanContextId,
                                   UINT4 *pu4FsDot1qFdbId,
                                   tMacAddr * pFsDot1qTpFdbAddress)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qTpFdbTable (pu4FsDot1qFdbId,
                                             pFsDot1qTpFdbAddress) ==
            SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qTpFdbTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qFdbId
                nextFsDot1qFdbId
                FsDot1qTpFdbAddress
                nextFsDot1qTpFdbAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qTpFdbTable (INT4 i4FsDot1qVlanContextId,
                                  INT4 *pi4NextFsDot1qVlanContextId,
                                  UINT4 u4FsDot1qFdbId,
                                  UINT4 *pu4NextFsDot1qFdbId,
                                  tMacAddr FsDot1qTpFdbAddress,
                                  tMacAddr * pNextFsDot1qTpFdbAddress)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qTpFdbTable (u4FsDot1qFdbId, pu4NextFsDot1qFdbId,
                                            FsDot1qTpFdbAddress,
                                            pNextFsDot1qTpFdbAddress) ==
            SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qTpFdbTable
           (pu4NextFsDot1qFdbId, pNextFsDot1qTpFdbAddress) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qTpGroupTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpGroupAddress
                FsDot1qTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qTpGroupTable (INT4 *pi4FsDot1qVlanContextId,
                                     UINT4 *pu4FsDot1qVlanIndex,
                                     tMacAddr * pFsDot1qTpGroupAddress,
                                     INT4 *pi4FsDot1qTpPort)
{
    UINT1              *pEgressPorts = NULL;
    UINT4               u4ContextId;
    UINT4               u4TempContextId;
    UINT4               u4IfIndex;
    UINT4               u4PrvIfIndex;
    tSNMP_OCTET_STRING_TYPE SnmpEgressPorts;
    UINT2               u2LocalPortId;
    UINT1               u1Result = VLAN_FALSE;
    UINT4               u4FsDot1qVlanIndex;
    tMacAddr            FsDot1qTpGroupAddress;

    pEgressPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pEgressPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhGetFirstIndexFsDot1qTpGroupTable: Error in allocating memory "
                  "for pEgressPorts\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (pEgressPorts, 0, sizeof (tLocalPortList));
    SnmpEgressPorts.pu1_OctetList = pEgressPorts;
    SnmpEgressPorts.i4_Length = sizeof (tLocalPortList);

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        UtilPlstReleaseLocalPortList (pEgressPorts);
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pEgressPorts);
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            UtilPlstReleaseLocalPortList (pEgressPorts);
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qTpGroupTable (pu4FsDot1qVlanIndex,
                                               pFsDot1qTpGroupAddress) ==
            SNMP_SUCCESS)
        {
            do
            {
                /* since we are having seperate table for portlist, 
                 * we have to get the ports mapped to this context */
                nmhGetDot1qTpGroupEgressPorts (*pu4FsDot1qVlanIndex,
                                               *pFsDot1qTpGroupAddress,
                                               &SnmpEgressPorts);

                if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
                {
                    do
                    {
                        if (VlanGetContextInfoFromIfIndex (u4IfIndex,
                                                           &u4TempContextId,
                                                           &u2LocalPortId) !=
                            VLAN_SUCCESS)
                        {
                            UtilPlstReleaseLocalPortList (pEgressPorts);
                            VlanReleaseContext ();
                            return SNMP_FAILURE;
                        }
                        VLAN_IS_MEMBER_PORT (SnmpEgressPorts.pu1_OctetList,
                                             u2LocalPortId, u1Result);
                        if (u1Result == VLAN_TRUE)
                        {
                            *pi4FsDot1qTpPort = (INT4) u4IfIndex;
                            VlanReleaseContext ();
                            UtilPlstReleaseLocalPortList (pEgressPorts);
                            return SNMP_SUCCESS;
                        }
                        u4PrvIfIndex = u4IfIndex;
                    }
                    while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                         &u4IfIndex) ==
                           VLAN_SUCCESS);
                }
                u4FsDot1qVlanIndex = *pu4FsDot1qVlanIndex;
                VLAN_CPY_MAC_ADDR (FsDot1qTpGroupAddress,
                                   pFsDot1qTpGroupAddress);
            }
            while (nmhGetNextIndexDot1qTpGroupTable
                   (u4FsDot1qVlanIndex, pu4FsDot1qVlanIndex,
                    FsDot1qTpGroupAddress, pFsDot1qTpGroupAddress) ==
                   SNMP_SUCCESS);
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    UtilPlstReleaseLocalPortList (pEgressPorts);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qTpGroupTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
                FsDot1qTpGroupAddress
                nextFsDot1qTpGroupAddress
                FsDot1qTpPort
                nextFsDot1qTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qTpGroupTable (INT4 i4FsDot1qVlanContextId,
                                    INT4 *pi4NextFsDot1qVlanContextId,
                                    UINT4 u4FsDot1qVlanIndex,
                                    UINT4 *pu4NextFsDot1qVlanIndex,
                                    tMacAddr FsDot1qTpGroupAddress,
                                    tMacAddr * pNextFsDot1qTpGroupAddress,
                                    INT4 i4FsDot1qTpPort,
                                    INT4 *pi4NextFsDot1qTpPort)
{
    UINT1              *pEgressPorts = NULL;
    UINT4               u4ContextId;
    UINT4               u4TempContextId;
    UINT4               u4PortContextId;
    UINT4               u4IfIndex;
    UINT4               u4PrvIfIndex;
    UINT2               u2PortLocalPortId;
    UINT2               u2LocalPortId;
    UINT1               u1Result = VLAN_FALSE;
    tSNMP_OCTET_STRING_TYPE SnmpEgressPorts;

    pEgressPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pEgressPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhGetNextIndexFsDot1qTpGroupTable: Error in allocating memory "
                  "for pEgressPorts\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (pEgressPorts, 0, sizeof (tLocalPortList));
    SnmpEgressPorts.pu1_OctetList = pEgressPorts;
    SnmpEgressPorts.i4_Length = sizeof (tLocalPortList);

    if (i4FsDot1qTpPort != 0)
    {
        if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                           &u4PortContextId,
                                           &u2PortLocalPortId) != VLAN_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pEgressPorts);
            return SNMP_FAILURE;
        }

        if (u4PortContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            UtilPlstReleaseLocalPortList (pEgressPorts);
            return SNMP_FAILURE;
        }
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            UtilPlstReleaseLocalPortList (pEgressPorts);
            return SNMP_FAILURE;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            UtilPlstReleaseLocalPortList (pEgressPorts);
            return SNMP_FAILURE;
        }

        /* since we are having seperate table for portlist, we have to get 
         * the next port mapped to this context, if it fails only we have 
         * to look for the next index (vlanid, GroupAddress) */
        if (VlanGetNextPortInCurrContext ((UINT4) i4FsDot1qTpPort,
                                          &u4IfIndex) == VLAN_SUCCESS)
        {
            nmhGetDot1qTpGroupEgressPorts (u4FsDot1qVlanIndex,
                                           FsDot1qTpGroupAddress,
                                           &SnmpEgressPorts);
            do
            {
                if (VlanGetContextInfoFromIfIndex
                    (u4IfIndex, &u4TempContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    UtilPlstReleaseLocalPortList (pEgressPorts);
                    return SNMP_FAILURE;
                }
                VLAN_IS_MEMBER_PORT (SnmpEgressPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result);
                if (u1Result == VLAN_TRUE)
                {
                    *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                    *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
                    *pu4NextFsDot1qVlanIndex = u4FsDot1qVlanIndex;
                    VLAN_CPY_MAC_ADDR ((UINT1 *) *pNextFsDot1qTpGroupAddress,
                                       FsDot1qTpGroupAddress);
                    VlanReleaseContext ();
                    UtilPlstReleaseLocalPortList (pEgressPorts);
                    return SNMP_SUCCESS;
                }
                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                 &u4IfIndex) == VLAN_SUCCESS);
        }

        if (nmhGetNextIndexDot1qTpGroupTable (u4FsDot1qVlanIndex,
                                              pu4NextFsDot1qVlanIndex,
                                              FsDot1qTpGroupAddress,
                                              pNextFsDot1qTpGroupAddress) ==
            SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            /* we have to get the first port mapped to this context */
            nmhGetDot1qTpGroupEgressPorts (*pu4NextFsDot1qVlanIndex,
                                           *pNextFsDot1qTpGroupAddress,
                                           &SnmpEgressPorts);
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {

                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pEgressPorts);
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpEgressPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result);
                    if (u1Result == VLAN_TRUE)
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pEgressPorts);
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pEgressPorts);
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pEgressPorts);
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qTpGroupTable (pu4NextFsDot1qVlanIndex,
                                               pNextFsDot1qTpGroupAddress) ==
            SNMP_SUCCESS)
        {
            /* we have to get the first port mapped to this context */
            nmhGetDot1qTpGroupEgressPorts (*pu4NextFsDot1qVlanIndex,
                                           *pNextFsDot1qTpGroupAddress,
                                           &SnmpEgressPorts);
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pEgressPorts);
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpEgressPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result);
                    if (u1Result == VLAN_TRUE)
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pEgressPorts);
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
        }
    }
    while (VLAN_SNMP_TRUE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qForwardAllLearntPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qForwardAllLearntPortTable (INT4 *pi4FsDot1qVlanContextId,
                                                  UINT4 *pu4FsDot1qVlanIndex,
                                                  INT4 *pi4FsDot1qTpPort)
{
    return (nmhGetNextIndexFsDot1qForwardAllLearntPortTable
            (0, pi4FsDot1qVlanContextId, 1, pu4FsDot1qVlanIndex,
             0, pi4FsDot1qTpPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qForwardAllLearntPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
                FsDot1qTpPort
                nextFsDot1qTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qForwardAllLearntPortTable (INT4 i4FsDot1qVlanContextId,
                                                 INT4
                                                 *pi4NextFsDot1qVlanContextId,
                                                 UINT4 u4FsDot1qVlanIndex,
                                                 UINT4 *pu4NextFsDot1qVlanIndex,
                                                 INT4 i4FsDot1qTpPort,
                                                 INT4 *pi4NextFsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    UINT4               u4TempContextId;
    UINT4               u4PrvIfIndex;
    UINT4               u4TempVlanId = 0;
    tLocalPortList      Ports;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    UINT2               u2LocalPortId;
    UINT1               u1Result1 = VLAN_FALSE;

    MEMSET (Ports, 0, sizeof (tLocalPortList));
    SnmpPorts.pu1_OctetList = Ports;
    SnmpPorts.i4_Length = sizeof (tLocalPortList);

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        /* since we are having seperate table for portlist, we have to get 
         * the next port mapped to this context, if it fails only we have 
         * to look for the next index (vlanid) */
        if (VlanGetNextPortInCurrContext ((UINT4) i4FsDot1qTpPort,
                                          &u4IfIndex) == VLAN_SUCCESS)
        {
            nmhGetDot1qForwardAllPorts (u4FsDot1qVlanIndex, &SnmpPorts);
            do
            {
                if (VlanGetContextInfoFromIfIndex
                    (u4IfIndex, &u4TempContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result1);
                if (u1Result1 == VLAN_TRUE)
                {
                    *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                    *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
                    *pu4NextFsDot1qVlanIndex = u4FsDot1qVlanIndex;

                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                 &u4IfIndex) == VLAN_SUCCESS);
        }

        while (nmhGetNextIndexDot1qForwardAllTable (u4FsDot1qVlanIndex,
                                                    pu4NextFsDot1qVlanIndex) ==
               SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qForwardAllPorts (*pu4NextFsDot1qVlanIndex,
                                            &SnmpPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    if (u1Result1 == VLAN_TRUE)
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
            u4FsDot1qVlanIndex = *pu4NextFsDot1qVlanIndex;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

        while (nmhGetNextIndexDot1qForwardAllTable (u4TempVlanId,
                                                    pu4NextFsDot1qVlanIndex) ==
               SNMP_SUCCESS)
        {
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qForwardAllPorts (*pu4NextFsDot1qVlanIndex,
                                            &SnmpPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    if (u1Result1 == VLAN_TRUE)
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
            /* If there is no member port in vlan, then get next vlan. */
            u4TempVlanId = *pu4NextFsDot1qVlanIndex;
        }
    }
    while (VLAN_SNMP_TRUE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qForwardAllStatusTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qForwardAllStatusTable (INT4 *pi4FsDot1qVlanContextId,
                                              UINT4 *pu4FsDot1qVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qForwardAllTable
            (pu4FsDot1qVlanIndex) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qForwardAllStatusTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qForwardAllStatusTable (INT4 i4FsDot1qVlanContextId,
                                             INT4 *pi4NextFsDot1qVlanContextId,
                                             UINT4 u4FsDot1qVlanIndex,
                                             UINT4 *pu4NextFsDot1qVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qForwardAllTable (u4FsDot1qVlanIndex,
                                                 pu4NextFsDot1qVlanIndex) ==
            SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qForwardAllTable
           (pu4NextFsDot1qVlanIndex) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qForwardAllPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qForwardAllPortConfigTable (INT4 *pi4FsDot1qVlanContextId,
                                                  UINT4 *pu4FsDot1qVlanIndex,
                                                  INT4 *pi4FsDot1qTpPort)
{
    return (nmhGetNextIndexFsDot1qForwardAllPortConfigTable
            (0, pi4FsDot1qVlanContextId, 1, pu4FsDot1qVlanIndex,
             0, pi4FsDot1qTpPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qForwardAllPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
                FsDot1qTpPort
                nextFsDot1qTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qForwardAllPortConfigTable (INT4 i4FsDot1qVlanContextId,
                                                 INT4
                                                 *pi4NextFsDot1qVlanContextId,
                                                 UINT4 u4FsDot1qVlanIndex,
                                                 UINT4 *pu4NextFsDot1qVlanIndex,
                                                 INT4 i4FsDot1qTpPort,
                                                 INT4 *pi4NextFsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    UINT4               u4TempContextId;
    UINT4               u4PrvIfIndex;
    UINT4               u4TempVlanId = 0;
    tLocalPortList      Ports;
    tLocalPortList      ForbidPorts;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    tSNMP_OCTET_STRING_TYPE SnmpForbidPorts;
    UINT2               u2LocalPortId;
    UINT1               u1Result1 = VLAN_FALSE;
    UINT1               u1Result2 = VLAN_FALSE;

    MEMSET (Ports, 0, sizeof (tLocalPortList));
    SnmpPorts.pu1_OctetList = Ports;
    SnmpPorts.i4_Length = sizeof (tLocalPortList);

    MEMSET (ForbidPorts, 0, sizeof (tLocalPortList));
    SnmpForbidPorts.pu1_OctetList = ForbidPorts;
    SnmpForbidPorts.i4_Length = sizeof (tLocalPortList);

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        /* since we are having seperate table for portlist, we have to get 
         * the next port mapped to this context, if it fails only we have 
         * to look for the next index (vlanid) */
        if (VlanGetNextPortInCurrContext ((UINT4) i4FsDot1qTpPort,
                                          &u4IfIndex) == VLAN_SUCCESS)
        {
            nmhGetDot1qForwardAllPorts (u4FsDot1qVlanIndex, &SnmpPorts);
            nmhGetDot1qForwardAllForbiddenPorts (u4FsDot1qVlanIndex,
                                                 &SnmpForbidPorts);
            do
            {
                if (VlanGetContextInfoFromIfIndex
                    (u4IfIndex, &u4TempContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result1);
                VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result2);
                if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                {
                    *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                    *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
                    *pu4NextFsDot1qVlanIndex = u4FsDot1qVlanIndex;

                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                 &u4IfIndex) == VLAN_SUCCESS);
        }

        while (nmhGetNextIndexDot1qForwardAllTable (u4FsDot1qVlanIndex,
                                                    pu4NextFsDot1qVlanIndex) ==
               SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qForwardAllPorts (*pu4NextFsDot1qVlanIndex,
                                            &SnmpPorts);
                nmhGetDot1qForwardAllForbiddenPorts (*pu4NextFsDot1qVlanIndex,
                                                     &SnmpForbidPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result2);
                    if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
            u4FsDot1qVlanIndex = *pu4NextFsDot1qVlanIndex;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

        while (nmhGetNextIndexDot1qForwardAllTable (u4TempVlanId,
                                                    pu4NextFsDot1qVlanIndex) ==
               SNMP_SUCCESS)
        {
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qForwardAllPorts (*pu4NextFsDot1qVlanIndex,
                                            &SnmpPorts);
                nmhGetDot1qForwardAllForbiddenPorts (*pu4NextFsDot1qVlanIndex,
                                                     &SnmpForbidPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result2);
                    if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
            u4TempVlanId = *pu4NextFsDot1qVlanIndex;
        }
    }
    while (VLAN_SNMP_TRUE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qForwardUnregLearntPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qForwardUnregLearntPortTable (INT4
                                                    *pi4FsDot1qVlanContextId,
                                                    UINT4 *pu4FsDot1qVlanIndex,
                                                    INT4 *pi4FsDot1qTpPort)
{
    return (nmhGetNextIndexFsDot1qForwardUnregLearntPortTable
            (0, pi4FsDot1qVlanContextId, 1, pu4FsDot1qVlanIndex,
             0, pi4FsDot1qTpPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qForwardUnregLearntPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
                FsDot1qTpPort
                nextFsDot1qTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qForwardUnregLearntPortTable (INT4 i4FsDot1qVlanContextId,
                                                   INT4
                                                   *pi4NextFsDot1qVlanContextId,
                                                   UINT4 u4FsDot1qVlanIndex,
                                                   UINT4
                                                   *pu4NextFsDot1qVlanIndex,
                                                   INT4 i4FsDot1qTpPort,
                                                   INT4 *pi4NextFsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    UINT4               u4TempContextId;
    UINT4               u4PrvIfIndex;
    UINT4               u4TempVlanId = 0;
    tLocalPortList      Ports;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    UINT2               u2LocalPortId;
    UINT1               u1Result1 = VLAN_FALSE;

    MEMSET (Ports, 0, sizeof (tLocalPortList));
    SnmpPorts.pu1_OctetList = Ports;
    SnmpPorts.i4_Length = sizeof (tLocalPortList);

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        /* since we are having seperate table for portlist, we have to get 
         * the next port mapped to this context, if it fails only we have 
         * to look for the next index (vlanid) */
        if (VlanGetNextPortInCurrContext ((UINT4) i4FsDot1qTpPort,
                                          &u4IfIndex) == VLAN_SUCCESS)
        {
            nmhGetDot1qForwardUnregisteredPorts (u4FsDot1qVlanIndex,
                                                 &SnmpPorts);
            do
            {
                if (VlanGetContextInfoFromIfIndex
                    (u4IfIndex, &u4TempContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result1);
                if (u1Result1 == VLAN_TRUE)
                {
                    *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                    *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
                    *pu4NextFsDot1qVlanIndex = u4FsDot1qVlanIndex;

                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                 &u4IfIndex) == VLAN_SUCCESS);

        }

        while (nmhGetNextIndexDot1qForwardUnregisteredTable (u4FsDot1qVlanIndex,
                                                             pu4NextFsDot1qVlanIndex)
               == SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qForwardUnregisteredPorts (*pu4NextFsDot1qVlanIndex,
                                                     &SnmpPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    if (u1Result1 == VLAN_TRUE)
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
            u4FsDot1qVlanIndex = *pu4NextFsDot1qVlanIndex;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

        while (nmhGetNextIndexDot1qForwardUnregisteredTable (u4TempVlanId,
                                                             pu4NextFsDot1qVlanIndex)
               == SNMP_SUCCESS)
        {
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qForwardAllPorts (*pu4NextFsDot1qVlanIndex,
                                            &SnmpPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    if (u1Result1 == VLAN_TRUE)
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
            /* If there is no member port in vlan, then get next vlan. */
            u4TempVlanId = *pu4NextFsDot1qVlanIndex;
        }
    }
    while (VLAN_SNMP_TRUE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qForwardUnregStatusTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qForwardUnregStatusTable (INT4 *pi4FsDot1qVlanContextId,
                                                UINT4 *pu4FsDot1qVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qForwardUnregisteredTable
            (pu4FsDot1qVlanIndex) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qForwardUnregStatusTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qForwardUnregStatusTable (INT4 i4FsDot1qVlanContextId,
                                               INT4
                                               *pi4NextFsDot1qVlanContextId,
                                               UINT4 u4FsDot1qVlanIndex,
                                               UINT4 *pu4NextFsDot1qVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qForwardUnregisteredTable (u4FsDot1qVlanIndex,
                                                          pu4NextFsDot1qVlanIndex)
            == SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qForwardUnregisteredTable
           (pu4NextFsDot1qVlanIndex) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qForwardUnregPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qForwardUnregPortConfigTable (INT4
                                                    *pi4FsDot1qVlanContextId,
                                                    UINT4 *pu4FsDot1qVlanIndex,
                                                    INT4 *pi4FsDot1qTpPort)
{
    return (nmhGetNextIndexFsDot1qForwardUnregPortConfigTable
            (0, pi4FsDot1qVlanContextId, 1, pu4FsDot1qVlanIndex,
             0, pi4FsDot1qTpPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qForwardUnregPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
                FsDot1qTpPort
                nextFsDot1qTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qForwardUnregPortConfigTable (INT4 i4FsDot1qVlanContextId,
                                                   INT4
                                                   *pi4NextFsDot1qVlanContextId,
                                                   UINT4 u4FsDot1qVlanIndex,
                                                   UINT4
                                                   *pu4NextFsDot1qVlanIndex,
                                                   INT4 i4FsDot1qTpPort,
                                                   INT4 *pi4NextFsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    UINT4               u4TempContextId;
    UINT4               u4PrvIfIndex;
    UINT4               u4TempVlanId = 0;
    tLocalPortList      Ports;
    tLocalPortList      ForbidPorts;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    tSNMP_OCTET_STRING_TYPE SnmpForbidPorts;
    UINT2               u2LocalPortId;
    UINT1               u1Result1 = VLAN_FALSE;
    UINT1               u1Result2 = VLAN_FALSE;

    MEMSET (Ports, 0, sizeof (tLocalPortList));
    SnmpPorts.pu1_OctetList = Ports;
    SnmpPorts.i4_Length = sizeof (tLocalPortList);

    MEMSET (ForbidPorts, 0, sizeof (tLocalPortList));
    SnmpForbidPorts.pu1_OctetList = ForbidPorts;
    SnmpForbidPorts.i4_Length = sizeof (tLocalPortList);

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        /* since we are having seperate table for portlist, we have to get 
         * the next port mapped to this context, if it fails only we have 
         * to look for the next index (vlanid) */
        if (VlanGetNextPortInCurrContext ((UINT4) i4FsDot1qTpPort,
                                          &u4IfIndex) == VLAN_SUCCESS)
        {
            nmhGetDot1qForwardUnregisteredPorts (u4FsDot1qVlanIndex,
                                                 &SnmpPorts);
            nmhGetDot1qForwardUnregisteredForbiddenPorts (u4FsDot1qVlanIndex,
                                                          &SnmpForbidPorts);
            do
            {
                if (VlanGetContextInfoFromIfIndex
                    (u4IfIndex, &u4TempContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result1);
                VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result2);
                if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                {
                    *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                    *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
                    *pu4NextFsDot1qVlanIndex = u4FsDot1qVlanIndex;

                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                 &u4IfIndex) == VLAN_SUCCESS);
        }

        while (nmhGetNextIndexDot1qForwardUnregisteredTable (u4FsDot1qVlanIndex,
                                                             pu4NextFsDot1qVlanIndex)
               == SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qForwardUnregisteredPorts (*pu4NextFsDot1qVlanIndex,
                                                     &SnmpPorts);
                nmhGetDot1qForwardUnregisteredForbiddenPorts
                    (*pu4NextFsDot1qVlanIndex, &SnmpForbidPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result2);
                    if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
            u4FsDot1qVlanIndex = *pu4NextFsDot1qVlanIndex;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

        while (nmhGetNextIndexDot1qForwardUnregisteredTable (u4TempVlanId,
                                                             pu4NextFsDot1qVlanIndex)
               == SNMP_SUCCESS)
        {
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qForwardUnregisteredPorts (*pu4NextFsDot1qVlanIndex,
                                                     &SnmpPorts);
                nmhGetDot1qForwardUnregisteredForbiddenPorts
                    (*pu4NextFsDot1qVlanIndex, &SnmpForbidPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result2);
                    if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
            u4TempVlanId = *pu4NextFsDot1qVlanIndex;
        }
    }
    while (VLAN_SNMP_TRUE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qStaticUnicastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qStaticUnicastTable (INT4 *pi4FsDot1qVlanContextId,
                                           UINT4 *pu4FsDot1qFdbId,
                                           tMacAddr *
                                           pFsDot1qStaticUnicastAddress,
                                           INT4
                                           *pi4FsDot1qStaticUnicastReceivePort)
{
    UINT4               u4ContextId;
    INT4                i4TempRcvPort;
    tVlanPortEntry     *pPortEntry;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qStaticUnicastTable (pu4FsDot1qFdbId,
                                                     pFsDot1qStaticUnicastAddress,
                                                     &i4TempRcvPort) ==
            SNMP_SUCCESS)
        {
            if (i4TempRcvPort != 0)
            {
                pPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
                *pi4FsDot1qStaticUnicastReceivePort = pPortEntry->u4IfIndex;
            }
            else
            {
                *pi4FsDot1qStaticUnicastReceivePort = i4TempRcvPort;
            }
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qStaticUnicastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qFdbId
                nextFsDot1qFdbId
                FsDot1qStaticUnicastAddress
                nextFsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                nextFsDot1qStaticUnicastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qStaticUnicastTable (INT4 i4FsDot1qVlanContextId,
                                          INT4 *pi4NextFsDot1qVlanContextId,
                                          UINT4 u4FsDot1qFdbId,
                                          UINT4 *pu4NextFsDot1qFdbId,
                                          tMacAddr FsDot1qStaticUnicastAddress,
                                          tMacAddr *
                                          pNextFsDot1qStaticUnicastAddress,
                                          INT4
                                          i4FsDot1qStaticUnicastReceivePort,
                                          INT4
                                          *pi4NextFsDot1qStaticUnicastReceivePort)
{
    UINT4               u4ContextId;
    INT4                i4TempRcvPort;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2PortLocalPortId = 0;

    if (VlanValidateIfIndex (i4FsDot1qVlanContextId,
                             i4FsDot1qStaticUnicastReceivePort,
                             &u2PortLocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qStaticUnicastTable (u4FsDot1qFdbId,
                                                    pu4NextFsDot1qFdbId,
                                                    FsDot1qStaticUnicastAddress,
                                                    pNextFsDot1qStaticUnicastAddress,
                                                    (INT4) u2PortLocalPortId,
                                                    &i4TempRcvPort) ==
            SNMP_SUCCESS)
        {
            if (i4TempRcvPort != 0)
            {
                pPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
                *pi4NextFsDot1qStaticUnicastReceivePort = pPortEntry->u4IfIndex;
            }
            else
            {
                *pi4NextFsDot1qStaticUnicastReceivePort = i4TempRcvPort;
            }

            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qStaticUnicastTable (pu4NextFsDot1qFdbId,
                                                    pNextFsDot1qStaticUnicastAddress,
                                                    &i4TempRcvPort) !=
           SNMP_SUCCESS);

    if (i4TempRcvPort != 0)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
        *pi4NextFsDot1qStaticUnicastReceivePort = pPortEntry->u4IfIndex;
    }
    else
    {
        *pi4NextFsDot1qStaticUnicastReceivePort = i4TempRcvPort;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qStaticAllowedToGoTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                FsDot1qTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qStaticAllowedToGoTable (INT4 *pi4FsDot1qVlanContextId,
                                               UINT4 *pu4FsDot1qFdbId,
                                               tMacAddr *
                                               pFsDot1qStaticUnicastAddress,
                                               INT4
                                               *pi4FsDot1qStaticUnicastReceivePort,
                                               INT4 *pi4FsDot1qTpPort)
{
    tMacAddr            MacAddr;

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    return (nmhGetNextIndexFsDot1qStaticAllowedToGoTable
            (0, pi4FsDot1qVlanContextId, 1, pu4FsDot1qFdbId, MacAddr,
             pFsDot1qStaticUnicastAddress, 0,
             pi4FsDot1qStaticUnicastReceivePort, 0, pi4FsDot1qTpPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qStaticAllowedToGoTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qFdbId
                nextFsDot1qFdbId
                FsDot1qStaticUnicastAddress
                nextFsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                nextFsDot1qStaticUnicastReceivePort
                FsDot1qTpPort
                nextFsDot1qTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qStaticAllowedToGoTable (INT4 i4FsDot1qVlanContextId,
                                              INT4 *pi4NextFsDot1qVlanContextId,
                                              UINT4 u4FsDot1qFdbId,
                                              UINT4 *pu4NextFsDot1qFdbId,
                                              tMacAddr
                                              FsDot1qStaticUnicastAddress,
                                              tMacAddr *
                                              pNextFsDot1qStaticUnicastAddress,
                                              INT4
                                              i4FsDot1qStaticUnicastReceivePort,
                                              INT4
                                              *pi4NextFsDot1qStaticUnicastReceivePort,
                                              INT4 i4FsDot1qTpPort,
                                              INT4 *pi4NextFsDot1qTpPort)
{
    UINT4               u4ContextId;
    INT4                i4TempRcvPort;
    tVlanPortEntry     *pRcvPortEntry = NULL;
    tLocalPortList      Ports;
    UINT2               u2TpPortLocalPortId = 0;
    UINT2               u2RcvPortLocalPortId = 0;

    MEMSET (Ports, 0, sizeof (tLocalPortList));

    /* Validate the given Receive Port */
    if (VlanValidateIfIndex (i4FsDot1qVlanContextId,
                             i4FsDot1qStaticUnicastReceivePort,
                             &u2RcvPortLocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Validate the given TpPort */
    if (VlanValidateIfIndex (i4FsDot1qVlanContextId, i4FsDot1qTpPort,
                             &u2TpPortLocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        /* At first we have to scan through the portlist and get the ports, Only 
         * when the scan completes we have to look for the next indices (FdbId, 
         * MacAddress, RcvPort) */
        if (VlanGetNextPortForStUcastEntry
            (u4FsDot1qFdbId, FsDot1qStaticUnicastAddress, u2RcvPortLocalPortId,
             i4FsDot1qTpPort, pi4NextFsDot1qTpPort) == VLAN_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            *pu4NextFsDot1qFdbId = u4FsDot1qFdbId;
            *pi4NextFsDot1qStaticUnicastReceivePort =
                i4FsDot1qStaticUnicastReceivePort;
            VLAN_CPY_MAC_ADDR
                ((UINT1 *) *pNextFsDot1qStaticUnicastAddress,
                 FsDot1qStaticUnicastAddress);
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }

        /* Since the scanning of portlist completed, now we have look for 
         * the Next Indices (FdbId, MacAddress, RcvPort) */
        if (nmhGetNextIndexDot1qStaticUnicastTable (u4FsDot1qFdbId,
                                                    pu4NextFsDot1qFdbId,
                                                    FsDot1qStaticUnicastAddress,
                                                    pNextFsDot1qStaticUnicastAddress,
                                                    (INT4) u2RcvPortLocalPortId,
                                                    &i4TempRcvPort) ==
            SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            *pi4NextFsDot1qStaticUnicastReceivePort = 0;

            if (i4TempRcvPort != 0)
            {
                /* Get and store the IfIndex */
                pRcvPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
                *pi4NextFsDot1qStaticUnicastReceivePort =
                    pRcvPortEntry->u4IfIndex;
            }

            /* After getting the Indices (FdbId, MacAddress, RcvPort), 
             * we have to get the first port present in the portlist 
             * for this entry. */
            if (VlanGetNextPortForStUcastEntry
                (*pu4NextFsDot1qFdbId, *pNextFsDot1qStaticUnicastAddress,
                 (UINT2) i4TempRcvPort, 0,
                 pi4NextFsDot1qTpPort) == VLAN_SUCCESS)
            {
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
    }

    /* We will be here only when all the entries in the static unicast table
     * was scanned for the given context. Next we have look for next Active 
     * context and their static unicast entries. */
    do
    {
        VlanReleaseContext ();
        /* Getting the next Active context */
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

        /* Getting the first static unicast entry */
        if (nmhGetFirstIndexDot1qStaticUnicastTable (pu4NextFsDot1qFdbId,
                                                     pNextFsDot1qStaticUnicastAddress,
                                                     &i4TempRcvPort) ==
            SNMP_SUCCESS)
        {
            *pi4NextFsDot1qStaticUnicastReceivePort = 0;

            if (i4TempRcvPort != 0)
            {
                pRcvPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
                *pi4NextFsDot1qStaticUnicastReceivePort =
                    pRcvPortEntry->u4IfIndex;
            }

            /* we have to get the first port present in the portlist 
             * for this entry. */
            if (VlanGetNextPortForStUcastEntry
                (*pu4NextFsDot1qFdbId, *pNextFsDot1qStaticUnicastAddress,
                 (UINT2) i4TempRcvPort, 0,
                 pi4NextFsDot1qTpPort) == VLAN_SUCCESS)
            {
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
    }
    while (VLAN_SNMP_TRUE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qStaticMulticastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qStaticMulticastTable (INT4 *pi4FsDot1qVlanContextId,
                                             UINT4 *pu4FsDot1qVlanIndex,
                                             tMacAddr *
                                             pFsDot1qStaticMulticastAddress,
                                             INT4
                                             *pi4FsDot1qStaticMulticastReceivePort)
{
    UINT4               u4ContextId;
    INT4                i4TempRcvPort;
    tVlanPortEntry     *pPortEntry;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qStaticMulticastTable (pu4FsDot1qVlanIndex,
                                                       pFsDot1qStaticMulticastAddress,
                                                       &i4TempRcvPort) ==
            SNMP_SUCCESS)
        {
            if (i4TempRcvPort != 0)
            {
                pPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
                *pi4FsDot1qStaticMulticastReceivePort = pPortEntry->u4IfIndex;
            }
            else
            {
                *pi4FsDot1qStaticMulticastReceivePort = i4TempRcvPort;
            }
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qStaticMulticastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                nextFsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                nextFsDot1qStaticMulticastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qStaticMulticastTable (INT4 i4FsDot1qVlanContextId,
                                            INT4 *pi4NextFsDot1qVlanContextId,
                                            UINT4 u4FsDot1qVlanIndex,
                                            UINT4 *pu4NextFsDot1qVlanIndex,
                                            tMacAddr
                                            FsDot1qStaticMulticastAddress,
                                            tMacAddr *
                                            pNextFsDot1qStaticMulticastAddress,
                                            INT4
                                            i4FsDot1qStaticMulticastReceivePort,
                                            INT4
                                            *pi4NextFsDot1qStaticMulticastReceivePort)
{
    UINT4               u4ContextId;
    INT4                i4TempRcvPort;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2PortLocalPortId = 0;

    if (VlanValidateIfIndex (i4FsDot1qVlanContextId,
                             i4FsDot1qStaticMulticastReceivePort,
                             &u2PortLocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qStaticMulticastTable (u4FsDot1qVlanIndex,
                                                      pu4NextFsDot1qVlanIndex,
                                                      FsDot1qStaticMulticastAddress,
                                                      pNextFsDot1qStaticMulticastAddress,
                                                      (INT4) u2PortLocalPortId,
                                                      &i4TempRcvPort) ==
            SNMP_SUCCESS)
        {
            if (i4TempRcvPort != 0)
            {
                pPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
                *pi4NextFsDot1qStaticMulticastReceivePort =
                    pPortEntry->u4IfIndex;
            }
            else
            {
                *pi4NextFsDot1qStaticMulticastReceivePort = i4TempRcvPort;
            }

            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qStaticMulticastTable
           (pu4NextFsDot1qVlanIndex,
            pNextFsDot1qStaticMulticastAddress,
            &i4TempRcvPort) != SNMP_SUCCESS);

    if (i4TempRcvPort != 0)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
        *pi4NextFsDot1qStaticMulticastReceivePort = pPortEntry->u4IfIndex;
    }
    else
    {
        *pi4NextFsDot1qStaticMulticastReceivePort = i4TempRcvPort;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qStaticMcastPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                FsDot1qTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qStaticMcastPortTable (INT4 *pi4FsDot1qVlanContextId,
                                             UINT4 *pu4FsDot1qVlanIndex,
                                             tMacAddr *
                                             pFsDot1qStaticMulticastAddress,
                                             INT4
                                             *pi4FsDot1qStaticMulticastReceivePort,
                                             INT4 *pi4FsDot1qTpPort)
{
    UINT1              *pPorts = NULL;
    UINT1              *pForbidPorts = NULL;
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    INT4                i4TempRcvPort;
    tVlanPortEntry     *pRcvPortEntry = NULL;
    UINT4               u4TempContextId;
    UINT4               u4PrvIfIndex;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    tSNMP_OCTET_STRING_TYPE SnmpForbidPorts;
    UINT2               u2LocalPortId;
    UINT1               u1Result1 = VLAN_FALSE;
    UINT1               u1Result2 = VLAN_FALSE;

    pPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhGetFirstIndexFsDot1qStaticMcastPortTable: Error in allocating memory "
                  "for pPorts\r\n");
        return SNMP_FAILURE;
    }

    pForbidPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pForbidPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhGetFirstIndexFsDot1qStaticMcastPortTable: Error in allocating memory "
                  "for pForbidPorts\r\n");
        UtilPlstReleaseLocalPortList (pPorts);
        return SNMP_FAILURE;
    }

    MEMSET (pPorts, 0, sizeof (tLocalPortList));
    SnmpPorts.pu1_OctetList = pPorts;
    SnmpPorts.i4_Length = sizeof (tLocalPortList);

    MEMSET (pForbidPorts, 0, sizeof (tLocalPortList));
    SnmpForbidPorts.pu1_OctetList = pForbidPorts;
    SnmpForbidPorts.i4_Length = sizeof (tLocalPortList);

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        UtilPlstReleaseLocalPortList (pPorts);
        UtilPlstReleaseLocalPortList (pForbidPorts);
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pPorts);
            UtilPlstReleaseLocalPortList (pForbidPorts);
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            UtilPlstReleaseLocalPortList (pPorts);
            UtilPlstReleaseLocalPortList (pForbidPorts);
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qStaticMulticastTable (pu4FsDot1qVlanIndex,
                                                       pFsDot1qStaticMulticastAddress,
                                                       &i4TempRcvPort) ==
            SNMP_SUCCESS)
        {
            if (i4TempRcvPort != 0)
            {
                pRcvPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
                *pi4FsDot1qStaticMulticastReceivePort =
                    pRcvPortEntry->u4IfIndex;
            }
            else
            {
                *pi4FsDot1qStaticMulticastReceivePort = i4TempRcvPort;
            }

            /* since we are having seperate table for portlist, 
             * we have to get the ports mapped to this context */

            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qStaticMulticastStaticEgressPorts
                    (*pu4FsDot1qVlanIndex, *pFsDot1qStaticMulticastAddress,
                     i4TempRcvPort, &SnmpPorts);
                nmhGetDot1qStaticMulticastForbiddenEgressPorts
                    (*pu4FsDot1qVlanIndex, *pFsDot1qStaticMulticastAddress,
                     i4TempRcvPort, &SnmpForbidPorts);

                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pPorts);
                        UtilPlstReleaseLocalPortList (pForbidPorts);
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result2);
                    if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                    {
                        *pi4FsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pPorts);
                        UtilPlstReleaseLocalPortList (pForbidPorts);
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    UtilPlstReleaseLocalPortList (pPorts);
    UtilPlstReleaseLocalPortList (pForbidPorts);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qStaticMcastPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                nextFsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                nextFsDot1qStaticMulticastReceivePort
                FsDot1qTpPort
                nextFsDot1qTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qStaticMcastPortTable (INT4 i4FsDot1qVlanContextId,
                                            INT4 *pi4NextFsDot1qVlanContextId,
                                            UINT4 u4FsDot1qVlanIndex,
                                            UINT4 *pu4NextFsDot1qVlanIndex,
                                            tMacAddr
                                            FsDot1qStaticMulticastAddress,
                                            tMacAddr *
                                            pNextFsDot1qStaticMulticastAddress,
                                            INT4
                                            i4FsDot1qStaticMulticastReceivePort,
                                            INT4
                                            *pi4NextFsDot1qStaticMulticastReceivePort,
                                            INT4 i4FsDot1qTpPort,
                                            INT4 *pi4NextFsDot1qTpPort)
{
    UINT1              *pPorts = NULL;
    UINT1              *pForbidPorts = NULL;
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    INT4                i4TempRcvPort;
    tVlanPortEntry     *pRcvPortEntry = NULL;
    UINT2               u2PortLocalPortId;
    UINT2               u2RcvPortLocalPortId;
    UINT4               u4TempContextId;
    UINT4               u4PrvIfIndex;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    tSNMP_OCTET_STRING_TYPE SnmpForbidPorts;
    UINT2               u2LocalPortId;
    UINT1               u1Result1 = VLAN_FALSE;
    UINT1               u1Result2 = VLAN_FALSE;

    pPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhGetNextIndexFsDot1qStaticMcastPortTable: Error in allocating memory "
                  "for pPorts\r\n");
        return SNMP_FAILURE;
    }

    pForbidPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pForbidPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhGetNextIndexFsDot1qStaticMcastPortTable: Error in allocating memory "
                  "for pForbidPorts\r\n");
        UtilPlstReleaseLocalPortList (pPorts);
        return SNMP_FAILURE;
    }

    MEMSET (pPorts, 0, sizeof (tLocalPortList));
    SnmpPorts.pu1_OctetList = pPorts;
    SnmpPorts.i4_Length = sizeof (tLocalPortList);

    MEMSET (pForbidPorts, 0, sizeof (tLocalPortList));
    SnmpForbidPorts.pu1_OctetList = pForbidPorts;
    SnmpForbidPorts.i4_Length = sizeof (tLocalPortList);

    if (VlanValidateIfIndex (i4FsDot1qVlanContextId,
                             i4FsDot1qTpPort,
                             &u2PortLocalPortId) != VLAN_SUCCESS)
    {
        UtilPlstReleaseLocalPortList (pPorts);
        UtilPlstReleaseLocalPortList (pForbidPorts);
        return SNMP_FAILURE;
    }

    if (VlanValidateIfIndex (i4FsDot1qVlanContextId,
                             i4FsDot1qStaticMulticastReceivePort,
                             &u2RcvPortLocalPortId) != VLAN_SUCCESS)
    {
        UtilPlstReleaseLocalPortList (pPorts);
        UtilPlstReleaseLocalPortList (pForbidPorts);
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            UtilPlstReleaseLocalPortList (pPorts);
            UtilPlstReleaseLocalPortList (pForbidPorts);
            return SNMP_FAILURE;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            UtilPlstReleaseLocalPortList (pPorts);
            UtilPlstReleaseLocalPortList (pForbidPorts);
            return SNMP_FAILURE;
        }

        /* since we are having seperate table for portlist, we have to get 
         * the next port mapped to this context, if it fails only we have 
         * to look for the next index (vlanid) */
        if (VlanGetNextPortInCurrContext ((UINT4) i4FsDot1qTpPort,
                                          &u4IfIndex) == VLAN_SUCCESS)
        {
            nmhGetDot1qStaticMulticastStaticEgressPorts
                (u4FsDot1qVlanIndex, FsDot1qStaticMulticastAddress,
                 i4FsDot1qStaticMulticastReceivePort, &SnmpPorts);
            nmhGetDot1qStaticMulticastForbiddenEgressPorts
                (u4FsDot1qVlanIndex, FsDot1qStaticMulticastAddress,
                 i4FsDot1qStaticMulticastReceivePort, &SnmpForbidPorts);
            do
            {
                if (VlanGetContextInfoFromIfIndex
                    (u4IfIndex, &u4TempContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    UtilPlstReleaseLocalPortList (pPorts);
                    UtilPlstReleaseLocalPortList (pForbidPorts);
                    return SNMP_FAILURE;
                }
                VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result1);
                VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result2);
                if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                {
                    *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                    *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
                    *pu4NextFsDot1qVlanIndex = u4FsDot1qVlanIndex;
                    *pi4NextFsDot1qStaticMulticastReceivePort =
                        i4FsDot1qStaticMulticastReceivePort;
                    VLAN_CPY_MAC_ADDR
                        ((UINT1 *) *pNextFsDot1qStaticMulticastAddress,
                         FsDot1qStaticMulticastAddress);

                    VlanReleaseContext ();
                    UtilPlstReleaseLocalPortList (pPorts);
                    UtilPlstReleaseLocalPortList (pForbidPorts);
                    return SNMP_SUCCESS;
                }
                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                 &u4IfIndex) == VLAN_SUCCESS);
        }

        if (nmhGetNextIndexDot1qStaticMulticastTable (u4FsDot1qVlanIndex,
                                                      pu4NextFsDot1qVlanIndex,
                                                      FsDot1qStaticMulticastAddress,
                                                      pNextFsDot1qStaticMulticastAddress,
                                                      (INT4)
                                                      u2RcvPortLocalPortId,
                                                      &i4TempRcvPort) ==
            SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            if (i4TempRcvPort != 0)
            {
                pRcvPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
                *pi4NextFsDot1qStaticMulticastReceivePort =
                    pRcvPortEntry->u4IfIndex;
            }
            else
            {
                *pi4NextFsDot1qStaticMulticastReceivePort = 0;
            }

            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qStaticMulticastStaticEgressPorts
                    (*pu4NextFsDot1qVlanIndex,
                     *pNextFsDot1qStaticMulticastAddress,
                     i4TempRcvPort, &SnmpPorts);
                nmhGetDot1qStaticMulticastForbiddenEgressPorts
                    (*pu4NextFsDot1qVlanIndex,
                     *pNextFsDot1qStaticMulticastAddress,
                     i4TempRcvPort, &SnmpForbidPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pPorts);
                        UtilPlstReleaseLocalPortList (pForbidPorts);
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result2);
                    if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pPorts);
                        UtilPlstReleaseLocalPortList (pForbidPorts);
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pPorts);
            UtilPlstReleaseLocalPortList (pForbidPorts);
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pPorts);
            UtilPlstReleaseLocalPortList (pForbidPorts);
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qStaticMulticastTable
            (pu4NextFsDot1qVlanIndex,
             pNextFsDot1qStaticMulticastAddress,
             &i4TempRcvPort) == SNMP_SUCCESS)
        {
            if (i4TempRcvPort != 0)
            {
                pRcvPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
                *pi4NextFsDot1qStaticMulticastReceivePort =
                    pRcvPortEntry->u4IfIndex;
            }
            else
            {
                *pi4NextFsDot1qStaticMulticastReceivePort = i4TempRcvPort;
            }
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qStaticMulticastStaticEgressPorts
                    (*pu4NextFsDot1qVlanIndex,
                     *pNextFsDot1qStaticMulticastAddress,
                     i4TempRcvPort, &SnmpPorts);
                nmhGetDot1qStaticMulticastForbiddenEgressPorts
                    (*pu4NextFsDot1qVlanIndex,
                     *pNextFsDot1qStaticMulticastAddress,
                     i4TempRcvPort, &SnmpForbidPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pPorts);
                        UtilPlstReleaseLocalPortList (pForbidPorts);
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result2);
                    if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pPorts);
                        UtilPlstReleaseLocalPortList (pForbidPorts);
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
        }
    }
    while (VLAN_SNMP_TRUE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qVlanNumDeletesTable
 Input       :  The Indices
                FsDot1qVlanContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qVlanNumDeletesTable (INT4 *pi4FsDot1qVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsDot1qVlanContextId = (INT4) u4ContextId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qVlanNumDeletesTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qVlanNumDeletesTable (INT4 i4FsDot1qVlanContextId,
                                           INT4 *pi4NextFsDot1qVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                  &u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qVlanCurrentTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanTimeMark
                FsDot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qVlanCurrentTable (INT4 *pi4FsDot1qVlanContextId,
                                         UINT4 *pu4FsDot1qVlanTimeMark,
                                         UINT4 *pu4FsDot1qVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qVlanCurrentTable (pu4FsDot1qVlanTimeMark,
                                                   pu4FsDot1qVlanIndex) ==
            SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qVlanCurrentTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanTimeMark
                nextFsDot1qVlanTimeMark
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qVlanCurrentTable (INT4 i4FsDot1qVlanContextId,
                                        INT4 *pi4NextFsDot1qVlanContextId,
                                        UINT4 u4FsDot1qVlanTimeMark,
                                        UINT4 *pu4NextFsDot1qVlanTimeMark,
                                        UINT4 u4FsDot1qVlanIndex,
                                        UINT4 *pu4NextFsDot1qVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qVlanCurrentTable (u4FsDot1qVlanTimeMark,
                                                  pu4NextFsDot1qVlanTimeMark,
                                                  u4FsDot1qVlanIndex,
                                                  pu4NextFsDot1qVlanIndex) ==
            SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qVlanCurrentTable
           (pu4NextFsDot1qVlanTimeMark,
            pu4NextFsDot1qVlanIndex) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qVlanEgressPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanTimeMark
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qVlanEgressPortTable (INT4 *pi4FsDot1qVlanContextId,
                                            UINT4 *pu4FsDot1qVlanTimeMark,
                                            UINT4 *pu4FsDot1qVlanIndex,
                                            INT4 *pi4FsDot1qTpPort)
{

    return (nmhGetNextIndexFsDot1qVlanEgressPortTable
            (0, pi4FsDot1qVlanContextId, 0, pu4FsDot1qVlanTimeMark,
             1, pu4FsDot1qVlanIndex, 0, pi4FsDot1qTpPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qVlanEgressPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanTimeMark
                nextFsDot1qVlanTimeMark
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
                FsDot1qTpPort
                nextFsDot1qTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qVlanEgressPortTable (INT4 i4FsDot1qVlanContextId,
                                           INT4 *pi4NextFsDot1qVlanContextId,
                                           UINT4 u4FsDot1qVlanTimeMark,
                                           UINT4 *pu4NextFsDot1qVlanTimeMark,
                                           UINT4 u4FsDot1qVlanIndex,
                                           UINT4 *pu4NextFsDot1qVlanIndex,
                                           INT4 i4FsDot1qTpPort,
                                           INT4 *pi4NextFsDot1qTpPort)
{
    UINT1              *pPorts = NULL;
    UINT1              *pUntagPorts = NULL;
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    UINT4               u4TempContextId;
    UINT4               u4PrvIfIndex;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    tSNMP_OCTET_STRING_TYPE SnmpUntagPorts;
    UINT2               u2LocalPortId;
    UINT1               u1Result1 = VLAN_FALSE;
    UINT1               u1Result2 = VLAN_FALSE;

    pPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhGetFirstIndexFsDot1qStaticMcastPortTable: Error in allocating memory "
                  "for pPorts\r\n");
        return SNMP_FAILURE;
    }

    pUntagPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pUntagPorts == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC, VLAN_NAME,
                  "nmhGetNextIndexFsDot1qVlanEgressPortTable: Error in allocating memory "
                  "for pUntagPorts\r\n");
        UtilPlstReleaseLocalPortList (pPorts);
        return SNMP_FAILURE;
    }

    MEMSET (pPorts, 0, sizeof (tLocalPortList));
    SnmpPorts.pu1_OctetList = pPorts;
    SnmpPorts.i4_Length = sizeof (tLocalPortList);

    MEMSET (pUntagPorts, 0, sizeof (tLocalPortList));
    SnmpUntagPorts.pu1_OctetList = pUntagPorts;
    SnmpUntagPorts.i4_Length = sizeof (tLocalPortList);

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            UtilPlstReleaseLocalPortList (pPorts);
            UtilPlstReleaseLocalPortList (pUntagPorts);
            return SNMP_FAILURE;
        }

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            UtilPlstReleaseLocalPortList (pPorts);
            UtilPlstReleaseLocalPortList (pUntagPorts);
            return SNMP_FAILURE;
        }
        if ((VLAN_ZERO == u4FsDot1qVlanTimeMark) && (1 == u4FsDot1qVlanIndex))
        {
            nmhGetNextIndexDot1qVlanCurrentTable (u4FsDot1qVlanTimeMark,
                                                  pu4NextFsDot1qVlanTimeMark,
                                                  u4FsDot1qVlanIndex,
                                                  pu4NextFsDot1qVlanIndex);
            if (NULL == pu4NextFsDot1qVlanTimeMark)
            {
                VlanReleaseContext ();
                UtilPlstReleaseLocalPortList (pPorts);
                UtilPlstReleaseLocalPortList (pUntagPorts);
                return SNMP_FAILURE;
            }

            u4FsDot1qVlanTimeMark = *pu4NextFsDot1qVlanTimeMark;
        }

        /* since we are having seperate table for portlist, we have to get 
         * the next port mapped to this context, if it fails only we have 
         * to look for the next index (vlanid) */
        if (VlanGetNextPortInCurrContext ((UINT4) i4FsDot1qTpPort,
                                          &u4IfIndex) == VLAN_SUCCESS)
        {
            nmhGetDot1qVlanCurrentEgressPorts (u4FsDot1qVlanTimeMark,
                                               u4FsDot1qVlanIndex, &SnmpPorts);
            nmhGetDot1qVlanCurrentUntaggedPorts (u4FsDot1qVlanTimeMark,
                                                 u4FsDot1qVlanIndex,
                                                 &SnmpUntagPorts);
            do
            {
                if (VlanGetContextInfoFromIfIndex
                    (u4IfIndex, &u4TempContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    UtilPlstReleaseLocalPortList (pPorts);
                    UtilPlstReleaseLocalPortList (pUntagPorts);
                    return SNMP_FAILURE;
                }
                VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result1);
                VLAN_IS_MEMBER_PORT (SnmpUntagPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result2);
                if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                {
                    *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                    *pu4NextFsDot1qVlanTimeMark = u4FsDot1qVlanTimeMark;
                    *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
                    *pu4NextFsDot1qVlanIndex = u4FsDot1qVlanIndex;

                    VlanReleaseContext ();
                    UtilPlstReleaseLocalPortList (pPorts);
                    UtilPlstReleaseLocalPortList (pUntagPorts);
                    return SNMP_SUCCESS;
                }
                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                 &u4IfIndex) == VLAN_SUCCESS);
        }

        while (nmhGetNextIndexDot1qVlanCurrentTable (u4FsDot1qVlanTimeMark,
                                                     pu4NextFsDot1qVlanTimeMark,
                                                     u4FsDot1qVlanIndex,
                                                     pu4NextFsDot1qVlanIndex) ==
               SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qVlanCurrentEgressPorts (*pu4NextFsDot1qVlanTimeMark,
                                                   *pu4NextFsDot1qVlanIndex,
                                                   &SnmpPorts);
                nmhGetDot1qVlanCurrentUntaggedPorts
                    (*pu4NextFsDot1qVlanTimeMark, *pu4NextFsDot1qVlanIndex,
                     &SnmpUntagPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pPorts);
                        UtilPlstReleaseLocalPortList (pUntagPorts);
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    VLAN_IS_MEMBER_PORT (SnmpUntagPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result2);
                    if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pPorts);
                        UtilPlstReleaseLocalPortList (pUntagPorts);
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
            u4FsDot1qVlanIndex = *pu4NextFsDot1qVlanIndex;
            u4FsDot1qVlanTimeMark = *pu4NextFsDot1qVlanTimeMark;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pPorts);
            UtilPlstReleaseLocalPortList (pUntagPorts);
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pPorts);
            UtilPlstReleaseLocalPortList (pUntagPorts);
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qVlanCurrentTable
            (pu4NextFsDot1qVlanTimeMark,
             pu4NextFsDot1qVlanIndex) == SNMP_SUCCESS)
        {
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qVlanCurrentEgressPorts (*pu4NextFsDot1qVlanTimeMark,
                                                   *pu4NextFsDot1qVlanIndex,
                                                   &SnmpPorts);
                nmhGetDot1qVlanCurrentUntaggedPorts
                    (*pu4NextFsDot1qVlanTimeMark, *pu4NextFsDot1qVlanIndex,
                     &SnmpUntagPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pPorts);
                        UtilPlstReleaseLocalPortList (pUntagPorts);
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    VLAN_IS_MEMBER_PORT (SnmpUntagPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result2);
                    if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        UtilPlstReleaseLocalPortList (pPorts);
                        UtilPlstReleaseLocalPortList (pUntagPorts);
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
        }
    }
    while (VLAN_SNMP_TRUE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qVlanStaticTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qVlanStaticTable (INT4 *pi4FsDot1qVlanContextId,
                                        UINT4 *pu4FsDot1qVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qVlanStaticTable
            (pu4FsDot1qVlanIndex) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qVlanStaticTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qVlanStaticTable (INT4 i4FsDot1qVlanContextId,
                                       INT4 *pi4NextFsDot1qVlanContextId,
                                       UINT4 u4FsDot1qVlanIndex,
                                       UINT4 *pu4NextFsDot1qVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qVlanStaticTable (u4FsDot1qVlanIndex,
                                                 pu4NextFsDot1qVlanIndex) ==
            SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;

            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qVlanStaticTable
           (pu4NextFsDot1qVlanIndex) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qVlanStaticPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qVlanStaticPortConfigTable (INT4 *pi4FsDot1qVlanContextId,
                                                  UINT4 *pu4FsDot1qVlanIndex,
                                                  INT4 *pi4FsDot1qTpPort)
{
    return (nmhGetNextIndexFsDot1qVlanStaticPortConfigTable
            (0, pi4FsDot1qVlanContextId, 1, pu4FsDot1qVlanIndex,
             0, pi4FsDot1qTpPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qVlanStaticPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
                FsDot1qTpPort
                nextFsDot1qTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qVlanStaticPortConfigTable (INT4 i4FsDot1qVlanContextId,
                                                 INT4
                                                 *pi4NextFsDot1qVlanContextId,
                                                 UINT4 u4FsDot1qVlanIndex,
                                                 UINT4 *pu4NextFsDot1qVlanIndex,
                                                 INT4 i4FsDot1qTpPort,
                                                 INT4 *pi4NextFsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    UINT4               u4TempContextId;
    UINT4               u4PrvIfIndex;
    UINT4               u4TempVlanId = 0;
    tLocalPortList      Ports;
    tLocalPortList      ForbidPorts;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    tSNMP_OCTET_STRING_TYPE SnmpForbidPorts;
    UINT2               u2LocalPortId;
    UINT1               u1Result1 = VLAN_FALSE;
    UINT1               u1Result2 = VLAN_FALSE;

    MEMSET (Ports, 0, sizeof (tLocalPortList));
    SnmpPorts.pu1_OctetList = Ports;
    SnmpPorts.i4_Length = sizeof (tLocalPortList);

    MEMSET (ForbidPorts, 0, sizeof (tLocalPortList));
    SnmpForbidPorts.pu1_OctetList = ForbidPorts;
    SnmpForbidPorts.i4_Length = sizeof (tLocalPortList);

    if ((VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
        && (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_FALSE))
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        /* Select context is successful and vlan is started on this
         * context. */

        /* since we are having seperate table for portlist, we have to get 
         * the next port mapped to this context, if it fails only we have 
         * to look for the next index (vlanid) */
        if (VlanGetNextPortInCurrContext ((UINT4) i4FsDot1qTpPort,
                                          &u4IfIndex) == VLAN_SUCCESS)
        {
            nmhGetDot1qVlanStaticEgressPorts (u4FsDot1qVlanIndex, &SnmpPorts);
            nmhGetDot1qVlanForbiddenEgressPorts (u4FsDot1qVlanIndex,
                                                 &SnmpForbidPorts);
            do
            {
                if (VlanGetContextInfoFromIfIndex
                    (u4IfIndex, &u4TempContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    return SNMP_FAILURE;
                }
                VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result1);
                VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                     u2LocalPortId, u1Result2);
                if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                {
                    *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                    *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
                    *pu4NextFsDot1qVlanIndex = u4FsDot1qVlanIndex;

                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                 &u4IfIndex) == VLAN_SUCCESS);
        }

        while (nmhGetNextIndexDot1qVlanStaticTable (u4FsDot1qVlanIndex,
                                                    pu4NextFsDot1qVlanIndex) ==
               SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qVlanStaticEgressPorts (*pu4NextFsDot1qVlanIndex,
                                                  &SnmpPorts);
                nmhGetDot1qVlanForbiddenEgressPorts (*pu4NextFsDot1qVlanIndex,
                                                     &SnmpForbidPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result2);
                    if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
            u4FsDot1qVlanIndex = *pu4NextFsDot1qVlanIndex;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

        u4TempVlanId = 0;

        while (nmhGetNextIndexDot1qVlanStaticTable (u4TempVlanId,
                                                    pu4NextFsDot1qVlanIndex) ==
               SNMP_SUCCESS)
        {
            /* we have to get the first port mapped to this context */
            if (VlanGetFirstPortInCurrContext (&u4IfIndex) == VLAN_SUCCESS)
            {
                nmhGetDot1qVlanStaticEgressPorts (*pu4NextFsDot1qVlanIndex,
                                                  &SnmpPorts);
                nmhGetDot1qVlanForbiddenEgressPorts (*pu4NextFsDot1qVlanIndex,
                                                     &SnmpForbidPorts);
                do
                {
                    if (VlanGetContextInfoFromIfIndex
                        (u4IfIndex, &u4TempContextId,
                         &u2LocalPortId) != VLAN_SUCCESS)
                    {
                        VlanReleaseContext ();
                        return SNMP_FAILURE;
                    }
                    VLAN_IS_MEMBER_PORT (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result1);
                    VLAN_IS_MEMBER_PORT (SnmpForbidPorts.pu1_OctetList,
                                         u2LocalPortId, u1Result2);
                    if ((u1Result1 == VLAN_TRUE) || (u1Result2 == VLAN_TRUE))
                    {
                        *pi4NextFsDot1qTpPort = (INT4) u4IfIndex;
                        VlanReleaseContext ();
                        return SNMP_SUCCESS;
                    }
                    u4PrvIfIndex = u4IfIndex;
                }
                while (VlanGetNextPortInCurrContext (u4PrvIfIndex,
                                                     &u4IfIndex) ==
                       VLAN_SUCCESS);
            }
            /* If there is no member port in vlan, then get next vlan. */
            u4TempVlanId = *pu4NextFsDot1qVlanIndex;
        }
    }
    while (VLAN_SNMP_TRUE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qNextFreeLocalVlanIndexTable
 Input       :  The Indices
                FsDot1qVlanContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qNextFreeLocalVlanIndexTable (INT4
                                                    *pi4FsDot1qVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qNextFreeLocalVlanIndexTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qNextFreeLocalVlanIndexTable (INT4 i4FsDot1qVlanContextId,
                                                   INT4
                                                   *pi4NextFsDot1qVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                  &u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qPortVlanTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qPortVlanTable (INT4 *pi4FsDot1dBasePort)
{
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    *pi4FsDot1dBasePort = 0;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex ((UINT4) u4IfIndex,
                                           &u4ContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
        VlanReleaseContext ();
        *pi4FsDot1dBasePort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qPortVlanTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qPortVlanTable (INT4 i4FsDot1dBasePort,
                                     INT4 *pi4NextFsDot1dBasePort)
{
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (VlanGetNextPortInSystem (i4FsDot1dBasePort, &u4IfIndex) == VLAN_SUCCESS)
    {
        if (VlanGetContextInfoFromIfIndex ((UINT4) u4IfIndex,
                                           &u4ContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
        VlanReleaseContext ();
        *pi4NextFsDot1dBasePort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qPortVlanStatisticsTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qPortVlanStatisticsTable (INT4 *pi4FsDot1dBasePort,
                                                UINT4 *pu4FsDot1qVlanIndex)
{
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2LocalPortId;

    if (VlanGetFirstPortInSystem (&u4IfIndex) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        *pi4FsDot1dBasePort = (INT4) u4IfIndex;
        if (VlanGetContextInfoFromIfIndex ((UINT4) *pi4FsDot1dBasePort,
                                           &u4ContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
        {
            if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

            if (pPortEntry != NULL)
            {
                pCurrEntry = VlanGetNextCurrVlanEntry (0, u2LocalPortId);

                if (pCurrEntry != NULL)
                {
                    *pu4FsDot1qVlanIndex = (UINT4) pCurrEntry->VlanId;
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
            }
        }

    }
    while (VlanGetNextPortInSystem (*pi4FsDot1dBasePort,
                                    &u4IfIndex) == VLAN_SUCCESS);
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qPortVlanStatisticsTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qPortVlanStatisticsTable (INT4 i4FsDot1dBasePort,
                                               INT4 *pi4NextFsDot1dBasePort,
                                               UINT4 u4FsDot1qVlanIndex,
                                               UINT4 *pu4NextFsDot1qVlanIndex)
{
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    tVlanCurrEntry     *pCurrEntry;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2LocalPortId;

    do
    {
        u4IfIndex = (UINT4) i4FsDot1dBasePort;
        if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                           &u4ContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
        {
            if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

            if (pPortEntry != NULL)
            {
                pCurrEntry = VlanGetNextCurrVlanEntry (u4FsDot1qVlanIndex,
                                                       u2LocalPortId);
                if (pCurrEntry != NULL)
                {
                    *pi4NextFsDot1dBasePort = (INT4) u4IfIndex;
                    *pu4NextFsDot1qVlanIndex = (UINT4) pCurrEntry->VlanId;
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
            }
        }
        u4FsDot1qVlanIndex = 0;
    }
    while (VlanGetNextPortInSystem (u4IfIndex,
                                    (UINT4 *) &i4FsDot1dBasePort) ==
           VLAN_SUCCESS);

    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qPortVlanHCStatisticsTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qPortVlanHCStatisticsTable (INT4 *pi4FsDot1dBasePort,
                                                  UINT4 *pu4FsDot1qVlanIndex)
{
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2LocalPortId;

    if (VlanGetFirstPortInSystem (&u4IfIndex) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        *pi4FsDot1dBasePort = (INT4) u4IfIndex;
        if (VlanGetContextInfoFromIfIndex ((UINT4) *pi4FsDot1dBasePort,
                                           &u4ContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
        {
            if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

            if ((pPortEntry != NULL) && (pPortEntry->u1IsHCPort == VLAN_TRUE))
            {
                pCurrEntry = VlanGetNextCurrVlanEntry (0, u2LocalPortId);

                if (pCurrEntry != NULL)
                {
                    *pu4FsDot1qVlanIndex = (UINT4) pCurrEntry->VlanId;
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
            }
        }

    }
    while (VlanGetNextPortInSystem (*pi4FsDot1dBasePort,
                                    &u4IfIndex) == VLAN_SUCCESS);
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qPortVlanHCStatisticsTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qPortVlanHCStatisticsTable (INT4 i4FsDot1dBasePort,
                                                 INT4 *pi4NextFsDot1dBasePort,
                                                 UINT4 u4FsDot1qVlanIndex,
                                                 UINT4 *pu4NextFsDot1qVlanIndex)
{
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    tVlanCurrEntry     *pCurrEntry;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2LocalPortId;

    u4IfIndex = (UINT4) i4FsDot1dBasePort;
    do
    {
        if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                           &u4ContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
        {
            if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

            if ((pPortEntry != NULL) && (pPortEntry->u1IsHCPort == VLAN_TRUE))
            {
                pCurrEntry = VlanGetNextCurrVlanEntry (u4FsDot1qVlanIndex,
                                                       u2LocalPortId);
                if (pCurrEntry != NULL)
                {
                    *pi4NextFsDot1dBasePort = (INT4) u4IfIndex;
                    *pu4NextFsDot1qVlanIndex = (UINT4) pCurrEntry->VlanId;
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
            }
        }
        u4FsDot1qVlanIndex = 0;
    }
    while (VlanGetNextPortInSystem (i4FsDot1dBasePort,
                                    &u4IfIndex) == VLAN_SUCCESS);

    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qLearningConstraintsTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qConstraintVlan
                FsDot1qConstraintSet
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qLearningConstraintsTable (INT4 *pi4FsDot1qVlanContextId,
                                                 UINT4
                                                 *pu4FsDot1qConstraintVlan,
                                                 INT4 *pi4FsDot1qConstraintSet)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qLearningConstraintsTable
            (pu4FsDot1qConstraintVlan, pi4FsDot1qConstraintSet) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qLearningConstraintsTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qConstraintVlan
                nextFsDot1qConstraintVlan
                FsDot1qConstraintSet
                nextFsDot1qConstraintSet
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qLearningConstraintsTable (INT4 i4FsDot1qVlanContextId,
                                                INT4
                                                *pi4NextFsDot1qVlanContextId,
                                                UINT4 u4FsDot1qConstraintVlan,
                                                UINT4
                                                *pu4NextFsDot1qConstraintVlan,
                                                INT4 i4FsDot1qConstraintSet,
                                                INT4
                                                *pi4NextFsDot1qConstraintSet)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qLearningConstraintsTable
            (u4FsDot1qConstraintVlan, pu4NextFsDot1qConstraintVlan,
             i4FsDot1qConstraintSet,
             pi4NextFsDot1qConstraintSet) == SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;

            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qLearningConstraintsTable
           (pu4NextFsDot1qConstraintVlan,
            pi4NextFsDot1qConstraintSet) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1qConstraintDefaultTable
 Input       :  The Indices
                FsDot1qVlanContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1qConstraintDefaultTable (INT4 *pi4FsDot1qVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1qConstraintDefaultTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1qConstraintDefaultTable (INT4 i4FsDot1qVlanContextId,
                                              INT4 *pi4NextFsDot1qVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                  &u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1vProtocolGroupTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1vProtocolTemplateFrameType
                FsDot1vProtocolTemplateProtocolValue
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1vProtocolGroupTable (INT4 *pi4FsDot1qVlanContextId,
                                           INT4
                                           *pi4FsDot1vProtocolTemplateFrameType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsDot1vProtocolTemplateProtocolValue)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1vProtocolGroupTable
            (pi4FsDot1vProtocolTemplateFrameType,
             pFsDot1vProtocolTemplateProtocolValue) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1vProtocolGroupTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1vProtocolTemplateFrameType
                nextFsDot1vProtocolTemplateFrameType
                FsDot1vProtocolTemplateProtocolValue
                nextFsDot1vProtocolTemplateProtocolValue
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1vProtocolGroupTable (INT4 i4FsDot1qVlanContextId,
                                          INT4 *pi4NextFsDot1qVlanContextId,
                                          INT4
                                          i4FsDot1vProtocolTemplateFrameType,
                                          INT4
                                          *pi4NextFsDot1vProtocolTemplateFrameType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsDot1vProtocolTemplateProtocolValue,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextFsDot1vProtocolTemplateProtocolValue)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {

        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1vProtocolGroupTable
            (i4FsDot1vProtocolTemplateFrameType,
             pi4NextFsDot1vProtocolTemplateFrameType,
             pFsDot1vProtocolTemplateProtocolValue,
             pNextFsDot1vProtocolTemplateProtocolValue) == SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1vProtocolGroupTable
           (pi4NextFsDot1vProtocolTemplateFrameType,
            pNextFsDot1vProtocolTemplateProtocolValue) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1vProtocolPortTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1vProtocolPortGroupId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1vProtocolPortTable (INT4 *pi4FsDot1dBasePort,
                                          INT4 *pi4FsDot1vProtocolPortGroupId)
{
    return (nmhGetNextIndexFsDot1vProtocolPortTable (0, pi4FsDot1dBasePort,
                                                     0,
                                                     pi4FsDot1vProtocolPortGroupId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1vProtocolPortTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
                FsDot1vProtocolPortGroupId
                nextFsDot1vProtocolPortGroupId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1vProtocolPortTable (INT4 i4FsDot1dBasePort,
                                         INT4 *pi4NextFsDot1dBasePort,
                                         INT4 i4FsDot1vProtocolPortGroupId,
                                         INT4
                                         *pi4NextFsDot1vProtocolPortGroupId)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    tVlanPortEntry     *pNextPortEntry = NULL;
    tVlanPortVidSet    *pVlanPortVidSetEntry = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    UINT1               u1IsEntryFound = VLAN_FALSE;
    UINT2               u2Port = 0;
    UINT2               u2FirstPort = 0;

    if ((i4FsDot1dBasePort < 0) || (i4FsDot1vProtocolPortGroupId < 0))
    {
        return SNMP_FAILURE;
    }

    u2FirstPort = (UINT2) i4FsDot1dBasePort;

    RB_OFFSET_SCAN (VLAN_GLOBAL_PORT_RBTREE (), pVlanPortEntry,
                    pNextPortEntry, tVlanPortEntry *)
    {
        if (pVlanPortEntry->u4IfIndex >= u2FirstPort)
        {
            if (VlanGetContextInfoFromIfIndex
                (pVlanPortEntry->u4IfIndex, &u4ContextId,
                 &u2LocalPortId) != VLAN_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
            {
                continue;
            }
            u2Port = (UINT2) pVlanPortEntry->u4IfIndex;

            VLAN_SLL_SCAN (&pVlanPortEntry->VlanVidSet, pVlanPortVidSetEntry,
                           tVlanPortVidSet *)
            {
                if ((u2FirstPort < u2Port) || ((u2FirstPort == u2Port) &&
                                               (i4FsDot1vProtocolPortGroupId <
                                                (INT4)
                                                pVlanPortVidSetEntry->
                                                u4ProtGrpId)))
                {
                    if (u1IsEntryFound == VLAN_FALSE)
                    {

                        *pi4NextFsDot1dBasePort = (INT4) u2Port;
                        *pi4NextFsDot1vProtocolPortGroupId
                            = pVlanPortVidSetEntry->u4ProtGrpId;
                        u1IsEntryFound = VLAN_TRUE;
                    }
                    else
                    {

                        if ((*pi4NextFsDot1dBasePort > (INT4) u2Port) ||
                            ((*pi4NextFsDot1dBasePort == (INT4) u2Port) &&
                             (*pi4NextFsDot1vProtocolPortGroupId >
                              (INT4) pVlanPortVidSetEntry->u4ProtGrpId)))
                        {

                            *pi4NextFsDot1dBasePort = (INT4) u2Port;
                            *pi4NextFsDot1vProtocolPortGroupId
                                = pVlanPortVidSetEntry->u4ProtGrpId;

                        }
                    }
                }
            }
        }
    }

    if (u1IsEntryFound == VLAN_TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/************************** Proprietary MIB Get Routines ********************/

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanGlobalsTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanGlobalsTable (INT4
                                                 *pi4FsMIDot1qFutureVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIDot1qFutureVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanGlobalsTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                nextFsMIDot1qFutureVlanContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1qFutureVlanGlobalsTable (INT4
                                                i4FsMIDot1qFutureVlanContextId,
                                                INT4
                                                *pi4NextFsMIDot1qFutureVlanContextId)
{
    UINT4               u4ContextId;

    if (VlanGetNextActiveContext ((UINT4) i4FsMIDot1qFutureVlanContextId,
                                  &u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIDot1qFutureVlanContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanPortTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanPortTable (INT4 *pi4FsMIDot1qFutureVlanPort)
{
    UINT4               u4IfIndex;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_SUCCESS)
    {
        *pi4FsMIDot1qFutureVlanPort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanPortTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                nextFsMIDot1qFutureVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1qFutureVlanPortTable (INT4 i4FsMIDot1qFutureVlanPort,
                                             INT4
                                             *pi4NextFsMIDot1qFutureVlanPort)
{
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (VlanGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();

    if (VlanGetNextPortInSystem (i4FsMIDot1qFutureVlanPort,
                                 &u4IfIndex) == VLAN_SUCCESS)
    {
        *pi4NextFsMIDot1qFutureVlanPort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanPortMacMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanPortMacMapTable (INT4
                                                    *pi4FsMIDot1qFutureVlanPort,
                                                    tMacAddr *
                                                    pFsMIDot1qFutureVlanPortMacMapAddr)
{
    INT4                i4VlanFirstPort = 0;
    tMacAddr            MacAddr;

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    if (nmhGetNextIndexFsMIDot1qFutureVlanPortMacMapTable (i4VlanFirstPort,
                                                           pi4FsMIDot1qFutureVlanPort,
                                                           MacAddr,
                                                           pFsMIDot1qFutureVlanPortMacMapAddr)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanPortMacMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                nextFsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr
                nextFsMIDot1qFutureVlanPortMacMapAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1qFutureVlanPortMacMapTable (INT4
                                                   i4FsMIDot1qFutureVlanPort,
                                                   INT4
                                                   *pi4NextFsMIDot1qFutureVlanPort,
                                                   tMacAddr
                                                   FsMIDot1qFutureVlanPortMacMapAddr,
                                                   tMacAddr *
                                                   pNextFsMIDot1qFutureVlanPortMacMapAddr)
{
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4PrevIfIndex = 0;
    UINT2               u2LocalPortId = 0;
    UINT4               u4NextLocalPortId = 0;

    if (i4FsMIDot1qFutureVlanPort != 0)
    {
        u4IfIndex = i4FsMIDot1qFutureVlanPort;

    }
    else
    {
        if (VlanGetFirstPortInSystem (&u4IfIndex) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((nmhGetNextIndexDot1qFutureVlanPortMacMapTable ((INT4) u2LocalPortId,
                                                        (INT4 *)
                                                        &u4NextLocalPortId,
                                                        FsMIDot1qFutureVlanPortMacMapAddr,
                                                        pNextFsMIDot1qFutureVlanPortMacMapAddr)
         == SNMP_SUCCESS))
    {
        if ((UINT2) u4NextLocalPortId >= VLAN_MAX_PORTS + 1)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (u2LocalPortId == (UINT2) u4NextLocalPortId)
        {
            *pi4NextFsMIDot1qFutureVlanPort =
                VLAN_GET_IFINDEX ((UINT2) u4NextLocalPortId);
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        VlanReleaseContext ();

        u4PrevIfIndex = u4IfIndex;

        if (VlanGetNextPortInSystem ((INT4) u4PrevIfIndex, &u4IfIndex) !=
            VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        MEMSET (FsMIDot1qFutureVlanPortMacMapAddr, 0, ETHERNET_ADDR_SIZE);

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qFutureVlanPortMacMapTable ((INT4) u2LocalPortId,
                                                           (INT4 *)
                                                           &u4NextLocalPortId,
                                                           FsMIDot1qFutureVlanPortMacMapAddr,
                                                           pNextFsMIDot1qFutureVlanPortMacMapAddr)
            == SNMP_SUCCESS)
        {
            if (u2LocalPortId == (UINT2) u4NextLocalPortId)
            {
                break;
            }
        }
    }
    while (VLAN_TRUE);

    if ((UINT2) u4NextLocalPortId >= VLAN_MAX_PORTS + 1)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4NextFsMIDot1qFutureVlanPort =
        VLAN_GET_IFINDEX ((UINT2) u4NextLocalPortId);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanFidMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanFidMapTable (INT4
                                                *pi4FsMIDot1qFutureVlanContextId,
                                                UINT4
                                                *pu4FsMIDot1qFutureVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsMIDot1qFutureVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qFutureVlanFidMapTable
            (pu4FsMIDot1qFutureVlanIndex) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext (*pi4FsMIDot1qFutureVlanContextId,
                                     &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanFidMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                nextFsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex
                nextFsMIDot1qFutureVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1qFutureVlanFidMapTable (INT4
                                               i4FsMIDot1qFutureVlanContextId,
                                               INT4
                                               *pi4NextFsMIDot1qFutureVlanContextId,
                                               UINT4 u4FsMIDot1qFutureVlanIndex,
                                               UINT4
                                               *pu4NextFsMIDot1qFutureVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexDot1qFutureVlanFidMapTable
        (u4FsMIDot1qFutureVlanIndex,
         pu4NextFsMIDot1qFutureVlanIndex) == SNMP_SUCCESS)
    {
        *pi4NextFsMIDot1qFutureVlanContextId = i4FsMIDot1qFutureVlanContextId;

        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsMIDot1qFutureVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsMIDot1qFutureVlanContextId = (INT4) u4ContextId;
        *pi4NextFsMIDot1qFutureVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qFutureVlanFidMapTable
           (pu4NextFsMIDot1qFutureVlanIndex) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanTunnelConfigTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanTunnelConfigTable (INT4
                                                      *pi4FsMIDot1qFutureVlanContextId)
{
    UNUSED_PARAM (pi4FsMIDot1qFutureVlanContextId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanTunnelConfigTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                nextFsMIDot1qFutureVlanContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1qFutureVlanTunnelConfigTable (INT4
                                                     i4FsMIDot1qFutureVlanContextId,
                                                     INT4
                                                     *pi4NextFsMIDot1qFutureVlanContextId)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    UNUSED_PARAM (pi4NextFsMIDot1qFutureVlanContextId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanTunnelTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanTunnelTable (INT4
                                                *pi4FsMIDot1qFutureVlanPort)
{

    UNUSED_PARAM (pi4FsMIDot1qFutureVlanPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanTunnelTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                nextFsMIDot1qFutureVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1qFutureVlanTunnelTable (INT4 i4FsMIDot1qFutureVlanPort,
                                               INT4
                                               *pi4NextFsMIDot1qFutureVlanPort)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (pi4NextFsMIDot1qFutureVlanPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanTunnelProtocolTable (INT4
                                                        *pi4FsMIDot1qFutureVlanPort)
{
    return (nmhGetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable
            (0, pi4FsMIDot1qFutureVlanPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                nextFsMIDot1qFutureVlanPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable (INT4
                                                       i4FsMIDot1qFutureVlanPort,
                                                       INT4
                                                       *pi4NextFsMIDot1qFutureVlanPort)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (pi4NextFsMIDot1qFutureVlanPort);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanCounterTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanCounterTable (INT4
                                                 *pi4FsMIDot1qFutureVlanContextId,
                                                 UINT4
                                                 *pu4FsMIDot1qFutureVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsMIDot1qFutureVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qFutureVlanCounterTable
            (pu4FsMIDot1qFutureVlanIndex) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext (*pi4FsMIDot1qFutureVlanContextId,
                                     &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanCounterTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                nextFsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId
                nextFsMIDot1qFutureVlanCounterVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsMIDot1qFutureVlanCounterTable
    (INT4 i4FsMIDot1qFutureVlanContextId,
     INT4 *pi4NextFsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex, UINT4 *pu4NextFsMIDot1qFutureVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexDot1qFutureVlanCounterTable
        (u4FsMIDot1qFutureVlanIndex,
         pu4NextFsMIDot1qFutureVlanIndex) == SNMP_SUCCESS)
    {
        *pi4NextFsMIDot1qFutureVlanContextId = i4FsMIDot1qFutureVlanContextId;

        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsMIDot1qFutureVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsMIDot1qFutureVlanContextId = (INT4) u4ContextId;
        *pi4NextFsMIDot1qFutureVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qFutureVlanCounterTable
           (pu4NextFsMIDot1qFutureVlanIndex) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanUnicastMacControlTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsMIDot1qFutureVlanUnicastMacControlTable
    (INT4 *pi4FsMIDot1qFutureVlanContextId, UINT4 *pu4FsMIDot1qFutureVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsMIDot1qFutureVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qFutureVlanUnicastMacControlTable
            (pu4FsMIDot1qFutureVlanIndex) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext (*pi4FsMIDot1qFutureVlanContextId,
                                     &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanUnicastMacControlTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                nextFsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex
                nextFsMIDot1qFutureVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsMIDot1qFutureVlanUnicastMacControlTable
    (INT4 i4FsMIDot1qFutureVlanContextId,
     INT4 *pi4NextFsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex, UINT4 *pu4NextFsMIDot1qFutureVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexDot1qFutureVlanUnicastMacControlTable
        (u4FsMIDot1qFutureVlanIndex,
         pu4NextFsMIDot1qFutureVlanIndex) == SNMP_SUCCESS)
    {
        *pi4NextFsMIDot1qFutureVlanContextId = i4FsMIDot1qFutureVlanContextId;

        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsMIDot1qFutureVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsMIDot1qFutureVlanContextId = (INT4) u4ContextId;
        *pi4NextFsMIDot1qFutureVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qFutureVlanUnicastMacControlTable
           (pu4NextFsMIDot1qFutureVlanIndex) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* VALIDATE Routines */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qBaseTable
 Input       :  The Indices
                FsDot1qVlanContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qBaseTable (INT4 i4FsDot1qVlanContextId)
{
    if (VLAN_IS_VC_VALID (i4FsDot1qVlanContextId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qFdbTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qFdbTable (INT4 i4FsDot1qVlanContextId,
                                         UINT4 u4FsDot1qFdbId)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFdbTable (u4FsDot1qFdbId);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qTpFdbTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qTpFdbAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qTpFdbTable (INT4 i4FsDot1qVlanContextId,
                                           UINT4 u4FsDot1qFdbId,
                                           tMacAddr FsDot1qTpFdbAddress)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qTpFdbTable
        (u4FsDot1qFdbId, FsDot1qTpFdbAddress);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qTpGroupTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpGroupAddress
                FsDot1qTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qTpGroupTable (INT4 i4FsDot1qVlanContextId,
                                             UINT4 u4FsDot1qVlanIndex,
                                             tMacAddr FsDot1qTpGroupAddress,
                                             INT4 i4FsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qTpGroupTable
        (u4FsDot1qVlanIndex, FsDot1qTpGroupAddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qForwardAllLearntPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qForwardAllLearntPortTable (INT4
                                                          i4FsDot1qVlanContextId,
                                                          UINT4
                                                          u4FsDot1qVlanIndex,
                                                          INT4 i4FsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1qForwardAllTable (u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qForwardAllStatusTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qForwardAllStatusTable (INT4
                                                      i4FsDot1qVlanContextId,
                                                      UINT4 u4FsDot1qVlanIndex)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qForwardAllTable
        (u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qForwardAllPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qForwardAllPortConfigTable (INT4
                                                          i4FsDot1qVlanContextId,
                                                          UINT4
                                                          u4FsDot1qVlanIndex,
                                                          INT4 i4FsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceDot1qForwardAllTable (u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qForwardUnregLearntPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qForwardUnregLearntPortTable (INT4
                                                            i4FsDot1qVlanContextId,
                                                            UINT4
                                                            u4FsDot1qVlanIndex,
                                                            INT4
                                                            i4FsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qForwardUnregisteredTable
        (u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qForwardUnregStatusTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qForwardUnregStatusTable (INT4
                                                        i4FsDot1qVlanContextId,
                                                        UINT4
                                                        u4FsDot1qVlanIndex)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qForwardUnregisteredTable
        (u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qForwardUnregPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qForwardUnregPortConfigTable (INT4
                                                            i4FsDot1qVlanContextId,
                                                            UINT4
                                                            u4FsDot1qVlanIndex,
                                                            INT4
                                                            i4FsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qForwardUnregisteredTable
        (u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qStaticUnicastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qStaticUnicastTable (INT4 i4FsDot1qVlanContextId,
                                                   UINT4 u4FsDot1qFdbId,
                                                   tMacAddr
                                                   FsDot1qStaticUnicastAddress,
                                                   INT4
                                                   i4FsDot1qStaticUnicastReceivePort)
{
    UINT4               u4RcvContextId;
    UINT2               u2RcvLocalPortId;
    INT1                i1RetVal;

    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4RcvContextId, &u2RcvLocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4RcvContextId = (UINT4) i4FsDot1qVlanContextId;
        u2RcvLocalPortId = 0;
    }

    if (u4RcvContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qStaticUnicastTable
        (u4FsDot1qFdbId, FsDot1qStaticUnicastAddress, (INT4) u2RcvLocalPortId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qStaticAllowedToGoTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                FsDot1qTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qStaticAllowedToGoTable (INT4
                                                       i4FsDot1qVlanContextId,
                                                       UINT4 u4FsDot1qFdbId,
                                                       tMacAddr
                                                       FsDot1qStaticUnicastAddress,
                                                       INT4
                                                       i4FsDot1qStaticUnicastReceivePort,
                                                       INT4 i4FsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT4               u4RcvContextId;
    UINT2               u2LocalPortId;
    UINT2               u2RcvLocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4RcvContextId, &u2RcvLocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4RcvContextId = i4FsDot1qVlanContextId;
        u2RcvLocalPortId = (UINT2) i4FsDot1qStaticUnicastReceivePort;
    }

    if ((u4ContextId != (UINT4) i4FsDot1qVlanContextId) ||
        (u4RcvContextId != (UINT4) i4FsDot1qVlanContextId))
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qStaticUnicastTable
        (u4FsDot1qFdbId, FsDot1qStaticUnicastAddress, (INT4) u2RcvLocalPortId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qStaticMulticastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qStaticMulticastTable (INT4
                                                     i4FsDot1qVlanContextId,
                                                     UINT4 u4FsDot1qVlanIndex,
                                                     tMacAddr
                                                     FsDot1qStaticMulticastAddress,
                                                     INT4
                                                     i4FsDot1qStaticMulticastReceivePort)
{
    UINT4               u4RcvContextId;
    UINT2               u2RcvLocalPortId;
    INT1                i1Retval;

    if (i4FsDot1qStaticMulticastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticMulticastReceivePort,
             &u4RcvContextId, &u2RcvLocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (u4RcvContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u2RcvLocalPortId = (UINT2) i4FsDot1qStaticMulticastReceivePort;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1Retval = nmhValidateIndexInstanceDot1qStaticMulticastTable
        (u4FsDot1qVlanIndex, FsDot1qStaticMulticastAddress,
         (INT4) u2RcvLocalPortId);

    VlanReleaseContext ();
    return i1Retval;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qStaticMcastPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                FsDot1qTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qStaticMcastPortTable (INT4
                                                     i4FsDot1qVlanContextId,
                                                     UINT4 u4FsDot1qVlanIndex,
                                                     tMacAddr
                                                     FsDot1qStaticMulticastAddress,
                                                     INT4
                                                     i4FsDot1qStaticMulticastReceivePort,
                                                     INT4 i4FsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT4               u4RcvContextId;
    UINT2               u2LocalPortId;
    UINT2               u2RcvLocalPortId;
    INT1                i1Retval;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4FsDot1qStaticMulticastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticMulticastReceivePort,
             &u4RcvContextId, &u2RcvLocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4RcvContextId = (UINT4) i4FsDot1qVlanContextId;
        u2RcvLocalPortId = 0;
    }

    if ((u4ContextId != (UINT4) i4FsDot1qVlanContextId) ||
        (u4RcvContextId != (UINT4) i4FsDot1qVlanContextId))
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1Retval = nmhValidateIndexInstanceDot1qStaticMulticastTable
        (u4FsDot1qVlanIndex, FsDot1qStaticMulticastAddress,
         (INT4) u2RcvLocalPortId);

    VlanReleaseContext ();
    return i1Retval;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qVlanNumDeletesTable
 Input       :  The Indices
                FsDot1qVlanContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qVlanNumDeletesTable (INT4 i4FsDot1qVlanContextId)
{
    if (VLAN_IS_VC_VALID (i4FsDot1qVlanContextId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qVlanCurrentTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanTimeMark
                FsDot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qVlanCurrentTable (INT4 i4FsDot1qVlanContextId,
                                                 UINT4 u4FsDot1qVlanTimeMark,
                                                 UINT4 u4FsDot1qVlanIndex)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qVlanCurrentTable
        (u4FsDot1qVlanTimeMark, u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qVlanEgressPortTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanTimeMark
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qVlanEgressPortTable (INT4 i4FsDot1qVlanContextId,
                                                    UINT4 u4FsDot1qVlanTimeMark,
                                                    UINT4 u4FsDot1qVlanIndex,
                                                    INT4 i4FsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qVlanCurrentTable
        (u4FsDot1qVlanTimeMark, u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qVlanStaticTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qVlanStaticTable (INT4 i4FsDot1qVlanContextId,
                                                UINT4 u4FsDot1qVlanIndex)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qVlanStaticTable
        (u4FsDot1qVlanIndex);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qVlanStaticPortConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qVlanStaticPortConfigTable (INT4
                                                          i4FsDot1qVlanContextId,
                                                          UINT4
                                                          u4FsDot1qVlanIndex,
                                                          INT4 i4FsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qVlanStaticTable
        (u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qNextFreeLocalVlanIndexTable
 Input       :  The Indices
                FsDot1qVlanContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qNextFreeLocalVlanIndexTable (INT4
                                                            i4FsDot1qVlanContextId)
{
    if (VLAN_IS_VC_VALID (i4FsDot1qVlanContextId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qPortVlanTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qPortVlanTable (INT4 i4FsDot1dBasePort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qPortVlanTable
        ((INT4) u2LocalPortId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qPortVlanStatisticsTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qPortVlanStatisticsTable (INT4 i4FsDot1dBasePort,
                                                        UINT4
                                                        u4FsDot1qVlanIndex)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qPortVlanStatisticsTable
        ((INT4) u2LocalPortId, u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qPortVlanHCStatisticsTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qPortVlanHCStatisticsTable (INT4
                                                          i4FsDot1dBasePort,
                                                          UINT4
                                                          u4FsDot1qVlanIndex)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qPortVlanHCStatisticsTable
        ((INT4) u2LocalPortId, u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qLearningConstraintsTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qConstraintVlan
                FsDot1qConstraintSet
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qLearningConstraintsTable (INT4
                                                         i4FsDot1qVlanContextId,
                                                         UINT4
                                                         u4FsDot1qConstraintVlan,
                                                         INT4
                                                         i4FsDot1qConstraintSet)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qLearningConstraintsTable
        (u4FsDot1qConstraintVlan, i4FsDot1qConstraintSet);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1qConstraintDefaultTable
 Input       :  The Indices
                FsDot1qVlanContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1qConstraintDefaultTable (INT4
                                                       i4FsDot1qVlanContextId)
{
    if (VLAN_IS_VC_VALID (i4FsDot1qVlanContextId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1vProtocolGroupTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1vProtocolTemplateFrameType
                FsDot1vProtocolTemplateProtocolValue
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1vProtocolGroupTable (INT4 i4FsDot1qVlanContextId,
                                                   INT4
                                                   i4FsDot1vProtocolTemplateFrameType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsDot1vProtocolTemplateProtocolValue)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1vProtocolGroupTable
        (i4FsDot1vProtocolTemplateFrameType,
         pFsDot1vProtocolTemplateProtocolValue);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1vProtocolPortTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1vProtocolPortGroupId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1vProtocolPortTable (INT4 i4FsDot1dBasePort,
                                                  INT4
                                                  i4FsDot1vProtocolPortGroupId)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1vProtocolPortTable
        ((INT4) u2LocalPortId, i4FsDot1vProtocolPortGroupId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable (INT4
                                                         i4FsMIDot1qFutureVlanContextId)
{
    if (VLAN_IS_VC_VALID (i4FsMIDot1qFutureVlanContextId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable (INT4
                                                      i4FsMIDot1qFutureVlanPort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFutureVlanPortTable
        ((INT4) u2LocalPortId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanPortMacMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr
 Output      :  The Routines Validates the Given Indices.                                         Returns     :  SNMP_SUCCESS or SNMP_FAILURE                                                     ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanPortMacMapTable (INT4
                                                            i4FsMIDot1qFutureVlanPort,
                                                            tMacAddr
                                                            FsMIDot1qFutureVlanPortMacMapAddr)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFutureVlanPortMacMapTable
        ((INT4) u2LocalPortId, FsMIDot1qFutureVlanPortMacMapAddr);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanFidMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanFidMapTable (INT4
                                                        i4FsMIDot1qFutureVlanContextId,
                                                        UINT4
                                                        u4FsMIDot1qFutureVlanIndex)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFutureVlanFidMapTable
        (u4FsMIDot1qFutureVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelConfigTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelConfigTable (INT4
                                                              i4FsMIDot1qFutureVlanContextId)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelTable (INT4
                                                        i4FsMIDot1qFutureVlanPort)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable (INT4
                                                                i4FsMIDot1qFutureVlanPort)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable (INT4
                                                         i4FsMIDot1qFutureVlanContextId,
                                                         UINT4
                                                         u4FsMIDot1qFutureVlanIndex)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFutureVlanCounterTable
        (u4FsMIDot1qFutureVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanUnicastMacControlTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanUnicastMacControlTable (INT4
                                                                   i4FsMIDot1qFutureVlanContextId,
                                                                   UINT4
                                                                   u4FsMIDot1qFutureVlanIndex)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFutureVlanUnicastMacControlTable
        (u4FsMIDot1qFutureVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/************************** fsmsbext MIB Get Routines ***********************/

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dExtBaseTable
 Input       :  The Indices
                FsDot1dBridgeContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dExtBaseTable (INT4 i4FsDot1dBridgeContextId)
{
    if (VLAN_IS_VC_VALID (i4FsDot1dBridgeContextId) != VLAN_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dExtBaseTable
 Input       :  The Indices
                FsDot1dBridgeContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dExtBaseTable (INT4 *pi4FsDot1dBridgeContextId)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsDot1dBridgeContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dExtBaseTable
 Input       :  The Indices
                FsDot1dBridgeContextId
                nextFsDot1dBridgeContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dExtBaseTable (INT4 i4FsDot1dBridgeContextId,
                                    INT4 *pi4NextFsDot1dBridgeContextId)
{
    UINT4               u4ContextId;

    if (VlanGetNextActiveContext ((UINT4) i4FsDot1dBridgeContextId,
                                  &u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsDot1dBridgeContextId = (INT4) u4ContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dPortCapabilitiesTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dPortCapabilitiesTable (INT4 i4FsDot1dBasePort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1dPortCapabilitiesTable
        ((INT4) u2LocalPortId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dPortCapabilitiesTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dPortCapabilitiesTable (INT4 *pi4FsDot1dBasePort)
{
    UINT4               u4IfIndex;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_SUCCESS)
    {
        *pi4FsDot1dBasePort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dPortCapabilitiesTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dPortCapabilitiesTable (INT4 i4FsDot1dBasePort,
                                             INT4 *pi4NextFsDot1dBasePort)
{
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (VlanGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();

    if (VlanGetNextPortInSystem (i4FsDot1dBasePort, &u4IfIndex) == VLAN_SUCCESS)
    {
        *pi4NextFsDot1dBasePort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dPortPriorityTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dPortPriorityTable (INT4 i4FsDot1dBasePort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1dPortPriorityTable
        ((INT4) u2LocalPortId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dPortPriorityTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dPortPriorityTable (INT4 *pi4FsDot1dBasePort)
{
    UINT4               u4IfIndex;

    if (VlanGetFirstPortInSystem (&u4IfIndex) == VLAN_SUCCESS)
    {
        *pi4FsDot1dBasePort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dPortPriorityTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dPortPriorityTable (INT4 i4FsDot1dBasePort,
                                         INT4 *pi4NextFsDot1dBasePort)
{
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (VlanGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();

    if (VlanGetNextPortInSystem (i4FsDot1dBasePort, &u4IfIndex) == VLAN_SUCCESS)
    {
        *pi4NextFsDot1dBasePort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dUserPriorityRegenTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dUserPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dUserPriorityRegenTable (INT4 i4FsDot1dBasePort,
                                                       INT4
                                                       i4FsDot1dUserPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1dUserPriorityRegenTable
        ((INT4) u2LocalPortId, i4FsDot1dUserPriority);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dUserPriorityRegenTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dUserPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dUserPriorityRegenTable (INT4 *pi4FsDot1dBasePort,
                                               INT4 *pi4FsDot1dUserPriority)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetFirstIndexFsDot1dTrafficClassTable
        (pi4FsDot1dBasePort, pi4FsDot1dUserPriority);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dUserPriorityRegenTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
                FsDot1dUserPriority
                nextFsDot1dUserPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dUserPriorityRegenTable (INT4 i4FsDot1dBasePort,
                                              INT4 *pi4NextFsDot1dBasePort,
                                              INT4 i4FsDot1dUserPriority,
                                              INT4 *pi4NextFsDot1dUserPriority)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetNextIndexFsDot1dTrafficClassTable (i4FsDot1dBasePort,
                                                        pi4NextFsDot1dBasePort,
                                                        i4FsDot1dUserPriority,
                                                        pi4NextFsDot1dUserPriority);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dTrafficClassTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dTrafficClassPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dTrafficClassTable (INT4 i4FsDot1dBasePort,
                                                  INT4
                                                  i4FsDot1dTrafficClassPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1dTrafficClassTable
        ((INT4) u2LocalPortId, i4FsDot1dTrafficClassPriority);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dTrafficClassTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dTrafficClassPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dTrafficClassTable (INT4 *pi4FsDot1dBasePort,
                                          INT4 *pi4FsDot1dTrafficClassPriority)
{
    UINT4               u4IfIndex;
    UINT4               u4ContextId;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2LocalPortId;

    if (VlanGetFirstPortInSystem (&u4IfIndex) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        *pi4FsDot1dBasePort = (INT4) u4IfIndex;
        if (VlanGetContextInfoFromIfIndex ((UINT4) *pi4FsDot1dBasePort,
                                           &u4ContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
        {

            if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

            if (pPortEntry != NULL)
            {
                *pi4FsDot1dTrafficClassPriority = 0;
                VlanReleaseContext ();
                return SNMP_SUCCESS;;
            }
        }
    }
    while (VlanGetNextPortInSystem (*pi4FsDot1dBasePort,
                                    &u4IfIndex) == VLAN_SUCCESS);
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dTrafficClassTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
                FsDot1dTrafficClassPriority
                nextFsDot1dTrafficClassPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dTrafficClassTable (INT4 i4FsDot1dBasePort,
                                         INT4 *pi4NextFsDot1dBasePort,
                                         INT4 i4FsDot1dTrafficClassPriority,
                                         INT4
                                         *pi4NextFsDot1dTrafficClassPriority)
{
    UINT4               u4ContextId;
    UINT4               u4IfIndex;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2LocalPortId;

    do
    {
        u4IfIndex = (UINT4) i4FsDot1dBasePort;
        if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                           &u4ContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) == VLAN_SUCCESS)
        {

            if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }

            if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
            {
                VlanReleaseContext ();
                return SNMP_FAILURE;
            }
            pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

            if (pPortEntry != NULL)
            {
                if (i4FsDot1dTrafficClassPriority < VLAN_MAX_PRIORITY - 1)
                {
                    *pi4NextFsDot1dBasePort = i4FsDot1dBasePort;

                    if (i4FsDot1dTrafficClassPriority < 0)
                    {
                        *pi4NextFsDot1dTrafficClassPriority = 0;
                    }
                    else
                    {
                        *pi4NextFsDot1dTrafficClassPriority =
                            i4FsDot1dTrafficClassPriority + 1;
                    }
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;;
                }
            }
            i4FsDot1dTrafficClassPriority = (-1);
        }
    }
    while (VlanGetNextPortInSystem (u4IfIndex,
                                    (UINT4 *) &i4FsDot1dBasePort) ==
           VLAN_SUCCESS);

    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dPortOutboundAccessPriorityTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dRegenUserPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dPortOutboundAccessPriorityTable (INT4
                                                                i4FsDot1dBasePort,
                                                                INT4
                                                                i4FsDot1dRegenUserPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1dPortOutboundAccessPriorityTable
        ((INT4) u2LocalPortId, i4FsDot1dRegenUserPriority);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dPortOutboundAccessPriorityTable
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dRegenUserPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dPortOutboundAccessPriorityTable (INT4
                                                        *pi4FsDot1dBasePort,
                                                        INT4
                                                        *pi4FsDot1dRegenUserPriority)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetFirstIndexFsDot1dTrafficClassTable
        (pi4FsDot1dBasePort, pi4FsDot1dRegenUserPriority);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dPortOutboundAccessPriorityTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
                FsDot1dRegenUserPriority
                nextFsDot1dRegenUserPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dPortOutboundAccessPriorityTable (INT4 i4FsDot1dBasePort,
                                                       INT4
                                                       *pi4NextFsDot1dBasePort,
                                                       INT4
                                                       i4FsDot1dRegenUserPriority,
                                                       INT4
                                                       *pi4NextFsDot1dRegenUserPriority)
{
    INT1                i1RetVal;

    i1RetVal = nmhGetNextIndexFsDot1dTrafficClassTable (i4FsDot1dBasePort,
                                                        pi4NextFsDot1dBasePort,
                                                        i4FsDot1dRegenUserPriority,
                                                        pi4NextFsDot1dRegenUserPriority);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dTpHCPortTable
 Input       :  The Indices
                FsDot1dTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dTpHCPortTable (INT4 i4FsDot1dTpPort)
{
    UINT4               u4ContextId;
    tVlanPortEntry     *pPortEntry;
    UINT2               u2LocalPortId;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

    if (pPortEntry != NULL)
    {
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dTpHCPortTable
 Input       :  The Indices
                FsDot1dTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dTpHCPortTable (INT4 *pi4FsDot1dTpPort)
{
    UNUSED_PARAM (pi4FsDot1dTpPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dTpHCPortTable
 Input       :  The Indices
                FsDot1dTpPort
                nextFsDot1dTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dTpHCPortTable (INT4 i4FsDot1dTpPort,
                                     INT4 *pi4NextFsDot1dTpPort)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pi4NextFsDot1dTpPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dTpPortOverflowTable
 Input       :  The Indices
                FsDot1dTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dTpPortOverflowTable (INT4 i4FsDot1dTpPort)
{
    UINT4               u4ContextId;
    tVlanPortEntry     *pPortEntry;
    UINT2               u2LocalPortId;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

    if (pPortEntry != NULL)
    {
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dTpPortOverflowTable
 Input       :  The Indices
                FsDot1dTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dTpPortOverflowTable (INT4 *pi4FsDot1dTpPort)
{
    UNUSED_PARAM (pi4FsDot1dTpPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dTpPortOverflowTable
 Input       :  The Indices
                FsDot1dTpPort
                nextFsDot1dTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dTpPortOverflowTable (INT4 i4FsDot1dTpPort,
                                           INT4 *pi4NextFsDot1dTpPort)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pi4NextFsDot1dTpPort);
    return SNMP_FAILURE;
}

PRIVATE INT4
VlanGetStaticUnicastAllowedToGoPorts (UINT4 u4FdbId, tMacAddr MacAddr,
                                      UINT2 u2RcvPort, tLocalPortList Ports)
{
    tVlanTempPortList  *pTmpPortList = NULL;
    tVlanStUcastEntry  *pVlanStUcastEntry = NULL;
    UINT4               u4FidIndex;

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        return VLAN_FAILURE;
    }

    pVlanStUcastEntry = VlanGetStaticUcastEntryWithExactRcvPort
        (u4FidIndex, MacAddr, u2RcvPort);

    if (pVlanStUcastEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    if (pVlanStUcastEntry->u1RowStatus == VLAN_ACTIVE)
    {
        VLAN_MEMCPY (Ports, pVlanStUcastEntry->AllowedToGo,
                     sizeof (tLocalPortList));
    }
    else
    {
        VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId == VLAN_CURR_CONTEXT_ID ()) &&
                (pTmpPortList->u1Type == VLAN_ST_UCAST_TABLE) &&
                (pTmpPortList->PortListTbl.StUcastTbl.u4FdbId == u4FdbId) &&
                (pTmpPortList->PortListTbl.StUcastTbl.u2RcvPort == u2RcvPort) &&
                (VLAN_ARE_MAC_ADDR_EQUAL
                 (pTmpPortList->PortListTbl.StUcastTbl.MacAddr, MacAddr)))
            {
                break;
            }
        }
        if (pTmpPortList == NULL)
        {
            return VLAN_FAILURE;
        }
        VLAN_MEMCPY (Ports, pTmpPortList->PortListTbl.StUcastTbl.AllowedToGo,
                     sizeof (tLocalPortList));
    }
    return VLAN_SUCCESS;
}

PRIVATE INT4
VlanValidateIfIndex (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 *pu2LocalPort)
{
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;

    if (u4IfIndex != 0)
    {
        if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4TempContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return VLAN_FAILURE;
        }
        if (u4TempContextId != u4ContextId)
        {
            return VLAN_FAILURE;
        }
        *pu2LocalPort = u2LocalPortId;
    }
    return VLAN_SUCCESS;
}

PRIVATE INT4
VlanGetNextPortForStUcastEntry (UINT4 u4FdbId, tMacAddr UcastAddress,
                                UINT2 u2RcvPort, INT4 i4Port, INT4 *pi4NextPort)
{
    UINT1              *pPorts = NULL;
    UINT4               u4IfIndex;
    UINT4               u4PrvIfIndex;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanGetNextPortInCurrContext ((UINT4) i4Port,
                                      &u4IfIndex) == VLAN_SUCCESS)
    {
        pPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pPorts == NULL)
        {
            VLAN_TRC (VLAN_MOD_TRC, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      VLAN_NAME,
                      "VlanGetNextPortForStUcastEntry: Error in allocating memory "
                      "for pPorts\r\n");
            return VLAN_FAILURE;
        }
        MEMSET (pPorts, 0, sizeof (tLocalPortList));

        if (VlanGetStaticUnicastAllowedToGoPorts
            (u4FdbId, UcastAddress, u2RcvPort, pPorts) == VLAN_SUCCESS)
        {
            do
            {
                if (VlanGetContextInfoFromIfIndex
                    (u4IfIndex, &u4TempContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    UtilPlstReleaseLocalPortList (pPorts);
                    return VLAN_FAILURE;
                }
                VLAN_IS_MEMBER_PORT (pPorts, u2LocalPortId, u1Result);
                if (u1Result == VLAN_TRUE)
                {
                    *pi4NextPort = (INT4) u4IfIndex;
                    UtilPlstReleaseLocalPortList (pPorts);
                    return VLAN_SUCCESS;
                }
                u4PrvIfIndex = u4IfIndex;
            }
            while (VlanGetNextPortInCurrContext
                   (u4PrvIfIndex, &u4IfIndex) == VLAN_SUCCESS);
        }
        UtilPlstReleaseLocalPortList (pPorts);
    }
    return VLAN_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanTpFdbTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qTpFdbAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanTpFdbTable (INT4
                                                       i4FsDot1qVlanContextId,
                                                       UINT4 u4FsDot1qFdbId,
                                                       tMacAddr
                                                       FsDot1qTpFdbAddress)
{
    return (nmhValidateIndexInstanceFsDot1qTpFdbTable (i4FsDot1qVlanContextId,
                                                       u4FsDot1qFdbId,
                                                       FsDot1qTpFdbAddress));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanTpFdbTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qTpFdbAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanTpFdbTable (INT4 *pi4FsDot1qVlanContextId,
                                               UINT4 *pu4FsDot1qFdbId,
                                               tMacAddr * pFsDot1qTpFdbAddress)
{
    return (nmhGetFirstIndexFsDot1qTpFdbTable (pi4FsDot1qVlanContextId,
                                               pu4FsDot1qFdbId,
                                               pFsDot1qTpFdbAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanTpFdbTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qFdbId
                nextFsDot1qFdbId
                FsDot1qTpFdbAddress
                nextFsDot1qTpFdbAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1qFutureVlanTpFdbTable (INT4 i4FsDot1qVlanContextId,
                                              INT4 *pi4NextFsDot1qVlanContextId,
                                              UINT4 u4FsDot1qFdbId,
                                              UINT4 *pu4NextFsDot1qFdbId,
                                              tMacAddr FsDot1qTpFdbAddress,
                                              tMacAddr *
                                              pNextFsDot1qTpFdbAddress)
{
    return (nmhGetNextIndexFsDot1qTpFdbTable (i4FsDot1qVlanContextId,
                                              pi4NextFsDot1qVlanContextId,
                                              u4FsDot1qFdbId,
                                              pu4NextFsDot1qFdbId,
                                              FsDot1qTpFdbAddress,
                                              pNextFsDot1qTpFdbAddress));
}

/* LOW LEVEL Routines for Table : FsMIDot1qFutureVlanWildCardTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanWildCardTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsMIDot1qFutureVlanWildCardTable
    (INT4 i4FsDot1qVlanContextId, tMacAddr WildCardMacAddress)
{
    UINT1               i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFutureVlanWildCardTable
        (WildCardMacAddress);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanWildCardTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsMIDot1qFutureVlanWildCardTable
    (INT4 *pi4FsDot1qVlanContextId, tMacAddr * pWildCardMacAddress)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qFutureVlanWildCardTable
            (pWildCardMacAddress) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }

        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanWildCardTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                nextFsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress
                nextFsMIDot1qFutureVlanWildCardMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsMIDot1qFutureVlanWildCardTable
    (INT4 i4FsDot1qVlanContextId, INT4 *pi4NextFsDot1qVlanContextId,
     tMacAddr WildCardMacAddress, tMacAddr * pNextWildCardMacAddress)
{
    UINT4               u4ContextId;

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (nmhGetNextIndexDot1qFutureVlanWildCardTable
            (WildCardMacAddress, pNextWildCardMacAddress) == SNMP_SUCCESS)
        {
            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qFutureVlanWildCardTable
           (pNextWildCardMacAddress) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsMIDot1qFutureVlanWildCardPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanWildCardPortTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress
                FsDot1qTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsMIDot1qFutureVlanWildCardPortTable
    (INT4 i4FsDot1qVlanContextId, tMacAddr WildCardMacAddress,
     INT4 i4FsDot1qTpPort)
{
    UINT4               u4RcvContextId;
    UINT2               u2RcvLocalPortId;
    UINT1               i1RetVal;

    if (i4FsDot1qTpPort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qTpPort,
             &u4RcvContextId, &u2RcvLocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4RcvContextId = (UINT4) i4FsDot1qVlanContextId;
        u2RcvLocalPortId = 0;
    }

    if (u4RcvContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFutureVlanWildCardTable
        (WildCardMacAddress);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanWildCardPortTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress
                FsDot1qTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsMIDot1qFutureVlanWildCardPortTable
    (INT4 *pi4FsDot1qVlanContextId, tMacAddr * pWildCardMacAddress,
     INT4 *pi4FsDot1qTpPort)
{
    tMacAddr            MacAddr;

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    return (nmhGetNextIndexFsMIDot1qFutureVlanWildCardPortTable
            (0, pi4FsDot1qVlanContextId, MacAddr, pWildCardMacAddress,
             0, pi4FsDot1qTpPort));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanWildCardPortTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                nextFsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress
                nextFsMIDot1qFutureVlanWildCardMacAddress
                FsDot1qTpPort
                nextFsDot1qTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsMIDot1qFutureVlanWildCardPortTable
    (INT4 i4FsDot1qVlanContextId, INT4 *pi4NextFsDot1qVlanContextId,
     tMacAddr WildCardMacAddress, tMacAddr * pNextWildCardMacAddress,
     INT4 i4FsDot1qTpPort, INT4 *pi4NextFsDot1qTpPort)
{
    UINT4               u4ContextId;
    UINT2               u2TpPortLocalPortId;

    /* Validate the given TpPort */
    if (VlanValidateIfIndex (i4FsDot1qVlanContextId, i4FsDot1qTpPort,
                             &u2TpPortLocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        /* At first we have to scan through the portlist and get the ports,
         * Only when the scan completes we have to look for the next indices
         */

        if (VlanGetNextPortInWildCardEntry (WildCardMacAddress, i4FsDot1qTpPort,
                                            pi4NextFsDot1qTpPort)
            == VLAN_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;

        }
        else
        {
            if (nmhGetNextIndexDot1qFutureVlanWildCardTable
                (WildCardMacAddress, pNextWildCardMacAddress) == SNMP_SUCCESS)
            {

                *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;

                if (VlanGetNextPortInWildCardEntry (*pNextWildCardMacAddress,
                                                    0, pi4NextFsDot1qTpPort)
                    == VLAN_SUCCESS)
                {
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;

                }

            }
        }

    }

    /* We will be here only when all the entries in the wildcard table
     * was scanned for the given context. Next we have look for next Active 
     * context and their wildcard entries. */
    do
    {
        VlanReleaseContext ();
        /* Getting the next Active context */
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

        /* Getting the first wildcard entry */
        if (nmhGetFirstIndexDot1qFutureVlanWildCardTable
            (pNextWildCardMacAddress) == SNMP_SUCCESS)
        {
            /* we have to get the first port present in the portlist 
             * for this entry. */
            if (VlanGetNextPortInWildCardEntry (*pNextWildCardMacAddress, 0,
                                                pi4NextFsDot1qTpPort)
                == VLAN_SUCCESS)
            {
                VlanReleaseContext ();
                return SNMP_SUCCESS;

            }

        }
    }
    while (VLAN_SNMP_TRUE);

}

PRIVATE INT4
VlanGetNextPortInWildCardEntry (tMacAddr WildCardMacAddress, INT4 i4Port,
                                INT4 *pi4NextPort)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;
    UINT4               u4IfIndex;
    UINT4               u4PrvIfIndex;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               u1Result = VLAN_FALSE;

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);
    if (pWildCardEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    if (VlanGetNextPortInCurrContext ((UINT4) i4Port,
                                      &u4IfIndex) == VLAN_SUCCESS)
    {
        do
        {
            if (VlanGetContextInfoFromIfIndex
                (u4IfIndex, &u4TempContextId, &u2LocalPortId) != VLAN_SUCCESS)
            {
                return VLAN_FAILURE;
            }

            VLAN_IS_MEMBER_PORT (pWildCardEntry->EgressPorts,
                                 u2LocalPortId, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                *pi4NextPort = (INT4) u4IfIndex;
                return VLAN_SUCCESS;
            }
            u4PrvIfIndex = u4IfIndex;
        }
        while (VlanGetNextPortInCurrContext
               (u4PrvIfIndex, &u4IfIndex) == VLAN_SUCCESS);
    }
    return VLAN_FAILURE;
}

/* LOW LEVEL Routines for Table : FsMIDot1qFutureStaticUnicastExtnTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureStaticUnicastExtnTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsMIDot1qFutureStaticUnicastExtnTable
    (INT4 i4FsDot1qVlanContextId,
     UINT4 u4FsDot1qFdbId,
     tMacAddr FsDot1qStaticUnicastAddress,
     INT4 i4FsDot1qStaticUnicastReceivePort)
{
    UINT4               u4RcvContextId;
    UINT2               u2RcvLocalPortId;
    INT1                i1RetVal;

    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4RcvContextId, &u2RcvLocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4RcvContextId = i4FsDot1qVlanContextId;
        u2RcvLocalPortId = 0;
    }

    if (u4RcvContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qStaticUnicastTable
        (u4FsDot1qFdbId, FsDot1qStaticUnicastAddress, (INT4) u2RcvLocalPortId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureStaticUnicastExtnTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsMIDot1qFutureStaticUnicastExtnTable
    (INT4 *pi4FsDot1qVlanContextId,
     UINT4 *pu4FsDot1qFdbId,
     tMacAddr * pFsDot1qStaticUnicastAddress,
     INT4 *pi4FsDot1qStaticUnicastReceivePort)
{
    UINT4               u4ContextId;
    INT4                i4TempRcvPort;
    tVlanPortEntry     *pPortEntry;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        *pi4FsDot1qVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qStaticUnicastTable (pu4FsDot1qFdbId,
                                                     pFsDot1qStaticUnicastAddress,
                                                     &i4TempRcvPort) ==
            SNMP_SUCCESS)
        {
            if (i4TempRcvPort != 0)
            {
                pPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
                *pi4FsDot1qStaticUnicastReceivePort = pPortEntry->u4IfIndex;
            }
            else
            {
                *pi4FsDot1qStaticUnicastReceivePort = i4TempRcvPort;
            }
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext
           (*pi4FsDot1qVlanContextId, &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureStaticUnicastExtnTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qFdbId
                nextFsDot1qFdbId
                FsDot1qStaticUnicastAddress
                nextFsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                nextFsDot1qStaticUnicastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsMIDot1qFutureStaticUnicastExtnTable
    (INT4 i4FsDot1qVlanContextId,
     INT4 *pi4NextFsDot1qVlanContextId,
     UINT4 u4FsDot1qFdbId,
     UINT4 *pu4NextFsDot1qFdbId,
     tMacAddr FsDot1qStaticUnicastAddress,
     tMacAddr * pNextFsDot1qStaticUnicastAddress,
     INT4 i4FsDot1qStaticUnicastReceivePort,
     INT4 *pi4NextFsDot1qStaticUnicastReceivePort)
{
    UINT4               u4ContextId;
    INT4                i4TempRcvPort;
    tVlanPortEntry     *pPortEntry = NULL;
    UINT2               u2PortLocalPortId = 0;

    if (VlanValidateIfIndex (i4FsDot1qVlanContextId,
                             i4FsDot1qStaticUnicastReceivePort,
                             &u2PortLocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) == VLAN_SUCCESS)
    {
        if (nmhGetNextIndexDot1qStaticUnicastTable (u4FsDot1qFdbId,
                                                    pu4NextFsDot1qFdbId,
                                                    FsDot1qStaticUnicastAddress,
                                                    pNextFsDot1qStaticUnicastAddress,
                                                    (INT4) u2PortLocalPortId,
                                                    &i4TempRcvPort) ==
            SNMP_SUCCESS)
        {
            if (i4TempRcvPort != 0)
            {
                pPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
                *pi4NextFsDot1qStaticUnicastReceivePort = pPortEntry->u4IfIndex;
            }
            else
            {
                *pi4NextFsDot1qStaticUnicastReceivePort = i4TempRcvPort;
            }

            *pi4NextFsDot1qVlanContextId = i4FsDot1qVlanContextId;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsDot1qVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsDot1qVlanContextId = (INT4) u4ContextId;
        *pi4NextFsDot1qVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qStaticUnicastTable (pu4NextFsDot1qFdbId,
                                                    pNextFsDot1qStaticUnicastAddress,
                                                    &i4TempRcvPort) !=
           SNMP_SUCCESS);

    if (i4TempRcvPort != 0)
    {
        pPortEntry = VLAN_GET_PORT_ENTRY (i4TempRcvPort);
        *pi4NextFsDot1qStaticUnicastReceivePort = pPortEntry->u4IfIndex;
    }
    else
    {
        *pi4NextFsDot1qStaticUnicastReceivePort = i4TempRcvPort;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIDot1qFutureVlanPortSubnetMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapTable (INT4
                                                               i4FsMIDot1qFutureVlanPort,
                                                               UINT4
                                                               u4FsMIDot1qFutureVlanPortSubnetMapAddr)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapTable
        ((INT4) u2LocalPortId, u4FsMIDot1qFutureVlanPortSubnetMapAddr);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanPortSubnetMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsMIDot1qFutureVlanPortSubnetMapTable (INT4
                                                       *pi4FsMIDot1qFutureVlanPort,
                                                       UINT4
                                                       *pu4FsMIDot1qFutureVlanPortSubnetMapAddr)
{
    if (nmhGetNextIndexDot1qFutureVlanPortSubnetMapTable
        (0, pi4FsMIDot1qFutureVlanPort,
         0, pu4FsMIDot1qFutureVlanPortSubnetMapAddr) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                nextFsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr
                nextFsMIDot1qFutureVlanPortSubnetMapAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapTable (INT4
                                                      i4FsMIDot1qFutureVlanPort,
                                                      INT4
                                                      *pi4NextFsMIDot1qFutureVlanPort,
                                                      UINT4
                                                      u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                                      UINT4
                                                      *pu4NextFsMIDot1qFutureVlanPortSubnetMapAddr)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT4               u4IfIndex = VLAN_INIT_VAL;
    UINT4               u4PrevIfIndex = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    UINT4               u4NextLocalPortId = VLAN_INIT_VAL;

    if (i4FsMIDot1qFutureVlanPort != VLAN_INIT_VAL)
    {
        u4IfIndex = i4FsMIDot1qFutureVlanPort;
    }
    else
    {
        if (VlanGetFirstPortInSystem (&u4IfIndex) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((nmhGetNextIndexDot1qFutureVlanPortSubnetMapTable ((INT4) u2LocalPortId,
                                                           (INT4 *)
                                                           &u4NextLocalPortId,
                                                           u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                                           pu4NextFsMIDot1qFutureVlanPortSubnetMapAddr)
         == SNMP_SUCCESS))
    {
        if ((UINT2) u4NextLocalPortId >= VLAN_MAX_PORTS + 1)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (u2LocalPortId == (UINT2) u4NextLocalPortId)
        {
            *pi4NextFsMIDot1qFutureVlanPort =
                VLAN_GET_PHY_PORT ((UINT2) u4NextLocalPortId);
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        VlanReleaseContext ();

        u4PrevIfIndex = u4IfIndex;

        if (VlanGetNextPortInSystem ((INT4) u4PrevIfIndex, &u4IfIndex) !=
            VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIDot1qFutureVlanPortSubnetMapAddr = VLAN_INIT_VAL;

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qFutureVlanPortSubnetMapTable
            ((INT4) u2LocalPortId, (INT4 *) &u4NextLocalPortId,
             u4FsMIDot1qFutureVlanPortSubnetMapAddr,
             pu4NextFsMIDot1qFutureVlanPortSubnetMapAddr) == SNMP_SUCCESS)
        {
            if (u2LocalPortId == (UINT2) u4NextLocalPortId)
            {
                break;
            }
        }
    }
    while (VLAN_TRUE);
    if ((UINT2) u4NextLocalPortId >= VLAN_MAX_PORTS + 1)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4NextFsMIDot1qFutureVlanPort =
        VLAN_GET_PHY_PORT ((UINT2) u4NextLocalPortId);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIDot1qFutureStVlanExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureStVlanExtTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsMIDot1qFutureStVlanExtTable
    (INT4 i4FsDot1qVlanContextId, UINT4 u4FsDot1qVlanIndex)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFutureStVlanExtTable
        (u4FsDot1qVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureStVlanExtTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFutureStVlanExtTable (INT4 *pi4FsDot1qVlanContextId,
                                               UINT4 *pu4FsDot1qVlanIndex)
{
    return (nmhGetFirstIndexFsDot1qVlanStaticTable (pi4FsDot1qVlanContextId,
                                                    pu4FsDot1qVlanIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureStVlanExtTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1qFutureStVlanExtTable (INT4 i4FsDot1qVlanContextId,
                                              INT4 *pi4NextFsDot1qVlanContextId,
                                              UINT4 u4FsDot1qVlanIndex,
                                              UINT4 *pu4NextFsDot1qVlanIndex)
{
    return (nmhGetNextIndexFsDot1qVlanStaticTable (i4FsDot1qVlanContextId,
                                                   pi4NextFsDot1qVlanContextId,
                                                   u4FsDot1qVlanIndex,
                                                   pu4NextFsDot1qVlanIndex));
}

/* LOW LEVEL Routines for Table : FsMIDot1qFutureVlanPortSubnetMapExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapExtTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapExtTable
    (INT4 i4FsMIDot1qFutureVlanPort,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtMask)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapExtTable
        ((INT4) u2LocalPortId, u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
         u4FsMIDot1qFutureVlanPortSubnetMapExtMask);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
    (INT4 *pi4FsMIDot1qFutureVlanPort,
     UINT4 *pu4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 *pu4FsMIDot1qFutureVlanPortSubnetMapExtMask)
{
    if (nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
        (0, pi4FsMIDot1qFutureVlanPort,
         0, pu4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
         0, pu4FsMIDot1qFutureVlanPortSubnetMapExtMask) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                nextFsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                nextFsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask
                nextFsMIDot1qFutureVlanPortSubnetMapExtMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
    (INT4 i4FsMIDot1qFutureVlanPort,
     INT4 *pi4NextFsMIDot1qFutureVlanPort,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 *pu4NextFsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
     UINT4 *pu4NextFsMIDot1qFutureVlanPortSubnetMapExtMask)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT4               u4IfIndex = VLAN_INIT_VAL;
    UINT4               u4PrevIfIndex = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    UINT4               u4NextLocalPortId = VLAN_INIT_VAL;

    if (i4FsMIDot1qFutureVlanPort != VLAN_INIT_VAL)
    {
        u4IfIndex = i4FsMIDot1qFutureVlanPort;
    }
    else
    {
        if (VlanGetFirstPortInSystem (&u4IfIndex) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if ((nmhGetNextIndexDot1qFutureVlanPortSubnetMapExtTable
         ((INT4) u2LocalPortId, (INT4 *) &u4NextLocalPortId,
          u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
          pu4NextFsMIDot1qFutureVlanPortSubnetMapExtAddr,
          u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
          pu4NextFsMIDot1qFutureVlanPortSubnetMapExtMask) == SNMP_SUCCESS))
    {
        if ((UINT2) u4NextLocalPortId >= VLAN_MAX_PORTS + 1)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (u2LocalPortId == (UINT2) u4NextLocalPortId)
        {
            *pi4NextFsMIDot1qFutureVlanPort =
                VLAN_GET_PHY_PORT ((UINT2) u4NextLocalPortId);
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    do
    {
        VlanReleaseContext ();

        u4PrevIfIndex = u4IfIndex;

        if (VlanGetNextPortInSystem ((INT4) u4PrevIfIndex, &u4IfIndex) !=
            VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4FsMIDot1qFutureVlanPortSubnetMapExtAddr = VLAN_INIT_VAL;
        u4FsMIDot1qFutureVlanPortSubnetMapExtMask = VLAN_INIT_VAL;

        if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        if (nmhGetNextIndexDot1qFutureVlanPortSubnetMapExtTable
            ((INT4) u2LocalPortId, (INT4 *) &u4NextLocalPortId,
             u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
             pu4NextFsMIDot1qFutureVlanPortSubnetMapExtAddr,
             u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
             pu4NextFsMIDot1qFutureVlanPortSubnetMapExtMask) == SNMP_SUCCESS)
        {
            if (u2LocalPortId == (UINT2) u4NextLocalPortId)
            {
                break;
            }
        }
    }
    while (VLAN_TRUE);
    if ((UINT2) u4NextLocalPortId >= VLAN_MAX_PORTS + 1)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4NextFsMIDot1qFutureVlanPort =
        VLAN_GET_PHY_PORT ((UINT2) u4NextLocalPortId);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIDot1qFuturePortVlanExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFuturePortVlanExtTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIDot1qFuturePortVlanExtTable (INT4
                                                         i4FsDot1qVlanContextId,
                                                         UINT4
                                                         u4FsDot1qVlanIndex,
                                                         INT4 i4FsDot1qTpPort)
{

    return (nmhValidateIndexInstanceFsDot1qVlanStaticPortConfigTable
            (i4FsDot1qVlanContextId, u4FsDot1qVlanIndex, i4FsDot1qTpPort));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFuturePortVlanExtTable
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFuturePortVlanExtTable (INT4 *pi4FsDot1qVlanContextId,
                                                 UINT4 *pu4FsDot1qVlanIndex,
                                                 INT4 *pi4FsDot1qTpPort)
{

    return (nmhGetFirstIndexFsDot1qVlanStaticPortConfigTable
            (pi4FsDot1qVlanContextId, pu4FsDot1qVlanIndex, pi4FsDot1qTpPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFuturePortVlanExtTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                FsDot1qVlanIndex
                nextFsDot1qVlanIndex
                FsDot1qTpPort
                nextFsDot1qTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIDot1qFuturePortVlanExtTable (INT4 i4FsDot1qVlanContextId,
                                                INT4
                                                *pi4NextFsDot1qVlanContextId,
                                                UINT4 u4FsDot1qVlanIndex,
                                                UINT4 *pu4NextFsDot1qVlanIndex,
                                                INT4 i4FsDot1qTpPort,
                                                INT4 *pi4NextFsDot1qTpPort)
{

    return (nmhGetNextIndexFsDot1qVlanStaticPortConfigTable
            (i4FsDot1qVlanContextId, pi4NextFsDot1qVlanContextId,
             u4FsDot1qVlanIndex, pu4NextFsDot1qVlanIndex,
             i4FsDot1qTpPort, pi4NextFsDot1qTpPort));
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIDot1qFutureVlanLoopbackTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanLoopbackVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanLoopbackTable (INT4
                                                          i4FsMIDot1qFutureVlanContextId,
                                                          UINT4
                                                          u4FsMIDot1qFutureVlanIndex)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1qFutureVlanLoopbackTable
        (u4FsMIDot1qFutureVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIDot1qFutureVlanLoopbackTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanLoopbackVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanLoopbackTable (INT4
                                                  *pi4FsMIDot1qFutureVlanContextId,
                                                  UINT4
                                                  *pu4FsMIDot1qFutureVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanGetFirstActiveContext (&u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VLAN_BASE_BRIDGE_MODE () == DOT_1D_BRIDGE_MODE)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }

        *pi4FsMIDot1qFutureVlanContextId = (INT4) u4ContextId;

        if (nmhGetFirstIndexDot1qFutureVlanLoopbackTable
            (pu4FsMIDot1qFutureVlanIndex) == SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        VlanReleaseContext ();
    }
    while (VlanGetNextActiveContext ((UINT4) *pi4FsMIDot1qFutureVlanContextId,
                                     &u4ContextId) == VLAN_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIDot1qFutureVlanLoopbackTable
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                nextFsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanLoopbackVlanId
                nextFsMIDot1qFutureVlanLoopbackVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsMIDot1qFutureVlanLoopbackTable
    (INT4 i4FsMIDot1qFutureVlanContextId,
     INT4 *pi4NextFsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex, UINT4 *pu4NextFsMIDot1qFutureVlanIndex)
{
    UINT4               u4ContextId;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetNextIndexDot1qFutureVlanLoopbackTable
        (u4FsMIDot1qFutureVlanIndex,
         pu4NextFsMIDot1qFutureVlanIndex) == SNMP_SUCCESS)
    {
        *pi4NextFsMIDot1qFutureVlanContextId = i4FsMIDot1qFutureVlanContextId;

        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    do
    {
        VlanReleaseContext ();
        if (VlanGetNextActiveContext ((UINT4) i4FsMIDot1qFutureVlanContextId,
                                      &u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4FsMIDot1qFutureVlanContextId = (INT4) u4ContextId;
        *pi4NextFsMIDot1qFutureVlanContextId = (INT4) u4ContextId;

    }
    while (nmhGetFirstIndexDot1qFutureVlanLoopbackTable
           (pu4NextFsMIDot1qFutureVlanIndex) != SNMP_SUCCESS);

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
