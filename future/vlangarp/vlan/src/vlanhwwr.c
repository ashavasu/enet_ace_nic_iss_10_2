/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: vlanhwwr.c,v 1.81 2016/10/20 09:20:13 siva Exp $                                                                     */
/* Licensee Aricent Inc., 2005-2006                        */
/*                                                                           */
/*  FILE NAME             : vlanhwwr.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Jul 2006                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains VLAN H/W Wrapper routines.    */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 1.0                                              */
/*  Date(DD/MM/YYYY)      : 26/07/2007                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/
#include "vlaninc.h"
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwSetAgingTime                               */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          set the Aging time Value.                        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual Switch Context Identfier   */
/*                          i4AgingTime - Aging Time.                        */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHwSetAgingTime (UINT4 u4ContextId, INT4 i4AgingTime)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiBrgSetAgingTime (u4ContextId, i4AgingTime) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4AgingTime);
#endif

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwSetShortAgingTime                          */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          set the Aging time Value.                        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual Switch Context Identfier   */
/*                          u2Port      - Interface Index.                   */
/*                          i4AgingTime - Aging Time.                        */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/

INT4
VlanHwSetShortAgingTime (UINT4 u4ContextId, UINT2 u2Port, INT4 i4AgingTime)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetShortAgeout (u4ContextId,
                                          VLAN_GET_PHY_PORT (u2Port),
                                          i4AgingTime) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (i4AgingTime);
#endif

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwResetShortAgingTime                        */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          reset the Aging time Value.                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual Switch Context Identfier   */
/*                          u2Port      - Interface Index.                   */
/*                          i4AgingTime - Aging Time.                        */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/

INT4
VlanHwResetShortAgingTime (UINT4 u4ContextId, UINT2 u2Port, INT4 i4AgingTime)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {

        if (VlanFsMiVlanHwResetShortAgeout
            (u4ContextId, VLAN_GET_PHY_PORT (u2Port),
             i4AgingTime) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (i4AgingTime);
#endif

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetDot1dStats                                */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          get the statistic information.                   */
/*                                                                           */
/*    Input(s)            : i4Port - Interface for which Statistics needed   */
/*                          i1StatType - Statistic Type                      */
/*                                                                           */
/*    Output(s)           : pu4Value - Stats Value.                          */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetDot1dStats (INT4 i4Port, INT1 i1StatType, UINT4 *pu4Value)
{
#ifndef NPAPI_WANTED
    tVlanPortEntry     *pVlanPortEntry = NULL;
#endif
    *pu4Value = 0;
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (CfaFsHwGetStat (VLAN_GET_PHY_PORT (i4Port),
                            i1StatType, pu4Value) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
    }
#else

    if ((VLAN_IS_PORT_VALID (i4Port) == VLAN_FALSE))
    {
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (i4Port);

    if (pVlanPortEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    switch (i1StatType)
    {

        case VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS:

            *pu4Value = VLAN_GET_PORT_ENTRY (i4Port)->u4NoFilterInDiscard;
            break;

        default:
            break;
    }
#endif
    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetStats                                     */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          get the statistic information.                   */
/*                                                                           */
/*    Input(s)            : i4Port - Interface for which Statistics needed   */
/*                          i1StatType - Statistic Type                      */
/*                                                                           */
/*    Output(s)           : pu4Value - Stats Value.                          */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetStats (INT4 i4Port, tVlanId VlanId, INT1 i1StatType, UINT4 *pu4Value)
{
#ifndef NPAPI_WANTED
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanPortInfoPerVlan *pPortInfoPerVlan = NULL;
#endif

    *pu4Value = 0;

#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwGetPortStats (VLAN_CURR_CONTEXT_ID (),
                                        (UINT2) VLAN_GET_PHY_PORT (i4Port),
                                        VlanId, i1StatType,
                                        pu4Value) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    if (VLAN_IS_SW_STATS_ENABLED () == VLAN_TRUE)
    {

        pCurrEntry = VlanGetVlanEntry (VlanId);

        if (pCurrEntry == NULL)
        {
            return VLAN_FAILURE;
        }

        pPortInfoPerVlan = VlanGetPortStatsEntry (pCurrEntry, (UINT2) i4Port);

        if (pPortInfoPerVlan == NULL)
        {
            return VLAN_FAILURE;
        }

        switch (i1StatType)
        {

            case VLAN_STAT_VLAN_PORT_IN_FRAMES:

                *pu4Value = pPortInfoPerVlan->u4InFrames;
                break;

            case VLAN_STAT_VLAN_PORT_OUT_FRAMES:

                *pu4Value = pPortInfoPerVlan->u4OutFrames;
                break;

            case VLAN_STAT_VLAN_PORT_IN_DISCARDS:

                *pu4Value = pPortInfoPerVlan->u4InDiscards;
                break;
#ifdef SYS_HC_WANTED
            case VLAN_STAT_VLAN_PORT_IN_OVERFLOW:

                *pu4Value = pPortInfoPerVlan->u4InOverflow;
                break;

            case VLAN_STAT_VLAN_PORT_OUT_OVERFLOW:

                *pu4Value = pPortInfoPerVlan->u4OutOverflow;
                break;

            case VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW:

                *pu4Value = pPortInfoPerVlan->u4InOverflowDiscards;
                break;
#endif
            default:
                break;
        }
    }
    else
    {
        *pu4Value = 0;
    }
#endif
    return (VLAN_SUCCESS);
}

#ifdef SYS_HC_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetStats64                                   */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          get the statistic information.                   */
/*                                                                           */
/*    Input(s)            : i4Port - Interface for which Statistics needed   */
/*                          i1StatType - Statistic Type                      */
/*                                                                           */
/*    Output(s)           : pu4Value - Stats Value.                          */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetStats64 (INT4 i4Port, tVlanId VlanId, INT1 i1StatType,
                tSNMP_COUNTER64_TYPE * pu8Value)
{

    pu8Value->lsn = 0;
    pu8Value->msn = 0;

#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwGetPortStats64 (VLAN_CURR_CONTEXT_ID (),
                                          VLAN_GET_PHY_PORT (i4Port),
                                          VlanId, i1StatType,
                                          pu8Value) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanPortInfoPerVlan *pPortInfoPerVlan = NULL;

    pCurrEntry = VlanGetVlanEntry (VlanId);

    if (pCurrEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    pPortInfoPerVlan = VlanGetPortStatsEntry (pCurrEntry, (UINT2) i4Port);

    if (pPortInfoPerVlan == NULL)
    {
        return VLAN_FAILURE;
    }

    switch (i1StatType)
    {

        case VLAN_STAT_VLAN_PORT_IN_FRAMES:

            pu8Value->lsn = pPortInfoPerVlan->u4InFrames;
            pu8Value->msn = pPortInfoPerVlan->u4InOverflow;
            break;

        case VLAN_STAT_VLAN_PORT_OUT_FRAMES:

            pu8Value->lsn = pPortInfoPerVlan->u4OutFrames;
            pu8Value->msn = pPortInfoPerVlan->u4OutOverflow;
            break;

        case VLAN_STAT_VLAN_PORT_IN_DISCARDS:

            pu8Value->lsn = pPortInfoPerVlan->u4InDiscards;
            pu8Value->msn = pPortInfoPerVlan->u4InOverflowDiscards;
            break;

        default:
            break;
    }
#endif
    return (VLAN_SUCCESS);
}
#endif
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwSetDefGroupInfo                            */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          set the default group information.               */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual Switch Context Identfier   */
/*                          VlanId - Vlan identifier number                  */
/*                          u1Type  - All Groups / Unreg Groups              */
/*                          HwPortList - Port List                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHwSetDefGroupInfo (UINT4 u4ContextId, tVlanId VlanId,
                       UINT1 u1Type, tLocalPortList HwPortList)
{
#ifdef NPAPI_WANTED
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));
        VlanConvertToIfPortList (HwPortList, PortList);

        if (u1Type == VLAN_ALL_GROUPS)
        {
            if (FsMiWrVlanHwSetAllGroupsPorts (u4ContextId, VlanId,
                                               PortList) != FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }
        }
        else
        {
            if (FsMiWrVlanHwSetUnRegGroupsPorts (u4ContextId, VlanId,
                                                 PortList) != FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }
        }
#else
        if (u1Type == VLAN_ALL_GROUPS)
        {
            if (FsMiWrVlanHwSetAllGroupsPorts (u4ContextId, VlanId,
                                               HwPortList) != FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }
        }
        else
        {
            if (FsMiWrVlanHwSetUnRegGroupsPorts (u4ContextId, VlanId,
                                                 HwPortList) != FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }
        }
#endif
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (HwPortList);
#endif

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwResetDefGroupInfo                          */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          reset the default group information.             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual Switch Context Identfier   */
/*                          VlanId - Vlan identifier number                  */
/*                          u1Type  - All Groups / Unreg Groups              */
/*                          PortList - Port List                             */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHwResetDefGroupInfo (UINT4 u4ContextId, tVlanId VlanId,
                         UINT1 u1Type, tLocalPortList HwPortList)
{
#ifdef NPAPI_WANTED
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));
        VlanConvertToIfPortList (HwPortList, PortList);

        if (u1Type == VLAN_ALL_GROUPS)
        {
            if (FsMiWrVlanHwResetAllGroupsPorts (u4ContextId, VlanId,
                                                 PortList) != FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }
        }
        else
        {
            if (FsMiWrVlanHwResetUnRegGroupsPorts (u4ContextId, VlanId,
                                                   PortList) != FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }
        }
#else
        if (u1Type == VLAN_ALL_GROUPS)
        {
            if (FsMiWrVlanHwResetAllGroupsPorts (u4ContextId, VlanId,
                                                 HwPortList) != FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }
        }
        else
        {
            if (FsMiWrVlanHwResetUnRegGroupsPorts (u4ContextId, VlanId,
                                                   HwPortList) != FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }
        }
#endif
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (HwPortList);
#endif

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwAddStMcastEntry                            */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          add an multicast entry in the hw if Vlan is      */
/*                          enabled                                          */
/*                                                                           */
/*    Input(s)            : u4RcvPort -- Recieve Port                        */
/*                          VlanId - Vlan identifier number                  */
/*                          MacAddr - Multicast address                      */
/*                          HwPortList - Port List                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHwAddStMcastEntry (tVlanId VlanId, tMacAddr MacAddr, UINT4 u4RcvPort,
                       tLocalPortList HwPortList)
{
#ifdef NPAPI_WANTED
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));
        VlanConvertToIfPortList (HwPortList, PortList);

        if (FsMiWrVlanHwAddStMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                         MacAddr, VLAN_GET_PHY_PORT (u4RcvPort),
                                         PortList) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
#else
        if (FsMiWrVlanHwAddStMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                         MacAddr, VLAN_GET_PHY_PORT (u4RcvPort),
                                         HwPortList) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
#endif
    }
#else
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (HwPortList);
#endif
    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : FsMiWrVlanHwAddMcastEntry                        */
/*                                                                           */
/*    Description         : This function adds an entry to the hardware      */
/*                          Multicast table.                                 */
/*                          This function can be called, when the Multicast  */
/*                          entry is already present in the Hardware. In     */
/*                          which case the Old Multicast member ports        */
/*                          must be removed and the new ports ('PortBmp')    */
/*                          must be added in the Hardware.                   */
/*                                                                           */
/*    Input(s)            : VlanId   - VlanId                                */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                          PortBmp - List of Ports.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*****************************************************************************/
INT4
VlanHwAddMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                     tLocalPortList HwPortList)
{
#ifdef NPAPI_WANTED
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));
        VlanConvertToIfPortList (HwPortList, PortList);

        if (FsMiWrVlanHwAddMcastEntry (u4ContextId, VlanId,
                                       MacAddr, PortList) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
#else
        if (FsMiWrVlanHwAddMcastEntry (u4ContextId, VlanId,
                                       MacAddr, HwPortList) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
#endif
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (HwPortList);
#endif
    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwAddVlanEntry                               */
/*                                                                           */
/*    Description         : This function calls a hardware api to add a      */
/*                          Vlan entry in the hw.                            */
/*                          When portlist is not supported,the ports         */
/*                          are specifically set/reset                       */
/*                                                                           */
/*    Input(s)            : VlanId  - Vlan identifier number                 */
/*                          HwPortList  - Member ports of VLAN               */
/*                          HwUnTagPorts - Untagged ports of VLAN            */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_SUCCESS / VLAN_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHwAddVlanEntry (tVlanId VlanId, tLocalPortList HwPortList,
                    tLocalPortList HwUnTagPorts)
{
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal = VLAN_FAILURE;
#if (VLAN_HW_PORTLIST_SUPPORTED () == VLAN_FALSE)
    INT4                i4RetVal = VLAN_FAILURE;
    UINT2               u2Port;
    UINT1               u1IsMemberPort = VLAN_FALSE;
    UINT1               u1IsNewMemberPort = VLAN_FALSE;
    UINT1               u1IsUnTagPort = VLAN_FALSE;
    UINT1               u1IsNewUnTagPort = VLAN_FALSE;
    UINT1               u1IsLearnedPort = VLAN_FALSE;
    tVlanCurrEntry     *pVlanEntry = NULL;
    tHwMiVlanEntry      HwVlanEntry;
#endif
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
    tPortList           UnTagPortList;

    MEMSET (PortList, 0, sizeof (tPortList));
    MEMSET (UnTagPortList, 0, sizeof (tPortList));

    VlanConvertToIfPortList (HwPortList, PortList);
    VlanConvertToIfPortList (HwUnTagPorts, UnTagPortList);
#endif

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return VLAN_SUCCESS;
    }

    /* If port list is supported , then FsMiWrVlanHwAddVlanEntry is called to
     * add a new vlan entry or modify the member/untag ports.
     * If Port list is not supported,then FsMiWrVlanHwAddVlanEntry is called
     * only for adding a new vlan and VlanHwSetmemberPort and 
     * VlanHwResetMemberPort are called to modify member/untag ports of 
     * existing vlan.
     */
#if (VLAN_HW_PORTLIST_SUPPORTED () == VLAN_TRUE)
#ifdef NP_BACKWD_COMPATIBILITY
    i4HwRetVal = FsMiWrVlanHwAddVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                           PortList, UnTagPortList);
#else
    i4HwRetVal = FsMiWrVlanHwAddVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                           HwPortList, HwUnTagPorts);
#endif
    if (i4HwRetVal == FNP_SUCCESS)
    {
        return VLAN_SUCCESS;
    }
    else
    {
        return VLAN_FAILURE;
    }
#else

    /* When port list is not supported , FsMiVlanHwAddVlanEntry should be 
     * called when vlan is added to h/w for the first time.
     * For further modifications  VlanHwSetVlanMemberPort() or 
     * VlanHwResetVlanMemberPort could be called
     */

    MEMSET (&HwVlanEntry, 0, sizeof (tHwMiVlanEntry));

    i4HwRetVal =
        VlanHwGetVlanInfo (VLAN_CURR_CONTEXT_ID (), VlanId, &HwVlanEntry);
    if (i4HwRetVal == VLAN_FAILURE)
    {
#ifdef NP_BACKWD_COMPATIBILITY
        i4HwRetVal = FsMiWrVlanHwAddVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                               PortList, UnTagPortList);
#else
        i4HwRetVal = FsMiWrVlanHwAddVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                               HwPortList, HwUnTagPorts);
#endif
        if (i4HwRetVal == FNP_SUCCESS)
        {
            return VLAN_SUCCESS;
        }
        else
        {
            return VLAN_FAILURE;
        }
    }

    pVlanEntry = VlanGetVlanEntry (VlanId);

    if (pVlanEntry == NULL)
    {
        return VLAN_SUCCESS;
    }

    VLAN_SCAN_PORT_TABLE (u2Port)
    {
        VLAN_IS_CURR_EGRESS_PORT (pVlanEntry, u2Port, u1IsMemberPort);

        VLAN_IS_MEMBER_PORT (HwPortList, u2Port, u1IsNewMemberPort);

        if (pVlanEntry->pStVlanEntry != NULL)
        {
            VLAN_IS_UNTAGGED_PORT (pVlanEntry->pStVlanEntry,
                                   u2Port, u1IsUnTagPort);

            VLAN_IS_MEMBER_PORT (HwUnTagPorts, u2Port, u1IsNewUnTagPort);
        }

        VLAN_IS_CURR_LEARNT_PORT (pVlanEntry, u2Port, u1IsLearnedPort);

        if ((u1IsNewMemberPort == VLAN_TRUE) && (u1IsMemberPort == VLAN_FALSE))
        {
            if (pVlanEntry->pStVlanEntry != NULL)
            {
                if (u1IsNewUnTagPort == VLAN_TRUE)
                {
                    /* we add a new static untag member port here */
                    i4RetVal = VlanHwSetVlanMemberPort (VlanId, u2Port,
                                                        VLAN_FALSE);
                }
                else
                {
                    /* we add a new static tag member port here */
                    i4RetVal = VlanHwSetVlanMemberPort (VlanId, u2Port,
                                                        VLAN_TRUE);
                }
            }
            else
            {
                /* when a dynamically learnt vlan is learnt on a 
                 * new port ,we add that port to vlan mem ports here
                 */
                i4RetVal = VlanHwSetVlanMemberPort (VlanId, u2Port, VLAN_TRUE);
            }

            if (i4RetVal == VLAN_FAILURE)
            {
                return VLAN_FAILURE;
            }
        }
        else if ((u1IsNewMemberPort == VLAN_FALSE) &&
                 (u1IsMemberPort == VLAN_TRUE))
        {

            /* the port doesnt exist in the new member port list ,so 
             * it is reset from hardware
             */
            i4RetVal = VlanHwResetVlanMemberPort (VlanId, u2Port);

            if (i4RetVal == VLAN_FAILURE)
            {
                return VLAN_FAILURE;
            }

        }
        else if ((u1IsNewMemberPort == VLAN_TRUE) &&
                 (u1IsMemberPort == VLAN_TRUE))
        {
            if (pVlanEntry->pStVlanEntry != NULL)
            {
                /* this section deals with modifying an untag member 
                 * port as tagged or vice versa*/
                if ((u1IsNewUnTagPort == VLAN_TRUE) &&
                    (u1IsUnTagPort == VLAN_FALSE))
                {
                    /* for setting a untag port as tagged port or vice versa 
                     * the port should be reset from h/w and added again
                     * as tag or untag */

                    i4RetVal = VlanHwResetVlanMemberPort (VlanId, u2Port);

                    if (i4RetVal == VLAN_FAILURE)
                    {
                        return VLAN_FAILURE;
                    }

                    i4RetVal = VlanHwSetVlanMemberPort (VlanId, u2Port,
                                                        VLAN_FALSE);
                    if (i4RetVal == VLAN_FAILURE)
                    {
                        return VLAN_FAILURE;
                    }
                }
                else if ((u1IsNewUnTagPort == VLAN_FALSE) &&
                         (u1IsUnTagPort == VLAN_TRUE))
                {

                    i4RetVal = VlanHwResetVlanMemberPort (VlanId, u2Port);

                    if (i4RetVal == VLAN_FAILURE)
                    {
                        return VLAN_FAILURE;
                    }

                    i4RetVal = VlanHwSetVlanMemberPort (VlanId, u2Port,
                                                        VLAN_TRUE);
                    if (i4RetVal == VLAN_FAILURE)
                    {
                        return VLAN_FAILURE;
                    }
                }
                else if ((u1IsLearnedPort == VLAN_TRUE) &&
                         (u1IsNewUnTagPort == VLAN_TRUE))
                {
                    /*
                     * if the port is already added to h/w through
                     * dynamic learning and is now added statically
                     * as an untag port the port has to be set as 
                     * untag port in h/w
                     */
                    i4RetVal = VlanHwResetVlanMemberPort (VlanId, u2Port);

                    if (i4RetVal == VLAN_FAILURE)
                    {
                        return VLAN_FAILURE;
                    }

                    i4RetVal = VlanHwSetVlanMemberPort (VlanId, u2Port,
                                                        VLAN_FALSE);
                    if (i4RetVal == VLAN_FAILURE)
                    {
                        return VLAN_FAILURE;
                    }
                }
            }
        }
    }

    return (VLAN_SUCCESS);
#endif
#else
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (HwPortList);
    UNUSED_PARAM (HwUnTagPorts);
#endif

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : VlanHwAddStaticUcastEntry                        */
/*                                                                           */
/*    Description         : This function calls a hardware api to add an     */
/*                          static unicast entry in the h/w unicast table    */
/*                          when VLAN is enabled                             */
/*                                                                           */
/*    Input(s)            : u4Fid            - Fid                           */
/*                          MacAddr         - Static Mac Address.            */
/*                          u4IfIndex        - Received Interface Index      */
/*                          AllowedToGoPortBmp - PortList                    */
/*                          u1Status         -  Status of entry takes values */
/*                          like  permanent, deleteOnReset and               */
/*                           deleteOnTimeout                                 */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_SUCCESS / VLAN_FAILURE                */
/*****************************************************************************/
INT4
VlanHwAddStaticUcastEntry (UINT4 u4Fid, tMacAddr MacAddress, UINT4 u4RcvPort,
                           tLocalPortList AllowedToGoPort, UINT1 u1Status,
                           tMacAddr ConnectionId)
{
#ifdef NPAPI_WANTED
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));
        VlanConvertToIfPortList (AllowedToGoPort, PortList);

        if (FsMiWrVlanHwAddStaticUcastEntry (VLAN_CURR_CONTEXT_ID (),
                                             u4Fid, MacAddress,
                                             VLAN_GET_PHY_PORT (u4RcvPort),
                                             PortList, u1Status,
                                             ConnectionId) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
#else
        if (FsMiWrVlanHwAddStaticUcastEntry (VLAN_CURR_CONTEXT_ID (),
                                             u4Fid, MacAddress,
                                             VLAN_GET_PHY_PORT (u4RcvPort),
                                             AllowedToGoPort, u1Status,
                                             ConnectionId) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
#endif
    }
#else
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddress);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (AllowedToGoPort);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (ConnectionId);
#endif
    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*    Function Name             : VlanHwDelStMcastEntry                      */
/*                                                                           */
/*    Description               : This function calls the h/w api to delete  */
/*                                the multicast entry in the multicast table */
/*                                when Vlan is enabled                       */
/*                                                                           */
/*    Input(s)                  : VlanId   - VlanId                          */
/*                                MacAddr -  Multicast Mac Address.          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_SUCCESS / VLAN_FAILURE                */
/*****************************************************************************/
INT4
VlanHwDelStMcastEntry (tVlanId VlanId, tMacAddr MacAddr, UINT4 u4RcvPort)
{
    UNUSED_PARAM (u4RcvPort);
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                         MacAddr) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
    }
#else
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (MacAddr);
#endif

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*    Function Name             : VlanHwDelMcastEntry                        */
/*                                                                           */
/*    Description               : This function calls the h/w api to delete  */
/*                                the multicast entry in the multicast table */
/*                                when Vlan is enabled                       */
/*                                                                           */
/*    Input(s)                  : VlanId   - VlanId                          */
/*                                MacAddr -  Multicast Mac Address.          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_SUCCESS / VLAN_FAILURE                */
/*****************************************************************************/
INT4
VlanHwDelMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwDelMcastEntry (u4ContextId,
                                         VlanId, MacAddr) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
#endif

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : VlanHwDelVlanEntry                               */
/*                                                                           */
/*    Description         : This function calls an h/w api for deleting the  */
/*                          an vlan entry from the h/w database when Vlan is */
/*                          enabled                                          */
/*                                                                           */
/*    Input(s)            : VlanId     - VlanId                              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns                 : VLAN_SUCCESS / VLAN_FAILURE                  */
/*****************************************************************************/
INT4
VlanHwDelVlanEntry (UINT4 u4ContextId, tVlanId VlanId)
{
#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanId <= VLAN_VFI_MIN_ID)
        {
            i4RetVal = VlanFsMiVlanHwDelVlanEntry (u4ContextId, VlanId);
            if (i4RetVal != FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }

        }
        else if (VLAN_VFI_MAX_ID == VLAN_VFI_MIN_ID)
        {
            /* No VPLS VLAN exist */
            i4RetVal = FNP_FAILURE;
        }
        else
        {
            /* VLAN ID greater than VLAN_VFI_MIN_ID is for VPLS VLAN.
               So returning success */
            i4RetVal = FNP_SUCCESS;
        }

    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
#endif

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : VlanHwDelStaticUcastEntry                        */
/*                                                                           */
/*    Description         : This function calls a h/w api for deleting a     */
/*                          static unicast entry in the hardware iff Vlan is */
/*                          enabled                                          */
/*                                                                           */
/*    Input(s)            : u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u4IfIndex        - Received Interface Index      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns             :VLAN_SUCCESS/VLAN_FAILURE                         */
/*****************************************************************************/
INT4
VlanHwDelStaticUcastEntry (UINT4 u4Fid, tMacAddr MacAddr, UINT4 u4IfIndex)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwDelStaticUcastEntry (VLAN_CURR_CONTEXT_ID (), u4Fid,
                                               MacAddr,
                                               VLAN_GET_PHY_PORT (u4IfIndex))
            != FNP_SUCCESS)
        {

            return (VLAN_FAILURE);
        }
    }
#else

    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4IfIndex);
#endif

    return (VLAN_SUCCESS);
}

/**************************************************************************/
/* Function Name       : VlanHwAddDynVlanEntry                            */
/*                                                                        */
/* Description         : This function invokes NPAPI to add the VLAN      */
/*                       entry to the hardware. This function should be   */
/*                       invoked only for dynamic VLANs that are learnt   */
/*                       newly through GVRP.                              */
/*                                                                        */
/* Input(s)            : VlanId - VLAN to be added.                       */
/*                       u2Port - Port number to be added.                */
/*                                                                        */
/* Output(s)           : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                        */
/**************************************************************************/
INT4
VlanHwAddDynVlanEntry (tVlanId VlanId, UINT2 u2Port)
{
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;
    tHwMiVlanEntry      HwVlanEntry;
    tLocalPortList      HwPortList;
    tLocalPortList      HwUnTagPorts;
    UINT1               u1Result;
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
    tPortList           UnTagPortList;
#endif

    i4HwRetVal = FNP_SUCCESS;

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return (VLAN_SUCCESS);
    }

    if (VlanRedIsRelearningInProgress () != VLAN_TRUE)
    {
        /* Relearning either not started or it is completed */
        MEMSET (HwPortList, 0, sizeof (tLocalPortList));
        MEMSET (HwUnTagPorts, 0, sizeof (tLocalPortList));

        VLAN_SET_MEMBER_PORT (HwPortList, u2Port);
#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));
        MEMSET (UnTagPortList, 0, sizeof (tPortList));

        VlanConvertToIfPortList (HwPortList, PortList);
        VlanConvertToIfPortList (HwUnTagPorts, UnTagPortList);

        i4HwRetVal =
            FsMiWrVlanHwAddVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                      PortList, UnTagPortList);
#else
        i4HwRetVal =
            FsMiWrVlanHwAddVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                      HwPortList, HwUnTagPorts);
#endif
    }
    else
    {
        /* 
         * relearning not over...hence there might be a possibility that
         * the entry could already exist in the h/w. i.e lets say VLAN 10
         * is present in the hardware with members = P1, P2 and P3. During
         * relearning if FsMiWrVlanHwAddVlanEntry is invoked for P1 alone, then
         * the member port list will be overwritten with P1 only which will
         * result in data traffic loss for P2 and P3 till they are programmed.
         * Hence during relearning we will get the entry from the h/w and 
         * based on that we will program the hardware.
         */
        MEMSET (&HwVlanEntry, 0, sizeof (tHwMiVlanEntry));

        i4HwRetVal =
            VlanHwGetVlanInfo (VLAN_CURR_CONTEXT_ID (), VlanId, &HwVlanEntry);

        if (i4HwRetVal == FNP_SUCCESS)
        {
            /* Entry already present in hardware. Do add port */
            VLAN_IS_MEMBER_PORT (HwVlanEntry.HwMemberPbmp, u2Port, u1Result);

            if (u1Result != VLAN_TRUE)
            {
                /* new member of the VLAN */
                i4HwRetVal
                    =
                    VlanFsMiVlanHwSetVlanMemberPort (VLAN_CURR_CONTEXT_ID (),
                                                     VlanId,
                                                     VLAN_GET_PHY_PORT (u2Port),
                                                     VLAN_TRUE);
            }
        }
        else
        {
            /* fresh entry */
            MEMSET (HwPortList, 0, sizeof (tLocalPortList));
            MEMSET (HwUnTagPorts, 0, sizeof (tLocalPortList));

            VLAN_SET_MEMBER_PORT (HwPortList, u2Port);
#ifdef NP_BACKWD_COMPATIBILITY
            MEMSET (PortList, 0, sizeof (tPortList));
            MEMSET (UnTagPortList, 0, sizeof (tPortList));

            VlanConvertToIfPortList (HwPortList, PortList);
            VlanConvertToIfPortList (HwUnTagPorts, UnTagPortList);

            i4HwRetVal =
                FsMiWrVlanHwAddVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                          PortList, UnTagPortList);
#else
            i4HwRetVal =
                FsMiWrVlanHwAddVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                          HwPortList, HwUnTagPorts);
#endif
        }
    }

    if (i4HwRetVal != FNP_SUCCESS)
    {
        return (VLAN_FAILURE);
    }

#else
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
#endif
    return (VLAN_SUCCESS);
}

/**************************************************************************/
/* Function Name       : VlanHwDelDynVlanEntry                            */
/*                                                                        */
/* Description         : This function invokes NPAPI to delete the VLAN   */
/*                       entry from the hardware. This function should be */
/*                       invoked only for the dynamic VLAN entries which  */
/*                       are getting deleted as a result of last member   */
/*                       leaving the group. This can also be invoked if   */
/*                       all dynamic info needs to be removed.            */
/*                                                                        */
/* Input(s)            : VlanId - VLAN to be deleted.                     */
/*                       u2Port - Equals VLAN_INVALID_PORT for removing   */
/*                                the complete dynamic VLAN entry.        */
/*                                Equals port number of port on which the */
/*                                last member leave is received.          */
/*                                                                        */
/* Output(s)           : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                        */
/**************************************************************************/
INT4
VlanHwDelDynVlanEntry (tVlanId VlanId, UINT2 u2Port)
{
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;
    INT4                i4RetVal;
    tHwMiVlanEntry      HwVlanEntry;

    i4HwRetVal = FNP_SUCCESS;

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return (VLAN_SUCCESS);
    }

    if ((VlanRedIsRelearningInProgress () != VLAN_TRUE) ||
        (u2Port == VLAN_INVALID_PORT))
    {
        /* Relearning either not started or it is completed  OR
         * all dynamic info is getting deleted and no static info is
         * present in s/w. Hence this entry can be deleted irrespective of
         * relearning is in progress or not. */
        i4HwRetVal =
            VlanFsMiVlanHwDelVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId);
    }
    else
    {
        /* 
         * Relearning is in progress.
         * Case 1. - Entry may not be present in hardware. i.e the last member
         * leave is not synchronised between active and standby. As a result
         * the active wud have deleted the entry from the hardware.
         * Case 2. - Few member port additions may not have synced between 
         * the active and the standby node.
         */
        MEMSET (&HwVlanEntry, 0, sizeof (tHwMiVlanEntry));

        i4RetVal =
            VlanHwGetVlanInfo (VLAN_CURR_CONTEXT_ID (), VlanId, &HwVlanEntry);

        /* 
         * If entry is not present in H/W just return success.
         * i4HwRetVal still holds FNP_SUCCESS.
         */
        if (i4RetVal == FNP_SUCCESS)
        {
            /* Entry already present in hardware. Do add port */
            VLAN_RESET_MEMBER_PORT (HwVlanEntry.HwMemberPbmp, u2Port);

            /* Check if any other port is present */
            if (MEMCMP (HwVlanEntry.HwMemberPbmp, gNullPortList,
                        sizeof (tLocalPortList)) == 0)
            {
                /* Really a last member leave */
                i4HwRetVal =
                    VlanFsMiVlanHwDelVlanEntry (VLAN_CURR_CONTEXT_ID (),
                                                VlanId);
            }
            else
            {
                /* Just reset the port in hardware */
                i4HwRetVal =
                    VlanFsMiVlanHwResetVlanMemberPort (VLAN_CURR_CONTEXT_ID (),
                                                       VlanId,
                                                       VLAN_GET_PHY_PORT
                                                       (u2Port));
            }
        }
    }

    if (i4HwRetVal != FNP_SUCCESS)
    {
        return (VLAN_FAILURE);
    }

#else
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
#endif
    return (VLAN_SUCCESS);
}

/**************************************************************************/
/* Function Name       : VlanHwSetVlanMemberPort                          */
/*                                                                        */
/* Description         : This function invokes NPAPI to add a new member  */
/*                       port to a VLAN.                                  */
/*                                                                        */
/* Input(s)            : VlanId - VLAN identifier.                        */
/*                       u2Port - Port number to be added.                */
/*                       u1IsTagged - Indicates whether it is a tagged/   */
/*                       untagged member.                                 */
/*                                                                        */
/* Output(s)           : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                        */
/**************************************************************************/
INT4
VlanHwSetVlanMemberPort (tVlanId VlanId, UINT2 u2Port, UINT1 u1IsTagged)
{
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;
    tHwMiVlanEntry      HwVlanEntry;
    tLocalPortList      HwPortList;
    tLocalPortList      HwUnTagPorts;
    UINT1               u1Result;
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
    tPortList           UnTagPortList;
#endif

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return (VLAN_SUCCESS);
    }

    if (VlanRedIsRelearningInProgress () != VLAN_TRUE)
    {
        i4HwRetVal =
            VlanFsMiVlanHwSetVlanMemberPort (VLAN_CURR_CONTEXT_ID (), VlanId,
                                             VLAN_GET_PHY_PORT (u2Port),
                                             u1IsTagged);
    }
    else
    {
        /* relearning is not yet over. The entry might not be there in the
         * hardware. If not present we need to invoke add entry first */

        MEMSET (&HwVlanEntry, 0, sizeof (tHwMiVlanEntry));

        i4HwRetVal =
            VlanHwGetVlanInfo (VLAN_CURR_CONTEXT_ID (), VlanId, &HwVlanEntry);

        if (i4HwRetVal == FNP_SUCCESS)
        {
            /* Entry already present in hardware. Do add port */
            VLAN_IS_MEMBER_PORT (HwVlanEntry.HwMemberPbmp, u2Port, u1Result);

            if (u1Result != VLAN_TRUE)
            {
                /* new member of the VLAN */
                i4HwRetVal
                    =
                    VlanFsMiVlanHwSetVlanMemberPort (VLAN_CURR_CONTEXT_ID (),
                                                     VlanId,
                                                     VLAN_GET_PHY_PORT (u2Port),
                                                     u1IsTagged);
            }
        }
        else
        {
            /* fresh entry */
            MEMSET (HwPortList, 0, sizeof (tLocalPortList));
            MEMSET (HwUnTagPorts, 0, sizeof (tLocalPortList));

            VLAN_SET_MEMBER_PORT (HwPortList, u2Port);

            if (u1IsTagged == VLAN_FALSE)
            {
                VLAN_SET_MEMBER_PORT (HwUnTagPorts, u2Port);
            }

#ifdef NP_BACKWD_COMPATIBILITY
            MEMSET (PortList, 0, sizeof (tPortList));
            MEMSET (UnTagPortList, 0, sizeof (tPortList));

            VlanConvertToIfPortList (HwPortList, PortList);
            VlanConvertToIfPortList (HwUnTagPorts, UnTagPortList);

            i4HwRetVal =
                FsMiWrVlanHwAddVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                          PortList, UnTagPortList);
#else
            i4HwRetVal =
                FsMiWrVlanHwAddVlanEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                          HwPortList, HwUnTagPorts);
#endif
        }
    }

    if (i4HwRetVal != FNP_SUCCESS)
    {
        return (VLAN_FAILURE);
    }
#else

    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1IsTagged);
#endif

    return (VLAN_SUCCESS);
}

/**************************************************************************/
/* Function Name       : VlanHwResetVlanMemberPort                        */
/*                                                                        */
/* Description         : This function invokes NPAPI to remove a  member  */
/*                       port from a VLAN.                                */
/*                                                                        */
/* Input(s)            : VlanId - VLAN identifier.                        */
/*                       u2Port - Port number to be removed.              */
/*                                                                        */
/* Output(s)           : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                        */
/**************************************************************************/
INT4
VlanHwResetVlanMemberPort (tVlanId VlanId, UINT2 u2Port)
{
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;
    INT4                i4RetVal;
    tHwMiVlanEntry      HwVlanEntry;
    UINT1               u1Result;

    i4HwRetVal = FNP_SUCCESS;

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return (VLAN_SUCCESS);
    }

    if (VlanRedIsRelearningInProgress () != VLAN_TRUE)
    {
        i4HwRetVal =
            VlanFsMiVlanHwResetVlanMemberPort (VLAN_CURR_CONTEXT_ID (), VlanId,
                                               VLAN_GET_PHY_PORT (u2Port));
    }
    else
    {
        /* Relearning is not yet over.                                    
         * Case 1. Entry might have been deleted by the active node and the
         * delete is not synced between active and standby.
         * Case 2. Member port is deleted from h/w by the active node and
         * it is not synced.
         * Case 3. Member port is present in h/w and s/w and a leave arrives
         * during relearning period.
         */

        MEMSET (&HwVlanEntry, 0, sizeof (tHwMiVlanEntry));

        i4RetVal =
            VlanHwGetVlanInfo (VLAN_CURR_CONTEXT_ID (), VlanId, &HwVlanEntry);

        if (i4RetVal == FNP_SUCCESS)
        {
            /* Entry already present in hardware. */
            VLAN_IS_MEMBER_PORT (HwVlanEntry.HwMemberPbmp, u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                /* member still present in h/w. This is case 3. */
                i4HwRetVal =
                    VlanFsMiVlanHwResetVlanMemberPort (VLAN_CURR_CONTEXT_ID (),
                                                       VlanId,
                                                       VLAN_GET_PHY_PORT
                                                       (u2Port));
            }
        }
    }

    if (i4HwRetVal != FNP_SUCCESS)
    {
        return (VLAN_FAILURE);
    }
#else
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
#endif

    return (VLAN_SUCCESS);

}

/**************************************************************************/
/* Function Name       : VlanHwAddDynMcastEntry                           */
/*                                                                        */
/* Description         : This function invokes NPAPI to add the multicast */
/*                       entry to the hardware. This function should be   */
/*                       invoked only for dynamic mcast that are learnt   */
/*                       newly through GMRP.                              */
/*                                                                        */
/* Input(s)            : VlanId - VLAN to be added.                       */
/*                       MacAddr - Multicast mac address.                 */
/*                       u2Port - Port number to be added.                */
/*                                                                        */
/* Output(s)           : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                        */
/**************************************************************************/
INT4
VlanHwAddDynMcastEntry (tVlanId VlanId, tMacAddr MacAddr, UINT2 u2Port)
{
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;
    tLocalPortList      HwPortList;
    UINT1               u1Result;
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif

    i4HwRetVal = FNP_SUCCESS;

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return (VLAN_SUCCESS);
    }

    if ((VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()) ==
         SNOOP_ENABLED)
        || (VlanSnoopIsMldSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()) ==
            SNOOP_ENABLED))
    {
        /* 
         * IGS is enabled. Check whether NPAPI can be invoked. Bcos
         * in case of redundancy, when the dynamic updates are applied
         * in the standby, the NP shud not be disturbed as it was
         * already programmed by the active node.
         */

        if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
        {
            MEMSET (HwPortList, 0, sizeof (tLocalPortList));

            if (u2Port != 0)
            {
                VLAN_SET_MEMBER_PORT (HwPortList, u2Port);
            }
#ifdef NP_BACKWD_COMPATIBILITY
            MEMSET (PortList, 0, sizeof (tPortList));
            VlanConvertToIfPortList (HwPortList, PortList);

            i4HwRetVal =
                FsMiWrVlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                           MacAddr, PortList);
#else
            i4HwRetVal =
                FsMiWrVlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                           MacAddr, HwPortList);
#endif
        }

        if (i4HwRetVal == FNP_SUCCESS)
        {
            return VLAN_SUCCESS;
        }
        else
        {
            return VLAN_FAILURE;
        }
    }

    if (VlanRedIsMcastRelrningInProgress () != VLAN_TRUE)
    {
        MEMSET (HwPortList, 0, sizeof (tLocalPortList));

        if (u2Port != 0)
        {
            VLAN_SET_MEMBER_PORT (HwPortList, u2Port);
        }

#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));
        VlanConvertToIfPortList (HwPortList, PortList);

        i4HwRetVal =
            FsMiWrVlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr,
                                       PortList);
#else
        i4HwRetVal =
            FsMiWrVlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr,
                                       HwPortList);
#endif
    }
    else
    {
        /* 
         * relearning not over...hence there might be a possibility that
         * the entry could already exist in the h/w. i.e lets say V10, MG1 
         * is present in the hardware with members = P1, P2 and P3. During
         * relearning if FsMiWrVlanHwAddMcastEntry is invoked for P1 alone, then
         * the member port list will be overwritten with P1 only which will
         * result in data traffic loss for P2 and P3 till they are programmed.
         * Hence during relearning we will get the entry from the h/w and 
         * based on that we will program the hardware.
         */
        MEMSET (HwPortList, 0, sizeof (tLocalPortList));

#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));

        i4HwRetVal =
            FsMiWrVlanHwGetMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr,
                                       PortList);
#else
        i4HwRetVal =
            FsMiWrVlanHwGetMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr,
                                       HwPortList);
#endif

        if (i4HwRetVal == FNP_SUCCESS)
        {
            /* Entry already present in hardware. Do add port */
            VLAN_IS_MEMBER_PORT (HwPortList, u2Port, u1Result);

            if (u1Result != VLAN_TRUE)
            {
                /* new member of the MCAST */
                i4HwRetVal =
                    VlanFsMiVlanHwSetMcastPort (VLAN_CURR_CONTEXT_ID (), VlanId,
                                                MacAddr,
                                                VLAN_GET_PHY_PORT (u2Port));
            }
        }
        else
        {
            /* fresh entry */
            MEMSET (HwPortList, 0, sizeof (tLocalPortList));
            if (u2Port != 0)
            {
                VLAN_SET_MEMBER_PORT (HwPortList, u2Port);
            }
#ifdef NP_BACKWD_COMPATIBILITY
            MEMSET (PortList, 0, sizeof (tPortList));
            VlanConvertToIfPortList (HwPortList, PortList);
            i4HwRetVal =
                FsMiWrVlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                           MacAddr, PortList);
#else
            i4HwRetVal =
                FsMiWrVlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                           MacAddr, HwPortList);
#endif
        }
    }
    if (i4HwRetVal != FNP_SUCCESS)
    {
        return (VLAN_FAILURE);
    }
#else
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u2Port);
#endif

    return (VLAN_SUCCESS);
}

/**************************************************************************/
/* Function Name       : VlanHwSetMcastPort                               */
/*                                                                        */
/* Description         : This function invokes NPAPI to add a new member  */
/*                       port to a multicast entry                        */
/*                                                                        */
/* Input(s)            : VlanId - VLAN identifier.                        */
/*                       MacAddr - Multicast mac address.                 */
/*                       u2Port - Port number to be added.                */
/*                                                                        */
/* Output(s)           : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                        */
/**************************************************************************/
INT4
VlanHwSetMcastPort (tVlanId VlanId, tMacAddr MacAddr, UINT2 u2Port)
{
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;
    tLocalPortList      HwPortList;
    UINT1               u1Result;
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif

    i4HwRetVal = FNP_SUCCESS;

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return (VLAN_SUCCESS);
    }

    if ((VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()) ==
         SNOOP_ENABLED)
        || (VlanSnoopIsMldSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()) ==
            SNOOP_ENABLED))
    {
        /* 
         * IGS is enabled. Check whether NPAPI can be invoked. Bcos
         * in case of redundancy, when the dynamic updates are applied
         * in the standby, the NP shud not be disturbed as it was
         * already programmed by the active node.
         */

        if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
        {
            i4HwRetVal =
                VlanFsMiVlanHwSetMcastPort (VLAN_CURR_CONTEXT_ID (), VlanId,
                                            MacAddr,
                                            VLAN_GET_PHY_PORT (u2Port));
        }

        if (i4HwRetVal == FNP_SUCCESS)
        {
            return VLAN_SUCCESS;
        }
        else
        {
            return VLAN_FAILURE;
        }
    }

    /* GMRP Case */
    if (VlanRedIsMcastRelrningInProgress () != VLAN_TRUE)
    {
        i4HwRetVal =
            VlanFsMiVlanHwSetMcastPort (VLAN_CURR_CONTEXT_ID (), VlanId,
                                        MacAddr, VLAN_GET_PHY_PORT (u2Port));
    }
    else
    {
        /* relearning is not yet over. The entry might not be there in the
         * hardware. If not present we need to invoke add entry first */

        MEMSET (HwPortList, 0, sizeof (tLocalPortList));

#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));
        i4HwRetVal =
            FsMiWrVlanHwGetMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr,
                                       PortList);
#else
        i4HwRetVal =
            FsMiWrVlanHwGetMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr,
                                       HwPortList);
#endif

        if (i4HwRetVal == FNP_SUCCESS)
        {
            /* Entry already present in hardware. Do add port */
            VLAN_IS_MEMBER_PORT (HwPortList, u2Port, u1Result);

            if (u1Result != VLAN_TRUE)
            {
                /* new member of the VLAN */
                i4HwRetVal =
                    VlanFsMiVlanHwSetMcastPort (VLAN_CURR_CONTEXT_ID (), VlanId,
                                                MacAddr,
                                                VLAN_GET_PHY_PORT (u2Port));
            }
        }
        else
        {
            /* fresh entry */
            MEMSET (HwPortList, 0, sizeof (tLocalPortList));

            VLAN_SET_MEMBER_PORT (HwPortList, u2Port);

#ifdef NP_BACKWD_COMPATIBILITY
            MEMSET (PortList, 0, sizeof (tPortList));
            VlanConvertToIfPortList (HwPortList, PortList);
            i4HwRetVal =
                FsMiWrVlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                           MacAddr, PortList);
#else
            i4HwRetVal =
                FsMiWrVlanHwAddMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                           MacAddr, HwPortList);
#endif
        }
    }

    if (i4HwRetVal != FNP_SUCCESS)
    {
        return (VLAN_FAILURE);
    }

#else
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u2Port);
#endif

    return (VLAN_SUCCESS);
}

/**************************************************************************/
/* Function Name       : VlanHwResetMcastPort                             */
/*                                                                        */
/* Description         : This function invokes NPAPI to remove a  member  */
/*                       port from a multicast entry.                     */
/*                                                                        */
/* Input(s)            : VlanId - VLAN identifier.                        */
/*                       MacAddr - Multicast mac address.                 */
/*                       u2Port - Port number to be removed.              */
/*                                                                        */
/* Output(s)           : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                        */
/**************************************************************************/
INT4
VlanHwResetMcastPort (tVlanId VlanId, tMacAddr MacAddr, UINT2 u2Port)
{
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;
    INT4                i4RetVal;
    tLocalPortList      HwPortList;
    UINT1               u1Result;
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif

    i4HwRetVal = FNP_SUCCESS;

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return (VLAN_SUCCESS);
    }

    if (VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()) == IGS_ENABLED)
    {
        /* 
         * IGS is enabled. Check whether NPAPI can be invoked. Bcos
         * in case of redundancy, when the dynamic updates are applied
         * in the standby, the NP shud not be disturbed as it was
         * already programmed by the active node.
         */

        if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
        {
            i4HwRetVal =
                VlanFsMiVlanHwResetMcastPort (VLAN_CURR_CONTEXT_ID (), VlanId,
                                              MacAddr,
                                              VLAN_GET_PHY_PORT (u2Port));
        }

        if (i4HwRetVal == FNP_SUCCESS)
        {
            return VLAN_SUCCESS;
        }
        else
        {
            return VLAN_FAILURE;
        }
    }

    /* GMRP Case */
    if (VlanRedIsMcastRelrningInProgress () != VLAN_TRUE)
    {
        i4HwRetVal =
            VlanFsMiVlanHwResetMcastPort (VLAN_CURR_CONTEXT_ID (), VlanId,
                                          MacAddr, VLAN_GET_PHY_PORT (u2Port));
    }
    else
    {
        /* Relearning is not yet over.                                    
         * Case 1. Entry might have been deleted by the active node and the
         * delete is not synced between active and standby.
         * Case 2. Member port is deleted from h/w by the active node and
         * it is not synced.
         * Case 3. Member port is present in h/w and s/w and a leave arrives
         * during relearning period.
         */

        MEMSET (HwPortList, 0, sizeof (tLocalPortList));

#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));
        i4RetVal =
            FsMiWrVlanHwGetMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr,
                                       PortList);
#else
        i4RetVal =
            FsMiWrVlanHwGetMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr,
                                       HwPortList);
#endif

        if (i4RetVal == FNP_SUCCESS)
        {
            /* Entry already present in hardware. */
            VLAN_IS_MEMBER_PORT (HwPortList, u2Port, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                /* member still present in h/w. This is case 3. */
                i4HwRetVal =
                    VlanFsMiVlanHwResetMcastPort (VLAN_CURR_CONTEXT_ID (),
                                                  VlanId, MacAddr,
                                                  VLAN_GET_PHY_PORT (u2Port));
            }
        }
    }

    if (i4HwRetVal != FNP_SUCCESS)
    {
        return (VLAN_FAILURE);
    }

#else
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u2Port);
#endif

    return (VLAN_SUCCESS);
}

/**************************************************************************/
/* Function Name       : VlanHwDelDynMcastEntry                           */
/*                                                                        */
/* Description         : This function invokes NPAPI to delete the MCAST  */
/*                       entry from the hardware. This function should be */
/*                       invoked only for the dynamic GRP  entries which  */
/*                       are getting deleted as a result of last member   */
/*                       leaving the group. This can also be invoked if   */
/*                       all dynamic info needs to be removed.            */
/*                                                                        */
/* Input(s)            : VlanId - VLAN to be deleted.                     */
/*                       MacAddr - Multicast mac address to be deleted.   */
/*                       u2Port - Equals VLAN_INVALID_PORT for removing   */
/*                                the complete dynamic GROUP entry.       */
/*                                Equals port number of port on which the */
/*                                last member leave is received.          */
/*                                                                        */
/* Output(s)           : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                        */
/**************************************************************************/
INT4
VlanHwDelDynMcastEntry (tVlanId VlanId, tMacAddr MacAddr, UINT2 u2Port)
{
#ifdef NPAPI_WANTED
    INT4                i4HwRetVal;
    INT4                i4RetVal;
    tLocalPortList      HwPortList;
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif

    i4HwRetVal = FNP_SUCCESS;

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return (VLAN_SUCCESS);
    }

    if (VlanSnoopIsIgmpSnoopingEnabled (VLAN_CURR_CONTEXT_ID ()) ==
        SNOOP_ENABLED)
    {
        /* 
         * IGS is enabled. Check whether NPAPI can be invoked. Bcos
         * in case of redundancy, when the dynamic updates are applied
         * in the standby, the NP shud not be disturbed as it was
         * already programmed by the active node.
         */

        if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
        {
            i4HwRetVal =
                VlanFsMiVlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                             MacAddr);
        }

        if (i4HwRetVal == FNP_SUCCESS)
        {
            return VLAN_SUCCESS;
        }
        else
        {
            return VLAN_FAILURE;
        }
    }

    /* GMRP Case */
    if ((VlanRedIsMcastRelrningInProgress () != VLAN_TRUE) ||
        (u2Port == VLAN_INVALID_PORT))
    {
        /* Relearning either not started or it is completed  OR
         * all dynamic info is getting deleted and no static info is
         * present in s/w. Hence this entry can be deleted irrespective of
         * relearning is in progress or not. */
        i4HwRetVal =
            VlanFsMiVlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId,
                                         MacAddr);
    }
    else
    {
        /* 
         * Relearning is in progress.
         * Case 1. - Entry may not be present in hardware. i.e the last member
         * leave is not synchronised between active and standby. As a result
         * the active wud have deleted the entry from the hardware.
         * Case 2. - Few member port additions may not have synced between 
         * the active and the standby node.
         */
        MEMSET (HwPortList, 0, sizeof (tLocalPortList));
#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));

        i4RetVal =
            FsMiWrVlanHwGetMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr,
                                       PortList);
#else
        i4RetVal =
            FsMiWrVlanHwGetMcastEntry (VLAN_CURR_CONTEXT_ID (), VlanId, MacAddr,
                                       HwPortList);
#endif
        /* 
         * If entry is not present in H/W just return success.
         * i4HwRetVal still holds FNP_SUCCESS.
         */
        if (i4RetVal == FNP_SUCCESS)
        {
            /* Entry already present in hardware. Do add port */
            if (u2Port != 0)
            {
                VLAN_RESET_MEMBER_PORT (HwPortList, u2Port);
            }

            /* Check if any other port is present */
            if (MEMCMP (HwPortList, gNullPortList, sizeof (tLocalPortList)) ==
                0)
            {
                /* Really a last member leave */
                i4HwRetVal =
                    VlanFsMiVlanHwDelMcastEntry (VLAN_CURR_CONTEXT_ID (),
                                                 VlanId, MacAddr);
            }
            else if (u2Port != 0)
            {
                /* Just reset the port in hardware */
                i4HwRetVal =
                    VlanFsMiVlanHwResetMcastPort (VLAN_CURR_CONTEXT_ID (),
                                                  VlanId, MacAddr,
                                                  VLAN_GET_PHY_PORT (u2Port));
            }
        }
    }

    if (i4HwRetVal != FNP_SUCCESS)
    {
        return (VLAN_FAILURE);
    }

#else
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u2Port);
#endif

    return (VLAN_SUCCESS);

}

/*****************************************************************************/
/*    Function Name       : VlanHwDelVlanProtocolMap                         */
/*                                                                           */
/*    Description         : This function deletes the mapped VlanId  from    */
/*                          protocol group                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port - Local Port index                        */
/*                          pProtoTemplate - pointer to protocol template    */
/*                          u4GroupId  - Group Id                            */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwDelVlanProtocolMap (UINT4 u4ContextId, UINT2 u2Port,
                          UINT4 u4GroupId, tVlanProtoTemplate * pProtoTemplate)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwDelVlanProtocolMap
            (u4ContextId, VLAN_GET_PHY_PORT (u2Port), u4GroupId,
             pProtoTemplate) == FNP_FAILURE)
        {
            return (VLAN_FAILURE);
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4GroupId);
    UNUSED_PARAM (pProtoTemplate);
#endif
    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : VlanHwAddVlanProtocolMap                         */
/*                                                                           */
/*    Description         : This function maps the valid and existence group */
/*                          ID to Vlan ID                                    */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port - Local Port index                        */
/*                          u4GroupId  - Group Id                            */
/*                          pProtoTemplate - pointer to protocol template    */
/*                          tVlanId - VLAN Id                                */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwAddVlanProtocolMap (UINT4 u4ContextId, UINT2 u2Port, UINT4 u4GroupId,
                          tVlanProtoTemplate * pProtoTemplate, tVlanId VlanId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwAddVlanProtocolMap
            (u4ContextId, VLAN_GET_PHY_PORT (u2Port), u4GroupId, pProtoTemplate,
             VlanId) == FNP_FAILURE)
        {
            return (VLAN_FAILURE);
        }
    }

#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4GroupId);
    UNUSED_PARAM (pProtoTemplate);
    UNUSED_PARAM (VlanId);
#endif
    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : VlanHwVlanDisable                                */
/*                                                                           */
/*    Description         : This function disables the hardware.             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            :                                                   */
/*                                VLAN_SUCCESS: Disabled and entries flushed */
/*                                VLAN_FAILURE: Disabling not supported      */
/*                                VLAN_DISABLED_BUT_NOT_DELETED:             */
/*                                 Hardware supports disabling but the       */
/*                                 entries were not flushed                  */
/*****************************************************************************/
INT4
VlanHwVlanDisable (UINT4 u4ContextId)
{
    INT4                i4RetVal = VLAN_SUCCESS;
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        i4RetVal = VlanFsMiVlanHwVlanDisable (u4ContextId);

        if (i4RetVal == FNP_SUCCESS)
        {
            i4RetVal = VLAN_SUCCESS;
        }
        else if (i4RetVal == FNP_VLAN_DISABLED_BUT_NOT_DELETED)
        {
            i4RetVal = VLAN_DISABLED_BUT_NOT_DELETED;
        }
        else
        {
            i4RetVal = VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VlanHwPortMacLearningStatus                          */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwPortMacLearningStatus (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwPortMacLearningStatus (u4ContextId,
                                                 u4IfIndex,
                                                 u1Status) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwPortUnicastMacSecType.                      */
/*                                                                           */
/* Description        : This function is called for configuring the Unicast  */
/*                      mac limit for a Port                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex  - Port identifier.                        */
/*                      i4MacSecType - Mac security level                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On VLAN_SUCCESS                         */
/*                      VLAN_FAILURE - On failure                             */                                              /*****************************************************************************/
INT4
VlanHwPortUnicastMacSecType (UINT4 u4ContextId, UINT4 u4IfIndex,
                            INT4 i4MacSecType)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwPortUnicastMacSecType (u4ContextId,
                                                u4IfIndex,
                                                i4MacSecType) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4MacSecType);
#endif
    return VLAN_SUCCESS;
}




/*****************************************************************************/
/*    Function Name       : VlanHwFlushFdbId                                 */
/*                                                                           */
/*    Description         : This function flushes all the learnt FDB entries */
/*                          in the FDB table                                 */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Fid       - The FDB ID                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwFlushFdbId (UINT4 u4ContextId, UINT4 u4FdbId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
        {
            /* VLAN module is DISABLED */
            return VLAN_SUCCESS;
        }

        if (VLAN_IS_FDB_ID_VALID (u4FdbId) == VLAN_FALSE)
        {
            /*Fdb Id is not in the  allowed range */

            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                             "Flushing based on Port-FdbId called for invalid port \n");

            return VLAN_FAILURE;
        }

        if (VlanFsMiVlanHwFlushFdbId (u4ContextId, u4FdbId) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }

    /* Clears the Software learning database with the FdbId */
    VlanFlushLocalFdbId (u4FdbId);
#ifdef ICCH_WANTED
    VlanIcchSyncVlanFlush (u4FdbId);
#endif 
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4FdbId);
#endif
    return VLAN_SUCCESS;

}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*    Function Name       : VlanHwGetVlanInfo                                */
/*                                                                           */
/*    Description         : This function returns the VLAN membership of the */
/*                          given VLAN in the hardware if it is present.     */
/*                          The VLAN membership returned should not include  */
/*                          the physical ports that are part of aggregation  */
/*                          group. i.e. if P1, P2 and P3 are members of      */
/*                          agg. group P26, then the port list returned      */
/*                          should contain P26 and not P1, P2 and P3.        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId - VLAN id for which the info needs to be  */
/*                          retrieved.                                       */
/*                                                                           */
/*    Output(s)           : pHwEntry - VLAN info pointer.                    */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwGetVlanInfo (UINT4 u4ContextId, tVlanId VlanId, tHwMiVlanEntry * pHwEntry)
{
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (FsMiWrVlanHwGetVlanInfo (u4ContextId, VlanId, pHwEntry)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwGetFdbEntry                                */
/*                                                                           */
/*    Description         : This function gets the entry from  hardware      */
/*                           Unicast Mac table.                              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4FdbId  - FdbId                                 */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                                                                           */
/*    Output(s)           : pEntry  - Pointer to Unicast Mac Entry           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*****************************************************************************/
INT4
VlanHwGetFdbEntry (UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr,
                   tHwUnicastMacEntry * pEntry)
{
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwGetFdbEntry (u4ContextId, u4FdbId, MacAddr,
                                       pEntry) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }

    return VLAN_SUCCESS;
}

#ifndef SW_LEARNING
/*****************************************************************************/
/*    Function Name       : VlanHwGetFdbCount                                */
/*                                                                           */
/*    Description         : This function gets the Dynamic Fdb Count for the */
/*                          given FdbId                                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4FdbId  - FdbId.                                */
/*                                                                           */
/*    Output(s)           : pu4Count - Count of the given FdbId.             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/ VLAN_FAILURE                        */
/*****************************************************************************/
INT4
VlanHwGetFdbCount (UINT4 u4ContextId, UINT4 u4FdbId, UINT4 *pu4Count)
{

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwGetFdbCount (u4ContextId, u4FdbId, pu4Count)
            == FNP_FAILURE)
        {
            return (VLAN_FAILURE);
        }
    }

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : VlanHwGetFirstTpFdbEntry                         */
/*                                                                           */
/*    Description         : This function gets the first Fdb entry in the    */
/*                          lexicographic order.                             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          pu4FdbId - Pointer to the Fdb id                 */
/*                          pMacAddr - Pointer to the Mac Address.           */
/*                                                                           */
/*    Output(s)           : The first FdbId and Mac address.                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*****************************************************************************/
INT4
VlanHwGetFirstTpFdbEntry (UINT4 u4ContextId, UINT4 *pu4FdbId, tMacAddr MacAddr)
{

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return VLAN_FAILURE;
    }

    if (VlanFsMiVlanHwGetFirstTpFdbEntry (u4ContextId, pu4FdbId,
                                          MacAddr) != FNP_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwGetNextTpFdbEntry                          */
/*                                                                           */
/*    Description         : This function gets the next Fdb entry in the     */
/*                          lexicographic order.                             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4FdbId - The current FdbId.                     */
/*                          pMacAddr - Pointer to the current Mac addr.      */
/*    Input(s)            : pu4NextContextId - Pointer to next Context Id    */
/*                          pu4NextFdbId - Pointer to the next Fdb Id.       */
/*                          pNextMacAddr - Pointer to the next mac addr.     */
/*                                                                           */
/*    Output(s)           : The next FdbId and Mac address.                  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*****************************************************************************/
INT4
VlanHwGetNextTpFdbEntry (UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr,
                         UINT4 *pu4NextContextId, UINT4 *pu4NextFdbId,
                         tMacAddr NextMacAddr)
{
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return VLAN_FAILURE;
    }

    if (VlanFsMiVlanHwGetNextTpFdbEntry (u4ContextId, u4FdbId, MacAddr,
                                         pu4NextContextId, pu4NextFdbId,
                                         NextMacAddr) != FNP_SUCCESS)
    {
        return VLAN_FAILURE;
    }

    return VLAN_SUCCESS;
}
#endif /* !SW_LEARNING */
#endif

/*****************************************************************************/
/*    Function Name       : VlanHwFlushPort                                  */
/*                                                                           */
/*    Description         : This function flushes the FDB entries            */
/*                          learnt on this port. Flushing                    */
/*                          is done based on port in all  FDB Table          */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Local Port Index                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwFlushPort (UINT4 u4ContextId, UINT2 u2Port, INT4 i4OptimizeFlag)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {

        if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
        {
            /* VLAN module is DISABLED */
            return VLAN_SUCCESS;
        }

        if (VlanFsMiVlanHwFlushPort (u4ContextId, VLAN_GET_PHY_PORT (u2Port),
                                     i4OptimizeFlag) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
        VlanDeleteFdbEntriesForPort (u2Port);
#ifdef ICCH_WANTED
    /* Send out notification to the Peer to Clear the fdb enteries pertaining
     * to this port */
  VlanIcchSyncPortFlush(VLAN_GET_PHY_PORT (u2Port));
#endif
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (i4OptimizeFlag);

#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwFlushPortFdbId                             */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires learnt on  */
/*                          a specific port in a specific Fdb table          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Port Interface Index               */
/*                          u4Fid   - Fdb Id in which the Entry has to be    */
/*                                   flushed.                                */
/*                           i4OptimizeFlag - Indicates whether this call can*/
/*                                            be grouped with the flush call */
/*                                            for other ports as a single    */
/*                                            flush call                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/

INT4
VlanHwFlushPortFdbId (UINT4 u4ContextId, UINT2 u2Port, UINT4 u4FdbId,
                      INT4 i4OptimizeFlag)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
        {
            /* VLAN module is DISABLED */
            return VLAN_SUCCESS;
        }

        if (VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE)
        {

            /* Port number is not in the allowed range */
            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                             "Flushing based on Port-FdbId called for invalid port \n");

            return VLAN_FAILURE;
        }

        if (VLAN_IS_FDB_ID_VALID (u4FdbId) == VLAN_FALSE)
        {
            /*Fdb Id is not in the  allowed range */

            VLAN_GLOBAL_TRC (u4ContextId, VLAN_MOD_TRC, ALL_FAILURE_TRC,
                             "Flushing based on Port-FdbId called for invalid port \n");

            return VLAN_FAILURE;
        
        }
         if (VLAN_GET_PORT_ENTRY (u2Port) == NULL)
         {
            return VLAN_FAILURE;
         }
       if (VlanFsMiVlanHwFlushPortFdbId
            (u4ContextId, VLAN_GET_PHY_PORT (u2Port), u4FdbId,
             i4OptimizeFlag) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
        VlanFlushLocalPortFdbId (u4FdbId, u2Port);
#ifdef ICCH_WANTED
        /* Sends Sync-up message to the peer to enable the peer delete the 
         * remote fdb entries pertaining to the port learnt on the VLAN specified */
        VlanIcchSyncPortVlanFlush (u4FdbId, VLAN_GET_PHY_PORT (u2Port));
#endif
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (i4OptimizeFlag);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwDeleteAllFdbEntries                        */
/*                                                                           */
/*    Description         : This function flushes all entres in FDB table    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/

INT4
VlanHwDeleteAllFdbEntries (UINT4 u4ContextId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VLAN_MODULE_STATUS_FOR_CONTEXT (u4ContextId) == VLAN_DISABLED)
        {
            /* VLAN module is DISABLED */
            return VLAN_SUCCESS;
        }

        if (VlanFsMiNpDeleteAllFdbEntries (u4ContextId) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
    VlanFlushLocalFdb ();
    VlanFlushRemoteFdb (VLAN_FALSE);
#ifdef ICCH_WANTED
        /* Sends request to clear remote peer MAC entires in MC-LAG */
    VlanIcchSyncClearMacTable ();
#endif
#else
    UNUSED_PARAM (u4ContextId);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetDefUserPriority                         */
/*                                                                           */
/*    Description         : This function set the default user priority to   */
/*                          port                                             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Represents the Local Port          */
/*                          i4DefPriority - default user priority            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetDefUserPriority (UINT4 u4ContextId, UINT2 u2Port, INT4 i4DefPriority)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        /*
         * Default user priority can be set even if the priority module is
         * disabled, since all priorities will be mapping to the same traffic
         * class queue. Hence setting default user priority in the hardware
         * will not cause any problems. Also the VLAN module include priority 
         * in the tag header. So the priority changes should be given to h/w.
         */
        if (VlanFsMiVlanHwSetDefUserPriority (u4ContextId,
                                              VLAN_GET_PHY_PORT (u2Port),
                                              i4DefPriority) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (i4DefPriority);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetPortNumTrafClasses                      */
/*                                                                           */
/*    Description         : This function set the number of traffic classes  */
/*                          for the port.                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Represents the Local Port          */
/*                          i4NumTraffClass - number of traffic classes      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetPortNumTrafClasses (UINT4 u4ContextId, UINT2 u2Port,
                             INT4 i4NumTraffClass)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortNumTrafClasses (u4ContextId,
                                                 VLAN_GET_PHY_PORT (u2Port),
                                                 i4NumTraffClass) !=
            FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (i4NumTraffClass);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetRegenUserPriority                       */
/*                                                                           */
/*    Description         : This function overides the priority information  */
/*                          in the priority regeneration table maintained by */
/*                          each port for the user priority                  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Represents the Local Port          */
/*                          i4UserPriority  - user priority information      */
/*                          i4RegenPriority - regenerated user priority      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetRegenUserPriority (UINT4 u4ContextId, UINT2 u2Port,
                            INT4 i4UserPriority, INT4 i4RegenPriority)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {

        if (VlanFsMiVlanHwSetRegenUserPriority (u4ContextId,
                                                VLAN_GET_PHY_PORT (u2Port),
                                                i4UserPriority, i4RegenPriority)
            != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }

#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (i4UserPriority);
    UNUSED_PARAM (i4RegenPriority);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetTraffClassMap                           */
/*                                                                           */
/*    Description         : This function set the traffic class for which    */
/*                          user priority of the port should map into        */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Represents the Local Port          */
/*                          i4UserPriority  - user priority                  */
/*                          i4TraffClass - traffic class to be mapped        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetTraffClassMap (UINT4 u4ContextId, UINT2 u2Port,
                        INT4 i4UserPriority, INT4 i4TraffClass)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {

        if (VlanFsMiVlanHwSetTraffClassMap
            (u4ContextId, VLAN_GET_PHY_PORT (u2Port), i4UserPriority,
             i4TraffClass) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (i4UserPriority);
    UNUSED_PARAM (i4TraffClass);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanEnablePriModule                              */
/*                                                                           */
/*    Description         : This function enables priority module in the     */
/*                         hardware. This function calls appropriate NPAPI's */
/*                         for the configurations made when the module is    */
/*                         disabled. VLAN Port table MUST be initialised     */
/*                         before calling this function.                     */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanEnablePriModule (VOID)
{
#ifdef NPAPI_WANTED
    /* 
     * Initialise 802.1p parameters in hardware which are configured when the
     * module is disabled
     */
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        VlanHwEnablePriorityModule ();
    }
#else
    VLAN_CURR_CONTEXT_PTR ()->VlanPriTimer.u1IsTmrActive = VLAN_FALSE;
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDisablePriModule                             */
/*                                                                           */
/*    Description         : This function disables priority module in the    */
/*                         hardware. This function calls appropriate NPAPI's */
/*                         for the revoking the configurable parameters to   */
/*                         to default values.                                */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDisablePriModule (VOID)
{
#ifdef NPAPI_WANTED

    /* 
     * Reinitialise hardware to default values since the priority module
     * is disabled.
     */
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        VlanHwDisablePriorityModule ();
    }
#else
    if (VLAN_TRUE == gpVlanContextInfo->VlanPriTimer.u1IsTmrActive)
    {

        /* STOP the active timer here */
        VLAN_STOP_TIMER (VLAN_PRIORITY_TIMER);

        /* Some packets could still be there in the priority queues. */

        VlanDrainAllPriQ ();
    }
#endif /* !NPAPI_WANTED */
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwDeInit                                    */
/*                                                                           */
/*    Description         : This function deinitialises all the hardware     */
/*                          related tables and parameters.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwDeInit (UINT4 u4ContextId)
{
#ifdef NPAPI_WANTED

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        /* Disable VLAN in hardware */
        if (VlanFsMiVlanHwDeInit (u4ContextId) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetPortPvid                                */
/*                                                                           */
/*    Description         : This function sets the given VlanId as PortPvid  */
/*                          for the Port.                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId     - Vlan to which the port is going to  */
/*                                        be an member                       */
/*                          u2Port      - Represents the Local Port          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetPortPvid (UINT4 u4ContextId, UINT2 u2Port, tVlanId VlanId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortPvid (u4ContextId, VLAN_GET_PHY_PORT (u2Port),
                                       VlanId) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetPortAccFrameType                        */
/*                                                                           */
/*    Description         : This function sets the Acceptable frame types    */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Represents the Local Port          */
/*                          u1AccFrameType - Acceptable Frame types          */
/*                                           parameter value.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetPortAccFrameType (UINT4 u4ContextId, UINT2 u2Port, INT1 u1AccFrameType)
{
#ifdef NPAPI_WANTED

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortAccFrameType (u4ContextId,
                                               VLAN_GET_PHY_PORT (u2Port),
                                               u1AccFrameType) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1AccFrameType);
    UNUSED_PARAM (u2Port);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetPortIngFiltering                        */
/*                                                                           */
/*    Description         : This function sets the Ingress Filtering         */
/*                          parameter for the Given Port.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Represents the Local Port          */
/*                          u1IngFilterEnable - FNP_TRUE if Ingress          */
/*                                              filtering enabled, otherwise */
/*                                              FNP_FALSE                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetPortIngFiltering (UINT4 u4ContextId, UINT2 u2Port,
                           UINT1 u1IngFilterEnable)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (u1IngFilterEnable == VLAN_ENABLED)
        {
            if (VlanFsMiVlanHwSetPortIngFiltering (u4ContextId,
                                                   VLAN_GET_PHY_PORT (u2Port),
                                                   FNP_TRUE) == FNP_FAILURE)
            {
                return VLAN_FAILURE;
            }
        }
        else
        {
            if (VlanFsMiVlanHwSetPortIngFiltering (u4ContextId,
                                                   VLAN_GET_PHY_PORT (u2Port),
                                                   FNP_FALSE) == FNP_FAILURE)
            {
                return VLAN_FAILURE;
            }

        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1IngFilterEnable);
    UNUSED_PARAM (u2Port);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetSubnetBasedStatusOnPort                 */
/*                                                                           */
/*    Description         : This function enable or disable the Subnet based */
/*                          Vlan Type on the port                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Represents the Local Port          */
/*                          u1SubnetBasedVlanEnable - FNP_TRUE/FNP_FALSE     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetSubnetBasedStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                                  UINT1 u1SubnetBasedVlanEnable)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (u1SubnetBasedVlanEnable == VLAN_ENABLED)
        {
            if (VlanFsMiVlanHwSetSubnetBasedStatusOnPort (u4ContextId,
                                                          VLAN_GET_PHY_PORT
                                                          (u2Port),
                                                          FNP_TRUE) ==
                FNP_FAILURE)
            {
                return VLAN_FAILURE;
            }
        }
        else
        {
            if (VlanFsMiVlanHwSetSubnetBasedStatusOnPort (u4ContextId,
                                                          VLAN_GET_PHY_PORT
                                                          (u2Port),
                                                          FNP_FALSE) ==
                FNP_FAILURE)
            {
                return VLAN_FAILURE;
            }

        }

        /* Add/Delete all Subnet-VLAN mapping entries in the port 
         * based on enable/disable status respectively*/
        if ((VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE))
        {
            return VLAN_FAILURE;
        }
        if (VLAN_PORT_SUBNET_BASED (u2Port) != u1SubnetBasedVlanEnable)
        {
#ifdef BCMX_WANTED
            u2Port = VLAN_INIT_VAL;
#endif
            VlanHandleSubnetBasedOnPort (u2Port, u1SubnetBasedVlanEnable);
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1SubnetBasedVlanEnable);
    UNUSED_PARAM (u2Port);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetMacBasedStatusOnPort                    */
/*                                                                           */
/*    Description         : This function enable or disable the Mac based    */
/*                          Vlan Type on the port                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Represents the Local Port          */
/*                          u1MacBasedVlanEnable - FNP_TRUE/FNP_FALSE        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetMacBasedStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                               UINT1 u1MacBasedVlanEnable)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (u1MacBasedVlanEnable == VLAN_ENABLED)
        {
            if (VlanFsMiVlanHwSetMacBasedStatusOnPort (u4ContextId,
                                                       VLAN_GET_PHY_PORT
                                                       (u2Port),
                                                       FNP_TRUE) == FNP_FAILURE)
            {
                return VLAN_FAILURE;
            }
        }
        else
        {
            if (VlanFsMiVlanHwSetMacBasedStatusOnPort (u4ContextId,
                                                       VLAN_GET_PHY_PORT
                                                       (u2Port),
                                                       FNP_FALSE) ==
                FNP_FAILURE)
            {
                return VLAN_FAILURE;
            }

        }

        if ((VLAN_IS_PORT_VALID (u2Port) == VLAN_FALSE))
        {
            return VLAN_FAILURE;
        }

        if (VLAN_PORT_MAC_BASED (u2Port) != u1MacBasedVlanEnable)
        {
            VlanHandleMacBasedOnPort (u2Port, u1MacBasedVlanEnable);
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1MacBasedVlanEnable);
    UNUSED_PARAM (u2Port);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwEnableProtoVlanOnPort                      */
/*                                                                           */
/*    Description         : This function enable or disable the  Port        */
/*                          Protocol based type on the port                  */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Represents the Local Port          */
/*                           u1VlanProtoEnable - FNP_TRUE/FNP_FALSE          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwEnableProtoVlanOnPort (UINT4 u4ContextId, UINT2 u2Port,
                             UINT1 u1VlanProtoEnable)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (u1VlanProtoEnable == VLAN_ENABLED)
        {
            if (VlanFsMiVlanHwEnableProtoVlanOnPort (u4ContextId,
                                                     VLAN_GET_PHY_PORT (u2Port),
                                                     FNP_TRUE) == FNP_FAILURE)
            {
                return VLAN_FAILURE;
            }
        }
        else
        {
            if (VlanFsMiVlanHwEnableProtoVlanOnPort (u4ContextId,
                                                     VLAN_GET_PHY_PORT (u2Port),
                                                     FNP_FALSE) == FNP_FAILURE)
            {
                return VLAN_FAILURE;
            }
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1VlanProtoEnable);
    UNUSED_PARAM (u2Port);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwAddPortSubnetVlanEntry                     */
/*                                                                           */
/*    Description         : This function adds or updates port based Subnet  */
/*                          vlan entry                                       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Port      - Represents the Local Port          */
/*                          SubnetAddr     - IP Subnet address               */
/*                          VlanId      - Vlan ID                            */
/*                          bArpOption   - FNP_TRUE/FNP_FALSE                */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwAddPortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4Port, UINT4 SubnetAddr,
                              UINT4 u4SubnetMask, tVlanId VlanId,
                              BOOL1 bArpOption)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwAddPortSubnetVlanEntry
            (u4ContextId, u4Port, SubnetAddr, VlanId,
             bArpOption) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }

        /* New NP API to provide subnet mask as one of the input
         * to subnet based VLAN entry addition */
        if (VlanFsMiVlanHwUpdatePortSubnetVlanEntry
            (u4ContextId, u4Port, SubnetAddr, u4SubnetMask,
             VlanId, bArpOption, VLAN_ADD) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (SubnetAddr);
    UNUSED_PARAM (u4SubnetMask);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (bArpOption);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwDeletePortMacVlanEntry                     */
/*                                                                           */
/*    Description         : This function delete port based subnet vlan entry*/
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Port      - Represents the Local Port          */
/*                          SubnetAddr  - IP Subnet address                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwDeletePortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4Port,
                                 UINT4 u4SubnetAddr, UINT4 u4SubnetMask)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwDeletePortSubnetVlanEntry
            (u4ContextId, u4Port, u4SubnetAddr) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }

        /* New NP API to provide subnet mask as one of the input
         * to subnet based VLAN entry deletion */
        if (VlanFsMiVlanHwUpdatePortSubnetVlanEntry
            (u4ContextId, u4Port, u4SubnetAddr, u4SubnetMask,
             VLAN_INIT_VAL, VLAN_INIT_VAL, VLAN_DELETE) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4SubnetAddr);
    UNUSED_PARAM (u4SubnetMask);
    UNUSED_PARAM (u4Port);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwAddPortMacVlanEntry                        */
/*                                                                           */
/*    Description         : This function adds or updates port based mac     */
/*                          vlan entry                                       */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Port      - Represents the Local Port          */
/*                          MacAddr     - mac address                        */
/*                          VlanId      - Vlan ID                            */
/*                          bSuppressOption   - FNP_TRUE/FNP_FALSE           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwAddPortMacVlanEntry (UINT4 u4ContextId, UINT4 u4Port, tMacAddr MacAddr,
                           tVlanId VlanId, BOOL1 bSuppressOption)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwAddPortMacVlanEntry (u4ContextId, u4Port,
                                               MacAddr, VlanId,
                                               bSuppressOption) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (bSuppressOption);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwDeletePortMacVlanEntry                     */
/*                                                                           */
/*    Description         : This function deletes port based mac vlan entry  */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Port      - Represents the Local Port          */
/*                          MacAddr     - mac address                        */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwDeletePortMacVlanEntry (UINT4 u4ContextId, UINT4 u4Port, tMacAddr MacAddr)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwDeletePortMacVlanEntry (u4ContextId, u4Port,
                                                  MacAddr) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4Port);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetPortTunnelMode                          */
/*                                                                           */
/*    Description         : This function sets the tunnel mode of a port.    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Represents the Local Port          */
/*                          u4Mode    - Tunnel mode of a port.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetPortTunnelMode (UINT4 u4ContextId, UINT4 u2Port, UINT4 u4Mode)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortTunnelMode (u4ContextId,
                                             VLAN_GET_PHY_PORT (u2Port), u4Mode)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4Mode);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetTunnelFilter                            */
/*                                                                           */
/*    Description         : This function creates/deletes h/w filters for    */
/*                          Provider Gvrp/Gmrp/STP Macaddress. This enables  */
/*                          the tunnel PDUs to be switched by software.      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          i4BridgeMode - Bridge Mode                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetTunnelFilter (UINT4 u4ContextId, INT4 i4BridgeMode)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetTunnelFilter (u4ContextId, i4BridgeMode)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }

#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4BridgeMode);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwCheckTagAtEgress                           */
/*                                                                           */
/*    Description         : This function specifies whether the underlying   */
/*                          H/W supports the exlusion of the provider VLAN   */
/*                          Tag by itself.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : pu1TagSet - VLAN_TRUE/ VLAN_FALSE                */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwCheckTagAtEgress (UINT4 u4ContextId, UINT1 *pu1TagSet)
{
#ifdef NPAPI_WANTED

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwCheckTagAtEgress (u4ContextId, pu1TagSet) ==
            FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    *pu1TagSet = VLAN_FALSE;
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwCheckTagAtEgress                           */
/*                                                                           */
/*    Description         : This function specifies whether the underlying   */
/*                          H/W supports the inclusion of the provider VLAN  */
/*                          Tag by itself.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : pu1TagSet - VLAN_TRUE/ VLAN_FALSE                */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwCheckTagAtIngress (UINT4 u4ContextId, UINT1 *pu1TagSet)
{
#ifdef NPAPI_WANTED

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwCheckTagAtIngress (u4ContextId, pu1TagSet) ==
            FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }

#else
    UNUSED_PARAM (u4ContextId);
    *pu1TagSet = VLAN_FALSE;
#endif

    return VLAN_SUCCESS;
}

#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
/*****************************************************************************/
/*    Function Name       : VlanHwScanProtocolVlanTbl                        */
/*                                                                           */
/*    Description         : This function scans the protocol VLAN table and  */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          ProtoVlanCallBack - Protocol VLAN call back      */
/*                          function pointer.                                */
/*                          u4Port - Port for which the scan needs to be     */
/*                          performed.                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwScanProtocolVlanTbl (UINT4 u4ContextId, UINT4 u4Port,
                           FsMiVlanHwProtoCb ProtoVlanCallBack)
{
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwScanProtocolVlanTbl (u4ContextId,
                                               VLAN_GET_PHY_PORT (u4Port),
                                               ProtoVlanCallBack) ==
            FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwScanMulticastTbl                           */
/*                                                                           */
/*    Description         : This function scans the VLAN multicast table and */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          McastCallBack - Multicast call back function     */
/*                          pointer.                                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwScanMulticastTbl (UINT4 u4ContextId, FsMiVlanHwMcastCb McastCallBack)
{

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {

        if (VlanFsMiVlanHwScanMulticastTbl (u4ContextId, McastCallBack)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
}
#endif
/*****************************************************************************/
/*    Function Name       : VlanHwGetMcastEntry                              */
/*                                                                           */
/*    Description         : This function gets the given multicast entry     */
/*                          present in the hardware.                         */
/*                          The MCAST membership returned should not include */
/*                          the physical ports that are part of aggregation  */
/*                          group. i.e. if P1, P2 and P3 are members of      */
/*                          agg. group P26, then the port list returned      */
/*                          should contain P26 and not P1, P2 and P3.        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId - VLAN Id of the mcast entry.             */
/*                          MacAdd - Mcast mac address.                      */
/*                                                                           */
/*    Output(s)           : pHwMcastPorts- pointer to Mcast Ports in Hardware*/
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwGetMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                     tLocalPortList PortList)
{
#ifdef NPAPI_WANTED
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           HwPortList;
    MEMSET (HwPortList, 0, sizeof (tPortList));
    VlanConvertToIfPortList (PortList, HwPortList);
#endif

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
#ifdef NP_BACKWD_COMPATIBILITY
        if (FsMiWrVlanHwGetMcastEntry (u4ContextId, VlanId, MacAddr,
                                       HwPortList) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
#else
        if (FsMiWrVlanHwGetMcastEntry (u4ContextId, VlanId, MacAddr,
                                       PortList) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
#endif
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (PortList);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwGetStMcastEntry                            */
/*                                                                           */
/*    Description         : This function gets the given static mcast entry  */
/*                          present in the hardware.                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          VlanId - VLAN Id of the mcast entry.             */
/*                          MacAdd - Mcast mac address.                      */
/*                          u4RcvPort - Receive port.                        */
/*                          pHwMcastPorts - pointer to get Mcast Ports in    */
/*                                  Hardware                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwGetStMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr,
                       UINT4 u4RcvPort, tLocalPortList HwPortList)
{
#ifdef NPAPI_WANTED
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));
        VlanConvertToIfPortList (HwPortList, PortList);
        if (FsMiWrVlanHwGetStMcastEntry (u4ContextId, VlanId, MacAddr,
                                         VLAN_GET_PHY_PORT (u4RcvPort),
                                         PortList) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
#else
        if (FsMiWrVlanHwGetStMcastEntry (u4ContextId, VlanId, MacAddr,
                                         VLAN_GET_PHY_PORT (u4RcvPort),
                                         HwPortList) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
#endif
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u4RcvPort);
    UNUSED_PARAM (HwPortList);
#endif

    return VLAN_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*    Function Name       : VlanHwScanUnicastTbl                             */
/*                                                                           */
/*    Description         : This function scans the VLAN multicast table and */
/*                          calls the callback provided by the application.  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          UcastCallBack - Multicast call back function     */
/*                          pointer.                                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwScanUnicastTbl (UINT4 u4ContextId, FsMiVlanHwUcastCb UcastCallBack)
{
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwScanUnicastTbl (u4ContextId, UcastCallBack)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
}
#endif
/*****************************************************************************/
/*    Function Name       : VlanHwGetVlanProtocolMap                         */
/*                                                                           */
/*    Description         : This function returns the VLAN id for which the  */
/*                          protocol group is mapped.                        */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Represents the Local Port          */
/*                          u4GroupId  - Group Id                            */
/*                          pProtoTemplate - pointer to protocol template    */
/*                                                                           */
/*    Output(s)           : pVlanId - Pointer to the VLAN ID.                */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwGetVlanProtocolMap (UINT4 u4ContextId, UINT2 u2Port,
                          UINT4 u4GroupId,
                          tVlanProtoTemplate * pProtoTemplate,
                          tVlanId * pVlanId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwGetVlanProtocolMap (u4ContextId,
                                              VLAN_GET_PHY_PORT (u2Port),
                                              u4GroupId, pProtoTemplate,
                                              pVlanId) == FNP_FAILURE)
        {
            return VLAN_FAILURE;

        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4GroupId);
    UNUSED_PARAM (pProtoTemplate);
    UNUSED_PARAM (pVlanId);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwGetStaticUcastEntry                        */
/*                                                                           */
/*    Description         : This function gets the static unicast entry      */
/*                          for the given FDB Id and mac address.            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Fid            - Fid                           */
/*                          pMacAddr         - Pointer to the Mac Address.   */
/*                          u2RcvPort        - Received Interface Index      */
/*                                                                           */
/*    Output(s)           : pAllowedToGoPorts - pointer to AllowedToGoPorts  */
/*                          pu1Status        -  Status of entry takes values */
/*                          like  permanent, deleteOnReset and               */
/*                           deleteOnTimeout                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwGetStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr,
                           UINT2 u2RcvPort, tLocalPortList AllowedToGoPorts,
                           UINT1 *pu1Status, tMacAddr ConnectionId)
{
#ifdef NPAPI_WANTED
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
#ifdef NP_BACKWD_COMPATIBILITY
        MEMSET (PortList, 0, sizeof (tPortList));
        VlanConvertToIfPortList (AllowedToGoPorts, PortList);

        if (FsMiWrVlanHwGetStaticUcastEntry (u4ContextId, u4Fid, MacAddr,
                                             VLAN_GET_PHY_PORT (u2RcvPort),
                                             PortList,
                                             pu1Status) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }

        if (FsMiWrVlanHwGetStaticUcastEntryEx (u4ContextId, u4Fid, MacAddr,
                                               VLAN_GET_PHY_PORT (u2RcvPort),
                                               PortList,
                                               pu1Status, ConnectionId)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
#else
        if (FsMiWrVlanHwGetStaticUcastEntry (u4ContextId, u4Fid, MacAddr,
                                             VLAN_GET_PHY_PORT (u2RcvPort),
                                             AllowedToGoPorts,
                                             pu1Status) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }

        if (FsMiWrVlanHwGetStaticUcastEntryEx (u4ContextId, u4Fid, MacAddr,
                                               VLAN_GET_PHY_PORT (u2RcvPort),
                                               AllowedToGoPorts,
                                               pu1Status, ConnectionId)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
#endif

    }

#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u2RcvPort);
    UNUSED_PARAM (AllowedToGoPorts);
    UNUSED_PARAM (pu1Status);
    UNUSED_PARAM (ConnectionId);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwGetSyncedTnlProtocolMacAddr                    */
/*                                                                           */
/* Description        : This function validates filter mac address with sync */
/*                      mac address.                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      MacAddr - MAC address used for protocol tunneling.   */
/*                      u2Protocol - L2 protocol for which the tunnel MAC    */
/*                                   address is configured for tunneling.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwGetSyncedTnlProtocolMacAddr (UINT4 u4ContextId, UINT2 u2Protocol,
                                   tMacAddr MacAddr)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwGetSyncedTnlProtocolMacAddr (u4ContextId, u2Protocol,
                                                       MacAddr) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (u2Protocol);
#endif

    return VLAN_SUCCESS;
}

#endif /* L2RED_WANTED */

/*****************************************************************************/
/*    Function Name       : VlanHwInit                                       */
/*                                                                           */
/*    Description         : This function takes care of initialising the     */
/*                          hardware related parameters.                     */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwInit (UINT4 u4ContextId)
{
#ifdef NPAPI_WANTED

    /*  VLAN_IS_NP_PROGRAMMING_ALLOWED () check is not needed here because
     *  FsMiVlanHwInit initialisation needs to be done for both active and
     *  standby node. This can not be done when vlan receives GO_ACTIVE,
     *  because during standby to active transition if we do
     *  initilalisation, all sync up done in NPAPI level will be losed.
     */
    if (VlanFsMiVlanHwInit (u4ContextId) == FNP_FAILURE)
    {
        return VLAN_FAILURE;
    }

    /* Configures the HW mac learning method adopted in the system */
    if (FNP_FAILURE ==
        VlanFsVlanHwGetMacLearningMode (&gu4VlanPlatformLearningMode))
    {
        return VLAN_FAILURE;
    }

#else
    UNUSED_PARAM (u4ContextId);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwTraffClassMapInit                          */
/*                                                                           */
/*    Description         : This function sets which cosq a given priority   */
/*                          should fall into in all units.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u1Priority - Cosq Priority                       */
/*                          i4CosqValue - Cosq Value                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/

INT4
VlanHwTraffClassMapInit (UINT4 u4ContextId, UINT1 u1Priority, INT4 i4CosqValue)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwTraffClassMapInit (u4ContextId, u1Priority,
                                             i4CosqValue) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Priority);
    UNUSED_PARAM (i4CosqValue);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwVlanEnable                                 */
/*                                                                           */
/*    Description         : This function enables hardware.                  */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwVlanEnable (UINT4 u4ContextId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwVlanEnable (u4ContextId) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetVlanLearningType                        */
/*                                                                           */
/*    Description         : This function programs the Vlan Learning Type    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u1LearningType - IVL/SVL/HVL(Hybrid Vlan Learn)  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetVlanLearningType (UINT4 u4ContextId, UINT1 u1LearningType)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetVlanLearningType (u4ContextId, u1LearningType)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1LearningType);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwAssociateVlanFdb                           */
/*                                                                           */
/*    Description         : This function will be used to associate a VlanId */
/*                          with an Fid in H/w.                              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Fid - FDB Id.                                  */
/*                          VlanId - Vlan Identifier.                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwAssociateVlanFdb (UINT4 u4ContextId, UINT4 u4Fid, tVlanId VlanId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwAssociateVlanFdb (u4ContextId, u4Fid, VlanId)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (VlanId);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwDisassociateVlanFdb                        */
/*                                                                           */
/*    Description         : This function will be used to disassociate a     */
/*                          VlanId from a Fid in H/W                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Fid - FDB ID.                                  */
/*                          VlanId - VlanIdentifier.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwDisassociateVlanFdb (UINT4 u4ContextId, UINT4 u4Fid, tVlanId VlanId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwDisassociateVlanFdb (u4ContextId, u4Fid, VlanId)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (VlanId);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwCreateFdbId                                */
/*                                                                           */
/*    Description         : This function will be used to create an Fid      */
/*                          entry in H/W.                                    */
/*                          If the FID is already created in the hardware,   */
/*                          the function should return success.              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Fid - Fdb Id                                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwCreateFdbId (UINT4 u4ContextId, UINT4 u4Fid)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwCreateFdbId (u4ContextId, u4Fid) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwDeleteFdbId                                */
/*                                                                           */
/*    Description         : This function will be used to delete an Fid      */
/*                          entry in H/W.                                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4Fid - FDB Id                                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwDeleteFdbId (UINT4 u4ContextId, UINT4 u4Fid)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwDeleteFdbId (u4ContextId, u4Fid) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetBrgMode                                 */
/*                                                                           */
/*    Description         : This function will be used to configure the      */
/*                          bridge mode in H/W.                              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4BridgeMode - CB/PB/PCB/PEB                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetBrgMode (UINT4 u4ContextId, UINT4 u4BridgeMode)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetBrgMode (u4ContextId, u4BridgeMode) == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4BridgeMode);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanHwSetBaseBrgMode                             */
/*                                                                           */
/*    Description         : This function will be used to configure the      */
/*                          base-bridge mode in H/W.                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u4BridgeMode - TB/VB                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetBaseBrgMode (UINT4 u4ContextId, UINT4 u4BaseBridgeMode)
{
#if defined (NPAPI_WANTED) && !defined (NP_BACKWD_COMPATIBILITY)
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetBaseBridgeMode (u4ContextId, u4BaseBridgeMode)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4BaseBridgeMode);
#endif
    return VLAN_SUCCESS;
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/*    Function Name       : VlanHwSyncDefaultVlanId                          */
/*                                                                           */
/*    Description         : This function will be used to configure the      */
/*                          default vlan Id in H/W.                          */
/*                                                                           */
/*    Input(s)            : u2DefaultVlanId                                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSyncDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSyncDefaultVlanId (u4ContextId, VlanId) ==
            FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanRedHwUpdateDBForDefaultVlanId                */
/*                                                                           */
/*    Description         : This function will be used to configure the      */
/*                          default vlan Id in H/W.                          */
/*                                                                           */
/*    Input(s)            : VlanId - Default Vlan Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanRedHwUpdateDBForDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanRedHwUpdateDBForDefaultVlanId (u4ContextId, VlanId)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
#endif
    return VLAN_SUCCESS;
}
#endif

/*****************************************************************************/
/*    Function Name       : VlanHwSetDefaultVlanId                           */
/*                                                                           */
/*    Description         : This function will be used to configure the      */
/*                          default vlan Id in H/W.                          */
/*                                                                           */
/*    Input(s)            : u2DefaultVlanId                                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanHwSetDefaultVlanId (UINT4 u4ContextId, UINT2 u2DefaultVlanId)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetDefaultVlanId (u4ContextId, u2DefaultVlanId)
            == FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2DefaultVlanId);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleSubnetBasedOnPort                      */
/*                                                                           */
/*    Description         : This function adds/deletes the all the Subnet map*/
/*                          entries for the given port from the VLAN-Subnet  */
/*                          map table in the hardware.                       */
/*                                                                           */
/*    Input(s)            : u4Port - Port for which the entries to           */
/*                          be deleted.                                      */
/*                        : u1Flag - Flag for enable/disable status          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleSubnetBasedOnPort (UINT4 u4Port, UINT1 u1Flag)
{
    tVlanSubnetMapEntry *pSubnetMapEntry = NULL;
    tVlanSubnetMapIndex SubnetMapIndex;

    MEMSET (&SubnetMapIndex, VLAN_INIT_VAL, sizeof (tVlanSubnetMapIndex));

    pSubnetMapEntry = VlanSubnetMapTableGetFirstEntryOnPort (u4Port);

    if (pSubnetMapEntry == NULL)
    {
        return;
    }

    do
    {
        SubnetMapIndex.u4Port = pSubnetMapEntry->u4Port;
        SubnetMapIndex.SubnetAddr = pSubnetMapEntry->SubnetAddr;
        SubnetMapIndex.SubnetMask = pSubnetMapEntry->SubnetMask;

        if (u1Flag == VLAN_SNMP_TRUE)
        {
            VlanHwAddPortSubnetVlanEntry (VLAN_CURR_CONTEXT_ID (),
                                          VLAN_GET_PHY_PORT ((UINT2) u4Port),
                                          pSubnetMapEntry->SubnetAddr,
                                          pSubnetMapEntry->SubnetMask,
                                          pSubnetMapEntry->VlanId,
                                          pSubnetMapEntry->u1ArpOption);
        }
        else
        {
            VlanHwDeletePortSubnetVlanEntry (VLAN_CURR_CONTEXT_ID (),
                                             VLAN_GET_PHY_PORT ((UINT2) u4Port),
                                             pSubnetMapEntry->SubnetAddr,
                                             pSubnetMapEntry->SubnetMask);
        }

        pSubnetMapEntry = VlanSubnetMapTableGetNextEntryOnPort (SubnetMapIndex);

    }
    while (pSubnetMapEntry != NULL);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleMacBasedOnPort                         */
/*                                                                           */
/*    Description         : This function adds/deletes the all the MAC map   */
/*                          entries for the given port from the VLAN MAC MAP */
/*                          table in the hardware.                           */
/*                                                                           */
/*    Input(s)            : u4Port - Port for which the entries to           */
/*                          be deleted.                                      */
/*                                                                           */
/*                        : u1Flag - Flag for enable/disable status          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleMacBasedOnPort (UINT4 u4Port, UINT1 u1Flag)
{
    UINT2               u2HashIndex = 0;
    tVlanMacMapEntry   *pMacMapEntry = NULL;
    tVlanMacMapEntry   *pCurrMacEntry = NULL;

    VLAN_HASH_MAC_MAP_ADDR (u4Port, u2HashIndex);

    pMacMapEntry = VLAN_GET_MAC_MAP_ENTRY (u2HashIndex);

    while (pMacMapEntry != NULL)
    {
        pCurrMacEntry = pMacMapEntry->pNextHashNode;
        if (pMacMapEntry->u4Port == u4Port)
        {
            if (u1Flag == VLAN_SNMP_TRUE)
            {
                if (VlanHwAddPortMacVlanEntry (VLAN_CURR_CONTEXT_ID (),
                                               VLAN_GET_PHY_PORT ((UINT2)
                                                                  u4Port),
                                               pMacMapEntry->MacAddr,
                                               pMacMapEntry->VlanId,
                                               pMacMapEntry->u1McastOption) !=
                    VLAN_SUCCESS)
                {
                    VLAN_TRC (VLAN_MOD_TRC, MGMT_TRC | ALL_FAILURE_TRC,
                              VLAN_NAME, "Updating MAC VLAN entry failed\n");
                    return;
                }
            }
            else
            {
                VlanHwDeletePortMacVlanEntry (VLAN_CURR_CONTEXT_ID (),
                                              VLAN_GET_PHY_PORT
                                              ((UINT2) u4Port),
                                              pMacMapEntry->MacAddr);
            }
        }

        pMacMapEntry = pCurrMacEntry;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetStats                                     */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          get vlan statistics information.                 */
/*                                                                           */
/*    Input(s)            : i4Port - Interface for which Statistics needed   */
/*                          i1StatType - Statistic Type                      */
/*                                                                           */
/*    Output(s)           : pu4Value - Stats Value.                          */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHwGetVlanStats (INT4 u4ContextId, UINT4 i4VlanId, UINT1 i1StatType,
                    UINT4 *pu4Value)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        switch (i1StatType)
        {
            case VLAN_STAT_UCAST_IN_FRAMES:
                i1StatType = NP_STAT_VLAN_UCAST_IN_FRAMES;
                break;
            case VLAN_STAT_MCAST_BCAST_IN_FRAMES:
                i1StatType = NP_STAT_VLAN_MCAST_BCAST_IN_FRAMES;
                break;
            case VLAN_STAT_UNKNOWN_UCAST_OUT_FRAMES:
                i1StatType = NP_STAT_VLAN_UNKNOWN_UCAST_OUT_FRAMES;
                break;
            case VLAN_STAT_UCAST_OUT_FRAMES:
                i1StatType = NP_STAT_VLAN_UCAST_OUT_FRAMES;
                break;
            case VLAN_STAT_BCAST_OUT_FRAMES:
                i1StatType = NP_STAT_VLAN_BCAST_OUT_FRAMES;
                break;
            case VLAN_STAT_VLAN_IN_FRAMES:
                i1StatType = NP_STAT_VLAN_IN_FRAMES;
                break;
            case VLAN_STAT_VLAN_IN_BYTES:
                i1StatType = NP_STAT_VLAN_IN_BYTES;
                break;
            case VLAN_STAT_VLAN_OUT_FRAMES:
                i1StatType = NP_STAT_VLAN_OUT_FRAMES;
                break;
            case VLAN_STAT_VLAN_OUT_BYTES:
                i1StatType = NP_STAT_VLAN_OUT_BYTES;
                break;
            case VLAN_STAT_VLAN_DISCARD_FRAMES:
                i1StatType = NP_STAT_VLAN_DISCARD_FRAMES;
                break;
            case VLAN_STAT_VLAN_DISCARD_BYTES:
                i1StatType = NP_STAT_VLAN_DISCARD_BYTES;
                break;

        }
        if (VlanFsMiVlanHwGetVlanStats (u4ContextId,
                                        i4VlanId,
                                        i1StatType, pu4Value) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    tVlanCurrEntry     *pCurrEntry;
    tVlanStats         *pVlanCounter;

    UNUSED_PARAM (u4ContextId);
    pCurrEntry = VlanGetVlanEntry ((tVlanId) i4VlanId);

    if (pCurrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pVlanCounter = pCurrEntry->pVlanStats;
    if (pVlanCounter == NULL)
    {
        return SNMP_FAILURE;
    }
    switch (i1StatType)
    {
        case VLAN_STAT_UCAST_IN_FRAMES:
            *pu4Value = pVlanCounter->u4RxUcastCnt;
            break;
        case VLAN_STAT_MCAST_BCAST_IN_FRAMES:
            *pu4Value = pVlanCounter->u4RxMcastBcastCnt;
            break;
        case VLAN_STAT_UNKNOWN_UCAST_OUT_FRAMES:
            *pu4Value = pVlanCounter->u4TxUnknUcastCnt;
            break;
        case VLAN_STAT_UCAST_OUT_FRAMES:
            *pu4Value = pVlanCounter->u4TxUcastCnt;
            break;
        case VLAN_STAT_BCAST_OUT_FRAMES:
            *pu4Value = pVlanCounter->u4TxBcastCnt;
            break;
    }
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwMacLearningStatus                          */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          set the mac learning status.                     */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context ID of the current context. */
/*                          VlanId - Vlan Id for which the learning status   */
/*                                   has to be configured.                   */
/*                          EgressPorts - Member ports of the given VLAN     */
/*                          u1Status - Mac Learning status to be set.        */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHwMacLearningStatus (UINT4 u4ContextId, tVlanId VlanId,
                         tLocalPortList EgressPorts, UINT1 u1Status)
{
#ifdef NPAPI_WANTED
    UINT4               u4FdbId;
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           EgressPortList;
#endif

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return VLAN_SUCCESS;
    }

    /* If EgressPorts is gNullPortList, then disable/enable learning for
     * given vlan, otherwise 
     * disable/enable learning for given ports of vlan.
     */
    if (VlanGetFdbIdFromVlanId (VlanId, &u4FdbId) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }
#ifdef NP_BACKWD_COMPATIBILITY
    MEMSET (EgressPortList, 0, sizeof (tPortList));
    VlanConvertToIfPortList (EgressPorts, EgressPortList);

    if (FsMiWrVlanHwMacLearningStatus (u4ContextId, VlanId, (UINT2) u4FdbId,
                                       EgressPortList, u1Status) != FNP_SUCCESS)
    {
        return VLAN_FAILURE;
    }
#else
    if (FsMiWrVlanHwMacLearningStatus (u4ContextId, VlanId, (UINT2) u4FdbId,
                                       EgressPorts, u1Status) != FNP_SUCCESS)
    {
        return VLAN_FAILURE;
    }
#endif
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (EgressPorts);
    UNUSED_PARAM (u1Status);

#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwMacLearningLimit                           */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          set the mac learning Limit.                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context ID of the current context. */
/*                          VlanId - Vlan Id for which the learning  limit   */
/*                                   has to be configured.                   */
/*                          u4MacLimit - Mac Learning limit to be set.       */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHwMacLearningLimit (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4MacLimit)
{
#ifdef NPAPI_WANTED
    UINT4               u4FdbId = 0;
    INT4                i4RetVal = VLAN_SUCCESS;

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return VLAN_SUCCESS;
    }

    if (VlanGetFdbIdFromVlanId (VlanId, &u4FdbId) == VLAN_FAILURE)
    {
        return VLAN_FAILURE;
    }

    i4RetVal = VlanFsMiVlanHwMacLearningLimit (u4ContextId, VlanId, (UINT2) u4FdbId,
               u4MacLimit);

    if (i4RetVal != FNP_SUCCESS)
    {
        if (i4RetVal == FNP_NOT_SUPPORTED)
        {
            return VLAN_UNSUPPORTED;
        }
        return VLAN_FAILURE;
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4MacLimit);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetFidPortLearningStatus                       */
/*                                                                           */
/* Description        : This function is called for configuring the Mac      */
/*                      learning status for a Port in the given Fid and      */
/*                      in give  context.                                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4Fid       - Fdb Identifier                         */
/*                      HwPortList - Port list where MAC learning status    */
/*                      should be applied                                    */
/*                      u2Port - Port where mac lreaning status will applied */
/*                               it will be used only if HwPortList is null.*/
/*                      u1Action - learning enable/disable                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/

INT4
VlanHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid,
                                tLocalPortList HwPortList, UINT2 u2Port,
                                UINT1 u1Action)
{
    INT4                i4RetVal = VLAN_SUCCESS;
#ifdef NPAPI_WANTED
#ifdef NP_BACKWD_COMPATIBILITY
    tPortList           PortList;
#endif
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return VLAN_SUCCESS;
    }
#ifdef NP_BACKWD_COMPATIBILITY
    MEMSET (PortList, 0, sizeof (tPortList));
    VlanConvertToIfPortList (HwPortList, PortList);

    i4RetVal = FsMiWrVlanHwSetFidPortLearningStatus
        (u4ContextId, u4Fid, PortList, VLAN_GET_PHY_PORT (u2Port), u1Action);
#else
    i4RetVal = FsMiWrVlanHwSetFidPortLearningStatus
        (u4ContextId, u4Fid, HwPortList, VLAN_GET_PHY_PORT (u2Port), u1Action);
#endif
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Fid);
    UNUSED_PARAM (HwPortList);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1Action);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetProtocolTunnelStatusOnPort                  */
/*                                                                           */
/* Description        : This function is called for configuring the STP      */
/*                      protocol tunnel status for the given port in         */
/*                      given context.                                       */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u2Port       - Local Port Identifier.                */
/*                      i4TunnelProtocol - Value to configure as status.  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                                     tVlanHwTunnelFilters ProtocolId,
                                     INT4 i4TunnelProtocol)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return VLAN_SUCCESS;
    }

    return (VlanFsMiVlanHwSetProtocolTunnelStatusOnPort
            (u4ContextId, VLAN_GET_PHY_PORT (u2Port), ProtocolId,
             i4TunnelProtocol));
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (ProtocolId);
    UNUSED_PARAM (i4TunnelProtocol);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetProtectedStatusOnPort                       */
/*                                                                           */
/* Description        : This function is called for configuring the          */
/*                      Protected/Split Horizon Status on por.               */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u2Port       - Local Port Identifier.                */
/*                      i4TunnelProtocolStp - Value to configure as status.  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS - On FNP_SUCCESS                         */
/*                      FNP_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetProtectedStatusOnPort (UINT4 u4ContextId, UINT2 u2Port, INT4
                                i4SetValDot1qFutureVlanPortProtected)
{
    INT4                i4RetVal = VLAN_SUCCESS;
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return VLAN_SUCCESS;
    }
    i4RetVal = VlanFsMiVlanHwSetPortProtectedStatus
        (u4ContextId, VLAN_GET_PHY_PORT (u2Port),
         i4SetValDot1qFutureVlanPortProtected);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (i4SetValDot1qFutureVlanPortProtected);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwSwitchMacLearningLimit                     */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          set the mac learning Limit for the context.      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context ID of the current context. */
/*                          u4MacLimit - Mac Learning limit to be set.       */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHwSwitchMacLearningLimit (UINT4 u4ContextId, UINT4 u4MacLimit)
{
#ifdef NPAPI_WANTED

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_FALSE)
    {
        return VLAN_SUCCESS;
    }

    if (VlanFsMiVlanHwSwitchMacLearningLimit (u4ContextId, u4MacLimit)
        != FNP_SUCCESS)
    {
        return VLAN_FAILURE;
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MacLimit);
#endif

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwSetDefGroupInfoForSisp                     */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          set the default group information.               */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual Switch Context Identfier   */
/*                          VlanId - Vlan identifier number                  */
/*                          u1Type  - All Groups / Unreg Groups              */
/*                          PortList - Port List                             */
/*                          u4DstPort - Port to be programmed                */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHwSetDefGroupInfoForSisp (UINT4 u4ContextId, tVlanId VlanId,
                              UINT1 u1Type, tLocalPortList HwPortList,
                              UINT4 u4DstPort)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (u1Type == VLAN_ALL_GROUPS)
        {
            if (FsMiWrVlanHwSetAllGroupsPortsForSisp (u4ContextId, VlanId,
                                                      HwPortList,
                                                      u4DstPort) != FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }
        }
        else
        {
            if (FsMiWrVlanHwSetUnRegGroupsPortsForSisp (u4ContextId, VlanId,
                                                        HwPortList,
                                                        u4DstPort) !=
                FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (HwPortList);
    UNUSED_PARAM (u4DstPort);
#endif

    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHandleRemoveMacBasedOnPort                   */
/*                                                                           */
/*    Description         : This function deletes the all the MAC map        */
/*                          entries for the given ifindex from the VLAN      */
/*                          MAC MAP table in the hardware.                   */
/*                                                                           */
/*    Input(s)            : u4DstPort - Port for which the entries to        */
/*                          be deleted.                                      */
/*                                                                           */
/*                        : u4SrcPort - Portchannel index for which the MAC  */
/*                          MAC MAP entries have been configured             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanHandleRemoveMacBasedOnPort (UINT4 u4DstPort, UINT4 u4SrcPort)
{
    UINT2               u2HashIndex = 0;
    tVlanMacMapEntry   *pMacMapEntry = NULL;
    tVlanMacMapEntry   *pCurrMacEntry = NULL;

    VLAN_HASH_MAC_MAP_ADDR (u4SrcPort, u2HashIndex);
    pMacMapEntry = VLAN_GET_MAC_MAP_ENTRY (u2HashIndex);
    while (pMacMapEntry != NULL)
    {
        pCurrMacEntry = pMacMapEntry->pNextHashNode;
        if (pMacMapEntry->u4Port == u4SrcPort)
        {
            VlanHwDeletePortMacVlanEntry (VLAN_CURR_CONTEXT_ID (),
                                          u4DstPort, pMacMapEntry->MacAddr);
        }

        pMacMapEntry = pCurrMacEntry;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwResetDefGroupInfoForPort                   */
/*                                                                           */
/*    Description         : This function calls a hardware api to            */
/*                          reset the default group information.             */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual Switch Context Identfier   */
/*                          VlanId - Vlan identifier number                  */
/*                          u4IfIndex - Interface Index                      */
/*                          u1Type  - All Groups / Unreg Groups              */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/

INT4
VlanHwResetDefGroupInfoForPort (UINT4 u4ContextId, tVlanId VlanId,
                                UINT4 u4IfIndex, UINT1 u1Type)
{

#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (FsMiWrVlanHwResetDefGroupInfoForPort (u4ContextId, VlanId,
                                                  u4IfIndex,
                                                  u1Type) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Type);
#endif

    return (VLAN_SUCCESS);
}

/**************************************************************************/
/* Function Name       : VlanMiHwResetVlanMemberPort                      */
/*                                                                        */
/* Description         : This function invokes NPAPI to remove a  member  */
/*                       port from a VLAN.                                */
/*                                                                        */
/* Input(s)            : VlanId - VLAN identifier.                        */
/*                       u2Port - Port number to be removed.              */
/*                                                                        */
/* Output(s)           : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                        */
/**************************************************************************/

INT4
VlanMiHwResetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {

        if (VlanFsMiVlanHwResetVlanMemberPort (u4ContextId,
                                               VlanId,
                                               u4IfIndex) != FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);
#endif
    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : VlanHwForwardOnPortList                         */
/*    Description         : This function transmits the provided packet to   */
/*                          the given port list. This function should also   */
/*                          apply the egress rules for the ports and         */
/*                          add/update/remove VLAN tag of the packet based on*/
/*                          the VLAN configuration before transmitting the   */
/*                          packet.                                          */
/*    Input (s)           : u4ContextId   - Context Identifier               */
/*                          pVlanTag      - Pointer to frame's VLAN tag      */
/*                                          information.                     */
/*                          pFrame        - Pointer to the frame to be       */
/*                                          transmitted.                     */
/*                          PortList      - Bit list containing list of pots */
/*                                          on which the frame will be       */
/*                                          transmitted.                     */
/*    Output(s)           : None                                             */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*****************************************************************************/
INT4
VlanHwForwardOnPortList (UINT4 u4ContextId,
                         tVlanTag * pVlanTag,
                         tCRU_BUF_CHAIN_HEADER * pFrame,
                         tLocalPortList PortList)
{
#ifdef NPAPI_WANTED
    UINT1              *pu1LocalPortList = NULL;
    UINT1              *pu1TempLocalPortList = NULL;
    tVlanCurrEntry     *pVlanEntry = NULL;
    UINT1              *pu1Packet = NULL;
    tHwVlanFwdInfo      VlanFwdInfo;
    static UINT4        au4HwTagPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    static UINT4        au4HwUnTagPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    INT4                i4RetVal = VLAN_FORWARD;
    UINT2               u2TagPortCounter = 0;
    UINT2               u2UnTagPortCounter = 0;
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT2               u2PacketLen = 0;
    static UINT1        au1Frame[VLAN_FRAME_MAXIMUM_LENGTH];
    UINT1               u1TagPortFlag = 0;
    UINT1               u1UnPortFlag = 0;
    tMacAddr            DestAddr;

    MEMSET (au4HwTagPorts, 0x00, L2IWF_MAX_PORTS_PER_CONTEXT * sizeof (UINT4));
    MEMSET (au4HwUnTagPorts, 0x00,
            L2IWF_MAX_PORTS_PER_CONTEXT * sizeof (UINT4));
    MEMSET (&VlanFwdInfo, 0x00, sizeof (tHwVlanFwdInfo));
    pVlanEntry = VlanGetVlanEntry (pVlanTag->OuterVlanTag.u2VlanId);
    if (pVlanEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    VLAN_COPY_FROM_BUF (pFrame, DestAddr, 0, ETHERNET_ADDR_SIZE);

    pu1LocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1LocalPortList == NULL)
    {
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Memory allocation failed for tLocalPortList\n");
        return VLAN_FAILURE;
    }
    MEMSET (pu1LocalPortList, 0, sizeof (tLocalPortList));
    if (pVlanEntry->pStVlanEntry != NULL)
    {
        VLAN_GET_UNTAGGED_PORTS (pVlanEntry->pStVlanEntry, pu1LocalPortList);
    }

    pu1TempLocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pu1TempLocalPortList == NULL)
    {
        UtilPlstReleaseLocalPortList (pu1LocalPortList);
        VLAN_TRC (VLAN_MOD_TRC, ALL_FAILURE_TRC, VLAN_NAME,
                  "Memory allocation failed for tLocalPortList\n");
        return VLAN_FAILURE;
    }
    MEMSET (pu1TempLocalPortList, 0, sizeof (tLocalPortList));
    VLAN_GET_CURR_EGRESS_PORTS (pVlanEntry, pu1TempLocalPortList);
    /* Form the HwPort Array */
    for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
    {
        if (PortList[u2ByteInd] == 0)
        {
            continue;
        }
        /* Form the tagged+untagged list */
        u1TagPortFlag = PortList[u2ByteInd] & pu1TempLocalPortList[u2ByteInd];
        for (u2BitIndex = 0;
             ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1TagPortFlag != 0)
              && (u2TagPortCounter < L2IWF_MAX_PORTS_PER_CONTEXT));
             u2BitIndex++)
        {

            if ((u1TagPortFlag & VLAN_BIT8) != 0)
            {
                u2Port =
                    (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                             u2BitIndex + 1);
                /* Apply egress rules to the packet */
                i4RetVal = VlanEgressFiltering (u2Port, pVlanEntry, DestAddr);
                if (i4RetVal == VLAN_FORWARD)
                {
                    au4HwTagPorts[u2TagPortCounter++] = u2Port;
                }
                else
                {
                    /* If the port is blocked, then disable it in egress list */
                    VLAN_RESET_MEMBER_PORT (PortList, u2Port);
                }
            }
            u1TagPortFlag = (UINT1) (u1TagPortFlag << 1);
        }
        /* Form the untagged list */
        if (pVlanEntry->pStVlanEntry != NULL)
        {
            u1UnPortFlag = PortList[u2ByteInd] & pu1LocalPortList[u2ByteInd];
        }
        for (u2BitIndex = 0;
             ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1UnPortFlag != 0)
              && (u2UnTagPortCounter < L2IWF_MAX_PORTS_PER_CONTEXT));
             u2BitIndex++)
        {

            if ((u1UnPortFlag & VLAN_BIT8) != 0)
            {
                u2Port =
                    (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                             u2BitIndex + 1);
                au4HwUnTagPorts[u2UnTagPortCounter++] = u2Port;
            }
            u1UnPortFlag = (UINT1) (u1UnPortFlag << 1);
        }
    }
    UtilPlstReleaseLocalPortList (pu1LocalPortList);
    UtilPlstReleaseLocalPortList (pu1TempLocalPortList);
    /* Fill the nessary parameters to transmit the frame to multiple ports
     *      */
    VlanFwdInfo.TagPorts.i4Length = u2TagPortCounter;
    VlanFwdInfo.TagPorts.pu4PortArray = au4HwTagPorts;

    VlanFwdInfo.UnTagPorts.i4Length = u2UnTagPortCounter;
    VlanFwdInfo.UnTagPorts.pu4PortArray = au4HwUnTagPorts;

    u2PacketLen = (UINT2) VLAN_GET_BUF_LEN (pFrame);
    pu1Packet = VLAN_BUF_IF_LINEAR (pFrame, 0, u2PacketLen);
    VlanFwdInfo.u4ContextId = u4ContextId;
    MEMCPY (&VlanFwdInfo.VlanTag, pVlanTag, sizeof (tVlanTag));
    if (pu1Packet == NULL)
    {
        MEMSET (au1Frame, 0x00, u2PacketLen);
        VLAN_COPY_FROM_BUF (pFrame, au1Frame, 0, u2PacketLen);
        pu1Packet = au1Frame;
    }
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        /* Call the H/W API to transmit the frame to multiple ports */
        if (VlanFsVlanHwForwardPktOnPorts (pu1Packet, u2PacketLen,
                                           &VlanFwdInfo) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
    return VLAN_SUCCESS;
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pVlanTag);
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (PortList);
    return VLAN_FAILURE;
#endif
}

/*****************************************************************************/
/*    Function Name       : VlanHwFlushPortFdbList                             */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires learnt on  */
/*                          a specific port in a specific Fdb table          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Port      - Port Interface Index               */
/*                          u4Fid   - Fdb Id in which the Entry has to be    */
/*                                   flushed.                                */
/*                           i4OptimizeFlag - Indicates whether this call can*/
/*                                            be grouped with the flush call */
/*                                            for other ports as a single    */
/*                                            flush call                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*****************************************************************************/

INT4
VlanHwFlushFdbList (UINT4 u4ContextId, tVlanFlushInfo * pVlanFlushInfo)
{
#ifdef NPAPI_WANTED

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwFlushPortFdbList (u4ContextId, pVlanFlushInfo) ==
            FNP_FAILURE)
        {
            return VLAN_FAILURE;
        }

    }

    VlanDeleteFdbEntriesForPort ( pVlanFlushInfo->u4IfIndex);
#ifdef ICCH_WANTED
    VlanIcchSyncPortFlush ( pVlanFlushInfo->u4IfIndex);
#endif
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pVlanFlushInfo);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanHwSetMcastIndex                              */
/*                                                                           */
/*    Description         : This function calls a Npapi to set the Multicast */
/*                          Index to be programmed in  Hardware              */
/*                                                                           */
/*    Input(s)            : MacAddr - Multicast address                      */
/*                          VlanId - Vlan identifier number                  */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion         : None.                                       */
/*                                                                           */
/*    Returns                  : VLAN_SUCCESS / VLAN_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanHwSetMcastIndex (tMacAddr MacAddr, tVlanId VlanId)
{
#ifdef NPAPI_WANTED
    tHwMcastIndexInfo   HwMcastIndexInfo;
    UINT4               u4McastIndex = 0;

    MEMSET (&HwMcastIndexInfo, 0, sizeof (tHwMcastIndexInfo));
    HwMcastIndexInfo.u4Opcode = VLAN_OPCODE_GET_MCAST_INDEX;
    HwMcastIndexInfo.VlanId = VlanId;
    MEMCPY (&(HwMcastIndexInfo.MacAddr[0]), &MacAddr[0], CFA_ENET_ADDR_LEN);

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        /* Set the Multicast Index in Hardware for Self Node */
        if (VlanFsMiVlanHwSetMcastIndex (HwMcastIndexInfo, &u4McastIndex) !=
            FNP_SUCCESS)
        {
            return (VLAN_FAILURE);
        }
        if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
        {
            /* In Dual Unit Stacking, Multicast Entry should be programmed 
             * in hardware with same Index in the peer node.
             * Invoke the same Npapi with the different opcode .
             * Pass the Index to be programmed in Hardware to peer Node */
            HwMcastIndexInfo.u4Opcode = VLAN_OPCODE_SET_MCAST_INDEX;
            HwMcastIndexInfo.u4McastIndex = u4McastIndex;
            if (VlanFsMiVlanHwSetMcastIndex (HwMcastIndexInfo, &u4McastIndex) !=
                FNP_SUCCESS)
            {
                return (VLAN_FAILURE);
            }
        }
    }
#else
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
#endif

    return (VLAN_SUCCESS);
}

/* Port Property Set. */
/*****************************************************************************/
/* Function Name      : VlanHwSetPortProperty.                               */
/*                                                                           */
/* Description        : This function sets the Port Properties               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      PortProperty - Port Property.                        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On success                            */
/*                      VLAN_FAILURE - On failure                            */
/*****************************************************************************/
INT4
VlanHwSetPortProperty (UINT4 u4ContextId, UINT4 u4IfIndex,
                       tHwVlanPortProperty VlanPortProperty)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortProperty (u4ContextId,
                                           u4IfIndex,
                                           VlanPortProperty) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (VlanPortProperty);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPortIngressEtherType.                       */
/*                                                                           */
/* Description        : This function is called when the ingress ether type  */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Ingress Ether type          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On Success                             */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPortIngressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                               UINT2 u2EtherType)
{
#ifdef NPAPI_WANTED
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4 u4LocalContextId = 0;
    UINT2 u2LocalPort = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4LocalContextId, &u2LocalPort) ==
        VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pVlanPortEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortIngressEtherType (u4ContextId,
                                                   u4IfIndex,
                                                   u2EtherType) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
        if (VlanFsMiVlanHwSetPortEgressEtherType (u4ContextId,
                                                  u4IfIndex,
                                                  pVlanPortEntry->
                                                  u2EgressEtherType) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherType);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetPortEgressEtherType.                        */
/*                                                                           */
/* Description        : This function is called when the Egress ether type   */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      u4IfIndex - Port Index.                              */
/*                      u2EtherType - Configured Egress Ether type           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On Success                            */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetPortEgressEtherType (UINT4 u4ContextId, UINT4 u4IfIndex,
                              UINT2 u2EtherType)
{
#ifdef NPAPI_WANTED
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4 u4LocalContextId = 0;
    UINT2 u2LocalPort = 0;

    if (VlanVcmGetContextInfoFromIfIndex (u4IfIndex, &u4LocalContextId, &u2LocalPort) ==
        VCM_FAILURE)
    {
        return VLAN_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPort);

    if (pVlanPortEntry == NULL)
    {
        return VLAN_FAILURE;
    }

    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetPortIngressEtherType (u4ContextId,
                                                   u4IfIndex,
                                                   pVlanPortEntry->
                                                   u2IngressEtherType) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }

        if (VlanFsMiVlanHwSetPortEgressEtherType (u4ContextId,
                                                  u4IfIndex,
                                                  u2EtherType) != FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2EtherType);
#endif
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanHwSetVlanLoopbackStatus                          */
/*                                                                           */
/* Description        : This function is called when the Loopback status is  */
/*                      configured for a vlan.                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Context Identifier.            */
/*                      VlanId - Vlan Identifier                             */
/*                      i4LoopbackStatus - Enabled/Disabled                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS - On Success                            */
/*                      VLAN_FAILURE - On failure                             */
/*****************************************************************************/
INT4
VlanHwSetVlanLoopbackStatus (UINT4 u4ContextId, tVlanId VlanId,
                             INT4 i4LoopbackStatus)
{
#ifdef NPAPI_WANTED
    if (VLAN_IS_NP_PROGRAMMING_ALLOWED () == VLAN_TRUE)
    {
        if (VlanFsMiVlanHwSetVlanLoopbackStatus (u4ContextId,
                                                 VlanId,
                                                 i4LoopbackStatus) !=
            FNP_SUCCESS)
        {
            return VLAN_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (i4LoopbackStatus);
#endif
    return VLAN_SUCCESS;
}

/***************************************************************************
 *
 *    Function Name       : VlanHwEvbConfigSChIface 
 *
 *    Description         : This function is called when EVB S-channel 
 *                          is configured. 
 *
 *    Input(s)            : pVlanEvbHwConfigInfo - Vlan EVB hardware            
 *                                                 configuration information     
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : VLAN_SUCCESS OR VLAN_FAILURE
 *
 *****************************************************************************/

INT4
VlanHwEvbConfigSChIface (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo)
{
    
#ifdef NPAPI_WANTED
    if(FsMiWrVlanHwEvbConfigSChIface(pVlanEvbHwConfigInfo) != FNP_SUCCESS)
    {
        return VLAN_FAILURE;
    }

#else
    UNUSED_PARAM(pVlanEvbHwConfigInfo);
#endif
    return VLAN_SUCCESS;
}


/***************************************************************************
 *
 *    Function Name       : VlanHwSetBridgePortType 
 *
 *    Description         : This function is called when Bridge Port type 
 *                          is changed to UAP. 
 *
 *    Input(s)            : pVlanHwPortInfo       - Vlan EVB hardware            
 *                                                 configuration information     
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : VLAN_SUCCESS OR VLAN_FAILURE
 *
 *****************************************************************************/

INT4
VlanHwSetBridgePortType (tVlanHwPortInfo *pVlanHwPortInfo)
{
#ifdef NPAPI_WANTED
    if (FsMiWrVlanHwSetBridgePortType(pVlanHwPortInfo) != FNP_SUCCESS)
    {
        return VLAN_FAILURE;
    }

#else
    UNUSED_PARAM (pVlanHwPortInfo);
#endif
    return VLAN_SUCCESS;
}


