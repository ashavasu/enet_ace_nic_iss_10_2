/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanl2wr.c,v 1.20 2015/07/31 05:51:05 siva Exp $
 *
 * Description: This file contains the L2IWF calls.                      
 *
 *******************************************************************/

#include "vlaninc.h"

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetBridgeMode                               */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      bridge mode.                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : pu4BridgeMode  - bridge mode.                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode)
{
    return (L2IwfGetBridgeMode (u4ContextId, pu4BridgeMode));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetPbPortState                              */
/*                                                                           */
/* Description        : This function is used to get Provider Edge Port State*/
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                                                                           */
/* Output(s)          : u1PortState - Port State of the PEP Port(CEP,SVID)   */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

UINT1
VlanL2IwfGetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId)
{
    return (L2IwfGetPbPortState (u4CepIfIndex, SVlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetPbPortType                               */
/*                                                                           */
/* Description        : This function is used to get the port type of a      */
/*                      Physical port                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                      u4IfIndex   - IfIndex of the port.                   */
/*                                                                           */
/* Output(s)          : *pu1PortType - CEP/CNP/PNP/PCEP/PCNP/PPNP            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
VlanL2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType)
{
    return (L2IwfGetPbPortType (u4IfIndex, pu1PortType));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetProtocolEnabledStatusOnPort              */
/*                                                                           */
/* Description        : This routine returns the given protocols status on   */
/*                      given Port. It accesses the L2Iwf common database.   */
/*                                                                           */
/* Input(s)           : u2IfIndex    - Index of the port whose port type     */
/*                                     is to be obtained.                    */
/*                      u2Protocol   - Protocol whose port status is needed. */
/*                                                                           */
/* Output(s)          : pu1Status    - Status of the given protocol on the   */
/*                                     given port.                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfGetProtocolEnabledStatusOnPort (UINT2 u2IfIndex, UINT2 u2Protocol,
                                         UINT1 *pu1Status)
{
    return (L2IwfGetProtocolEnabledStatusOnPort (u2IfIndex, u2Protocol,
                                                 pu1Status));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetVlanPortGvrpStatus                       */
/*                                                                           */
/* Description        : This routine returns the port's gvrp enabled/disabled*/
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2IfIndex   - Index of the port whose port type      */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : u1GvrpStatus- Gvrp enabled/disabled                  */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfGetVlanPortGvrpStatus (UINT2 u2IfIndex, UINT1 *pu1GvrpStatus)
{
    return (L2IwfGetVlanPortGvrpStatus (u2IfIndex, pu1GvrpStatus));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetVlanPortMvrpStatus                       */
/*                                                                           */
/* Description        : This routine returns the port's mvrp enabled/disabled*/
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Index of the port whose port type      */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : u1MvrpStatus- Gvrp enabled/disabled                  */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfGetVlanPortMvrpStatus (UINT4 u4IfIndex, UINT1 *pu1MvrpStatus)
{
    return (L2IwfGetVlanPortMvrpStatus (u4IfIndex, pu1MvrpStatus));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfIsVlanElan                                  */
/*                                                                           */
/* Description        : This function is called to check whether the VLAN    */
/*                      service type is configured as ELAN                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch Identifier              */
/*                      VlanId      - Vlan Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE / OSIX_FALSE                               */
/*****************************************************************************/

BOOL1
VlanL2IwfIsVlanElan (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfIsVlanElan (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfMiGetVlanFdbId                              */
/*                                                                           */
/* Description        : This routine returns the FdbId corresponding to the  */
/*                      given VlanId. It accesses the L2Iwf common database  */
/*                      for the given context                                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Fdb Id is to obtained       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FdbId value corresponding to the VlanId              */
/*****************************************************************************/
UINT4
VlanL2IwfMiGetVlanFdbId (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfMiGetVlanFdbId (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbConfigVidTransEntry                       */
/*                                                                           */
/* Description        : This routine configures an entry in vid translation  */
/*                      table. An entry configured through this function     */
/*                      results in inserting the node in the ingress vid     */
/*                      translation table and also in the egress vid         */
/*                      translation table.                                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                      VidTransEntryInfo  - This contains the following:    */
/*                              - u2Port - Port Id.                          */
/*                              - LocalVid - Local Vid for the entry.        */
/*                              - RelayVid - Relay Vid for the entry.        */
/*                              - u1RowStatus - Row Status for the entry.    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfPbConfigVidTransEntry (UINT4 u4ContextId,
                                tVidTransEntryInfo VidTransEntryInfo)
{
    return (L2IwfPbConfigVidTransEntry (u4ContextId, VidTransEntryInfo));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbCreateVidTransTable                       */
/*                                                                           */
/* Description        : This routine creates the vid translation table for a */
/*                      given context. This function will be called when     */
/*                      vlan is started for 1ad bridge.                      */
/*                                                                           */
/* Input(s)           : u4ContextId - virtual context id                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfPbCreateVidTransTable (UINT4 u4ContextId)
{
    return (L2IwfPbCreateVidTransTable (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbDeleteVidTransTable                       */
/*                                                                           */
/* Description        : This routine deletes the vid translation table for   */
/*                      given context. This function will be called when     */
/*                      vlan is shutdown for a context.                      */
/*                                                                           */
/* Input(s)           : u4ContextId - virtual context id                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfPbDeleteVidTransTable (UINT4 u4ContextId)
{
    return (L2IwfPbDeleteVidTransTable (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbGetLocalVidFromRelayVid                   */
/*                                                                           */
/* Description        : This routine gets the local vid for the corresponding*/
/*                      relay vid.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                      u2LocalPort - Per Context Port Index.                */
/*                      RelayVid    - Relay Vlan Id                          */
/*                                                                           */
/* Output(s)          : pLocalVid - LocalVid value for the given rely vid    */
/*                      Value.                                               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfPbGetLocalVidFromRelayVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                    tVlanId RelayVid, tVlanId * pLocalVid)
{
    return (L2IwfPbGetLocalVidFromRelayVid (u4ContextId, u2LocalPort,
                                            RelayVid, pLocalVid));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbGetNextVidTransTblEntry                   */
/*                                                                           */
/* Description        : This routine returns the valid lowest vid translation*/
/*                      table entry whose (port, Vid) pair is greater than   */
/*                      the given (port,LocalVid) pair.                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2Port      - Per Context port index                 */
/*                      LocalVid    - Local Vlan Id                          */
/*                                                                           */
/* Output(s)          : pOutVidTransEntryInfo - vid translation entry whose  */
/*                      (u2Port, LocalVid) pair is next (greater) to given   */
/*                      (u2Port, LocalVid) pair.                             */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfPbGetNextVidTransTblEntry (UINT4 u4ContextId, UINT2 u2Port,
                                    tVlanId LocalVid,
                                    tVidTransEntryInfo * pOutVidTransEntryInfo)
{
    return (L2IwfPbGetNextVidTransTblEntry (u4ContextId, u2Port, LocalVid,
                                            pOutVidTransEntryInfo));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbGetRelayVidFromLocalVid                   */
/*                                                                           */
/* Description        : This routine gets the relay vid for the corresponding*/
/*                      local vid.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                      u2LocalPort - Per Context Port Index.                */
/*                      LocalVid    - Local Vlan Id                          */
/*                                                                           */
/* Output(s)          : pRelayVid - RelayVid value for the given local vid   */
/*                      Value.                                               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfPbGetRelayVidFromLocalVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                    tVlanId LocalVid, tVlanId * pRelayVid)
{
    return (L2IwfPbGetRelayVidFromLocalVid (u4ContextId, u2LocalPort,
                                            LocalVid, pRelayVid));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbGetVidTransEntry                          */
/*                                                                           */
/* Description        : This routine gets an entry with the given inputs.    */
/*                      Based on u1IsLocalVid value, the search will be done */
/*                      on the ingress/egress vid translation table.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                      VidTransEntryInfo  - This contains the following:    */
/*                              - u2Port - Port Id.                          */
/*                              - LocalVid - Local Vid for the entry.        */
/*                              - RelayVid - Relay Vid for the entry.        */
/*                              - u1RowStatus - Row Status for the entry.    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfPbGetVidTransEntry (UINT4 u4ContextId, UINT2 u2LocalPort,
                             tVlanId SearchVid, UINT1 u1IsLocalVid,
                             tVidTransEntryInfo * pOutVidTransInfo)
{
    return (L2IwfPbGetVidTransEntry (u4ContextId, u2LocalPort, SearchVid,
                                     u1IsLocalVid, pOutVidTransInfo));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbGetVidTransStatus                         */
/*                                                                           */
/* Description        : This routine gets the vid translation status for the */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2LocalPort - Per Context port index                 */
/*                                                                           */
/* Output(s)          : pu1Status - vid translation status for this port.    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfPbGetVidTransStatus (UINT4 u4ContextId, UINT2 u2LocalPort,
                              UINT1 *pu1Status)
{
    return (L2IwfPbGetVidTransStatus (u4ContextId, u2LocalPort, pu1Status));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbGetVlanServiceType                        */
/*                                                                           */
/* Description        : This function is used to get the type of service the */
/*                      VLAN provides. The service type is updated by VLAN   */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : VlanId - Service VLAN Identifier                     */
/*                                                                           */
/* Output(s)          : *pu1ServiceType - VLAN_E_LINE /VLAN_E_LAN            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
VlanL2IwfPbGetVlanServiceType (UINT4 u4ContextId, tVlanId VlanId,
                               UINT1 *pu1ServiceType)
{
    return (L2IwfPbGetVlanServiceType (u4ContextId, VlanId, pu1ServiceType));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbSetPortType                               */
/*                                                                           */
/* Description        : This function is used to set the port type of a      */
/*                      Physical port in L2IWF                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                      u2Port      - port Index.                            */
/*                      i4PortType  - Port type                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanL2IwfPbSetPortType (UINT4 u4IfIndex, INT4 i4PortType)
{
    return (L2IwfPbSetPortType (u4IfIndex, i4PortType));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbSetVidTransStatus                         */
/*                                                                           */
/* Description        : This routine sets the vid translation status for the */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2LocalPortId - Per Context port index               */
/*                      u1Status - Vid translation status for this port.     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VlanL2IwfPbSetVidTransStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                              UINT1 u1Status)
{
    return (L2IwfPbSetVidTransStatus (u4ContextId, u2LocalPortId, u1Status));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfPbSetVlanServiceType                        */
/*                                                                           */
/* Description        : This function is used to set the VLAN service type   */
/*                      in L2IWF. The service type is updated by VLAN        */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : VlanId - Service VLAN Identifier                     */
/*                                                                           */
/* Output(s)          : u1ServiceType - VLAN_E_LINE /VLAN_E_LAN              */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
VlanL2IwfPbSetVlanServiceType (UINT4 u4ContextId, tVlanId VlanId,
                               UINT1 u1ServiceType)
{
    return (L2IwfPbSetVlanServiceType (u4ContextId, VlanId, u1ServiceType));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfRegisterSTDBRI                              */
/*                                                                           */
/* Description        : This routine is called to register the standard      */
/*                      bridge mib when bridge module is disabled            */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
VlanL2IwfRegisterSTDBRI (VOID)
{
    L2IwfRegisterSTDBRI ();
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfResetVlanEgressPort                         */
/*                                                                           */
/* Description        : This routine is called from VLAN to remove the given */
/*                      port from the Vlan's Egress port list in the L2Iwf   */
/*                      common database.                                     */
/*                      This function should not be called directly instead  */
/*                      call                                                 */
/*                      VlanNotifyResetVlanMemberToL2IwfAndEnhFltCriteria.   */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Egress portlist is to be    */
/*                                    updated.                               */
/*                      u2LocalPortId - Local Port number of the port to be  */
/*                                    added to the Egress port list          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfResetVlanEgressPort (UINT4 u4ContextId, tVlanId VlanId,
                              UINT2 u2LocalPortId)
{
    return (L2IwfResetVlanEgressPort (u4ContextId, VlanId, u2LocalPortId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfSetPortVlanTunnelStatus                     */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the       */
/*                      Port's Tunnelling status in the L2Iwf common         */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2LocalPortId - Local port index of the port whose   */
/*                                    oper edge                              */
/*                                    status is to be updated.               */
/*                      bTunnelPort - Boolean value indicating whether the   */
/*                                    port is a Tunnel port                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfSetPortVlanTunnelStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                  BOOL1 bTunnelPort)
{
    return (L2IwfSetPortVlanTunnelStatus (u4ContextId, u2LocalPortId,
                                          bTunnelPort));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2IwfSetProtocolTunnelStatusOnPort           */
/*                                                                           */
/*    Description         : This function sets the tunnel status (Tunnel/    */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) on a port        */
/*                                                                           */
/*    Input(s)            : u2Port         - Port number                     */
/*                          u2Protocol     - L2 Protocol                     */
/*                          u1TunnelStatus - Protocol tunnel status          */
/*                                           on a port.                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : L2IWF_SUCCESS / L2IWF_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2IwfSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                                        UINT2 u2Protocol, UINT1 u1TunnelStatus)
{
    return (L2IwfSetProtocolTunnelStatusOnPort (u4ContextId, u2Port,
                                                u2Protocol, u1TunnelStatus));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2IwfSetProtocolTunnelStatusPerVlan          */
/*                                                                           */
/*    Description         : This function sets the tunnel status (Tunnel/    */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) per vlan         */
/*                                                                           */
/*    Input(s)            : u4VlanId       - Vlan ID                         */
/*                          u4ContextId    - Context ID                      */
/*                          u2Protocol     - L2 Protocol                     */
/*                          u1TunnelStatus - Protocol tunnel status          */
/*                                           on a particular Vlan            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : L2IWF_SUCCESS / L2IWF_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2IwfSetProtocolTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                                         UINT2 u2Protocol, UINT1 u1TunnelStatus)
{
    return (L2IwfSetProtocolTunnelStatusPerVlan (u4ContextId, u4VlanId,
                                                 u2Protocol, u1TunnelStatus));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2IwfGetProtocolTunnelStatusPerVlan          */
/*                                                                           */
/*    Description         : This function gets the tunnel status (Tunnel/    */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) per vlan         */
/*                                                                           */
/*    Input(s)            : u4VlanId       - Vlan ID                         */
/*                          u4ContextId    - Context ID                      */
/*                          u2Protocol     - L2 Protocol                     */
/*                          u1TunnelStatus - Protocol tunnel status          */
/*                                           on a particular Vlan            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanL2IwfGetProtocolTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                                         UINT2 u2Protocol,
                                         UINT1 *pu1TunnelStatus)
{
    return (L2IwfGetProtocolTunnelStatusPerVlan (u4ContextId, u4VlanId,
                                                 u2Protocol, pu1TunnelStatus));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2IwfSetTunnelStatusPerVlan                  */
/*                                                                           */
/*    Description         : This function sets the tunnel status             */
/*                                                                           */
/*    Input(s)            : u4VlanId        - VLan ID                        */
/*                          u4ContextId     - Context Id                     */
/*                          u1TunnelStatus  - Tunnel status(enable/Disable)  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2IwfSetTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                                 UINT1 u1TunnelStatus)
{
    return (L2IwfSetTunnelStatusPerVlan
            (u4ContextId, u4VlanId, u1TunnelStatus));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2IwfSetOverrideOption                       */
/*                                                                           */
/*    Description         : This function sets the override option status    */
/*                                                                           */
/*    Input(s)            : u2Port         - Port ID                         */
/*                          u1OverrideOption  - OverrideOption               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2IwfSetOverrideOption (UINT2 u2Port, UINT1 u1OverrideOption)
{
    UINT4       u4IfIndex = 0;
    u4IfIndex = VLAN_GET_IFINDEX (u2Port);
    return (L2IwfSetOverrideOption (u4IfIndex, u1OverrideOption));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2IwfGetOverrideOption                       */
/*                                                                           */
/*    Description         : This function gets the override option status    */
/*                                                                           */
/*    Input(s)            : u2Port           - PortNumber                    */
/*                          pu1OverrideOption  - OverrideOption              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanL2IwfGetOverrideOption (UINT2 u2Port, UINT1 *pu1OverrideOption)
{
    UINT4       u4IfIndex = 0;
    u4IfIndex = VLAN_GET_IFINDEX (u2Port);
    return (L2IwfGetOverrideOption (u4IfIndex, pu1OverrideOption));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfUpdtVlanEgressPortList                      */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the given */
/*                      portlist to the Vlan's Egress port list in the L2Iwf */
/*                      common database                                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Context to which the vlan belongs      */
/*                      VlanId      - Vlan whose Egress portlist is to be    */
/*                                    updated.                               */
/*                      AddedPorts - Added members                           */
/*                      DeletedPorts - Deleted members                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfUpdtVlanEgressPortList (UINT4 u4ContextId, tVlanId VlanId,
                                 tLocalPortList AddedPorts,
                                 tLocalPortList DeletedPorts)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN + 1];

    MEMSET (au1VlanName, 0, (VLAN_STATIC_MAX_NAME_LEN + 1));
    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);
    if (pStVlanEntry != NULL)
    {
        MEMCPY (au1VlanName, pStVlanEntry->au1VlanName,
                pStVlanEntry->u1VlanNameLen);
    }

    return (L2IwfUpdateVlanEgressPortList (u4ContextId, VlanId, au1VlanName,
                                           AddedPorts, DeletedPorts));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfUpdtVlanUntagPortList                       */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the given */
/*                      portlist to the Vlan's untagged port list in the     */
/*                      L2IWF common database                                */
/*                                                                           */
/* Input(s)           : u4ContextId - Context to which the vlan belongs      */
/*                      VlanId      - Vlan whose Untag portlist is to be     */
/*                                    updated.                               */
/*                      AddedPorts - Added members                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfUpdtVlanUntagPortList (UINT4 u4ContextId, tVlanId VlanId,
                                tLocalPortList AddedPorts)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN + 1];

    MEMSET (au1VlanName, 0, (VLAN_STATIC_MAX_NAME_LEN + 1));
    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);
    if (pStVlanEntry != NULL)
    {
        MEMCPY (au1VlanName, pStVlanEntry->au1VlanName,
                pStVlanEntry->u1VlanNameLen);
    }

    return (L2IwfUpdateVlanUntagPortList (u4ContextId, VlanId, au1VlanName,
                                          AddedPorts));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfSetVlanEgressPort                           */
/*                                                                           */
/* Description        : This routine is called from VLAN to add the given    */
/*                      port to the Vlan's Egress port list in the L2Iwf     */
/*                      common database.                                     */
/*                      This function should not be called directly instead  */
/*                      call VlanNotifySetVlanMemberToL2IwfAndEnhFltCriteria.*/
/*                                                                           */
/* Input(s)           : VlanId      - Vlan whose Egress portlist is to be    */
/*                                    updated.                               */
/*                      u2LocalPortId - Local Port number of the port to be  */
/*                                    added to the Egress port list          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfSetVlanEgressPort (UINT4 u4ContextId, tVlanId VlanId,
                            UINT2 u2LocalPortId, INT4 i4PortType)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN + 1];
    INT4                i4Result;
    UINT1               u1IsUntagged = VLAN_FALSE;

    MEMSET (au1VlanName, 0, (VLAN_STATIC_MAX_NAME_LEN + 1));
    pStVlanEntry = VlanGetStaticVlanEntry (VlanId);
    if (pStVlanEntry != NULL)
    {
        MEMCPY (au1VlanName, pStVlanEntry->au1VlanName,
                pStVlanEntry->u1VlanNameLen);
    }

    if (i4PortType == VLAN_ADD_UNTAGGED_PORT)
    {
        u1IsUntagged = VLAN_TRUE;
    }

    i4Result = L2IwfSetVlanEgressPort (u4ContextId,
                                       VlanId,
                                       au1VlanName,
                                       u2LocalPortId, u1IsUntagged);

    return i4Result;
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfUpdateVlanName                              */
/*                                                                           */
/* Description        : This routine updates the vlan name for the given     */
/*                      vlan id in the PortVlan RBTree                       */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id to which this vlan belongs*/
/*                      VlanId        - VLAN ID value                        */
/*                      pVlanName     - Pointer to vlan name                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfUpdateVlanName (UINT4 u4ContextId, tVlanId VlanId, UINT1 *pVlanName)
{
    return (L2IwfUpdateVlanName (u4ContextId, VlanId, pVlanName));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfUpdateEnhFilterStatus                       */
/*                                                                           */
/* Description        : This routine updates the status of Enhanced Filtering*/
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id to which this vlan belongs*/
/*                      bEnhFilterStatus - Status of Enhanced Filtering      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfUpdateEnhFilterStatus (UINT4 u4ContextId, BOOL1 b1EnhFilterStatus)
{
    return (L2IwfUpdateEnhFilterStatus (u4ContextId, b1EnhFilterStatus));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfSetVlanFdbId                                */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the       */
/*                      Vlan's FdbId in the L2Iwf common database for the    */
/*                      given context.                                       */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Fdb Id is to updated        */
/*                      u4FdbId     - FdbId corresponding to the VlanId      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfSetVlanFdbId (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4FdbId)
{
    return (L2IwfSetVlanFdbId (u4ContextId, VlanId, u4FdbId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfStartHwAgeTimer                             */
/*                                                                           */
/* Description        : This routine is called to register the standard      */
/*                      bridge mib when bridge module is disabled            */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanL2IwfStartHwAgeTimer (UINT4 u4ContextId, UINT4 u4AgeOutInt)
{
    L2IwfStartHwAgeTimer (u4ContextId, u4AgeOutInt);
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfSetInterfaceType                            */
/*                                                                           */
/* Description        : This routine is called to Set the ComponentType         */
/*                      Row Statusin L2IWF for a ContextId                     */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanL2IwfSetInterfaceType (UINT4 u4ContextId, UINT1 u1InterfaceType)
{
    return (L2IwfSetInterfaceType (u4ContextId, u1InterfaceType));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetInterfaceType                            */
/*                                                                           */
/* Description        : This routine is called to Get the ComponentType         */
/*                      from L2IWF for a ContextId                             */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanL2IwfGetInterfaceType (UINT4 u4ContextId, UINT1 *pu1InterfaceType)
{
    return (L2IwfGetInterfaceType (u4ContextId, pu1InterfaceType));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfUpdatePortDeiBit                            */
/*                                                                           */
/* Description        : This routine is called to Update the Dei Bit value   */
/*                      in L2IWF for the Port.                               */
/*                                                                           */
/* Input(s)           :  u4IfIndex -Actual Port Number                       */
/*                       u1DeiBitValue -Current value of Dei Bit             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanL2IwfUpdatePortDeiBit (UINT4 u4IfIndex, UINT1 u1DeiBitValue)
{
    return (L2IwfUpdatePortDeiBit (u4IfIndex, u1DeiBitValue));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfSetAdminIntfTypeFlag                        */
/*                                                                           */
/* Description        : This routine is called to Set the Admin Interface    */
/*                      Type Flag in L2IWF                                   */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanL2IwfSetAdminIntfTypeFlag (UINT4 u4ContextId, UINT1 u1AdminIntfTypeFlag)
{
    return (L2IwfSetAdminIntfTypeFlag (u4ContextId, u1AdminIntfTypeFlag));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetAdminIntfTypeFlag                        */
/*                                                                           */
/* Description        : This routine is called to Get the Admin Interface     */
/*                      Type Flag in L2IWF ContextId                         */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanL2IwfGetAdminIntfTypeFlag (UINT4 u4ContextId, UINT1 *pu1AdminIntfTypeFlag)
{
    return (L2IwfGetAdminIntfTypeFlag (u4ContextId, pu1AdminIntfTypeFlag));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfChangeBridgeModeChange                      */
/*                                                                           */
/* Description        : This routine is called to trigger L2IWF regarding the*/
/*                      change of Bridge Mode                                */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanL2IwfChangeBridgeModeChange (UINT4 u4ContextId)
{
    return (L2IwfChangeBridgeModeChange (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetCnpPortCount                             */
/*                                                                           */
/* Description        : This routine to get the Count of CNP Ports           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanL2IwfGetCnpPortCount (UINT4 u4ContextId, UINT4 *pu4Count)
{
    return (L2IwfGetCnpPortCount (u4ContextId, pu4Count));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetAllToOneBndlStatus                       */
/*                                                                           */
/* Description        : This routine to get the All to one bundling status   */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                                                                           */
/* Output(s)          : pu1Status - All to one bundling status               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
VlanL2IwfGetAllToOneBndlStatus (UINT4 u4ContextId, UINT1 *pu1Status)
{
    return (L2IwfGetAllToOneBndlStatus (u4ContextId, pu1Status));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfIsBrgModeChangeAllowed                      */
/*                                                                           */
/* Description        : This routine is called from VLAN to get the          */
/*                      information from L2IWF if Bridge mode change is      */
/*                      allowed or not. This Call queries all the dependent  */
/*                      module APIs to know if Bridge Mode change is allowed */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                      u4BrgMode   - New Bridge Mode                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE/L2IWF_FALSE                               */
/*****************************************************************************/
INT1
VlanL2IwfIsBrgModeChangeAllowed (UINT4 u4ContextId, UINT4 u4BrgMode)
{
    return (L2IwfIsBridgeModeChangeAllowed (u4ContextId, u4BrgMode));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfConfigPvlanMapping                          */
/*                                                                           */
/* Description        : This routine is called from VLAN to configure the    */
/*                      pvlan mapping in the given context.                  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                      tVlanId     - Vlan Id as index                       */
/*                      u1PvlanType - Type of PVLAN, it can any of the       */
/*                                    following values.                      */
/*                                     - Normal                              */
/*                                     - Primary                             */
/*                                     - Isolated                            */
/*                                     - Community                           */
/*                      PrimaryVlanId     - PrimaryVlanId                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfConfigPvlanMapping (UINT4 u4ContextId, tVlanId VlanId,
                             UINT1 u1PvlanType, tVlanId PrimaryVlanId,
                             UINT1 u1ConfigType)
{
    return (L2IwfConfigPvlanMapping (u4ContextId, VlanId, u1PvlanType,
                                     PrimaryVlanId, u1ConfigType));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetPVlanMappingInfo                         */
/*                      This function gives the mapping between primary and  */
/*                      secondary vlan. This function will be invoked by     */
/*                      Spanning tree, GMRP, MMRP, IGS to know the type of   */
/*                      vlan and to get the primary to seconday vlan mapping */
/*                      and vice versa.                                      */
/*                                                                           */
/* Input(s)           : pL2PvlanMappingInfo - Pointer to the structure       */
/*                                            containing Private vlan mapping*/
/*                                            information. The following are */
/*                                            the fields in this structure   */
/*                                            that are input to this API.    */
/*                      u1RequestType - Type of request. It can take the     */
/*                                      following values:                    */
/*                                        1.L2IWF_VLAN_TYPE                  */
/*                                        2.L2IWF_MAPPED_VLANS               */
/*                                        3.L2IWF_NUM_MAPPED_VLANS           */
/*                                                                           */
/*                                      If L2IWF_VLAN_TYPE is passed, then   */
/*                                      pMappedVlans will not be filled, but */
/*                                      only value for pVlanType is filled.  */
/*                                                                           */
/*                                      If L2IWF_MAPPED_VLANS is passed,     */
/*                                      then both pVlanMapped Vlans and      */
/*                                      pVlanType are filled.                */
/*                                                                           */
/*                                      If L2IWF_NUM_MAPPED_VLANS is passed, */
/*                                      the the number of asscoiated vlans   */
/*                                      for the given vlan will be filled.   */
/*                      InVlanId - Id of the given Vlan.                     */
/*                                                                           */
/* Output(s)          : pL2PvlanMappingInfo - pointer to the structure       */
/*                                            containing Private vlan mapping*/
/*                                            information. The following are */
/*                                            the fields in this structure   */
/*                                            that are output to this API.   */
/*                                                                           */
/*                      pMappedVlans - If the InVlanId is a primary vlan,    */
/*                                     then this pointer points to an array  */
/*                                     containing the list of secondary vlans*/
/*                                     associated with that primary vlan.    */
/*                                                                           */
/*                                     If the InVlanId is a secondary vlan,  */
/*                                     this *pMappedVlans will contain the ID*/
/*                                     of primary vlan associated with the   */
/*                                     given secondary vlan (InVlanId).      */
/*                                                                           */
/*                                     If u1RequestTyps is                   */
/*                                     L2IWF_MAPPED_VLANS, then only this    */
/*                                     array will be filled. Otherwise this  */
/*                                     will not filled.                      */
/*                                                                           */
/*                      pu1VlanType - This gives the type of vlan of the     */
/*                                    given input vlan (InVlanId). Possible  */
/*                                    values for this parameter are:         */
/*                                      1.L2IWF_VLAN_TYPE_NORMAL,            */
/*                                      2.L2IWF_VLAN_TYPE_PRIMARY,           */
/*                                      3.L2IWF_VLAN_TYPE_ISOLATED,          */
/*                                      4.L2IWF_VLAN_TYPE_COMMUNITY          */
/*                                                                           */
/*                      pu2NumMappedVlans - Number of vlans present in the   */
/*                                          array pointed by pMappedVlans.   */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
VlanL2IwfGetPVlanMappingInfo (tL2PvlanMappingInfo * pL2PvlanMappingInfo)
{
    L2IwfGetPVlanMappingInfo (pL2PvlanMappingInfo);
}

/*****************************************************************************/
/* Function Name        : VlanL2IwfDeletePvlanMapping                        */
/*                                                                           */
/* Description          : This function called by vlan module to delete the  */
/*                        pvlan entry.                                       */
/*                                                                           */
/* Input (s)            : u4ContextId - Context ID value.                    */
/*                        VlanId -  Vlan Id Value to be delete from the pvlan*/
/*                                          domain.                          */
/*                                                                           */
/* Output               : None                                               */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS or L2IWF_FAILURE.                    */
/*****************************************************************************/
INT4
VlanL2IwfDeletePvlanMapping (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfDeletePvlanMapping (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : VlanL2IwfGetVlanPortType                             */
/*                                                                           */
/* Description        : This routine returns the port tunnel type            */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global Port Index of the port whose port */
/*                               type is to be obtained.                     */
/*                                                                           */
/* Output(s)          : pPortType   - Port Type VLAN_ACCESS_PORT/            */
/*                                              VLAN_HYBRID_PORT/            */
/*                                              VLAN_TRUNK_PORT/             */
/*                                              VLAN_PROMISCOUS_PORT/        */
/*                                              VLAN_HOST_PORT               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
VlanL2IwfGetVlanPortType (UINT4 u4IfIndex, UINT1 *pPortType)
{
    return (L2IwfGetVlanPortType (u4IfIndex, pPortType));
}
