/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnmiget.c,v 1.39.22.1 2018/03/15 13:00:06 siva Exp $
 *
 * Description: This file contains routines to get the VLAN objects.
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*                                                                           */
/*  FILE NAME             : vlanget.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains routines to get the VLAN      */
/*                          objects.                                         */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#include "vlaninc.h"
#include "vcm.h"

/****************************************************************************
 Function    :  nmhGetFsDot1qVlanVersionNumber
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                retValFsDot1qVlanVersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qVlanVersionNumber (INT4 i4FsDot1qVlanContextId,
                                INT4 *pi4RetValFsDot1qVlanVersionNumber)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qVlanVersionNumber (pi4RetValFsDot1qVlanVersionNumber);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qMaxVlanId
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                retValFsDot1qMaxVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qMaxVlanId (INT4 i4FsDot1qVlanContextId,
                        INT4 *pi4RetValFsDot1qMaxVlanId)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qMaxVlanId (pi4RetValFsDot1qMaxVlanId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qMaxSupportedVlans
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                retValFsDot1qMaxSupportedVlans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qMaxSupportedVlans (INT4 i4FsDot1qVlanContextId,
                                UINT4 *pu4RetValFsDot1qMaxSupportedVlans)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qMaxSupportedVlans (pu4RetValFsDot1qMaxSupportedVlans);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qNumVlans
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                retValFsDot1qNumVlans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qNumVlans (INT4 i4FsDot1qVlanContextId,
                       UINT4 *pu4RetValFsDot1qNumVlans)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qNumVlans (pu4RetValFsDot1qNumVlans);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qFdbDynamicCount
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId

                The Object 
                retValFsDot1qFdbDynamicCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qFdbDynamicCount (INT4 i4FsDot1qVlanContextId, UINT4 u4FsDot1qFdbId,
                              UINT4 *pu4RetValFsDot1qFdbDynamicCount)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFdbDynamicCount
        (u4FsDot1qFdbId, pu4RetValFsDot1qFdbDynamicCount);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpFdbPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qTpFdbAddress
                

                The Object 
                retValFsDot1qTpFdbPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpFdbPort (INT4 i4FsDot1qVlanContextId, UINT4 u4FsDot1qFdbId,
                        tMacAddr FsDot1qTpFdbAddress,
                        INT4 *pi4RetValFsDot1qTpFdbPort)
{
    INT1                i1RetVal;
    INT4                i4LocalPortNum;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qTpFdbPort (u4FsDot1qFdbId,
                                     FsDot1qTpFdbAddress, &i4LocalPortNum);

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4LocalPortNum != 0)
        {
            *pi4RetValFsDot1qTpFdbPort = VLAN_GET_IFINDEX (i4LocalPortNum);
        }
        else
        {
            *pi4RetValFsDot1qTpFdbPort = 0;
        }
    }

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpFdbStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qTpFdbAddress
                

                The Object 
                retValFsDot1qTpFdbStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpFdbStatus (INT4 i4FsDot1qVlanContextId, UINT4 u4FsDot1qFdbId,
                          tMacAddr FsDot1qTpFdbAddress,
                          INT4 *pi4RetValFsDot1qTpFdbStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qTpFdbStatus (u4FsDot1qFdbId,
                                       FsDot1qTpFdbAddress,
                                       pi4RetValFsDot1qTpFdbStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpFdbPw
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qTpFdbAddress

                The Object 
                retValFsDot1qTpFdbPw
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpFdbPw (INT4 i4FsDot1qVlanContextId, UINT4 u4FsDot1qFdbId,
                      tMacAddr FsDot1qTpFdbAddress,
                      UINT4 *pu4RetValFsDot1qTpFdbPw)
{
#ifdef MPLS_WANTED
    UINT1               u1Status;
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanGetVplsFdbInfo (u4FsDot1qFdbId, FsDot1qTpFdbAddress,
                            pu4RetValFsDot1qTpFdbPw, &u1Status) != VLAN_SUCCESS)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }
    VlanReleaseContext ();
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qFdbId);
    UNUSED_PARAM (FsDot1qTpFdbAddress);
    *pu4RetValFsDot1qTpFdbPw = 0;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpGroupIsLearnt
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpGroupAddress
                FsDot1qTpPort
                

                The Object 
                retValFsDot1qTpGroupIsLearnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpGroupIsLearnt (INT4 i4FsDot1qVlanContextId,
                              UINT4 u4FsDot1qVlanIndex,
                              tMacAddr FsDot1qTpGroupAddress,
                              INT4 i4FsDot1qTpPort,
                              INT4 *pi4RetValFsDot1qTpGroupIsLearnt)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    tVlanCurrEntry     *pCurrEntry;
    tVlanGroupEntry    *pGroupEntry;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry != NULL)
    {
        pGroupEntry = VlanGetGroupEntry (FsDot1qTpGroupAddress, pCurrEntry);

        if (pGroupEntry != NULL)
        {
            VLAN_IS_MEMBER_PORT (pGroupEntry->LearntPorts,
                                 u2LocalPortId, u1Result);

            if (u1Result == VLAN_TRUE)
            {
                *pi4RetValFsDot1qTpGroupIsLearnt = VLAN_SNMP_TRUE;
            }
            else
            {
                *pi4RetValFsDot1qTpGroupIsLearnt = VLAN_SNMP_FALSE;
            }
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qForwardAllIsLearnt
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                retValFsDot1qForwardAllIsLearnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qForwardAllIsLearnt (INT4 i4FsDot1qVlanContextId,
                                 UINT4 u4FsDot1qVlanIndex, INT4 i4FsDot1qTpPort,
                                 INT4 *pi4RetValFsDot1qForwardAllIsLearnt)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT4               u4ContextId;
    tVlanCurrEntry     *pCurrEntry;
    UINT2               u2LocalPortId;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry != NULL)
    {
        /* check whether the port is present in ForwardAll */
        VLAN_IS_MEMBER_PORT (pCurrEntry->AllGrps.Ports,
                             u2LocalPortId, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            /* check in the ForwardAll Static table */
            VLAN_IS_MEMBER_PORT (pCurrEntry->AllGrps.StaticPorts,
                                 u2LocalPortId, u1Result);
            if (u1Result == VLAN_FALSE)
            {
                /* check in VlanCurrEntry Egress ports */
                u1Result = VLAN_FALSE;
                VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2LocalPortId, u1Result);
                if (u1Result == VLAN_TRUE)
                {
                    *pi4RetValFsDot1qForwardAllIsLearnt = VLAN_SNMP_TRUE;
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
            }
        }
        *pi4RetValFsDot1qForwardAllIsLearnt = VLAN_SNMP_FALSE;
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    VlanReleaseContext ();
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4FsDot1qTpPort);
    UNUSED_PARAM (pi4RetValFsDot1qForwardAllIsLearnt);
#endif /* VLAN_EXTENDED_FILTER */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qForwardAllRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qForwardAllRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qForwardAllRowStatus (INT4 i4FsDot1qVlanContextId,
                                  UINT4 u4FsDot1qVlanIndex,
                                  INT4 *pi4RetValFsDot1qForwardAllRowStatus)
{
#ifdef VLAN_EXTENDED_FILTER
    tVlanCurrEntry     *pCurrEntry;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry != NULL)
    {
        *pi4RetValFsDot1qForwardAllRowStatus = pCurrEntry->AllGrps.u1RowStatus;
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    VlanReleaseContext ();
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (pi4RetValFsDot1qForwardAllRowStatus);
#endif /* VLAN_EXTENDED_FILTER */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qForwardAllPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                retValFsDot1qForwardAllPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qForwardAllPort (INT4 i4FsDot1qVlanContextId,
                             UINT4 u4FsDot1qVlanIndex, INT4 i4FsDot1qTpPort,
                             INT4 *pi4RetValFsDot1qForwardAllPort)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    tVlanCurrEntry     *pCurrEntry;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4FsDot1qVlanContextId != (INT4) u4ContextId)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry != NULL)
    {
        /* check whether the port is present in ForwardAll */
        VLAN_IS_MEMBER_PORT (pCurrEntry->AllGrps.Ports,
                             u2LocalPortId, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            /* check in VlanCurrEntry Egress ports */
            u1Result = VLAN_FALSE;
            VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2LocalPortId, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                /* 1 indicates member port */
                *pi4RetValFsDot1qForwardAllPort = VLAN_ADD_MEMBER_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
        else
        {
            VLAN_IS_MEMBER_PORT (pCurrEntry->AllGrps.ForbiddenPorts,
                                 u2LocalPortId, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                /* 2 indicates forbidden port */
                *pi4RetValFsDot1qForwardAllPort = VLAN_ADD_FORBIDDEN_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
        /* 0 indicates non-member port */
        *pi4RetValFsDot1qForwardAllPort = VLAN_NON_MEMBER_PORT;
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    VlanReleaseContext ();
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4FsDot1qTpPort);
    UNUSED_PARAM (pi4RetValFsDot1qForwardAllPort);
#endif /* VLAN_EXTENDED_FILTER */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qForwardUnregIsLearnt
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                retValFsDot1qForwardUnregIsLearnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qForwardUnregIsLearnt (INT4 i4FsDot1qVlanContextId,
                                   UINT4 u4FsDot1qVlanIndex,
                                   INT4 i4FsDot1qTpPort,
                                   INT4 *pi4RetValFsDot1qForwardUnregIsLearnt)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT4               u4ContextId;
    tVlanCurrEntry     *pCurrEntry;
    UINT2               u2LocalPortId;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4FsDot1qVlanContextId != (INT4) u4ContextId)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry != NULL)
    {
        /* check whether the port is present in ForwardAll */
        VLAN_IS_MEMBER_PORT (pCurrEntry->UnRegGrps.Ports,
                             u2LocalPortId, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            /* check in the ForwardAll Static table */
            u1Result = VLAN_FALSE;
            VLAN_IS_MEMBER_PORT (pCurrEntry->UnRegGrps.StaticPorts,
                                 u2LocalPortId, u1Result);
            if (u1Result == VLAN_FALSE)
            {
                /* check in VlanCurrEntry Egress ports */
                VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2LocalPortId, u1Result);
                if (u1Result == VLAN_TRUE)
                {
                    *pi4RetValFsDot1qForwardUnregIsLearnt = VLAN_SNMP_TRUE;
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
            }
        }
        *pi4RetValFsDot1qForwardUnregIsLearnt = VLAN_SNMP_FALSE;
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    VlanReleaseContext ();
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4FsDot1qTpPort);
    UNUSED_PARAM (pi4RetValFsDot1qForwardUnregIsLearnt);
#endif /* VLAN_EXTENDED_FILTER */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qForwardUnregRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qForwardUnregRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qForwardUnregRowStatus (INT4 i4FsDot1qVlanContextId,
                                    UINT4 u4FsDot1qVlanIndex,
                                    INT4 *pi4RetValFsDot1qForwardUnregRowStatus)
{
#ifdef VLAN_EXTENDED_FILTER
    tVlanCurrEntry     *pCurrEntry;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry != NULL)
    {
        /* check whether the port is present in ForwardAll */
        *pi4RetValFsDot1qForwardUnregRowStatus =
            pCurrEntry->UnRegGrps.u1RowStatus;
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    VlanReleaseContext ();
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (pi4RetValFsDot1qForwardUnregRowStatus);
#endif /* VLAN_EXTENDED_FILTER */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qForwardUnregPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                retValFsDot1qForwardUnregPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qForwardUnregPort (INT4 i4FsDot1qVlanContextId,
                               UINT4 u4FsDot1qVlanIndex, INT4 i4FsDot1qTpPort,
                               INT4 *pi4RetValFsDot1qForwardUnregPort)
{
#ifdef VLAN_EXTENDED_FILTER
    UINT4               u4ContextId;
    tVlanCurrEntry     *pCurrEntry;
    UINT2               u2LocalPortId;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4FsDot1qVlanContextId != (INT4) u4ContextId)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry != NULL)
    {
        /* check whether the port is present in ForwardAll */
        VLAN_IS_MEMBER_PORT (pCurrEntry->UnRegGrps.Ports,
                             u2LocalPortId, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            /* check in VlanCurrEntry Egress ports */
            u1Result = VLAN_FALSE;
            VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2LocalPortId, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                /* 1 indicates member port */
                *pi4RetValFsDot1qForwardUnregPort = VLAN_ADD_MEMBER_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
        else
        {
            VLAN_IS_MEMBER_PORT (pCurrEntry->UnRegGrps.ForbiddenPorts,
                                 u2LocalPortId, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                /* 2 indicates forbidden port */
                *pi4RetValFsDot1qForwardUnregPort = VLAN_ADD_FORBIDDEN_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
        /* 0 indicates non-member port */
        *pi4RetValFsDot1qForwardUnregPort = VLAN_NON_MEMBER_PORT;
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    VlanReleaseContext ();
#else
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    UNUSED_PARAM (i4FsDot1qTpPort);
    UNUSED_PARAM (pi4RetValFsDot1qForwardUnregPort);
#endif /* VLAN_EXTENDED_FILTER */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qStaticUnicastRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                

                The Object 
                retValFsDot1qStaticUnicastRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qStaticUnicastRowStatus (INT4 i4FsDot1qVlanContextId,
                                     UINT4 u4FsDot1qFdbId,
                                     tMacAddr FsDot1qStaticUnicastAddress,
                                     INT4 i4FsDot1qStaticUnicastReceivePort,
                                     INT4
                                     *pi4RetValFsDot1qStaticUnicastRowStatus)
{
    tVlanStUcastEntry  *pVlanStUcastEntry;
    UINT4               u4FidIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u2LocalPortId = 0;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FsDot1qFdbId);

    if (u4FidIndex != VLAN_INVALID_FID_INDEX)
    {
        pVlanStUcastEntry = VlanGetStaticUcastEntryWithExactRcvPort (u4FidIndex,
                                                                     FsDot1qStaticUnicastAddress,
                                                                     u2LocalPortId);
        if (pVlanStUcastEntry != NULL)
        {
            *pi4RetValFsDot1qStaticUnicastRowStatus =
                (INT4) pVlanStUcastEntry->u1RowStatus;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qStaticUnicastStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                

                The Object 
                retValFsDot1qStaticUnicastStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qStaticUnicastStatus (INT4 i4FsDot1qVlanContextId,
                                  UINT4 u4FsDot1qFdbId,
                                  tMacAddr FsDot1qStaticUnicastAddress,
                                  INT4 i4FsDot1qStaticUnicastReceivePort,
                                  INT4 *pi4RetValFsDot1qStaticUnicastStatus)
{
    tVlanTempPortList  *pTmpPortList = NULL;
    tVlanStUcastEntry  *pVlanStUcastEntry = NULL;
    UINT4               u4FidIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId = 0;

    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FsDot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pVlanStUcastEntry = VlanGetStaticUcastEntryWithExactRcvPort
        (u4FidIndex, FsDot1qStaticUnicastAddress, u2LocalPortId);
    if (pVlanStUcastEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pVlanStUcastEntry->u1RowStatus == VLAN_ACTIVE)
    {
        *pi4RetValFsDot1qStaticUnicastStatus = pVlanStUcastEntry->u1Status;
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    else
    {
        VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId == (UINT4) i4FsDot1qVlanContextId) &&
                (pTmpPortList->u1Type == VLAN_ST_UCAST_TABLE) &&
                (pTmpPortList->PortListTbl.StUcastTbl.u4FdbId ==
                 u4FsDot1qFdbId) &&
                (pTmpPortList->PortListTbl.StUcastTbl.u2RcvPort ==
                 u2LocalPortId) &&
                (VLAN_ARE_MAC_ADDR_EQUAL
                 (pTmpPortList->PortListTbl.StUcastTbl.MacAddr,
                  FsDot1qStaticUnicastAddress)))
            {
                *pi4RetValFsDot1qStaticUnicastStatus =
                    pTmpPortList->PortListTbl.StUcastTbl.u1Status;
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
    }

    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qStaticAllowedIsMember
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort
                FsDot1qTpPort
                

                The Object 
                retValFsDot1qStaticAllowedIsMember
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qStaticAllowedIsMember (INT4 i4FsDot1qVlanContextId,
                                    UINT4 u4FsDot1qFdbId,
                                    tMacAddr FsDot1qStaticUnicastAddress,
                                    INT4 i4FsDot1qStaticUnicastReceivePort,
                                    INT4 i4FsDot1qTpPort,
                                    INT4 *pi4RetValFsDot1qStaticAllowedIsMember)
{
    tVlanTempPortList  *pTmpPortList = NULL;
    tVlanStUcastEntry  *pVlanStUcastEntry = NULL;
    UINT4               u4ContextId;
    UINT4               u4RcvPortContextId;
    UINT4               u4FidIndex;
    UINT2               u2LocalPortId;
    UINT2               u2RcvPortLocalPortId;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4RcvPortContextId, &u2RcvPortLocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4RcvPortContextId = i4FsDot1qVlanContextId;
        u2RcvPortLocalPortId = 0;
    }
    if ((u4ContextId != (UINT4) i4FsDot1qVlanContextId) ||
        (u4RcvPortContextId != (UINT4) i4FsDot1qVlanContextId))
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FsDot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pVlanStUcastEntry = VlanGetStaticUcastEntryWithExactRcvPort
        (u4FidIndex, FsDot1qStaticUnicastAddress, u2RcvPortLocalPortId);
    if (pVlanStUcastEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pVlanStUcastEntry->u1RowStatus == VLAN_ACTIVE)
    {
        VLAN_IS_MEMBER_PORT (pVlanStUcastEntry->AllowedToGo,
                             u2LocalPortId, u1Result);
    }
    else
    {
        VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId == (UINT4) i4FsDot1qVlanContextId) &&
                (pTmpPortList->u1Type == VLAN_ST_UCAST_TABLE) &&
                (pTmpPortList->PortListTbl.StUcastTbl.u4FdbId ==
                 u4FsDot1qFdbId) &&
                (pTmpPortList->PortListTbl.StUcastTbl.u2RcvPort ==
                 u2RcvPortLocalPortId) &&
                (VLAN_ARE_MAC_ADDR_EQUAL
                 (pTmpPortList->PortListTbl.StUcastTbl.MacAddr,
                  FsDot1qStaticUnicastAddress)))
            {
                break;
            }
        }

        if (pTmpPortList == NULL)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
        VLAN_IS_MEMBER_PORT (pTmpPortList->PortListTbl.StUcastTbl.AllowedToGo,
                             u2LocalPortId, u1Result);
    }

    u1Result = (u1Result == VLAN_TRUE) ? VLAN_SNMP_TRUE : VLAN_SNMP_FALSE;
    *pi4RetValFsDot1qStaticAllowedIsMember = u1Result;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qStaticMulticastRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                

                The Object 
                retValFsDot1qStaticMulticastRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qStaticMulticastRowStatus (INT4 i4FsDot1qVlanContextId,
                                       UINT4 u4FsDot1qVlanIndex,
                                       tMacAddr FsDot1qStaticMulticastAddress,
                                       INT4 i4FsDot1qStaticMulticastReceivePort,
                                       INT4
                                       *pi4RetValFsDot1qStaticMulticastRowStatus)
{
    tVlanCurrEntry     *pCurrEntry;
    tVlanStMcastEntry  *pStMcastEntry;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (i4FsDot1qStaticMulticastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticMulticastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u2LocalPortId = 0;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry != NULL)
    {

        pStMcastEntry = VlanGetStMcastEntryWithExactRcvPort
            (FsDot1qStaticMulticastAddress, u2LocalPortId, pCurrEntry);

        if (pStMcastEntry != NULL)
        {
            *pi4RetValFsDot1qStaticMulticastRowStatus =
                (INT4) pStMcastEntry->u1RowStatus;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qStaticMulticastStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                

                The Object 
                retValFsDot1qStaticMulticastStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qStaticMulticastStatus (INT4 i4FsDot1qVlanContextId,
                                    UINT4 u4FsDot1qVlanIndex,
                                    tMacAddr FsDot1qStaticMulticastAddress,
                                    INT4 i4FsDot1qStaticMulticastReceivePort,
                                    INT4 *pi4RetValFsDot1qStaticMulticastStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (i4FsDot1qStaticMulticastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticMulticastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u2LocalPortId = 0;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qStaticMulticastStatus (u4FsDot1qVlanIndex,
                                                 FsDot1qStaticMulticastAddress,
                                                 u2LocalPortId,
                                                 pi4RetValFsDot1qStaticMulticastStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qStaticMcastPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qStaticMulticastAddress
                FsDot1qStaticMulticastReceivePort
                FsDot1qTpPort
                

                The Object 
                retValFsDot1qStaticMcastPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qStaticMcastPort (INT4 i4FsDot1qVlanContextId,
                              UINT4 u4FsDot1qVlanIndex,
                              tMacAddr FsDot1qStaticMulticastAddress,
                              INT4 i4FsDot1qStaticMulticastReceivePort,
                              INT4 i4FsDot1qTpPort,
                              INT4 *pi4RetValFsDot1qStaticMcastPort)
{
    UINT4               u4ContextId;
    UINT4               u4RcvPortContextId;
    tVlanCurrEntry     *pCurrEntry;
    tVlanStMcastEntry  *pStMcastEntry;
    UINT2               u2LocalPortId;
    UINT2               u2RcvPortLocalPortId;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4FsDot1qStaticMulticastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticMulticastReceivePort,
             &u4RcvPortContextId, &u2RcvPortLocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u4RcvPortContextId = (UINT4) i4FsDot1qVlanContextId;
        u2RcvPortLocalPortId = 0;
    }

    if ((u4ContextId != (UINT4) i4FsDot1qVlanContextId) ||
        (u4RcvPortContextId != (UINT4) i4FsDot1qVlanContextId))
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry != NULL)
    {

        pStMcastEntry = VlanGetStMcastEntryWithExactRcvPort
            (FsDot1qStaticMulticastAddress, u2RcvPortLocalPortId, pCurrEntry);

        if (pStMcastEntry != NULL)
        {
            VLAN_IS_MEMBER_PORT (pStMcastEntry->EgressPorts,
                                 u2LocalPortId, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                /* 1 indicates static egress port */
                *pi4RetValFsDot1qStaticMcastPort = VLAN_ADD_MEMBER_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
            else
            {
                VLAN_IS_MEMBER_PORT (pStMcastEntry->ForbiddenPorts,
                                     u2LocalPortId, u1Result);
                if (u1Result == VLAN_TRUE)
                {
                    /* 2 indicates forbidden port */
                    *pi4RetValFsDot1qStaticMcastPort = VLAN_ADD_FORBIDDEN_PORT;
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
            }
            /* 0 indicates non-member port */
            *pi4RetValFsDot1qStaticMcastPort = VLAN_NON_MEMBER_PORT;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
    }
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qVlanNumDeletes
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                retValFsDot1qVlanNumDeletes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qVlanNumDeletes (INT4 i4FsDot1qVlanContextId,
                             UINT4 *pu4RetValFsDot1qVlanNumDeletes)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qVlanNumDeletes (pu4RetValFsDot1qVlanNumDeletes);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qVlanFdbId
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanTimeMark
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qVlanFdbId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qVlanFdbId (INT4 i4FsDot1qVlanContextId,
                        UINT4 u4FsDot1qVlanTimeMark, UINT4 u4FsDot1qVlanIndex,
                        UINT4 *pu4RetValFsDot1qVlanFdbId)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qVlanFdbId (u4FsDot1qVlanTimeMark,
                                     u4FsDot1qVlanIndex,
                                     pu4RetValFsDot1qVlanFdbId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qVlanStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanTimeMark
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qVlanStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qVlanStatus (INT4 i4FsDot1qVlanContextId,
                         UINT4 u4FsDot1qVlanTimeMark, UINT4 u4FsDot1qVlanIndex,
                         INT4 *pi4RetValFsDot1qVlanStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qVlanStatus (u4FsDot1qVlanTimeMark,
                                      u4FsDot1qVlanIndex,
                                      pi4RetValFsDot1qVlanStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qVlanCreationTime
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanTimeMark
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qVlanCreationTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qVlanCreationTime (INT4 i4FsDot1qVlanContextId,
                               UINT4 u4FsDot1qVlanTimeMark,
                               UINT4 u4FsDot1qVlanIndex,
                               UINT4 *pu4RetValFsDot1qVlanCreationTime)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qVlanCreationTime (u4FsDot1qVlanTimeMark,
                                            u4FsDot1qVlanIndex,
                                            pu4RetValFsDot1qVlanCreationTime);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qVlanCurrentEgressPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanTimeMark
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                retValFsDot1qVlanCurrentEgressPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qVlanCurrentEgressPort (INT4 i4FsDot1qVlanContextId,
                                    UINT4 u4FsDot1qVlanTimeMark,
                                    UINT4 u4FsDot1qVlanIndex,
                                    INT4 i4FsDot1qTpPort,
                                    INT4 *pi4RetValFsDot1qVlanCurrentEgressPort)
{
    UINT4               u4ContextId;
    tVlanCurrEntry     *pCurrEntry;
    UINT2               u2LocalPortId;
    UINT1               u1Result = VLAN_FALSE;

    UNUSED_PARAM (u4FsDot1qVlanTimeMark);

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pCurrEntry = VlanGetVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pCurrEntry != NULL)
    {
        VLAN_IS_CURR_EGRESS_PORT (pCurrEntry, u2LocalPortId, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            if (pCurrEntry->pStVlanEntry != NULL)
            {
                VLAN_IS_UNTAGGED_PORT (pCurrEntry->pStVlanEntry,
                                       u2LocalPortId, u1Result);
                if (u1Result == VLAN_TRUE)
                {
                    /* untagged port */
                    *pi4RetValFsDot1qVlanCurrentEgressPort =
                        VLAN_ADD_UNTAGGED_PORT;
                    VlanReleaseContext ();
                    return SNMP_SUCCESS;
                }
            }
            /* tagged Port */
            *pi4RetValFsDot1qVlanCurrentEgressPort = VLAN_ADD_TAGGED_PORT;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        /* non-member port */
        *pi4RetValFsDot1qVlanCurrentEgressPort = VLAN_NON_MEMBER_PORT;
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qVlanStaticName
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qVlanStaticName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qVlanStaticName (INT4 i4FsDot1qVlanContextId,
                             UINT4 u4FsDot1qVlanIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsDot1qVlanStaticName)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qVlanStaticName
        (u4FsDot1qVlanIndex, pRetValFsDot1qVlanStaticName);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qVlanStaticRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qVlanStaticRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qVlanStaticRowStatus (INT4 i4FsDot1qVlanContextId,
                                  UINT4 u4FsDot1qVlanIndex,
                                  INT4 *pi4RetValFsDot1qVlanStaticRowStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qVlanStaticRowStatus
        (u4FsDot1qVlanIndex, pi4RetValFsDot1qVlanStaticRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qVlanStaticPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object 
                retValFsDot1qVlanStaticPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qVlanStaticPort (INT4 i4FsDot1qVlanContextId,
                             UINT4 u4FsDot1qVlanIndex, INT4 i4FsDot1qTpPort,
                             INT4 *pi4RetValFsDot1qVlanStaticPort)
{
    UINT4               u4ContextId;
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT2               u2LocalPortId;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4FsDot1qVlanIndex);

    if (pStVlanEntry != NULL)
    {
        VLAN_IS_EGRESS_PORT (pStVlanEntry, u2LocalPortId, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            VLAN_IS_UNTAGGED_PORT (pStVlanEntry, u2LocalPortId, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                /* untagged port */
                *pi4RetValFsDot1qVlanStaticPort = VLAN_ADD_UNTAGGED_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
            /* tagged port */
            *pi4RetValFsDot1qVlanStaticPort = VLAN_ADD_TAGGED_PORT;
            VlanReleaseContext ();
            return SNMP_SUCCESS;
        }
        else
        {
            VLAN_IS_FORBIDDEN_PORT (pStVlanEntry, u2LocalPortId, u1Result);
            if (u1Result == VLAN_TRUE)
            {
                /* forbidden port */
                *pi4RetValFsDot1qVlanStaticPort = VLAN_ADD_ST_FORBIDDEN_PORT;
                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
        /* non-member port */
        *pi4RetValFsDot1qVlanStaticPort = VLAN_NON_MEMBER_PORT;
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qNextFreeLocalVlanIndex
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                retValFsDot1qNextFreeLocalVlanIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qNextFreeLocalVlanIndex (INT4 i4FsDot1qVlanContextId,
                                     INT4
                                     *pi4RetValFsDot1qNextFreeLocalVlanIndex)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qNextFreeLocalVlanIndex
        (pi4RetValFsDot1qNextFreeLocalVlanIndex);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qPvid
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1qPvid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qPvid (INT4 i4FsDot1dBasePort, UINT4 *pu4RetValFsDot1qPvid)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qPvid ((INT4) u2LocalPortId, pu4RetValFsDot1qPvid);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qPortAcceptableFrameTypes
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1qPortAcceptableFrameTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qPortAcceptableFrameTypes (INT4 i4FsDot1dBasePort,
                                       INT4
                                       *pi4RetValFsDot1qPortAcceptableFrameTypes)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qPortAcceptableFrameTypes ((INT4) u2LocalPortId,
                                                    pi4RetValFsDot1qPortAcceptableFrameTypes);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qPortIngressFiltering
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1qPortIngressFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qPortIngressFiltering (INT4 i4FsDot1dBasePort,
                                   INT4 *pi4RetValFsDot1qPortIngressFiltering)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qPortIngressFiltering ((INT4) u2LocalPortId,
                                                pi4RetValFsDot1qPortIngressFiltering);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpVlanPortInFrames
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qTpVlanPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpVlanPortInFrames (INT4 i4FsDot1dBasePort,
                                 UINT4 u4FsDot1qVlanIndex,
                                 UINT4 *pu4RetValFsDot1qTpVlanPortInFrames)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qTpVlanPortInFrames
        ((INT4) u2LocalPortId, u4FsDot1qVlanIndex,
         pu4RetValFsDot1qTpVlanPortInFrames);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpVlanPortOutFrames
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qTpVlanPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpVlanPortOutFrames (INT4 i4FsDot1dBasePort,
                                  UINT4 u4FsDot1qVlanIndex,
                                  UINT4 *pu4RetValFsDot1qTpVlanPortOutFrames)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qTpVlanPortOutFrames
        ((INT4) u2LocalPortId, u4FsDot1qVlanIndex,
         pu4RetValFsDot1qTpVlanPortOutFrames);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpVlanPortInDiscards
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qTpVlanPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpVlanPortInDiscards (INT4 i4FsDot1dBasePort,
                                   UINT4 u4FsDot1qVlanIndex,
                                   UINT4 *pu4RetValFsDot1qTpVlanPortInDiscards)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qTpVlanPortInDiscards
        ((INT4) u2LocalPortId, u4FsDot1qVlanIndex,
         pu4RetValFsDot1qTpVlanPortInDiscards);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpVlanPortInOverflowFrames
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qTpVlanPortInOverflowFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpVlanPortInOverflowFrames (INT4 i4FsDot1dBasePort,
                                         UINT4 u4FsDot1qVlanIndex,
                                         UINT4
                                         *pu4RetValFsDot1qTpVlanPortInOverflowFrames)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qTpVlanPortInOverflowFrames
        ((INT4) u2LocalPortId, u4FsDot1qVlanIndex,
         pu4RetValFsDot1qTpVlanPortInOverflowFrames);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpVlanPortOutOverflowFrames
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qTpVlanPortOutOverflowFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpVlanPortOutOverflowFrames (INT4 i4FsDot1dBasePort,
                                          UINT4 u4FsDot1qVlanIndex,
                                          UINT4
                                          *pu4RetValFsDot1qTpVlanPortOutOverflowFrames)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qTpVlanPortOutOverflowFrames
        ((INT4) u2LocalPortId, u4FsDot1qVlanIndex,
         pu4RetValFsDot1qTpVlanPortOutOverflowFrames);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpVlanPortInOverflowDiscards
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qTpVlanPortInOverflowDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpVlanPortInOverflowDiscards (INT4 i4FsDot1dBasePort,
                                           UINT4 u4FsDot1qVlanIndex,
                                           UINT4
                                           *pu4RetValFsDot1qTpVlanPortInOverflowDiscards)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qTpVlanPortInOverflowDiscards
        ((INT4) u2LocalPortId, u4FsDot1qVlanIndex,
         pu4RetValFsDot1qTpVlanPortInOverflowDiscards);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpVlanPortHCInFrames
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qTpVlanPortHCInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpVlanPortHCInFrames (INT4 i4FsDot1dBasePort,
                                   UINT4 u4FsDot1qVlanIndex,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValFsDot1qTpVlanPortHCInFrames)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qTpVlanPortHCInFrames
        ((INT4) u2LocalPortId, u4FsDot1qVlanIndex,
         pu8RetValFsDot1qTpVlanPortHCInFrames);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpVlanPortHCOutFrames
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qTpVlanPortHCOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpVlanPortHCOutFrames (INT4 i4FsDot1dBasePort,
                                    UINT4 u4FsDot1qVlanIndex,
                                    tSNMP_COUNTER64_TYPE *
                                    pu8RetValFsDot1qTpVlanPortHCOutFrames)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qTpVlanPortHCOutFrames
        ((INT4) u2LocalPortId, u4FsDot1qVlanIndex,
         pu8RetValFsDot1qTpVlanPortHCOutFrames);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qTpVlanPortHCInDiscards
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1qVlanIndex

                The Object 
                retValFsDot1qTpVlanPortHCInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qTpVlanPortHCInDiscards (INT4 i4FsDot1dBasePort,
                                     UINT4 u4FsDot1qVlanIndex,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsDot1qTpVlanPortHCInDiscards)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qTpVlanPortHCInDiscards
        ((INT4) u2LocalPortId, u4FsDot1qVlanIndex,
         pu8RetValFsDot1qTpVlanPortHCInDiscards);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qConstraintType
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qConstraintVlan
                FsDot1qConstraintSet

                The Object 
                retValFsDot1qConstraintType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qConstraintType (INT4 i4FsDot1qVlanContextId,
                             UINT4 u4FsDot1qConstraintVlan,
                             INT4 i4FsDot1qConstraintSet,
                             INT4 *pi4RetValFsDot1qConstraintType)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qConstraintType (u4FsDot1qConstraintVlan,
                                          i4FsDot1qConstraintSet,
                                          pi4RetValFsDot1qConstraintType);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qConstraintStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qConstraintVlan
                FsDot1qConstraintSet

                The Object 
                retValFsDot1qConstraintStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qConstraintStatus (INT4 i4FsDot1qVlanContextId,
                               UINT4 u4FsDot1qConstraintVlan,
                               INT4 i4FsDot1qConstraintSet,
                               INT4 *pi4RetValFsDot1qConstraintStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qConstraintStatus (u4FsDot1qConstraintVlan,
                                            i4FsDot1qConstraintSet,
                                            pi4RetValFsDot1qConstraintStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qConstraintSetDefault
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                retValFsDot1qConstraintSetDefault
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qConstraintSetDefault (INT4 i4FsDot1qVlanContextId,
                                   INT4 *pi4RetValFsDot1qConstraintSetDefault)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qConstraintSetDefault
        (pi4RetValFsDot1qConstraintSetDefault);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qConstraintTypeDefault
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                retValFsDot1qConstraintTypeDefault
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qConstraintTypeDefault (INT4 i4FsDot1qVlanContextId,
                                    INT4 *pi4RetValFsDot1qConstraintTypeDefault)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qConstraintTypeDefault
        (pi4RetValFsDot1qConstraintTypeDefault);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1vProtocolGroupId
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1vProtocolTemplateFrameType
                FsDot1vProtocolTemplateProtocolValue

                The Object 
                retValFsDot1vProtocolGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1vProtocolGroupId (INT4 i4FsDot1qVlanContextId,
                              INT4 i4FsDot1vProtocolTemplateFrameType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsDot1vProtocolTemplateProtocolValue,
                              INT4 *pi4RetValFsDot1vProtocolGroupId)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1vProtocolGroupId (i4FsDot1vProtocolTemplateFrameType,
                                           pFsDot1vProtocolTemplateProtocolValue,
                                           pi4RetValFsDot1vProtocolGroupId);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1vProtocolGroupRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1vProtocolTemplateFrameType
                FsDot1vProtocolTemplateProtocolValue

                The Object 
                retValFsDot1vProtocolGroupRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1vProtocolGroupRowStatus (INT4 i4FsDot1qVlanContextId,
                                     INT4 i4FsDot1vProtocolTemplateFrameType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsDot1vProtocolTemplateProtocolValue,
                                     INT4
                                     *pi4RetValFsDot1vProtocolGroupRowStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1vProtocolGroupRowStatus
        (i4FsDot1vProtocolTemplateFrameType,
         pFsDot1vProtocolTemplateProtocolValue,
         pi4RetValFsDot1vProtocolGroupRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1vProtocolPortGroupVid
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1vProtocolPortGroupId

                The Object 
                retValFsDot1vProtocolPortGroupVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1vProtocolPortGroupVid (INT4 i4FsDot1dBasePort,
                                   INT4 i4FsDot1vProtocolPortGroupId,
                                   INT4 *pi4RetValFsDot1vProtocolPortGroupVid)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1vProtocolPortGroupVid
        ((INT4) u2LocalPortId, i4FsDot1vProtocolPortGroupId,
         pi4RetValFsDot1vProtocolPortGroupVid);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1vProtocolPortRowStatus
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1vProtocolPortGroupId

                The Object 
                retValFsDot1vProtocolPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1vProtocolPortRowStatus (INT4 i4FsDot1dBasePort,
                                    INT4 i4FsDot1vProtocolPortGroupId,
                                    INT4 *pi4RetValFsDot1vProtocolPortRowStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1vProtocolPortRowStatus
        ((INT4) u2LocalPortId, i4FsDot1vProtocolPortGroupId,
         pi4RetValFsDot1vProtocolPortRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/************************** Proprietary MIB Get Routines ********************/

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanGlobalTrace
 Input       :  The Indices

                The Object 
                retValFsMIDot1qFutureVlanGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanGlobalTrace (INT4
                                      *pi4RetValFsMIDot1qFutureVlanGlobalTrace)
{
#ifdef TRACE_WANTED
    *pi4RetValFsMIDot1qFutureVlanGlobalTrace = gu4VlanGlobalTrace;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsMIDot1qFutureVlanGlobalTrace);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureVlanStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                 INT4 *pi4RetValFsMIDot1qFutureVlanStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanStatus (pi4RetValFsMIDot1qFutureVlanStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanMacBasedOnAllPorts
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureVlanMacBasedOnAllPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanMacBasedOnAllPorts (INT4
                                             i4FsMIDot1qFutureVlanContextId,
                                             INT4
                                             *pi4RetValFsMIDot1qFutureVlanMacBasedOnAllPorts)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanMacBasedOnAllPorts
        (pi4RetValFsMIDot1qFutureVlanMacBasedOnAllPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureBaseBridgeMode
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureBaseBridgeMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureBaseBridgeMode (INT4 i4FsMIDot1qFutureVlanContextId,
                                     INT4
                                     *pi4RetValFsMIDot1qFutureBaseBridgeMode)
{
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = (nmhGetDot1qFutureVlanBaseBridgeMode
                (pi4RetValFsMIDot1qFutureBaseBridgeMode));
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanSubnetBasedOnAllPorts
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                retValFsMIDot1qFutureVlanSubnetBasedOnAllPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanSubnetBasedOnAllPorts (INT4
                                                i4FsMIDot1qFutureVlanContextId,
                                                INT4
                                                *pi4RetValFsMIDot1qFutureVlanSubnetBasedOnAllPorts)
{
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanSubnetBasedOnAllPorts
        (pi4RetValFsMIDot1qFutureVlanSubnetBasedOnAllPorts);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanGlobalMacLearningStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                retValFsMIDot1qFutureVlanGlobalMacLearningStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanGlobalMacLearningStatus (INT4
                                                  i4FsMIDot1qFutureVlanContextId,
                                                  INT4
                                                  *pi4RetValFsMIDot1qFutureVlanGlobalMacLearningStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanGlobalMacLearningStatus
        (pi4RetValFsMIDot1qFutureVlanGlobalMacLearningStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria (INT4
                                                         i4FsMIDot1qFutureVlanContextId,
                                                         INT4
                                                         *pi4RetValFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanApplyEnhancedFilteringCriteria
        (pi4RetValFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortProtoBasedOnAllPorts
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureVlanPortProtoBasedOnAllPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortProtoBasedOnAllPorts (INT4
                                                   i4FsMIDot1qFutureVlanContextId,
                                                   INT4
                                                   *pi4RetValFsMIDot1qFutureVlanPortProtoBasedOnAllPorts)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortProtoBasedOnAllPorts
        (pi4RetValFsMIDot1qFutureVlanPortProtoBasedOnAllPorts);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanShutdownStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureVlanShutdownStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanShutdownStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                         INT4
                                         *pi4RetValFsMIDot1qFutureVlanShutdownStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanShutdownStatus
        (pi4RetValFsMIDot1qFutureVlanShutdownStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanDebug
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureVlanDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanDebug (INT4 i4FsMIDot1qFutureVlanContextId,
                                INT4 *pi4RetValFsMIDot1qFutureVlanDebug)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanDebug (pi4RetValFsMIDot1qFutureVlanDebug);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanLearningMode
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureVlanLearningMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanLearningMode (INT4 i4FsMIDot1qFutureVlanContextId,
                                       INT4
                                       *pi4RetValFsMIDot1qFutureVlanLearningMode)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanLearningMode
        (pi4RetValFsMIDot1qFutureVlanLearningMode);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanHybridTypeDefault
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureVlanHybridTypeDefault
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanHybridTypeDefault (INT4 i4FsMIDot1qFutureVlanContextId,
                                            INT4
                                            *pi4RetValFsMIDot1qFutureVlanHybridTypeDefault)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanHybridTypeDefault
        (pi4RetValFsMIDot1qFutureVlanHybridTypeDefault);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanOperStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureVlanOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanOperStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                     INT4
                                     *pi4RetValFsMIDot1qFutureVlanOperStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanOperStatus
        (pi4RetValFsMIDot1qFutureVlanOperStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 *  Function    :  nmhGetFsMIDot1qFutureVlanContextName
 *  Input       :  The Indices
 *                 FsMIDot1qFutureVlanContextId
 *
 *                 The Object
 *                 retValFsMIDot1qFutureVlanContextName
 *                 
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *   
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanContextName (INT4 i4FsMIDot1qFutureVlanContextId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMIDot1qFutureVlanContextName)
{
    UINT1               au1TempContextName[VLAN_SWITCH_ALIAS_LEN];

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanVcmGetAliasName (i4FsMIDot1qFutureVlanContextId, au1TempContextName)
        == VCM_SUCCESS)
    {
        MEMCPY (pRetValFsMIDot1qFutureVlanContextName->pu1_OctetList,
                au1TempContextName, STRLEN (au1TempContextName));
        pRetValFsMIDot1qFutureVlanContextName->i4_Length =
            STRLEN (au1TempContextName);
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    VlanReleaseContext ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanPortType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortType (INT4 i4FsMIDot1qFutureVlanPort,
                                   INT4 *pi4RetValFsMIDot1qFutureVlanPortType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortType
        ((INT4) u2LocalPortId, pi4RetValFsMIDot1qFutureVlanPortType);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortMacBasedClassification
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanPortMacBasedClassification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortMacBasedClassification (INT4
                                                     i4FsMIDot1qFutureVlanPort,
                                                     INT4
                                                     *pi4RetValFsMIDot1qFutureVlanPortMacBasedClassification)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortMacBasedClassification
        ((INT4) u2LocalPortId,
         pi4RetValFsMIDot1qFutureVlanPortMacBasedClassification);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortPortProtoBasedClassification
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanPortPortProtoBasedClassification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortPortProtoBasedClassification (INT4
                                                           i4FsMIDot1qFutureVlanPort,
                                                           INT4
                                                           *pi4RetValFsMIDot1qFutureVlanPortPortProtoBasedClassification)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortPortProtoBasedClassification
        ((INT4) u2LocalPortId,
         pi4RetValFsMIDot1qFutureVlanPortPortProtoBasedClassification);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanFilteringUtilityCriteria
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanFilteringUtilityCriteria
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanFilteringUtilityCriteria
    (INT4 i4FsMIDot1qFutureVlanPort,
     INT4 *pi4RetValFsMIDot1qFutureVlanFilteringUtilityCriteria)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanFilteringUtilityCriteria
        (u2LocalPortId, pi4RetValFsMIDot1qFutureVlanFilteringUtilityCriteria);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortMacMapVid
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanMacMapAddr
                

                The Object 
                retValFsMIDot1qFutureVlanMacMapVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortMacMapVid (INT4 i4FsMIDot1qFutureVlanPort,
                                        tMacAddr
                                        FsMIDot1qFutureVlanPortMacMapAddr,
                                        INT4
                                        *pi4RetValFsMIDot1qFutureVlanPortMacMapVid)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortMacMapVid ((INT4) u2LocalPortId,
                                                   FsMIDot1qFutureVlanPortMacMapAddr,
                                                   pi4RetValFsMIDot1qFutureVlanPortMacMapVid);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortMacMapName
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr

                The Object 
                retValFsMIDot1qFutureVlanPortMacMapName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortMacMapName (INT4 i4FsMIDot1qFutureVlanPort,
                                         tMacAddr
                                         FsMIDot1qFutureVlanPortMacMapAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValFsMIDot1qFutureVlanPortMacMapName)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortMacMapName ((INT4) u2LocalPortId,
                                                    FsMIDot1qFutureVlanPortMacMapAddr,
                                                    pRetValFsMIDot1qFutureVlanPortMacMapName);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortMacMapMcastBcastOption
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr

                The Object 
                retValFsMIDot1qFutureVlanPortMacMapMcastBcastOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortMacMapMcastBcastOption (INT4
                                                     i4FsMIDot1qFutureVlanPort,
                                                     tMacAddr
                                                     FsMIDot1qFutureVlanPortMacMapAddr,
                                                     INT4
                                                     *pi4RetValFsMIDot1qFutureVlanPortMacMapMcastBcastOption)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortMacMapMcastBcastOption ((INT4) u2LocalPortId,
                                                         FsMIDot1qFutureVlanPortMacMapAddr,
                                                         pi4RetValFsMIDot1qFutureVlanPortMacMapMcastBcastOption);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortMacMapRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortMacMapAddr

                The Object 
                retValFsMIDot1qFutureVlanPortMacMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortMacMapRowStatus (INT4 i4FsMIDot1qFutureVlanPort,
                                              tMacAddr
                                              FsMIDot1qFutureVlanPortMacMapAddr,
                                              INT4
                                              *pi4RetValFsMIDot1qFutureVlanPortMacMapRowStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortMacMapRowStatus
        ((INT4) u2LocalPortId,
         FsMIDot1qFutureVlanPortMacMapAddr,
         pi4RetValFsMIDot1qFutureVlanPortMacMapRowStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanFid
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object 
                retValFsMIDot1qFutureVlanFid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanFid (INT4 i4FsMIDot1qFutureVlanContextId,
                              UINT4 u4FsMIDot1qFutureVlanIndex,
                              UINT4 *pu4RetValFsMIDot1qFutureVlanFid)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanFid (u4FsMIDot1qFutureVlanIndex,
                                         pu4RetValFsMIDot1qFutureVlanFid);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanBridgeMode
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureVlanBridgeMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanBridgeMode (INT4 i4FsMIDot1qFutureVlanContextId,
                                     INT4
                                     *pi4RetValFsMIDot1qFutureVlanBridgeMode)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    *pi4RetValFsMIDot1qFutureVlanBridgeMode = VLAN_INVALID_BRIDGE_MODE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanTunnelBpduPri
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureVlanTunnelBpduPri
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanTunnelBpduPri (INT4 i4FsMIDot1qFutureVlanContextId,
                                        INT4
                                        *pi4RetValFsMIDot1qFutureVlanTunnelBpduPri)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    *pi4RetValFsMIDot1qFutureVlanTunnelBpduPri = 0;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanTunnelStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanTunnelStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanTunnelStatus (INT4 i4FsMIDot1qFutureVlanPort,
                                       INT4
                                       *pi4RetValFsMIDot1qFutureVlanTunnelStatus)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    *pi4RetValFsMIDot1qFutureVlanTunnelStatus = VLAN_DISABLED;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanTunnelStpPDUs
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanTunnelStpPDUs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanTunnelStpPDUs (INT4 i4FsMIDot1qFutureVlanPort,
                                        INT4
                                        *pi4RetValFsMIDot1qFutureVlanTunnelStpPDUs)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (pi4RetValFsMIDot1qFutureVlanTunnelStpPDUs);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanTunnelStpPDUsRecvd
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanTunnelStpPDUsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanTunnelStpPDUsRecvd (INT4 i4FsMIDot1qFutureVlanPort,
                                             UINT4
                                             *pu4RetValFsMIDot1qFutureVlanTunnelStpPDUsRecvd)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (pu4RetValFsMIDot1qFutureVlanTunnelStpPDUsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanTunnelStpPDUsSent
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanTunnelStpPDUsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanTunnelStpPDUsSent (INT4 i4FsMIDot1qFutureVlanPort,
                                            UINT4
                                            *pu4RetValFsMIDot1qFutureVlanTunnelStpPDUsSent)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (pu4RetValFsMIDot1qFutureVlanTunnelStpPDUsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUs
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanTunnelGvrpPDUs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUs (INT4 i4FsMIDot1qFutureVlanPort,
                                         INT4 *pi4RetValTunnelGvrpPDUs)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (pi4RetValTunnelGvrpPDUs);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUsRecvd
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanTunnelGvrpPDUsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUsRecvd (INT4 i4FsMIDot1qFutureVlanPort,
                                              UINT4
                                              *pu4RetValTunnelGvrpPDUsRecvd)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (pu4RetValTunnelGvrpPDUsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUsSent
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanTunnelGvrpPDUsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUsSent (INT4 i4FsMIDot1qFutureVlanPort,
                                             UINT4 *pu4RetValTunnelGvrpPDUsSent)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (pu4RetValTunnelGvrpPDUsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanTunnelIgmpPkts
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanTunnelIgmpPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanTunnelIgmpPkts (INT4 i4FsMIDot1qFutureVlanPort,
                                         INT4 *pi4RetValTunnelIgmpPkts)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (pi4RetValTunnelIgmpPkts);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanTunnelIgmpPktsRecvd
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanTunnelIgmpPktsRecvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanTunnelIgmpPktsRecvd (INT4 i4FsMIDot1qFutureVlanPort,
                                              UINT4
                                              *pu4RetValTunnelIgmpPktsRecvd)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (pu4RetValTunnelIgmpPktsRecvd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanTunnelIgmpPktsSent
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object 
                retValFsMIDot1qFutureVlanTunnelIgmpPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanTunnelIgmpPktsSent (INT4 i4FsMIDot1qFutureVlanPort,
                                             UINT4 *pu4RetValTunnelIgmpPktsSent)
{

    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);
    UNUSED_PARAM (pu4RetValTunnelIgmpPktsSent);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterRxUcast
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId

                The Object
                retValFsMIDot1qFutureVlanCounterRxUcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanCounterRxUcast
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanCounterRxUcast)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterRxUcast
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanCounterRxUcast);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterRxMcastBcast
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId

                The Object
                retValFsMIDot1qFutureVlanCounterRxMcastBcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanCounterRxMcastBcast
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanCounterRxMcastBcast)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterRxMcastBcast
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanCounterRxMcastBcast);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterTxUnknUcast
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId

                The Object
                retValFsMIDot1qFutureVlanCounterTxUnknUcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanCounterTxUnknUcast
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanCounterTxUnknUcast)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterTxUnknUcast
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanCounterTxUnknUcast);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterTxUcast
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId

                The Object
                retValFsMIDot1qFutureVlanCounterTxUcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanCounterTxUcast
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanCounterTxUcast)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterTxUcast
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanCounterTxUcast);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterTxBcast
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId

                The Object
                retValFsMIDot1qFutureVlanCounterTxBcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanCounterTxBcast
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanCounterTxBcast)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterTxBcast
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanCounterTxBcast);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterRxFrames
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId

                The Object
                retValFsMIDot1qFutureVlanCounterRxFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanCounterRxFrames
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanCounterRxFrames)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterRxFrames
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanCounterRxFrames);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterRxBytes
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId

                The Object
                retValFsMIDot1qFutureVlanCounterRxBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanCounterRxBytes
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanCounterRxBytes)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterRxBytes
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanCounterRxBytes);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterTxFrames
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId

                The Object
                retValFsMIDot1qFutureVlanCounterTxFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanCounterTxFrames
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanCounterTxFrames)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterTxFrames
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanCounterTxFrames);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterTxBytes
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId

                The Object
                retValFsMIDot1qFutureVlanCounterTxBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanCounterTxBytes
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanCounterTxBytes)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterTxBytes
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanCounterTxBytes);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterDiscardFrames
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId

                The Object
                retValFsMIDot1qFutureVlanCounterDiscardFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanCounterDiscardFrames
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanCounterDiscardFrames)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterDiscardFrames
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanCounterDiscardFrames);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterDiscardBytes
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanCounterVlanId

                The Object
                retValFsMIDot1qFutureVlanCounterDiscardBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanCounterDiscardBytes
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanCounterDiscardBytes)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterDiscardBytes
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanCounterDiscardBytes);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanCounterStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                retValFsMIDot1qFutureVlanCounterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanCounterStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                        UINT4 u4FsMIDot1qFutureVlanIndex,
                                        INT4
                                        *pi4RetValFsMIDot1qFutureVlanCounterStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanCounterStatus
        (u4FsMIDot1qFutureVlanIndex, pi4RetValFsMIDot1qFutureVlanCounterStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanUnicastMacLimit
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                retValFsMIDot1qFutureVlanUnicastMacLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanUnicastMacLimit
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     UINT4 *pu4RetValFsMIDot1qFutureVlanUnicastMacLimit)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanUnicastMacLimit
        (u4FsMIDot1qFutureVlanIndex,
         pu4RetValFsMIDot1qFutureVlanUnicastMacLimit);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanAdminMacLearningStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                retValFsMIDot1qFutureVlanAdminMacLearningStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanAdminMacLearningStatus
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     INT4 *pi4RetValFsMIDot1qFutureVlanAdminMacLearningStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanAdminMacLearningStatus
        (u4FsMIDot1qFutureVlanIndex,
         pi4RetValFsMIDot1qFutureVlanAdminMacLearningStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanOperMacLearningStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                retValFsMIDot1qFutureVlanOperMacLearningStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanOperMacLearningStatus
    (INT4 i4FsMIDot1qFutureVlanContextId,
     UINT4 u4FsMIDot1qFutureVlanIndex,
     INT4 *pi4RetValFsMIDot1qFutureVlanOperMacLearningStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanOperMacLearningStatus
        (u4FsMIDot1qFutureVlanIndex,
         pi4RetValFsMIDot1qFutureVlanOperMacLearningStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/************************** fsmsbext MIB Get Routines ***********************/

/****************************************************************************
 Function    :  nmhGetFsDot1dDeviceCapabilities
 Input       :  The Indices
                FsDot1dBridgeContextId

                The Object
                retValFsDot1dDeviceCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dDeviceCapabilities (INT4 i4FsDot1dBridgeContextId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsDot1dDeviceCapabilities)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1dBridgeContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dDeviceCapabilities (pRetValFsDot1dDeviceCapabilities);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTrafficClassesEnabled
 Input       :  The Indices
                FsDot1dBridgeContextId

                The Object
                retValFsDot1dTrafficClassesEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTrafficClassesEnabled (INT4 i4FsDot1dBridgeContextId,
                                    INT4 *pi4RetValFsDot1dTrafficClassesEnabled)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsDot1dBridgeContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dTrafficClassesEnabled
        (pi4RetValFsDot1dTrafficClassesEnabled);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortCapabilities
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortCapabilities (INT4 i4FsDot1dBasePort,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsDot1dPortCapabilities)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dPortCapabilities ((INT4) u2LocalPortId,
                                            pRetValFsDot1dPortCapabilities);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortDefaultUserPriority
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortDefaultUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortDefaultUserPriority (INT4 i4FsDot1dBasePort,
                                      INT4
                                      *pi4RetValFsDot1dPortDefaultUserPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dPortDefaultUserPriority ((INT4) u2LocalPortId,
                                                   pi4RetValFsDot1dPortDefaultUserPriority);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortNumTrafficClasses
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortNumTrafficClasses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortNumTrafficClasses (INT4 i4FsDot1dBasePort,
                                    INT4 *pi4RetValFsDot1dPortNumTrafficClasses)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dPortNumTrafficClasses ((INT4) u2LocalPortId,
                                                 pi4RetValFsDot1dPortNumTrafficClasses);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dRegenUserPriority
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dUserPriority

                The Object
                retValFsDot1dRegenUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dRegenUserPriority (INT4 i4FsDot1dBasePort,
                                INT4 i4FsDot1dUserPriority,
                                INT4 *pi4RetValFsDot1dRegenUserPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dRegenUserPriority ((INT4) u2LocalPortId,
                                             i4FsDot1dUserPriority,
                                             pi4RetValFsDot1dRegenUserPriority);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTrafficClass
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dTrafficClassPriority

                The Object
                retValFsDot1dTrafficClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTrafficClass (INT4 i4FsDot1dBasePort,
                           INT4 i4FsDot1dTrafficClassPriority,
                           INT4 *pi4RetValFsDot1dTrafficClass)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dTrafficClass ((INT4) u2LocalPortId,
                                        i4FsDot1dTrafficClassPriority,
                                        pi4RetValFsDot1dTrafficClass);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortOutboundAccessPriority
 Input       :  The Indices
                FsDot1dBasePort
                FsDot1dRegenUserPriority

                The Object
                retValFsDot1dPortOutboundAccessPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortOutboundAccessPriority (INT4 i4FsDot1dBasePort,
                                         INT4 i4FsDot1dRegenUserPriority,
                                         INT4
                                         *pi4RetValFsDot1dPortOutboundAccessPriority)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dPortOutboundAccessPriority ((INT4) u2LocalPortId,
                                                      i4FsDot1dRegenUserPriority,
                                                      pi4RetValFsDot1dPortOutboundAccessPriority);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTpHCPortInFrames
 Input       :  The Indices
                FsDot1dTpPort

                The Object
                retValFsDot1dTpHCPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpHCPortInFrames (INT4 i4FsDot1dTpPort,
                               tSNMP_COUNTER64_TYPE *
                               pu8RetValFsDot1dTpHCPortInFrames)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pu8RetValFsDot1dTpHCPortInFrames);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTpHCPortOutFrames
 Input       :  The Indices
                FsDot1dTpPort

                The Object
                retValFsDot1dTpHCPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpHCPortOutFrames (INT4 i4FsDot1dTpPort, tSNMP_COUNTER64_TYPE
                                * pu8RetValFsDot1dTpHCPortOutFrames)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pu8RetValFsDot1dTpHCPortOutFrames);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTpHCPortInDiscards
 Input       :  The Indices
                FsDot1dTpPort

                The Object
                retValFsDot1dTpHCPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpHCPortInDiscards (INT4 i4FsDot1dTpPort,
                                 tSNMP_COUNTER64_TYPE *
                                 pu8RetValFsDot1dTpHCPortInDiscards)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pu8RetValFsDot1dTpHCPortInDiscards);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTpPortInOverflowFrames
 Input       :  The Indices
                FsDot1dTpPort

                The Object
                retValFsDot1dTpPortInOverflowFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpPortInOverflowFrames (INT4 i4FsDot1dTpPort,
                                     UINT4
                                     *pu4RetValFsDot1dTpPortInOverflowFrames)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pu4RetValFsDot1dTpPortInOverflowFrames);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTpPortOutOverflowFrames
 Input       :  The Indices
                FsDot1dTpPort

                The Object
                retValFsDot1dTpPortOutOverflowFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpPortOutOverflowFrames (INT4 i4FsDot1dTpPort,
                                      UINT4
                                      *pu4RetValFsDot1dTpPortOutOverflowFrames)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pu4RetValFsDot1dTpPortOutOverflowFrames);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dTpPortInOverflowDiscards
 Input       :  The Indices
                FsDot1dTpPort

                The Object
                retValFsDot1dTpPortInOverflowDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dTpPortInOverflowDiscards (INT4 i4FsDot1dTpPort,
                                       UINT4
                                       *pu4RetValFsDot1dTpPortInOverflowDiscards)
{
    UNUSED_PARAM (i4FsDot1dTpPort);
    UNUSED_PARAM (pu4RetValFsDot1dTpPortInOverflowDiscards);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanOldTpFdbPort
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qTpFdbAddress
                                                                            
                The Object
                retValFsMIDot1qFutureVlanOldTpFdbPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanOldTpFdbPort (INT4 i4FsDot1qVlanContextId,
                                       UINT4 u4FsDot1qFdbId,
                                       tMacAddr FsDot1qTpFdbAddress,
                                       INT4
                                       *pi4RetValFsMIDot1qFutureVlanOldTpFdbPort)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qFdbId);
    UNUSED_PARAM (FsDot1qTpFdbAddress);

    *pi4RetValFsMIDot1qFutureVlanOldTpFdbPort = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureConnectionIdentifier
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qTpFdbAddress

                The Object
                retValFsMIDot1qFutureConnectionIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureConnectionIdentifier
    (INT4 i4FsDot1qVlanContextId,
     UINT4 u4FsDot1qFdbId,
     tMacAddr FsDot1qTpFdbAddress,
     tMacAddr * pRetValFsMIDot1qFutureConnectionIdentifier)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureConnectionIdentifier
        (u4FsDot1qFdbId, FsDot1qTpFdbAddress,
         pRetValFsMIDot1qFutureConnectionIdentifier);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanWildCardRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress

                The Object 
                retValFsMIDot1qFutureVlanWildCardRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanWildCardRowStatus
    (INT4 i4FsDot1qVlanContextId, tMacAddr WildCardMacAddress,
     INT4 *pi4RetValWildCardRowStatus)
{
    tVlanWildCardEntry *pWildCardEntry = NULL;

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (pWildCardEntry != NULL)
    {
        *pi4RetValWildCardRowStatus = pWildCardEntry->u1RowStatus;

        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    VlanReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanIsWildCardEgressPort
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanWildCardMacAddress
                FsDot1qTpPort

                The Object 
                retValFsMIDot1qFutureVlanIsWildCardEgressPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsMIDot1qFutureVlanIsWildCardEgressPort
    (INT4 i4FsDot1qVlanContextId, tMacAddr WildCardMacAddress,
     INT4 i4FsDot1qTpPort, INT4 *pi4RetValIsWildCardEgressPort)
{
    tVlanTempPortList  *pTmpPortList = NULL;
    tVlanWildCardEntry *pWildCardEntry = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
    {
        return SNMP_FAILURE;
    }
    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pWildCardEntry = VlanGetWildCardEntry (WildCardMacAddress);

    if (pWildCardEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pWildCardEntry->u1RowStatus == VLAN_ACTIVE)
    {
        VLAN_IS_MEMBER_PORT (pWildCardEntry->EgressPorts,
                             u2LocalPortId, u1Result);
    }
    else
    {
        VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId == (UINT4) i4FsDot1qVlanContextId) &&
                (pTmpPortList->u1Type == VLAN_WILDCARD_TABLE) &&
                (VLAN_ARE_MAC_ADDR_EQUAL
                 (pTmpPortList->PortListTbl.WildCardTbl.MacAddr,
                  WildCardMacAddress)))
            {
                break;
            }
        }

        if (pTmpPortList == NULL)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
        VLAN_IS_MEMBER_PORT (pTmpPortList->PortListTbl.WildCardTbl.EgressPorts,
                             u2LocalPortId, u1Result);
    }

    u1Result = (u1Result == VLAN_TRUE) ? VLAN_SNMP_TRUE : VLAN_SNMP_FALSE;
    *pi4RetValIsWildCardEgressPort = u1Result;

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortProtected
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortProtected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortProtected (INT4 i4FsMIDot1qFutureVlanPort,
                                        INT4
                                        *pi4RetValFsMIDot1qFutureVlanPortProtected)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortProtected ((INT4) u2LocalPortId,
                                                   pi4RetValFsMIDot1qFutureVlanPortProtected);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortSubnetBasedClassification
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortSubnetBasedClassification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsMIDot1qFutureVlanPortSubnetBasedClassification
    (INT4 i4FsMIDot1qFutureVlanPort,
     INT4 *pi4RetValFsMIDot1qFutureVlanPortSubnetBasedClassification)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext ((INT4) u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortSubnetBasedClassification
        ((INT4) u2LocalPortId,
         pi4RetValFsMIDot1qFutureVlanPortSubnetBasedClassification);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortUnicastMacLearning
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortUnicastMacLearning
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortUnicastMacLearning (INT4 i4FsMIDot1qFutureVlanPort,
                                                 INT4
                                                 *pi4RetValFsMIDot1qFutureVlanPortUnicastMacLearning)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortUnicastMacLearning ((INT4) u2LocalPort,
                                                     pi4RetValFsMIDot1qFutureVlanPortUnicastMacLearning);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureUnicastMacLearningLimit
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureUnicastMacLearningLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureUnicastMacLearningLimit (INT4
                                              i4FsMIDot1qFutureVlanContextId,
                                              UINT4
                                              *pu4RetValFsMIDot1qFutureUnicastMacLearningLimit)
{
    INT1                i1RetVal;

    if (VlanSelectContext (i4FsMIDot1qFutureVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureUnicastMacLearningLimit
        (pu4RetValFsMIDot1qFutureUnicastMacLearningLimit);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureStaticConnectionIdentifier
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qFdbId
                FsDot1qStaticUnicastAddress
                FsDot1qStaticUnicastReceivePort

                The Object
                retValFsMIDot1qFutureStaticConnectionIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsMIDot1qFutureStaticConnectionIdentifier
    (INT4 i4FsDot1qVlanContextId,
     UINT4 u4FsDot1qFdbId,
     tMacAddr FsDot1qStaticUnicastAddress,
     INT4 i4FsDot1qStaticUnicastReceivePort,
     tMacAddr * pRetValFsMIDot1qFutureStaticConnectionIdentifier)
{
    tVlanTempPortList  *pTmpPortList = NULL;
    tVlanStUcastEntry  *pVlanStUcastEntry = NULL;
    UINT4               u4FidIndex;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId = 0;

    if (i4FsDot1qStaticUnicastReceivePort != 0)
    {
        if (VlanGetContextInfoFromIfIndex
            ((UINT4) i4FsDot1qStaticUnicastReceivePort,
             &u4ContextId, &u2LocalPortId) != VLAN_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (u4ContextId != (UINT4) i4FsDot1qVlanContextId)
        {
            return SNMP_FAILURE;
        }
    }

    if (VlanSelectContext (i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u4FidIndex = VlanGetFidEntryFromFdbId (u4FsDot1qFdbId);

    if (u4FidIndex == VLAN_INVALID_FID_INDEX)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pVlanStUcastEntry = VlanGetStaticUcastEntryWithExactRcvPort
        (u4FidIndex, FsDot1qStaticUnicastAddress, u2LocalPortId);
    if (pVlanStUcastEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (pVlanStUcastEntry->u1RowStatus == VLAN_ACTIVE)
    {
        MEMCPY (pRetValFsMIDot1qFutureStaticConnectionIdentifier,
                pVlanStUcastEntry->ConnectionId, sizeof (tMacAddr));

        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }
    else
    {
        VLAN_SLL_SCAN (&gVlanTempPortList, pTmpPortList, tVlanTempPortList *)
        {
            if ((pTmpPortList->u4ContextId == (UINT4) i4FsDot1qVlanContextId) &&
                (pTmpPortList->u1Type == VLAN_ST_UCAST_TABLE) &&
                (pTmpPortList->PortListTbl.StUcastTbl.u4FdbId ==
                 u4FsDot1qFdbId) &&
                (pTmpPortList->PortListTbl.StUcastTbl.u2RcvPort ==
                 u2LocalPortId) &&
                (VLAN_ARE_MAC_ADDR_EQUAL
                 (pTmpPortList->PortListTbl.StUcastTbl.MacAddr,
                  FsDot1qStaticUnicastAddress)))
            {
                MEMCPY (pRetValFsMIDot1qFutureStaticConnectionIdentifier,
                        pTmpPortList->PortListTbl.StUcastTbl.ConnectionId,
                        sizeof (tMacAddr));

                VlanReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
    }

    VlanReleaseContext ();
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortSubnetMapVid
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr

                The Object
                retValFsMIDot1qFutureVlanPortSubnetMapVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortSubnetMapVid (INT4 i4FsMIDot1qFutureVlanPort,
                                           UINT4
                                           u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                           INT4
                                           *pi4RetValFsMIDot1qFutureVlanPortSubnetMapVid)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortSubnetMapVid
        ((INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapAddr,
         pi4RetValFsMIDot1qFutureVlanPortSubnetMapVid);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortSubnetMapARPOption
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr

                The Object
                retValFsMIDot1qFutureVlanPortSubnetMapARPOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortSubnetMapARPOption (INT4 i4FsMIDot1qFutureVlanPort,
                                                 UINT4
                                                 u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                                 INT4
                                                 *pi4RetValFsMIDot1qFutureVlanPortSubnetMapARPOption)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortSubnetMapARPOption
        ((INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapAddr,
         pi4RetValFsMIDot1qFutureVlanPortSubnetMapARPOption);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortSubnetMapRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapAddr

                The Object
                retValFsMIDot1qFutureVlanPortSubnetMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortSubnetMapRowStatus (INT4 i4FsMIDot1qFutureVlanPort,
                                                 UINT4
                                                 u4FsMIDot1qFutureVlanPortSubnetMapAddr,
                                                 INT4
                                                 *pi4RetValFsMIDot1qFutureVlanPortSubnetMapRowStatus)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortSubnetMapRowStatus
        ((INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapAddr,
         pi4RetValFsMIDot1qFutureVlanPortSubnetMapRowStatus);

    VlanReleaseContext ();

    return i1RetVal;
}

/****************************************************************************
Function    :  nmhGetFsMIDot1qFutureStVlanType
Input       :  The Indices
FsDot1qVlanContextId
FsDot1qVlanIndex

The Object
retValFsMIDot1qFutureStVlanType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIDot1qFutureStVlanType (INT4 i4FsDot1qVlanContextId,
                                 UINT4 u4FsDot1qVlanIndex,
                                 INT4 *pi4RetValFsMIDot1qFutureStVlanType)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4FsDot1qVlanIndex);
    *pi4RetValFsMIDot1qFutureStVlanType = L2IWF_NORMAL_VLAN;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsMIDot1qFutureStVlanVid
Input       :  The Indices
FsDot1qVlanContextId
FsDot1qVlanIndex

The Object
retValFsMIDot1qFutureStVlanVid
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIDot1qFutureStVlanVid (INT4 i4FsDot1qVlanContextId,
                                UINT4 u4FsDot1qVlanIndex,
                                INT4 *pi4RetValFsMIDot1qFutureStVlanVid)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    *pi4RetValFsMIDot1qFutureStVlanVid = u4FsDot1qVlanIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanSwStatsEnabled
 Input       :  The Indices

                The Object 
                retValFsMIDot1qFutureVlanSwStatsEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanSwStatsEnabled (INT4
                                         *pi4RetValFsMIDot1qFutureVlanSwStatsEnabled)
{
    return ((nmhGetDot1qFutureVlanSwStatsEnabled
             (pi4RetValFsMIDot1qFutureVlanSwStatsEnabled)));
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortSubnetMapExtVid
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask

                The Object
                retValFsMIDot1qFutureVlanPortSubnetMapExtVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsMIDot1qFutureVlanPortSubnetMapExtVid
    (INT4 i4FsMIDot1qFutureVlanPort,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
     INT4 *pi4RetValFsMIDot1qFutureVlanPortSubnetMapExtVid)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetDot1qFutureVlanPortSubnetMapExtVid ((INT4) u2LocalPortId,
                                                  u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
                                                  u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
                                                  pi4RetValFsMIDot1qFutureVlanPortSubnetMapExtVid)
        != SNMP_SUCCESS)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortSubnetMapExtARPOption
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask

                The Object
                retValFsMIDot1qFutureVlanPortSubnetMapExtARPOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsMIDot1qFutureVlanPortSubnetMapExtARPOption
    (INT4 i4FsMIDot1qFutureVlanPort,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
     INT4 *pi4RetValFsMIDot1qFutureVlanPortSubnetMapExtARPOption)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortSubnetMapExtARPOption
        ((INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
         u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
         pi4RetValFsMIDot1qFutureVlanPortSubnetMapExtARPOption);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortSubnetMapExtRowStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                FsMIDot1qFutureVlanPortSubnetMapExtAddr
                FsMIDot1qFutureVlanPortSubnetMapExtMask

                The Object
                retValFsMIDot1qFutureVlanPortSubnetMapExtRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsMIDot1qFutureVlanPortSubnetMapExtRowStatus
    (INT4 i4FsMIDot1qFutureVlanPort,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
     UINT4 u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
     INT4 *pi4RetValFsMIDot1qFutureVlanPortSubnetMapExtRowStatus)
{
    UINT4               u4ContextId = VLAN_INIT_VAL;
    UINT2               u2LocalPortId = VLAN_INIT_VAL;
    INT1                i1RetVal = VLAN_INIT_VAL;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortSubnetMapExtRowStatus
        ((INT4) u2LocalPortId,
         u4FsMIDot1qFutureVlanPortSubnetMapExtAddr,
         u4FsMIDot1qFutureVlanPortSubnetMapExtMask,
         pi4RetValFsMIDot1qFutureVlanPortSubnetMapExtRowStatus);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanGlobalsFdbFlush
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                retValFsMIDot1qFutureVlanGlobalsFdbFlush
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanGlobalsFdbFlush (INT4 i4FsMIDot1qFutureVlanContextId,
                                          INT4
                                          *pi4RetValFsMIDot1qFutureVlanGlobalsFdbFlush)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanGlobalsFdbFlush
        (pi4RetValFsMIDot1qFutureVlanGlobalsFdbFlush);
    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanUserDefinedTPID
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                retValFsMIDot1qFutureVlanUserDefinedTPID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanUserDefinedTPID (INT4 i4FsMIDot1qFutureVlanContextId,
                                          INT4
                                          *pi4RetValFsMIDot1qFutureVlanUserDefinedTPID)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanUserDefinedTPID
        (pi4RetValFsMIDot1qFutureVlanUserDefinedTPID);
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanRemoteFdbFlush
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                retValFsMIDot1qFutureVlanRemoteFdbFlush
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanRemoteFdbFlush (INT4 i4FsMIDot1qFutureVlanContextId,
                                         INT4
                                         *pi4RetValFsMIDot1qFutureVlanRemoteFdbFlush)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanRemoteFdbFlush
        (pi4RetValFsMIDot1qFutureVlanRemoteFdbFlush);
    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortFdbFlush
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortFdbFlush
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortFdbFlush (INT4 i4FsMIDot1qFutureVlanPort,
                                       INT4
                                       *pi4RetValFsMIDot1qFutureVlanPortFdbFlush)
{
    tVlanPortEntry     *pVlanPortEntry = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);
    if (pVlanPortEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIDot1qFutureVlanPortFdbFlush = pVlanPortEntry->bPortFdbFlush;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortIngressEtherType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortIngressEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortIngressEtherType (INT4 i4FsMIDot1qFutureVlanPort,
                                               INT4
                                               *pi4RetValFsMIDot1qFutureVlanPortIngressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortIngressEtherType ((INT4) u2LocalPort,
                                                   pi4RetValFsMIDot1qFutureVlanPortIngressEtherType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortEgressEtherType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortEgressEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortEgressEtherType (INT4 i4FsMIDot1qFutureVlanPort,
                                              INT4
                                              *pi4RetValFsMIDot1qFutureVlanPortEgressEtherType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortEgressEtherType ((INT4) u2LocalPort,
                                                  pi4RetValFsMIDot1qFutureVlanPortEgressEtherType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortEgressTPIDType
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortEgressTPIDType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/ INT1
nmhGetFsMIDot1qFutureVlanPortEgressTPIDType (INT4 i4FsMIDot1qFutureVlanPort,
                                             INT4
                                             *pi4RetValFsMIDot1qFutureVlanPortEgressTPIDType)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortEgressTPIDType ((INT4) u2LocalPort,
                                                 pi4RetValFsMIDot1qFutureVlanPortEgressTPIDType);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortAllowableTPID1
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortAllowableTPID1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortAllowableTPID1 (INT4 i4FsMIDot1qFutureVlanPort,
                                             INT4
                                             *pi4RetValFsMIDot1qFutureVlanPortAllowableTPID1)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortAllowableTPID1 ((INT4) u2LocalPort,
                                                 pi4RetValFsMIDot1qFutureVlanPortAllowableTPID1);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortAllowableTPID2
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortAllowableTPID2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortAllowableTPID2 (INT4 i4FsMIDot1qFutureVlanPort,
                                             INT4
                                             *pi4RetValFsMIDot1qFutureVlanPortAllowableTPID2)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortAllowableTPID2 ((INT4) u2LocalPort,
                                                 pi4RetValFsMIDot1qFutureVlanPortAllowableTPID2);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortAllowableTPID3
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortAllowableTPID3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortAllowableTPID3 (INT4 i4FsMIDot1qFutureVlanPort,
                                             INT4
                                             *pi4RetValFsMIDot1qFutureVlanPortAllowableTPID3)
{
    UINT4               u4ContextId = VLAN_INVALID_CONTEXT;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    INT1                i1RetVal = SNMP_FAILURE;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPort) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortAllowableTPID3 ((INT4) u2LocalPort,
                                                 pi4RetValFsMIDot1qFutureVlanPortAllowableTPID3);

    VlanReleaseContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureStVlanFdbFlush
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object
                retValFsMIDot1qFutureStVlanFdbFlush
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureStVlanFdbFlush (INT4 i4FsDot1qVlanContextId,
                                     UINT4 u4FsDot1qVlanIndex,
                                     INT4
                                     *pi4RetValFsMIDot1qFutureStVlanFdbFlush)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1qFutureVlanPortFdbFlush
        (u4FsDot1qVlanIndex, pi4RetValFsMIDot1qFutureStVlanFdbFlush);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureStVlanEgressEthertype
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex

                The Object
                retValFsMIDot1qFutureStVlanEgressEthertype
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureStVlanEgressEthertype (INT4 i4FsDot1qVlanContextId,
                                            UINT4 u4FsDot1qVlanIndex,
                                            INT4
                                            *pi4RetValFsMIDot1qFutureStVlanEgressEthertype)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i1RetVal = nmhGetDot1qFutureStVlanEgressEthertype
        (u4FsDot1qVlanIndex, pi4RetValFsMIDot1qFutureStVlanEgressEthertype);

    VlanReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFuturePortVlanFdbFlush
 Input       :  The Indices
                FsDot1qVlanContextId
                FsDot1qVlanIndex
                FsDot1qTpPort

                The Object
                retValFsMIDot1qFuturePortVlanFdbFlush
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFuturePortVlanFdbFlush (INT4 i4FsDot1qVlanContextId,
                                       UINT4 u4FsDot1qVlanIndex,
                                       INT4 i4FsDot1qTpPort,
                                       INT4
                                       *pi4RetValFsMIDot1qFuturePortVlanFdbFlush)
{
    tStaticVlanEntry   *pStVlanEntry = NULL;
    UINT4               u4ContextId = VLAN_ZERO;
    UINT2               u2LocalPortId = VLAN_ZERO;
    UINT1               u1Result = VLAN_FALSE;

    if (VlanSelectContext ((UINT4) i4FsDot1qVlanContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4FsDot1qTpPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != VLAN_SUCCESS)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    if (u2LocalPortId >= VLAN_MAX_PORTS + 1)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    pStVlanEntry = VlanGetStaticVlanEntry ((tVlanId) u4FsDot1qVlanIndex);
    if (pStVlanEntry == NULL)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;
    }

    VLAN_GET_FDB_FLUSH (pStVlanEntry, u2LocalPortId, u1Result);

    *pi4RetValFsMIDot1qFuturePortVlanFdbFlush = u1Result;
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanLoopbackStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId
                FsMIDot1qFutureVlanIndex

                The Object
                retValFsMIDot1qFutureVlanLoopbackStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanLoopbackStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                         UINT4 u4FsMIDot1qFutureVlanIndex,
                                         INT4
                                         *pi4RetValFsMIDot1qFutureVlanLoopbackStatus)
{
    INT1                i1RetVal;

    if (VlanSelectContext ((UINT4) i4FsMIDot1qFutureVlanContextId) !=
        VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanLoopbackStatus
        (u4FsMIDot1qFutureVlanIndex,
         pi4RetValFsMIDot1qFutureVlanLoopbackStatus);

    VlanReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFuturePortPacketReflectionStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanPort
                The Object
                retValFsMIDot1qFuturePortPacketReflectionStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFuturePortPacketReflectionStatus (INT4 i4FsMIDot1qFutureVlanPort,
                                                 INT4
                                                 *pi4RetValFsMIDot1qFuturePortPacketReflectionStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1RetVal = 0;

    if (VlanGetContextInfoFromIfIndex
        ((UINT4) i4FsMIDot1qFutureVlanPort, &u4ContextId,
         &u2LocalPortId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFuturePortPacketReflectionStatus ((INT4) u2LocalPortId,
                                                     pi4RetValFsMIDot1qFuturePortPacketReflectionStatus);

    if (i1RetVal == SNMP_FAILURE)
    {
        VlanReleaseContext ();
        return SNMP_FAILURE;

    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}
