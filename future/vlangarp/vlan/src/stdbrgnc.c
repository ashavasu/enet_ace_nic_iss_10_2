
/* $Id: stdbrgnc.c,v 1.1 2015/12/31 11:29:24 siva Exp $
    ISS Wrapper module
    module P-BRIDGE-MIB

 */

#include "lr.h"
#include "vlaninc.h"
#include "fssnmp.h"
#include "stdbrgnc.h"

/********************************************************************
* FUNCTION NcDot1dDeviceCapabilitiesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dDeviceCapabilitiesGet (
                UINT1 *pDot1dDeviceCapabilities )
{

    INT1 i1RetVal;
    tSNMP_OCTET_STRING_TYPE RetValDot1dDeviceCapabilities ;

    RetValDot1dDeviceCapabilities.pu1_OctetList = pDot1dDeviceCapabilities;
    
    i1RetVal = nmhGetDot1dDeviceCapabilities(
                 &RetValDot1dDeviceCapabilities );

    return i1RetVal;

} /* NcDot1dDeviceCapabilitiesGet */

/********************************************************************
* FUNCTION NcDot1dTrafficClassesEnabledSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dTrafficClassesEnabledSet (
                INT4 i4Dot1dTrafficClassesEnabled )
{

		INT1 i1RetVal;
		if (!i4Dot1dTrafficClassesEnabled)
		{
				i4Dot1dTrafficClassesEnabled = VLAN_SNMP_FALSE ;
		} 

		VLAN_LOCK ();
		i1RetVal = nmhSetDot1dTrafficClassesEnabled(
						i4Dot1dTrafficClassesEnabled);
		VLAN_UNLOCK ();

		return i1RetVal;

} /* NcDot1dTrafficClassesEnabledSet */

/********************************************************************
* FUNCTION NcDot1dTrafficClassesEnabledTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dTrafficClassesEnabledTest (UINT4 *pu4Error,
                INT4 i4Dot1dTrafficClassesEnabled )
{
		INT1 i1RetVal;
		if (!i4Dot1dTrafficClassesEnabled)
		{
				i4Dot1dTrafficClassesEnabled = VLAN_SNMP_FALSE ;
		} 

		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1dTrafficClassesEnabled(pu4Error,
						i4Dot1dTrafficClassesEnabled);
		VLAN_UNLOCK ();

		return i1RetVal;

} /* NcDot1dTrafficClassesEnabledTest */

/********************************************************************
* FUNCTION NcDot1dGmrpStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dGmrpStatusSet (
                INT4 i4Dot1dGmrpStatus )
{

		INT1 i1RetVal;

		GARP_LOCK ();
		i1RetVal = nmhSetDot1dGmrpStatus(
						i4Dot1dGmrpStatus);
		GARP_UNLOCK ();

		return i1RetVal;

} /* NcDot1dGmrpStatusSet */

/********************************************************************
* FUNCTION NcDot1dGmrpStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dGmrpStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1dGmrpStatus )
{

    INT1 i1RetVal;

	GARP_LOCK ();

    i1RetVal = nmhTestv2Dot1dGmrpStatus(pu4Error,
                i4Dot1dGmrpStatus);

	GARP_UNLOCK ();

    return i1RetVal;


} /* NcDot1dGmrpStatusTest */

/********************************************************************
* FUNCTION NcDot1dTpHCPortInFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dTpHCPortInFramesGet (
                INT4 i4Dot1dTpPort,
                unsigned long long *pu8Dot1dTpHCPortInFrames )
{

		INT1 i1RetVal;
		tSNMP_COUNTER64_TYPE u8Dot1dTpHCPortInFrames ;

		MEMSET(&u8Dot1dTpHCPortInFrames ,  0 , sizeof (u8Dot1dTpHCPortInFrames));

		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1dTpHCPortTable(
								i4Dot1dTpPort) == SNMP_FAILURE)
		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}

		i1RetVal = nmhGetDot1dTpHCPortInFrames(
						i4Dot1dTpPort,
						&u8Dot1dTpHCPortInFrames );

		*pu8Dot1dTpHCPortInFrames =((unsigned long long)(u8Dot1dTpHCPortInFrames.msn)) << 32 | u8Dot1dTpHCPortInFrames.lsn;
		VLAN_UNLOCK ();

		return i1RetVal;

} /* NcDot1dTpHCPortInFramesGet */

/********************************************************************
* FUNCTION NcDot1dTpHCPortOutFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dTpHCPortOutFramesGet (
                INT4 i4Dot1dTpPort,
                unsigned long long *pu8Dot1dTpHCPortOutFrames )
{

		INT1 i1RetVal;
		tSNMP_COUNTER64_TYPE u8Dot1dTpHCPortOutFrames ;

		MEMSET(&u8Dot1dTpHCPortOutFrames ,  0 , sizeof (u8Dot1dTpHCPortOutFrames));
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1dTpHCPortTable(
								i4Dot1dTpPort) == SNMP_FAILURE)
		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}
		i1RetVal = nmhGetDot1dTpHCPortOutFrames(
						i4Dot1dTpPort,
						&u8Dot1dTpHCPortOutFrames );

		*pu8Dot1dTpHCPortOutFrames = ((unsigned long long)(u8Dot1dTpHCPortOutFrames.msn ))<< 32 | u8Dot1dTpHCPortOutFrames.lsn;
		VLAN_UNLOCK ();

		return i1RetVal;


} /* NcDot1dTpHCPortOutFramesGet */

/********************************************************************
* FUNCTION NcDot1dTpHCPortInDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dTpHCPortInDiscardsGet (
                INT4 i4Dot1dTpPort,
                unsigned long long  *pu8Dot1dTpHCPortInDiscards )
{
		INT1 i1RetVal;
		tSNMP_COUNTER64_TYPE u8Dot1dTpHCPortInDiscards ;

		MEMSET(&u8Dot1dTpHCPortInDiscards ,  0 , sizeof (u8Dot1dTpHCPortInDiscards));
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1dTpHCPortTable(
								i4Dot1dTpPort) == SNMP_FAILURE)
		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}
		i1RetVal = nmhGetDot1dTpHCPortInDiscards(
						i4Dot1dTpPort,
						&u8Dot1dTpHCPortInDiscards );

		*pu8Dot1dTpHCPortInDiscards = ((unsigned long long)(u8Dot1dTpHCPortInDiscards.msn)) << 32 | u8Dot1dTpHCPortInDiscards.lsn;
		VLAN_UNLOCK ();

		return i1RetVal;

} /* NcDot1dTpHCPortInDiscardsGet */

/********************************************************************
* FUNCTION NcDot1dTpPortInOverflowFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dTpPortInOverflowFramesGet (
                INT4 i4Dot1dTpPort,
                UINT4 *pu4Dot1dTpPortInOverflowFrames )
{
		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1dTpPortOverflowTable(
								i4Dot1dTpPort) == SNMP_FAILURE)
		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}
		i1RetVal = nmhGetDot1dTpPortInOverflowFrames(
						i4Dot1dTpPort,
						pu4Dot1dTpPortInOverflowFrames );
		VLAN_UNLOCK ();

		return i1RetVal;

} /* NcDot1dTpPortInOverflowFramesGet */

/********************************************************************
* FUNCTION NcDot1dTpPortOutOverflowFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dTpPortOutOverflowFramesGet (
                INT4 i4Dot1dTpPort,
                UINT4 *pu4Dot1dTpPortOutOverflowFrames )
{
		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1dTpPortOverflowTable(
								i4Dot1dTpPort) == SNMP_FAILURE)
		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}
		i1RetVal = nmhGetDot1dTpPortOutOverflowFrames(
						i4Dot1dTpPort,
						pu4Dot1dTpPortOutOverflowFrames );
		VLAN_UNLOCK ();

		return i1RetVal;

} /* NcDot1dTpPortOutOverflowFramesGet */

/********************************************************************
* FUNCTION NcDot1dTpPortInOverflowDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dTpPortInOverflowDiscardsGet (
                INT4 i4Dot1dTpPort,
                UINT4 *pu4Dot1dTpPortInOverflowDiscards )
{
		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1dTpPortOverflowTable(
								i4Dot1dTpPort) == SNMP_FAILURE)
		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}
		i1RetVal = nmhGetDot1dTpPortInOverflowDiscards(
						i4Dot1dTpPort,
						pu4Dot1dTpPortInOverflowDiscards );

		VLAN_UNLOCK ();
		return i1RetVal;

} /* NcDot1dTpPortInOverflowDiscardsGet */

/********************************************************************
* FUNCTION NcDot1dRegenUserPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dRegenUserPrioritySet (
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1dUserPriority,
                INT4 i4Dot1dRegenUserPriority )
{
		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhSetDot1dRegenUserPriority(
						i4Dot1dBasePort,
						i4Dot1dUserPriority,
						i4Dot1dRegenUserPriority);

		VLAN_UNLOCK ();
		return i1RetVal;


} /* NcDot1dRegenUserPrioritySet */

/********************************************************************
* FUNCTION NcDot1dRegenUserPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dRegenUserPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1dUserPriority,
                INT4 i4Dot1dRegenUserPriority )
{

		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1dRegenUserPriority(pu4Error,
						i4Dot1dBasePort,
						i4Dot1dUserPriority,
						i4Dot1dRegenUserPriority);
		VLAN_UNLOCK ();
		return i1RetVal;

} /* NcDot1dRegenUserPriorityTest */

/********************************************************************
* FUNCTION NcDot1dTrafficClassSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dTrafficClassSet (
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1dTrafficClassPriority,
                INT4 i4Dot1dTrafficClass )
{
		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhSetDot1dTrafficClass(
						i4Dot1dBasePort,
						i4Dot1dTrafficClassPriority,
						i4Dot1dTrafficClass);
		VLAN_UNLOCK ();
		return i1RetVal;

} /* NcDot1dTrafficClassSet */

/********************************************************************
* FUNCTION NcDot1dTrafficClassTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dTrafficClassTest (UINT4 *pu4Error,
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1dTrafficClassPriority,
                INT4 i4Dot1dTrafficClass )
{
		INT1 i1RetVal;
		VLAN_LOCK ();
		i1RetVal = nmhTestv2Dot1dTrafficClass(pu4Error,
						i4Dot1dBasePort,
						i4Dot1dTrafficClassPriority,
						i4Dot1dTrafficClass);
		VLAN_UNLOCK ();
		return i1RetVal;

} /* NcDot1dTrafficClassTest */

/********************************************************************
* FUNCTION NcDot1dPortOutboundAccessPriorityGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcDot1dPortOutboundAccessPriorityGet (
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1dRegenUserPriority,
                INT4 *pi4Dot1dPortOutboundAccessPriority )
{
		INT1 i1RetVal;
		VLAN_LOCK ();
		if (nmhValidateIndexInstanceDot1dPortOutboundAccessPriorityTable(
								i4Dot1dBasePort,
								i4Dot1dRegenUserPriority) == SNMP_FAILURE)
		{
				VLAN_UNLOCK ();
				return SNMP_FAILURE;
		}
		i1RetVal = nmhGetDot1dPortOutboundAccessPriority(
						i4Dot1dBasePort,
						i4Dot1dRegenUserPriority,
						pi4Dot1dPortOutboundAccessPriority );
		VLAN_UNLOCK ();
		return i1RetVal;

} /* NcDot1dPortOutboundAccessPriorityGet */

/* END i_P_BRIDGE_MIB.c */
