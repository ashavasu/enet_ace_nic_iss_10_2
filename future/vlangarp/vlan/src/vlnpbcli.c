/*$Id: vlnpbcli.c,v 1.133 2017/11/16 13:39:20 siva Exp $*/
#ifndef __VLNPBCLI_C__
#define __VLNPBCLI_C__
/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : vlnpbcli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : ISS TEAM                                         |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : Provider Bridge                                  |
 * |                                                                           |
 * |  MODULE NAME           : Provider Bridge configuration                    |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for set/get objects in           | 
 * |                          fspb.mib                                         |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#include "vlaninc.h"
#include "fsdot1cli.h"

INT4
cli_process_pb_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[VLAN_MAX_ARGS];
    INT1                argno = 0;
    UINT2               u2Port = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4Inst = 0;
    INT4                i4Val = 0;
    UINT4               u4SVlanClass;
    UINT4               u4ErrCode;
    UINT4               u4IfIndex = 0;
    UINT4               u4Action = 0;
    UINT4               u4Protocol = 0;
    tVlanId             VlanId = 0;
    tMacAddr            au1InMacAddr;
    UINT4               u4ContextId = 0;
    UINT4               u4TmpContextId;
    UINT2               u2LocalPortId = 0;

    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);
    VLAN_LOCK ();

    if (VlanPbCliSelectContextOnMode
        (CliHandle, u4Command, &u4ContextId, &u2LocalPortId) == VLAN_FAILURE)
    {
        VLAN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    i4Inst = (INT4) va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
        if (VcmGetContextInfoFromIfIndex
            ((UINT4) u4IfIndex, &u4TmpContextId,
             &u2LocalPortId) != VLAN_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%% Port not mapped to any of the context \r\n");
            i4RetStatus = CLI_FAILURE;
        }
    }
    /*remaining arguments */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == VLAN_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_PB_MAC_LIMIT:

            i4RetStatus =
                VlanPbSetMulticastMacLimit (CliHandle, *((INT4 *) args[0]));
            break;
        case CLI_PB_NO_MAC_LIMIT:

            if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
            {
                CliPrintf (CliHandle, "\r%% Bridge is in Customer Mode\r\n");
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            i4RetStatus =
                VlanPbSetMulticastMacLimit (CliHandle,
                                            PB_DEFAULT_MULTICAST_MAC_LIMIT);

            break;

        case CLI_PB_PORT_CVLANID:

            i4RetStatus =
                VlanPbSetCustomerVlanId (CliHandle, (INT4) u2LocalPortId,
                                         *((UINT4 *) args[0]));
            break;
        case CLI_PB_CVLAN_STATUS:

            i4RetStatus =
                VlanPbSetPortCustomerVlanStatus (CliHandle,
                                                 (INT4) u2LocalPortId,
                                                 CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_PB_SVLAN_CLASS:
        case CLI_PB_NO_SVLAN_CLASS:

            if (u4Command == CLI_PB_SVLAN_CLASS)
            {
                u4Action = CLI_ADD;
            }
            else
            {
                u4Action = CLI_DELETE;
            }
            u4SVlanClass = CLI_PTR_TO_U4 (args[0]);
            switch (u4SVlanClass)
            {
                case CLI_PB_SRCMAC:
                    StrToMac ((UINT1 *) args[2], au1InMacAddr);
                    i4RetStatus =
                        VlanPbSVlanClassSrcMacBase (CliHandle,
                                                    (INT4) u2LocalPortId,
                                                    *(UINT4 *) args[1],
                                                    au1InMacAddr, u4Action);
                    break;
                case CLI_PB_SRCMAC_CVLAN:
                    StrToMac ((UINT1 *) args[3], au1InMacAddr);
                    i4RetStatus =
                        VlanPbSVlanClassSrcMacCVlanBase (CliHandle,
                                                         (INT4) u2LocalPortId,
                                                         *(UINT4 *) args[1],
                                                         *(UINT4 *) args[2],
                                                         au1InMacAddr,
                                                         u4Action);

                    break;
                case CLI_PB_DSTMAC:
                    StrToMac ((UINT1 *) args[2], au1InMacAddr);
                    i4RetStatus =
                        VlanPbSVlanClassDstMacBase (CliHandle,
                                                    (INT4) u2LocalPortId,
                                                    *(UINT4 *) args[1],
                                                    au1InMacAddr, u4Action);
                    break;
                case CLI_PB_DSTMAC_CVLAN:
                    StrToMac ((UINT1 *) args[3], au1InMacAddr);
                    i4RetStatus =
                        VlanPbSVlanClassDstMacCVlanBase (CliHandle,
                                                         (INT4) u2LocalPortId,
                                                         *(UINT4 *) args[1],
                                                         *(UINT4 *) args[2],
                                                         au1InMacAddr,
                                                         u4Action);
                    break;
                case CLI_PB_DSCP:
                    i4RetStatus =
                        VlanPbSVlanClassDSCPBase (CliHandle,
                                                  (INT4) u2LocalPortId,
                                                  *(UINT4 *) args[1],
                                                  *(UINT4 *) args[2], u4Action);
                    break;
                case CLI_PB_DSCP_CVLAN:
                    i4RetStatus =
                        VlanPbSVlanClassDSCPCVlanBase (CliHandle,
                                                       (INT4) u2LocalPortId,
                                                       *(UINT4 *) args[1],
                                                       *(UINT4 *) args[2],
                                                       *(UINT4 *) args[3],
                                                       u4Action);

                    break;
                case CLI_PB_SRCIP:
                    i4RetStatus =
                        VlanPbSVlanClassSrcIPBase (CliHandle,
                                                   (INT4) u2LocalPortId,
                                                   *(UINT4 *) args[1],
                                                   *(UINT4 *) args[2],
                                                   u4Action);
                    break;
                case CLI_PB_SRCIP_DSTIP:
                    i4RetStatus =
                        VlanPbSVlanClassSrcIPDstIPBase (CliHandle,
                                                        (INT4) u2LocalPortId,
                                                        *(UINT4 *) args[1],
                                                        *(UINT4 *) args[2],
                                                        *(UINT4 *) args[3],
                                                        u4Action);
                    break;
                case CLI_PB_DSTIP:
                    i4RetStatus =
                        VlanPbSVlanClassDstIPBase (CliHandle,
                                                   (INT4) u2LocalPortId,
                                                   *(UINT4 *) args[1],
                                                   *(UINT4 *) args[2],
                                                   u4Action);
                    break;
                case CLI_PB_DSTIP_CVLAN:
                    i4RetStatus =
                        VlanPbSVlanClassDstIPCVlanBase (CliHandle,
                                                        (INT4) u2LocalPortId,
                                                        *(UINT4 *) args[1],
                                                        *(UINT4 *) args[2],
                                                        *(UINT4 *) args[3],
                                                        u4Action);
                    break;

            }
            break;

        case CLI_PB_PORT_LEARNING_STATUS:
            i4RetStatus =
                VlanPbSetPortUnicastLearningStatus (CliHandle,
                                                    (INT4) u2LocalPortId,
                                                    CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_PB_PORT_SERVICE_REGEN_PRIORITY:
            i4RetStatus = VlanPbSetCNPServiceRegenPriority (CliHandle,
                                                            (INT4)
                                                            u2LocalPortId,
                                                            *(UINT4 *) args[0],
                                                            *(INT4 *) args[1],
                                                            *(INT4 *) args[2]);
            break;

        case CLI_PB_PORT_NO_SERVICE_REGEN_PRIORITY:
            /*The default value of regenerated Priority will always be same
             * as the received priority*/
            i4RetStatus = VlanPbSetCNPServiceRegenPriority (CliHandle,
                                                            (INT4)
                                                            u2LocalPortId,
                                                            *(UINT4 *) args[0],
                                                            *(INT4 *) args[1],
                                                            *(INT4 *) args[1]);
            break;

        case CLI_PB_PORT_PCP_DECODE:
            i4RetStatus = VlanPbSetPcpDecodingTable (CliHandle,
                                                     (INT4) u2LocalPortId,
                                                     CLI_PTR_TO_I4 (args[0]),
                                                     *(INT4 *) args[1],
                                                     *(INT4 *) args[2],
                                                     CLI_PTR_TO_I4 (args[3]));

            break;

        case CLI_PB_PORT_NO_PCP_DECODE:
            i4RetStatus = VlanPbResetPcpDecodingTable (CliHandle,
                                                       (INT4) u2LocalPortId,
                                                       CLI_PTR_TO_I4 (args[0]));

            break;

        case CLI_PB_PORT_NO_PCP_ENCODE:
            i4RetStatus = VlanPbResetPcpEncodingTable (CliHandle,
                                                       (INT4) u2LocalPortId,
                                                       CLI_PTR_TO_I4 (args[0]));

            break;

        case CLI_PB_PORT_PCP_ENCODE:
            i4RetStatus = VlanPbSetPcpEncodingTable (CliHandle,
                                                     (INT4) u2LocalPortId,
                                                     CLI_PTR_TO_I4 (args[0]),
                                                     *(INT4 *) args[1],
                                                     *(INT4 *) args[2],
                                                     CLI_PTR_TO_I4 (args[3]));

            break;

        case CLI_PB_SET_VLAN_SERVICE_TYPE:
            i4Val = CLI_GET_VLANID ();

            if (i4Val == CLI_ERROR)
            {
                return CLI_FAILURE;
            }

            VlanId = (tVlanId) i4Val;

            if (VLAN_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
            {
                CliPrintf (CliHandle, "\r%% Bridge is in Customer Mode\r\n");
                VlanReleaseContext ();
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            i4RetStatus = VlanPbSetVlanServiceType (CliHandle,
                                                    VlanId,
                                                    CLI_PTR_TO_I4 (args[0]));

            break;

        case CLI_VLAN_PB_L2PROTOCOL_TUNNEL:
            VlanId = (tVlanId) CLI_GET_VLANID ();
            i4RetStatus = VlanPbSetL2ProtocolTunnelStatusPerVLAN (CliHandle,
                                                                  u4ContextId,
                                                                  VlanId,
                                                                  CLI_PTR_TO_U4
                                                                  (args[0]),
                                                                  VLAN_TUNNEL_PROTOCOL_TUNNEL);
            break;
        case CLI_VLAN_PB_L2PROTOCOL_PEER:
            VlanId = (tVlanId) CLI_GET_VLANID ();
            i4RetStatus = VlanPbSetL2ProtocolTunnelStatusPerVLAN (CliHandle,
                                                                  u4ContextId,
                                                                  VlanId,
                                                                  CLI_PTR_TO_U4
                                                                  (args[0]),
                                                                  VLAN_TUNNEL_PROTOCOL_PEER);
            break;
        case CLI_VLAN_PB_L2PROTOCOL_DISCARD:
            VlanId = (tVlanId) CLI_GET_VLANID ();
            i4RetStatus = VlanPbSetL2ProtocolTunnelStatusPerVLAN (CliHandle,
                                                                  u4ContextId,
                                                                  VlanId,
                                                                  CLI_PTR_TO_U4
                                                                  (args[0]),
                                                                  VLAN_TUNNEL_PROTOCOL_DISCARD);
            break;

        case CLI_PB_PORT_REQ_DROP_ENC:
            i4RetStatus = VlanPbSetPortReqDropEncoding (CliHandle,
                                                        (INT4) u2LocalPortId,
                                                        CLI_PTR_TO_I4 (args
                                                                       [0]));
            break;

        case CLI_PB_PORT_PCP_SEL_ROW:
            i4RetStatus = VlanPbSetPortPcpSelRow (CliHandle,
                                                  (INT4) u2LocalPortId,
                                                  CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_PB_PORT_USE_DEI:
            i4RetStatus = VlanPbSetPortUseDei (CliHandle, (INT4) u2LocalPortId,
                                               CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_PB_PORT_MAC_LIMIT:
            i4RetStatus =
                VlanPbSetPortUnicastLimit (CliHandle, (INT4) u2LocalPortId,
                                           *(UINT4 *) args[0]);
            break;
        case CLI_PB_NO_PORT_MAC_LIMIT:
            i4RetStatus =
                VlanPbSetPortUnicastLimit (CliHandle, (INT4) u2LocalPortId,
                                           PB_DEFAULT_PORT_MAC_LIMIT);
            break;
        case CLI_PB_PORT_ETHERTYPE:
            i4RetStatus =
                VlanPbSetPortEtherType (CliHandle, (INT4) u2LocalPortId,
                                        CLI_PTR_TO_U4 (args[0]),
                                        *(UINT4 *) args[1]);
            break;
        case CLI_PB_NO_PORT_ETHERTYPE:
            i4RetStatus =
                VlanPbSetPortEtherType (CliHandle, (INT4) u2LocalPortId,
                                        CLI_PTR_TO_U4 (args[0]),
                                        PB_DEFAULT_PORT_ETHERTYPE);
            break;
        case CLI_PB_PORT_ETHERTYPE_SWAP:
            i4RetStatus =
                VlanPbPortEtherTypeSwapStatus (CliHandle, (INT4) u2LocalPortId,
                                               CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_PB_PORT_SVLAN_SWAP:
            i4RetStatus =
                VlanPbPortSVlanSwapStatus (CliHandle, (INT4) u2LocalPortId,
                                           CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_PB_PORT_RELAY_ETHERTYPE:
            i4RetStatus =
                VlanPbPortRelayEthertype (CliHandle, (INT4) u2LocalPortId,
                                          *(UINT4 *) args[0],
                                          *(UINT4 *) args[1]);
            break;
        case CLI_PB_PORT_NO_RELAY_ETHERTYPE:
            i4RetStatus =
                VlanPbDeletePortRelayEthertype (CliHandle, (INT4) u2LocalPortId,
                                                *(UINT4 *) args[0]);
            break;

        case CLI_PB_PORT_RELAY_VLANID:
            i4RetStatus =
                VlanPbPortRelayVlanID (CliHandle, (INT4) u2LocalPortId,
                                       *(UINT4 *) args[0], *(UINT4 *) args[1]);
            break;
        case CLI_PB_PORT_NO_RELAY_VLAN:
            i4RetStatus =
                VlanPbDeletePortRelayVlanID (CliHandle, (INT4) u2LocalPortId,
                                             *(UINT4 *) args[0]);
            break;
        case CLI_PB_CLASS_TYPE:
            i4RetStatus =
                VlanPbSetClassType (CliHandle, (INT4) u2LocalPortId,
                                    CLI_PTR_TO_U4 (args[0]));
            break;
        case CLI_PB_NO_CLASS_TYPE:

            i4RetStatus = VlanPbSetClassType (CliHandle, (INT4) u2LocalPortId,
                                              CLI_PB_PVID);

            break;

        case CLI_PB_DOT1X_TUNNEL_ADDRESS:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetDot1xTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_LACP_TUNNEL_ADDRESS:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetLacpTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_STP_TUNNEL_ADDRESS:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetStpTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_GVRP_TUNNEL_ADDRESS:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetGvrpTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_GMRP_TUNNEL_ADDRESS:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetGmrpTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_MVRP_TUNNEL_ADDRESS:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetMvrpTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_MMRP_TUNNEL_ADDRESS:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetMmrpTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_EOAM_TUNNEL_ADDRESS:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetEoamTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_ECFM_TUNNEL_ADDRESS:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetEcfmTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_ELMI_TUNNEL_ADDRESS:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetElmiTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_LLDP_TUNNEL_ADDRESS:

            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetLldpTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_IGMP_TUNNEL_ADDRESS:
            StrToMac ((UINT1 *) args[0], au1InMacAddr);
            i4RetStatus = VlanPbSetIgmpTunnelAddress (CliHandle, au1InMacAddr);
            break;

        case CLI_PB_NO_L2_TUNNEL_ADDRESS:

            u4Protocol = CLI_PTR_TO_U4 (args[0]);
            MEMSET (au1InMacAddr, 0, sizeof (tMacAddr));
            switch (u4Protocol)
            {
                case CLI_PB_L2_PROTO_DOT1X:
                    MEMCPY (au1InMacAddr, gVlanProviderDot1xAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetDot1xTunnelAddress (CliHandle, au1InMacAddr);
                    break;

                case CLI_PB_L2_PROTO_LACP:
                    MEMCPY (au1InMacAddr, gVlanProviderLacpAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetLacpTunnelAddress (CliHandle, au1InMacAddr);
                    break;

                case CLI_PB_L2_PROTO_STP:
                    MEMCPY (au1InMacAddr, gVlanProviderStpAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetStpTunnelAddress (CliHandle, au1InMacAddr);
                    break;

                case CLI_PB_L2_PROTO_GVRP:
                    MEMCPY (au1InMacAddr, gVlanProviderGvrpAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetGvrpTunnelAddress (CliHandle, au1InMacAddr);
                    break;

                case CLI_PB_L2_PROTO_MVRP:
                    MEMCPY (au1InMacAddr, gVlanProviderMvrpAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetMvrpTunnelAddress (CliHandle, au1InMacAddr);
                    break;

                case CLI_PB_L2_PROTO_GMRP:
                    MEMCPY (au1InMacAddr, gVlanProviderGmrpAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetGmrpTunnelAddress (CliHandle, au1InMacAddr);
                    break;

                case CLI_PB_L2_PROTO_MMRP:
                    MEMCPY (au1InMacAddr, gVlanProviderMmrpAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetMmrpTunnelAddress (CliHandle, au1InMacAddr);
                    break;

                case CLI_PB_L2_PROTO_IGMP:
                    MEMCPY (au1InMacAddr, gVlanProviderIgmpAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetIgmpTunnelAddress (CliHandle, au1InMacAddr);
                    break;

                case CLI_PB_L2_PROTO_ELMI:
                    MEMCPY (au1InMacAddr, gVlanProviderElmiAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetElmiTunnelAddress (CliHandle, au1InMacAddr);
                    break;

                case CLI_PB_L2_PROTO_LLDP:
                    MEMCPY (au1InMacAddr, gVlanProviderLldpAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetLldpTunnelAddress (CliHandle, au1InMacAddr);
                    break;

                case CLI_PB_L2_PROTO_ECFM:
                    MEMCPY (au1InMacAddr, gVlanProviderEcfmAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetEcfmTunnelAddress (CliHandle, au1InMacAddr);
                    break;

                case CLI_PB_L2_PROTO_EOAM:
                    MEMCPY (au1InMacAddr, gVlanProviderEoamAddr,
                            sizeof (tMacAddr));
                    i4RetStatus =
                        VlanPbSetEoamTunnelAddress (CliHandle, au1InMacAddr);
                    break;
            }
            break;

        case CLI_PB_L2PROTOCOL_TUNNEL:
            i4RetStatus =
                VlanPbSetPbL2ProtocolTunnel (CliHandle, (INT4) u2LocalPortId,
                                             CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_PB_L2PROTOCOL_PEER:
            i4RetStatus =
                VlanPbSetPbL2ProtocolPeer (CliHandle, (INT4) u2LocalPortId,
                                           CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_PB_L2PROTOCOL_DISCARD:
            i4RetStatus =
                VlanPbSetPbL2ProtocolDiscard (CliHandle, (INT4) u2LocalPortId,
                                              CLI_PTR_TO_U4 (args[0]));

            break;

        case CLI_PB_L2PROTOCOL_DEFAULT:
            i4RetStatus =
                VlanPbSetPbL2ProtocolDefault (CliHandle, (INT4) u2LocalPortId,
                                              CLI_PTR_TO_U4 (args[0]));

            break;

        case CLI_PB_L2PROTOCOL_OVERRIDE:
            i4RetStatus =
                VlanPbSetPbL2ProtocolOverride (CliHandle, (INT4) u2LocalPortId,
                                               CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_PB_SET_PEP_PVID:
            i4RetStatus =
                VlanPbSetPepPvid (CliHandle, (INT4) u2LocalPortId,
                                  *(INT4 *) args[0], *(INT4 *) args[1]);
            break;

        case CLI_PB_SET_PEP_ACCPT_FRAME_TYPE:
            i4RetStatus =
                VlanPbSetPepAccpFrameType (CliHandle, (INT4) u2LocalPortId,
                                           (tVlanId) (*(UINT4 *) args[0]),
                                           CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_PB_CVID_REG_CONFIG:
            i4RetStatus =
                VlanPbConfCvidRegEntry (CliHandle, (INT4) u2LocalPortId,
                                        *(UINT4 *) args[0],
                                        *(UINT4 *) args[1],
                                        CLI_PTR_TO_U4 (args[2]),
                                        CLI_PTR_TO_U4 (args[3]),
                                        CLI_PTR_TO_I4 (args[4]));

            break;

        case CLI_PB_CVID_REG_DESTROY:
            i4RetStatus =
                VlanPbDelCvidRegEntry (CliHandle, (INT4) u2LocalPortId,
                                       *(UINT4 *) args[0]);
            break;

        case CLI_PB_SVLAN_PRIORITY_CONFIG:
            i4RetStatus =
                VlanPbConfSVlanPriority (CliHandle, (INT4) u2LocalPortId,
                                         CLI_PTR_TO_U4 (args[0]),
                                         CLI_PTR_TO_U4 (args[1]),
                                         CLI_PTR_TO_U4 (args[2]));
            break;

        case CLI_PB_SET_PEP_INGRESS_FILTERING:
            i4RetStatus =
                VlanPbSetPepIngressFiltering (CliHandle, (INT4) u2LocalPortId,
                                              (tVlanId) (*(UINT4 *) args[0]),
                                              CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_PB_SET_PEP_DEF_USER_PRIO:
            i4RetStatus =
                VlanPbSetPepDefUserPriority (CliHandle, (INT4) u2LocalPortId,
                                             (tVlanId) (*(UINT4 *) args[0]),
                                             *(INT4 *) args[1]);
            break;

        case CLI_PB_SET_PEP_COS_PRESERVATION:
            i4RetStatus =
                VlanPbSetPepCosPreservation (CliHandle, (INT4) u2LocalPortId,
                                             (tVlanId) (*(UINT4 *) args[0]),
                                             CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_PB_RESET_PEP_ACCPT_FRAME_TYPE:
            i4RetStatus =
                VlanPbSetPepAccpFrameType (CliHandle, (INT4) u2LocalPortId,
                                           (tVlanId) (*(UINT4 *) args[0]),
                                           VLAN_ADMIT_ALL_FRAMES);
            break;

        case CLI_PB_RESET_PEP_INGRESS_FILTERING:
            i4RetStatus =
                VlanPbSetPepIngressFiltering (CliHandle, (INT4) u2LocalPortId,
                                              (tVlanId) (*(UINT4 *) args[0]),
                                              VLAN_ENABLED);
            break;

        case CLI_PB_RESET_PEP_DEF_USER_PRIO:
            i4RetStatus =
                VlanPbSetPepDefUserPriority (CliHandle, (INT4) u2LocalPortId,
                                             (tVlanId) (*(UINT4 *) args[0]),
                                             VLAN_DEF_USER_PRIORITY);
            break;

        case CLI_PB_RESET_PEP_COS_PRESERVATION:
            i4RetStatus =
                VlanPbSetPepCosPreservation (CliHandle, (INT4) u2LocalPortId,
                                             (tVlanId) (*(UINT4 *) args[0]),
                                             VLAN_DISABLED);
            break;

        case CLI_PB_BRIDGE_MODE:
            /* args[0] - Bridge Mode */
            i4RetStatus = VlanPbSetBridgeMode (CliHandle,
                                               CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_PB_TUNNEL_BPDU_PRIORITY:
            /* args[0] - Tunnelled BPDU Priority */
            i4RetStatus = VlanPbSetTunnelBpduPriority (CliHandle,
                                                       *((INT4 *) args[0]));
            break;

        case CLI_PB_NO_TUNNEL_BPDU_PRIORITY:

            i4RetStatus =
                VlanPbSetTunnelBpduPriority (CliHandle,
                                             VLAN_DEF_TUNNEL_BPDU_PRIORITY);
            break;

        case CLI_PB_PORT_TUNNEL_STATUS:

            i4RetStatus = VlanPbSetPortTunnelStatus (CliHandle,
                                                     (UINT4) u2LocalPortId,
                                                     VLAN_ENABLED);
            break;

        case CLI_PB_NO_PORT_TUNNEL_STATUS:
            i4RetStatus = VlanPbSetPortTunnelStatus (CliHandle,
                                                     (UINT4) u2LocalPortId,
                                                     VLAN_DISABLED);
            break;

        case CLI_PB_CLR_L2_TUNNEL_COUNTERS:

            if (u4IfIndex != 0)
            {
                if (VlanGetContextInfoFromIfIndex
                    ((UINT4) u4IfIndex, &u4TmpContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Port not mapped to any of the context \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                if (u4TmpContextId != u4ContextId)
                {
                    CliPrintf (CliHandle,
                               "\r%% Port not mapped to current context \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                u2Port = u2LocalPortId;
            }

            if (args[0] != NULL)
            {
                VlanId = (tVlanId) * args[0];
                if (VlanId > VLAN_MAX_VLAN_ID || VlanId <= 0)
                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");
                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }
            }

            i4RetStatus = VlanPbClearL2TunnelCounters (CliHandle,
                                                       (UINT4) u2Port, VlanId);
            break;

        case CLI_PB_CLR_L2_DISCARD_COUNTERS:

            if (u4IfIndex != 0)
            {
                if (VlanGetContextInfoFromIfIndex
                    ((UINT4) u4IfIndex, &u4TmpContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Port not mapped to any of the context \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                if (u4TmpContextId != u4ContextId)
                {
                    CliPrintf (CliHandle,
                               "\r%% Port not mapped to current context \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                u2Port = u2LocalPortId;
            }

            if (args[0] != NULL)
            {
                VlanId = (tVlanId) * args[0];
                if (VlanId > VLAN_MAX_VLAN_ID || VlanId <= 0)
                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");
                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }
            }

            i4RetStatus = VlanPbClearL2DiscardCounters (CliHandle,
                                                        (UINT4) u2Port, VlanId);
            break;

        case CLI_PB_CLR_L2_COUNTERS:

            if (u4IfIndex != 0)
            {
                if (VlanGetContextInfoFromIfIndex
                    ((UINT4) u4IfIndex, &u4TmpContextId,
                     &u2LocalPortId) != VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Port not mapped to any of the context \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                if (u4TmpContextId != u4ContextId)
                {
                    CliPrintf (CliHandle,
                               "\r%% Port not mapped to current context \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                u2Port = u2LocalPortId;
            }

            if (args[0] != NULL)
            {
                VlanId = (tVlanId) * args[0];
                if (VlanId > VLAN_MAX_VLAN_ID || VlanId <= 0)
                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");
                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }
            }
            i4RetStatus = VlanPbClearL2TunnelCounters (CliHandle,
                                                       (UINT4) u2Port, VlanId);

            i4RetStatus = VlanPbClearL2DiscardCounters (CliHandle,
                                                        (UINT4) u2Port, VlanId);
            break;

        case CLI_PB_CUSTOMER_VLAN_STATUS:

            if (args[0] != NULL)
            {
                VlanId = (tVlanId) * args[0];
                if (VlanId > VLAN_DEV_MAX_CUSTOMER_VLAN_ID || VlanId <= 0)
                {
                    CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                               "specified.\r\n");
                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }
            }
            i4RetStatus =
                VlanPbSetCounterStatus (CliHandle, (UINT4) u2LocalPortId,
                                        VlanId, CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_PB_CLEAR_CVLAN_STATS:

            if (VlanPbCheckSystemStatus (CliHandle,
                                         u4ContextId) != VLAN_SUCCESS)
            {
                break;
            }

            if (args[0] != NULL)
            {
                VlanId = (INT4) CLI_PTR_TO_U4 (args[0]);
                if (VlanId > VLAN_DEV_MAX_CUSTOMER_VLAN_ID || VlanId <= 0)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Customer-VLAN range "
                               "specified.\r\n");
                    VLAN_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }
            }
            i4RetStatus =
                VlanPbClearCVlanStat (CliHandle, u4ContextId, u4IfIndex,
                                      VlanId);

            break;

        case CLI_PB_CEP_UNTAG_EGRESS_STATUS:

            i4RetStatus =
                VlanPbSetCEPPortEgressUntaggedStatus (CliHandle,
                                                      (INT4) u2LocalPortId,
                                                      CLI_PTR_TO_U4 (args[0]));
            break;

        default:
            /* Given command does not match with any SET command. */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            VlanReleaseContext ();
            VLAN_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    VlanReleaseContext ();

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_VLAN_PB) &&
            (u4ErrCode < CLI_PB_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s",
                       PbCliErrString[CLI_ERR_OFFSET_VLAN_PB (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    VLAN_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;

}

INT4
cli_process_pb_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[VLAN_MAX_ARGS];
    INT1                argno = 0;
    UINT1               u1Variable;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4Inst = 0;
    UINT4               u4ErrCode;
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4TempContextId = VLAN_CLI_INVALID_CONTEXT;
    UINT1              *pu1ContextName = NULL;
    UINT2               u2LocalPortId = 0;
    tVlanId             VlanId = 0;

    CliRegisterLock (CliHandle, VlanLock, VlanUnLock);
    VLAN_LOCK ();

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    i4Inst = (INT4) va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     * After the u4IfIndex. (ie) In all the cli commands we are passing
     * IfIndex as the first argument in variable argument list. Like that
     * as the second argument we have to pass context-name*/
    pu1ContextName = va_arg (ap, UINT1 *);

    /*remaining arguments */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == VLAN_MAX_ARGS)
            break;
    }
    va_end (ap);

    while (VlanCliGetContextForShowCmd
           (CliHandle, pu1ContextName, u4IfIndex,
            u4TempContextId, &u4ContextId, &u2LocalPortId) == VLAN_SUCCESS)
    {
        switch (u4Command)
        {
            case CLI_PB_SHOW_SVLAN:

                if (VlanPbCheckSystemStatus (CliHandle,
                                             u4ContextId) != VLAN_SUCCESS)
                {
                    break;
                }

                i4RetStatus = VlanPbShowSVlanConf (CliHandle, u4ContextId,
                                                   CLI_PTR_TO_U4 (args[0]));
                break;

            case CLI_PB_SHOW_SVLAN_MAPPING:

                if (VlanPbCheckSystemStatus (CliHandle, u4ContextId)
                    != VLAN_SUCCESS)
                {
                    break;
                }

                i4RetStatus = VlanPbShowSVlanMapping (CliHandle, u4ContextId,
                                                      (INT4) u4IfIndex);
                break;

            case CLI_PB_SHOW_ETHERTYPE_MAPPING:

                if (VlanPbCheckSystemStatus (CliHandle, u4ContextId)
                    != VLAN_SUCCESS)
                {
                    break;
                }

                i4RetStatus =
                    VlanPbShowEtherTypeMapping (CliHandle,
                                                u4ContextId, u4IfIndex);
                break;

            case CLI_PB_SHOW_TUNNEL_MAC_ADDRESS:

                i4RetStatus = VlanPbShowTunnelMacAddress (CliHandle,
                                                          u4ContextId);
                break;

            case CLI_PB_SHOW_PEP_CONFIG:
            case CLI_PB_SHOW_PORT_CONFIG:
            case CLI_PB_SHOW_PORT_PCP_ENCODE:
            case CLI_PB_SHOW_PORT_PCP_DECODE:
            case CLI_PB_SHOW_PORT_PRIORITY_REGEN:

                i4RetStatus = VlanPbShowPortTables (CliHandle, u4Command,
                                                    u4ContextId, u4IfIndex);
                break;

            case CLI_PB_MULTICAST_MAC_LIMIT:

                if (VlanPbCheckSystemStatus (CliHandle, u4ContextId)
                    != VLAN_SUCCESS)
                {
                    break;
                }

                i4RetStatus = VlanPbShowMulticastLimit (CliHandle, u4ContextId);
                break;

            case CLI_PB_SHOW_DOT1Q_TUNNEL:

                i4RetStatus = VlanPbCliShowDot1qTunnelTable (CliHandle,
                                                             u4ContextId,
                                                             u4IfIndex);
                break;

            case CLI_PB_SHOW_L2PROTOCOL_TUNNEL:

                if (args[0] != NULL)
                {
                    /* Summary */
                    u1Variable = VLAN_TRUE;
                }
                else
                {
                    /* Summary */
                    u1Variable = VLAN_FALSE;
                }

                i4RetStatus = VlanPbCliShowL2ProtocolTunnel (CliHandle,
                                                             u4ContextId,
                                                             u4IfIndex,
                                                             u1Variable);
                break;

            case CLI_PB_SHOW_SERVICE_VLAN_TUNNEL:

                if (args[0] != NULL)
                {
                    VlanId = (tVlanId) CLI_PTR_TO_U4 (args[0]);;
                    if (VlanId > VLAN_MAX_VLAN_ID || VlanId <= 0)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                                   "specified.\r\n");
                        VLAN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                i4RetStatus = VlanPbCliShowL2ProtocolServiceVlan (CliHandle,
                                                                  u4ContextId,
                                                                  CLI_PTR_TO_U4
                                                                  (args[1]),
                                                                  VlanId);
                break;

            case CLI_PB_SHOW_DISC_STATS:

                if (args[0] != NULL)
                {
                    VlanId = (tVlanId) CLI_PTR_TO_U4 (args[0]);
                    if (VlanId > VLAN_MAX_VLAN_ID || VlanId <= 0)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid VLAN range "
                                   "specified.\r\n");
                        VLAN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                i4RetStatus = VlanPbCliShowDiscardStats (CliHandle,
                                                         u4IfIndex,
                                                         u4ContextId, VlanId);
                break;
#ifdef PBB_WANTED
            case CLI_PB_PBB_ISID_SHOW:

                i4RetStatus = VlanPbCliShowPBBIsid (CliHandle,
                                                    u4ContextId,
                                                    *((UINT4 *) (VOID *)
                                                      args[0]));
                break;
#endif
            case CLI_PB_SHOW_CVLAN_STATS:

                if (VlanPbCheckSystemStatus (CliHandle,
                                             u4ContextId) != VLAN_SUCCESS)
                {
                    break;
                }
                if (args[0] != NULL)
                {
                    VlanId = (tVlanId) CLI_PTR_TO_U4 (args[0]);
                    if (VlanId > VLAN_DEV_MAX_CUSTOMER_VLAN_ID || VlanId <= 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Invalid Customer-VLAN range "
                                   "specified.\r\n");
                        VLAN_UNLOCK ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                i4RetStatus =
                    VlanPbShowCVlanStat (CliHandle, u4ContextId, u4IfIndex,
                                         VlanId);

                break;

            default:
                /* Given command does not match with any of the SHOW commands */
                CliPrintf (CliHandle, "\r%% Unknown command \r\n");
                VLAN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
        }
        /* If SwitchName or Interface is given as input for show command
         * then we have to come out of the Loop */
        if ((pu1ContextName != NULL) || (u4IfIndex != 0))
        {
            break;
        }
        u4TempContextId = u4ContextId;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_VLAN_PB) &&
            (u4ErrCode < CLI_PB_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s",
                       PbCliErrString[CLI_ERR_OFFSET_VLAN_PB (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    VLAN_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetMulticastMacLimit                        */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Multicast Mac Limit        */
/*                                                                          */
/*     INPUT            : u4PbMacLimit - Multicast Mac Limit Size           */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSetMulticastMacLimit (tCliHandle CliHandle, UINT4 u4PbMacLimit)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPbMulticastMacLimit (&u4ErrCode, u4PbMacLimit) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPbMulticastMacLimit (u4PbMacLimit) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbSetBridgeMode                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the bridge mode.                */
/*                                                                           */
/*     INPUT            : CliHandle        - Context in which the CLI        */
/*                                             command is processed          */
/*                                                                           */
/*                        u4BridgeMode       - CUSTOMER / PROVIDER           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully        */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                 */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbSetBridgeMode (tCliHandle CliHandle, UINT4 u4BridgeMode)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanBridgeMode (&u4ErrCode, u4BridgeMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanBridgeMode (u4BridgeMode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle,
               "\r\n <Information> Changing bridge mode is successful\n\
              Requires system reboot\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbSetTunnelBpduPriority                       */
/*                                                                           */
/*     DESCRIPTION      : This function configures priority of the           */
/*                        STP Bpdus tunneled.                                */
/*                                                                           */
/*     INPUT            : CliHandle          - Context in which the CLI      */
/*                                             command is processed          */
/*                                                                           */
/*                        i4VlanTunnelBpduPri- 0-7                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully        */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                 */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbSetTunnelBpduPriority (tCliHandle CliHandle, INT4 i4VlanTunnelBpduPri)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelBpduPri
        (&u4ErrCode, i4VlanTunnelBpduPri) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelBpduPri (i4VlanTunnelBpduPri) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbSetPortTunnelStatus                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the tunnelling status of a      */
/*                        port.                                              */
/*                                                                           */
/*     INPUT            : CliHandle    - Context in which the CLI            */
/*                                       command is processed                */
/*                                                                           */
/*                        u4Port       - Port which is to be configured      */
/*                                                                           */
/*                        u1Status     - VLAN_ENABLED / VLAN_DISABLED        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully        */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                 */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbSetPortTunnelStatus (tCliHandle CliHandle, UINT4 u4Port, UINT1 u1Status)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelStatus (&u4ErrCode, u4Port,
                                     u1Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelStatus (u4Port, u1Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbCliShowDot1qTunnelTable                      */
/*                                                                           */
/*     DESCRIPTION      : This function display the ports in which           */
/*                        tunneling is enabled                               */
/*                                                                           */
/*     INPUT            : CliHandle    - Context in which the CLI            */
/*                                       command is processed                */
/*                                                                           */
/*                        u4ContextId - Context Identifier                   */
/*                        u4Port       - Port which is to be displayed       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully         */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                  */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbCliShowDot1qTunnelTable (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4Port)
{
    INT4                i4Result = SNMP_SUCCESS;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    INT4                i4TunnelStatus;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1Name[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1IsShowAll = TRUE;

    MEMSET (au1Name, 0, CFA_MAX_PORT_NAME_LENGTH);

    CliPrintf (CliHandle, "\r\n Interface \r\n");

    CliPrintf (CliHandle, " --------- \r\n");

    if (u4Port == VLAN_DISP_ALL_INTERFACES)
    {
        /* Display all entries in the Tunnel Table */
        i4Result =
            VlanGetFirstPortInContext (u4ContextId, (UINT4 *) &i4NextPort);

        if (i4Result == VLAN_FAILURE)
        {
            CliPrintf (CliHandle, "\rNo ports have been configured "
                       "as dot1qtunnel.\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Display Tunnel Protocol info for the given Interface */
        i4Result = nmhValidateIndexInstanceFsMIVlanTunnelTable (u4Port);

        i4NextPort = (INT4) u4Port;

        if (i4Result == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    do
    {
        nmhGetFsMIVlanTunnelStatus (i4NextPort, &i4TunnelStatus);

        if (i4TunnelStatus == VLAN_ENABLED)
        {
            /* 
             * CFA returns OSIX_SUCCESS/FAILURE. Ideally this should not fail.
             * Hence we are not checking for the return value.
             */
            VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1Name);
            u4PagingStatus = CliPrintf (CliHandle, "  %s\r\n", au1Name);
        }

        if (u4Port != VLAN_DISP_ALL_INTERFACES)
        {
            if (i4TunnelStatus == VLAN_DISABLED)
            {
                CliPrintf (CliHandle,
                           " The port hasn't been configured "
                           "as dot1q-tunnel port.\r\n");
            }
            /* Displaying only for a particular interface */
            u1IsShowAll = FALSE;
        }

        i4CurrentPort = i4NextPort;

        if (VlanGetNextPortInContext (u4ContextId, (UINT4) i4CurrentPort,
                                      (UINT4 *) &i4NextPort) == VLAN_FAILURE)
        {
            u1IsShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1IsShowAll = FALSE;
        }
    }
    while (u1IsShowAll == TRUE);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbClearL2TunnelCounters                       */
/*                                                                           */
/*     DESCRIPTION      : This function Clears the L2Protocol Tunnel Counters*/
/*                                                                           */
/*     INPUT            : tCliHandle  CliHandle - Context in which the CLI   */
/*                                             command is processed          */
/*                        UINT4 u4Port       - Port                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully        */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                 */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbClearL2TunnelCounters (tCliHandle CliHandle, UINT4 u4Port, tVlanId VlanId)
{
    UINT4               u4PortNo;
    tVlanId             VlanStartId = 0;
    tVlanPortEntry     *pPortEntry;
    tVlanCurrEntry     *pVlanCurrEntry = NULL;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return CLI_FAILURE;
    }

    if (VlanId)
    {
        pVlanCurrEntry = VLAN_GET_CURR_ENTRY (VlanId);
        if (pVlanCurrEntry == NULL)
        {
            return CLI_FAILURE;
        }
        VLAN_TUNNEL_GVRP_PDUS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_TUNNEL_GMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_TUNNEL_IGMP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_TUNNEL_MVRP_PDUS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_TUNNEL_MMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_TUNNEL_ECFM_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_TUNNEL_GVRP_PDUS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_TUNNEL_GMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_TUNNEL_IGMP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_TUNNEL_MVRP_PDUS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_TUNNEL_MMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_TUNNEL_ECFM_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        return CLI_SUCCESS;
    }
    else if (u4Port == VLAN_DISP_ALL_INTERFACES)
    {
        VlanStartId = VLAN_START_VLAN_INDEX ();
        VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
        {
            pVlanCurrEntry = VlanGetVlanEntry (VlanId);
            if (pVlanCurrEntry == NULL)
            {
                continue;
            }

            VLAN_TUNNEL_GVRP_PDUS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_TUNNEL_GMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_TUNNEL_IGMP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_TUNNEL_MVRP_PDUS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_TUNNEL_MMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_TUNNEL_ECFM_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_TUNNEL_GVRP_PDUS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_TUNNEL_GMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_TUNNEL_IGMP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_TUNNEL_MVRP_PDUS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_TUNNEL_MMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_TUNNEL_ECFM_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        }
    }
    if (u4Port == VLAN_DISP_ALL_INTERFACES)
    {
        /* Clear counters on all ports */
        VLAN_SCAN_PORT_TABLE (u4PortNo)
        {
            pPortEntry = VLAN_GET_PORT_ENTRY (u4PortNo);

            VLAN_TUNNEL_DOT1X_PKTS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_DOT1X_PKTS_TX (pPortEntry) = 0;

            VLAN_TUNNEL_LACP_PKTS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_LACP_PKTS_TX (pPortEntry) = 0;

            VLAN_TUNNEL_STP_BPDUS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_STP_BPDUS_TX (pPortEntry) = 0;

            VLAN_TUNNEL_GVRP_PDUS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_GVRP_PDUS_TX (pPortEntry) = 0;

            VLAN_TUNNEL_GMRP_PKTS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_GMRP_PKTS_TX (pPortEntry) = 0;

            VLAN_TUNNEL_IGMP_PKTS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_IGMP_PKTS_TX (pPortEntry) = 0;

            VLAN_TUNNEL_MVRP_PDUS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_MVRP_PDUS_TX (pPortEntry) = 0;

            VLAN_TUNNEL_MMRP_PKTS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_MMRP_PKTS_TX (pPortEntry) = 0;

            VLAN_TUNNEL_EOAM_PKTS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_EOAM_PKTS_TX (pPortEntry) = 0;

            VLAN_TUNNEL_LLDP_PKTS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_LLDP_PKTS_TX (pPortEntry) = 0;

            VLAN_TUNNEL_ELMI_PKTS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_ELMI_PKTS_TX (pPortEntry) = 0;

            VLAN_TUNNEL_ECFM_PKTS_RX (pPortEntry) = 0;
            VLAN_TUNNEL_ECFM_PKTS_TX (pPortEntry) = 0;

        }
    }
    else
    {
        if (VLAN_IS_PORT_VALID (u4Port) == VLAN_FALSE)
        {
            return CLI_FAILURE;
        }

        pPortEntry = VLAN_GET_PORT_ENTRY (u4Port);

        if (pPortEntry == NULL)
        {
            CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
            return CLI_FAILURE;
        }

        VLAN_TUNNEL_DOT1X_PKTS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_DOT1X_PKTS_TX (pPortEntry) = 0;

        VLAN_TUNNEL_LACP_PKTS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_LACP_PKTS_TX (pPortEntry) = 0;

        VLAN_TUNNEL_STP_BPDUS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_STP_BPDUS_TX (pPortEntry) = 0;

        VLAN_TUNNEL_GVRP_PDUS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_GVRP_PDUS_TX (pPortEntry) = 0;

        VLAN_TUNNEL_GMRP_PKTS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_GMRP_PKTS_TX (pPortEntry) = 0;

        VLAN_TUNNEL_IGMP_PKTS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_IGMP_PKTS_TX (pPortEntry) = 0;

        VLAN_TUNNEL_MVRP_PDUS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_MVRP_PDUS_TX (pPortEntry) = 0;

        VLAN_TUNNEL_MMRP_PKTS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_MMRP_PKTS_TX (pPortEntry) = 0;

        VLAN_TUNNEL_EOAM_PKTS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_EOAM_PKTS_TX (pPortEntry) = 0;

        VLAN_TUNNEL_LLDP_PKTS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_LLDP_PKTS_TX (pPortEntry) = 0;

        VLAN_TUNNEL_ELMI_PKTS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_ELMI_PKTS_TX (pPortEntry) = 0;

        VLAN_TUNNEL_ECFM_PKTS_RX (pPortEntry) = 0;
        VLAN_TUNNEL_ECFM_PKTS_TX (pPortEntry) = 0;

    }

    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbClearL2DiscardCounters                       */
/*                                                                           */
/*     DESCRIPTION      : This function Clears L2Protocol Discard Counters.  */
/*                                                                           */
/*     INPUT            : tCliHandle  CliHandle - Context in which the CLI   */
/*                                             command is processed          */
/*                        UINT4 u4Port       - Port                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully        */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                 */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbClearL2DiscardCounters (tCliHandle CliHandle, UINT4 u4Port,
                              tVlanId VlanId)
{
    UINT4               u4PortNo;
    tVlanId             VlanStartId = 0;
    tVlanPortEntry     *pPortEntry;
    tVlanCurrEntry     *pVlanCurrEntry;

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        return CLI_FAILURE;
    }

    if (VlanId)
    {
        pVlanCurrEntry = VLAN_GET_CURR_ENTRY (VlanId);
        if (pVlanCurrEntry == NULL)
        {
            return CLI_FAILURE;
        }
        VLAN_DISCARD_GVRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_GMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_IGMP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_MVRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_MMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_ECFM_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_ELMI_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_EOAM_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_LLDP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_PTP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_GVRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_GMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_IGMP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_MVRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_MMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_ECFM_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_ELMI_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_EOAM_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_LLDP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        VLAN_DISCARD_PTP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        return CLI_SUCCESS;
    }
    else if (u4Port == VLAN_DISP_ALL_INTERFACES)
    {
        VlanStartId = VLAN_START_VLAN_INDEX ();
        VLAN_SCAN_VLAN_TABLE_FROM_MID (VlanStartId, VlanId)
        {
            pVlanCurrEntry = VlanGetVlanEntry (VlanId);
            if (pVlanCurrEntry == NULL)
            {
                continue;
            }
            VLAN_DISCARD_GVRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_GMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_IGMP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_MVRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_MMRP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_ECFM_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_ELMI_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_EOAM_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_LLDP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_PTP_PKTS_RX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_GVRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_GMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_IGMP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_MVRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_MMRP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_ECFM_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_ELMI_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_EOAM_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_LLDP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
            VLAN_DISCARD_PTP_PKTS_TX_PER_VLAN (pVlanCurrEntry) = 0;
        }
    }

    if (u4Port == VLAN_DISP_ALL_INTERFACES)
    {
        /* Clear counters on all ports */
        VLAN_SCAN_PORT_TABLE (u4PortNo)
        {
            pPortEntry = VLAN_GET_PORT_ENTRY (u4PortNo);

            VLAN_DISCARD_DOT1X_PKTS_RX (pPortEntry) = 0;
            VLAN_DISCARD_DOT1X_PKTS_TX (pPortEntry) = 0;

            VLAN_DISCARD_LACP_PKTS_RX (pPortEntry) = 0;
            VLAN_DISCARD_LACP_PKTS_TX (pPortEntry) = 0;

            VLAN_DISCARD_STP_BPDUS_RX (pPortEntry) = 0;
            VLAN_DISCARD_STP_BPDUS_TX (pPortEntry) = 0;

            VLAN_DISCARD_GVRP_PKTS_RX (pPortEntry) = 0;
            VLAN_DISCARD_GVRP_PKTS_TX (pPortEntry) = 0;

            VLAN_DISCARD_GMRP_PKTS_RX (pPortEntry) = 0;
            VLAN_DISCARD_GMRP_PKTS_TX (pPortEntry) = 0;

            VLAN_DISCARD_IGMP_PKTS_RX (pPortEntry) = 0;
            VLAN_DISCARD_IGMP_PKTS_TX (pPortEntry) = 0;

            VLAN_DISCARD_MVRP_PKTS_RX (pPortEntry) = 0;
            VLAN_DISCARD_MVRP_PKTS_TX (pPortEntry) = 0;

            VLAN_DISCARD_MMRP_PKTS_RX (pPortEntry) = 0;
            VLAN_DISCARD_MMRP_PKTS_TX (pPortEntry) = 0;

            VLAN_DISCARD_EOAM_PKTS_RX (pPortEntry) = 0;
            VLAN_DISCARD_EOAM_PKTS_TX (pPortEntry) = 0;

            VLAN_DISCARD_LLDP_PKTS_RX (pPortEntry) = 0;
            VLAN_DISCARD_LLDP_PKTS_TX (pPortEntry) = 0;

            VLAN_DISCARD_ELMI_PKTS_RX (pPortEntry) = 0;
            VLAN_DISCARD_ELMI_PKTS_TX (pPortEntry) = 0;

            VLAN_DISCARD_ECFM_PKTS_RX (pPortEntry) = 0;
            VLAN_DISCARD_ECFM_PKTS_TX (pPortEntry) = 0;

        }
    }
    else
    {
        if (VLAN_IS_PORT_VALID (u4Port) == VLAN_FALSE)
        {
            return CLI_FAILURE;
        }

        pPortEntry = VLAN_GET_PORT_ENTRY (u4Port);

        if (pPortEntry == NULL)
        {
            CLI_SET_ERR (CLI_VLAN_INVALID_PORT_STATUS_ERR);
            return CLI_FAILURE;
        }

        VLAN_DISCARD_DOT1X_PKTS_RX (pPortEntry) = 0;
        VLAN_DISCARD_DOT1X_PKTS_TX (pPortEntry) = 0;

        VLAN_DISCARD_LACP_PKTS_RX (pPortEntry) = 0;
        VLAN_DISCARD_LACP_PKTS_TX (pPortEntry) = 0;

        VLAN_DISCARD_STP_BPDUS_RX (pPortEntry) = 0;
        VLAN_DISCARD_STP_BPDUS_TX (pPortEntry) = 0;

        VLAN_DISCARD_GVRP_PKTS_RX (pPortEntry) = 0;
        VLAN_DISCARD_GVRP_PKTS_TX (pPortEntry) = 0;

        VLAN_DISCARD_GMRP_PKTS_RX (pPortEntry) = 0;
        VLAN_DISCARD_GMRP_PKTS_TX (pPortEntry) = 0;

        VLAN_DISCARD_IGMP_PKTS_RX (pPortEntry) = 0;
        VLAN_DISCARD_IGMP_PKTS_TX (pPortEntry) = 0;

        VLAN_DISCARD_MVRP_PKTS_RX (pPortEntry) = 0;
        VLAN_DISCARD_MVRP_PKTS_TX (pPortEntry) = 0;

        VLAN_DISCARD_MMRP_PKTS_RX (pPortEntry) = 0;
        VLAN_DISCARD_MMRP_PKTS_TX (pPortEntry) = 0;

        VLAN_DISCARD_EOAM_PKTS_RX (pPortEntry) = 0;
        VLAN_DISCARD_EOAM_PKTS_TX (pPortEntry) = 0;

        VLAN_DISCARD_LLDP_PKTS_RX (pPortEntry) = 0;
        VLAN_DISCARD_LLDP_PKTS_TX (pPortEntry) = 0;

        VLAN_DISCARD_ELMI_PKTS_RX (pPortEntry) = 0;
        VLAN_DISCARD_ELMI_PKTS_TX (pPortEntry) = 0;

        VLAN_DISCARD_ECFM_PKTS_RX (pPortEntry) = 0;
        VLAN_DISCARD_ECFM_PKTS_TX (pPortEntry) = 0;

    }

    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetCustomerVlanId                           */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Customer vlan id for port  */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4CVlanId - Customer Vlan Id                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSetCustomerVlanId (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4CVlanId)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPbPortCVlan (&u4ErrCode, i4PortId, u4CVlanId) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPbPortCVlan (i4PortId, u4CVlanId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetCEPPortEgressUntaggedStatus              */
/*                                                                          */
/*     DESCRIPTION      : This function will set the status to allow/deny   */
/*                        untagged frames on CEP                            */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4Status  - Status (Allow/Deny untagged frames)   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSetCEPPortEgressUntaggedStatus (tCliHandle CliHandle, INT4 i4PortId,
                                      UINT4 u4Status)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPbPortEgressUntaggedStatus (&u4ErrCode, i4PortId,
                                               (INT4) u4Status) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsPbPortEgressUntaggedStatus (i4PortId, (INT4) u4Status) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPortUnicastLearningStatus                */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Unicast Mac Learning status*/
/*                        for Port                                          */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4Status - Enable/Disable status                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSetPortUnicastLearningStatus (tCliHandle CliHandle, INT4 i4PortId,
                                    UINT4 u4Status)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPbPortUnicastMacLearning (&u4ErrCode, i4PortId, u4Status) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPbPortUnicastMacLearning (i4PortId, u4Status) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetCNPServiceRegenPriority                  */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Regenerated priority for a */
/*                       specific priority on a particular internal CNP     */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4VlanId  - service vlan identifier               */
/*                        i4RecvPriority - Received Priority                */
/*                        i4RegenPriority - Regenerated Priority            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSetCNPServiceRegenPriority (tCliHandle CliHandle, INT4 i4PortId,
                                  UINT4 u4VlanId, INT4 i4RecvPriority, INT4
                                  i4RegenPriority)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Dot1adServicePriorityRegenRegeneratedPriority
        (&u4ErrCode, i4PortId, u4VlanId, i4RecvPriority, i4RegenPriority) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1adServicePriorityRegenRegeneratedPriority (i4PortId, u4VlanId,
                                                             i4RecvPriority,
                                                             i4RegenPriority) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPcpDecodingTable                         */
/*                                                                          */
/*     DESCRIPTION      : This function will configure the priority and drop*/
/*                        eligible indicator for recieved PCP on Particular */
/*                        Port in PCP Decoding Table                        */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        i4SelRow - selection row identifier               */
/*                        i4Pcp  - Priority code point                      */
/*                        i4Priority - Configured priority                  */
/*                        i4Dei - Drop eligible indicator                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSetPcpDecodingTable (tCliHandle CliHandle, INT4 i4PortId,
                           INT4 i4SelRow, INT4 i4Pcp, INT4 i4Priority,
                           INT4 i4Dei)
{
    UINT4               u4ErrCode = 0;
    INT4                i4PrevDei = 0;
    INT4                i4PrevPriority = 0;

    /* Explanation for setting the PCP Decoding table.
     * Example:
     * cases where the normal test and set alone will fail:
     * case1: PCP    Priority    DE
     *         4        5         F
     *         5        6         T
     * If we try to config the valid input (PCP: 5, Pri-5, DE-F), then
     * this routine will return failure.
     * Say if test drop eligible is done first and then the test priority, 
     * the following case 2 may fail.
     * case2: PCP    Priority    DE
     *         4        5         F
     *         5        5         F
     * If we try to config the valid input (PCP: 5, Pri-6, DE-T), then
     * this routine will return failure.
     * For taking care of above scenario,this function is implemented
     * as following:
     *     - Step 1: Test drop eligble
     *               If success set drop eligble.
     *               Test priority
     *               If success set priority 
     *       Step 2: If step 1 fails:
     *               Revert the settings done in step 1.
     *               Test priority
     *               If success set priority
     *               Test drop eligble.
     *               If success set drop eligible.
     *       Step 3: If step 2 fails, revert the settings done in step2 and
     *               return failure. Otherwise return success.*/

    if (nmhGetDot1adPcpDecodingDropEligible (i4PortId, i4SelRow, i4Pcp,
                                             &i4PrevDei) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhGetDot1adPcpDecodingPriority (i4PortId, i4SelRow, i4Pcp,
                                         &i4PrevPriority) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*STEP : 1 */

    if (nmhTestv2Dot1adPcpDecodingDropEligible (&u4ErrCode, i4PortId, i4SelRow,
                                                i4Pcp, i4Dei) == SNMP_SUCCESS)
    {
        if (nmhSetDot1adPcpDecodingDropEligible
            (i4PortId, i4SelRow, i4Pcp, i4Dei) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2Dot1adPcpDecodingPriority (&u4ErrCode, i4PortId, i4SelRow,
                                                i4Pcp,
                                                i4Priority) == SNMP_SUCCESS)
        {
            if (nmhSetDot1adPcpDecodingPriority
                (i4PortId, i4SelRow, i4Pcp, i4Priority) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            return CLI_SUCCESS;
        }

        /* Setting in order, drop eligible first and then priority failed.
         * So revert the setting during the operation, and try other
         * order. */
        if (nmhSetDot1adPcpDecodingDropEligible
            (i4PortId, i4SelRow, i4Pcp, i4PrevDei) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /*STEP : 2 */
    if (nmhTestv2Dot1adPcpDecodingPriority (&u4ErrCode, i4PortId, i4SelRow,
                                            i4Pcp, i4Priority) == SNMP_SUCCESS)

    {
        if (nmhSetDot1adPcpDecodingPriority (i4PortId, i4SelRow, i4Pcp,
                                             i4Priority) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2Dot1adPcpDecodingDropEligible (&u4ErrCode, i4PortId,
                                                    i4SelRow, i4Pcp,
                                                    i4Dei) == SNMP_SUCCESS)
        {
            if (nmhSetDot1adPcpDecodingDropEligible (i4PortId, i4SelRow,
                                                     i4Pcp,
                                                     i4Dei) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            return CLI_SUCCESS;
        }

        /* Test failed in second order (priority set first and then
         * drop eligible). So revert the settings. */
        /*Restoring the configured Priority */
        if (nmhSetDot1adPcpDecodingPriority (i4PortId, i4SelRow, i4Pcp,
                                             i4PrevPriority) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        return CLI_FAILURE;
    }

    /*STEP : 3 */
    return CLI_FAILURE;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPcpEncodingTable                         */
/*                                                                          */
/*     DESCRIPTION      : This function will configure the PCP in the       */
/*                        Priority Encoding Table                           */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        i4SelRow - selection row identifier               */
/*                        i4Pcp  - configured Priority code point           */
/*                        i4Priority - priority                             */
/*                        i4Dei - Drop eligible indicator                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetPcpEncodingTable (tCliHandle CliHandle, INT4 i4PortId,
                           INT4 i4SelRow, INT4 i4Priority, INT4 i4Pcp,
                           INT4 i4Dei)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Dot1adPcpEncodingPcpValue
        (&u4ErrCode, i4PortId, i4SelRow, i4Priority, i4Dei,
         i4Pcp) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1adPcpEncodingPcpValue (i4PortId, i4SelRow,
                                         i4Priority, i4Dei,
                                         i4Pcp) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbResetPcpDecodingTable                       */
/*                                                                          */
/*     DESCRIPTION      : This function resets the PCP decoding table values*/
/*                        to default values.                                */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        i4SelRow - selection row identifier               */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbResetPcpDecodingTable (tCliHandle CliHandle, INT4 i4PortId, INT4 i4SelRow)
{
    INT4                i4Priority;
    INT4                i4DropEligible;
    UINT1               u1Pcp;

    for (u1Pcp = 0; u1Pcp < VLAN_PB_MAX_PCP; u1Pcp++)
    {
        VlanPbGetDecodingPriority (i4SelRow, (INT4) u1Pcp, &i4Priority);

        VlanPbGetDecodingDropEligible (i4SelRow, (INT4) u1Pcp, &i4DropEligible);

        if (nmhSetDot1adPcpDecodingPriority (i4PortId, i4SelRow, (INT4) u1Pcp,
                                             i4Priority) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetDot1adPcpDecodingDropEligible
            (i4PortId, i4SelRow, (INT4) u1Pcp, i4DropEligible) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbResetPcpEncodingTable                       */
/*                                                                          */
/*     DESCRIPTION      : This function resets the PCP encoding table values*/
/*                        to default values.                                */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        i4SelRow - selection row identifier               */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbResetPcpEncodingTable (tCliHandle CliHandle, INT4 i4PortId, INT4 i4SelRow)
{
    INT4                i4Pcp;
    UINT1               u1Priority;

    for (u1Priority = 0; u1Priority < VLAN_PB_MAX_PRIORITY; u1Priority++)
    {
        VlanPbGetEncodingPcpVal (i4SelRow, u1Priority, VLAN_DE_FALSE, &i4Pcp);

        if (nmhSetDot1adPcpEncodingPcpValue (i4PortId, i4SelRow, (INT4)
                                             u1Priority, VLAN_SNMP_FALSE,
                                             i4Pcp) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        VlanPbGetEncodingPcpVal (i4SelRow, u1Priority, VLAN_DE_TRUE, &i4Pcp);

        if (nmhSetDot1adPcpEncodingPcpValue (i4PortId, i4SelRow, (INT4)
                                             u1Priority, VLAN_SNMP_TRUE,
                                             i4Pcp) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPortCustomerVlanStatus                   */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Customer vlan status       */
/*                        for Port                                          */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4Status - Enable/Disable status                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSetPortCustomerVlanStatus (tCliHandle CliHandle, INT4 i4PortId,
                                 UINT4 u4Status)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPbPortCVlanClassifyStatus (&u4ErrCode, i4PortId, u4Status) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPbPortCVlanClassifyStatus (i4PortId, u4Status) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPortUnicastLimit                         */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Unicast Mac Limit for Port */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4PortMacLimit - Mac Limit size                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSetPortUnicastLimit (tCliHandle CliHandle, INT4 i4PortId,
                           UINT4 u4PortMacLimit)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPbPortUnicastMacLimit (&u4ErrCode, i4PortId, u4PortMacLimit)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPbPortUnicastMacLimit (i4PortId, u4PortMacLimit) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPortEtherType                            */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Ether Type for Port        */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4Direction- Ingress/Egress direction             */
/*                        u4EtherType - Ether Type to be configured         */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSetPortEtherType (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4Direction,
                        UINT4 u4EtherType)
{
    UINT4               u4ErrCode;
    INT4                i4TmpEtherType = 0;

    i4TmpEtherType = (INT4) u4EtherType;

    if (u4Direction == CLI_PB_INGRESS)
    {
        if (nmhTestv2FsPbPortSVlanIngressEtherType
            (&u4ErrCode, i4PortId, i4TmpEtherType) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbPortSVlanIngressEtherType (i4PortId, i4TmpEtherType)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsPbPortSVlanEgressEtherType
            (&u4ErrCode, i4PortId, i4TmpEtherType) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbPortSVlanEgressEtherType (i4PortId, i4TmpEtherType)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbPortEtherTypeSwapStatus                     */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Ether Type swap status     */
/*                        for Port                                          */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4Status - Enable/Disable status                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbPortEtherTypeSwapStatus (tCliHandle CliHandle, INT4 i4PortId,
                               UINT4 u4Status)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPbPortSVlanEtherTypeSwapStatus
        (&u4ErrCode, i4PortId, u4Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPbPortSVlanEtherTypeSwapStatus (i4PortId, u4Status) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbPortSVlanSwapStatus                         */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Service vlan swap status   */
/*                        for Port                                          */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4Status - Enable/Disable status                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbPortSVlanSwapStatus (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4Status)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPbPortSVlanTranslationStatus (&u4ErrCode, i4PortId, u4Status)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPbPortSVlanTranslationStatus (i4PortId, u4Status) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbPortRelayEthertype                          */
/*                                                                          */
/*     DESCRIPTION      : This function will configure EtherType for        */
/*                        swapping for a VIP                                         */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4EtherType - Local Ether Type                    */
/*                        u4RelayEtherType - Swap Ether Type                */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbPortRelayEthertype (tCliHandle CliHandle, INT4 i4PortId,
                          UINT4 u4EtherType, UINT4 u4RelayEtherType)
{
    UINT4               u4ErrCode;
    INT4                i4FsPbEtherTypeSwapStatus;
    INT4                i4RetFsPbEtherTypeSwapStatus;
    INT4                i4RetStatus;
    INT4                i4TempEtherType = 0;

    i4RetStatus =
        nmhGetFsPbEtherTypeSwapRowStatus (i4PortId, (INT4) u4EtherType,
                                          &i4RetFsPbEtherTypeSwapStatus);

    if (i4RetStatus == SNMP_FAILURE)
    {
        /*Create Entry */
        i4FsPbEtherTypeSwapStatus = (INT4) VLAN_CREATE_AND_WAIT;
    }
    else
    {
        i4RetStatus = nmhGetFsPbRelayEtherType (i4PortId, (INT4) u4EtherType,
                                                &i4TempEtherType);

        if (u4RelayEtherType == (UINT4) i4TempEtherType)
        {
            return CLI_SUCCESS;
        }
        i4FsPbEtherTypeSwapStatus = VLAN_NOT_IN_SERVICE;
    }

    if (nmhTestv2FsPbEtherTypeSwapRowStatus
        (&u4ErrCode, i4PortId, (INT4) u4EtherType,
         i4FsPbEtherTypeSwapStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPbEtherTypeSwapRowStatus
        (i4PortId, (INT4) u4EtherType,
         i4FsPbEtherTypeSwapStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsPbRelayEtherType
        (&u4ErrCode, i4PortId, (INT4) u4EtherType,
         (INT4) u4RelayEtherType) != SNMP_SUCCESS)
    {
        if (i4FsPbEtherTypeSwapStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbEtherTypeSwapRowStatus
                (i4PortId, (INT4) u4EtherType, VLAN_DESTROY) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }
        else if (nmhSetFsPbEtherTypeSwapRowStatus
                 (i4PortId, (INT4) u4EtherType,
                  i4RetFsPbEtherTypeSwapStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        return CLI_FAILURE;
    }
    if (nmhSetFsPbRelayEtherType
        (i4PortId, (INT4) u4EtherType, u4RelayEtherType) != SNMP_SUCCESS)
    {
        if (i4FsPbEtherTypeSwapStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbEtherTypeSwapRowStatus
                (i4PortId, (INT4) u4EtherType, VLAN_DESTROY) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }
        else if (nmhSetFsPbEtherTypeSwapRowStatus
                 (i4PortId, (INT4) u4EtherType,
                  i4RetFsPbEtherTypeSwapStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (i4FsPbEtherTypeSwapStatus == VLAN_CREATE_AND_WAIT)
    {
        if (nmhSetFsPbEtherTypeSwapRowStatus
            (i4PortId, (INT4) u4EtherType, VLAN_ACTIVE) != SNMP_SUCCESS)
        {
            if (nmhSetFsPbEtherTypeSwapRowStatus
                (i4PortId, (INT4) u4EtherType, VLAN_DESTROY) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (nmhSetFsPbEtherTypeSwapRowStatus
             (i4PortId, (INT4) u4EtherType,
              i4RetFsPbEtherTypeSwapStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbDeletePortRelayEthertype                    */
/*                                                                          */
/*     DESCRIPTION      : This function will delete swap EtherType entry    */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4EtherType - Local Ether Type                    */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbDeletePortRelayEthertype (tCliHandle CliHandle, INT4 i4PortId,
                                UINT4 u4EtherType)
{
    UINT4               u4ErrCode;
    INT4                i4TmpEtherType = 0;

    i4TmpEtherType = (INT4) u4EtherType;
    if (nmhTestv2FsPbEtherTypeSwapRowStatus
        (&u4ErrCode, i4PortId, i4TmpEtherType, VLAN_DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPbEtherTypeSwapRowStatus
        (i4PortId, i4TmpEtherType, VLAN_DESTROY) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbPortRelayVlanID                             */
/*                                                                          */
/*     DESCRIPTION      : This function will configure SVlan Id for         */
/*                        swapping                                          */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4VlanId - Local SVlan Id                         */
/*                        u4RelayVlanId - Swap SVlan Id                     */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbPortRelayVlanID (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4VlanId,
                       UINT4 u4RelayVlanId)
{
    UINT4               u4ErrCode;
    INT4                i4Dot1adVidTranslationRowStatus;
    INT4                i4RetDot1adVidTranslationRowStatus;
    INT4                i4RetStatus;
    INT4                i4TempVlanId = 0;
    INT4                i4VlanId = 0;

    i4VlanId = (INT4) u4VlanId;

    i4RetStatus =
        nmhGetDot1adVidTranslationRowStatus (i4PortId, i4VlanId,
                                             &i4RetDot1adVidTranslationRowStatus);

    if (i4RetStatus == SNMP_FAILURE)
    {
        /*Create Entry */
        i4Dot1adVidTranslationRowStatus = VLAN_CREATE_AND_WAIT;
    }
    else
    {
        i4RetStatus = nmhGetDot1adVidTranslationRelayVid (i4PortId, i4VlanId,
                                                          &i4TempVlanId);

        if ((UINT4) i4TempVlanId == u4RelayVlanId)
        {
            return CLI_SUCCESS;
        }
        i4Dot1adVidTranslationRowStatus = VLAN_NOT_IN_SERVICE;
    }

    if (nmhTestv2Dot1adVidTranslationRowStatus
        (&u4ErrCode, i4PortId, i4VlanId,
         i4Dot1adVidTranslationRowStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1adVidTranslationRowStatus
        (i4PortId, i4VlanId, i4Dot1adVidTranslationRowStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2Dot1adVidTranslationRelayVid
        (&u4ErrCode, i4PortId, i4VlanId, u4RelayVlanId) != SNMP_SUCCESS)
    {
        if (i4Dot1adVidTranslationRowStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetDot1adVidTranslationRowStatus
                (i4PortId, i4VlanId, VLAN_DESTROY) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }
        else if (nmhSetDot1adVidTranslationRowStatus
                 (i4PortId, i4VlanId,
                  i4RetDot1adVidTranslationRowStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        return CLI_FAILURE;
    }
    if (nmhSetDot1adVidTranslationRelayVid (i4PortId, i4VlanId, u4RelayVlanId)
        != SNMP_SUCCESS)
    {
        if (i4Dot1adVidTranslationRowStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetDot1adVidTranslationRowStatus
                (i4PortId, i4VlanId, VLAN_DESTROY) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }
        else if (nmhSetDot1adVidTranslationRowStatus
                 (i4PortId, i4VlanId,
                  i4RetDot1adVidTranslationRowStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (i4Dot1adVidTranslationRowStatus == VLAN_CREATE_AND_WAIT)
    {
        if (nmhSetDot1adVidTranslationRowStatus
            (i4PortId, i4VlanId, VLAN_ACTIVE) != SNMP_SUCCESS)
        {
            if (nmhSetDot1adVidTranslationRowStatus
                (i4PortId, i4VlanId, VLAN_DESTROY) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (nmhSetDot1adVidTranslationRowStatus
             (i4PortId, i4VlanId,
              i4RetDot1adVidTranslationRowStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbDeletePortRelayVlanID                       */
/*                                                                          */
/*     DESCRIPTION      : This function will Delete swap SVlan entry        */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4VlanId - Local SVlan Id                         */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbDeletePortRelayVlanID (tCliHandle CliHandle, INT4 i4PortId,
                             UINT4 u4VlanId)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1adVidTranslationRowStatus
        (&u4ErrCode, i4PortId, u4VlanId, VLAN_DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1adVidTranslationRowStatus (i4PortId, u4VlanId, VLAN_DESTROY)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetDot1xTunnelAddress                       */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for Dot1x protocol tunneling.  */
/*                                                                          */
/*     INPUT            : pu1Dot1xAddress  - Dot1x tunnel address           */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetDot1xTunnelAddress (tCliHandle CliHandle, UINT1 *pu1Dot1xAddress)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelDot1xAddress (&u4ErrCode, pu1Dot1xAddress) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelDot1xAddress (pu1Dot1xAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetLacpTunnelAddress                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for LACP protocol tunneling.   */
/*                                                                          */
/*     INPUT            : pu1LacpAddress  - LACP tunnel address             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetLacpTunnelAddress (tCliHandle CliHandle, UINT1 *pu1LacpAddress)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelLacpAddress (&u4ErrCode, pu1LacpAddress) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelLacpAddress (pu1LacpAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetStpTunnelAddress                         */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for STP protocol tunneling.    */
/*                                                                          */
/*     INPUT            : pu1StpAddress  - STP tunnel address               */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetStpTunnelAddress (tCliHandle CliHandle, UINT1 *pu1StpAddress)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelStpAddress (&u4ErrCode, pu1StpAddress) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelStpAddress (pu1StpAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetGvrpTunnelAddress                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for GVRP protocol tunneling.   */
/*                                                                          */
/*     INPUT            : pu1GvrpAddress  - GVRP tunnel address             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetGvrpTunnelAddress (tCliHandle CliHandle, UINT1 *pu1GvrpAddress)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelGvrpAddress (&u4ErrCode, pu1GvrpAddress) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelGvrpAddress (pu1GvrpAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetMvrpTunnelAddress                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for MVRP protocol tunneling.   */
/*                                                                          */
/*     INPUT            : pu1MvrpAddress  - MVRP tunnel address             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetMvrpTunnelAddress (tCliHandle CliHandle, UINT1 *pu1MvrpAddress)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelMvrpAddress (&u4ErrCode, pu1MvrpAddress)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelMvrpAddress (pu1MvrpAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetGmrpTunnelAddress                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for GMRP protocol tunneling.   */
/*                                                                          */
/*     INPUT            : pu1GmrpAddress  - GMRP tunnel address             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetGmrpTunnelAddress (tCliHandle CliHandle, UINT1 *pu1GmrpAddress)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelGmrpAddress (&u4ErrCode, pu1GmrpAddress) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsVlanTunnelGmrpAddress (pu1GmrpAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetMmrpTunnelAddress                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for MMRP protocol tunneling.   */
/*                                                                          */
/*     INPUT            : pu1MmrpAddress  - MMRP tunnel address             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetMmrpTunnelAddress (tCliHandle CliHandle, UINT1 *pu1MmrpAddress)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelMmrpAddress (&u4ErrCode,
                                          pu1MmrpAddress) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelMmrpAddress (pu1MmrpAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetEoamTunnelAddress                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for EOAM protocol tunneling.   */
/*                                                                          */
/*     INPUT            : pu1EoamAddress  - EOAM tunnel address             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetEoamTunnelAddress (tCliHandle CliHandle, UINT1 *pu1EoamAddress)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelEoamAddress (&u4ErrCode,
                                          pu1EoamAddress) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelEoamAddress (pu1EoamAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetEcfmTunnelAddress                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for ECFM protocol tunneling.   */
/*                                                                          */
/*     INPUT            : pu1EcfmAddress  - ECFM tunnel address             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetEcfmTunnelAddress (tCliHandle CliHandle, UINT1 *pu1EcfmAddress)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelEcfmAddress (&u4ErrCode,
                                          pu1EcfmAddress) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelEcfmAddress (pu1EcfmAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetElmiTunnelAddress                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for ELMI protocol tunneling.   */
/*                                                                          */
/*     INPUT            : pu1ElmiAddress  - ELMI tunnel address             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetElmiTunnelAddress (tCliHandle CliHandle, UINT1 *pu1ElmiAddress)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelElmiAddress (&u4ErrCode,
                                          pu1ElmiAddress) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelElmiAddress (pu1ElmiAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetlldpTunnelAddress                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for LLDP protocol tunneling.   */
/*                                                                          */
/*     INPUT            : pu1EoamAddress  - EOAM tunnel address             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetLldpTunnelAddress (tCliHandle CliHandle, UINT1 *pu1LldpAddress)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelLldpAddress (&u4ErrCode,
                                          pu1LldpAddress) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelLldpAddress (pu1LldpAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetIgmpTunnelAddress                        */
/*                                                                          */
/*     DESCRIPTION      : This function configures the destination MAC      */
/*                        address to be used for IGMP protocol tunneling.   */
/*                                                                          */
/*     INPUT            : pu1IgmpAddress  - IGMP tunnel address             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetIgmpTunnelAddress (tCliHandle CliHandle, UINT1 *pu1IgmpAddress)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsVlanTunnelIgmpAddress (&u4ErrCode,
                                          pu1IgmpAddress) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelIgmpAddress (pu1IgmpAddress) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPbL2ProtocolTunnel                       */
/*                                                                          */
/*     DESCRIPTION      : This function enables L2 protocol tunneling on    */
/*                        a given port                                      */
/*                                                                          */
/*     INPUT            : i4PortId  - Port on which tunneling to be enabled */
/*                        uyType    - L2 protocol                           */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetPbL2ProtocolTunnel (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4Type)
{
    UINT4               u4ErrCode;
    switch (u4Type)
    {
        case CLI_PB_L2_PROTO_DOT1X:
            if (nmhTestv2FsVlanTunnelProtocolDot1x (&u4ErrCode, i4PortId,
                                                    VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolDot1x (i4PortId,
                                                 VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_LACP:
            if (nmhTestv2FsVlanTunnelProtocolLacp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolLacp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_STP:
            if (nmhTestv2FsVlanTunnelProtocolStp (&u4ErrCode, i4PortId,
                                                  VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolStp (i4PortId,
                                               VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_GVRP:
            if (nmhTestv2FsVlanTunnelProtocolGvrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolGvrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_MVRP:

            if (nmhTestv2FsVlanTunnelProtocolMvrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolMvrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_GMRP:
            if (nmhTestv2FsVlanTunnelProtocolGmrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolGmrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_MMRP:

            if (nmhTestv2FsVlanTunnelProtocolMmrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolMmrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_IGMP:
            if (nmhTestv2FsVlanTunnelProtocolIgmp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolIgmp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_ELMI:
            if (nmhTestv2FsVlanTunnelProtocolElmi (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolElmi (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_LLDP:
            if (nmhTestv2FsVlanTunnelProtocolLldp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolLldp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_ECFM:
            if (nmhTestv2FsVlanTunnelProtocolEcfm (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolEcfm (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_EOAM:
            if (nmhTestv2FsVlanTunnelProtocolEoam (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_TUNNEL)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolEoam (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_TUNNEL) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPbL2ProtocolPeer                         */
/*                                                                          */
/*     DESCRIPTION      : This function enables L2 protocol peering on      */
/*                        a given port                                      */
/*                                                                          */
/*     INPUT            : i4PortId  - Port on which tunneling to be enabled */
/*                        uyType    - L2 protocol                           */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetPbL2ProtocolPeer (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4Type)
{
    UINT4               u4ErrCode;
    switch (u4Type)
    {
        case CLI_PB_L2_PROTO_DOT1X:
            if (nmhTestv2FsVlanTunnelProtocolDot1x (&u4ErrCode, i4PortId,
                                                    VLAN_TUNNEL_PROTOCOL_PEER)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolDot1x (i4PortId,
                                                 VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_LACP:
            if (nmhTestv2FsVlanTunnelProtocolLacp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolLacp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_STP:
            if (nmhTestv2FsVlanTunnelProtocolStp (&u4ErrCode, i4PortId,
                                                  VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolStp (i4PortId,
                                               VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_GVRP:
            if (nmhTestv2FsVlanTunnelProtocolGvrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolGvrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_MVRP:

            if (nmhTestv2FsVlanTunnelProtocolMvrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolMvrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_GMRP:
            if (nmhTestv2FsVlanTunnelProtocolGmrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolGmrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_MMRP:

            if (nmhTestv2FsVlanTunnelProtocolMmrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolMmrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_IGMP:
            if (nmhTestv2FsVlanTunnelProtocolIgmp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolIgmp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_ELMI:
            if (nmhTestv2FsVlanTunnelProtocolElmi (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolElmi (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_LLDP:
            if (nmhTestv2FsVlanTunnelProtocolLldp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolLldp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_ECFM:
            if (nmhTestv2FsVlanTunnelProtocolEcfm (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolEcfm (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_EOAM:
            if (nmhTestv2FsVlanTunnelProtocolEoam (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolEoam (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_PEER) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPbL2ProtocolDefault                      */
/*                                                                          */
/*     DESCRIPTION      : This function sets the default L2CP tunnel status */
/*                        for the protocols for a given port.               */
/*                                                                          */
/*     INPUT            : i4PortId  - Port on which tunnel status to be set */
/*                        uyType    - L2 protocol                           */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetPbL2ProtocolDefault (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4Type)
{
    UINT4               u4ErrCode;
    UINT2               u2Port;
    INT4                i4PortType = VLAN_CUSTOMER_BRIDGE_PORT;
    INT4                i4TunnelStatus = VLAN_TUNNEL_PROTOCOL_DISCARD;

    u2Port = (UINT2) i4PortId;
    if (VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE)
    {
        i4PortType = VLAN_PB_PORT_TYPE (u2Port);
    }
    i4TunnelStatus =
        (INT4) gau1PbDefPortProtoTunnelStatus[i4PortType - 1][u4Type - 1];

    switch (u4Type)
    {
        case CLI_PB_L2_PROTO_DOT1X:
            if (nmhTestv2FsVlanTunnelProtocolDot1x (&u4ErrCode, i4PortId,
                                                    i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolDot1x (i4PortId,
                                                 i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_LACP:
            if (nmhTestv2FsVlanTunnelProtocolLacp (&u4ErrCode, i4PortId,
                                                   i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolLacp (i4PortId,
                                                i4TunnelStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_STP:
            if (nmhTestv2FsVlanTunnelProtocolStp (&u4ErrCode, i4PortId,
                                                  i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolStp (i4PortId,
                                               i4TunnelStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_GVRP:
            if (nmhTestv2FsVlanTunnelProtocolGvrp (&u4ErrCode, i4PortId,
                                                   i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolGvrp (i4PortId,
                                                i4TunnelStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_MVRP:

            if (nmhTestv2FsVlanTunnelProtocolMvrp (&u4ErrCode, i4PortId,
                                                   i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolMvrp (i4PortId,
                                                i4TunnelStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_GMRP:
            if (nmhTestv2FsVlanTunnelProtocolGmrp (&u4ErrCode, i4PortId,
                                                   i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolGmrp (i4PortId,
                                                i4TunnelStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_MMRP:
            if (nmhTestv2FsVlanTunnelProtocolMmrp (&u4ErrCode, i4PortId,
                                                   i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolMmrp (i4PortId,
                                                i4TunnelStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_IGMP:
            if (nmhTestv2FsVlanTunnelProtocolIgmp (&u4ErrCode, i4PortId,
                                                   i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolIgmp (i4PortId,
                                                i4TunnelStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_ELMI:
            if (nmhTestv2FsVlanTunnelProtocolElmi (&u4ErrCode, i4PortId,
                                                   i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolElmi (i4PortId,
                                                i4TunnelStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_LLDP:
            if (nmhTestv2FsVlanTunnelProtocolLldp (&u4ErrCode, i4PortId,
                                                   i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolLldp (i4PortId,
                                                i4TunnelStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_ECFM:
            if (nmhTestv2FsVlanTunnelProtocolEcfm (&u4ErrCode, i4PortId,
                                                   i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolEcfm (i4PortId,
                                                i4TunnelStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_EOAM:
            if (nmhTestv2FsVlanTunnelProtocolEoam (&u4ErrCode, i4PortId,
                                                   i4TunnelStatus)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolEoam (i4PortId,
                                                i4TunnelStatus) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        default:
            break;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPbL2ProtocolDiscard                      */
/*                                                                          */
/*     DESCRIPTION      : This function sets the option to discard the L2   */
/*                        protocols received on a given port.               */
/*                                                                          */
/*     INPUT            : i4PortId  - Port on which tunneling to be enabled */
/*                        uyType    - L2 protocol                           */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetPbL2ProtocolDiscard (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4Type)
{
    UINT4               u4ErrCode;
    switch (u4Type)
    {
        case CLI_PB_L2_PROTO_DOT1X:
            if (nmhTestv2FsVlanTunnelProtocolDot1x (&u4ErrCode, i4PortId,
                                                    VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolDot1x (i4PortId,
                                                 VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_LACP:
            if (nmhTestv2FsVlanTunnelProtocolLacp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolLacp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_STP:
            if (nmhTestv2FsVlanTunnelProtocolStp (&u4ErrCode, i4PortId,
                                                  VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolStp (i4PortId,
                                               VLAN_TUNNEL_PROTOCOL_DISCARD) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_GVRP:
            if (nmhTestv2FsVlanTunnelProtocolGvrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolGvrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_MVRP:

            if (nmhTestv2FsVlanTunnelProtocolMvrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolMvrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_GMRP:
            if (nmhTestv2FsVlanTunnelProtocolGmrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolGmrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_MMRP:
            if (nmhTestv2FsVlanTunnelProtocolMmrp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolMmrp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        case CLI_PB_L2_PROTO_IGMP:
            if (nmhTestv2FsVlanTunnelProtocolIgmp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolIgmp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_ELMI:
            if (nmhTestv2FsVlanTunnelProtocolElmi (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolElmi (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_LLDP:
            if (nmhTestv2FsVlanTunnelProtocolLldp (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolLldp (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_ECFM:
            if (nmhTestv2FsVlanTunnelProtocolEcfm (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolEcfm (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
        case CLI_PB_L2_PROTO_EOAM:
            if (nmhTestv2FsVlanTunnelProtocolEoam (&u4ErrCode, i4PortId,
                                                   VLAN_TUNNEL_PROTOCOL_DISCARD)
                != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsVlanTunnelProtocolEoam (i4PortId,
                                                VLAN_TUNNEL_PROTOCOL_DISCARD) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
    }
    return CLI_SUCCESS;
}

INT4
VlanPbSetPbL2ProtocolOverride (tCliHandle CliHandle, INT4 i4PortId,
                               UINT4 u4Type)
{
    UINT4               u4ErrCode;
    INT4                OverrideOption = 0;

    if (u4Type == VLAN_ENABLED)
    {
        OverrideOption = VLAN_OVERRIDE_OPTION_ENABLE;
    }
    else if (u4Type == VLAN_DISABLED)
    {
        OverrideOption = VLAN_OVERRIDE_OPTION_DISABLE;
    }

    if (nmhTestv2FsVlanTunnelOverrideOption (&u4ErrCode, i4PortId,
                                             OverrideOption) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVlanTunnelOverrideOption (i4PortId,
                                          OverrideOption) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPepPvid                                  */
/*                                                                          */
/*     DESCRIPTION      : This function sets PVID for the provider edge     */
/*                        port.                                             */
/*                                                                          */
/*     INPUT            : i4PortId  - Customer edge port                    */
/*                        i4SVlanId   - Service VLAN ID                       */
/*                        i4Pvid    - PVID to be set for the port           */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetPepPvid (tCliHandle CliHandle, INT4 i4PortId, INT4 i4SVlanId,
                  INT4 i4Pvid)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1adPepPvid (&u4ErrCode, i4PortId, i4SVlanId, i4Pvid) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1adPepPvid (i4PortId, i4SVlanId, i4Pvid) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPepAccpFrameType                         */
/*                                                                          */
/*     DESCRIPTION      : This function sets the acceptable frame type      */
/*                        parameter for the provider edge port              */
/*                                                                          */
/*     INPUT            : i4PortId  - Customer edge port                    */
/*                        SVlanId   - Service VLAN ID                       */
/*                        i4AccptFrameType - Acceptable frame type          */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetPepAccpFrameType (tCliHandle CliHandle, INT4 i4PortId, tVlanId SVlanId,
                           INT4 i4AccptFrameType)
{
    UINT4               u4ErrCode;
    if (nmhTestv2Dot1adPepAccptableFrameTypes (&u4ErrCode, i4PortId,
                                               SVlanId, i4AccptFrameType) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1adPepAccptableFrameTypes (i4PortId, SVlanId, i4AccptFrameType)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPepDefUserPriority                       */
/*                                                                          */
/*     DESCRIPTION      : This function sets the default user priority value*/
/*                        for provider edge ports.                          */
/*                                                                          */
/*     INPUT            : i4PortId  - Customer edge port                    */
/*                        SVlanId   - Service VLAN ID                       */
/*                        i4DefUserPrio - Default user priority             */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetPepDefUserPriority (tCliHandle CliHandle, INT4 i4PortId,
                             tVlanId SVlanId, INT4 i4DefUserPrio)
{
    UINT4               u4ErrCode;
    if (nmhTestv2Dot1adPepDefaultUserPriority (&u4ErrCode, i4PortId, SVlanId,
                                               i4DefUserPrio) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetDot1adPepDefaultUserPriority (i4PortId, SVlanId, i4DefUserPrio)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPepIngressFiltering                      */
/*                                                                          */
/*     DESCRIPTION      : This function enables/disables ingress filtering  */
/*                        on provider edge ports.                           */
/*                                                                          */
/*     INPUT            : i4PortId  - Customer edge port                    */
/*                        SVlanId   - Service VLAN ID                       */
/*                        i4IngFiltering - Ingress filtering                */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetPepIngressFiltering (tCliHandle CliHandle, INT4 i4PortId,
                              tVlanId SVlanId, INT4 i4IngFiltering)
{
    UINT4               u4ErrCode;
    if (nmhTestv2Dot1adPepIngressFiltering (&u4ErrCode, i4PortId, SVlanId,
                                            i4IngFiltering) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetDot1adPepIngressFiltering (i4PortId, SVlanId, i4IngFiltering)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPepCosPreservation                       */
/*                                                                          */
/*     DESCRIPTION      : This function enables/disables COS preservation   */
/*                        on provider edge ports.                           */
/*                                                                          */
/*     INPUT            : i4PortId  - Customer edge port                    */
/*                        SVlanId   - Service VLAN ID                       */
/*                        i4CosPreservation - COS Preservation              */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetPepCosPreservation (tCliHandle CliHandle, INT4 i4PortId,
                             tVlanId SVlanId, INT4 i4CosPreservation)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPbPepExtCosPreservation (&u4ErrCode, i4PortId, SVlanId,
                                            i4CosPreservation) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPbPepExtCosPreservation (i4PortId, SVlanId, i4CosPreservation)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowTunnelMacAddress                        */
/*                                                                          */
/*     DESCRIPTION      : This function will display the tunnel MAC         */
/*                        addresses configured for the Layer 2 protocols    */
/*                        in the given context.                             */
/*                                                                          */
/*     INPUT            : CliHandle - CLI Handle                            */
/*                        u4ContextId - Virtual ContextId.                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
UINT4
VlanPbShowTunnelMacAddress (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tMacAddr            TunnelAddress;
    UINT4               u4PagingStatus;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];

    CliPrintf (CliHandle, " VLAN tunnel MAC address  \r\n");
    CliPrintf (CliHandle, "--------------------------\r\n");

    nmhGetFsMIVlanTunnelDot1xAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, " Dot1x tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    nmhGetFsMIVlanTunnelLacpAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, " LACP  tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    nmhGetFsMIVlanTunnelStpAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, "  STP  tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    nmhGetFsMIVlanTunnelMvrpAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, " MVRP  tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    nmhGetFsMIVlanTunnelMmrpAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, " MMRP  tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    nmhGetFsMIVlanTunnelGvrpAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, " GVRP  tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    nmhGetFsMIVlanTunnelGmrpAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, " GMRP  tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    nmhGetFsMIVlanTunnelElmiAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, " ELMI  tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    nmhGetFsMIVlanTunnelLldpAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, " LLDP  tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    nmhGetFsMIVlanTunnelEcfmAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, " ECFM  tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    nmhGetFsMIVlanTunnelEoamAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, " EOAM  tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    nmhGetFsMIVlanTunnelIgmpAddress ((INT4) u4ContextId, &TunnelAddress);
    CliPrintf (CliHandle, " IGMP  tunnel MAC address :  ");
    PrintMacAddress (TunnelAddress, au1String);
    CliPrintf (CliHandle, "%-21s\r\n", au1String);

    u4PagingStatus = CliPrintf (CliHandle, "\r\n");

    return u4PagingStatus;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowPortTables                              */
/*                                                                          */
/*     DESCRIPTION      : This function will display the provider edge or   */
/*                        core port table configurations.                   */
/*                                                                          */
/*     INPUT            : u4Command - command giving the port table to be   */
/*                                    shown.                                */
/*                        pu1InContextName - Context Name given in command. */
/*                        u4IfIndex - IfIndex of the port.                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbShowPortTables (tCliHandle CliHandle, UINT4 u4Command,
                      UINT4 u4ContextId, UINT4 u4IfIndex)
{
    INT4                i4RetStatus = CLI_FAILURE;

    if (VlanPbCheckSystemStatus (CliHandle, u4ContextId) != VLAN_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (VlanPbPrintTableHeading (CliHandle, u4Command) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    i4RetStatus = VlanPbShowPortTablesInContext (CliHandle, u4Command,
                                                 u4ContextId, u4IfIndex);

    return i4RetStatus;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowPortTablesInContext                     */
/*                                                                          */
/*     DESCRIPTION      : This function will display the provider edge or   */
/*                        core port table configurationsi per Context.      */
/*                                                                          */
/*     INPUT            : u4Command - command giving the port table to be   */
/*                                    shown.                                */
/*                        u4ContextId - Context Identifier.                 */
/*                        i4PortId - Port Id. (if (0) then show for all     */
/*                                             ports in the context. )      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbShowPortTablesInContext (tCliHandle CliHandle, UINT4 u4Command,
                               UINT4 u4ContextId, INT4 i4PortId)
{
    INT4                i4CurrentPort;
    INT4                i4NextPortId;
    INT4                i4BridgeMode = VLAN_INVALID_BRIDGE_MODE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4TempContextId;
    UINT1               u1isShowAll = TRUE;
    UINT2               u2LocalPortId;

    nmhGetFsMIVlanBridgeMode ((INT4) u4ContextId, &i4BridgeMode);

    switch (u4Command)
    {
        case CLI_PB_SHOW_PEP_CONFIG:
        case CLI_PB_SHOW_PORT_PRIORITY_REGEN:
            if ((i4PortId == 0) &&
                (i4BridgeMode != VLAN_PROVIDER_EDGE_BRIDGE_MODE))
            {
                CliPrintf (CliHandle, "\r%% Bridge mode is not Provide"
                           "Edge.\r\n");
                return CLI_FAILURE;

            }
            break;
        case CLI_PB_SHOW_PORT_PCP_ENCODE:
        case CLI_PB_SHOW_PORT_PCP_DECODE:
        case CLI_PB_SHOW_PORT_CONFIG:
            if ((i4PortId == 0) &&
                (i4BridgeMode != VLAN_PROVIDER_EDGE_BRIDGE_MODE) &&
                (i4BridgeMode != VLAN_PROVIDER_CORE_BRIDGE_MODE) &&
                (i4BridgeMode != VLAN_PBB_ICOMPONENT_BRIDGE_MODE) &&
                (i4BridgeMode != VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
            {
                CliPrintf (CliHandle, "\r%% Bridge mode is not Provide Edge or "
                           "Core or Backbone.\r\n");
                return CLI_FAILURE;

            }
            break;
        default:
            return CLI_FAILURE;
    }

    if (nmhGetFirstIndexFsMIPbPortInfoTable (&i4NextPortId) == SNMP_SUCCESS)
    {
        do
        {
            if (VlanGetContextInfoFromIfIndex
                ((UINT4) i4NextPortId, &u4TempContextId,
                 &u2LocalPortId) == VLAN_SUCCESS)
            {
                if (((i4PortId != 0) && (i4NextPortId == i4PortId))
                    || ((i4PortId == 0) && (u4TempContextId == u4ContextId)))

                {
                    switch (u4Command)
                    {
                        case CLI_PB_SHOW_PEP_CONFIG:
                            VlanPbShowPepConfiguration (CliHandle,
                                                        i4NextPortId,
                                                        &u4PagingStatus);
                            break;
                        case CLI_PB_SHOW_PORT_PCP_ENCODE:
                            VlanPbShowPcpEncodingTable (CliHandle,
                                                        i4NextPortId);
                            break;
                        case CLI_PB_SHOW_PORT_PCP_DECODE:
                            VlanPbShowPcpDecodingTable (CliHandle,
                                                        i4NextPortId);
                            break;
                        case CLI_PB_SHOW_PORT_PRIORITY_REGEN:
                            VlanPbShowPriorityRegenTable (CliHandle,
                                                          i4NextPortId);
                            break;
                        case CLI_PB_SHOW_PORT_CONFIG:
                            VlanPbShowProviderBridgePortConfig (CliHandle,
                                                                i4NextPortId,
                                                                u4ContextId);
                            break;
                    }

                    if (i4PortId != 0)
                    {
                        break;
                    }

                }
            }

            i4CurrentPort = i4NextPortId;

            if (nmhGetNextIndexFsMIPbPortInfoTable
                (i4CurrentPort, &i4NextPortId) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

        CliPrintf (CliHandle, "\r\n");
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowPepConfiguration                        */
/*                                                                          */
/*     DESCRIPTION      : This function will display the PEP configuration  */
/*                        for the given port.                               */
/*                                                                          */
/*     INPUT            : i4PortId - Port Id.                               */
/*                        pu4PagingStatus - Paging Status.                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbShowPepConfiguration (tCliHandle CliHandle, INT4 i4PortId,
                            UINT4 *pu4PagingStatus)
{
    INT4                i4CurrentPort;
    INT4                i4NextPort;
    INT4                i4Cpvid;
    INT4                i4AccptFrameType;
    INT4                i4DefUserPrio;
    INT4                i4IngFiltering;
    INT4                i4CosPreservation;
    INT4                i4CurrSVlanId;
    INT4                i4NextSVlanId;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1PepOperStatus = VLAN_OPER_DOWN;
    UINT1               u1EnteredLoop = VLAN_FALSE;

    i4CurrentPort = i4PortId - 1;
    i4CurrSVlanId = VLAN_MAX_VLAN_ID;

    while ((nmhGetNextIndexDot1adMIPepTable
            (i4CurrentPort, &i4NextPort, i4CurrSVlanId,
             &i4NextSVlanId) == SNMP_SUCCESS) && (i4NextPort == i4PortId))
    {
        u1EnteredLoop = VLAN_TRUE;

        VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
        CliPrintf (CliHandle, "\nPort %s\r\n", au1NameStr);

        nmhGetDot1adMIPepPvid (i4NextPort, (UINT4) i4NextSVlanId, &i4Cpvid);
        nmhGetDot1adMIPepAccptableFrameTypes (i4NextPort, (UINT4) i4NextSVlanId,
                                              &i4AccptFrameType);
        nmhGetDot1adMIPepDefaultUserPriority (i4NextPort, (UINT4) i4NextSVlanId,
                                              &i4DefUserPrio);
        nmhGetDot1adMIPepIngressFiltering (i4NextPort, (UINT4) i4NextSVlanId,
                                           &i4IngFiltering);
        nmhGetFsMIPbPepExtCosPreservation (i4NextPort, (UINT4) i4NextSVlanId,
                                           &i4CosPreservation);

        VlanMiPbGetPepOperStatus ((UINT4) i4NextPort, (UINT4) i4NextSVlanId,
                                  &u1PepOperStatus);

        CliPrintf (CliHandle, " %-35s : %d\r\n", "Service VLAN-ID",
                   i4NextSVlanId);

        CliPrintf (CliHandle, " %-35s : %d\r\n", "Port VLAN-ID", i4Cpvid);

        if (i4AccptFrameType == VLAN_ADMIT_ALL_FRAMES)
        {
            CliPrintf (CliHandle,
                       " %-35s : Admit all\r\n", "Acceptable Frame Type");
        }
        else if (i4AccptFrameType == VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
        {
            CliPrintf (CliHandle,
                       " %-35s : Admit only VLAN Tagged\r\n",
                       "Acceptable Frame Type");
        }
        else if (i4AccptFrameType ==
                 VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
        {
            CliPrintf (CliHandle,
                       " %-35s : Admit only untagged and priority tagged\r\n",
                       "Acceptable Frame Type");
        }

        else
        {
            CliPrintf (CliHandle,
                       " %-35s : Unknown\r\n", "Acceptable Frame Type");
        }

        if (i4IngFiltering == VLAN_SNMP_TRUE)
        {
            CliPrintf (CliHandle, " %-35s : Enabled\r\n", "Ingress Filtering");
        }
        else if (i4IngFiltering == VLAN_SNMP_FALSE)
        {
            CliPrintf (CliHandle, " %-35s : Disabled\r\n", "Ingress Filtering");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Unknown\r\n", "Ingress Filtering");
        }

        CliPrintf (CliHandle, " %-35s : %d\r\n", "Default Priority",
                   i4DefUserPrio);

        if (i4CosPreservation == VLAN_ENABLED)
        {
            CliPrintf (CliHandle, " %-35s : Enabled\r\n", "COS Preservation");
        }
        else if (i4CosPreservation == VLAN_DISABLED)
        {
            CliPrintf (CliHandle, " %-35s : Disabled\r\n", "COS Preservation");
        }
        else
        {
            CliPrintf (CliHandle, " %-35s : Unknown\r\n", "COS Preservation");
        }

        if (u1PepOperStatus == VLAN_OPER_UP)
        {
            CliPrintf (CliHandle, " %-35s : Up", "Oper status");
        }
        else if (u1PepOperStatus == VLAN_OPER_DOWN)
        {
            CliPrintf (CliHandle, " %-35s : Down", "Oper status");
        }

        i4CurrentPort = i4NextPort;
        i4CurrSVlanId = i4NextSVlanId;
    }

    if (u1EnteredLoop == VLAN_TRUE)
    {
        *pu4PagingStatus = CliPrintf (CliHandle,
                                      "\r\n-------------------------"
                                      "---------------------------\r\n");
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbPrintTableHeading                           */
/*                                                                          */
/*     DESCRIPTION      : This function will display the heading for the    */
/*                        table identified by the command argument.         */
/*                                                                          */
/*     INPUT            : u4Command - Command identifing the table.         */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbPrintTableHeading (tCliHandle CliHandle, UINT4 u4Command)
{

    switch (u4Command)
    {
        case CLI_PB_SHOW_PEP_CONFIG:
            CliPrintf (CliHandle, "\r\nProvider Edge Port configuration\r\n");
            CliPrintf (CliHandle, "-------------------------------------\r\n");
            break;
        case CLI_PB_SHOW_PORT_PCP_ENCODE:
            CliPrintf (CliHandle, "\r\nPcp Encoding Table\r\n");
            CliPrintf (CliHandle, "-------------------------------------\r\n");
            break;
        case CLI_PB_SHOW_PORT_PCP_DECODE:
            CliPrintf (CliHandle, "\r\nPcp Decoding Table\r\n");
            CliPrintf (CliHandle, "-------------------------------------\r\n");
            break;
        case CLI_PB_SHOW_PORT_PRIORITY_REGEN:
            CliPrintf (CliHandle, "\r\nService Priority Regeneration table"
                       "\r\n");
            CliPrintf (CliHandle, "-----------------------------------\r\n");
            break;
        case CLI_PB_SHOW_PORT_CONFIG:
            CliPrintf (CliHandle,
                       "\r\nProvider Bridge Port configuration table\r\n");
            CliPrintf (CliHandle, "-----------------------------------------"
                       "\r\n");
            break;
        default:
            return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetClassType                                */
/*                                                                          */
/*     DESCRIPTION      : This function configures the Service Vlan         */
/*                        classification mechanism for the port             */
/*                                                                          */
/*     INPUT            : i4PortId  - Port Index                            */
/*                        u4ClassType - Service vlan classification type    */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSetClassType (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4ClassType)
{

    UINT4               u4ErrCode;

    if (nmhTestv2FsPbPortSVlanClassificationMethod
        (&u4ErrCode, i4PortId, u4ClassType) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPbPortSVlanClassificationMethod (i4PortId, u4ClassType) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowSrcMacSVlanConf                         */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Source Mac Based       */
/*                        Service Vlan classification                       */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4ContextId - Context Id of the Switch            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowSrcMacSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 *pu4PagingStatus)
{
    tMacAddr            u1SrcMacAddr;
    tMacAddr            u1NextSrcMacAddr;
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4SVlanId;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;

    MEMSET (u1NextSrcMacAddr, 0, sizeof (tMacAddr));
    MEMSET (u1SrcMacAddr, 0, sizeof (tMacAddr));

    if (nmhGetFirstIndexFsMIPbSrcMacSVlanTable
        (&i4NextPort, &u1NextSrcMacAddr) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port    Src MAC Address                \r\n");
        CliPrintf (CliHandle,
                   " -----------------------------------------------------------\r\n");
        do
        {
            if (VlanGetContextInfoFromIfIndex
                ((UINT4) i4NextPort, &u4TempContextId,
                 &u2LocalPortId) == VLAN_SUCCESS)
            {

                if (u4TempContextId == u4ContextId)
                {
                    nmhGetFsMIPbSrcMacSVlan (i4NextPort, u1NextSrcMacAddr,
                                             &i4SVlanId);

                    CliPrintf (CliHandle, " %-16d", i4SVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-9s", au1NameStr);

                    PrintMacAddress (u1NextSrcMacAddr, au1String);
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, "%-21s\r\n", au1String);
                }
            }

            i4CurrentPort = i4NextPort;
            MEMCPY (u1SrcMacAddr, u1NextSrcMacAddr, sizeof (u1SrcMacAddr));

            if (nmhGetNextIndexFsMIPbSrcMacSVlanTable
                (i4CurrentPort, &i4NextPort, u1SrcMacAddr,
                 &u1NextSrcMacAddr) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

        CliPrintf (CliHandle, "\r\n");

    }

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowDstMacSVlanConf                         */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Destination Mac Based  */
/*                        Service Vlan classification                       */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4ContextId - Context Id of the switch            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowDstMacSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 *pu4PagingStatus)
{
    tMacAddr            u1DstMacAddr;
    tMacAddr            u1NextDstMacAddr;
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4SVlanId;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;

    MEMSET (u1NextDstMacAddr, 0, sizeof (tMacAddr));
    MEMSET (u1DstMacAddr, 0, sizeof (tMacAddr));

    if (nmhGetFirstIndexFsMIPbDstMacSVlanTable
        (&i4NextPort, &u1NextDstMacAddr) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port    Dst MAC Address                   \r\n");
        CliPrintf (CliHandle,
                   " -----------------------------------------------------------\r\n");
        do
        {

            if (VlanGetContextInfoFromIfIndex
                ((UINT4) i4NextPort, &u4TempContextId,
                 &u2LocalPortId) == VLAN_SUCCESS)
            {

                if (u4TempContextId == u4ContextId)
                {
                    nmhGetFsMIPbDstMacSVlan (i4NextPort, u1NextDstMacAddr,
                                             &i4SVlanId);

                    CliPrintf (CliHandle, " %-16d", i4SVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-9s", au1NameStr);

                    PrintMacAddress (u1NextDstMacAddr, au1String);
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, "%-21s\r\n", au1String);
                }
            }

            i4CurrentPort = i4NextPort;
            MEMCPY (u1DstMacAddr, u1NextDstMacAddr, sizeof (u1DstMacAddr));

            if (nmhGetNextIndexFsMIPbDstMacSVlanTable
                (i4CurrentPort, &i4NextPort, u1DstMacAddr,
                 &u1NextDstMacAddr) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

        CliPrintf (CliHandle, "\r\n");
    }

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowCVlanSrcMacSVlanConf                    */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Customer Vlan and      */
/*     Source Mac Based Service Vlan classification                         */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4ContextId - Context Id of the switch            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowCVlanSrcMacSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 *pu4PagingStatus)
{
    tMacAddr            u1SrcMacAddr;
    tMacAddr            u1NextSrcMacAddr;
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4SVlanId;
    INT4                i4CVlanId;
    INT4                i4NextCVlanId;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;

    MEMSET (u1NextSrcMacAddr, 0, sizeof (tMacAddr));
    MEMSET (u1SrcMacAddr, 0, sizeof (tMacAddr));

    if (nmhGetFirstIndexFsMIPbCVlanSrcMacSVlanTable
        (&i4NextPort, &i4NextCVlanId, &u1NextSrcMacAddr) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port    Src MAC Address       Customer Vlan\r\n");
        CliPrintf (CliHandle,
                   " -----------------------------------------------------------\r\n");
        do
        {

            if (VlanGetContextInfoFromIfIndex
                ((UINT4) i4NextPort, &u4TempContextId,
                 &u2LocalPortId) == VLAN_SUCCESS)
            {

                if (u4TempContextId == u4ContextId)
                {
                    nmhGetFsMIPbCVlanSrcMacSVlan (i4NextPort,
                                                  i4NextCVlanId,
                                                  u1NextSrcMacAddr, &i4SVlanId);
                    CliPrintf (CliHandle, " %-16d", i4SVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-9s", au1NameStr);

                    PrintMacAddress (u1NextSrcMacAddr, au1String);
                    CliPrintf (CliHandle, "%-21s", au1String);
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, "%-21d\r\n", i4NextCVlanId);
                }
            }

            i4CurrentPort = i4NextPort;
            i4CVlanId = i4NextCVlanId;
            MEMCPY (u1SrcMacAddr, u1NextSrcMacAddr, sizeof (u1SrcMacAddr));

            if (nmhGetNextIndexFsMIPbCVlanSrcMacSVlanTable
                (i4CurrentPort, &i4NextPort, i4CVlanId,
                 &i4NextCVlanId,
                 u1SrcMacAddr, &u1NextSrcMacAddr) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

        CliPrintf (CliHandle, "\r\n");
    }

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowCVlanDstMacSVlanConf                    */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Customer Vlan and      */
/*                        Destination Mac Based Service Vlan classification */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4ContextId - Context Id of the switch            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowCVlanDstMacSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 *pu4PagingStatus)
{
    tMacAddr            u1DstMacAddr;
    tMacAddr            u1NextDstMacAddr;
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4SVlanId;
    INT4                i4CVlanId;
    INT4                i4NextCVlanId;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;

    MEMSET (u1NextDstMacAddr, 0, sizeof (tMacAddr));
    MEMSET (u1DstMacAddr, 0, sizeof (tMacAddr));

    if (nmhGetFirstIndexFsMIPbCVlanDstMacSVlanTable
        (&i4NextPort, &i4NextCVlanId, &u1NextDstMacAddr) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port    Dst MAC Address      Customer Vlan\r\n");
        CliPrintf (CliHandle,
                   " -----------------------------------------------------------\r\n");
        do
        {

            if (VlanGetContextInfoFromIfIndex
                ((UINT4) i4NextPort, &u4TempContextId,
                 &u2LocalPortId) == VLAN_SUCCESS)
            {

                if (u4TempContextId == u4ContextId)
                {
                    nmhGetFsMIPbCVlanDstMacSVlan (i4NextPort,
                                                  i4NextCVlanId,
                                                  u1NextDstMacAddr, &i4SVlanId);
                    CliPrintf (CliHandle, " %-16d", i4SVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-9s", au1NameStr);

                    PrintMacAddress (u1NextDstMacAddr, au1String);
                    CliPrintf (CliHandle, "%-21s", au1String);
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, "%-21d\r\n", i4NextCVlanId);
                }
            }

            i4CurrentPort = i4NextPort;
            i4CVlanId = i4NextCVlanId;
            MEMCPY (u1DstMacAddr, u1NextDstMacAddr, sizeof (u1DstMacAddr));

            if (nmhGetNextIndexFsMIPbCVlanDstMacSVlanTable
                (i4CurrentPort, &i4NextPort, i4CVlanId,
                 &i4NextCVlanId,
                 u1DstMacAddr, &u1NextDstMacAddr) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

        CliPrintf (CliHandle, "\r\n");

    }

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowDscpSVlanConf                           */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Dscp Based             */
/*                        Service Vlan classification                       */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4ContextId - Context Id of the switch            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowDscpSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 *pu4PagingStatus)
{
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4SVlanId;
    INT4                i4DSCPValue;
    INT4                i4NextDSCPValue;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;

    if (nmhGetFirstIndexFsMIPbDscpSVlanTable
        (&i4NextPort, &i4NextDSCPValue) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port         Dscp                      \r\n");
        CliPrintf (CliHandle,
                   " -----------------------------------------------------------\r\n");

        do
        {
            if (VlanGetContextInfoFromIfIndex
                ((UINT4) i4NextPort, &u4TempContextId,
                 &u2LocalPortId) == VLAN_SUCCESS)
            {

                if (u4TempContextId == u4ContextId)
                {
                    nmhGetFsMIPbDscpSVlan (i4NextPort, i4NextDSCPValue,
                                           &i4SVlanId);
                    CliPrintf (CliHandle, " %-16d", i4SVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "   %-9s", au1NameStr);

                    *pu4PagingStatus =
                        CliPrintf (CliHandle, "   %-21d\r\n", i4NextDSCPValue);
                }
            }

            i4CurrentPort = i4NextPort;
            i4DSCPValue = i4NextDSCPValue;

            if (nmhGetNextIndexFsMIPbDscpSVlanTable
                (i4CurrentPort, &i4NextPort, i4DSCPValue,
                 &i4NextDSCPValue) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

        CliPrintf (CliHandle, "\r\n");

    }

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowCVlanDscpSVlanConf                      */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Customer Vlan and Dscp */
/*                        Based Service Vlan classification                 */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4ContextId - Context Id of the switch            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowCVlanDscpSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 *pu4PagingStatus)
{
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4SVlanId;
    INT4                i4DSCPValue;
    INT4                i4NextDSCPValue;
    INT4                i4CVlanId;
    INT4                i4NextCVlanId;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;

    if (nmhGetFirstIndexFsMIPbCVlanDscpSVlanTable
        (&i4NextPort, &i4NextCVlanId, &i4NextDSCPValue) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port       Dscp        Customer Vlan\r\n");
        CliPrintf (CliHandle,
                   " -----------------------------------------------------------\r\n");
        do
        {
            if (VlanGetContextInfoFromIfIndex ((UINT4) i4NextPort,
                                               &u4TempContextId,
                                               &u2LocalPortId) == VLAN_SUCCESS)
            {
                if (u4TempContextId == u4ContextId)
                {
                    nmhGetFsMIPbCVlanDscpSVlan (i4NextPort, i4NextCVlanId,
                                                i4NextDSCPValue, &i4SVlanId);
                    CliPrintf (CliHandle, " %-16d", i4SVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, " %-11s", au1NameStr);

                    CliPrintf (CliHandle, "    %-21d", i4NextDSCPValue);
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, " %-21d\r\n", i4NextCVlanId);
                }
            }

            i4CurrentPort = i4NextPort;
            i4CVlanId = i4NextCVlanId;
            i4DSCPValue = i4NextDSCPValue;

            if (nmhGetNextIndexFsMIPbCVlanDscpSVlanTable
                (i4CurrentPort, &i4NextPort, i4CVlanId,
                 &i4NextCVlanId, i4DSCPValue, &i4NextDSCPValue) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

        CliPrintf (CliHandle, "\r\n");

    }

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowSrcIPSVlanConf                          */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Source IP Based        */
/*                        Service Vlan classification                       */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4ContextId - Context Id of the switch            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowSrcIPSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 *pu4PagingStatus)
{
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4SVlanId;
    UINT4               u4SrcIPAddr;
    UINT4               u4NextSrcIPAddr;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;
    CHR1               *pu1Address = NULL;

    pu1Address = (CHR1 *) & au1Address[0];

    if (nmhGetFirstIndexFsMIPbSrcIpAddrSVlanTable
        (&i4NextPort, &u4NextSrcIPAddr) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port     Src IP Address                   \r\n");
        CliPrintf (CliHandle,
                   " -----------------------------------------------------------\r\n");
        do
        {
            if (VlanGetContextInfoFromIfIndex ((UINT4) i4NextPort,
                                               &u4TempContextId,
                                               &u2LocalPortId) == VLAN_SUCCESS)
            {
                if (u4TempContextId == u4ContextId)
                {
                    nmhGetFsMIPbSrcIpSVlan (i4NextPort, u4NextSrcIPAddr,
                                            &i4SVlanId);

                    CliPrintf (CliHandle, " %-16d", i4SVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-9s", au1NameStr);

                    CLI_CONVERT_IPADDR_TO_STR (pu1Address, u4NextSrcIPAddr);
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, "%-21s\r\n", pu1Address);
                }
            }

            i4CurrentPort = i4NextPort;
            u4SrcIPAddr = u4NextSrcIPAddr;

            if (nmhGetNextIndexFsMIPbSrcIpAddrSVlanTable
                (i4CurrentPort, &i4NextPort, u4SrcIPAddr,
                 &u4NextSrcIPAddr) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

        CliPrintf (CliHandle, "\r\n");

    }

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowDstIPSVlanConf                          */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Destination IP Based   */
/*                        Service Vlan classification                       */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4ContextId - Context Id of the switch            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowDstIPSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 *pu4PagingStatus)
{
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4SVlanId;
    UINT4               u4DstIPAddr;
    UINT4               u4NextDstIPAddr;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;
    CHR1               *pu1Address = NULL;

    pu1Address = (CHR1 *) & au1Address[0];

    if (nmhGetFirstIndexFsMIPbDstIpAddrSVlanTable
        (&i4NextPort, &u4NextDstIPAddr) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port     Dst IP Address                   \r\n");
        CliPrintf (CliHandle,
                   " -----------------------------------------------------------\r\n");
        do
        {
            if (VlanGetContextInfoFromIfIndex ((UINT4) i4NextPort,
                                               &u4TempContextId,
                                               &u2LocalPortId) == VLAN_SUCCESS)
            {
                if (u4TempContextId == u4ContextId)
                {
                    nmhGetFsMIPbDstIpSVlan (i4NextPort, u4NextDstIPAddr,
                                            &i4SVlanId);

                    CliPrintf (CliHandle, " %-16d", i4SVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-9s", au1NameStr);

                    CLI_CONVERT_IPADDR_TO_STR (pu1Address, u4NextDstIPAddr);
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, "%-21s\r\n", pu1Address);
                }
            }

            i4CurrentPort = i4NextPort;
            u4DstIPAddr = u4NextDstIPAddr;

            if (nmhGetNextIndexFsMIPbDstIpAddrSVlanTable
                (i4CurrentPort, &i4NextPort, u4DstIPAddr,
                 &u4NextDstIPAddr) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

        CliPrintf (CliHandle, "\r\n");
    }
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowSrcDstIPSVlanConf                       */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Source and Destination */
/*                        IP Based Service Vlan classification              */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4ContextId - Context Id of the switch            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowSrcDstIPSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 *pu4PagingStatus)
{
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4SVlanId;
    UINT4               u4SrcIPAddr;
    UINT4               u4NextSrcIPAddr;
    UINT4               u4DstIPAddr;
    UINT4               u4NextDstIPAddr;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;
    CHR1               *pu1Address = NULL;

    pu1Address = (CHR1 *) & au1Address[0];

    if (nmhGetFirstIndexFsMIPbSrcDstIpSVlanTable
        (&i4NextPort, &u4NextSrcIPAddr, &u4NextDstIPAddr) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port    Src IP Address       Dst IP Address\r\n");
        CliPrintf (CliHandle,
                   " -----------------------------------------------------------\r\n");
        do
        {

            if (VlanGetContextInfoFromIfIndex ((UINT4) i4NextPort,
                                               &u4TempContextId,
                                               &u2LocalPortId) == VLAN_SUCCESS)
            {
                if (u4TempContextId == u4ContextId)
                {
                    nmhGetFsMIPbSrcDstIpSVlan (i4NextPort, u4NextSrcIPAddr,
                                               u4NextDstIPAddr, &i4SVlanId);
                    CliPrintf (CliHandle, " %-16d", i4SVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-9s", au1NameStr);

                    CLI_CONVERT_IPADDR_TO_STR (pu1Address, u4NextSrcIPAddr);
                    CliPrintf (CliHandle, "%-21s", pu1Address);
                    CLI_CONVERT_IPADDR_TO_STR (pu1Address, u4NextDstIPAddr);
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, "%-21s\r\n", pu1Address);
                }
            }

            i4CurrentPort = i4NextPort;
            u4SrcIPAddr = u4NextSrcIPAddr;
            u4DstIPAddr = u4NextDstIPAddr;

            if (nmhGetNextIndexFsMIPbSrcDstIpSVlanTable
                (i4CurrentPort, &i4NextPort, u4SrcIPAddr,
                 &u4NextSrcIPAddr, u4DstIPAddr,
                 &u4NextDstIPAddr) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

        CliPrintf (CliHandle, "\r\n");
    }
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowCVlanDstIPSVlanConf                     */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Customer Vlan and      */
/*                        Destination IP Based Service Vlan classification  */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowCVlanDstIPSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 *pu4PagingStatus)
{
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4SVlanId;
    UINT4               u4DstIPAddr;
    UINT4               u4NextDstIPAddr;
    INT4                i4CVlanId;
    INT4                i4NextCVlanId;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;
    CHR1               *pu1Address = NULL;

    pu1Address = (CHR1 *) & au1Address[0];

    if (nmhGetFirstIndexFsMIPbCVlanDstIpSVlanTable
        (&i4NextPort, &i4NextCVlanId, &u4NextDstIPAddr) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port    Dst IP Address       Customer Vlan\r\n");
        CliPrintf (CliHandle,
                   " -----------------------------------------------------------\r\n");
        do
        {

            if (VlanGetContextInfoFromIfIndex ((UINT4) i4NextPort,
                                               &u4TempContextId,
                                               &u2LocalPortId) == VLAN_SUCCESS)
            {

                if (u4TempContextId == u4ContextId)
                {
                    nmhGetFsMIPbCVlanDstIpSVlan (i4NextPort, i4NextCVlanId,
                                                 u4NextDstIPAddr, &i4SVlanId);
                    CliPrintf (CliHandle, " %-16d", i4SVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-9s", au1NameStr);

                    CLI_CONVERT_IPADDR_TO_STR (pu1Address, u4NextDstIPAddr);
                    CliPrintf (CliHandle, "%-21s", pu1Address);
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, "%-21d\r\n", i4NextCVlanId);
                }
            }

            i4CurrentPort = i4NextPort;
            i4CVlanId = i4NextCVlanId;
            u4DstIPAddr = u4NextDstIPAddr;

            if (nmhGetNextIndexFsMIPbCVlanDstIpSVlanTable
                (i4CurrentPort, &i4NextPort, i4CVlanId,
                 &i4NextCVlanId, u4DstIPAddr, &u4NextDstIPAddr) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

    }
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowCVlanSVlanConf                          */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Customer Vlan Based    */
/*                        Service Vlan classification                       */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowCVlanSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 *pu4PagingStatus)
{
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4SVlanId;
    INT4                i4CVlanId;
    INT4                i4NextCVlanId;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    INT4                i4UntagPep;
    INT4                i4UntagCep;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;
    INT4                i4RelayCVlanId;
    INT4                i4SVlanPriorityType = 0;
    INT4                i4SVlanPriority = 0;

    if (nmhGetFirstIndexDot1adMICVidRegistrationTable
        (&i4NextPort, &i4NextCVlanId) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port    Customer Vlan  Untag-pep  Untag-cep  Relay CVlan Id  SVLAN Pri Type  SVLAN Priority\r\n");
        CliPrintf (CliHandle,
                   " ------------------------------------------------------------------------------------------------------------\r\n");
        do
        {

            if (VlanGetContextInfoFromIfIndex ((UINT4) i4NextPort,
                                               (UINT4 *) &u4TempContextId,
                                               &u2LocalPortId) == VLAN_SUCCESS)
            {
                if (u4TempContextId == u4ContextId)
                {

                    nmhGetDot1adMICVidRegistrationSVid (i4NextPort,
                                                        i4NextCVlanId,
                                                        &i4SVlanId);

                    CliPrintf (CliHandle, " %-16d", i4SVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-9s", au1NameStr);

                    CliPrintf (CliHandle, " %-16d", i4NextCVlanId);

                    nmhGetDot1adMICVidRegistrationUntaggedPep (i4NextPort,
                                                               i4NextCVlanId,
                                                               &i4UntagPep);

                    if (i4UntagPep == VLAN_SNMP_FALSE)
                    {
                        CliPrintf (CliHandle, " %-11s", "False");
                    }
                    else
                    {
                        CliPrintf (CliHandle, " %-11s", "True");
                    }

                    nmhGetDot1adMICVidRegistrationUntaggedCep (i4NextPort,
                                                               i4NextCVlanId,
                                                               &i4UntagCep);

                    if (i4UntagCep == VLAN_TRUE)
                    {
                        CliPrintf (CliHandle, " %-11s", "True");
                    }
                    else
                    {
                        CliPrintf (CliHandle, " %-11s", "False");
                    }

                    nmhGetDot1adMICVidRegistrationRelayCVid (i4NextPort,
                                                             i4NextCVlanId,
                                                             &i4RelayCVlanId);

                    CliPrintf (CliHandle, " %-16d", i4RelayCVlanId);

                    nmhGetDot1adMICVidRegistrationSVlanPriorityType (i4NextPort,
                                                                     i4NextCVlanId,
                                                                     &i4SVlanPriorityType);
                    nmhGetDot1adMICVidRegistrationSVlanPriority (i4NextPort,
                                                                 i4NextCVlanId,
                                                                 &i4SVlanPriority);
                    switch (i4SVlanPriorityType)
                    {
                        case VLAN_SVLAN_PRIORITY_TYPE_NONE:
                            CliPrintf (CliHandle, " %-11s", "NONE");
                            CliPrintf (CliHandle, " %-11s", "NA");
                            break;
                        case VLAN_SVLAN_PRIORITY_TYPE_FIXED:
                            CliPrintf (CliHandle, " %-11s", "FIXED");
                            CliPrintf (CliHandle, " %-16d", i4SVlanPriority);
                            break;
                        case VLAN_SVLAN_PRIORITY_TYPE_COPY:
                            CliPrintf (CliHandle, " %-11s", "COPY");
                            CliPrintf (CliHandle, " %-11s", "NA");
                            break;
                        default:
                            break;
                    }

                    *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
                }
            }

            i4CurrentPort = i4NextPort;
            i4CVlanId = i4NextCVlanId;

            if (nmhGetNextIndexDot1adMICVidRegistrationTable
                (i4CurrentPort, &i4NextPort, i4CVlanId,
                 &i4NextCVlanId) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowPvidSVlanConf                           */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Pvid Based             */
/*                        Service Vlan classification                       */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        pu4PagingStatus - Contains 'CliPrintf' Status     */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbShowPvidSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 *pu4PagingStatus)
{
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4Cpvid = 0;
    INT4                i4NextSVlanId;
    INT4                i4CurrSVlanId;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;
    if (nmhGetFirstIndexDot1adMIPepTable
        (&i4NextPort, &i4NextSVlanId) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   " Service Vlan     Port        pvid                              \r\n");
        CliPrintf (CliHandle,
                   " -----------------------------------------------------------\r\n");
        do
        {

            if (VlanGetContextInfoFromIfIndex ((UINT4) i4NextPort,
                                               (UINT4 *) &u4TempContextId,
                                               &u2LocalPortId) == VLAN_SUCCESS)
            {
                if (u4TempContextId == u4ContextId)
                {
                    CliPrintf (CliHandle, " %-16d", i4NextSVlanId);

                    VlanCfaCliGetIfName (i4NextPort, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "%-13s", au1NameStr);

                    nmhGetDot1adMIPepPvid (i4NextPort, i4NextSVlanId, &i4Cpvid);
                    CliPrintf (CliHandle, " %-16d", i4Cpvid);

                    *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
                }
            }

            i4CurrentPort = i4NextPort;
            i4CurrSVlanId = i4NextSVlanId;

            if (nmhGetNextIndexDot1adMIPepTable
                (i4CurrentPort, &i4NextPort, i4CurrSVlanId,
                 &i4NextSVlanId) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (*pu4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowSVlanConf                               */
/*                                                                          */
/*     DESCRIPTION      : This function will Display SVlan classification   */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4ContextId - Switch Context Id                   */
/*                        u4ClassType - To display only specified           */
/*                        classification                                    */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbShowSVlanConf (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4ClassType)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;

    CliPrintf (CliHandle, "\r\nService Vlan Classification\r\n");
    CliPrintf (CliHandle, "-------------------------------\r\n");

    if (u4ClassType == CLI_PB_SRCMAC || u4ClassType == ALLTYPE)
    {
        VlanPbShowSrcMacSVlanConf (CliHandle, u4ContextId, &u4PagingStatus);
    }
    if ((u4PagingStatus == CLI_SUCCESS) &&
        (u4ClassType == CLI_PB_DSTMAC || u4ClassType == ALLTYPE))
    {
        VlanPbShowDstMacSVlanConf (CliHandle, u4ContextId, &u4PagingStatus);
    }
    if ((u4PagingStatus == CLI_SUCCESS) &&
        (u4ClassType == CLI_PB_SRCMAC_CVLAN || u4ClassType == ALLTYPE))
    {
        VlanPbShowCVlanSrcMacSVlanConf (CliHandle, u4ContextId,
                                        &u4PagingStatus);
    }
    if ((u4PagingStatus == CLI_SUCCESS) &&
        (u4ClassType == CLI_PB_DSTMAC_CVLAN || u4ClassType == ALLTYPE))
    {
        VlanPbShowCVlanDstMacSVlanConf (CliHandle, u4ContextId,
                                        &u4PagingStatus);
    }
    if ((u4PagingStatus == CLI_SUCCESS) &&
        (u4ClassType == CLI_PB_DSCP || u4ClassType == ALLTYPE))
    {
        VlanPbShowDscpSVlanConf (CliHandle, u4ContextId, &u4PagingStatus);
    }
    if ((u4PagingStatus == CLI_SUCCESS) &&
        (u4ClassType == CLI_PB_DSCP_CVLAN || u4ClassType == ALLTYPE))
    {
        VlanPbShowCVlanDscpSVlanConf (CliHandle, u4ContextId, &u4PagingStatus);
    }
    if ((u4PagingStatus == CLI_SUCCESS) &&
        (u4ClassType == CLI_PB_SRCIP || u4ClassType == ALLTYPE))
    {
        VlanPbShowSrcIPSVlanConf (CliHandle, u4ContextId, &u4PagingStatus);
    }
    if ((u4PagingStatus == CLI_SUCCESS) &&
        (u4ClassType == CLI_PB_DSTIP || u4ClassType == ALLTYPE))
    {
        VlanPbShowDstIPSVlanConf (CliHandle, u4ContextId, &u4PagingStatus);
    }
    if ((u4PagingStatus == CLI_SUCCESS) &&
        (u4ClassType == CLI_PB_SRCIP_DSTIP || u4ClassType == ALLTYPE))
    {
        VlanPbShowSrcDstIPSVlanConf (CliHandle, u4ContextId, &u4PagingStatus);
    }
    if ((u4PagingStatus == CLI_SUCCESS) &&
        (u4ClassType == CLI_PB_DSTIP_CVLAN || u4ClassType == ALLTYPE))
    {
        VlanPbShowCVlanDstIPSVlanConf (CliHandle, u4ContextId, &u4PagingStatus);
    }
    if ((u4PagingStatus == CLI_SUCCESS) &&
        (u4ClassType == CLI_PB_CVLAN || u4ClassType == ALLTYPE))
    {
        VlanPbShowCVlanSVlanConf (CliHandle, u4ContextId, &u4PagingStatus);
    }
    if ((u4PagingStatus == CLI_SUCCESS) &&
        (u4ClassType == CLI_PB_PVID || u4ClassType == ALLTYPE))
    {
        VlanPbShowPvidSVlanConf (CliHandle, u4ContextId, &u4PagingStatus);
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowSVlanMapping                            */
/*                                                                          */
/*     DESCRIPTION      : This function will Display SVlan Mapping          */
/*                        Information                                       */
/*                                                                          */
/*     INPUT            : i4PortId    - Port Index                          */
/*                        u4ContextId - Switch Context Id                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbShowSVlanMapping (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4PortId)
{
    INT4                i4CurrentPort = 0;
    INT4                i4NextPortId;
    INT4                i4LocalSVlanId = 0;
    INT4                i4LocalNextSVlanId;
    INT4                i4RelaySVlanId;
    UINT4               u4TempContextId;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT2               u2LocalPortId;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = TRUE;

    if (nmhGetFirstIndexDot1adMIVidTranslationTable
        (&i4NextPortId, &i4LocalNextSVlanId) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nService Vlan Mapping\r\n");
        CliPrintf (CliHandle, "-------------------------\r\n");
        do
        {
            if (VlanGetContextInfoFromIfIndex
                ((UINT4) i4NextPortId, &u4TempContextId,
                 &u2LocalPortId) == VLAN_SUCCESS)
            {

                if (((i4PortId != 0) && (i4NextPortId == i4PortId))
                    || ((i4PortId == 0) && (u4TempContextId == u4ContextId)))
                {

                    if (i4CurrentPort != i4NextPortId)
                    {
                        MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
                        VlanCfaCliGetIfName (i4NextPortId, (INT1 *) au1NameStr);
                        CliPrintf (CliHandle, "Port %s\r\n", au1NameStr);
                        CliPrintf (CliHandle, "-----------\r\n");
                        CliPrintf (CliHandle, "%20s%20s\r\n",
                                   " Local service vlan", "Relay service vlan");
                    }
                    nmhGetDot1adMIVidTranslationRelayVid (i4NextPortId,
                                                          i4LocalNextSVlanId,
                                                          &i4RelaySVlanId);
                    u4PagingStatus =
                        CliPrintf (CliHandle, " %10d%20d\r\n",
                                   i4LocalNextSVlanId, i4RelaySVlanId);
                }
            }
            i4CurrentPort = i4NextPortId;
            i4LocalSVlanId = i4LocalNextSVlanId;

            if (nmhGetNextIndexDot1adMIVidTranslationTable
                (i4CurrentPort, &i4NextPortId, i4LocalSVlanId,
                 &i4LocalNextSVlanId) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowEtherTypeMapping                        */
/*                                                                          */
/*     DESCRIPTION      : This function will Display EtherType Mapping      */
/*                        Information                                       */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4ContextId - Switch Context Id                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbShowEtherTypeMapping (tCliHandle CliHandle, UINT4 u4ContextId,
                            INT4 i4PortId)
{
    INT4                i4CurrentPort = 0;
    INT4                i4NextPortId;
    INT4                i4LocalEtherType = 0;
    INT4                i4LocalNextEtherType;
    INT4                i4RelayEtherType;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    if (nmhGetFirstIndexFsMIPbEtherTypeSwapTable
        (&i4NextPortId, &i4LocalNextEtherType) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nEtherType Mapping\r\n");
        CliPrintf (CliHandle, "-----------------------\r\n");
        do
        {
            if (VlanGetContextInfoFromIfIndex
                ((UINT4) i4NextPortId, &u4TempContextId,
                 &u2LocalPortId) == VLAN_SUCCESS)
            {

                if (((i4PortId != 0) && (i4NextPortId == i4PortId))
                    || ((i4PortId == 0) && (u4TempContextId == u4ContextId)))
                {
                    if (i4CurrentPort != i4NextPortId)
                    {
                        MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
                        VlanCfaCliGetIfName (i4NextPortId, (INT1 *) au1NameStr);
                        CliPrintf (CliHandle, "Port %s\r\n", au1NameStr);
                        CliPrintf (CliHandle, "-----------\r\n");
                        CliPrintf (CliHandle, " %20s%20s\r\n",
                                   "Local EtherType", "Relay EtherType");

                    }

                    nmhGetFsMIPbRelayEtherType (i4NextPortId,
                                                i4LocalNextEtherType,
                                                &i4RelayEtherType);
                    u4PagingStatus =
                        CliPrintf (CliHandle, " %10s%x%20s%x\r\n", "0x",
                                   i4LocalNextEtherType, "0x",
                                   i4RelayEtherType);
                }
            }
            i4CurrentPort = i4NextPortId;
            i4LocalEtherType = i4LocalNextEtherType;

            if (nmhGetNextIndexFsMIPbEtherTypeSwapTable
                (i4CurrentPort, &i4NextPortId, i4LocalEtherType,
                 &i4LocalNextEtherType) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt, no more print required, exit */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll == TRUE);
    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowProviderBridgePortConfig                */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Protvider-Bridge Port  */
/*                        Information                                       */
/*                                                                          */
/*     INPUT            : i4PortId    - Port Index                          */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbShowProviderBridgePortConfig (tCliHandle CliHandle, INT4 i4PortId,
                                    UINT4 u4ContextId)
{
    INT4                i4PortSVlanClass = VLAN_INIT_VAL;
    INT4                i4EtherTypeSwapStatus = VLAN_INIT_VAL;
    INT4                i4SVlanTranslationStatus = VLAN_INIT_VAL;
    INT4                i4UnicastMacLearning = VLAN_INIT_VAL;
    INT4                i4ReqDropEncoding = VLAN_INIT_VAL;
    INT4                i4UseDei = VLAN_INIT_VAL;
    INT4                i4PcpSelectionRow = VLAN_INIT_VAL;
    INT4                i4SVlanPriorityType = VLAN_INIT_VAL;
    INT4                i4SVlanPriority = VLAN_INIT_VAL;
    UINT4               u4UnicastMacLimit = VLAN_INIT_VAL;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4CVlanStatus = 0;
    INT4                i4TunnelStatus = 0;
    INT4                i4CVlanId = 0;
    UINT1               u1BrgPortType = VLAN_PROVIDER_NETWORK_PORT;
    UINT1               u1InterfaceType = 0;
    INT4                i4BridgeMode = VLAN_CUSTOMER_BRIDGE_MODE;
#ifdef NPAPI_WANTED
    INT4                i4UntagFrameOnCEP = 0;
#endif

    nmhGetFsMIVlanBridgeMode ((INT4) u4ContextId, &i4BridgeMode);

    VlanCfaCliGetIfName (i4PortId, (INT1 *) au1NameStr);
    CliPrintf (CliHandle, "Port %s\r\n", au1NameStr);

    VlanL2IwfGetPbPortType ((UINT4) i4PortId, &u1BrgPortType);

    nmhGetFsMIPbPortSVlanClassificationMethod (i4PortId, &i4PortSVlanClass);
    nmhGetFsMIPbPortSVlanEtherTypeSwapStatus (i4PortId, &i4EtherTypeSwapStatus);
    nmhGetFsMIPbPortSVlanTranslationStatus (i4PortId,
                                            &i4SVlanTranslationStatus);
    nmhGetDot1adMIPortReqDropEncoding (i4PortId, &i4ReqDropEncoding);
    nmhGetDot1adMIPortUseDei (i4PortId, &i4UseDei);
    nmhGetDot1adMIPortPcpSelectionRow (i4PortId, &i4PcpSelectionRow);
    nmhGetFsMIPbPortUnicastMacLearning (i4PortId, &i4UnicastMacLearning);
    nmhGetFsMIPbPortUnicastMacLimit (i4PortId, &u4UnicastMacLimit);
    nmhGetDot1adMIPortSVlanPriorityType (i4PortId, &i4SVlanPriorityType);
    nmhGetDot1adMIPortSVlanPriority (i4PortId, &i4SVlanPriority);

    CliPrintf (CliHandle, " Port Type                         " "  :");
    switch (u1BrgPortType)
    {
        case VLAN_PROVIDER_NETWORK_PORT:
            CliPrintf (CliHandle, " Provider Network Port\r\n");
            break;
        case VLAN_CNP_PORTBASED_PORT:
            CliPrintf (CliHandle, " Customer Network Port(Port-Based)\r\n");
            break;
        case VLAN_CNP_TAGGED_PORT:
            if ((i4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE) ||
                (i4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
            {
                if (VlanL2IwfGetInterfaceType (u4ContextId,
                                               &u1InterfaceType) !=
                    VLAN_FAILURE)
                {

                    if (u1InterfaceType == VLAN_S_INTERFACE_TYPE)
                    {
                        CliPrintf (CliHandle,
                                   " Customer Network Port(Stag-Based)\r\n");
                    }

                    else if (u1InterfaceType == VLAN_C_INTERFACE_TYPE)
                    {
                        CliPrintf (CliHandle,
                                   " Customer Network Port(Ctag-Based)\r\n");
                    }
                }
            }
            else
            {
                CliPrintf (CliHandle, " Customer Network Port(Stag-Based)\r\n");
            }
            break;
        case VLAN_CUSTOMER_EDGE_PORT:
            CliPrintf (CliHandle, " Customer Edge Port\r\n");
            break;
        case VLAN_PROP_CUSTOMER_EDGE_PORT:
            CliPrintf (CliHandle, " Prop Customer Edge Port\r\n");
            break;
        case VLAN_PROP_CUSTOMER_NETWORK_PORT:
            CliPrintf (CliHandle, " Prop  Customer Network Port\r\n");
            break;
        case VLAN_PROP_PROVIDER_NETWORK_PORT:
            CliPrintf (CliHandle, " Prop Provider Network Port\r\n");
            break;
        case VLAN_CUSTOMER_BACKBONE_PORT:
            CliPrintf (CliHandle, " Customer Backbone Port\r\n");
            break;
        case VLAN_PROVIDER_INSTANCE_PORT:
            CliPrintf (CliHandle, " Provider Instance Port\r\n");
            break;
        case VLAN_VIRTUAL_INSTANCE_PORT:
            CliPrintf (CliHandle, " Virtual Instance Port\r\n");
            break;
        default:
            CliPrintf (CliHandle, " Unknown\r\n");
            break;
    }
    if (VlanMiPbIsTunnelingValid ((UINT4) i4PortId) == VLAN_TRUE)
    {
        nmhGetFsMIVlanTunnelProtocolDot1x (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_DOT1X);

        nmhGetFsMIVlanTunnelProtocolLacp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_LACP);

        nmhGetFsMIVlanTunnelProtocolStp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_STP);

        nmhGetFsMIVlanTunnelProtocolMvrp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_MVRP);

        nmhGetFsMIVlanTunnelProtocolMmrp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_MMRP);

        nmhGetFsMIVlanTunnelProtocolGvrp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_GVRP);

        nmhGetFsMIVlanTunnelProtocolGmrp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_GMRP);

        nmhGetFsMIVlanTunnelProtocolIgmp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_IGMP);

        nmhGetFsMIVlanTunnelProtocolElmi (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_ELMI);

        nmhGetFsMIVlanTunnelProtocolLldp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_LLDP);

        nmhGetFsMIVlanTunnelProtocolEcfm (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_ECFM);

        nmhGetFsMIVlanTunnelProtocolEoam (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_EOAM);
    }

    CliPrintf (CliHandle, " Service Vlan Classification         :");
    switch (i4PortSVlanClass)
    {
        case CLI_PB_SRCMAC:
            CliPrintf (CliHandle, " Source Mac Address\r\n");
            break;
        case CLI_PB_DSTMAC:
            CliPrintf (CliHandle, " Destination Mac Address\r\n");
            break;
        case CLI_PB_SRCMAC_CVLAN:
            CliPrintf (CliHandle, " Source Mac and Customer Vlan\r\n");
            break;
        case CLI_PB_DSTMAC_CVLAN:
            CliPrintf (CliHandle, " Destination Mac and Customer Vlan\r\n");
            break;
        case CLI_PB_DSCP:
            CliPrintf (CliHandle, " DSCP\r\n");
            break;
        case CLI_PB_DSCP_CVLAN:
            CliPrintf (CliHandle, " DSCP and Cutomer Vlan\r\n");
            break;
        case CLI_PB_SRCIP:
            CliPrintf (CliHandle, " Source IP Address\r\n");
            break;
        case CLI_PB_DSTIP:
            CliPrintf (CliHandle, " Destination IP " "Address\r\n");
            break;
        case CLI_PB_SRCIP_DSTIP:
            CliPrintf (CliHandle, " Source IP and Destination IP Address\r\n");
            break;
        case CLI_PB_DSTIP_CVLAN:
            CliPrintf (CliHandle, " Destination IP and Customer Vlan\r\n");
            break;
        case CLI_PB_CVLAN:
            CliPrintf (CliHandle, " Customer Vlan\r\n");
            break;
        case CLI_PB_PVID:
            CliPrintf (CliHandle, " PVID\r\n");
            break;
        default:
            CliPrintf (CliHandle, " \r\n");
            break;
    }
    CliPrintf (CliHandle, " EtherType Swap Status             " "  :");

    if (i4EtherTypeSwapStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, " Enable\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Disable\r\n");
    }

    CliPrintf (CliHandle, " Service Vlan Translation Status     :");

    if (i4SVlanTranslationStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, " Enable\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Disable\r\n");
    }
    CliPrintf (CliHandle, " Require Drop Encoding               :");
    if (i4ReqDropEncoding == VLAN_TRUE)
    {
        CliPrintf (CliHandle, " True\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " False \r\n");
    }

    CliPrintf (CliHandle, " Use_Dei                             :");
    if (i4UseDei == VLAN_SNMP_TRUE)
    {
        CliPrintf (CliHandle, " True\r\n");
    }
    else if (i4UseDei == VLAN_SNMP_FALSE)
    {
        CliPrintf (CliHandle, " False\r\n");
    }

    CliPrintf (CliHandle, " PCP Selection Row                   : ");

    switch (i4PcpSelectionRow)
    {
        case VLAN_8P0D_SEL_ROW:
            CliPrintf (CliHandle, "8P0D \r\n");
            break;
        case VLAN_7P1D_SEL_ROW:
            CliPrintf (CliHandle, "7P1D \r\n");
            break;
        case VLAN_6P2D_SEL_ROW:
            CliPrintf (CliHandle, "6P2D \r\n");
            break;
        case VLAN_5P3D_SEL_ROW:
            CliPrintf (CliHandle, "5P3D \r\n");
            break;
        default:
            CliPrintf (CliHandle, " \r\n");
            break;
    }

    CliPrintf (CliHandle, " Unicast Mac Learning Status       " "  :");

    if (i4UnicastMacLearning == CLI_PB_ENABLED)
    {
        CliPrintf (CliHandle, " Enable\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Disable\r\n");
    }
    /* When HwUnicastMacLearningLimitSup is supported to 
     * display unicast mac learning limit values
     */
    if (ISS_HW_SUPPORTED ==
        IssGetHwCapabilities (ISS_HW_UNICAST_MAC_LEARNING_LIMIT))
    {
        CliPrintf (CliHandle,
                   " Unicast Mac Learning Limit          : %d\r\n",
                   u4UnicastMacLimit);
    }

    if ((u1BrgPortType == VLAN_CNP_TAGGED_PORT) ||
        (u1BrgPortType == VLAN_CNP_PORTBASED_PORT))
    {
        CliPrintf (CliHandle, " SVLAN Priority Type                 : ");
        switch (i4SVlanPriorityType)
        {
            case VLAN_SVLAN_PRIORITY_TYPE_NONE:
                CliPrintf (CliHandle, "NONE \r\n");
                break;
            case VLAN_SVLAN_PRIORITY_TYPE_FIXED:
                CliPrintf (CliHandle, "FIXED \r\n");
                CliPrintf (CliHandle,
                           " SVLAN Priority                      : %d\r\n",
                           i4SVlanPriority);
                break;
            case VLAN_SVLAN_PRIORITY_TYPE_COPY:
                CliPrintf (CliHandle, "COPY \r\n");
                break;
            default:
                break;
        }
    }

    if (nmhValidateIndexInstanceFsMIPbPortBasedCVlanTable
        (i4PortId) == SNMP_SUCCESS)
    {
        nmhGetFsMIPbPortCVlanClassifyStatus (i4PortId, &i4CVlanStatus);
        nmhGetFsMIPbPortCVlan (i4PortId, &i4CVlanId);
#ifdef NPAPI_WANTED
        nmhGetFsMIPbPortEgressUntaggedStatus (i4PortId, &i4UntagFrameOnCEP);
#endif

        if (i4CVlanStatus == VLAN_ENABLED)
        {
            CliPrintf (CliHandle,
                       " Customer Vlan                       " ": %d\r\n",
                       i4CVlanId);
            CliPrintf (CliHandle,
                       " Customer Vlan Status                " ": Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " Customer Vlan Status                "
                       ": Disabled\r\n");
        }
#ifdef NPAPI_WANTED
        if (i4UntagFrameOnCEP == VLAN_ENABLED)
        {
            CliPrintf (CliHandle, " Egress Untag from CEP status        :"
                       " Allow\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " Egress Untag from CEP status        :"
                       " Deny\r\n");
        }
#endif
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowMulticastLimit                          */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Multicast configuration*/
/*                        Information                                       */
/*                                                                          */
/*     INPUT            : i4PortId    - Port Index                          */
/*                        u4ContextId - Switch Context Id                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbShowMulticastLimit (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4MulticastMacLimit = 0;

    nmhGetFsMIPbMulticastMacLimit (u4ContextId, &u4MulticastMacLimit);
    CliPrintf (CliHandle, "%-33s : %d\r\n", "Multicast Mac Limit",
               u4MulticastMacLimit);
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowPcpEncodingTable                        */
/*                                                                          */
/*     DESCRIPTION      : This function will Display PCP Decoding table     */
/*                                                                          */
/*     INPUT            : i4PortId    - Port Index                          */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbShowPcpEncodingTable (tCliHandle CliHandle, INT4 i4PortId)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    INT4                i4CurrPortId;
    INT4                i4CurrPcpSelRow;
    INT4                i4CurrPriority;
    INT4                i4CurrDropEligible;

    INT4                i4CurrPcpValue;

    INT4                i4NextPortId;
    INT4                i4NextPcpSelRow;
    INT4                i4NextPriority;
    INT4                i4NextDropEligible;

    UINT1               u1Priority = 0;
    UINT1               u1PrintSelRowFlag = VLAN_TRUE;
    UINT1               u1PrintPortFlag = VLAN_TRUE;

    i4CurrPortId = i4PortId;
    i4CurrPcpSelRow = 0;
    i4CurrPriority = 0;
    i4CurrDropEligible = VLAN_FALSE;

    while (nmhGetNextIndexDot1adMIPcpEncodingTable (i4CurrPortId, &i4NextPortId,
                                                    i4CurrPcpSelRow,
                                                    &i4NextPcpSelRow,
                                                    i4CurrPriority,
                                                    &i4NextPriority,
                                                    i4CurrDropEligible,
                                                    &i4NextDropEligible) ==
           SNMP_SUCCESS)
    {

        if (i4CurrPortId != i4NextPortId)
        {
            break;
        }

        if (u1PrintPortFlag == VLAN_TRUE)
        {
            /*Print the Port Name first */
            VlanCfaCliGetIfName (i4NextPortId, (INT1 *) au1NameStr);
            CliPrintf (CliHandle, "\r\nPort %s\r\n", au1NameStr);
            CliPrintf (CliHandle, "-----------\r\n");
            /*Printing the Priority & DE Values */
            CliPrintf (CliHandle, "DropEligible: ");
            for (u1Priority = 0; u1Priority <= VLAN_HIGHEST_PRIORITY;
                 u1Priority++)
            {
                CliPrintf (CliHandle, "%d%-3s", u1Priority, "DE");
                CliPrintf (CliHandle, "%-2d", u1Priority);

            }
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "Priority    :\r\n");
            CliPrintf (CliHandle,
                       "---------------------------------------------------------------\r\n");
            u1PrintPortFlag = VLAN_FALSE;
        }

        if (u1PrintSelRowFlag == VLAN_TRUE)
        {
            switch (i4NextPcpSelRow)
            {
                case VLAN_8P0D_SEL_ROW:
                    CliPrintf (CliHandle, "8P0D        : ");
                    break;
                case VLAN_7P1D_SEL_ROW:
                    CliPrintf (CliHandle, "7P1D        : ");
                    break;
                case VLAN_6P2D_SEL_ROW:
                    CliPrintf (CliHandle, "6P2D        : ");
                    break;
                case VLAN_5P3D_SEL_ROW:
                    CliPrintf (CliHandle, "5P3D        : ");
                    break;
                default:
                    break;
            }
            u1PrintSelRowFlag = VLAN_FALSE;
        }

        nmhGetDot1adMIPcpEncodingPcpValue (i4NextPortId, i4NextPcpSelRow,
                                           i4NextPriority, i4NextDropEligible,
                                           &i4CurrPcpValue);

        CliPrintf (CliHandle, "%-3d", i4CurrPcpValue);
        if ((i4NextPriority == VLAN_HIGHEST_PRIORITY) &&
            (i4NextDropEligible == VLAN_SNMP_FALSE))
        {
            u1PrintSelRowFlag = VLAN_TRUE;
            CliPrintf (CliHandle, "\r\n");
        }

        i4CurrPortId = i4NextPortId;
        i4CurrPcpSelRow = i4NextPcpSelRow;
        i4CurrPriority = i4NextPriority;
        i4CurrDropEligible = i4NextDropEligible;

    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowPcpDecodingTable                        */
/*                                                                          */
/*     DESCRIPTION      : This function will Display PCP Decoding table     */
/*                                                                          */
/*     INPUT            : i4PortId    - Port Index                          */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbShowPcpDecodingTable (tCliHandle CliHandle, INT4 i4PortId)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    INT4                i4CurrPortId;
    INT4                i4CurrPcpSelRow;
    INT4                i4CurrPcpValue;

    INT4                i4CurrPriority;
    INT4                i4CurrDropEligible;

    INT4                i4NextPortId;
    INT4                i4NextPcpSelRow;
    INT4                i4NextPcpValue;

    UINT1               u1PcpValue;
    UINT1               u1PrintSelRowFlag = VLAN_TRUE;
    UINT1               u1PrintPortFlag = VLAN_TRUE;

    i4CurrPortId = i4PortId;
    i4CurrPcpSelRow = 0;
    i4CurrPcpValue = 0;

    while (nmhGetNextIndexDot1adMIPcpDecodingTable (i4CurrPortId, &i4NextPortId,
                                                    i4CurrPcpSelRow,
                                                    &i4NextPcpSelRow,
                                                    i4CurrPcpValue,
                                                    &i4NextPcpValue) ==
           SNMP_SUCCESS)
    {

        if (i4CurrPortId != i4NextPortId)
        {
            break;
        }

        if (u1PrintPortFlag == VLAN_TRUE)
        {
            /*Print the Port Name first */
            VlanCfaCliGetIfName (i4NextPortId, (INT1 *) au1NameStr);
            CliPrintf (CliHandle, "\r\nPort %s\r\n", au1NameStr);
            CliPrintf (CliHandle, "-----------\r\n");
            /*Printing the PCP Values */
            CliPrintf (CliHandle, "PCP   : ");
            for (u1PcpValue = 0; u1PcpValue <= VLAN_HIGHEST_PRIORITY;
                 u1PcpValue++)
            {
                CliPrintf (CliHandle, "%-5d", u1PcpValue);
            }
            CliPrintf (CliHandle,
                       "\r\n----------------------------------------------\r\n");

            u1PrintPortFlag = VLAN_FALSE;
        }

        if (u1PrintSelRowFlag == VLAN_TRUE)
        {
            switch (i4NextPcpSelRow)
            {
                case VLAN_8P0D_SEL_ROW:
                    CliPrintf (CliHandle, "8P0D  : ");
                    break;
                case VLAN_7P1D_SEL_ROW:
                    CliPrintf (CliHandle, "7P1D  : ");
                    break;
                case VLAN_6P2D_SEL_ROW:
                    CliPrintf (CliHandle, "6P2D  : ");
                    break;
                case VLAN_5P3D_SEL_ROW:
                    CliPrintf (CliHandle, "5P3D  : ");
                    break;
                default:
                    break;
            }
            u1PrintSelRowFlag = VLAN_FALSE;
        }

        nmhGetDot1adMIPcpDecodingPriority (i4NextPortId, i4NextPcpSelRow,
                                           i4NextPcpValue, &i4CurrPriority);

        nmhGetDot1adMIPcpDecodingDropEligible (i4NextPortId, i4NextPcpSelRow,
                                               i4NextPcpValue,
                                               &i4CurrDropEligible);

        if (i4CurrDropEligible == VLAN_TRUE)
        {
            CliPrintf (CliHandle, "%d%-4s", i4CurrPriority, "DE");
        }
        else
        {
            CliPrintf (CliHandle, "%-5d", i4CurrPriority);
        }

        if (i4NextPcpValue == VLAN_HIGHEST_PRIORITY)
        {
            u1PrintSelRowFlag = VLAN_TRUE;
            CliPrintf (CliHandle, "\r\n");
        }

        i4CurrPortId = i4NextPortId;
        i4CurrPcpSelRow = i4NextPcpSelRow;
        i4CurrPcpValue = i4NextPcpValue;

    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowPriorityRegenTable                      */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Service priority       */
/*                        Regeneration table.                               */
/*                                                                          */
/*     INPUT            : i4PortId    - Port Index                          */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbShowPriorityRegenTable (tCliHandle CliHandle, INT4 i4PortId)
{
    INT4                i4NextPortId = 0;
    INT4                i4CurrentPort = 0;
    INT4                i4NextVlan = 0;
    INT4                i4CurrentVlan = 0;
    INT4                i4NextRecvPriority = 0;
    INT4                i4NextRegenPriority = 0;
    INT4                i4CurrentRecvPriority = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;
    UINT1               u1Entered = VLAN_FALSE;

    if (nmhGetFirstIndexDot1adMIServicePriorityRegenerationTable
        (&i4NextPortId, &i4NextVlan, &i4NextRecvPriority) == SNMP_SUCCESS)
    {

        do
        {
            if (i4NextPortId == i4PortId)
            {
                nmhGetDot1adMIServicePriorityRegenRegeneratedPriority
                    (i4NextPortId, i4NextVlan, i4NextRecvPriority,
                     &i4NextRegenPriority);

                if ((i4CurrentPort != i4NextPortId) ||
                    (i4CurrentVlan != i4NextVlan))
                {
                    VlanCfaCliGetIfName (i4NextPortId, (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, " %s : %-13s", "Port", au1NameStr);
                    CliPrintf (CliHandle, " %s : %-25d\r\n", "Service VLAN-ID",
                               i4NextVlan);
                    CliPrintf (CliHandle, " %-20s", "Receive Priority");
                    CliPrintf (CliHandle, " %-25s\r\n", "Regenerated Priority");
                    CliPrintf (CliHandle,
                               "-----------------     ---------------------\r\n");

                    u1Entered = VLAN_TRUE;
                }

                CliPrintf (CliHandle, "   %-20d", i4NextRecvPriority);
                u4PagingStatus = CliPrintf (CliHandle, "    %-25d\r\n",
                                            i4NextRegenPriority);

            }

            i4CurrentPort = i4NextPortId;
            i4CurrentVlan = i4NextVlan;
            i4CurrentRecvPriority = i4NextRecvPriority;

            if (nmhGetNextIndexDot1adMIServicePriorityRegenerationTable
                (i4CurrentPort, &i4NextPortId, i4CurrentVlan,
                 &i4NextVlan,
                 i4CurrentRecvPriority, &i4NextRecvPriority) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);

        if (u1Entered == VLAN_TRUE)
        {
            CliPrintf (CliHandle, "\r\n");
        }

    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSVlanClassSrcMacBase                        */
/*                                                                          */
/*     DESCRIPTION      : This function will configure/delete 'source mac'  */
/*                        base service vlan                                 */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4SVlanId - service vlan id                       */
/*                        u1SrcMacAddr - source mac address                 */
/*                        u4Flag    - Addition/Delete flag                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSVlanClassSrcMacBase (tCliHandle CliHandle, INT4 i4PortId,
                            UINT4 u4SVlanId, tMacAddr u1SrcMacAddr,
                            UINT4 u4Flag)
{
    UINT4               u4ErrCode;
    INT4                i4FsPbSrcMacStatus;
    INT4                i4RetFsPbSrcMacStatus;
    INT4                i4RetStatus;
    INT4                i4VlanId = 0;

    if (u4Flag == CLI_ADD)
    {
        i4RetStatus =
            nmhGetFsPbSrcMacRowStatus (i4PortId, u1SrcMacAddr,
                                       &i4RetFsPbSrcMacStatus);

        if (i4RetStatus == SNMP_FAILURE)
        {
            /*Create Entry */
            i4FsPbSrcMacStatus = VLAN_CREATE_AND_WAIT;
        }
        else
        {
            i4RetStatus =
                nmhGetFsPbSrcMacSVlan (i4PortId, u1SrcMacAddr, &i4VlanId);

            if ((UINT4) i4VlanId == u4SVlanId)
            {
                return CLI_SUCCESS;
            }

            i4FsPbSrcMacStatus = VLAN_NOT_IN_SERVICE;
        }

        if (nmhTestv2FsPbSrcMacRowStatus
            (&u4ErrCode, i4PortId, u1SrcMacAddr,
             i4FsPbSrcMacStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbSrcMacRowStatus
            (i4PortId, u1SrcMacAddr, i4FsPbSrcMacStatus) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbSrcMacSVlan
            (&u4ErrCode, i4PortId, u1SrcMacAddr,
             (INT4) u4SVlanId) != SNMP_SUCCESS)
        {
            if (i4FsPbSrcMacStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbSrcMacRowStatus
                    (i4PortId, u1SrcMacAddr, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbSrcMacRowStatus (i4PortId, u1SrcMacAddr,
                                                i4RetFsPbSrcMacStatus)
                     != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            return CLI_FAILURE;
        }
        if (nmhSetFsPbSrcMacSVlan (i4PortId, u1SrcMacAddr, (INT4) u4SVlanId) !=
            SNMP_SUCCESS)
        {
            if (i4FsPbSrcMacStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbSrcMacRowStatus
                    (i4PortId, u1SrcMacAddr, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbSrcMacRowStatus (i4PortId, u1SrcMacAddr,
                                                i4RetFsPbSrcMacStatus)
                     != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (i4FsPbSrcMacStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbSrcMacRowStatus (i4PortId, u1SrcMacAddr, VLAN_ACTIVE)
                != SNMP_SUCCESS)
            {
                if (nmhSetFsPbSrcMacRowStatus
                    (i4PortId, u1SrcMacAddr, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else if (nmhSetFsPbSrcMacRowStatus (i4PortId, u1SrcMacAddr,
                                            i4RetFsPbSrcMacStatus)
                 != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2FsPbSrcMacRowStatus
            (&u4ErrCode, i4PortId, u1SrcMacAddr, VLAN_DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbSrcMacRowStatus (i4PortId, u1SrcMacAddr, VLAN_DESTROY) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSVlanClassSrcMacCVlanBase                   */
/*                                                                          */
/*     DESCRIPTION      : This function will configure/delete 'source mac   */
/*                        and customer vlan' base service vlan              */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4SVlanId - service vlan id                       */
/*                        u1SrcMacAddr - source mac address                 */
/*                        u4CVlanId - customer vlan id                      */
/*                        u4Flag    - Addition/Delete flag                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSVlanClassSrcMacCVlanBase (tCliHandle CliHandle, INT4 i4PortId,
                                 UINT4 u4SVlanId, UINT4 u4CVlanId,
                                 tMacAddr u1SrcMacAddr, UINT4 u4Flag)
{
    UINT4               u4ErrCode;
    INT4                i4FsPbCVlanSrcMacStatus;
    INT4                i4RetFsPbCVlanSrcMacStatus;
    INT4                i4RetStatus;
    INT4                i4VlanId = 0;

    if (u4Flag == CLI_ADD)
    {
        i4RetStatus =
            nmhGetFsPbCVlanSrcMacRowStatus (i4PortId, u4CVlanId, u1SrcMacAddr,
                                            &i4RetFsPbCVlanSrcMacStatus);

        if (i4RetStatus == SNMP_FAILURE)
        {
            /*Create Entry */
            i4FsPbCVlanSrcMacStatus = VLAN_CREATE_AND_WAIT;
        }
        else
        {
            i4RetStatus =
                nmhGetFsPbCVlanSrcMacSVlan (i4PortId, u4CVlanId, u1SrcMacAddr,
                                            &i4VlanId);

            if ((UINT4) i4VlanId == u4SVlanId)
            {
                return CLI_SUCCESS;
            }

            i4FsPbCVlanSrcMacStatus = VLAN_NOT_IN_SERVICE;
        }

        if (nmhTestv2FsPbCVlanSrcMacRowStatus
            (&u4ErrCode, i4PortId, u4CVlanId, u1SrcMacAddr,
             i4FsPbCVlanSrcMacStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbCVlanSrcMacRowStatus
            (i4PortId, u4CVlanId, u1SrcMacAddr,
             i4FsPbCVlanSrcMacStatus) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbCVlanSrcMacSVlan
            (&u4ErrCode, i4PortId, u4CVlanId, u1SrcMacAddr,
             (INT4) u4SVlanId) != SNMP_SUCCESS)
        {
            if (i4FsPbCVlanSrcMacStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbCVlanSrcMacRowStatus
                    (i4PortId, u4CVlanId, u1SrcMacAddr,
                     VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbCVlanSrcMacRowStatus
                     (i4PortId, u4CVlanId, u1SrcMacAddr,
                      i4RetFsPbCVlanSrcMacStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            return CLI_FAILURE;
        }
        if (nmhSetFsPbCVlanSrcMacSVlan
            (i4PortId, u4CVlanId, u1SrcMacAddr, (INT4) u4SVlanId)
            != SNMP_SUCCESS)
        {
            if (i4FsPbCVlanSrcMacStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbCVlanSrcMacRowStatus
                    (i4PortId, u4CVlanId, u1SrcMacAddr,
                     VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbCVlanSrcMacRowStatus
                     (i4PortId, u4CVlanId, u1SrcMacAddr,
                      i4RetFsPbCVlanSrcMacStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (i4FsPbCVlanSrcMacStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbCVlanSrcMacRowStatus
                (i4PortId, u4CVlanId, u1SrcMacAddr,
                 VLAN_ACTIVE) != SNMP_SUCCESS)
            {

                if (nmhSetFsPbCVlanSrcMacRowStatus
                    (i4PortId, u4CVlanId, u1SrcMacAddr,
                     VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

        }
        else if (nmhSetFsPbCVlanSrcMacRowStatus
                 (i4PortId, u4CVlanId, u1SrcMacAddr,
                  i4RetFsPbCVlanSrcMacStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2FsPbCVlanSrcMacRowStatus
            (&u4ErrCode, i4PortId, u4CVlanId, u1SrcMacAddr,
             VLAN_DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbCVlanSrcMacRowStatus
            (i4PortId, u4CVlanId, u1SrcMacAddr, VLAN_DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSVlanClassDstMacBase                        */
/*                                                                          */
/*     DESCRIPTION      : This function will configure/delete 'destination  */
/*                        mac' base service vlan                            */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4SVlanId - service vlan id                       */
/*                        u1DstMacAddr - destination mac address            */
/*                        u4Flag    - Addition/Delete flag                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSVlanClassDstMacBase (tCliHandle CliHandle, INT4 i4PortId,
                            UINT4 u4SVlanId, tMacAddr u1DstMacAddr,
                            UINT4 u4Flag)
{
    UINT4               u4ErrCode;
    INT4                i4FsPbDstMacStatus;
    INT4                i4RetFsPbDstMacStatus;
    INT4                i4RetStatus;
    INT4                i4VlanId = 0;

    if (u4Flag == CLI_ADD)
    {
        i4RetStatus =
            nmhGetFsPbDstMacRowStatus (i4PortId, u1DstMacAddr,
                                       &i4RetFsPbDstMacStatus);

        if (i4RetStatus == SNMP_FAILURE)
        {
            /*Create Entry */
            i4FsPbDstMacStatus = VLAN_CREATE_AND_WAIT;
        }
        else
        {
            i4RetStatus = nmhGetFsPbDstMacSVlan (i4PortId, u1DstMacAddr,
                                                 &i4VlanId);

            if ((UINT4) i4VlanId == u4SVlanId)
            {
                return CLI_SUCCESS;
            }
            i4FsPbDstMacStatus = VLAN_NOT_IN_SERVICE;
        }

        if (nmhTestv2FsPbDstMacRowStatus
            (&u4ErrCode, i4PortId, u1DstMacAddr,
             i4FsPbDstMacStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbDstMacRowStatus
            (i4PortId, u1DstMacAddr, i4FsPbDstMacStatus) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbDstMacSVlan
            (&u4ErrCode, i4PortId, u1DstMacAddr, (INT4) u4SVlanId)
            != SNMP_SUCCESS)
        {
            if (i4FsPbDstMacStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbDstMacRowStatus
                    (i4PortId, u1DstMacAddr, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbDstMacRowStatus
                     (i4PortId, u1DstMacAddr,
                      i4RetFsPbDstMacStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            return CLI_FAILURE;
        }
        if (nmhSetFsPbDstMacSVlan (i4PortId, u1DstMacAddr, (INT4) u4SVlanId)
            != SNMP_SUCCESS)
        {
            if (i4FsPbDstMacStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbDstMacRowStatus
                    (i4PortId, u1DstMacAddr, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbDstMacRowStatus
                     (i4PortId, u1DstMacAddr,
                      i4RetFsPbDstMacStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (i4FsPbDstMacStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbDstMacRowStatus (i4PortId, u1DstMacAddr, VLAN_ACTIVE)
                != SNMP_SUCCESS)
            {
                if (nmhSetFsPbDstMacRowStatus
                    (i4PortId, u1DstMacAddr, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else if (nmhSetFsPbDstMacRowStatus
                 (i4PortId, u1DstMacAddr,
                  i4RetFsPbDstMacStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2FsPbDstMacRowStatus
            (&u4ErrCode, i4PortId, u1DstMacAddr, VLAN_DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbDstMacRowStatus (i4PortId, u1DstMacAddr, VLAN_DESTROY) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSVlanClassDstMacCVlanBase                   */
/*                                                                          */
/*     DESCRIPTION      : This function will configure/delete 'destination  */
/*                        mac and customer vlan' base service vlan          */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4SVlanId - service vlan id                       */
/*                        u1DstMacAddr - source mac address                 */
/*                        u4CVlanId - customer vlan id                      */
/*                        u4Flag    - Addition/Delete flag                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSVlanClassDstMacCVlanBase (tCliHandle CliHandle, INT4 i4PortId,
                                 UINT4 u4SVlanId, UINT4 u4CVlanId,
                                 tMacAddr u1DstMacAddr, UINT4 u4Flag)
{
    UINT4               u4ErrCode;
    INT4                i4FsPbCVlanDstMacStatus;
    INT4                i4RetFsPbCVlanDstMacStatus;
    INT4                i4RetStatus;
    INT4                i4VlanId = 0;

    if (u4Flag == CLI_ADD)
    {
        i4RetStatus =
            nmhGetFsPbCVlanDstMacRowStatus (i4PortId, u4CVlanId, u1DstMacAddr,
                                            &i4RetFsPbCVlanDstMacStatus);

        if (i4RetStatus == SNMP_FAILURE)
        {
            /*Create Entry */
            i4FsPbCVlanDstMacStatus = VLAN_CREATE_AND_WAIT;
        }
        else
        {
            i4RetStatus = nmhGetFsPbCVlanDstMacSVlan (i4PortId, u4CVlanId,
                                                      u1DstMacAddr, &i4VlanId);

            if (i4VlanId == (INT4) u4SVlanId)
            {
                return CLI_SUCCESS;
            }
            i4FsPbCVlanDstMacStatus = VLAN_NOT_IN_SERVICE;
        }

        if (nmhTestv2FsPbCVlanDstMacRowStatus
            (&u4ErrCode, i4PortId, u4CVlanId, u1DstMacAddr,
             i4FsPbCVlanDstMacStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbCVlanDstMacRowStatus
            (i4PortId, u4CVlanId, u1DstMacAddr,
             i4FsPbCVlanDstMacStatus) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbCVlanDstMacSVlan
            (&u4ErrCode, i4PortId, u4CVlanId, u1DstMacAddr,
             (INT4) u4SVlanId) != SNMP_SUCCESS)
        {

            if (i4FsPbCVlanDstMacStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbCVlanDstMacRowStatus
                    (i4PortId, u4CVlanId, u1DstMacAddr,
                     VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbCVlanDstMacRowStatus
                     (i4PortId, u4CVlanId, u1DstMacAddr,
                      i4RetFsPbCVlanDstMacStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            return CLI_FAILURE;
        }
        if (nmhSetFsPbCVlanDstMacSVlan
            (i4PortId, u4CVlanId, u1DstMacAddr, (INT4) u4SVlanId)
            != SNMP_SUCCESS)
        {
            if (i4FsPbCVlanDstMacStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbCVlanDstMacRowStatus
                    (i4PortId, u4CVlanId, u1DstMacAddr,
                     VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbCVlanDstMacRowStatus
                     (i4PortId, u4CVlanId, u1DstMacAddr,
                      i4RetFsPbCVlanDstMacStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (i4FsPbCVlanDstMacStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbCVlanDstMacRowStatus
                (i4PortId, u4CVlanId, u1DstMacAddr,
                 VLAN_ACTIVE) != SNMP_SUCCESS)
            {
                if (nmhSetFsPbCVlanDstMacRowStatus
                    (i4PortId, u4CVlanId, u1DstMacAddr,
                     VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else if (nmhSetFsPbCVlanDstMacRowStatus
                 (i4PortId, u4CVlanId, u1DstMacAddr,
                  i4RetFsPbCVlanDstMacStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2FsPbCVlanDstMacRowStatus
            (&u4ErrCode, i4PortId, u4CVlanId, u1DstMacAddr,
             VLAN_DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbCVlanDstMacRowStatus
            (i4PortId, u4CVlanId, u1DstMacAddr, VLAN_DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSVlanClassDSCPBase                          */
/*                                                                          */
/*     DESCRIPTION      : This function will configure/delete 'Dscp'        */
/*                        base service vlan                                 */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4SVlanId - service vlan id                       */
/*                        DSCPVlaue - DSCP Vlaue                            */
/*                        u4Flag    - Addition/Delete flag                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSVlanClassDSCPBase (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4SVlanId,
                          UINT4 DSCPValue, UINT4 u4Flag)
{
    UINT4               u4ErrCode;
    INT4                i4FsPbDscpStatus;
    INT4                i4RetFsPbDscpStatus;
    INT4                i4RetStatus;
    INT4                i4VlanId = 0;

    if (u4Flag == CLI_ADD)
    {
        i4RetStatus =
            nmhGetFsPbDscpRowStatus (i4PortId, DSCPValue, &i4RetFsPbDscpStatus);

        if (i4RetStatus == SNMP_FAILURE)
        {
            /*Create Entry */
            i4FsPbDscpStatus = VLAN_CREATE_AND_WAIT;
        }
        else
        {
            i4RetStatus = nmhGetFsPbDscpSVlan (i4PortId, DSCPValue, &i4VlanId);

            if ((UINT4) i4VlanId == u4SVlanId)
            {
                return CLI_SUCCESS;
            }
            i4FsPbDscpStatus = VLAN_NOT_IN_SERVICE;
        }

        if (nmhTestv2FsPbDscpRowStatus
            (&u4ErrCode, i4PortId, DSCPValue, i4FsPbDscpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbDscpRowStatus (i4PortId, DSCPValue, i4FsPbDscpStatus) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbDscpSVlan (&u4ErrCode, i4PortId, DSCPValue,
                                    (INT4) u4SVlanId) != SNMP_SUCCESS)
        {
            if (i4FsPbDscpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbDscpRowStatus (i4PortId, DSCPValue, VLAN_DESTROY)
                    != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbDscpRowStatus
                     (i4PortId, DSCPValue, i4RetFsPbDscpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            return CLI_FAILURE;
        }
        if (nmhSetFsPbDscpSVlan (i4PortId, DSCPValue, (INT4) u4SVlanId)
            != SNMP_SUCCESS)
        {
            if (i4FsPbDscpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbDscpRowStatus (i4PortId, DSCPValue, VLAN_DESTROY)
                    != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbDscpRowStatus
                     (i4PortId, DSCPValue, i4RetFsPbDscpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (i4FsPbDscpStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbDscpRowStatus (i4PortId, DSCPValue, VLAN_ACTIVE) !=
                SNMP_SUCCESS)
            {
                if (nmhSetFsPbDscpRowStatus (i4PortId, DSCPValue, VLAN_DESTROY)
                    != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else if (nmhSetFsPbDscpRowStatus
                 (i4PortId, DSCPValue, i4RetFsPbDscpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2FsPbDscpRowStatus
            (&u4ErrCode, i4PortId, DSCPValue, VLAN_DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbDscpRowStatus (i4PortId, DSCPValue, VLAN_DESTROY) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSVlanClassDSCPCVlanBase                     */
/*                                                                          */
/*     DESCRIPTION      : This function will configure/delete 'Dscp and     */
/*                        customer vlan' base service vlan                  */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4SVlanId - service vlan id                       */
/*                        DSCPVlaue - DSCP Vlaue                            */
/*                        u4CVlanId - customer vlan id                      */
/*                        u4Flag    - Addition/Delete flag                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSVlanClassDSCPCVlanBase (tCliHandle CliHandle, INT4 i4PortId,
                               UINT4 u4SVlanId, UINT4 u4CVlanId,
                               UINT4 DSCPValue, UINT4 u4Flag)
{
    UINT4               u4ErrCode;
    INT4                i4FsPbCVlanDscpStatus;
    INT4                i4RetFsPbCVlanDscpStatus;
    INT4                i4RetStatus;
    UINT4               u4VlanId = 0;

    if (u4Flag == CLI_ADD)
    {
        i4RetStatus =
            nmhGetFsPbCVlanDscpRowStatus (i4PortId, u4CVlanId, DSCPValue,
                                          &i4RetFsPbCVlanDscpStatus);

        if (i4RetStatus == SNMP_FAILURE)
        {
            /*Create Entry */
            i4FsPbCVlanDscpStatus = VLAN_CREATE_AND_WAIT;
        }
        else
        {
            i4RetStatus =
                nmhGetFsPbCVlanDscpSVlan (i4PortId, (INT4) u4CVlanId,
                                          (INT4) DSCPValue,
                                          (INT4 *) &u4SVlanId);
            if (u4VlanId == u4SVlanId)
            {
                return CLI_SUCCESS;
            }
            i4FsPbCVlanDscpStatus = VLAN_NOT_IN_SERVICE;
        }

        if (nmhTestv2FsPbCVlanDscpRowStatus
            (&u4ErrCode, i4PortId, u4CVlanId, DSCPValue,
             i4FsPbCVlanDscpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbCVlanDscpRowStatus
            (i4PortId, u4CVlanId, DSCPValue,
             i4FsPbCVlanDscpStatus) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbCVlanDscpSVlan
            (&u4ErrCode, i4PortId, (INT4) u4CVlanId, (INT4) DSCPValue,
             (INT4) u4SVlanId) != SNMP_SUCCESS)
        {
            if (i4FsPbCVlanDscpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbCVlanDscpRowStatus
                    (i4PortId, u4CVlanId, DSCPValue,
                     VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbCVlanDscpRowStatus
                     (i4PortId, u4CVlanId, DSCPValue,
                      i4RetFsPbCVlanDscpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            return CLI_FAILURE;
        }
        if (nmhSetFsPbCVlanDscpSVlan (i4PortId, (INT4) u4CVlanId,
                                      (INT4) DSCPValue,
                                      (INT4) u4SVlanId) != SNMP_SUCCESS)
        {
            if (i4FsPbCVlanDscpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbCVlanDscpRowStatus
                    (i4PortId, u4CVlanId, DSCPValue,
                     VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbCVlanDscpRowStatus
                     (i4PortId, u4CVlanId, DSCPValue,
                      i4RetFsPbCVlanDscpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (i4FsPbCVlanDscpStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbCVlanDscpRowStatus
                (i4PortId, u4CVlanId, DSCPValue, VLAN_ACTIVE) != SNMP_SUCCESS)
            {
                if (nmhSetFsPbCVlanDscpRowStatus
                    (i4PortId, u4CVlanId, DSCPValue,
                     VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else if (nmhSetFsPbCVlanDscpRowStatus
                 (i4PortId, u4CVlanId, DSCPValue,
                  i4RetFsPbCVlanDscpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2FsPbCVlanDscpRowStatus
            (&u4ErrCode, i4PortId, u4CVlanId, DSCPValue,
             VLAN_DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbCVlanDscpRowStatus
            (i4PortId, u4CVlanId, DSCPValue, VLAN_DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSVlanClassSrcIPBase                         */
/*                                                                          */
/*     DESCRIPTION      : This function will configure/delete 'source ip'   */
/*                        base service vlan                                 */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4SVlanId - service vlan id                       */
/*                        SrcIP     - Source IP Address                     */
/*                        u4Flag    - Addition/Delete flag                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSVlanClassSrcIPBase (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4SVlanId,
                           UINT4 SrcIP, UINT4 u4Flag)
{
    UINT4               u4ErrCode;
    INT4                i4FsPbSrcIpStatus;
    INT4                i4RetFsPbSrcIpStatus;
    INT4                i4RetStatus;
    UINT4               u4VlanId = 0;

    if (u4Flag == CLI_ADD)
    {
        i4RetStatus =
            nmhGetFsPbSrcIpRowStatus (i4PortId, SrcIP, &i4RetFsPbSrcIpStatus);

        if (i4RetStatus == SNMP_FAILURE)
        {
            /*Create Entry */
            i4FsPbSrcIpStatus = VLAN_CREATE_AND_WAIT;
        }
        else
        {
            i4RetStatus = nmhGetFsPbSrcIpSVlan (i4PortId, SrcIP,
                                                (INT4 *) &u4VlanId);

            if (u4VlanId == u4SVlanId)
            {
                return CLI_SUCCESS;
            }
            i4FsPbSrcIpStatus = VLAN_NOT_IN_SERVICE;
        }

        if (nmhTestv2FsPbSrcIpRowStatus
            (&u4ErrCode, i4PortId, SrcIP, i4FsPbSrcIpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbSrcIpRowStatus (i4PortId, SrcIP, i4FsPbSrcIpStatus) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbSrcIpSVlan (&u4ErrCode, i4PortId, SrcIP,
                                     (INT4) u4SVlanId) != SNMP_SUCCESS)
        {
            if (i4FsPbSrcIpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbSrcIpRowStatus (i4PortId, SrcIP, VLAN_DESTROY) !=
                    SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbSrcIpRowStatus
                     (i4PortId, SrcIP, i4RetFsPbSrcIpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            return CLI_FAILURE;
        }
        if (nmhSetFsPbSrcIpSVlan (i4PortId, SrcIP, (INT4) u4SVlanId)
            != SNMP_SUCCESS)
        {
            if (i4FsPbSrcIpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbSrcIpRowStatus (i4PortId, SrcIP, VLAN_DESTROY) !=
                    SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbSrcIpRowStatus
                     (i4PortId, SrcIP, i4RetFsPbSrcIpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (i4FsPbSrcIpStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbSrcIpRowStatus (i4PortId, SrcIP, VLAN_ACTIVE) !=
                SNMP_SUCCESS)
            {
                if (nmhSetFsPbSrcIpRowStatus (i4PortId, SrcIP, VLAN_DESTROY) !=
                    SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else if (nmhSetFsPbSrcIpRowStatus
                 (i4PortId, SrcIP, i4RetFsPbSrcIpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2FsPbSrcIpRowStatus
            (&u4ErrCode, i4PortId, SrcIP, VLAN_DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbSrcIpRowStatus (i4PortId, SrcIP, VLAN_DESTROY) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSVlanClassSrcIPDstIPBase                    */
/*                                                                          */
/*     DESCRIPTION      : This function will configure/delete 'source ip    */
/*                        and destination ip' base service vlan             */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4SVlanId - service vlan id                       */
/*                        SrcIP     - Source IP Address                     */
/*                        DstIP     - Destination IP Address                */
/*                        u4Flag    - Addition/Delete flag                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSVlanClassSrcIPDstIPBase (tCliHandle CliHandle, INT4 i4PortId,
                                UINT4 u4SVlanId, UINT4 SrcIP, UINT4 DstIP,
                                UINT4 u4Flag)
{
    UINT4               u4ErrCode;
    INT4                i4FsPbSrcDstIpStatus;
    INT4                i4RetFsPbSrcDstIpStatus;
    INT4                i4RetStatus;
    INT4                i4VlanId = 0;

    if (u4Flag == CLI_ADD)
    {
        i4RetStatus =
            nmhGetFsPbSrcDstIpRowStatus (i4PortId, SrcIP, DstIP,
                                         &i4RetFsPbSrcDstIpStatus);

        if (i4RetStatus == SNMP_FAILURE)
        {
            /*Create Entry */
            i4FsPbSrcDstIpStatus = VLAN_CREATE_AND_WAIT;
        }
        else
        {
            i4RetStatus = nmhGetFsPbSrcDstIpSVlan (i4PortId, SrcIP, DstIP,
                                                   &i4VlanId);

            if ((UINT4) i4VlanId == u4SVlanId)
            {
                return CLI_SUCCESS;
            }
            i4FsPbSrcDstIpStatus = VLAN_NOT_IN_SERVICE;
        }

        if (nmhTestv2FsPbSrcDstIpRowStatus
            (&u4ErrCode, i4PortId, SrcIP, DstIP,
             i4FsPbSrcDstIpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbSrcDstIpRowStatus
            (i4PortId, SrcIP, DstIP, i4FsPbSrcDstIpStatus) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbSrcDstIpSVlan
            (&u4ErrCode, i4PortId, SrcIP, DstIP, (INT4) u4SVlanId)
            != SNMP_SUCCESS)
        {
            if (i4FsPbSrcDstIpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbSrcDstIpRowStatus
                    (i4PortId, SrcIP, DstIP, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbSrcDstIpRowStatus
                     (i4PortId, SrcIP, DstIP,
                      i4RetFsPbSrcDstIpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            return CLI_FAILURE;
        }
        if (nmhSetFsPbSrcDstIpSVlan (i4PortId, SrcIP, DstIP,
                                     (INT4) u4SVlanId) != SNMP_SUCCESS)
        {
            if (i4FsPbSrcDstIpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbSrcDstIpRowStatus
                    (i4PortId, SrcIP, DstIP, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbSrcDstIpRowStatus
                     (i4PortId, SrcIP, DstIP,
                      i4RetFsPbSrcDstIpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (i4FsPbSrcDstIpStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbSrcDstIpRowStatus
                (i4PortId, SrcIP, DstIP, VLAN_ACTIVE) != SNMP_SUCCESS)
            {
                if (nmhSetFsPbSrcDstIpRowStatus
                    (i4PortId, SrcIP, DstIP, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else if (nmhSetFsPbSrcDstIpRowStatus
                 (i4PortId, SrcIP, DstIP,
                  i4RetFsPbSrcDstIpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2FsPbSrcDstIpRowStatus
            (&u4ErrCode, i4PortId, SrcIP, DstIP, VLAN_DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbSrcDstIpRowStatus (i4PortId, SrcIP, DstIP, VLAN_DESTROY)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSVlanClassDstIPBase                         */
/*                                                                          */
/*     DESCRIPTION      : This function will configure/delete 'destination  */
/*                        ip' base service vlan                             */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4SVlanId - service vlan id                       */
/*                        DstIP     - Destination IP Address                */
/*                        u4Flag    - Addition/Delete flag                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSVlanClassDstIPBase (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4SVlanId,
                           UINT4 DstIP, UINT4 u4Flag)
{
    UINT4               u4ErrCode;
    INT4                i4FsPbDstIpStatus;
    INT4                i4RetFsPbDstIpStatus;
    INT4                i4RetStatus;
    INT4                i4VlanId = 0;

    if (u4Flag == CLI_ADD)
    {
        i4RetStatus =
            nmhGetFsPbDstIpRowStatus (i4PortId, DstIP, &i4RetFsPbDstIpStatus);

        if (i4RetStatus == SNMP_FAILURE)
        {
            /*Create Entry */
            i4FsPbDstIpStatus = VLAN_CREATE_AND_WAIT;
        }
        else
        {
            i4RetStatus = nmhGetFsPbDstIpSVlan (i4PortId, DstIP, &i4VlanId);

            if ((UINT4) i4VlanId == u4SVlanId)
            {
                return CLI_SUCCESS;
            }
            i4FsPbDstIpStatus = VLAN_NOT_IN_SERVICE;
        }

        if (nmhTestv2FsPbDstIpRowStatus
            (&u4ErrCode, i4PortId, DstIP, i4FsPbDstIpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbDstIpRowStatus (i4PortId, DstIP, i4FsPbDstIpStatus) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbDstIpSVlan (&u4ErrCode, i4PortId, DstIP,
                                     (INT4) u4SVlanId) != SNMP_SUCCESS)
        {
            if (i4FsPbDstIpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbDstIpRowStatus (i4PortId, DstIP, VLAN_DESTROY) !=
                    SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbDstIpRowStatus
                     (i4PortId, DstIP, i4RetFsPbDstIpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            return CLI_FAILURE;
        }
        if (nmhSetFsPbDstIpSVlan (i4PortId, DstIP, (INT4) u4SVlanId)
            != SNMP_SUCCESS)
        {
            if (i4FsPbDstIpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbDstIpRowStatus (i4PortId, DstIP, VLAN_DESTROY) !=
                    SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbDstIpRowStatus
                     (i4PortId, DstIP, i4RetFsPbDstIpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (i4FsPbDstIpStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbDstIpRowStatus (i4PortId, DstIP, VLAN_ACTIVE) !=
                SNMP_SUCCESS)
            {
                if (nmhSetFsPbDstIpRowStatus (i4PortId, DstIP, VLAN_DESTROY) !=
                    SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else if (nmhSetFsPbDstIpRowStatus
                 (i4PortId, DstIP, i4RetFsPbDstIpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2FsPbDstIpRowStatus
            (&u4ErrCode, i4PortId, DstIP, VLAN_DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbDstIpRowStatus (i4PortId, DstIP, VLAN_DESTROY) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSVlanClassDstIPCVlanBase                    */
/*                                                                          */
/*     DESCRIPTION      : This function will configure/delete 'destination  */
/*                        ip and customer vlan' base service vlan           */
/*                                                                          */
/*     INPUT            : u4Port    - Port Index                            */
/*                        u4SVlanId - service vlan id                       */
/*                        DstIP     - Destination IP Address                */
/*                        u4CVlanId - customer vlan id                      */
/*                        u4Flag    - Addition/Delete flag                  */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbSVlanClassDstIPCVlanBase (tCliHandle CliHandle, INT4 i4PortId,
                                UINT4 u4SVlanId, UINT4 u4CVlanId, UINT4 DstIP,
                                UINT4 u4Flag)
{
    UINT4               u4ErrCode;
    INT4                i4FsPbCVlanDstIpStatus;
    INT4                i4RetFsPbCVlanDstIpStatus;
    INT4                i4RetStatus;
    INT4                i4VlanId = 0;

    if (u4Flag == CLI_ADD)
    {
        i4RetStatus =
            nmhGetFsPbCVlanDstIpRowStatus (i4PortId, u4CVlanId, DstIP,
                                           &i4RetFsPbCVlanDstIpStatus);

        if (i4RetStatus == SNMP_FAILURE)
        {
            /*Create Entry */
            i4FsPbCVlanDstIpStatus = VLAN_CREATE_AND_WAIT;
        }
        else
        {
            i4RetStatus = nmhGetFsPbCVlanDstIpSVlan (i4PortId, u4CVlanId,
                                                     DstIP, &i4VlanId);
            if ((UINT4) i4VlanId == u4SVlanId)
            {
                return CLI_SUCCESS;
            }

            i4FsPbCVlanDstIpStatus = VLAN_NOT_IN_SERVICE;
        }

        if (nmhTestv2FsPbCVlanDstIpRowStatus
            (&u4ErrCode, i4PortId, u4CVlanId, DstIP,
             i4FsPbCVlanDstIpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbCVlanDstIpRowStatus
            (i4PortId, u4CVlanId, DstIP,
             i4FsPbCVlanDstIpStatus) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbCVlanDstIpSVlan
            (&u4ErrCode, i4PortId, u4CVlanId, DstIP, (INT4) u4SVlanId)
            != SNMP_SUCCESS)
        {

            if (i4FsPbCVlanDstIpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbCVlanDstIpRowStatus
                    (i4PortId, u4CVlanId, DstIP, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbCVlanDstIpRowStatus
                     (i4PortId, u4CVlanId, DstIP,
                      i4RetFsPbCVlanDstIpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            return CLI_FAILURE;
        }
        if (nmhSetFsPbCVlanDstIpSVlan (i4PortId, u4CVlanId, DstIP, u4SVlanId) !=
            SNMP_SUCCESS)
        {
            if (i4FsPbCVlanDstIpStatus == VLAN_CREATE_AND_WAIT)
            {
                if (nmhSetFsPbCVlanDstIpRowStatus
                    (i4PortId, u4CVlanId, DstIP, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
            }
            else if (nmhSetFsPbCVlanDstIpRowStatus
                     (i4PortId, u4CVlanId, DstIP,
                      i4RetFsPbCVlanDstIpStatus) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (i4FsPbCVlanDstIpStatus == VLAN_CREATE_AND_WAIT)
        {
            if (nmhSetFsPbCVlanDstIpRowStatus
                (i4PortId, u4CVlanId, DstIP, VLAN_ACTIVE) != SNMP_SUCCESS)
            {
                if (nmhSetFsPbCVlanDstIpRowStatus
                    (i4PortId, u4CVlanId, DstIP, VLAN_DESTROY) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else if (nmhSetFsPbCVlanDstIpRowStatus
                 (i4PortId, u4CVlanId, DstIP,
                  i4RetFsPbCVlanDstIpStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2FsPbCVlanDstIpRowStatus
            (&u4ErrCode, i4PortId, u4CVlanId, DstIP,
             VLAN_DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbCVlanDstIpRowStatus
            (i4PortId, u4CVlanId, DstIP, VLAN_DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbConfCvidRegEntry                            */
/*                                                                          */
/*     DESCRIPTION      : This function creates an entry in CVID            */
/*                        registration table.                               */
/*                                                                          */
/*     INPUT            : i4PortId   - Port number                          */
/*                        u4CVlanId    - customer VLAN-ID                   */
/*                        u4SVlanId    - Service VLAN-ID                    */
/*                        u4UntagPep - Untag PEP value                      */
/*                        u4UntagCep - Untag CEP value                      */
/*                        u4RelayCVlanId - Relay customer VLAN-ID           */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbConfCvidRegEntry (tCliHandle CliHandle, INT4 i4PortId,
                        UINT4 u4CVlanId, UINT4 u4SVlanId,
                        UINT4 u4UntagPep, UINT4 u4UntagCep, INT4 u4RelayCVlanId)
{
    UINT4               u4ErrCode;
    INT4                i4RetFsPbCVidRowStatus;
    INT4                i4RetStatus = SNMP_SUCCESS;
    INT4                i4CurrVlan = 0;
    INT4                i4CurrUntagCEP = VLAN_SNMP_FALSE;
    UINT1               u1DestroyFlag = VLAN_TRUE;

    i4RetStatus =
        nmhGetDot1adCVidRegistrationRowStatus (i4PortId, (tVlanId) u4CVlanId,
                                               &i4RetFsPbCVidRowStatus);

    if (i4RetStatus == SNMP_FAILURE)
    {
        /* Entry does not exist. So create entry. */
        if (nmhTestv2Dot1adCVidRegistrationRowStatus
            (&u4ErrCode, i4PortId, (tVlanId) u4CVlanId,
             VLAN_CREATE_AND_WAIT) == SNMP_SUCCESS)
        {
            if (nmhSetDot1adCVidRegistrationRowStatus (i4PortId,
                                                       (tVlanId) u4CVlanId,
                                                       VLAN_CREATE_AND_WAIT)
                == SNMP_SUCCESS)
            {
                if (nmhTestv2Dot1adCVidRegistrationSVid
                    (&u4ErrCode, i4PortId, (tVlanId) u4CVlanId,
                     (tVlanId) u4SVlanId) == SNMP_SUCCESS)
                {
                    if (nmhSetDot1adCVidRegistrationSVid (i4PortId,
                                                          (tVlanId) u4CVlanId,
                                                          (tVlanId) u4SVlanId)
                        == SNMP_SUCCESS)
                    {
                        if ((u4UntagCep == VLAN_CLI_INVALID) &&
                            (u4UntagPep == VLAN_CLI_INVALID)
                            && u4RelayCVlanId == VLAN_RELAY_CVLAN_INVALID)
                        {
                            /* Succes in setting all values. So make 
                             * the entry active. */
                            if (nmhSetDot1adCVidRegistrationRowStatus (i4PortId,
                                                                       (tVlanId)
                                                                       u4CVlanId,
                                                                       VLAN_ACTIVE)
                                == SNMP_FAILURE)
                            {
                                CLI_FATAL_ERROR (CliHandle);
                                return CLI_FAILURE;
                            }

                            return CLI_SUCCESS;
                        }
                        u1DestroyFlag = VLAN_FALSE;
                    }

                    else
                    {
                        /* SVLAN set failed so destroy the row */
                        u1DestroyFlag = VLAN_TRUE;
                    }
                }
                else
                {
                    /* SVLAN test failed so destroy the row */
                    u1DestroyFlag = VLAN_TRUE;
                }
            }
        }
        else
        {
            return CLI_FAILURE;
        }

        if (u1DestroyFlag == VLAN_TRUE)
        {
            nmhSetDot1adCVidRegistrationRowStatus (i4PortId,
                                                   (tVlanId) u4CVlanId,
                                                   VLAN_DESTROY);
            if ((CLI_SUCCESS == CLI_GET_ERR (&u4ErrCode)) && (0 == u4ErrCode))
            {
                CLI_FATAL_ERROR (CliHandle);
            }
            return CLI_FAILURE;
        }
    }
    else
    {
        /* CVID registration entry exists. */
        if (i4RetFsPbCVidRowStatus == VLAN_NOT_READY)
        {
            CliPrintf (CliHandle,
                       "\r%% Row is not ready for configuration.\r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        else
        {
            /* Entry exists and is in active/ not-in-service state. */
            nmhGetDot1adCVidRegistrationSVid (i4PortId, (tVlanId) u4CVlanId,
                                              &i4CurrVlan);

            /*Changing the SVLAN is not Valid */
            if (i4CurrVlan != (INT4) u4SVlanId)
            {
                CLI_SET_ERR (CLI_VLAN_CVLAN_MAPPING_ERR);
                CliPrintf (CliHandle,
                           "\r%% Existing mapping found for specified Customer Vlan\r\n");
                return CLI_FAILURE;
            }
        }

        nmhGetDot1adCVidRegistrationUntaggedCep (i4PortId, (tVlanId) u4CVlanId,
                                                 &i4CurrUntagCEP);

        if (u4UntagCep != VLAN_CLI_INVALID)
        {
            if (nmhTestv2Dot1adCVidRegistrationUntaggedCep
                (&u4ErrCode, i4PortId, (tVlanId) u4CVlanId,
                 (INT4) u4UntagCep) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetDot1adCVidRegistrationUntaggedCep
                (i4PortId, (tVlanId) u4CVlanId, u4UntagCep) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        if (u4UntagPep != VLAN_CLI_INVALID)
        {
            if (nmhTestv2Dot1adCVidRegistrationUntaggedPep
                (&u4ErrCode, i4PortId, (tVlanId) u4CVlanId,
                 (INT4) u4UntagPep) != SNMP_SUCCESS)
            {
                /* Revert UntagCEP value. */
                nmhSetDot1adCVidRegistrationUntaggedCep (i4PortId,
                                                         (tVlanId) u4CVlanId,
                                                         i4CurrUntagCEP);
                return CLI_FAILURE;
            }
            if (nmhSetDot1adCVidRegistrationUntaggedPep
                (i4PortId, (tVlanId) u4CVlanId, u4UntagPep) != SNMP_SUCCESS)
            {
                /*If this set fails we have to revert the untagcep here */
                nmhSetDot1adCVidRegistrationUntaggedCep (i4PortId,
                                                         (tVlanId) u4CVlanId,
                                                         i4CurrUntagCEP);
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        if (u4RelayCVlanId != VLAN_RELAY_CVLAN_INVALID)
        {
            if (nmhTestv2Dot1adCVIdRegistrationRelayCVid
                (&u4ErrCode, i4PortId, (tVlanId) u4CVlanId,
                 (INT4) u4RelayCVlanId) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetDot1adCVIdRegistrationRelayCVid
                (i4PortId, (tVlanId) u4CVlanId,
                 (INT4) u4RelayCVlanId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        return CLI_SUCCESS;
    }

    /* Entry was created in this command. */
    if (u4UntagCep != VLAN_CLI_INVALID)
    {
        if (nmhTestv2Dot1adCVidRegistrationUntaggedCep
            (&u4ErrCode, i4PortId, (tVlanId) u4CVlanId, (INT4) u4UntagCep) !=
            SNMP_SUCCESS)
        {
            /* Delete the row. */
            nmhSetDot1adCVidRegistrationRowStatus (i4PortId,
                                                   (tVlanId) u4CVlanId,
                                                   VLAN_DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetDot1adCVidRegistrationUntaggedCep
            (i4PortId, (tVlanId) u4CVlanId, (INT4) u4UntagCep) != SNMP_SUCCESS)
        {
            /* Delete the row. */
            nmhSetDot1adCVidRegistrationRowStatus (i4PortId,
                                                   (tVlanId) u4CVlanId,
                                                   VLAN_DESTROY);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4UntagPep != VLAN_CLI_INVALID)
    {
        if (nmhTestv2Dot1adCVidRegistrationUntaggedPep
            (&u4ErrCode, i4PortId, (tVlanId) u4CVlanId, (INT4) u4UntagPep) !=
            SNMP_SUCCESS)
        {
            /* Delete the row. */
            nmhSetDot1adCVidRegistrationRowStatus (i4PortId,
                                                   (tVlanId) u4CVlanId,
                                                   VLAN_DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetDot1adCVidRegistrationUntaggedPep (i4PortId,
                                                     (tVlanId) u4CVlanId,
                                                     (INT4) u4UntagPep) !=
            SNMP_SUCCESS)
        {
            /* Delete the row. */
            nmhSetDot1adCVidRegistrationRowStatus (i4PortId,
                                                   (tVlanId) u4CVlanId,
                                                   VLAN_DESTROY);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4RelayCVlanId != VLAN_RELAY_CVLAN_INVALID)
    {
        if (nmhTestv2Dot1adCVIdRegistrationRelayCVid
            (&u4ErrCode, i4PortId, (tVlanId) u4CVlanId,
             (INT4) u4RelayCVlanId) != SNMP_SUCCESS)
        {
            nmhSetDot1adCVidRegistrationRowStatus (i4PortId,
                                                   (tVlanId) u4CVlanId,
                                                   VLAN_DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetDot1adCVIdRegistrationRelayCVid
            (i4PortId, (INT4) u4CVlanId, (INT4) u4RelayCVlanId) != SNMP_SUCCESS)
        {
            /* Delete the row. */
            nmhSetDot1adCVidRegistrationRowStatus (i4PortId,
                                                   (tVlanId) u4CVlanId,
                                                   VLAN_DESTROY);

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Succes in setting all values. So make the entry active. */
    if (nmhSetDot1adCVidRegistrationRowStatus (i4PortId, (tVlanId) u4CVlanId,
                                               VLAN_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbDelCvidRegEntry                             */
/*                                                                          */
/*     DESCRIPTION      : This function deletes an entry from VID           */
/*                        registration table.                               */
/*                                                                          */
/*     INPUT            : i4PortId   - Port number                          */
/*                        u4CVlanId    - customer VLAN-ID                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbDelCvidRegEntry (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4CVlanId)
{
    UINT4               u4ErrCode;
    INT4                i4RetFsPbCVidRowStatus;
    INT4                i4RetStatus;

    i4RetStatus =
        nmhGetDot1adCVidRegistrationRowStatus (i4PortId, (tVlanId) u4CVlanId,
                                               &i4RetFsPbCVidRowStatus);

    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_PB_CVID_REG_ERR);
        return CLI_FAILURE;
    }
    if (nmhTestv2Dot1adCVidRegistrationRowStatus
        (&u4ErrCode, i4PortId, (tVlanId) u4CVlanId,
         VLAN_DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1adCVidRegistrationRowStatus (i4PortId,
                                               (tVlanId) u4CVlanId,
                                               VLAN_DESTROY) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbConfSVlanPriority                           */
/*                                                                          */
/*     DESCRIPTION      : This function configures the SVLAN Priority type  */
/*                        and SVLAN Priority for CEP, CNP S-Tagged and      */
/*                        PortBased ports                                   */
/*                                                                          */
/*     INPUT            : i4PortId   - Port number                          */
/*                        u4CVlanId    - customer VLAN-ID                   */
/*                        u1PriorityType - COPY/FIXED                       */
/*                        u1Priority     - SVLAN Priority                   */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbConfSVlanPriority (tCliHandle CliHandle, INT4 i4PortId, UINT4 u4CVlanId,
                         UINT4 u4PriorityType, UINT4 u4Priority)
{

    if (VLAN_PB_PORT_TYPE ((UINT2) i4PortId) == VLAN_CUSTOMER_EDGE_PORT)
    {
        return (VlanPbConfCVIDSVlanPriority (CliHandle, i4PortId, u4CVlanId,
                                             u4PriorityType, u4Priority));
    }
    else                        /* The port should be either CNP S-taggeg or CNP PortBased */
    {
        if ((VLAN_PB_PORT_TYPE ((UINT2) i4PortId) != VLAN_CNP_TAGGED_PORT) &&
            (VLAN_PB_PORT_TYPE ((UINT2) i4PortId) != VLAN_CNP_PORTBASED_PORT))
        {
            CLI_SET_ERR (CLI_PB_CNP_CONFIG);
            return CLI_FAILURE;
        }

        return (VlanPbConfPortSVlanPriority (CliHandle, i4PortId,
                                             u4PriorityType, u4Priority));
    }
    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbConfCVIDSVlanPriority                       */
/*                                                                          */
/*     DESCRIPTION      : This function configures the SVLAN Priority type  */
/*                        and SVLAN Priority for CEP ports based on CVID reg*/
/*                        entry.                                            */
/*     INPUT            : i4PortId   - Port number                          */
/*                        u4CVlanId    - customer VLAN-ID                   */
/*                        u4PriorityType - COPY/FIXED                       */
/*                        u4Priority     - SVLAN Priority                   */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbConfCVIDSVlanPriority (tCliHandle CliHandle, INT4 i4PortId,
                             UINT4 u4CVlanId, UINT4 u4PriorityType,
                             UINT4 u4Priority)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetFsPbCVidRowStatus = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4NextPortId = 0;
    INT4                i4CVlanId = 0;
    INT4                i4NextCVlanId = 0;

    if (u4CVlanId != 0)            /*Configure for given CVID Reg entry */
    {
        i4RetStatus = nmhGetDot1adCVidRegistrationRowStatus (i4PortId,
                                                             (tVlanId)
                                                             u4CVlanId,
                                                             &i4RetFsPbCVidRowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_PB_CVID_REG_ERR);
            return CLI_FAILURE;
        }

        if (i4RetFsPbCVidRowStatus == VLAN_ACTIVE)
        {
            if (nmhTestv2Dot1adCVidRegistrationRowStatus (&u4ErrCode, i4PortId,
                                                          (tVlanId) u4CVlanId,
                                                          VLAN_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetDot1adCVidRegistrationRowStatus (i4PortId,
                                                       (tVlanId) u4CVlanId,
                                                       VLAN_NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        if (nmhTestv2Dot1adCVidRegistrationSVlanPriorityType
            (&u4ErrCode, i4PortId, (tVlanId) u4CVlanId,
             (INT4) u4PriorityType) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetDot1adCVidRegistrationSVlanPriorityType
            (i4PortId, (tVlanId) u4CVlanId,
             (INT4) u4PriorityType) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (u4PriorityType == VLAN_SVLAN_PRIORITY_TYPE_FIXED)
        {
            if (nmhTestv2Dot1adCVidRegistrationSVlanPriority
                (&u4ErrCode, i4PortId, (tVlanId) u4CVlanId,
                 (INT4) u4Priority) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetDot1adCVidRegistrationSVlanPriority
                (i4PortId, (tVlanId) u4CVlanId,
                 (INT4) u4Priority) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        /* Succes in setting all values. So make the entry active. */
        if (nmhSetDot1adCVidRegistrationRowStatus
            (i4PortId, (tVlanId) u4CVlanId, VLAN_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else                        /* The SVLAN Priority setting has to be applied for all the CVID entries. */
    {
        while (nmhGetNextIndexDot1adCVidRegistrationTable
               (i4PortId, &i4NextPortId, i4CVlanId,
                &i4NextCVlanId) == SNMP_SUCCESS)
        {
            if (i4PortId != i4NextPortId)
            {
                break;
            }

            i4RetStatus =
                nmhGetDot1adCVidRegistrationRowStatus (i4NextPortId,
                                                       (tVlanId) i4NextCVlanId,
                                                       &i4RetFsPbCVidRowStatus);
            if (i4RetStatus == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_PB_CVID_REG_ERR);
                return CLI_FAILURE;
            }

            if (i4RetFsPbCVidRowStatus == VLAN_ACTIVE)
            {
                if (nmhTestv2Dot1adCVidRegistrationRowStatus
                    (&u4ErrCode, i4NextPortId, (tVlanId) i4NextCVlanId,
                     VLAN_NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetDot1adCVidRegistrationRowStatus
                    (i4NextPortId, (tVlanId) i4NextCVlanId,
                     VLAN_NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }

            if (nmhTestv2Dot1adCVidRegistrationSVlanPriorityType
                (&u4ErrCode, i4NextPortId, (tVlanId) i4NextCVlanId,
                 (INT4) u4PriorityType) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetDot1adCVidRegistrationSVlanPriorityType
                (i4NextPortId, (tVlanId) i4NextCVlanId,
                 (INT4) u4PriorityType) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if (u4PriorityType == VLAN_SVLAN_PRIORITY_TYPE_FIXED)
            {
                if (nmhTestv2Dot1adCVidRegistrationSVlanPriority
                    (&u4ErrCode, i4NextPortId, (tVlanId) i4NextCVlanId,
                     (INT4) u4Priority) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetDot1adCVidRegistrationSVlanPriority
                    (i4NextPortId, (tVlanId) i4NextCVlanId,
                     (INT4) u4Priority) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
            /* Succes in setting all values. So make the entry active. */
            if (nmhSetDot1adCVidRegistrationRowStatus
                (i4NextPortId, (tVlanId) i4NextCVlanId,
                 VLAN_ACTIVE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            i4CVlanId = i4NextCVlanId;
        }
    }
    return CLI_SUCCESS;

}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbConfPortSVlanPriority                       */
/*                                                                          */
/*     DESCRIPTION      : This function configures the SVLAN Priority type  */
/*                        and SVLAN Priority for CNP S-tagged and PortBased */
/*                        ports.                                            */
/*     INPUT            : i4PortId   - Port number                          */
/*                        u4PriorityType - COPY/FIXED                       */
/*                        u4Priority     - SVLAN Priority                   */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbConfPortSVlanPriority (tCliHandle CliHandle, INT4 i4PortId,
                             UINT4 u4PriorityType, UINT4 u4Priority)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Dot1adPortSVlanPriorityType (&u4ErrCode, i4PortId,
                                              (INT4) u4PriorityType) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetDot1adPortSVlanPriorityType (i4PortId, (INT4) u4PriorityType) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (u4PriorityType == VLAN_SVLAN_PRIORITY_TYPE_FIXED)
    {
        if (nmhTestv2Dot1adPortSVlanPriority (&u4ErrCode, i4PortId,
                                              (INT4) u4Priority) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetDot1adPortSVlanPriority (i4PortId, (INT4) u4Priority) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningConfigScalars                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays current Provider Bridge     */
/*                        configuration for scalars objects                  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT1 *pu1HeadFlag)
{
    UINT1               au1ContextName[VLAN_SWITCH_ALIAS_LEN];
    UINT4               u4SysMode;
    UINT4               u4MulticastMacLimit;
    INT4                i4BpduPri = 0;
    INT4                i4BridgeMode = VLAN_CUSTOMER_BRIDGE_MODE;
    tMacAddr            Dot1xTunnelAddress;
    tMacAddr            LacpTunnelAddress;
    tMacAddr            StpTunnelAddress;
    tMacAddr            GvrpTunnelAddress;
    tMacAddr            GmrpTunnelAddress;
    tMacAddr            MvrpTunnelAddress;
    tMacAddr            MmrpTunnelAddress;
    tMacAddr            ElmiTunnelAddress;
    tMacAddr            LldpTunnelAddress;
    tMacAddr            EcfmTunnelAddress;
    tMacAddr            EoamTunnelAddress;
    tMacAddr            IgmpTunnelAddress;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1Flag = VLAN_FALSE;
    u4SysMode = VlanVcmGetSystemModeExt (VLANMOD_PROTOCOL_ID);
    MEMSET (&Dot1xTunnelAddress, 0, sizeof (tMacAddr));
    MEMSET (&LacpTunnelAddress, 0, sizeof (tMacAddr));
    MEMSET (&LldpTunnelAddress, 0, sizeof (tMacAddr));
    MEMSET (&GvrpTunnelAddress, 0, sizeof (tMacAddr));
    MEMSET (&GmrpTunnelAddress, 0, sizeof (tMacAddr));
    MEMSET (&EcfmTunnelAddress, 0, sizeof (tMacAddr));
    MEMSET (&EoamTunnelAddress, 0, sizeof (tMacAddr));
    MEMSET (&MmrpTunnelAddress, 0, sizeof (tMacAddr));
    MEMSET (&MvrpTunnelAddress, 0, sizeof (tMacAddr));
    MEMSET (&ElmiTunnelAddress, 0, sizeof (tMacAddr));
    MEMSET (&IgmpTunnelAddress, 0, sizeof (tMacAddr));
    MEMSET (&StpTunnelAddress, 0, sizeof (tMacAddr));
    if (VLAN_SYSTEM_CONTROL_FOR_CONTEXT (u4ContextId) == VLAN_SNMP_TRUE)
    {
        return CLI_SUCCESS;
    }

    nmhGetFsMIVlanBridgeMode ((INT4) u4ContextId, &i4BridgeMode);

    if ((i4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE) ||
        (i4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
        (i4BridgeMode == VLAN_PROVIDER_CORE_BRIDGE_MODE) ||
        (i4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE) ||
        (i4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        nmhGetFsMIPbMulticastMacLimit ((INT4) u4ContextId,
                                       &u4MulticastMacLimit);

        if (u4MulticastMacLimit != PB_DEFAULT_MULTICAST_MAC_LIMIT)
        {
            u1Flag = VLAN_TRUE;
        }
    }

    nmhGetFsMIVlanTunnelBpduPri ((INT4) u4ContextId, &i4BpduPri);
    nmhGetFsMIVlanTunnelDot1xAddress ((INT4) u4ContextId, &Dot1xTunnelAddress);
    nmhGetFsMIVlanTunnelLacpAddress ((INT4) u4ContextId, &LacpTunnelAddress);
    nmhGetFsMIVlanTunnelStpAddress ((INT4) u4ContextId, &StpTunnelAddress);
    nmhGetFsMIVlanTunnelGvrpAddress ((INT4) u4ContextId, &GvrpTunnelAddress);
    nmhGetFsMIVlanTunnelGmrpAddress ((INT4) u4ContextId, &GmrpTunnelAddress);
    nmhGetFsMIVlanTunnelMvrpAddress ((INT4) u4ContextId, &MvrpTunnelAddress);
    nmhGetFsMIVlanTunnelMmrpAddress ((INT4) u4ContextId, &MmrpTunnelAddress);
    nmhGetFsMIVlanTunnelElmiAddress ((INT4) u4ContextId, &ElmiTunnelAddress);
    nmhGetFsMIVlanTunnelLldpAddress ((INT4) u4ContextId, &LldpTunnelAddress);
    nmhGetFsMIVlanTunnelEcfmAddress ((INT4) u4ContextId, &EcfmTunnelAddress);
    nmhGetFsMIVlanTunnelEoamAddress ((INT4) u4ContextId, &EoamTunnelAddress);
    nmhGetFsMIVlanTunnelIgmpAddress ((INT4) u4ContextId, &IgmpTunnelAddress);

    if ((u4SysMode == VCM_MI_MODE) && (*pu1HeadFlag == VLAN_FALSE))
    {
        if ((u1Flag == VLAN_TRUE)
            || (i4BpduPri != VLAN_DEF_TUNNEL_BPDU_PRIORITY)
            ||
            (MEMCMP
             (Dot1xTunnelAddress, gVlanProviderDot1xAddr,
              ETHERNET_ADDR_SIZE) != 0)
            ||
            (MEMCMP
             (LacpTunnelAddress, gVlanProviderLacpAddr,
              ETHERNET_ADDR_SIZE) != 0)
            ||
            (MEMCMP
             (IgmpTunnelAddress, gVlanProviderIgmpAddr,
              ETHERNET_ADDR_SIZE) != 0)
            ||
            (MEMCMP
             (EoamTunnelAddress, gVlanProviderEoamAddr,
              ETHERNET_ADDR_SIZE) != 0)
            ||
            (MEMCMP
             (EcfmTunnelAddress, gVlanProviderEcfmAddr,
              ETHERNET_ADDR_SIZE) != 0)
            ||
            (MEMCMP
             (LldpTunnelAddress, gVlanProviderLldpAddr,
              ETHERNET_ADDR_SIZE) != 0)
            ||
            (MEMCMP
             (ElmiTunnelAddress, gVlanProviderElmiAddr,
              ETHERNET_ADDR_SIZE) != 0)
            ||
            (MEMCMP
             (MmrpTunnelAddress, gVlanProviderMmrpAddr,
              ETHERNET_ADDR_SIZE) != 0)
            ||
            (MEMCMP
             (MvrpTunnelAddress, gVlanProviderMvrpAddr,
              ETHERNET_ADDR_SIZE) != 0)
            ||
            (MEMCMP
             (GmrpTunnelAddress, gVlanProviderGmrpAddr,
              ETHERNET_ADDR_SIZE) != 0)
            ||
            (MEMCMP (StpTunnelAddress, gVlanProviderStpAddr, ETHERNET_ADDR_SIZE)
             != 0)
            ||
            (MEMCMP
             (GvrpTunnelAddress, gVlanProviderGvrpAddr,
              ETHERNET_ADDR_SIZE) != 0))

        {
            VLAN_MEMSET (au1ContextName, 0, VLAN_SWITCH_ALIAS_LEN);

            VlanVcmGetAliasName (u4ContextId, au1ContextName);
            if (STRLEN (au1ContextName) != 0)
            {
                CliPrintf (CliHandle, "\rswitch  %s \r\n", au1ContextName);
                *pu1HeadFlag = VLAN_TRUE;
            }
        }

    }

    if (u1Flag == VLAN_TRUE)
    {
        CliPrintf (CliHandle, "multicast-mac limit %d\r\n",
                   u4MulticastMacLimit);
    }
    if (i4BpduPri != VLAN_DEF_TUNNEL_BPDU_PRIORITY)
    {
        CliPrintf (CliHandle, "l2protocol-tunnel cos %d\r\n", i4BpduPri);
    }

    if (MEMCMP (Dot1xTunnelAddress, gVlanProviderDot1xAddr, ETHERNET_ADDR_SIZE)
        != 0)
    {
        PrintMacAddress (Dot1xTunnelAddress, au1String);
        CliPrintf (CliHandle, "dot1x-tunnel-address %s\r\n", au1String);
    }

    if (MEMCMP (LacpTunnelAddress, gVlanProviderLacpAddr, ETHERNET_ADDR_SIZE) !=
        0)
    {
        PrintMacAddress (LacpTunnelAddress, au1String);
        CliPrintf (CliHandle, "lacp-tunnel-address %s\r\n", au1String);
    }

    if (MEMCMP (StpTunnelAddress, gVlanProviderStpAddr, ETHERNET_ADDR_SIZE) !=
        0)
    {
        PrintMacAddress (StpTunnelAddress, au1String);
        CliPrintf (CliHandle, "stp-tunnel-address %s\r\n", au1String);
    }

    if (MEMCMP (GvrpTunnelAddress, gVlanProviderGvrpAddr, ETHERNET_ADDR_SIZE) !=
        0)
    {
        PrintMacAddress (GvrpTunnelAddress, au1String);
        CliPrintf (CliHandle, "gvrp-tunnel-address %s\r\n", au1String);
    }

    if (MEMCMP (GmrpTunnelAddress, gVlanProviderGmrpAddr, ETHERNET_ADDR_SIZE) !=
        0)
    {
        PrintMacAddress (GmrpTunnelAddress, au1String);
        CliPrintf (CliHandle, "gmrp-tunnel-address %s\r\n", au1String);
    }

    if (MEMCMP (MvrpTunnelAddress, gVlanProviderMvrpAddr, ETHERNET_ADDR_SIZE) !=
        0)
    {
        PrintMacAddress (MvrpTunnelAddress, au1String);
        CliPrintf (CliHandle, "mvrp-tunnel-address %s\r\n", au1String);
    }

    if (MEMCMP (MmrpTunnelAddress, gVlanProviderMmrpAddr, ETHERNET_ADDR_SIZE) !=
        0)
    {
        PrintMacAddress (MmrpTunnelAddress, au1String);
        CliPrintf (CliHandle, "mmrp-tunnel-address %s\r\n", au1String);
    }

    if (MEMCMP (ElmiTunnelAddress, gVlanProviderElmiAddr, ETHERNET_ADDR_SIZE) !=
        0)
    {
        PrintMacAddress (ElmiTunnelAddress, au1String);
        CliPrintf (CliHandle, "elmi-tunnel-address %s\r\n", au1String);
    }

    if (MEMCMP (LldpTunnelAddress, gVlanProviderLldpAddr, ETHERNET_ADDR_SIZE) !=
        0)
    {
        PrintMacAddress (LldpTunnelAddress, au1String);
        CliPrintf (CliHandle, "lldp-tunnel-address %s\r\n", au1String);
    }

    if (MEMCMP (EcfmTunnelAddress, gVlanProviderEcfmAddr, ETHERNET_ADDR_SIZE) !=
        0)
    {
        PrintMacAddress (EcfmTunnelAddress, au1String);
        CliPrintf (CliHandle, "ecfm-tunnel-address %s\r\n", au1String);
    }
    if (MEMCMP (EoamTunnelAddress, gVlanProviderEoamAddr, ETHERNET_ADDR_SIZE) !=
        0)
    {
        PrintMacAddress (EoamTunnelAddress, au1String);
        CliPrintf (CliHandle, "eoam-tunnel-address %s\r\n", au1String);
    }
    if (MEMCMP (IgmpTunnelAddress, gVlanProviderIgmpAddr, ETHERNET_ADDR_SIZE) !=
        0)
    {
        PrintMacAddress (IgmpTunnelAddress, au1String);
        CliPrintf (CliHandle, "igmp-tunnel-address %s\r\n", au1String);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningSrcMacSVlanTable                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for SrcMacSVlanTable                               */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningSrcMacSVlanTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                   UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4SVlanId = 0;
    tMacAddr            SrcMacAddr;
    tMacAddr            NextSrcMacAddr;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];

    MEMSET (SrcMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextSrcMacAddr, 0, sizeof (tMacAddr));
    while (nmhGetNextIndexFsPbSrcMacSVlanTable (i4LocalPort,
                                                &i4NextPort,
                                                SrcMacAddr,
                                                &NextSrcMacAddr) ==
           SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetFsPbSrcMacSVlan (i4NextPort, NextSrcMacAddr, &i4SVlanId);
        PrintMacAddress (NextSrcMacAddr, au1String);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus = CliPrintf (CliHandle,
                                      "switchport service vlan %d SrcMac %s\r\n",
                                      i4SVlanId, au1String);
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        MEMCPY (SrcMacAddr, NextSrcMacAddr, sizeof (tMacAddr));
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningDstMacSVlanTable                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for DstMacSVlanTable                               */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningDstMacSVlanTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                   UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4SVlanId = 0;
    tMacAddr            DstMacAddr;
    tMacAddr            NextDstMacAddr;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];

    MEMSET (DstMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextDstMacAddr, 0, sizeof (tMacAddr));

    while (nmhGetNextIndexFsPbDstMacSVlanTable (i4LocalPort,
                                                &i4NextPort,
                                                DstMacAddr,
                                                &NextDstMacAddr) ==
           SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetFsPbDstMacSVlan (i4NextPort, NextDstMacAddr, &i4SVlanId);
        PrintMacAddress (NextDstMacAddr, au1String);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus = CliPrintf (CliHandle,
                                      "switchport service vlan %d DstMac %s\r\n",
                                      i4SVlanId, au1String);
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        MEMCPY (DstMacAddr, NextDstMacAddr, sizeof (tMacAddr));
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningCVlanSrcMacSVlanTable             */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for CVlanSrcMacSVlanTable                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningCVlanSrcMacSVlanTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                        UINT4 *pu4PagingStatus,
                                        UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4SVlanId = 0;
    INT4                i4CVlanId = 0;
    INT4                i4NextCVlanId = 0;
    tMacAddr            SrcMacAddr;
    tMacAddr            NextSrcMacAddr;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];

    MEMSET (SrcMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextSrcMacAddr, 0, sizeof (tMacAddr));

    while (nmhGetNextIndexFsPbCVlanSrcMacSVlanTable
           (i4LocalPort, &i4NextPort, i4CVlanId,
            &i4NextCVlanId, SrcMacAddr, &NextSrcMacAddr) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetFsPbCVlanSrcMacSVlan (i4NextPort, i4NextCVlanId,
                                    NextSrcMacAddr, &i4SVlanId);

        PrintMacAddress (NextSrcMacAddr, au1String);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus = CliPrintf (CliHandle,
                                      "switchport service vlan %d "
                                      "customer vlan %d SrcMac %s\r\n",
                                      i4SVlanId, i4NextCVlanId, au1String);
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4CVlanId = i4NextCVlanId;
        MEMCPY (SrcMacAddr, NextSrcMacAddr, sizeof (tMacAddr));
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningCVlanDstMacSVlanTable             */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for CVlanDstMacSVlanTable                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningCVlanDstMacSVlanTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                        UINT4 *pu4PagingStatus,
                                        UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4SVlanId = 0;
    INT4                i4CVlanId = 0;
    INT4                i4NextCVlanId = 0;
    tMacAddr            DstMacAddr;
    tMacAddr            NextDstMacAddr;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];

    MEMSET (DstMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextDstMacAddr, 0, sizeof (tMacAddr));
    while (nmhGetNextIndexFsPbCVlanDstMacSVlanTable
           (i4LocalPort, &i4NextPort, i4CVlanId,
            &i4NextCVlanId, DstMacAddr, &NextDstMacAddr) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetFsPbCVlanDstMacSVlan (i4NextPort, i4NextCVlanId,
                                    NextDstMacAddr, &i4SVlanId);
        PrintMacAddress (NextDstMacAddr, au1String);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus = CliPrintf (CliHandle,
                                      "switchport service vlan %d "
                                      "customer vlan %d DstMac %s\r\n",
                                      i4SVlanId, i4NextCVlanId, au1String);
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4CVlanId = i4NextCVlanId;
        MEMCPY (DstMacAddr, NextDstMacAddr, sizeof (tMacAddr));
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningDscpSVlanTable                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for DscpSVlanTable                                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningDscpSVlanTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                 UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4DSCPValue = 0;
    INT4                i4NextDSCPValue = 0;
    INT4                i4SVlanId = 0;

    while (nmhGetNextIndexFsPbDscpSVlanTable
           (i4LocalPort, &i4NextPort, i4DSCPValue, &i4NextDSCPValue)
           == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetFsPbDscpSVlan (i4NextPort, i4NextDSCPValue, &i4SVlanId);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus =
            CliPrintf (CliHandle,
                       "switchport service vlan %d DSCP %d\r\n",
                       i4SVlanId, i4NextDSCPValue);
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4DSCPValue = i4NextDSCPValue;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningCVlanDscpSVlanTable               */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for CVlanDscpSVlanTable                            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningCVlanDscpSVlanTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                      UINT4 *pu4PagingStatus,
                                      UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4DSCPValue = 0;
    INT4                i4NextDSCPValue = 0;
    INT4                i4CVlanId = 0;
    INT4                i4NextCVlanId = 0;
    INT4                i4SVlanId = 0;

    while (nmhGetNextIndexFsPbCVlanDscpSVlanTable
           (i4LocalPort, &i4NextPort, i4CVlanId,
            &i4NextCVlanId, i4DSCPValue, &i4NextDSCPValue) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetFsPbCVlanDscpSVlan (i4NextPort, i4NextCVlanId,
                                  i4NextDSCPValue, &i4SVlanId);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus = CliPrintf (CliHandle,
                                      "switchport service vlan %d "
                                      "customer vlan %d DSCP %d\r\n",
                                      i4SVlanId, i4NextCVlanId,
                                      i4NextDSCPValue);
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4CVlanId = i4NextCVlanId;
        i4DSCPValue = i4NextDSCPValue;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningSrcIPSVlanTable                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for SrcIpAddrSVlanTable                            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningSrcIPSVlanTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                  UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4SVlanId = 0;
    UINT4               u4SrcIPAddr = 0;
    UINT4               u4NextSrcIPAddr = 0;
    CHR1               *pu1Address = NULL;
    UINT1               au1Address[MAX_ADDR_LEN];

    pu1Address = (CHR1 *) & au1Address[0];

    while (nmhGetNextIndexFsPbSrcIpAddrSVlanTable
           (i4LocalPort, &i4NextPort, u4SrcIPAddr, &u4NextSrcIPAddr)
           == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetFsPbSrcIpSVlan (i4NextPort, u4NextSrcIPAddr, &i4SVlanId);

        CLI_CONVERT_IPADDR_TO_STR (pu1Address, u4NextSrcIPAddr);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus =
            CliPrintf (CliHandle,
                       "switchport service vlan %d SrcIP %s\r\n",
                       i4SVlanId, pu1Address);
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        u4SrcIPAddr = u4NextSrcIPAddr;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningDstIPSVlanTable                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for DstIpAddrSVlanTable                            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningDstIPSVlanTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                  UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4SVlanId = 0;
    UINT4               u4DstIPAddr = 0;
    UINT4               u4NextDstIPAddr = 0;
    CHR1               *pu1Address = NULL;
    UINT1               au1Address[MAX_ADDR_LEN];

    pu1Address = (CHR1 *) & au1Address[0];

    while (nmhGetNextIndexFsPbDstIpAddrSVlanTable
           (i4LocalPort, &i4NextPort, u4DstIPAddr, &u4NextDstIPAddr)
           == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetFsPbDstIpSVlan (i4NextPort, u4NextDstIPAddr, &i4SVlanId);

        CLI_CONVERT_IPADDR_TO_STR (pu1Address, u4NextDstIPAddr);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus =
            CliPrintf (CliHandle,
                       "switchport service vlan %d DstIP %s\r\n",
                       i4SVlanId, pu1Address);

        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        u4DstIPAddr = u4NextDstIPAddr;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningSrcDstIPSVlanTable                */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for SrcDstIpSVlanTable                             */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningSrcDstIPSVlanTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                     UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4SVlanId = 0;
    UINT4               u4SrcIPAddr = 0;
    UINT4               u4NextSrcIPAddr = 0;
    UINT4               u4DstIPAddr = 0;
    UINT4               u4NextDstIPAddr = 0;
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1Address2[MAX_ADDR_LEN];
    CHR1               *pu1Address = NULL;
    CHR1               *pu1Address2 = NULL;

    pu1Address = (CHR1 *) & au1Address[0];
    pu1Address2 = (CHR1 *) & au1Address2[0];

    while (nmhGetNextIndexFsPbSrcDstIpSVlanTable
           (i4LocalPort, &i4NextPort, u4SrcIPAddr, &u4NextSrcIPAddr,
            u4DstIPAddr, &u4NextDstIPAddr) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetFsPbSrcDstIpSVlan (i4NextPort, u4NextSrcIPAddr,
                                 u4NextDstIPAddr, &i4SVlanId);

        CLI_CONVERT_IPADDR_TO_STR (pu1Address, u4NextSrcIPAddr);
        CLI_CONVERT_IPADDR_TO_STR (pu1Address2, u4NextDstIPAddr);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus = CliPrintf (CliHandle,
                                      "switchport service vlan %d SrcIP %s DstIP %s\r\n",
                                      i4SVlanId, pu1Address, pu1Address2);
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        u4SrcIPAddr = u4NextSrcIPAddr;
        u4DstIPAddr = u4NextDstIPAddr;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningCVlanDstIPSVlanTable              */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for CVlanDstIpSVlanTable                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningCVlanDstIPSVlanTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                       UINT4 *pu4PagingStatus,
                                       UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4CVlanId = 0;
    INT4                i4NextCVlanId = 0;
    INT4                i4SVlanId = 0;
    UINT4               u4DstIPAddr = 0;
    UINT4               u4NextDstIPAddr = 0;
    UINT1               au1Address[MAX_ADDR_LEN];
    CHR1               *pu1Address = NULL;

    pu1Address = (CHR1 *) & au1Address[0];

    while (nmhGetNextIndexFsPbCVlanDstIpSVlanTable
           (i4LocalPort, &i4NextPort, i4CVlanId,
            &i4NextCVlanId, u4DstIPAddr, &u4NextDstIPAddr) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetFsPbCVlanDstIpSVlan (i4NextPort, i4NextCVlanId,
                                   u4NextDstIPAddr, &i4SVlanId);

        CLI_CONVERT_IPADDR_TO_STR (pu1Address, u4NextDstIPAddr);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus = CliPrintf (CliHandle,
                                      "switchport service vlan %d customer vlan %d DstIP %s\r\n",
                                      i4SVlanId, i4NextCVlanId, pu1Address);
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4CVlanId = i4NextCVlanId;
        u4DstIPAddr = u4NextDstIPAddr;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningCVidSVlanTable                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for CVidRegistrationTable                          */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningCVidSVlanTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                 UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4UntaggedPep = VLAN_SNMP_FALSE;
    INT4                i4UntaggedCep = VLAN_SNMP_FALSE;
    INT4                i4CVlanId = 0;
    INT4                i4NextCVlanId = 0;
    INT4                i4SVlanId = 0;
    INT4                i4RelayCVlanId = 0;
    INT4                i4SVlanPriorityType;
    INT4                i4SVlanPriority;
    UINT4               u4CurrContextId = 0;
    INT4                i4CVlanCounterStatus = 0;

    while (nmhGetNextIndexDot1adCVidRegistrationTable
           (i4LocalPort, &i4NextPort, i4CVlanId,
            &i4NextCVlanId) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetDot1adCVidRegistrationSVid (i4NextPort,
                                          i4NextCVlanId, &i4SVlanId);
        nmhGetDot1adCVIdRegistrationRelayCVid (i4NextPort,
                                               i4NextCVlanId, &i4RelayCVlanId);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        CliPrintf (CliHandle,
                   "switchport customer-vlan %d service-vlan %d",
                   i4NextCVlanId, i4SVlanId);

        if (i4NextCVlanId != i4RelayCVlanId)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle, " relay-vlan-id %d", i4RelayCVlanId);
        }
        nmhGetDot1adCVidRegistrationUntaggedPep (i4NextPort,
                                                 i4NextCVlanId, &i4UntaggedPep);

        nmhGetDot1adCVidRegistrationUntaggedCep (i4NextPort,
                                                 i4NextCVlanId, &i4UntaggedCep);

        if (i4UntaggedPep == VLAN_SNMP_TRUE)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle, " untagged-pep true");
        }

        if (i4UntaggedCep == VLAN_SNMP_TRUE)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle, " untagged-cep true");
        }

        CliPrintf (CliHandle, "\r\n");

        nmhGetDot1adCVidRegistrationSVlanPriorityType (i4NextPort,
                                                       i4NextCVlanId,
                                                       &i4SVlanPriorityType);
        nmhGetDot1adCVidRegistrationSVlanPriority (i4NextPort, i4NextCVlanId,
                                                   &i4SVlanPriority);

        switch (i4SVlanPriorityType)
        {
            case VLAN_SVLAN_PRIORITY_TYPE_FIXED:
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport customer-vlan %d svlan-priotype fixed %d\r\n",
                           i4NextCVlanId, i4SVlanPriority);
                break;
            case VLAN_SVLAN_PRIORITY_TYPE_COPY:
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport customer-vlan %d svlan-priotype copy\r\n",
                           i4NextCVlanId);
                break;
            default:
                break;
        }

        nmhGetFsMIPbPortCVlanCounterStatus ((INT4) u4CurrContextId, i4NextPort,
                                            (UINT4) i4NextCVlanId,
                                            &i4CVlanCounterStatus);
        if (VLAN_ENABLED == i4CVlanCounterStatus)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle, "set customer-vlan enable customer-vlan %d",
                       i4NextCVlanId);
        }
        if (VlanSelectContext (u4CurrContextId) == VLAN_FAILURE)
        {
            break;
        }

        *pu4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r\n");

        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4CVlanId = i4NextCVlanId;
    }

}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningEtherTypeSwapTable                */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for EtherTypeSwapTable                             */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningEtherTypeSwapTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                     UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4EtherType = 0;
    INT4                i4NextEtherType = 0;
    INT4                i4RelayEtherType = 0;

    while (nmhGetNextIndexFsPbEtherTypeSwapTable
           (i4LocalPort, &i4NextPort, i4EtherType, &i4NextEtherType)
           == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetFsPbRelayEtherType (i4NextPort, i4NextEtherType,
                                  &i4RelayEtherType);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus = CliPrintf (CliHandle,
                                      "switchport dot1q ethertype mapping 0x%x 0x%x\r\n",
                                      i4NextEtherType, i4RelayEtherType);
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4EtherType = i4NextEtherType;
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningVidTranslationTable               */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for VidTranslationTable                            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningVIDTranslationTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                      UINT4 *pu4PagingStatus,
                                      UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4SVlanId = 0;
    INT4                i4NextSVlanId = 0;
    INT4                i4RelaySVlanId = 0;

    while (nmhGetNextIndexDot1adVidTranslationTable
           (i4LocalPort, &i4NextPort, i4SVlanId,
            &i4NextSVlanId) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetDot1adVidTranslationRelayVid (i4NextPort,
                                            i4NextSVlanId, &i4RelaySVlanId);
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        *pu4PagingStatus = CliPrintf (CliHandle,
                                      "switchport service vlan mapping %d %d\r\n",
                                      i4NextSVlanId, i4RelaySVlanId);
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4SVlanId = i4NextSVlanId;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningConfigPerVlanTable                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the VLAN  service type      */
/*                        configuration                                      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4CurrentContextId -  Virtual context Id           */
/*                        i4CurrentVlanId    -  Vlan Id                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbShowRunningConfigPerVlanTable (tCliHandle CliHandle,
                                     UINT4 u4CurrentContextId,
                                     UINT4 u4CurrentVlanId, UINT1 *pu1HeadFlag)
{
    UINT4               u4NextContextId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4ServiceProtocolEnum = 1;
    INT4                i4NextServiceProtocolEnum = 0;
    INT4                i4ServiceVlanTunnelProtocolStatus = 0;

    while (nmhGetNextIndexFsMIServiceVlanTunnelProtocolTable
           ((INT4) u4CurrentContextId, (INT4 *) &u4NextContextId,
            (INT4) u4CurrentVlanId, &i4NextVlanId, i4ServiceProtocolEnum,
            &i4NextServiceProtocolEnum) == SNMP_SUCCESS)
    {
        if (((INT4) u4CurrentVlanId != i4NextVlanId)
            || (u4CurrentContextId != u4NextContextId))
        {
            break;
        }

        if (nmhGetFsMIServiceVlanTunnelProtocolStatus
            ((INT4) u4CurrentContextId, (INT4) u4CurrentVlanId,
             i4ServiceProtocolEnum,
             &i4ServiceVlanTunnelProtocolStatus) == SNMP_SUCCESS)
        {
            if ((i4ServiceProtocolEnum == L2_PROTO_GVRP) &&
                (i4ServiceVlanTunnelProtocolStatus !=
                 VLAN_TUNNEL_PROTOCOL_TUNNEL))
            {
                if (i4ServiceVlanTunnelProtocolStatus ==
                    VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintCtxt (CliHandle, u4CurrentContextId,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard gvrp\r\n");
                }
            }
            if ((i4ServiceProtocolEnum == L2_PROTO_GMRP) &&
                (i4ServiceVlanTunnelProtocolStatus !=
                 VLAN_TUNNEL_PROTOCOL_TUNNEL))
            {
                if (i4ServiceVlanTunnelProtocolStatus ==
                    VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintCtxt (CliHandle, u4CurrentContextId,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard gmrp\r\n");
                }
            }
            if ((i4ServiceProtocolEnum == L2_PROTO_MVRP) &&
                (i4ServiceVlanTunnelProtocolStatus !=
                 VLAN_TUNNEL_PROTOCOL_TUNNEL))
            {
                if (i4ServiceVlanTunnelProtocolStatus ==
                    VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintCtxt (CliHandle, u4CurrentContextId,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard mvrp\r\n");
                }
            }
            if ((i4ServiceProtocolEnum == L2_PROTO_MMRP) &&
                (i4ServiceVlanTunnelProtocolStatus !=
                 VLAN_TUNNEL_PROTOCOL_TUNNEL))
            {
                if (i4ServiceVlanTunnelProtocolStatus ==
                    VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintCtxt (CliHandle, u4CurrentContextId,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard mmrp\r\n");
                }
            }
            if ((i4ServiceProtocolEnum == L2_PROTO_IGMP) &&
                (i4ServiceVlanTunnelProtocolStatus !=
                 VLAN_TUNNEL_PROTOCOL_TUNNEL))
            {
                if (i4ServiceVlanTunnelProtocolStatus ==
                    VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintCtxt (CliHandle, u4CurrentContextId,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard igmp\r\n");
                }
                if (i4ServiceVlanTunnelProtocolStatus ==
                    VLAN_TUNNEL_PROTOCOL_PEER)
                {
                    VlanCliPrintCtxt (CliHandle, u4CurrentContextId,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-peer igmp\r\n");
                }
            }
            if ((i4ServiceProtocolEnum == L2_PROTO_ECFM) &&
                (i4ServiceVlanTunnelProtocolStatus !=
                 VLAN_TUNNEL_PROTOCOL_PEER))
            {
                if (i4ServiceVlanTunnelProtocolStatus ==
                    VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintCtxt (CliHandle, u4CurrentContextId,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard ecfm\r\n");
                }
                if (i4ServiceVlanTunnelProtocolStatus ==
                    VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintCtxt (CliHandle, u4CurrentContextId,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel ecfm\r\n");
                }
            }
        }
        u4CurrentContextId = u4NextContextId;
        u4CurrentVlanId = (UINT4) i4NextVlanId;
        i4ServiceProtocolEnum = i4NextServiceProtocolEnum;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningPepConfigTable                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for PEP configuration table                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningPepConfigTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                 UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag)
{
    INT4                i4SVlanId = 0;
    INT4                i4NextSVlanId = 0;
    INT4                i4AccptFrameType = 0;
    INT4                i4DefUserPriority = 0;
    INT4                i4IngFiltering = 0;
    INT4                i4Cpvid = 0;
    INT4                i4NextPort = 0;
    INT4                i4NextCVlanPort = 0;
    INT4                i4CosPreservation = 0;
    INT4                i4CVlanId = 0;
    INT4                i4NextCVlanId = 0;
    tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry = NULL;

    while (nmhGetNextIndexDot1adPepTable
           (i4LocalPort, &i4NextPort, i4SVlanId,
            &i4NextSVlanId) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetDot1adPepPvid (i4NextPort, i4NextSVlanId, &i4Cpvid);

        pVlanPbLogicalPortEntry =
            VlanPbGetLogicalPortEntry (i4NextPort, (tVlanId) i4NextSVlanId);
        if (pVlanPbLogicalPortEntry == NULL)
        {
            continue;
        }
        /* By default CVID is the CPVID for a specific SVID.
         * To skip displaying default value, comparison with CVID is required,
         * but CVID needs to be retreived from CVidRegistrationTable for the
         * specific SVID
         */
        i4NextCVlanPort = i4NextPort;
        while (nmhGetNextIndexDot1adCVidRegistrationTable
               (i4LocalPort, &i4NextCVlanPort, i4CVlanId,
                &i4NextCVlanId) == SNMP_SUCCESS)
        {
            if (i4LocalPort != i4NextCVlanPort)
            {
                break;
            }
            nmhGetDot1adCVidRegistrationSVid (i4NextCVlanPort,
                                              i4NextCVlanId, &i4SVlanId);
            if ((i4SVlanId == i4NextSVlanId) && (i4NextCVlanId != i4Cpvid))
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                *pu4PagingStatus = CliPrintf (CliHandle,
                                              "service-vlan %d pvid %d\r\n",
                                              i4NextSVlanId, i4Cpvid);
                break;
            }
            i4CVlanId = i4NextCVlanId;
        }

        nmhGetDot1adPepAccptableFrameTypes (i4NextPort, i4NextSVlanId,
                                            &i4AccptFrameType);

        if (i4AccptFrameType != VLAN_ADMIT_ALL_FRAMES)
        {
            if (i4AccptFrameType ==
                VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAMES)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                *pu4PagingStatus = CliPrintf (CliHandle,
                                              "service-vlan %d acceptable-frame-type"
                                              " untaggedandprioritytagged \r\n",
                                              i4NextSVlanId);
            }
            else if (i4AccptFrameType == VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                *pu4PagingStatus = CliPrintf (CliHandle,
                                              "service-vlan %d acceptable-frame-type"
                                              " tagged \r\n", i4NextSVlanId);
            }
        }

        nmhGetDot1adPepDefaultUserPriority (i4NextPort, i4NextSVlanId,
                                            &i4DefUserPriority);

        if (i4DefUserPriority != VLAN_DEF_USER_PRIORITY)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            *pu4PagingStatus = CliPrintf (CliHandle,
                                          "service-vlan %d def-user-priority"
                                          " %d \r\n", i4NextSVlanId,
                                          i4DefUserPriority);

        }
        nmhGetDot1adPepIngressFiltering (i4NextPort, i4NextSVlanId,
                                         &i4IngFiltering);

        if (i4IngFiltering != VLAN_SNMP_FALSE)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            *pu4PagingStatus = CliPrintf (CliHandle,
                                          "service-vlan %d ingress-filter"
                                          " enable \r\n", i4NextSVlanId);

        }
        nmhGetFsPbPepExtCosPreservation (i4NextPort, i4NextSVlanId,
                                         &i4CosPreservation);
        if (i4CosPreservation == VLAN_ENABLED)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            *pu4PagingStatus = CliPrintf (CliHandle,
                                          "service-vlan  %d cos-preservation enable \r\n",
                                          i4NextSVlanId);
        }

        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4SVlanId = i4NextSVlanId;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningConfigInterfaceDetails            */
/*                                                                           */
/*     DESCRIPTION      : This function displays current VLAN configurations */
/*                        for a particular interface                         */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        u1PbPortType - Port type                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4LocalPort,
                                         UINT1 u1PbPortType, UINT1 *pu1HeadFlag)
{
    INT4                i4ClassType = 0;
    INT4                i4CVlanId = 0;
#ifdef NPAPI_WANTED
    INT4                i4UntagFrameOnCEP = 0;
#endif
    INT4                i4EtherType = 0;
    INT4                i4TunnelStatus = VLAN_TUNNEL_PROTOCOL_INVALID;
    INT4                i4Status;
    INT4                i4RetVal = VLAN_ENABLED;
    INT4                i4SVlanPriorityType = VLAN_SVLAN_PRIORITY_TYPE_NONE;
    INT4                i4SVlanPriority = 0;
    INT4                i4TunnelOverrideStatus;
    UINT4               u4MacLimit = 0;
    UINT4               u4BridgeMode = VLAN_INVALID_BRIDGE_MODE;
    UINT4               u4PagingStatus = CLI_SUCCESS;

    if (nmhValidateIndexInstanceFsPbPortBasedCVlanTable (i4LocalPort) ==
        SNMP_SUCCESS)
    {
        if (u1PbPortType == VLAN_CUSTOMER_EDGE_PORT ||
            u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT ||
            u1PbPortType == VLAN_PROP_CUSTOMER_NETWORK_PORT)
        {
            nmhGetFsPbPortCVlan (i4LocalPort, &i4CVlanId);
            if (i4CVlanId != VLAN_NULL_VLAN_ID &&
                i4CVlanId != VLAN_DEFAULT_PORT_VID)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle, "switchport dot1q customer vlan %d\r\n",
                           i4CVlanId);
            }
#ifdef NPAPI_WANTED
            nmhGetFsPbPortEgressUntaggedStatus (i4LocalPort,
                                                &i4UntagFrameOnCEP);

            if (i4UntagFrameOnCEP == VLAN_ENABLED)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport dot1q customer vlan egress-untag"
                           " allow\r\n");
            }
#endif
            nmhGetFsPbPortCVlanClassifyStatus (i4LocalPort, &i4Status);

            if (u1PbPortType == VLAN_CUSTOMER_EDGE_PORT)
            {
                /*
                 * By default,for CEP port, dot1q customer vlan is in enable state
                 * since it is a default value, printing if it is in  disable state 
                 */
                if (i4Status == CLI_PB_DISABLED)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport dot1q customer vlan disable\r\n");
                }
            }
            else
            {
                /*
                 * By default,apart from CEP port,dot1q customer vlan state is in disable
                 * state. since it is a default value, printing if it is in enable state
                 */
                if (i4Status == CLI_PB_ENABLED)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport dot1q customer vlan enable\r\n");
                }
            }
        }
    }

    i4Status = nmhGetFsVlanTunnelStatus (i4LocalPort, &i4RetVal);
    nmhGetFsVlanBridgeMode ((INT4 *) &u4BridgeMode);
    if (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE ||
        u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE ||
        u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE)
    {

        if ((i4Status == SNMP_SUCCESS) && i4RetVal != VLAN_DISABLED)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle, "switchport mode dot1q-tunnel\r\n");
        }
    }

    if (i4RetVal == VLAN_ENABLED)
    {
        nmhGetFsVlanTunnelProtocolDot1x (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_DOT1X, i4LocalPort);

        nmhGetFsVlanTunnelProtocolLacp (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_LACP, i4LocalPort);

        nmhGetFsVlanTunnelProtocolStp (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_STP, i4LocalPort);

        nmhGetFsVlanTunnelProtocolGvrp (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_GVRP, i4LocalPort);

        nmhGetFsVlanTunnelProtocolGmrp (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_GMRP, i4LocalPort);

        nmhGetFsVlanTunnelProtocolIgmp (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_IGMP, i4LocalPort);

        nmhGetFsVlanTunnelProtocolEoam (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_EOAM, i4LocalPort);

        nmhGetFsVlanTunnelProtocolMvrp (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_MVRP, i4LocalPort);

        nmhGetFsVlanTunnelProtocolMmrp (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_MMRP, i4LocalPort);
        nmhGetFsVlanTunnelProtocolEcfm (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_ECFM, i4LocalPort);
        nmhGetFsVlanTunnelProtocolElmi (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_ELMI, i4LocalPort);
        nmhGetFsVlanTunnelProtocolLldp (i4LocalPort, &i4TunnelStatus);
        VlanPbDisplayProtoTunnelStatus (CliHandle, u1PbPortType, u4BridgeMode,
                                        i4TunnelStatus, pu1HeadFlag,
                                        CLI_PB_L2_PROTO_LLDP, i4LocalPort);

    }

    if (nmhValidateIndexInstanceFsPbPortInfoTable (i4LocalPort) == SNMP_SUCCESS)
    {
        nmhGetDot1adPortPcpSelectionRow (i4LocalPort, &i4RetVal);

        switch (i4RetVal)
        {
            case VLAN_7P1D_SEL_ROW:
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport provider-bridge pcp-selection-row 7P1D\r\n");
                break;
            case VLAN_6P2D_SEL_ROW:
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport provider-bridge pcp-selection-row 6P2D\r\n");
                break;
            case VLAN_5P3D_SEL_ROW:
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport provider-bridge pcp-selection-row 5P3D\r\n");
                break;
        }

        nmhGetDot1adPortUseDei (i4LocalPort, &i4RetVal);
        if (i4RetVal == VLAN_SNMP_TRUE)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle,
                       "switchport provider-bridge use-dei true\r\n");
        }

        nmhGetDot1adPortReqDropEncoding (i4LocalPort, &i4RetVal);
        if (i4RetVal == VLAN_SNMP_TRUE)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle,
                       "switchport provider-bridge require-drop-encoding true\r\n");
        }

        nmhGetDot1adPortSVlanPriorityType (i4LocalPort, &i4SVlanPriorityType);
        nmhGetDot1adPortSVlanPriority (i4LocalPort, &i4SVlanPriority);

        switch (i4SVlanPriorityType)
        {
            case VLAN_SVLAN_PRIORITY_TYPE_FIXED:
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport svlan-priotype fixed %d\r\n",
                           i4SVlanPriority);
                break;
            case VLAN_SVLAN_PRIORITY_TYPE_COPY:
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle, "switchport svlan-priotype copy\r\n");
                break;
            default:
                break;
        }
        nmhGetFsPbPortSVlanClassificationMethod (i4LocalPort, &i4ClassType);

        if (((u1PbPortType == VLAN_CUSTOMER_EDGE_PORT) &&
             (i4ClassType != CLI_PB_CVLAN)) ||
            (((u1PbPortType == VLAN_PROP_CUSTOMER_EDGE_PORT) ||
              (u1PbPortType == VLAN_PROP_CUSTOMER_NETWORK_PORT))
             && (i4ClassType != CLI_PB_PVID)))
        {
            switch (i4ClassType)
            {
                case CLI_PB_SRCMAC:
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport service vlan classify srcMac\r\n");
                }
                    break;
                case CLI_PB_DSTMAC:
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport service vlan classify dstMac\r\n");
                }
                    break;
                case CLI_PB_SRCMAC_CVLAN:
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport service vlan classify"
                               " cvlanSrcMac\r\n");
                }
                    break;
                case CLI_PB_DSTMAC_CVLAN:
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport service vlan classify"
                               " cvlanDstMac\r\n");
                }
                    break;
                case CLI_PB_DSCP:
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport service vlan classify dscp\r\n");
                }
                    break;
                case CLI_PB_DSCP_CVLAN:
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport service vlan classify"
                               " cvlanDscp\r\n");
                }
                    break;
                case CLI_PB_SRCIP:
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport service vlan classify srcIp\r\n");
                }
                    break;
                case CLI_PB_DSTIP:
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport service vlan classify dstIp\r\n");
                }
                    break;
                case CLI_PB_SRCIP_DSTIP:
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport service vlan classify srcIpDstIp\r\n");
                }
                    break;
                case CLI_PB_DSTIP_CVLAN:
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle,
                               "switchport service vlan classify"
                               " cvlanDstIp\r\n");
                }
                    break;
            }

        }

        nmhGetFsPbPortSVlanIngressEtherType (i4LocalPort, &i4EtherType);
        /* For PPNP 0x8100 is the default value for Ingress ethertype,
         * and for all other port types 0x88a8 is the default value
         */
        if (u1PbPortType == VLAN_PROP_PROVIDER_NETWORK_PORT)
        {
            if (i4EtherType != VLAN_PROTOCOL_ID)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport dot1q ingress ether-type 0x%x\r\n",
                           i4EtherType);
            }
        }
        else
        {
            if (i4EtherType != PB_DEFAULT_PORT_ETHERTYPE)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport dot1q ingress ether-type 0x%x\r\n",
                           i4EtherType);
            }
        }

        nmhGetFsPbPortSVlanEgressEtherType (i4LocalPort, &i4EtherType);
        /* For CEP, CNP_PORTBASED and PPNP 0x8100 is the default value for egress ethertype
         * and for all other port types 0x88a8 is the default value
         */
        if (u1PbPortType == VLAN_CUSTOMER_EDGE_PORT ||
            u1PbPortType == VLAN_PROP_PROVIDER_NETWORK_PORT ||
            u1PbPortType == VLAN_CNP_PORTBASED_PORT)
        {
            if (i4EtherType != VLAN_PROTOCOL_ID)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport dot1q egress ether-type 0x%x\r\n",
                           i4EtherType);
            }
        }
        else
        {
            if (i4EtherType != PB_DEFAULT_PORT_ETHERTYPE)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "switchport dot1q egress ether-type 0x%x\r\n",
                           i4EtherType);
            }
        }
        nmhGetFsPbPortSVlanEtherTypeSwapStatus (i4LocalPort, &i4Status);
        if (i4Status == CLI_PB_ENABLED)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle, "set switchport ether-type swap enable\r\n");
        }
        nmhGetFsPbPortSVlanTranslationStatus (i4LocalPort, &i4Status);
        if (u1PbPortType == VLAN_PROVIDER_NETWORK_PORT ||
            u1PbPortType == VLAN_CNP_TAGGED_PORT)
        {
            if (i4Status == CLI_PB_DISABLED)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "set switchport service vlan swap disable\r\n");
            }
        }
        else
        {
            if (i4Status == CLI_PB_ENABLED)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle,
                           "set switchport service vlan swap enable\r\n");
            }
        }
        nmhGetFsPbPortUnicastMacLearning (i4LocalPort, &i4Status);
        if (i4Status != CLI_PB_ENABLED)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle,
                       "switchport unicast-mac learning disable\r\n");
        }
        nmhGetFsPbPortUnicastMacLimit (i4LocalPort, &u4MacLimit);

        if (u4MacLimit != PB_DEFAULT_PORT_MAC_LIMIT)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            CliPrintf (CliHandle,
                       "switchport unicast-mac learning limit %d\r\n",
                       u4MacLimit);
        }
    }

    VlanPbShowRunningSrcMacSVlanTable (CliHandle, i4LocalPort, &u4PagingStatus,
                                       pu1HeadFlag);

    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningDstMacSVlanTable (CliHandle, i4LocalPort,
                                           &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningCVlanSrcMacSVlanTable (CliHandle, i4LocalPort,
                                                &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningCVlanDstMacSVlanTable (CliHandle, i4LocalPort,
                                                &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningDscpSVlanTable (CliHandle, i4LocalPort,
                                         &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningCVlanDscpSVlanTable (CliHandle, i4LocalPort,
                                              &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningSrcIPSVlanTable (CliHandle, i4LocalPort,
                                          &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningDstIPSVlanTable (CliHandle, i4LocalPort,
                                          &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningSrcDstIPSVlanTable (CliHandle, i4LocalPort,
                                             &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningCVlanDstIPSVlanTable (CliHandle, i4LocalPort,
                                               &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningCVidSVlanTable (CliHandle, i4LocalPort,
                                         &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningEtherTypeSwapTable (CliHandle, i4LocalPort,
                                             &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningVIDTranslationTable (CliHandle, i4LocalPort,
                                              &u4PagingStatus, pu1HeadFlag);
    }

    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningPcpDecodingTable (CliHandle, i4LocalPort,
                                           &u4PagingStatus, pu1HeadFlag);
    }

    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningPcpEncodingTable (CliHandle, i4LocalPort,
                                           &u4PagingStatus, pu1HeadFlag);
    }

    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningServicePrioRegenTable (CliHandle, i4LocalPort,
                                                &u4PagingStatus, pu1HeadFlag);
    }

    if (u4PagingStatus == CLI_SUCCESS)
    {
        VlanPbShowRunningPepConfigTable (CliHandle, i4LocalPort,
                                         &u4PagingStatus, pu1HeadFlag);
    }
    if (u4PagingStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetFsVlanTunnelOverrideOption (i4LocalPort, &i4TunnelOverrideStatus);
    if (i4TunnelOverrideStatus == VLAN_OVERRIDE_OPTION_ENABLE)
    {
        VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
        CliPrintf (CliHandle, "l2protocol-tunnel override enable\r\n");
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbPrintProtocolTunnelStatus                   */
/*                                                                          */
/*     DESCRIPTION      : This function displays the protocol tunnel status */
/*                        on a port.                                        */
/*                                                                          */
/*     INPUT            : i4TunnelStatus  - Tunnel status                   */
/*                        i4Protocol      - L2 protocol                     */
/*                                                                          */
/*     OUTPUT           : None                                              */
/*                                                                          */
/*     RETURNS          : None                                              */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbPrintProtocolTunnelStatus (tCliHandle CliHandle, INT4 i4TunnelStatus,
                                 INT4 i4Protocol)
{
    switch (i4Protocol)
    {
        case CLI_PB_L2_PROTO_DOT1X:
            CliPrintf (CliHandle, " Dot1x Protocol Tunnel Status        :");
            break;
        case CLI_PB_L2_PROTO_LACP:
            CliPrintf (CliHandle, " LACP Protocol Tunnel Status         :");
            break;
        case CLI_PB_L2_PROTO_STP:
            CliPrintf (CliHandle, " Spanning Tree Tunnel Status         :");
            break;
        case CLI_PB_L2_PROTO_GVRP:
            CliPrintf (CliHandle, " GVRP Protocol Tunnel Status         :");
            break;
        case CLI_PB_L2_PROTO_GMRP:
            CliPrintf (CliHandle, " GMRP Protocol Tunnel Status         :");
            break;
        case CLI_PB_L2_PROTO_IGMP:
            CliPrintf (CliHandle, " IGMP Protocol Tunnel Status         :");
            break;
        case CLI_PB_L2_PROTO_MVRP:
            CliPrintf (CliHandle, " MVRP Protocol Tunnel Status         :");
            break;
        case CLI_PB_L2_PROTO_MMRP:
            CliPrintf (CliHandle, " MMRP Protocol Tunnel Status         :");
            break;
        case CLI_PB_L2_PROTO_ELMI:
            CliPrintf (CliHandle, " ELMI Protocol Tunnel Status         :");
            break;
        case CLI_PB_L2_PROTO_LLDP:
            CliPrintf (CliHandle, " LLDP Protocol Tunnel Status         :");
            break;
        case CLI_PB_L2_PROTO_ECFM:
            CliPrintf (CliHandle, " ECFM Protocol Tunnel Status         :");
            break;
        case CLI_PB_L2_PROTO_EOAM:
            CliPrintf (CliHandle, " EOAM Protocol Tunnel Status         :");
            break;
        default:
            break;
    }

    switch (i4TunnelStatus)
    {
        case VLAN_TUNNEL_PROTOCOL_PEER:
            CliPrintf (CliHandle, " Peer\r\n");
            break;
        case VLAN_TUNNEL_PROTOCOL_TUNNEL:
            CliPrintf (CliHandle, " Tunnel\r\n");
            break;
        case VLAN_TUNNEL_PROTOCOL_DISCARD:
            CliPrintf (CliHandle, " Discard\r\n");
            break;
        default:
            CliPrintf (CliHandle, " Unknown\r\n");
            break;
    }
    return;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbDisplayProtoTunnelStatus                    */
/*                                                                          */
/*     DESCRIPTION      : This function displays the CLI commands for the   */
/*                        protocol tunnel status configuration on a port.   */
/*                                                                          */
/*     INPUT            : i4TunnelStatus  - Tunnel status                   */
/*                        i4Protocol      - L2 protocol                     */
/*                        u1BrgPortType   - bridge port-type                */
/*                        u4BridgeMode    - Bridge mode                     */
/*                                                                          */
/*     OUTPUT           : CliHandle                                         */
/*                                                                          */
/*     RETURNS          : None                                              */
/*                                                                          */
/****************************************************************************/
VOID
VlanPbDisplayProtoTunnelStatus (tCliHandle CliHandle, UINT1 u1BrgPortType,
                                UINT4 u4BridgeMode, INT4 i4TunnelStatus,
                                UINT1 *pu1HeadFlag, INT4 i4Protocol,
                                INT4 i4PortId)
{

    switch (i4Protocol)
    {
            /* The following is the Default values for PEB/PCB
             * ---------------------------------------------------------
             *         CEP       CNP(P)     PCEP     PCNP
             * dot1x   peer      peer       peer     peer
             * lacp    peer      peer       peer     peer
             * eoam    peer      peer       peer     peer
             * stp     peer      tunnel     tunnel   tunnel
             * gvrp    discard   tunnel     tunnel   tunnel
             * gmrp    discard   tunnel     tunnel   tunnel
             * igmp    tunnel    tunnel     tunnel   tunnel
             *
             * and for Customer/Provider bridge l2protocol-peer is the 
             * default value for all the protocols.
             */

        case L2_PROTO_DOT1X:
            if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                CliPrintf (CliHandle, "l2protocol-tunnel dot1x\r\n");
            }
            if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                CliPrintf (CliHandle, "l2protocol-discard dot1x\r\n");
            }
            break;

        case L2_PROTO_LACP:
            if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                CliPrintf (CliHandle, "l2protocol-tunnel lacp\r\n");
            }
            if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                CliPrintf (CliHandle, "l2protocol-discard lacp\r\n");
            }
            break;

        case L2_PROTO_STP:
            if ((u4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel stp\r\n");
                }
                if ((i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                    && (VlanAstIsStpEnabled (i4PortId, VLAN_CURR_CONTEXT_ID ())
                        == OSIX_SUCCESS))
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard stp\r\n");
                }
            }
            else if (u1BrgPortType == VLAN_CUSTOMER_EDGE_PORT)
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel stp\r\n");
                }
                if ((i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                    && (VlanAstIsStpEnabled (i4PortId, VLAN_CURR_CONTEXT_ID ())
                        == OSIX_SUCCESS))
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard stp\r\n");
                }
            }
            else
            {
                if ((i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                    && (VlanAstIsStpEnabled (i4PortId, VLAN_CURR_CONTEXT_ID ())
                        == OSIX_SUCCESS))
                {
                    CliPrintf (CliHandle, "l2protocol-discard stp\r\n");
                }
            }
            /* As for PEB/PCB STP peering can be enabled only on CEP where it is
             * default vlaue. And for Customer bridge/ Provider Bridge STP 
             * Peering is the default value. So no need to print 
             * `l2protocol-peer stp` at any condition.
             */
            break;

        case L2_PROTO_GVRP:
            if ((u4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE
                    || u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE
                    || u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel gvrp\r\n");
                }
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard gvrp\r\n");
                }
            }
            else if (u1BrgPortType == VLAN_CUSTOMER_EDGE_PORT)
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel gvrp\r\n");
                }
            }
            else
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard gvrp\r\n");
                }
            }
            /* For PEB/PCB GVRP Peering is not supported. And for Customer/
             * Provider Bridge GVRP Peer is the default value. So no need to
             * print `l2protocol-peer gvrp`.
             */
            break;
        case L2_PROTO_GMRP:
            if ((u4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE
                    || u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE
                    || u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel gmrp\r\n");
                }
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard gmrp\r\n");
                }
            }
            else if (u1BrgPortType == VLAN_CUSTOMER_EDGE_PORT)
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel gmrp\r\n");
                }
            }
            else
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard gmrp\r\n");
                }
            }
            /* For PEB/PCB GMRP Peering is not supported. And for Customer/
             * Provider bridge GMRP Peer is the default value. So no need to 
             * print `l2protocol-peer gmrp`
             */
            break;
        case L2_PROTO_IGMP:
            if ((u4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE
                    || u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE
                    || u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel igmp\r\n");
                }
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard igmp\r\n");
                }
            }
            else
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard igmp\r\n");
                }
            }
            /* For PEB/PCB IGMP Peering is not supported. And for Customer/
             * Provider bridge GMRP Peer is the default value. So no need to 
             * print `l2protocol-peer igmp`
             */
            break;
        case L2_PROTO_EOAM:
            if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                CliPrintf (CliHandle, "l2protocol-tunnel eoam\r\n");
            }
            if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                CliPrintf (CliHandle, "l2protocol-discard eoam\r\n");
            }
            break;

        case L2_PROTO_MVRP:
            if ((u4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE
                    || u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE
                    || u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel mvrp\r\n");
                }
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard mvrp\r\n");
                }
            }
            else if (u1BrgPortType == VLAN_CUSTOMER_EDGE_PORT)
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel mvrp\r\n");
                }
            }
            else
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard mvrp\r\n");
                }
            }
            break;
        case L2_PROTO_MMRP:
            if ((u4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel mmrp\r\n");
                }
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard mmrp\r\n");
                }
            }
            else if (u1BrgPortType == VLAN_CUSTOMER_EDGE_PORT)
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel mmrp\r\n");
                }
            }
            else
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard mmrp\r\n");
                }
            }
            break;
        case L2_PROTO_ECFM:
            if ((u4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel ecfm\r\n");
                }
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard ecfm\r\n");
                }
            }
            else if ((u1BrgPortType == VLAN_CUSTOMER_EDGE_PORT)
                     || (u1BrgPortType == VLAN_CNP_PORTBASED_PORT))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel ecfm\r\n");
                }
                else if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard ecfm\r\n");
                }
            }
            else
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard ecfm\r\n");
                }
            }
            break;
        case L2_PROTO_ELMI:
            if ((u4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel elmi\r\n");
                }
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard elmi\r\n");
                }
            }
            else if ((u1BrgPortType == VLAN_CUSTOMER_EDGE_PORT)
                     || (u1BrgPortType == VLAN_CNP_PORTBASED_PORT))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel elmi\r\n");
                }
                else if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard elmi\r\n");
                }
            }
            else
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard elmi\r\n");
                }
            }

            break;
        case L2_PROTO_LLDP:
            if ((u4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE)
                || (u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel lldp\r\n");
                }
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard lldp\r\n");
                }
            }
            else if ((u1BrgPortType == VLAN_CUSTOMER_EDGE_PORT)
                     || (u1BrgPortType == VLAN_CNP_PORTBASED_PORT))
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-tunnel lldp\r\n");
                }
                else if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard lldp\r\n");
                }
            }
            else
            {
                if (i4TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanCliPrintIntf (CliHandle, (UINT4) i4PortId, pu1HeadFlag);
                    CliPrintf (CliHandle, "l2protocol-discard lldp\r\n");
                }
            }

            break;

    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : DisplayPbServiceVlanInfo                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the VLAN  service type      */
/*                        configuration                                      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4VlanId - Specified Vlan id                       */
/*                                   for configuration                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
DisplayPbServiceVlanInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4VlanId, UINT1 *pu1isActiveVlan)
{
    INT4                i4ServiceType;

    if (nmhGetFsMIPbSVlanConfigServiceType ((INT4) u4ContextId, u4VlanId,
                                            &i4ServiceType) == SNMP_SUCCESS)
    {
        if (i4ServiceType == VLAN_E_LINE)
        {
            if (*pu1isActiveVlan == FALSE)
            {
                CliPrintf (CliHandle, "!\r\n");
                CliPrintf (CliHandle, "vlan %d\r\n", u4VlanId);
                *pu1isActiveVlan = TRUE;
            }
            CliPrintf (CliHandle, "service-type e-line \r\n");
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbCliVlanDisplay                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the VLAN service type       */
/*                       & Fdb Mac learning Status configuration.This        */
/*                       function is called from show VLAN Current database  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4VlanId - Specified Vlan id                       */
/*                                   for configuration                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
VlanPbCliVlanDisplay (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4VlanId)
{
    INT4                i4VlanServiceType = VLAN_E_LINE;
    INT4                i4LearningStatus = VLAN_DISABLED;
    INT4                i4ContextId = 0;

    i4ContextId = (INT4) u4ContextId;

    if (nmhValidateIndexInstanceFsMIPbSVlanConfigTable (i4ContextId,
                                                        u4VlanId)
        != SNMP_SUCCESS)
    {
        return;
    }

    nmhGetFsMIPbSVlanConfigServiceType (i4ContextId, u4VlanId,
                                        &i4VlanServiceType);

    if (i4VlanServiceType == VLAN_E_LINE)
    {
        CliPrintf (CliHandle, "ServiceType         : E-LINE \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "ServiceType         : E-LAN \r\n");
    }

    nmhGetFsMIDot1qFutureVlanAdminMacLearningStatus (i4ContextId,
                                                     u4VlanId,
                                                     &i4LearningStatus);

    if (i4LearningStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "MacLearning Admin-Status  : Enabled \r\n");
    }
    else if (i4LearningStatus == VLAN_DISABLED)
    {
        CliPrintf (CliHandle, "MacLearning Admin-Status  : Disabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "MacLearning Admin-Status  : Default \r\n");
    }

    nmhGetFsMIDot1qFutureVlanOperMacLearningStatus (i4ContextId,
                                                    u4VlanId,
                                                    &i4LearningStatus);

    if (i4LearningStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, "MacLearning Oper-Status   : Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "MacLearning Oper-Status   : Disabled \r\n");
    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningPcpDecodingTable                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays current Pcp decoding table  */
/*                        configurations                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningPcpDecodingTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                   UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4NextPcpSelRow = 0;
    INT4                i4NextPcpVal = 0;
    INT4                i4CurrPcpSelRow = 0;
    INT4                i4CurrPcpVal = 0;
    INT4                i4Priority = 0;
    INT4                i4DropEligible = 0;
    INT4                i4TempPriority = 0;
    INT4                i4TempDropEligible = 0;

    while (nmhGetNextIndexDot1adPcpDecodingTable
           (i4LocalPort, &i4NextPort, i4CurrPcpSelRow, &i4NextPcpSelRow,
            i4CurrPcpVal, &i4NextPcpVal) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetDot1adPcpDecodingPriority (i4NextPort, i4NextPcpSelRow,
                                         i4NextPcpVal, &i4Priority);

        nmhGetDot1adPcpDecodingDropEligible (i4NextPort,
                                             i4NextPcpSelRow,
                                             i4NextPcpVal, &i4DropEligible);

        VlanPbGetDecodingPriority (i4NextPcpSelRow, i4NextPcpVal,
                                   &i4TempPriority);
        VlanPbGetDecodingDropEligible (i4NextPcpSelRow,
                                       i4NextPcpVal, &i4TempDropEligible);

        if (i4TempDropEligible == VLAN_DE_FALSE)
        {
            i4TempDropEligible = VLAN_SNMP_FALSE;
        }

        if ((i4Priority != i4TempPriority) ||
            (i4DropEligible != i4TempDropEligible))
        {
            switch (i4NextPcpSelRow)
            {
                case VLAN_8P0D_SEL_ROW:
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "pcp-decoding 8P0D pcp %d",
                               i4NextPcpVal);
                    break;
                case VLAN_7P1D_SEL_ROW:
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "pcp-decoding 7P1D pcp %d",
                               i4NextPcpVal);
                    break;
                case VLAN_6P2D_SEL_ROW:
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "pcp-decoding 6P2D pcp %d",
                               i4NextPcpVal);
                    break;
                case VLAN_5P3D_SEL_ROW:
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "pcp-decoding 5P3D pcp %d",
                               i4NextPcpVal);
                    break;
            }

            CliPrintf (CliHandle, " priority %d", i4Priority);

            if (i4DropEligible == VLAN_SNMP_FALSE)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle, " drop-eligible false");
            }
            else if (i4DropEligible == VLAN_SNMP_TRUE)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle, " drop-eligible true");
            }

            *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
        }
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4CurrPcpSelRow = i4NextPcpSelRow;
        i4CurrPcpVal = i4NextPcpVal;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningPcpEncodingTable                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays current Pcp encoding table  */
/*                        configurations                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningPcpEncodingTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                   UINT4 *pu4PagingStatus, UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4NextPcpSelRow = 0;
    INT4                i4NextPriority = 0;
    INT4                i4NextDropEligible = 0;
    INT4                i4CurrPcpSelRow = 0;
    INT4                i4CurrPriority = 0;
    INT4                i4CurrDropEligible = 0;
    INT4                i4PcpVal = 0;
    INT4                i4TempPcpVal = 0;
    INT4                i4TempDropEligible = 0;

    while (nmhGetNextIndexDot1adPcpEncodingTable
           (i4LocalPort, &i4NextPort, i4CurrPcpSelRow, &i4NextPcpSelRow,
            i4CurrPriority, &i4NextPriority, i4CurrDropEligible,
            &i4NextDropEligible) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        if (i4NextDropEligible == VLAN_SNMP_FALSE)
        {
            i4TempDropEligible = VLAN_DE_FALSE;
        }
        else
        {
            i4TempDropEligible = VLAN_DE_TRUE;
        }
        nmhGetDot1adPcpEncodingPcpValue (i4NextPort, i4NextPcpSelRow,
                                         i4NextPriority,
                                         i4NextDropEligible, &i4PcpVal);

        VlanPbGetEncodingPcpVal (i4NextPcpSelRow, i4NextPriority,
                                 i4TempDropEligible, &i4TempPcpVal);

        if (i4PcpVal != i4TempPcpVal)
        {
            switch (i4NextPcpSelRow)
            {
                case VLAN_8P0D_SEL_ROW:
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "pcp-encoding 8P0D priority %d",
                               i4NextPriority);
                    break;
                case VLAN_7P1D_SEL_ROW:
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "pcp-encoding 7P1D priority %d",
                               i4NextPriority);
                    break;
                case VLAN_6P2D_SEL_ROW:
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "pcp-encoding 6P2D priority %d",
                               i4NextPriority);
                    break;
                case VLAN_5P3D_SEL_ROW:
                    VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort,
                                      pu1HeadFlag);
                    CliPrintf (CliHandle, "pcp-encoding 5P3D priority %d",
                               i4NextPriority);
                    break;

            }

            if (i4NextDropEligible == VLAN_SNMP_FALSE)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle, " drop-eligible false");
            }
            else if (i4NextDropEligible == VLAN_SNMP_TRUE)
            {
                VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
                CliPrintf (CliHandle, " drop-eligible true");
            }

            *pu4PagingStatus = CliPrintf (CliHandle, " pcp %d\r\n", i4PcpVal);
        }
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4CurrPcpSelRow = i4NextPcpSelRow;
        i4CurrPriority = i4NextPriority;
        i4CurrDropEligible = i4NextDropEligible;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowRunningServicePrioRegenTable             */
/*                                                                           */
/*     DESCRIPTION      : This function displays current service priority    */
/*                        regeneration table configurations                  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4LocalPort - Local port of a particular context   */
/*                        pu4PagingStatus - CLI paging status                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
VlanPbShowRunningServicePrioRegenTable (tCliHandle CliHandle, INT4 i4LocalPort,
                                        UINT4 *pu4PagingStatus,
                                        UINT1 *pu1HeadFlag)
{
    INT4                i4NextPort = 0;
    INT4                i4NextVlan = 0;
    INT4                i4NextRcvPriority = 0;
    INT4                i4CurrVlan = 0;
    INT4                i4CurrRcvPriority = 0;
    INT4                i4RegenPriority = 0;

    while (nmhGetNextIndexDot1adServicePriorityRegenerationTable
           (i4LocalPort, &i4NextPort, i4CurrVlan,
            &i4NextVlan, i4CurrRcvPriority, &i4NextRcvPriority) == SNMP_SUCCESS)
    {
        if (i4LocalPort != i4NextPort)
        {
            break;
        }
        nmhGetDot1adServicePriorityRegenRegeneratedPriority
            (i4NextPort, i4NextVlan, i4NextRcvPriority, &i4RegenPriority);

        /*By default RCV Priority and Regen Priority will be same */
        if (i4RegenPriority != i4NextRcvPriority)
        {
            VlanCliPrintIntf (CliHandle, (UINT4) i4LocalPort, pu1HeadFlag);
            *pu4PagingStatus = CliPrintf (CliHandle,
                                          "service-vlan %d recv-priority %d regen-priority %d\r\n",
                                          i4NextVlan, i4NextRcvPriority,
                                          i4RegenPriority);
        }
        if (*pu4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4CurrVlan = i4NextVlan;
        i4CurrRcvPriority = i4NextRcvPriority;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetVlanServiceType                           */
/*                                                                           */
/*     DESCRIPTION      : This function configures the VLAN service type     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        VlanId - Specified VlanId                          */
/*                        i4ServiceType - ELINE/ELAN                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbSetVlanServiceType (tCliHandle CliHandle, tVlanId VlanId,
                          INT4 i4ServiceType)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPbSVlanConfigServiceType (&u4ErrorCode, VlanId,
                                             i4ServiceType) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPbSVlanConfigServiceType (VlanId, i4ServiceType) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetL2ProtocolTunnelStatusPerVLAN            */
/*                                                                          */
/*     DESCRIPTION      : This function will Set the Protocol Tunnel staus  */
/*                                                                          */
/*     INPUT            : u4ContextId  - Context Id                         */
/*                        tVlanId      - VlanId                             */
/*                        u4L2PType    - ProtocolId                         */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                        u4TunnelStatus - Tunnel Status                    */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbSetL2ProtocolTunnelStatusPerVLAN (tCliHandle CliHandle, UINT4 u4ContextId,
                                        tVlanId VlanId, UINT4 u4L2PType,
                                        UINT4 u4TunnelStatus)
{
    UINT4               u4ErrCode;

    if (VlanId > VLAN_MAX_VLAN_ID || VlanId <= 0)
    {
        CliPrintf (CliHandle, "\r%% Invalid VLAN range " "specified.\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMIServiceVlanTunnelProtocolStatus (&u4ErrCode,
                                                      (INT4) u4ContextId,
                                                      (INT4) VlanId,
                                                      (INT4) u4L2PType,
                                                      (INT4) u4TunnelStatus) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIServiceVlanTunnelProtocolStatus (u4ContextId,
                                                   VlanId, u4L2PType,
                                                   u4TunnelStatus) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPortReqDropEncoding                       */
/*                                                                           */
/*     DESCRIPTION      : This function configures the Req drop encoding for */
/*                        Port.                                              */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Port - Specified Port                            */
/*                        i4ReqDropEncoding - TRUE /FALSE                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbSetPortReqDropEncoding (tCliHandle CliHandle, INT4 i4PortId,
                              INT4 i4ReqDropEncoding)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2Dot1adPortReqDropEncoding (&u4ErrorCode, i4PortId,
                                            i4ReqDropEncoding) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1adPortReqDropEncoding (i4PortId, i4ReqDropEncoding)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPortPcpSelRow                             */
/*                                                                           */
/*     DESCRIPTION      : This function configures the for Pcp Selection row */
/*                        for a Port.                                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Port - Specified Port                            */
/*                        i4PcpSelRow - VLAN_8P0D_SEL_ROW /                  */
/*                                      VLAN_7P1D_SEL_ROW /                  */
/*                                      VLAN_6P2D_SEL_ROW /                  */
/*                                      VLAN_5P3D_SEL_ROW                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbSetPortPcpSelRow (tCliHandle CliHandle, INT4 i4PortId, INT4 i4PcpSelRow)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2Dot1adPortPcpSelectionRow (&u4ErrorCode, i4PortId,
                                            i4PcpSelRow) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1adPortPcpSelectionRow (i4PortId, i4PcpSelRow) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbSetPortUseDei                                */
/*                                                                           */
/*     DESCRIPTION      : This function configures the Use Dei for Port      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Port - Specified Port                            */
/*                        i4UseDei - TRUE /FALSE                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbSetPortUseDei (tCliHandle CliHandle, INT4 i4PortId, INT4 i4UseDei)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2Dot1adPortUseDei (&u4ErrorCode, i4PortId, i4UseDei)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1adPortUseDei (i4PortId, i4UseDei) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : VlanPbCheckSystemStatus                            */
/*                                                                           */
/*     DESCRIPTION      : This function check Bridge mode, Vlan Status and   */
/*                        Validates Context.                                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - Context Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbCheckSystemStatus (tCliHandle CliHandle, UINT4 u4ContextId)
{
    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Virtual Switch\r\n ");
        return VLAN_FAILURE;
    }

    if (VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r%% VLAN switching is shutdown\r\n");
        VlanReleaseContext ();
        return VLAN_FAILURE;
    }

    VlanReleaseContext ();
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbCliShowL2ProtocolServiceVlan                     */
/*                                                                           */
/*     DESCRIPTION      : This function show entries in the per vlan         */
/*                        table.                                             */
/*                                                                           */
/*     INPUT            : tCliHandle  CliHandle - Context in which the CLI   */
/*                                             command is processed          */
/*                        u4ContextId - Context Identifier                   */
/*                        VlanId      - Vlan ID                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully         */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                  */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbCliShowL2ProtocolServiceVlan (tCliHandle CliHandle, UINT4 u4ContextId,
                                    UINT1 u1Summary, tVlanId VlanId)
{
    UINT4               u4NextContextId = 0;
    tMacAddr            ServiceVlanRsvdMacaddress;
    tMacAddr            ServiceVlanTunnelMacaddress;
    INT4                i4ServiceVlanTunnelProtocolStatus;
    INT4                i4ServiceProtocolEnum = L2_PROTO_GVRP;
    INT4                i4NextServiceProtocolEnum = 0;
    INT4                i4CurrentVlanId = VlanId;
    INT4                i4NextVlanId = 0;
    INT4                i4Result = 0;
    UINT1               au1String[VLAN_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1TunnelStatus[3][8] = { "Peer", "Tunnel", "Discard" };
    UINT1               u1ProtocolList[12][6] =
        { "DOT1X", "LACP", "STP", "GVRP", "GMPR", "IGMP",
        "MVRP", "MMRP", "EOAM", "ELMI", "LLDP", "ECFM"
    };
    if (u1Summary)
    {
        i4Result =
            VlanPbCliShowL2PrtclServiceVlanTunnelSummary (CliHandle,
                                                          u4ContextId,
                                                          u1Summary);
        return i4Result;
    }

    CliPrintf (CliHandle, "\r\nService Vlan Tunneling:\r\n\r\n");

    CliPrintf (CliHandle, " VLAN ID ");

    CliPrintf (CliHandle, " Protocol ");

    CliPrintf (CliHandle, " Reserved Mac         ");

    CliPrintf (CliHandle, " Tunnel Mac           ");

    CliPrintf (CliHandle, " Status\r\n");

    CliPrintf (CliHandle, " ------- ");

    CliPrintf (CliHandle, " -------- ");

    CliPrintf (CliHandle, " ------------         ");

    CliPrintf (CliHandle, " ----------           ");

    CliPrintf (CliHandle, " ----------\r\n");

    if (!i4CurrentVlanId)
    {
        if (nmhGetFirstIndexFsMIServiceVlanTunnelProtocolTable
            ((INT4 *) &u4ContextId, (INT4 *) &i4CurrentVlanId,
             (INT4 *) &i4ServiceProtocolEnum) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhValidateIndexInstanceFsMIServiceVlanTunnelProtocolTable
            (u4ContextId, i4CurrentVlanId,
             i4ServiceProtocolEnum) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    while (nmhGetNextIndexFsMIServiceVlanTunnelProtocolTable
           (u4ContextId, (INT4 *) &u4NextContextId, i4CurrentVlanId,
            (INT4 *) &i4NextVlanId, i4ServiceProtocolEnum,
            (INT4 *) &i4NextServiceProtocolEnum) == SNMP_SUCCESS)
    {
        if (VlanId && i4CurrentVlanId != i4NextVlanId)
        {
            break;
        }
        if (i4ServiceProtocolEnum == L2_PROTO_GVRP
            || i4ServiceProtocolEnum == L2_PROTO_GMRP
            || i4ServiceProtocolEnum == L2_PROTO_IGMP
            || i4ServiceProtocolEnum == L2_PROTO_MVRP
            || i4ServiceProtocolEnum == L2_PROTO_MMRP
            || i4ServiceProtocolEnum == L2_PROTO_ECFM)
        {
            nmhGetFsMIServiceVlanTunnelProtocolStatus (u4ContextId,
                                                       i4CurrentVlanId,
                                                       i4ServiceProtocolEnum,
                                                       &i4ServiceVlanTunnelProtocolStatus);
            nmhGetFsMIServiceVlanRsvdMacaddress (u4ContextId, i4CurrentVlanId,
                                                 i4ServiceProtocolEnum,
                                                 &ServiceVlanRsvdMacaddress);
            nmhGetFsMIServiceVlanTunnelMacaddress (u4ContextId, i4CurrentVlanId,
                                                   i4ServiceProtocolEnum,
                                                   &ServiceVlanTunnelMacaddress);
            CliPrintf (CliHandle, " %-7u ", i4CurrentVlanId);
            CliPrintf (CliHandle, " %-8s ",
                       u1ProtocolList[i4ServiceProtocolEnum - 1]);
            PrintMacAddress (ServiceVlanRsvdMacaddress, au1String);
            CliPrintf (CliHandle, " %-17s ", au1String);
            PrintMacAddress (ServiceVlanTunnelMacaddress, au1String);
            CliPrintf (CliHandle, " %-17s ", au1String);
            CliPrintf (CliHandle, " %-17s\r\n",
                       u1TunnelStatus[i4ServiceVlanTunnelProtocolStatus - 1]);
        }
        u4ContextId = u4NextContextId;
        i4CurrentVlanId = i4NextVlanId;
        i4ServiceProtocolEnum = i4NextServiceProtocolEnum;
    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbCliShowL2ProtocolDiscard                     */
/*                                                                           */
/*     DESCRIPTION      : This function shows the discarded packet count     */
/*                                                                           */
/*                                                                           */
/*     INPUT            : tCliHandle  CliHandle - Context in which the CLI   */
/*                                             command is processed          */
/*                        u4ContextId - Context Identifier                   */
/*                        VlanId      - Vlan ID                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully         */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                  */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbCliShowL2ProtocolDiscard (tCliHandle CliHandle, UINT4 u4ContextId,
                                tVlanId VlanId)
{

    UINT4               u4NextContextId = 0;
    UINT4               u4ServiceVlanDiscardPktsRx = 0;
    UINT4               u4ServiceVlanDiscardPktsTx = 0;
    INT4                i4ServiceVlanTunnelProtocolStatus;
    INT4                i4ServiceProtocolEnum = L2_PROTO_GVRP;
    INT4                i4NextServiceProtocolEnum = 0;
    INT4                i4NextVlanId = 0;
    UINT1               u1ProtocolList[12][6] =
        { "DOT1X", "LACP", "STP", "GVRP", "GMPR", "IGMP",
        "MVRP", "MMRP", "EOAM", "ELMI", "LLDP", "ECFM"
    };

    CliPrintf (CliHandle, "\r\nService Vlan Discarding:\r\n\r\n");

    CliPrintf (CliHandle, " VLAN ID ");

    CliPrintf (CliHandle, " Protocol ");

    CliPrintf (CliHandle, " Discard counter:Rx");

    CliPrintf (CliHandle, " Discard counter:Tx\r\n");

    CliPrintf (CliHandle, " ------- ");

    CliPrintf (CliHandle, " -------- ");

    CliPrintf (CliHandle, " ------------------ ");

    CliPrintf (CliHandle, " ------------------\r\n");

    if (nmhValidateIndexInstanceFsMIServiceVlanTunnelProtocolTable
        (u4ContextId, VlanId, i4ServiceProtocolEnum) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    while (nmhGetNextIndexFsMIServiceVlanTunnelProtocolTable
           (u4ContextId, (INT4 *) &u4NextContextId, VlanId,
            (INT4 *) &i4NextVlanId, i4ServiceProtocolEnum,
            (INT4 *) &i4NextServiceProtocolEnum) == SNMP_SUCCESS)
    {
        if (VlanId != i4NextVlanId)
        {
            break;
        }
        nmhGetFsMIServiceVlanTunnelProtocolStatus (u4ContextId, VlanId,
                                                   i4ServiceProtocolEnum,
                                                   &i4ServiceVlanTunnelProtocolStatus);
        if ((i4ServiceProtocolEnum == L2_PROTO_GVRP
             || i4ServiceProtocolEnum == L2_PROTO_GMRP
             || i4ServiceProtocolEnum == L2_PROTO_IGMP
             || i4ServiceProtocolEnum == L2_PROTO_MVRP
             || i4ServiceProtocolEnum == L2_PROTO_MMRP
             || i4ServiceProtocolEnum == L2_PROTO_ECFM)
            && i4ServiceVlanTunnelProtocolStatus ==
            VLAN_TUNNEL_PROTOCOL_DISCARD)
        {
            nmhGetFsMIServiceVlanTunnelProtocolStatus (u4ContextId, VlanId,
                                                       i4ServiceProtocolEnum,
                                                       &i4ServiceVlanTunnelProtocolStatus);

            nmhGetFsMIServiceVlanDiscardPktsRx (u4ContextId, VlanId,
                                                i4ServiceProtocolEnum,
                                                &u4ServiceVlanDiscardPktsRx);
            nmhGetFsMIServiceVlanDiscardPktsTx (u4ContextId, VlanId,
                                                i4ServiceProtocolEnum,
                                                &u4ServiceVlanDiscardPktsTx);

            CliPrintf (CliHandle, " %-7u ", VlanId);
            CliPrintf (CliHandle, " %-8s ",
                       u1ProtocolList[i4ServiceProtocolEnum - 1]);
            CliPrintf (CliHandle, " %-17u ", u4ServiceVlanDiscardPktsRx);
            CliPrintf (CliHandle, " %-17u\r\n", u4ServiceVlanDiscardPktsTx);
        }
        u4ContextId = u4NextContextId;
        VlanId = i4NextVlanId;
        i4ServiceProtocolEnum = i4NextServiceProtocolEnum;
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbCliShowL2PrtclServiceVlanTunnelSummary       */
/*                                                                           */
/*     DESCRIPTION      : This function show entries in the Tunnel protocol  */
/*                        table.                                             */
/*                                                                           */
/*     INPUT            : tCliHandle  CliHandle - Context in which the CLI   */
/*                                             command is processed          */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully         */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                  */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbCliShowL2PrtclServiceVlanTunnelSummary (tCliHandle CliHandle,
                                              UINT4 u4ContextId,
                                              UINT1 u1Summary)
{
    UINT4               u4NextContextId = 0;
    UINT4               u4ServiceVlanTunnelPktsRecvd = 0;
    UINT4               u4ServiceVlanTunnelPktsSent = 0;
    UINT4               u4CurrentContextId = 0;
    INT4                i4ServiceVlanTunnelProtocolStatus = 0;
    INT4                i4ServiceProtocolEnum = L2_PROTO_GVRP;
    INT4                i4NextServiceProtocolEnum = 0;
    INT4                i4CurrentVlanId = 0;
    INT4                i4NextVlanId = 0;
    UINT1               u1TunnelStatus[3][8] = { "Peer", "Tunnel", "Discard" };
    UINT1               u1ProtocolList[12][6] =
        { "DOT1X", "LACP", "STP", "GVRP", "GMPR", "IGMP",
        "MVRP", "MMRP", "EOAM", "ELMI", "LLDP", "ECFM"
    };

    CliPrintf (CliHandle, "\r\nService Vlan Tunneling Summary:\r\n\r\n");

    CliPrintf (CliHandle, " VLAN ID ");

    CliPrintf (CliHandle, " Protocol ");

    CliPrintf (CliHandle, " Received Pkt      ");

    CliPrintf (CliHandle, " Sent Pkt       ");

    CliPrintf (CliHandle, " Status\r\n");

    CliPrintf (CliHandle, " ------- ");

    CliPrintf (CliHandle, " -------- ");

    CliPrintf (CliHandle, " ------------      ");

    CliPrintf (CliHandle, " ----------     ");

    CliPrintf (CliHandle, " ----------\r\n");

    if (nmhGetFirstIndexFsMIServiceVlanTunnelProtocolTable
        ((INT4 *) &u4CurrentContextId, (INT4 *) &i4CurrentVlanId,
         (INT4 *) &i4ServiceProtocolEnum) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    while (nmhGetNextIndexFsMIServiceVlanTunnelProtocolTable
           (u4CurrentContextId, (INT4 *) &u4NextContextId, i4CurrentVlanId,
            (INT4 *) &i4NextVlanId, i4ServiceProtocolEnum,
            (INT4 *) &i4NextServiceProtocolEnum) == SNMP_SUCCESS)
    {
        if (u1Summary == VLAN_SNMP_TRUE && u4CurrentContextId != u4ContextId)
        {
            u4CurrentContextId = u4NextContextId;
            i4CurrentVlanId = i4NextVlanId;
            i4ServiceProtocolEnum = i4NextServiceProtocolEnum;
            continue;
        }
        if (i4ServiceProtocolEnum == L2_PROTO_GVRP
            || i4ServiceProtocolEnum == L2_PROTO_GMRP
            || i4ServiceProtocolEnum == L2_PROTO_IGMP
            || i4ServiceProtocolEnum == L2_PROTO_MVRP
            || i4ServiceProtocolEnum == L2_PROTO_MMRP
            || i4ServiceProtocolEnum == L2_PROTO_ECFM)
        {
            nmhGetFsMIServiceVlanTunnelProtocolStatus (u4CurrentContextId,
                                                       i4CurrentVlanId,
                                                       i4ServiceProtocolEnum,
                                                       &i4ServiceVlanTunnelProtocolStatus);
            nmhGetFsMIServiceVlanTunnelPktsRecvd (u4CurrentContextId,
                                                  i4CurrentVlanId,
                                                  i4ServiceProtocolEnum,
                                                  &u4ServiceVlanTunnelPktsRecvd);
            nmhGetFsMIServiceVlanTunnelPktsSent (u4CurrentContextId,
                                                 i4CurrentVlanId,
                                                 i4ServiceProtocolEnum,
                                                 &u4ServiceVlanTunnelPktsSent);

            CliPrintf (CliHandle, " %-7u ", i4CurrentVlanId);
            CliPrintf (CliHandle, " %-8s ",
                       u1ProtocolList[i4ServiceProtocolEnum - 1]);
            CliPrintf (CliHandle, " %-17u ", u4ServiceVlanTunnelPktsRecvd);
            CliPrintf (CliHandle, " %-17u ", u4ServiceVlanTunnelPktsSent);
            CliPrintf (CliHandle, " %-14s\r\n",
                       u1TunnelStatus[i4ServiceVlanTunnelProtocolStatus - 1]);
        }

        u4CurrentContextId = u4NextContextId;
        i4CurrentVlanId = i4NextVlanId;
        i4ServiceProtocolEnum = i4NextServiceProtocolEnum;
    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbCliShowL2PrtclTunnelSummary                  */
/*                                                                           */
/*     DESCRIPTION      : This function show entries in the Tunnel protocol  */
/*                        table.                                             */
/*                                                                           */
/*     INPUT            : tCliHandle  CliHandle - Context in which the CLI   */
/*                                             command is processed          */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully         */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                  */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbCliShowL2PrtclTunnelSummary (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4TunnelBpduPri = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    UINT4               u4BpduTunnelStatus;
    UINT4               u4GvrpTunnelStatus = 0;
    UINT4               u4MvrpTunnelStatus = 0;
    UINT4               u4IgmpTunnelStatus = 0;
    UINT4               u4Dot1xTunnelStatus = 0;
    UINT4               u4LacpTunnelStatus = 0;
    UINT4               u4GmrpTunnelStatus = 0;
    UINT4               u4MmrpTunnelStatus = 0;
    UINT4               u4EoamTunnelStatus = 0;
    UINT4               u4EcfmTunnelStatus = 0;
    UINT4               u4ElmiTunnelStatus = 0;
    UINT4               u4LldpTunnelStatus = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1Name[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1IsShowAll = TRUE;
    UINT1               u1OperStatus;
    INT4                i4Result;
    tVlanPortEntry     *pVlanPortEntry;
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;

    nmhGetFsMIVlanTunnelBpduPri (u4ContextId, &i4TunnelBpduPri);

    CliPrintf (CliHandle, "\r\nCOS for Encapsulated Packet :");

    CliPrintf (CliHandle, " %d\r\n\r\n", i4TunnelBpduPri);

    CliPrintf (CliHandle, " Port ");

    CliPrintf (CliHandle, " Protocol ");

    CliPrintf (CliHandle, " Status\r\n");

    CliPrintf (CliHandle, " ---- ");

    CliPrintf (CliHandle, " -------- ");

    CliPrintf (CliHandle, " ------\r\n");

    MEMSET (au1Name, 0, CFA_MAX_PORT_NAME_LENGTH);

    i4Result = VlanGetFirstPortInContext (u4ContextId, (UINT4 *) &i4NextPort);

    if (i4Result == VLAN_FAILURE)
    {
        return CLI_FAILURE;
    }

    do
    {
        if (VlanGetContextInfoFromIfIndex ((UINT4) i4NextPort, &u4TempContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);
        u1OperStatus = pVlanPortEntry->u1OperStatus;

        u4BpduTunnelStatus = (UINT4) VLAN_STP_TUNNEL_STATUS (pVlanPortEntry);
        u4IgmpTunnelStatus = (UINT4) VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry);
        u4Dot1xTunnelStatus = (UINT4) VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry);
        u4LacpTunnelStatus = (UINT4) VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry);
        u4MvrpTunnelStatus = (UINT4) VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry);
        u4MmrpTunnelStatus = (UINT4) VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry);
        u4GvrpTunnelStatus = (UINT4) VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry);
        u4GmrpTunnelStatus = (UINT4) VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry);
        u4EoamTunnelStatus = (UINT4) VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry);
        u4LldpTunnelStatus = (UINT4) VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry);
        u4EcfmTunnelStatus = (UINT4) VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry);
        u4ElmiTunnelStatus = (UINT4) VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry);

        /*In case of 1AD Bridges, Protocol tunnel status are 
         * skipping to display on network ports*/
        if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
            (VLAN_PB_PROTO_TUNNEL_PORT (u2LocalPortId) != VLAN_TRUE))
        {
            i4CurrentPort = i4NextPort;
            if (VlanGetNextPortInContext (u4ContextId, (UINT4) i4CurrentPort,
                                          (UINT4 *) &i4NextPort) ==
                VLAN_FAILURE)
            {
                u1IsShowAll = FALSE;
            }
            /* As summary displays the information for all the interfaces,
             * skip this interface and proceed with next interface index */
            continue;
        }
        VlanReleaseContext ();

        if (u4Dot1xTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  Dot1x   ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = CliPrintf (CliHandle, " down \r\n");
            }
        }

        if (u4LacpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  LACP    ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = CliPrintf (CliHandle, " down \r\n");
            }
        }

        if (u4BpduTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  STP     ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = CliPrintf (CliHandle, " down \r\n");
            }
        }

        if (u4GvrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  GVRP    ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = CliPrintf (CliHandle, " down \r\n");
            }
        }

        if (u4GmrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  GMRP    ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = CliPrintf (CliHandle, " down \r\n");
            }
        }

        if (u4MvrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  MVRP    ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = CliPrintf (CliHandle, " down \r\n");
            }
        }

        if (u4MmrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  MMRP    ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " down \r\n");
            }
        }

        if (u4IgmpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  IGMP    ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " down \r\n");
            }
        }

        if (u4EoamTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  EOAM    ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " down \r\n");
            }
        }

        if (u4EcfmTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  ECFM    ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " down \r\n");
            }
        }

        if (u4ElmiTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  ELMI    ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " down \r\n");
            }
        }

        if (u4LldpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "  LLDP    ");

            if (u1OperStatus == VLAN_OPER_UP)
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " up   \r\n");
            }
            else
            {
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, " down \r\n");
            }
        }

        i4CurrentPort = i4NextPort;
        if (VlanGetNextPortInContext (u4ContextId, (UINT4) i4CurrentPort,
                                      (UINT4 *) &i4NextPort) == VLAN_FAILURE)
        {
            u1IsShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1IsShowAll = FALSE;
        }
    }
    while (u1IsShowAll == TRUE);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbCliShowL2ProtocolTunnel                      */
/*                                                                           */
/*     DESCRIPTION      : This function show entries in the Tunnel protocol  */
/*                        table.                                             */
/*                                                                           */
/*     INPUT            : CliHandle - Context in which the CLI               */
/*                                      command is processed                 */
/*                                                                           */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*                        u4Port       - Port which is to be displayed       */
/*                                                                           */
/*                        u1Summary    - Summary Option Present / Not        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully         */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                  */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbCliShowL2ProtocolTunnel (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4Port, UINT1 u1Summary)
{
    UINT4               u4RetVal = 0;
    UINT4               u4PDURecvd = 0;
    INT4                i4RetVal = 0;
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    INT4                i4PagingStatus = CLI_SUCCESS;
    UINT1               u1OperStatus;
    INT4                i4Result;
    UINT4               u4BpduTunnelStatus;
    UINT4               u4GvrpTunnelStatus = 0;
    UINT4               u4GmrpTunnelStatus = 0;
    UINT4               u4MvrpTunnelStatus = 0;
    UINT4               u4MmrpTunnelStatus = 0;
    UINT4               u4IgmpTunnelStatus = 0;
    UINT4               u4Dot1xTunnelStatus = 0;
    UINT4               u4LacpTunnelStatus = 0;
    UINT4               u4EcfmTunnelStatus = 0;
    UINT4               u4EoamTunnelStatus = 0;
    UINT4               u4LldpTunnelStatus = 0;
    UINT4               u4ElmiTunnelStatus = 0;
    tVlanPortEntry     *pVlanPortEntry;
    UINT1               u1IsShowAll = TRUE;
    UINT1               au1Name[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4TempContextId;
    UINT2               u2LocalPortId;

    MEMSET (au1Name, 0, CFA_MAX_PORT_NAME_LENGTH);

    /* If summary is present then this function is called */
    if (u1Summary == VLAN_TRUE)
    {
        i4Result = VlanPbCliShowL2PrtclTunnelSummary (CliHandle, u4ContextId);
        return i4Result;
    }

    nmhGetFsMIVlanTunnelBpduPri (u4ContextId, &i4RetVal);

    CliPrintf (CliHandle, "\r\n COS for Encapsulated Packet :");

    CliPrintf (CliHandle, " %d\r\n\r\n", i4RetVal);

    CliPrintf (CliHandle, " Port ");

    CliPrintf (CliHandle, " Protocol ");

    CliPrintf (CliHandle, " Encapsulation Counter ");

    CliPrintf (CliHandle, " Decapsulation Counter \r\n");

    CliPrintf (CliHandle, " ---- ");

    CliPrintf (CliHandle, " -------- ");

    CliPrintf (CliHandle, " --------------------- ");

    CliPrintf (CliHandle, " --------------------- \r\n");

    if (u4Port == VLAN_DISP_ALL_INTERFACES)
    {
        /* Display all entries in the Tunnel Protocol Table */
        i4Result = VlanGetFirstPortInContext (u4ContextId,
                                              (UINT4 *) &i4NextPort);
        i4Result = (i4Result == VLAN_SUCCESS) ? SNMP_SUCCESS : SNMP_FAILURE;
    }
    else
    {
        /* Display Tunnel Protocol info for the given Interface */
        i4Result =
            (INT4) nmhValidateIndexInstanceFsMIVlanTunnelProtocolTable (u4Port);

        i4NextPort = (INT4) u4Port;
    }

    if (i4Result == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    do
    {
        if (VlanGetContextInfoFromIfIndex ((UINT4) i4NextPort, &u4TempContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

        u1OperStatus = pVlanPortEntry->u1OperStatus;

        u4BpduTunnelStatus = VLAN_STP_TUNNEL_STATUS (pVlanPortEntry);
        u4IgmpTunnelStatus = VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry);
        u4Dot1xTunnelStatus = VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry);
        u4LacpTunnelStatus = VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry);
        u4MvrpTunnelStatus = (UINT4) VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry);
        u4MmrpTunnelStatus = (UINT4) VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry);
        u4GvrpTunnelStatus = (UINT4) VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry);
        u4GmrpTunnelStatus = (UINT4) VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry);
        u4EcfmTunnelStatus = (UINT4) VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry);
        u4EoamTunnelStatus = (UINT4) VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry);
        u4LldpTunnelStatus = (UINT4) VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry);
        u4ElmiTunnelStatus = (UINT4) VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry);

        /*In case of 1AD Bridges, Protocol tunnel status are not
         * allowed to display on network ports*/
        if ((VLAN_PB_802_1AD_BRIDGE () == VLAN_TRUE) &&
            (VLAN_PB_PROTO_TUNNEL_PORT (u2LocalPortId) != VLAN_TRUE))
        {
            u1IsShowAll = FALSE;
            i4CurrentPort = i4NextPort;

            /* If the command is issued to display information specific  to an
             * interface then no need to display the interface information, if
             * the command is issued to display the tunnel information for all 
             * the interfaces  skip this interface and proceed with next interface 
             * by reseting the flag u1IsShowAll to true */

            if ((VlanGetNextPortInContext (u4ContextId, (UINT4) i4CurrentPort,
                                           (UINT4 *) &i4NextPort) !=
                 VLAN_FAILURE) && (u4Port == VLAN_DISP_ALL_INTERFACES))
            {
                u1IsShowAll = TRUE;
            }
            continue;
        }

        VlanReleaseContext ();
        VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

        if ((u4Dot1xTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolDot1xPktsRecvd (i4NextPort,
                                                        &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolDot1xPktsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "   Dot1x     ");

            CliPrintf (CliHandle, "%4d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }

        if ((u4LacpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolLacpPktsRecvd (i4NextPort, &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolLacpPktsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);

            CliPrintf (CliHandle, "   LACP     ");

            CliPrintf (CliHandle, "%5d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }

        if ((u4BpduTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolStpPDUsRecvd (i4NextPort, &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolStpPDUsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);
            CliPrintf (CliHandle, "   STP    ");

            CliPrintf (CliHandle, "%7d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }

        if ((u4GvrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolGvrpPDUsRecvd (i4NextPort, &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolGvrpPDUsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);
            CliPrintf (CliHandle, "   GVRP    ");

            CliPrintf (CliHandle, "%6d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }

        if ((u4GmrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolGmrpPktsRecvd (i4NextPort, &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolGmrpPktsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);
            CliPrintf (CliHandle, "   GMRP    ");

            CliPrintf (CliHandle, "%6d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }
        if ((u4MvrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolMvrpPktsRecvd (i4NextPort, &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolMvrpPktsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);
            CliPrintf (CliHandle, "   MVRP    ");

            CliPrintf (CliHandle, "%6d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }

        if ((u4MmrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolMmrpPktsRecvd (i4NextPort, &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolMmrpPktsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);
            CliPrintf (CliHandle, "   MMRP    ");

            CliPrintf (CliHandle, "%6d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }

        if ((u4IgmpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolIgmpPktsRecvd (i4NextPort, &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolIgmpPktsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);
            CliPrintf (CliHandle, "   IGMP    ");

            CliPrintf (CliHandle, "%6d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }

        if ((u4EcfmTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolEcfmPktsRecvd (i4NextPort, &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolEcfmPktsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);
            CliPrintf (CliHandle, "   ECFM    ");

            CliPrintf (CliHandle, "%6d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }

        if ((u4EoamTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolEoamPktsRecvd (i4NextPort, &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolEoamPktsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);
            CliPrintf (CliHandle, "   EOAM    ");

            CliPrintf (CliHandle, "%6d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }

        if ((u4LldpTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolLldpPktsRecvd (i4NextPort, &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolLldpPktsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);
            CliPrintf (CliHandle, "   LLDP    ");

            CliPrintf (CliHandle, "%6d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }

        if ((u4ElmiTunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            && (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanTunnelProtocolElmiPktsRecvd (i4NextPort, &u4PDURecvd);
            nmhGetFsMIVlanTunnelProtocolElmiPktsSent (i4NextPort, &u4RetVal);

            CliPrintf (CliHandle, " %-6s ", au1Name);
            CliPrintf (CliHandle, "   ELMI    ");

            CliPrintf (CliHandle, "%6d         ", u4PDURecvd);

            i4PagingStatus = CliPrintf (CliHandle, "\t%10d\r\n", u4RetVal);
        }

        if (u4Port != VLAN_DISP_ALL_INTERFACES)
        {
            /* Displaying only for a particular interface */
            break;
        }
        i4CurrentPort = i4NextPort;

        if (VlanGetNextPortInContext
            (u4ContextId, (UINT4) i4CurrentPort, (UINT4 *) &i4NextPort)
            == VLAN_FAILURE)
        {
            u1IsShowAll = FALSE;
        }

        if (i4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1IsShowAll = FALSE;
        }
    }
    while (u1IsShowAll == TRUE);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbCliShowDiscardStats                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays the L2 protocols discard    */
/*                        statistics.                                        */
/*                                                                           */
/*     INPUT            : CliHandle - Context in which the CLI               */
/*                                      command is processed                 */
/*                                                                           */
/*                        u4Port    - Port for which the statistics are to   */
/*                                    be displayed.                          */
/*                                                                           */
/*                        u4ContextId - Context Id to be displayed           */
/*                                                                           */
/*                        VlanId      - Vlan ID                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS - when command is successfully         */
/*                                       executed                            */
/*                        CLI_FAILURE - when command is not                  */
/*                                       successfully executed               */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbCliShowDiscardStats (tCliHandle CliHandle, UINT4 u4Port,
                           UINT4 u4ContextId, tVlanId VlanId)
{
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    INT4                i4PagingStatus = CLI_SUCCESS;
    UINT4               u4DiscPktsRx = 0;
    UINT4               u4DiscPktsTx = 0;
    UINT4               u4StpTunnelStatus;
    UINT4               u4GvrpTunnelStatus = 0;
    UINT4               u4MvrpTunnelStatus = 0;
    UINT4               u4MmrpTunnelStatus = 0;
    UINT4               u4GmrpTunnelStatus = 0;
    UINT4               u4IgmpTunnelStatus;
    UINT4               u4Dot1xTunnelStatus;
    UINT4               u4LacpTunnelStatus;
    UINT4               u4ElmiTunnelStatus;
    UINT4               u4LldpTunnelStatus;
    UINT4               u4EcfmTunnelStatus;
    UINT4               u4EoamTunnelStatus = 0;
    UINT4               u4TempContextId;
    tVlanPortEntry     *pVlanPortEntry;
    UINT2               u2LocalPortId;
    UINT1               u1OperStatus;
    UINT1               u1IsShowAll = TRUE;
    UINT1               au1Name[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1Name, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (VlanId)
    {
        return (VlanPbCliShowL2ProtocolDiscard
                (CliHandle, u4ContextId, VlanId));
    }
    if (u4Port == VLAN_DISP_ALL_INTERFACES)
    {
        /* Display all entries in the Discard stats Table */
        if (VlanGetFirstPortInContext (u4ContextId, (UINT4 *) &i4NextPort)
            != VLAN_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Display discard stats for the given Interface */
        if (nmhValidateIndexInstanceFsMIVlanDiscardStatsTable ((INT4) u4Port)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        i4NextPort = (INT4) u4Port;
    }

    do
    {
        if (VlanGetContextInfoFromIfIndex ((UINT4) i4NextPort, &u4TempContextId,
                                           &u2LocalPortId) != VLAN_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        pVlanPortEntry = VLAN_GET_PORT_ENTRY (u2LocalPortId);

        u1OperStatus = pVlanPortEntry->u1OperStatus;

        u4StpTunnelStatus = VLAN_STP_TUNNEL_STATUS (pVlanPortEntry);
        u4IgmpTunnelStatus = VLAN_IGMP_TUNNEL_STATUS (pVlanPortEntry);
        u4Dot1xTunnelStatus = VLAN_DOT1X_TUNNEL_STATUS (pVlanPortEntry);
        u4LacpTunnelStatus = VLAN_LACP_TUNNEL_STATUS (pVlanPortEntry);
        u4MvrpTunnelStatus = VLAN_MVRP_TUNNEL_STATUS (pVlanPortEntry);
        u4MmrpTunnelStatus = VLAN_MMRP_TUNNEL_STATUS (pVlanPortEntry);
        u4GvrpTunnelStatus = VLAN_GVRP_TUNNEL_STATUS (pVlanPortEntry);
        u4GmrpTunnelStatus = VLAN_GMRP_TUNNEL_STATUS (pVlanPortEntry);
        u4ElmiTunnelStatus = VLAN_ELMI_TUNNEL_STATUS (pVlanPortEntry);
        u4LldpTunnelStatus = VLAN_LLDP_TUNNEL_STATUS (pVlanPortEntry);
        u4EcfmTunnelStatus = VLAN_ECFM_TUNNEL_STATUS (pVlanPortEntry);
        u4EoamTunnelStatus = VLAN_EOAM_TUNNEL_STATUS (pVlanPortEntry);

        VlanReleaseContext ();

        VlanCfaCliGetIfName ((UINT4) i4NextPort, (INT1 *) au1Name);

        CliPrintf (CliHandle, " Port %s \r\n", au1Name);
        CliPrintf (CliHandle, "------------\r\n");

        CliPrintf (CliHandle, " Protocol ");

        CliPrintf (CliHandle, " Discard-Counter:Rx  ");

        CliPrintf (CliHandle, " Discard-Counter:Tx \r\n");

        CliPrintf (CliHandle, " -------- ");

        CliPrintf (CliHandle, " ------------------  ");
        CliPrintf (CliHandle, " ------------------- \r\n");

        if ((u4Dot1xTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardDot1xPktsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardDot1xPktsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   Dot1x    ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);
            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if ((u4LacpTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardLacpPktsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardLacpPktsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   LACP     ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);
            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if ((u4StpTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardStpPDUsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardStpPDUsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   STP      ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);
            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if ((u4GvrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardGvrpPktsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardGvrpPktsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   GVRP     ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);
            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if ((u4GmrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardGmrpPktsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardGmrpPktsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   GMRP     ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);
            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if ((u4MvrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardMvrpPktsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardMvrpPktsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   MVRP     ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);
            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if ((u4MmrpTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardMmrpPktsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardMmrpPktsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   MMRP     ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);
            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if ((u4IgmpTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardIgmpPktsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardIgmpPktsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   IGMP     ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);

            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if ((u4ElmiTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardElmiPktsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardElmiPktsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   ELMI     ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);

            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if ((u4LldpTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardLldpPktsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardLldpPktsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   LLDP     ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);

            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if ((u4EcfmTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardEcfmPktsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardEcfmPktsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   ECFM     ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);

            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if ((u4EoamTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD) &&
            (u1OperStatus == VLAN_OPER_UP))
        {
            nmhGetFsMIVlanDiscardEoamPktsRx (i4NextPort, &u4DiscPktsRx);
            nmhGetFsMIVlanDiscardEoamPktsTx (i4NextPort, &u4DiscPktsTx);

            CliPrintf (CliHandle, "   EOAM     ");

            CliPrintf (CliHandle, "%8d         ", u4DiscPktsRx);

            i4PagingStatus =
                CliPrintf (CliHandle, "%13d         \r\n", u4DiscPktsTx);
        }

        if (u4Port != VLAN_DISP_ALL_INTERFACES)
        {
            /* Displaying only for a particular interface */
            break;
        }

        i4CurrentPort = i4NextPort;

        if (VlanGetNextPortInContext (u4ContextId, (UINT4) i4CurrentPort,
                                      (UINT4 *) &i4NextPort) == VLAN_FAILURE)
        {
            u1IsShowAll = FALSE;
        }

        if (i4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1IsShowAll = FALSE;
        }
    }
    while (u1IsShowAll == TRUE);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanShowProtocolTunnelStatus                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the L2 protocol tunnel      */
/*                        status.                                            */
/*                                                                           */
/*     INPUT            : CliHandle - Context in which the CLI               */
/*                                      command is processed                 */
/*                                                                           */
/*                        i4Port    - Port for which the tunnel status to    */
/*                                    be displayed.                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
VlanShowProtocolTunnelStatus (tCliHandle CliHandle, INT4 i4PortId)
{
    INT4                i4TunnelStatus;
    UINT2               u2LocalPort;
    UINT4               u4ContextId;
    INT4                i4BridgeMode;

    i4TunnelStatus = VLAN_TUNNEL_PROTOCOL_INVALID;
    u4ContextId = VLAN_DEF_CONTEXT_ID;
    i4BridgeMode = VLAN_CUSTOMER_BRIDGE_MODE;

    if (VlanGetContextInfoFromIfIndex ((UINT4) i4PortId, &u4ContextId,
                                       &u2LocalPort) != VLAN_SUCCESS)
    {
        return;
    }

    nmhGetFsMIVlanBridgeMode ((INT4) u4ContextId, &i4BridgeMode);

    if (i4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE ||
        i4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE ||
        i4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE)
    {
        nmhGetFsMIVlanTunnelStatus (i4PortId, &i4TunnelStatus);
        CliPrintf (CliHandle, " Tunnel Status                       :");
        switch (i4TunnelStatus)
        {
            case VLAN_ENABLED:
                CliPrintf (CliHandle, " Enabled\r\n");
                break;
            case VLAN_DISABLED:
                CliPrintf (CliHandle, " Disabled\r\n");
                break;
            default:
                break;
        }
    }

    if ((i4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE) ||
        (i4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE ||
         i4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE ||
         i4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        nmhGetFsMIVlanTunnelProtocolDot1x (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_DOT1X);

        nmhGetFsMIVlanTunnelProtocolLacp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_LACP);

        nmhGetFsMIVlanTunnelProtocolStp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_STP);

        nmhGetFsMIVlanTunnelProtocolMvrp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_MVRP);

        nmhGetFsMIVlanTunnelProtocolMmrp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_MMRP);

        nmhGetFsMIVlanTunnelProtocolGvrp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_GVRP);

        nmhGetFsMIVlanTunnelProtocolGmrp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_GMRP);

        nmhGetFsMIVlanTunnelProtocolIgmp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_IGMP);

        nmhGetFsMIVlanTunnelProtocolElmi (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_ELMI);

        nmhGetFsMIVlanTunnelProtocolLldp (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_LLDP);

        nmhGetFsMIVlanTunnelProtocolEcfm (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_ECFM);

        nmhGetFsMIVlanTunnelProtocolEoam (i4PortId, &i4TunnelStatus);
        VlanPbPrintProtocolTunnelStatus (CliHandle,
                                         i4TunnelStatus, CLI_PB_L2_PROTO_EOAM);

    }

    return;
}

#ifdef PBB_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VlanPbCliShowPBBIsid                               */
/*                                                                           */
/*     DESCRIPTION      : This function will display wildcard entries        */
/*                        for given context and Mac addresss                 */
/*                                                                           */
/*     INPUT            : u4ContextId   - Context id                         */
/*                        CliHandle - contains error messages                */
/*                        u4Isid    - service instance                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbCliShowPBBIsid (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Isid)
{

    UINT4               u4IfIndex;
    INT4                i4IngressEtherType;
    INT4                i4EgressEtherType;
    INT4                i4UnicastMacLearning;
    INT4                i4ReqDropEncoding;
    INT4                i4UseDei;
    INT4                i4PcpSelectionRow;
    UINT4               u4UnicastMacLimit;
    INT4                i4SVlanTranslationStatus;
    INT4                i4CurrentPort = 0;
    INT4                i4NextPortId;
    INT4                i4LocalSVlanId = 0;
    INT4                i4LocalNextSVlanId;
    INT4                i4RelaySVlanId;
    UINT4               u4TempContextId;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT2               u2LocalPortId;
    UINT1               u1isShowAll = TRUE;

    CliPrintf (CliHandle,
               "\r\nProvider Backbone Bridge configuration table\r\n");
    CliPrintf (CliHandle, "-------------------------------\r\n");

    if (VlanPbbCliGetVipIndexOfIsid (u4ContextId, u4Isid, &u4IfIndex) !=
        VLAN_SUCCESS)
    {
        /* If Vip is not present with the Isid then return Success */
        /* Since this functions shows the PBB- I component specific 
         * attributes 
         */
        return (CLI_SUCCESS);
    }

    CliPrintf (CliHandle, "\r\nPCP Encoding Table: ");
    CliPrintf (CliHandle, "\r\n-------------------\r\n ");
    VlanPbShowPcpEncodingTable (CliHandle, (INT4) u4IfIndex);
    CliPrintf (CliHandle, "\r\nPCP Decoding Table: ");
    CliPrintf (CliHandle, "\r\n-------------------\r\n ");
    VlanPbShowPcpDecodingTable (CliHandle, (INT4) u4IfIndex);
    nmhGetFsMIPbPortSVlanIngressEtherType ((INT4) u4IfIndex,
                                           &i4IngressEtherType);
    nmhGetFsMIPbPortSVlanEgressEtherType ((INT4) u4IfIndex, &i4EgressEtherType);
    nmhGetDot1adMIPortReqDropEncoding ((INT4) u4IfIndex, &i4ReqDropEncoding);
    nmhGetDot1adMIPortUseDei ((INT4) u4IfIndex, &i4UseDei);
    nmhGetDot1adMIPortPcpSelectionRow ((INT4) u4IfIndex, &i4PcpSelectionRow);
    nmhGetFsMIPbPortUnicastMacLearning ((INT4) u4IfIndex,
                                        &i4UnicastMacLearning);
    nmhGetFsMIPbPortUnicastMacLimit ((INT4) u4IfIndex, &u4UnicastMacLimit);

    CliPrintf (CliHandle,
               "\r\nIngress EtherType                   : 0x%x\r\n",
               i4IngressEtherType);
    CliPrintf (CliHandle,
               "Egress EtherType                    : 0x%x\r\n",
               i4EgressEtherType);
    CliPrintf (CliHandle, "Require Drop Encoding               :");
    if (i4ReqDropEncoding == VLAN_TRUE)
    {
        CliPrintf (CliHandle, " True\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " False \r\n");
    }

    CliPrintf (CliHandle, "Use_Dei                             :");
    if (i4UseDei == VLAN_SNMP_TRUE)
    {
        CliPrintf (CliHandle, " True\r\n");
    }
    else if (i4UseDei == VLAN_SNMP_FALSE)
    {
        CliPrintf (CliHandle, " False\r\n");
    }

    CliPrintf (CliHandle, "\r\nPCP Selection Row                   : ");

    switch (i4PcpSelectionRow)
    {
        case VLAN_8P0D_SEL_ROW:
            CliPrintf (CliHandle, "8P0D \r\n");
            break;
        case VLAN_7P1D_SEL_ROW:
            CliPrintf (CliHandle, "7P1D \r\n");
            break;
        case VLAN_6P2D_SEL_ROW:
            CliPrintf (CliHandle, "6P2D \r\n");
            break;
        case VLAN_5P3D_SEL_ROW:
            CliPrintf (CliHandle, "5P3D \r\n");
            break;
        default:
            CliPrintf (CliHandle, " \r\n");
            break;
    }

    CliPrintf (CliHandle,
               "\r\nCustomer Unicast Mac Learning Status       " "  :");

    if (i4UnicastMacLearning == CLI_PB_ENABLED)
    {
        CliPrintf (CliHandle, " Enable\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Disable\r\n");
    }

    CliPrintf (CliHandle,
               "Customer Unicast Mac Learning Limit          : %d\r\n",
               u4UnicastMacLimit);
    CliPrintf (CliHandle, "\nService Vlan Translation Status              :");
    nmhGetFsMIPbPortSVlanTranslationStatus ((INT4) u4IfIndex,
                                            &i4SVlanTranslationStatus);

    if (i4SVlanTranslationStatus == VLAN_ENABLED)
    {
        CliPrintf (CliHandle, " Enable\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Disable\r\n");
    }

    if (nmhGetFirstIndexDot1adMIVidTranslationTable
        (&i4NextPortId, &i4LocalNextSVlanId) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nService Vlan Mapping Table\r\n");
        CliPrintf (CliHandle, "-------------------------\r\n");
        do
        {
            if (VlanGetContextInfoFromIfIndex
                ((UINT4) i4NextPortId, &u4TempContextId,
                 &u2LocalPortId) == VLAN_SUCCESS)
            {

                if (((u4IfIndex != 0) && (i4NextPortId == (INT4) u4IfIndex))
                    || ((u4IfIndex == 0) && (u4TempContextId == u4ContextId)))
                {

                    if (i4CurrentPort != i4NextPortId)
                    {
                        CliPrintf (CliHandle, "%20s%20s\r\n",
                                   " Local service vlan", "Relay service vlan");
                    }
                    nmhGetDot1adMIVidTranslationRelayVid (i4NextPortId,
                                                          i4LocalNextSVlanId,
                                                          &i4RelaySVlanId);
                    u4PagingStatus =
                        (UINT4) CliPrintf (CliHandle, " %10d%20d\r\n",
                                           i4LocalNextSVlanId, i4RelaySVlanId);

                }
            }
            i4CurrentPort = i4NextPortId;
            i4LocalSVlanId = i4LocalNextSVlanId;

            if (nmhGetNextIndexDot1adMIVidTranslationTable
                (i4CurrentPort, &i4NextPortId, i4LocalSVlanId,
                 &i4LocalNextSVlanId) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }

    return CLI_SUCCESS;
}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbCliSelectContextOnMode                     */
/*                                                                           */
/*    Description         : This function is used to check the Mode of       */
/*                          the command and also if it a Config Mode         */
/*                          command it will do SelectContext for the         */
/*                          Context.                                         */
/*                                                                           */
/*    Input(s)            : u4Cmd - CLI Command.                             */
/*                                                                           */
/*    Output(s)           : CliHandle - Contains error messages.             */
/*                          pu4ContextId - Context Id.                       */
/*                          pu2LocalPort - Local Port Number.                */
/*                          pu2Flag      - Show command or Not.              */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanPbCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                              UINT4 *pu4Context, UINT2 *pu2LocalPort)
{
    UINT4               u4ContextId;
    INT4                i4PortId = 0;
    UINT1               u1IntfCmdFlag = VLAN_FALSE;    /* This flag is used in MI
                                                       case, to know whether
                                                       the command is an
                                                       a interface mode
                                                       command or pb mode
                                                       command (for future
                                                       reference). */

    *pu4Context = VLAN_DEF_CONTEXT_ID;

    /* Get the Context-Id from the pCliContext structure */
    u4ContextId = (UINT4) CLI_GET_CXT_ID ();

    /* Check if the command is a switch mode command */
    if (u4ContextId != VLAN_CLI_INVALID_CONTEXT)
    {
        /* Switch-mode Command */
        *pu4Context = u4ContextId;
    }
    else
    {
        /* This flag (u1IntfCmdFlag) is used only in case of MI.
         * If the command is a pb mode command (future reference)then the
         * context-id won't be VLAN_CLI_INVALID_CONTEXT, So this is an
         * interface mode command. Now by refering this flag we have to
         * get the context-id and local port number from the
         * IfIndex (CLI_GET_IFINDEX). */
        u1IntfCmdFlag = GARP_TRUE;
    }

    i4PortId = CLI_GET_IFINDEX ();

    /* In SI both the i4PortId and Localport are same. So no need to call
     * VlanGetContextInfoFromIfIndex. */
    *pu2LocalPort = (UINT2) i4PortId;

    if (VlanVcmGetSystemMode (VLANMOD_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (u1IntfCmdFlag == VLAN_TRUE)
        {
            /* This is an Interface Mode command
             * Get the context Id from the IfIndex */
            if (i4PortId != -1)
            {
                if (VlanGetContextInfoFromIfIndex
                    ((UINT4) i4PortId, pu4Context, pu2LocalPort)
                    != VLAN_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    return VLAN_FAILURE;
                }
            }
        }
    }

    /* Since we are calling SI nmh routine for Configuration commands
     * we have to do SelectContext for Config commands*/
    if (VlanSelectContext (*pu4Context) != VLAN_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
        return VLAN_FAILURE;
    }

    if ((VLAN_SYSTEM_CONTROL () == VLAN_SNMP_TRUE) &&
        (u4Cmd != CLI_PB_BRIDGE_MODE))
    {
        CliPrintf (CliHandle, "\r%% VLAN switching is shutdown\r\n");
        VlanReleaseContext ();
        return VLAN_FAILURE;
    }
    return VLAN_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbShowCVlanStat                               */
/*                                                                          */
/*     DESCRIPTION      : This function will Display Customer vlan          */
/*                        statistics                                        */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
VlanPbShowCVlanStat (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Port,
                     INT4 i4CvlanId)
{

    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4NextCVlanId;
    UINT4               u4Frames = 0;
    UINT1               au1Name[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4TblIndex;
    UINT2               u2LocalPort = VLAN_INVALID_PORT_INDEX;
    tVlanCVlanSVlanEntry *pCVlanSVlanEntry = NULL;

    MEMSET (au1Name, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (i4CvlanId == 0)
    {

        i4CurrentPort = i4Port;
        while (nmhGetNextIndexDot1adMICVidRegistrationTable
               (i4CurrentPort, &i4NextPort, i4CvlanId, &i4NextCVlanId))
        {

            if (((INT4) i4NextPort > i4Port))
            {
                break;
            }

            VlanCfaCliGetIfName (i4Port, (INT1 *) au1Name);

            CliPrintf (CliHandle, " Customer Vlan Statistics         \n");
            CliPrintf (CliHandle, " -----------------------------------\n");

            CliPrintf (CliHandle,
                       " Interface Id                     : %-6s\r\n", au1Name);
            CliPrintf (CliHandle, " Customer Vlan Id                 : %d\r\n",
                       i4NextCVlanId);
            u4Frames = 0;
            nmhGetFsMIPbPortCVlanCounterRxUcast (u4ContextId, i4NextPort,
                                                 i4NextCVlanId, &u4Frames);
            CliPrintf (CliHandle, " Unicast frames received          : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIPbPortCVlanCounterRxFrames (u4ContextId, i4NextPort,
                                                  i4NextCVlanId, &u4Frames);
            CliPrintf (CliHandle, " In Frames                        : %u\r\n",
                       u4Frames);

            u4Frames = 0;
            nmhGetFsMIPbPortCVlanCounterRxBytes (u4ContextId, i4NextPort,
                                                 i4NextCVlanId, &u4Frames);
            CliPrintf (CliHandle, " In Bytes                         : %u\r\n",
                       u4Frames);
            if (VlanGetContextInfoFromIfIndex ((UINT4) i4NextPort,
                                               &u4ContextId, &u2LocalPort)
                == VLAN_FAILURE)
            {
                return VLAN_FAILURE;
            }

            if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
            {
                return VLAN_FAILURE;
            }
            u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
            pCVlanSVlanEntry =
                VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                          (UINT2) u2LocalPort,
                                          (tVlanId) i4NextCVlanId);

            if ((pCVlanSVlanEntry == NULL))
            {
                VlanReleaseContext ();
                return VLAN_FAILURE;
            }
            VlanReleaseContext ();

            if (pCVlanSVlanEntry->u1CvlanStatus == VLAN_ENABLED)
            {
                CliPrintf (CliHandle,
                           " Customer Vlan Statistics Collection is Enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           " Customer Vlan Statistics Collection is Disabled\r\n");
            }

            i4CurrentPort = i4NextPort;
            i4CvlanId = i4NextCVlanId;
        }

    }
    else
    {
        if (VlanGetContextInfoFromIfIndex ((UINT4) i4Port,
                                           &u4ContextId, &u2LocalPort)
            == VLAN_FAILURE)
        {
            return VLAN_FAILURE;
        }

        if (VlanSelectContext (u4ContextId) == VLAN_FAILURE)
        {
            return VLAN_FAILURE;
        }
        u4TblIndex = VlanPbGetSVlanTableIndex (VLAN_SVLAN_PORT_CVLAN_TYPE);
        pCVlanSVlanEntry =
            VlanPbGetCVlanSVlanEntry (u4TblIndex,
                                      (UINT2) u2LocalPort, (tVlanId) i4CvlanId);

        if ((pCVlanSVlanEntry == NULL))
        {
            VlanReleaseContext ();
            return VLAN_FAILURE;
        }
        VlanReleaseContext ();

        VlanCfaCliGetIfName (i4Port, (INT1 *) au1Name);

        CliPrintf (CliHandle, " Customer Vlan Statistics         \n");
        CliPrintf (CliHandle, " -----------------------------------\n");

        CliPrintf (CliHandle, " Interface Id                     : %-6s\r\n",
                   au1Name);
        CliPrintf (CliHandle, " Customer Vlan Id                 : %d\r\n",
                   i4CvlanId);
        u4Frames = 0;
        nmhGetFsMIPbPortCVlanCounterRxUcast (u4ContextId, u2LocalPort,
                                             i4CvlanId, &u4Frames);
        CliPrintf (CliHandle, " Unicast frames received          : %u\r\n",
                   u4Frames);

        u4Frames = 0;
        nmhGetFsMIPbPortCVlanCounterRxFrames (u4ContextId, u2LocalPort,
                                              i4CvlanId, &u4Frames);
        CliPrintf (CliHandle, " In Frames                        : %u\r\n",
                   u4Frames);

        u4Frames = 0;
        nmhGetFsMIPbPortCVlanCounterRxBytes (u4ContextId, u2LocalPort,
                                             i4CvlanId, &u4Frames);
        CliPrintf (CliHandle, " In Bytes                         : %u\r\n",
                   u4Frames);
        if (pCVlanSVlanEntry->u1CvlanStatus == VLAN_ENABLED)
        {
            CliPrintf (CliHandle,
                       " Customer Vlan Statistics Collection is Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " Customer Vlan Statistics Collection is Disabled\r\n");
        }

    }

    return VLAN_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : VlanPbClearCVlanStat                              */
/*                                                                          */
/*     DESCRIPTION      : This function will Clear   Customer vlan          */
/*                        statistics                                        */
/*                                                                          */
/*     INPUT            : u4Port      - Port Index                          */
/*                        u4ContextId - Switch Context Id                   */
/*                                                                          */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
VlanPbClearCVlanStat (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Port,
                      INT4 i4CvlanId)
{
    INT4                i4CurrentPort;
    INT4                i4NextPort;
    INT4                i4NextCVlanId;
    UINT1               au1Name[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4ErrorCode;
    UINT1               u1Flag = 0;
    UINT1               u1BrgPortType = 0;
    MEMSET (au1Name, 0, CFA_MAX_PORT_NAME_LENGTH);

    UNUSED_PARAM (CliHandle);
    VlanL2IwfGetPbPortType ((UINT4) i4Port, &u1BrgPortType);
    u4ContextId = CLI_GET_CXT_ID ();
    if (i4CvlanId == 0)
    {

        i4CurrentPort = i4Port;
        while (nmhGetNextIndexDot1adMICVidRegistrationTable
               (i4CurrentPort, &i4NextPort, i4CvlanId, &i4NextCVlanId))
        {

            if (((INT4) i4NextPort != i4Port))
            {
                break;
            }
            u1Flag = 1;
            if (nmhTestv2FsMIPbPortCVlanClearCounter (&u4ErrorCode,
                                                      (INT4) u4ContextId,
                                                      i4NextPort,
                                                      (UINT4) i4NextCVlanId,
                                                      VLAN_TRUE) !=
                SNMP_SUCCESS)

            {
                return CLI_FAILURE;
            }
            if (nmhSetFsMIPbPortCVlanClearCounter ((INT4) u4ContextId,
                                                   i4NextPort,
                                                   (UINT4) i4NextCVlanId,
                                                   VLAN_TRUE) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            i4CurrentPort = i4NextPort;
            i4CvlanId = i4NextCVlanId;

        }
        if (u1Flag == 0)
        {
            if (u1BrgPortType != VLAN_CUSTOMER_EDGE_PORT)
            {
                CLI_SET_ERR (CLI_PB_CNP_PNP_ERR);
            }
            else
            {
                CLI_SET_ERR (CLI_PB_CVID_REG_ERR);
            }
            return CLI_FAILURE;
        }
    }
    else
    {
        if (u1BrgPortType != VLAN_CUSTOMER_EDGE_PORT)
        {
            CLI_SET_ERR (CLI_PB_CNP_PNP_ERR);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsMIPbPortCVlanClearCounter (&u4ErrorCode,
                                                  (INT4) u4ContextId,
                                                  i4Port,
                                                  (UINT4) i4CvlanId,
                                                  VLAN_TRUE) != SNMP_SUCCESS)

        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMIPbPortCVlanClearCounter ((INT4) u4ContextId,
                                               i4Port,
                                               (UINT4) i4CvlanId,
                                               VLAN_TRUE) != SNMP_SUCCESS)

        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPbSetCounterStatus                           */
/*                                                                           */
/*    Description         : This function is used to enable/disable the      */
/*                          statistics collection on the configured VLAN     */
/*                                                                           */
/*    Input(s)            : CliHandle       - Cli Handle                     */
/*                          u4VlanId        - Vlan Id                        */
/*                          i4PortId        - Port Number                    */
/*                          i4CounterStatus - Counter Status Enable/Disable  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbSetCounterStatus (tCliHandle CliHandle, INT4 i4Port, INT4 i4VlanId,
                        INT4 i4CounterStatus)
{
    UINT4               u4ContextId;
    INT4                i4CurrentPort = 0;
    INT4                i4NextPort;
    INT4                i4NextCVlanId;
    UINT1               au1Name[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4ErrorCode;
    UINT1               u1Flag = 0;

    MEMSET (au1Name, 0, CFA_MAX_PORT_NAME_LENGTH);
    UNUSED_PARAM (CliHandle);
    u4ContextId = VLAN_DEF_CONTEXT_ID;

    u4ContextId = CLI_GET_CXT_ID ();
    if (i4VlanId == 0)
    {
        i4CurrentPort = i4Port;
        while (nmhGetNextIndexDot1adMICVidRegistrationTable
               (i4CurrentPort, &i4NextPort, i4VlanId,
                &i4NextCVlanId) != SNMP_FAILURE)
        {

            if (((INT4) i4NextPort != i4Port))
            {
                break;
            }

            u1Flag = 1;
            if (nmhTestv2FsMIPbPortCVlanCounterStatus (&u4ErrorCode,
                                                       u4ContextId,
                                                       i4NextPort,
                                                       i4NextCVlanId,
                                                       i4CounterStatus) !=
                SNMP_SUCCESS)

            {
                return CLI_FAILURE;
            }
            if (nmhSetFsMIPbPortCVlanCounterStatus (u4ContextId,
                                                    i4NextPort,
                                                    i4NextCVlanId,
                                                    i4CounterStatus) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            i4CurrentPort = i4NextPort;
            i4VlanId = i4NextCVlanId;

        }
        if (u1Flag == 0)
        {
            CLI_SET_ERR (CLI_PB_CVID_ENTRY_ERR);
            return CLI_FAILURE;

        }
    }
    else
    {
        if (nmhTestv2FsMIPbPortCVlanCounterStatus (&u4ErrorCode,
                                                   u4ContextId,
                                                   i4Port,
                                                   i4VlanId,
                                                   i4CounterStatus) !=
            SNMP_SUCCESS)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIPbPortCVlanCounterStatus (u4ContextId,
                                                i4Port,
                                                i4VlanId,
                                                i4CounterStatus) !=
            SNMP_SUCCESS)

        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }

    return CLI_SUCCESS;
}

#endif /*__VLNPBCLI_C__*/
