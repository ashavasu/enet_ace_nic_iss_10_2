/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsdot1lw.h,v 1.7 2013/12/05 12:44:52 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Dot1adPortTable. */
INT1
nmhValidateIndexInstanceDot1adPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adPortTable  */

INT1
nmhGetFirstIndexDot1adPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adPortPcpSelectionRow ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1adPortUseDei ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1adPortReqDropEncoding ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1adPortSVlanPriorityType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1adPortSVlanPriority ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adPortPcpSelectionRow ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1adPortUseDei ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1adPortReqDropEncoding ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1adPortSVlanPriorityType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1adPortSVlanPriority ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adPortPcpSelectionRow ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1adPortUseDei ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1adPortReqDropEncoding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1adPortSVlanPriorityType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1adPortSVlanPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adVidTranslationTable. */
INT1
nmhValidateIndexInstanceDot1adVidTranslationTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adVidTranslationTable  */

INT1
nmhGetFirstIndexDot1adVidTranslationTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adVidTranslationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adVidTranslationRelayVid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adVidTranslationRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adVidTranslationRelayVid ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adVidTranslationRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adVidTranslationRelayVid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adVidTranslationRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adVidTranslationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adCVidRegistrationTable. */
INT1
nmhValidateIndexInstanceDot1adCVidRegistrationTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adCVidRegistrationTable  */

INT1
nmhGetFirstIndexDot1adCVidRegistrationTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adCVidRegistrationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adCVidRegistrationSVid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adCVidRegistrationUntaggedPep ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adCVidRegistrationUntaggedCep ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adCVidRegistrationRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adCVIdRegistrationRelayCVid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adCVidRegistrationSVlanPriorityType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adCVidRegistrationSVlanPriority ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adCVidRegistrationSVid ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adCVidRegistrationUntaggedPep ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adCVidRegistrationUntaggedCep ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adCVidRegistrationRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adCVIdRegistrationRelayCVid ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adCVidRegistrationSVlanPriorityType ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adCVidRegistrationSVlanPriority ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adCVidRegistrationSVid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adCVidRegistrationUntaggedPep ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adCVidRegistrationUntaggedCep ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adCVidRegistrationRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adCVIdRegistrationRelayCVid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adCVidRegistrationSVlanPriorityType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adCVidRegistrationSVlanPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adCVidRegistrationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adPepTable. */
INT1
nmhValidateIndexInstanceDot1adPepTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adPepTable  */

INT1
nmhGetFirstIndexDot1adPepTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adPepTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adPepPvid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adPepDefaultUserPriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adPepAccptableFrameTypes ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adPepIngressFiltering ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adPepPvid ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adPepDefaultUserPriority ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adPepAccptableFrameTypes ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adPepIngressFiltering ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adPepPvid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adPepDefaultUserPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adPepAccptableFrameTypes ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adPepIngressFiltering ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adPepTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adServicePriorityRegenerationTable. */
INT1
nmhValidateIndexInstanceDot1adServicePriorityRegenerationTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adServicePriorityRegenerationTable  */

INT1
nmhGetFirstIndexDot1adServicePriorityRegenerationTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adServicePriorityRegenerationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adServicePriorityRegenRegeneratedPriority ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adServicePriorityRegenRegeneratedPriority ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adServicePriorityRegenRegeneratedPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adServicePriorityRegenerationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adPcpDecodingTable. */
INT1
nmhValidateIndexInstanceDot1adPcpDecodingTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adPcpDecodingTable  */

INT1
nmhGetFirstIndexDot1adPcpDecodingTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adPcpDecodingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adPcpDecodingPriority ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adPcpDecodingDropEligible ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adPcpDecodingPriority ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adPcpDecodingDropEligible ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adPcpDecodingPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adPcpDecodingDropEligible ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adPcpDecodingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adPcpEncodingTable. */
INT1
nmhValidateIndexInstanceDot1adPcpEncodingTable ARG_LIST((INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adPcpEncodingTable  */

INT1
nmhGetFirstIndexDot1adPcpEncodingTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adPcpEncodingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adPcpEncodingPcpValue ARG_LIST((INT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adPcpEncodingPcpValue ARG_LIST((INT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adPcpEncodingPcpValue ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adPcpEncodingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
