/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbsdb.h,v 1.6 2013/12/05 12:44:52 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSDOT1DB_H
#define _FSDOT1DB_H

UINT1 Dot1adPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1adVidTranslationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1adCVidRegistrationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1adPepTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1adServicePriorityRegenerationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1adPcpDecodingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1adPcpEncodingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsdot1 [] ={1,3,6,1,4,1,2076,130};
tSNMP_OID_TYPE fsdot1OID = {8, fsdot1};


UINT4 Dot1adPortNum [ ] ={1,3,6,1,4,1,2076,130,1,1,1,1};
UINT4 Dot1adPortPcpSelectionRow [ ] ={1,3,6,1,4,1,2076,130,1,1,1,2};
UINT4 Dot1adPortUseDei [ ] ={1,3,6,1,4,1,2076,130,1,1,1,3};
UINT4 Dot1adPortReqDropEncoding [ ] ={1,3,6,1,4,1,2076,130,1,1,1,4};
UINT4 Dot1adPortSVlanPriorityType [ ] ={1,3,6,1,4,1,2076,130,1,1,1,5};
UINT4 Dot1adPortSVlanPriority [ ] ={1,3,6,1,4,1,2076,130,1,1,1,6};
UINT4 Dot1adVidTranslationLocalVid [ ] ={1,3,6,1,4,1,2076,130,1,2,1,1};
UINT4 Dot1adVidTranslationRelayVid [ ] ={1,3,6,1,4,1,2076,130,1,2,1,2};
UINT4 Dot1adVidTranslationRowStatus [ ] ={1,3,6,1,4,1,2076,130,1,2,1,3};
UINT4 Dot1adCVidRegistrationCVid [ ] ={1,3,6,1,4,1,2076,130,1,3,1,1};
UINT4 Dot1adCVidRegistrationSVid [ ] ={1,3,6,1,4,1,2076,130,1,3,1,2};
UINT4 Dot1adCVidRegistrationUntaggedPep [ ] ={1,3,6,1,4,1,2076,130,1,3,1,3};
UINT4 Dot1adCVidRegistrationUntaggedCep [ ] ={1,3,6,1,4,1,2076,130,1,3,1,4};
UINT4 Dot1adCVidRegistrationSVlanPriorityType [ ] ={1,3,6,1,4,1,2076,130,1,3,1,5};
UINT4 Dot1adCVidRegistrationSVlanPriority [ ] ={1,3,6,1,4,1,2076,130,1,3,1,6};
UINT4 Dot1adCVidRegistrationRowStatus [ ] ={1,3,6,1,4,1,2076,130,1,3,1,7};
UINT4 Dot1adCVIdRegistrationRelayCVid [ ] ={1,3,6,1,4,1,2076,130,1,3,1,8};
UINT4 Dot1adPepPvid [ ] ={1,3,6,1,4,1,2076,130,1,4,1,1};
UINT4 Dot1adPepDefaultUserPriority [ ] ={1,3,6,1,4,1,2076,130,1,4,1,2};
UINT4 Dot1adPepAccptableFrameTypes [ ] ={1,3,6,1,4,1,2076,130,1,4,1,3};
UINT4 Dot1adPepIngressFiltering [ ] ={1,3,6,1,4,1,2076,130,1,4,1,4};
UINT4 Dot1adServicePriorityRegenReceivedPriority [ ] ={1,3,6,1,4,1,2076,130,1,5,1,1};
UINT4 Dot1adServicePriorityRegenRegeneratedPriority [ ] ={1,3,6,1,4,1,2076,130,1,5,1,2};
UINT4 Dot1adPcpDecodingPcpSelRow [ ] ={1,3,6,1,4,1,2076,130,1,6,1,1};
UINT4 Dot1adPcpDecodingPcpValue [ ] ={1,3,6,1,4,1,2076,130,1,6,1,2};
UINT4 Dot1adPcpDecodingPriority [ ] ={1,3,6,1,4,1,2076,130,1,6,1,3};
UINT4 Dot1adPcpDecodingDropEligible [ ] ={1,3,6,1,4,1,2076,130,1,6,1,4};
UINT4 Dot1adPcpEncodingPcpSelRow [ ] ={1,3,6,1,4,1,2076,130,1,7,1,1};
UINT4 Dot1adPcpEncodingPriority [ ] ={1,3,6,1,4,1,2076,130,1,7,1,2};
UINT4 Dot1adPcpEncodingDropEligible [ ] ={1,3,6,1,4,1,2076,130,1,7,1,3};
UINT4 Dot1adPcpEncodingPcpValue [ ] ={1,3,6,1,4,1,2076,130,1,7,1,4};


tMbDbEntry fsdot1MibEntry[]= {

{{12,Dot1adPortNum}, GetNextIndexDot1adPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1adPortTableINDEX, 1, 0, 0, NULL},

{{12,Dot1adPortPcpSelectionRow}, GetNextIndexDot1adPortTable, Dot1adPortPcpSelectionRowGet, Dot1adPortPcpSelectionRowSet, Dot1adPortPcpSelectionRowTest, Dot1adPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPortTableINDEX, 1, 0, 0, "1"},

{{12,Dot1adPortUseDei}, GetNextIndexDot1adPortTable, Dot1adPortUseDeiGet, Dot1adPortUseDeiSet, Dot1adPortUseDeiTest, Dot1adPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPortTableINDEX, 1, 0, 0, "2"},

{{12,Dot1adPortReqDropEncoding}, GetNextIndexDot1adPortTable, Dot1adPortReqDropEncodingGet, Dot1adPortReqDropEncodingSet, Dot1adPortReqDropEncodingTest, Dot1adPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPortTableINDEX, 1, 0, 0, "2"},

{{12,Dot1adPortSVlanPriorityType}, GetNextIndexDot1adPortTable, Dot1adPortSVlanPriorityTypeGet, Dot1adPortSVlanPriorityTypeSet, Dot1adPortSVlanPriorityTypeTest, Dot1adPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPortTableINDEX, 1, 0, 0, "2"},

{{12,Dot1adPortSVlanPriority}, GetNextIndexDot1adPortTable, Dot1adPortSVlanPriorityGet, Dot1adPortSVlanPrioritySet, Dot1adPortSVlanPriorityTest, Dot1adPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPortTableINDEX, 1, 0, 0, "0"},

{{12,Dot1adVidTranslationLocalVid}, GetNextIndexDot1adVidTranslationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adVidTranslationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adVidTranslationRelayVid}, GetNextIndexDot1adVidTranslationTable, Dot1adVidTranslationRelayVidGet, Dot1adVidTranslationRelayVidSet, Dot1adVidTranslationRelayVidTest, Dot1adVidTranslationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adVidTranslationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adVidTranslationRowStatus}, GetNextIndexDot1adVidTranslationTable, Dot1adVidTranslationRowStatusGet, Dot1adVidTranslationRowStatusSet, Dot1adVidTranslationRowStatusTest, Dot1adVidTranslationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adVidTranslationTableINDEX, 2, 0, 1, NULL},

{{12,Dot1adCVidRegistrationCVid}, GetNextIndexDot1adCVidRegistrationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adCVidRegistrationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adCVidRegistrationSVid}, GetNextIndexDot1adCVidRegistrationTable, Dot1adCVidRegistrationSVidGet, Dot1adCVidRegistrationSVidSet, Dot1adCVidRegistrationSVidTest, Dot1adCVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adCVidRegistrationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adCVidRegistrationUntaggedPep}, GetNextIndexDot1adCVidRegistrationTable, Dot1adCVidRegistrationUntaggedPepGet, Dot1adCVidRegistrationUntaggedPepSet, Dot1adCVidRegistrationUntaggedPepTest, Dot1adCVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adCVidRegistrationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adCVidRegistrationUntaggedCep}, GetNextIndexDot1adCVidRegistrationTable, Dot1adCVidRegistrationUntaggedCepGet, Dot1adCVidRegistrationUntaggedCepSet, Dot1adCVidRegistrationUntaggedCepTest, Dot1adCVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adCVidRegistrationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adCVidRegistrationSVlanPriorityType}, GetNextIndexDot1adCVidRegistrationTable, Dot1adCVidRegistrationSVlanPriorityTypeGet, Dot1adCVidRegistrationSVlanPriorityTypeSet, Dot1adCVidRegistrationSVlanPriorityTypeTest, Dot1adCVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adCVidRegistrationTableINDEX, 2, 0, 0, "2"},

{{12,Dot1adCVidRegistrationSVlanPriority}, GetNextIndexDot1adCVidRegistrationTable, Dot1adCVidRegistrationSVlanPriorityGet, Dot1adCVidRegistrationSVlanPrioritySet, Dot1adCVidRegistrationSVlanPriorityTest, Dot1adCVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adCVidRegistrationTableINDEX, 2, 0, 0, "0"},

{{12,Dot1adCVidRegistrationRowStatus}, GetNextIndexDot1adCVidRegistrationTable, Dot1adCVidRegistrationRowStatusGet, Dot1adCVidRegistrationRowStatusSet, Dot1adCVidRegistrationRowStatusTest, Dot1adCVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adCVidRegistrationTableINDEX, 2, 0, 1, NULL},

{{12,Dot1adCVIdRegistrationRelayCVid}, GetNextIndexDot1adCVidRegistrationTable, Dot1adCVIdRegistrationRelayCVidGet, Dot1adCVIdRegistrationRelayCVidSet, Dot1adCVIdRegistrationRelayCVidTest, Dot1adCVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adCVidRegistrationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adPepPvid}, GetNextIndexDot1adPepTable, Dot1adPepPvidGet, Dot1adPepPvidSet, Dot1adPepPvidTest, Dot1adPepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPepTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adPepDefaultUserPriority}, GetNextIndexDot1adPepTable, Dot1adPepDefaultUserPriorityGet, Dot1adPepDefaultUserPrioritySet, Dot1adPepDefaultUserPriorityTest, Dot1adPepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPepTableINDEX, 2, 0, 0, "0"},

{{12,Dot1adPepAccptableFrameTypes}, GetNextIndexDot1adPepTable, Dot1adPepAccptableFrameTypesGet, Dot1adPepAccptableFrameTypesSet, Dot1adPepAccptableFrameTypesTest, Dot1adPepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPepTableINDEX, 2, 0, 0, "1"},

{{12,Dot1adPepIngressFiltering}, GetNextIndexDot1adPepTable, Dot1adPepIngressFilteringGet, Dot1adPepIngressFilteringSet, Dot1adPepIngressFilteringTest, Dot1adPepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPepTableINDEX, 2, 0, 0, "2"},

{{12,Dot1adServicePriorityRegenReceivedPriority}, GetNextIndexDot1adServicePriorityRegenerationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adServicePriorityRegenerationTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adServicePriorityRegenRegeneratedPriority}, GetNextIndexDot1adServicePriorityRegenerationTable, Dot1adServicePriorityRegenRegeneratedPriorityGet, Dot1adServicePriorityRegenRegeneratedPrioritySet, Dot1adServicePriorityRegenRegeneratedPriorityTest, Dot1adServicePriorityRegenerationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adServicePriorityRegenerationTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adPcpDecodingPcpSelRow}, GetNextIndexDot1adPcpDecodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adPcpDecodingPcpValue}, GetNextIndexDot1adPcpDecodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adPcpDecodingPriority}, GetNextIndexDot1adPcpDecodingTable, Dot1adPcpDecodingPriorityGet, Dot1adPcpDecodingPrioritySet, Dot1adPcpDecodingPriorityTest, Dot1adPcpDecodingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adPcpDecodingDropEligible}, GetNextIndexDot1adPcpDecodingTable, Dot1adPcpDecodingDropEligibleGet, Dot1adPcpDecodingDropEligibleSet, Dot1adPcpDecodingDropEligibleTest, Dot1adPcpDecodingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adPcpEncodingPcpSelRow}, GetNextIndexDot1adPcpEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adPcpEncodingTableINDEX, 4, 0, 0, NULL},

{{12,Dot1adPcpEncodingPriority}, GetNextIndexDot1adPcpEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adPcpEncodingTableINDEX, 4, 0, 0, NULL},

{{12,Dot1adPcpEncodingDropEligible}, GetNextIndexDot1adPcpEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adPcpEncodingTableINDEX, 4, 0, 0, NULL},

{{12,Dot1adPcpEncodingPcpValue}, GetNextIndexDot1adPcpEncodingTable, Dot1adPcpEncodingPcpValueGet, Dot1adPcpEncodingPcpValueSet, Dot1adPcpEncodingPcpValueTest, Dot1adPcpEncodingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adPcpEncodingTableINDEX, 4, 0, 0, NULL},
};
tMibData fsdot1Entry = { 31, fsdot1MibEntry };
#endif /* _FSDOT1DB_H */

