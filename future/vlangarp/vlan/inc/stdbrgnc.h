
/* $Id: stdbrgnc.h,v 1.1 2015/12/31 11:29:39 siva Exp $
    ISS Wrapper header
    module P-BRIDGE-MIB

 */
#ifndef _H_i_P_BRIDGE_MIB
#define _H_i_P_BRIDGE_MIB

/********************************************************************
* FUNCTION NcDot1dDeviceCapabilitiesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dDeviceCapabilitiesGet (
                UINT1 *pncx_list_tDot1dDeviceCapabilities );

/********************************************************************
* FUNCTION NcDot1dTrafficClassesEnabledSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dTrafficClassesEnabledSet (
                INT4 i4Dot1dTrafficClassesEnabled );

/********************************************************************
* FUNCTION NcDot1dTrafficClassesEnabledTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dTrafficClassesEnabledTest (UINT4 *pu4Error,
                INT4 i4Dot1dTrafficClassesEnabled );

/********************************************************************
* FUNCTION NcDot1dGmrpStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dGmrpStatusSet (
                INT4 i4Dot1dGmrpStatus );

/********************************************************************
* FUNCTION NcDot1dGmrpStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dGmrpStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1dGmrpStatus );

/********************************************************************
* FUNCTION NcDot1dTpHCPortInFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dTpHCPortInFramesGet (
                INT4 i4Dot1dTpPort,
                unsigned long long  *pu8Dot1dTpHCPortInFrames );

/********************************************************************
* FUNCTION NcDot1dTpHCPortOutFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dTpHCPortOutFramesGet (
                INT4 i4Dot1dTpPort,
                unsigned long long  *pu8Dot1dTpHCPortOutFrames );

/********************************************************************
* FUNCTION NcDot1dTpHCPortInDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dTpHCPortInDiscardsGet (
                INT4 i4Dot1dTpPort,
                unsigned long long *pu8Dot1dTpHCPortInDiscards );

/********************************************************************
* FUNCTION NcDot1dTpPortInOverflowFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dTpPortInOverflowFramesGet (
                INT4 i4Dot1dTpPort,
                UINT4 *pu4Dot1dTpPortInOverflowFrames );

/********************************************************************
* FUNCTION NcDot1dTpPortOutOverflowFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dTpPortOutOverflowFramesGet (
                INT4 i4Dot1dTpPort,
                UINT4 *pu4Dot1dTpPortOutOverflowFrames );

/********************************************************************
* FUNCTION NcDot1dTpPortInOverflowDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dTpPortInOverflowDiscardsGet (
                INT4 i4Dot1dTpPort,
                UINT4 *pu4Dot1dTpPortInOverflowDiscards );

/********************************************************************
* FUNCTION NcDot1dRegenUserPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dRegenUserPrioritySet (
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1dUserPriority,
                INT4 i4Dot1dRegenUserPriority );

/********************************************************************
* FUNCTION NcDot1dRegenUserPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dRegenUserPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1dUserPriority,
                INT4 i4Dot1dRegenUserPriority );

/********************************************************************
* FUNCTION NcDot1dTrafficClassSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dTrafficClassSet (
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1dTrafficClassPriority,
                INT4 i4Dot1dTrafficClass );

/********************************************************************
* FUNCTION NcDot1dTrafficClassTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dTrafficClassTest (UINT4 *pu4Error,
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1dTrafficClassPriority,
                INT4 i4Dot1dTrafficClass );

/********************************************************************
* FUNCTION NcDot1dPortOutboundAccessPriorityGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1dPortOutboundAccessPriorityGet (
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1dRegenUserPriority,
                INT4 *pi4Dot1dPortOutboundAccessPriority );

/* END i_P_BRIDGE_MIB.c */


#endif
