/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmvledb.h,v 1.14 2015/06/05 09:40:35 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMVLEDB_H
#define _FSMVLEDB_H

UINT1 FsMIVlanBridgeInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIVlanTunnelContextInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIVlanTunnelTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIVlanTunnelProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIVlanTunnelProtocolStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIVlanDiscardStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIServiceVlanTunnelProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsmvle [] ={1,3,6,1,4,1,2076,138};
tSNMP_OID_TYPE fsmvleOID = {8, fsmvle};


UINT4 FsMIVlanContextId [ ] ={1,3,6,1,4,1,2076,138,1,1,1,1};
UINT4 FsMIVlanBridgeMode [ ] ={1,3,6,1,4,1,2076,138,1,1,1,2};
UINT4 FsMIVlanTunnelBpduPri [ ] ={1,3,6,1,4,1,2076,138,2,1,1,1};
UINT4 FsMIVlanTunnelStpAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,2};
UINT4 FsMIVlanTunnelLacpAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,3};
UINT4 FsMIVlanTunnelDot1xAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,4};
UINT4 FsMIVlanTunnelGvrpAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,5};
UINT4 FsMIVlanTunnelGmrpAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,6};
UINT4 FsMIVlanTunnelMvrpAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,7};
UINT4 FsMIVlanTunnelMmrpAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,8};
UINT4 FsMIVlanTunnelElmiAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,9};
UINT4 FsMIVlanTunnelLldpAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,10};
UINT4 FsMIVlanTunnelEcfmAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,11};
UINT4 FsMIVlanTunnelEoamAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,12};
UINT4 FsMIVlanTunnelIgmpAddress [ ] ={1,3,6,1,4,1,2076,138,2,1,1,13};
UINT4 FsMIVlanPort [ ] ={1,3,6,1,4,1,2076,138,2,2,1,1};
UINT4 FsMIVlanTunnelStatus [ ] ={1,3,6,1,4,1,2076,138,2,2,1,2};
UINT4 FsMIVlanTunnelProtocolDot1x [ ] ={1,3,6,1,4,1,2076,138,2,3,1,1};
UINT4 FsMIVlanTunnelProtocolLacp [ ] ={1,3,6,1,4,1,2076,138,2,3,1,2};
UINT4 FsMIVlanTunnelProtocolStp [ ] ={1,3,6,1,4,1,2076,138,2,3,1,3};
UINT4 FsMIVlanTunnelProtocolGvrp [ ] ={1,3,6,1,4,1,2076,138,2,3,1,4};
UINT4 FsMIVlanTunnelProtocolGmrp [ ] ={1,3,6,1,4,1,2076,138,2,3,1,5};
UINT4 FsMIVlanTunnelProtocolIgmp [ ] ={1,3,6,1,4,1,2076,138,2,3,1,6};
UINT4 FsMIVlanTunnelProtocolMvrp [ ] ={1,3,6,1,4,1,2076,138,2,3,1,7};
UINT4 FsMIVlanTunnelProtocolMmrp [ ] ={1,3,6,1,4,1,2076,138,2,3,1,8};
UINT4 FsMIVlanTunnelProtocolElmi [ ] ={1,3,6,1,4,1,2076,138,2,3,1,9};
UINT4 FsMIVlanTunnelProtocolLldp [ ] ={1,3,6,1,4,1,2076,138,2,3,1,10};
UINT4 FsMIVlanTunnelProtocolEcfm [ ] ={1,3,6,1,4,1,2076,138,2,3,1,11};
UINT4 FsMIVlanTunnelOverrideOption [ ] ={1,3,6,1,4,1,2076,138,2,3,1,12};
UINT4 FsMIVlanTunnelProtocolEoam [ ] ={1,3,6,1,4,1,2076,138,2,3,1,13};
UINT4 FsMIVlanTunnelProtocolDot1xPktsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,1};
UINT4 FsMIVlanTunnelProtocolDot1xPktsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,2};
UINT4 FsMIVlanTunnelProtocolLacpPktsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,3};
UINT4 FsMIVlanTunnelProtocolLacpPktsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,4};
UINT4 FsMIVlanTunnelProtocolStpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,5};
UINT4 FsMIVlanTunnelProtocolStpPDUsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,6};
UINT4 FsMIVlanTunnelProtocolGvrpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,7};
UINT4 FsMIVlanTunnelProtocolGvrpPDUsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,8};
UINT4 FsMIVlanTunnelProtocolGmrpPktsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,9};
UINT4 FsMIVlanTunnelProtocolGmrpPktsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,10};
UINT4 FsMIVlanTunnelProtocolIgmpPktsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,11};
UINT4 FsMIVlanTunnelProtocolIgmpPktsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,12};
UINT4 FsMIVlanTunnelProtocolMvrpPktsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,13};
UINT4 FsMIVlanTunnelProtocolMvrpPktsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,14};
UINT4 FsMIVlanTunnelProtocolMmrpPktsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,15};
UINT4 FsMIVlanTunnelProtocolMmrpPktsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,16};
UINT4 FsMIVlanTunnelProtocolElmiPktsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,17};
UINT4 FsMIVlanTunnelProtocolElmiPktsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,18};
UINT4 FsMIVlanTunnelProtocolLldpPktsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,19};
UINT4 FsMIVlanTunnelProtocolLldpPktsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,20};
UINT4 FsMIVlanTunnelProtocolEcfmPktsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,21};
UINT4 FsMIVlanTunnelProtocolEcfmPktsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,22};
UINT4 FsMIVlanTunnelProtocolEoamPktsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,4,1,23};
UINT4 FsMIVlanTunnelProtocolEoamPktsSent [ ] ={1,3,6,1,4,1,2076,138,2,4,1,24};
UINT4 FsMIVlanDiscardDot1xPktsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,1};
UINT4 FsMIVlanDiscardDot1xPktsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,2};
UINT4 FsMIVlanDiscardLacpPktsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,3};
UINT4 FsMIVlanDiscardLacpPktsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,4};
UINT4 FsMIVlanDiscardStpPDUsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,5};
UINT4 FsMIVlanDiscardStpPDUsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,6};
UINT4 FsMIVlanDiscardGvrpPktsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,7};
UINT4 FsMIVlanDiscardGvrpPktsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,8};
UINT4 FsMIVlanDiscardGmrpPktsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,9};
UINT4 FsMIVlanDiscardGmrpPktsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,10};
UINT4 FsMIVlanDiscardIgmpPktsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,11};
UINT4 FsMIVlanDiscardIgmpPktsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,12};
UINT4 FsMIVlanDiscardMvrpPktsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,13};
UINT4 FsMIVlanDiscardMvrpPktsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,14};
UINT4 FsMIVlanDiscardMmrpPktsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,15};
UINT4 FsMIVlanDiscardMmrpPktsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,16};
UINT4 FsMIVlanDiscardElmiPktsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,17};
UINT4 FsMIVlanDiscardElmiPktsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,18};
UINT4 FsMIVlanDiscardLldpPktsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,19};
UINT4 FsMIVlanDiscardLldpPktsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,20};
UINT4 FsMIVlanDiscardEcfmPktsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,21};
UINT4 FsMIVlanDiscardEcfmPktsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,22};
UINT4 FsMIVlanDiscardEoamPktsRx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,23};
UINT4 FsMIVlanDiscardEoamPktsTx [ ] ={1,3,6,1,4,1,2076,138,3,1,1,24};
UINT4 FsMIServiceVlanId [ ] ={1,3,6,1,4,1,2076,138,2,5,1,1};
UINT4 FsMIServiceProtocolEnum [ ] ={1,3,6,1,4,1,2076,138,2,5,1,2};
UINT4 FsMIServiceVlanRsvdMacaddress [ ] ={1,3,6,1,4,1,2076,138,2,5,1,3};
UINT4 FsMIServiceVlanTunnelMacaddress [ ] ={1,3,6,1,4,1,2076,138,2,5,1,4};
UINT4 FsMIServiceVlanTunnelProtocolStatus [ ] ={1,3,6,1,4,1,2076,138,2,5,1,5};
UINT4 FsMIServiceVlanTunnelPktsRecvd [ ] ={1,3,6,1,4,1,2076,138,2,5,1,6};
UINT4 FsMIServiceVlanTunnelPktsSent [ ] ={1,3,6,1,4,1,2076,138,2,5,1,7};
UINT4 FsMIServiceVlanDiscardPktsRx [ ] ={1,3,6,1,4,1,2076,138,2,5,1,8};
UINT4 FsMIServiceVlanDiscardPktsTx [ ] ={1,3,6,1,4,1,2076,138,2,5,1,9};



tMbDbEntry fsmvleMibEntry[]= {

{{12,FsMIVlanContextId}, GetNextIndexFsMIVlanBridgeInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIVlanBridgeInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanBridgeMode}, GetNextIndexFsMIVlanBridgeInfoTable, FsMIVlanBridgeModeGet, FsMIVlanBridgeModeSet, FsMIVlanBridgeModeTest, FsMIVlanBridgeInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanBridgeInfoTableINDEX, 1, 0, 0, "7"},

{{12,FsMIVlanTunnelBpduPri}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelBpduPriGet, FsMIVlanTunnelBpduPriSet, FsMIVlanTunnelBpduPriTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, "7"},

{{12,FsMIVlanTunnelStpAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelStpAddressGet, FsMIVlanTunnelStpAddressSet, FsMIVlanTunnelStpAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelLacpAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelLacpAddressGet, FsMIVlanTunnelLacpAddressSet, FsMIVlanTunnelLacpAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelDot1xAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelDot1xAddressGet, FsMIVlanTunnelDot1xAddressSet, FsMIVlanTunnelDot1xAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelGvrpAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelGvrpAddressGet, FsMIVlanTunnelGvrpAddressSet, FsMIVlanTunnelGvrpAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelGmrpAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelGmrpAddressGet, FsMIVlanTunnelGmrpAddressSet, FsMIVlanTunnelGmrpAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelMvrpAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelMvrpAddressGet, FsMIVlanTunnelMvrpAddressSet, FsMIVlanTunnelMvrpAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelMmrpAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelMmrpAddressGet, FsMIVlanTunnelMmrpAddressSet, FsMIVlanTunnelMmrpAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelElmiAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelElmiAddressGet, FsMIVlanTunnelElmiAddressSet, FsMIVlanTunnelElmiAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelLldpAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelLldpAddressGet, FsMIVlanTunnelLldpAddressSet, FsMIVlanTunnelLldpAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelEcfmAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelEcfmAddressGet, FsMIVlanTunnelEcfmAddressSet, FsMIVlanTunnelEcfmAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelEoamAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelEoamAddressGet, FsMIVlanTunnelEoamAddressSet, FsMIVlanTunnelEoamAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelIgmpAddress}, GetNextIndexFsMIVlanTunnelContextInfoTable, FsMIVlanTunnelIgmpAddressGet, FsMIVlanTunnelIgmpAddressSet, FsMIVlanTunnelIgmpAddressTest, FsMIVlanTunnelContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIVlanTunnelContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanPort}, GetNextIndexFsMIVlanTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIVlanTunnelTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelStatus}, GetNextIndexFsMIVlanTunnelTable, FsMIVlanTunnelStatusGet, FsMIVlanTunnelStatusSet, FsMIVlanTunnelStatusTest, FsMIVlanTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolDot1x}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolDot1xGet, FsMIVlanTunnelProtocolDot1xSet, FsMIVlanTunnelProtocolDot1xTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolLacp}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolLacpGet, FsMIVlanTunnelProtocolLacpSet, FsMIVlanTunnelProtocolLacpTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolStp}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolStpGet, FsMIVlanTunnelProtocolStpSet, FsMIVlanTunnelProtocolStpTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolGvrp}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolGvrpGet, FsMIVlanTunnelProtocolGvrpSet, FsMIVlanTunnelProtocolGvrpTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolGmrp}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolGmrpGet, FsMIVlanTunnelProtocolGmrpSet, FsMIVlanTunnelProtocolGmrpTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolIgmp}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolIgmpGet, FsMIVlanTunnelProtocolIgmpSet, FsMIVlanTunnelProtocolIgmpTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolMvrp}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolMvrpGet, FsMIVlanTunnelProtocolMvrpSet, FsMIVlanTunnelProtocolMvrpTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolMmrp}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolMmrpGet, FsMIVlanTunnelProtocolMmrpSet, FsMIVlanTunnelProtocolMmrpTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolElmi}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolElmiGet, FsMIVlanTunnelProtocolElmiSet, FsMIVlanTunnelProtocolElmiTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolLldp}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolLldpGet, FsMIVlanTunnelProtocolLldpSet, FsMIVlanTunnelProtocolLldpTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolEcfm}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolEcfmGet, FsMIVlanTunnelProtocolEcfmSet, FsMIVlanTunnelProtocolEcfmTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelOverrideOption}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelOverrideOptionGet, FsMIVlanTunnelOverrideOptionSet, FsMIVlanTunnelOverrideOptionTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, "2"},

{{12,FsMIVlanTunnelProtocolEoam}, GetNextIndexFsMIVlanTunnelProtocolTable, FsMIVlanTunnelProtocolEoamGet, FsMIVlanTunnelProtocolEoamSet, FsMIVlanTunnelProtocolEoamTest, FsMIVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolDot1xPktsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolDot1xPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolDot1xPktsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolDot1xPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolLacpPktsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolLacpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolLacpPktsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolLacpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolStpPDUsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolStpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolStpPDUsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolStpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolGvrpPDUsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolGvrpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolGvrpPDUsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolGvrpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolGmrpPktsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolGmrpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolGmrpPktsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolGmrpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolIgmpPktsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolIgmpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolIgmpPktsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolIgmpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolMvrpPktsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolMvrpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolMvrpPktsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolMvrpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolMmrpPktsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolMmrpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolMmrpPktsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolMmrpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolElmiPktsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolElmiPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolElmiPktsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolElmiPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolLldpPktsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolLldpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolLldpPktsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolLldpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolEcfmPktsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolEcfmPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolEcfmPktsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolEcfmPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolEoamPktsRecvd}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolEoamPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanTunnelProtocolEoamPktsSent}, GetNextIndexFsMIVlanTunnelProtocolStatsTable, FsMIVlanTunnelProtocolEoamPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIServiceVlanId}, GetNextIndexFsMIServiceVlanTunnelProtocolTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIServiceVlanTunnelProtocolTableINDEX, 3, 0, 0, NULL},

{{12,FsMIServiceProtocolEnum}, GetNextIndexFsMIServiceVlanTunnelProtocolTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIServiceVlanTunnelProtocolTableINDEX, 3, 0, 0, NULL},

{{12,FsMIServiceVlanRsvdMacaddress}, GetNextIndexFsMIServiceVlanTunnelProtocolTable, FsMIServiceVlanRsvdMacaddressGet, FsMIServiceVlanRsvdMacaddressSet, FsMIServiceVlanRsvdMacaddressTest, FsMIServiceVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIServiceVlanTunnelProtocolTableINDEX, 3, 0, 0, NULL},

{{12,FsMIServiceVlanTunnelMacaddress}, GetNextIndexFsMIServiceVlanTunnelProtocolTable, FsMIServiceVlanTunnelMacaddressGet, FsMIServiceVlanTunnelMacaddressSet, FsMIServiceVlanTunnelMacaddressTest, FsMIServiceVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIServiceVlanTunnelProtocolTableINDEX, 3, 0, 0, NULL},

{{12,FsMIServiceVlanTunnelProtocolStatus}, GetNextIndexFsMIServiceVlanTunnelProtocolTable, FsMIServiceVlanTunnelProtocolStatusGet, FsMIServiceVlanTunnelProtocolStatusSet, FsMIServiceVlanTunnelProtocolStatusTest, FsMIServiceVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIServiceVlanTunnelProtocolTableINDEX, 3, 0, 0, NULL},

{{12,FsMIServiceVlanTunnelPktsRecvd}, GetNextIndexFsMIServiceVlanTunnelProtocolTable, FsMIServiceVlanTunnelPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIServiceVlanTunnelProtocolTableINDEX, 3, 0, 0, NULL},

{{12,FsMIServiceVlanTunnelPktsSent}, GetNextIndexFsMIServiceVlanTunnelProtocolTable, FsMIServiceVlanTunnelPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIServiceVlanTunnelProtocolTableINDEX, 3, 0, 0, NULL},

{{12,FsMIServiceVlanDiscardPktsRx}, GetNextIndexFsMIServiceVlanTunnelProtocolTable, FsMIServiceVlanDiscardPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIServiceVlanTunnelProtocolTableINDEX, 3, 0, 0, NULL},

{{12,FsMIServiceVlanDiscardPktsTx}, GetNextIndexFsMIServiceVlanTunnelProtocolTable, FsMIServiceVlanDiscardPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIServiceVlanTunnelProtocolTableINDEX, 3, 0, 0, NULL},

{{12,FsMIVlanDiscardDot1xPktsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardDot1xPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardDot1xPktsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardDot1xPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardLacpPktsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardLacpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardLacpPktsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardLacpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardStpPDUsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardStpPDUsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardStpPDUsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardStpPDUsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardGvrpPktsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardGvrpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardGvrpPktsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardGvrpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardGmrpPktsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardGmrpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardGmrpPktsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardGmrpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardIgmpPktsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardIgmpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardIgmpPktsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardIgmpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardMvrpPktsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardMvrpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardMvrpPktsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardMvrpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardMmrpPktsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardMmrpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardMmrpPktsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardMmrpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardElmiPktsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardElmiPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardElmiPktsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardElmiPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardLldpPktsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardLldpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardLldpPktsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardLldpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardEcfmPktsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardEcfmPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardEcfmPktsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardEcfmPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardEoamPktsRx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardEoamPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIVlanDiscardEoamPktsTx}, GetNextIndexFsMIVlanDiscardStatsTable, FsMIVlanDiscardEoamPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},
};
tMibData fsmvleEntry = { 87, fsmvleMibEntry };

#endif /* _FSMVLEDB_H */

