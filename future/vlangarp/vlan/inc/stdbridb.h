/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbridb.h,v 1.11 2012/01/21 09:25:45 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDBRIDB_H
#define _STDBRIDB_H

UINT1 Dot1dBasePortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dStpPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dStpExtPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dTpFdbTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot1dTpPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dStaticTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdbri [] ={1,3,6,1,2,1,17};
tSNMP_OID_TYPE stdbriOID = {7, stdbri};


/* Generated OID's for tables */
UINT4 Dot1dBasePortTable [] ={1,3,6,1,2,1,17,1,4};
tSNMP_OID_TYPE Dot1dBasePortTableOID = {9, Dot1dBasePortTable};


UINT4 Dot1dStpPortTable [] ={1,3,6,1,2,1,17,2,15};
tSNMP_OID_TYPE Dot1dStpPortTableOID = {9, Dot1dStpPortTable};


UINT4 Dot1dStpExtPortTable [] ={1,3,6,1,2,1,17,2,19};
tSNMP_OID_TYPE Dot1dStpExtPortTableOID = {9, Dot1dStpExtPortTable};


UINT4 Dot1dTpFdbTable [] ={1,3,6,1,2,1,17,4,3};
tSNMP_OID_TYPE Dot1dTpFdbTableOID = {9, Dot1dTpFdbTable};


UINT4 Dot1dTpPortTable [] ={1,3,6,1,2,1,17,4,4};
tSNMP_OID_TYPE Dot1dTpPortTableOID = {9, Dot1dTpPortTable};


UINT4 Dot1dStaticTable [] ={1,3,6,1,2,1,17,5,1};
tSNMP_OID_TYPE Dot1dStaticTableOID = {9, Dot1dStaticTable};




UINT4 Dot1dBaseBridgeAddress [ ] ={1,3,6,1,2,1,17,1,1};
UINT4 Dot1dBaseNumPorts [ ] ={1,3,6,1,2,1,17,1,2};
UINT4 Dot1dBaseType [ ] ={1,3,6,1,2,1,17,1,3};
UINT4 Dot1dBasePort [ ] ={1,3,6,1,2,1,17,1,4,1,1};
UINT4 Dot1dBasePortIfIndex [ ] ={1,3,6,1,2,1,17,1,4,1,2};
UINT4 Dot1dBasePortCircuit [ ] ={1,3,6,1,2,1,17,1,4,1,3};
UINT4 Dot1dBasePortDelayExceededDiscards [ ] ={1,3,6,1,2,1,17,1,4,1,4};
UINT4 Dot1dBasePortMtuExceededDiscards [ ] ={1,3,6,1,2,1,17,1,4,1,5};
UINT4 Dot1dStpProtocolSpecification [ ] ={1,3,6,1,2,1,17,2,1};
UINT4 Dot1dStpPriority [ ] ={1,3,6,1,2,1,17,2,2};
UINT4 Dot1dStpTimeSinceTopologyChange [ ] ={1,3,6,1,2,1,17,2,3};
UINT4 Dot1dStpTopChanges [ ] ={1,3,6,1,2,1,17,2,4};
UINT4 Dot1dStpDesignatedRoot [ ] ={1,3,6,1,2,1,17,2,5};
UINT4 Dot1dStpRootCost [ ] ={1,3,6,1,2,1,17,2,6};
UINT4 Dot1dStpRootPort [ ] ={1,3,6,1,2,1,17,2,7};
UINT4 Dot1dStpMaxAge [ ] ={1,3,6,1,2,1,17,2,8};
UINT4 Dot1dStpHelloTime [ ] ={1,3,6,1,2,1,17,2,9};
UINT4 Dot1dStpHoldTime [ ] ={1,3,6,1,2,1,17,2,10};
UINT4 Dot1dStpForwardDelay [ ] ={1,3,6,1,2,1,17,2,11};
UINT4 Dot1dStpBridgeMaxAge [ ] ={1,3,6,1,2,1,17,2,12};
UINT4 Dot1dStpBridgeHelloTime [ ] ={1,3,6,1,2,1,17,2,13};
UINT4 Dot1dStpBridgeForwardDelay [ ] ={1,3,6,1,2,1,17,2,14};
UINT4 Dot1dStpPort [ ] ={1,3,6,1,2,1,17,2,15,1,1};
UINT4 Dot1dStpPortPriority [ ] ={1,3,6,1,2,1,17,2,15,1,2};
UINT4 Dot1dStpPortState [ ] ={1,3,6,1,2,1,17,2,15,1,3};
UINT4 Dot1dStpPortEnable [ ] ={1,3,6,1,2,1,17,2,15,1,4};
UINT4 Dot1dStpPortPathCost [ ] ={1,3,6,1,2,1,17,2,15,1,5};
UINT4 Dot1dStpPortDesignatedRoot [ ] ={1,3,6,1,2,1,17,2,15,1,6};
UINT4 Dot1dStpPortDesignatedCost [ ] ={1,3,6,1,2,1,17,2,15,1,7};
UINT4 Dot1dStpPortDesignatedBridge [ ] ={1,3,6,1,2,1,17,2,15,1,8};
UINT4 Dot1dStpPortDesignatedPort [ ] ={1,3,6,1,2,1,17,2,15,1,9};
UINT4 Dot1dStpPortForwardTransitions [ ] ={1,3,6,1,2,1,17,2,15,1,10};
UINT4 Dot1dStpPortPathCost32 [ ] ={1,3,6,1,2,1,17,2,15,1,11};
UINT4 Dot1dStpVersion [ ] ={1,3,6,1,2,1,17,2,16};
UINT4 Dot1dStpTxHoldCount [ ] ={1,3,6,1,2,1,17,2,17};
UINT4 Dot1dStpPortProtocolMigration [ ] ={1,3,6,1,2,1,17,2,19,1,1};
UINT4 Dot1dStpPortAdminEdgePort [ ] ={1,3,6,1,2,1,17,2,19,1,2};
UINT4 Dot1dStpPortOperEdgePort [ ] ={1,3,6,1,2,1,17,2,19,1,3};
UINT4 Dot1dStpPortAdminPointToPoint [ ] ={1,3,6,1,2,1,17,2,19,1,4};
UINT4 Dot1dStpPortOperPointToPoint [ ] ={1,3,6,1,2,1,17,2,19,1,5};
UINT4 Dot1dStpPortAdminPathCost [ ] ={1,3,6,1,2,1,17,2,19,1,6};
UINT4 Dot1dTpLearnedEntryDiscards [ ] ={1,3,6,1,2,1,17,4,1};
UINT4 Dot1dTpAgingTime [ ] ={1,3,6,1,2,1,17,4,2};
UINT4 Dot1dTpFdbAddress [ ] ={1,3,6,1,2,1,17,4,3,1,1};
UINT4 Dot1dTpFdbPort [ ] ={1,3,6,1,2,1,17,4,3,1,2};
UINT4 Dot1dTpFdbStatus [ ] ={1,3,6,1,2,1,17,4,3,1,3};
UINT4 Dot1dTpPort [ ] ={1,3,6,1,2,1,17,4,4,1,1};
UINT4 Dot1dTpPortMaxInfo [ ] ={1,3,6,1,2,1,17,4,4,1,2};
UINT4 Dot1dTpPortInFrames [ ] ={1,3,6,1,2,1,17,4,4,1,3};
UINT4 Dot1dTpPortOutFrames [ ] ={1,3,6,1,2,1,17,4,4,1,4};
UINT4 Dot1dTpPortInDiscards [ ] ={1,3,6,1,2,1,17,4,4,1,5};
UINT4 Dot1dStaticAddress [ ] ={1,3,6,1,2,1,17,5,1,1,1};
UINT4 Dot1dStaticReceivePort [ ] ={1,3,6,1,2,1,17,5,1,1,2};
UINT4 Dot1dStaticAllowedToGoTo [ ] ={1,3,6,1,2,1,17,5,1,1,3};
UINT4 Dot1dStaticStatus [ ] ={1,3,6,1,2,1,17,5,1,1,4};


tSNMP_OID_TYPE Dot1dBaseBridgeAddressOID = {9, Dot1dBaseBridgeAddress};


tSNMP_OID_TYPE Dot1dBaseNumPortsOID = {9, Dot1dBaseNumPorts};


tSNMP_OID_TYPE Dot1dBaseTypeOID = {9, Dot1dBaseType};


tSNMP_OID_TYPE Dot1dStpProtocolSpecificationOID = {9, Dot1dStpProtocolSpecification};


tSNMP_OID_TYPE Dot1dStpPriorityOID = {9, Dot1dStpPriority};


tSNMP_OID_TYPE Dot1dStpTimeSinceTopologyChangeOID = {9, Dot1dStpTimeSinceTopologyChange};


tSNMP_OID_TYPE Dot1dStpTopChangesOID = {9, Dot1dStpTopChanges};


tSNMP_OID_TYPE Dot1dStpDesignatedRootOID = {9, Dot1dStpDesignatedRoot};


tSNMP_OID_TYPE Dot1dStpRootCostOID = {9, Dot1dStpRootCost};


tSNMP_OID_TYPE Dot1dStpRootPortOID = {9, Dot1dStpRootPort};


tSNMP_OID_TYPE Dot1dStpMaxAgeOID = {9, Dot1dStpMaxAge};


tSNMP_OID_TYPE Dot1dStpHelloTimeOID = {9, Dot1dStpHelloTime};


tSNMP_OID_TYPE Dot1dStpHoldTimeOID = {9, Dot1dStpHoldTime};


tSNMP_OID_TYPE Dot1dStpForwardDelayOID = {9, Dot1dStpForwardDelay};


tSNMP_OID_TYPE Dot1dStpBridgeMaxAgeOID = {9, Dot1dStpBridgeMaxAge};


tSNMP_OID_TYPE Dot1dStpBridgeHelloTimeOID = {9, Dot1dStpBridgeHelloTime};


tSNMP_OID_TYPE Dot1dStpBridgeForwardDelayOID = {9, Dot1dStpBridgeForwardDelay};


tSNMP_OID_TYPE Dot1dStpVersionOID = {9, Dot1dStpVersion};


tSNMP_OID_TYPE Dot1dStpTxHoldCountOID = {9, Dot1dStpTxHoldCount};


tSNMP_OID_TYPE Dot1dTpLearnedEntryDiscardsOID = {9, Dot1dTpLearnedEntryDiscards};


tSNMP_OID_TYPE Dot1dTpAgingTimeOID = {9, Dot1dTpAgingTime};




tMbDbEntry Dot1dBaseBridgeAddressMibEntry[]= {

{{9,Dot1dBaseBridgeAddress}, NULL, Dot1dBaseBridgeAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dBaseBridgeAddressEntry = { 1, Dot1dBaseBridgeAddressMibEntry };

tMbDbEntry Dot1dBaseNumPortsMibEntry[]= {

{{9,Dot1dBaseNumPorts}, NULL, Dot1dBaseNumPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dBaseNumPortsEntry = { 1, Dot1dBaseNumPortsMibEntry };

tMbDbEntry Dot1dBaseTypeMibEntry[]= {

{{9,Dot1dBaseType}, NULL, Dot1dBaseTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dBaseTypeEntry = { 1, Dot1dBaseTypeMibEntry };

tMbDbEntry Dot1dBasePortTableMibEntry[]= {

{{11,Dot1dBasePort}, GetNextIndexDot1dBasePortTable, Dot1dBasePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dBasePortIfIndex}, GetNextIndexDot1dBasePortTable, Dot1dBasePortIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dBasePortCircuit}, GetNextIndexDot1dBasePortTable, Dot1dBasePortCircuitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, Dot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dBasePortDelayExceededDiscards}, GetNextIndexDot1dBasePortTable, Dot1dBasePortDelayExceededDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dBasePortMtuExceededDiscards}, GetNextIndexDot1dBasePortTable, Dot1dBasePortMtuExceededDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dBasePortTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot1dBasePortTableEntry = { 5, Dot1dBasePortTableMibEntry };

tMbDbEntry Dot1dStpProtocolSpecificationMibEntry[]= {

{{9,Dot1dStpProtocolSpecification}, NULL, Dot1dStpProtocolSpecificationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpProtocolSpecificationEntry = { 1, Dot1dStpProtocolSpecificationMibEntry };

tMbDbEntry Dot1dStpPriorityMibEntry[]= {

{{9,Dot1dStpPriority}, NULL, Dot1dStpPriorityGet, Dot1dStpPrioritySet, Dot1dStpPriorityTest, Dot1dStpPriorityDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpPriorityEntry = { 1, Dot1dStpPriorityMibEntry };

tMbDbEntry Dot1dStpTimeSinceTopologyChangeMibEntry[]= {

{{9,Dot1dStpTimeSinceTopologyChange}, NULL, Dot1dStpTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpTimeSinceTopologyChangeEntry = { 1, Dot1dStpTimeSinceTopologyChangeMibEntry };

tMbDbEntry Dot1dStpTopChangesMibEntry[]= {

{{9,Dot1dStpTopChanges}, NULL, Dot1dStpTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpTopChangesEntry = { 1, Dot1dStpTopChangesMibEntry };

tMbDbEntry Dot1dStpDesignatedRootMibEntry[]= {

{{9,Dot1dStpDesignatedRoot}, NULL, Dot1dStpDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpDesignatedRootEntry = { 1, Dot1dStpDesignatedRootMibEntry };

tMbDbEntry Dot1dStpRootCostMibEntry[]= {

{{9,Dot1dStpRootCost}, NULL, Dot1dStpRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpRootCostEntry = { 1, Dot1dStpRootCostMibEntry };

tMbDbEntry Dot1dStpRootPortMibEntry[]= {

{{9,Dot1dStpRootPort}, NULL, Dot1dStpRootPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpRootPortEntry = { 1, Dot1dStpRootPortMibEntry };

tMbDbEntry Dot1dStpMaxAgeMibEntry[]= {

{{9,Dot1dStpMaxAge}, NULL, Dot1dStpMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpMaxAgeEntry = { 1, Dot1dStpMaxAgeMibEntry };

tMbDbEntry Dot1dStpHelloTimeMibEntry[]= {

{{9,Dot1dStpHelloTime}, NULL, Dot1dStpHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpHelloTimeEntry = { 1, Dot1dStpHelloTimeMibEntry };

tMbDbEntry Dot1dStpHoldTimeMibEntry[]= {

{{9,Dot1dStpHoldTime}, NULL, Dot1dStpHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpHoldTimeEntry = { 1, Dot1dStpHoldTimeMibEntry };

tMbDbEntry Dot1dStpForwardDelayMibEntry[]= {

{{9,Dot1dStpForwardDelay}, NULL, Dot1dStpForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpForwardDelayEntry = { 1, Dot1dStpForwardDelayMibEntry };

tMbDbEntry Dot1dStpBridgeMaxAgeMibEntry[]= {

{{9,Dot1dStpBridgeMaxAge}, NULL, Dot1dStpBridgeMaxAgeGet, Dot1dStpBridgeMaxAgeSet, Dot1dStpBridgeMaxAgeTest, Dot1dStpBridgeMaxAgeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpBridgeMaxAgeEntry = { 1, Dot1dStpBridgeMaxAgeMibEntry };

tMbDbEntry Dot1dStpBridgeHelloTimeMibEntry[]= {

{{9,Dot1dStpBridgeHelloTime}, NULL, Dot1dStpBridgeHelloTimeGet, Dot1dStpBridgeHelloTimeSet, Dot1dStpBridgeHelloTimeTest, Dot1dStpBridgeHelloTimeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpBridgeHelloTimeEntry = { 1, Dot1dStpBridgeHelloTimeMibEntry };

tMbDbEntry Dot1dStpBridgeForwardDelayMibEntry[]= {

{{9,Dot1dStpBridgeForwardDelay}, NULL, Dot1dStpBridgeForwardDelayGet, Dot1dStpBridgeForwardDelaySet, Dot1dStpBridgeForwardDelayTest, Dot1dStpBridgeForwardDelayDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dStpBridgeForwardDelayEntry = { 1, Dot1dStpBridgeForwardDelayMibEntry };

tMbDbEntry Dot1dStpPortTableMibEntry[]= {

{{11,Dot1dStpPort}, GetNextIndexDot1dStpPortTable, Dot1dStpPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortPriority}, GetNextIndexDot1dStpPortTable, Dot1dStpPortPriorityGet, Dot1dStpPortPrioritySet, Dot1dStpPortPriorityTest, Dot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortState}, GetNextIndexDot1dStpPortTable, Dot1dStpPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortEnable}, GetNextIndexDot1dStpPortTable, Dot1dStpPortEnableGet, Dot1dStpPortEnableSet, Dot1dStpPortEnableTest, Dot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortPathCost}, GetNextIndexDot1dStpPortTable, Dot1dStpPortPathCostGet, Dot1dStpPortPathCostSet, Dot1dStpPortPathCostTest, Dot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortDesignatedRoot}, GetNextIndexDot1dStpPortTable, Dot1dStpPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortDesignatedCost}, GetNextIndexDot1dStpPortTable, Dot1dStpPortDesignatedCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortDesignatedBridge}, GetNextIndexDot1dStpPortTable, Dot1dStpPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortDesignatedPort}, GetNextIndexDot1dStpPortTable, Dot1dStpPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortForwardTransitions}, GetNextIndexDot1dStpPortTable, Dot1dStpPortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortPathCost32}, GetNextIndexDot1dStpPortTable, Dot1dStpPortPathCost32Get, Dot1dStpPortPathCost32Set, Dot1dStpPortPathCost32Test, Dot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot1dStpPortTableEntry = { 11, Dot1dStpPortTableMibEntry };

tMbDbEntry Dot1dStpVersionMibEntry[]= {

{{9,Dot1dStpVersion}, NULL, Dot1dStpVersionGet, Dot1dStpVersionSet, Dot1dStpVersionTest, Dot1dStpVersionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData Dot1dStpVersionEntry = { 1, Dot1dStpVersionMibEntry };

tMbDbEntry Dot1dStpTxHoldCountMibEntry[]= {

{{9,Dot1dStpTxHoldCount}, NULL, Dot1dStpTxHoldCountGet, Dot1dStpTxHoldCountSet, Dot1dStpTxHoldCountTest, Dot1dStpTxHoldCountDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},
};
tMibData Dot1dStpTxHoldCountEntry = { 1, Dot1dStpTxHoldCountMibEntry };

tMbDbEntry Dot1dStpExtPortTableMibEntry[]= {

{{11,Dot1dStpPortProtocolMigration}, GetNextIndexDot1dStpExtPortTable, Dot1dStpPortProtocolMigrationGet, Dot1dStpPortProtocolMigrationSet, Dot1dStpPortProtocolMigrationTest, Dot1dStpExtPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dStpExtPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortAdminEdgePort}, GetNextIndexDot1dStpExtPortTable, Dot1dStpPortAdminEdgePortGet, Dot1dStpPortAdminEdgePortSet, Dot1dStpPortAdminEdgePortTest, Dot1dStpExtPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dStpExtPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortOperEdgePort}, GetNextIndexDot1dStpExtPortTable, Dot1dStpPortOperEdgePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1dStpExtPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortAdminPointToPoint}, GetNextIndexDot1dStpExtPortTable, Dot1dStpPortAdminPointToPointGet, Dot1dStpPortAdminPointToPointSet, Dot1dStpPortAdminPointToPointTest, Dot1dStpExtPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dStpExtPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortOperPointToPoint}, GetNextIndexDot1dStpExtPortTable, Dot1dStpPortOperPointToPointGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1dStpExtPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortAdminPathCost}, GetNextIndexDot1dStpExtPortTable, Dot1dStpPortAdminPathCostGet, Dot1dStpPortAdminPathCostSet, Dot1dStpPortAdminPathCostTest, Dot1dStpExtPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dStpExtPortTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot1dStpExtPortTableEntry = { 6, Dot1dStpExtPortTableMibEntry };

tMbDbEntry Dot1dTpLearnedEntryDiscardsMibEntry[]= {

{{9,Dot1dTpLearnedEntryDiscards}, NULL, Dot1dTpLearnedEntryDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dTpLearnedEntryDiscardsEntry = { 1, Dot1dTpLearnedEntryDiscardsMibEntry };

tMbDbEntry Dot1dTpAgingTimeMibEntry[]= {

{{9,Dot1dTpAgingTime}, NULL, Dot1dTpAgingTimeGet, Dot1dTpAgingTimeSet, Dot1dTpAgingTimeTest, Dot1dTpAgingTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData Dot1dTpAgingTimeEntry = { 1, Dot1dTpAgingTimeMibEntry };

tMbDbEntry Dot1dTpFdbTableMibEntry[]= {

{{11,Dot1dTpFdbAddress}, GetNextIndexDot1dTpFdbTable, Dot1dTpFdbAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1dTpFdbTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpFdbPort}, GetNextIndexDot1dTpFdbTable, Dot1dTpFdbPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dTpFdbTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpFdbStatus}, GetNextIndexDot1dTpFdbTable, Dot1dTpFdbStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1dTpFdbTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot1dTpFdbTableEntry = { 3, Dot1dTpFdbTableMibEntry };

tMbDbEntry Dot1dTpPortTableMibEntry[]= {

{{11,Dot1dTpPort}, GetNextIndexDot1dTpPortTable, Dot1dTpPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPortMaxInfo}, GetNextIndexDot1dTpPortTable, Dot1dTpPortMaxInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPortInFrames}, GetNextIndexDot1dTpPortTable, Dot1dTpPortInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPortOutFrames}, GetNextIndexDot1dTpPortTable, Dot1dTpPortOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPortInDiscards}, GetNextIndexDot1dTpPortTable, Dot1dTpPortInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dTpPortTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot1dTpPortTableEntry = { 5, Dot1dTpPortTableMibEntry };

tMbDbEntry Dot1dStaticTableMibEntry[]= {

{{11,Dot1dStaticAddress}, GetNextIndexDot1dStaticTable, Dot1dStaticAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1dStaticTableINDEX, 2, 0, 0, NULL},

{{11,Dot1dStaticReceivePort}, GetNextIndexDot1dStaticTable, Dot1dStaticReceivePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dStaticTableINDEX, 2, 0, 0, NULL},

{{11,Dot1dStaticAllowedToGoTo}, GetNextIndexDot1dStaticTable, Dot1dStaticAllowedToGoToGet, Dot1dStaticAllowedToGoToSet, Dot1dStaticAllowedToGoToTest, Dot1dStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1dStaticTableINDEX, 2, 0, 0, NULL},

{{11,Dot1dStaticStatus}, GetNextIndexDot1dStaticTable, Dot1dStaticStatusGet, Dot1dStaticStatusSet, Dot1dStaticStatusTest, Dot1dStaticTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dStaticTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot1dStaticTableEntry = { 4, Dot1dStaticTableMibEntry };

#endif /* _STDBRIDB_H */

