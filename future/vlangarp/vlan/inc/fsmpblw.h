/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpblw.h,v 1.10 2016/01/11 12:56:20 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIPbContextInfoTable. */
INT1
nmhValidateIndexInstanceFsMIPbContextInfoTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbContextInfoTable  */

INT1
nmhGetFirstIndexFsMIPbContextInfoTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbContextInfoTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbMulticastMacLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelStpAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIPbTunnelLacpAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIPbTunnelDot1xAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIPbTunnelGvrpAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIPbTunnelGmrpAddress ARG_LIST((INT4 ,tMacAddr * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbMulticastMacLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIPbTunnelStpAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIPbTunnelLacpAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIPbTunnelDot1xAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIPbTunnelGvrpAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIPbTunnelGmrpAddress ARG_LIST((INT4  ,tMacAddr ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbMulticastMacLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIPbTunnelStpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIPbTunnelLacpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIPbTunnelDot1xAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIPbTunnelGvrpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIPbTunnelGmrpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbContextInfoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbPortInfoTable. */
INT1
nmhValidateIndexInstanceFsMIPbPortInfoTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbPortInfoTable  */

INT1
nmhGetFirstIndexFsMIPbPortInfoTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbPortInfoTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbPortSVlanClassificationMethod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbPortSVlanIngressEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbPortSVlanEgressEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbPortSVlanEtherTypeSwapStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbPortSVlanTranslationStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbPortUnicastMacLearning ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbPortUnicastMacLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbPortBundleStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbPortMultiplexStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbPortSVlanClassificationMethod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbPortSVlanIngressEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbPortSVlanEgressEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbPortSVlanEtherTypeSwapStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbPortSVlanTranslationStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbPortUnicastMacLearning ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbPortUnicastMacLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIPbPortBundleStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbPortMultiplexStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbPortSVlanClassificationMethod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbPortSVlanIngressEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbPortSVlanEgressEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbPortSVlanEtherTypeSwapStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbPortSVlanTranslationStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbPortUnicastMacLearning ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbPortUnicastMacLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIPbPortBundleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbPortMultiplexStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbPortInfoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbSrcMacSVlanTable. */
INT1
nmhValidateIndexInstanceFsMIPbSrcMacSVlanTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbSrcMacSVlanTable  */

INT1
nmhGetFirstIndexFsMIPbSrcMacSVlanTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbSrcMacSVlanTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbSrcMacSVlan ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIPbSrcMacRowStatus ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbSrcMacSVlan ARG_LIST((INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsMIPbSrcMacRowStatus ARG_LIST((INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbSrcMacSVlan ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsMIPbSrcMacRowStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbSrcMacSVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbDstMacSVlanTable. */
INT1
nmhValidateIndexInstanceFsMIPbDstMacSVlanTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbDstMacSVlanTable  */

INT1
nmhGetFirstIndexFsMIPbDstMacSVlanTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbDstMacSVlanTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbDstMacSVlan ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIPbDstMacRowStatus ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbDstMacSVlan ARG_LIST((INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsMIPbDstMacRowStatus ARG_LIST((INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbDstMacSVlan ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsMIPbDstMacRowStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbDstMacSVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbCVlanSrcMacSVlanTable. */
INT1
nmhValidateIndexInstanceFsMIPbCVlanSrcMacSVlanTable ARG_LIST((INT4  , INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbCVlanSrcMacSVlanTable  */

INT1
nmhGetFirstIndexFsMIPbCVlanSrcMacSVlanTable ARG_LIST((INT4 * , INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbCVlanSrcMacSVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbCVlanSrcMacSVlan ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIPbCVlanSrcMacRowStatus ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbCVlanSrcMacSVlan ARG_LIST((INT4  , INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsMIPbCVlanSrcMacRowStatus ARG_LIST((INT4  , INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbCVlanSrcMacSVlan ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsMIPbCVlanSrcMacRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbCVlanSrcMacSVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbCVlanDstMacSVlanTable. */
INT1
nmhValidateIndexInstanceFsMIPbCVlanDstMacSVlanTable ARG_LIST((INT4  , INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbCVlanDstMacSVlanTable  */

INT1
nmhGetFirstIndexFsMIPbCVlanDstMacSVlanTable ARG_LIST((INT4 * , INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbCVlanDstMacSVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbCVlanDstMacSVlan ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIPbCVlanDstMacRowStatus ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbCVlanDstMacSVlan ARG_LIST((INT4  , INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsMIPbCVlanDstMacRowStatus ARG_LIST((INT4  , INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbCVlanDstMacSVlan ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsMIPbCVlanDstMacRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbCVlanDstMacSVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbDscpSVlanTable. */
INT1
nmhValidateIndexInstanceFsMIPbDscpSVlanTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbDscpSVlanTable  */

INT1
nmhGetFirstIndexFsMIPbDscpSVlanTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbDscpSVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbDscpSVlan ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbDscpRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbDscpSVlan ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPbDscpRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbDscpSVlan ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbDscpRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbDscpSVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbCVlanDscpSVlanTable. */
INT1
nmhValidateIndexInstanceFsMIPbCVlanDscpSVlanTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbCVlanDscpSVlanTable  */

INT1
nmhGetFirstIndexFsMIPbCVlanDscpSVlanTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbCVlanDscpSVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbCVlanDscpSVlan ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbCVlanDscpRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbCVlanDscpSVlan ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPbCVlanDscpRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbCVlanDscpSVlan ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbCVlanDscpRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbCVlanDscpSVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbSrcIpAddrSVlanTable. */
INT1
nmhValidateIndexInstanceFsMIPbSrcIpAddrSVlanTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbSrcIpAddrSVlanTable  */

INT1
nmhGetFirstIndexFsMIPbSrcIpAddrSVlanTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbSrcIpAddrSVlanTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbSrcIpSVlan ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIPbSrcIpRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbSrcIpSVlan ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIPbSrcIpRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbSrcIpSVlan ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIPbSrcIpRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbSrcIpAddrSVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbDstIpAddrSVlanTable. */
INT1
nmhValidateIndexInstanceFsMIPbDstIpAddrSVlanTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbDstIpAddrSVlanTable  */

INT1
nmhGetFirstIndexFsMIPbDstIpAddrSVlanTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbDstIpAddrSVlanTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbDstIpSVlan ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIPbDstIpRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbDstIpSVlan ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIPbDstIpRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbDstIpSVlan ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIPbDstIpRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbDstIpAddrSVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbSrcDstIpSVlanTable. */
INT1
nmhValidateIndexInstanceFsMIPbSrcDstIpSVlanTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbSrcDstIpSVlanTable  */

INT1
nmhGetFirstIndexFsMIPbSrcDstIpSVlanTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbSrcDstIpSVlanTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbSrcDstIpSVlan ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIPbSrcDstIpRowStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbSrcDstIpSVlan ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIPbSrcDstIpRowStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbSrcDstIpSVlan ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIPbSrcDstIpRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbSrcDstIpSVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbCVlanDstIpSVlanTable. */
INT1
nmhValidateIndexInstanceFsMIPbCVlanDstIpSVlanTable ARG_LIST((INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbCVlanDstIpSVlanTable  */

INT1
nmhGetFirstIndexFsMIPbCVlanDstIpSVlanTable ARG_LIST((INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbCVlanDstIpSVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbCVlanDstIpSVlan ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIPbCVlanDstIpRowStatus ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbCVlanDstIpSVlan ARG_LIST((INT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIPbCVlanDstIpRowStatus ARG_LIST((INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbCVlanDstIpSVlan ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIPbCVlanDstIpRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbCVlanDstIpSVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbPortBasedCVlanTable. */
INT1
nmhValidateIndexInstanceFsMIPbPortBasedCVlanTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbPortBasedCVlanTable  */

INT1
nmhGetFirstIndexFsMIPbPortBasedCVlanTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbPortBasedCVlanTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbPortCVlan ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbPortCVlanClassifyStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbPortEgressUntaggedStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbPortCVlan ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbPortCVlanClassifyStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbPortEgressUntaggedStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbPortCVlan ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbPortCVlanClassifyStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbPortEgressUntaggedStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbPortBasedCVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbEtherTypeSwapTable. */
INT1
nmhValidateIndexInstanceFsMIPbEtherTypeSwapTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbEtherTypeSwapTable  */

INT1
nmhGetFirstIndexFsMIPbEtherTypeSwapTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbEtherTypeSwapTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbRelayEtherType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIPbEtherTypeSwapRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbRelayEtherType ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIPbEtherTypeSwapRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbRelayEtherType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbEtherTypeSwapRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbEtherTypeSwapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbSVlanConfigTable. */
INT1
nmhValidateIndexInstanceFsMIPbSVlanConfigTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbSVlanConfigTable  */

INT1
nmhGetFirstIndexFsMIPbSVlanConfigTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbSVlanConfigTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbSVlanConfigServiceType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbSVlanConfigServiceType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbSVlanConfigServiceType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbSVlanConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbTunnelProtocolTable. */
INT1
nmhValidateIndexInstanceFsMIPbTunnelProtocolTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbTunnelProtocolTable  */

INT1
nmhGetFirstIndexFsMIPbTunnelProtocolTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbTunnelProtocolTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbTunnelProtocolDot1x ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbTunnelProtocolLacp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbTunnelProtocolStp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbTunnelProtocolGvrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbTunnelProtocolGmrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPbTunnelProtocolIgmp ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbTunnelProtocolDot1x ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbTunnelProtocolLacp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbTunnelProtocolStp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbTunnelProtocolGvrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbTunnelProtocolGmrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPbTunnelProtocolIgmp ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbTunnelProtocolDot1x ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbTunnelProtocolLacp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbTunnelProtocolStp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbTunnelProtocolGvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbTunnelProtocolGmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPbTunnelProtocolIgmp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbTunnelProtocolTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbTunnelProtocolStatsTable. */
INT1
nmhValidateIndexInstanceFsMIPbTunnelProtocolStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbTunnelProtocolStatsTable  */

INT1
nmhGetFirstIndexFsMIPbTunnelProtocolStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbTunnelProtocolStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbTunnelProtocolDot1xPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelProtocolDot1xPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelProtocolLacpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelProtocolLacpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelProtocolStpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelProtocolStpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelProtocolGvrpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelProtocolGvrpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelProtocolGmrpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelProtocolGmrpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelProtocolIgmpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPbTunnelProtocolIgmpPktsSent ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIPbPepExtTable. */
INT1
nmhValidateIndexInstanceFsMIPbPepExtTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbPepExtTable  */

INT1
nmhGetFirstIndexFsMIPbPepExtTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbPepExtTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbPepExtCosPreservation ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbPepExtCosPreservation ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbPepExtCosPreservation ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbPepExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIPbPortCVlanCounterTable. */
INT1
nmhValidateIndexInstanceFsMIPbPortCVlanCounterTable ARG_LIST((INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPbPortCVlanCounterTable  */

INT1
nmhGetFirstIndexFsMIPbPortCVlanCounterTable ARG_LIST((INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPbPortCVlanCounterTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPbPortCVlanCounterRxUcast ARG_LIST((INT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIPbPortCVlanCounterRxFrames ARG_LIST((INT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIPbPortCVlanCounterRxBytes ARG_LIST((INT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIPbPortCVlanCounterStatus ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIPbPortCVlanClearCounter ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPbPortCVlanCounterStatus ARG_LIST((INT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIPbPortCVlanClearCounter ARG_LIST((INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPbPortCVlanCounterStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIPbPortCVlanClearCounter ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPbPortCVlanCounterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


