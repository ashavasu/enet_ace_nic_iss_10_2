/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2004-2005                                          */
/* $Id: vlanglob.h,v 1.45 2016/07/07 14:17:25 siva Exp $                    */
/*  FILE NAME             : vlanglob.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN                                             */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 20 Mar 2005                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains constant definitions used in  */
/*                          VLAN Module.                                     */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 1.0                                              */
/*  Date(DD/MM/YYYY)      :                                                  */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#ifndef _VLANGLOB_H
#define _VLANGLOB_H
/*-------------- GLOBAL DECLARATIONS -------------------------------*/
tVlanContextInfo   *gapVlanContextInfo [SYS_DEF_MAX_NUM_CONTEXTS];
tVlanContextInfo *gpVlanContextInfo = NULL;
tRBTree  gVlanPortTable; /* IfIndex based port RBTree  (tVlanPortEntry)*/
tVLAN_SLL      gVlanTempPortList; /* Temp Portlist maintained by MI to updated 
                                     the portlist. */
UINT4          gu4VlanGlobalTrace = VLAN_DISABLED;
UINT4        gu4BaseBridgeMode = DOT_1Q_VLAN_MODE;
#ifdef MPLS_WANTED
tRBTree        gpVlanL2VpnPortMapTbl;
tRBTree        gpVlanL2VpnPortVlanMapTbl;
#endif /*MPLS_WANTED*/

tVlanRegTbl    gaVlanRegTbl[VLAN_MAX_REG_MODULES];

UINT1 gau1VlanShutDownStatus[SYS_DEF_MAX_NUM_CONTEXTS];
UINT1 gau1VlanStatus[SYS_DEF_MAX_NUM_CONTEXTS];
UINT1 gua1VlanDupBuff[VLAN_DEFAULT_PKT_SIZE];
#ifdef SYSLOG_WANTED
INT4 gi4VlanSysLogId;
#endif

#ifndef NPAPI_WANTED
UINT1 gu1SwStatsEnabled = VLAN_SNMP_TRUE;
#else
UINT1 gu1SwStatsEnabled = VLAN_SNMP_FALSE;
#endif

UINT4 gu4VlanPlatformLearningMode = VLAN_HW_UPDATED_CPU_LEARNING;
UINT4 gu4SubnetGlobalOption = VLAN_INIT_VAL;

/* GLOBAL Declarations commmon to all context  */
tVlanTaskInfo       gVlanTaskInfo;
/* PRIORITY to Traffic class mapping table */
UINT1             gau1PriTrfClassMap[VLAN_MAX_PRIORITY][VLAN_MAX_PRIORITY] =
    { {0, 0, 0, 1, 1, 1, 1, 2},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 1},
{0, 0, 0, 1, 1, 2, 2, 3},
{0, 1, 1, 2, 2, 3, 3, 4},
{0, 1, 1, 2, 3, 4, 4, 5},
{0, 1, 2, 3, 4, 5, 5, 6},
{0, 1, 2, 3, 4, 5, 6, 7}
};
UINT2 gu2VlanMacMapCount = 0;
UINT2 gu2VlanSubnetMapCount = 0;

UINT1 gau1PbDefPortProtoTunnelStatus[VLAN_MAX_PB_PORT_TYPES][VLAN_L2_PROTOCOLS] =
/*Protocol order*/
/*DOT1X LACP STP GVRP GMRP IGMP MVRP MMRP EOAM ELMI LLDP ECFM */
{ {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},           /*VLAN_PROVIDER_NETWORK_PORT*/
  {1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1},    /*VLAN_CNP_PORTBASED_PORT*/
  {1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1},    /*VLAN_CNP_TAGGED_PORT*/
  {1, 1, 1, 3, 3, 2, 3, 3, 1, 1, 1, 1},    /*VLAN_CUSTOMER_EDGE_PORT*/
  {1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1},    /*VLAN_PROP_CUSTOMER_EDGE_PORT*/
  {1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1},    /*VLAN_PROP_CUSTOMER_NETWORK_PORT*/
  {1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1},    /*VLAN_PROP_PROVIDER_NETWORK_PORT*/
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},    /*VLAN_CUSTOMER_BRIDGE_PORT*/
};

UINT1               gau1PortBitMaskMap[VLAN_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02};

tMacAddr            gNullMacAddress = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
tMacAddr            gMmrpAddress = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x20 };
tMacAddr            gGmrpAddress = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x20 };
tMacAddr            gGvrpAddress = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x21 };
tMacAddr            gReservedAddress = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0F };
tMacAddr            gMulticastAddress = { 0x01, 0x00, 0x5E, 0x00, 0x00, 0x00 };
tMacAddr            gVlanVrrpIpv6MCastAddress = { 0x33, 0x33, 0x00, 0x00, 0x00, 0x12 };
tMacAddr            gMvrpAddress  = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x21 };
tMacAddr            gEoamAddress  = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x02 };
tMacAddr            gBcastAddress = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
tLocalPortList           gNullPortList;
tLocalPortList      gVlanLocalPortList;
tPortList           gNullIfPortList;
tMacAddr            gVlanProviderStpAddr = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd0 };
tMacAddr       gVlanProviderGvrpAddr =    { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd1};
tMacAddr            gVlanProviderGmrpAddr = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd2 };
tMacAddr            gVlanProviderMvrpAddr =    { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd5};
tMacAddr            gVlanProviderMmrpAddr =    { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd6};
tMacAddr            gVlanProviderDot1xAddr = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd3 };
tMacAddr            gVlanProviderLacpAddr = { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd4 };
tMacAddr            gVlanProviderElmiAddr =    { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd7};
tMacAddr            gVlanProviderLldpAddr =    { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd8};
tMacAddr            gVlanProviderEcfmAddr =    { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xd9};
tMacAddr            gVlanProviderEoamAddr =    { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xda};
tMacAddr            gVlanProviderIgmpAddr =    { 0x01, 0x00, 0x0c, 0xcd, 0xcd, 0xdb};

/*  gu1IsVlanInitialised  - Indicates whether VLAN module is initialised or not
 * during boot time. This is to avoid posting messages to VLAN module before
 * it is initialised. */
UINT1               gu1IsVlanInitialised = VLAN_FALSE;
tVlanNodeStatus     gVlanNodeStatus = VLAN_NODE_IDLE;    /* Should not be kept 
                                                            inside gVlanInfo */
/* Sizing Params Structure. */
tFsModSizingParams gFsVlanSizingParams [] =
{
    {"tVlanQMsg", "VLAN_CFG_Q_DEPTH", sizeof (tVlanQMsg),
     VLAN_CFG_Q_DEPTH, VLAN_CFG_Q_DEPTH, 0},

    {"tVlanQInPkt", "VLAN_Q_DEPTH", sizeof (tVlanQInPkt),
     VLAN_Q_DEPTH, VLAN_Q_DEPTH, 0},

    {"tVlanFidEntry", "VLAN_DEV_MAX_NUM_VLAN", sizeof (tVlanFidEntry),
     VLAN_DEV_MAX_NUM_VLAN, VLAN_DEV_MAX_NUM_VLAN, 0},

    {"tVlanPortEntry", "VLAN_MAX_PORTS_IN_SYSTEM", sizeof (tVlanPortEntry),
     VLAN_MAX_PORTS_IN_SYSTEM, VLAN_MAX_PORTS_IN_SYSTEM, 0 },

    {"tVlanCurrEntry", "VLAN_DEV_MAX_NUM_VLAN", sizeof (tVlanCurrEntry),
     VLAN_DEV_MAX_NUM_VLAN, VLAN_DEV_MAX_NUM_VLAN, 0},

    {"tVlanFdbEntry", "VLAN_MAX_FDB_ENTRIES", sizeof (tVlanFdbEntry),
     VLAN_MAX_FDB_ENTRIES, VLAN_MAX_FDB_ENTRIES, 0},

#ifdef SW_LEARNING
    {"tVlanFdbInfo", "VLAN_MAX_FDB_ENTRIES", sizeof (tVlanFdbInfo),
     VLAN_MAX_FDB_ENTRIES, VLAN_MAX_FDB_ENTRIES, 0},
#endif

    {"tVlanStUcastEntry", "VLAN_MAX_ST_UCAST_ENTRIES", sizeof (tVlanStUcastEntry),
     VLAN_MAX_ST_UCAST_ENTRIES, VLAN_MAX_ST_UCAST_ENTRIES, 0},

    {"tVlanStMcastEntry", "VLAN_MAX_ST_MCAST_ENTRIES", sizeof (tVlanStMcastEntry),
     VLAN_MAX_ST_MCAST_ENTRIES, VLAN_MAX_ST_MCAST_ENTRIES, 0},

    {"tVlanGroupEntry", "VLAN_MAX_GROUP_ENTRIES", sizeof (tVlanGroupEntry),
     VLAN_MAX_GROUP_ENTRIES, VLAN_MAX_GROUP_ENTRIES, 0},

    {"tVlanMacMapEntry", "VLAN_MAX_MAC_MAP_ENTRIES", sizeof (tVlanMacMapEntry),
     VLAN_MAX_MAC_MAP_ENTRIES, VLAN_MAX_MAC_MAP_ENTRIES, 0},
   
#ifndef NPAPI_WANTED
    {"tVlanPortInfoPerVlan", "VLAN_MAX_PORT_VLAN_STAT_ENTRIES",
      sizeof (tVlanPortInfoPerVlan), VLAN_MAX_PORT_VLAN_STAT_ENTRIES, 
      VLAN_MAX_PORT_VLAN_STAT_ENTRIES, 0},

    {"tVlanStats", "VLAN_DEV_MAX_NUM_VLAN", sizeof (tVlanStats),
     VLAN_DEV_MAX_NUM_VLAN, VLAN_DEV_MAX_NUM_VLAN, 0},
#endif

    {"tVlanProtGrpEntry", "VLAN_MAX_PROTO_GROUP_ENTRIES", sizeof (tVlanProtGrpEntry),
     VLAN_MAX_PROTO_GROUP_ENTRIES, VLAN_MAX_PROTO_GROUP_ENTRIES, 0},

    {"tVlanPortVidSet", "VLAN_MAX_PORT_PROTOCOL_ENTRIES", sizeof (tVlanPortVidSet),
     VLAN_MAX_PORT_PROTOCOL_ENTRIES, VLAN_MAX_PORT_PROTOCOL_ENTRIES, 0},

    {"tVlanMacControl", "VLAN_DEV_MAX_NUM_VLAN", sizeof (tVlanMacControl),
     VLAN_DEV_MAX_NUM_VLAN, VLAN_DEV_MAX_NUM_VLAN, 0},

    {"tVlanWildCardEntry", "VLAN_MAX_WILDCARD_ENTRIES", sizeof (tVlanWildCardEntry),
     VLAN_MAX_WILDCARD_ENTRIES, VLAN_MAX_WILDCARD_ENTRIES, 0},

    {"tVlanContextInfo", "SYS_DEF_MAX_NUM_CONTEXTS", sizeof (tVlanContextInfo),
     SYS_DEF_MAX_NUM_CONTEXTS, SYS_DEF_MAX_NUM_CONTEXTS, 0},

    {"VLAN NP Port Array", "VLAN_MAX_NUM_PORT_ARRAYS_PER_NP", 
     (VLAN_MAX_PORTS * sizeof (UINT4)),
     VLAN_MAX_NUM_PORT_ARRAYS_PER_NP, VLAN_MAX_NUM_PORT_ARRAYS_PER_NP, 0},

    {"tVlanTempPortList", "VLAN_MAX_TEMP_PORTLIST", sizeof (tVlanTempPortList),
     VLAN_MAX_TEMP_PORTLIST, VLAN_MAX_TEMP_PORTLIST, 0},

#ifdef MPLS_WANTED
    {"tVlanL2VpnMap", "VLAN_MAX_L2VPN_ENTRIES", sizeof (tVlanL2VpnMap),
     VLAN_MAX_L2VPN_ENTRIES, VLAN_MAX_L2VPN_ENTRIES, 0},
#endif

    {"tVlanPbPortEntry", "VLAN_MAX_PORTS_IN_SYSTEM + 1", sizeof (tVlanPbPortEntry),
     VLAN_MAX_PORTS_IN_SYSTEM + 1, VLAN_MAX_PORTS_IN_SYSTEM + 1, 0},

#ifdef PB_WANTED
    {"tVlanCVlanSVlanEntry", "SVLAN_PORT_CVLAN_MAX_ENTRIES", sizeof (tVlanCVlanSVlanEntry),
     SVLAN_PORT_CVLAN_MAX_ENTRIES, SVLAN_PORT_CVLAN_MAX_ENTRIES, 0},
      
    {"tVlanCVlanSrcMacSVlanEntry", "SVLAN_PORT_CVLAN_SRCMAC_MAX_ENTRIES", 
     sizeof (tVlanCVlanSrcMacSVlanEntry), SVLAN_PORT_CVLAN_SRCMAC_MAX_ENTRIES,
     SVLAN_PORT_CVLAN_SRCMAC_MAX_ENTRIES, 0},

    {"tVlanCVlanDstMacSVlanEntry", "SVLAN_PORT_CVLAN_DSTMAC_MAX_ENTRIES", 
     sizeof (tVlanCVlanDstMacSVlanEntry), SVLAN_PORT_CVLAN_DSTMAC_MAX_ENTRIES,
     SVLAN_PORT_CVLAN_DSTMAC_MAX_ENTRIES, 0},

    {"tVlanCVlanDscpSVlanEntry", "SVLAN_PORT_CVLAN_DSCP_MAX_ENTRIES",
     sizeof (tVlanCVlanDscpSVlanEntry), SVLAN_PORT_CVLAN_DSCP_MAX_ENTRIES,
     SVLAN_PORT_CVLAN_DSCP_MAX_ENTRIES, 0},

    {"tVlanCVlanDstIpSVlanEntry", "SVLAN_PORT_CVLAN_DSTIP_MAX_ENTRIES",
     sizeof (tVlanCVlanDstIpSVlanEntry), SVLAN_PORT_CVLAN_DSTIP_MAX_ENTRIES,
     SVLAN_PORT_CVLAN_DSTIP_MAX_ENTRIES, 0},

    {"tVlanSrcMacSVlanEntry", "SVLAN_PORT_SRCMAC_MAX_ENTRIES", sizeof (tVlanSrcMacSVlanEntry),
     SVLAN_PORT_SRCMAC_MAX_ENTRIES, SVLAN_PORT_SRCMAC_MAX_ENTRIES, 0},

    {"tVlanDstMacSVlanEntry", "SVLAN_PORT_DSTMAC_MAX_ENTRIES", sizeof (tVlanDstMacSVlanEntry),
     SVLAN_PORT_DSTMAC_MAX_ENTRIES, SVLAN_PORT_DSTMAC_MAX_ENTRIES, 0},

    {"tVlanDscpSVlanEntry", "SVLAN_PORT_DSCP_MAX_ENTRIES", sizeof (tVlanDscpSVlanEntry),
     SVLAN_PORT_DSCP_MAX_ENTRIES, SVLAN_PORT_DSCP_MAX_ENTRIES, 0},

    {"tVlanSrcIpSVlanEntry", "SVLAN_PORT_SRCIP_MAX_ENTRIES", sizeof (tVlanSrcIpSVlanEntry),
     SVLAN_PORT_SRCIP_MAX_ENTRIES, SVLAN_PORT_SRCIP_MAX_ENTRIES, 0},

    {"tVlanDstIpSVlanEntry", "SVLAN_PORT_DSTIP_MAX_ENTRIES", sizeof (tVlanDstIpSVlanEntry),
     SVLAN_PORT_DSTIP_MAX_ENTRIES, SVLAN_PORT_DSTIP_MAX_ENTRIES, 0},

    {"tVlanSrcDstIpSVlanEntry", "SVLAN_PORT_SRCIP_DSTIP_MAX_ENTRIES",
     sizeof (tVlanSrcDstIpSVlanEntry), SVLAN_PORT_SRCIP_DSTIP_MAX_ENTRIES,
     SVLAN_PORT_SRCIP_DSTIP_MAX_ENTRIES, 0},

    {"tEtherTypeSwapEntry", "VLAN_MAX_ETHER_TYPE_SWAP_ENTRIES", sizeof (tEtherTypeSwapEntry),
     VLAN_MAX_ETHER_TYPE_SWAP_ENTRIES, VLAN_MAX_ETHER_TYPE_SWAP_ENTRIES, 0},

    {"tVlanPbLogicalPortEntry", "VLAN_PB_MAX_LOGICAL_PORT_ENTRIES", 
     sizeof (tVlanPbLogicalPortEntry), VLAN_PB_MAX_LOGICAL_PORT_ENTRIES, 
     VLAN_PB_MAX_LOGICAL_PORT_ENTRIES, 0},
#endif

#ifdef L2RED_WANTED
    {"tVlanRedGvrpDeletedEntry", "VLAN_DEV_MAX_NUM_VLAN", 
     sizeof (tVlanRedGvrpDeletedEntry), VLAN_RED_GARP_ENTRIES, 
     VLAN_RED_GARP_ENTRIES, 0},

    {"tVlanRedGmrpDeletedEntry", "VLAN_MAX_GROUP_ENTRIES", sizeof (tVlanRedGmrpDeletedEntry),
     VLAN_RED_GARP_ENTRIES, VLAN_RED_GARP_ENTRIES, 0},

    {"tVlanRedGvrpLearntEntry", "VLAN_DEV_MAX_NUM_VLAN", 
     sizeof (tVlanRedGvrpLearntEntry), VLAN_RED_GARP_ENTRIES, 
     VLAN_RED_GARP_ENTRIES, 0},

    {"tVlanRedGmrpLearntEntry", "VLAN_MAX_GROUP_ENTRIES", sizeof (tVlanRedGmrpLearntEntry),
     VLAN_RED_GARP_ENTRIES, VLAN_RED_GARP_ENTRIES, 0},
#endif
    
    {"tStaticVlanEntry", "VLAN_DEV_MAX_NUM_VLAN", sizeof (tStaticVlanEntry),
     VLAN_DEV_MAX_NUM_VLAN, VLAN_DEV_MAX_NUM_VLAN, 0},

    {"VLAN_PVLAN_SIZING_ID", "VLAN_PVLAN_MAX_BLKS",
        (sizeof (UINT2) * (VLAN_MAX_COMMUNITY_VLANS + 1)),
     VLAN_PVLAN_MAX_BLKS, VLAN_PVLAN_MAX_BLKS, 0},

    {"tVlanBasePortEntry","VLAN_MAX_PORTS_IN_SYSTEM",sizeof(tVlanBasePortEntry),
     VLAN_MAX_PORTS_IN_SYSTEM, VLAN_MAX_PORTS_IN_SYSTEM, 0 },

    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo gFsVlanSizingInfo;
UINT4 gu4VlanCliContext=0;

tCVlanInfo          gVlanPbCVlanInfo;
tVlanList           gVlanNullVlanList; 

/* Mac Aging Task Related information */
tVLAN_SLL      gVlanMcagEntries;
tMcagTaskInfo       gMcagTaskInfo;
/*Since the timer is running globally for Mac Aging task*/
tMcagTimer          gMcagTimer;


#ifdef MBSM_WANTED
INT4 gi4ProtoId = 0;
UINT4 gu4ContextIdProcessed = 0;
UINT2 gu2ByteIndex = 0;
UINT2 gu2BitIndex = 0;
UINT1 gu1IsBatchingStarted = OSIX_FALSE;
UINT1 gu1PortFlag = 0;
tMbsmSlotInfo gMbsmSlotInfo;
tMbsmPortInfo gMbsmPortInfo;
#endif
#ifdef SW_LEARNING
#ifdef MBSM_WANTED
   tRBTree             gVlanFDBInfo;
#endif /* MBSM_WANTD */
#endif /* SW_LEARNING */
#endif
