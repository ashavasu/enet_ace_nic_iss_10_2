/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : vlanhash.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN                                             */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains hashing related macros.       */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#ifndef _VLANHASH_H
#define _VLANHASH_H

#define VLAN_HASH_2BYTE(u2Val)\
    ((((u2Val) & 0xFF00) >> 8) ^\
     ((u2Val) & 0x00FF)) 

#define VLAN_HASH_6BYTE(pMacAddr)\
        ((pMacAddr[5]) ^\
         (pMacAddr[4]) ^\
         (pMacAddr[3]) ^\
         (pMacAddr[2]) ^\
         (pMacAddr[1]) ^\
	 (pMacAddr[0]))
       	
#define VLAN_HASH_MAC_ADDR(pMacAddr, u2HashIndex)\
        u2HashIndex = (UINT2) VLAN_HASH_6BYTE (pMacAddr); \
        u2HashIndex = (UINT2) (u2HashIndex % VLAN_MAX_BUCKETS);
#define VLAN_HASH_MAC_MAP_ADDR(u4Port, u2HashIndex)\
        u2HashIndex = (UINT2) (u4Port% VLAN_MAX_MAC_BUCKETS);
 	
#define VLAN_INIT_HASH_TABLE(HashTbl) \
        {\
          UINT2 u2LoopVal;\
          \
          for (u2LoopVal = 0; \
               u2LoopVal < VLAN_MAX_BUCKETS;\
               u2LoopVal++) {\
               \
               HashTbl [u2LoopVal] = NULL ;\
          }\
        }

#endif /* End of File */
