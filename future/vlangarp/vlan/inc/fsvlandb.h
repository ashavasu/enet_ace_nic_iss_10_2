/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvlandb.h,v 1.39.22.1 2018/03/15 12:59:54 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSVLANDB_H
#define _FSVLANDB_H

UINT1 Dot1qFutureVlanPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1qFutureVlanPortMacMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot1qFutureVlanFidMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1qFutureVlanTunnelTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1qFutureVlanTunnelProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1qFutureVlanCounterTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1qFutureVlanUnicastMacControlTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1qFutureVlanTpFdbTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot1qFutureVlanWildCardTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot1qFutureStaticUnicastExtnTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1qFutureVlanPortSubnetMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Dot1qFutureStVlanExtTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1qFutureVlanPortSubnetMapExtTableINDEX [] = 
{SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,
    SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Dot1qFutureVlanLoopbackTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsvlan [] ={1,3,6,1,4,1,2076,65};
tSNMP_OID_TYPE fsvlanOID = {8, fsvlan};


UINT4 Dot1qFutureVlanStatus [ ] ={1,3,6,1,4,1,2076,65,1,1};
UINT4 Dot1qFutureVlanMacBasedOnAllPorts [ ] ={1,3,6,1,4,1,2076,65,1,2};
UINT4 Dot1qFutureVlanPortProtoBasedOnAllPorts [ ] ={1,3,6,1,4,1,2076,65,1,3};
UINT4 Dot1qFutureVlanShutdownStatus [ ] ={1,3,6,1,4,1,2076,65,1,5};
UINT4 Dot1qFutureGarpShutdownStatus [ ] ={1,3,6,1,4,1,2076,65,1,6};
UINT4 Dot1qFutureVlanDebug [ ] ={1,3,6,1,4,1,2076,65,1,7};
UINT4 Dot1qFutureVlanLearningMode [ ] ={1,3,6,1,4,1,2076,65,1,8};
UINT4 Dot1qFutureVlanHybridTypeDefault [ ] ={1,3,6,1,4,1,2076,65,1,9};
UINT4 Dot1qFutureVlanPort [ ] ={1,3,6,1,4,1,2076,65,1,10,1,1};
UINT4 Dot1qFutureVlanPortType [ ] ={1,3,6,1,4,1,2076,65,1,10,1,2};
UINT4 Dot1qFutureVlanPortMacBasedClassification [ ] ={1,3,6,1,4,1,2076,65,1,10,1,3};
UINT4 Dot1qFutureVlanPortPortProtoBasedClassification [ ] ={1,3,6,1,4,1,2076,65,1,10,1,4};
UINT4 Dot1qFutureVlanFilteringUtilityCriteria [ ] ={1,3,6,1,4,1,2076,65,1,10,1,5};
UINT4 Dot1qFutureVlanPortProtected [ ] ={1,3,6,1,4,1,2076,65,1,10,1,6};
UINT4 Dot1qFutureVlanPortSubnetBasedClassification [ ] ={1,3,6,1,4,1,2076,65,1,10,1,7};
UINT4 Dot1qFutureVlanPortUnicastMacLearning [ ] ={1,3,6,1,4,1,2076,65,1,10,1,8};
UINT4 Dot1qFutureVlanPortIngressEtherType [ ] ={1,3,6,1,4,1,2076,65,1,10,1,9};
UINT4 Dot1qFutureVlanPortEgressEtherType [ ] ={1,3,6,1,4,1,2076,65,1,10,1,10};
UINT4 Dot1qFutureVlanPortEgressTPIDType [ ] ={1,3,6,1,4,1,2076,65,1,10,1,11};
UINT4 Dot1qFutureVlanPortAllowableTPID1 [ ] ={1,3,6,1,4,1,2076,65,1,10,1,12};
UINT4 Dot1qFutureVlanPortAllowableTPID2 [ ] ={1,3,6,1,4,1,2076,65,1,10,1,13};
UINT4 Dot1qFutureVlanPortAllowableTPID3 [ ] ={1,3,6,1,4,1,2076,65,1,10,1,14};
UINT4 Dot1qFutureVlanPortUnicastMacSecType [ ] ={1,3,6,1,4,1,2076,65,1,10,1,15};
UINT4 Dot1qFuturePortPacketReflectionStatus [ ] ={1,3,6,1,4,1,2076,65,1,10,1,16}; 
UINT4 Dot1qFutureVlanPortMacMapAddr [ ] ={1,3,6,1,4,1,2076,65,1,11,1,1};
UINT4 Dot1qFutureVlanPortMacMapVid [ ] ={1,3,6,1,4,1,2076,65,1,11,1,2};
UINT4 Dot1qFutureVlanPortMacMapName [ ] ={1,3,6,1,4,1,2076,65,1,11,1,3};
UINT4 Dot1qFutureVlanPortMacMapMcastBcastOption [ ] ={1,3,6,1,4,1,2076,65,1,11,1,4};
UINT4 Dot1qFutureVlanPortMacMapRowStatus [ ] ={1,3,6,1,4,1,2076,65,1,11,1,5};
UINT4 Dot1qFutureVlanIndex [ ] ={1,3,6,1,4,1,2076,65,1,12,1,1};
UINT4 Dot1qFutureVlanFid [ ] ={1,3,6,1,4,1,2076,65,1,12,1,2};
UINT4 Dot1qFutureVlanOperStatus [ ] ={1,3,6,1,4,1,2076,65,1,13};
UINT4 Dot1qFutureGvrpOperStatus [ ] ={1,3,6,1,4,1,2076,65,1,14};
UINT4 Dot1qFutureGmrpOperStatus [ ] ={1,3,6,1,4,1,2076,65,1,15};
UINT4 Dot1qFutureVlanBridgeMode [ ] ={1,3,6,1,4,1,2076,65,2,1};
UINT4 Dot1qFutureVlanTunnelBpduPri [ ] ={1,3,6,1,4,1,2076,65,2,2};
UINT4 Dot1qFutureVlanTunnelStatus [ ] ={1,3,6,1,4,1,2076,65,2,3,1,1};
UINT4 Dot1qFutureVlanTunnelStpPDUs [ ] ={1,3,6,1,4,1,2076,65,2,4,1,1};
UINT4 Dot1qFutureVlanTunnelStpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,65,2,4,1,2};
UINT4 Dot1qFutureVlanTunnelStpPDUsSent [ ] ={1,3,6,1,4,1,2076,65,2,4,1,3};
UINT4 Dot1qFutureVlanTunnelGvrpPDUs [ ] ={1,3,6,1,4,1,2076,65,2,4,1,4};
UINT4 Dot1qFutureVlanTunnelGvrpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,65,2,4,1,5};
UINT4 Dot1qFutureVlanTunnelGvrpPDUsSent [ ] ={1,3,6,1,4,1,2076,65,2,4,1,6};
UINT4 Dot1qFutureVlanTunnelIgmpPkts [ ] ={1,3,6,1,4,1,2076,65,2,4,1,7};
UINT4 Dot1qFutureVlanTunnelIgmpPktsRecvd [ ] ={1,3,6,1,4,1,2076,65,2,4,1,8};
UINT4 Dot1qFutureVlanTunnelIgmpPktsSent [ ] ={1,3,6,1,4,1,2076,65,2,4,1,9};
UINT4 Dot1qFutureVlanCounterRxUcast [ ] ={1,3,6,1,4,1,2076,65,1,16,1,1};
UINT4 Dot1qFutureVlanCounterRxMcastBcast [ ] ={1,3,6,1,4,1,2076,65,1,16,1,2};
UINT4 Dot1qFutureVlanCounterTxUnknUcast [ ] ={1,3,6,1,4,1,2076,65,1,16,1,3};
UINT4 Dot1qFutureVlanCounterTxUcast [ ] ={1,3,6,1,4,1,2076,65,1,16,1,4};
UINT4 Dot1qFutureVlanCounterTxBcast [ ] ={1,3,6,1,4,1,2076,65,1,16,1,5};
UINT4 Dot1qFutureVlanCounterRxFrames [ ] ={1,3,6,1,4,1,2076,65,1,16,1,6};
UINT4 Dot1qFutureVlanCounterRxBytes [ ] ={1,3,6,1,4,1,2076,65,1,16,1,7};
UINT4 Dot1qFutureVlanCounterTxFrames [ ] ={1,3,6,1,4,1,2076,65,1,16,1,8};
UINT4 Dot1qFutureVlanCounterTxBytes [ ] ={1,3,6,1,4,1,2076,65,1,16,1,9};
UINT4 Dot1qFutureVlanCounterDiscardFrames [ ] ={1,3,6,1,4,1,2076,65,1,16,1,10};
UINT4 Dot1qFutureVlanCounterDiscardBytes [ ] ={1,3,6,1,4,1,2076,65,1,16,1,11};
UINT4 Dot1qFutureVlanCounterStatus [ ] ={1,3,6,1,4,1,2076,65,1,16,1,12};
UINT4 Dot1qFutureVlanUnicastMacLimit [ ] ={1,3,6,1,4,1,2076,65,1,17,1,1};
UINT4 Dot1qFutureVlanAdminMacLearningStatus [ ] ={1,3,6,1,4,1,2076,65,1,17,1,2};
UINT4 Dot1qFutureVlanOperMacLearningStatus [ ] ={1,3,6,1,4,1,2076,65,1,17,1,3};
UINT4 Dot1qFutureVlanPortFdbFlush [ ] ={1,3,6,1,4,1,2076,65,1,17,1,4};
UINT4 Dot1qFutureVlanRemoteFdbFlush [ ] ={1,3,6,1,4,1,2076,65,1,17,1,5};
UINT4 Dot1qFutureGarpDebug [ ] ={1,3,6,1,4,1,2076,65,1,18};
UINT4 Dot1qFutureVlanTpFdbPw [ ] ={1,3,6,1,4,1,2076,65,1,19,1,1};
UINT4 Dot1qTpOldFdbPort [ ] ={1,3,6,1,4,1,2076,65,1,19,1,2};
UINT4 Dot1qFutureConnectionIdentifier [ ] ={1,3,6,1,4,1,2076,65,1,19,1,3};
UINT4 Dot1qFutureVlanWildCardMacAddress [ ] ={1,3,6,1,4,1,2076,65,1,20,1,1};
UINT4 Dot1qFutureVlanWildCardEgressPorts [ ] ={1,3,6,1,4,1,2076,65,1,20,1,2};
UINT4 Dot1qFutureVlanWildCardRowStatus [ ] ={1,3,6,1,4,1,2076,65,1,20,1,3};
UINT4 Dot1qFutureStaticConnectionIdentifier [ ] ={1,3,6,1,4,1,2076,65,1,21,1,1};
UINT4 Dot1qFutureUnicastMacLearningLimit [ ] ={1,3,6,1,4,1,2076,65,1,22};
UINT4 Dot1qFutureVlanBaseBridgeMode [ ] ={1,3,6,1,4,1,2076,65,1,23};
UINT4 Dot1qFutureVlanSubnetBasedOnAllPorts [ ] ={1,3,6,1,4,1,2076,65,1,24};
UINT4 Dot1qFutureVlanPortSubnetMapAddr [ ] ={1,3,6,1,4,1,2076,65,1,25,1,1};
UINT4 Dot1qFutureVlanPortSubnetMapVid [ ] ={1,3,6,1,4,1,2076,65,1,25,1,2};
UINT4 Dot1qFutureVlanPortSubnetMapARPOption [ ] ={1,3,6,1,4,1,2076,65,1,25,1,3};
UINT4 Dot1qFutureVlanPortSubnetMapRowStatus [ ] ={1,3,6,1,4,1,2076,65,1,25,1,4};
UINT4 Dot1qFutureStVlanType [ ] ={1,3,6,1,4,1,2076,65,1,29,1,1};
UINT4 Dot1qFutureStVlanVid [ ] ={1,3,6,1,4,1,2076,65,1,29,1,2};
UINT4 Dot1qFutureStVlanEgressEthertype [ ] ={1,3,6,1,4,1,2076,65,1,29,1,3};
UINT4 Dot1qFutureVlanGlobalMacLearningStatus [ ] ={1,3,6,1,4,1,2076,65,1,26};
UINT4 Dot1qFutureVlanApplyEnhancedFilteringCriteria [ ] ={1,3,6,1,4,1,2076,65,1,27};
UINT4 Dot1qFutureVlanSwStatsEnabled [ ] ={1,3,6,1,4,1,2076,65,1,28};
UINT4 Dot1qFutureVlanPortSubnetMapExtAddr [ ] ={1,3,6,1,4,1,2076,65,1,30,1,1};
UINT4 Dot1qFutureVlanPortSubnetMapExtMask [ ] ={1,3,6,1,4,1,2076,65,1,30,1,2};
UINT4 Dot1qFutureVlanPortSubnetMapExtVid [ ] ={1,3,6,1,4,1,2076,65,1,30,1,3};
UINT4 Dot1qFutureVlanPortSubnetMapExtARPOption [ ] =
{1,3,6,1,4,1,2076,65,1,30,1,4};
UINT4 Dot1qFutureVlanPortSubnetMapExtRowStatus [ ] =
{1,3,6,1,4,1,2076,65,1,30,1,5};
UINT4 Dot1qFutureVlanGlobalsFdbFlush [ ] ={1,3,6,1,4,1,2076,65,1,31};
UINT4 Dot1qFutureVlanUserDefinedTPID [ ] ={1,3,6,1,4,1,2076,65,1,32};
UINT4 Dot1qFutureVlanLoopbackStatus [ ] ={1,3,6,1,4,1,2076,65,1,33,1,1};




tMbDbEntry fsvlanMibEntry[]= {

{{10,Dot1qFutureVlanStatus}, NULL, Dot1qFutureVlanStatusGet, Dot1qFutureVlanStatusSet, Dot1qFutureVlanStatusTest, Dot1qFutureVlanStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Dot1qFutureVlanMacBasedOnAllPorts}, NULL, Dot1qFutureVlanMacBasedOnAllPortsGet, Dot1qFutureVlanMacBasedOnAllPortsSet, Dot1qFutureVlanMacBasedOnAllPortsTest, Dot1qFutureVlanMacBasedOnAllPortsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Dot1qFutureVlanPortProtoBasedOnAllPorts}, NULL, Dot1qFutureVlanPortProtoBasedOnAllPortsGet, Dot1qFutureVlanPortProtoBasedOnAllPortsSet, Dot1qFutureVlanPortProtoBasedOnAllPortsTest, Dot1qFutureVlanPortProtoBasedOnAllPortsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Dot1qFutureVlanShutdownStatus}, NULL, Dot1qFutureVlanShutdownStatusGet, Dot1qFutureVlanShutdownStatusSet, Dot1qFutureVlanShutdownStatusTest, Dot1qFutureVlanShutdownStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Dot1qFutureGarpShutdownStatus}, NULL, Dot1qFutureGarpShutdownStatusGet, Dot1qFutureGarpShutdownStatusSet, Dot1qFutureGarpShutdownStatusTest, Dot1qFutureGarpShutdownStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Dot1qFutureVlanDebug}, NULL, Dot1qFutureVlanDebugGet, Dot1qFutureVlanDebugSet, Dot1qFutureVlanDebugTest, Dot1qFutureVlanDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Dot1qFutureVlanLearningMode}, NULL, Dot1qFutureVlanLearningModeGet, Dot1qFutureVlanLearningModeSet, Dot1qFutureVlanLearningModeTest, Dot1qFutureVlanLearningModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Dot1qFutureVlanHybridTypeDefault}, NULL, Dot1qFutureVlanHybridTypeDefaultGet, Dot1qFutureVlanHybridTypeDefaultSet, Dot1qFutureVlanHybridTypeDefaultTest, Dot1qFutureVlanHybridTypeDefaultDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,Dot1qFutureVlanPort}, GetNextIndexDot1qFutureVlanPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanPortType}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortTypeGet, Dot1qFutureVlanPortTypeSet, Dot1qFutureVlanPortTypeTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, "3"},

{{12,Dot1qFutureVlanPortMacBasedClassification}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortMacBasedClassificationGet, Dot1qFutureVlanPortMacBasedClassificationSet, Dot1qFutureVlanPortMacBasedClassificationTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanPortPortProtoBasedClassification}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortPortProtoBasedClassificationGet, Dot1qFutureVlanPortPortProtoBasedClassificationSet, Dot1qFutureVlanPortPortProtoBasedClassificationTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanFilteringUtilityCriteria}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanFilteringUtilityCriteriaGet, Dot1qFutureVlanFilteringUtilityCriteriaSet, Dot1qFutureVlanFilteringUtilityCriteriaTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, "1"},

{{12,Dot1qFutureVlanPortProtected}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortProtectedGet, Dot1qFutureVlanPortProtectedSet, Dot1qFutureVlanPortProtectedTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, "2"},

{{12,Dot1qFutureVlanPortSubnetBasedClassification}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortSubnetBasedClassificationGet, Dot1qFutureVlanPortSubnetBasedClassificationSet, Dot1qFutureVlanPortSubnetBasedClassificationTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},
{{12,Dot1qFutureVlanPortUnicastMacLearning}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortUnicastMacLearningGet, Dot1qFutureVlanPortUnicastMacLearningSet, Dot1qFutureVlanPortUnicastMacLearningTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, "1"},

{{12,Dot1qFutureVlanPortIngressEtherType}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortIngressEtherTypeGet, Dot1qFutureVlanPortIngressEtherTypeSet, Dot1qFutureVlanPortIngressEtherTypeTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanPortEgressEtherType}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortEgressEtherTypeGet, Dot1qFutureVlanPortEgressEtherTypeSet, Dot1qFutureVlanPortEgressEtherTypeTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanPortEgressTPIDType}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortEgressTPIDTypeGet, Dot1qFutureVlanPortEgressTPIDTypeSet, Dot1qFutureVlanPortEgressTPIDTypeTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, "1"},

{{12,Dot1qFutureVlanPortAllowableTPID1}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortAllowableTPID1Get, Dot1qFutureVlanPortAllowableTPID1Set, Dot1qFutureVlanPortAllowableTPID1Test, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, "0"},

{{12,Dot1qFutureVlanPortAllowableTPID2}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortAllowableTPID2Get, Dot1qFutureVlanPortAllowableTPID2Set, Dot1qFutureVlanPortAllowableTPID2Test, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, "0"},

{{12,Dot1qFutureVlanPortAllowableTPID3}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortAllowableTPID3Get, Dot1qFutureVlanPortAllowableTPID3Set, Dot1qFutureVlanPortAllowableTPID3Test, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, "0"},

{{12,Dot1qFutureVlanPortUnicastMacSecType}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFutureVlanPortUnicastMacSecTypeGet, Dot1qFutureVlanPortUnicastMacSecTypeSet, Dot1qFutureVlanPortUnicastMacSecTypeTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, "3"},

{{12,Dot1qFuturePortPacketReflectionStatus}, GetNextIndexDot1qFutureVlanPortTable, Dot1qFuturePortPacketReflectionStatusGet, Dot1qFuturePortPacketReflectionStatusSet, Dot1qFuturePortPacketReflectionStatusTest, Dot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortTableINDEX, 1, 0, 0, "2"},


{{12,Dot1qFutureVlanPortMacMapAddr}, GetNextIndexDot1qFutureVlanPortMacMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot1qFutureVlanPortMacMapTableINDEX, 2, 0, 0, NULL},

{{12,Dot1qFutureVlanPortMacMapVid}, GetNextIndexDot1qFutureVlanPortMacMapTable, Dot1qFutureVlanPortMacMapVidGet, Dot1qFutureVlanPortMacMapVidSet, Dot1qFutureVlanPortMacMapVidTest, Dot1qFutureVlanPortMacMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortMacMapTableINDEX, 2, 0, 0, NULL},

{{12,Dot1qFutureVlanPortMacMapName}, GetNextIndexDot1qFutureVlanPortMacMapTable, Dot1qFutureVlanPortMacMapNameGet, Dot1qFutureVlanPortMacMapNameSet, Dot1qFutureVlanPortMacMapNameTest, Dot1qFutureVlanPortMacMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qFutureVlanPortMacMapTableINDEX, 2, 0, 0, NULL},

{{12,Dot1qFutureVlanPortMacMapMcastBcastOption}, GetNextIndexDot1qFutureVlanPortMacMapTable, Dot1qFutureVlanPortMacMapMcastBcastOptionGet, Dot1qFutureVlanPortMacMapMcastBcastOptionSet, Dot1qFutureVlanPortMacMapMcastBcastOptionTest, Dot1qFutureVlanPortMacMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortMacMapTableINDEX, 2, 0, 0, "1"},

{{12,Dot1qFutureVlanPortMacMapRowStatus}, GetNextIndexDot1qFutureVlanPortMacMapTable, Dot1qFutureVlanPortMacMapRowStatusGet, Dot1qFutureVlanPortMacMapRowStatusSet, Dot1qFutureVlanPortMacMapRowStatusTest, Dot1qFutureVlanPortMacMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortMacMapTableINDEX, 2, 0, 1, NULL},

{{12,Dot1qFutureVlanIndex}, GetNextIndexDot1qFutureVlanFidMapTable, Dot1qFutureVlanIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1qFutureVlanFidMapTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanFid}, GetNextIndexDot1qFutureVlanFidMapTable, Dot1qFutureVlanFidGet, Dot1qFutureVlanFidSet, Dot1qFutureVlanFidTest, Dot1qFutureVlanFidMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1qFutureVlanFidMapTableINDEX, 1, 0, 0, NULL},

{{10,Dot1qFutureVlanOperStatus}, NULL, Dot1qFutureVlanOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Dot1qFutureGvrpOperStatus}, NULL, Dot1qFutureGvrpOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Dot1qFutureGmrpOperStatus}, NULL, Dot1qFutureGmrpOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,Dot1qFutureVlanCounterRxUcast}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterRxUcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanCounterRxMcastBcast}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterRxMcastBcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanCounterTxUnknUcast}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterTxUnknUcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanCounterTxUcast}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterTxUcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanCounterTxBcast}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterTxBcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, NULL},
{{12,Dot1qFutureVlanCounterRxFrames}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterRxFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanCounterRxBytes}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterRxBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanCounterTxFrames}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterTxFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanCounterTxBytes}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterTxBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanCounterDiscardFrames}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterDiscardFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanCounterDiscardBytes}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterDiscardBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanCounterStatus}, GetNextIndexDot1qFutureVlanCounterTable, Dot1qFutureVlanCounterStatusGet, Dot1qFutureVlanCounterStatusSet, Dot1qFutureVlanCounterStatusTest, Dot1qFutureVlanCounterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanCounterTableINDEX, 1, 0, 0, "2"},

{{12,Dot1qFutureVlanUnicastMacLimit}, GetNextIndexDot1qFutureVlanUnicastMacControlTable, Dot1qFutureVlanUnicastMacLimitGet, Dot1qFutureVlanUnicastMacLimitSet, Dot1qFutureVlanUnicastMacLimitTest, Dot1qFutureVlanUnicastMacControlTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1qFutureVlanUnicastMacControlTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanAdminMacLearningStatus}, GetNextIndexDot1qFutureVlanUnicastMacControlTable, Dot1qFutureVlanAdminMacLearningStatusGet, Dot1qFutureVlanAdminMacLearningStatusSet, Dot1qFutureVlanAdminMacLearningStatusTest, Dot1qFutureVlanUnicastMacControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanUnicastMacControlTableINDEX, 1, 0, 0, "3"},

{{12,Dot1qFutureVlanOperMacLearningStatus}, GetNextIndexDot1qFutureVlanUnicastMacControlTable, Dot1qFutureVlanOperMacLearningStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1qFutureVlanUnicastMacControlTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanPortFdbFlush}, GetNextIndexDot1qFutureVlanUnicastMacControlTable, Dot1qFutureVlanPortFdbFlushGet, Dot1qFutureVlanPortFdbFlushSet, Dot1qFutureVlanPortFdbFlushTest, Dot1qFutureVlanUnicastMacControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanUnicastMacControlTableINDEX, 1, 0, 0, "2"},

{{12,Dot1qFutureVlanRemoteFdbFlush}, NULL, Dot1qFutureVlanRemoteFdbFlushGet, Dot1qFutureVlanRemoteFdbFlushSet, Dot1qFutureVlanRemoteFdbFlushTest, Dot1qFutureVlanRemoteFdbFlushDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Dot1qFutureGarpDebug}, NULL, Dot1qFutureGarpDebugGet, Dot1qFutureGarpDebugSet, Dot1qFutureGarpDebugTest, Dot1qFutureGarpDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,Dot1qFutureVlanTpFdbPw}, GetNextIndexDot1qFutureVlanTpFdbTable, Dot1qFutureVlanTpFdbPwGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1qFutureVlanTpFdbTableINDEX, 2, 0, 0, NULL},

{{12,Dot1qTpOldFdbPort}, GetNextIndexDot1qFutureVlanTpFdbTable, Dot1qTpOldFdbPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1qFutureVlanTpFdbTableINDEX, 2, 0, 0, NULL},

{{12,Dot1qFutureConnectionIdentifier}, GetNextIndexDot1qFutureVlanTpFdbTable, Dot1qFutureConnectionIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1qFutureVlanTpFdbTableINDEX, 2, 0, 0, NULL},

{{12,Dot1qFutureVlanWildCardMacAddress}, GetNextIndexDot1qFutureVlanWildCardTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot1qFutureVlanWildCardTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanWildCardEgressPorts}, GetNextIndexDot1qFutureVlanWildCardTable, Dot1qFutureVlanWildCardEgressPortsGet, Dot1qFutureVlanWildCardEgressPortsSet, Dot1qFutureVlanWildCardEgressPortsTest, Dot1qFutureVlanWildCardTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qFutureVlanWildCardTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureVlanWildCardRowStatus}, GetNextIndexDot1qFutureVlanWildCardTable, Dot1qFutureVlanWildCardRowStatusGet, Dot1qFutureVlanWildCardRowStatusSet, Dot1qFutureVlanWildCardRowStatusTest, Dot1qFutureVlanWildCardTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanWildCardTableINDEX, 1, 0, 1, NULL},

{{12,Dot1qFutureStaticConnectionIdentifier}, GetNextIndexDot1qFutureStaticUnicastExtnTable, Dot1qFutureStaticConnectionIdentifierGet, Dot1qFutureStaticConnectionIdentifierSet, Dot1qFutureStaticConnectionIdentifierTest, Dot1qFutureStaticUnicastExtnTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot1qFutureStaticUnicastExtnTableINDEX, 3, 0, 0, NULL},

{{10,Dot1qFutureUnicastMacLearningLimit}, NULL, Dot1qFutureUnicastMacLearningLimitGet, Dot1qFutureUnicastMacLearningLimitSet, Dot1qFutureUnicastMacLearningLimitTest, Dot1qFutureUnicastMacLearningLimitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Dot1qFutureVlanBaseBridgeMode}, NULL, Dot1qFutureVlanBaseBridgeModeGet, Dot1qFutureVlanBaseBridgeModeSet, Dot1qFutureVlanBaseBridgeModeTest, Dot1qFutureVlanBaseBridgeModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Dot1qFutureVlanSubnetBasedOnAllPorts}, NULL, Dot1qFutureVlanSubnetBasedOnAllPortsGet, Dot1qFutureVlanSubnetBasedOnAllPortsSet, Dot1qFutureVlanSubnetBasedOnAllPortsTest, Dot1qFutureVlanSubnetBasedOnAllPortsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,Dot1qFutureVlanPortSubnetMapAddr}, GetNextIndexDot1qFutureVlanPortSubnetMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Dot1qFutureVlanPortSubnetMapTableINDEX, 2, 0, 0, NULL},

{{12,Dot1qFutureVlanPortSubnetMapVid}, GetNextIndexDot1qFutureVlanPortSubnetMapTable, Dot1qFutureVlanPortSubnetMapVidGet, Dot1qFutureVlanPortSubnetMapVidSet, Dot1qFutureVlanPortSubnetMapVidTest, Dot1qFutureVlanPortSubnetMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortSubnetMapTableINDEX, 2, 0, 0, NULL},

{{12,Dot1qFutureVlanPortSubnetMapARPOption}, GetNextIndexDot1qFutureVlanPortSubnetMapTable, Dot1qFutureVlanPortSubnetMapARPOptionGet, Dot1qFutureVlanPortSubnetMapARPOptionSet, Dot1qFutureVlanPortSubnetMapARPOptionTest, Dot1qFutureVlanPortSubnetMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortSubnetMapTableINDEX, 2, 0, 0, "1"},

{{12,Dot1qFutureVlanPortSubnetMapRowStatus}, GetNextIndexDot1qFutureVlanPortSubnetMapTable, Dot1qFutureVlanPortSubnetMapRowStatusGet, Dot1qFutureVlanPortSubnetMapRowStatusSet, Dot1qFutureVlanPortSubnetMapRowStatusTest, Dot1qFutureVlanPortSubnetMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanPortSubnetMapTableINDEX, 2, 0, 1, NULL},

{{10,Dot1qFutureVlanGlobalMacLearningStatus}, NULL, Dot1qFutureVlanGlobalMacLearningStatusGet, Dot1qFutureVlanGlobalMacLearningStatusSet, Dot1qFutureVlanGlobalMacLearningStatusTest, Dot1qFutureVlanGlobalMacLearningStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Dot1qFutureVlanApplyEnhancedFilteringCriteria}, NULL, Dot1qFutureVlanApplyEnhancedFilteringCriteriaGet, Dot1qFutureVlanApplyEnhancedFilteringCriteriaSet, Dot1qFutureVlanApplyEnhancedFilteringCriteriaTest, Dot1qFutureVlanApplyEnhancedFilteringCriteriaDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Dot1qFutureVlanSwStatsEnabled}, NULL, Dot1qFutureVlanSwStatsEnabledGet, Dot1qFutureVlanSwStatsEnabledSet, Dot1qFutureVlanSwStatsEnabledTest, Dot1qFutureVlanSwStatsEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,Dot1qFutureStVlanType}, GetNextIndexDot1qFutureStVlanExtTable, Dot1qFutureStVlanTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1qFutureStVlanExtTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureStVlanVid}, GetNextIndexDot1qFutureStVlanExtTable, Dot1qFutureStVlanVidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1qFutureStVlanExtTableINDEX, 1, 0, 0, NULL},

{{12,Dot1qFutureStVlanEgressEthertype}, GetNextIndexDot1qFutureStVlanExtTable, Dot1qFutureStVlanEgressEthertypeGet, Dot1qFutureStVlanEgressEthertypeSet, Dot1qFutureStVlanEgressEthertypeTest, Dot1qFutureStVlanExtTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1qFutureStVlanExtTableINDEX, 1, 0, 0, "33024"},


{{12,Dot1qFutureVlanPortSubnetMapExtAddr},
GetNextIndexDot1qFutureVlanPortSubnetMapExtTable, NULL, NULL, NULL, NULL,
SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS,
Dot1qFutureVlanPortSubnetMapExtTableINDEX, 3, 0, 0, NULL},

{{12,Dot1qFutureVlanPortSubnetMapExtMask},
GetNextIndexDot1qFutureVlanPortSubnetMapExtTable, NULL, NULL, NULL, NULL,
SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS,
 Dot1qFutureVlanPortSubnetMapExtTableINDEX, 3, 0, 0, NULL},

{{12,Dot1qFutureVlanPortSubnetMapExtVid},
GetNextIndexDot1qFutureVlanPortSubnetMapExtTable,
Dot1qFutureVlanPortSubnetMapExtVidGet, Dot1qFutureVlanPortSubnetMapExtVidSet,
Dot1qFutureVlanPortSubnetMapExtVidTest,Dot1qFutureVlanPortSubnetMapExtTableDep,
SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
Dot1qFutureVlanPortSubnetMapExtTableINDEX, 3, 0, 0, NULL},

{{12,Dot1qFutureVlanPortSubnetMapExtARPOption},
GetNextIndexDot1qFutureVlanPortSubnetMapExtTable,
Dot1qFutureVlanPortSubnetMapExtARPOptionGet,
Dot1qFutureVlanPortSubnetMapExtARPOptionSet,
Dot1qFutureVlanPortSubnetMapExtARPOptionTest,
Dot1qFutureVlanPortSubnetMapExtTableDep,SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
Dot1qFutureVlanPortSubnetMapExtTableINDEX, 3, 0, 0, "1"},

{{12,Dot1qFutureVlanPortSubnetMapExtRowStatus},
GetNextIndexDot1qFutureVlanPortSubnetMapExtTable,
Dot1qFutureVlanPortSubnetMapExtRowStatusGet,
Dot1qFutureVlanPortSubnetMapExtRowStatusSet,
Dot1qFutureVlanPortSubnetMapExtRowStatusTest,
Dot1qFutureVlanPortSubnetMapExtTableDep,SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
Dot1qFutureVlanPortSubnetMapExtTableINDEX, 3, 0, 1, NULL},

{{10,Dot1qFutureVlanGlobalsFdbFlush}, NULL, Dot1qFutureVlanGlobalsFdbFlushGet, Dot1qFutureVlanGlobalsFdbFlushSet, Dot1qFutureVlanGlobalsFdbFlushTest, Dot1qFutureVlanGlobalsFdbFlushDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Dot1qFutureVlanUserDefinedTPID}, NULL, Dot1qFutureVlanUserDefinedTPIDGet, Dot1qFutureVlanUserDefinedTPIDSet, Dot1qFutureVlanUserDefinedTPIDTest, Dot1qFutureVlanUserDefinedTPIDDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,Dot1qFutureVlanLoopbackStatus}, GetNextIndexDot1qFutureVlanLoopbackTable, Dot1qFutureVlanLoopbackStatusGet, Dot1qFutureVlanLoopbackStatusSet, Dot1qFutureVlanLoopbackStatusTest, Dot1qFutureVlanLoopbackTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanLoopbackTableINDEX, 1, 0, 0, "2"},

{{10,Dot1qFutureVlanBridgeMode}, NULL, Dot1qFutureVlanBridgeModeGet, Dot1qFutureVlanBridgeModeSet, Dot1qFutureVlanBridgeModeTest, Dot1qFutureVlanBridgeModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, NULL},

{{10,Dot1qFutureVlanTunnelBpduPri}, NULL, Dot1qFutureVlanTunnelBpduPriGet, Dot1qFutureVlanTunnelBpduPriSet, Dot1qFutureVlanTunnelBpduPriTest, Dot1qFutureVlanTunnelBpduPriDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 1, 0, "7"},

{{12,Dot1qFutureVlanTunnelStatus}, GetNextIndexDot1qFutureVlanTunnelTable, Dot1qFutureVlanTunnelStatusGet, Dot1qFutureVlanTunnelStatusSet, Dot1qFutureVlanTunnelStatusTest, Dot1qFutureVlanTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanTunnelTableINDEX, 1, 1, 0, "2"},

{{12,Dot1qFutureVlanTunnelStpPDUs}, GetNextIndexDot1qFutureVlanTunnelProtocolTable, Dot1qFutureVlanTunnelStpPDUsGet, Dot1qFutureVlanTunnelStpPDUsSet, Dot1qFutureVlanTunnelStpPDUsTest, Dot1qFutureVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, "2"},

{{12,Dot1qFutureVlanTunnelStpPDUsRecvd}, GetNextIndexDot1qFutureVlanTunnelProtocolTable, Dot1qFutureVlanTunnelStpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,Dot1qFutureVlanTunnelStpPDUsSent}, GetNextIndexDot1qFutureVlanTunnelProtocolTable, Dot1qFutureVlanTunnelStpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,Dot1qFutureVlanTunnelGvrpPDUs}, GetNextIndexDot1qFutureVlanTunnelProtocolTable, Dot1qFutureVlanTunnelGvrpPDUsGet, Dot1qFutureVlanTunnelGvrpPDUsSet, Dot1qFutureVlanTunnelGvrpPDUsTest, Dot1qFutureVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, "1"},

{{12,Dot1qFutureVlanTunnelGvrpPDUsRecvd}, GetNextIndexDot1qFutureVlanTunnelProtocolTable, Dot1qFutureVlanTunnelGvrpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,Dot1qFutureVlanTunnelGvrpPDUsSent}, GetNextIndexDot1qFutureVlanTunnelProtocolTable, Dot1qFutureVlanTunnelGvrpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,Dot1qFutureVlanTunnelIgmpPkts}, GetNextIndexDot1qFutureVlanTunnelProtocolTable, Dot1qFutureVlanTunnelIgmpPktsGet, Dot1qFutureVlanTunnelIgmpPktsSet, Dot1qFutureVlanTunnelIgmpPktsTest, Dot1qFutureVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, "1"},

{{12,Dot1qFutureVlanTunnelIgmpPktsRecvd}, GetNextIndexDot1qFutureVlanTunnelProtocolTable, Dot1qFutureVlanTunnelIgmpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,Dot1qFutureVlanTunnelIgmpPktsSent}, GetNextIndexDot1qFutureVlanTunnelProtocolTable, Dot1qFutureVlanTunnelIgmpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},
};
tMibData fsvlanEntry = { 91, fsvlanMibEntry };

#endif /* _FSVLANDB_H */

