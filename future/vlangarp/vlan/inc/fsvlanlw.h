/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvlanlw.h,v 1.32.22.1 2018/03/15 12:59:55 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanStatus ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanMacBasedOnAllPorts ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanPortProtoBasedOnAllPorts ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanShutdownStatus ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanDebug ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanLearningMode ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanHybridTypeDefault ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanGlobalMacLearningStatus ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanApplyEnhancedFilteringCriteria ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanSwStatsEnabled ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanStatus ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanMacBasedOnAllPorts ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanPortProtoBasedOnAllPorts ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanShutdownStatus ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanDebug ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanLearningMode ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanHybridTypeDefault ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanGlobalMacLearningStatus ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanApplyEnhancedFilteringCriteria ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanSwStatsEnabled ARG_LIST((INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanMacBasedOnAllPorts ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortProtoBasedOnAllPorts ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanShutdownStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanLearningMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanHybridTypeDefault ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanGlobalMacLearningStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanApplyEnhancedFilteringCriteria ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanSwStatsEnabled ARG_LIST((UINT4 *  ,INT4 ));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanMacBasedOnAllPorts ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanPortProtoBasedOnAllPorts ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanShutdownStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanLearningMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanHybridTypeDefault ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanGlobalMacLearningStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanApplyEnhancedFilteringCriteria ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanSwStatsEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto Validate Index Instance for Dot1qFutureVlanPortTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanPortTable  */

INT1
nmhGetFirstIndexDot1qFutureVlanPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanPortType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortMacBasedClassification ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortPortProtoBasedClassification ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanFilteringUtilityCriteria ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortProtected ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortSubnetBasedClassification ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortUnicastMacLearning ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortIngressEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortEgressEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortEgressTPIDType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortAllowableTPID1 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortAllowableTPID2 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortAllowableTPID3 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortUnicastMacSecType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFuturePortPacketReflectionStatus ARG_LIST((INT4 ,INT4 *));


INT1
nmhGetFsPbPortUnicastMacLearning ARG_LIST((INT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanPortType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortMacBasedClassification ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortPortProtoBasedClassification ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanFilteringUtilityCriteria ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortProtected ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortSubnetBasedClassification ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortUnicastMacLearning ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortIngressEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortEgressEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortEgressTPIDType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortAllowableTPID1 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortAllowableTPID2 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortAllowableTPID3 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortUnicastMacSecType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFuturePortPacketReflectionStatus ARG_LIST((INT4  ,INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanPortType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortMacBasedClassification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortPortProtoBasedClassification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanFilteringUtilityCriteria ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortProtected ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortSubnetBasedClassification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortUnicastMacLearning ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortIngressEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortEgressEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortEgressTPIDType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortAllowableTPID1 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortAllowableTPID2 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortAllowableTPID3 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortUnicastMacSecType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFuturePortPacketReflectionStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qFutureVlanPortMacMapTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanPortMacMapTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanPortMacMapTable  */

INT1
nmhGetFirstIndexDot1qFutureVlanPortMacMapTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanPortMacMapTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanPortMacMapVid ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortMacMapName ARG_LIST((INT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qFutureVlanPortMacMapMcastBcastOption ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortMacMapRowStatus ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanPortMacMapVid ARG_LIST((INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortMacMapName ARG_LIST((INT4  , tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1qFutureVlanPortMacMapMcastBcastOption ARG_LIST((INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortMacMapRowStatus ARG_LIST((INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanPortMacMapVid ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortMacMapName ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1qFutureVlanPortMacMapMcastBcastOption ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortMacMapRowStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanPortMacMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qFutureVlanFidMapTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanFidMapTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanFidMapTable  */

INT1
nmhGetFirstIndexDot1qFutureVlanFidMapTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanFidMapTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanFid ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanFid ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanFid ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanFidMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanOperStatus ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanBridgeMode ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanTunnelBpduPri ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanBridgeMode ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanTunnelBpduPri ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanBridgeMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanTunnelBpduPri ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanBridgeMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanTunnelBpduPri ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qFutureVlanTunnelTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanTunnelTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanTunnelTable  */

INT1
nmhGetFirstIndexDot1qFutureVlanTunnelTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanTunnelTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanTunnelStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanTunnelStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanTunnelStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanTunnelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qFutureVlanTunnelProtocolTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanTunnelProtocolTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanTunnelProtocolTable  */

INT1
nmhGetFirstIndexDot1qFutureVlanTunnelProtocolTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanTunnelProtocolTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanTunnelStpPDUs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanTunnelStpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanTunnelStpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanTunnelGvrpPDUs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanTunnelGvrpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanTunnelGvrpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanTunnelIgmpPkts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanTunnelIgmpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanTunnelIgmpPktsSent ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanTunnelStpPDUs ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanTunnelGvrpPDUs ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanTunnelIgmpPkts ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanTunnelStpPDUs ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanTunnelGvrpPDUs ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanTunnelIgmpPkts ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanTunnelProtocolTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qFutureVlanCounterTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanCounterTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanCounterTable  */

INT1
nmhGetFirstIndexDot1qFutureVlanCounterTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanCounterTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanCounterRxUcast ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanCounterRxMcastBcast ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanCounterTxUnknUcast ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanCounterTxUcast ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanCounterTxBcast ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanCounterRxFrames ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanCounterRxBytes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanCounterTxFrames ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanCounterTxBytes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanCounterDiscardFrames ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanCounterDiscardBytes ARG_LIST((UINT4 ,UINT4 *));


INT1
nmhGetDot1qFutureVlanCounterStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanCounterStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanCounterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanCounterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qFutureVlanUnicastMacControlTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanUnicastMacControlTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanUnicastMacControlTable  */

INT1
nmhGetFirstIndexDot1qFutureVlanUnicastMacControlTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanUnicastMacControlTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanUnicastMacLimit ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot1qFutureVlanAdminMacLearningStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanOperMacLearningStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortFdbFlush ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanUnicastMacLimit ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot1qFutureVlanAdminMacLearningStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortFdbFlush ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanUnicastMacLimit ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot1qFutureVlanAdminMacLearningStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortFdbFlush ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanUnicastMacControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanRemoteFdbFlush ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanRemoteFdbFlush ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanRemoteFdbFlush ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanRemoteFdbFlush ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureGarpDebug ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureGarpDebug ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureGarpDebug ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureGarpDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qFutureVlanTpFdbTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanTpFdbTable ARG_LIST((UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanTpFdbTable  */

INT1
nmhGetFirstIndexDot1qFutureVlanTpFdbTable ARG_LIST((UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanTpFdbTable ARG_LIST((UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanTpFdbPw ARG_LIST((UINT4  , tMacAddr ,UINT4 *));

INT1
nmhGetDot1qTpOldFdbPort ARG_LIST((UINT4  , tMacAddr ,INT4 *));

INT1
nmhGetDot1qFutureConnectionIdentifier ARG_LIST((UINT4  , tMacAddr ,tMacAddr * ));

/* Proto Validate Index Instance for Dot1qFutureVlanWildCardTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanWildCardTable ARG_LIST((tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanWildCardTable  */

INT1
nmhGetFirstIndexDot1qFutureVlanWildCardTable ARG_LIST((tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanWildCardTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanWildCardEgressPorts ARG_LIST((tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qFutureVlanWildCardRowStatus ARG_LIST((tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanWildCardEgressPorts ARG_LIST((tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1qFutureVlanWildCardRowStatus ARG_LIST((tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanWildCardEgressPorts ARG_LIST((UINT4 *  ,tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1qFutureVlanWildCardRowStatus ARG_LIST((UINT4 *  ,tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanWildCardTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qFutureStaticUnicastExtnTable. */
INT1
nmhValidateIndexInstanceDot1qFutureStaticUnicastExtnTable ARG_LIST((UINT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureStaticUnicastExtnTable  */

INT1
nmhGetFirstIndexDot1qFutureStaticUnicastExtnTable ARG_LIST((UINT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureStaticUnicastExtnTable ARG_LIST((UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureStaticConnectionIdentifier ARG_LIST((UINT4  , tMacAddr  , INT4 ,tMacAddr * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureStaticConnectionIdentifier ARG_LIST((UINT4  , tMacAddr  , INT4  ,tMacAddr ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureStaticConnectionIdentifier ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,tMacAddr ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureStaticUnicastExtnTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureUnicastMacLearningLimit ARG_LIST((UINT4 *));

INT1
nmhGetDot1qFutureVlanBaseBridgeMode ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanSubnetBasedOnAllPorts ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureUnicastMacLearningLimit ARG_LIST((UINT4 ));

INT1
nmhSetDot1qFutureVlanBaseBridgeMode ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanSubnetBasedOnAllPorts ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureUnicastMacLearningLimit ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Dot1qFutureVlanBaseBridgeMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanSubnetBasedOnAllPorts ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureUnicastMacLearningLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanBaseBridgeMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanSubnetBasedOnAllPorts ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qFutureVlanPortSubnetMapTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanPortSubnetMapTable  */

INT1
nmhGetFirstIndexDot1qFutureVlanPortSubnetMapTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanPortSubnetMapTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanPortSubnetMapVid ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortSubnetMapARPOption ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanPortSubnetMapVid ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortSubnetMapARPOption ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanPortSubnetMapVid ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortSubnetMapARPOption ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanPortSubnetMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qFutureStVlanExtTable. */
INT1
nmhValidateIndexInstanceDot1qFutureStVlanExtTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureStVlanExtTable  */

INT1
nmhGetFirstIndexDot1qFutureStVlanExtTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureStVlanExtTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureStVlanType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot1qFutureStVlanVid ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot1qFutureStVlanEgressEthertype ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */



INT1
nmhSetDot1qFutureStVlanEgressEthertype ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */


INT1
nmhTestv2Dot1qFutureStVlanEgressEthertype ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureStVlanExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for Dot1qFutureVlanPortSubnetMapExtTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanPortSubnetMapExtTable ARG_LIST
((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanPortSubnetMapExtTabb
e  */

INT1
nmhGetFirstIndexDot1qFutureVlanPortSubnetMapExtTable ARG_LIST
((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanPortSubnetMapExtTable ARG_LIST
((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanPortSubnetMapExtVid ARG_LIST
((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortSubnetMapExtARPOption ARG_LIST
((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1qFutureVlanPortSubnetMapExtRowStatus ARG_LIST
((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanPortSubnetMapExtVid ARG_LIST
((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortSubnetMapExtARPOption ARG_LIST
((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDot1qFutureVlanPortSubnetMapExtRowStatus ARG_LIST
((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanPortSubnetMapExtVid ARG_LIST
((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortSubnetMapExtARPOption ARG_LIST
((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanPortSubnetMapExtRowStatus ARG_LIST
((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanPortSubnetMapExtTable ARG_LIST
((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanGlobalsFdbFlush ARG_LIST((INT4 *));

INT1
nmhGetDot1qFutureVlanUserDefinedTPID ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qFutureVlanGlobalsFdbFlush ARG_LIST((INT4 ));

INT1
nmhSetDot1qFutureVlanUserDefinedTPID ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qFutureVlanGlobalsFdbFlush ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qFutureVlanUserDefinedTPID ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qFutureVlanGlobalsFdbFlush ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qFutureVlanUserDefinedTPID ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qFutureVlanLoopbackTable. */
INT1
nmhValidateIndexInstanceDot1qFutureVlanLoopbackTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFutureVlanLoopbackTable  */

INT1
nmhGetFirstIndexDot1qFutureVlanLoopbackTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFutureVlanLoopbackTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFutureVlanLoopbackStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhSetDot1qFutureVlanLoopbackStatus  ARG_LIST((UINT4 ,INT4));

INT1
nmhTestv2Dot1qFutureVlanLoopbackStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhDepv2Dot1qFutureVlanLoopbackTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


