/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsvllw.h,v 1.6 2012/05/31 12:45:36 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsDot1qBaseTable. */
INT1
nmhValidateIndexInstanceFsDot1qBaseTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qBaseTable  */

INT1
nmhGetFirstIndexFsDot1qBaseTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qBaseTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qVlanVersionNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1qMaxVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1qMaxSupportedVlans ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot1qNumVlans ARG_LIST((INT4 ,UINT4 *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qBaseTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qFdbTable. */
INT1
nmhValidateIndexInstanceFsDot1qFdbTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qFdbTable  */

INT1
nmhGetFirstIndexFsDot1qFdbTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qFdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qFdbDynamicCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsDot1qTpFdbTable. */
INT1
nmhValidateIndexInstanceFsDot1qTpFdbTable ARG_LIST((INT4  , UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qTpFdbTable  */

INT1
nmhGetFirstIndexFsDot1qTpFdbTable ARG_LIST((INT4 * , UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qTpFdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qTpFdbPort ARG_LIST((INT4  , UINT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsDot1qTpFdbStatus ARG_LIST((INT4  , UINT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsDot1qTpFdbPw ARG_LIST((INT4  , UINT4  , tMacAddr ,UINT4 *));

/* Proto Validate Index Instance for FsDot1qTpGroupTable. */
INT1
nmhValidateIndexInstanceFsDot1qTpGroupTable ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qTpGroupTable  */

INT1
nmhGetFirstIndexFsDot1qTpGroupTable ARG_LIST((INT4 * , UINT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qTpGroupTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qTpGroupIsLearnt ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsDot1qForwardAllLearntPortTable. */
INT1
nmhValidateIndexInstanceFsDot1qForwardAllLearntPortTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qForwardAllLearntPortTable  */

INT1
nmhGetFirstIndexFsDot1qForwardAllLearntPortTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qForwardAllLearntPortTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qForwardAllIsLearnt ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsDot1qForwardAllStatusTable. */
INT1
nmhValidateIndexInstanceFsDot1qForwardAllStatusTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qForwardAllStatusTable  */

INT1
nmhGetFirstIndexFsDot1qForwardAllStatusTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qForwardAllStatusTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qForwardAllRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qForwardAllRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qForwardAllRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qForwardAllStatusTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qForwardAllPortConfigTable. */
INT1
nmhValidateIndexInstanceFsDot1qForwardAllPortConfigTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qForwardAllPortConfigTable  */

INT1
nmhGetFirstIndexFsDot1qForwardAllPortConfigTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qForwardAllPortConfigTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qForwardAllPort ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qForwardAllPort ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qForwardAllPort ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qForwardAllPortConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qForwardUnregLearntPortTable. */
INT1
nmhValidateIndexInstanceFsDot1qForwardUnregLearntPortTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qForwardUnregLearntPortTable  */

INT1
nmhGetFirstIndexFsDot1qForwardUnregLearntPortTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qForwardUnregLearntPortTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qForwardUnregIsLearnt ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsDot1qForwardUnregStatusTable. */
INT1
nmhValidateIndexInstanceFsDot1qForwardUnregStatusTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qForwardUnregStatusTable  */

INT1
nmhGetFirstIndexFsDot1qForwardUnregStatusTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qForwardUnregStatusTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qForwardUnregRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qForwardUnregRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qForwardUnregRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qForwardUnregStatusTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qForwardUnregPortConfigTable. */
INT1
nmhValidateIndexInstanceFsDot1qForwardUnregPortConfigTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qForwardUnregPortConfigTable  */

INT1
nmhGetFirstIndexFsDot1qForwardUnregPortConfigTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qForwardUnregPortConfigTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qForwardUnregPort ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qForwardUnregPort ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qForwardUnregPort ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qForwardUnregPortConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qStaticUnicastTable. */
INT1
nmhValidateIndexInstanceFsDot1qStaticUnicastTable ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qStaticUnicastTable  */

INT1
nmhGetFirstIndexFsDot1qStaticUnicastTable ARG_LIST((INT4 * , UINT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qStaticUnicastTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qStaticUnicastRowStatus ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4 ,INT4 *));

INT1
nmhGetFsDot1qStaticUnicastStatus ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qStaticUnicastRowStatus ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));

INT1
nmhSetFsDot1qStaticUnicastStatus ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qStaticUnicastRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));

INT1
nmhTestv2FsDot1qStaticUnicastStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qStaticUnicastTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qStaticAllowedToGoTable. */
INT1
nmhValidateIndexInstanceFsDot1qStaticAllowedToGoTable ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qStaticAllowedToGoTable  */

INT1
nmhGetFirstIndexFsDot1qStaticAllowedToGoTable ARG_LIST((INT4 * , UINT4 * , tMacAddr *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qStaticAllowedToGoTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qStaticAllowedIsMember ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qStaticAllowedIsMember ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qStaticAllowedIsMember ARG_LIST((UINT4 *  ,INT4  , UINT4  , tMacAddr  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qStaticAllowedToGoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qStaticMulticastTable. */
INT1
nmhValidateIndexInstanceFsDot1qStaticMulticastTable ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qStaticMulticastTable  */

INT1
nmhGetFirstIndexFsDot1qStaticMulticastTable ARG_LIST((INT4 * , UINT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qStaticMulticastTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qStaticMulticastRowStatus ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4 ,INT4 *));

INT1
nmhGetFsDot1qStaticMulticastStatus ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qStaticMulticastRowStatus ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));

INT1
nmhSetFsDot1qStaticMulticastStatus ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qStaticMulticastRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));

INT1
nmhTestv2FsDot1qStaticMulticastStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qStaticMulticastTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qStaticMcastPortTable. */
INT1
nmhValidateIndexInstanceFsDot1qStaticMcastPortTable ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qStaticMcastPortTable  */

INT1
nmhGetFirstIndexFsDot1qStaticMcastPortTable ARG_LIST((INT4 * , UINT4 * , tMacAddr *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qStaticMcastPortTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qStaticMcastPort ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qStaticMcastPort ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qStaticMcastPort ARG_LIST((UINT4 *  ,INT4  , UINT4  , tMacAddr  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qStaticMcastPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qVlanNumDeletesTable. */
INT1
nmhValidateIndexInstanceFsDot1qVlanNumDeletesTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qVlanNumDeletesTable  */

INT1
nmhGetFirstIndexFsDot1qVlanNumDeletesTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qVlanNumDeletesTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qVlanNumDeletes ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsDot1qVlanCurrentTable. */
INT1
nmhValidateIndexInstanceFsDot1qVlanCurrentTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qVlanCurrentTable  */

INT1
nmhGetFirstIndexFsDot1qVlanCurrentTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qVlanCurrentTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qVlanFdbId ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsDot1qVlanStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsDot1qVlanCreationTime ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsDot1qVlanEgressPortTable. */
INT1
nmhValidateIndexInstanceFsDot1qVlanEgressPortTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qVlanEgressPortTable  */

INT1
nmhGetFirstIndexFsDot1qVlanEgressPortTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qVlanEgressPortTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qVlanCurrentEgressPort ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsDot1qVlanStaticTable. */
INT1
nmhValidateIndexInstanceFsDot1qVlanStaticTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qVlanStaticTable  */

INT1
nmhGetFirstIndexFsDot1qVlanStaticTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qVlanStaticTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qVlanStaticName ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot1qVlanStaticRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qVlanStaticName ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDot1qVlanStaticRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qVlanStaticName ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDot1qVlanStaticRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qVlanStaticTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qVlanStaticPortConfigTable. */
INT1
nmhValidateIndexInstanceFsDot1qVlanStaticPortConfigTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qVlanStaticPortConfigTable  */

INT1
nmhGetFirstIndexFsDot1qVlanStaticPortConfigTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qVlanStaticPortConfigTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qVlanStaticPort ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qVlanStaticPort ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qVlanStaticPort ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qVlanStaticPortConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qNextFreeLocalVlanIndexTable. */
INT1
nmhValidateIndexInstanceFsDot1qNextFreeLocalVlanIndexTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qNextFreeLocalVlanIndexTable  */

INT1
nmhGetFirstIndexFsDot1qNextFreeLocalVlanIndexTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qNextFreeLocalVlanIndexTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qNextFreeLocalVlanIndex ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FsDot1qPortVlanTable. */
INT1
nmhValidateIndexInstanceFsDot1qPortVlanTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qPortVlanTable  */

INT1
nmhGetFirstIndexFsDot1qPortVlanTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qPortVlanTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qPvid ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot1qPortAcceptableFrameTypes ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1qPortIngressFiltering ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qPvid ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot1qPortAcceptableFrameTypes ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1qPortIngressFiltering ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qPvid ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot1qPortAcceptableFrameTypes ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1qPortIngressFiltering ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qPortVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qPortVlanStatisticsTable. */
INT1
nmhValidateIndexInstanceFsDot1qPortVlanStatisticsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qPortVlanStatisticsTable  */

INT1
nmhGetFirstIndexFsDot1qPortVlanStatisticsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qPortVlanStatisticsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qTpVlanPortInFrames ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsDot1qTpVlanPortOutFrames ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsDot1qTpVlanPortInDiscards ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsDot1qTpVlanPortInOverflowFrames ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsDot1qTpVlanPortOutOverflowFrames ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsDot1qTpVlanPortInOverflowDiscards ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsDot1qPortVlanHCStatisticsTable. */
INT1
nmhValidateIndexInstanceFsDot1qPortVlanHCStatisticsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qPortVlanHCStatisticsTable  */

INT1
nmhGetFirstIndexFsDot1qPortVlanHCStatisticsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qPortVlanHCStatisticsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qTpVlanPortHCInFrames ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsDot1qTpVlanPortHCOutFrames ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsDot1qTpVlanPortHCInDiscards ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for FsDot1qLearningConstraintsTable. */
INT1
nmhValidateIndexInstanceFsDot1qLearningConstraintsTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qLearningConstraintsTable  */

INT1
nmhGetFirstIndexFsDot1qLearningConstraintsTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qLearningConstraintsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qConstraintType ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsDot1qConstraintStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qConstraintType ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsDot1qConstraintStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qConstraintType ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsDot1qConstraintStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qLearningConstraintsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1qConstraintDefaultTable. */
INT1
nmhValidateIndexInstanceFsDot1qConstraintDefaultTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1qConstraintDefaultTable  */

INT1
nmhGetFirstIndexFsDot1qConstraintDefaultTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1qConstraintDefaultTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1qConstraintSetDefault ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1qConstraintTypeDefault ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1qConstraintSetDefault ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1qConstraintTypeDefault ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1qConstraintSetDefault ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1qConstraintTypeDefault ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1qConstraintDefaultTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1vProtocolGroupTable. */
INT1
nmhValidateIndexInstanceFsDot1vProtocolGroupTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsDot1vProtocolGroupTable  */

INT1
nmhGetFirstIndexFsDot1vProtocolGroupTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1vProtocolGroupTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1vProtocolGroupId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsDot1vProtocolGroupRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1vProtocolGroupId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsDot1vProtocolGroupRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1vProtocolGroupId ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsDot1vProtocolGroupRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1vProtocolGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1vProtocolPortTable. */
INT1
nmhValidateIndexInstanceFsDot1vProtocolPortTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1vProtocolPortTable  */

INT1
nmhGetFirstIndexFsDot1vProtocolPortTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1vProtocolPortTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1vProtocolPortGroupVid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsDot1vProtocolPortRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1vProtocolPortGroupVid ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsDot1vProtocolPortRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1vProtocolPortGroupVid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsDot1vProtocolPortRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1vProtocolPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
