/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: vlnpbstb.h,v 1.40 2016/03/23 12:27:00 siva Exp $
*
* Description: Stub function header file
*********************************************************************/
#ifndef _VLNPBSTB_H
#define _VLNPBSTB_H

#define VLAN_PB_PCP_ENCODE_PCP(pVlanPbPortEntry,u1PcpSelRow,u1Prio,u1DropEligible) \
   pVlanPbPortEntry->au1PcpEncoding[u1PcpSelRow-1][u1Prio][u1DropEligible]

VOID RegisterFSPB PROTO ((VOID));

VOID RegisterFSMPB PROTO ((VOID));

VOID RegisterFSMVLE PROTO ((VOID));

VOID RegisterSTDDOT PROTO ((VOID));

VOID RegisterFSVLNE PROTO ((VOID));

VOID RegisterFSDOT1 PROTO ((VOID));

INT4 VlanPbInit PROTO ((VOID));

VOID VlanPbDeInit PROTO ((VOID));

VOID
VlanPbUpdateSVlanClassifyParams PROTO ((tCRU_BUF_CHAIN_DESC *pFrame,
                               tVlanSVlanClassificationParams *pVlanSVlanIndex));

INT4
VlanPbGetServiceVlanId PROTO ((tVlanSVlanClassificationParams SVlanClassify,
                           tVlanId * pSVlanId));

VOID
VlanPbUpdateTagEtherType PROTO((tCRU_BUF_CHAIN_DESC * pFrame, 
                              UINT2 u2EtherType));

INT4
VlanPbConfEtherSwapEntry PROTO ((VOID));

INT4
VlanPbConfSVlanTranslationTable PROTO ((UINT2, UINT2));


INT4
VlanHandlePbRemovePortTablesFromHw PROTO((tVlanQMsg *, UINT2 , UINT2 ));

VOID
VlanPbConfHwSVlanClassificationTable PROTO ((VOID));


VOID
VlanPbRemoveHwSVlanClassificationTable PROTO((VOID));

VOID
VlanPbRemoveHwSVlanTranslationTable PROTO((UINT2, UINT2));

INT4
VlanPbRemoveSVlanClassificationEntriesForPort PROTO((UINT2 u2Port));

INT4
VlanPbRemoveSVlanTranslationEntriesForPort PROTO ((UINT2 u2Port));

INT4
VlanPbRemoveEtherTypeSwapEntriesForPort PROTO ((UINT2 u2Port));

UINT1
VlanPbProcessFrame PROTO ((UINT2 u2Port,tCRU_BUF_CHAIN_DESC *pFrame,
                   tVlanIfMsg *pVlanMsg, tVlanId *pCustomerVlan, tVlanId *pServiceVlan,
                   UINT1 *pu1Action));

VOID  VlanPbUpdateTagInFrame PROTO ((tCRU_BUF_CHAIN_DESC *pFrame,
                                     tVlanPortEntry* pOutPortEntry, 
                                     tVlanId SVlanId));

INT4
cli_process_pb_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

INT4
cli_process_pb_show_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command, ...));

VOID
VlanPbSetDefPbPortInfo PROTO ((tVlanPortEntry* pPortEntry));

VOID
VlanPbHandleResetPbPortPropertiesToHw (UINT2 u2Port);

INT4
VlanPbHandleCopyPbPortPropertiesToHw PROTO ((UINT2 u2DstPort, 
                                           UINT2 u2SrcPort, 
                                           UINT1 u1InterfaceType));

VOID
VlanPbTagFrame PROTO((tCRU_BUF_CHAIN_DESC * pFrame,
                      tVlanId VlanId, UINT1 u1Priority));


INT4 VlanPbShowRunningConfigInterfaceDetails PROTO((tCliHandle, INT4, UINT1, UINT1 *pu1HeadFlag));

tVlanId
VlanPbGetTranslatedVlanBasedOnRelaySVlan PROTO ((UINT2 u2Port,
                                               tVlanId u2RelaySVlan));

VOID
VlanPbRemoveHwSVlanEtherTypeSwapTable PROTO ((VOID));

VOID
VlanPbConfHwFidProperties PROTO ((VOID));

UINT1 VlanPbIsPortMacLearnLimitExceeded PROTO ((tVlanPbPortEntry *));

UINT1 VlanPbGetPortMacLearningStatus PROTO ((tVlanPbPortEntry *));

UINT1 VlanPbIsMulticastMacLimitExceeded PROTO ((VOID));

VOID VlanPbUpdateCurrentMulticastMacCount PROTO ((UINT1));

UINT1 VlanPbHandleNoSVlanIfCep PROTO ((tVlanPortEntry*)); 

INT4
VlanPbAllocateFdbControl PROTO ((VOID));

INT4
VlanPbReleaseFdbControl PROTO ((VOID));

INT4
VlanPbShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 *pu1HeadFlag);

UINT1* VlanPbGetMemBlock PROTO ((UINT1 u1BlockType));

INT4  VlanPbHandlePktOnPep (tCRU_BUF_CHAIN_DESC *pFrame, tVlanIfMsg *pVlanIf);
INT4
VlanPbHandlePktRxOnCep (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                        tVlanTag *pVlanTag);
INT4
VlanPbHandlePktTxOnCep (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port, tVlanId SVlanId, 
                        tVlanIfMsg *pVlanIf, UINT1 u1IsCustBpdu);

VOID  VlanPbReleaseMemBlock PROTO ((UINT1 u1BlockType, UINT1* pu1Buf));

VOID VlanPbUpdatePepOperStatusToAst PROTO((VOID));

INT4 VlanPbValidatePortType (UINT2 u2LocalPort, UINT1 u1PbPortType);

INT1 VlanPbSetDot1adPortType (tVlanPortEntry *pVlanPortEntry, 
                              UINT1 u1PbPortType);

VOID VlanPbInitPcpValues (UINT2 u2Port);
VOID
VlanPbClassifySVlan (tCRU_BUF_CHAIN_DESC * pFrame,UINT2 u2Port,
                     UINT1 u1PktType, tVlanId CVlanId, tVlanId * pSVlanId);

VOID VlanPbNotifySVlanStatusForPep PROTO((tVlanId SVlanId, UINT1 u1RowStatus));
INT4 
VlanPbEtherTypeSwap (tCRU_BUF_CHAIN_DESC *pFrame, UINT2 u2Port, UINT2 u2EtherType);
VOID
VlanPbGetPcpDecodeVal (UINT2 u2Port, UINT1 u1InPcp, UINT1 *pu1Priority,
                       UINT1 *pu1DropEligible);
INT4
DisplayPbServiceVlanInfo (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4VlanId,
            UINT1 *pu1isActiveVlan);

VOID
VlanPbShowRunningConfigPerVlanTable (tCliHandle CliHandle,
                                     UINT4 u4CurrentContextId,
                                     UINT4 i4CurrentVlanId, UINT1 *pu1HeadFlag);

INT4 VlanPbInitProviderBridge PROTO((VOID));

INT4
VlanPbValidateBridgeMode (INT4 i4BridgeMode, UINT4 *pu4ErrorCode);


INT4
VlanPbCheckVlanServiceType (tVlanId VlanId, tLocalPortList InPorts); 

INT4
VlanPbRemovePortCVidEntries PROTO((UINT2 u2Port));



VOID 
VlanShowProtocolTunnelStatus (tCliHandle CliHandle,INT4 i4PortId);

VOID RegisterFSM1AD PROTO((VOID));

INT4
VlanPbCliShowL2ProtocolTunnel (tCliHandle CliHandle, UINT4 u4ContextId,
                                 UINT4 u4Port, UINT1 u1Summary);

INT4
VlanPbIsMacLearningAllowed PROTO((tVlanFdbEntry *pFdbEntry, 
                                 tVlanPortEntry *pVlanPortEntry));

VOID
VlanPbUpdateLearntMacCount PROTO((tVlanPortEntry *pVlanPortEntry,
                            tVlanFdbEntry *pFdbEntry,
                            UINT1 u1Flag));

INT4 VlanPbGetBridgeMode (INT4 i4Context, INT4 *pi4BridgeMode);

BOOL1 
VlanPbCheckEtherTypeSwap (UINT2 u2Port, UINT2 u2EtherType);

VOID
VlanGetCVlansFromCVlanSVlanTable PROTO ((UINT4 u4TblIndex,
                                         UINT2 u2Port, tVlanId SVlanId,
                                         tCVlanInfo *pCVlanInfo));

UINT4 VlanPbGetSVlanTableIndex PROTO ((INT4 i4TableType));

INT4
VlanGetPrimaryContextEtherType(UINT4 u4IfIndex, UINT2 *pu2EtherValue, 
          UINT1 u1EtherType);

#ifdef MBSM_WANTED
INT4 VlanMbsmUpdtPbPortPropertiesToHw (tMbsmSlotInfo * pSlotInfo,
                                       UINT2 u2PortNum);

INT4 VlanMbsmUpdtPepStatusToHw (UINT2 u2Port, tMbsmSlotInfo * pSlotInfo);

INT4 VlanMbsmUpdateCvidEntriesToHw (UINT2 u2Port, UINT4 u4SVlanId,
                                    tMbsmSlotInfo * pSlotInfo);

INT4 VlanMbsmUpdateVlanSwapTableToHw (UINT2 u2Port, tMbsmSlotInfo * pSlotInfo);

INT4 VlanMbsmUpdtMcastMacLimitToHw (tMbsmSlotInfo * pSlotInfo);
#endif

#endif

