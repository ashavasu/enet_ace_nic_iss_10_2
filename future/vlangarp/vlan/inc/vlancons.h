/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlancons.h,v 1.108 2018/01/19 12:41:34 siva Exp $
 *
 * Description: This file contains VLAN constant definitions.
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*                                                                           */
/*  FILE NAME             : vlancons.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN                                             */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains constant definitions used in  */
/*                          VLAN Module.                                     */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#ifndef _VLANCONS_H
#define _VLANCONS_H


#define VLAN_INIT_VAL  0

#define VLAN_EQUAL     1
#define VLAN_LESSER    2
#define VLAN_GREATER   3
#define VLAN_NOT_EQUAL 4

#define VLAN_RBTREE_KEY_EQUAL   0
#define VLAN_RBTREE_KEY_LESSER  -1
#define VLAN_RBTREE_KEY_GREATER 1

#define VLAN_VALID   1
#define VLAN_INVALID 2

#define VLAN_RELAY_CVLAN_INVALID -1 
#define VLAN_ZERO             0 /*To use zero in the code*/

#define VLAN_MGMT_VLAN_ID     4094

#define VLAN_LLDP_PORT_NOTIFY_TRUE   1
#define VLAN_LLDP_PORT_NOTIFY_FALSE  0

#define VLAN_DEF_FID_INDEX    0
#define VLAN_MIN_TRACE_VAL           0
#define VLAN_MAX_TRACE_VAL           0x0007FFFF

#define VLAN_MIN_CONSTRAINT_SET        0
#define VLAN_MAX_CONSTRAINT_SET        65535

#define VLAN_MAX_CONSTRAINTS           25
#define VLAN_INVALID_CONSTRAINT_INDEX  (VLAN_MAX_CONSTRAINTS + 1)

#define VLAN_MAX_COUNTER_SIZE          0xFFFFFFFF

#define VLAN_CONTEXT_ALIAS_LEN  L2IWF_CONTEXT_ALIAS_LEN

#define VLAN_MAC_ADDR_LEN   6
#define VLAN_INVALID_PORT_INDEX (VLAN_MAX_PORTS + 1)
 /*Invalid VLAN ID is VLAN_MAX_VLAN_ID + 1 i.e. 4095 */
#define VLAN_INVALID_VLAN_INDEX      (VLAN_MAX_VLAN_ID + 1)
#define VLAN_INVALID_FID_INDEX       (VLAN_MAX_FID_ENTRIES + 1)

#define VLAN_VERSION 1

#define VLAN_MAX_FDB_ID       VLAN_MAX_VLAN_ID 

#define VLAN_ALL_PORTS    0

#define VLAN_MAX_PORT_PROTOCOL_ENTRIES   VLAN_MAX_PROTO_GROUP_ENTRIES


#define VLAN_UNTAGGED_FRAME_MAXIMUM_LENGTH 1514
#define VLAN_SINGLE_TAGGED_FRAME_LENGTH 1518

#define VLAN_ENET_SPEED_1G   1000000000
/* For different size of the protocol values */
#define VLAN_2BYTE_PROTO_TEMP_SIZE 2   
#define VLAN_5BYTE_PROTO_TEMP_SIZE 5   

/* The protocol value string length
 * VLAN_STR_MAX_PROTO_LEN = (2 *VLAN_MAX_PROTO_SIZE) + 4 dots */
#define VLAN_STR_MAX_PROTO_LEN 14    

/* General Protocol values  */
#define VLAN_FRAME_IPX_RAW_802_3 0xFFFF       
#define VLAN_FRAME_IP_802_3 0xFEFE       

/* Protocol length of the frames */
#define VLAN_FRAME_SNAP_OTHER_LENGTH 3 
#define VLAN_FRAME_RFC_1042_LENGTH 6 
#define VLAN_FRAME_SNAP_802_1H_LENGTH 6 

/* Protocol Address length in ARP packet */
#define VLAN_ARP_PA_LENGTH_OFFSET     6
#define VLAN_IP_DEF_NET_MASK                  0xffffffff
#define VLAN_IP_CLASS_A_DEF_MASK              0xff000000
#define VLAN_IP_CLASS_B_DEF_MASK              0xffff0000
#define VLAN_IP_CLASS_C_DEF_MASK              0xffffff00

/* Offset values for extracting the protocol */
#define VLAN_2BYTE_OFFSET  2        
#define VLAN_FRAME_IPX_RAW_802_3_OFFSET VLAN_2BYTE_OFFSET 
#define VLAN_FRAME_RFC_1042_OFFSET (VLAN_2BYTE_OFFSET + VLAN_FRAME_RFC_1042_LENGTH)   
#define VLAN_FRAME_SNAP_802_1H_OFFSET (VLAN_2BYTE_OFFSET + VLAN_FRAME_SNAP_802_1H_LENGTH)      
#define VLAN_FRAME_SNAP_OTHER_OFFSET (VLAN_2BYTE_OFFSET + VLAN_FRAME_SNAP_OTHER_LENGTH)      


#define VLAN_PB_MAX_PCP_SEL_ROW       4
#define VLAN_PB_MAX_PRIORITY          8
#define VLAN_PB_MAX_DROP_ELIGIBLE     2
#define VLAN_PB_MAX_PCP               8

#define VLAN_PB_PORT_BASED    1
#define VLAN_PB_STAGGED       2

#define VLAN_DEF_CONSTRAINT_SET          1
#define VLAN_DEF_CONSTRAINT_TYPE         VLAN_INDEP_LEARNING

#define VLAN_DEF_VLAN_ID        gVlanDefVlanId
#define VLAN_DEFAULT_PORT_VID   VLAN_DEF_VLAN_ID
#define VLAN_MGMT_PORT_VID      4094
/*Default Port Vlan ID for Pseudowire*/
#define VLAN_L2VPN_DEF_PORT_VID 4097

#define VLAN_HC_PORT_SPEED   650000000
#define VLAN_MIN_PRIORITY  VLAN_DEF_USER_PRIORITY
#define VLAN_DEF_USER_PRIORITY 0

#define VLAN_OVERRIDE_OPTION_ENABLE  1
#define VLAN_OVERRIDE_OPTION_DISABLE 2

#ifdef NPAPI_WANTED
#define VLAN_MAX_TRAFF_CLASS  VLAN_DEV_MAX_NUM_COSQ 
#else
#define VLAN_MAX_TRAFF_CLASS   8
#endif

#define VLAN_ID_CFI_MASK            0x1FFF

#define VLAN_DATA_PACKET            1
#define VLAN_RESERVED_PACKET        2
#define VLAN_STP_PACKET             3
#define VLAN_GVRP_PACKET            4
#define VLAN_GMRP_PACKET            5
#define VLAN_MVRP_PACKET            6
#define VLAN_MMRP_PACKET            7

#define VLAN_LLC_HEADER_LEN 3
#define VLAN_DISP_ALL_INTERFACES 0

#define VLAN_DEFAULT_BRIDGE_MODE            VLAN_CUSTOMER_BRIDGE_MODE

#define VLAN_DEF_TUNNEL_BPDU_PRIORITY       VLAN_HIGHEST_PRIORITY
#define VLAN_DEFAULT_PKT_SIZE    1522

/* Default Trafffic class NPAPI_WANTED */
#define  VLAN_DEF_TRAFF_CLASS  1


#define VLAN_IP                       1
#define VLAN_IPX                      2
#define VLAN_RAW_IPX                  3
#define VLAN_ARP                      4
#define VLAN_RARP                     5
#define VLAN_NETBIOS                  6
#define VLAN_APPLETALK                7
#define VLAN_PROTO_OTHER              8

#define VLAN_DISP_MAC_ALL      1
#define VLAN_DISP_MAC_AGEING   2
#define VLAN_DISP_MAC_COUNT    3

#define VLAN_DYNAMIC_ENTRY  1
#define VLAN_STATIC_ENTRY   2

#define VLAN_PORT_STRING_LEN 10
#define VLAN_AGEOUT_TIMER   1
#define VLAN_PRIORITY_TIMER 2
#define VLAN_RED_RELEARN_TIMER    3
#define VLAN_RED_PERIODIC_TIMER   4
#define VLAN_MBSM_TIMER 5

/*********** VLAN Messages ***********************/

#define VLAN_PORT_CREATE_MSG              1
#define VLAN_PORT_DELETE_MSG              2
#define VLAN_PORT_OPER_UP_MSG             3
#define VLAN_PORT_OPER_DOWN_MSG           4
#define VLAN_COPY_PORT_INFO_MSG           7
#define VLAN_REMOVE_PORT_INFO_MSG         8
#define VLAN_COPY_PORT_MCAST_INFO_MSG     9 
#define VLAN_REMOVE_PORT_MCAST_INFO_MSG   10

#ifdef SW_AGE_TIMER
#define VLAN_SHORT_AGEOUT_MSG             11
#define VLAN_LONG_AGEOUT_MSG              12
#endif
#ifndef NPAPI_WANTED
#define VLAN_DELETE_FDB_MSG               5
#define VLAN_FLUSH_FDB_MSG                6
#define VLAN_FLUSH_FDBID_MSG              13
#define VLAN_FLUSH_FDB_LIST_MSG           35
#endif

#define VLAN_RM_MSG               14    /* For messages from RM -> VLAN */
#define VLAN_DYN_DATA_APPLIED_MSG 15    /* Message from GARP -> VLAN */

/* MI related messages */
#define VLAN_CREATE_CONTEXT_MSG       16
#define VLAN_DELETE_CONTEXT_MSG       17
#define VLAN_PORT_MAP_MSG         18
#define VLAN_PORT_UNMAP_MSG       19


#define VLAN_MODULE_SHUTDOWN              20 
#define VLAN_MODULE_START                 21

/* Ucast Port properties related */
#define VLAN_COPY_PORT_UCAST_INFO_MSG     22
#define VLAN_REMOVE_PORT_UCAST_INFO_MSG   23

/* MPLS - VLAN related messages */
#define VLAN_ADD_L2VPN_INFO     24
#define VLAN_DEL_L2VPN_INFO     25

/* Delete port from port channel and copy port-channel properties to port */
#define VLAN_PORT_DEL_COPY_PROPERTIES_MSG 26 

#define VLAN_FDBENTRY_DELETE_MSG    27
#define VLAN_UPDATE_CONTEXT_NAME    28
#define VLAN_PORT_P2P_UPDATE_MSG          29
#define VLAN_PORT_STP_STATE_UPDATE_MSG    30
#define VLAN_FDBENTRY_ALL_DELETE_MSG      31
#define VLAN_FDBENTRY_ADD_MSG    33

/* Interface Type Change Message */
#define VLAN_UPDATE_INTF_TYPE_MSG   34

/* STP Status Change Message*/
#define VLAN_STP_STATUS_CHG_MSG     36
#define VLAN_ICCH_MSG               37
#define VLAN_MCAG_FDBENTRY          38
#define VLAN_EVB_DEF_SBP_CREATION   39
#define VLAN_EVB_CDCP_LLDP_MSG      40
#define VLAN_MCLAG_ENABLE_STATUS    41
#define VLAN_MCLAG_DISABLE_STATUS    42
#define VLAN_ICCH_SYNC_MSG           43
#define VLAN_ICCH_SYNC_MSG_LEN       51

/*VLAN Message from IGS  */
#define VLAN_FORWARD_UNREG_PORTS_MSG 43
/*****************************************************/


#define VLAN_GROUP_DYN_INFO_PRESENT     1
#define VLAN_GROUP_DYN_INFO_NOT_PRESENT 2

#define VLAN_RED_GVRP_INFO    1
#define VLAN_RED_GMRP_INFO    2

#define VLAN_NUM_SVLAN_CLASSIFY_TABLES   11

#define VLAN_DT_FRAME                    1 
#define VLAN_ST_FRAME                    2
#define VLAN_UNTAGGED_FRAME              3

#define VLAN_CUSTOMER_PROTOCOL_ID      0x8100

#define VLAN_IGMP_CONTROL_PKT           2


#define VLAN_NORMAL_AGEOUT_TIME    SEC_TO_STAP(30000)
#define VLAN_MIN_AGEOUT_TIME        10            
#define VLAN_MAX_AGEOUT_TIME        1000000       
#define VLAN_FDB_TIME_TO_MGMT(x)      (x)
#define VLAN_MGMT_TO_FDB_TIME(x)      (x)
#define BRG_HIGH_SPEED                650000000

#define VLAN_SIZING_CONTEXT_COUNT  (FsVLANSizingParams[MAX_VLAN_CONTEXT_INFO_SIZING_ID].u4PreAllocatedUnits)
#define VLAN_MAX_CONTEXTS         VLAN_SIZING_CONTEXT_COUNT
#define VLAN_INVALID_CONTEXT        (VLAN_SIZING_CONTEXT_COUNT + 1)

#define VLAN_CLI_INVALID_CONTEXT   0xFFFFFFFF
#define VLAN_SWITCH_ALIAS_LEN      L2IWF_CONTEXT_ALIAS_LEN

#define VLAN_MAX_TEMP_PORTLIST     1024 
#define VLAN_MISC_BLK_CNT          4

#define VLAN_FORWARD_ALL_TABLE     1
#define VLAN_FORWARD_UNREG_TABLE   2
#define VLAN_ST_UCAST_TABLE        3
#define VLAN_ST_MCAST_TABLE        4
#define VLAN_WILDCARD_TABLE        5

#define VLAN_NON_MEMBER_PORT       0
#define VLAN_ADD_MEMBER_PORT       1
#define VLAN_ADD_FORBIDDEN_PORT    2
#define VLAN_DEL_MEMBER_PORT       3
#define VLAN_DEL_FORBIDDEN_PORT    4

#define VLAN_ADD_TAGGED_PORT       1
#define VLAN_ADD_UNTAGGED_PORT     2
#define VLAN_ADD_ST_FORBIDDEN_PORT 3
#define VLAN_DEL_TAGGED_PORT       4
#define VLAN_DEL_UNTAGGED_PORT     5
#define VLAN_DEL_ST_FORBIDDEN_PORT 6
#define VLAN_TRAP_OID_LEN               12
#define VLAN_OBJ_NAME_LEN               257
#define VLAN_SRC_RELEARN_TRAP_TYPE      2
#define VLAN_SWITCH_MAC_LIMIT_TRAP_TYPE 3

#define VLAN_STAT_UCAST_IN_FRAMES 1
#define VLAN_STAT_MCAST_BCAST_IN_FRAMES 2
#define VLAN_STAT_UNKNOWN_UCAST_OUT_FRAMES 3
#define VLAN_STAT_UCAST_OUT_FRAMES 4
#define VLAN_STAT_BCAST_OUT_FRAMES 5
#define VLAN_STAT_VLAN_IN_FRAMES 6
#define VLAN_STAT_VLAN_IN_BYTES 7
#define VLAN_STAT_VLAN_OUT_FRAMES 8
#define VLAN_STAT_VLAN_OUT_BYTES 9
#define VLAN_STAT_VLAN_DISCARD_FRAMES 10 
#define VLAN_STAT_VLAN_DISCARD_BYTES 11
#define VLAN_RED_GARP_ENTRIES      1

#define VLAN_PVLAN_MAX_BLKS 3

#define VLAN_MAX_NUM_PORT_ARRAYS_PER_NP   4

#define VLAN_UNSET_RESTORE_FLAG    1
#define VLAN_SET_RESTORE_FLAG      2

#ifdef MPLS_WANTED
#define VLAN_L2VPN_MAP_ENTRY       1
#endif /*MPLS_WANTED*/

#define  UNKNOWN_BRIDGE      1 
#define  TRANSPARENT_BRIDGE  2     /* Only this value is used */
#define  SOURCE_ROUTE        3 

#define VLAN_SHOW_SINGLE_CTXT 1
#define VLAN_SHOW_ALL_CTXT 2
#define VLAN_MAX_IFNAME_LEN 20

#define VLAN_PORT_PROPERTY_MAC_LEARN     1
#define VLAN_PORT_PROPERTY_EGRESS        2
#define VLAN_PORT_PROPERTY_UNTAGGED      4
#define VLAN_PORT_PROPERTY_FORBIDDEN     8
#define VLAN_PORT_PROPERTY_CUR_EGRESS    16
#define VLAN_PORT_PROPERTY_CUR_LEARNT    32
#define VLAN_PORT_PROPERTY_CUR_TRUNK     64

#ifdef ICCH_WANTED
#define VLAN_LA_PORT_UP_IN_BNDL     0
#define VLAN_LA_PORT_DOWN           2
#endif

#define VLAN_MAX_FDB_DEL_PER_BATCH  10
#define VLAN_MAX_FDB_SW_DEL_PER_BATCH 40 * VLAN_MAX_FDB_DEL_PER_BATCH

/* EVB Queue Message constants */
#define VLAN_EVB_MAX_OUI_LEN                    3
#define VLAN_EVB_CDCP_TLV_LEN                   512

/* Offset to access port-channel number from port-channel type */
#define VLAN_MCLAG_AGG_NUM_OFFSET               2
#endif
