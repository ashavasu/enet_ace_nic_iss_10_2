/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbpdb.h,v 1.4 2008/08/20 15:29:50 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSPBDB_H
#define _FSPBDB_H

UINT1 FsPbPortInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPbSrcMacSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsPbDstMacSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsPbCVlanSrcMacSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsPbCVlanDstMacSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsPbDscpSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPbCVlanDscpSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPbSrcIpAddrSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsPbDstIpAddrSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsPbSrcDstIpSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsPbCVlanDstIpSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsPbPortBasedCVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPbEtherTypeSwapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPbSVlanConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsPbTunnelProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPbTunnelProtocolStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPbPepExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fspb [] ={1,3,6,1,4,1,2076,113};
tSNMP_OID_TYPE fspbOID = {8, fspb};


UINT4 FsPbMulticastMacLimit [ ] ={1,3,6,1,4,1,2076,113,1,1};
UINT4 FsPbTunnelStpAddress [ ] ={1,3,6,1,4,1,2076,113,1,2};
UINT4 FsPbTunnelLacpAddress [ ] ={1,3,6,1,4,1,2076,113,1,3};
UINT4 FsPbTunnelDot1xAddress [ ] ={1,3,6,1,4,1,2076,113,1,4};
UINT4 FsPbTunnelGvrpAddress [ ] ={1,3,6,1,4,1,2076,113,1,5};
UINT4 FsPbTunnelGmrpAddress [ ] ={1,3,6,1,4,1,2076,113,1,6};
UINT4 FsPbPort [ ] ={1,3,6,1,4,1,2076,113,2,1,1,1};
UINT4 FsPbPortSVlanClassificationMethod [ ] ={1,3,6,1,4,1,2076,113,2,1,1,2};
UINT4 FsPbPortSVlanIngressEtherType [ ] ={1,3,6,1,4,1,2076,113,2,1,1,3};
UINT4 FsPbPortSVlanEgressEtherType [ ] ={1,3,6,1,4,1,2076,113,2,1,1,4};
UINT4 FsPbPortSVlanEtherTypeSwapStatus [ ] ={1,3,6,1,4,1,2076,113,2,1,1,5};
UINT4 FsPbPortSVlanTranslationStatus [ ] ={1,3,6,1,4,1,2076,113,2,1,1,6};
UINT4 FsPbPortUnicastMacLearning [ ] ={1,3,6,1,4,1,2076,113,2,1,1,7};
UINT4 FsPbPortUnicastMacLimit [ ] ={1,3,6,1,4,1,2076,113,2,1,1,8};
UINT4 FsPbSrcMacAddress [ ] ={1,3,6,1,4,1,2076,113,2,2,1,1};
UINT4 FsPbSrcMacSVlan [ ] ={1,3,6,1,4,1,2076,113,2,2,1,2};
UINT4 FsPbSrcMacRowStatus [ ] ={1,3,6,1,4,1,2076,113,2,2,1,3};
UINT4 FsPbDstMacAddress [ ] ={1,3,6,1,4,1,2076,113,2,3,1,1};
UINT4 FsPbDstMacSVlan [ ] ={1,3,6,1,4,1,2076,113,2,3,1,2};
UINT4 FsPbDstMacRowStatus [ ] ={1,3,6,1,4,1,2076,113,2,3,1,3};
UINT4 FsPbCVlanSrcMacCVlan [ ] ={1,3,6,1,4,1,2076,113,2,4,1,1};
UINT4 FsPbCVlanSrcMacAddr [ ] ={1,3,6,1,4,1,2076,113,2,4,1,2};
UINT4 FsPbCVlanSrcMacSVlan [ ] ={1,3,6,1,4,1,2076,113,2,4,1,3};
UINT4 FsPbCVlanSrcMacRowStatus [ ] ={1,3,6,1,4,1,2076,113,2,4,1,4};
UINT4 FsPbCVlanDstMacCVlan [ ] ={1,3,6,1,4,1,2076,113,2,5,1,1};
UINT4 FsPbCVlanDstMacAddr [ ] ={1,3,6,1,4,1,2076,113,2,5,1,2};
UINT4 FsPbCVlanDstMacSVlan [ ] ={1,3,6,1,4,1,2076,113,2,5,1,3};
UINT4 FsPbCVlanDstMacRowStatus [ ] ={1,3,6,1,4,1,2076,113,2,5,1,4};
UINT4 FsPbDscp [ ] ={1,3,6,1,4,1,2076,113,2,6,1,1};
UINT4 FsPbDscpSVlan [ ] ={1,3,6,1,4,1,2076,113,2,6,1,2};
UINT4 FsPbDscpRowStatus [ ] ={1,3,6,1,4,1,2076,113,2,6,1,3};
UINT4 FsPbCVlanDscpCVlan [ ] ={1,3,6,1,4,1,2076,113,2,7,1,1};
UINT4 FsPbCVlanDscp [ ] ={1,3,6,1,4,1,2076,113,2,7,1,2};
UINT4 FsPbCVlanDscpSVlan [ ] ={1,3,6,1,4,1,2076,113,2,7,1,3};
UINT4 FsPbCVlanDscpRowStatus [ ] ={1,3,6,1,4,1,2076,113,2,7,1,4};
UINT4 FsPbSrcIpAddr [ ] ={1,3,6,1,4,1,2076,113,2,8,1,1};
UINT4 FsPbSrcIpSVlan [ ] ={1,3,6,1,4,1,2076,113,2,8,1,2};
UINT4 FsPbSrcIpRowStatus [ ] ={1,3,6,1,4,1,2076,113,2,8,1,3};
UINT4 FsPbDstIpAddr [ ] ={1,3,6,1,4,1,2076,113,2,9,1,1};
UINT4 FsPbDstIpSVlan [ ] ={1,3,6,1,4,1,2076,113,2,9,1,2};
UINT4 FsPbDstIpRowStatus [ ] ={1,3,6,1,4,1,2076,113,2,9,1,3};
UINT4 FsPbSrcDstSrcIpAddr [ ] ={1,3,6,1,4,1,2076,113,2,10,1,1};
UINT4 FsPbSrcDstDstIpAddr [ ] ={1,3,6,1,4,1,2076,113,2,10,1,2};
UINT4 FsPbSrcDstIpSVlan [ ] ={1,3,6,1,4,1,2076,113,2,10,1,3};
UINT4 FsPbSrcDstIpRowStatus [ ] ={1,3,6,1,4,1,2076,113,2,10,1,4};
UINT4 FsPbCVlanDstIpCVlan [ ] ={1,3,6,1,4,1,2076,113,2,11,1,1};
UINT4 FsPbCVlanDstIp [ ] ={1,3,6,1,4,1,2076,113,2,11,1,2};
UINT4 FsPbCVlanDstIpSVlan [ ] ={1,3,6,1,4,1,2076,113,2,11,1,3};
UINT4 FsPbCVlanDstIpRowStatus [ ] ={1,3,6,1,4,1,2076,113,2,11,1,4};
UINT4 FsPbPortCVlan [ ] ={1,3,6,1,4,1,2076,113,2,12,1,1};
UINT4 FsPbPortCVlanClassifyStatus [ ] ={1,3,6,1,4,1,2076,113,2,12,1,2};
UINT4 FsPbLocalEtherType [ ] ={1,3,6,1,4,1,2076,113,2,13,1,1};
UINT4 FsPbRelayEtherType [ ] ={1,3,6,1,4,1,2076,113,2,13,1,2};
UINT4 FsPbEtherTypeSwapRowStatus [ ] ={1,3,6,1,4,1,2076,113,2,13,1,3};
UINT4 FsPbSVlanConfigServiceType [ ] ={1,3,6,1,4,1,2076,113,2,14,1,1};
UINT4 FsPbTunnelProtocolDot1x [ ] ={1,3,6,1,4,1,2076,113,2,15,1,1};
UINT4 FsPbTunnelProtocolLacp [ ] ={1,3,6,1,4,1,2076,113,2,15,1,2};
UINT4 FsPbTunnelProtocolStp [ ] ={1,3,6,1,4,1,2076,113,2,15,1,3};
UINT4 FsPbTunnelProtocolGvrp [ ] ={1,3,6,1,4,1,2076,113,2,15,1,4};
UINT4 FsPbTunnelProtocolGmrp [ ] ={1,3,6,1,4,1,2076,113,2,15,1,5};
UINT4 FsPbTunnelProtocolIgmp [ ] ={1,3,6,1,4,1,2076,113,2,15,1,6};
UINT4 FsPbTunnelProtocolDot1xPktsRecvd [ ] ={1,3,6,1,4,1,2076,113,2,16,1,1};
UINT4 FsPbTunnelProtocolDot1xPktsSent [ ] ={1,3,6,1,4,1,2076,113,2,16,1,2};
UINT4 FsPbTunnelProtocolLacpPktsRecvd [ ] ={1,3,6,1,4,1,2076,113,2,16,1,3};
UINT4 FsPbTunnelProtocolLacpPktsSent [ ] ={1,3,6,1,4,1,2076,113,2,16,1,4};
UINT4 FsPbTunnelProtocolStpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,113,2,16,1,5};
UINT4 FsPbTunnelProtocolStpPDUsSent [ ] ={1,3,6,1,4,1,2076,113,2,16,1,6};
UINT4 FsPbTunnelProtocolGvrpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,113,2,16,1,7};
UINT4 FsPbTunnelProtocolGvrpPDUsSent [ ] ={1,3,6,1,4,1,2076,113,2,16,1,8};
UINT4 FsPbTunnelProtocolGmrpPktsRecvd [ ] ={1,3,6,1,4,1,2076,113,2,16,1,9};
UINT4 FsPbTunnelProtocolGmrpPktsSent [ ] ={1,3,6,1,4,1,2076,113,2,16,1,10};
UINT4 FsPbTunnelProtocolIgmpPktsRecvd [ ] ={1,3,6,1,4,1,2076,113,2,16,1,11};
UINT4 FsPbTunnelProtocolIgmpPktsSent [ ] ={1,3,6,1,4,1,2076,113,2,16,1,12};
UINT4 FsPbPepExtCosPreservation [ ] ={1,3,6,1,4,1,2076,113,2,17,1,1};


tMbDbEntry fspbMibEntry[]= {

{{10,FsPbMulticastMacLimit}, NULL, FsPbMulticastMacLimitGet, FsPbMulticastMacLimitSet, FsPbMulticastMacLimitTest, FsPbMulticastMacLimitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPbTunnelStpAddress}, NULL, FsPbTunnelStpAddressGet, FsPbTunnelStpAddressSet, FsPbTunnelStpAddressTest, FsPbTunnelStpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPbTunnelLacpAddress}, NULL, FsPbTunnelLacpAddressGet, FsPbTunnelLacpAddressSet, FsPbTunnelLacpAddressTest, FsPbTunnelLacpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPbTunnelDot1xAddress}, NULL, FsPbTunnelDot1xAddressGet, FsPbTunnelDot1xAddressSet, FsPbTunnelDot1xAddressTest, FsPbTunnelDot1xAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPbTunnelGvrpAddress}, NULL, FsPbTunnelGvrpAddressGet, FsPbTunnelGvrpAddressSet, FsPbTunnelGvrpAddressTest, FsPbTunnelGvrpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsPbTunnelGmrpAddress}, NULL, FsPbTunnelGmrpAddressGet, FsPbTunnelGmrpAddressSet, FsPbTunnelGmrpAddressTest, FsPbTunnelGmrpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsPbPort}, GetNextIndexFsPbPortInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsPbPortSVlanClassificationMethod}, GetNextIndexFsPbPortInfoTable, FsPbPortSVlanClassificationMethodGet, FsPbPortSVlanClassificationMethodSet, FsPbPortSVlanClassificationMethodTest, FsPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsPbPortSVlanIngressEtherType}, GetNextIndexFsPbPortInfoTable, FsPbPortSVlanIngressEtherTypeGet, FsPbPortSVlanIngressEtherTypeSet, FsPbPortSVlanIngressEtherTypeTest, FsPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPbPortInfoTableINDEX, 1, 0, 0, "34984"},

{{12,FsPbPortSVlanEgressEtherType}, GetNextIndexFsPbPortInfoTable, FsPbPortSVlanEgressEtherTypeGet, FsPbPortSVlanEgressEtherTypeSet, FsPbPortSVlanEgressEtherTypeTest, FsPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPbPortInfoTableINDEX, 1, 0, 0, "34984"},

{{12,FsPbPortSVlanEtherTypeSwapStatus}, GetNextIndexFsPbPortInfoTable, FsPbPortSVlanEtherTypeSwapStatusGet, FsPbPortSVlanEtherTypeSwapStatusSet, FsPbPortSVlanEtherTypeSwapStatusTest, FsPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsPbPortSVlanTranslationStatus}, GetNextIndexFsPbPortInfoTable, FsPbPortSVlanTranslationStatusGet, FsPbPortSVlanTranslationStatusSet, FsPbPortSVlanTranslationStatusTest, FsPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsPbPortUnicastMacLearning}, GetNextIndexFsPbPortInfoTable, FsPbPortUnicastMacLearningGet, FsPbPortUnicastMacLearningSet, FsPbPortUnicastMacLearningTest, FsPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsPbPortUnicastMacLimit}, GetNextIndexFsPbPortInfoTable, FsPbPortUnicastMacLimitGet, FsPbPortUnicastMacLimitSet, FsPbPortUnicastMacLimitTest, FsPbPortInfoTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsPbSrcMacAddress}, GetNextIndexFsPbSrcMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsPbSrcMacSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsPbSrcMacSVlan}, GetNextIndexFsPbSrcMacSVlanTable, FsPbSrcMacSVlanGet, FsPbSrcMacSVlanSet, FsPbSrcMacSVlanTest, FsPbSrcMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbSrcMacSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsPbSrcMacRowStatus}, GetNextIndexFsPbSrcMacSVlanTable, FsPbSrcMacRowStatusGet, FsPbSrcMacRowStatusSet, FsPbSrcMacRowStatusTest, FsPbSrcMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbSrcMacSVlanTableINDEX, 2, 0, 1, NULL},

{{12,FsPbDstMacAddress}, GetNextIndexFsPbDstMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsPbDstMacSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsPbDstMacSVlan}, GetNextIndexFsPbDstMacSVlanTable, FsPbDstMacSVlanGet, FsPbDstMacSVlanSet, FsPbDstMacSVlanTest, FsPbDstMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbDstMacSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsPbDstMacRowStatus}, GetNextIndexFsPbDstMacSVlanTable, FsPbDstMacRowStatusGet, FsPbDstMacRowStatusSet, FsPbDstMacRowStatusTest, FsPbDstMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbDstMacSVlanTableINDEX, 2, 0, 1, NULL},

{{12,FsPbCVlanSrcMacCVlan}, GetNextIndexFsPbCVlanSrcMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbCVlanSrcMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanSrcMacAddr}, GetNextIndexFsPbCVlanSrcMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsPbCVlanSrcMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanSrcMacSVlan}, GetNextIndexFsPbCVlanSrcMacSVlanTable, FsPbCVlanSrcMacSVlanGet, FsPbCVlanSrcMacSVlanSet, FsPbCVlanSrcMacSVlanTest, FsPbCVlanSrcMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbCVlanSrcMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanSrcMacRowStatus}, GetNextIndexFsPbCVlanSrcMacSVlanTable, FsPbCVlanSrcMacRowStatusGet, FsPbCVlanSrcMacRowStatusSet, FsPbCVlanSrcMacRowStatusTest, FsPbCVlanSrcMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbCVlanSrcMacSVlanTableINDEX, 3, 0, 1, NULL},

{{12,FsPbCVlanDstMacCVlan}, GetNextIndexFsPbCVlanDstMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbCVlanDstMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanDstMacAddr}, GetNextIndexFsPbCVlanDstMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsPbCVlanDstMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanDstMacSVlan}, GetNextIndexFsPbCVlanDstMacSVlanTable, FsPbCVlanDstMacSVlanGet, FsPbCVlanDstMacSVlanSet, FsPbCVlanDstMacSVlanTest, FsPbCVlanDstMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbCVlanDstMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanDstMacRowStatus}, GetNextIndexFsPbCVlanDstMacSVlanTable, FsPbCVlanDstMacRowStatusGet, FsPbCVlanDstMacRowStatusSet, FsPbCVlanDstMacRowStatusTest, FsPbCVlanDstMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbCVlanDstMacSVlanTableINDEX, 3, 0, 1, NULL},

{{12,FsPbDscp}, GetNextIndexFsPbDscpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPbDscpSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsPbDscpSVlan}, GetNextIndexFsPbDscpSVlanTable, FsPbDscpSVlanGet, FsPbDscpSVlanSet, FsPbDscpSVlanTest, FsPbDscpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbDscpSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsPbDscpRowStatus}, GetNextIndexFsPbDscpSVlanTable, FsPbDscpRowStatusGet, FsPbDscpRowStatusSet, FsPbDscpRowStatusTest, FsPbDscpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbDscpSVlanTableINDEX, 2, 0, 1, NULL},

{{12,FsPbCVlanDscpCVlan}, GetNextIndexFsPbCVlanDscpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbCVlanDscpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanDscp}, GetNextIndexFsPbCVlanDscpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPbCVlanDscpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanDscpSVlan}, GetNextIndexFsPbCVlanDscpSVlanTable, FsPbCVlanDscpSVlanGet, FsPbCVlanDscpSVlanSet, FsPbCVlanDscpSVlanTest, FsPbCVlanDscpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbCVlanDscpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanDscpRowStatus}, GetNextIndexFsPbCVlanDscpSVlanTable, FsPbCVlanDscpRowStatusGet, FsPbCVlanDscpRowStatusSet, FsPbCVlanDscpRowStatusTest, FsPbCVlanDscpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbCVlanDscpSVlanTableINDEX, 3, 0, 1, NULL},

{{12,FsPbSrcIpAddr}, GetNextIndexFsPbSrcIpAddrSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPbSrcIpAddrSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsPbSrcIpSVlan}, GetNextIndexFsPbSrcIpAddrSVlanTable, FsPbSrcIpSVlanGet, FsPbSrcIpSVlanSet, FsPbSrcIpSVlanTest, FsPbSrcIpAddrSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbSrcIpAddrSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsPbSrcIpRowStatus}, GetNextIndexFsPbSrcIpAddrSVlanTable, FsPbSrcIpRowStatusGet, FsPbSrcIpRowStatusSet, FsPbSrcIpRowStatusTest, FsPbSrcIpAddrSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbSrcIpAddrSVlanTableINDEX, 2, 0, 1, NULL},

{{12,FsPbDstIpAddr}, GetNextIndexFsPbDstIpAddrSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPbDstIpAddrSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsPbDstIpSVlan}, GetNextIndexFsPbDstIpAddrSVlanTable, FsPbDstIpSVlanGet, FsPbDstIpSVlanSet, FsPbDstIpSVlanTest, FsPbDstIpAddrSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbDstIpAddrSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsPbDstIpRowStatus}, GetNextIndexFsPbDstIpAddrSVlanTable, FsPbDstIpRowStatusGet, FsPbDstIpRowStatusSet, FsPbDstIpRowStatusTest, FsPbDstIpAddrSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbDstIpAddrSVlanTableINDEX, 2, 0, 1, NULL},

{{12,FsPbSrcDstSrcIpAddr}, GetNextIndexFsPbSrcDstIpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPbSrcDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbSrcDstDstIpAddr}, GetNextIndexFsPbSrcDstIpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPbSrcDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbSrcDstIpSVlan}, GetNextIndexFsPbSrcDstIpSVlanTable, FsPbSrcDstIpSVlanGet, FsPbSrcDstIpSVlanSet, FsPbSrcDstIpSVlanTest, FsPbSrcDstIpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbSrcDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbSrcDstIpRowStatus}, GetNextIndexFsPbSrcDstIpSVlanTable, FsPbSrcDstIpRowStatusGet, FsPbSrcDstIpRowStatusSet, FsPbSrcDstIpRowStatusTest, FsPbSrcDstIpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbSrcDstIpSVlanTableINDEX, 3, 0, 1, NULL},

{{12,FsPbCVlanDstIpCVlan}, GetNextIndexFsPbCVlanDstIpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbCVlanDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanDstIp}, GetNextIndexFsPbCVlanDstIpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsPbCVlanDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanDstIpSVlan}, GetNextIndexFsPbCVlanDstIpSVlanTable, FsPbCVlanDstIpSVlanGet, FsPbCVlanDstIpSVlanSet, FsPbCVlanDstIpSVlanTest, FsPbCVlanDstIpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbCVlanDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsPbCVlanDstIpRowStatus}, GetNextIndexFsPbCVlanDstIpSVlanTable, FsPbCVlanDstIpRowStatusGet, FsPbCVlanDstIpRowStatusSet, FsPbCVlanDstIpRowStatusTest, FsPbCVlanDstIpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbCVlanDstIpSVlanTableINDEX, 3, 0, 1, NULL},

{{12,FsPbPortCVlan}, GetNextIndexFsPbPortBasedCVlanTable, FsPbPortCVlanGet, FsPbPortCVlanSet, FsPbPortCVlanTest, FsPbPortBasedCVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbPortBasedCVlanTableINDEX, 1, 0, 0, NULL},

{{12,FsPbPortCVlanClassifyStatus}, GetNextIndexFsPbPortBasedCVlanTable, FsPbPortCVlanClassifyStatusGet, FsPbPortCVlanClassifyStatusSet, FsPbPortCVlanClassifyStatusTest, FsPbPortBasedCVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbPortBasedCVlanTableINDEX, 1, 0, 0, NULL},

{{12,FsPbLocalEtherType}, GetNextIndexFsPbEtherTypeSwapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPbEtherTypeSwapTableINDEX, 2, 0, 0, NULL},

{{12,FsPbRelayEtherType}, GetNextIndexFsPbEtherTypeSwapTable, FsPbRelayEtherTypeGet, FsPbRelayEtherTypeSet, FsPbRelayEtherTypeTest, FsPbEtherTypeSwapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPbEtherTypeSwapTableINDEX, 2, 0, 0, NULL},

{{12,FsPbEtherTypeSwapRowStatus}, GetNextIndexFsPbEtherTypeSwapTable, FsPbEtherTypeSwapRowStatusGet, FsPbEtherTypeSwapRowStatusSet, FsPbEtherTypeSwapRowStatusTest, FsPbEtherTypeSwapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbEtherTypeSwapTableINDEX, 2, 0, 1, NULL},

{{12,FsPbSVlanConfigServiceType}, GetNextIndexFsPbSVlanConfigTable, FsPbSVlanConfigServiceTypeGet, FsPbSVlanConfigServiceTypeSet, FsPbSVlanConfigServiceTypeTest, FsPbSVlanConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbSVlanConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsPbTunnelProtocolDot1x}, GetNextIndexFsPbTunnelProtocolTable, FsPbTunnelProtocolDot1xGet, FsPbTunnelProtocolDot1xSet, FsPbTunnelProtocolDot1xTest, FsPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolLacp}, GetNextIndexFsPbTunnelProtocolTable, FsPbTunnelProtocolLacpGet, FsPbTunnelProtocolLacpSet, FsPbTunnelProtocolLacpTest, FsPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolStp}, GetNextIndexFsPbTunnelProtocolTable, FsPbTunnelProtocolStpGet, FsPbTunnelProtocolStpSet, FsPbTunnelProtocolStpTest, FsPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolGvrp}, GetNextIndexFsPbTunnelProtocolTable, FsPbTunnelProtocolGvrpGet, FsPbTunnelProtocolGvrpSet, FsPbTunnelProtocolGvrpTest, FsPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolGmrp}, GetNextIndexFsPbTunnelProtocolTable, FsPbTunnelProtocolGmrpGet, FsPbTunnelProtocolGmrpSet, FsPbTunnelProtocolGmrpTest, FsPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolIgmp}, GetNextIndexFsPbTunnelProtocolTable, FsPbTunnelProtocolIgmpGet, FsPbTunnelProtocolIgmpSet, FsPbTunnelProtocolIgmpTest, FsPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolDot1xPktsRecvd}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolDot1xPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolDot1xPktsSent}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolDot1xPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolLacpPktsRecvd}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolLacpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolLacpPktsSent}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolLacpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolStpPDUsRecvd}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolStpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolStpPDUsSent}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolStpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolGvrpPDUsRecvd}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolGvrpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolGvrpPDUsSent}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolGvrpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolGmrpPktsRecvd}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolGmrpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolGmrpPktsSent}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolGmrpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolIgmpPktsRecvd}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolIgmpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbTunnelProtocolIgmpPktsSent}, GetNextIndexFsPbTunnelProtocolStatsTable, FsPbTunnelProtocolIgmpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsPbPepExtCosPreservation}, GetNextIndexFsPbPepExtTable, FsPbPepExtCosPreservationGet, FsPbPepExtCosPreservationSet, FsPbPepExtCosPreservationTest, FsPbPepExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbPepExtTableINDEX, 2, 0, 0, NULL},
};
tMibData fspbEntry = { 74, fspbMibEntry };
#endif /* _FSPBDB_H */

