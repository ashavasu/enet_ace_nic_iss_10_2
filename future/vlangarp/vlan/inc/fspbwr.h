/* $Id: fspbwr.h,v 1.10 2016/01/11 12:56:20 siva Exp $ */
#ifndef _FSPBWR_H
#define _FSPBWR_H

VOID RegisterFSPB(VOID);

VOID UnRegisterFSPB(VOID);
INT4 FsPbMulticastMacLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelStpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelLacpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelDot1xAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelGvrpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelGmrpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsPbMulticastMacLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelStpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelLacpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelDot1xAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelGvrpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelGmrpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsPbMulticastMacLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelStpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelLacpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelDot1xAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelGvrpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelGmrpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbMulticastMacLimitDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbTunnelStpAddressDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbTunnelLacpAddressDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbTunnelDot1xAddressDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbTunnelGvrpAddressDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbTunnelGmrpAddressDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);






INT4 GetNextIndexFsPbPortInfoTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbPortSVlanClassificationMethodGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanIngressEtherTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanEgressEtherTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanEtherTypeSwapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanTranslationStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortUnicastMacLearningGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortUnicastMacLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortBundleStatusGet(tSnmpIndex *, tRetVal *);                                                                       INT4 FsPbPortMultiplexStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanClassificationMethodSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanIngressEtherTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanEgressEtherTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanEtherTypeSwapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanTranslationStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortUnicastMacLearningSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortUnicastMacLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortBundleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortMultiplexStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanClassificationMethodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanIngressEtherTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanEgressEtherTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanEtherTypeSwapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortSVlanTranslationStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortUnicastMacLearningTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortUnicastMacLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortBundleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortMultiplexStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortInfoTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 GetNextIndexFsPbSrcMacSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbSrcMacSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcMacRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcMacSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcMacRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcMacSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbSrcMacRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbSrcMacSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbDstMacSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbDstMacSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsPbDstMacRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbDstMacSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsPbDstMacRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbDstMacSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbDstMacRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbDstMacSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbCVlanSrcMacSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbCVlanSrcMacSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanSrcMacRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanSrcMacSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanSrcMacRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanSrcMacSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanSrcMacRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanSrcMacSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbCVlanDstMacSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbCVlanDstMacSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstMacRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstMacSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstMacRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstMacSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstMacRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstMacSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbDscpSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbDscpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsPbDscpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbDscpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsPbDscpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbDscpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbDscpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbDscpSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbCVlanDscpSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbCVlanDscpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDscpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDscpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDscpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDscpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDscpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDscpSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbSrcIpAddrSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbSrcIpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcIpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcIpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcIpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcIpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbSrcIpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbSrcIpAddrSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbDstIpAddrSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbDstIpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsPbDstIpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbDstIpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsPbDstIpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbDstIpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbDstIpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbDstIpAddrSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbSrcDstIpSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbSrcDstIpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcDstIpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcDstIpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcDstIpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbSrcDstIpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbSrcDstIpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbSrcDstIpSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbCVlanDstIpSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbCVlanDstIpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstIpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstIpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstIpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstIpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstIpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbCVlanDstIpSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbPortBasedCVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbPortCVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortCVlanClassifyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortEgressUntaggedStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortCVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortCVlanClassifyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortEgressUntaggedStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPortCVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortCVlanClassifyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortEgressUntaggedStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPortBasedCVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbEtherTypeSwapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbRelayEtherTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbEtherTypeSwapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbRelayEtherTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsPbEtherTypeSwapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbRelayEtherTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbEtherTypeSwapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbEtherTypeSwapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsPbSVlanConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbSVlanConfigServiceTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbSVlanConfigServiceTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsPbSVlanConfigServiceTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbSVlanConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsPbTunnelProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbTunnelProtocolDot1xGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolLacpGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolStpGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolGvrpGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolGmrpGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolIgmpGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolDot1xSet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolLacpSet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolStpSet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolGvrpSet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolGmrpSet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolIgmpSet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolDot1xTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolLacpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolStpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolGvrpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolGmrpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolIgmpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFsPbTunnelProtocolStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbTunnelProtocolDot1xPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolDot1xPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolLacpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolLacpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolStpPDUsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolStpPDUsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolGvrpPDUsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolGvrpPDUsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolGmrpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolGmrpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolIgmpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbTunnelProtocolIgmpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPbPepExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbPepExtCosPreservationGet(tSnmpIndex *, tRetVal *);
INT4 FsPbPepExtCosPreservationSet(tSnmpIndex *, tRetVal *);
INT4 FsPbPepExtCosPreservationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbPepExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif /* _FSPBWR_H */
