/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmid1lw.h,v 1.4 2016/07/16 11:15:03 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIEvbSystemTable. */
INT1
nmhValidateIndexInstanceFsMIEvbSystemTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEvbSystemTable  */

INT1
nmhGetFirstIndexFsMIEvbSystemTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEvbSystemTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEvbSystemControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIEvbSystemModuleStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIEvbSystemTraceLevel ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIEvbSystemTrapStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIEvbSystemStatsClear ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIEvbSchannelIdMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIEvbSystemRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEvbSystemControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIEvbSystemModuleStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIEvbSystemTraceLevel ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIEvbSystemTrapStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIEvbSystemStatsClear ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIEvbSchannelIdMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIEvbSystemRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEvbSystemControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIEvbSystemModuleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIEvbSystemTraceLevel ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIEvbSystemTrapStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIEvbSystemStatsClear ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIEvbSchannelIdMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIEvbSystemRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEvbSystemTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEvbCAPConfigTable. */
INT1
nmhValidateIndexInstanceFsMIEvbCAPConfigTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEvbCAPConfigTable  */

INT1
nmhGetFirstIndexFsMIEvbCAPConfigTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEvbCAPConfigTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEvbCAPSChannelID ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEvbCAPSChannelIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEvbCAPSChNegoStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIEvbPhyPort ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEvbSchID ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIEvbSChannelFilterStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEvbCAPSChannelID ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIEvbCAPSChannelIfIndex ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIEvbSChannelFilterStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEvbCAPSChannelID ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIEvbCAPSChannelIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIEvbSChannelFilterStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEvbCAPConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEvbUAPConfigTable. */
INT1
nmhValidateIndexInstanceFsMIEvbUAPConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEvbUAPConfigTable  */

INT1
nmhGetFirstIndexFsMIEvbUAPConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEvbUAPConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEvbUAPSchCdcpMode ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIEvbUAPSchCdcpMode ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIEvbUAPSchCdcpMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIEvbUAPConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIEvbUAPStatsTable. */
INT1
nmhValidateIndexInstanceFsMIEvbUAPStatsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIEvbUAPStatsTable  */

INT1
nmhGetFirstIndexFsMIEvbUAPStatsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIEvbUAPStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIEvbTxCdcpCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEvbRxCdcpCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEvbSChAllocFailCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEvbSChActiveFailCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEvbSVIDPoolExceedsCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEvbCdcpRejectStationReq ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMIEvbCdcpOtherDropCount ARG_LIST((UINT4 ,UINT4 *));
