/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: vlantdfs.h,v 1.127 2016/05/19 10:57:08 siva Exp $    */
/*                                                                           */
/*  FILE NAME             : vlantdfs.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : VLAN                                             */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains type definitions of the data  */
/*                          structures used in VLAN Module.                  */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#ifndef _VLANTDFS_H
#define _VLANTDFS_H

/* VLAN Sizing IDs*/
enum 
{
    VLAN_CFG_Q_SIZING_ID = 0,
    VLAN_Q_SIZING_ID,
    VLAN_FID_SIZING_ID,
    VLAN_PORTS_SIZING_ID,
    VLAN_CURR_SIZING_ID,
    VLAN_FDB_SIZING_ID,
#ifdef SW_LEARNING    
    VLAN_FDB_INFO_SIZING_ID,
#endif
    VLAN_ST_UCAST_SIZING_ID,
    VLAN_ST_MCAST_SIZING_ID,
    VLAN_GROUP_SIZING_ID,
    VLAN_MAC_MAP_SIZING_ID,
#ifndef NPAPI_WANTED
    VLAN_PORT_INFO_PER_VLN_SIZING_ID,
    VLAN_STATS_SIZING_ID,
#endif
    VLAN_PROTO_GRP_SIZING_ID,
    VLAN_PORT_PROTO_SIZING_ID,
    VLAN_MAC_CTRL_SIZING_ID,
    VLAN_WILDCARD_SIZING_ID,
    L2IWF_CONTEXTS_SIZING_ID,
    VLAN_MISC_INFO_SIZING_ID,
    VLAN_TMP_PORTLST_SIZING_ID,
#ifdef MPLS_WANTED
    VLAN_L2VPN_SIZING_ID,
#endif
    VLAN_PBPORT_SIZING_ID,
#ifdef PB_WANTED
    VLAN_SVLN_PORT_CVLN_SIZING_ID,
    VLAN_SVLN_PORT_CVLN_SMAC_SIZING_ID,
    VLAN_SVLN_PORT_CVLN_DMAC_SIZING_ID,
    VLAN_SVLN_PORT_CVLN_DSCP_SIZING_ID,
    VLAN_SVLN_PORT_CVLN_DSTIP_SIZING_ID,
    VLAN_SVLN_PORT_SMAC_SIZING_ID,
    VLAN_SVLN_PORT_DMAC_SIZING_ID,
    VLAN_SVLN_PORT_DSCP_SIZING_ID,
    VLAN_SVLN_PORT_SRCIP_SIZING_ID,
    VLAN_SVLN_PORT_DSTIP_SIZING_ID,
    VLAN_SVLN_PORT_SRCIP_DSTIP_SIZING_ID,
    VLAN_ETHER_TYPE_SWP_SIZING_ID,
    VLAN_PB_LOG_PORT_SIZING_ID,
#endif
#ifdef L2RED_WANTED
    VLAN_RED_GVRP_DEL_SIZING_ID,
    VLAN_RED_GMRP_DEL_SIZING_ID,
    VLAN_RED_GVRP_LEARNT_SIZING_ID,
    VLAN_RED_GMRP_LEARNT_SIZING_ID,
#endif

    VLAN_ST_VLAN_SIZING_ID,
    VLAN_PVLAN_SIZING_ID,
    VLAN_BASE_PORT_SIZING_ID,
    VLAN_NO_SIZING_ID
};

/************************* Dynamic Unicast Entry ***************************/
typedef struct _tVlanFdbEntry {
   struct _tVlanFdbEntry *pNextHashNode;
   tMacAddr               MacAddr;
   tVlanId                VlanId;
   UINT4                  u4TimeStamp;
   UINT4                  u4PwVcIndex;
   UINT2                  u2Port;
   UINT1                  u1Status;
   UINT1                  u1StRefCount; /* 
                                         * No of static entries present for 
                                         * this unicast entry...
                                         */
} tVlanFdbEntry;

/***************************************************************************/
typedef struct VlanContextInfo   tVlanContextInfo;
typedef tVlanFdbEntry*    tVlanFdbHashTable [VLAN_MAX_BUCKETS]; 

/************************* Static Unicast Entry ****************************/
typedef struct _tVlanStUcastEntry {
   struct _tVlanStUcastEntry *pNextNode;
   tVlanFdbEntry            *pFdbEntry; /* Pointer to the entry in 
                                         * the unicast table 
                                         */
   tLocalPortList                 AllowedToGo;
   tRBNodeEmbd            RBNode;
   UINT4                     u4TimeStamp;
   tMacAddr                  MacAddr;
   tMacAddr                  ConnectionId;
   UINT2                     u2RcvPort;
   tVlanId                VlanId;
   UINT1                     u1Status;
   UINT1                     u1RowStatus; /* Row status added for MI */
   UINT1                       au1Reserved[2];
} tVlanStUcastEntry;
/***************************************************************************/

/****************************** FDB Table **********************************/
typedef struct _tVlanFidEntry {
   UINT4              u4Fid;
   UINT4              u4Count;
   UINT4              u4DynamicUnicastCount; /* For dynamic unicast limit on the
                                                FID table. It may be possible 
                                                that this contains one value greater
                                                than the actual number of entries 
                                                learned to handle trap generation*/
   tVlanFdbHashTable  UcastHashTbl;   /* tVlanFdbEntry */
   tVlanStUcastEntry *pStUcastTbl;
   UINT2              u2VlanRefCount;
#ifndef VLAN_PORTLIST_RBTREE_WANTED 
   tLocalPortList     MacLearnPortList;
#endif
   UINT1              u1Reserved[2];
} tVlanFidEntry;
/***************************************************************************/



/*********************** Static/Dynamic Multicast Entry ********************/
typedef struct _tVlanGroupEntry {
   struct _tVlanGroupEntry   *pNextNode;
   tLocalPortList                 LearntPorts; 
   tMacAddr                  MacAddr;
   UINT1                     u1StRefCount;
   UINT1                     u1IsLearntInfoChanged; /* Indicates changes to the 
                                                       mcast learnt ports */
} tVlanGroupEntry;
/***************************************************************************/

/************************* Static Multicast Entry **************************/
typedef struct _tVlanStMcastEntry {
   struct _tVlanStMcastEntry *pNextNode;
   tVlanGroupEntry           *pGroupEntry; 
   tLocalPortList                 EgressPorts; /* This is mutually exclusive 
                                           * with ForbiddenPorts ports.
                                           */
   tLocalPortList                 ForbiddenPorts;
   UINT4                     u4TimeStamp;
   tMacAddr                  MacAddr;
   UINT2                     u2RcvPort;
   UINT1                     u1Status;
   UINT1                     u1RowStatus;
   UINT1                     au1Pad[2];
} tVlanStMcastEntry;
/***************************************************************************/

/************************* Static Vlan Entry  ******************************/
typedef struct _tStaticVlanEntry {
  struct _tStaticVlanEntry *pNextNode;
  VOID                     *pCurrEntry;
#ifndef VLAN_PORTLIST_RBTREE_WANTED
  tLocalPortList                 EgressPorts;   /* This includes untag ports also */
  tLocalPortList                 UnTagPorts;
  tLocalPortList                 ForbiddenPorts;
  tLocalPortList                 FdbFlushStatus;
#endif
  UINT2                     u2EgressEtherType; /*Egress EtherType for a VLAN */
  UINT1                     au1VlanName[VLAN_STATIC_MAX_NAME_LEN];
  tVlanId                   VlanId;
  UINT1                     u1RowStatus;
  UINT1                     u1VlanNameLen;
  /* This flag(u1NameModifiedFlag) set to VLAN_TRUE whenever VLAN name is 
   * modified in VLAN module and reset to VLAN_FALSE when modified VLAN name is
   * intimated and updated in l2iwf. By default it is set to VLAN_FALSE */
  UINT1                     u1NameModifiedFlag;
  BOOLEAN                   bVlanFdbFlush;
  UINT1                     u1ServiceType;
  UINT1                     au1Pad[3];
 } tStaticVlanEntry;
/***************************************************************************/

/****************************** Statistics Entry ***************************/
typedef struct _tVlanPortInfoPerVlan {
   struct _tVlanPortInfoPerVlan    *pNextNode;
   UINT2                      u2Port;
#ifdef SYS_HC_WANTED
   UINT1                      u1IsHCPort;
   UINT1                      u1Reserved;
   UINT4                      u4InOverflow;
   UINT4                      u4OutOverflow;
   UINT4                      u4InOverflowDiscards;
#else                         
   UINT2                      u2Reserved;
#endif                        
   UINT4                      u4InFrames;
   UINT4                      u4OutFrames;
   UINT4                      u4InDiscards;
   BOOL1                      b1LckStatus;
   UINT1                      au1Pad[3];
} tVlanPortInfoPerVlan;

typedef struct _tVlanStats {
   UINT4                    u4RxUcastCnt;
   UINT4                    u4RxMcastBcastCnt;
   UINT4                    u4TxUnknUcastCnt;
   UINT4                    u4TxUcastCnt;
   UINT4                    u4TxBcastCnt;
} tVlanStats;

/***************************************************************************/

/***************************** Default Group Entry *************************/
/* 
 * Default Group behaviour is as follows. 
 * For "AllGroups", the default value for
 *    StaticPorts is all 1's.
 *    ForbiddenPorts is all 0's
 * For "UnregisteredGroups", the default value for
 *    StaticPorts is all 0's
 *    ForbiddenPorts is all 0's
 */
typedef struct _tVlanDefaultGrps {
   tLocalPortList  Ports;          /* 
                               * This object includes all static and learnt 
                               * ports and this is mutually exclusive with
                               * forbidden ports
                               */
   tLocalPortList  StaticPorts;
   tLocalPortList  ForbiddenPorts;
   UINT1      u1RowStatus; /*Added row status for MI */
   UINT1      au1Pad[3];
} tVlanDefaultGrps;
/***************************************************************************/

/************* MAC Learning Parameters *****************/
typedef struct _tVlanMacControl
{
    UINT4       u4UnicastMacLearningLimit;
    UINT1       u1AdminUnicastMacLearningStatus;
    UINT1       u1OperationalUnicastMacLearningStatus;
    UINT2       u2Padding;
} tVlanMacControl;

/*************************** TRAFFIC CLASS QUEUE INFO **********************/
typedef struct _tVlanPriQ {
   tCRU_BUF_CHAIN_DESC *pFirstBuf; /* Pointer to the first buffer in 
                                      this queue */
   tCRU_BUF_CHAIN_DESC *pLastBuf;  /* Pointer to the last buffer in this
                                      queue. Used to link other buffers to this
                                      chain. */

}tVlanPriQ;   

/******************************* VID SET **********************************/ 
typedef struct _tVlanPortVidSet {
    tVLAN_SLL_NODE  NextNode;
    UINT4  u4ProtGrpId;
    tVlanId  VlanId;
    UINT1 u1RowStatus;
    UINT1 u1Reserved;
} tVlanPortVidSet;
/***************************************************************************/


/******************************* Protocol Group Entry ***********************/
typedef struct _tVlanProtGrpEntry{
    tVLAN_HASH_NODE  NextHashNode;
    tVlanProtoTemplate  VlanProtoTemplate;
    UINT4 u4ProtGrpId;
    UINT1 u1RowStatus;
    UINT1 au1Reserved[3];
}tVlanProtGrpEntry;
/***************************************************************************/
/* 
 * Added the following data structures for 802.1ad
 * implementation.
 */

/* PCP decoding table */
typedef struct _tVlanPbPcpDecVal
{
   UINT1   u1Priority;      /* Priority associated with the 
                               incoming frame */
   UINT1   u1DropEligible;  /* Drop eligible value associated with the 
                               incoming frame. VLAN_DE_TRUE/VLAN_DE_FALSE  */
   UINT1   au1Pad[2]; 
}tVlanPbPcpDecVal;

/* 
 * Protocol tunnel status configuration on physical ports.
 * This could be set to tunnel/peer/discard.
 */
typedef struct _tVlanTunnelL2ProtoInfo {
   UINT1               u1Dot1xTunnelStatus; 
   UINT1               u1LacpTunnelStatus; 
   UINT1               u1StpTunnelStatus; 
   UINT1               u1GvrpTunnelStatus;
   UINT1               u1MvrpTunnelStatus; 
   UINT1               u1GmrpTunnelStatus; 
   UINT1               u1MmrpTunnelStatus;
   UINT1               u1IgmpTunnelStatus; 
   UINT1               u1ElmiTunnelStatus; 
   UINT1               u1LldpTunnelStatus;
   UINT1               u1EcfmTunnelStatus;
   UINT1               u1EoamTunnelStatus;
   UINT1               u1PtpTunnelStatus;
   UINT1               u1OtherTunnelStatus[VLAN_MAX_OTHER_PROTOCOL_SUPPORT];
   UINT1               au1Pad[1]; 
}tVlanTunnelL2ProtoInfo;

/*
 * Protocol tunnel statistics information on physical ports.
 */
typedef struct _tVlanTunnelL2ProtoStats {
   UINT4               u4Dot1xPktsRecvd;
   UINT4               u4Dot1xPktsSent;
   UINT4               u4LacpPktsRecvd;
   UINT4               u4LacpPktsSent;
   UINT4               u4StpBpdusRecvd;
   UINT4               u4StpBpdusSent;
   UINT4               u4GvrpPktsRecvd;
   UINT4               u4GvrpPktsSent;
   UINT4               u4GmrpPktsRecvd;
   UINT4               u4GmrpPktsSent;
   UINT4               u4MvrpPktsRecvd;
   UINT4               u4MvrpPktsSent;
   UINT4               u4MmrpPktsRecvd;
   UINT4               u4MmrpPktsSent;
   UINT4               u4IgmpPktsRecvd;
   UINT4               u4IgmpPktsSent;
   UINT4               u4ElmiPktsRecvd;
   UINT4               u4ElmiPktsSent;
   UINT4               u4LldpPktsRecvd;
   UINT4               u4LldpPktsSent;
   UINT4               u4EcfmPktsRecvd;
   UINT4               u4EcfmPktsSent;
   UINT4               u4EoamPktsRecvd;
   UINT4               u4EoamPktsSent;
   UINT4               u4PtpPktsRecvd;
   UINT4               u4PtpPktsSent;
} tVlanTunnelL2ProtoStats;

/* 
 * Protocol Discard statistics information on physical ports.
 */
typedef struct _tVlanDiscardL2ProtoStats {
   UINT4        u4DiscDot1xPktsRx;
   UINT4        u4DiscDot1xPktsTx;
   UINT4        u4DiscLacpPktsRx;
   UINT4        u4DiscLacpPktsTx;
   UINT4        u4DiscStpBpdusRx;
   UINT4        u4DiscStpBpdusTx;
   UINT4        u4DiscGvrpPktsRx;
   UINT4        u4DiscGvrpPktsTx;
   UINT4        u4DiscGmrpPktsRx;
   UINT4        u4DiscGmrpPktsTx;
   UINT4        u4DiscMvrpPktsRx;
   UINT4        u4DiscMvrpPktsTx;
   UINT4        u4DiscMmrpPktsRx;
   UINT4        u4DiscMmrpPktsTx;
   UINT4        u4DiscIgmpPktsRx;
   UINT4        u4DiscIgmpPktsTx;
   UINT4        u4DiscElmiPktsRx;
   UINT4        u4DiscElmiPktsTx;
   UINT4        u4DiscLldpPktsRx;
   UINT4        u4DiscLldpPktsTx;
   UINT4        u4DiscEcfmPktsRx;
   UINT4        u4DiscEcfmPktsTx;
   UINT4        u4DiscEoamPktsRx;
   UINT4        u4DiscEoamPktsTx;
   UINT4        u4DiscPtpPktsRx;
   UINT4        u4DiscPtpPktsTx;
} tVlanDiscardL2ProtoStats;

/*
 * Protocol Reserved MAC Address information 
 */
typedef struct _tVlanL2ProtoRsvdMacAddr {
   tMacAddr             ProviderRsvdDot1xAddr; /* Reserved MAC address for L2
       protocols */
   tMacAddr             ProviderRsvdLacpAddr;
   tMacAddr             ProviderRsvdStpAddr;
   tMacAddr             ProviderRsvdGvrpAddr;
   tMacAddr             ProviderRsvdMvrpAddr;
   tMacAddr             ProviderRsvdIgmpAddr;
   tMacAddr             ProviderRsvdGmrpAddr;
   tMacAddr             ProviderRsvdMmrpAddr;
   tMacAddr             ProviderRsvdElmiAddr;
   tMacAddr             ProviderRsvdLldpAddr;
   tMacAddr             ProviderRsvdEcfmAddr;
   tMacAddr             ProviderRsvdEoamAddr;
   tMacAddr             ProviderRsvdPtpAddr;
   tMacAddr             ProviderRsvdOtherAddr[VLAN_MAX_OTHER_PROTOCOL_SUPPORT];
}tVlanL2ProtoRsvdMacAddr;

/*
 * Protocol Tunnel MAC Address information 
 */
typedef struct _tVlanL2ProtoTunnelMacAddr {
   tMacAddr             ProviderTunnelDot1xAddr; /* Tunnel MAC address for L2
       protocols */
   tMacAddr             ProviderTunnelLacpAddr;
   tMacAddr             ProviderTunnelStpAddr;
   tMacAddr             ProviderTunnelGvrpAddr;
   tMacAddr             ProviderTunnelMvrpAddr;
   tMacAddr             ProviderTunnelIgmpAddr;
   tMacAddr             ProviderTunnelGmrpAddr;
   tMacAddr             ProviderTunnelMmrpAddr;
   tMacAddr             ProviderTunnelElmiAddr;
   tMacAddr             ProviderTunnelLldpAddr;
   tMacAddr             ProviderTunnelEcfmAddr;
   tMacAddr             ProviderTunnelEoamAddr;
   tMacAddr             ProviderTunnelPtpAddr;
   tMacAddr             ProviderTunnelOtherAddr[VLAN_MAX_OTHER_PROTOCOL_SUPPORT];
}tVlanL2ProtoTunnelMacAddr;

/* 
 * Physical port configurations specific to Provider Bridges
 */
typedef struct  _tVlanPbPortEntry {
   UINT4         u4MacLearningLimit;
   UINT4         u4NumLearntMacEntries;
   tVlanPbPcpDecVal au1PcpDecoding[VLAN_PB_MAX_PCP_SEL_ROW][VLAN_PB_MAX_PCP];
   UINT1         au1PcpEncoding[VLAN_PB_MAX_PCP_SEL_ROW][VLAN_PB_MAX_PRIORITY]
                               [VLAN_PB_MAX_DROP_ELIGIBLE];
   tVlanId       CVlanId;
   UINT1         u1PbPortType;
   UINT1         u1DefCVlanClassify;
   UINT1         u1EtherTypeSwapStatus;
   UINT1         u1MacLearningStatus;
   UINT1         u1SVlanTableType;
   UINT1         u1PortServiceType;
   UINT1         u1PcpSelRow;
   UINT1         u1UseDei; /* VLAN_SNMP_TRUE/VLAN_SNMP_FALSE */
   UINT1         u1ReqDropEncoding; /* VLAN_SNMP_TRUE/VLAN_SNMP_FALSE */
   UINT1         u1MultiplexStatus;/*Port supports multiplexing or not.*/
   UINT1         u1BundleStatus; /*Port supports bundling or not*/
   UINT1         u1SVlanPriorityType;/*SVLAN Priority type of outgoing S-tagged packets [copy/fixed]*/
   UINT1         u1SVlanPriority;/*Fixed SVLAN Priority value of outgoing S-tagged packets*/
   UINT1         u1EncapType; /* Set when port as configured with DOT1q */
   UINT1         u1UntagFrameOnCEP; /* If Set to Enabled/allow untagged frames will be
                                       sent on CEP port from PNP. If Set to Disabled/deny
                                       the untagged frames will be dropped on CEP */
   UINT1         au1Reserved[3];
}tVlanPbPortEntry;

#define VLAN_PB_PORT_AS_DOT1Q_ENA 1 
#define VLAN_PB_PORT_AS_DOT1Q_DIS 0 


/****************************** Current Vlan Entry *************************/
typedef struct _tVlanCurrEntry {
   tVlanStMcastEntry           *pStMcastTable;   /* tVlanStMcastEntry */
   tVlanGroupEntry             *pGroupTable;     /* tVlanGroupEntry   */
#ifndef NPAPI_WANTED
   tVlanPortInfoPerVlan        *pPortInfoTable; /* tVlanPortInfoPerVlan    */
   tVlanStats                  *pVlanStats;
#endif /*NPAPI_WANTED*/
   tVlanMacControl             *pVlanMacControl;
   tStaticVlanEntry            *pStVlanEntry;   /* Present only if 
                                                   Static info exists */
   INT4                         i4VlanCounterStatus; /* VLAN Counter status for RMON feature */
   INT4                         i4VlanLoopbackStatus; /* VLAN Loopback status */
   VOID                         *pVlanHwStatsEntry; /* Hold platform specific data */
   UINT4                        u4TimeStamp;
   UINT4                        u4CreationTimeStamp;
   UINT4                        u4FidIndex;     /* Array index of the Fid 
                                                   Table */
#ifdef SW_LEARNING
   UINT4                        u4UnicastStaticCount;
   UINT4                        u4UnicastDynamicCount;
   UINT4                        u4MulticastStaticCount;
   UINT4                        u4MulticastDynamicCount;
#endif
   tVlanId                      VlanId;
   UINT2                        u2OperPortsCount;/* Count of the operationally
          up ports in the VLAN */
#ifdef VLAN_EXTENDED_FILTER 
   tVlanDefaultGrps             AllGrps; 
   tVlanDefaultGrps             UnRegGrps;
#endif   
#ifndef VLAN_PORTLIST_RBTREE_WANTED
   tLocalPortList               EgressPorts;  /* Static Egress ports + Learnt
                                               * ports + Trunk Ports
                                               */
   tLocalPortList               LearntPorts;
   tLocalPortList               TrunkPorts; 
#endif   
   UINT2                        u2NextVlanInd; /* To get the next Vlan index */
   UINT1                        u1IsLearntInfoChanged; /* Indicates change of 
                                                          vlan learnt ports */
   UINT1                        u1IsGrpChanged; /* Indicates change of some
                                                   mcast learnt ports in VLAN */
   tVlanTunnelL2ProtoInfo   VlanTunnelL2ProtoInfo; /* Protocol Tunnel Status Configuration */
   tVlanTunnelL2ProtoStats  VlanTunnelL2ProtoStats; /* Protocol Tunnel Statistics information */
   tVlanDiscardL2ProtoStats  VlanDiscardL2ProtoStats; /* Protocol Discard Statistics information */
   tVlanL2ProtoRsvdMacAddr      VlanL2ProtoRsvdMacAddr; /* Protocol Reserved MAC address information */
   tVlanL2ProtoTunnelMacAddr    VlanL2ProtoTunnelMacAddr; /* Protocol Tunnel MAC address information */
   UINT1   u1TunnelStatus; /* Tunnel Status of a vlan ENABLED/DISABLED */
   UINT1   u1IsMclagEnabled;
   UINT1                        au1Pad[2];

} tVlanCurrEntry;
/***************************************************************************/


/****************************** Port Table Entry ***************************/

typedef struct _tVlanPortEntry {
   tRBNodeEmbd        NextPortEntry; /* next port entry  */
   tRBNodeEmbd        RbNode;
   tVlanPbPortEntry  *pVlanPbPortEntry;
#ifndef NPAPI_WANTED
   tVlanPriQ          VlanPriQ [VLAN_MAX_TRAFF_CLASS]; /* Traffic queues for
                                                          all traffic classes.*/
#endif /* !NPAPI_WANTED  */
   tVLAN_SLL          VlanVidSet;
   UINT4              u4ContextId; /* Context identifier */
   UINT4              u4IfIndex; /* Interface index */
   UINT4              u4PhyIfIndex; /* Physical interface index */
   UINT4              u4NoFilterInDiscard; 
   UINT4              u4FilterInDiscardOverflow; 
   UINT4              u4IfSpeed;
   UINT4              u4MacLearningLimit;  /* Unicast Mac Learning limit for port*/
   UINT4              u4NumLearntMacEntries;  /* Number of Learnt Mac Entries for port*/
   UINT4       u4OverrideOption;
   INT4              i4UnicastMacSecType;
   tVlanTunnelL2ProtoInfo  VlanTunnelL2ProtoInfo;
   tVlanTunnelL2ProtoStats VlanTunnelL2ProtoStats;
   tVlanDiscardL2ProtoStats VlanDiscardL2ProtoStats;
   UINT2              u2Port;   /*port number which is local to the context */ 
   tVlanId            Pvid;
   UINT2              u2NextPortInd;
   UINT2              u2IngressEtherType; /* The ethertype to be matched against the 
                                             outer tag in incoming frame */
   UINT2              u2EgressEtherType; /* The ethertype to be put in the outer tag 
                                            present in the outgoing frame */
   UINT2              u2FiltUtilityCriteria;

   UINT2              u2AllowableTPID1;
   UINT2              u2AllowableTPID2;
   UINT2              u2AllowableTPID3;
   UINT1              au1PriorityRegen [VLAN_MAX_PRIORITY];
   UINT1              au1TrfClassMap [VLAN_MAX_PRIORITY];

   UINT2              u2MacVlanEntryCount;   
   UINT2              u2SubnetVlanEntryCount;   
   UINT1              u1IfType;                        /* Interface type */
   UINT1              u1PortType;
   UINT1              u1PortProtected;
   UINT1              u1NumTrfClass;
   UINT1              u1AccpFrmTypes;
   UINT1              u1IngFiltering;
   UINT1              u1IsHCPort;
   UINT1      u1DefUserPriority;
   UINT1      u1OperStatus;     /* Operational status of the port */
   UINT1      u1MacBasedClassification;/*mac based classification */
   UINT1      u1PortProtoBasedClassification; /*port and protocol based,*/
   UINT1      u1SubnetBasedClassification;    /*Subnet based classification */
   UINT1      u1TunnelStatus; /* Indicates tunnelling status */
   UINT1      u1MacLearningStatus;  /* Unicast Mac Learning status for port*/
   UINT1      u1RowStatus; /* The row status is added for the support of
                              ieee8021BridgeDot1dPortTable */
   UINT1              u1EgressTPIDType; /*The Egress TPID type for a port can portbased/vlanbased */
   BOOLEAN              bPortFdbFlush;
   UINT1   u1OtherProtEnableStatus;
   INT4    i4ReflectionStatus;

}tVlanPortEntry;

/****************************** Base Port Table Entry ***********************/

typedef struct _tVlanBasePortEntry {
   tRBNodeEmbd        NextPortEntry; /* next port entry  */
   tRBNodeEmbd        RbNode;
   UINT4              u4ContextId; /* Context identifier */
   UINT4              u4BasePort;   /*port number which is local to the context */
   UINT4              u4IfIndex; /* Interface index */
   UINT4              u4ComponentId; /* Base Bridge Component identifier */
   UINT2              u2NextPortInd;
   UINT1              u1IfType;                        /* Interface type */
   UINT1              u1BrgPortType;
   UINT1              u1OperStatus;
   UINT1              u1RowStatus; /* The row status is added for the support of
                                      ieee8021BridgeDot1dPortTable */
   UINT1              au1Pad[2];
}tVlanBasePortEntry;

/***************************************************************************/

/****************************** Vlan Info Record ***************************/
typedef struct _tVlanInfo {
#ifdef SW_AGE_TIMER
   INT4    i4ShortAgingTime;      /* The short aging time to be applied */
   INT4    bUseShortAgingTime;    /* Boolean to indicate whether the short or
                                     long ageout should be applied */
   INT4    i4RapidAgingNumPorts;  /* Number of ports for which short aging was
                                     requested by the spanning tree protocol */
#endif
   UINT4   u4BridgeMode;         /* CUSTOMER/PROVIDER Bridge */
   UINT4   u4NumDelVlans;
   UINT4   u4NextFreeFdbId;
   UINT4   u4MaxVlans;
   UINT4   u4NumActiveVlans;     /* No. of active static vlan entries */
   UINT4   u4NumConfiguredVlans; /* No. of configured static vlan entries */
   UINT4   u4FreeConstEntries;
   UINT4   u4SwitchDynamicUnicastSize;  /*Configured Dynamic Unicast Mac limit
                                          by the user. By default it is equal 
                                          to VLAN_DEV_MAX_L2_TABLE_SIZE -
                                          VLAN_DEV_MAX_ST_UCAST_ENTRIES*/
   UINT4   u4SwitchDynamicUnicastCount; /*Free running counter to check the 
                                         Dynamic unicast mac entries learnt with 
                                         the configured Dynamic unicast count.
                                         This counter is updated only in 
                                         simulation. */
   UINT4   u4SwitchStaticUnicastCount;  /* Free running counter to check the Static Unicast Entries
                                           with configured Static unicast limit */
   UINT4   u4SwitchMulticastLimit; /*Configured Multicast Limit by the user.
                                      By default, VLAN_DEV_MAX_MCAST_TABLE_SIZE */
   UINT4   u4SwitchCurrentMulticastCount; /*Free running counter to check the
                                             Multicast entries with configured multicast
                                             limit*/

   tVLAN_HASH_TABLE         *pProtGrpDb; 

   tVlanId MaxVlanId;
   UINT1   u1DefConstType;
   UINT1   u1VlanVersion;
   
   UINT1   u1VlanLearningType; 
   UINT1   u1TrfClassEnabled;
   UINT1   u1MacBasedClassification;  /*  mac based classification */
   UINT1   u1PortProtoBasedClassification; /* Port and protocol based,*/
   UINT1   u1SubnetBasedClassification;    /* Subnet based classification */

   UINT1   u1L2BpduPriority;  /* Def. priority to be used for BPDUs to be
                                    tunnelled */
   UINT1   u1HwIngTagSupport; /* This Flag will be set if the H/w supports
                                 tagging the packet recvd on a tunnel port that is 
                                 given to CPU*/
   UINT1   u1HwEgUnTagSupport; /* This Flag will be set if the H/w supports
                                 untagging the packet to be sent on a tunnel port 
                                 that is given from CPU*/
   UINT1   u1ProtocolAdminStatus;

   UINT1   u1GvrpStatus;
   UINT1   u1GmrpStatus;
   UINT1   u1MvrpStatus;
   UINT1   u1MmrpStatus;
   UINT1   au1Pad[3];
} tVlanInfo;
/***************************************************************************/

/*************************** Constraints Table Entry ***********************/
typedef struct _tVlanConstraintEntry {
   tVlanId               VlanId;
   UINT2                 u2ConstraintSet;
   UINT1                 u1ConstraintType;
   UINT1                 u1RowStatus;
   UINT2                 u2Reserved;
   UINT4                 u4FidIndex; /* Array index into the FID table */
} tVlanConstraintEntry;

/****************************** Mac Map Entry ******************************/
typedef struct _tVlanMacMapEntry {
   struct _tVlanMacMapEntry *pNextHashNode;
   UINT4                    u4Port;
   tMacAddr                 MacAddr;
   tVlanId                  VlanId;
   UINT1                    au1VlanName[VLAN_STATIC_MAX_NAME_LEN]; 
   UINT1                    u1RowStatus;
   UINT1                    u1VlanNameLen;
   UINT1                    u1McastOption;
   UINT1                    u1Pad;
} tVlanMacMapEntry;

typedef struct _tMacMapIndex {
    UINT4        u4Port;
    tMacAddr     MacAddr;
    UINT2        u2Pad;
} tMacMapIndex;
 
/***************************************************************************/

/************************* Subnet-Vlan Map Entry ***************************/
typedef UINT4       tIPAddr;

typedef struct _tVlanSubnetMapEntry {
   tVLAN_SLL_NODE           NextNode;
   tRBNodeEmbd              RBNode;
   UINT4                    u4Port;
   tIPAddr                  SubnetAddr;
   tIPAddr                  SrcIPAddr;
   tIPAddr                  SubnetMask;
   tVlanId                  VlanId;
   UINT1                    u1ArpOption;
   UINT1                    u1RowStatus;
} tVlanSubnetMapEntry;

typedef struct _tVlanSubnetMapIndex {
    UINT4        u4Port;
    tIPAddr      SubnetAddr;
    tIPAddr      SubnetMask;
} tVlanSubnetMapIndex;

/***************************************************************************/

typedef struct _VlanIfMsg {
   tMacAddr             SrcAddr;
   tMacAddr             DestAddr;
   UINT4                u4ContextId;  /*Virtual Switch Identifier*/
   UINT4                u4IfIndex; /* Physical port number */
   UINT4                u4TimeStamp;
#ifdef HVPLS_WANTED
   UINT4                u4PwVcIndex;
#endif
   tVlanId              SVlanId; /* Added for packets handling on PEP ports */
   UINT2                u2LocalPort; /* local port specific to context */
   UINT2                u2Length;
   UINT1                u1Priority; /* Incoming Pkt Priority from MPLS Module*/
   UINT1                u1MplsMode; /* RAW / TAGGED */
} tVlanIfMsg;

typedef struct _VlanQInPkt {
   tVlanIfMsg             VlanIfMsg;
   tCRU_BUF_CHAIN_HEADER *pPktInQ; 
} tVlanQInPkt;

typedef struct _VlanPoolIds {
   tMemPoolId VlanCurrPoolId;       /* For entries in VLAN Current Table */ 
   tMemPoolId VlanFdbPoolId;        /* For entries in VLAN FDB table */
   tMemPoolId VlanStUcastPoolId;    /* For Static Unicast entries */
   tMemPoolId VlanStMcastPoolId;    /* For Static Multicast entries */
   tMemPoolId VlanGroupPoolId;      /* For VLAN Group entries */
   tMemPoolId VlanMacMapPoolId;     /* For MAC-VLAN ID map entries */
   tMemPoolId VlanSubnetMapPoolId;     /* For Subnet-VLAN ID map entries */
   tMemPoolId VlanStEntriesPoolId;  /* For static VLAN entries */
   tMemPoolId VlanWildCardPoolId;    /* For WildCard entries */
   tMemPoolId VlanPortStatsPoolId;  /* For port status per VLAN entries */
   tMemPoolId VlanStatsPoolId;      /* For VLAN statistics */
   tMemPoolId VlanMacControlPoolId;  /* For MAC Learning Parameters */
   tMemPoolId VlanProtoGroupPoolId;  /* For protocol group table entries */
   tMemPoolId VlanPortProtoPoolId;  /* For port and protocol 
                                       based per VLAN entries */
   tMemPoolId VlanQMsgPoolId;  /* For Config Msg */
   tMemPoolId VlanPktQPoolId;  /* For Incoming packet */
   tMemPoolId VlanPortPoolId;  /* For Vlan Port entry table */
   tMemPoolId VlanContextPoolId;  /* For Vlan Port entry table */
   tMemPoolId VlanFidPoolId;  /* For Vlan Port entry table */
   tMemPoolId VlanTmpPortListId;  /* For MI Temp Portlist */
   tMemPoolId VlanMiscPoolId;  /* For MI to Form array ports and send it to NPAPI */
   tMemPoolId VlanPvlanPoolId;
   tMemPoolId  VlanL2VpnMapPoolId;   /* For tVlanL2VpnMap */
   tMemPoolId  VlanBasePortPoolId;   /* For IEEE 8021 Vlan Base Port table */  
   tMemPoolId  PortVlanMapPoolId;    /* For tPortVlanMapEntry */ 
   tMemPoolId  VlanHwStatsMapPoolId;    /* For VOID pointer in tVlanCurrEntry */ 
   tMemPoolId  VlanRedCachePoolId;    /* For Pending entries - VLAN Hardware Audit */ 
   tMemPoolId  VlanRedStUcastCachePoolId;    /* For Static unicast Pending entries - 
                                              * VLAN Hardware Audit */ 
   tMemPoolId  VlanRedStMcastCachePoolId;    /* For Static multicast Pending entries - 
                                              * VLAN Hardware Audit */ 
   tMemPoolId  VlanRedProtoVlanCachePoolId;    /* For Proto-based VLAN Pending entries - 
                                              * VLAN Hardware Audit */ 
   tMemPoolId   VlanMcagQMsgPoolId; /* For MAC Aging QMsg */
   tMemPoolId   VlanMcagMacAgingEntryPoolId; /* For MAC Aging Entries */
}tVlanPoolIds;

typedef struct _tVlanTaskInfo {
    tOsixSemId     VlanSemId; 
#ifdef SW_LEARNING
    tOsixSemId     VlanShadowTblSemId; 
#endif
    tVlanPoolIds   VlanPoolIds;
    tOsixTaskId    VlanTaskId;
    tOsixQId       VlanQId;
    tOsixQId       VlanCfgQId;
    tTimerListId   VlanTmrListId;
} tVlanTaskInfo;

#ifdef L2RED_WANTED 
/* tVlanRmData - Used to enqueue message to RM */
typedef struct _tVlanRmData {
    UINT4         u4Events;
    /* VOID should be made as tPeerId */
    VOID         *PeerId;
    tRmMsg       *pRmMsg;
    UINT2         u2DataLen;
    UINT1         au1Reserved[2];
}tVlanRmData;
#endif /* L2RED_WANTED */

/*Structure used for enqueuing FBD entry delete message*/
typedef struct _tFdbData {
    tVlanId      VlanId;
    tMacAddr     au1MacAddr;
    tMacAddr     au1ConnectionId;
    UINT1        au1Reserved[2];
}tFdbData;

/* EVB Application Id Information */
typedef struct _EvbAppId
{
    UINT1   au1OUI[VLAN_EVB_MAX_OUI_LEN];/* TLV OUI */
    UINT1   u1SubType;                   /* TLV Sub Type */
    UINT2   u2TlvType;                   /* TLV Type */
    UINT1   au1Reserved[2];              /* Padding */
}tEvbAppId;

/* EVB TLV Param Information from LLDP */
typedef struct _EvbTlvParam
{
    UINT1    au1EvbTlv[VLAN_EVB_CDCP_TLV_LEN]; /* CDCP TLV from LLDP */
    INT4     i4RemIndex;             /* Remote index for the remote entry */
    UINT4    u4RemLastUpdateTime;    /* Remote Last Update time*/
    tMacAddr RemLocalDestMacAddr;    /* Remote Destination MAC for this TLV*/
    UINT2    u2RxTlvLen;             /* TLV Receive length. */
}tEvbTlvParam;

/* EVB Queue Message */
typedef struct _EvbQueMsg
{
    UINT4        u4IfIndex;   /* Uap Interface Index */
    UINT4        u4MsgType;   /* Message Type */
    tEvbAppId    EvbAppId;    /* Application Id */
    tEvbTlvParam EvbTlvParam; /* Application TLV parameter */ 
}tEvbQueMsg;

typedef struct _tVlanQMsg {
   UINT1         au1FdbList[VLAN_LIST_SIZE];
   UINT4         u4ContextId; /*context */    
   UINT4         u4Port;
   UINT4         u4DstPort;
   UINT2         u2LocalPort; /* this is used when create port is called */
   UINT2         u2MsgType;
#ifdef SW_AGE_TIMER
   UINT4         u4FdbId;
   INT4          i4AgingTime;
#endif
#ifdef MBSM_WANTED
   struct MbsmIndication {
          tMbsmProtoMsg *pMbsmProtoMsg;
          }MbsmCardUpdate;
#endif
#ifdef L2RED_WANTED
   tVlanRmData     RmData;
#endif /* L2RED_WANTED */
#ifdef ICCH_WANTED
   tVlanIcchData IcchData; /* Icch Data posted by ICCH module */
#endif
   tEvbQueMsg    *pEvbQueMsg;
   tVplsInfo     L2VpnData;
   tFdbData      FdbData;
   UINT4         u4PwIndex;
   UINT1         u1EntryType;
   UINT1         u1IntfType;
   UINT1         u1ModType;
   UINT1         au1Reserved[1];
   BOOL1         bTunnelSTPStatus;
   UINT1         au1Pad[3];
}tVlanQMsg;

typedef struct _tVlanTimer{
   tTmrAppTimer  TmrNode;
   tVlanContextInfo *pVlanContextInfo; 
   UINT1         u1TmrType;
   UINT1         u1IsTmrActive;
   UINT1         au1Reserved [2];   
}tVlanTimer;

typedef tVlanMacMapEntry* tVlanMacMapHashTable [VLAN_MAX_MAC_BUCKETS];

typedef struct _tVlanFdbInfo
{
   tVLAN_SLL_NODE         NextNode;
   tRBNodeEmbd            RBNode;
   UINT4                  u4FdbId;
   UINT2                  u2RemoteId; /*To identify between LOCAL and REMOTE
                                        FDB Entry when MC-LAG is enabled*/
   tMacAddr               ConnectionId;
   UINT4                  u4PwVcIndex;
   UINT2                  u2Port;
   tMacAddr               MacAddr;
   UINT1                  u1EntryType;
   UINT1                  au1Pad[3]; /*The three padding bytes are used for
                                       special purposes like displaying remote
                                       FDB etc. and should be preserved */
}tVlanFdbInfo;

typedef struct  _tVlanPbFidControl{
    UINT4       u4UnicastMacLearningLimit;
    UINT2       u2Fid;
    UINT1       u1AdminUnicastMacLearningStatus;
    UINT1       u1OperationalUnicastMacLearningStatus;
}tVlanPbFidControl;

/************************* WildCard Entry ******************************/
typedef struct _tVlanWildCardEntry {
   tVLAN_SLL_NODE            NextNode;
   tLocalPortList            EgressPorts; 
   tMacAddr                  MacAddr;
   UINT1                     u1RowStatus;
   UINT1                     au1Pad[1];
} tVlanWildCardEntry;
/***************************************************************************/

/************************ Port Vlan Map Entry ***************************/
typedef struct __tPortVlanMapEntry {
    tRBNodeEmbd        RBNode;      /* RB Node */
    UINT2              u2VlanId;    /* VLAN identifier */
    UINT2              u2Port;      /* Local Port identifier */
    UINT2              u2BitMask;   /* Properties of u2Port for u2VlanId like
                                       VLAN_PORT_PROPERTY_MAC_LEARN,
                                       VLAN_PORT_PROPERTY_EGRESS,
                                       VLAN_PORT_PROPERTY_UNTAGGED,
                                       VLAN_PORT_PROPERTY_FORBIDDEN,
                                       VLAN_PORT_PROPERTY_CUR_EGRESS,
                                       VLAN_PORT_PROPERTY_CUR_LEARNT,
                                       VLAN_PORT_PROPERTY_CUR_TRUNK */
    BOOLEAN            bPortVlanFdbFlush;
    UINT1              au1Pad[1];   /*Padding */
} tPortVlanMapEntry;

struct VlanContextInfo {
   tRBTree             VlanPortList; /* list of port entries (tVlanPortEntry)*/ 
   tVlanTimer          VlanAgeOutTimer; /* ageout timer for FBD entries */
#ifdef MBSM_WANTED
   tVlanTimer          VlanMbsmTimer; /* Timer for MBSM Staggering */
#endif
#ifndef NPAPI_WANTED
   tVlanTimer          VlanPriTimer; /* Priority Queue flush timer */
#endif
   tVlanFidEntry       *apVlanFidTable[VLAN_MAX_FID_ENTRIES];
   tVlanCurrEntry      *apVlanCurrTable[VLAN_MAX_VLAN_ID + 2];
   tRBTree             VlanFdbInfo;
   tRBTree             pVlanEtherTypeSwapTable; /* Ethertype swap table */
   tRBTree             pVlanSVlanTranslationTable;/* VID translation table */
   tRBTree             pVlanLogicalPortTable;/* Logical port (PEP/CNP)table */
   tRBTree             apVlanSVlanTable[VLAN_NUM_SVLAN_CLASSIFY_TABLES];
   /* S-VLAN classification table.*/
   tStaticVlanEntry   *pStaticVlanTable;    /* Static VLAN Table */
   tStaticVlanEntry   *apStaticVlanInfo[VLAN_DEV_MAX_VLAN_ID];
   tVlanPortEntry     *apVlanPortEntry[VLAN_MAX_PORTS + 2]; /* array of 
                                                               pointers for local port based port table */
   tVlanBasePortEntry     *apVlanBasePortEntry[VLAN_MAX_PORTS + 1]; /* array of
                                                               pointers for Base port based port table */
   tVlanConstraintEntry aVlanConstraintTable[VLAN_MAX_CONSTRAINTS];
   tVLAN_SLL              WildCardTable;
   tVlanMacMapHashTable VlanMacMapTable;
   tRBTree              VlanSubnetMapTable;
   tRBTree              PortVlanMapTable;    /* Added for tPortVlanMapEntry*/
   tRBTree             VlanStaticUnicastInfo;
   tVlanPbFidControl   *apVlanPbFdbControl; /* A single block of size
                                               (sizeof(tVlanPbFidControl)*
                                               VLAN_MAX_FDB_ID) is created
                                               and used as an array*/
   tVlanInfo            VlanInfo;
   UINT4               u4ContextId; 
   UINT4               u4VlanDbg;
   UINT4               u4AgeOutTime;
   UINT4          u4BaseBridgeMode;
   INT4                 i4BridgeBaseRowStatus;
   tVlanId              VlanStartVlanInd;
   UINT2               u2VlanStartPortInd;
   UINT2               u2VlanLastPortInd;
   UINT2                u2NumVidTransEntries;
   tMacAddr             PropProviderDot1xAddr; /* Tunnel MAC addresses for LACP,
                                                  Dot1x, STP, GVRP, GMRP and IGMP 
                                                  are made context based.
                                                 */
   tMacAddr             PropProviderLacpAddr;
   tMacAddr             PropProviderStpAddr;
   tMacAddr             PropProviderGvrpAddr;
   tMacAddr             PropProviderMvrpAddr;
   tMacAddr             PropProviderGmrpAddr;
   tMacAddr             PropProviderMmrpAddr;
   tMacAddr             PropProviderElmiAddr;
   tMacAddr             PropProviderLldpAddr;
   tMacAddr             PropProviderEcfmAddr;
   tMacAddr             PropProviderEoamAddr;
   tMacAddr             PropProviderIgmpAddr;
   tMacAddr             PropProviderOtherAddr;
   tMacAddr             VlanSysMacAddress;
   UINT2                u2UserDefinedTPID; /*User defined TPID configurable for a Port */
   UINT1                au1ContextStr[VLAN_CONTEXT_ALIAS_LEN + 1];
   UINT1                u1GlobalMacLearningStatus; /*Global control for Mac Learning
                                                     Status*/
   UINT1                u1ApplyEnhancedFilteringCriteria;
   BOOLEAN              bGlobalFdbFlush;
   BOOLEAN              bRemoteFdbFlush; /* To flush remote FDB entries from MC-LAG node.*/
   UINT1                     au1Pad;
};

typedef struct _tWildCardTbl {
   tLocalPortList            EgressPorts; 
   tMacAddr                  MacAddr;
   UINT1                     au1Pad[2];
} tWildCardTbl;

typedef struct _tFwdAllUnregTbl{
   tLocalPortList      StaticPorts;    /* Local Static Egress Portlist*/
   tLocalPortList      ForbiddenPorts; /* Local Forbidden Portlist */
   tVlanId             VlanId;         /* Index - VlanId */
   UINT1               au1Reserved[2]; /* Padding */
}tFwdAllUnregTbl;

typedef struct _tStUcastTbl{
   tLocalPortList      AllowedToGo;    /* Local AllowedToGo Portlist */
   UINT4               u4FdbId;        /* Index - FDB-Id */
   tMacAddr            MacAddr;        /* Index - MAC-Address */
   tMacAddr            ConnectionId;
   UINT2               u2RcvPort;      /* Index - Receive Port */
   UINT1               u1Status;       /* Static Unicast Status */
   UINT1               au1Reserved;    /* Padding */
}tStUcastTbl;

typedef struct _tStMcastTbl{
   tLocalPortList      StaticPorts;    /* Local Static Egress Portlist */
   tLocalPortList      ForbiddenPorts; /* Local Forbidden Portlist */
   tMacAddr            MacAddr;        /* Index - MAC-Address */
   tVlanId             VlanId;         /* Index - Vlan */
   UINT2               u2RcvPort;      /* Index - Receive Port */
   UINT1               au1Reserved[2]; /* Padding */
}tStMcastTbl;

typedef struct _tVlanTempPortList{
   tVLAN_SLL_NODE       NextNode;
   UINT4                u4ContextId;   /* Index - Context-Id */
   union {
       tFwdAllUnregTbl  FwdTbl;
       tStUcastTbl      StUcastTbl;
       tStMcastTbl      StMcastTbl;
       tWildCardTbl     WildCardTbl;
   } PortListTbl;
   UINT1                u1Type;        /* Index - Type of entry 
                                                  VLAN_FORWARD_ALL_TABLE 
                                                  VLAN_FORWARD_UNREG_TABLE 
                                                  VLAN_ST_UCAST_TABLE
                                                  VLAN_ST_MCAST_TABLE
                                                  VLAN_WILDCARD_TABLE */
   UINT1               au1Reserved[3]; /* Padding */
}tVlanTempPortList;

#define VLAN_INCR_HC_OR_TP_FILTER_IN_DISCARDS(u4PortNo)                \
   if (VLAN_FILTER_IN_DISCARD_OVERFLOW(u4PortNo) == TRUE ) {           \
         VLAN_GET_PORT_ENTRY (u2Port)->u4FilterInDiscardOverflow++;       \
   }                                                                  \
   else {                                                             \
          VLAN_GET_PORT_ENTRY (u2Port)->u4NoFilterInDiscard++;\
   }

#define VLAN_FILTER_IN_DISCARD_OVERFLOW(u4PortNo)                      \
   ( (VLAN_GET_PORT_ENTRY (u2Port)->u4IfSpeed > VLAN_HC_PORT_SPEED) &&     \
     ((VLAN_GET_PORT_ENTRY (u2Port)->u4NoFilterInDiscard &               \
     0xffffffff) ==  0xffffffff) )                                   \

#define  VLAN_INCR_PORT_FILTER_IN_DISCARDS(u4PortNo)        \
     VLAN_GET_PORT_ENTRY (u2Port)->u4NoFilterInDiscard++;

/*********************Provider Bridging related Data structure*********/

typedef struct _tVlanSVlanClassificationParams {
    UINT2                 u2Port;
    tVlanId               CVlanId;
    tMacAddr              SrcMacAddr;
    tMacAddr              DstMacAddr;
    UINT4                 u4SrcIp;
    UINT4                 u4DstIp;
    UINT4                 u4Dscp;
    UINT1                 u1FrameType;
    UINT1                 au1Reserved[3];
}tVlanSVlanClassificationParams; 

/*********************Provider Bridging related Data structure*********/

/************** MPLS VLAN Interaction *********************************/

typedef struct VlanL2VpnMap {
    tRBNodeEmbd   L2VpnPortBased;  /*Port Based Service Type RBTree*/
    tRBNodeEmbd   L2VpnPortVlanBased; /* (Port+Vlan)/ (Vlan) Based Service
                                           Type RBTree*/
    UINT4         u4ContextId;         /*Virtual Switch identifier*/
    UINT4         u4IfIndex;           /* Interface Identifier*/
    UINT4         u4VplsIndex;         /* VPLS Identifier */
    tVlanId       VlanId;            /* VLAN Identifier */
    UINT1         u1PwVcMode;
    UINT1         au1Reserved[1];    /*For Padding */
} tVlanL2VpnMap;

/************** MPLS VLAN Interaction *********************************/

typedef struct _tHwMiVlanEntry {
    tLocalPortList    HwMemberPbmp;
    tLocalPortList    HwUntagPbmp;
    tLocalPortList    HwFwdAllPbmp;   /* Only if supported */
    tLocalPortList    HwFwdUnregPbmp; /* Only if supported */ 
}tHwMiVlanEntry;

/*************** Trap releated structure ****************************/

typedef struct _tVlanTempFdbInfo {
    tMacAddr  au1ConnectionId;
    UINT2     u2Port;
    UINT1     u1Status;
    UINT1     au1Reserved[3];
}tVlanTempFdbInfo;

/* BELOW STRUCTURE IS USED FOR MEMPOOL CREATION ONLY*/
/* START */
typedef struct NpVlanPortArray{
    UINT4  PortArray[VLAN_MAX_PORTS];
}tVlanNpPortArray;
typedef struct VlanPvlanEntry{
    UINT2  PvlanArray[VLAN_MAX_COMMUNITY_VLANS + 1];
}tVlanPvlanEntry;
/*END */

/* To enable/disable Per VLAN RMON Counter Stats */
#define VLAN_COUNTER_ENABLE  1
#define VLAN_COUNTER_DISABLE 2
#define VLAN_CNTR_STATUS pVlanEntry->i4VlanCounterStatus


/***************ICCH Related structure*******************************/

typedef struct _tMcagTaskInfo {
    tOsixSemId     McagSemId;
    tOsixTaskId    McagTaskId;
    tOsixQId       McagQId;
    tTimerListId   MacAgingTmrListId;
} tMcagTaskInfo;

typedef struct _tMcagTimer{
   tTmrAppTimer  TmrNode;
   UINT1         u1TmrType;
   UINT1         u1IsTmrActive;
   UINT1         au1Reserved [2];
}tMcagTimer;

typedef struct _tMcagQMsg {
   UINT2         u2MsgType;
   tFdbData      FdbData;
}tMcagQMsg;

typedef struct _tVlanMcagEntries{
    tVLAN_SLL_NODE       NextNode;
    tFdbData      FdbData;
}tVlanMcagEntries;

#endif
