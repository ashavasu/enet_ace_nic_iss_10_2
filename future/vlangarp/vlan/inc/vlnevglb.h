/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevglb.h,v 1.3 2016/02/02 12:20:10 siva Exp $
 *
 * Description: This file contains VLAN EVB global declarations. 
 *
 *******************************************************************/
#ifndef _VLNEVGLB_H
#define _VLNEVGLB_H

tEvbGlobalInfo gEvbGlobalInfo;

UINT1 gau1EvbCdcpOUI [VLAN_EVB_CDCP_TLV_OUI_LEN] = {0x00,0x80,0xc2};
UINT1 gu1EvbCdcpSubtype = {0x0e};
UINT4 gu4EvbBulkUpdPrevPort = 0;
UINT4 gu4EvbBulkUpdPrevContxt = 0;

#endif  /* _VLNEVGLB_H */


