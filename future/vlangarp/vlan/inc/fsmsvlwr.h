#ifndef _FSMSVLWR_H
#define _FSMSVLWR_H
INT4 GetNextIndexFsDot1qBaseTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMSVL(VOID);

VOID UnRegisterFSMSVL(VOID);
INT4 FsDot1qVlanVersionNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qMaxVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qMaxSupportedVlansGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qNumVlansGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qGvrpStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qGvrpStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qGvrpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qBaseTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDot1qFdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qFdbDynamicCountGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1qTpFdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qTpFdbPortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qTpFdbStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qTpFdbPwGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1qTpGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qTpGroupIsLearntGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1qForwardAllLearntPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qForwardAllIsLearntGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1qForwardAllStatusTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qForwardAllRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardAllRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardAllRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardAllStatusTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDot1qForwardAllPortConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qForwardAllPortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardAllPortSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardAllPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardAllPortConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDot1qForwardUnregLearntPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qForwardUnregIsLearntGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1qForwardUnregStatusTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qForwardUnregRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardUnregRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardUnregRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardUnregStatusTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDot1qForwardUnregPortConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qForwardUnregPortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardUnregPortSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardUnregPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qForwardUnregPortConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDot1qStaticUnicastTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qStaticUnicastRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticUnicastStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticUnicastRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticUnicastStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticUnicastRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticUnicastStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticUnicastTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsDot1qStaticAllowedToGoTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qStaticAllowedIsMemberGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticAllowedIsMemberSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticAllowedIsMemberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticAllowedToGoTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDot1qStaticMulticastTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qStaticMulticastRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticMulticastStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticMulticastRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticMulticastStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticMulticastRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticMulticastStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticMulticastTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsDot1qStaticMcastPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qStaticMcastPortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticMcastPortSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticMcastPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qStaticMcastPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDot1qVlanNumDeletesTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qVlanNumDeletesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1qVlanCurrentTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qVlanFdbIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qVlanStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qVlanCreationTimeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1qVlanEgressPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qVlanCurrentEgressPortGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1qVlanStaticTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qVlanStaticNameGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qVlanStaticRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qVlanStaticNameSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qVlanStaticRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qVlanStaticNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qVlanStaticRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qVlanStaticTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsDot1qVlanStaticPortConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qVlanStaticPortGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qVlanStaticPortSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qVlanStaticPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qVlanStaticPortConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDot1qNextFreeLocalVlanIndexTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qNextFreeLocalVlanIndexGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1qPortVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qPvidGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortAcceptableFrameTypesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortIngressFilteringGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortGvrpStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortGvrpFailedRegistrationsGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortGvrpLastPduOriginGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortRestrictedVlanRegistrationGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPvidSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortAcceptableFrameTypesSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortIngressFilteringSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortGvrpStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortRestrictedVlanRegistrationSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qPvidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortAcceptableFrameTypesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortIngressFilteringTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortGvrpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortRestrictedVlanRegistrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qPortVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexFsDot1qPortVlanStatisticsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qTpVlanPortInFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qTpVlanPortOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qTpVlanPortInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qTpVlanPortInOverflowFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qTpVlanPortOutOverflowFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qTpVlanPortInOverflowDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1qPortVlanHCStatisticsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qTpVlanPortHCInFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qTpVlanPortHCOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qTpVlanPortHCInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1qLearningConstraintsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qConstraintTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qConstraintStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qConstraintTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qConstraintStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qConstraintTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qConstraintStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qLearningConstraintsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsDot1qConstraintDefaultTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1qConstraintSetDefaultGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qConstraintTypeDefaultGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qConstraintSetDefaultSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qConstraintTypeDefaultSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1qConstraintSetDefaultTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qConstraintTypeDefaultTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1qConstraintDefaultTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsDot1vProtocolGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1vProtocolGroupIdGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolGroupRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolGroupIdSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolGroupRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolGroupIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolGroupRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolGroupTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsDot1vProtocolPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1vProtocolPortGroupVidGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolPortGroupVidSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolPortGroupVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1vProtocolPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


#endif /* _FSMSVLWR_H */
