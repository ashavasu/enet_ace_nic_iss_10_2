/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlminpwr.h,v 1.26 2015/09/29 10:24:31 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#ifndef _VLANMINP_H
#define _VLANMINP_H


INT4 FsMiVlanHwInit PROTO ((UINT4 u4ContextId));
INT4 FsMiVlanHwDeInit PROTO ((UINT4 u4ContextId));
INT4 FsMiWrVlanHwSetAllGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, tPortList Ports));
INT4 FsMiWrVlanHwConfigSChIface PROTO ((tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo));
INT4 FsMiWrVlanHwResetAllGroupsPorts  PROTO ((UINT4 u4ContextId, tVlanId, tPortList));
INT4 FsMiWrVlanHwSetUnRegGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, tPortList Ports));
INT4 FsMiWrVlanHwResetUnRegGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId, tPortList));
INT4 FsMiWrVlanHwAddStaticUcastEntry PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr, 
                                UINT4 u4RcvPort, tLocalPortList AllowedToGoPorts, 
    UINT1 u1Status, tMacAddr ConnectionId));
INT4 FsMiVlanHwDelStaticUcastEntry PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr, 
                        UINT4 u4RcvPort));
INT1
FsMiBrgSetAgingTime (UINT4 u4ContextId, INT4 i4AgingTime);

INT4 FsMiVlanHwSetAllGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                        tHwPortArray *HwPortArray));
INT4 FsMiVlanHwConfigSChIface PROTO ((tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo));

INT4 FsMiVlanHwResetAllGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                    tHwPortArray *HwPortArray));
INT4 FsMiVlanHwResetAllGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                           tHwPortArray *HwPortArray));
INT4 FsMiVlanHwSetUnRegGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                           tHwPortArray *HwPortArray));
INT4 FsMiVlanHwResetUnRegGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                             tHwPortArray *HwPortArray));
INT4 FsMiVlanHwAddMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                     tMacAddr MacAddr, tHwPortArray *HwPortArray));
INT4 FsMiVlanHwAddStMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                       tMacAddr MacAddr, INT4 i4RcvPort, 
                                       tHwPortArray *HwPortArray));
INT4 FsMiVlanHwAddVlanEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                    tHwPortArray *HwPortArray, 
                                    tHwPortArray *HwUnTagPortArray));
INT4 FsMiVlanHwAddStaticUcastEntry PROTO ((UINT4 u4ContextId, UINT4 u4Fid, 
                                           tMacAddr MacAddr, UINT4 u4Port, 
                                           tHwPortArray *HwPortArray, UINT1 u1Status));
INT4 FsMiVlanHwMacLearningStatus PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                         UINT2 u2FdbId, tHwPortArray *HwPortArray, 
                                         UINT1 u1Status));

#ifndef SW_LEARNING
INT4
FsMiVlanHwGetFdbCount PROTO ((UINT4 u4ContextId, UINT4 u4FdbId, UINT4 *pu4Count));

INT4
FsMiVlanHwGetFirstTpFdbEntry PROTO ((UINT4 u4ContextId, UINT4 *pu4FdbId, tMacAddr MacAddr));

INT4
FsMiVlanHwGetNextTpFdbEntry PROTO ((UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr, UINT4* pu4NextContextId,
                           UINT4 *pu4NextFdbId, tMacAddr NextMacAddr));

INT4 FsMiVlanHwGetFdbEntry PROTO ((UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr, 
                        tHwUnicastMacEntry *pEntry));
    
#endif

INT4 FsMiWrVlanHwAddMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, 
                        tPortList PortList));

INT4 FsMiWrVlanHwAddStMcastEntry PROTO ((UINT4 u4ContextId, tVlanId, tMacAddr , INT4, tPortList));

INT4 FsMiVlanHwSetMcastPort PROTO ((UINT4 u4ContextId, tVlanId      VlanId, tMacAddr MacAddr, 
                        UINT4       u4Port));

INT4 FsMiVlanHwResetMcastPort PROTO ((UINT4 u4ContextId, tVlanId      VlanId, tMacAddr MacAddr, 
                        UINT4       u4Port));

INT4 FsMiVlanHwDelMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr));

INT4 FsMiVlanHwDelStMcastEntry PROTO ((UINT4 u4ContextId, tVlanId, tMacAddr, INT4));

INT4 FsMiWrVlanHwAddVlanEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tPortList HwPortList, 
                        tPortList UnTagPorts));
INT4 FsMiVlanHwDelVlanEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId));

INT4 FsMiWrVlanHwMacLearningStatus (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2FdbId,
                                    tLocalPortList HwPortList, UINT1 u1Status);

INT4 FsMiVlanHwSetVlanMemberPort PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT4 u4Port, 
                        UINT1 u1IsTagged));
INT4 FsMiVlanHwResetVlanMemberPort PROTO ((UINT4 u4ContextId, tVlanId VlanId, UINT4 u4Port));

INT4 FsMiVlanHwSetPortPvid PROTO ((UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId));
#ifdef L2RED_WANTED
INT4 FsMiVlanHwSyncDefaultVlanId PROTO ((UINT4 u4ContextId, tVlanId VlanId));
INT4 FsMiVlanRedHwUpdateDBForDefaultVlanId PROTO ((UINT4 u4ContextId, 
                                             tVlanId VlanId));
#endif
INT4 FsMiVlanHwSetDefaultVlanId PROTO ((UINT4 u4ContextId, tVlanId DefaultVlanId));
INT4 FsMiVlanHwSetPortAccFrameType PROTO ((UINT4 u4ContextId, UINT4 u4Port, INT1 i1AccFrameType));
INT4 FsMiVlanHwSetPortIngFiltering PROTO ((UINT4 u4ContextId, UINT4 u4Port, UINT1 u1IngFiltering));
INT4 FsMiVlanHwVlanEnable PROTO ((UINT4 u4ContextId));
INT4 FsMiVlanHwVlanDisable PROTO ((UINT4 u4ContextId));

INT4 FsMiVlanHwSetVlanLearningType PROTO ((UINT4 u4ContextId, UINT1 u1LearningType));
INT4 FsMiVlanHwSetMacBasedStatusOnPort PROTO((UINT4, UINT4, UINT1));
INT4 FsMiVlanHwSetSubnetBasedStatusOnPort PROTO((UINT4, UINT4, UINT1));
INT4 FsMiVlanHwAddPortMacVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
        tMacAddr MacAddr, tVlanId VlanId, BOOL1 bSuppressOption);
INT4 FsMiVlanHwAddPortSubnetVlanEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
        UINT4 SubnetAddr, tVlanId VlanId, BOOL1 bARPOption));

INT4 FsMiVlanHwDeletePortMacVlanEntry (UINT4 u4ContextId,UINT4 u4IfIndex,
        tMacAddr MacAddr);
INT4 FsMiVlanHwDeletePortSubnetVlanEntry PROTO ((UINT4 u4ContextId,UINT4 u4IfIndex,
        UINT4 SubnetAddr));

INT4 FsMiVlanHwEnableProtoVlanOnPort PROTO((UINT4, UINT4, UINT1));
INT4 FsMiVlanHwSetDefUserPriority PROTO ((UINT4 u4ContextId, UINT4 u4Port, INT4 i4DefPriority));
INT4 FsMiVlanHwSetPortNumTrafClasses PROTO ((UINT4 u4ContextId, UINT4 u4InPort, 
                        INT4 i4NumTraffClass));
INT4 FsMiVlanHwSetRegenUserPriority PROTO ((UINT4 u4ContextId, UINT4 u4Port, INT4 i4UserPriority, 
                        INT4 i4RegenPriority));
INT4 FsMiVlanHwSetTraffClassMap PROTO ((UINT4 u4ContextId, UINT4 u4InPort, INT4 i4UserPriority, 
                        INT4 i4TraffClass));
INT4 FsMiVlanHwGmrpEnable PROTO ((UINT4 u4ContextId));
INT4 FsMiVlanHwGmrpDisable PROTO ((UINT4 u4ContextId));

INT4 FsMiVlanHwGvrpEnable PROTO ((UINT4 u4ContextId));
INT4 FsMiVlanHwGvrpDisable PROTO ((UINT4 u4ContextId));
INT4 FsMiNpDeleteAllFdbEntries PROTO((UINT4 u4ContextId));

INT4 FsMiVlanHwAddVlanProtocolMap PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4GroupId,
                            tVlanProtoTemplate * pProtoTemplate, 
                            tVlanId VlanId));
INT4 FsMiVlanHwDelVlanProtocolMap PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4GroupId,
                            tVlanProtoTemplate * pProtoTemplate));

INT4 FsMiVlanHwGetPortStats PROTO ((UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId, 
                          UINT1 u1StatsType,
                          UINT4 *pu4PortStatsValue));

INT4 FsMiVlanHwGetPortStats64 PROTO ((UINT4 u4ContextId, UINT4 u4Port, tVlanId VlanId, 
                                    UINT1 u1StatsType,
                             tSNMP_COUNTER64_TYPE *pValue));

INT4 FsMiVlanHwSetPortTunnelMode      PROTO ((UINT4 u4ContextId, UINT4, UINT4));
INT4 FsMiVlanHwSetTunnelFilter PROTO ((UINT4 u4ContextId, INT4));
INT4 FsMiVlanHwCheckTagAtEgress PROTO ((UINT4 u4ContextId, UINT1 *));
INT4 FsMiVlanHwCheckTagAtIngress PROTO ((UINT4 u4ContextId, UINT1 *));

INT4 FsMiVlanHwCreateFdbId PROTO ((UINT4 u4ContextId, UINT4 u4Fid));
INT4 FsMiVlanHwDeleteFdbId PROTO ((UINT4 u4ContextId, UINT4 u4Fid));
INT4 FsMiVlanHwAssociateVlanFdb PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tVlanId VlanId));
INT4 FsMiVlanHwDisassociateVlanFdb PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tVlanId VlanId));

INT4 FsMiVlanHwFlushPortFdbId PROTO ((UINT4 u4ContextId, UINT4 u4Port, UINT4 u4Fid, 
                                    INT4 i4OptimizeFlag));

INT4 FsMiVlanHwFlushPort PROTO ((UINT4 u4ContextId, UINT4 u4Port, INT4 i4OptimizeFlag));
INT4 FsMiVlanHwFlushFdbId PROTO ((UINT4 u4ContextId, UINT4 u4Fid));

INT4 FsMiVlanHwSetShortAgeout PROTO ((UINT4 u4ContextId, UINT4 u4PortNum, INT4 i4AgingTime));
INT4 FsMiVlanHwResetShortAgeout PROTO ((UINT4 u4ContextId, UINT4 u4PortNum, INT4 i4LongAgeout));

INT4 FsMiVlanHwGetVlanInfo PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                                   tHwVlanPortArray * pHwEntry));
INT4 FsMiWrVlanHwGetVlanInfo PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                                     tHwMiVlanEntry * pHwEntry));
INT4 FsMiVlanHwGetMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                     tMacAddr MacAddr, 
                                     tHwPortArray *HwPortArray));
INT4 FsMiWrVlanHwGetMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                       tMacAddr MacAddr, tPortList HwPortList));
INT4 FsMiVlanHwTraffClassMapInit PROTO ((UINT4 u4ContextId, UINT1 u1Priority, INT4 i4CosqValue));

INT4 FsMiVlanHwSetBrgMode PROTO ((UINT4 u4ContextId, UINT4 u4BridgeMode));

INT4 FsMiVlanHwSetPortIngressEtherType PROTO ((UINT4 u4ContextId,
                                   UINT4 u4IfIndex, UINT2 u2EtherType));
INT4 FsMiVlanHwSetPortEgressEtherType PROTO ((UINT4 u4ContextId,
                                   UINT4 u4IfIndex, UINT2 u2EtherType));
INT4 FsMiVlanHwGetVlanStats PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                        UINT1 u1StatsType, UINT4 *pu4VlanStatsValue));
INT4 FsMiVlanHwResetVlanStats PROTO ((UINT4 u4ContextId, tVlanId VlanId));
INT4
FsMiVlanHwMacLearningLimit (UINT4 u4ContextId, tVlanId VlanId, 
                            UINT2 u2FdbId, UINT4 u4MacLimit);

INT4
FsMiWrVlanHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid,
                                      tLocalPortList PortBmp, UINT4 u4Port,
                                      UINT1 u1Action);

INT4
FsMiVlanHwSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                     tVlanHwTunnelFilters ProtocolId,
                                         UINT4 u4TunnelStatus);

INT4
FsMiVlanHwSetPortProtectedStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                  INT4 u4ProtectedStatus);

INT4
FsMiVlanHwSwitchMacLearningLimit (UINT4 u4ContextId, UINT4 u4MacLimit);

#ifdef L2RED_WANTED
typedef VOID (*FsMiVlanHwProtoCb)(UINT4 u4Port, UINT4 u4GroupId, 
                                tVlanProtoTemplate *pProto,
                                tVlanId VlanId);

typedef VOID (*FsMiVlanHwMcastCb)(tVlanId VlanId, tMacAddr  MacAddr, 
                                    UINT4 u4RcvPort, tPortList PortList);

typedef VOID (*FsMiVlanHwUcastCb)(UINT4 u4FdbId, tMacAddr  MacAddr, 
                                UINT4 u4RcvPort, tHwUnicastMacEntry *pEntry, 
                                tPortList AllowedList, UINT1 u1Status);

INT4 FsMiVlanHwScanProtocolVlanTbl (UINT4 u4ContextId, UINT4 u4Port, 
                                  FsMiVlanHwProtoCb ProtoVlanCallBack);
INT4 FsMiVlanHwGetVlanProtocolMap (UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4GroupId,
                                 tVlanProtoTemplate * pProtoTemplate, 
                                 tVlanId *pVlanId);
INT4 FsMiVlanHwScanMulticastTbl (UINT4 u4ContextId, FsMiVlanHwMcastCb McastCallBack);
INT4 FsMiVlanHwGetStMcastEntry (UINT4 u4ContextId, tVlanId VlanId, 
                                tMacAddr MacAddr, UINT4 u4RcvPort, 
                                tHwPortArray *pHwPortArray);
INT4 FsMiWrVlanHwGetStMcastEntry (UINT4 u4ContextId, tVlanId VlanId, 
                                  tMacAddr MacAddr, UINT4 u4RcvPort, 
                                  tPortList HwPortList);
INT4 FsMiVlanHwScanUnicastTbl (UINT4 u4ContextId, FsMiVlanHwUcastCb UcastCallBack);
INT4 FsMiVlanHwGetStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid, 
                                    tMacAddr MacAddr, UINT4 u4Port, 
                                    tHwPortArray *pHwPortArray,
                                    UINT1 *pu1Status);
INT4 FsMiVlanHwGetStaticUcastEntryEx (UINT4 u4ContextId, UINT4 u4Fid,
                                    tMacAddr MacAddr, UINT4 u4Port,
                                    tHwPortArray *pHwPortArray,
                                    UINT1 *pu1Status, tMacAddr ConnectionId);
INT4 FsMiWrVlanHwGetStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid, 
                                      tMacAddr MacAddr, UINT4 u4Port, 
                                      tPortList AllowedToGoPortBmp,
                                      UINT1 *pu1Status);
INT4 FsMiWrVlanHwGetStaticUcastEntryEx (UINT4 u4ContextId, UINT4 u4Fid,
                                      tMacAddr MacAddr, UINT4 u4Port,
                                      tPortList AllowedToGoPortBmp,
                                      UINT1 *pu1Status, tMacAddr ConnectionId);
INT4 FsMiVlanHwGetFirstProtocolVlanEntry (UINT4 *pu4Port, 
                                        tVlanProtoTemplate *pProtoTemplate,
                                        UINT4 *pu4GroupId, 
                                        tVlanId *pVlanId);
INT4 FsMiVlanHwGetNextProtocolVlanEntry (UINT4 u4CurrPort, 
                                       UINT4 *pu4NextPort, 
                                       tVlanProtoTemplate *pCurrProtoTemplate,
                                       tVlanProtoTemplate *pNextProtoTemplate,
                                       UINT4 u4CurrGroupId, 
                                       UINT4 *pu4NextGroupId, 
                                       tVlanId *pVlanId);
INT4 FsMiVlanHwGetFirstL2UcastEntry (UINT4 *pu4FdbId, tMacAddr MacAddr, 
                                   tHwUnicastMacEntry *pHwEntry);
INT4 FsMiVlanHwGetNextL2UcastEntry (UINT4 u4CurrFdbId, UINT4 *pu4NextFdbId, 
                                  tMacAddr CurrMacAddr, 
                                  tMacAddr NextMacAddr, 
                                  tHwUnicastMacEntry *pHwEntry);
INT4 FsMiVlanHwGetSyncedTnlProtocolMacAddr (UINT4 u4ContextId,
                                            UINT2 u2Protocol, 
                                            tMacAddr MacAddr);

#endif

INT4 FsMiVlanNpHwRunMacAgeing (UINT4 u4ContextId);

#ifdef MBSM_WANTED
INT4 FsMiVlanMbsmHwInit                   (UINT4, tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetDefaultVlanId       (UINT4, tVlanId, tMbsmSlotInfo *);
INT1 FsMiBrgMbsmSetAgingTime              (UINT4, INT4, tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetAllGroupsPorts      (UINT4, tVlanId, tHwPortArray *, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwEvbConfigSChIface      (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetUnRegGroupsPorts    (UINT4, tVlanId, 
                                           tHwPortArray *, tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwAddStaticUcastEntry    (UINT4, UINT4, tMacAddr, UINT4, 
                                           tHwPortArray *, UINT1, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetUcastPort           (UINT4 u4ContextId, UINT4 u4FdbId,
                                           tMacAddr MacAddr, UINT4 u4RcvPort,
                                           UINT4 u4AllowedToGoPort,
                                           UINT1 u1Status, tMbsmSlotInfo * pSlotInfo);
INT4 FsMiVlanMbsmHwResetUcastPort         (UINT4 u4ContextId, UINT4 u4FdbId,
                                           tMacAddr MacAddr, UINT4 u4RcvPort,
                                           UINT4 u4AllowedToGoPort,
                                           tMbsmSlotInfo * pSlotInfo);
INT4 FsMiVlanMbsmHwDelStaticUcastEntry    (UINT4, UINT4, tMacAddr, UINT4, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwAddMcastEntry          (UINT4, tVlanId, tMacAddr, 
                                           tHwPortArray *, tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwAddStMcastEntry        (UINT4, tVlanId, tMacAddr, INT4, 
                                           tHwPortArray *, tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwDelStMcastEntry        (UINT4 u4ContextId, tVlanId VlanId,
                                           tMacAddr MacAddr, UINT4 u4RcvPort,
                                           tMbsmSlotInfo * pSlotInfo);
INT4 FsMiVlanMbsmHwSetMcastPort           (UINT4 u4ContextId, tVlanId VlanId,
                                           tMacAddr MacAddr, UINT4 u4Port,
                                           tMbsmSlotInfo * pSlotInfo);
INT4 FsMiVlanMbsmHwResetMcastPort         (UINT4, tVlanId, tMacAddr, UINT4, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwAddVlanEntry           (UINT4, tVlanId, tHwPortArray *, 
                                           tHwPortArray *, tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetVlanMemberPort      (UINT4 u4ContextId, tVlanId VlanId,
                                           UINT4 u4Port, UINT1 u1IsTagged,
                                           tMbsmSlotInfo * pSlotInfo);
INT4 FsMiVlanMbsmHwResetVlanMemberPort    (UINT4, tVlanId, UINT4, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetPortPvid            (UINT4, UINT4, tVlanId, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetPortAccFrameType    (UINT4, UINT4, UINT1, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetPortIngFiltering    (UINT4, UINT4, UINT1, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwEnableProtoVlanOnPort  (UINT4, UINT4, UINT1, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetDefUserPriority     (UINT4, UINT4, INT4, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetPortNumTrafClasses  (UINT4, UINT4, INT4, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetRegenUserPriority   (UINT4, UINT4, INT4, INT4, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetTraffClassMap       (UINT4, UINT4, INT4, INT4, 
                                           tMbsmSlotInfo *);
INT4 FsMiVlanMbsmHwSetTunnelFilter        (UINT4, INT4, tMacAddr, UINT2,
                                           tMbsmSlotInfo *);

INT4 FsMiVlanMbsmHwTraffClassMapInit      (UINT4, UINT1, INT4, tMbsmSlotInfo *);

INT4 FsMiWrVlanMbsmHwAddVlanEntry         (UINT4, tVlanId, tPortList, 
                                           tLocalPortList, tMbsmSlotInfo *);
                                           
INT4 FsMiWrVlanMbsmHwAddStaticUcastEntry  (UINT4, UINT4, tMacAddr, UINT4, 
                                           tLocalPortList, UINT1, 
                                           tMbsmSlotInfo *, tMacAddr);

INT4 FsMiWrVlanMbsmHwAddStMcastEntry      (UINT4, tVlanId, tMacAddr, INT4, 
                                           tLocalPortList, tMbsmSlotInfo *);
                                           
INT4 FsMiWrVlanMbsmHwAddMcastEntry        (UINT4, tVlanId, tMacAddr, 
                                           tLocalPortList, tMbsmSlotInfo *);
                                           
INT4 FsMiWrVlanMbsmHwSetAllGroupsPorts    (UINT4, tVlanId, tLocalPortList, 
                                           tMbsmSlotInfo *);
INT4 FsMiWrVlanMbsmHwEvbConfigSChIface    (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo,
                                           tMbsmSlotInfo *);
                                           
INT4 FsMiWrVlanMbsmHwSetUnRegGroupsPorts  (UINT4, tVlanId, tLocalPortList, 
                                           tMbsmSlotInfo *);
INT4
FsMiVlanMbsmHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid, 
                                        UINT4 u4Port, UINT1 u1Action,
                                        tMbsmSlotInfo * pSlotInfo);
INT4
FsMiVlanMbsmHwMacLearningLimit (UINT4 u4ContextId, tVlanId VlanId,
                                UINT2 u2FdbId, UINT4 u4MacLimit,
                                tMbsmSlotInfo * pSlotInfo);
INT4
FsMiVlanMbsmHwSwitchMacLearningLimit (UINT4 u4ContextId, UINT4 u4MacLimit,
                                      tMbsmSlotInfo * pSlotInfo);
#endif
INT4 FsMiVlanHwSetVlanLoopbackStatus PROTO ((UINT4 u4ContextId,
                                   tVlanId VlanId, INT4 i4LoopbackStatus));

INT4 FsMiVlanHwGetPortFromFdb (UINT2 u2VlanId, UINT1 *MacAddr, UINT4 *pu4Port);
#endif
