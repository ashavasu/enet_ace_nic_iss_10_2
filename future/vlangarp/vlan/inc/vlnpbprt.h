
/* $Id: vlnpbprt.h,v 1.31 2015/11/20 10:20:26 siva Exp $*/
#ifndef _VLNPBPRT_H
#define _VLNPBPRT_H

INT4 VlanPbInit PROTO ((VOID));

VOID VlanPbDeInit PROTO ((VOID));

INT4 VlanPbTableInit PROTO ((VOID));

VOID VlanPbTableDeInit PROTO ((VOID));

UINT1* VlanPbGetMemBlock PROTO ((UINT1 u1BlockType));

VOID  VlanPbReleaseMemBlock PROTO ((UINT1 u1BlockType, UINT1* pu1Buf));

INT4 VlanPbCreateSVlanTables PROTO ((VOID));

INT4
VlanPbGetPepOperStatus (UINT2 u2Port, tVlanId SVlanId, 
                        UINT1 *pu1OperStatus);

VOID
VlanPbUpdateTagEtherType PROTO((tCRU_BUF_CHAIN_DESC * pFrame, 
                              UINT2 u2EtherType));
INT4
VlanPbPortSrcMacCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
    
INT4
VlanPbPortDstMacCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

INT4
VlanPbPortDscpCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

INT4
VlanPbPortSrcIpCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

INT4
VlanPbPortDstIpCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

INT4
VlanPbPortSrcDstIpCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

INT4
VlanPbPortCVlanCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

INT4
VlanPbPortCVlanSrcMacCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

INT4
VlanPbPortCVlanDstMacCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

INT4
VlanPbPortCVlanDscpCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

INT4
VlanPbPortCVlanDstIpCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

UINT4 VlanPbGetSVlanTableIndex PROTO ((INT4 i4TableType));

INT4
VlanPbGetFirstSrcMacSVlanEntry PROTO ((UINT4     u4PrioIndex,
                              INT4*     pi4FsPbPort,
                              tMacAddr FsPbSrcMacAddress));

INT4
VlanPbGetFirstDstMacSVlanEntry PROTO ((UINT4     u4PrioIndex,
                              INT4*     pi4FsPbPort,
                              tMacAddr  FsPbDstMacAddress));
INT4
VlanPbGetFirstDscpSVlanEntry PROTO ((UINT4     u4PrioIndex,
                            INT4*     pi4FsPbPort,
                            INT4*     pi4FsPbDscp));

INT4
VlanPbGetFirstSrcIpSVlanEntry PROTO ((UINT4    u4PrioIndex,
                            INT4*     pi4FsPbPort,
                            UINT4*    pu4FsPbSrcIpAddr));


INT4
VlanPbGetFirstDstIpSVlanEntry PROTO ((UINT4    u4PrioIndex,
                            INT4*     pi4FsPbPort,
                            UINT4*    pu4FsPbDstIpAddr));

INT4
VlanPbGetFirstSrcDstIpSVlanEntry PROTO ((UINT4    u4PrioIndex,
                                INT4*     pi4FsPbPort,
                                UINT4*    pu4FsPbSrcIpAddr,
                                UINT4*    pu4FsPbDstIpAddr));


INT4
VlanPbGetFirstCVlanSVlanEntry PROTO ((UINT4    u4PrioIndex,
                             INT4*    pi4FsPbPort,
                             UINT4*   pu4FsPbCVlan));


INT4
VlanPbGetFirstCVlanSrcMacSVlanEntry PROTO ((UINT4    u4PrioIndex,
                                   INT4*    pi4FsPbPort,
                                   UINT4*   pu4FsPbCVlan,
                                   tMacAddr FsPbSrcMac));
INT4
VlanPbGetFirstCVlanDstMacSVlanEntry PROTO ((UINT4    u4PrioIndex,
                                   INT4*    pi4FsPbPort,
                                   UINT4*   pu4FsPbCVlan,
                                   tMacAddr FsPbDstMac));

INT4
VlanPbGetFirstCVlanDscpSVlanEntry PROTO ((UINT4    u4PrioIndex,
                                 INT4*    pi4FsPbPort,
                                 UINT4*   pu4FsPbCVlan,
                                 UINT4*   pu4FsPbDscp));

INT4
VlanPbGetFirstCVlanDstIpSVlanEntry PROTO ((UINT4    u4PrioIndex,
                                 INT4*    pi4FsPbPort,
                                 UINT4*   pu4FsPbCVlan,
                                 UINT4*   pu4FsPbDstIp));

INT4
VlanPbGetNextSrcMacSVlanEntry PROTO ((UINT4    u4PrioIndex,
                             INT4     i4FsPbPort,
                             tMacAddr FsPbSrcMacAddress,
                             INT4*    pi4NextFsPbPort,
                             tMacAddr NextFsPbSrcMacAddress));


INT4
VlanPbGetNextDstMacSVlanEntry PROTO ((UINT4    u4PrioIndex,
                             INT4     i4FsPbPort,
                             tMacAddr FsPbDstMacAddress,
                             INT4*    pi4NextFsPbPort,
                             tMacAddr NextFsPbDstMacAddress));

INT4
VlanPbGetNextDscpSVlanEntry PROTO ((UINT4    u4PrioIndex,
                           INT4     i4FsPbPort,
                           INT4     i4FsPbDscp,
                           INT4*    pi4NextFsPbPort,
                           INT4*    pi4NextFsPbDscp));

INT4
VlanPbGetNextSrcIpSVlanEntry PROTO ((UINT4   u4PrioIndex,
                           INT4     i4FsPbPort,
                           UINT4    u4SrcIp,
                           INT4*    pi4NextFsPbPort,
                           UINT4*   pu4NextFsPbSrcIp));

INT4
VlanPbGetNextDstIpSVlanEntry PROTO ((UINT4   u4PrioIndex,
                           INT4     i4FsPbPort,
                           UINT4    u4DstIp,
                           INT4*    pi4NextFsPbPort,
                           UINT4*   pu4NextFsPbDstIp));

INT4
VlanPbGetNextSrcDstIpSVlanEntry PROTO ((UINT4    u4PrioIndex,
                               INT4     i4FsPbPort,
                               UINT4    u4SrcIp,
                               UINT4    u4DstIp,
                               INT4*    pi4NextFsPbPort,
                               UINT4*   pu4NextFsPbSrcIp,
                               UINT4*   pu4NextFsPbDstIp));

INT4
VlanPbGetNextCVlanSVlanEntry PROTO ((UINT4   u4PrioIndex,
                            INT4     i4FsPbPort,
                            UINT4    u4CVid,
                            INT4*    pi4NextFsPbPort,
                            UINT4*   pu4NextFsPbCVid));


INT4
VlanPbGetNextCVlanSrcMacSVlanEntry PROTO ((UINT4   u4PrioIndex,
                                  INT4     i4FsPbPort,
                                  UINT4    u4CVid,
                                  tMacAddr FsPbMacAddr,
                                  INT4*    pi4NextFsPbPort,
                                  UINT4*   pu4NextFsPbCVid,
                                  tMacAddr FsPbNextMacAddr));

INT4
VlanPbGetNextCVlanDstMacSVlanEntry PROTO ((UINT4   u4PrioIndex,
                                  INT4     i4FsPbPort,
                                  UINT4    u4CVid,
                                  tMacAddr FsPbMacAddr,
                                  INT4*    pi4NextFsPbPort,
                                  UINT4*   pu4NextFsPbCVid,
                                  tMacAddr FsPbNextMacAddr));


INT4
VlanPbGetNextCVlanDstIpSVlanEntry PROTO ((UINT4     u4PrioIndex,
                                  INT4     i4FsPbPort,
                                  UINT4    u4CVid,
                                  UINT4    u4DstIp,
                                  INT4*    pi4NextFsPbPort,
                                  UINT4*   pu4NextFsPbCVid,
                                  UINT4*   pu4FsPbNextIpAddr));


INT4
VlanPbGetNextCVlanDscpSVlanEntry PROTO ((UINT4     u4PrioIndex,
                                INT4      i4FsPbPort,
                                UINT4     u4CVid,
                                INT4      i4Dscp,
                                INT4*     pi4NextFsPbPort,
                                UINT4*    pu4NextFsPbCVid,
                                INT4*     pi4FsPbNextDscp));


tVlanSrcMacSVlanEntry* VlanPbGetSrcMacAddrSVlanEntry PROTO ((UINT4 u4PrioIndex,
                             UINT2 u2Port, tMacAddr SrcMacAddress));


tVlanDstMacSVlanEntry* VlanPbGetDstMacAddrSVlanEntry PROTO ((UINT4 u4PrioIndex,
                             UINT2 u2Port, tMacAddr DstMacAddress));

tVlanDscpSVlanEntry* VlanPbGetDscpSVlanEntry PROTO ((UINT4 u4PrioIndex,
                       UINT2 u2Port, UINT4 u4Dscp));


tVlanSrcIpSVlanEntry*
VlanPbGetSrcIpSVlanEntry PROTO ((UINT4 u4PrioIndex,
                       UINT2 u2Port,
                       UINT4 u4SrcIp));

tVlanDstIpSVlanEntry*
VlanPbGetDstIpSVlanEntry PROTO ((UINT4 u4PrioIndex,
                       UINT2 u2Port,
                       UINT4 u4DstIp));

tVlanSrcDstIpSVlanEntry*
VlanPbGetSrcDstIpSVlanEntry PROTO ((UINT4 u4PrioIndex,
                           UINT2 u2Port,
                           UINT4 u4SrcIp,
                           UINT4 u4DstIp));
tVlanCVlanSVlanEntry*
VlanPbGetCVlanSVlanEntry PROTO ((UINT4    u4PrioIndex,
                        UINT2    u2Port,
                        tVlanId  CVlanId));

tVlanCVlanSrcMacSVlanEntry*
VlanPbGetCVlanSrcMacSVlanEntry PROTO ((UINT4    u4PrioIndex,
                              UINT2    u2Port,
                              tVlanId  CVlanId,
                              tMacAddr SrcMac));

tVlanCVlanDstMacSVlanEntry*
VlanPbGetCVlanDstMacSVlanEntry PROTO ((UINT4    u4PrioIndex,
                              UINT2    u2Port,
                              tVlanId  CVlanId,
                              tMacAddr DstMac));
tVlanCVlanDscpSVlanEntry*
VlanPbGetCVlanDscpSVlanEntry PROTO ((UINT4    u4PrioIndex,
                            UINT2    u2Port,
                            tVlanId  CVlanId,
                            UINT4    u4Dscp));

tVlanCVlanDstIpSVlanEntry*
VlanPbGetCVlanDstIpSVlanEntry PROTO ((UINT4    u4PrioIndex,
                             UINT2    u2Port,
                             tVlanId  CVlanId,
                             UINT4    u4DstIp));

VOID
VlanPbUpdateSVlanClassifyParams PROTO ((tCRU_BUF_CHAIN_DESC *pFrame,
                               tVlanSVlanClassificationParams *pVlanSVlanIndex));

INT4
VlanPbGetServiceVlanId PROTO ((tVlanSVlanClassificationParams SVlanClassify,
                           tVlanId * pSVlanId));

INT4 VlanPbDelVIDTranslationEntry PROTO ((UINT2 , tVlanId ));

tEtherTypeSwapEntry * VlanPbAddEtherTypeSwapEntry PROTO ((UINT2 , UINT2 ));

INT4 VlanPbDelEtherTypeSwapEntry PROTO ((UINT2 , UINT2 ));

tEtherTypeSwapEntry * VlanPbGetEtherTypeSwapEntry PROTO ((UINT2 , UINT2 ));

INT4 VlanPbCmpVlanTranslationEntry  PROTO ((tRBElem *, tRBElem *));

INT4 VlanPbCmpEtherTypeSwapEntry PROTO ((tRBElem *, tRBElem *));

INT4
VlanPbConfEtherSwapEntry PROTO ((VOID));

INT4
VlanPbConfSVlanTranslationTable PROTO ((UINT2, UINT2));

VOID
VlanPbConfHwSVlanClassificationTable PROTO ((VOID));

VOID
VlanPbRemoveHwSVlanTranslationTable PROTO((UINT2, UINT2));

VOID
VlanPbRemoveHwSVlanEtherTypeSwapTable PROTO ((VOID));

INT4
VlanPbRemoveSVlanClassificationEntriesForPort PROTO((UINT2 u2Port));

INT4
VlanPbRemoveSVlanTranslationEntriesForPort PROTO ((UINT2 u2Port));

INT4
VlanPbRemoveEtherTypeSwapEntriesForPort PROTO ((UINT2 u2Port));

UINT1
VlanPbProcessFrame PROTO ((UINT2 u2Port,tCRU_BUF_CHAIN_DESC *pFrame,
                   tVlanIfMsg *pVlanMsg, tVlanId *pCustomerVlan, tVlanId *pServiceVlan,
                   UINT1 *pu1Action));


VOID
VlanPbSetDefPbPortInfo PROTO ((tVlanPortEntry * pPortEntry));

VOID
VlanPbHandleResetPbPortPropertiesToHw (UINT2 u2Port);

INT4
VlanPbHandleCopyPbPortPropertiesToHw PROTO ((UINT2 u2DstPort, 
                                             UINT2 u2SrcPort,
                                             UINT1 u1InterfaceType));


VOID  VlanPbConfHwFidProperties PROTO ((VOID));

VOID  VlanPbUpdateTagInFrame PROTO ((tCRU_BUF_CHAIN_DESC *pFrame,
                                     tVlanPortEntry* pOutPortEntry, 
                                     tVlanId SVlanId));

VOID
VlanPbTagFrame PROTO((tCRU_BUF_CHAIN_DESC * pFrame,
                      tVlanId VlanId, UINT1 u1Priority));

VOID
VlanPbRemoveHwSVlanClassificationTable PROTO ((VOID));


UINT1 VlanPbIsMulticastMacLimitExceeded PROTO ((VOID));

VOID VlanPbUpdateCurrentMulticastMacCount PROTO ((UINT1));

UINT1 VlanPbHandleNoSVlanIfCep PROTO ((tVlanPortEntry*)); 

INT4
VlanPbAllocateFdbControl PROTO ((VOID));

INT4
VlanPbReleaseFdbControl PROTO ((VOID));

VOID 
VlanPbHwRemovePortCVlanSVlans PROTO ((VOID));

VOID 
VlanPbHwRemovePortCVlanSrcMacSVlans PROTO ((VOID));

VOID 
VlanPbHwRemovePortCVlanDstMacSVlans PROTO ((VOID));

VOID 
VlanPbHwRemovePortCVlanDscpSVlans PROTO ((VOID));

VOID 
VlanPbHwRemovePortCVlanDstIpSVlans PROTO ((VOID));

VOID 
VlanPbHwRemovePortSrcMacSVlans PROTO ((VOID));

VOID 
VlanPbHwRemovePortDstMacSVlans PROTO ((VOID));

VOID 
VlanPbHwRemovePortDscpSVlans PROTO ((VOID));

VOID 
VlanPbHwRemovePortSrcIpSVlans PROTO ((VOID));

VOID 
VlanPbHwRemovePortDstIpSVlans PROTO ((VOID));

VOID 
VlanPbHwRemovePortSrcDstIpSVlans PROTO ((VOID));

VOID
VlanPbHwAddPortCVidSVlans PROTO ((VOID));

VOID
VlanPbHwAddPortSrcMacSVlans PROTO ((VOID));
    
VOID
VlanPbHwAddPortDstMacSVlans PROTO ((VOID));

VOID
VlanPbHwAddPortDscpSVlans PROTO ((VOID));

VOID
VlanPbHwAddPortSrcIpSVlans PROTO ((VOID));

VOID
VlanPbHwAddPortDstIpSVlans PROTO ((VOID));

VOID
VlanPbHwAddPortSrcDstIpSVlans PROTO ((VOID));

VOID
VlanPbHwAddPortCVlanSrcMacSVlans PROTO ((VOID));

VOID
VlanPbHwAddPortCVlanDstMacSVlans PROTO ((VOID));

VOID
VlanPbHwAddPortCVlanDscpSVlans PROTO ((VOID));

VOID
VlanPbHwAddPortCVlanDstIpSVlans PROTO ((VOID));

INT4
VlanPbHwRemoveCvidEntriesBasedOnPortAndSVlan (UINT2 u2Port, 
       tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry, UINT1 u1Index);

INT4
VlanPbRemoveLogicalPortEntriesForPort PROTO ((UINT2 u2Port));

INT4
VlanPbHwRemovePortCVlanSrcMacForPort PROTO ((UINT2 u2Port, UINT1 u2Index));

INT4
VlanPbHwRemovePortCVlanDstMacSVlanForPort PROTO ((UINT2 u2Port,
                                                  UINT1 u1Index));
                                          
INT4
VlanPbHwRemovePortCVlanDscpForPort PROTO ((UINT2 u2Port,
                                           UINT1 u1Index));
INT4
VlanPbHwRemovePortCVlanDstIpForPort PROTO ((UINT2 u2Port,
                                           UINT1 u1Index));

INT4
VlanPbHwRemovePortSrcMacSVlanForPort PROTO ((UINT2 u2Port,
                                             UINT1 u1Index));

INT4
VlanPbHwRemovePortDstMacSVlanForPort PROTO ((UINT2 u2Port,
                                             UINT1 u1Index));

INT4
VlanPbHwRemovePortSrcIpSVlanForPort PROTO ((UINT2 u2Port,
                                           UINT1 u1Index));

INT4
VlanPbHwRemovePortDstIpSVlanForPort PROTO ((UINT2 u2Port,
                                            UINT1 u1Index));

INT4
VlanPbHwRemovePortDscpSVlanForPort PROTO ((UINT2 u2Port,
                                           UINT1 u1Index));

INT4
VlanPbHwRemovePortSrcDstIpSVlanForPort PROTO ((UINT2 u2Port,
                                               UINT1 u1Index));

INT4 VlanPbHandleInFrame PROTO((UINT2 u2Port, tCRU_BUF_CHAIN_DESC *pFrame,
                          tVlanTag *pVlanTag));

UINT1 VlanPbGetPortMacLearningStatus PROTO ((tVlanPbPortEntry *));
UINT1 VlanPbIsPortMacLearnLimitExceeded PROTO ((tVlanPbPortEntry *));

VOID VlanPbNotifySVlanStatusForPep PROTO((tVlanId SVlanId, UINT1 u1RowStatus));
INT4 VlanPbCmpLogicalPortEntry PROTO((tRBElem * pNodeA, tRBElem * pNodeB));
INT4 VlanConfHwSVlanTranslationTable PROTO((VOID));
INT4 VlanPbIsSVlanActive PROTO((tVlanId SVlanId));

VOID VlanPbGetPcpDecodeVal PROTO((UINT2 u2Port, UINT1 u1InPcp, UINT1 *pu1Priority,
                           UINT1 *pu1DropEligible));

INT4 VlanPbCreateLogicalPortEntry PROTO((UINT2 u2Port, tVlanId CVlanId, 
                                         tVlanId SVlanId));
INT4 VlanPbDelLogicalPortEntry PROTO((UINT2 u2Port, tVlanId SVlanId));
INT4 VlanPbConfCustomerEdgePort PROTO((UINT2 u2Port));

INT4 VlanPbConfCustomerNetworkPort PROTO((UINT2 u2Port, UINT1 u1ServiceType));
INT4 VlanPbConfPropCustomerEdgePort PROTO((UINT2 u2Port));
INT4 VlanPbConfPropCustomerNetworkPort PROTO((UINT2 u2Port));
INT4 VlanPbConfProviderNetworkPort PROTO((UINT2 u2Port));
INT4 VlanPbConfPropProviderNetworkPort PROTO((UINT2 u2Port));
INT4 VlanPbConfVirtualInstancePort PROTO((UINT2 u2Port));
INT4 VlanPbConfCustomerBackbonePort PROTO((UINT2 u2Port));
tVlanPbLogicalPortEntry * VlanPbGetLogicalPortEntry PROTO((UINT2 u2Port, 
                                                           tVlanId SVlanId));
tVlanPbLogicalPortEntry *
VlanPbGetFirstLogicalPortEntry PROTO((INT4 *pi4FsPbPort, 
                                           UINT4 *pu4FsPbSVlan));
tVlanPbLogicalPortEntry *
VlanPbGetNextLogicalPortEntry PROTO((INT4 i4FsPbPort, tVlanId SVlanId,
                                    INT4 *pi4NextFsPbPort, UINT4 *pu4NextSVlanId));
UINT1 VlanPbIsCvidEntryActive PROTO((UINT2 u2Port, tVlanId SVlanId));
INT4 VlanPbGetUntaggedCepStatus PROTO ((UINT2 u2Port, tVlanId SVlanId));
VOID VlanPbUpdatePepOperStatus PROTO((UINT2 u2Port, tVlanId SVlanId, UINT1 u1OperUpdate, tVlanPbLogicalPortEntry * pVlanPbLogicalPortEntry));
VOID VlanPbNotifyCVidStatusForPep PROTO((UINT2 u2Port, tVlanId SVid,
                                   UINT1 u1RowStatus));
INT4 VlanPbIngressEtherTypeSwap PROTO((tCRU_BUF_CHAIN_DESC *pFrame, UINT2 u2Port,
                                 UINT2 *pu2TagProtoId)); 
VOID VlanPbClassifyIncomingFrame PROTO((tCRU_BUF_CHAIN_DESC *pFrame, UINT2 u2TagProtoId, 
                             UINT1 *pu1PktType, tVlanId *pCVlanTag, 
                             tVlanId *pSVlanTag));
VOID
VlanPbClassifySVlan (tCRU_BUF_CHAIN_DESC * pFrame,UINT2 u2Port,
                     UINT1 u1PktType, tVlanId CVlanId, tVlanId * pSVlanId);

VOID VlanPbInitPcpValues (UINT2 u2Port);

INT4
VlanPbPepIngressFiltering (tVlanPbLogicalPortEntry *pVlanPbLogicalPortEntry, 
                           tVlanId CvidRegSvid, UINT1 u1IsFrameTagged);
INT4
VlanPbServicePriorityRegen (UINT2 u2Port, tVlanId SVlanId, UINT1 u1InPriority,
                            UINT1 *pu1RegenPriority);  
INT4 
VlanPbHandlePktRxOnCep (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                        tVlanTag *pVlanTag);
INT4
VlanPbHandlePktTxOnCep (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                        tVlanId SVlanId, tVlanIfMsg * pVlanIf, 
                        UINT1 u1IsCustBpdu);

INT4  VlanPbHandlePktOnPep (tCRU_BUF_CHAIN_DESC *pFrame, tVlanIfMsg *pVlanIf);

INT4
VlanPbPepHandleInCustBpdu (tCRU_BUF_CHAIN_DESC *pFrame, tVlanId SVlanId);

INT4
VlanPbNpUpdateCvidEntriesInHw (UINT2 u2Port, tVlanId SVlanId, UINT2 u2DstPort, UINT1 u1Flag);

INT4
VlanPbAddCvidEntriesInHw (UINT2 u2Port, tVlanId SVlanId);
INT4
VlanPbDelCvidEntriesFromHw (UINT2 u2Port, tVlanId SVlanId);
INT4
VlanPbNpAddPepEntriesInHw (UINT2 u2Port, tVlanId SVlanId);
INT4
VlanPbNpDelPepEntriesFromHw (UINT2 u2Port, tVlanId SVlanId);


VOID VlanPbRemoveHwLogicalPortTable PROTO((VOID));
VOID VlanPbConfHwCvidAndPepTables PROTO((VOID));
INT4 VlanPbValidatePortType (UINT2 u2LocalPort, UINT1 u1PbPortType);
INT1 VlanPbSetDot1adPortType (tVlanPortEntry *pVlanPortEntry, 
                              UINT1 u1PbPortType);
INT4 VlanPbValidatePcpDecodingValues (INT4 i4Port, INT4 i4PcpSelRow,
                                      INT4 i4PcpValue, UINT1 u1InPriority,
                                      UINT1 u1InDropEligible);
INT4 VlanPbValidateBridgeMode (INT4 i4BridgeMode, UINT4 *pu4ErrorCode);

INT4 VlanPbIsTunnelingValidOnCep PROTO ((UINT2 u2Port));
INT4 VlanPbIsAnyProtTunnelSet PROTO ((UINT2 u2Port));
INT4 VlanMiPbIsCreateIvrValid (UINT4 u4ContextId, UINT2 u2IfIvrVlanId);

VOID VlanPbGetDecodingPriority PROTO ((INT4 i4SelRow, INT4 i4PcpVal, INT4 *pi4Priority));
VOID VlanPbGetDecodingDropEligible PROTO ((INT4 i4SelRow, INT4 i4PcpVal, INT4 *pi4DropEligible));
VOID VlanPbGetEncodingPcpVal PROTO((INT4 i4SelRow, INT4 Priority, INT4 i4DropEligible, INT4 *pi4PcpVal));


VOID VlanPbUpdatePepOperStatusToAst PROTO((VOID));
INT4 
VlanPbEtherTypeSwap (tCRU_BUF_CHAIN_DESC *pFrame, UINT2 u2Port, UINT2 u2EtherType);
INT4 VlanMiPbIsTunnelingValid (UINT4 u4IfIndex);
INT4 VlanMiPbGetPepOperStatus (UINT4 u4IfIndex, UINT4 u4SVlanId, 
                               UINT1 *pu1OperStatus);

INT4
VlanPbCheckVlanServiceType (tVlanId VlanId, tLocalPortList InPorts); 

INT4
VlanPbRemovePortCVidEntries PROTO((UINT2 u2Port));

INT4
VlanPbIsMacLearningAllowed PROTO((tVlanFdbEntry * pFdbEntry,
                            tVlanPortEntry * pVlanPortEntry));

VOID
VlanPbUpdateLearntMacCount PROTO((tVlanPortEntry *pVlanPortEntry,
                            tVlanFdbEntry *pFdbEntry, 
                            UINT1 u1Flag));

INT4 VlanPbIsServiceTypeValid (tVlanId VlanId, UINT1 u1InServiceType);

INT4 VlanPbGetBridgeMode (INT4 i4Context, INT4 *pi4BridgeMode);
    
VOID
VlanGetCVlansFromCVlanSVlanTable PROTO ((UINT4 u4TblIndex, 
                                         UINT2 u2Port, tVlanId SVlanId, 
                                         tCVlanInfo *pCVlanInfo));

VOID
VlanGetCVlansFromCVlanSrcMacSVlanTable PROTO ((UINT4 u4TblIndex, 
                                               UINT2 u2Port, tVlanId SVlanId, 
                                               tVlanList CVlanList));

VOID
VlanGetCVlansFromCVlanDstMacSVlanTable PROTO ((UINT4 u4TblIndex, 
                                               UINT2 u2Port, tVlanId SVlanId, 
                                               tVlanList CVlanList));

VOID
VlanGetCVlansFromCVlanDscpSVlanTable PROTO ((UINT4 u4TblIndex, 
                                             UINT2 u2Port, tVlanId SVlanId, 
                                             tVlanList CVlanList));

VOID
VlanGetCVlansFromCVlanDstIpSVlanTable PROTO ((UINT4 u4TblIndex, 
                                              UINT2 u2Port, tVlanId SVlanId, 
                                              tVlanList CVlanList));

INT4
VlanHandlePbRemovePortTablesFromHw PROTO((tVlanQMsg *, UINT2 , UINT2 ));

BOOL1 
VlanPbCheckEtherTypeSwap (UINT2 u2Port, UINT2 u2EtherType);

INT4
VlanGetPrimaryContextEtherType(UINT4 u4IfIndex, UINT2 *pu2EtherValue, 
          UINT1 u1EtherType);

INT4 
VlanSendMsrNotification (UINT2 u2Port);

INT1 VlanPbSetSVlanConfigServiceType (UINT4 u4Dot1qVlanIndex,
                                   INT4 i4SetValFsPbSVlanConfigServiceType);
/* vlnpbsisp.c */
INT4
VlanSispCopyIngressEtherTypeToSispPorts PROTO ((UINT4 u4PhyIfIndex,
            UINT2 u2IngressEtherType));
INT4
VlanSispCopyEgressEtherTypeToSispPorts PROTO ((UINT4 u4PhyIfIndex,
                                              UINT2 u2EgressEtherType));

#endif

