#ifndef _STDRSLEX_H
#define _STDRSLEX_H

extern INT1 nmhGetDot1dStpVersion ARG_LIST((INT4 *));

extern INT1 nmhGetDot1dStpTxHoldCount ARG_LIST((INT4 *));

extern INT1 nmhSetDot1dStpVersion ARG_LIST((INT4 ));

extern INT1 nmhSetDot1dStpTxHoldCount ARG_LIST((INT4 ));

extern INT1 nmhTestv2Dot1dStpVersion ARG_LIST((UINT4 *  ,INT4 ));

extern INT1 nmhTestv2Dot1dStpTxHoldCount ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2Dot1dStpVersion ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2Dot1dStpTxHoldCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1dStpExtPortTable. */
extern INT1 nmhValidateIndexInstanceDot1dStpExtPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dStpExtPortTable  */

extern INT1 nmhGetFirstIndexDot1dStpExtPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1 nmhGetNextIndexDot1dStpExtPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1 nmhGetDot1dStpPortProtocolMigration ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDot1dStpPortAdminEdgePort ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDot1dStpPortOperEdgePort ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDot1dStpPortAdminPointToPoint ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDot1dStpPortOperPointToPoint ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDot1dStpPortAdminPathCost ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetDot1dStpPortProtocolMigration ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetDot1dStpPortAdminEdgePort ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetDot1dStpPortAdminPointToPoint ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetDot1dStpPortAdminPathCost ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2Dot1dStpPortProtocolMigration ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Dot1dStpPortAdminEdgePort ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Dot1dStpPortAdminPointToPoint ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Dot1dStpPortAdminPathCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2Dot1dStpExtPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif
