/* $Id: vlanred.h,v 1.34 2016/07/16 11:15:03 siva Exp $     */
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2005-2006                         */
/*****************************************************************************/
/*    FILE  NAME            : vlanred.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VLAN Redundancy                                */
/*    MODULE NAME           : VLAN/GARP                                      */
/*                                                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 02 June 2005                                   */
/*    AUTHOR                : L2RED TEAM                                     */
/*    DESCRIPTION           : This file contains all constants and typedefs  */
/*                            for VLAN redundancy module.                    */
/*---------------------------------------------------------------------------*/
#ifndef _VLANRED_H
#define _VLANRED_H

#define VLAN_RELEARN_COMPLETE        1
#define VLAN_RELEARN_NOT_COMPLETE    2

#define VLAN_RED_MAX_VLAN_DEL_BKTS   100
#define VLAN_RED_MAX_GRP_DEL_BKTS    100

#define VLAN_RED_GARP_RELEARN_INT     5 /* in secs */
#define VLAN_RED_PERIODIC_UPD_INT    10 /* in secs */

#define VLAN_RED_TYPE_FIELD_SIZE     1
#define VLAN_RED_LEN_FIELD_SIZE      2
#define VLAN_RED_CONTEXT_FIELD_SIZE  4
#define VLAN_RED_VLAN_ID_LEN         2
#define VLAN_RED_FDB_ID_LEN          4
#define VLAN_RED_PORT_NO_LEN         4
#define VLAN_RED_PORT_OPER_STATUS_SIZE     1
#define VLAN_RED_MAC_ADDR_LEN     6

#define VLAN_RED_VLAN_TABLE_MASK   0x000001
#define VLAN_RED_MCAST_TABLE_MASK  0x000002
#define VLAN_RED_UCAST_TABLE_MASK  0x000004
#define VLAN_RED_PROTO_TABLE_MASK  0x000008

#ifdef NPAPI_WANTED
#define VLAN_RED_BRG_MODE_CHG_FLAG_SIZE 1
#endif

    /*                                                                                        *    VLAN Dynamic Update message                                                         *
     *    <- 1 byte ->|<- 2 B ->|<- 4B   ->|<- 2B->|<- 1B ->|<-sizeof(tHwVlanPortList)->|
     *    ------------------------------------------------------------------------------
     *    | Msg. Type | Length  | contextId|VLAN Id|Syncstate|    tHwVlanPortList       |
     *    |           |         |          |       |         |                          |
     *    ------------------------------------------------------------------------------
     */

#define VLAN_RED_CACHE_INFO_SIZE        1 + 2 + 1 + 2 + 4 + sizeof (tHwVlanPortList)
    /*
     *    VLAN Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B ->|<- 4B   ->|<- 4B->|<-  4B   ->|<-szof(tLocalPortList)->|     *
     *    ------------------------------------------------------------------------------
     *    | Msg. Type | Length  | contextId|FDB Id |<-RcvPort->|      AllowedPorts     |
     *    |           |         |          |       |           |                       |
     *    ------------------------------------------------------------------------------
     *                                                                                        *    ->|<-sizeof(MacAddress)->|<-sizeof(MacAddress)->|<- 1B ->|<-  1B  ->|
     *    --------------------------------------------------------------------                *    | MacAddress             | ConnectionId         |u1Status|SyncState |
     *    |                        |                      |        |          |               *    ---------------------------------------------------------------------
     */

#define VLAN_RED_ST_UCAST_CACHE_INFO_SIZE     1 + 2 + 4 + 4 + 4 + sizeof (tLocalPortList) + sizeof (tMacAddr) + sizeof (tMacAddress) + 1 + 1 + 1


    /*
     *    VLAN Dynamic Update message                                                         *
     *    <- 1 byte ->|<- 2 B ->|<- 4B   ->|<- 2B->|<-  4B   ->|<-szof(tLocalPortList)->|     *                                                                                        *    ------------------------------------------------------------------------------      *    | Msg. Type | Length  | contextId|Vlan Id |<-RcvPort->|      AllowedPorts     |
     *    |           |         |          |       |           |                       |      *    ------------------------------------------------------------------------------
     *
     *    ->|<-sizeof(MacAddress)->|<-  1B  ->|
     *    -------------------------------------
     *    | MacAddress             |SyncState |
     *    |                        |          |
     *    -------------------------------------
     */

#define VLAN_RED_MCAST_CACHE_INFO_SIZE     1 + 2 + 2 + 4 + 4 + sizeof (tLocalPortList) + sizeof (tMacAddr) + 1
    /*
     *    Protocol VLAN Dynamic Update message
     *
     * <- 1 byte ->|<- 2 B ->|<- 4B   ->|<- 4B->|<-  2B   ->|<-(tVlanProtoTemplate)->|  *
     *    ------------------------------------------------------------------------------
     *    | Msg. Type | Length  | contextId|Grp Id |<-  Port ->|      VlanProtTemplate  |
     *    |           |         |          |       |           |                        |
     *    ------------------------------------------------------------------------------
     *                                                                                        *    ->|<-tVlanId->|<- 1B ->|<-  1B  ->|
     *    -----------------------------------                                                 *      | VlanId    |SyncState|Action   |
     *      |           |         |         |
     *    -----------------------------------
     */

#define VLAN_RED_PROTO_VLAN_CACHE_INFO_SIZE     1 + 2 + 4 + 4 + 2 + sizeof (tVlanProtoTemplate) + 2 + 1 + 1
#define VLAN_RED_MAX_MSG_SIZE              1500
#define VLAN_RED_DEL_ALL_UPD_SIZE          7
#define VLAN_RED_BULK_REQ_MSG_SIZE         3
#define VLAN_RED_BULK_UPD_TAIL_MSG_SIZE    3
       
#define VLAN_RED_ENTRY_PRESENT_IN_BOTH     1
#define VLAN_RED_ENTRY_PRESENT_IN_HW_ONLY  2
#define VLAN_RED_ENTRY_PRESENT_IN_SW_ONLY  3
#define VLAN_RED_ENTRY_NOT_PRESENT_IN_BOTH 4 

/* gVlanRedInfo.u1AuditFlag takes any of the four values.
 *
 * VLAN_RED_AUDIT_STOP  -- Mean the audit event should not happen or already
 *                         done.
 * VLAN_RED_AUDIT_START -- Means, audit event is started and its currently
 *                         going on.  */
#define VLAN_RED_AUDIT_STOP              0
#define VLAN_RED_AUDIT_START             1

#define VLAN_AUDIT_TASK_PRIORITY     240 /* should have lower priority */
#define VLAN_AUDIT_TASK            ((UINT1*)"VLAU")

#define VLAN_RED_SEM_NAME          ((const UINT1 *)"VLRD")
#define VLAN_RED_LOCK()            VlanRedLock ()
#define VLAN_RED_UNLOCK()          VlanRedUnLock ()

#define VLAN_RED_AUDIT_START_EVENT    0x00000001
#define VLAN_RED_PERFORM_MCAST_AUDIT  0x00000002
#define VLAN_RED_AUDIT_TASK_DEL_EVENT 0x00000004

#define VLAN_RED_NO_OF_VLANS_PER_SUB_UPDATE         10
#define VLAN_RED_NO_OF_PORTS_PER_SUB_UPDATE         10
#define VLAN_RED_BULK_UPD_REMOVE_VLAN               2

/* Macros */
#define VLAN_RED_NUM_STANDBY_NODES() (gVlanRedInfo.u1NumStandbyNodes)

#define VLAN_RED_AUDIT_FLAG() (gVlanRedInfo.u1AuditFlag)

#define VLAN_RED_BULK_REQ_RECD() (gVlanRedInfo.bBulkReqRcvd)

#define VLAN_RED_IS_UPD_TMR_RUNNING() \
   ((gVlanRedInfo.PeriodicTimer.u1IsTmrActive == VLAN_TRUE)? VLAN_TRUE:VLAN_FALSE)

#define VLAN_RED_IS_RELEARN_TMR_RUNNING() \
   ((gVlanRedInfo.RelearnTimer.u1IsTmrActive == VLAN_TRUE)? VLAN_TRUE:VLAN_FALSE)

#define VLAN_RED_VLANS_CHANGED() (gVlanRedInfo.u4NumVlansChanged)
#define VLAN_RED_GRPS_CHANGED()  (gVlanRedInfo.u4NumGrpsChanged)
#define VLAN_RED_VLAN_DELETES()  (gVlanRedInfo.u4NumVlanDelUpdates)
#define VLAN_RED_GRP_DELETES()   (gVlanRedInfo.u4NumGrpDelUpdates)

#define VLAN_RED_INCR_VLANS_CHANGED() (gVlanRedInfo.u4NumVlansChanged++)
#define VLAN_RED_INCR_GRPS_CHANGED()  (gVlanRedInfo.u4NumGrpsChanged++)
#define VLAN_RED_INCR_VLAN_DELETES()  (gVlanRedInfo.u4NumVlanDelUpdates++)
#define VLAN_RED_INCR_GRP_DELETES()  (gVlanRedInfo.u4NumGrpDelUpdates++)

#define VLAN_RED_DECR_VLANS_CHANGED() (gVlanRedInfo.u4NumVlansChanged--)
#define VLAN_RED_DECR_GRPS_CHANGED()  (gVlanRedInfo.u4NumGrpsChanged--)
#define VLAN_RED_DECR_VLAN_DELETES()  (gVlanRedInfo.u4NumVlanDelUpdates--)
#define VLAN_RED_DECR_GRP_DELETES()  (gVlanRedInfo.u4NumGrpDelUpdates--)
    
#define VLAN_RED_GET_STATIC_CONFIG_STATUS() VlanRmGetStaticConfigStatus()
    
#define VLAN_RED_HW_AUD_BRG_MODE_CONTEXT() \
    (gVlanRedInfo.HwAudBrgModeChgEntry.u4ContextId)
#define VLAN_RED_HW_AUD_BRG_MODE_FLAG() \
    (gVlanRedInfo.HwAudBrgModeChgEntry.u1BrgModeChgFlag)

/* Macros to write in to RM buffer. */
#define VLAN_RM_PUT_1_BYTE(pMsg, pu2Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu2Offset), u1MesgType); \
        *(pu2Offset) = (UINT2)(*(pu2Offset)+1);\
}while (0)

#define VLAN_RM_PUT_2_BYTE(pMsg, pu2Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu2Offset), u2MesgType); \
        *(pu2Offset) = (UINT2)(*(pu2Offset)+2);\
}while (0)

#define VLAN_RM_PUT_4_BYTE(pMsg, pu2Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu2Offset), u4MesgType); \
        *(pu2Offset) = (UINT2)(*(pu2Offset)+ 4);\
}while (0)

#define VLAN_RM_PUT_N_BYTE(pdest, psrc, pu2Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET(pdest, psrc, *(pu2Offset), u4Size); \
        *(pu2Offset) = (UINT2) (*(pu2Offset)+ (UINT2)u4Size); \
}while (0)

#define VLAN_RM_GET_1_BYTE(pMsg, pu2Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu2Offset), u1MesgType); \
        *(pu2Offset) = (UINT2)(*(pu2Offset)+1);\
}while (0)

#define VLAN_RM_GET_2_BYTE(pMsg, pu2Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu2Offset), u2MesgType); \
        *(pu2Offset) = (UINT2)(*(pu2Offset)+2);\
}while (0)

#define VLAN_RM_GET_4_BYTE(pMsg, pu2Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu2Offset),u4MesgType); \
        *(pu2Offset) = (UINT2)(*(pu2Offset)+ 4);\
}while (0)

#define VLAN_RM_GET_N_BYTE(psrc, pdest, pu2Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu2Offset), u4Size); \
        *(pu2Offset) = (UINT2) (*(pu2Offset)+(UINT2)u4Size); \
}while (0)

#define VLAN_RED_SYNC_FAILED              -1
#define VLAN_RED_SYNC_HW_NOT_UPDATED      0
#define VLAN_RED_SYNC_HW_UPDATED          1
/* Represents the message types encoded in the update messages */
typedef enum {
    VLAN_BULK_REQ_MESSAGE = RM_BULK_UPDT_REQ_MSG,
    VLAN_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG,
    VLAN_UPDATE_MESSAGE,
    VLAN_GRP_UPDATE_MESSAGE,
    VLAN_DEL_UPDATE_MESSAGE,
    VLAN_GRP_DEL_UPDATE_MESSAGE,
    VLAN_DEL_ALL_INFO_MESSAGE,
    VLAN_GRP_DEL_ALL_INFO_MESSAGE,
    VLAN_PORT_OPER_STATUS_MESSAGE,
    VLAN_MCAST_DEL_ONTIMEOUT,
    VLAN_UCAST_DEL_ONTIMEOUT,
    VLAN_HWAUD_BRG_MODE_CHG,
    VLAN_RED_SYNC_ST_UCAST_CACHE_INFO,
    VLAN_RED_SYNC_MCAST_CACHE_INFO,
    VLAN_RED_SYNC_PROTO_CACHE_INFO,
    VLAN_RED_SYNC_CACHE_INFO,
    VLAN_EVB_RED_CDCP_TLV_CHANGE,
    VLAN_EVB_RED_SBP_ADD_SYNC_UP,
    VLAN_EVB_RED_SBP_DEL_SYNC_UP,
    VLAN_EVB_RED_CDCP_REMOTE_ROLE
}tVlanRmMessageType;

typedef struct _VlanRedProtoVlanCacheNode {
    tRBNodeEmbd  RbNode;  /* RbNode for the cache entry */
    tVlanProtoTemplate VlanProtoTemplate; 
    UINT4 u4ContextId; 
    UINT4 u4GroupId;
    UINT2 u2Port;
    tVlanId VlanId;
    INT1    i1Action;       /* VLAN_ADD /VLAN_DELETE */
    INT1    i1RedSyncState; /* Present in NP/Not. */
    UINT1   au1Rsvd[2];
}tVlanRedProtoVlanCacheNode;


typedef struct _VlanRedMcastCacheNode {
    tRBNodeEmbd  RbNode;  /* RbNode for the cache entry */
    tLocalPortList HwPortList;
    tMacAddr      MacAddr;
    tVlanId      VlanId;  /* FDB Identifier */
    UINT4        u4ContextId; /* Context Identifier */
    UINT4        u4RcvPort;
    INT1        i1RedSyncState; /* Hardware status - Present in NP or not */
    UINT1        au1Pad[3];
}tVlanRedMcastCacheNode;


typedef struct _VlanRedStUcastCacheNode {
    tRBNodeEmbd  RbNode;  /* RbNode for the cache entry */
    tLocalPortList AllowedToGoToPorts;
    tMacAddr  MacAddr;
    tMacAddr  ConnectionId;
    UINT4        u4ContextId; /* Context Identifier */
    UINT4        u4RcvPort;
    
    UINT4       u4FdbId;  /* FDB Identifier */
      
    INT1        i1RedSyncState; /* Hardware status - Present in NP or not */
    INT1        i1Action; /* Operation - ADD/DELETE*/
    UINT1       u1Status;
  
    UINT1        au1Pad[1];
}tVlanRedStUcastCacheNode;



typedef struct _VlanRedCacheNode {
    tRBNodeEmbd  RbNode;  /* RbNode for the cache entry */
    tHwVlanPortList  HwPortList; 
    UINT4        u4ContextId; /* Context Identifier */
    tVlanId      VlanId;  /* VLAN Identifier */
    INT1        i1RedSyncState; /* Hardware status - Present in NP or not */
    UINT1        au1Pad[1];
}tVlanRedCacheNode;

/* The following structure defines an entry in the GVRP learnt ports array */
typedef struct _tVlanRedGvrpLearntEntry {
    tVLAN_SLL_NODE  NextLearntEntry; /* next entry in the linked list */
    tVlanId         VlanId;          /* VLAN ID */
    UINT1           au1Reserved[2];
    tLocalPortList       LearntPortList;  /* GVRP learnt port list for a VLAN */
}tVlanRedGvrpLearntEntry;
    
/* GVRP Learnt ports array */
typedef struct _tVlanRedVlanArray {
    tVlanRedGvrpLearntEntry    *pLearntEntry;     /* Pointer to the entry 
                                                     containing the learnt port 
                                                     list */
}tVlanRedVlanArray;    

/* GMRP Learnt Port entry */
typedef struct _tVlanRedGmrpLearntEntry {
    tRBNodeEmbd       RBNode;
    tVlanId           VlanId;
    tMacAddr          McastAddr;
    tLocalPortList         LearntPortList; /* GMRP learnt port list for a GROUP */
}tVlanRedGmrpLearntEntry;

/* GVRP Deleted Entry */
typedef struct _tVlanRedGvrpDeletedEntry {
    tTMO_HASH_NODE     NextDeletedEntry;
    tVlanId            VlanId;
    UINT1              au1Reserved[2];
}tVlanRedGvrpDeletedEntry;

/* GMRP Deleted Entry */
typedef struct _tVlanRedGmrpDeletedEntry {
    tTMO_HASH_NODE     NextDeletedEntry;
    tVlanId            VlanId;
    tMacAddr           McastAddr;
}tVlanRedGmrpDeletedEntry;

#ifdef NPAPI_WANTED
/* Indication of Bridge Mode Change in Active can be stored here 
 * and when the same is applied on Standby, it can be reset and if 
 * during switchover, if the Flag is set, audit in Vlan can be done 
 * for this context alone
 */
typedef struct _tVlanHwAudEntry {
    UINT4           u4ContextId;
    UINT1           u1BrgModeChgFlag;
    UINT1           au1Reserved[3];
}tVlanHwAudEntry;
#endif

typedef struct _tVlanRedGlobalInfo {
   tVLAN_HASH_TABLE    *pVlanDeletedTable;
   tVLAN_HASH_TABLE    *pGrpDeletedTable;
   tVlanRedVlanArray   *pGvrpLrntTable;
   tVLAN_SLL            GvrpLrntList;   /* Node of type tVlanRedGvrpLearntEntry */
   tRBTree              GmrpLrntTable;
   tRBTree              VlanRedCacheTable;
   tRBTree              VlanRedStUcastCacheTable;
   tRBTree              VlanRedMcastCacheTable;
   tRBTree              VlanRedProtoVlanCacheTable; /* RBTree for pending Protocol VLAN 
                                                     * entries to be used for optimized
                                                     * audit. 
                                                     */
   tVlanTimer           RelearnTimer;
   tVlanTimer           PeriodicTimer;
#ifdef NPAPI_WANTED
   tVlanHwAudEntry      HwAudBrgModeChgEntry;
#endif
   UINT4                u4NumVlanDelUpdates; /* # of Deleted Vlans which needs
                                              * to be synchronised with the
                                              * Standby node. This is nothing
                                              * but the number of nodes present
                                              * in pVlanDeletedTable.
                                              */
   UINT4                u4NumGrpDelUpdates;  /* # of Deleted Group entries which 
                                              * needs to be synchronised with the
                                              * Standby node. This is nothing
                                              * but the number of nodes present
                                              * in pGrpDeletedTable.
                                              */
   UINT4                u4NumVlansChanged;
   UINT4                u4NumGrpsChanged;
   UINT4                BulkUpdNextPort;     /* Points to the Next Port No for
                                                which next bulk update has to
                                                be generated.
                                             */
   UINT4                u4BulkUpdNextContext;
   tVlanId              BulkUpdNextVlanId;   /* Points to the Next Vlan Id for
                                                which next bulk update has to
                                                be generated.
                                             */
   BOOL1                bBulkReqRcvd;        /* To check whether bulk request
                                                msg recieved from standby
                                                before RM_STANDBY_UP event.
                                             */  
   UINT1                u1NumStandbyNodes;
   UINT1                u1AuditFlag;
   UINT1                u1OptimizedAudit;   /* Flag to identify whether FULL Audit or 
                                             * optimized HW Audit is needed for VLAN, 
                                             * static Unicast MAC, static/dynamic MCAST MAC
                                             * When set to VLAN_TRUE, optimized Audit will 
                                             * be performed. This will be set to VLAN_TRUE
                                             * if OPTIMIZED_HW_AUDIT_WANTED is defined.
                                             * When set to VLAN_FALSE, full Audit will be
                                             * performed
                                             */
   UINT1                u1ForceFullAudit;   /* In case the memory allocation failure happens for 
                                             * the cache nodes used in OPTIMIZED AUDIT, then 
                                             * Optimized HW Audit will be switched to FULL Audit
                                             * VLAN_RED_VLAN_TABLE_MASK - when this bit mask is
                                             * set then FULL VLAN Audit will be done. 
                                             * VLAN_RED_MCAST_TABLE_MASK - when this bit mask is set
                                             * then FULL MCAST MAC Audit will be done
                                             * VLAN_RED_UCAST_TABLE_MASK - when this bit mask is set
                                             * then FULL UCAST MAC Audit will be done
                                             */
   UINT1                au1Reserved[1];
}tVlanRedGlobalInfo;

typedef struct _tVlanRedTaskInfo {
    tMemPoolId       VlanLrntPoolId;
    tMemPoolId       GrpLrntPoolId;
    tMemPoolId       VlanDelPoolId;
    tMemPoolId       GrpDelPoolId;
    tOsixTaskId      VlanAuditTaskId;
    tOsixSemId       RedSemId;
}tVlanRedTaskInfo;

/* Function prototypes */
VOID VlanRedProcessUpdateMsg (tVlanQMsg *pVlanQMsg);
INT4 VlanRedInitActiveInfo (VOID);
VOID VlanRedDeInitActiveInfo (VOID);
INT4 VlanRedInitStandbyInfo (VOID);
VOID VlanRedDeInitStandbyInfo (VOID);
INT4 VlanRedMakeNodeActiveFromIdle (VOID);
INT4 VlanRedMakeNodeStandbyFromIdle (VOID);
INT4 VlanRedMakeNodeStandbyFromActive (VOID);
INT4 VlanRedMakeNodeActiveFromStandby (VOID);
INT4 VlanRedProcessDataAppliedMsg (VOID);
VOID VlanRedHandleRelearnTmrExpiry (VOID);
INT4 VlanRedUpdateNpapiDefaultVlanId (UINT4 u4Context);
INT4 VlanRedGmrpTableCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
VOID VlanRedDelAllActiveInfo (VOID);
VOID VlanRedDelAllStandbyInfo (VOID);
VOID VlanRedDelAllGvrpLrntEntries (VOID);
VOID VlanRedDelAllGmrpLrntEntries (VOID);
VOID VlanRedSendBulkUpdates (VOID);
VOID VlanRedHandleStandbyUpEvent (VOID);
VOID VlanRedHandlePeriodicTmrExpiry (VOID);
VOID VlanRedSendPeriodicUpdates (VOID);
VOID VlanRedHandleUpdates (tRmMsg *pMsg, UINT2 u2DataLen);
VOID VlanRedHandleVlanUpdate (tVlanId VlanId, tLocalPortList PortList); 
VOID VlanRedHandleGroupUpdate (tVlanId VlanId, tMacAddr McastAddr, 
                               tLocalPortList PortList); 
VOID VlanRedHandleVlanDelUpdate (tVlanId VlanId);
VOID VlanRedHandleGrpDelUpdate (tVlanId VlanId, tMacAddr McastAddr);
VOID VlanRedHandleDelAllVlanInfo (UINT4 u4Port);
VOID VlanRedHandleDelAllGrpInfo (UINT4 u4Port);
INT4 VlanRedHandleUcastAddrDelOnTimeOut (UINT4 u4FidIndex, tMacAddr UcastAddr,
                                         UINT4 RcvPortId);
INT4 VlanRedHandleMcastAddrDelOnTimeOut (UINT2 u2VlanId, tMacAddr McastAddr,
                                         UINT4 RcvPortId);
INT4 VlanRedHandlePortOperStatusUpdate (UINT4 u4Port, UINT1 u1Status);

tVlanRedGvrpLearntEntry *VlanRedGetGvrpLearntEntry (tVlanId VlanId);
tVlanRedGvrpLearntEntry *VlanRedModifyGvrpLearntEntry (tVlanId VlanId, 
                                                       tLocalPortList PortList);
VOID VlanRedAddGvrpLearntEntry (tVlanRedGvrpLearntEntry *pGvrpEntry);
VOID VlanRedDelGvrpLearntEntry (tVlanId VlanId);
tVlanRedGmrpLearntEntry *VlanRedGetGmrpLearntEntry (tVlanId VlanId, 
                                                    tMacAddr McastAddr);
tVlanRedGmrpLearntEntry *VlanRedModifyGmrpLearntEntry (tVlanId VlanId, 
                                                       tMacAddr McastAddr, 
                                                       tLocalPortList PortList);
INT4 VlanRedAddGmrpLearntEntry (tVlanRedGmrpLearntEntry *pGmrpEntry);

UINT4 VlanRedGetVlanDelHashIndex (tVlanId VlanId);
UINT4 VlanRedGetGroupDelHashIndex (tVlanId VlanId, tMacAddr McastAddr);
VOID VlanRedDelAllVlanDeletes (VOID);
VOID VlanRedDelAllGroupDeletes (VOID);
VOID VlanRedAuditMain (INT1 *pi1Param);
VOID VlanRedSetChangedFlag (VOID);
INT4 VlanRedSendMsgToRm (tRmMsg *pMsg, UINT2 u2OffSet);
INT4 VlanRedLock (VOID);
INT4 VlanRedUnLock (VOID);
INT4 VlanRedGmrpLrntFreeFn (tRBElem *pRBElem, UINT4 u4ContextId);

VOID VlanRedSendBulkReqMsg (VOID);
VOID VlanRedSyncBulkUpdNextVlanId (tVlanId VlanId, UINT1 u1UpdType); 
VOID VlanRedHandleBulkUpdateEvent (VOID);
INT4 VlanRedSendBulkUpdTailMsg (VOID);

VOID VlanRedSyncWildCardTbl (UINT4 u4Context);
VOID VlanRedHandleDynSyncAudit (VOID);
VOID VlanExecuteCmdAndCalculateChkSum  (VOID);
#ifdef NPAPI_WANTED
VOID VlanRedStartAudit (VOID);
VOID VlanRedContextAudit (UINT4 u4ContextId);
VOID VlanRedSyncVlanTable (UINT4 u4Context);
VOID VlanRedSyncPortParams (UINT4 u4Context);
VOID VlanRedSyncDefaultVlanId (VOID);

VOID VlanRedSyncFdb (UINT4 u4Context);
VOID
VlanRedSyncFdbs (UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr,
              UINT4 u4RcvPort);
VOID VlanRedSyncUpVlanMemberPorts (tVlanCurrEntry *pVlanEntry, 
                                   tHwMiVlanEntry *pHwEntry);
VOID VlanRedSyncPortProtocolParams (UINT4 u4Context, UINT2 u2Port);
VOID VlanRedProtocolVlanCb (UINT4 u4IfIndex, UINT4 u4HwGroupId, 
                            tVlanProtoTemplate *pProtoTemplate,
                            tVlanId VlanId);
VOID                                                                                     VlanRedSyncPendingVlanEntries (UINT4 u4Context);
VOID 
VlanRedSyncMcastTable (UINT4 u4Context);
VOID
VlanRedSyncPendingMcastEntries (UINT4 u4Context);
VOID 
VlanRedSyncMcastEntry (UINT4 u4Context,
                       tVlanId VlanId,
                       tMacAddr MacAddr,
                       UINT4 u4RcvPort);

VOID VlanRedVlanMcastCb (tVlanId VlanId, tMacAddr McastAddr, UINT2 u2RcvPort, 
                         tLocalPortList HwPortList);
VOID VlanRedVlanUcastCb (UINT4 u4FdbId, tMacAddr MacAddr, UINT2 u2RcvPort, 
                         tHwUnicastMacEntry *pUcastEntry, tLocalPortList AllowedList, 
                         UINT1 u1Status);
VOID VlanRedNotifyMcastAddFailure (tVlanId VlanId, tMacAddr MacAddr, 
                                   tLocalPortList LearntPortList, 
                                   tLocalPortList FailedPortList);
VOID VlanRedWildCardCb (tMacAddr MacAddr);
INT4 VlanRedProcessStUcastSyncInfo (tVlanRedStUcastCacheNode *pUpdtVlanRedCacheNode );                 
INT4 VlanRedProcessMcastSyncInfo (tVlanRedMcastCacheNode *pUpdtVlanRedCacheNode );                 
INT4 VlanRedAddMcastCacheEntry (tVlanRedMcastCacheNode *pUpdtVlanRedCacheNode );
tVlanRedMcastCacheNode * VlanRedGetMcastCacheEntry (UINT4 u4ContextId,
                             tMacAddr MacAddres,
                             tVlanId u4FdbId,
                             UINT4 u4RcvPort);
INT4
VlanRedDelMcastCacheEntry (UINT4 u4ContextId,
                             tMacAddr MacAddres,
                             tVlanId u4FdbId,
                             UINT4 u4RcvPort);

INT4 VlanRedAddStUcastCacheEntry (tVlanRedStUcastCacheNode *pUpdtVlanRedCacheNode );
tVlanRedStUcastCacheNode * VlanRedGetStUcastCacheEntry (UINT4 u4ContextId,
                             tMacAddr MacAddres,
                             UINT4 u4FdbId,
                             UINT4 u4RcvPort);
INT4
VlanRedDelStUcastCacheEntry (UINT4 u4ContextId,
                             tMacAddr MacAddres,
                             UINT4 u4FdbId,
                             UINT4 u4RcvPort);
INT4 VlanRedStUcastSendCacheInfo (tVlanRedStUcastCacheNode *pVlanRedCacheNode );
INT4 VlanRedMcastSendCacheInfo (tVlanRedMcastCacheNode *pVlanRedCacheNode );
INT4
VlanRedHwUpdateMcastSync (UINT4 u4ContextId,
                            tVlanId VlanId,
                            tMacAddr MacAddress,
                            UINT4 u4RcvPort, INT1 i1RedSyncState);
INT4
VlanRedSyncMcastSwUpdate (UINT4 u4ContextId, 
                            tVlanId VlanId,
                            tMacAddr MacAddress,
                            UINT4 u4RcvPort,
                            UINT1 u1Action);
INT4 VlanRedMcastCheckAndClearCache (tVlanRedMcastCacheNode *pVlanRedCacheNode );
VOID VlanRedProcessMcastCacheInfo (tRmMsg * pMsg , UINT4 u4ContextId, UINT2 *pu2OffSet );

VOID
VlanRedSyncPendingProtoVlanEntries (UINT4 u4Context);
VOID
VlanRedAuditProtoVlanTable (tVlanProtoTemplate *pVlanProtoTemplate,UINT4 u4Context, UINT4 u4GroupId,  UINT2 u2Port);
 INT4
 VlanRedHwUpdateProtoVlanSync (tVlanProtoTemplate *pVlanProtoTemplate, UINT4 u4ContextId,
                             UINT4 u4GroupId,
                             UINT4 u4IfIndex,
                             INT1 i1RedSyncState,
                             INT1 i1Action);
INT4
VlanRedSyncProtoVlanSwUpdate (UINT4 u4ContextId,
                            UINT4  u4GroupId,
                            UINT2  u2Port,
                            UINT1 u1Action);

INT4 VlanRedProtoCheckAndClearCache (tVlanRedProtoVlanCacheNode *pVlanRedCacheNode);
VOID
VlanRedProcessProtoVlanCacheInfo (tRmMsg * pMsg, UINT4 u4ContextId,                    
UINT2 *pu2OffSet);                 
INT4
VlanRedProcessProtoSyncInfo (tVlanRedProtoVlanCacheNode *pUpdtVlanRedCacheNode);
INT4
VlanRedSendProtoVlanCacheInfo (tVlanRedProtoVlanCacheNode  *pVlanRedCacheNode);
 tVlanRedProtoVlanCacheNode *
VlanRedGetProtoVlanCacheEntry (UINT4 u4ContextId,                                      
UINT4  u4GroupId, UINT2 u2Port);         
INT4 VlanRedAddProtoVlanCacheEntry (tVlanRedProtoVlanCacheNode *pUpdtVlanRedCacheNode);           
INT4
VlanRedDelProtoVlanCache (UINT4 u4ContextId, UINT4 u4GroupId, UINT2 u2Port);
INT4
VlanRedProtoVlanCacheFreeNodes (tRBElem * pRBElem, UINT4 u4Arg);
INT4
VlanRedHwUpdateStUcastSync (UINT4 u4ContextId,
                            UINT4 u4FdbId,
                            tMacAddr MacAddress,
                            UINT4 u4RcvPort, INT1 i1RedSyncState, INT1 i1Action);
INT4
VlanRedSyncStUcastSwUpdate (UINT4 u4ContextId,
                            UINT4 u4FdbId,
                            tMacAddr MacAddress,
                            UINT4 u4RcvPort,
                            UINT1 u1Action);
INT4 VlanRedStUcastCheckAndClearCache (tVlanRedStUcastCacheNode *pVlanRedCacheNode );
VOID VlanRedProcessStUcastCacheInfo (tRmMsg * pMsg , UINT4 u4ContextId, UINT2 *pu2OffSet );

INT4 VlanRedProcessSyncInfo (tVlanRedCacheNode *pUpdtVlanRedCacheNode );                 
INT4 VlanRedAddCacheEntry (tVlanRedCacheNode *pUpdtVlanRedCacheNode );
tVlanRedCacheNode * VlanRedGetCacheEntry (UINT4 u4ContextId , tVlanId VlanId );
INT4 VlanRedDelCacheEntry (UINT4 u4ContextId , tVlanId VlanId );
INT4 VlanRedSendCacheInfo (tVlanRedCacheNode *pVlanRedCacheNode );
INT4 VlanRedSyncHwUpdate (UINT4 u4ContextId , tVlanId VlanId , INT1 i1RedSyncState );
INT4 VlanRedSyncSwUpdate (UINT4 u4ContextId , tVlanId VlanId, UINT1 u1Action);
INT4 VlanRedCheckAndClearCache (tVlanRedCacheNode *pVlanRedCacheNode );
VOID VlanRedProcessCacheInfo (tRmMsg * pMsg , UINT4 u4ContextId, UINT2 *pu2OffSet );
INT4 VlanRedCacheRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4 VlanRedStUcastCacheRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4 VlanRedMcastCacheRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4
VlanRedProtoVlanRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);

INT4
VlanRedSendStUcastCacheInfo (tVlanRedStUcastCacheNode  *pVlanRedCacheNode);
INT4
VlanRedSendMcastCacheInfo (tVlanRedMcastCacheNode  *pVlanRedCacheNode);
INT4
VlanRedCacheFreeNodes (tRBElem * pRBElem, UINT4 u4Arg);
INT4
VlanRedMcastCacheFreeNodes (tRBElem * pRBElem, UINT4 u4Arg);
INT4
VlanRedStUcastCacheFreeNodes (tRBElem * pRBElem, UINT4 u4Arg);
VOID VlanRedAuditVlanTable (UINT4 u4Context, tVlanId VlanId);
VOID VlanRedSyncPendingFdbs (UINT4 u4Context);
VOID
VlanSyncFdbs (UINT4 u4ContextId, UINT4 u4FdbId, tMacAddr MacAddr,
              tLocalPortList AllowedToGoToPorts, UINT4 u4RcvPort);
#endif /* NPAPI_WANTED */
#ifndef NP_BACKWD_COMPATIBILITY
VOID VlanWrRedVlanMcastCb (tVlanId VlanId, tMacAddr McastAddr, UINT4 u4RcvPort,
                           tHwPortArray *pHwPortArray);

VOID VlanWrRedVlanUcastCb (UINT4 u4FdbId, tMacAddr MacAddr, UINT4 u4RcvPort,
                           tHwUnicastMacEntry * pUcastEntry,
                           tHwPortArray *pHwPortArray, UINT1 u1Status);
#else
VOID VlanWrRedVlanMcastCb (tVlanId VlanId, tMacAddr McastAddr, UINT4 u4RcvPort,
                           tPortList HwPortList);
                           
VOID VlanWrRedVlanUcastCb (UINT4 u4FdbId, tMacAddr MacAddr, UINT4 u4RcvPort,
                           tHwUnicastMacEntry * pUcastEntry,
                           tPortList AllowedList, UINT1 u1Status);
#endif
#endif /* _VLANRED_H */
