/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : vlnpbred.h                                      */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 20 Dec 2006                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains VLAN PB redundancy related    */
/*                          prototype.                                       */
/*****************************************************************************/

#ifndef _VLNPBRED_H_
#define _VLNPBRED_H_

VOID VlanPbRedSyncPbPortProperties (UINT4 u4Context, UINT2 u2Port);

VOID VlanPbRedSyncCvidEtries(UINT4 u4Context, UINT2 u2Port);

VOID VlanPbRedSyncVidTranslationEntries (UINT4 u4Context, UINT2 u2Port);

#endif /*_VLNPBRED_H_*/
