/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmpvlwr.h,v 1.26.22.1 2018/03/15 12:59:54 siva Exp $
 *
 * Description: This file contains wrappers used in VLAN module.
 *
 *******************************************************************/
#ifndef _FSMPVLWR_H
#define _FSMPVLWR_H

VOID RegisterFSMPVL(VOID);

VOID UnRegisterFSMPVL(VOID);
INT4 FsMIDot1qFutureVlanGlobalTraceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanGlobalTraceSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanGlobalTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanGlobalTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsMIDot1qFutureVlanGlobalsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanMacBasedOnAllPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortProtoBasedOnAllPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanShutdownStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureGarpShutdownStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanLearningModeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanHybridTypeDefaultGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureGvrpOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureGmrpOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanContextNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureGarpDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureUnicastMacLearningLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureBaseBridgeModeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanSubnetBasedOnAllPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanGlobalMacLearningStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanApplyEnhancedFilteringCriteriaGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanGlobalsFdbFlushGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanUserDefinedTPIDGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanRemoteFdbFlushGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanMacBasedOnAllPortsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortProtoBasedOnAllPortsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanShutdownStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureGarpShutdownStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanLearningModeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanHybridTypeDefaultSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureGarpDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureUnicastMacLearningLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureBaseBridgeModeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanSubnetBasedOnAllPortsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanGlobalMacLearningStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanApplyEnhancedFilteringCriteriaSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanGlobalsFdbFlushSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanUserDefinedTPIDSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanRemoteFdbFlushSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanMacBasedOnAllPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortProtoBasedOnAllPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanShutdownStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureGarpShutdownStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanLearningModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanHybridTypeDefaultTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureGarpDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureUnicastMacLearningLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureBaseBridgeModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanSubnetBasedOnAllPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanGlobalMacLearningStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanApplyEnhancedFilteringCriteriaTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanGlobalsFdbFlushTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanUserDefinedTPIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanRemoteFdbFlushTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanGlobalsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);










INT4 GetNextIndexFsMIDot1qFutureVlanPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanPortTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacBasedClassificationGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortPortProtoBasedClassificationGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanFilteringUtilityCriteriaGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortProtectedGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetBasedClassificationGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortUnicastMacLearningGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpJoinEmptyTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpJoinEmptyRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpJoinInTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpJoinInRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpLeaveInTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpLeaveInRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpEmptyTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpEmptyRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpLeaveAllTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpLeaveAllRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGmrpDiscardCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpJoinEmptyTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpJoinEmptyRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpJoinInTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpJoinInRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpLeaveInTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpLeaveInRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpEmptyTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpEmptyRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpLeaveAllTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpLeaveAllRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortGvrpDiscardCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortFdbFlushGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortIngressEtherTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortEgressEtherTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortEgressTPIDTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortAllowableTPID1Get(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortAllowableTPID2Get(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortAllowableTPID3Get(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortClearGarpStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFuturePortPacketReflectionStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacBasedClassificationSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortPortProtoBasedClassificationSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanFilteringUtilityCriteriaSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortProtectedSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetBasedClassificationSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortUnicastMacLearningSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortFdbFlushSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortIngressEtherTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortEgressEtherTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortEgressTPIDTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortAllowableTPID1Set(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortAllowableTPID2Set(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortAllowableTPID3Set(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortClearGarpStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFuturePortPacketReflectionStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacBasedClassificationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortPortProtoBasedClassificationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanFilteringUtilityCriteriaTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortProtectedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetBasedClassificationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortUnicastMacLearningTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortFdbFlushTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortIngressEtherTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortEgressEtherTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortEgressTPIDTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortAllowableTPID1Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortAllowableTPID2Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortAllowableTPID3Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortClearGarpStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFuturePortPacketReflectionStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexFsMIDot1qFutureVlanPortMacMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanPortMacMapVidGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapMcastBcastOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapVidSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapNameSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapMcastBcastOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapMcastBcastOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortMacMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexFsMIDot1qFutureVlanFidMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanFidGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanFidSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanFidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanFidMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIDot1qFutureVlanTunnelConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanBridgeModeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelBpduPriGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanBridgeModeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelBpduPriSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanBridgeModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelBpduPriTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIDot1qFutureVlanTunnelTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanTunnelStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanTunnelStpPDUsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelStpPDUsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelStpPDUsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelGvrpPDUsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelGvrpPDUsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelGvrpPDUsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelIgmpPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelIgmpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelIgmpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelStpPDUsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelGvrpPDUsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelIgmpPktsSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelStpPDUsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelGvrpPDUsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelIgmpPktsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanTunnelProtocolTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexFsMIDot1qFutureVlanCounterTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanCounterRxUcastGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterRxMcastBcastGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterTxUnknUcastGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterTxUcastGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterTxBcastGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterRxFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterRxBytesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterTxFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterTxBytesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterDiscardFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterDiscardBytesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanCounterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIDot1qFutureVlanUnicastMacControlTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanUnicastMacLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanAdminMacLearningStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanOperMacLearningStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanUnicastMacLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanAdminMacLearningStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanUnicastMacLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanAdminMacLearningStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanUnicastMacControlTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 FsMIDot1qFutureGarpGlobalTraceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureGarpGlobalTraceSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureGarpGlobalTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureGarpGlobalTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsMIDot1qFutureVlanTpFdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanOldTpFdbPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureConnectionIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIDot1qFutureVlanWildCardTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanWildCardRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanWildCardRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanWildCardRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanWildCardTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIDot1qFutureVlanWildCardPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanIsWildCardEgressPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanIsWildCardEgressPortSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanIsWildCardEgressPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanWildCardPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIDot1qFutureStaticUnicastExtnTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureStaticConnectionIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureStaticConnectionIdentifierSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureStaticConnectionIdentifierTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureStaticUnicastExtnTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIDot1qFutureVlanPortSubnetMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanPortSubnetMapVidGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapARPOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapVidSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapARPOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapARPOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMIDot1qFutureVlanSwStatsEnabledGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanSwStatsEnabledSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanSwStatsEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanSwStatsEnabledDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMIDot1qFutureStVlanExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureStVlanTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureStVlanVidGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureStVlanFdbFlushGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureStVlanEgressEthertypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureStVlanFdbFlushSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureStVlanEgressEthertypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureStVlanFdbFlushTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureStVlanEgressEthertypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureStVlanExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable
(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanPortSubnetMapExtVidGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapExtARPOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapExtRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapExtVidSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapExtARPOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapExtRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapExtVidTest
(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapExtARPOptionTest
(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapExtRowStatusTest
(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanPortSubnetMapExtTableDep
(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIDot1qFuturePortVlanExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFuturePortVlanFdbFlushGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFuturePortVlanFdbFlushSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFuturePortVlanFdbFlushTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFuturePortVlanExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIDot1qFutureVlanLoopbackTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIDot1qFutureVlanLoopbackStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanLoopbackStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanLoopbackStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIDot1qFutureVlanLoopbackTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSMPVLWR_H */
