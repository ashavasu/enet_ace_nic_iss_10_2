/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsbedb.h,v 1.6 2011/03/21 12:26:43 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMSBEDB_H
#define _FSMSBEDB_H

UINT1 FsDot1dExtBaseTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1dPortCapabilitiesTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot1dPortPriorityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot1dUserPriorityRegenTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot1dTrafficClassTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot1dPortOutboundAccessPriorityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot1dPortGarpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot1dPortGmrpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot1dTpHCPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot1dTpPortOverflowTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsmsbe [] ={1,3,6,1,4,1,2076,116,6};
tSNMP_OID_TYPE fsmsbeOID = {9, fsmsbe};


/* Generated OID's for tables */
UINT4 FsDot1dExtBaseTable [] ={1,3,6,1,4,1,2076,116,6,1,1,1};
tSNMP_OID_TYPE FsDot1dExtBaseTableOID = {12, FsDot1dExtBaseTable};


UINT4 FsDot1dPortCapabilitiesTable [] ={1,3,6,1,4,1,2076,116,6,1,1,2};
tSNMP_OID_TYPE FsDot1dPortCapabilitiesTableOID = {12, FsDot1dPortCapabilitiesTable};


UINT4 FsDot1dPortPriorityTable [] ={1,3,6,1,4,1,2076,116,6,1,2,1};
tSNMP_OID_TYPE FsDot1dPortPriorityTableOID = {12, FsDot1dPortPriorityTable};


UINT4 FsDot1dUserPriorityRegenTable [] ={1,3,6,1,4,1,2076,116,6,1,2,2};
tSNMP_OID_TYPE FsDot1dUserPriorityRegenTableOID = {12, FsDot1dUserPriorityRegenTable};


UINT4 FsDot1dTrafficClassTable [] ={1,3,6,1,4,1,2076,116,6,1,2,3};
tSNMP_OID_TYPE FsDot1dTrafficClassTableOID = {12, FsDot1dTrafficClassTable};


UINT4 FsDot1dPortOutboundAccessPriorityTable [] ={1,3,6,1,4,1,2076,116,6,1,2,4};
tSNMP_OID_TYPE FsDot1dPortOutboundAccessPriorityTableOID = {12, FsDot1dPortOutboundAccessPriorityTable};


UINT4 FsDot1dPortGarpTable [] ={1,3,6,1,4,1,2076,116,6,1,3,1};
tSNMP_OID_TYPE FsDot1dPortGarpTableOID = {12, FsDot1dPortGarpTable};


UINT4 FsDot1dPortGmrpTable [] ={1,3,6,1,4,1,2076,116,6,1,4,1};
tSNMP_OID_TYPE FsDot1dPortGmrpTableOID = {12, FsDot1dPortGmrpTable};


UINT4 FsDot1dTpHCPortTable [] ={1,3,6,1,4,1,2076,116,4,5};
tSNMP_OID_TYPE FsDot1dTpHCPortTableOID = {10, FsDot1dTpHCPortTable};


UINT4 FsDot1dTpPortOverflowTable [] ={1,3,6,1,4,1,2076,116,4,6};
tSNMP_OID_TYPE FsDot1dTpPortOverflowTableOID = {10, FsDot1dTpPortOverflowTable};




UINT4 FsDot1dBridgeContextId [ ] ={1,3,6,1,4,1,2076,116,6,1,1,1,1,1};
UINT4 FsDot1dDeviceCapabilities [ ] ={1,3,6,1,4,1,2076,116,6,1,1,1,1,2};
UINT4 FsDot1dTrafficClassesEnabled [ ] ={1,3,6,1,4,1,2076,116,6,1,1,1,1,3};
UINT4 FsDot1dGmrpStatus [ ] ={1,3,6,1,4,1,2076,116,6,1,1,1,1,4};
UINT4 FsDot1dPortCapabilities [ ] ={1,3,6,1,4,1,2076,116,6,1,1,2,1,1};
UINT4 FsDot1dPortDefaultUserPriority [ ] ={1,3,6,1,4,1,2076,116,6,1,2,1,1,1};
UINT4 FsDot1dPortNumTrafficClasses [ ] ={1,3,6,1,4,1,2076,116,6,1,2,1,1,2};
UINT4 FsDot1dUserPriority [ ] ={1,3,6,1,4,1,2076,116,6,1,2,2,1,1};
UINT4 FsDot1dRegenUserPriority [ ] ={1,3,6,1,4,1,2076,116,6,1,2,2,1,2};
UINT4 FsDot1dTrafficClassPriority [ ] ={1,3,6,1,4,1,2076,116,6,1,2,3,1,1};
UINT4 FsDot1dTrafficClass [ ] ={1,3,6,1,4,1,2076,116,6,1,2,3,1,2};
UINT4 FsDot1dPortOutboundAccessPriority [ ] ={1,3,6,1,4,1,2076,116,6,1,2,4,1,1};
UINT4 FsDot1dPortGarpJoinTime [ ] ={1,3,6,1,4,1,2076,116,6,1,3,1,1,1};
UINT4 FsDot1dPortGarpLeaveTime [ ] ={1,3,6,1,4,1,2076,116,6,1,3,1,1,2};
UINT4 FsDot1dPortGarpLeaveAllTime [ ] ={1,3,6,1,4,1,2076,116,6,1,3,1,1,3};
UINT4 FsDot1dPortGmrpStatus [ ] ={1,3,6,1,4,1,2076,116,6,1,4,1,1,1};
UINT4 FsDot1dPortGmrpFailedRegistrations [ ] ={1,3,6,1,4,1,2076,116,6,1,4,1,1,2};
UINT4 FsDot1dPortGmrpLastPduOrigin [ ] ={1,3,6,1,4,1,2076,116,6,1,4,1,1,3};
UINT4 FsDot1dPortRestrictedGroupRegistration [ ] ={1,3,6,1,4,1,2076,116,6,1,4,1,1,4};
UINT4 FsDot1dTpHCPortInFrames [ ] ={1,3,6,1,4,1,2076,116,4,5,1,1};
UINT4 FsDot1dTpHCPortOutFrames [ ] ={1,3,6,1,4,1,2076,116,4,5,1,2};
UINT4 FsDot1dTpHCPortInDiscards [ ] ={1,3,6,1,4,1,2076,116,4,5,1,3};
UINT4 FsDot1dTpPortInOverflowFrames [ ] ={1,3,6,1,4,1,2076,116,4,6,1,1};
UINT4 FsDot1dTpPortOutOverflowFrames [ ] ={1,3,6,1,4,1,2076,116,4,6,1,2};
UINT4 FsDot1dTpPortInOverflowDiscards [ ] ={1,3,6,1,4,1,2076,116,4,6,1,3};




tMbDbEntry FsDot1dTpHCPortTableMibEntry[]= {

{{12,FsDot1dTpHCPortInFrames}, GetNextIndexFsDot1dTpHCPortTable, FsDot1dTpHCPortInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsDot1dTpHCPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dTpHCPortOutFrames}, GetNextIndexFsDot1dTpHCPortTable, FsDot1dTpHCPortOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsDot1dTpHCPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dTpHCPortInDiscards}, GetNextIndexFsDot1dTpHCPortTable, FsDot1dTpHCPortInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsDot1dTpHCPortTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dTpHCPortTableEntry = { 3, FsDot1dTpHCPortTableMibEntry };

tMbDbEntry FsDot1dTpPortOverflowTableMibEntry[]= {

{{12,FsDot1dTpPortInOverflowFrames}, GetNextIndexFsDot1dTpPortOverflowTable, FsDot1dTpPortInOverflowFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dTpPortOverflowTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dTpPortOutOverflowFrames}, GetNextIndexFsDot1dTpPortOverflowTable, FsDot1dTpPortOutOverflowFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dTpPortOverflowTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dTpPortInOverflowDiscards}, GetNextIndexFsDot1dTpPortOverflowTable, FsDot1dTpPortInOverflowDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dTpPortOverflowTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dTpPortOverflowTableEntry = { 3, FsDot1dTpPortOverflowTableMibEntry };

tMbDbEntry FsDot1dExtBaseTableMibEntry[]= {

{{14,FsDot1dBridgeContextId}, GetNextIndexFsDot1dExtBaseTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1dExtBaseTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1dDeviceCapabilities}, GetNextIndexFsDot1dExtBaseTable, FsDot1dDeviceCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDot1dExtBaseTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1dTrafficClassesEnabled}, GetNextIndexFsDot1dExtBaseTable, FsDot1dTrafficClassesEnabledGet, FsDot1dTrafficClassesEnabledSet, FsDot1dTrafficClassesEnabledTest, FsDot1dExtBaseTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dExtBaseTableINDEX, 1, 0, 0, "1"},

{{14,FsDot1dGmrpStatus}, GetNextIndexFsDot1dExtBaseTable, FsDot1dGmrpStatusGet, FsDot1dGmrpStatusSet, FsDot1dGmrpStatusTest, FsDot1dExtBaseTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dExtBaseTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dExtBaseTableEntry = { 4, FsDot1dExtBaseTableMibEntry };

tMbDbEntry FsDot1dPortCapabilitiesTableMibEntry[]= {

{{14,FsDot1dPortCapabilities}, GetNextIndexFsDot1dPortCapabilitiesTable, FsDot1dPortCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDot1dPortCapabilitiesTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dPortCapabilitiesTableEntry = { 1, FsDot1dPortCapabilitiesTableMibEntry };

tMbDbEntry FsDot1dPortPriorityTableMibEntry[]= {

{{14,FsDot1dPortDefaultUserPriority}, GetNextIndexFsDot1dPortPriorityTable, FsDot1dPortDefaultUserPriorityGet, FsDot1dPortDefaultUserPrioritySet, FsDot1dPortDefaultUserPriorityTest, FsDot1dPortPriorityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dPortPriorityTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1dPortNumTrafficClasses}, GetNextIndexFsDot1dPortPriorityTable, FsDot1dPortNumTrafficClassesGet, FsDot1dPortNumTrafficClassesSet, FsDot1dPortNumTrafficClassesTest, FsDot1dPortPriorityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dPortPriorityTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dPortPriorityTableEntry = { 2, FsDot1dPortPriorityTableMibEntry };

tMbDbEntry FsDot1dUserPriorityRegenTableMibEntry[]= {

{{14,FsDot1dUserPriority}, GetNextIndexFsDot1dUserPriorityRegenTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsDot1dUserPriorityRegenTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1dRegenUserPriority}, GetNextIndexFsDot1dUserPriorityRegenTable, FsDot1dRegenUserPriorityGet, FsDot1dRegenUserPrioritySet, FsDot1dRegenUserPriorityTest, FsDot1dUserPriorityRegenTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dUserPriorityRegenTableINDEX, 2, 0, 0, NULL},
};
tMibData FsDot1dUserPriorityRegenTableEntry = { 2, FsDot1dUserPriorityRegenTableMibEntry };

tMbDbEntry FsDot1dTrafficClassTableMibEntry[]= {

{{14,FsDot1dTrafficClassPriority}, GetNextIndexFsDot1dTrafficClassTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsDot1dTrafficClassTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1dTrafficClass}, GetNextIndexFsDot1dTrafficClassTable, FsDot1dTrafficClassGet, FsDot1dTrafficClassSet, FsDot1dTrafficClassTest, FsDot1dTrafficClassTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dTrafficClassTableINDEX, 2, 0, 0, NULL},
};
tMibData FsDot1dTrafficClassTableEntry = { 2, FsDot1dTrafficClassTableMibEntry };

tMbDbEntry FsDot1dPortOutboundAccessPriorityTableMibEntry[]= {

{{14,FsDot1dPortOutboundAccessPriority}, GetNextIndexFsDot1dPortOutboundAccessPriorityTable, FsDot1dPortOutboundAccessPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1dPortOutboundAccessPriorityTableINDEX, 2, 0, 0, NULL},
};
tMibData FsDot1dPortOutboundAccessPriorityTableEntry = { 1, FsDot1dPortOutboundAccessPriorityTableMibEntry };

tMbDbEntry FsDot1dPortGarpTableMibEntry[]= {

{{14,FsDot1dPortGarpJoinTime}, GetNextIndexFsDot1dPortGarpTable, FsDot1dPortGarpJoinTimeGet, FsDot1dPortGarpJoinTimeSet, FsDot1dPortGarpJoinTimeTest, FsDot1dPortGarpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dPortGarpTableINDEX, 1, 0, 0, "20"},

{{14,FsDot1dPortGarpLeaveTime}, GetNextIndexFsDot1dPortGarpTable, FsDot1dPortGarpLeaveTimeGet, FsDot1dPortGarpLeaveTimeSet, FsDot1dPortGarpLeaveTimeTest, FsDot1dPortGarpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dPortGarpTableINDEX, 1, 0, 0, "60"},

{{14,FsDot1dPortGarpLeaveAllTime}, GetNextIndexFsDot1dPortGarpTable, FsDot1dPortGarpLeaveAllTimeGet, FsDot1dPortGarpLeaveAllTimeSet, FsDot1dPortGarpLeaveAllTimeTest, FsDot1dPortGarpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dPortGarpTableINDEX, 1, 0, 0, "1000"},
};
tMibData FsDot1dPortGarpTableEntry = { 3, FsDot1dPortGarpTableMibEntry };

tMbDbEntry FsDot1dPortGmrpTableMibEntry[]= {

{{14,FsDot1dPortGmrpStatus}, GetNextIndexFsDot1dPortGmrpTable, FsDot1dPortGmrpStatusGet, FsDot1dPortGmrpStatusSet, FsDot1dPortGmrpStatusTest, FsDot1dPortGmrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dPortGmrpTableINDEX, 1, 0, 0, "1"},

{{14,FsDot1dPortGmrpFailedRegistrations}, GetNextIndexFsDot1dPortGmrpTable, FsDot1dPortGmrpFailedRegistrationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dPortGmrpTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1dPortGmrpLastPduOrigin}, GetNextIndexFsDot1dPortGmrpTable, FsDot1dPortGmrpLastPduOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsDot1dPortGmrpTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1dPortRestrictedGroupRegistration}, GetNextIndexFsDot1dPortGmrpTable, FsDot1dPortRestrictedGroupRegistrationGet, FsDot1dPortRestrictedGroupRegistrationSet, FsDot1dPortRestrictedGroupRegistrationTest, FsDot1dPortGmrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dPortGmrpTableINDEX, 1, 0, 0, "2"},
};
tMibData FsDot1dPortGmrpTableEntry = { 4, FsDot1dPortGmrpTableMibEntry };

#endif /* _FSMSBEDB_H */

