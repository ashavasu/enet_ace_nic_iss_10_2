/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1evlw.h,v 1.2 2016/07/16 11:15:03 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeEvbSysType ARG_LIST((INT4 *));

INT1
nmhGetIeee8021BridgeEvbSysNumExternalPorts ARG_LIST((UINT4 *));

INT1
nmhGetIeee8021BridgeEvbSysEvbLldpTxEnable ARG_LIST((INT4 *));

INT1
nmhGetIeee8021BridgeEvbSysEvbLldpManual ARG_LIST((INT4 *));

INT1
nmhGetIeee8021BridgeEvbSysEvbLldpGidCapable ARG_LIST((INT4 *));

INT1
nmhGetIeee8021BridgeEvbSysEcpAckTimer ARG_LIST((INT4 *));

INT1
nmhGetIeee8021BridgeEvbSysEcpMaxRetries ARG_LIST((INT4 *));

INT1
nmhGetIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay ARG_LIST((INT4 *));

INT1
nmhGetIeee8021BridgeEvbSysVdpDfltReinitKeepAlive ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeEvbSysEvbLldpTxEnable ARG_LIST((INT4 ));

INT1
nmhSetIeee8021BridgeEvbSysEvbLldpManual ARG_LIST((INT4 ));

INT1
nmhSetIeee8021BridgeEvbSysEvbLldpGidCapable ARG_LIST((INT4 ));

INT1
nmhSetIeee8021BridgeEvbSysEcpAckTimer ARG_LIST((INT4 ));

INT1
nmhSetIeee8021BridgeEvbSysEcpMaxRetries ARG_LIST((INT4 ));

INT1
nmhSetIeee8021BridgeEvbSysVdpDfltRsrcWaitDelay ARG_LIST((INT4 ));

INT1
nmhSetIeee8021BridgeEvbSysVdpDfltReinitKeepAlive ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeEvbSysEvbLldpTxEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbSysEvbLldpManual ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbSysEvbLldpGidCapable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbSysEcpAckTimer ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbSysEcpMaxRetries ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeEvbSysEvbLldpTxEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ieee8021BridgeEvbSysEvbLldpManual ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ieee8021BridgeEvbSysEvbLldpGidCapable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ieee8021BridgeEvbSysEcpAckTimer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ieee8021BridgeEvbSysEcpMaxRetries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgeEvbSbpTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeEvbSbpTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeEvbSbpTable  */

INT1
nmhGetFirstIndexIeee8021BridgeEvbSbpTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeEvbSbpTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeEvbSbpLldpManual ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbSbpVdpOperRsrcWaitDelay ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbSbpVdpOperReinitKeepAlive ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbSbpVdpOperToutKeepAlive ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeEvbSbpLldpManual ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeEvbSbpLldpManual ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeEvbSbpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgeEvbVSIDBTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBTable ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeEvbVSIDBTable  */

INT1
nmhGetFirstIndexIeee8021BridgeEvbVSIDBTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeEvbVSIDBTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeEvbVSITimeSinceCreate ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbVsiVdpOperCmd ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIeee8021BridgeEvbVsiOperRevert ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIeee8021BridgeEvbVsiOperHard ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIeee8021BridgeEvbVsiOperReason ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021BridgeEvbVSIMgrID ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021BridgeEvbVSIType ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIeee8021BridgeEvbVSITypeVersion ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021BridgeEvbVSIMvFormat ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIeee8021BridgeEvbVSINumMACs ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIeee8021BridgeEvbVDPMachineState ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIeee8021BridgeEvbVDPCommandsSucceeded ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbVDPCommandsFailed ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbVDPCommandReverts ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbVDPCounterDiscontinuity ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for Ieee8021BridgeEvbVSIDBMacTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeEvbVSIDBMacTable ARG_LIST((UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tMacAddr  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeEvbVSIDBMacTable  */

INT1
nmhGetFirstIndexIeee8021BridgeEvbVSIDBMacTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tMacAddr *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeEvbVSIDBMacTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , UINT4 , UINT4 *));

/* Proto Validate Index Instance for Ieee8021BridgeEvbUAPConfigTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeEvbUAPConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeEvbUAPConfigTable  */

INT1
nmhGetFirstIndexIeee8021BridgeEvbUAPConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeEvbUAPConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeEvbUAPComponentId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbUAPPort ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbUapConfigIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbUAPSchCdcpAdminEnable ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbUAPSchAdminCDCPRole ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbUAPSchAdminCDCPChanCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbUAPSchOperCDCPChanCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbUAPSchOperState ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbSchCdcpRemoteEnabled ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbSchCdcpRemoteRole ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbUAPConfigStorageType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbUAPConfigRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeEvbUAPSchCdcpAdminEnable ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgeEvbUAPSchAdminCDCPRole ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgeEvbUAPSchAdminCDCPChanCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetIeee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetIeee8021BridgeEvbUAPConfigStorageType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgeEvbUAPConfigRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeEvbUAPSchCdcpAdminEnable ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPRole ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPChanCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbUAPConfigStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbUAPConfigRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeEvbUAPConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgeEvbCAPConfigTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeEvbCAPConfigTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeEvbCAPConfigTable  */

INT1
nmhGetFirstIndexIeee8021BridgeEvbCAPConfigTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeEvbCAPConfigTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeEvbCAPComponentId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbCapConfigIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbCAPPort ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbCAPSChannelID ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbCAPAssociateSBPOrURPCompID ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbCAPAssociateSBPOrURPPort ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbCAPRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeEvbCAPAssociateSBPOrURPCompID ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021BridgeEvbCAPAssociateSBPOrURPPort ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021BridgeEvbCAPRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeEvbCAPAssociateSBPOrURPCompID ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbCAPAssociateSBPOrURPPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbCAPRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeEvbCAPConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgeEvbURPTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeEvbURPTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeEvbURPTable  */

INT1
nmhGetFirstIndexIeee8021BridgeEvbURPTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeEvbURPTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeEvbURPIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbURPBindToISSPort ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbURPLldpManual ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeEvbURPVdpOperRsrcWaitDelay ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbURPVdpOperRespWaitDelay ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbURPVdpOperReinitKeepAlive ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeEvbURPIfIndex ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgeEvbURPBindToISSPort ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021BridgeEvbURPLldpManual ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgeEvbURPVdpOperRespWaitDelay ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021BridgeEvbURPVdpOperReinitKeepAlive ARG_LIST((UINT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeEvbURPIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbURPBindToISSPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbURPLldpManual ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbURPVdpOperRespWaitDelay ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021BridgeEvbURPVdpOperReinitKeepAlive ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeEvbURPTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgeEvbEcpTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeEvbEcpTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeEvbEcpTable  */

INT1
nmhGetFirstIndexIeee8021BridgeEvbEcpTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeEvbEcpTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeEvbEcpOperAckTimerInit ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbEcpOperMaxRetries ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbEcpTxFrameCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbEcpTxRetryCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbEcpTxFailures ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeEvbEcpRxFrameCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));


