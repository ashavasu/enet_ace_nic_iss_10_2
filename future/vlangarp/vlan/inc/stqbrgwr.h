#ifndef _STQBRGWR_H
#define _STQBRGWR_H
INT4 GetNextIndexIeee8021QBridgeTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTQBRG(VOID);

VOID UnRegisterSTQBRG(VOID);
INT4 Ieee8021QBridgeVlanVersionNumberGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeMaxVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeMaxSupportedVlansGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeNumVlansGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeMvrpEnabledStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeMvrpEnabledStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeMvrpEnabledStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeMvrpEnabledStatus ARG_LIST((UINT4  ,INT4 ));
INT4 GetNextIndexIeee8021QBridgeCVlanPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeCVlanPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeCVlanPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeCVlanPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeCVlanPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeCVlanPortRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));
INT4 GetNextIndexIeee8021QBridgeFdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeFdbDynamicCountGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeFdbLearnedEntryDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeFdbAgingTimeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeFdbAgingTimeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeFdbAgingTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeFdbTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeFdbAgingTime ARG_LIST((UINT4  , UINT4  ,INT4 ));
INT4 GetNextIndexIeee8021QBridgeTpFdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeTpFdbPortGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeTpFdbStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanNumDeletesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIeee8021QBridgeVlanCurrentTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeVlanFdbIdGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanCurrentEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanCurrentUntaggedPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanCreationTimeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIeee8021QBridgeTpGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeTpGroupEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeTpGroupLearntGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIeee8021QBridgeForwardAllTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeForwardAllPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardAllStaticPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardAllForbiddenPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardAllStaticPortsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardAllForbiddenPortsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardAllStaticPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardAllForbiddenPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardAllTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeForwardAllStaticPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeForwardAllForbiddenPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
INT4 GetNextIndexIeee8021QBridgeForwardUnregisteredTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeForwardUnregisteredPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardUnregisteredStaticPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardUnregisteredForbiddenPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardUnregisteredStaticPortsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardUnregisteredForbiddenPortsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardUnregisteredStaticPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardUnregisteredForbiddenPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeForwardUnregisteredTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeForwardUnregisteredStaticPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeForwardUnregisteredForbiddenPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
INT4 GetNextIndexIeee8021QBridgeStaticUnicastTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeStaticUnicastStaticEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastForbiddenEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastStaticEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastForbiddenEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastStaticEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastForbiddenEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticUnicastTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeStaticUnicastStaticEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeStaticUnicastForbiddenEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeStaticUnicastStorageType ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgeStaticUnicastRowStatus ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));
INT4 GetNextIndexIeee8021QBridgeStaticMulticastTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeStaticMulticastStaticEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastForbiddenEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastStaticEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastForbiddenEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastStaticEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastForbiddenEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeStaticMulticastTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeStaticMulticastStaticEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeStaticMulticastForbiddenEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeStaticMulticastStorageType ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgeStaticMulticastRowStatus ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));
INT4 GetNextIndexIeee8021QBridgeVlanStaticTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeVlanStaticNameGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanForbiddenEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticUntaggedPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticNameSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanForbiddenEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticUntaggedPortsSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanForbiddenEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticUntaggedPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeVlanStaticTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeVlanStaticName ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeVlanStaticEgressPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeVlanForbiddenEgressPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeVlanStaticUntaggedPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeVlanStaticRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));
INT4 GetNextIndexIeee8021QBridgeNextFreeLocalVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeNextFreeLocalVlanIndexGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIeee8021QBridgePortVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgePvidGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortAcceptableFrameTypesGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortIngressFilteringGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortMvrpEnabledStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortMvrpFailedRegistrationsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortMvrpLastPduOriginGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortRestrictedVlanRegistrationGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePvidSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortAcceptableFrameTypesSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortIngressFilteringSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortMvrpEnabledStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortRestrictedVlanRegistrationSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePvidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortAcceptableFrameTypesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortIngressFilteringTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortMvrpEnabledStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortRestrictedVlanRegistrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgePortVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgePvid ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021QBridgePortAcceptableFrameTypes ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgePortIngressFiltering ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgePortMvrpEnabledStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgePortRestrictedVlanRegistration ARG_LIST((UINT4  , UINT4  ,INT4 ));
INT4 GetNextIndexIeee8021QBridgePortVlanStatisticsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeTpVlanPortInFramesGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeTpVlanPortOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeTpVlanPortInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIeee8021QBridgeLearningConstraintsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeLearningConstraintsTypeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintsStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintsTypeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintsStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintsTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintsStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeLearningConstraintsType ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgeLearningConstraintsStatus ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));
INT4 GetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeLearningConstraintDefaultsSetGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintDefaultsTypeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintDefaultsSetSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintDefaultsTypeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintDefaultsSetTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintDefaultsTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeLearningConstraintDefaultsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeLearningConstraintDefaultsSet ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgeLearningConstraintDefaultsType ARG_LIST((UINT4  ,INT4 ));
INT4 GetNextIndexIeee8021QBridgeProtocolGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeProtocolGroupIdGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolGroupRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolGroupIdSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolGroupRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolGroupIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolGroupRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolGroupTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeProtocolGroupId ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetIeee8021QBridgeProtocolGroupRowStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT4 GetNextIndexIeee8021QBridgeProtocolPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021QBridgeProtocolPortGroupVidGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolPortGroupVidSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolPortGroupVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021QBridgeProtocolPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/* Mid Level SET Routine for All Objects (for Incremental Save only).  */

INT1
nmhSetIeee8021QBridgeProtocolPortGroupVid ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgeProtocolPortRowStatus ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));
#endif /* _STQBRGWR_H */
