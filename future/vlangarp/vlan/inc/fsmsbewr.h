#ifndef _FSMSBEWR_H
#define _FSMSBEWR_H
INT4 GetNextIndexFsDot1dExtBaseTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMSBE(VOID);

VOID UnRegisterFSMSBE(VOID);
INT4 FsDot1dDeviceCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTrafficClassesEnabledGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dGmrpStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTrafficClassesEnabledSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dGmrpStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTrafficClassesEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dGmrpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dExtBaseTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsDot1dPortCapabilitiesTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dPortCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1dPortPriorityTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dPortDefaultUserPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortNumTrafficClassesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortDefaultUserPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortNumTrafficClassesSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortDefaultUserPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortNumTrafficClassesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortPriorityTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsDot1dUserPriorityRegenTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dRegenUserPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dRegenUserPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dRegenUserPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dUserPriorityRegenTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDot1dTrafficClassTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dTrafficClassGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTrafficClassSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTrafficClassTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dTrafficClassTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsDot1dPortOutboundAccessPriorityTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dPortOutboundAccessPriorityGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1dPortGarpTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dPortGarpJoinTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGarpLeaveTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGarpLeaveAllTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGarpJoinTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGarpLeaveTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGarpLeaveAllTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGarpJoinTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGarpLeaveTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGarpLeaveAllTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGarpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexFsDot1dPortGmrpTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dPortGmrpStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGmrpFailedRegistrationsGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGmrpLastPduOriginGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortRestrictedGroupRegistrationGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGmrpStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortRestrictedGroupRegistrationSet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGmrpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortRestrictedGroupRegistrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot1dPortGmrpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsDot1dTpHCPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dTpHCPortInFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpHCPortOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpHCPortInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot1dTpPortOverflowTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot1dTpPortInOverflowFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpPortOutOverflowFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsDot1dTpPortInOverflowDiscardsGet(tSnmpIndex *, tRetVal *);
#endif /* _FSMSBEWR_H */
