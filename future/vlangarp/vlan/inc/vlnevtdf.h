/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevtdf.h,v 1.6 2016/07/16 11:15:04 siva Exp $
 *
 * Description: This file contains VLAN EVB type definitions of the data 
 *              structures.
 *
 *******************************************************************/
#ifndef _VLNEVTDF_H
#define _VLNEVTDF_H

/* EVB Context Information */
typedef struct _EvbContextInfo
{
    UINT4 u4EvbSysCxtId;      /* Context Identifier */
    UINT4 u4EvbSysTrcLevel;   /* Trace level */
    INT4  i4EvbSysRowStatus;  /* Row status */
    INT4  i4EvbSysCtrlStatus; /* System control */
    INT4  i4EvbSysModStatus;  /* Module status */
    INT4  i4EvbSysTrapStatus; /* Trap status */
    INT4  i4EvbSysSChMode;    /* Auto or manual */
    UINT1 au1Reserved[4];     /* 64-bit padding */
}tEvbContextInfo;

/* EVB global information */
typedef struct _EvbGlobalInfo
{
    tEvbContextInfo *apEvbContextInfo[SYS_DEF_MAX_NUM_CONTEXTS];    
                                         /* EVB Context info */
    tRBTree         EvbUapIfTree;        /* RBTree for UAP Interface Entry */
    tRBTree         EvbSchIfTree;        /* RBTree for SChannel Interface Entry 
                                            based on UAP IfIndex and SVID*/
    tRBTree         EvbSchScidTree;      /* RBTree for SChannel Interface Entry 
                                            based on UAP IfIndex and SCID*/
    tMemPoolId      EvbCxtPoolId;        /* Pool Id for EVB Context table */
    tMemPoolId      EvbUapIfPoolId;      /* Pool Id for UAP Interface table */
    tMemPoolId      EvbSChIfPoolId;      /* Pool Id for SChannel If table */
    tMemPoolId      EvbQueIfPoolId;      /* Pool Id for Queue Interface table*/
    UINT4           u4EvbSysNumExternalPorts; /* Number of external ports 
                                                 available */
    UINT1           au1Reserved[4];      /* 64-bit padding */
}tEvbGlobalInfo;


/* EVB UAP Statistics */
typedef struct _EvbUapStatsEntry
{
    UINT4   u4EvbTxCdcpCount;        /* CDCPs transmitted */
    UINT4   u4EvbRxCdcpCount;        /* CDCPs received */
    UINT4   u4EvbSchAllocFailCount;  /* S-Channel allocation failed*/
    UINT4   u4EvbSchActiveFailCount; /* S-Channel activation failure */
    UINT4   u4SVIdPoolExcdCount;     /* SVID Pool exceeded */
    UINT4   u4EvbCdcpOtherDropCount; /* CDCP TLVs dropped for other failure*/
    UINT4   u4EvbCdcpRejectStationReq;/* No of (SCID,SVID)pair rejected */
    UINT1   au1Reserved[4];          /* 64-bit padding */
}tEvbUapStatsEntry;

/* EVB UAP Interface Information */
typedef struct _EvbUapIfEntry
{
    tRBNodeEmbd       UapIfNode;    /*Node for EvbUAPIfTree*/
    UINT4             u4UapIfIndex; /*CFA If Index of Uap. RBTree Index. It is
                                    introduced here to avoid padding for 64bit*/
    tEvbUapStatsEntry UapStats;     /* UAP Statstics*/
    tVlanList         UapSvidList;  /* SVID pool manager*/
    UINT1  au1UapCdcpTlv[VLAN_EVB_CDCP_TLV_LEN];/* CDCP TLV info on this UAP*/
    UINT4  u4UapIfCompId;           /* Component Id of UAP belongs to */
    UINT4  u4UapIfPort;             /* Local Port of UAP derived from VCM */
    UINT4   u4UapIfSchAdminCdcpSvidPoolLow;/* Lowest SVIDs available for CDCP 
                                             assignment*/
    UINT4   u4UapIfSchAdminCdcpSvidPoolHigh;/* Highest SVIDs available for CDCP 
                                             assignment*/
    INT4   i4UapIfStorageType;      /* Storage type . volatile/non-volatile */
    INT4   i4UapIfSchCdcpAdminEnable;     /* CDCP Admin Status */
    INT4   i4UapIfSchAdminCdcpChanCap;    /* No of channels admin configured*/
    INT4   i4UapIfCdcpOperState;          /* Running state of CDCP */
    INT4   i4UapIfSchCdcpRemoteEnabled;   /* CDCP state for remote S-channel*/
    INT4   i4UapIfRowStatus;              /* Row status */
    INT4   i4UapIfSChMode;                /* static/dynamic/hybrid */
    INT4   i4EvbSysEvbLldpTxEnable;       /* LLDP Tx Enable */
    INT4   i4UapIfSchCdcpRemoteRole;      /*  Peer Role - Station or Bridge */
    UINT2  u2UapCdcpTlvLength;            /* Valid bytes in au1UapCdcpTlv*/
    UINT1  u1EvbSysEvbLldpManual;
    UINT1  au1Reserved[1];                /* 64-bit Padding*/
    VOID (*pEvbLldpSChannelIfNotify) (UINT4, UINT4, UINT4);
}tEvbUapIfEntry;

/* S-Channel Information */
typedef struct _EvbSChIfEntry
{
    tRBNodeEmbd SChIfNode;      /* Node for EvbSChIfTree */
    UINT4       u4UapIfIndex;   /* CFA Index of UAP;Primary index of RBTree.It's
                                   introduced here to avoid padding for 64bit */
    tRBNodeEmbd SChScidNode;    /* Node for EvbSChScidTree */
    UINT4       u4SVId;         /* SVID; secondary index of EvbSChIfTree */
    UINT4       u4SChId;        /* S-Ch Id:Secondary index of EvbSChScidTree*/
    UINT4       u4SChHwIfIndex; /* HW index used for HA sync up */
    UINT4       u4SChIfPort;    /* Local Port of SBP derived from VCM */
    INT4        i4SChIfIndex;   /* CFA Index of SBP */
    INT4        i4SChIfRowStatus; /* Row status */
    INT4        i4RxFilterEntry;
    INT4        i4SChFilterStatus; /* Filter Status */
    INT4        i4NegoStatus;     /* For hybrid-free/negotiating/confirmed */
    UINT1       u1AdminStatus;    /* Admin status */
    UINT1       u1OperStatus;     /* Operational status*/
    UINT1       au1Reserved[6];   /* 64-bit Padding*/
}tEvbSChIfEntry;

#endif  /* _VLNEVTDF_H */
