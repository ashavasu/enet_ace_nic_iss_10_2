#ifndef _STDVLAWR_H
#define _STDVLAWR_H

VOID RegisterSTDVLA(VOID);

VOID UnRegisterSTDVLA(VOID);
INT4 Dot1qVlanVersionNumberGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qMaxVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qMaxSupportedVlansGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qNumVlansGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qGvrpStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qGvrpStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qGvrpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qGvrpStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexDot1qFdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFdbDynamicCountGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1qTpFdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qTpFdbPortGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qTpFdbStatusGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1qTpGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qTpGroupEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qTpGroupLearntGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1qForwardAllTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qForwardAllPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardAllStaticPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardAllForbiddenPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardAllStaticPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardAllForbiddenPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardAllStaticPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardAllForbiddenPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardAllTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1qForwardUnregisteredTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qForwardUnregisteredPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardUnregisteredStaticPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardUnregisteredForbiddenPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardUnregisteredStaticPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardUnregisteredForbiddenPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardUnregisteredStaticPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardUnregisteredForbiddenPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qForwardUnregisteredTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1qStaticUnicastTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qStaticUnicastAllowedToGoToGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticUnicastStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticUnicastAllowedToGoToSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticUnicastStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticUnicastAllowedToGoToTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticUnicastStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticUnicastTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1qStaticMulticastTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qStaticMulticastStaticEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticMulticastForbiddenEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticMulticastStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticMulticastStaticEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticMulticastForbiddenEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticMulticastStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticMulticastStaticEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticMulticastForbiddenEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticMulticastStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qStaticMulticastTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 Dot1qVlanNumDeletesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1qVlanCurrentTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qVlanFdbIdGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanCurrentEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanCurrentUntaggedPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanCreationTimeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1qVlanStaticTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qVlanStaticNameGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanForbiddenEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticUntaggedPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticNameSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanForbiddenEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticUntaggedPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanForbiddenEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticUntaggedPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qVlanStaticTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 Dot1qNextFreeLocalVlanIndexGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1qPortVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qPvidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPortAcceptableFrameTypesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPortIngressFilteringGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPortGvrpStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPortGvrpFailedRegistrationsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPortGvrpLastPduOriginGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPortRestrictedVlanRegistrationGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPvidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPortAcceptableFrameTypesSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPortIngressFilteringSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPortGvrpStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPortRestrictedVlanRegistrationSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qPvidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qPortAcceptableFrameTypesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qPortIngressFilteringTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qPortGvrpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qPortRestrictedVlanRegistrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qPortVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexDot1qPortVlanStatisticsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qTpVlanPortInFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qTpVlanPortOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qTpVlanPortInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qTpVlanPortInOverflowFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qTpVlanPortOutOverflowFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qTpVlanPortInOverflowDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1qPortVlanHCStatisticsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qTpVlanPortHCInFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qTpVlanPortHCOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qTpVlanPortHCInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1qLearningConstraintsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qConstraintTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qConstraintStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qConstraintTypeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qConstraintStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qConstraintTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qConstraintStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qLearningConstraintsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 Dot1qConstraintSetDefaultGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qConstraintTypeDefaultGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qConstraintSetDefaultSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qConstraintTypeDefaultSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qConstraintSetDefaultTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qConstraintTypeDefaultTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qConstraintSetDefaultDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qConstraintTypeDefaultDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 GetNextIndexDot1vProtocolGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1vProtocolGroupIdGet(tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolGroupRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolGroupIdSet(tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolGroupRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolGroupIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolGroupRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolGroupTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1vProtocolPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1vProtocolPortGroupVidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolPortGroupVidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolPortGroupVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1vProtocolPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


#endif /* _STDVLAWR_H */
