/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: vlpbmbsm.h,v 1.4 2013/12/18 11:59:52 siva Exp $                     */
/*                                                                           */
/*  FILE NAME             : vlpbmbsm.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 07 DEC 2006                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains VLAN MBSM Function Prototypes */
/*                                                                           */
/*****************************************************************************/

#ifndef _VLPBMBSM_H_
#define _VLPBMBSM_H_


#define MBSM_VLAN_PB    "MbsmVlanPb"

INT4 VlanMbsmUpdtPbPortPropertiesToHw (tMbsmSlotInfo * pSlotInfo,
                                       UINT2 u2PortNum);


INT4 VlanMbsmUpdtPepStatusToHw (UINT2 u2Port, tMbsmSlotInfo * pSlotInfo);

INT4 VlanMbsmUpdateCvidEntriesToHw (UINT2 u2Port, UINT4 u4SVlanId,
                                    tMbsmSlotInfo * pSlotInfo);

INT4 VlanMbsmUpdateVlanSwapTableToHw (UINT2 u2Port, tMbsmSlotInfo * pSlotInfo);

INT4 VlanAddMbsmAddEtherSwapTableToHw ( UINT2 u2Port, tMbsmSlotInfo * pSlotInfo);

INT4 VlanMbsmAddVidTransEntryToHw (UINT2 u2Port, tVlanId LocalVid, tVlanId RelayVid,
                                   tMbsmSlotInfo * pSlotInfo);

INT4 VlanMbsmUpdtMcastMacLimitToHw (tMbsmSlotInfo * pSlotInfo);

#endif
