/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanport.h,v 1.52 2015/09/25 11:48:18 siva Exp $
 *
 * Description     : This file contains util routines for VLAN objects
 *
 *******************************************************************/

/*                                                                           */
/*  FILE NAME             : vlanport.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN                                             */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains portability related constants */
/*                          and macros used in VLAN module.                  */
/*                                                                           */
/*****************************************************************************/

#ifndef _VLANPORT_H
#define _VLANPORT_H


#define   VLAN_MEMSET(inf1,data,cnt)  MEMSET(inf1,data,cnt)
#define   VLAN_MEMCPY(inf1,inf2,cnt)  MEMCPY(inf1,inf2,cnt)
#define   VLAN_MEMCMP(inf1,inf2,cnt)  MEMCMP(inf1,inf2,cnt)
 
#define VLAN_ETHERNET_INTERFACE_TYPE   6
#define VLAN_LAGG_INTERFACE_TYPE       CFA_LAGG /* 161 */
#define VLAN_MAX_MSG_LEN               1500
#define VLAN_MAX_BUCKETS               20
#define VLAN_MAX_MAC_BUCKETS           24

#define VLAN_PROTOCOL_HEADER_OFFSET    22
#define VLAN_TAGGED_PROTOCOL_HEADER_OFFSET    (VLAN_PROTOCOL_HEADER_OFFSET + VLAN_TAG_PID_LEN)

#define VLAN_MAX_ST_UCAST_ENTRIES     VLAN_DEV_MAX_ST_UCAST_ENTRIES   /* By default 1% of the L2 Table size.The required size shall be changed according to the requirement. */
#define VLAN_MAX_ST_MCAST_ENTRIES     VLAN_DEV_MAX_MCAST_TABLE_SIZE
#define VLAN_MAX_GROUP_ENTRIES        VLAN_DEV_MAX_MCAST_TABLE_SIZE

/* VLAN_MAX_ST_VLAN_ENTRIES should be always <= VLAN_MAX_CURR_ENTRIES */
#define VLAN_MAX_ST_VLAN_ENTRIES      VLAN_MAX_CURR_ENTRIES        
#define VLAN_MAX_FID_ENTRIES          VLAN_MAX_CURR_ENTRIES 

#ifdef NPAPI_WANTED
#define VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS NP_STAT_DOT1D_TP_PORT_IN_DISCARDS
#define VLAN_STAT_VLAN_PORT_IN_FRAMES NP_STAT_VLAN_PORT_IN_FRAMES
#define VLAN_STAT_VLAN_PORT_OUT_FRAMES NP_STAT_VLAN_PORT_OUT_FRAMES
#define VLAN_STAT_VLAN_PORT_IN_DISCARDS NP_STAT_VLAN_PORT_IN_DISCARDS
#define VLAN_STAT_VLAN_PORT_IN_OVERFLOW NP_STAT_VLAN_PORT_IN_OVERFLOW
#define VLAN_STAT_VLAN_PORT_OUT_OVERFLOW  NP_STAT_VLAN_PORT_OUT_OVERFLOW
#define VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW  NP_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW
/* Vlan  Stat Counters for RMON feature */
#define VLAN_STAT_VLAN_ETHER_DROP_EVENTS NP_STAT_VLAN_ETHER_DROP_EVENTS
#define VLAN_STAT_VLAN_ETHER_MCAST_PKTS NP_STAT_VLAN_ETHER_MCAST_PKTS
#define VLAN_STAT_VLAN_ETHER_BCAST_PKTS NP_STAT_VLAN_ETHER_BCAST_PKTS
#define VLAN_STAT_VLAN_ETHER_UNDERSIZE_PKTS NP_STAT_VLAN_ETHER_UNDERSIZE_PKTS
#define VLAN_STAT_VLAN_ETHER_FRAGMENTS NP_STAT_VLAN_ETHER_FRAGMENTS
#define VLAN_STAT_VLAN_ETHER_PKTS_64_OCTETS NP_STAT_VLAN_ETHER_PKTS_64_OCTETS
#define VLAN_STAT_VLAN_ETHER_PKTS_65_TO_127_OCTETS NP_STAT_VLAN_ETHER_PKTS_65_TO_127_OCTETS
#define VLAN_STAT_VLAN_ETHER_PKTS_128_TO_255_OCTETS NP_STAT_VLAN_ETHER_PKTS_128_TO_255_OCTETS
#define VLAN_STAT_VLAN_ETHER_PKTS_256_TO_511_OCTETS NP_STAT_VLAN_ETHER_PKTS_256_TO_511_OCTETS
#define VLAN_STAT_VLAN_ETHER_PKTS_512_TO_1023_OCTETS NP_STAT_VLAN_ETHER_PKTS_512_TO_1023_OCTETS
#define VLAN_STAT_VLAN_ETHER_PKTS_1024_TO_1518_OCTETS NP_STAT_VLAN_ETHER_PKTS_1024_TO_1518_OCTETS
#define VLAN_STAT_VLAN_ETHER_OVERSIZE_PKTS NP_STAT_VLAN_ETHER_OVERSIZE_PKTS
#define VLAN_STAT_VLAN_ETHER_JABBERS NP_STAT_VLAN_ETHER_JABBERS
#define VLAN_STAT_VLAN_ETHER_OCTETS NP_STAT_VLAN_ETHER_OCTETS
#define VLAN_STAT_VLAN_ETHER_PKTS NP_STAT_VLAN_ETHER_PKTS
#define VLAN_STAT_VLAN_ETHER_COLLISIONS NP_STAT_VLAN_ETHER_COLLISIONS
#define VLAN_STAT_VLAN_ETHER_CRC_ALIGN_ERRORS NP_STAT_VLAN_ETHER_CRC_ALIGN_ERRORS
#define VLAN_STAT_VLAN_ETHER_FCS_ERRORS NP_STAT_VLAN_ETHER_FCS_ERRORS

#ifndef SW_LEARNING
#define VLAN_MAX_FDB_ENTRIES 1 /* In case hw learning is enabled then there is no software entries */ 
#else
#define VLAN_MAX_FDB_ENTRIES VLAN_DEV_MAX_L2_TABLE_SIZE
#endif
#else

#define VLAN_STAT_DOT1D_TP_PORT_IN_DISCARDS   1 
#define VLAN_STAT_VLAN_PORT_IN_FRAMES         2
#define VLAN_STAT_VLAN_PORT_OUT_FRAMES        3
#define VLAN_STAT_VLAN_PORT_IN_DISCARDS       4 
#define VLAN_STAT_VLAN_PORT_IN_OVERFLOW       5 
#define VLAN_STAT_VLAN_PORT_OUT_OVERFLOW      6  
#define VLAN_STAT_VLAN_PORT_IN_DISCARDS_OVERFLOW 7 
#define VLAN_MAX_FDB_ENTRIES VLAN_DEV_MAX_L2_TABLE_SIZE

/* Vlan  Stat Counters for RMON feature */
#define VLAN_STAT_VLAN_ETHER_DROP_EVENTS                99
#define VLAN_STAT_VLAN_ETHER_MCAST_PKTS                 100
#define VLAN_STAT_VLAN_ETHER_BCAST_PKTS                 101
#define VLAN_STAT_VLAN_ETHER_UNDERSIZE_PKTS             102
#define VLAN_STAT_VLAN_ETHER_FRAGMENTS                  103
#define VLAN_STAT_VLAN_ETHER_PKTS_64_OCTETS             104
#define VLAN_STAT_VLAN_ETHER_PKTS_65_TO_127_OCTETS      105   
#define VLAN_STAT_VLAN_ETHER_PKTS_128_TO_255_OCTETS     106
#define VLAN_STAT_VLAN_ETHER_PKTS_256_TO_511_OCTETS     107
#define VLAN_STAT_VLAN_ETHER_PKTS_512_TO_1023_OCTETS    108
#define VLAN_STAT_VLAN_ETHER_PKTS_1024_TO_1518_OCTETS   109
#define VLAN_STAT_VLAN_ETHER_OVERSIZE_PKTS              110
#define VLAN_STAT_VLAN_ETHER_JABBERS                    111
#define VLAN_STAT_VLAN_ETHER_OCTETS                     112
#define VLAN_STAT_VLAN_ETHER_PKTS                       113
#define VLAN_STAT_VLAN_ETHER_COLLISIONS                 114
#define VLAN_STAT_VLAN_ETHER_CRC_ALIGN_ERRORS           115
#define VLAN_STAT_VLAN_ETHER_FCS_ERRORS                 116

#endif
      
/* TIMER Related macros */

#define VLAN_START_TIMER       VlanStartTimer 

#define VLAN_STOP_TIMER        VlanStopTimer


#define VLAN_CREATE_TMR_LIST(pu1Task, u4Event, pFn, pTmrId) \
        TmrCreateTimerList ((pu1Task), (u4Event), (pFn), (pTmrId))

#define VLAN_TMR_START(TmrListId, pTimerNode, u4Int) \
        TmrStartTimer ((TmrListId), (pTimerNode), (u4Int));

#define VLAN_TMR_STOP(TmrListId, pTimerNode) \
        TmrStopTimer ((TmrListId), (pTimerNode))

#define VLAN_DELETE_TMR_LIST(TmrListId) \
        TmrDeleteTimerList (TmrListId)

/* SEM Related Macros */

#define VLAN_CREATE_SEM(pu1Sem, u4Count, u4Flags, pSemId) \
        OsixCreateSem ((pu1Sem), (u4Count), (u4Flags), (pSemId))

#define VLAN_DELETE_SEM                 OsixSemDel
 
#define VLAN_TAKE_SEM(SemId) OsixSemTake(SemId)

#define VLAN_RELEASE_SEM(SemId) OsixSemGive(SemId)


#define VLAN_GET_TIMESTAMP(pu4Time) \
        *pu4Time = OsixGetSysUpTime () * SYS_TIME_TICKS_IN_A_SEC

#define VLAN_PRI_SCHEDULING_INT \
        (SYS_TIME_TICKS_IN_A_SEC / 100 + 1)

#define VLAN_MBSM_STAGERRING_TIME 1
 
   

#define VLAN_SPAWN_TASK                OsixTskCrt
#define VLAN_DELETE_TASK               OsixTskDel 
#define VLAN_SEND_EVENT                OsixEvtSend
#define VLAN_RECEIVE_EVENT             OsixEvtRecv

#define VLAN_GET_BUF(u1BufType, u4Size)     VlanGetBuf ((UINT1)(u1BufType), (UINT4)(u4Size))

#define VLAN_RELEASE_BUF(u1BufType, pu1Buf) VlanReleaseBuf ((UINT1)(u1BufType), (UINT1 *)(pu1Buf))

/* MEM POOL Related Macros */

#define VLAN_CREATE_MEM_POOL(u4Size, u4No, u4Type, pPoolId) \
        MemCreateMemPool ((u4Size), (u4No), (u4Type), (pPoolId))

#define VLAN_DELETE_MEM_POOL(PoolId) \
        MemDeleteMemPool ((PoolId))

#define VLAN_ALLOC_MEM_BLOCK(PoolId, pBlock) \
    (pBlock = MemAllocMemBlk (PoolId)) 

#define VLAN_RELEASE_MEM_BLOCK(PoolId, pu1Block) \
        MemReleaseMemBlock ((PoolId), (pu1Block))
   
   
#define VLAN_SEM_NODE_ID      SELF
#define VLAN_SEM_NAME                ((const UINT1 *)"VSM1")
#ifdef SW_LEARNING
#define VLAN_SHADOW_TBL_SEM_NODE_ID      SELF
#define VLAN_SHADOW_TBL_SEM_NAME     ((const UINT1 *)"VSM2")
#endif
    

   /* BUFFER Related constants and macros */
   
#define VLAN_ST_UCAST_ENTRY    1
#define VLAN_FDB_ENTRY         2
#define VLAN_ST_MCAST_ENTRY    3
#define VLAN_GROUP_ENTRY       4
    
#define VLAN_PORT_STATS_ENTRY  5

#define VLAN_CURR_ENTRY        6
#define VLAN_ST_VLAN_ENTRY     7
#define VLAN_MAC_MAP_ENTRY     8
#define VLAN_MESSAGE_BUFFER    9
#define VLAN_PROTO_GROUP_ENTRY 10
#define VLAN_PORT_PROTO_ENTRY  11
#define VLAN_QMSG_BUFF          12
#define VLAN_RED_GVRP_LEARNT_BUFF 13
#define VLAN_RED_GMRP_LEARNT_BUFF 14
#define VLAN_RED_GVRP_DEL_BUFF    15
#define VLAN_RED_GMRP_DEL_BUFF    16
#define VLAN_PORT_ENTRY           17
#define VLAN_CONTEXT_INFO         18
#define VLAN_FID_ENTRY            19
#define VLAN_TEMP_PORTLIST_ENTRY  20
#define VLAN_MISC                 21
#define VLAN_STATS_ENTRY  22
#define VLAN_MAC_CONTROL_ENTRY 23
#define VLAN_FDB_INFO             24
#define VLAN_Q_IN_PKT_BUFF        25
#define VLAN_WILD_CARD_ENTRY      26
#define VLAN_SUBNET_MAP_ENTRY     27
#define VLAN_BASE_PORT_ENTRY      28
#define VLAN_PORT_VLAN_MAP_ENTRY  29
#define VLAN_HW_STATS_ENTRY       30
#define VLAN_RED_CACHE_ENTRY      31
#define VLAN_RED_ST_UCAST_CACHE_ENTRY      32
#define VLAN_RED_MCAST_CACHE_ENTRY      33
#define VLAN_RED_PROTO_VLAN_CACHE_ENTRY      34
#define VLAN_MCAG_QMSG_BUFF 35
#define VLAN_MCAG_MAC_AGING_ENTRIES 36


#define VLAN_COPY_FROM_BUF(pBuf, pu1Dst, u4Offset, u4Size) \
        CRU_BUF_Copy_FromBufChain ((pBuf), (UINT1 *)(pu1Dst), u4Offset, u4Size)

#define VLAN_COPY_TO_BUF(pBuf, pu1Src, u4Offset, u4Size) \
        CRU_BUF_Copy_OverBufChain ((pBuf), (UINT1 *)(pu1Src), u4Offset, u4Size)

#define VLAN_PREPEND_BUF(pChainDesc, pu1Src, u4Size) \
        CRU_BUF_Prepend_BufChain ((pChainDesc), (UINT1 *)(pu1Src), (u4Size))

#define VLAN_GET_BUF_LEN(pBuf) \
        CRU_BUF_Get_ChainValidByteCount(pBuf)  

#define VLAN_MOVE_OFFSET(pFrame, u4Offset) \
        CRU_BUF_Move_ValidOffset (pFrame, u4Offset)
   
#define VLAN_ALLOCATE_CRU_BUF(u4Size, u4Offset) \
        CRU_BUF_Allocate_MsgBufChain (u4Size, u4Offset)

#define VLAN_RELEASE_CRU_BUF(pBuf) \
        CRU_BUF_Release_MsgBufChain ((pBuf), 0)
   
#define VLAN_GET_MODULE_DATA_PTR(pBuf) CRU_BUF_Get_ModuleData (pBuf) 
   
#define VLAN_LINK_BUFFERS(pBuf1, pBuf2) \
        CRU_BUF_Link_BufChains (pBuf1, pBuf2) 

#define VLAN_UNLINK_BUFFERS(pBuf) \
        CRU_BUF_UnLink_BufChain(pBuf) 
   
#define VLAN_BUF_IF_LINEAR(pBuf, u4OffSet, u4Size) \
        CRU_BUF_Get_DataPtr_IfLinear ((tCRU_BUF_CHAIN_HEADER *)(pBuf), \
                                      (u4OffSet), (u4Size))
/* Porting macros to fsap2 */
#define tVLAN_HASH_NODE  tTMO_HASH_NODE  
#define VLAN_HASH_CREATE_TABLE TMO_HASH_Create_Table  
#define VLAN_HASH_DELETE_TABLE TMO_HASH_Delete_Table 
#define VLAN_HASH_ADD_NODE     TMO_HASH_Add_Node 
#define VLAN_HASH_SCAN_TABLE   TMO_HASH_Scan_Table
#define VLAN_HASH_DELETE_NODE  TMO_HASH_Delete_Node
#define VLAN_HASH_GET_FIRST_NODE  TMO_HASH_Get_First_Bucket_Node
#define VLAN_HASH_SCAN_BUCKET TMO_HASH_Scan_Bucket 
#define VLAN_SLL_ADD TMO_SLL_Add 
#define VLAN_SLL_DEL TMO_SLL_Delete 
#define VLAN_SLL_INIT TMO_SLL_Init    
#define VLAN_SLL_INIT_NODE TMO_SLL_Init_Node    
#define VLAN_SLL_SCAN TMO_SLL_Scan 
#define VLAN_SLL_COUNT TMO_SLL_Count
#define VLAN_SLL_GET   TMO_SLL_Get
#define VLAN_SLL_FIRST TMO_SLL_First
#define VLAN_SLL_NEXT  TMO_SLL_Next
#define tVLAN_SLL  tTMO_SLL 
#define tVLAN_SLL_NODE tTMO_SLL_NODE 
#define tVLAN_HASH_TABLE tTMO_HASH_TABLE             

#define VLAN_HASH_DYN_Scan_Bucket(pHashTab, u2HashKey, pNode, \
                pTempNode, TypeCast)\
                VLAN_SLL_DYN_Scan (&((pHashTab)->HashList[u2HashKey]), \
                                   pNode, pTempNode, TypeCast)

#define VLAN_SLL_DYN_Scan TMO_DYN_SLL_Scan

#define VLAN_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)
#define VLAN_SEND_TO_QUEUE               OsixQueSend
#define VLAN_RECV_FROM_QUEUE             OsixQueRecv
#define VLAN_CREATE_QUEUE                OsixQueCrt
#define VLAN_DELETE_QUEUE                OsixQueDel
#define VLAN_GET_TASK_ID                 OsixGetTaskId 
#define VLAN_SELF                        SELF

#define VLAN_TASK_PRIORITY   30
#define VLAN_TASK            ((const UINT1*)"VLAN")
    
#define VLAN_TASK_QUEUE      ((UINT1*)"VLAQ")
#define VLAN_CFG_QUEUE       ((UINT1*)"VLCQ")

#define VLAN_Q_DEPTH         500
#define VLAN_TASK_QUEUE_ID   gVlanTaskInfo.VlanQId
#define VLAN_CFG_QUEUE_ID    gVlanTaskInfo.VlanCfgQId
#ifdef SW_LEARNING
#define VLAN_ST_LOCK    VlanShadowTblLock
#define VLAN_ST_UNLOCK  VlanShadowTblUnLock
#endif

#define VLAN_TIMER_EXP_EVENT    0x01 
#define VLAN_MSG_ENQ_EVENT      0x02
#define VLAN_CFG_MSG_EVENT      0x04
#define VLAN_RED_BULK_UPD_EVENT 0x08
#define VLAN_MSR_COMPLETE_EVENT 0x10

#define VLAN_L2IWF_GETNEXT_VALID_PORT  VlanL2IwfGetNextValidPortForContext

#define VLAN_MAC_AGING_EVENT    0x12
#define VLAN_MAC_AGING_TIMER_EXP_EVENT 0x64


        
/* Transit time handling */
#define   MAX_VLAN_TRANSIT_DELAY      1
#define   VLAN_TRANSIT_DELAY_TICKS    MAX_VLAN_TRANSIT_DELAY *     \
                                      SYS_TIME_TICKS_IN_A_SEC

#endif
