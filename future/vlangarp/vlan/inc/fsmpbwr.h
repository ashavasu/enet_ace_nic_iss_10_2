/* $Id: fsmpbwr.h,v 1.9 2016/01/11 12:56:20 siva Exp $ */
#ifndef _FSMPBWR_H
#define _FSMPBWR_H
INT4 GetNextIndexFsMIPbContextInfoTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMPB(VOID);

VOID UnRegisterFSMPB(VOID);
INT4 FsMIPbMulticastMacLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelStpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelLacpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelDot1xAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelGvrpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelGmrpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbMulticastMacLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelStpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelLacpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelDot1xAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelGvrpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelGmrpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbMulticastMacLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelStpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelLacpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelDot1xAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelGvrpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelGmrpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbContextInfoTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFsMIPbPortInfoTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbPortSVlanClassificationMethodGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanIngressEtherTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanEgressEtherTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanEtherTypeSwapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanTranslationStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortUnicastMacLearningGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortUnicastMacLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortBundleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortMultiplexStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanClassificationMethodSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanIngressEtherTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanEgressEtherTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanEtherTypeSwapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanTranslationStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortUnicastMacLearningSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortUnicastMacLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortBundleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortMultiplexStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanClassificationMethodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanIngressEtherTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanEgressEtherTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanEtherTypeSwapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortSVlanTranslationStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortUnicastMacLearningTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortUnicastMacLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortBundleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortMultiplexStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortInfoTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 GetNextIndexFsMIPbSrcMacSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbSrcMacSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcMacRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcMacSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcMacRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcMacSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcMacRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcMacSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbDstMacSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbDstMacSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstMacRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstMacSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstMacRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstMacSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstMacRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstMacSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbCVlanSrcMacSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbCVlanSrcMacSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanSrcMacRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanSrcMacSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanSrcMacRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanSrcMacSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanSrcMacRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanSrcMacSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbCVlanDstMacSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbCVlanDstMacSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstMacRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstMacSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstMacRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstMacSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstMacRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstMacSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbDscpSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbDscpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDscpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDscpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDscpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDscpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbDscpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbDscpSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbCVlanDscpSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbCVlanDscpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDscpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDscpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDscpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDscpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDscpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDscpSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbSrcIpAddrSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbSrcIpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcIpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcIpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcIpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcIpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcIpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcIpAddrSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbDstIpAddrSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbDstIpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstIpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstIpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstIpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstIpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstIpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbDstIpAddrSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbSrcDstIpSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbSrcDstIpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcDstIpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcDstIpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcDstIpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcDstIpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcDstIpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbSrcDstIpSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbCVlanDstIpSVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbCVlanDstIpSVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstIpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstIpSVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstIpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstIpSVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstIpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbCVlanDstIpSVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbPortBasedCVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbPortCVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanClassifyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortEgressUntaggedStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanClassifyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortEgressUntaggedStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanClassifyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortEgressUntaggedStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortBasedCVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbEtherTypeSwapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbRelayEtherTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbEtherTypeSwapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRelayEtherTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbEtherTypeSwapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbRelayEtherTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbEtherTypeSwapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbEtherTypeSwapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsMIPbSVlanConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbSVlanConfigServiceTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSVlanConfigServiceTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbSVlanConfigServiceTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbSVlanConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIPbTunnelProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbTunnelProtocolDot1xGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolLacpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolStpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolGvrpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolGmrpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolIgmpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolDot1xSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolLacpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolStpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolGvrpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolGmrpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolIgmpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolDot1xTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolLacpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolStpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolGvrpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolGmrpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolIgmpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFsMIPbTunnelProtocolStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbTunnelProtocolDot1xPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolDot1xPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolLacpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolLacpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolStpPDUsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolStpPDUsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolGvrpPDUsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolGvrpPDUsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolGmrpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolGmrpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolIgmpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbTunnelProtocolIgmpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIPbPepExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbPepExtCosPreservationGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPepExtCosPreservationSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPepExtCosPreservationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPepExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIPbPortCVlanCounterTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIPbPortCVlanPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanCounterRxUcastGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanCounterRxFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanCounterRxBytesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanCounterStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanClearCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanCounterStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanClearCounterSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanCounterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanClearCounterTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPbPortCVlanCounterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif /* _FSMPBWR_H */
