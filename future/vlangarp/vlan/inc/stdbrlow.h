/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbrlow.h,v 1.8 2012/05/31 12:45:36 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dDeviceCapabilities ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1dTrafficClassesEnabled ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dTrafficClassesEnabled ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dTrafficClassesEnabled ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1dTrafficClassesEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1dPortCapabilitiesTable. */
INT1
nmhValidateIndexInstanceDot1dPortCapabilitiesTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dPortCapabilitiesTable  */

INT1
nmhGetFirstIndexDot1dPortCapabilitiesTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dPortCapabilitiesTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dPortCapabilities ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Type for Low Level GET FIRST fn for Dot1dPortPriorityTable  */

INT1
nmhGetFirstIndexDot1dPortPriorityTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dPortPriorityTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dPortDefaultUserPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dPortNumTrafficClasses ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dPortDefaultUserPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1dPortNumTrafficClasses ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dPortDefaultUserPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1dPortNumTrafficClasses ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1dPortPriorityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1dUserPriorityRegenTable. */
INT1
nmhValidateIndexInstanceDot1dUserPriorityRegenTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dUserPriorityRegenTable  */

INT1
nmhGetFirstIndexDot1dUserPriorityRegenTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dUserPriorityRegenTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dRegenUserPriority ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dRegenUserPriority ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dRegenUserPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1dUserPriorityRegenTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1dTrafficClassTable. */
INT1
nmhValidateIndexInstanceDot1dTrafficClassTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dTrafficClassTable  */

INT1
nmhGetFirstIndexDot1dTrafficClassTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dTrafficClassTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dTrafficClass ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dTrafficClass ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dTrafficClass ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1dTrafficClassTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1dPortOutboundAccessPriorityTable. */
INT1
nmhValidateIndexInstanceDot1dPortOutboundAccessPriorityTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dPortOutboundAccessPriorityTable  */

INT1
nmhGetFirstIndexDot1dPortOutboundAccessPriorityTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dPortOutboundAccessPriorityTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dPortOutboundAccessPriority ARG_LIST((INT4  , INT4 ,INT4 *));


/* Proto Validate Index Instance for Dot1dTpHCPortTable. */
INT1
nmhValidateIndexInstanceDot1dTpHCPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dTpHCPortTable  */

INT1
nmhGetFirstIndexDot1dTpHCPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dTpHCPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dTpHCPortInFrames ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot1dTpHCPortOutFrames ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot1dTpHCPortInDiscards ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for Dot1dTpPortOverflowTable. */
INT1
nmhValidateIndexInstanceDot1dTpPortOverflowTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dTpPortOverflowTable  */

INT1
nmhGetFirstIndexDot1dTpPortOverflowTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dTpPortOverflowTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dTpPortInOverflowFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1dTpPortOutOverflowFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1dTpPortInOverflowDiscards ARG_LIST((INT4 ,UINT4 *));
