/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpblw.h,v 1.1 2011/06/17 05:40:11 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Ieee8021PbVidTranslationTable. */
INT1
nmhValidateIndexInstanceIeee8021PbVidTranslationTable ARG_LIST((UINT4  , UINT4  , INT4 ));


/* Proto Type for Low Level GET FIRST fn for Ieee8021PbVidTranslationTable  */

INT1
nmhGetFirstIndexIeee8021PbVidTranslationTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbVidTranslationTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbVidTranslationRelayVid ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021PbVidTranslationRowStatus ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbVidTranslationRelayVid ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021PbVidTranslationRowStatus ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbVidTranslationRelayVid ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbVidTranslationRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbVidTranslationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021PbCVidRegistrationTable. */
INT1
nmhValidateIndexInstanceIeee8021PbCVidRegistrationTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbCVidRegistrationTable  */

INT1
nmhGetFirstIndexIeee8021PbCVidRegistrationTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbCVidRegistrationTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbCVidRegistrationSVid ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021PbCVidRegistrationUntaggedPep ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021PbCVidRegistrationUntaggedCep ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021PbCVidRegistrationRowStatus ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbCVidRegistrationSVid ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021PbCVidRegistrationUntaggedPep ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021PbCVidRegistrationUntaggedCep ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021PbCVidRegistrationRowStatus ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbCVidRegistrationSVid ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbCVidRegistrationUntaggedPep ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbCVidRegistrationUntaggedCep ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbCVidRegistrationRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbCVidRegistrationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021PbEdgePortTable. */
INT1
nmhValidateIndexInstanceIeee8021PbEdgePortTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbEdgePortTable  */

INT1
nmhGetFirstIndexIeee8021PbEdgePortTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbEdgePortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbEdgePortPVID ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021PbEdgePortDefaultUserPriority ARG_LIST((UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetIeee8021PbEdgePortAcceptableFrameTypes ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021PbEdgePortEnableIngressFiltering ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbEdgePortPVID ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021PbEdgePortDefaultUserPriority ARG_LIST((UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhSetIeee8021PbEdgePortAcceptableFrameTypes ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021PbEdgePortEnableIngressFiltering ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbEdgePortPVID ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbEdgePortDefaultUserPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021PbEdgePortAcceptableFrameTypes ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbEdgePortEnableIngressFiltering ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbEdgePortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021PbServicePriorityRegenerationTable. */
INT1
nmhValidateIndexInstanceIeee8021PbServicePriorityRegenerationTable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbServicePriorityRegenerationTable  */

INT1
nmhGetFirstIndexIeee8021PbServicePriorityRegenerationTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbServicePriorityRegenerationTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbServicePriorityRegenerationRegeneratedPriority ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbServicePriorityRegenerationRegeneratedPriority ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbServicePriorityRegenerationRegeneratedPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbServicePriorityRegenerationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021PbCnpTable. */
INT1
nmhValidateIndexInstanceIeee8021PbCnpTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbCnpTable  */

INT1
nmhGetFirstIndexIeee8021PbCnpTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbCnpTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbCnpCComponentId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021PbCnpSVid ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021PbCnpRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbCnpCComponentId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021PbCnpSVid ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021PbCnpRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbCnpCComponentId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021PbCnpSVid ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbCnpRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbCnpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021PbPnpTable. */
INT1
nmhValidateIndexInstanceIeee8021PbPnpTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbPnpTable  */

INT1
nmhGetFirstIndexIeee8021PbPnpTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbPnpTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbPnpRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbPnpRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbPnpRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbPnpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021PbCepTable. */
INT1
nmhValidateIndexInstanceIeee8021PbCepTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbCepTable  */

INT1
nmhGetFirstIndexIeee8021PbCepTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbCepTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbCepCComponentId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021PbCepCepPortNumber ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021PbCepRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbCepRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbCepRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbCepTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
