/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevprt.h,v 1.9 2016/07/16 11:15:03 siva Exp $
 *
 * Description: This file contains VLAN EVB prototypes.           
 *
 *******************************************************************/
#ifndef _VLNEVPRT_H
#define _VLNEVPRT_H
/* vlnevuap.c - prototypes start */
INT4
VlanEvbUapIfTblCreate(VOID);
VOID
VlanEvbUapIfTblDelete(VOID);
INT4
VlanEvbUapIfTblCmp(tRBElem * Node,
                   tRBElem * NodeIn);
INT4
VlanEvbUapIfAddEntry (UINT4, tEvbUapIfEntry **);
tEvbUapIfEntry *
VlanEvbUapIfGetEntry(UINT4);
tEvbUapIfEntry *
VlanEvbUapIfGetFirstEntry (VOID);
tEvbUapIfEntry *
VlanEvbUapIfGetNextEntry (UINT4);
INT4
VlanEvbUapIfDelEntry (UINT4);
VOID
VlanEvbUapIfDelAllEntries (VOID);
INT4
VlanEvbUapGetOperStatus (UINT4, UINT1 *);
INT4
VlanEvbUapSetOperStatusUp (tEvbUapIfEntry *pUapIfEntry);
INT4
VlanEvbUapSetOperStatusDown (tEvbUapIfEntry *pUapIfEntry);
INT4
VlanEvbUapSetOperStatus (UINT4, UINT1);
INT4
VlanEvbUapEnableCdcpStatus (tEvbUapIfEntry *pUapIfEntry);
INT4
VlanEvbUapDisableCdcpStatus (tEvbUapIfEntry *pUapIfEntry);
INT4
VlanEvbUapSetCdcpStatus (UINT4, INT4);
INT4
VlanEvbUapSetCdcpChanCap (UINT4, UINT4);
VOID
VlanEvbUapIfSetDefProperties (tVlanPortEntry *);
INT4
VlanEvbSbpCreateIndication (tEvbUapIfEntry *pUapIfEntry);
INT4
VlanEvbSbpDeleteIndication (tEvbUapIfEntry *pUapIfEntry);
INT4
VlanEvbCdcpPostCfgMessage (UINT2 u2MsgType, UINT4 u4Port);
INT4
VlanEvbCdcpProcessPacket (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4InPort);
INT4
VlanEvbUapIfWrAddEntry (UINT4 u4UapIfIndex);
/* vlnevuap.c - prototypes end */

/* vlnevsch.c - prototypes start */
INT4
VlanEvbSChIfTblCreate (VOID);
VOID
VlanEvbSChIfTblDelete (VOID);
INT4
VlanEvbSChIfTblCmp (tRBElem *pNode,
                    tRBElem *pNodeIn);
INT4
VlanEvbSChScidIfTblCmp (tRBElem * Node,
                        tRBElem * NodeIn);
INT4
VlanEvbSChIfAddEntry (UINT4 u4UapIfIndex, UINT4 u4SVId, UINT1 u1Mode,
                          UINT4 u4SChId, tEvbSChIfEntry **ppEvbSChIfEntry);
tEvbSChIfEntry *
VlanEvbSChIfGetEntry (UINT4 u4UapIfIndex, UINT4 u4SVId);
tEvbSChIfEntry *
VlanEvbSChScidIfGetEntry (UINT4 u4UapIfIndex, UINT4 u4SChId);
tEvbSChIfEntry  *
VlanEvbSChIfGetFirstEntry (VOID);
tEvbSChIfEntry  *
VlanEvbSChScidIfGetFirstEntry (VOID);
tEvbSChIfEntry *
VlanEvbSChIfGetNextEntry (UINT4 u4UapIfIndex, UINT4 u4SVId);
tEvbSChIfEntry *
VlanEvbSChScidIfGetNextEntry (UINT4 u4UapIfIndex, UINT4 u4SChId);
INT4
VlanEvbSChIfDelEntry (UINT4 u4UapIfIndex, UINT4 u4SVId, UINT4 u4SChId);
VOID
VlanEvbSChIfDelAllEntries (VOID);
VOID
VlanEvbSChIfDelAllEntriesOnUap (UINT4 u4UapIfIndex);
UINT4
VlanEvbGetFreeSChIfIndex (UINT4 u4UapIfIndex);
UINT4
VlanEvbSChGetNextSChannelId (UINT4 u4UapIfIndex);
UINT1
VlanEvbIsSChannelIdAvailable (tEvbUapIfEntry *pUapIfEntry, UINT4 u4SVId, 
                              UINT4 u4SChId);
INT4
VlanEvbSChIfUpdateLocalPort (UINT4 u4SChIfIndex);
INT4
VlanEvbSChSetStaticEntriesOperUp (tEvbUapIfEntry *pUapIfEntry);
INT4
VlanEvbSChSetOperUp (tEvbSChIfEntry *pSChIfEntry);
INT4
VlanEvbSChSetStaticEntriesOperDown (tEvbUapIfEntry *pUapIfEntry, UINT1 u1Ageout);
INT4
VlanEvbSChSetOperDown (tEvbSChIfEntry *pSChIfEntry);
INT4
VlanEvbSChSetStaticEntriesOperStatus (tEvbUapIfEntry *pUapIfEntry,
                                      UINT1 u1OperStatus);
INT4
VlanEvbSChSetDynamicEntryOperStatus (tEvbSChIfEntry *pSChIfEntry,
                                     UINT1 u1OperStatus);
INT4
VlanEvbSChSetAllEntriesOperStatus (tEvbUapIfEntry *pUapIfEntry,
                                   UINT1 u1OperStatus);
INT4
VlanEvbSChSetAdminStatus (UINT4 u4SChIfIndex, UINT1 u1Status);
INT4
VlanEvbHwConfigSChInterface (tEvbSChIfEntry *pSChIfEntry, UINT1 u1OpCode);

INT4
VlanEvbGetFreeDynSChIndexForUap (UINT4 u4UapIfIndex, INT4 *pi4Index);

INT4
VlanEvbSchIfDelDynamicEntry (UINT4 u4SbpIfIndex);
INT4
VlanEvbSChHwIndication (tEvbSChIfEntry *pSChIfEntry, UINT1 u1OpCode);
VOID
VlanEvbSChUpdateCfaOperStatus (tEvbUapIfEntry *pUapIfEntry,
                                              UINT1 u1OperStatus);
/* vlnevsch.c - prototypes end */

/* vlnevini.c - prototypes start */
INT4
VlanEvbStart (UINT4 u4ContextId);
VOID
VlanEvbShutdown (UINT4 u4ContextId);  
VOID
VlanEvbEnable (UINT4 u4ContextId);
VOID
VlanEvbDisable (UINT4 u4ContextId);
VOID
VlanEvbClearStats (UINT4 u4ContextId);
UINT1
VlanEvbGetSystemStatus (UINT4 u4ContextId);
VOID
VlanEvbPostCdcpMsg (tLldpAppTlv *pLldpAppTlv);
VOID
VlanEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex, tVlanEvbSbpArray  *pSbpArray);
INT4
VlanGetSChIfIndex (UINT4 u4UapIfIndex, UINT4 u4SVID, UINT4 *pu4SChIfIndex);
INT4
VlanGetSChInfoFromSChIfIndex (UINT4 u4SChIfIndex, UINT4 *pu4UapIfIndex,
                              UINT2 *pu2SVID);
/* vlnevini.c - prototypes end */

/* vlnevcdc.c - prototypes start */
INT4
VlanEvbCdcpPortReg (tEvbUapIfEntry * pEvbUapIfEntry);
INT4
VlanEvbCdcpPortDeReg (tEvbUapIfEntry * pEvbUapIfEntry);
INT4
VlanEvbCdcpPortUpdate (tEvbUapIfEntry * pEvbUapIfEntry);
INT4
VlanEvbCdcpProcessTlv (tEvbQueMsg * pMsg);
VOID
VlanEvbCdcpProcessHybridPairs (UINT1 *pu1RxBuf, tEvbUapIfEntry *pUapIfEntry,
                               UINT2 u2Length);
VOID
VlanEvbCdcpProcessDynamicPairs (UINT1 *pu1RxBuf, tEvbUapIfEntry *pUapIfEntry,
                               UINT2 u2Length);
VOID
VlanEvbCdcpDeleteHybridPairs (tEvbUapIfEntry *pUapIfEntry, UINT2 u2NewTxLen);
VOID
VlanEvbCdcpDeleteDynamicPairs (tEvbUapIfEntry *pUapIfEntry, UINT2 u2NewTxLen);
VOID
VlanEvbCdcpConstructTlv (tEvbUapIfEntry * pEvbUapIfEntry);
INT4
VlanEvbCdcpPortReReg (VOID);

INT4
VlanEvbPortPostMsgToCdcp (tEvbUapIfEntry *pEvbUapIfEntry, UINT1 u1MsgType);
INT4
VlanEvbCdcpUtlTlvStatusChange (tEvbUapIfEntry * pEvbUapIfEntry,
                               INT4 i4NewTlvStatus);
INT4
VlanEvbCdcpProcessAgeout (UINT4 u4UapIfIndex);
/* vlnevcdc.c - prototypes end*/

/* vlnevtrp.c - prototypes start */
INT4
VlanEvbSendSChannelStatusTrap (tEvbSChIfEntry * pSchIfEntry);

INT4
VlanEvbSendSChannelTrap (UINT1 u1TrapId, UINT4 u4UapIfIndex, UINT4 u4SVId);

tSNMP_OID_TYPE     *
VlanEvbCfaMakeObjIdFromDotNew (UINT1 *textStr);

tSNMP_OID_TYPE     *
VlanEvbMakeObjIdFromDotNew (INT1 *pi1TextStr);

/* vlnevtrp.c - prototypes end */

/* vlnevred.c - prototypes start */
INT4
VlanEvbRedProcessCdcpTlvSyncUp (tRmMsg * pRmMsg, UINT2 *pu2Offset);
INT4
VlanEvbRedProcessRemoteRoleSyncUp (tRmMsg * pRmMsg, UINT2 *pu2Offset);
INT4
VlanEvbRedProcessSbpSyncUp (tRmMsg * pRmMsg,  UINT1 u1MsgType, UINT2 *pu2Offset);
INT4 VlanEvbRedSendDynamicUpdates ( UINT1 u1MsgType, VOID *pSyncUpMsg);
VOID
VlanEvbRedGetMsgLen (UINT1 u1MsgType, UINT2 *pu2MsgLen);
VOID
VlanEvbRedConstructSbpSyncUp (tRmMsg * pRmMsgBuf, tEvbSChIfEntry * pSchPortInfo,
                             UINT2 *pu2Offset);
VOID
VlanEvbRedConstructCdcpTlvSyncUp (tRmMsg * pRmMsgBuf, tEvbUapIfEntry * pUapPortInfo,
                             UINT2 *pu2Offset);
VOID
VlanEvbRedConstructRemoteRoleSyncUp (tRmMsg * pRmMsgBuf, tEvbUapIfEntry * pUapPortInfo,
                             UINT2 *pu2Offset);
VOID
VlanEvbRedSendSbpBulkSyncup (VOID);
VOID
VlanEvbRedSendRemoteRoleBulkSyncup (VOID);
VOID
VlanEvbRedSendCdcpTlvBulkSyncup (VOID);
/* vlnevred.c - prototypes end*/

#endif  /* _VLNEVPRT_H */


