/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1d1db.h,v 1.3 2015/09/13 09:17:33 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STD1D1DB_H
#define _STD1D1DB_H

UINT1 Ieee8021BridgeBaseTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgeBasePortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgeBaseIfToPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Ieee8021BridgePhyPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT1 Ieee8021BridgeTpPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgePortPriorityTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgeUserPriorityRegenTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgeTrafficClassTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgePortOutboundAccessPriorityTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgePortDecodingTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Ieee8021BridgePortEncodingTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Ieee8021BridgeServiceAccessPriorityTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgeILanIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Ieee8021BridgeDot1dPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 std1d1 [] ={1,3,111,2,802,1,1,2};
tSNMP_OID_TYPE std1d1OID = {8, std1d1};


/* Generated OID's for tables */
UINT4 Ieee8021BridgeBaseTable [] ={1,3,111,2,802,1,1,2,1,1,1};
tSNMP_OID_TYPE Ieee8021BridgeBaseTableOID = {11, Ieee8021BridgeBaseTable};


UINT4 Ieee8021BridgeBasePortTable [] ={1,3,111,2,802,1,1,2,1,1,4};
tSNMP_OID_TYPE Ieee8021BridgeBasePortTableOID = {11, Ieee8021BridgeBasePortTable};


UINT4 Ieee8021BridgeTpPortTable [] ={1,3,111,2,802,1,1,2,1,2,1};
tSNMP_OID_TYPE Ieee8021BridgeTpPortTableOID = {11, Ieee8021BridgeTpPortTable};


UINT4 Ieee8021BridgePortPriorityTable [] ={1,3,111,2,802,1,1,2,1,3,1};
tSNMP_OID_TYPE Ieee8021BridgePortPriorityTableOID = {11, Ieee8021BridgePortPriorityTable};


UINT4 Ieee8021BridgeUserPriorityRegenTable [] ={1,3,111,2,802,1,1,2,1,3,2};
tSNMP_OID_TYPE Ieee8021BridgeUserPriorityRegenTableOID = {11, Ieee8021BridgeUserPriorityRegenTable};


UINT4 Ieee8021BridgeTrafficClassTable [] ={1,3,111,2,802,1,1,2,1,3,3};
tSNMP_OID_TYPE Ieee8021BridgeTrafficClassTableOID = {11, Ieee8021BridgeTrafficClassTable};


UINT4 Ieee8021BridgePortOutboundAccessPriorityTable [] ={1,3,111,2,802,1,1,2,1,3,4};
tSNMP_OID_TYPE Ieee8021BridgePortOutboundAccessPriorityTableOID = {11, Ieee8021BridgePortOutboundAccessPriorityTable};


UINT4 Ieee8021BridgePortDecodingTable [] ={1,3,111,2,802,1,1,2,1,3,5};
tSNMP_OID_TYPE Ieee8021BridgePortDecodingTableOID = {11, Ieee8021BridgePortDecodingTable};


UINT4 Ieee8021BridgePortEncodingTable [] ={1,3,111,2,802,1,1,2,1,3,6};
tSNMP_OID_TYPE Ieee8021BridgePortEncodingTableOID = {11, Ieee8021BridgePortEncodingTable};


UINT4 Ieee8021BridgeServiceAccessPriorityTable [] ={1,3,111,2,802,1,1,2,1,3,7};
tSNMP_OID_TYPE Ieee8021BridgeServiceAccessPriorityTableOID = {11, Ieee8021BridgeServiceAccessPriorityTable};


UINT4 Ieee8021BridgeILanIfTable [] ={1,3,111,2,802,1,1,2,1,6,1};
tSNMP_OID_TYPE Ieee8021BridgeILanIfTableOID = {11, Ieee8021BridgeILanIfTable};


UINT4 Ieee8021BridgeDot1dPortTable [] ={1,3,111,2,802,1,1,2,1,7,1};
tSNMP_OID_TYPE Ieee8021BridgeDot1dPortTableOID = {11, Ieee8021BridgeDot1dPortTable};




UINT4 Ieee8021BridgeBaseComponentId [ ] ={1,3,111,2,802,1,1,2,1,1,1,1,1};
UINT4 Ieee8021BridgeBaseBridgeAddress [ ] ={1,3,111,2,802,1,1,2,1,1,1,1,2};
UINT4 Ieee8021BridgeBaseNumPorts [ ] ={1,3,111,2,802,1,1,2,1,1,1,1,3};
UINT4 Ieee8021BridgeBaseComponentType [ ] ={1,3,111,2,802,1,1,2,1,1,1,1,4};
UINT4 Ieee8021BridgeBaseDeviceCapabilities [ ] ={1,3,111,2,802,1,1,2,1,1,1,1,5};
UINT4 Ieee8021BridgeBaseTrafficClassesEnabled [ ] ={1,3,111,2,802,1,1,2,1,1,1,1,6};
UINT4 Ieee8021BridgeBaseMmrpEnabledStatus [ ] ={1,3,111,2,802,1,1,2,1,1,1,1,7};
UINT4 Ieee8021BridgeBaseRowStatus [ ] ={1,3,111,2,802,1,1,2,1,1,1,1,8};
UINT4 Ieee8021BridgeBasePortComponentId [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,1};
UINT4 Ieee8021BridgeBasePort [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,2};
UINT4 Ieee8021BridgeBasePortIfIndex [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,3};
UINT4 Ieee8021BridgeBasePortDelayExceededDiscards [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,4};
UINT4 Ieee8021BridgeBasePortMtuExceededDiscards [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,5};
UINT4 Ieee8021BridgeBasePortCapabilities [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,6};
UINT4 Ieee8021BridgeBasePortTypeCapabilities [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,7};
UINT4 Ieee8021BridgeBasePortType [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,8};
UINT4 Ieee8021BridgeBasePortExternal [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,9};
UINT4 Ieee8021BridgeBasePortAdminPointToPoint [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,10};
UINT4 Ieee8021BridgeBasePortOperPointToPoint [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,11};
UINT4 Ieee8021BridgeBasePortName [ ] ={1,3,111,2,802,1,1,2,1,1,4,1,12};
UINT4 Ieee8021BridgeTpPortComponentId [ ] ={1,3,111,2,802,1,1,2,1,2,1,1,1};
UINT4 Ieee8021BridgeTpPort [ ] ={1,3,111,2,802,1,1,2,1,2,1,1,2};
UINT4 Ieee8021BridgeTpPortMaxInfo [ ] ={1,3,111,2,802,1,1,2,1,2,1,1,3};
UINT4 Ieee8021BridgeTpPortInFrames [ ] ={1,3,111,2,802,1,1,2,1,2,1,1,4};
UINT4 Ieee8021BridgeTpPortOutFrames [ ] ={1,3,111,2,802,1,1,2,1,2,1,1,5};
UINT4 Ieee8021BridgeTpPortInDiscards [ ] ={1,3,111,2,802,1,1,2,1,2,1,1,6};
UINT4 Ieee8021BridgePortDefaultUserPriority [ ] ={1,3,111,2,802,1,1,2,1,3,1,1,1};
UINT4 Ieee8021BridgePortNumTrafficClasses [ ] ={1,3,111,2,802,1,1,2,1,3,1,1,2};
UINT4 Ieee8021BridgePortPriorityCodePointSelection [ ] ={1,3,111,2,802,1,1,2,1,3,1,1,3};
UINT4 Ieee8021BridgePortUseDEI [ ] ={1,3,111,2,802,1,1,2,1,3,1,1,4};
UINT4 Ieee8021BridgePortRequireDropEncoding [ ] ={1,3,111,2,802,1,1,2,1,3,1,1,5};
UINT4 Ieee8021BridgePortServiceAccessPrioritySelection [ ] ={1,3,111,2,802,1,1,2,1,3,1,1,6};
UINT4 Ieee8021BridgeUserPriority [ ] ={1,3,111,2,802,1,1,2,1,3,2,1,1};
UINT4 Ieee8021BridgeRegenUserPriority [ ] ={1,3,111,2,802,1,1,2,1,3,2,1,2};
UINT4 Ieee8021BridgeTrafficClassPriority [ ] ={1,3,111,2,802,1,1,2,1,3,3,1,1};
UINT4 Ieee8021BridgeTrafficClass [ ] ={1,3,111,2,802,1,1,2,1,3,3,1,2};
UINT4 Ieee8021BridgePortOutboundAccessPriority [ ] ={1,3,111,2,802,1,1,2,1,3,4,1,1};
UINT4 Ieee8021BridgePortDecodingComponentId [ ] ={1,3,111,2,802,1,1,2,1,3,5,1,1};
UINT4 Ieee8021BridgePortDecodingPortNum [ ] ={1,3,111,2,802,1,1,2,1,3,5,1,2};
UINT4 Ieee8021BridgePortDecodingPriorityCodePointRow [ ] ={1,3,111,2,802,1,1,2,1,3,5,1,3};
UINT4 Ieee8021BridgePortDecodingPriorityCodePoint [ ] ={1,3,111,2,802,1,1,2,1,3,5,1,4};
UINT4 Ieee8021BridgePortDecodingPriority [ ] ={1,3,111,2,802,1,1,2,1,3,5,1,5};
UINT4 Ieee8021BridgePortDecodingDropEligible [ ] ={1,3,111,2,802,1,1,2,1,3,5,1,6};
UINT4 Ieee8021BridgePortEncodingComponentId [ ] ={1,3,111,2,802,1,1,2,1,3,6,1,1};
UINT4 Ieee8021BridgePortEncodingPortNum [ ] ={1,3,111,2,802,1,1,2,1,3,6,1,2};
UINT4 Ieee8021BridgePortEncodingPriorityCodePointRow [ ] ={1,3,111,2,802,1,1,2,1,3,6,1,3};
UINT4 Ieee8021BridgePortEncodingPriorityCodePoint [ ] ={1,3,111,2,802,1,1,2,1,3,6,1,4};
UINT4 Ieee8021BridgePortEncodingDropEligible [ ] ={1,3,111,2,802,1,1,2,1,3,6,1,5};
UINT4 Ieee8021BridgePortEncodingPriority [ ] ={1,3,111,2,802,1,1,2,1,3,6,1,6};
UINT4 Ieee8021BridgeServiceAccessPriorityComponentId [ ] ={1,3,111,2,802,1,1,2,1,3,7,1,1};
UINT4 Ieee8021BridgeServiceAccessPriorityPortNum [ ] ={1,3,111,2,802,1,1,2,1,3,7,1,2};
UINT4 Ieee8021BridgeServiceAccessPriorityReceived [ ] ={1,3,111,2,802,1,1,2,1,3,7,1,3};
UINT4 Ieee8021BridgeServiceAccessPriorityValue [ ] ={1,3,111,2,802,1,1,2,1,3,7,1,4};
UINT4 Ieee8021BridgeILanIfRowStatus [ ] ={1,3,111,2,802,1,1,2,1,6,1,1,1};
UINT4 Ieee8021BridgeDot1dPortRowStatus [ ] ={1,3,111,2,802,1,1,2,1,7,1,1,1};
UINT4 Ieee8021BridgeBaseIfIndexComponentId [ ] ={1,3,111,2,802,1,1,2,1,1,5,1,1};
UINT4 Ieee8021BridgeBaseIfIndexPort [ ] ={1,3,111,2,802,1,1,2,1,1,5,1,2};
UINT4 Ieee8021BridgePhyPort [ ] ={1,3,111,2,802,1,1,2,1,1,6,1,1};
UINT4 Ieee8021BridgePhyPortIfIndex [ ] ={1,3,111,2,802,1,1,2,1,1,6,1,2};
UINT4 Ieee8021BridgePhyMacAddress [ ] ={1,3,111,2,802,1,1,2,1,1,6,1,3};
UINT4 Ieee8021BridgePhyPortToComponentId [ ] ={1,3,111,2,802,1,1,2,1,1,6,1,4};
UINT4 Ieee8021BridgePhyPortToInternalPort [ ] ={1,3,111,2,802,1,1,2,1,1,6,1,5};




tMbDbEntry Ieee8021BridgeBaseTableMibEntry[]= {

{{13,Ieee8021BridgeBaseComponentId}, GetNextIndexIeee8021BridgeBaseTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeBaseTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeBaseBridgeAddress}, GetNextIndexIeee8021BridgeBaseTable, Ieee8021BridgeBaseBridgeAddressGet, Ieee8021BridgeBaseBridgeAddressSet, Ieee8021BridgeBaseBridgeAddressTest, Ieee8021BridgeBaseTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Ieee8021BridgeBaseTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeBaseNumPorts}, GetNextIndexIeee8021BridgeBaseTable, Ieee8021BridgeBaseNumPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ieee8021BridgeBaseTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeBaseComponentType}, GetNextIndexIeee8021BridgeBaseTable, Ieee8021BridgeBaseComponentTypeGet, Ieee8021BridgeBaseComponentTypeSet, Ieee8021BridgeBaseComponentTypeTest, Ieee8021BridgeBaseTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeBaseTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeBaseDeviceCapabilities}, GetNextIndexIeee8021BridgeBaseTable, Ieee8021BridgeBaseDeviceCapabilitiesGet, Ieee8021BridgeBaseDeviceCapabilitiesSet, Ieee8021BridgeBaseDeviceCapabilitiesTest, Ieee8021BridgeBaseTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021BridgeBaseTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeBaseTrafficClassesEnabled}, GetNextIndexIeee8021BridgeBaseTable, Ieee8021BridgeBaseTrafficClassesEnabledGet, Ieee8021BridgeBaseTrafficClassesEnabledSet, Ieee8021BridgeBaseTrafficClassesEnabledTest, Ieee8021BridgeBaseTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeBaseTableINDEX, 1, 0, 0, "1"},

{{13,Ieee8021BridgeBaseMmrpEnabledStatus}, GetNextIndexIeee8021BridgeBaseTable, Ieee8021BridgeBaseMmrpEnabledStatusGet, Ieee8021BridgeBaseMmrpEnabledStatusSet, Ieee8021BridgeBaseMmrpEnabledStatusTest, Ieee8021BridgeBaseTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeBaseTableINDEX, 1, 0, 0, "1"},

{{13,Ieee8021BridgeBaseRowStatus}, GetNextIndexIeee8021BridgeBaseTable, Ieee8021BridgeBaseRowStatusGet, Ieee8021BridgeBaseRowStatusSet, Ieee8021BridgeBaseRowStatusTest, Ieee8021BridgeBaseTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeBaseTableINDEX, 1, 0, 1, NULL},
};
tMibData Ieee8021BridgeBaseTableEntry = { 8, Ieee8021BridgeBaseTableMibEntry };

tMbDbEntry Ieee8021BridgeBasePortTableMibEntry[]= {

{{13,Ieee8021BridgeBasePortComponentId}, GetNextIndexIeee8021BridgeBasePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeBasePort}, GetNextIndexIeee8021BridgeBasePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeBasePortIfIndex}, GetNextIndexIeee8021BridgeBasePortTable, Ieee8021BridgeBasePortIfIndexGet, Ieee8021BridgeBasePortIfIndexSet, Ieee8021BridgeBasePortIfIndexTest, Ieee8021BridgeBasePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeBasePortDelayExceededDiscards}, GetNextIndexIeee8021BridgeBasePortTable, Ieee8021BridgeBasePortDelayExceededDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeBasePortMtuExceededDiscards}, GetNextIndexIeee8021BridgeBasePortTable, Ieee8021BridgeBasePortMtuExceededDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeBasePortCapabilities}, GetNextIndexIeee8021BridgeBasePortTable, Ieee8021BridgeBasePortCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeBasePortTypeCapabilities}, GetNextIndexIeee8021BridgeBasePortTable, Ieee8021BridgeBasePortTypeCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeBasePortType}, GetNextIndexIeee8021BridgeBasePortTable, Ieee8021BridgeBasePortTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeBasePortExternal}, GetNextIndexIeee8021BridgeBasePortTable, Ieee8021BridgeBasePortExternalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeBasePortAdminPointToPoint}, GetNextIndexIeee8021BridgeBasePortTable, Ieee8021BridgeBasePortAdminPointToPointGet, Ieee8021BridgeBasePortAdminPointToPointSet, Ieee8021BridgeBasePortAdminPointToPointTest, Ieee8021BridgeBasePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, "2"},

{{13,Ieee8021BridgeBasePortOperPointToPoint}, GetNextIndexIeee8021BridgeBasePortTable, Ieee8021BridgeBasePortOperPointToPointGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeBasePortName}, GetNextIndexIeee8021BridgeBasePortTable, Ieee8021BridgeBasePortNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021BridgeBasePortTableINDEX, 2, 0, 0, NULL},
};
tMibData Ieee8021BridgeBasePortTableEntry = { 12, Ieee8021BridgeBasePortTableMibEntry };

tMbDbEntry Ieee8021BridgeBaseIfToPortTableMibEntry[]= {

{{13,Ieee8021BridgeBaseIfIndexComponentId}, GetNextIndexIeee8021BridgeBaseIfToPortTable, Ieee8021BridgeBaseIfIndexComponentIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeBaseIfToPortTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeBaseIfIndexPort}, GetNextIndexIeee8021BridgeBaseIfToPortTable, Ieee8021BridgeBaseIfIndexPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeBaseIfToPortTableINDEX, 1, 0, 0, NULL},

};
tMibData Ieee8021BridgeBaseIfToPortTableEntry = { 2, Ieee8021BridgeBaseIfToPortTableMibEntry};


tMbDbEntry Ieee8021BridgePhyPortTableMibEntry[]= {
     
{{13,Ieee8021BridgePhyPort}, GetNextIndexIeee8021BridgePhyPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgePhyPortTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgePhyPortIfIndex}, GetNextIndexIeee8021BridgePhyPortTable, Ieee8021BridgePhyPortIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgePhyPortTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgePhyMacAddress}, GetNextIndexIeee8021BridgePhyPortTable, Ieee8021BridgePhyMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Ieee8021BridgePhyPortTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgePhyPortToComponentId}, GetNextIndexIeee8021BridgePhyPortTable, Ieee8021BridgePhyPortToComponentIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgePhyPortTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgePhyPortToInternalPort}, GetNextIndexIeee8021BridgePhyPortTable, Ieee8021BridgePhyPortToInternalPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgePhyPortTableINDEX, 1, 0, 0, NULL},

};
tMibData Ieee8021BridgePhyPortTableEntry = { 5 , Ieee8021BridgeBaseIfToPortTableMibEntry };
tMbDbEntry Ieee8021BridgeTpPortTableMibEntry[]= {

{{13,Ieee8021BridgeTpPortComponentId}, GetNextIndexIeee8021BridgeTpPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeTpPortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeTpPort}, GetNextIndexIeee8021BridgeTpPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeTpPortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeTpPortMaxInfo}, GetNextIndexIeee8021BridgeTpPortTable, Ieee8021BridgeTpPortMaxInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ieee8021BridgeTpPortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeTpPortInFrames}, GetNextIndexIeee8021BridgeTpPortTable, Ieee8021BridgeTpPortInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021BridgeTpPortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeTpPortOutFrames}, GetNextIndexIeee8021BridgeTpPortTable, Ieee8021BridgeTpPortOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021BridgeTpPortTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeTpPortInDiscards}, GetNextIndexIeee8021BridgeTpPortTable, Ieee8021BridgeTpPortInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021BridgeTpPortTableINDEX, 2, 0, 0, NULL},
};
tMibData Ieee8021BridgeTpPortTableEntry = { 6, Ieee8021BridgeTpPortTableMibEntry };

tMbDbEntry Ieee8021BridgePortPriorityTableMibEntry[]= {

{{13,Ieee8021BridgePortDefaultUserPriority}, GetNextIndexIeee8021BridgePortPriorityTable, Ieee8021BridgePortDefaultUserPriorityGet, Ieee8021BridgePortDefaultUserPrioritySet, Ieee8021BridgePortDefaultUserPriorityTest, Ieee8021BridgePortPriorityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgePortPriorityTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgePortNumTrafficClasses}, GetNextIndexIeee8021BridgePortPriorityTable, Ieee8021BridgePortNumTrafficClassesGet, Ieee8021BridgePortNumTrafficClassesSet, Ieee8021BridgePortNumTrafficClassesTest, Ieee8021BridgePortPriorityTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021BridgePortPriorityTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgePortPriorityCodePointSelection}, GetNextIndexIeee8021BridgePortPriorityTable, Ieee8021BridgePortPriorityCodePointSelectionGet, Ieee8021BridgePortPriorityCodePointSelectionSet, Ieee8021BridgePortPriorityCodePointSelectionTest, Ieee8021BridgePortPriorityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgePortPriorityTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgePortUseDEI}, GetNextIndexIeee8021BridgePortPriorityTable, Ieee8021BridgePortUseDEIGet, Ieee8021BridgePortUseDEISet, Ieee8021BridgePortUseDEITest, Ieee8021BridgePortPriorityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgePortPriorityTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgePortRequireDropEncoding}, GetNextIndexIeee8021BridgePortPriorityTable, Ieee8021BridgePortRequireDropEncodingGet, Ieee8021BridgePortRequireDropEncodingSet, Ieee8021BridgePortRequireDropEncodingTest, Ieee8021BridgePortPriorityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgePortPriorityTableINDEX, 2, 0, 0, "2"},

{{13,Ieee8021BridgePortServiceAccessPrioritySelection}, GetNextIndexIeee8021BridgePortPriorityTable, Ieee8021BridgePortServiceAccessPrioritySelectionGet, Ieee8021BridgePortServiceAccessPrioritySelectionSet, Ieee8021BridgePortServiceAccessPrioritySelectionTest, Ieee8021BridgePortPriorityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgePortPriorityTableINDEX, 2, 0, 0, NULL},
};
tMibData Ieee8021BridgePortPriorityTableEntry = { 6, Ieee8021BridgePortPriorityTableMibEntry };

tMbDbEntry Ieee8021BridgeUserPriorityRegenTableMibEntry[]= {

{{13,Ieee8021BridgeUserPriority}, GetNextIndexIeee8021BridgeUserPriorityRegenTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeUserPriorityRegenTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021BridgeRegenUserPriority}, GetNextIndexIeee8021BridgeUserPriorityRegenTable, Ieee8021BridgeRegenUserPriorityGet, Ieee8021BridgeRegenUserPrioritySet, Ieee8021BridgeRegenUserPriorityTest, Ieee8021BridgeUserPriorityRegenTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgeUserPriorityRegenTableINDEX, 3, 0, 0, NULL},
};
tMibData Ieee8021BridgeUserPriorityRegenTableEntry = { 2, Ieee8021BridgeUserPriorityRegenTableMibEntry };

tMbDbEntry Ieee8021BridgeTrafficClassTableMibEntry[]= {

{{13,Ieee8021BridgeTrafficClassPriority}, GetNextIndexIeee8021BridgeTrafficClassTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeTrafficClassTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021BridgeTrafficClass}, GetNextIndexIeee8021BridgeTrafficClassTable, Ieee8021BridgeTrafficClassGet, Ieee8021BridgeTrafficClassSet, Ieee8021BridgeTrafficClassTest, Ieee8021BridgeTrafficClassTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021BridgeTrafficClassTableINDEX, 3, 0, 0, NULL},
};
tMibData Ieee8021BridgeTrafficClassTableEntry = { 2, Ieee8021BridgeTrafficClassTableMibEntry };

tMbDbEntry Ieee8021BridgePortOutboundAccessPriorityTableMibEntry[]= {

{{13,Ieee8021BridgePortOutboundAccessPriority}, GetNextIndexIeee8021BridgePortOutboundAccessPriorityTable, Ieee8021BridgePortOutboundAccessPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgePortOutboundAccessPriorityTableINDEX, 3, 0, 0, NULL},
};
tMibData Ieee8021BridgePortOutboundAccessPriorityTableEntry = { 1, Ieee8021BridgePortOutboundAccessPriorityTableMibEntry };

tMbDbEntry Ieee8021BridgePortDecodingTableMibEntry[]= {

{{13,Ieee8021BridgePortDecodingComponentId}, GetNextIndexIeee8021BridgePortDecodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgePortDecodingTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgePortDecodingPortNum}, GetNextIndexIeee8021BridgePortDecodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgePortDecodingTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgePortDecodingPriorityCodePointRow}, GetNextIndexIeee8021BridgePortDecodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021BridgePortDecodingTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgePortDecodingPriorityCodePoint}, GetNextIndexIeee8021BridgePortDecodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Ieee8021BridgePortDecodingTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgePortDecodingPriority}, GetNextIndexIeee8021BridgePortDecodingTable, Ieee8021BridgePortDecodingPriorityGet, Ieee8021BridgePortDecodingPrioritySet, Ieee8021BridgePortDecodingPriorityTest, Ieee8021BridgePortDecodingTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgePortDecodingTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgePortDecodingDropEligible}, GetNextIndexIeee8021BridgePortDecodingTable, Ieee8021BridgePortDecodingDropEligibleGet, Ieee8021BridgePortDecodingDropEligibleSet, Ieee8021BridgePortDecodingDropEligibleTest, Ieee8021BridgePortDecodingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgePortDecodingTableINDEX, 4, 0, 0, NULL},
};
tMibData Ieee8021BridgePortDecodingTableEntry = { 6, Ieee8021BridgePortDecodingTableMibEntry };

tMbDbEntry Ieee8021BridgePortEncodingTableMibEntry[]= {

{{13,Ieee8021BridgePortEncodingComponentId}, GetNextIndexIeee8021BridgePortEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgePortEncodingTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021BridgePortEncodingPortNum}, GetNextIndexIeee8021BridgePortEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgePortEncodingTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021BridgePortEncodingPriorityCodePointRow}, GetNextIndexIeee8021BridgePortEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021BridgePortEncodingTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021BridgePortEncodingPriorityCodePoint}, GetNextIndexIeee8021BridgePortEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Ieee8021BridgePortEncodingTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021BridgePortEncodingDropEligible}, GetNextIndexIeee8021BridgePortEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021BridgePortEncodingTableINDEX, 5, 0, 0, NULL},

{{13,Ieee8021BridgePortEncodingPriority}, GetNextIndexIeee8021BridgePortEncodingTable, Ieee8021BridgePortEncodingPriorityGet, Ieee8021BridgePortEncodingPrioritySet, Ieee8021BridgePortEncodingPriorityTest, Ieee8021BridgePortEncodingTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgePortEncodingTableINDEX, 5, 0, 0, NULL},
};
tMibData Ieee8021BridgePortEncodingTableEntry = { 6, Ieee8021BridgePortEncodingTableMibEntry };

tMbDbEntry Ieee8021BridgeServiceAccessPriorityTableMibEntry[]= {

{{13,Ieee8021BridgeServiceAccessPriorityComponentId}, GetNextIndexIeee8021BridgeServiceAccessPriorityTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeServiceAccessPriorityTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021BridgeServiceAccessPriorityPortNum}, GetNextIndexIeee8021BridgeServiceAccessPriorityTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeServiceAccessPriorityTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021BridgeServiceAccessPriorityReceived}, GetNextIndexIeee8021BridgeServiceAccessPriorityTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeServiceAccessPriorityTableINDEX, 3, 0, 0, NULL},

{{13,Ieee8021BridgeServiceAccessPriorityValue}, GetNextIndexIeee8021BridgeServiceAccessPriorityTable, Ieee8021BridgeServiceAccessPriorityValueGet, Ieee8021BridgeServiceAccessPriorityValueSet, Ieee8021BridgeServiceAccessPriorityValueTest, Ieee8021BridgeServiceAccessPriorityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgeServiceAccessPriorityTableINDEX, 3, 0, 0, NULL},
};
tMibData Ieee8021BridgeServiceAccessPriorityTableEntry = { 4, Ieee8021BridgeServiceAccessPriorityTableMibEntry };

tMbDbEntry Ieee8021BridgeILanIfTableMibEntry[]= {

{{13,Ieee8021BridgeILanIfRowStatus}, GetNextIndexIeee8021BridgeILanIfTable, Ieee8021BridgeILanIfRowStatusGet, Ieee8021BridgeILanIfRowStatusSet, Ieee8021BridgeILanIfRowStatusTest, Ieee8021BridgeILanIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeILanIfTableINDEX, 1, 0, 1, NULL},
};
tMibData Ieee8021BridgeILanIfTableEntry = { 1, Ieee8021BridgeILanIfTableMibEntry };

tMbDbEntry Ieee8021BridgeDot1dPortTableMibEntry[]= {

{{13,Ieee8021BridgeDot1dPortRowStatus}, GetNextIndexIeee8021BridgeDot1dPortTable, Ieee8021BridgeDot1dPortRowStatusGet, Ieee8021BridgeDot1dPortRowStatusSet, Ieee8021BridgeDot1dPortRowStatusTest, Ieee8021BridgeDot1dPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeDot1dPortTableINDEX, 2, 0, 1, NULL},
};
tMibData Ieee8021BridgeDot1dPortTableEntry = { 1, Ieee8021BridgeDot1dPortTableMibEntry };

#endif /* _STD1D1DB_H */

