enum {
    MAX_VLAN_RED_CACHE_ENTRIES_SIZING_ID,
    MAX_VLAN_RED_MCAST_CACHE_ENTRIES_SIZING_ID,
    MAX_VLAN_RED_PROTO_CACHE_ENTRIES_SIZING_ID,
    MAX_VLAN_RED_ST_UCAST_CACHE_ENTRIES_SIZING_ID,
    VLANRED_MAX_SIZING_ID
};


#ifdef  _VLANREDSZ_C
tMemPoolId VLANREDMemPoolIds[ VLANRED_MAX_SIZING_ID];
INT4  VlanredSizingMemCreateMemPools(VOID);
VOID  VlanredSizingMemDeleteMemPools(VOID);
INT4  VlanredSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _VLANREDSZ_C  */
extern tMemPoolId VLANREDMemPoolIds[ ];
extern INT4  VlanredSizingMemCreateMemPools(VOID);
extern VOID  VlanredSizingMemDeleteMemPools(VOID);
#endif /*  _VLANREDSZ_C  */


#ifdef  _VLANREDSZ_C
tFsModSizingParams FsVLANREDSizingParams [] = {
{ "tVlanRedCacheNode", "MAX_VLAN_RED_CACHE_ENTRIES", sizeof(tVlanRedCacheNode),MAX_VLAN_RED_CACHE_ENTRIES, MAX_VLAN_RED_CACHE_ENTRIES,0 },
{ "tVlanRedMcastCacheNode", "MAX_VLAN_RED_MCAST_CACHE_ENTRIES", sizeof(tVlanRedMcastCacheNode),MAX_VLAN_RED_MCAST_CACHE_ENTRIES, MAX_VLAN_RED_MCAST_CACHE_ENTRIES,0 },
{ "tVlanRedProtoVlanCacheNode", "MAX_VLAN_RED_PROTO_VLAN_ENTRIES", sizeof(tVlanRedProtoVlanCacheNode),MAX_VLAN_RED_PROTO_VLAN_ENTRIES, MAX_VLAN_RED_PROTO_VLAN_ENTRIES,0 },
{ "tVlanRedStUcastCacheNode", "MAX_VLAN_RED_ST_UCAST_CACHE_ENTRIES", sizeof(tVlanRedStUcastCacheNode),MAX_VLAN_RED_ST_UCAST_CACHE_ENTRIES, MAX_VLAN_RED_ST_UCAST_CACHE_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _VLANREDSZ_C  */
extern tFsModSizingParams FsVLANREDSizingParams [];
#endif /*  _VLANREDSZ_C  */


