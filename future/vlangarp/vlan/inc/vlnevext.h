/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevext.h,v 1.3 2016/02/02 12:20:10 siva Exp $
 *
 * Description: This file contains VLAN EVB External Declarations.
 *
 *******************************************************************/
#ifndef _VLNEVEXT_H
#define _VLNEVEXT_H

extern tEvbGlobalInfo      gEvbGlobalInfo;

extern UINT1 gau1EvbCdcpOUI[VLAN_EVB_CDCP_TLV_OUI_LEN];
extern UINT1 gu1EvbCdcpSubtype;
extern UINT4 gu4EvbBulkUpdPrevPort ;
extern UINT4 gu4EvbBulkUpdPrevContxt ;

#endif  /*_VLNEVEXT_H */

