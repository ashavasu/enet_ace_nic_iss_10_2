/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmid1db.h,v 1.5 2016/07/16 11:15:03 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMID1DB_H
#define _FSMID1DB_H

UINT1 FsMIEvbSystemTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIEvbCAPConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEvbUAPConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIEvbUAPStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmid1 [] ={1,3,6,1,4,1,29601,2,104};
tSNMP_OID_TYPE fsmid1OID = {9, fsmid1};


UINT4 FsMIEvbSystemContextId [ ] ={1,3,6,1,4,1,29601,2,104,1,1,1,1,1};
UINT4 FsMIEvbSystemControl [ ] ={1,3,6,1,4,1,29601,2,104,1,1,1,1,2};
UINT4 FsMIEvbSystemModuleStatus [ ] ={1,3,6,1,4,1,29601,2,104,1,1,1,1,3};
UINT4 FsMIEvbSystemTraceLevel [ ] ={1,3,6,1,4,1,29601,2,104,1,1,1,1,4};
UINT4 FsMIEvbSystemTrapStatus [ ] ={1,3,6,1,4,1,29601,2,104,1,1,1,1,5};
UINT4 FsMIEvbSystemStatsClear [ ] ={1,3,6,1,4,1,29601,2,104,1,1,1,1,6};
UINT4 FsMIEvbSchannelIdMode [ ] ={1,3,6,1,4,1,29601,2,104,1,1,1,1,7};
UINT4 FsMIEvbSystemRowStatus [ ] ={1,3,6,1,4,1,29601,2,104,1,1,1,1,8};
UINT4 FsMIEvbCAPSChannelID [ ] ={1,3,6,1,4,1,29601,2,104,1,2,1,1,1};
UINT4 FsMIEvbCAPSChannelIfIndex [ ] ={1,3,6,1,4,1,29601,2,104,1,2,1,1,2};
UINT4 FsMIEvbCAPSChNegoStatus [ ] ={1,3,6,1,4,1,29601,2,104,1,2,1,1,3};
UINT4 FsMIEvbPhyPort [ ] ={1,3,6,1,4,1,29601,2,104,1,2,1,1,4};
UINT4 FsMIEvbSchID [ ] ={1,3,6,1,4,1,29601,2,104,1,2,1,1,5};
UINT4 FsMIEvbSChannelFilterStatus [ ] ={1,3,6,1,4,1,29601,2,104,1,2,1,1,6};
UINT4 FsMIEvbUAPSchCdcpMode [ ] ={1,3,6,1,4,1,29601,2,104,1,2,2,1,1};
UINT4 FsMIEvbTxCdcpCount [ ] ={1,3,6,1,4,1,29601,2,104,1,3,1,1,1};
UINT4 FsMIEvbRxCdcpCount [ ] ={1,3,6,1,4,1,29601,2,104,1,3,1,1,2};
UINT4 FsMIEvbSChAllocFailCount [ ] ={1,3,6,1,4,1,29601,2,104,1,3,1,1,3};
UINT4 FsMIEvbSChActiveFailCount [ ] ={1,3,6,1,4,1,29601,2,104,1,3,1,1,4};
UINT4 FsMIEvbSVIDPoolExceedsCount [ ] ={1,3,6,1,4,1,29601,2,104,1,3,1,1,5};
UINT4 FsMIEvbCdcpRejectStationReq [ ] ={1,3,6,1,4,1,29601,2,104,1,3,1,1,6};
UINT4 FsMIEvbCdcpOtherDropCount [ ] ={1,3,6,1,4,1,29601,2,104,1,3,1,1,7};




tMbDbEntry fsmid1MibEntry[]= {

{{14,FsMIEvbSystemContextId}, GetNextIndexFsMIEvbSystemTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIEvbSystemTableINDEX, 1, 0, 0, NULL},

{{14,FsMIEvbSystemControl}, GetNextIndexFsMIEvbSystemTable, FsMIEvbSystemControlGet, FsMIEvbSystemControlSet, FsMIEvbSystemControlTest, FsMIEvbSystemTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEvbSystemTableINDEX, 1, 0, 0, "2"},

{{14,FsMIEvbSystemModuleStatus}, GetNextIndexFsMIEvbSystemTable, FsMIEvbSystemModuleStatusGet, FsMIEvbSystemModuleStatusSet, FsMIEvbSystemModuleStatusTest, FsMIEvbSystemTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEvbSystemTableINDEX, 1, 0, 0, "2"},

{{14,FsMIEvbSystemTraceLevel}, GetNextIndexFsMIEvbSystemTable, FsMIEvbSystemTraceLevelGet, FsMIEvbSystemTraceLevelSet, FsMIEvbSystemTraceLevelTest, FsMIEvbSystemTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEvbSystemTableINDEX, 1, 0, 0, "0"},

{{14,FsMIEvbSystemTrapStatus}, GetNextIndexFsMIEvbSystemTable, FsMIEvbSystemTrapStatusGet, FsMIEvbSystemTrapStatusSet, FsMIEvbSystemTrapStatusTest, FsMIEvbSystemTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEvbSystemTableINDEX, 1, 0, 0, "2"},

{{14,FsMIEvbSystemStatsClear}, GetNextIndexFsMIEvbSystemTable, FsMIEvbSystemStatsClearGet, FsMIEvbSystemStatsClearSet, FsMIEvbSystemStatsClearTest, FsMIEvbSystemTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEvbSystemTableINDEX, 1, 0, 0, "2"},

{{14,FsMIEvbSchannelIdMode}, GetNextIndexFsMIEvbSystemTable, FsMIEvbSchannelIdModeGet, FsMIEvbSchannelIdModeSet, FsMIEvbSchannelIdModeTest, FsMIEvbSystemTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEvbSystemTableINDEX, 1, 0, 0, "1"},

{{14,FsMIEvbSystemRowStatus}, GetNextIndexFsMIEvbSystemTable, FsMIEvbSystemRowStatusGet, FsMIEvbSystemRowStatusSet, FsMIEvbSystemRowStatusTest, FsMIEvbSystemTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEvbSystemTableINDEX, 1, 0, 1, NULL},

{{14,FsMIEvbCAPSChannelID}, GetNextIndexFsMIEvbCAPConfigTable, FsMIEvbCAPSChannelIDGet, FsMIEvbCAPSChannelIDSet, FsMIEvbCAPSChannelIDTest, FsMIEvbCAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIEvbCAPConfigTableINDEX, 2, 0, 0, NULL},

{{14,FsMIEvbCAPSChannelIfIndex}, GetNextIndexFsMIEvbCAPConfigTable, FsMIEvbCAPSChannelIfIndexGet, FsMIEvbCAPSChannelIfIndexSet, FsMIEvbCAPSChannelIfIndexTest, FsMIEvbCAPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEvbCAPConfigTableINDEX, 2, 0, 0, NULL},

{{14,FsMIEvbCAPSChNegoStatus}, GetNextIndexFsMIEvbCAPConfigTable, FsMIEvbCAPSChNegoStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIEvbCAPConfigTableINDEX, 2, 0, 0, "1"},

{{14,FsMIEvbPhyPort}, GetNextIndexFsMIEvbCAPConfigTable, FsMIEvbPhyPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEvbCAPConfigTableINDEX, 2, 0, 0, NULL},

{{14,FsMIEvbSchID}, GetNextIndexFsMIEvbCAPConfigTable, FsMIEvbSchIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIEvbCAPConfigTableINDEX, 2, 0, 0, NULL},

{{14,FsMIEvbSChannelFilterStatus}, GetNextIndexFsMIEvbCAPConfigTable, FsMIEvbSChannelFilterStatusGet, FsMIEvbSChannelFilterStatusSet, FsMIEvbSChannelFilterStatusTest, FsMIEvbCAPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEvbCAPConfigTableINDEX, 2, 0, 0, "2"},

{{14,FsMIEvbUAPSchCdcpMode}, GetNextIndexFsMIEvbUAPConfigTable, FsMIEvbUAPSchCdcpModeGet, FsMIEvbUAPSchCdcpModeSet, FsMIEvbUAPSchCdcpModeTest, FsMIEvbUAPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIEvbUAPConfigTableINDEX, 1, 0, 0, "1"},

{{14,FsMIEvbTxCdcpCount}, GetNextIndexFsMIEvbUAPStatsTable, FsMIEvbTxCdcpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIEvbUAPStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsMIEvbRxCdcpCount}, GetNextIndexFsMIEvbUAPStatsTable, FsMIEvbRxCdcpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIEvbUAPStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsMIEvbSChAllocFailCount}, GetNextIndexFsMIEvbUAPStatsTable, FsMIEvbSChAllocFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIEvbUAPStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsMIEvbSChActiveFailCount}, GetNextIndexFsMIEvbUAPStatsTable, FsMIEvbSChActiveFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIEvbUAPStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsMIEvbSVIDPoolExceedsCount}, GetNextIndexFsMIEvbUAPStatsTable, FsMIEvbSVIDPoolExceedsCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIEvbUAPStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsMIEvbCdcpRejectStationReq}, GetNextIndexFsMIEvbUAPStatsTable, FsMIEvbCdcpRejectStationReqGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIEvbUAPStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsMIEvbCdcpOtherDropCount}, GetNextIndexFsMIEvbUAPStatsTable, FsMIEvbCdcpOtherDropCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIEvbUAPStatsTableINDEX, 1, 0, 0, NULL},
};
tMibData fsmid1Entry = { 22, fsmid1MibEntry };

#endif /* _FSMID1DB_H */



