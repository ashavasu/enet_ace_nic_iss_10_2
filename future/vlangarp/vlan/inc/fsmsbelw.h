/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsbelw.h,v 1.5 2012/05/31 12:45:36 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsDot1dExtBaseTable. */
INT1
nmhValidateIndexInstanceFsDot1dExtBaseTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dExtBaseTable  */

INT1
nmhGetFirstIndexFsDot1dExtBaseTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dExtBaseTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dDeviceCapabilities ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot1dTrafficClassesEnabled ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1dTrafficClassesEnabled ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1dTrafficClassesEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsDot1dPortCapabilitiesTable. */
INT1
nmhValidateIndexInstanceFsDot1dPortCapabilitiesTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dPortCapabilitiesTable  */

INT1
nmhGetFirstIndexFsDot1dPortCapabilitiesTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dPortCapabilitiesTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dPortCapabilities ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsDot1dPortPriorityTable. */
INT1
nmhValidateIndexInstanceFsDot1dPortPriorityTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dPortPriorityTable  */

INT1
nmhGetFirstIndexFsDot1dPortPriorityTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dPortPriorityTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dPortDefaultUserPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dPortNumTrafficClasses ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1dPortDefaultUserPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1dPortNumTrafficClasses ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1dPortDefaultUserPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dPortNumTrafficClasses ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1dPortPriorityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1dUserPriorityRegenTable. */
INT1
nmhValidateIndexInstanceFsDot1dUserPriorityRegenTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dUserPriorityRegenTable  */

INT1
nmhGetFirstIndexFsDot1dUserPriorityRegenTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dUserPriorityRegenTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dRegenUserPriority ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1dRegenUserPriority ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1dRegenUserPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1dUserPriorityRegenTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1dTrafficClassTable. */
INT1
nmhValidateIndexInstanceFsDot1dTrafficClassTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dTrafficClassTable  */

INT1
nmhGetFirstIndexFsDot1dTrafficClassTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dTrafficClassTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dTrafficClass ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1dTrafficClass ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1dTrafficClass ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1dTrafficClassTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1dPortOutboundAccessPriorityTable. */
INT1
nmhValidateIndexInstanceFsDot1dPortOutboundAccessPriorityTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dPortOutboundAccessPriorityTable  */

INT1
nmhGetFirstIndexFsDot1dPortOutboundAccessPriorityTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dPortOutboundAccessPriorityTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dPortOutboundAccessPriority ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsDot1dTpHCPortTable. */
INT1
nmhValidateIndexInstanceFsDot1dTpHCPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dTpHCPortTable  */

INT1
nmhGetFirstIndexFsDot1dTpHCPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dTpHCPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dTpHCPortInFrames ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsDot1dTpHCPortOutFrames ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsDot1dTpHCPortInDiscards ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for FsDot1dTpPortOverflowTable. */
INT1
nmhValidateIndexInstanceFsDot1dTpPortOverflowTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dTpPortOverflowTable  */

INT1
nmhGetFirstIndexFsDot1dTpPortOverflowTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dTpPortOverflowTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dTpPortInOverflowFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot1dTpPortOutOverflowFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot1dTpPortInOverflowDiscards ARG_LIST((INT4 ,UINT4 *));
