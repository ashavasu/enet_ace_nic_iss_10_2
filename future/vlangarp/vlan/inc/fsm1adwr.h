/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: fsm1adwr.h,v 1.7 2013/12/05 12:44:52 siva Exp $
* Description: This header file contains all prototype of the wrapper  
*              routines in VLAN Modules.
****************************************************************************/
#ifndef _FSM1ADWR_H
#define _FSM1ADWR_H
INT4 GetNextIndexDot1adMIPortTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSM1AD(VOID);

VOID UnRegisterFSM1AD(VOID);
INT4 Dot1adMIPortPcpSelectionRowGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortUseDeiGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortReqDropEncodingGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortSVlanPriorityTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortSVlanPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortPcpSelectionRowSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortUseDeiSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortReqDropEncodingSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortSVlanPriorityTypeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortSVlanPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortPcpSelectionRowTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortUseDeiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortReqDropEncodingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortSVlanPriorityTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortSVlanPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexDot1adMIVidTranslationTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adMIVidTranslationRelayVidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIVidTranslationRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIVidTranslationRelayVidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIVidTranslationRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIVidTranslationRelayVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIVidTranslationRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIVidTranslationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1adMICVidRegistrationTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adMICVidRegistrationSVidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationUntaggedPepGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationUntaggedCepGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationRelayCVidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationSVlanPriorityTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationSVlanPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationSVidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationUntaggedPepSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationUntaggedCepSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationRelayCVidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationSVlanPriorityTypeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationSVlanPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationSVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationUntaggedPepTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationUntaggedCepTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationRelayCVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationSVlanPriorityTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationSVlanPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMICVidRegistrationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexDot1adMIPepTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adMIPepPvidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepDefaultUserPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepAccptableFrameTypesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepIngressFilteringGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepPvidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepDefaultUserPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepAccptableFrameTypesSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepIngressFilteringSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepPvidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepDefaultUserPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepAccptableFrameTypesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepIngressFilteringTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPepTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexDot1adMIServicePriorityRegenerationTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adMIServicePriorityRegenRegeneratedPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIServicePriorityRegenRegeneratedPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIServicePriorityRegenRegeneratedPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIServicePriorityRegenerationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexDot1adMIPcpDecodingTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adMIPcpDecodingPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPcpDecodingDropEligibleGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPcpDecodingPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPcpDecodingDropEligibleSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPcpDecodingPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPcpDecodingDropEligibleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPcpDecodingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1adMIPcpEncodingTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adMIPcpEncodingPcpValueGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPcpEncodingPcpValueSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPcpEncodingPcpValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adMIPcpEncodingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif /* _FSM1ADWR_H */
