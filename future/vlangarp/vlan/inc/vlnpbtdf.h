/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: vlnpbtdf.h,v 1.11 2015/04/25 12:27:04 siva Exp $
* Description: This header file contains all data structures used in  
*              in VLAN Module.
****************************************************************************/
#ifndef _VLNPBTDF_H
#define _VLNPBTDF_H

typedef INT4 (*tVlanSVlanTableCmpFn) (tRBElem*, tRBElem*);

typedef struct _tVlanSVlanTableInfo {
    INT4 i4MaxEntriesInTable;
    INT4 i4SVlanTableType;
    tVlanSVlanTableCmpFn  pSVlanTableCmp;
}tVlanSVlanTableInfo;


typedef struct _tVlanPbPoolInfo {

    tMemPoolId VlanCVlanPoolId; /* S-VLAN classify Port, C-VLAN based*/
    tMemPoolId VlanCVlanDstMacPoolId; /* S-VLAN classify Port, C-VLAN,
                                         DstMAC based */
    tMemPoolId VlanCVlanSrcMacPoolId; /* S-VLAN classify Port, C-VLAN,
                                         SrcMAC based */
    tMemPoolId VlanCVlanDscpPoolId;   /* S-VLAN classify Port, C-VLAN,
                                         Dscp based */
    tMemPoolId VlanCVlanDstIpPoolId;  /* S-VLAN classify Port, C-VLAN,
                                         Dst IP */
    tMemPoolId VlanSrcMacPoolId;      /* S-VLAN classify Port, SrcMAC */
    tMemPoolId VlanDstMacPoolId;      /* S-VLAN classify Port, DstMAC */
    tMemPoolId VlanDscpPoolId;        /* S-VLAN classify Port, Dscp */
    tMemPoolId VlanSrcIpPoolId;       /* S-VLAN classify Port, SrcIP */
    tMemPoolId VlanDstIpPoolId;       /* S-VLAN classify Port, DstIP */
    tMemPoolId VlanSrcDstIpPoolId;    /* S-VLAN classify Port, Src, dst IP */

    tMemPoolId EtherTypeSwapPoolId;   /* Ethertype swap table */
    tMemPoolId VlanFdbControlPoolId;
    tMemPoolId LogicalPortTblPoolId;   /* Logical port (PEP/internal CNP)table */
    tMemPoolId PbPortTblPoolId;  /* PB port entry pool ID */ 
}tVlanPbPoolInfo;

typedef struct _tVlanCVlanSVlanEntry {
    tRBNodeEmbd            RBNode;
    UINT2                  u2Port;
    tVlanId                CVlanId;
    tVlanId                SVlanId;
    tVlanId                RelayCVlanId;
    UINT1                  u1PepUntag; /* VLAN_SNMP_TRUE/VLAN_SNMP_FALSE */
    UINT1                  u1CepUntag;/* VLAN_SNMP_TRUE/VLAN_SNMP_FALSE */
    UINT1                  u1SVlanPriorityType;
    UINT1                  u1SVlanPriority;
    UINT1                  u1RowStatus;
    UINT1                  u1Reserved;
    UINT1                  u1CvlanStatus;
    BOOLEAN                bCvlanStatFlush;
}tVlanCVlanSVlanEntry; 

typedef struct _tVlanCVlanSrcMacSVlanEntry {
    tRBNodeEmbd            RBNode;
    UINT2                  u2Port;
    tVlanId                CVlanId;
    tMacAddr               SrcMac;
    tVlanId                SVlanId;
    UINT1                  u1RowStatus;
    UINT1                  au1Reserved[3];
}tVlanCVlanSrcMacSVlanEntry; 

typedef struct _tVlanCVlanDstMacSVlanEntry {
    tRBNodeEmbd            RBNode;
    UINT2                  u2Port;
    tVlanId                CVlanId;
    tMacAddr               DstMac;
    tVlanId                SVlanId;
    UINT1                  u1RowStatus;
    UINT1                  au1Reserved[3];
}tVlanCVlanDstMacSVlanEntry; 

typedef struct _tVlanCVlanDscpSVlanEntry {
    tRBNodeEmbd            RBNode;
    UINT2                  u2Port;
    tVlanId                CVlanId;
    UINT4                  u4Dscp;
    tVlanId                SVlanId;
    UINT1                  u1RowStatus;
    UINT1                  u1Reserved;
}tVlanCVlanDscpSVlanEntry; 

typedef struct _tVlanCVlanDstIpSVlanEntry {
    tRBNodeEmbd            RBNode;
    UINT2                  u2Port;
    tVlanId                CVlanId;
    UINT4                  u4DstIp;
    tVlanId                SVlanId;
    UINT1                  u1RowStatus;
    UINT1                  u1Reserved;
}tVlanCVlanDstIpSVlanEntry; 

typedef struct _tVlanSrcMacSVlanEntry {
    tRBNodeEmbd            RBNode;
    UINT2                  u2Port;
    tMacAddr               SrcMac;
    tVlanId                SVlanId;
    UINT1                  u1RowStatus;
    UINT1                  u1Reserved;
}tVlanSrcMacSVlanEntry; 

typedef struct _tVlanDstMacSVlanEntry {
    tRBNodeEmbd            RBNode;
    UINT2                  u2Port;
    tMacAddr               DstMac;
    tVlanId                SVlanId;
    UINT1                  u1RowStatus;
    UINT1                  u1Reserved;
}tVlanDstMacSVlanEntry;

typedef struct _tVlanDscpSVlanEntry {
    tRBNodeEmbd            RBNode;
    UINT4                  u4Dscp;
    UINT2                  u2Port;
    tVlanId                SVlanId;
    UINT1                  u1RowStatus;
    UINT1                  au1Reserved[3];
}tVlanDscpSVlanEntry; 

typedef struct _tVlanSrcIpSVlanEntry {
    tRBNodeEmbd            RBNode;
    UINT4                  u4SrcIp;
    UINT2                  u2Port;
    tVlanId                SVlanId;
    UINT1                  u1RowStatus;
    UINT1                  au1Reserved[3];
}tVlanSrcIpSVlanEntry; 

typedef struct _tVlanDstIpSVlanEntry {
    tRBNodeEmbd            RBNode;
    UINT4                  u4DstIp;
    UINT2                  u2Port;
    tVlanId                SVlanId;
    UINT1                  u1RowStatus;
    UINT1                  au1Reserved[3];
}tVlanDstIpSVlanEntry; 

typedef struct _tVlanSrcDstIpSVlanEntry {
    tRBNodeEmbd            RBNode;
    UINT4                  u4SrcIp;
    UINT4                  u4DstIp;
    UINT2                  u2Port;
    tVlanId                SVlanId;
    UINT1                  u1RowStatus;
    UINT1                  au1Reserved[3];
}tVlanSrcDstIpSVlanEntry; 


typedef struct _tVlanSVlanEpTagInfo {
    tVlanId            SVlanId;
    BOOL1              bUntaggedPep;
    BOOL1              bUntaggedCep;
}tVlanSVlanEpTagInfo;
/* Frame header IP, ARP/RARP */
typedef struct _tVlanIpHeader {
    UINT1               u1VerHdrlen;
    UINT1               u1Tos;
    UINT2               u2Totlen;
    /* Total length  IP header + DATA
     */
    UINT2               u2Id;
    /* Identification
     */
    UINT2               u2FlOffs;
    /* Flags + fragment offset
     */
    UINT1               u1Ttl;
    /* Time to live
     */
    UINT1               u1Proto;
    /* Protocol
     */
    UINT2               u2Cksum;
    /* Checksum value
     */
    UINT4               u4Src;
    /* Source address
     */
    UINT4               u4Dest;
    /* Destination address
     */
}tVlanIpHeader;

typedef struct _tVlanArpHeader {
    UINT2      u2HwAddrType;
    UINT2      u2protocolAddrType;
    UINT1      u1HwAddrLen;
    UINT1      u1ProtoAddrLen;
    UINT2      u2Operation;
    tMacAddr   SrcMacAddr;
    tMacAddr   DstMacAddr;
    UINT4      u4SrcIp;
    UINT4      u4DstIp;
}tVlanArpHeader;

typedef struct _EtherTypeSwapEntry{
    tRBNodeEmbd            RBNode;
    UINT2    u2Port;
    UINT2    u2LocalEtherType;
    UINT2    u2RelayEtherType;
    UINT1    u1RowStatus;
    UINT1    au1Pad[1];
}tEtherTypeSwapEntry;

typedef struct _VlanPbLogicalPortEntry {
   tRBNodeEmbd  RBNode;
   tVlanId      SVlanId;
   tVlanId      Cpvid;
   tVlanId      SDefpvid;
   UINT2        u2NoOfCvidEntries;
   UINT2        u2NoOfActiveCvidEntries;
   UINT1        au1RegenPriority[VLAN_PB_MAX_PRIORITY];
   UINT2        u2CepPort;
   UINT1        u1DefUserPriority;
   UINT1        u1IngressFiltering; /* VLAN_SNMP_TRUE/VLAN_SNMP_FALSE */
   UINT1        u1AccptFrameType;
   UINT1        u1OperStatus;
   UINT1        u1UnTagPepSet; /* VLAN_TRUE/VLAN_FALSE */
   UINT1        u1CosPreservation; /* VLAN_ENABLED/VLAN_DISABLED */
   UINT1        au1Pad[2];
}tVlanPbLogicalPortEntry;

#endif

