/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbrgdb.h,v 1.6 2008/12/31 06:24:19 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDBRGDB_H
#define _STDBRGDB_H

UINT1 Dot1dPortCapabilitiesTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1dPortPriorityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1dUserPriorityRegenTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dTrafficClassTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dPortOutboundAccessPriorityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dPortGarpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1dPortGmrpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1dTpHCPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dTpPortOverflowTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 stdbrg [] ={1,3,6,1,2,1,17,4,5};
tSNMP_OID_TYPE stdbrgOID = {9, stdbrg};


UINT4 Dot1dDeviceCapabilities [ ] ={1,3,6,1,2,1,17,6,1,1,1};
UINT4 Dot1dTrafficClassesEnabled [ ] ={1,3,6,1,2,1,17,6,1,1,2};
UINT4 Dot1dGmrpStatus [ ] ={1,3,6,1,2,1,17,6,1,1,3};
UINT4 Dot1dPortCapabilities [ ] ={1,3,6,1,2,1,17,6,1,1,4,1,1};
UINT4 Dot1dPortDefaultUserPriority [ ] ={1,3,6,1,2,1,17,6,1,2,1,1,1};
UINT4 Dot1dPortNumTrafficClasses [ ] ={1,3,6,1,2,1,17,6,1,2,1,1,2};
UINT4 Dot1dUserPriority [ ] ={1,3,6,1,2,1,17,6,1,2,2,1,1};
UINT4 Dot1dRegenUserPriority [ ] ={1,3,6,1,2,1,17,6,1,2,2,1,2};
UINT4 Dot1dTrafficClassPriority [ ] ={1,3,6,1,2,1,17,6,1,2,3,1,1};
UINT4 Dot1dTrafficClass [ ] ={1,3,6,1,2,1,17,6,1,2,3,1,2};
UINT4 Dot1dPortOutboundAccessPriority [ ] ={1,3,6,1,2,1,17,6,1,2,4,1,1};
UINT4 Dot1dPortGarpJoinTime [ ] ={1,3,6,1,2,1,17,6,1,3,1,1,1};
UINT4 Dot1dPortGarpLeaveTime [ ] ={1,3,6,1,2,1,17,6,1,3,1,1,2};
UINT4 Dot1dPortGarpLeaveAllTime [ ] ={1,3,6,1,2,1,17,6,1,3,1,1,3};
UINT4 Dot1dPortGmrpStatus [ ] ={1,3,6,1,2,1,17,6,1,4,1,1,1};
UINT4 Dot1dPortGmrpFailedRegistrations [ ] ={1,3,6,1,2,1,17,6,1,4,1,1,2};
UINT4 Dot1dPortGmrpLastPduOrigin [ ] ={1,3,6,1,2,1,17,6,1,4,1,1,3};
UINT4 Dot1dPortRestrictedGroupRegistration [ ] ={1,3,6,1,2,1,17,6,1,4,1,1,4};
UINT4 Dot1dTpHCPortInFrames [ ] ={1,3,6,1,2,1,17,4,5,1,1};
UINT4 Dot1dTpHCPortOutFrames [ ] ={1,3,6,1,2,1,17,4,5,1,2};
UINT4 Dot1dTpHCPortInDiscards [ ] ={1,3,6,1,2,1,17,4,5,1,3};
UINT4 Dot1dTpPortInOverflowFrames [ ] ={1,3,6,1,2,1,17,4,6,1,1};
UINT4 Dot1dTpPortOutOverflowFrames [ ] ={1,3,6,1,2,1,17,4,6,1,2};
UINT4 Dot1dTpPortInOverflowDiscards [ ] ={1,3,6,1,2,1,17,4,6,1,3};


tMbDbEntry stdbrgMibEntry[]= {

{{11,Dot1dTpHCPortInFrames}, GetNextIndexDot1dTpHCPortTable, Dot1dTpHCPortInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot1dTpHCPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpHCPortOutFrames}, GetNextIndexDot1dTpHCPortTable, Dot1dTpHCPortOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot1dTpHCPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpHCPortInDiscards}, GetNextIndexDot1dTpHCPortTable, Dot1dTpHCPortInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot1dTpHCPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPortInOverflowFrames}, GetNextIndexDot1dTpPortOverflowTable, Dot1dTpPortInOverflowFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dTpPortOverflowTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPortOutOverflowFrames}, GetNextIndexDot1dTpPortOverflowTable, Dot1dTpPortOutOverflowFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dTpPortOverflowTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPortInOverflowDiscards}, GetNextIndexDot1dTpPortOverflowTable, Dot1dTpPortInOverflowDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dTpPortOverflowTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dDeviceCapabilities}, NULL, Dot1dDeviceCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Dot1dTrafficClassesEnabled}, NULL, Dot1dTrafficClassesEnabledGet, Dot1dTrafficClassesEnabledSet, Dot1dTrafficClassesEnabledTest, Dot1dTrafficClassesEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,Dot1dGmrpStatus}, NULL, Dot1dGmrpStatusGet, Dot1dGmrpStatusSet, Dot1dGmrpStatusTest, Dot1dGmrpStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,Dot1dPortCapabilities}, GetNextIndexDot1dPortCapabilitiesTable, Dot1dPortCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1dPortCapabilitiesTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dPortDefaultUserPriority}, GetNextIndexDot1dPortPriorityTable, Dot1dPortDefaultUserPriorityGet, Dot1dPortDefaultUserPrioritySet, Dot1dPortDefaultUserPriorityTest, Dot1dPortPriorityTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dPortPriorityTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dPortNumTrafficClasses}, GetNextIndexDot1dPortPriorityTable, Dot1dPortNumTrafficClassesGet, Dot1dPortNumTrafficClassesSet, Dot1dPortNumTrafficClassesTest, Dot1dPortPriorityTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dPortPriorityTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dUserPriority}, GetNextIndexDot1dUserPriorityRegenTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1dUserPriorityRegenTableINDEX, 2, 0, 0, NULL},

{{13,Dot1dRegenUserPriority}, GetNextIndexDot1dUserPriorityRegenTable, Dot1dRegenUserPriorityGet, Dot1dRegenUserPrioritySet, Dot1dRegenUserPriorityTest, Dot1dUserPriorityRegenTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dUserPriorityRegenTableINDEX, 2, 0, 0, NULL},

{{13,Dot1dTrafficClassPriority}, GetNextIndexDot1dTrafficClassTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1dTrafficClassTableINDEX, 2, 0, 0, NULL},

{{13,Dot1dTrafficClass}, GetNextIndexDot1dTrafficClassTable, Dot1dTrafficClassGet, Dot1dTrafficClassSet, Dot1dTrafficClassTest, Dot1dTrafficClassTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dTrafficClassTableINDEX, 2, 0, 0, NULL},

{{13,Dot1dPortOutboundAccessPriority}, GetNextIndexDot1dPortOutboundAccessPriorityTable, Dot1dPortOutboundAccessPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dPortOutboundAccessPriorityTableINDEX, 2, 0, 0, NULL},

{{13,Dot1dPortGarpJoinTime}, GetNextIndexDot1dPortGarpTable, Dot1dPortGarpJoinTimeGet, Dot1dPortGarpJoinTimeSet, Dot1dPortGarpJoinTimeTest, Dot1dPortGarpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dPortGarpTableINDEX, 1, 0, 0, "20"},

{{13,Dot1dPortGarpLeaveTime}, GetNextIndexDot1dPortGarpTable, Dot1dPortGarpLeaveTimeGet, Dot1dPortGarpLeaveTimeSet, Dot1dPortGarpLeaveTimeTest, Dot1dPortGarpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dPortGarpTableINDEX, 1, 0, 0, "60"},

{{13,Dot1dPortGarpLeaveAllTime}, GetNextIndexDot1dPortGarpTable, Dot1dPortGarpLeaveAllTimeGet, Dot1dPortGarpLeaveAllTimeSet, Dot1dPortGarpLeaveAllTimeTest, Dot1dPortGarpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dPortGarpTableINDEX, 1, 0, 0, "1000"},

{{13,Dot1dPortGmrpStatus}, GetNextIndexDot1dPortGmrpTable, Dot1dPortGmrpStatusGet, Dot1dPortGmrpStatusSet, Dot1dPortGmrpStatusTest, Dot1dPortGmrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dPortGmrpTableINDEX, 1, 0, 0, "1"},

{{13,Dot1dPortGmrpFailedRegistrations}, GetNextIndexDot1dPortGmrpTable, Dot1dPortGmrpFailedRegistrationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dPortGmrpTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dPortGmrpLastPduOrigin}, GetNextIndexDot1dPortGmrpTable, Dot1dPortGmrpLastPduOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1dPortGmrpTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dPortRestrictedGroupRegistration}, GetNextIndexDot1dPortGmrpTable, Dot1dPortRestrictedGroupRegistrationGet, Dot1dPortRestrictedGroupRegistrationSet, Dot1dPortRestrictedGroupRegistrationTest, Dot1dPortGmrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dPortGmrpTableINDEX, 1, 0, 0, "2"},
};
tMibData stdbrgEntry = { 24, stdbrgMibEntry };
#endif /* _STDBRGDB_H */

