/*
   ISS Wrapper header
   module RSTP-MIB

*/
#ifndef _H_i_RSTP_MIB
#define _H_i_RSTP_MIB


#define CENTI_SECONDS 100

/********************************************************************
 * FUNCTION NcDot1dStpProtocolSpecificationGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpProtocolSpecificationGet (
				INT4 *pi4Dot1dStpProtocolSpecification );

/********************************************************************
 * FUNCTION NcDot1dStpPrioritySet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpPrioritySet (
				INT4 i4Dot1dStpPriority );

/********************************************************************
 * FUNCTION NcDot1dStpPriorityTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpPriorityTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpPriority );

/********************************************************************
 * FUNCTION NcDot1dStpTimeSinceTopologyChangeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpTimeSinceTopologyChangeGet (
				UINT4 *pu4Dot1dStpTimeSinceTopologyChange );

/********************************************************************
 * FUNCTION NcDot1dStpTopChangesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpTopChangesGet (
				UINT4 *pu4Dot1dStpTopChanges );

/********************************************************************
 * FUNCTION NcDot1dStpDesignatedRootGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpDesignatedRootGet (
				UINT1 *pau1Dot1dStpDesignatedRoot );

/********************************************************************
 * FUNCTION NcDot1dStpRootCostGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpRootCostGet (
				INT4 *pi4Dot1dStpRootCost );

/********************************************************************
 * FUNCTION NcDot1dStpRootPortGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpRootPortGet (
				INT4 *pi4Dot1dStpRootPort );

/********************************************************************
 * FUNCTION NcDot1dStpMaxAgeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpMaxAgeGet (
				INT4 *pi4Dot1dStpMaxAge );

/********************************************************************
 * FUNCTION NcDot1dStpHelloTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpHelloTimeGet (
				INT4 *pi4Dot1dStpHelloTime );

/********************************************************************
 * FUNCTION NcDot1dStpHoldTimeGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpHoldTimeGet (
				INT4 *pi4Dot1dStpHoldTime );

/********************************************************************
 * FUNCTION NcDot1dStpForwardDelayGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpForwardDelayGet (
				INT4 *pi4Dot1dStpForwardDelay );

/********************************************************************
 * FUNCTION NcDot1dStpBridgeMaxAgeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpBridgeMaxAgeSet (
				INT4 i4Dot1dStpBridgeMaxAge );

/********************************************************************
 * FUNCTION NcDot1dStpBridgeMaxAgeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpBridgeMaxAgeTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpBridgeMaxAge );

/********************************************************************
 * FUNCTION NcDot1dStpBridgeHelloTimeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpBridgeHelloTimeSet (
				INT4 i4Dot1dStpBridgeHelloTime );

/********************************************************************
 * FUNCTION NcDot1dStpBridgeHelloTimeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpBridgeHelloTimeTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpBridgeHelloTime );

/********************************************************************
 * FUNCTION NcDot1dStpBridgeForwardDelaySet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpBridgeForwardDelaySet (
				INT4 i4Dot1dStpBridgeForwardDelay );

/********************************************************************
 * FUNCTION NcDot1dStpBridgeForwardDelayTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpBridgeForwardDelayTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpBridgeForwardDelay );

/********************************************************************
 * FUNCTION NcDot1dStpVersionSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpVersionSet (
				INT4 i4Dot1dStpVersion );

/********************************************************************
 * FUNCTION NcDot1dStpVersionTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpVersionTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpVersion );

/********************************************************************
 * FUNCTION NcDot1dStpTxHoldCountSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpTxHoldCountSet (
				INT4 i4Dot1dStpTxHoldCount );

/********************************************************************
 * FUNCTION NcDot1dStpTxHoldCountTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
extern INT1 NcDot1dStpTxHoldCountTest (UINT4 *pu4Error,
				INT4 i4Dot1dStpTxHoldCount );

/* END i_RSTP_MIB.c */


#endif
