/* $Id: fsm1adnc.h,v 1.1 2016/06/21 09:54:51 siva Exp $
    ISS Wrapper header
    module ARICENT-MIDOT1AD-MIB

 */
#ifndef _H_i_ARICENT_MIDOT1AD_MIB
#define _H_i_ARICENT_MIDOT1AD_MIB


/********************************************************************
* FUNCTION NcDot1adMIPortPcpSelectionRowSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPortPcpSelectionRowSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortPcpSelectionRow );

/********************************************************************
* FUNCTION NcDot1adMIPortPcpSelectionRowTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPortPcpSelectionRowTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortPcpSelectionRow );

/********************************************************************
* FUNCTION NcDot1adMIPortUseDeiSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPortUseDeiSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortUseDei );

/********************************************************************
* FUNCTION NcDot1adMIPortUseDeiTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPortUseDeiTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortUseDei );

/********************************************************************
* FUNCTION NcDot1adMIPortReqDropEncodingSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPortReqDropEncodingSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortReqDropEncoding );

/********************************************************************
* FUNCTION NcDot1adMIPortReqDropEncodingTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPortReqDropEncodingTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortReqDropEncoding );

/********************************************************************
* FUNCTION NcDot1adMIPortSVlanPriorityTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPortSVlanPriorityTypeSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortSVlanPriorityType );

/********************************************************************
* FUNCTION NcDot1adMIPortSVlanPriorityTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPortSVlanPriorityTypeTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortSVlanPriorityType );

/********************************************************************
* FUNCTION NcDot1adMIPortSVlanPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPortSVlanPrioritySet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortSVlanPriority );

/********************************************************************
* FUNCTION NcDot1adMIPortSVlanPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPortSVlanPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPortSVlanPriority );

/********************************************************************
* FUNCTION NcDot1adMIVidTranslationRelayVidSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIVidTranslationRelayVidSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIVidTranslationLocalVid,
                INT4 i4Dot1adMIVidTranslationRelayVid );

/********************************************************************
* FUNCTION NcDot1adMIVidTranslationRelayVidTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIVidTranslationRelayVidTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIVidTranslationLocalVid,
                INT4 i4Dot1adMIVidTranslationRelayVid );

/********************************************************************
* FUNCTION NcDot1adMIVidTranslationRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIVidTranslationRowStatusSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIVidTranslationLocalVid,
                INT4 i4Dot1adMIVidTranslationRowStatus );

/********************************************************************
* FUNCTION NcDot1adMIVidTranslationRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIVidTranslationRowStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIVidTranslationLocalVid,
                INT4 i4Dot1adMIVidTranslationRowStatus );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVidSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationSVidSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVid );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVidTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationSVidTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVid );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationUntaggedPepSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationUntaggedPepSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationUntaggedPep );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationUntaggedPepTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationUntaggedPepTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationUntaggedPep );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationUntaggedCepSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationUntaggedCepSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationUntaggedCep );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationUntaggedCepTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationUntaggedCepTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationUntaggedCep );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationRowStatusSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationRowStatus );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationRowStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationRowStatus );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVlanPriorityTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationSVlanPriorityTypeSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVlanPriorityType );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVlanPriorityTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationSVlanPriorityTypeTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVlanPriorityType );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVlanPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationSVlanPrioritySet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVlanPriority );

/********************************************************************
* FUNCTION NcDot1adMICVidRegistrationSVlanPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMICVidRegistrationSVlanPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationCVid,
                INT4 i4Dot1adMICVidRegistrationSVlanPriority );

/********************************************************************
* FUNCTION NcDot1adMIPepPvidSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPepPvidSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepPvid );

/********************************************************************
* FUNCTION NcDot1adMIPepPvidTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPepPvidTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepPvid );

/********************************************************************
* FUNCTION NcDot1adMIPepDefaultUserPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPepDefaultUserPrioritySet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepDefaultUserPriority );

/********************************************************************
* FUNCTION NcDot1adMIPepDefaultUserPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPepDefaultUserPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepDefaultUserPriority );

/********************************************************************
* FUNCTION NcDot1adMIPepAccptableFrameTypesSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPepAccptableFrameTypesSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepAccptableFrameTypes );

/********************************************************************
* FUNCTION NcDot1adMIPepAccptableFrameTypesTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPepAccptableFrameTypesTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepAccptableFrameTypes );

/********************************************************************
* FUNCTION NcDot1adMIPepIngressFilteringSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPepIngressFilteringSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepIngressFiltering );

/********************************************************************
* FUNCTION NcDot1adMIPepIngressFilteringTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPepIngressFilteringTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIPepIngressFiltering );

/********************************************************************
* FUNCTION NcDot1adMIServicePriorityRegenRegeneratedPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIServicePriorityRegenRegeneratedPrioritySet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIServicePriorityRegenReceivedPriority,
                INT4 i4Dot1adMIServicePriorityRegenRegeneratedPriority );

/********************************************************************
* FUNCTION NcDot1adMIServicePriorityRegenRegeneratedPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIServicePriorityRegenRegeneratedPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMICVidRegistrationSVid,
                INT4 i4Dot1adMIServicePriorityRegenReceivedPriority,
                INT4 i4Dot1adMIServicePriorityRegenRegeneratedPriority );

/********************************************************************
* FUNCTION NcDot1adMIPcpDecodingPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPcpDecodingPrioritySet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpDecodingPcpSelRow,
                INT4 i4Dot1adMIPcpDecodingPcpValue,
                INT4 i4Dot1adMIPcpDecodingPriority );

/********************************************************************
* FUNCTION NcDot1adMIPcpDecodingPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPcpDecodingPriorityTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpDecodingPcpSelRow,
                INT4 i4Dot1adMIPcpDecodingPcpValue,
                INT4 i4Dot1adMIPcpDecodingPriority );

/********************************************************************
* FUNCTION NcDot1adMIPcpDecodingDropEligibleSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPcpDecodingDropEligibleSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpDecodingPcpSelRow,
                INT4 i4Dot1adMIPcpDecodingPcpValue,
                INT4 i4Dot1adMIPcpDecodingDropEligible );

/********************************************************************
* FUNCTION NcDot1adMIPcpDecodingDropEligibleTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPcpDecodingDropEligibleTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpDecodingPcpSelRow,
                INT4 i4Dot1adMIPcpDecodingPcpValue,
                INT4 i4Dot1adMIPcpDecodingDropEligible );

/********************************************************************
* FUNCTION NcDot1adMIPcpEncodingPcpValueSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPcpEncodingPcpValueSet (
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpEncodingPcpSelRow,
                INT4 i4Dot1adMIPcpEncodingPriority,
                INT4 i4Dot1adMIPcpEncodingDropEligible,
                INT4 i4Dot1adMIPcpEncodingPcpValue );

/********************************************************************
* FUNCTION NcDot1adMIPcpEncodingPcpValueTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1adMIPcpEncodingPcpValueTest (UINT4 *pu4Error,
                INT4 i4Dot1adMIPortNum,
                INT4 i4Dot1adMIPcpEncodingPcpSelRow,
                INT4 i4Dot1adMIPcpEncodingPriority,
                INT4 i4Dot1adMIPcpEncodingDropEligible,
                INT4 i4Dot1adMIPcpEncodingPcpValue );

/* END i_ARICENT_MIDOT1AD_MIB.c */


#endif
