/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1evdb.h,v 1.2 2016/07/16 11:15:03 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STD1EVDB_H
#define _STD1EVDB_H

UINT1 Ieee8021BridgeEvbSbpTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgeEvbVSIDBTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 Ieee8021BridgeEvbVSIDBMacTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgeEvbUAPConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgeEvbCAPConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgeEvbURPTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgeEvbEcpTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 std1ev [] ={1,3,111,2,802,1,1,24};
tSNMP_OID_TYPE std1evOID = {8, std1ev};


UINT4 Ieee8021BridgeEvbSysType [ ] ={1,3,111,2,802,1,1,24,1,1,1};
UINT4 Ieee8021BridgeEvbSysNumExternalPorts [ ] ={1,3,111,2,802,1,1,24,1,1,2};
UINT4 Ieee8021BridgeEvbSysEvbLldpTxEnable [ ] ={1,3,111,2,802,1,1,24,1,1,3};
UINT4 Ieee8021BridgeEvbSysEvbLldpManual [ ] ={1,3,111,2,802,1,1,24,1,1,4};
UINT4 Ieee8021BridgeEvbSysEvbLldpGidCapable [ ] ={1,3,111,2,802,1,1,24,1,1,5};
UINT4 Ieee8021BridgeEvbSysEcpAckTimer [ ] ={1,3,111,2,802,1,1,24,1,1,6};
UINT4 Ieee8021BridgeEvbSysEcpMaxRetries [ ] ={1,3,111,2,802,1,1,24,1,1,7};
UINT4 Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay [ ] ={1,3,111,2,802,1,1,24,1,1,8};
UINT4 Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive [ ] ={1,3,111,2,802,1,1,24,1,1,9};
UINT4 Ieee8021BridgeEvbSbpComponentID [ ] ={1,3,111,2,802,1,1,24,1,1,10,1,1};
UINT4 Ieee8021BridgeEvbSbpPortNumber [ ] ={1,3,111,2,802,1,1,24,1,1,10,1,2};
UINT4 Ieee8021BridgeEvbSbpLldpManual [ ] ={1,3,111,2,802,1,1,24,1,1,10,1,3};
UINT4 Ieee8021BridgeEvbSbpVdpOperRsrcWaitDelay [ ] ={1,3,111,2,802,1,1,24,1,1,10,1,4};
UINT4 Ieee8021BridgeEvbSbpVdpOperReinitKeepAlive [ ] ={1,3,111,2,802,1,1,24,1,1,10,1,5};
UINT4 Ieee8021BridgeEvbSbpVdpOperToutKeepAlive [ ] ={1,3,111,2,802,1,1,24,1,1,10,1,6};
UINT4 Ieee8021BridgeEvbVSIComponentID [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,1};
UINT4 Ieee8021BridgeEvbVSIPortNumber [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,2};
UINT4 Ieee8021BridgeEvbVSIIDType [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,3};
UINT4 Ieee8021BridgeEvbVSIID [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,4};
UINT4 Ieee8021BridgeEvbVSITimeSinceCreate [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,5};
UINT4 Ieee8021BridgeEvbVsiVdpOperCmd [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,6};
UINT4 Ieee8021BridgeEvbVsiOperRevert [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,7};
UINT4 Ieee8021BridgeEvbVsiOperHard [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,8};
UINT4 Ieee8021BridgeEvbVsiOperReason [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,9};
UINT4 Ieee8021BridgeEvbVSIMgrID [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,10};
UINT4 Ieee8021BridgeEvbVSIType [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,11};
UINT4 Ieee8021BridgeEvbVSITypeVersion [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,12};
UINT4 Ieee8021BridgeEvbVSIMvFormat [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,13};
UINT4 Ieee8021BridgeEvbVSINumMACs [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,14};
UINT4 Ieee8021BridgeEvbVDPMachineState [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,15};
UINT4 Ieee8021BridgeEvbVDPCommandsSucceeded [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,16};
UINT4 Ieee8021BridgeEvbVDPCommandsFailed [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,17};
UINT4 Ieee8021BridgeEvbVDPCommandReverts [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,18};
UINT4 Ieee8021BridgeEvbVDPCounterDiscontinuity [ ] ={1,3,111,2,802,1,1,24,1,2,1,1,19};
UINT4 Ieee8021BridgeEvbGroupID [ ] ={1,3,111,2,802,1,1,24,1,2,2,1,1};
UINT4 Ieee8021BridgeEvbVSIMac [ ] ={1,3,111,2,802,1,1,24,1,2,2,1,2};
UINT4 Ieee8021BridgeEvbVSIVlanId [ ] ={1,3,111,2,802,1,1,24,1,2,2,1,3};
UINT4 Ieee8021BridgeEvbUAPComponentId [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,1};
UINT4 Ieee8021BridgeEvbUAPPort [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,2};
UINT4 Ieee8021BridgeEvbUapConfigIfIndex [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,3};
UINT4 Ieee8021BridgeEvbUAPSchCdcpAdminEnable [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,4};
UINT4 Ieee8021BridgeEvbUAPSchAdminCDCPRole [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,5};
UINT4 Ieee8021BridgeEvbUAPSchAdminCDCPChanCap [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,6};
UINT4 Ieee8021BridgeEvbUAPSchOperCDCPChanCap [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,7};
UINT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,8};
UINT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,9};
UINT4 Ieee8021BridgeEvbUAPSchOperState [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,10};
UINT4 Ieee8021BridgeEvbSchCdcpRemoteEnabled [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,11};
UINT4 Ieee8021BridgeEvbSchCdcpRemoteRole [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,12};
UINT4 Ieee8021BridgeEvbUAPConfigStorageType [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,13};
UINT4 Ieee8021BridgeEvbUAPConfigRowStatus [ ] ={1,3,111,2,802,1,1,24,1,3,1,1,14};
UINT4 Ieee8021BridgeEvbSchID [ ] ={1,3,111,2,802,1,1,24,1,3,2,1,1};
UINT4 Ieee8021BridgeEvbCAPComponentId [ ] ={1,3,111,2,802,1,1,24,1,3,2,1,2};
UINT4 Ieee8021BridgeEvbCapConfigIfIndex [ ] ={1,3,111,2,802,1,1,24,1,3,2,1,3};
UINT4 Ieee8021BridgeEvbCAPPort [ ] ={1,3,111,2,802,1,1,24,1,3,2,1,4};
UINT4 Ieee8021BridgeEvbCAPSChannelID [ ] ={1,3,111,2,802,1,1,24,1,3,2,1,5};
UINT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPCompID [ ] ={1,3,111,2,802,1,1,24,1,3,2,1,6};
UINT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPPort [ ] ={1,3,111,2,802,1,1,24,1,3,2,1,7};
UINT4 Ieee8021BridgeEvbCAPRowStatus [ ] ={1,3,111,2,802,1,1,24,1,3,2,1,8};
UINT4 Ieee8021BridgeEvbURPComponentId [ ] ={1,3,111,2,802,1,1,24,1,3,3,1,1};
UINT4 Ieee8021BridgeEvbURPPort [ ] ={1,3,111,2,802,1,1,24,1,3,3,1,2};
UINT4 Ieee8021BridgeEvbURPIfIndex [ ] ={1,3,111,2,802,1,1,24,1,3,3,1,3};
UINT4 Ieee8021BridgeEvbURPBindToISSPort [ ] ={1,3,111,2,802,1,1,24,1,3,3,1,4};
UINT4 Ieee8021BridgeEvbURPLldpManual [ ] ={1,3,111,2,802,1,1,24,1,3,3,1,6};
UINT4 Ieee8021BridgeEvbURPVdpOperRsrcWaitDelay [ ] ={1,3,111,2,802,1,1,24,1,3,3,1,9};
UINT4 Ieee8021BridgeEvbURPVdpOperRespWaitDelay [ ] ={1,3,111,2,802,1,1,24,1,3,3,1,10};
UINT4 Ieee8021BridgeEvbURPVdpOperReinitKeepAlive [ ] ={1,3,111,2,802,1,1,24,1,3,3,1,11};
UINT4 Ieee8021BridgeEvbEcpComponentId [ ] ={1,3,111,2,802,1,1,24,1,3,4,1,1};
UINT4 Ieee8021BridgeEvbEcpPort [ ] ={1,3,111,2,802,1,1,24,1,3,4,1,2};
UINT4 Ieee8021BridgeEvbEcpOperAckTimerInit [ ] ={1,3,111,2,802,1,1,24,1,3,4,1,3};
UINT4 Ieee8021BridgeEvbEcpOperMaxRetries [ ] ={1,3,111,2,802,1,1,24,1,3,4,1,4};
UINT4 Ieee8021BridgeEvbEcpTxFrameCount [ ] ={1,3,111,2,802,1,1,24,1,3,4,1,5};
UINT4 Ieee8021BridgeEvbEcpTxRetryCount [ ] ={1,3,111,2,802,1,1,24,1,3,4,1,6};
UINT4 Ieee8021BridgeEvbEcpTxFailures [ ] ={1,3,111,2,802,1,1,24,1,3,4,1,7};
UINT4 Ieee8021BridgeEvbEcpRxFrameCount [ ] ={1,3,111,2,802,1,1,24,1,3,4,1,8};




tMbDbEntry std1evMibEntry[]= {

{{11,Ieee8021BridgeEvbSysType}, NULL, Ieee8021BridgeEvbSysTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Ieee8021BridgeEvbSysNumExternalPorts}, NULL, Ieee8021BridgeEvbSysNumExternalPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Ieee8021BridgeEvbSysEvbLldpTxEnable}, NULL, Ieee8021BridgeEvbSysEvbLldpTxEnableGet, Ieee8021BridgeEvbSysEvbLldpTxEnableSet, Ieee8021BridgeEvbSysEvbLldpTxEnableTest, Ieee8021BridgeEvbSysEvbLldpTxEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,Ieee8021BridgeEvbSysEvbLldpManual}, NULL, Ieee8021BridgeEvbSysEvbLldpManualGet, Ieee8021BridgeEvbSysEvbLldpManualSet, Ieee8021BridgeEvbSysEvbLldpManualTest, Ieee8021BridgeEvbSysEvbLldpManualDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,Ieee8021BridgeEvbSysEvbLldpGidCapable}, NULL, Ieee8021BridgeEvbSysEvbLldpGidCapableGet, Ieee8021BridgeEvbSysEvbLldpGidCapableSet, Ieee8021BridgeEvbSysEvbLldpGidCapableTest, Ieee8021BridgeEvbSysEvbLldpGidCapableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Ieee8021BridgeEvbSysEcpAckTimer}, NULL, Ieee8021BridgeEvbSysEcpAckTimerGet, Ieee8021BridgeEvbSysEcpAckTimerSet, Ieee8021BridgeEvbSysEcpAckTimerTest, Ieee8021BridgeEvbSysEcpAckTimerDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Ieee8021BridgeEvbSysEcpMaxRetries}, NULL, Ieee8021BridgeEvbSysEcpMaxRetriesGet, Ieee8021BridgeEvbSysEcpMaxRetriesSet, Ieee8021BridgeEvbSysEcpMaxRetriesTest, Ieee8021BridgeEvbSysEcpMaxRetriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{11,Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelay}, NULL, Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelayGet, Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelaySet, Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelayTest, Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelayDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Ieee8021BridgeEvbSysVdpDfltReinitKeepAlive}, NULL, Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveGet, Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveSet, Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveTest, Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,Ieee8021BridgeEvbSbpComponentID}, GetNextIndexIeee8021BridgeEvbSbpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeEvbSbpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbSbpPortNumber}, GetNextIndexIeee8021BridgeEvbSbpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeEvbSbpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbSbpLldpManual}, GetNextIndexIeee8021BridgeEvbSbpTable, Ieee8021BridgeEvbSbpLldpManualGet, Ieee8021BridgeEvbSbpLldpManualSet, Ieee8021BridgeEvbSbpLldpManualTest, Ieee8021BridgeEvbSbpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeEvbSbpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbSbpVdpOperRsrcWaitDelay}, GetNextIndexIeee8021BridgeEvbSbpTable, Ieee8021BridgeEvbSbpVdpOperRsrcWaitDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbSbpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbSbpVdpOperReinitKeepAlive}, GetNextIndexIeee8021BridgeEvbSbpTable, Ieee8021BridgeEvbSbpVdpOperReinitKeepAliveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbSbpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbSbpVdpOperToutKeepAlive}, GetNextIndexIeee8021BridgeEvbSbpTable, Ieee8021BridgeEvbSbpVdpOperToutKeepAliveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbSbpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSIComponentID}, GetNextIndexIeee8021BridgeEvbVSIDBTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSIPortNumber}, GetNextIndexIeee8021BridgeEvbVSIDBTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSIIDType}, GetNextIndexIeee8021BridgeEvbVSIDBTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSIID}, GetNextIndexIeee8021BridgeEvbVSIDBTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSITimeSinceCreate}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVSITimeSinceCreateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVsiVdpOperCmd}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVsiVdpOperCmdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVsiOperRevert}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVsiOperRevertGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVsiOperHard}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVsiOperHardGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVsiOperReason}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVsiOperReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSIMgrID}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVSIMgrIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSIType}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVSITypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSITypeVersion}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVSITypeVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSIMvFormat}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVSIMvFormatGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSINumMACs}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVSINumMACsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVDPMachineState}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVDPMachineStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVDPCommandsSucceeded}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVDPCommandsSucceededGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVDPCommandsFailed}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVDPCommandsFailedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVDPCommandReverts}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVDPCommandRevertsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVDPCounterDiscontinuity}, GetNextIndexIeee8021BridgeEvbVSIDBTable, Ieee8021BridgeEvbVDPCounterDiscontinuityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Ieee8021BridgeEvbVSIDBTableINDEX, 4, 0, 0, NULL},

{{13,Ieee8021BridgeEvbGroupID}, GetNextIndexIeee8021BridgeEvbVSIDBMacTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeEvbVSIDBMacTableINDEX, 7, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSIMac}, GetNextIndexIeee8021BridgeEvbVSIDBMacTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Ieee8021BridgeEvbVSIDBMacTableINDEX, 7, 0, 0, NULL},

{{13,Ieee8021BridgeEvbVSIVlanId}, GetNextIndexIeee8021BridgeEvbVSIDBMacTable, Ieee8021BridgeEvbVSIVlanIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbVSIDBMacTableINDEX, 7, 0, 0, NULL},

{{13,Ieee8021BridgeEvbUAPComponentId}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUAPComponentIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeEvbUAPPort}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUAPPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeEvbUapConfigIfIndex}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUapConfigIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeEvbUAPSchCdcpAdminEnable}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUAPSchCdcpAdminEnableGet, Ieee8021BridgeEvbUAPSchCdcpAdminEnableSet, Ieee8021BridgeEvbUAPSchCdcpAdminEnableTest, Ieee8021BridgeEvbUAPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeEvbUAPSchAdminCDCPRole}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUAPSchAdminCDCPRoleGet, Ieee8021BridgeEvbUAPSchAdminCDCPRoleSet, Ieee8021BridgeEvbUAPSchAdminCDCPRoleTest, Ieee8021BridgeEvbUAPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, "1"},

{{13,Ieee8021BridgeEvbUAPSchAdminCDCPChanCap}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUAPSchAdminCDCPChanCapGet, Ieee8021BridgeEvbUAPSchAdminCDCPChanCapSet, Ieee8021BridgeEvbUAPSchAdminCDCPChanCapTest, Ieee8021BridgeEvbUAPConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeEvbUAPSchOperCDCPChanCap}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUAPSchOperCDCPChanCapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLow}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLowGet, Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLowSet, Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLowTest, Ieee8021BridgeEvbUAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHigh}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHighGet, Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHighSet, Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHighTest, Ieee8021BridgeEvbUAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeEvbUAPSchOperState}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUAPSchOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeEvbSchCdcpRemoteEnabled}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbSchCdcpRemoteEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeEvbSchCdcpRemoteRole}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbSchCdcpRemoteRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021BridgeEvbUAPConfigStorageType}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUAPConfigStorageTypeGet, Ieee8021BridgeEvbUAPConfigStorageTypeSet, Ieee8021BridgeEvbUAPConfigStorageTypeTest, Ieee8021BridgeEvbUAPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 0, "3"},

{{13,Ieee8021BridgeEvbUAPConfigRowStatus}, GetNextIndexIeee8021BridgeEvbUAPConfigTable, Ieee8021BridgeEvbUAPConfigRowStatusGet, Ieee8021BridgeEvbUAPConfigRowStatusSet, Ieee8021BridgeEvbUAPConfigRowStatusTest, Ieee8021BridgeEvbUAPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeEvbUAPConfigTableINDEX, 1, 0, 1, NULL},

{{13,Ieee8021BridgeEvbSchID}, GetNextIndexIeee8021BridgeEvbCAPConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeEvbCAPConfigTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbCAPComponentId}, GetNextIndexIeee8021BridgeEvbCAPConfigTable, Ieee8021BridgeEvbCAPComponentIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbCAPConfigTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbCapConfigIfIndex}, GetNextIndexIeee8021BridgeEvbCAPConfigTable, Ieee8021BridgeEvbCapConfigIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021BridgeEvbCAPConfigTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbCAPPort}, GetNextIndexIeee8021BridgeEvbCAPConfigTable, Ieee8021BridgeEvbCAPPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbCAPConfigTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbCAPSChannelID}, GetNextIndexIeee8021BridgeEvbCAPConfigTable, Ieee8021BridgeEvbCAPSChannelIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbCAPConfigTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbCAPAssociateSBPOrURPCompID}, GetNextIndexIeee8021BridgeEvbCAPConfigTable, Ieee8021BridgeEvbCAPAssociateSBPOrURPCompIDGet, Ieee8021BridgeEvbCAPAssociateSBPOrURPCompIDSet, Ieee8021BridgeEvbCAPAssociateSBPOrURPCompIDTest, Ieee8021BridgeEvbCAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgeEvbCAPConfigTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbCAPAssociateSBPOrURPPort}, GetNextIndexIeee8021BridgeEvbCAPConfigTable, Ieee8021BridgeEvbCAPAssociateSBPOrURPPortGet, Ieee8021BridgeEvbCAPAssociateSBPOrURPPortSet, Ieee8021BridgeEvbCAPAssociateSBPOrURPPortTest, Ieee8021BridgeEvbCAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgeEvbCAPConfigTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbCAPRowStatus}, GetNextIndexIeee8021BridgeEvbCAPConfigTable, Ieee8021BridgeEvbCAPRowStatusGet, Ieee8021BridgeEvbCAPRowStatusSet, Ieee8021BridgeEvbCAPRowStatusTest, Ieee8021BridgeEvbCAPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeEvbCAPConfigTableINDEX, 2, 0, 1, NULL},

{{13,Ieee8021BridgeEvbURPComponentId}, GetNextIndexIeee8021BridgeEvbURPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeEvbURPTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbURPPort}, GetNextIndexIeee8021BridgeEvbURPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeEvbURPTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbURPIfIndex}, GetNextIndexIeee8021BridgeEvbURPTable, Ieee8021BridgeEvbURPIfIndexGet, Ieee8021BridgeEvbURPIfIndexSet, Ieee8021BridgeEvbURPIfIndexTest, Ieee8021BridgeEvbURPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeEvbURPTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbURPBindToISSPort}, GetNextIndexIeee8021BridgeEvbURPTable, Ieee8021BridgeEvbURPBindToISSPortGet, Ieee8021BridgeEvbURPBindToISSPortSet, Ieee8021BridgeEvbURPBindToISSPortTest, Ieee8021BridgeEvbURPTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgeEvbURPTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbURPLldpManual}, GetNextIndexIeee8021BridgeEvbURPTable, Ieee8021BridgeEvbURPLldpManualGet, Ieee8021BridgeEvbURPLldpManualSet, Ieee8021BridgeEvbURPLldpManualTest, Ieee8021BridgeEvbURPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgeEvbURPTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbURPVdpOperRsrcWaitDelay}, GetNextIndexIeee8021BridgeEvbURPTable, Ieee8021BridgeEvbURPVdpOperRsrcWaitDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbURPTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbURPVdpOperRespWaitDelay}, GetNextIndexIeee8021BridgeEvbURPTable, Ieee8021BridgeEvbURPVdpOperRespWaitDelayGet, Ieee8021BridgeEvbURPVdpOperRespWaitDelaySet, Ieee8021BridgeEvbURPVdpOperRespWaitDelayTest, Ieee8021BridgeEvbURPTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgeEvbURPTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbURPVdpOperReinitKeepAlive}, GetNextIndexIeee8021BridgeEvbURPTable, Ieee8021BridgeEvbURPVdpOperReinitKeepAliveGet, Ieee8021BridgeEvbURPVdpOperReinitKeepAliveSet, Ieee8021BridgeEvbURPVdpOperReinitKeepAliveTest, Ieee8021BridgeEvbURPTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021BridgeEvbURPTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbEcpComponentId}, GetNextIndexIeee8021BridgeEvbEcpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeEvbEcpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbEcpPort}, GetNextIndexIeee8021BridgeEvbEcpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021BridgeEvbEcpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbEcpOperAckTimerInit}, GetNextIndexIeee8021BridgeEvbEcpTable, Ieee8021BridgeEvbEcpOperAckTimerInitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbEcpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbEcpOperMaxRetries}, GetNextIndexIeee8021BridgeEvbEcpTable, Ieee8021BridgeEvbEcpOperMaxRetriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021BridgeEvbEcpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbEcpTxFrameCount}, GetNextIndexIeee8021BridgeEvbEcpTable, Ieee8021BridgeEvbEcpTxFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ieee8021BridgeEvbEcpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbEcpTxRetryCount}, GetNextIndexIeee8021BridgeEvbEcpTable, Ieee8021BridgeEvbEcpTxRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ieee8021BridgeEvbEcpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbEcpTxFailures}, GetNextIndexIeee8021BridgeEvbEcpTable, Ieee8021BridgeEvbEcpTxFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ieee8021BridgeEvbEcpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgeEvbEcpRxFrameCount}, GetNextIndexIeee8021BridgeEvbEcpTable, Ieee8021BridgeEvbEcpRxFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ieee8021BridgeEvbEcpTableINDEX, 2, 0, 0, NULL},
};
tMibData std1evEntry = { 75, std1evMibEntry };

#endif /* _STD1EVDB_H */



