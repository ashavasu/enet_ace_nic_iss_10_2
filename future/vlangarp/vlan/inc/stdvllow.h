/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdvllow.h,v 1.8 2012/05/31 12:45:36 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qVlanVersionNumber ARG_LIST((INT4 *));

INT1
nmhGetDot1qMaxVlanId ARG_LIST((INT4 *));

INT1
nmhGetDot1qMaxSupportedVlans ARG_LIST((UINT4 *));

INT1
nmhGetDot1qNumVlans ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Dot1qFdbTable. */
INT1
nmhValidateIndexInstanceDot1qFdbTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qFdbTable  */

INT1
nmhGetFirstIndexDot1qFdbTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qFdbTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qFdbDynamicCount ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot1qTpFdbTable. */
INT1
nmhValidateIndexInstanceDot1qTpFdbTable ARG_LIST((UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Dot1qTpFdbTable  */

INT1
nmhGetFirstIndexDot1qTpFdbTable ARG_LIST((UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qTpFdbTable ARG_LIST((UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qTpFdbPort ARG_LIST((UINT4  , tMacAddr ,INT4 *));

INT1
nmhGetDot1qTpFdbStatus ARG_LIST((UINT4  , tMacAddr ,INT4 *));

/* Proto Validate Index Instance for Dot1qTpGroupTable. */
INT1
nmhValidateIndexInstanceDot1qTpGroupTable ARG_LIST((UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Dot1qTpGroupTable  */

INT1
nmhGetFirstIndexDot1qTpGroupTable ARG_LIST((UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qTpGroupTable ARG_LIST((UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qTpGroupEgressPorts ARG_LIST((UINT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qTpGroupLearnt ARG_LIST((UINT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot1qForwardAllTable. */
INT1
nmhValidateIndexInstanceDot1qForwardAllTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qForwardAllTable  */

INT1
nmhGetFirstIndexDot1qForwardAllTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qForwardAllTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qForwardAllPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qForwardAllStaticPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qForwardAllForbiddenPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qForwardAllStaticPorts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1qForwardAllForbiddenPorts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qForwardAllStaticPorts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1qForwardAllForbiddenPorts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qForwardAllTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qForwardUnregisteredTable. */
INT1
nmhValidateIndexInstanceDot1qForwardUnregisteredTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qForwardUnregisteredTable  */

INT1
nmhGetFirstIndexDot1qForwardUnregisteredTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qForwardUnregisteredTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qForwardUnregisteredPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qForwardUnregisteredStaticPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qForwardUnregisteredForbiddenPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qForwardUnregisteredStaticPorts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1qForwardUnregisteredForbiddenPorts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qForwardUnregisteredStaticPorts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1qForwardUnregisteredForbiddenPorts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qForwardUnregisteredTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qStaticUnicastTable. */
INT1
nmhValidateIndexInstanceDot1qStaticUnicastTable ARG_LIST((UINT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qStaticUnicastTable  */

INT1
nmhGetFirstIndexDot1qStaticUnicastTable ARG_LIST((UINT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qStaticUnicastTable ARG_LIST((UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qStaticUnicastAllowedToGoTo ARG_LIST((UINT4  , tMacAddr  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qStaticUnicastStatus ARG_LIST((UINT4  , tMacAddr  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qStaticUnicastAllowedToGoTo ARG_LIST((UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1qStaticUnicastStatus ARG_LIST((UINT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qStaticUnicastAllowedToGoTo ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1qStaticUnicastStatus ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qStaticUnicastTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qStaticMulticastTable. */
INT1
nmhValidateIndexInstanceDot1qStaticMulticastTable ARG_LIST((UINT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qStaticMulticastTable  */

INT1
nmhGetFirstIndexDot1qStaticMulticastTable ARG_LIST((UINT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qStaticMulticastTable ARG_LIST((UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qStaticMulticastStaticEgressPorts ARG_LIST((UINT4  , tMacAddr  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qStaticMulticastForbiddenEgressPorts ARG_LIST((UINT4  , tMacAddr  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qStaticMulticastStatus ARG_LIST((UINT4  , tMacAddr  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qStaticMulticastStaticEgressPorts ARG_LIST((UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1qStaticMulticastForbiddenEgressPorts ARG_LIST((UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1qStaticMulticastStatus ARG_LIST((UINT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qStaticMulticastStaticEgressPorts ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1qStaticMulticastForbiddenEgressPorts ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1qStaticMulticastStatus ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qStaticMulticastTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qVlanNumDeletes ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Dot1qVlanCurrentTable. */
INT1
nmhValidateIndexInstanceDot1qVlanCurrentTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qVlanCurrentTable  */

INT1
nmhGetFirstIndexDot1qVlanCurrentTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qVlanCurrentTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qVlanFdbId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1qVlanCurrentEgressPorts ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qVlanCurrentUntaggedPorts ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qVlanStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDot1qVlanCreationTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot1qVlanStaticTable. */
INT1
nmhValidateIndexInstanceDot1qVlanStaticTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qVlanStaticTable  */

INT1
nmhGetFirstIndexDot1qVlanStaticTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qVlanStaticTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qVlanStaticName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qVlanStaticEgressPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qVlanForbiddenEgressPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qVlanStaticUntaggedPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1qVlanStaticRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qVlanStaticName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1qVlanStaticEgressPorts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1qVlanForbiddenEgressPorts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1qVlanStaticUntaggedPorts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1qVlanStaticRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qVlanStaticName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1qVlanStaticEgressPorts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1qVlanForbiddenEgressPorts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1qVlanStaticUntaggedPorts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1qVlanStaticRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qVlanStaticTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qNextFreeLocalVlanIndex ARG_LIST((INT4 *));

/* Proto Validate Index Instance for Dot1qPortVlanTable. */
INT1
nmhValidateIndexInstanceDot1qPortVlanTable ARG_LIST((INT4 ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qPvid ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1qPortAcceptableFrameTypes ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1qPortIngressFiltering ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qPvid ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot1qPortAcceptableFrameTypes ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1qPortIngressFiltering ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qPvid ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot1qPortAcceptableFrameTypes ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1qPortIngressFiltering ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qPortVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1qPortVlanStatisticsTable. */
INT1
nmhValidateIndexInstanceDot1qPortVlanStatisticsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qPortVlanStatisticsTable  */

INT1
nmhGetFirstIndexDot1qPortVlanStatisticsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qPortVlanStatisticsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qTpVlanPortInFrames ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1qTpVlanPortOutFrames ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1qTpVlanPortInDiscards ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1qTpVlanPortInOverflowFrames ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1qTpVlanPortOutOverflowFrames ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot1qTpVlanPortInOverflowDiscards ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot1qPortVlanHCStatisticsTable. */
INT1
nmhValidateIndexInstanceDot1qPortVlanHCStatisticsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qPortVlanHCStatisticsTable  */

INT1
nmhGetFirstIndexDot1qPortVlanHCStatisticsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qPortVlanHCStatisticsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qTpVlanPortHCInFrames ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot1qTpVlanPortHCOutFrames ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot1qTpVlanPortHCInDiscards ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for Dot1qLearningConstraintsTable. */
INT1
nmhValidateIndexInstanceDot1qLearningConstraintsTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1qLearningConstraintsTable  */

INT1
nmhGetFirstIndexDot1qLearningConstraintsTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1qLearningConstraintsTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qConstraintType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetDot1qConstraintStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qConstraintType ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetDot1qConstraintStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qConstraintType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1qConstraintStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qLearningConstraintsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1qConstraintSetDefault ARG_LIST((INT4 *));

INT1
nmhGetDot1qConstraintTypeDefault ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1qConstraintSetDefault ARG_LIST((INT4 ));

INT1
nmhSetDot1qConstraintTypeDefault ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1qConstraintSetDefault ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1qConstraintTypeDefault ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1qConstraintSetDefault ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Dot1qConstraintTypeDefault ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1vProtocolGroupTable. */
INT1
nmhValidateIndexInstanceDot1vProtocolGroupTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Dot1vProtocolGroupTable  */

INT1
nmhGetFirstIndexDot1vProtocolGroupTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1vProtocolGroupTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1vProtocolGroupId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetDot1vProtocolGroupRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1vProtocolGroupId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetDot1vProtocolGroupRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1vProtocolGroupId ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Dot1vProtocolGroupRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1vProtocolGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1vProtocolPortTable. */
INT1
nmhValidateIndexInstanceDot1vProtocolPortTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1vProtocolPortTable  */

INT1
nmhGetFirstIndexDot1vProtocolPortTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1vProtocolPortTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1vProtocolPortGroupVid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1vProtocolPortRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1vProtocolPortGroupVid ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1vProtocolPortRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1vProtocolPortGroupVid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1vProtocolPortRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1vProtocolPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
