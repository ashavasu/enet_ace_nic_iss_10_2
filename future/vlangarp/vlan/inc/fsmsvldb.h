/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsvldb.h,v 1.8 2015/05/21 13:45:43 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMSVLDB_H
#define _FSMSVLDB_H

UINT1 FsDot1qBaseTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qFdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot1qTpFdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsDot1qTpGroupTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qForwardAllLearntPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qForwardAllStatusTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot1qForwardAllPortConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qForwardUnregLearntPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qForwardUnregStatusTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot1qForwardUnregPortConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qStaticUnicastTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qStaticAllowedToGoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qStaticMulticastTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qStaticMcastPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qVlanNumDeletesTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qVlanCurrentTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot1qVlanEgressPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qVlanStaticTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot1qVlanStaticPortConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qNextFreeLocalVlanIndexTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qPortVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot1qPortVlanStatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot1qPortVlanHCStatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot1qLearningConstraintsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1qConstraintDefaultTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1vProtocolGroupTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsDot1vProtocolPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmsvl [] ={1,3,6,1,4,1,2076,116,7};
tSNMP_OID_TYPE fsmsvlOID = {9, fsmsvl};


UINT4 FsDot1qVlanContextId [ ] ={1,3,6,1,4,1,2076,116,7,1,1,1,1,1};
UINT4 FsDot1qVlanVersionNumber [ ] ={1,3,6,1,4,1,2076,116,7,1,1,1,1,2};
UINT4 FsDot1qMaxVlanId [ ] ={1,3,6,1,4,1,2076,116,7,1,1,1,1,3};
UINT4 FsDot1qMaxSupportedVlans [ ] ={1,3,6,1,4,1,2076,116,7,1,1,1,1,4};
UINT4 FsDot1qNumVlans [ ] ={1,3,6,1,4,1,2076,116,7,1,1,1,1,5};
UINT4 FsDot1qGvrpStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,1,1,1,6};
UINT4 FsDot1qFdbId [ ] ={1,3,6,1,4,1,2076,116,7,1,2,1,1,1};
UINT4 FsDot1qFdbDynamicCount [ ] ={1,3,6,1,4,1,2076,116,7,1,2,1,1,2};
UINT4 FsDot1qTpFdbAddress [ ] ={1,3,6,1,4,1,2076,116,7,1,2,2,1,1};
UINT4 FsDot1qTpFdbPort [ ] ={1,3,6,1,4,1,2076,116,7,1,2,2,1,2};
UINT4 FsDot1qTpFdbStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,2,2,1,3};
UINT4 FsDot1qTpFdbPw [ ] ={1,3,6,1,4,1,2076,116,7,1,2,2,1,4};
UINT4 FsDot1qVlanIndex [ ] ={1,3,6,1,4,1,2076,116,7,1,2,3,1,1};
UINT4 FsDot1qTpGroupAddress [ ] ={1,3,6,1,4,1,2076,116,7,1,2,3,1,2};
UINT4 FsDot1qTpPort [ ] ={1,3,6,1,4,1,2076,116,7,1,2,3,1,3};
UINT4 FsDot1qTpGroupIsLearnt [ ] ={1,3,6,1,4,1,2076,116,7,1,2,3,1,4};
UINT4 FsDot1qForwardAllIsLearnt [ ] ={1,3,6,1,4,1,2076,116,7,1,2,4,1,1};
UINT4 FsDot1qForwardAllRowStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,2,5,1,1};
UINT4 FsDot1qForwardAllPort [ ] ={1,3,6,1,4,1,2076,116,7,1,2,6,1,1};
UINT4 FsDot1qForwardUnregIsLearnt [ ] ={1,3,6,1,4,1,2076,116,7,1,2,7,1,1};
UINT4 FsDot1qForwardUnregRowStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,2,8,1,1};
UINT4 FsDot1qForwardUnregPort [ ] ={1,3,6,1,4,1,2076,116,7,1,2,9,1,1};
UINT4 FsDot1qStaticUnicastAddress [ ] ={1,3,6,1,4,1,2076,116,7,1,3,1,1,1};
UINT4 FsDot1qStaticUnicastReceivePort [ ] ={1,3,6,1,4,1,2076,116,7,1,3,1,1,2};
UINT4 FsDot1qStaticUnicastRowStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,3,1,1,3};
UINT4 FsDot1qStaticUnicastStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,3,1,1,4};
UINT4 FsDot1qStaticAllowedIsMember [ ] ={1,3,6,1,4,1,2076,116,7,1,3,2,1,1};
UINT4 FsDot1qStaticMulticastAddress [ ] ={1,3,6,1,4,1,2076,116,7,1,3,3,1,1};
UINT4 FsDot1qStaticMulticastReceivePort [ ] ={1,3,6,1,4,1,2076,116,7,1,3,3,1,2};
UINT4 FsDot1qStaticMulticastRowStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,3,3,1,3};
UINT4 FsDot1qStaticMulticastStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,3,3,1,4};
UINT4 FsDot1qStaticMcastPort [ ] ={1,3,6,1,4,1,2076,116,7,1,3,4,1,1};
UINT4 FsDot1qVlanNumDeletes [ ] ={1,3,6,1,4,1,2076,116,7,1,4,1,1,1};
UINT4 FsDot1qVlanTimeMark [ ] ={1,3,6,1,4,1,2076,116,7,1,4,2,1,1};
UINT4 FsDot1qVlanFdbId [ ] ={1,3,6,1,4,1,2076,116,7,1,4,2,1,2};
UINT4 FsDot1qVlanStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,4,2,1,3};
UINT4 FsDot1qVlanCreationTime [ ] ={1,3,6,1,4,1,2076,116,7,1,4,2,1,4};
UINT4 FsDot1qVlanCurrentEgressPort [ ] ={1,3,6,1,4,1,2076,116,7,1,4,3,1,1};
UINT4 FsDot1qVlanStaticName [ ] ={1,3,6,1,4,1,2076,116,7,1,4,4,1,1};
UINT4 FsDot1qVlanStaticRowStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,4,4,1,2};
UINT4 FsDot1qVlanStaticPort [ ] ={1,3,6,1,4,1,2076,116,7,1,4,5,1,1};
UINT4 FsDot1qNextFreeLocalVlanIndex [ ] ={1,3,6,1,4,1,2076,116,7,1,4,6,1,1};
UINT4 FsDot1qPvid [ ] ={1,3,6,1,4,1,2076,116,7,1,4,7,1,1};
UINT4 FsDot1qPortAcceptableFrameTypes [ ] ={1,3,6,1,4,1,2076,116,7,1,4,7,1,2};
UINT4 FsDot1qPortIngressFiltering [ ] ={1,3,6,1,4,1,2076,116,7,1,4,7,1,3};
UINT4 FsDot1qPortGvrpStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,4,7,1,4};
UINT4 FsDot1qPortGvrpFailedRegistrations [ ] ={1,3,6,1,4,1,2076,116,7,1,4,7,1,5};
UINT4 FsDot1qPortGvrpLastPduOrigin [ ] ={1,3,6,1,4,1,2076,116,7,1,4,7,1,6};
UINT4 FsDot1qPortRestrictedVlanRegistration [ ] ={1,3,6,1,4,1,2076,116,7,1,4,7,1,7};
UINT4 FsDot1qTpVlanPortInFrames [ ] ={1,3,6,1,4,1,2076,116,7,1,4,8,1,1};
UINT4 FsDot1qTpVlanPortOutFrames [ ] ={1,3,6,1,4,1,2076,116,7,1,4,8,1,2};
UINT4 FsDot1qTpVlanPortInDiscards [ ] ={1,3,6,1,4,1,2076,116,7,1,4,8,1,3};
UINT4 FsDot1qTpVlanPortInOverflowFrames [ ] ={1,3,6,1,4,1,2076,116,7,1,4,8,1,4};
UINT4 FsDot1qTpVlanPortOutOverflowFrames [ ] ={1,3,6,1,4,1,2076,116,7,1,4,8,1,5};
UINT4 FsDot1qTpVlanPortInOverflowDiscards [ ] ={1,3,6,1,4,1,2076,116,7,1,4,8,1,6};
UINT4 FsDot1qTpVlanPortHCInFrames [ ] ={1,3,6,1,4,1,2076,116,7,1,4,9,1,1};
UINT4 FsDot1qTpVlanPortHCOutFrames [ ] ={1,3,6,1,4,1,2076,116,7,1,4,9,1,2};
UINT4 FsDot1qTpVlanPortHCInDiscards [ ] ={1,3,6,1,4,1,2076,116,7,1,4,9,1,3};
UINT4 FsDot1qConstraintVlan [ ] ={1,3,6,1,4,1,2076,116,7,1,4,10,1,1};
UINT4 FsDot1qConstraintSet [ ] ={1,3,6,1,4,1,2076,116,7,1,4,10,1,2};
UINT4 FsDot1qConstraintType [ ] ={1,3,6,1,4,1,2076,116,7,1,4,10,1,3};
UINT4 FsDot1qConstraintStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,4,10,1,4};
UINT4 FsDot1qConstraintSetDefault [ ] ={1,3,6,1,4,1,2076,116,7,1,4,11,1,1};
UINT4 FsDot1qConstraintTypeDefault [ ] ={1,3,6,1,4,1,2076,116,7,1,4,11,1,2};
UINT4 FsDot1vProtocolTemplateFrameType [ ] ={1,3,6,1,4,1,2076,116,7,1,5,1,1,1};
UINT4 FsDot1vProtocolTemplateProtocolValue [ ] ={1,3,6,1,4,1,2076,116,7,1,5,1,1,2};
UINT4 FsDot1vProtocolGroupId [ ] ={1,3,6,1,4,1,2076,116,7,1,5,1,1,3};
UINT4 FsDot1vProtocolGroupRowStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,5,1,1,4};
UINT4 FsDot1vProtocolPortGroupId [ ] ={1,3,6,1,4,1,2076,116,7,1,5,2,1,1};
UINT4 FsDot1vProtocolPortGroupVid [ ] ={1,3,6,1,4,1,2076,116,7,1,5,2,1,2};
UINT4 FsDot1vProtocolPortRowStatus [ ] ={1,3,6,1,4,1,2076,116,7,1,5,2,1,3};


tMbDbEntry fsmsvlMibEntry[]= {

{{14,FsDot1qVlanContextId}, GetNextIndexFsDot1qBaseTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1qBaseTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1qVlanVersionNumber}, GetNextIndexFsDot1qBaseTable, FsDot1qVlanVersionNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1qBaseTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1qMaxVlanId}, GetNextIndexFsDot1qBaseTable, FsDot1qMaxVlanIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1qBaseTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1qMaxSupportedVlans}, GetNextIndexFsDot1qBaseTable, FsDot1qMaxSupportedVlansGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDot1qBaseTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1qNumVlans}, GetNextIndexFsDot1qBaseTable, FsDot1qNumVlansGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDot1qBaseTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1qGvrpStatus}, GetNextIndexFsDot1qBaseTable, FsDot1qGvrpStatusGet, FsDot1qGvrpStatusSet, FsDot1qGvrpStatusTest, FsDot1qBaseTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qBaseTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1qFdbId}, GetNextIndexFsDot1qFdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDot1qFdbTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qFdbDynamicCount}, GetNextIndexFsDot1qFdbTable, FsDot1qFdbDynamicCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1qFdbTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qTpFdbAddress}, GetNextIndexFsDot1qTpFdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsDot1qTpFdbTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qTpFdbPort}, GetNextIndexFsDot1qTpFdbTable, FsDot1qTpFdbPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDot1qTpFdbTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qTpFdbStatus}, GetNextIndexFsDot1qTpFdbTable, FsDot1qTpFdbStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1qTpFdbTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qTpFdbPw}, GetNextIndexFsDot1qTpFdbTable, FsDot1qTpFdbPwGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDot1qTpFdbTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qVlanIndex}, GetNextIndexFsDot1qTpGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDot1qTpGroupTableINDEX, 4, 0, 0, NULL},

{{14,FsDot1qTpGroupAddress}, GetNextIndexFsDot1qTpGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsDot1qTpGroupTableINDEX, 4, 0, 0, NULL},

{{14,FsDot1qTpPort}, GetNextIndexFsDot1qTpGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1qTpGroupTableINDEX, 4, 0, 0, NULL},

{{14,FsDot1qTpGroupIsLearnt}, GetNextIndexFsDot1qTpGroupTable, FsDot1qTpGroupIsLearntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1qTpGroupTableINDEX, 4, 0, 0, NULL},

{{14,FsDot1qForwardAllIsLearnt}, GetNextIndexFsDot1qForwardAllLearntPortTable, FsDot1qForwardAllIsLearntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1qForwardAllLearntPortTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qForwardAllRowStatus}, GetNextIndexFsDot1qForwardAllStatusTable, FsDot1qForwardAllRowStatusGet, FsDot1qForwardAllRowStatusSet, FsDot1qForwardAllRowStatusTest, FsDot1qForwardAllStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qForwardAllStatusTableINDEX, 2, 0, 1, NULL},

{{14,FsDot1qForwardAllPort}, GetNextIndexFsDot1qForwardAllPortConfigTable, FsDot1qForwardAllPortGet, FsDot1qForwardAllPortSet, FsDot1qForwardAllPortTest, FsDot1qForwardAllPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qForwardAllPortConfigTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qForwardUnregIsLearnt}, GetNextIndexFsDot1qForwardUnregLearntPortTable, FsDot1qForwardUnregIsLearntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1qForwardUnregLearntPortTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qForwardUnregRowStatus}, GetNextIndexFsDot1qForwardUnregStatusTable, FsDot1qForwardUnregRowStatusGet, FsDot1qForwardUnregRowStatusSet, FsDot1qForwardUnregRowStatusTest, FsDot1qForwardUnregStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qForwardUnregStatusTableINDEX, 2, 0, 1, NULL},

{{14,FsDot1qForwardUnregPort}, GetNextIndexFsDot1qForwardUnregPortConfigTable, FsDot1qForwardUnregPortGet, FsDot1qForwardUnregPortSet, FsDot1qForwardUnregPortTest, FsDot1qForwardUnregPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qForwardUnregPortConfigTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qStaticUnicastAddress}, GetNextIndexFsDot1qStaticUnicastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsDot1qStaticUnicastTableINDEX, 4, 0, 0, NULL},

{{14,FsDot1qStaticUnicastReceivePort}, GetNextIndexFsDot1qStaticUnicastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1qStaticUnicastTableINDEX, 4, 0, 0, NULL},

{{14,FsDot1qStaticUnicastRowStatus}, GetNextIndexFsDot1qStaticUnicastTable, FsDot1qStaticUnicastRowStatusGet, FsDot1qStaticUnicastRowStatusSet, FsDot1qStaticUnicastRowStatusTest, FsDot1qStaticUnicastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qStaticUnicastTableINDEX, 4, 0, 1, NULL},

{{14,FsDot1qStaticUnicastStatus}, GetNextIndexFsDot1qStaticUnicastTable, FsDot1qStaticUnicastStatusGet, FsDot1qStaticUnicastStatusSet, FsDot1qStaticUnicastStatusTest, FsDot1qStaticUnicastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qStaticUnicastTableINDEX, 4, 0, 0, "3"},

{{14,FsDot1qStaticAllowedIsMember}, GetNextIndexFsDot1qStaticAllowedToGoTable, FsDot1qStaticAllowedIsMemberGet, FsDot1qStaticAllowedIsMemberSet, FsDot1qStaticAllowedIsMemberTest, FsDot1qStaticAllowedToGoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qStaticAllowedToGoTableINDEX, 5, 0, 0, NULL},

{{14,FsDot1qStaticMulticastAddress}, GetNextIndexFsDot1qStaticMulticastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsDot1qStaticMulticastTableINDEX, 4, 0, 0, NULL},

{{14,FsDot1qStaticMulticastReceivePort}, GetNextIndexFsDot1qStaticMulticastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1qStaticMulticastTableINDEX, 4, 0, 0, NULL},

{{14,FsDot1qStaticMulticastRowStatus}, GetNextIndexFsDot1qStaticMulticastTable, FsDot1qStaticMulticastRowStatusGet, FsDot1qStaticMulticastRowStatusSet, FsDot1qStaticMulticastRowStatusTest, FsDot1qStaticMulticastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qStaticMulticastTableINDEX, 4, 0, 1, NULL},

{{14,FsDot1qStaticMulticastStatus}, GetNextIndexFsDot1qStaticMulticastTable, FsDot1qStaticMulticastStatusGet, FsDot1qStaticMulticastStatusSet, FsDot1qStaticMulticastStatusTest, FsDot1qStaticMulticastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qStaticMulticastTableINDEX, 4, 0, 0, "3"},

{{14,FsDot1qStaticMcastPort}, GetNextIndexFsDot1qStaticMcastPortTable, FsDot1qStaticMcastPortGet, FsDot1qStaticMcastPortSet, FsDot1qStaticMcastPortTest, FsDot1qStaticMcastPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qStaticMcastPortTableINDEX, 5, 0, 0, NULL},

{{14,FsDot1qVlanNumDeletes}, GetNextIndexFsDot1qVlanNumDeletesTable, FsDot1qVlanNumDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1qVlanNumDeletesTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1qVlanTimeMark}, GetNextIndexFsDot1qVlanCurrentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDot1qVlanCurrentTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qVlanFdbId}, GetNextIndexFsDot1qVlanCurrentTable, FsDot1qVlanFdbIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDot1qVlanCurrentTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qVlanStatus}, GetNextIndexFsDot1qVlanCurrentTable, FsDot1qVlanStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1qVlanCurrentTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qVlanCreationTime}, GetNextIndexFsDot1qVlanCurrentTable, FsDot1qVlanCreationTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsDot1qVlanCurrentTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qVlanCurrentEgressPort}, GetNextIndexFsDot1qVlanEgressPortTable, FsDot1qVlanCurrentEgressPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1qVlanEgressPortTableINDEX, 4, 0, 0, NULL},

{{14,FsDot1qVlanStaticName}, GetNextIndexFsDot1qVlanStaticTable, FsDot1qVlanStaticNameGet, FsDot1qVlanStaticNameSet, FsDot1qVlanStaticNameTest, FsDot1qVlanStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot1qVlanStaticTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qVlanStaticRowStatus}, GetNextIndexFsDot1qVlanStaticTable, FsDot1qVlanStaticRowStatusGet, FsDot1qVlanStaticRowStatusSet, FsDot1qVlanStaticRowStatusTest, FsDot1qVlanStaticTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qVlanStaticTableINDEX, 2, 0, 1, NULL},

{{14,FsDot1qVlanStaticPort}, GetNextIndexFsDot1qVlanStaticPortConfigTable, FsDot1qVlanStaticPortGet, FsDot1qVlanStaticPortSet, FsDot1qVlanStaticPortTest, FsDot1qVlanStaticPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qVlanStaticPortConfigTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qNextFreeLocalVlanIndex}, GetNextIndexFsDot1qNextFreeLocalVlanIndexTable, FsDot1qNextFreeLocalVlanIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDot1qNextFreeLocalVlanIndexTableINDEX, 1, 0, 0, NULL},

 /* Manually modified to store the default value of the Port Pvid  for
  ** maintaing the same value after restoration  */
{{14,FsDot1qPvid}, GetNextIndexFsDot1qPortVlanTable, FsDot1qPvidGet, FsDot1qPvidSet, FsDot1qPvidTest, FsDot1qPortVlanTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot1qPortVlanTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1qPortAcceptableFrameTypes}, GetNextIndexFsDot1qPortVlanTable, FsDot1qPortAcceptableFrameTypesGet, FsDot1qPortAcceptableFrameTypesSet, FsDot1qPortAcceptableFrameTypesTest, FsDot1qPortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qPortVlanTableINDEX, 1, 0, 0, "1"},

{{14,FsDot1qPortIngressFiltering}, GetNextIndexFsDot1qPortVlanTable, FsDot1qPortIngressFilteringGet, FsDot1qPortIngressFilteringSet, FsDot1qPortIngressFilteringTest, FsDot1qPortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qPortVlanTableINDEX, 1, 0, 0, "2"},

{{14,FsDot1qPortGvrpStatus}, GetNextIndexFsDot1qPortVlanTable, FsDot1qPortGvrpStatusGet, FsDot1qPortGvrpStatusSet, FsDot1qPortGvrpStatusTest, FsDot1qPortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qPortVlanTableINDEX, 1, 0, 0, "1"},

{{14,FsDot1qPortGvrpFailedRegistrations}, GetNextIndexFsDot1qPortVlanTable, FsDot1qPortGvrpFailedRegistrationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1qPortVlanTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1qPortGvrpLastPduOrigin}, GetNextIndexFsDot1qPortVlanTable, FsDot1qPortGvrpLastPduOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsDot1qPortVlanTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1qPortRestrictedVlanRegistration}, GetNextIndexFsDot1qPortVlanTable, FsDot1qPortRestrictedVlanRegistrationGet, FsDot1qPortRestrictedVlanRegistrationSet, FsDot1qPortRestrictedVlanRegistrationTest, FsDot1qPortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qPortVlanTableINDEX, 1, 0, 0, "2"},

{{14,FsDot1qTpVlanPortInFrames}, GetNextIndexFsDot1qPortVlanStatisticsTable, FsDot1qTpVlanPortInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qTpVlanPortOutFrames}, GetNextIndexFsDot1qPortVlanStatisticsTable, FsDot1qTpVlanPortOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qTpVlanPortInDiscards}, GetNextIndexFsDot1qPortVlanStatisticsTable, FsDot1qTpVlanPortInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qTpVlanPortInOverflowFrames}, GetNextIndexFsDot1qPortVlanStatisticsTable, FsDot1qTpVlanPortInOverflowFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qTpVlanPortOutOverflowFrames}, GetNextIndexFsDot1qPortVlanStatisticsTable, FsDot1qTpVlanPortOutOverflowFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qTpVlanPortInOverflowDiscards}, GetNextIndexFsDot1qPortVlanStatisticsTable, FsDot1qTpVlanPortInOverflowDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qTpVlanPortHCInFrames}, GetNextIndexFsDot1qPortVlanHCStatisticsTable, FsDot1qTpVlanPortHCInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsDot1qPortVlanHCStatisticsTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qTpVlanPortHCOutFrames}, GetNextIndexFsDot1qPortVlanHCStatisticsTable, FsDot1qTpVlanPortHCOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsDot1qPortVlanHCStatisticsTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qTpVlanPortHCInDiscards}, GetNextIndexFsDot1qPortVlanHCStatisticsTable, FsDot1qTpVlanPortHCInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsDot1qPortVlanHCStatisticsTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1qConstraintVlan}, GetNextIndexFsDot1qLearningConstraintsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDot1qLearningConstraintsTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qConstraintSet}, GetNextIndexFsDot1qLearningConstraintsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1qLearningConstraintsTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qConstraintType}, GetNextIndexFsDot1qLearningConstraintsTable, FsDot1qConstraintTypeGet, FsDot1qConstraintTypeSet, FsDot1qConstraintTypeTest, FsDot1qLearningConstraintsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qLearningConstraintsTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1qConstraintStatus}, GetNextIndexFsDot1qLearningConstraintsTable, FsDot1qConstraintStatusGet, FsDot1qConstraintStatusSet, FsDot1qConstraintStatusTest, FsDot1qLearningConstraintsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qLearningConstraintsTableINDEX, 3, 0, 1, NULL},

{{14,FsDot1qConstraintSetDefault}, GetNextIndexFsDot1qConstraintDefaultTable, FsDot1qConstraintSetDefaultGet, FsDot1qConstraintSetDefaultSet, FsDot1qConstraintSetDefaultTest, FsDot1qConstraintDefaultTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot1qConstraintDefaultTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1qConstraintTypeDefault}, GetNextIndexFsDot1qConstraintDefaultTable, FsDot1qConstraintTypeDefaultGet, FsDot1qConstraintTypeDefaultSet, FsDot1qConstraintTypeDefaultTest, FsDot1qConstraintDefaultTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1qConstraintDefaultTableINDEX, 1, 0, 0, NULL},

{{14,FsDot1vProtocolTemplateFrameType}, GetNextIndexFsDot1vProtocolGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsDot1vProtocolGroupTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1vProtocolTemplateProtocolValue}, GetNextIndexFsDot1vProtocolGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsDot1vProtocolGroupTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1vProtocolGroupId}, GetNextIndexFsDot1vProtocolGroupTable, FsDot1vProtocolGroupIdGet, FsDot1vProtocolGroupIdSet, FsDot1vProtocolGroupIdTest, FsDot1vProtocolGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot1vProtocolGroupTableINDEX, 3, 0, 0, NULL},

{{14,FsDot1vProtocolGroupRowStatus}, GetNextIndexFsDot1vProtocolGroupTable, FsDot1vProtocolGroupRowStatusGet, FsDot1vProtocolGroupRowStatusSet, FsDot1vProtocolGroupRowStatusTest, FsDot1vProtocolGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1vProtocolGroupTableINDEX, 3, 0, 1, NULL},

{{14,FsDot1vProtocolPortGroupId}, GetNextIndexFsDot1vProtocolPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1vProtocolPortTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1vProtocolPortGroupVid}, GetNextIndexFsDot1vProtocolPortTable, FsDot1vProtocolPortGroupVidGet, FsDot1vProtocolPortGroupVidSet, FsDot1vProtocolPortGroupVidTest, FsDot1vProtocolPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot1vProtocolPortTableINDEX, 2, 0, 0, NULL},

{{14,FsDot1vProtocolPortRowStatus}, GetNextIndexFsDot1vProtocolPortTable, FsDot1vProtocolPortRowStatusGet, FsDot1vProtocolPortRowStatusSet, FsDot1vProtocolPortRowStatusTest, FsDot1vProtocolPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1vProtocolPortTableINDEX, 2, 0, 1, NULL},
};
tMibData fsmsvlEntry = { 71, fsmsvlMibEntry };
#endif /* _FSMSVLDB_H */

