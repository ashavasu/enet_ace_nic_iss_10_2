
/* $Id: fsmsbrnc.h,v 1.1 2015/12/30 13:34:58 siva Exp $
    ISS Wrapper header
    module ARICENT-MIStdBRIDGE-MIB

 */
#ifndef _H_i_ARICENT_MIStdBRIDGE_MIB
#define _H_i_ARICENT_MIStdBRIDGE_MIB


/********************************************************************
* FUNCTION NcFsDot1dBaseBridgeAddressGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dBaseBridgeAddressGet (
                INT4 i4fsDot1dBaseContextId,
                UINT1 *ppfsDot1dBaseBridgeAddress );

/********************************************************************
* FUNCTION NcFsDot1dBaseNumPortsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dBaseNumPortsGet (
                INT4 i4fsDot1dBaseContextId,
                INT4 *pi4fsDot1dBaseNumPorts );

/********************************************************************
* FUNCTION NcFsDot1dBaseTypeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dBaseTypeGet (
                INT4 i4fsDot1dBaseContextId,
                INT4 *pi4fsDot1dBaseType );

/********************************************************************
* FUNCTION NcFsDot1dBasePortIfIndexGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dBasePortIfIndexGet (
                INT4 i4fsDot1dBasePort,
                INT4 *pi4fsDot1dBasePortIfIndex );

/********************************************************************
* FUNCTION NcFsDot1dBasePortCircuitGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dBasePortCircuitGet (
                INT4 i4fsDot1dBasePort,
                UINT1 *ppfsDot1dBasePortCircuit );

/********************************************************************
* FUNCTION NcFsDot1dBasePortDelayExceededDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dBasePortDelayExceededDiscardsGet (
                INT4 i4fsDot1dBasePort,
                UINT4 *pu4fsDot1dBasePortDelayExceededDiscards );

/********************************************************************
* FUNCTION NcFsDot1dBasePortMtuExceededDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dBasePortMtuExceededDiscardsGet (
                INT4 i4fsDot1dBasePort,
                UINT4 *pu4fsDot1dBasePortMtuExceededDiscards );

/********************************************************************
* FUNCTION NcFsDot1dStpProtocolSpecificationGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpProtocolSpecificationGet (
                INT4 i4fsDot1dStpContextId,
                INT4 pi4fsDot1dStpProtocolSpecification );

/********************************************************************
* FUNCTION NcFsDot1dStpPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPrioritySet (
                INT4 i4fsDot1dStpContextId,
                INT4 i4fsDot1dStpPriority );

/********************************************************************
* FUNCTION NcFsDot1dStpPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPriorityTest (UINT4 *pu4Error,
                INT4 i4fsDot1dStpContextId,
                INT4 i4fsDot1dStpPriority );

/********************************************************************
* FUNCTION NcFsDot1dStpTimeSinceTopologyChangeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpTimeSinceTopologyChangeGet (
                INT4 i4fsDot1dStpContextId,
                UINT4 *pu4fsDot1dStpTimeSinceTopologyChange );

/********************************************************************
* FUNCTION NcFsDot1dStpTopChangesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpTopChangesGet (
                INT4 i4fsDot1dStpContextId,
                UINT4 *pu4fsDot1dStpTopChanges );

/********************************************************************
* FUNCTION NcFsDot1dStpDesignatedRootGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpDesignatedRootGet (
                INT4 i4fsDot1dStpContextId,
                UINT1 *ppfsDot1dStpDesignatedRoot );

/********************************************************************
* FUNCTION NcFsDot1dStpRootCostGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpRootCostGet (
                INT4 i4fsDot1dStpContextId,
                INT4 *pi4fsDot1dStpRootCost );

/********************************************************************
* FUNCTION NcFsDot1dStpRootPortGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpRootPortGet (
                INT4 i4fsDot1dStpContextId,
                INT4 *pi4fsDot1dStpRootPort );

/********************************************************************
* FUNCTION NcFsDot1dStpMaxAgeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpMaxAgeGet (
                INT4 i4fsDot1dStpContextId,
                INT4 *pi4fsDot1dStpMaxAge );

/********************************************************************
* FUNCTION NcFsDot1dStpHelloTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpHelloTimeGet (
                INT4 i4fsDot1dStpContextId,
                INT4 *pi4fsDot1dStpHelloTime );

/********************************************************************
* FUNCTION NcFsDot1dStpHoldTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpHoldTimeGet (
                INT4 i4fsDot1dStpContextId,
                INT4 *pi4fsDot1dStpHoldTime );

/********************************************************************
* FUNCTION NcFsDot1dStpForwardDelayGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpForwardDelayGet (
                INT4 i4fsDot1dStpContextId,
                INT4 *pi4fsDot1dStpForwardDelay );

/********************************************************************
* FUNCTION NcFsDot1dStpBridgeMaxAgeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpBridgeMaxAgeSet (
                INT4 i4fsDot1dStpContextId,
                INT4 i4fsDot1dStpBridgeMaxAge );

/********************************************************************
* FUNCTION NcFsDot1dStpBridgeMaxAgeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpBridgeMaxAgeTest (UINT4 *pu4Error,
                INT4 i4fsDot1dStpContextId,
                INT4 i4fsDot1dStpBridgeMaxAge );

/********************************************************************
* FUNCTION NcFsDot1dStpBridgeHelloTimeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpBridgeHelloTimeSet (
                INT4 i4fsDot1dStpContextId,
                INT4 i4fsDot1dStpBridgeHelloTime );

/********************************************************************
* FUNCTION NcFsDot1dStpBridgeHelloTimeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpBridgeHelloTimeTest (UINT4 *pu4Error,
                INT4 i4fsDot1dStpContextId,
                INT4 i4fsDot1dStpBridgeHelloTime );

/********************************************************************
* FUNCTION NcFsDot1dStpBridgeForwardDelaySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpBridgeForwardDelaySet (
                INT4 i4fsDot1dStpContextId,
                INT4 i4fsDot1dStpBridgeForwardDelay );

/********************************************************************
* FUNCTION NcFsDot1dStpBridgeForwardDelayTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpBridgeForwardDelayTest (UINT4 *pu4Error,
                INT4 i4fsDot1dStpContextId,
                INT4 i4fsDot1dStpBridgeForwardDelay );

/********************************************************************
* FUNCTION NcFsDot1dStpPortPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortPrioritySet (
                INT4 i4fsDot1dStpPort,
                INT4 i4fsDot1dStpPortPriority );

/********************************************************************
* FUNCTION NcFsDot1dStpPortPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortPriorityTest (UINT4 *pu4Error,
                INT4 i4fsDot1dStpPort,
                INT4 i4fsDot1dStpPortPriority );

/********************************************************************
* FUNCTION NcFsDot1dStpPortStateGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortStateGet (
                INT4 i4fsDot1dStpPort,
                INT4 pi4fsDot1dStpPortState );

/********************************************************************
* FUNCTION NcFsDot1dStpPortEnableSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortEnableSet (
                INT4 i4fsDot1dStpPort,
                INT4 i4fsDot1dStpPortEnable );

/********************************************************************
* FUNCTION NcFsDot1dStpPortEnableTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortEnableTest (UINT4 *pu4Error,
                INT4 i4fsDot1dStpPort,
                INT4 i4fsDot1dStpPortEnable );

/********************************************************************
* FUNCTION NcFsDot1dStpPortPathCostSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortPathCostSet (
                INT4 i4fsDot1dStpPort,
                INT4 i4fsDot1dStpPortPathCost );

/********************************************************************
* FUNCTION NcFsDot1dStpPortPathCostTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortPathCostTest (UINT4 *pu4Error,
                INT4 i4fsDot1dStpPort,
                INT4 i4fsDot1dStpPortPathCost );

/********************************************************************
* FUNCTION NcFsDot1dStpPortDesignatedRootGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortDesignatedRootGet (
                INT4 i4fsDot1dStpPort,
                UINT1 *ppfsDot1dStpPortDesignatedRoot );

/********************************************************************
* FUNCTION NcFsDot1dStpPortDesignatedCostGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortDesignatedCostGet (
                INT4 i4fsDot1dStpPort,
                INT4 *pi4fsDot1dStpPortDesignatedCost );

/********************************************************************
* FUNCTION NcFsDot1dStpPortDesignatedBridgeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortDesignatedBridgeGet (
                INT4 i4fsDot1dStpPort,
                UINT1 *ppfsDot1dStpPortDesignatedBridge );

/********************************************************************
* FUNCTION NcFsDot1dStpPortDesignatedPortGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortDesignatedPortGet (
                INT4 i4fsDot1dStpPort,
                UINT1 *ppfsDot1dStpPortDesignatedPort );

/********************************************************************
* FUNCTION NcFsDot1dStpPortForwardTransitionsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortForwardTransitionsGet (
                INT4 i4fsDot1dStpPort,
                UINT4 *pu4fsDot1dStpPortForwardTransitions );

/********************************************************************
* FUNCTION NcFsDot1dStpPortPathCost32Set
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortPathCost32Set (
                INT4 i4fsDot1dStpPort,
                INT4 i4fsDot1dStpPortPathCost32 );

/********************************************************************
* FUNCTION NcFsDot1dStpPortPathCost32Test
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStpPortPathCost32Test (UINT4 *pu4Error,
                INT4 i4fsDot1dStpPort,
                INT4 i4fsDot1dStpPortPathCost32 );

/********************************************************************
* FUNCTION NcFsDot1dTpLearnedEntryDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpLearnedEntryDiscardsGet (
                INT4 i4fsDot1dBaseContextId,
                UINT4 *pu4fsDot1dTpLearnedEntryDiscards );

/********************************************************************
* FUNCTION NcFsDot1dTpAgingTimeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpAgingTimeSet (
                INT4 i4fsDot1dBaseContextId,
                INT4 i4fsDot1dTpAgingTime );

/********************************************************************
* FUNCTION NcFsDot1dTpAgingTimeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpAgingTimeTest (UINT4 *pu4Error,
                INT4 i4fsDot1dBaseContextId,
                INT4 i4fsDot1dTpAgingTime );

/********************************************************************
* FUNCTION NcFsDot1dTpFdbPortGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpFdbPortGet (
                INT4 i4fsDot1dBaseContextId,
                const UINT1 *pfsDot1dTpFdbAddress,
                INT4 *pi4fsDot1dTpFdbPort );

/********************************************************************
* FUNCTION NcFsDot1dTpFdbStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpFdbStatusGet (
                INT4 i4fsDot1dBaseContextId,
                const UINT1 *pfsDot1dTpFdbAddress,
                INT4 pi4fsDot1dTpFdbStatus );

/********************************************************************
* FUNCTION NcFsDot1dTpPortMaxInfoGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpPortMaxInfoGet (
                INT4 i4fsDot1dTpPort,
                INT4 *pi4fsDot1dTpPortMaxInfo );

/********************************************************************
* FUNCTION NcFsDot1dTpPortInFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpPortInFramesGet (
                INT4 i4fsDot1dTpPort,
                UINT4 *pu4fsDot1dTpPortInFrames );

/********************************************************************
* FUNCTION NcFsDot1dTpPortOutFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpPortOutFramesGet (
                INT4 i4fsDot1dTpPort,
                UINT4 *pu4fsDot1dTpPortOutFrames );

/********************************************************************
* FUNCTION NcFsDot1dTpPortInDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpPortInDiscardsGet (
                INT4 i4fsDot1dTpPort,
                UINT4 *pu4fsDot1dTpPortInDiscards );

/********************************************************************
* FUNCTION NcFsDot1dStaticRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStaticRowStatusSet (
                INT4 i4fsDot1dBaseContextId,
                const UINT1 *pfsDot1dStaticAddress,
                INT4 i4fsDot1dStaticReceivePort,
                INT4 i4fsDot1dStaticRowStatus );

/********************************************************************
* FUNCTION NcFsDot1dStaticRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStaticRowStatusTest (UINT4 *pu4Error,
                INT4 i4fsDot1dBaseContextId,
                const UINT1 *pfsDot1dStaticAddress,
                INT4 i4fsDot1dStaticReceivePort,
                INT4 i4fsDot1dStaticRowStatus );

/********************************************************************
* FUNCTION NcFsDot1dStaticStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStaticStatusSet (
                INT4 i4fsDot1dBaseContextId,
                const UINT1 *pfsDot1dStaticAddress,
                INT4 i4fsDot1dStaticReceivePort,
                INT4 i4fsDot1dStaticStatus );

/********************************************************************
* FUNCTION NcFsDot1dStaticStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStaticStatusTest (UINT4 *pu4Error,
                INT4 i4fsDot1dBaseContextId,
                const UINT1 *pfsDot1dStaticAddress,
                INT4 i4fsDot1dStaticReceivePort,
                 INT4 i4fsDot1dStaticStatus );

/********************************************************************
* FUNCTION NcFsDot1dStaticAllowedIsMemberSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStaticAllowedIsMemberSet (
                INT4 i4fsDot1dBaseContextId,
                const UINT1 *fsDot1dStaticAddress,
                INT4 i4fsDot1dStaticReceivePort,
                INT4 i4fsDot1dTpPort,
                INT4 i4fsDot1dStaticAllowedIsMember );

/********************************************************************
* FUNCTION NcFsDot1dStaticAllowedIsMemberTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dStaticAllowedIsMemberTest (UINT4 *pu4Error,
                INT4 i4fsDot1dBaseContextId,
                const UINT1 *fsDot1dStaticAddress,
                INT4 i4fsDot1dStaticReceivePort,
                INT4 i4fsDot1dTpPort,
                INT4 i4fsDot1dStaticAllowedIsMember );

/* END i_ARICENT_MIStdBRIDGE_MIB.c */


#endif
