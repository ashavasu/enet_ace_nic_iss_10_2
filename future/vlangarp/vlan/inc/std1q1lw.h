/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1q1lw.h,v 1.1 2011/11/21 14:02:19 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Ieee8021QBridgeTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeVlanVersionNumber ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021QBridgeMaxVlanId ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021QBridgeMaxSupportedVlans ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021QBridgeNumVlans ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021QBridgeMvrpEnabledStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeMvrpEnabledStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeMvrpEnabledStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021QBridgeCVlanPortTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeCVlanPortTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeCVlanPortTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeCVlanPortTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeCVlanPortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeCVlanPortRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeCVlanPortRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeCVlanPortRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeCVlanPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021QBridgeFdbTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeFdbTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeFdbTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeFdbTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeFdbTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeFdbDynamicCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021QBridgeFdbLearnedEntryDiscards ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021QBridgeFdbAgingTime ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeFdbAgingTime ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeFdbAgingTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeFdbTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021QBridgeTpFdbTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeTpFdbTable ARG_LIST((UINT4  , UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeTpFdbTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeTpFdbTable ARG_LIST((UINT4 * , UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeTpFdbTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeTpFdbPort ARG_LIST((UINT4  , UINT4  , tMacAddr ,UINT4 *));

INT1
nmhGetIeee8021QBridgeTpFdbStatus ARG_LIST((UINT4  , UINT4  , tMacAddr ,INT4 *));

/* Proto Validate Index Instance for Ieee8021QBridgeTpGroupTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeTpGroupTable ARG_LIST((UINT4  , UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeTpGroupTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeTpGroupTable ARG_LIST((UINT4 * , UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeTpGroupTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeTpGroupEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeTpGroupLearnt ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Ieee8021QBridgeForwardAllTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeForwardAllTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeForwardAllTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeForwardAllTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeForwardAllPorts ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeForwardAllStaticPorts ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeForwardAllForbiddenPorts ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeForwardAllStaticPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeForwardAllForbiddenPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeForwardAllStaticPorts ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021QBridgeForwardAllForbiddenPorts ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeForwardAllTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021QBridgeForwardUnregisteredTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeForwardUnregisteredTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeForwardUnregisteredTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeForwardUnregisteredTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeForwardUnregisteredPorts ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeForwardUnregisteredStaticPorts ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeForwardUnregisteredForbiddenPorts ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeForwardUnregisteredStaticPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeForwardUnregisteredForbiddenPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeForwardUnregisteredStaticPorts ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021QBridgeForwardUnregisteredForbiddenPorts ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeForwardUnregisteredTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021QBridgeStaticUnicastTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeStaticUnicastTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeStaticUnicastTable ARG_LIST((UINT4 * , UINT4 * , tMacAddr *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeStaticUnicastTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeStaticUnicastStaticEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeStaticUnicastForbiddenEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeStaticUnicastStorageType ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021QBridgeStaticUnicastRowStatus ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeStaticUnicastStaticEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeStaticUnicastForbiddenEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeStaticUnicastStorageType ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgeStaticUnicastRowStatus ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeStaticUnicastStaticEgressPorts ARG_LIST((UINT4 *  ,UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021QBridgeStaticUnicastForbiddenEgressPorts ARG_LIST((UINT4 *  ,UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021QBridgeStaticUnicastStorageType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021QBridgeStaticUnicastRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeStaticUnicastTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021QBridgeStaticMulticastTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeStaticMulticastTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeStaticMulticastTable ARG_LIST((UINT4 * , UINT4 * , tMacAddr *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeStaticMulticastTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeStaticMulticastStaticEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeStaticMulticastForbiddenEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeStaticMulticastStorageType ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021QBridgeStaticMulticastRowStatus ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeStaticMulticastStaticEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeStaticMulticastForbiddenEgressPorts ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeStaticMulticastStorageType ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgeStaticMulticastRowStatus ARG_LIST((UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeStaticMulticastStaticEgressPorts ARG_LIST((UINT4 *  ,UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021QBridgeStaticMulticastForbiddenEgressPorts ARG_LIST((UINT4 *  ,UINT4  , UINT4  , tMacAddr  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021QBridgeStaticMulticastStorageType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021QBridgeStaticMulticastRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , tMacAddr  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeStaticMulticastTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeVlanNumDeletes ARG_LIST((tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for Ieee8021QBridgeVlanCurrentTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeVlanCurrentTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeVlanCurrentTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeVlanCurrentTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeVlanFdbId ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021QBridgeVlanCurrentEgressPorts ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeVlanCurrentUntaggedPorts ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeVlanStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021QBridgeVlanCreationTime ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Ieee8021QBridgeVlanStaticTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeVlanStaticTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeVlanStaticTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeVlanStaticTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeVlanStaticName ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeVlanStaticEgressPorts ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeVlanForbiddenEgressPorts ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeVlanStaticUntaggedPorts ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021QBridgeVlanStaticRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeVlanStaticName ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeVlanStaticEgressPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeVlanForbiddenEgressPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeVlanStaticUntaggedPorts ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021QBridgeVlanStaticRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeVlanStaticName ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021QBridgeVlanStaticEgressPorts ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021QBridgeVlanForbiddenEgressPorts ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021QBridgeVlanStaticUntaggedPorts ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021QBridgeVlanStaticRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeVlanStaticTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021QBridgeNextFreeLocalVlanTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeNextFreeLocalVlanTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeNextFreeLocalVlanTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeNextFreeLocalVlanTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeNextFreeLocalVlanTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeNextFreeLocalVlanIndex ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Ieee8021QBridgePortVlanTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgePortVlanTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgePortVlanTable  */

INT1
nmhGetFirstIndexIeee8021QBridgePortVlanTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgePortVlanTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgePvid ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021QBridgePortAcceptableFrameTypes ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021QBridgePortIngressFiltering ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021QBridgePortMvrpEnabledStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021QBridgePortMvrpFailedRegistrations ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021QBridgePortMvrpLastPduOrigin ARG_LIST((UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetIeee8021QBridgePortRestrictedVlanRegistration ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgePvid ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021QBridgePortAcceptableFrameTypes ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgePortIngressFiltering ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgePortMvrpEnabledStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgePortRestrictedVlanRegistration ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgePvid ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021QBridgePortAcceptableFrameTypes ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021QBridgePortIngressFiltering ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021QBridgePortMvrpEnabledStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021QBridgePortRestrictedVlanRegistration ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgePortVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021QBridgePortVlanStatisticsTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgePortVlanStatisticsTable  */

INT1
nmhGetFirstIndexIeee8021QBridgePortVlanStatisticsTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgePortVlanStatisticsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeTpVlanPortInFrames ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021QBridgeTpVlanPortOutFrames ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021QBridgeTpVlanPortInDiscards ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for Ieee8021QBridgeLearningConstraintsTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintsTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeLearningConstraintsTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeLearningConstraintsTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeLearningConstraintsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeLearningConstraintsType ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021QBridgeLearningConstraintsStatus ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeLearningConstraintsType ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgeLearningConstraintsStatus ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeLearningConstraintsType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Ieee8021QBridgeLearningConstraintsStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeLearningConstraintsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021QBridgeLearningConstraintDefaultsTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintDefaultsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeLearningConstraintDefaultsTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeLearningConstraintDefaultsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeLearningConstraintDefaultsSet ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021QBridgeLearningConstraintDefaultsType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeLearningConstraintDefaultsSet ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgeLearningConstraintDefaultsType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsSet ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeLearningConstraintDefaultsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021QBridgeProtocolGroupTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeProtocolGroupTable ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeProtocolGroupTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeProtocolGroupTable ARG_LIST((UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeProtocolGroupTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeProtocolGroupId ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIeee8021QBridgeProtocolGroupRowStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeProtocolGroupId ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetIeee8021QBridgeProtocolGroupRowStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeProtocolGroupId ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Ieee8021QBridgeProtocolGroupRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeProtocolGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021QBridgeProtocolPortTable. */
INT1
nmhValidateIndexInstanceIeee8021QBridgeProtocolPortTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021QBridgeProtocolPortTable  */

INT1
nmhGetFirstIndexIeee8021QBridgeProtocolPortTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021QBridgeProtocolPortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021QBridgeProtocolPortGroupVid ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021QBridgeProtocolPortRowStatus ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021QBridgeProtocolPortGroupVid ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021QBridgeProtocolPortRowStatus ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021QBridgeProtocolPortGroupVid ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Ieee8021QBridgeProtocolPortRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021QBridgeProtocolPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
