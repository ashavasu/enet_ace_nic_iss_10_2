
/* $Id: fsmsbenc.h,v 1.1 2015/12/30 13:34:58 siva Exp $
    ISS Wrapper header
    module ARICENTP-BRIDGE-MIB

 */
#ifndef _H_i_ARICENTP_BRIDGE_MIB
#define _H_i_ARICENTP_BRIDGE_MIB


/********************************************************************
* FUNCTION NcFsDot1dTpHCPortInFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpHCPortInFramesGet (
                INT4 i4fsDot1dTpPort,
                UINT4 *pu8fsDot1dTpHCPortInFrames );

/********************************************************************
* FUNCTION NcFsDot1dTpHCPortOutFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpHCPortOutFramesGet (
                INT4 i4fsDot1dTpPort,
                UINT4 *pu8fsDot1dTpHCPortOutFrames );

/********************************************************************
* FUNCTION NcFsDot1dTpHCPortInDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpHCPortInDiscardsGet (
                INT4 i4fsDot1dTpPort,
                UINT4 *pu8fsDot1dTpHCPortInDiscards );

/********************************************************************
* FUNCTION NcFsDot1dTpPortInOverflowFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpPortInOverflowFramesGet (
                INT4 i4fsDot1dTpPort,
                UINT4 *pu4fsDot1dTpPortInOverflowFrames );

/********************************************************************
* FUNCTION NcFsDot1dTpPortOutOverflowFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpPortOutOverflowFramesGet (
                INT4 i4fsDot1dTpPort,
                UINT4 *pu4fsDot1dTpPortOutOverflowFrames );

/********************************************************************
* FUNCTION NcFsDot1dTpPortInOverflowDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTpPortInOverflowDiscardsGet (
                INT4 i4fsDot1dTpPort,
                UINT4 *pu4fsDot1dTpPortInOverflowDiscards );

/********************************************************************
* FUNCTION NcFsDot1dDeviceCapabilitiesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
/*Commneted due to open issue of ncx_list_t (bits)
 * extern INT1 NcFsDot1dDeviceCapabilitiesGet (
                INT4 i4fsDot1dBridgeContextId,
                ncx_list_t *pncx_list_tfsDot1dDeviceCapabilities );*/

/********************************************************************
* FUNCTION NcFsDot1dTrafficClassesEnabledSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTrafficClassesEnabledSet (
                INT4 i4fsDot1dBridgeContextId,
                INT4 i4fsDot1dTrafficClassesEnabled );

/********************************************************************
* FUNCTION NcFsDot1dTrafficClassesEnabledTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTrafficClassesEnabledTest (UINT4 *pu4Error,
                INT4 i4fsDot1dBridgeContextId,
                INT4 i4fsDot1dTrafficClassesEnabled );

/********************************************************************
* FUNCTION NcFsDot1dGmrpStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dGmrpStatusSet (
		INT4 i4fsDot1dBridgeContextId,
                INT4 i4fsDot1dGmrpStatus );

/********************************************************************
* FUNCTION NcFsDot1dGmrpStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dGmrpStatusTest (UINT4 *pu4Error,
		INT4 i4fsDot1dBridgeContextId,
                INT4 i4fsDot1dGmrpStatus );

/********************************************************************
* FUNCTION NcFsDot1dRegenUserPrioritySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dRegenUserPrioritySet (
                INT4 i4fsDot1dBasePort,
                INT4 i4fsDot1dUserPriority,
                INT4 i4fsDot1dRegenUserPriority );

/********************************************************************
* FUNCTION NcFsDot1dRegenUserPriorityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dRegenUserPriorityTest (UINT4 *pu4Error,
                INT4 i4fsDot1dBasePort,
                INT4 i4fsDot1dUserPriority,
                INT4 i4fsDot1dRegenUserPriority );

/********************************************************************
* FUNCTION NcFsDot1dTrafficClassSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTrafficClassSet (
                INT4 i4fsDot1dBasePort,
                INT4 i4fsDot1dTrafficClassPriority,
                INT4 i4fsDot1dTrafficClass );

/********************************************************************
* FUNCTION NcFsDot1dTrafficClassTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dTrafficClassTest (UINT4 *pu4Error,
                INT4 i4fsDot1dBasePort,
                INT4 i4fsDot1dTrafficClassPriority,
                INT4 i4fsDot1dTrafficClass );

/********************************************************************
* FUNCTION NcFsDot1dPortOutboundAccessPriorityGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsDot1dPortOutboundAccessPriorityGet (
                INT4 i4fsDot1dBasePort,
                INT4 i4fsDot1dRegenUserPriority,
                INT4 *pi4fsDot1dPortOutboundAccessPriority );

/* END i_ARICENTP_BRIDGE_MIB.c */


#endif
