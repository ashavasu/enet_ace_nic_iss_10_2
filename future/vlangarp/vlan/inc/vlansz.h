/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlansz.h,v 1.8 2015/09/14 13:28:40 siva Exp $
 *
 * Description: This file contains prototypes of mempool creation-
 *              deletion functions.
 * *******************************************************************/
enum {
    MAX_VLAN_BASE_PORT_ENTRIES_SIZING_ID,
    MAX_VLAN_CONFIG_Q_MESG_SIZING_ID,
    MAX_VLAN_CONTEXT_INFO_SIZING_ID,
    MAX_VLAN_CURR_ENTRIES_SIZING_ID,
    MAX_VLAN_FDB_ENTRIES_SIZING_ID,
    MAX_VLAN_FDB_INFO_SIZING_ID,
    MAX_VLAN_FID_ENTRIES_SIZING_ID,
    MAX_VLAN_GROUP_ENTRIES_SIZING_ID,
    MAX_VLAN_L2VPN_MAP_ENTRIES_SIZING_ID,
    MAX_VLAN_MAC_CONTROL_ENTRIES_SIZING_ID,
    MAX_VLAN_MAC_MAP_ENTRIES_SIZING_ID,
    MAX_VLAN_NUM_PORT_ARRARY_SIZING_ID,
    MAX_VLAN_PB_CVID_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_CVLAN_DSCP_SVLAN_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_CVLAN_DSTIP_SVLAN_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_CVLAN_DSTMAC_SVLAN_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_CVLAN_SRCMAC_SVLAN_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_DSCP_SVLAN_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_DSTIP_SVLAN_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_DSTMAC_SVLAN_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_ETHER_TYPE_SWAP_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_PEP_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_PORT_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_SRC_DSTIP_SVLAN_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_SRCIP_SVLAN_ENTRIES_SIZING_ID,
    MAX_VLAN_PB_SRCMAC_SVLAN_ENTRIES_SIZING_ID,
    MAX_VLAN_PER_PORT_PER_VLAN_STATS_SIZING_ID,
    MAX_VLAN_PKTS_IN_QUE_SIZING_ID,
    MAX_VLAN_PORT_ENTRIES_SIZING_ID,
    MAX_VLAN_PORT_PROTOCOL_ENTRIES_SIZING_ID,
    MAX_VLAN_PORT_VLAN_MAP_ENTRIES_SIZING_ID,
    MAX_VLAN_PROTO_GROUP_ENTRIES_SIZING_ID,
    MAX_VLAN_PVLAN_ENTRIES_SIZING_ID,
    MAX_VLAN_STATIC_ENTRIES_SIZING_ID,
    MAX_VLAN_STATS_ENTRIES_SIZING_ID,
    MAX_VLAN_ST_MCAST_ENTRIES_SIZING_ID,
    MAX_VLAN_ST_UCAST_ENTRIES_SIZING_ID,
    MAX_VLAN_SUBNET_MAP_ENTRIES_SIZING_ID,
    MAX_VLAN_TEMP_PORT_LIST_COUNT_SIZING_ID,
    MAX_VLAN_WILD_CARD_ENTRIES_SIZING_ID,
    MAX_VLAN_HW_STATS_ENTRIES_SIZING_ID,
    MAX_VLAN_MCAG_CONFIG_Q_MESG_SIZING_ID,
    MAX_VLAN_MCAG_MAC_AGING_ENTRIES_SIZING_ID,
#ifdef EVB_WANTED
    MAX_VLAN_EVB_CONTEXT_INFO_SIZING_ID,
    MAX_VLAN_EVB_UAP_ENTRIES_SIZING_ID,
    MAX_VLAN_EVB_SCH_ENTRIES_SIZING_ID,
    MAX_VLAN_EVB_QUE_ENTRIES_SIZING_ID,
#endif
    VLAN_MAX_SIZING_ID
};


#ifdef  _VLANSZ_C
tMemPoolId VLANMemPoolIds[ VLAN_MAX_SIZING_ID];
INT4  VlanSizingMemCreateMemPools(VOID);
VOID  VlanSizingMemDeleteMemPools(VOID);
INT4  VlanSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _VLANSZ_C  */
extern tMemPoolId VLANMemPoolIds[ ];
extern INT4  VlanSizingMemCreateMemPools(VOID);
extern VOID  VlanSizingMemDeleteMemPools(VOID);
#endif /*  _VLANSZ_C  */


#ifdef  _VLANSZ_C
tFsModSizingParams FsVLANSizingParams [] = {
{ "tVlanBasePortEntry", "MAX_VLAN_BASE_PORT_ENTRIES", sizeof(tVlanBasePortEntry),MAX_VLAN_BASE_PORT_ENTRIES, MAX_VLAN_BASE_PORT_ENTRIES,0 },
{ "tVlanQMsg", "MAX_VLAN_CONFIG_Q_MESG", sizeof(tVlanQMsg),MAX_VLAN_CONFIG_Q_MESG, MAX_VLAN_CONFIG_Q_MESG,0 },
{ "tVlanContextInfo", "MAX_VLAN_CONTEXT_INFO", sizeof(tVlanContextInfo),MAX_VLAN_CONTEXT_INFO, MAX_VLAN_CONTEXT_INFO,0 },
{ "tVlanCurrEntry", "MAX_VLAN_CURR_ENTRIES", sizeof(tVlanCurrEntry),MAX_VLAN_CURR_ENTRIES, MAX_VLAN_CURR_ENTRIES,0 },
{ "tVlanFdbEntry", "MAX_VLAN_FDB_ENTRIES", sizeof(tVlanFdbEntry),MAX_VLAN_FDB_ENTRIES, MAX_VLAN_FDB_ENTRIES,0 },
{ "tVlanFdbInfo", "MAX_VLAN_FDB_INFO", sizeof(tVlanFdbInfo),MAX_VLAN_FDB_INFO, MAX_VLAN_FDB_INFO,0 },
{ "tVlanFidEntry", "MAX_VLAN_FID_ENTRIES", sizeof(tVlanFidEntry),MAX_VLAN_FID_ENTRIES, MAX_VLAN_FID_ENTRIES,0 },
{ "tVlanGroupEntry", "MAX_VLAN_GROUP_ENTRIES", sizeof(tVlanGroupEntry),MAX_VLAN_GROUP_ENTRIES, MAX_VLAN_GROUP_ENTRIES,0 },
{ "tVlanL2VpnMap", "MAX_VLAN_L2VPN_MAP_ENTRIES", sizeof(tVlanL2VpnMap),MAX_VLAN_L2VPN_MAP_ENTRIES, MAX_VLAN_L2VPN_MAP_ENTRIES,0 },
{ "tVlanMacControl", "MAX_VLAN_MAC_CONTROL_ENTRIES", sizeof(tVlanMacControl),MAX_VLAN_MAC_CONTROL_ENTRIES, MAX_VLAN_MAC_CONTROL_ENTRIES,0 },
{ "tVlanMacMapEntry", "MAX_VLAN_MAC_MAP_ENTRIES", sizeof(tVlanMacMapEntry),MAX_VLAN_MAC_MAP_ENTRIES, MAX_VLAN_MAC_MAP_ENTRIES,0 },
{ "tVlanNpPortArray", "MAX_VLAN_NUM_PORT_ARRARY", sizeof(tVlanNpPortArray),MAX_VLAN_NUM_PORT_ARRARY, MAX_VLAN_NUM_PORT_ARRARY,0 },
{ "tVlanCVlanSVlanEntry", "MAX_VLAN_PB_CVID_ENTRIES", sizeof(tVlanCVlanSVlanEntry),MAX_VLAN_PB_CVID_ENTRIES, MAX_VLAN_PB_CVID_ENTRIES,0 },
{ "tVlanCVlanDscpSVlanEntry", "MAX_VLAN_PB_CVLAN_DSCP_SVLAN_ENTRIES", sizeof(tVlanCVlanDscpSVlanEntry),MAX_VLAN_PB_CVLAN_DSCP_SVLAN_ENTRIES, MAX_VLAN_PB_CVLAN_DSCP_SVLAN_ENTRIES,0 },
{ "tVlanCVlanDstIpSVlanEntry", "MAX_VLAN_PB_CVLAN_DSTIP_SVLAN_ENTRIES", sizeof(tVlanCVlanDstIpSVlanEntry),MAX_VLAN_PB_CVLAN_DSTIP_SVLAN_ENTRIES, MAX_VLAN_PB_CVLAN_DSTIP_SVLAN_ENTRIES,0 },
{ "tVlanCVlanDstMacSVlanEntry", "MAX_VLAN_PB_CVLAN_DSTMAC_SVLAN_ENTRIES", sizeof(tVlanCVlanDstMacSVlanEntry),MAX_VLAN_PB_CVLAN_DSTMAC_SVLAN_ENTRIES, MAX_VLAN_PB_CVLAN_DSTMAC_SVLAN_ENTRIES,0 },
{ "tVlanCVlanSrcMacSVlanEntry", "MAX_VLAN_PB_CVLAN_SRCMAC_SVLAN_ENTRIES", sizeof(tVlanCVlanSrcMacSVlanEntry),MAX_VLAN_PB_CVLAN_SRCMAC_SVLAN_ENTRIES, MAX_VLAN_PB_CVLAN_SRCMAC_SVLAN_ENTRIES,0 },
{ "tVlanDscpSVlanEntry", "MAX_VLAN_PB_DSCP_SVLAN_ENTRIES", sizeof(tVlanDscpSVlanEntry),MAX_VLAN_PB_DSCP_SVLAN_ENTRIES, MAX_VLAN_PB_DSCP_SVLAN_ENTRIES,0 },
{ "tVlanDstIpSVlanEntry", "MAX_VLAN_PB_DSTIP_SVLAN_ENTRIES", sizeof(tVlanDstIpSVlanEntry),MAX_VLAN_PB_DSTIP_SVLAN_ENTRIES, MAX_VLAN_PB_DSTIP_SVLAN_ENTRIES,0 },
{ "tVlanDstMacSVlanEntry", "MAX_VLAN_PB_DSTMAC_SVLAN_ENTRIES", sizeof(tVlanDstMacSVlanEntry),MAX_VLAN_PB_DSTMAC_SVLAN_ENTRIES, MAX_VLAN_PB_DSTMAC_SVLAN_ENTRIES,0 },
{ "tEtherTypeSwapEntry", "MAX_VLAN_PB_ETHER_TYPE_SWAP_ENTRIES", sizeof(tEtherTypeSwapEntry),MAX_VLAN_PB_ETHER_TYPE_SWAP_ENTRIES, MAX_VLAN_PB_ETHER_TYPE_SWAP_ENTRIES,0 },
{ "tVlanPbLogicalPortEntry", "MAX_VLAN_PB_PEP_ENTRIES", sizeof(tVlanPbLogicalPortEntry),MAX_VLAN_PB_PEP_ENTRIES, MAX_VLAN_PB_PEP_ENTRIES,0 },
{ "tVlanPbPortEntry", "MAX_VLAN_PB_PORT_ENTRIES", sizeof(tVlanPbPortEntry),MAX_VLAN_PB_PORT_ENTRIES, MAX_VLAN_PB_PORT_ENTRIES,0 },
{ "tVlanSrcDstIpSVlanEntry", "MAX_VLAN_PB_SRC_DSTIP_SVLAN_ENTRIES", sizeof(tVlanSrcDstIpSVlanEntry),MAX_VLAN_PB_SRC_DSTIP_SVLAN_ENTRIES, MAX_VLAN_PB_SRC_DSTIP_SVLAN_ENTRIES,0 },
{ "tVlanSrcIpSVlanEntry", "MAX_VLAN_PB_SRCIP_SVLAN_ENTRIES", sizeof(tVlanSrcIpSVlanEntry),MAX_VLAN_PB_SRCIP_SVLAN_ENTRIES, MAX_VLAN_PB_SRCIP_SVLAN_ENTRIES,0 },
{ "tVlanSrcMacSVlanEntry", "MAX_VLAN_PB_SRCMAC_SVLAN_ENTRIES", sizeof(tVlanSrcMacSVlanEntry),MAX_VLAN_PB_SRCMAC_SVLAN_ENTRIES, MAX_VLAN_PB_SRCMAC_SVLAN_ENTRIES,0 },
{ "tVlanPortInfoPerVlan", "MAX_VLAN_PER_PORT_PER_VLAN_STATS", sizeof(tVlanPortInfoPerVlan),MAX_VLAN_PER_PORT_PER_VLAN_STATS, MAX_VLAN_PER_PORT_PER_VLAN_STATS,0 },
{ "tVlanQInPkt", "MAX_VLAN_PKTS_IN_QUE", sizeof(tVlanQInPkt),MAX_VLAN_PKTS_IN_QUE, MAX_VLAN_PKTS_IN_QUE,0 },
{ "tVlanPortEntry", "MAX_VLAN_PORT_ENTRIES", sizeof(tVlanPortEntry),MAX_VLAN_PORT_ENTRIES, MAX_VLAN_PORT_ENTRIES,0 },
{ "tVlanPortVidSet", "MAX_VLAN_PORT_PROTOCOL_ENTRIES", sizeof(tVlanPortVidSet),MAX_VLAN_PORT_PROTOCOL_ENTRIES, MAX_VLAN_PORT_PROTOCOL_ENTRIES,0 },
{ "tPortVlanMapEntry", "MAX_VLAN_PORT_VLAN_MAP_ENTRIES", sizeof(tPortVlanMapEntry),MAX_VLAN_PORT_VLAN_MAP_ENTRIES, MAX_VLAN_PORT_VLAN_MAP_ENTRIES,0 },
{ "tVlanProtGrpEntry", "MAX_VLAN_PROTO_GROUP_ENTRIES", sizeof(tVlanProtGrpEntry),MAX_VLAN_PROTO_GROUP_ENTRIES, MAX_VLAN_PROTO_GROUP_ENTRIES,0 },
{ "tVlanPvlanEntry", "MAX_VLAN_PVLAN_ENTRIES", sizeof(tVlanPvlanEntry),MAX_VLAN_PVLAN_ENTRIES, MAX_VLAN_PVLAN_ENTRIES,0 },
{ "tStaticVlanEntry", "MAX_VLAN_STATIC_ENTRIES", sizeof(tStaticVlanEntry),MAX_VLAN_STATIC_ENTRIES, MAX_VLAN_STATIC_ENTRIES,0 },
{ "tVlanStats", "MAX_VLAN_STATS_ENTRIES", sizeof(tVlanStats),MAX_VLAN_STATS_ENTRIES, MAX_VLAN_STATS_ENTRIES,0 },
{ "tVlanStMcastEntry", "MAX_VLAN_ST_MCAST_ENTRIES", sizeof(tVlanStMcastEntry),MAX_VLAN_ST_MCAST_ENTRIES, MAX_VLAN_ST_MCAST_ENTRIES,0 },
{ "tVlanStUcastEntry", "MAX_VLAN_ST_UCAST_ENTRIES", sizeof(tVlanStUcastEntry),MAX_VLAN_ST_UCAST_ENTRIES, MAX_VLAN_ST_UCAST_ENTRIES,0 },
{ "tVlanSubnetMapEntry", "MAX_VLAN_SUBNET_MAP_ENTRIES", sizeof(tVlanSubnetMapEntry),MAX_VLAN_SUBNET_MAP_ENTRIES, MAX_VLAN_SUBNET_MAP_ENTRIES,0 },
{ "tVlanTempPortList", "MAX_VLAN_TEMP_PORT_LIST_COUNT", sizeof(tVlanTempPortList),MAX_VLAN_TEMP_PORT_LIST_COUNT, MAX_VLAN_TEMP_PORT_LIST_COUNT,0 },
{ "tVlanWildCardEntry", "MAX_VLAN_WILD_CARD_ENTRIES", sizeof(tVlanWildCardEntry),MAX_VLAN_WILD_CARD_ENTRIES, MAX_VLAN_WILD_CARD_ENTRIES,0 },
{ "UINT1 *", "MAX_VLAN_CURR_ENTRIES", VLAN_HW_STATS_ENTRY_LEN, MAX_VLAN_CURR_ENTRIES, MAX_VLAN_CURR_ENTRIES, 0 },
{ "tMcagQMsg", "MAX_VLAN_MCAG_CONFIG_Q_MESG", sizeof(tMcagQMsg),MAX_VLAN_MCAG_CONFIG_Q_MESG,MAX_VLAN_MCAG_CONFIG_Q_MESG,0 },
{ "tVlanMcagEntries", "MAX_VLAN_MCAG_MAC_AGING_ENTRIES", sizeof(tVlanMcagEntries),MAX_VLAN_MCAG_MAC_AGING_ENTRIES,MAX_VLAN_MCAG_MAC_AGING_ENTRIES,0 },
#ifdef EVB_WANTED
{ "tEvbContextInfo", "MAX_VLAN_EVB_CONTEXT_ENTRIES", sizeof(tEvbContextInfo),MAX_VLAN_EVB_CONTEXT_ENTRIES, MAX_VLAN_EVB_CONTEXT_ENTRIES, 0},
{"tEvbUapIfEntry", "MAX_VLAN_EVB_UAP_ENTRIES", sizeof(tEvbUapIfEntry), MAX_VLAN_EVB_UAP_ENTRIES , MAX_VLAN_EVB_UAP_ENTRIES,0},
{"tEvbSChIfEntry", "MAX_VLAN_EVB_SCH_ENTRIES", sizeof(tEvbSChIfEntry), MAX_VLAN_EVB_SCH_ENTRIES, MAX_VLAN_EVB_SCH_ENTRIES,0},
{"tEvbQueMsg", "MAX_VLAN_EVB_QUE_ENTRIES", sizeof(tEvbQueMsg), MAX_VLAN_EVB_QUE_ENTRIES, MAX_VLAN_EVB_QUE_ENTRIES,0},
#endif
{"\0","\0",0,0,0,0}
};
#else  /*  _VLANSZ_C  */
extern tFsModSizingParams FsVLANSizingParams [];
#endif /*  _VLANSZ_C  */


