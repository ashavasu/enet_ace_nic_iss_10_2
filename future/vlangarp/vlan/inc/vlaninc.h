/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: vlaninc.h,v 1.65 2016/02/28 10:56:39 siva Exp $
*
* Description: Header files to be included by all source files in vlan module
*********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : vlaninc.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN                                             */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains header files to be included   */
/*                          by all the source files of VLAN module.          */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/


#ifndef _VLANINC_H
#define _VLANINC_H

#include "lr.h"
#include "cfa.h"
#include "snmccons.h"
#include  "fssnmp.h" 
#include "fsutil.h"
#include "snmputil.h"
#include "bridge.h"
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "vcm.h" 
#include "l2iwf.h"
#include "dhcp.h"
#include "snp.h"
#include "garp.h"
#include "mrp.h"
#include "rstp.h"
#include "mstp.h"
#include "pvrst.h"
#include "pnac.h" 
#include "ecfm.h"
#include "elm.h"
#include "la.h" 
#include "lldp.h" 
#include "iss.h"
#include "isspi.h"
#include "redblack.h" 
#include "ofcl.h"
#include "eoam.h"
#include "icch.h"

#include "cfacli.h" 
#include "qosxtd.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#include "vlanmbsm.h"
#ifdef PB_WANTED
#include "vlpbmbsm.h"
#endif
#endif


#ifdef PBB_PERFORMANCE_WANTED
#include  <sys/time.h>
#endif
/* VLAN Related Files */

#ifdef ICCH_WANTED
#include "vlanicch.h"
#endif /* ICCH_WANTED */
#include "vlancons.h"
#include "vlanport.h"
#include "vlantdfs.h"
#include "vlanmacs.h"
#include "vlanhash.h"
#include "vlanprot.h"
#ifdef NPAPI_WANTED
#include "npapi.h"
#include "nputil.h"
#include "cfanp.h"
#include "brgnp.h"
#ifndef NP_BACKWD_COMPATIBILITY
#include "vlanminp.h"
#else
#include "vlannp.h"
#include "vlminpwr.h"
#endif
#endif

#include "vlanred.h"
#include "vlnpbred.h"

#include "vlanextn.h"
#include "stdbrlow.h"

#include "stdbrilw.h"
#include "std1q1lw.h"
#include "std1d1lw.h"
#include "fsvlanlw.h"
#include "stdvllow.h"
#include "vlantrc.h"
#ifndef  GARPINC_H
 #include "garpinc.h"
#endif 
#include "vlancli.h"
/* Provider Bridging related files */

#include "vlnpbtdf.h"
#ifdef PB_WANTED
#include "vlnpbcon.h"
#include "vlnpbext.h"
#include "vlnpbprt.h"
#include "vlnpbcli.h"
#include "fspblw.h"
#include "fspbwr.h"
#include "fsvlnelw.h"
#include "fsvlnewr.h"
#include "fsdot1wr.h"
#include "fsdot1lw.h"
#include "stdpbwr.h"
#include "stdpblw.h"

#ifdef NPAPI_WANTED
#include "vlnmpbnp.h"
#include "vlnpbnp.h"
#endif
#else
#include "vlnpbstb.h"
#endif

/* MI related low level routine headers */
#include "stdbrgwr.h"
#include "stdbriwr.h"
#include "std1q1wr.h"
#include "std1d1wr.h"
#include "fsvlanwr.h"
#include "stdvlawr.h"
#include "fsmsvlwr.h"
#include "fsmpvlwr.h"
#include "fsmsbewr.h"
#include "fsmsbrwr.h"
#include "fsmsvllw.h"
#include "fsmpvllw.h"
#include "fsmsbelw.h"
#ifdef PB_WANTED
#include "fsmpblw.h"
#include "fsmpbwr.h"
#include "fsmvlelw.h"
#include "fsmvlewr.h"
#include "fsm1adlw.h"
#include "fsm1adwr.h"
#endif

#include "vlnl2vpn.h"
#include "vlanhwwr.h"

#ifdef KERNEL_WANTED
#include "chrdev.h"
#endif /*KERNEL_WANTED */

#ifdef SYSLOG_WANTED
#include "fssyslog.h"
#endif

#include "msr.h"
#ifdef PBBTE_PERFORM_TRACE_WANTED
#include  <sys/time.h>
#endif

#ifdef EVB_WANTED
#include "vlnevcon.h"
#include "vlnevtdf.h"
#include "vlnevext.h"
#include "vlnevmac.h"
#include "vlnevprt.h"
#include "vlnevcli.h"
#include "std1evlw.h"
#include "std1evwr.h"
#include "std1lllw.h"
#include "std1llwr.h"
#include "fsmid1lw.h" 
#include "fsmid1wr.h" 
#else
#include "vlnevstb.h"
#endif /* EVB_WANTED */

#include "pbbte.h"
#include "vlansz.h"
#include "vlnrdsz.h"

#ifdef VXLAN_WANTED
#include "fsvxlan.h"
#endif
#ifdef ARP_WANTED
#include "arp.h"
#endif /* ARP_WANTED */
#include "issu.h"
#endif
