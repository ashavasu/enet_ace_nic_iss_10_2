/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsvlanwr.h,v 1.31.22.1 2018/03/15 12:59:55 siva Exp $
 *
 * Description: This file contains wrappers used in VLAN module.
 *
 *******************************************************************/
#ifndef _FSVLANWR_H
#define _FSVLANWR_H

VOID RegisterFSVLAN(VOID);

VOID UnRegisterFSVLAN(VOID);
INT4 Dot1qFutureVlanStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanMacBasedOnAllPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortProtoBasedOnAllPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanShutdownStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureGarpShutdownStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanDebugGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanLearningModeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanHybridTypeDefaultGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanGlobalMacLearningStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanApplyEnhancedFilteringCriteriaGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanSwStatsEnabledGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanMacBasedOnAllPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortProtoBasedOnAllPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanShutdownStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureGarpShutdownStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanDebugSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanLearningModeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanHybridTypeDefaultSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanGlobalMacLearningStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanApplyEnhancedFilteringCriteriaSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanSwStatsEnabledSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanMacBasedOnAllPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortProtoBasedOnAllPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanShutdownStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureGarpShutdownStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanLearningModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanHybridTypeDefaultTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanGlobalMacLearningStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanApplyEnhancedFilteringCriteriaTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanSwStatsEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanMacBasedOnAllPortsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanPortProtoBasedOnAllPortsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanShutdownStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureGarpShutdownStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanLearningModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanHybridTypeDefaultDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanGlobalMacLearningStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanApplyEnhancedFilteringCriteriaDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanSwStatsEnabledDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);







INT4 GetNextIndexDot1qFutureVlanPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanPortTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacBasedClassificationGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortPortProtoBasedClassificationGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanFilteringUtilityCriteriaGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortProtectedGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetBasedClassificationGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortUnicastMacLearningGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortIngressEtherTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortEgressEtherTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortEgressTPIDTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortAllowableTPID1Get(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortAllowableTPID2Get(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortAllowableTPID3Get(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortUnicastMacSecTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFuturePortPacketReflectionStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortTypeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacBasedClassificationSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortPortProtoBasedClassificationSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanFilteringUtilityCriteriaSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortProtectedSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetBasedClassificationSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortUnicastMacLearningSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortIngressEtherTypeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortEgressEtherTypeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortEgressTPIDTypeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortAllowableTPID1Set(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortAllowableTPID2Set(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortAllowableTPID3Set(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortUnicastMacSecTypeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFuturePortPacketReflectionStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacBasedClassificationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortPortProtoBasedClassificationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanFilteringUtilityCriteriaTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortProtectedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetBasedClassificationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortUnicastMacLearningTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortIngressEtherTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortEgressEtherTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortEgressTPIDTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortAllowableTPID1Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortAllowableTPID2Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortAllowableTPID3Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortUnicastMacSecTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFuturePortPacketReflectionStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexDot1qFutureVlanPortMacMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanPortMacMapVidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapNameGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapMcastBcastOptionGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapVidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapNameSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapMcastBcastOptionSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapMcastBcastOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortMacMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexDot1qFutureVlanFidMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanIndexGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanFidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanFidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanFidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanFidMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 Dot1qFutureVlanOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureGvrpOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureGmrpOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanBridgeModeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelBpduPriGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanBridgeModeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelBpduPriSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanBridgeModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelBpduPriTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanBridgeModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanTunnelBpduPriDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 GetNextIndexDot1qFutureVlanTunnelTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanTunnelStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexDot1qFutureVlanTunnelProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanTunnelStpPDUsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelStpPDUsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelStpPDUsSentGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelGvrpPDUsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelGvrpPDUsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelGvrpPDUsSentGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelIgmpPktsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelIgmpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelIgmpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelStpPDUsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelGvrpPDUsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelIgmpPktsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelStpPDUsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelGvrpPDUsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelIgmpPktsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanTunnelProtocolTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexDot1qFutureVlanCounterTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanCounterRxUcastGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterRxMcastBcastGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterTxUnknUcastGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterTxUcastGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterTxBcastGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterRxFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterRxBytesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterTxFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterTxBytesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterDiscardFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterDiscardBytesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanCounterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexDot1qFutureVlanUnicastMacControlTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanUnicastMacLimitGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanAdminMacLearningStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanOperMacLearningStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortFdbFlushGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanUnicastMacLimitSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanAdminMacLearningStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortFdbFlushSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanUnicastMacLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanAdminMacLearningStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortFdbFlushTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanUnicastMacControlTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 Dot1qFutureGarpDebugGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureGarpDebugSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureGarpDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureGarpDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexDot1qFutureVlanTpFdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanTpFdbPwGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qTpOldFdbPortGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureConnectionIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1qFutureVlanWildCardTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanWildCardEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanWildCardRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanWildCardEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanWildCardRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanWildCardEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanWildCardRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanWildCardTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1qFutureStaticUnicastExtnTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureStaticConnectionIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureStaticConnectionIdentifierSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureStaticConnectionIdentifierTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureStaticUnicastExtnTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 Dot1qFutureUnicastMacLearningLimitGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanBaseBridgeModeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanSubnetBasedOnAllPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureUnicastMacLearningLimitSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanBaseBridgeModeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanSubnetBasedOnAllPortsSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureUnicastMacLearningLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanBaseBridgeModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanSubnetBasedOnAllPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureUnicastMacLearningLimitDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanBaseBridgeModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanSubnetBasedOnAllPortsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexDot1qFutureVlanPortSubnetMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanPortSubnetMapVidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapARPOptionGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapVidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapARPOptionSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapARPOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexDot1qFutureStVlanExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureStVlanTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureStVlanVidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureStVlanEgressEthertypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureStVlanEgressEthertypeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureStVlanEgressEthertypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureStVlanExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexDot1qFutureVlanPortSubnetMapExtTable
(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanPortSubnetMapExtVidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapExtARPOptionGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapExtRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapExtVidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapExtARPOptionSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapExtRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapExtVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapExtARPOptionTest
(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapExtRowStatusTest
(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanPortSubnetMapExtTableDep
(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 Dot1qFutureVlanGlobalsFdbFlushGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanUserDefinedTPIDGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanGlobalsFdbFlushSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanUserDefinedTPIDSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanGlobalsFdbFlushTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanUserDefinedTPIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanGlobalsFdbFlushDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1qFutureVlanUserDefinedTPIDDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexDot1qFutureVlanLoopbackTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1qFutureVlanLoopbackStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanLoopbackStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanLoopbackStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanLoopbackTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 Dot1qFutureVlanRemoteFdbFlushGet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanRemoteFdbFlushSet(tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanRemoteFdbFlushTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1qFutureVlanRemoteFdbFlushDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSVLANWR_H */
