/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnevmac.h,v 1.6 2016/07/16 11:15:03 siva Exp $
 *
 * Description: This file contains VLAN EVB Macros.
 *
 *******************************************************************/
#ifndef _VLNEVMAC_H
#define _VLNEVMAC_H

/* EVB Current Context Pointer . NOTE: as of now set it as zero */ 
#define VLAN_EVB_CURR_CONTEXT_PTR()    (gEvbGlobalInfo.apEvbContextInfo[0])
#define VLAN_EVB_GET_CONTEXT_PTR(ctxId)    (gEvbGlobalInfo.apEvbContextInfo[ctxId])
#define VLAN_EVB_SYSTEM_STATUS(CtxId) \
        (((gEvbGlobalInfo.apEvbContextInfo[CtxId]) == NULL) ? \
        VLAN_EVB_SYSTEM_SHUTDOWN : (gEvbGlobalInfo.apEvbContextInfo[CtxId])->\
                                            i4EvbSysCtrlStatus)
#define VLAN_EVB_MODULE_STATUS(CtxId) \
        (((gEvbGlobalInfo.apEvbContextInfo[CtxId]) == NULL) ? \
        VLAN_EVB_MODULE_DISABLE : (gEvbGlobalInfo.apEvbContextInfo[CtxId])->\
                                   i4EvbSysModStatus)

#define VLAN_EVB_CDCP_TLV_INFO_STR_HDR_LEN   (VLAN_EVB_CDCP_TLV_OUI_LEN +      \
                                              VLAN_EVB_CDCP_TLV_SUBTYPE_LEN +  \
                                              VLAN_EVB_CDCP_TLV_RESERVED_LEN)

#define VLAN_EVBCDCP_TLV_LEN              ((VLAN_EVB_CDCP_TLV_TYPE_TLV_LEN_SIZE + \
                                       CDCP_TLV_INFO_STR_HDR_LEN + \
                                       (VLAN_EVB_CDCP_TABLE_ENTRY_LEN * \
                                        VLAN_EVB_MAX_CDCP_ENTRIES_PER_PORT)))


#define VLAN_EVB_CDCP_CONSTRUCT_TLV_HEADER(u2Type, u2Len, pu2Header) \
       *pu2Header = u2Type << (UINT2)VLAN_EVB_CDCP_TLV_LENGTH_IN_BITS;         \
       *pu2Header = *pu2Header | u2Len;

#define VLAN_EVB_CDCP_PUT_1BYTE(pu1PktBuf,u1Val) \
{                                                                              \
       *pu1PktBuf = u1Val;                                                     \
        pu1PktBuf += VLAN_EVB_CDCP_ONE;                                        \
}

/* Constructing 2 byte fields */ 
#define VLAN_EVB_CDCP_PUT_2BYTE(pu1PktBuf,u2Val) \
        u2Val = OSIX_HTONS(u2Val);                                             \
        MEMCPY (pu1PktBuf, &u2Val, VLAN_EVB_CDCP_TWO);                         \
        pu1PktBuf += VLAN_EVB_CDCP_TWO;

#define VLAN_EVB_CDCP_PUT_3BYTE(pu1PktBuf,u4Svid,u4Scid) \
{                                                                              \
       *pu1PktBuf =(UINT1)( u4Scid >> 4);                                               \
       pu1PktBuf += 1;                                                         \
       *pu1PktBuf =(UINT1)( (u4Scid << 4) | (u4Svid >> 8));                             \
       pu1PktBuf += 1;                                                         \
       *pu1PktBuf = (u4Svid & 0xff);                                             \
       pu1PktBuf += 1;                                                         \
}

#define VLAN_EVB_CDCP_PUT_3BYTE_WITH_OFFSET(au1Buf,u2OffSet,u4Scid,u4Svid) \
{                                                                              \
       (au1Buf[u2OffSet]) =(UINT1) (u4Scid >> 4);                                       \
       (au1Buf[u2OffSet+1]) =(UINT1)( (u4Scid << 4) | (u4Svid >> 8));                   \
       (au1Buf[u2OffSet+2]) =(UINT1) (u4Svid & 0xff);                                   \
}

#define VLAN_EVB_CDCP_PUT_CHNCAP(pu1PktBuf,i4ChanCap) \
{                                                                              \
        *pu1PktBuf =(UINT1) (i4ChanCap >> 8);                                  \
         pu1PktBuf += 1;                                                       \
        *pu1PktBuf =(UINT1) (i4ChanCap & 0xff);                                         \
         pu1PktBuf += 1;                                                       \
}

#define VLAN_EVB_CDCP_PUT_OUI(pu1PktBuf,au1OUI) \
        MEMCPY (pu1PktBuf, au1OUI, VLAN_EVB_CDCP_TLV_OUI_LEN);                 \
        pu1PktBuf += VLAN_EVB_CDCP_TLV_OUI_LEN;

#define VLAN_EVB_CDCP_FILL_APPL_ID(EvbAppId) \
{                                                                              \
        EvbAppId.u2TlvType = VLAN_EVB_CDCP_TLV_TYPE;                           \
        EvbAppId.u1SubType = VLAN_EVB_CDCP_SUBTYPE;                            \
        MEMCPY(EvbAppId.au1OUI, gau1EvbCdcpOUI, VLAN_EVB_MAX_OUI_LEN);         \
}

/* Packet Processing */
#define VLAN_EVB_GET_1BYTE(pu1Buf,u1Val) \
{                                                                              \
        MEMCPY (&u1Val, pu1Buf, VLAN_EVB_CDCP_ONE);                            \
        pu1Buf += VLAN_EVB_CDCP_ONE;                                           \
}

#define VLAN_EVB_GET_2BYTE(pu1Buf,u2Val) \
{                                                                              \
        MEMCPY (&u2Val, pu1Buf, VLAN_EVB_CDCP_TWO);                            \
        pu1Buf += VLAN_EVB_CDCP_TWO;                                           \
        u2Val = (UINT2) (OSIX_HTONS(u2Val));                                   \
}

#define VLAN_EVB_GET_3BYTE(pu1Buf,au1Val) \
{                                                                              \
        MEMCPY (au1Val, pu1Buf, VLAN_EVB_CDCP_THREE);                          \
        pu1Buf += VLAN_EVB_CDCP_THREE;                                         \
}

#define VLAN_EVB_GET_4BYTE(pu1Buf,u4Val) \
{                                                                              \
        MEMCPY (&u4Val, pu1Buf, VLAN_EVB_CDCP_FOUR);                           \
        pu1Buf += VLAN_EVB_CDCP_FOUR;                                          \
        u4Val = (UINT4) (OSIX_HTONL(u4Val));                                   \
}
/* Validations */
#define VLAN_EVB_CDCP_VALIDATE_TYPE(u2Val)              ((u2Val & 0xfe00) >> 9)
#define VLAN_EVB_CDCP_GET_LENGTH(u2Val)                 (u2Val & 0x01ff)
#define VLAN_EVB_CDCP_VALIDATE_OUI(au1OUI) \
        ((MEMCMP (au1OUI, gau1EvbCdcpOUI, VLAN_EVB_CDCP_THREE) == 0) ? VLAN_TRUE: VLAN_FALSE)

#define VLAN_EVB_CDCP_VALIDATE_SUBTYPE(u1SubType) \
        ((u1SubType == VLAN_EVB_CDCP_SUBTYPE) ? VLAN_TRUE:VLAN_FALSE)

#define VLAN_EVB_CDCP_VALIDATE_ROLE(u4Val) \
        ((((u4Val & 0x80000000)>>31) == VLAN_EVB_CDCP_ROLE_STATION)?VLAN_TRUE:VLAN_FALSE)

#define VLAN_EVB_CDCP_VALIDATE_SCOMP(u4Val) \
        ((((u4Val & 0x08000000)>>27) == 1) ? VLAN_TRUE : VLAN_FALSE)

#define VLAN_EVB_CDCP_VALIDATE_CHANCAP(u4Val,u4ChnCapCount,u4Ref) \
        (((u4ChnCapCount = (u4Val & 0x00000fff)) <= u4Ref) ? VLAN_TRUE : VLAN_FALSE)

#define VLAN_EVB_PARSE_PAIR(u4Pair,u2SCID,u2SVID) \
{                                                                              \
        u4Pair = OSIX_HTONL (u4Pair);                                          \
        /* SCID and SVID pairs are set in the first 3 bytes. u4Pair is as below\
         *  _______________________________________________________            \
         * | byte 1     |   byte 2         |   byte 3  |  byte 4   |           \
         * |____________|__________________|___________|___________|           \
         * | SCID-MSB   |SCID-LSB/SVID-MSB | SVID-MSB  |  00       |           \
         * |____________|__________________|___________|___________|           \
         */                                                                    \
        u2SCID = (UINT2)(u4Pair >> 20);                                                 \
        u2SVID = (UINT2)((u4Pair & 0x000fff00) >> 8);                                   \
}

/* Macro to get the pointer of the TLV pair after the default pair presence */
#define VLAN_EVB_CDCP_GET_TLV_START_PTR(au1Tlv,pu1Ptr) \
       pu1Ptr = &(au1Tlv [VLAN_EVB_CDCP_TLV_HDR_LEN + VLAN_EVB_CDCP_MANDATORY +\
                        VLAN_EVB_CDCP_THREE]);

#define VLAN_EVB_CDCP_PUT_ROLE(pu1PktBuf, u1Role)    \
        MEMCPY (pu1PktBuf, u1Role, VLAN_EVB_CDCP_TLV_OUI_LEN);     \
    pu1PktBuf += VLAN_EVB_CDCP_ONE;

#define VLAN_EVB_CDCP_PUT_SUBTYPE(pu1PktBuf, u1Subtype)    \
        MEMCPY (pu1PktBuf, u1Subtype, VLAN_EVB_CDCP_TLV_SUBTYPE_LEN);     \
    pu1PktBuf += VLAN_EVB_CDCP_TLV_SUBTYPE_LEN;

#define VLAN_EVB_CDCP_CONSTRUCT_RRSR_FIELDS(pu1TlvAttr) \
{                                                                              \
    *pu1TlvAttr = VLAN_EVB_CDCP_ROLE_BRIDGE <<                                 \
                  (UINT1)VLAN_EVB_CDCP_TLV_ROLE_IN_BITS;                       \
    *pu1TlvAttr = *pu1TlvAttr |                                                \
                  (VLAN_EVB_CDCP_ONE << VLAN_EVB_CDCP_TLV_RES1_IN_BITS);       \
    *pu1TlvAttr = *pu1TlvAttr |                                                \
                  (VLAN_EVB_CDCP_ONE << VLAN_EVB_CDCP_TLV_SCOMP_IN_BITS);      \
    *pu1TlvAttr = *pu1TlvAttr |  VLAN_EVB_CDCP_THREE;                          \
}

#define VLAN_EVB_CDCP_PUT_RES1_CHNCAP(u1CdcpChanCap,pu1ResChnCap) \
{                                                                              \
   *pu1ResChnCap = 0xF0 | u1CdcpChanCap;                                       \
}

/* EVB Memory block alloc and release macros */
#define VLAN_EVB_GET_BUF(u1BufType,u4Size) \
        VlanEvbGetBuf ((UINT1)(u1BufType), (UINT4)(u4Size))

#define VLAN_EVB_RELEASE_BUF(u1BufType,pu1Buf) \
        VlanEvbReleaseBuf ((UINT1)(u1BufType), (UINT1 *)(pu1Buf))

#define CFA_ISALPHA(c) (((c) >= 'a' && (c) <= 'z') || ((c) >= 'A' && (c) <= 'Z'))

#endif  /* _VLNEVMAC_H */
