/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsbrdb.h,v 1.6 2011/03/21 12:26:43 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMSBRDB_H
#define _FSMSBRDB_H

UINT1 FsDot1dBaseTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1dBasePortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1dStpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1dStpPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1dTpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1dTpFdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsDot1dTpPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1dStaticTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot1dStaticAllowedToGoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmsbr [] ={1,3,6,1,4,1,2076,116};
tSNMP_OID_TYPE fsmsbrOID = {8, fsmsbr};


/* Generated OID's for tables */
UINT4 FsDot1dBaseTable [] ={1,3,6,1,4,1,2076,116,1,1};
tSNMP_OID_TYPE FsDot1dBaseTableOID = {10, FsDot1dBaseTable};


UINT4 FsDot1dBasePortTable [] ={1,3,6,1,4,1,2076,116,1,2};
tSNMP_OID_TYPE FsDot1dBasePortTableOID = {10, FsDot1dBasePortTable};


UINT4 FsDot1dStpTable [] ={1,3,6,1,4,1,2076,116,2,1};
tSNMP_OID_TYPE FsDot1dStpTableOID = {10, FsDot1dStpTable};


UINT4 FsDot1dStpPortTable [] ={1,3,6,1,4,1,2076,116,2,2};
tSNMP_OID_TYPE FsDot1dStpPortTableOID = {10, FsDot1dStpPortTable};


UINT4 FsDot1dTpTable [] ={1,3,6,1,4,1,2076,116,4,1};
tSNMP_OID_TYPE FsDot1dTpTableOID = {10, FsDot1dTpTable};


UINT4 FsDot1dTpFdbTable [] ={1,3,6,1,4,1,2076,116,4,2};
tSNMP_OID_TYPE FsDot1dTpFdbTableOID = {10, FsDot1dTpFdbTable};


UINT4 FsDot1dTpPortTable [] ={1,3,6,1,4,1,2076,116,4,3};
tSNMP_OID_TYPE FsDot1dTpPortTableOID = {10, FsDot1dTpPortTable};


UINT4 FsDot1dStaticTable [] ={1,3,6,1,4,1,2076,116,5,1};
tSNMP_OID_TYPE FsDot1dStaticTableOID = {10, FsDot1dStaticTable};


UINT4 FsDot1dStaticAllowedToGoTable [] ={1,3,6,1,4,1,2076,116,5,2};
tSNMP_OID_TYPE FsDot1dStaticAllowedToGoTableOID = {10, FsDot1dStaticAllowedToGoTable};




UINT4 FsDot1dBaseContextId [ ] ={1,3,6,1,4,1,2076,116,1,1,1,1};
UINT4 FsDot1dBaseBridgeAddress [ ] ={1,3,6,1,4,1,2076,116,1,1,1,2};
UINT4 FsDot1dBaseNumPorts [ ] ={1,3,6,1,4,1,2076,116,1,1,1,3};
UINT4 FsDot1dBaseType [ ] ={1,3,6,1,4,1,2076,116,1,1,1,4};
UINT4 FsDot1dBasePort [ ] ={1,3,6,1,4,1,2076,116,1,2,1,1};
UINT4 FsDot1dBasePortIfIndex [ ] ={1,3,6,1,4,1,2076,116,1,2,1,2};
UINT4 FsDot1dBasePortCircuit [ ] ={1,3,6,1,4,1,2076,116,1,2,1,3};
UINT4 FsDot1dBasePortDelayExceededDiscards [ ] ={1,3,6,1,4,1,2076,116,1,2,1,4};
UINT4 FsDot1dBasePortMtuExceededDiscards [ ] ={1,3,6,1,4,1,2076,116,1,2,1,5};
UINT4 FsDot1dStpContextId [ ] ={1,3,6,1,4,1,2076,116,2,1,1,1};
UINT4 FsDot1dStpProtocolSpecification [ ] ={1,3,6,1,4,1,2076,116,2,1,1,2};
UINT4 FsDot1dStpPriority [ ] ={1,3,6,1,4,1,2076,116,2,1,1,3};
UINT4 FsDot1dStpTimeSinceTopologyChange [ ] ={1,3,6,1,4,1,2076,116,2,1,1,4};
UINT4 FsDot1dStpTopChanges [ ] ={1,3,6,1,4,1,2076,116,2,1,1,5};
UINT4 FsDot1dStpDesignatedRoot [ ] ={1,3,6,1,4,1,2076,116,2,1,1,6};
UINT4 FsDot1dStpRootCost [ ] ={1,3,6,1,4,1,2076,116,2,1,1,7};
UINT4 FsDot1dStpRootPort [ ] ={1,3,6,1,4,1,2076,116,2,1,1,8};
UINT4 FsDot1dStpMaxAge [ ] ={1,3,6,1,4,1,2076,116,2,1,1,9};
UINT4 FsDot1dStpHelloTime [ ] ={1,3,6,1,4,1,2076,116,2,1,1,10};
UINT4 FsDot1dStpHoldTime [ ] ={1,3,6,1,4,1,2076,116,2,1,1,11};
UINT4 FsDot1dStpForwardDelay [ ] ={1,3,6,1,4,1,2076,116,2,1,1,12};
UINT4 FsDot1dStpBridgeMaxAge [ ] ={1,3,6,1,4,1,2076,116,2,1,1,13};
UINT4 FsDot1dStpBridgeHelloTime [ ] ={1,3,6,1,4,1,2076,116,2,1,1,14};
UINT4 FsDot1dStpBridgeForwardDelay [ ] ={1,3,6,1,4,1,2076,116,2,1,1,15};
UINT4 FsDot1dStpPort [ ] ={1,3,6,1,4,1,2076,116,2,2,1,1};
UINT4 FsDot1dStpPortPriority [ ] ={1,3,6,1,4,1,2076,116,2,2,1,2};
UINT4 FsDot1dStpPortState [ ] ={1,3,6,1,4,1,2076,116,2,2,1,3};
UINT4 FsDot1dStpPortEnable [ ] ={1,3,6,1,4,1,2076,116,2,2,1,4};
UINT4 FsDot1dStpPortPathCost [ ] ={1,3,6,1,4,1,2076,116,2,2,1,5};
UINT4 FsDot1dStpPortDesignatedRoot [ ] ={1,3,6,1,4,1,2076,116,2,2,1,6};
UINT4 FsDot1dStpPortDesignatedCost [ ] ={1,3,6,1,4,1,2076,116,2,2,1,7};
UINT4 FsDot1dStpPortDesignatedBridge [ ] ={1,3,6,1,4,1,2076,116,2,2,1,8};
UINT4 FsDot1dStpPortDesignatedPort [ ] ={1,3,6,1,4,1,2076,116,2,2,1,9};
UINT4 FsDot1dStpPortForwardTransitions [ ] ={1,3,6,1,4,1,2076,116,2,2,1,10};
UINT4 FsDot1dStpPortPathCost32 [ ] ={1,3,6,1,4,1,2076,116,2,2,1,11};
UINT4 FsDot1dTpLearnedEntryDiscards [ ] ={1,3,6,1,4,1,2076,116,4,1,1,1};
UINT4 FsDot1dTpAgingTime [ ] ={1,3,6,1,4,1,2076,116,4,1,1,2};
UINT4 FsDot1dTpFdbAddress [ ] ={1,3,6,1,4,1,2076,116,4,2,1,1};
UINT4 FsDot1dTpFdbPort [ ] ={1,3,6,1,4,1,2076,116,4,2,1,2};
UINT4 FsDot1dTpFdbStatus [ ] ={1,3,6,1,4,1,2076,116,4,2,1,3};
UINT4 FsDot1dTpPort [ ] ={1,3,6,1,4,1,2076,116,4,3,1,1};
UINT4 FsDot1dTpPortMaxInfo [ ] ={1,3,6,1,4,1,2076,116,4,3,1,2};
UINT4 FsDot1dTpPortInFrames [ ] ={1,3,6,1,4,1,2076,116,4,3,1,3};
UINT4 FsDot1dTpPortOutFrames [ ] ={1,3,6,1,4,1,2076,116,4,3,1,4};
UINT4 FsDot1dTpPortInDiscards [ ] ={1,3,6,1,4,1,2076,116,4,3,1,5};
UINT4 FsDot1dStaticAddress [ ] ={1,3,6,1,4,1,2076,116,5,1,1,1};
UINT4 FsDot1dStaticReceivePort [ ] ={1,3,6,1,4,1,2076,116,5,1,1,2};
UINT4 FsDot1dStaticRowStatus [ ] ={1,3,6,1,4,1,2076,116,5,1,1,3};
UINT4 FsDot1dStaticStatus [ ] ={1,3,6,1,4,1,2076,116,5,1,1,4};
UINT4 FsDot1dStaticAllowedIsMember [ ] ={1,3,6,1,4,1,2076,116,5,2,1,1};




tMbDbEntry FsDot1dBaseTableMibEntry[]= {

{{12,FsDot1dBaseContextId}, GetNextIndexFsDot1dBaseTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1dBaseTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dBaseBridgeAddress}, GetNextIndexFsDot1dBaseTable, FsDot1dBaseBridgeAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsDot1dBaseTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dBaseNumPorts}, GetNextIndexFsDot1dBaseTable, FsDot1dBaseNumPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDot1dBaseTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dBaseType}, GetNextIndexFsDot1dBaseTable, FsDot1dBaseTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1dBaseTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dBaseTableEntry = { 4, FsDot1dBaseTableMibEntry };

tMbDbEntry FsDot1dBasePortTableMibEntry[]= {

{{12,FsDot1dBasePort}, GetNextIndexFsDot1dBasePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dBasePortIfIndex}, GetNextIndexFsDot1dBasePortTable, FsDot1dBasePortIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dBasePortCircuit}, GetNextIndexFsDot1dBasePortTable, FsDot1dBasePortCircuitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, FsDot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dBasePortDelayExceededDiscards}, GetNextIndexFsDot1dBasePortTable, FsDot1dBasePortDelayExceededDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dBasePortMtuExceededDiscards}, GetNextIndexFsDot1dBasePortTable, FsDot1dBasePortMtuExceededDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dBasePortTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dBasePortTableEntry = { 5, FsDot1dBasePortTableMibEntry };

tMbDbEntry FsDot1dStpTableMibEntry[]= {

{{12,FsDot1dStpContextId}, GetNextIndexFsDot1dStpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpProtocolSpecification}, GetNextIndexFsDot1dStpTable, FsDot1dStpProtocolSpecificationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPriority}, GetNextIndexFsDot1dStpTable, FsDot1dStpPriorityGet, FsDot1dStpPrioritySet, FsDot1dStpPriorityTest, FsDot1dStpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpTimeSinceTopologyChange}, GetNextIndexFsDot1dStpTable, FsDot1dStpTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpTopChanges}, GetNextIndexFsDot1dStpTable, FsDot1dStpTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpDesignatedRoot}, GetNextIndexFsDot1dStpTable, FsDot1dStpDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpRootCost}, GetNextIndexFsDot1dStpTable, FsDot1dStpRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpRootPort}, GetNextIndexFsDot1dStpTable, FsDot1dStpRootPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpMaxAge}, GetNextIndexFsDot1dStpTable, FsDot1dStpMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpHelloTime}, GetNextIndexFsDot1dStpTable, FsDot1dStpHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpHoldTime}, GetNextIndexFsDot1dStpTable, FsDot1dStpHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpForwardDelay}, GetNextIndexFsDot1dStpTable, FsDot1dStpForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpBridgeMaxAge}, GetNextIndexFsDot1dStpTable, FsDot1dStpBridgeMaxAgeGet, FsDot1dStpBridgeMaxAgeSet, FsDot1dStpBridgeMaxAgeTest, FsDot1dStpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpBridgeHelloTime}, GetNextIndexFsDot1dStpTable, FsDot1dStpBridgeHelloTimeGet, FsDot1dStpBridgeHelloTimeSet, FsDot1dStpBridgeHelloTimeTest, FsDot1dStpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpBridgeForwardDelay}, GetNextIndexFsDot1dStpTable, FsDot1dStpBridgeForwardDelayGet, FsDot1dStpBridgeForwardDelaySet, FsDot1dStpBridgeForwardDelayTest, FsDot1dStpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStpTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dStpTableEntry = { 15, FsDot1dStpTableMibEntry };

tMbDbEntry FsDot1dStpPortTableMibEntry[]= {

{{12,FsDot1dStpPort}, GetNextIndexFsDot1dStpPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortPriority}, GetNextIndexFsDot1dStpPortTable, FsDot1dStpPortPriorityGet, FsDot1dStpPortPrioritySet, FsDot1dStpPortPriorityTest, FsDot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortState}, GetNextIndexFsDot1dStpPortTable, FsDot1dStpPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortEnable}, GetNextIndexFsDot1dStpPortTable, FsDot1dStpPortEnableGet, FsDot1dStpPortEnableSet, FsDot1dStpPortEnableTest, FsDot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortPathCost}, GetNextIndexFsDot1dStpPortTable, FsDot1dStpPortPathCostGet, FsDot1dStpPortPathCostSet, FsDot1dStpPortPathCostTest, FsDot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortDesignatedRoot}, GetNextIndexFsDot1dStpPortTable, FsDot1dStpPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortDesignatedCost}, GetNextIndexFsDot1dStpPortTable, FsDot1dStpPortDesignatedCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortDesignatedBridge}, GetNextIndexFsDot1dStpPortTable, FsDot1dStpPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortDesignatedPort}, GetNextIndexFsDot1dStpPortTable, FsDot1dStpPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortForwardTransitions}, GetNextIndexFsDot1dStpPortTable, FsDot1dStpPortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dStpPortPathCost32}, GetNextIndexFsDot1dStpPortTable, FsDot1dStpPortPathCost32Get, FsDot1dStpPortPathCost32Set, FsDot1dStpPortPathCost32Test, FsDot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot1dStpPortTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dStpPortTableEntry = { 11, FsDot1dStpPortTableMibEntry };

tMbDbEntry FsDot1dTpTableMibEntry[]= {

{{12,FsDot1dTpLearnedEntryDiscards}, GetNextIndexFsDot1dTpTable, FsDot1dTpLearnedEntryDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dTpTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dTpAgingTime}, GetNextIndexFsDot1dTpTable, FsDot1dTpAgingTimeGet, FsDot1dTpAgingTimeSet, FsDot1dTpAgingTimeTest, FsDot1dTpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot1dTpTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dTpTableEntry = { 2, FsDot1dTpTableMibEntry };

tMbDbEntry FsDot1dTpFdbTableMibEntry[]= {

{{12,FsDot1dTpFdbAddress}, GetNextIndexFsDot1dTpFdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsDot1dTpFdbTableINDEX, 2, 0, 0, NULL},

{{12,FsDot1dTpFdbPort}, GetNextIndexFsDot1dTpFdbTable, FsDot1dTpFdbPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDot1dTpFdbTableINDEX, 2, 0, 0, NULL},

{{12,FsDot1dTpFdbStatus}, GetNextIndexFsDot1dTpFdbTable, FsDot1dTpFdbStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot1dTpFdbTableINDEX, 2, 0, 0, NULL},
};
tMibData FsDot1dTpFdbTableEntry = { 3, FsDot1dTpFdbTableMibEntry };

tMbDbEntry FsDot1dTpPortTableMibEntry[]= {

{{12,FsDot1dTpPort}, GetNextIndexFsDot1dTpPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dTpPortMaxInfo}, GetNextIndexFsDot1dTpPortTable, FsDot1dTpPortMaxInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dTpPortInFrames}, GetNextIndexFsDot1dTpPortTable, FsDot1dTpPortInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dTpPortOutFrames}, GetNextIndexFsDot1dTpPortTable, FsDot1dTpPortOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{12,FsDot1dTpPortInDiscards}, GetNextIndexFsDot1dTpPortTable, FsDot1dTpPortInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDot1dTpPortTableINDEX, 1, 0, 0, NULL},
};
tMibData FsDot1dTpPortTableEntry = { 5, FsDot1dTpPortTableMibEntry };

tMbDbEntry FsDot1dStaticTableMibEntry[]= {

{{12,FsDot1dStaticAddress}, GetNextIndexFsDot1dStaticTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsDot1dStaticTableINDEX, 3, 0, 0, NULL},

{{12,FsDot1dStaticReceivePort}, GetNextIndexFsDot1dStaticTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot1dStaticTableINDEX, 3, 0, 0, NULL},

{{12,FsDot1dStaticRowStatus}, GetNextIndexFsDot1dStaticTable, FsDot1dStaticRowStatusGet, FsDot1dStaticRowStatusSet, FsDot1dStaticRowStatusTest, FsDot1dStaticTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStaticTableINDEX, 3, 0, 1, NULL},

{{12,FsDot1dStaticStatus}, GetNextIndexFsDot1dStaticTable, FsDot1dStaticStatusGet, FsDot1dStaticStatusSet, FsDot1dStaticStatusTest, FsDot1dStaticTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStaticTableINDEX, 3, 0, 0, NULL},
};
tMibData FsDot1dStaticTableEntry = { 4, FsDot1dStaticTableMibEntry };

tMbDbEntry FsDot1dStaticAllowedToGoTableMibEntry[]= {

{{12,FsDot1dStaticAllowedIsMember}, GetNextIndexFsDot1dStaticAllowedToGoTable, FsDot1dStaticAllowedIsMemberGet, FsDot1dStaticAllowedIsMemberSet, FsDot1dStaticAllowedIsMemberTest, FsDot1dStaticAllowedToGoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot1dStaticAllowedToGoTableINDEX, 4, 0, 0, NULL},
};
tMibData FsDot1dStaticAllowedToGoTableEntry = { 1, FsDot1dStaticAllowedToGoTableMibEntry };

#endif /* _FSMSBRDB_H */

