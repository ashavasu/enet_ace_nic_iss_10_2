/** $Id: vlnpbglb.h,v 1.4 2011/11/30 11:25:06 siva Exp $ */
#ifndef _VLNPBGLB_H
#define _VLNPBGLB_H

/* 802.1ad PCP decoding table */
UINT1     gau1PcpDecPriority[VLAN_PB_MAX_PCP_SEL_ROW][VLAN_PB_MAX_PCP] =
    { {0, 1, 2, 3, 4, 5, 6, 7},
      {0, 1, 2, 3, 4, 4, 6, 7},
      {0, 1, 2, 2, 4, 4, 6, 7},
      {0, 0, 2, 2, 4, 4, 6, 7},
    };

UINT1     gau1PcpDecDropEligible[VLAN_PB_MAX_PCP_SEL_ROW][VLAN_PB_MAX_PCP] =
    { {0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 1, 0, 0, 0},
      {0, 0, 1, 0, 1, 0, 0, 0},
      {1, 0, 1, 0, 1, 0, 0, 0},
    };

/* 802.1ad PCP encoding table */
UINT1  gau1PcpEncValue[VLAN_PB_MAX_PCP_SEL_ROW][VLAN_PB_MAX_PRIORITY]
                                         [VLAN_PB_MAX_DROP_ELIGIBLE]=
   {
      {
         {0,0},
         {1,1},
         {2,2},
         {3,3},
         {4,4},
         {5,5},
         {6,6},
         {7,7},
      },
      {
         {0,0},
         {1,1},
         {2,2},
         {3,3},
         {5,4},
         {5,4},
         {6,6},
         {7,7},
      },
      {
         {0,0},
         {1,1},
         {3,2},
         {3,2},
         {5,4},
         {5,4},
         {6,6},
         {7,7},
      },
      {
         {1,0},
         {1,0},
         {3,2},
         {3,2},
         {5,4},
         {5,4},
         {6,6},
         {7,7},
      }
   };

/* The index into the table is the S-VLAN classification
 * priority.*/


#ifdef PB_WANTED
tVlanSVlanTableInfo       gVlanSVlanTableInfo[] = 
{
    {SVLAN_PORT_CVLAN_MAX_ENTRIES,
     VLAN_SVLAN_PORT_CVLAN_TYPE,
     VlanPbPortCVlanCmp},
     
    {SVLAN_PORT_CVLAN_SRCMAC_MAX_ENTRIES,
     VLAN_SVLAN_PORT_CVLAN_SRCMAC_TYPE,
     VlanPbPortCVlanSrcMacCmp},
     
    {SVLAN_PORT_CVLAN_DSTMAC_MAX_ENTRIES,
     VLAN_SVLAN_PORT_CVLAN_DSTMAC_TYPE,
     VlanPbPortCVlanDstMacCmp},
     
    {SVLAN_PORT_CVLAN_DSCP_MAX_ENTRIES,
     VLAN_SVLAN_PORT_CVLAN_DSCP_TYPE,
     VlanPbPortCVlanDscpCmp},

    {SVLAN_PORT_CVLAN_DSTIP_MAX_ENTRIES,
     VLAN_SVLAN_PORT_CVLAN_DSTIP_TYPE,
     VlanPbPortCVlanDstIpCmp},
     
    {SVLAN_PORT_SRCMAC_MAX_ENTRIES,
     VLAN_SVLAN_PORT_SRCMAC_TYPE,
     VlanPbPortSrcMacCmp},
     
    {SVLAN_PORT_DSTMAC_MAX_ENTRIES,
     VLAN_SVLAN_PORT_DSTMAC_TYPE,
     VlanPbPortDstMacCmp},
     
    {SVLAN_PORT_SRCIP_MAX_ENTRIES,
     VLAN_SVLAN_PORT_SRCIP_TYPE,
     VlanPbPortSrcIpCmp},
     
    {SVLAN_PORT_DSTIP_MAX_ENTRIES,
     VLAN_SVLAN_PORT_DSTIP_TYPE,
     VlanPbPortDstIpCmp},
     
    {SVLAN_PORT_DSCP_MAX_ENTRIES,
     VLAN_SVLAN_PORT_DSCP_TYPE,
     VlanPbPortDscpCmp},

    {SVLAN_PORT_SRCIP_DSTIP_MAX_ENTRIES,
     VLAN_SVLAN_PORT_SRCDSTIP_TYPE,
     VlanPbPortSrcDstIpCmp}
};
#endif

tVlanPbPoolInfo       gVlanPbPoolInfo;
#endif
