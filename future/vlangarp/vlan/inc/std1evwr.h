/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: std1evwr.h,v 1.1 2015/09/10 13:21:49 siva Exp $
* Description: This header file contains all prototype of the wrapper
*              routines in VLAN Modules.
****************************************************************************/

#ifndef _STD1EVWR_H
#define _STD1EVWR_H

VOID RegisterSTD1EV(VOID);

VOID UnRegisterSTD1EV(VOID);
INT4 Ieee8021BridgeEvbSysTypeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysNumExternalPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEvbLldpTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEvbLldpManualGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEvbLldpGidCapableGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEcpAckTimerGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEcpMaxRetriesGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelayGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEvbLldpTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEvbLldpManualSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEvbLldpGidCapableSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEcpAckTimerSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEcpMaxRetriesSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelaySet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEvbLldpTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEvbLldpManualTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEvbLldpGidCapableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEcpAckTimerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEcpMaxRetriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSysEvbLldpTxEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Ieee8021BridgeEvbSysEvbLldpManualDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Ieee8021BridgeEvbSysEvbLldpGidCapableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Ieee8021BridgeEvbSysEcpAckTimerDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Ieee8021BridgeEvbSysEcpMaxRetriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Ieee8021BridgeEvbSysVdpDfltRsrcWaitDelayDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Ieee8021BridgeEvbSysVdpDfltReinitKeepAliveDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexIeee8021BridgeEvbSbpTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeEvbSbpLldpManualGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSbpVdpOperRsrcWaitDelayGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSbpVdpOperReinitKeepAliveGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSbpVdpOperToutKeepAliveGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSbpLldpManualSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSbpLldpManualTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSbpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgeEvbVSIDBTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeEvbVSITimeSinceCreateGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVsiVdpOperCmdGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVsiOperRevertGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVsiOperHardGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVsiOperReasonGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVSIMgrIDGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVSITypeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVSITypeVersionGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVSIMvFormatGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVSINumMACsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVDPMachineStateGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVDPCommandsSucceededGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVDPCommandsFailedGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVDPCommandRevertsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbVDPCounterDiscontinuityGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIeee8021BridgeEvbVSIDBMacTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeEvbVSIVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIeee8021BridgeEvbUAPConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeEvbUAPComponentIdGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPPortGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUapConfigIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchCdcpAdminEnableGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPRoleGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPChanCapGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchOperCDCPChanCapGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLowGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHighGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchOperStateGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSchCdcpRemoteEnabledGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbSchCdcpRemoteRoleGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPConfigStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPConfigRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchCdcpAdminEnableSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPRoleSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPChanCapSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLowSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHighSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPConfigStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPConfigRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchCdcpAdminEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPRoleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPChanCapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolLowTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPSchAdminCDCPSVIDPoolHighTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPConfigStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPConfigRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbUAPConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgeEvbCAPConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeEvbCAPComponentIdGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCapConfigIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPPortGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPSChannelIDGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPCompIDGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPPortGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPCompIDSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPPortSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPCompIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPAssociateSBPOrURPPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbCAPConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgeEvbURPTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeEvbURPIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPBindToISSPortGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPLldpManualGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPVdpOperRsrcWaitDelayGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPVdpOperRespWaitDelayGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPVdpOperReinitKeepAliveGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPBindToISSPortSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPLldpManualSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPVdpOperRespWaitDelaySet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPVdpOperReinitKeepAliveSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPBindToISSPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPLldpManualTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPVdpOperRespWaitDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPVdpOperReinitKeepAliveTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbURPTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgeEvbEcpTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeEvbEcpOperAckTimerInitGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbEcpOperMaxRetriesGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbEcpTxFrameCountGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbEcpTxRetryCountGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbEcpTxFailuresGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeEvbEcpRxFrameCountGet(tSnmpIndex *, tRetVal *);
#endif /* _STD1EVWR_H */


