/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: fsmid1wr.h,v 1.3 2016/02/02 12:20:10 siva Exp $
* Description: This header file contains all prototype of the wrapper
*              routines in VLAN Modules.
****************************************************************************/

#ifndef _FSMID1WR_H
#define _FSMID1WR_H
INT4 GetNextIndexFsMIEvbSystemTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMID1(VOID);

VOID UnRegisterFSMID1(VOID);
INT4 FsMIEvbSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemTrapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemStatsClearGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSchannelIdModeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemTrapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemStatsClearSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSchannelIdModeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemTrapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemStatsClearTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSchannelIdModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSystemTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIEvbCAPConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIEvbCAPSChannelIDGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbCAPSChannelIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbCAPSChNegoStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbPhyPortGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSchIDGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSChannelFilterStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbCAPSChannelIDSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbCAPSChannelIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSChannelFilterStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbCAPSChannelIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEvbCAPSChannelIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSChannelFilterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEvbCAPConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIEvbUAPConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIEvbUAPSchCdcpModeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbUAPSchCdcpModeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbUAPSchCdcpModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEvbUAPConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIEvbUAPStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIEvbTxCdcpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbRxCdcpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSChAllocFailCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSChActiveFailCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbSVIDPoolExceedsCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbCdcpRejectStationReqGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEvbCdcpOtherDropCountGet(tSnmpIndex *, tRetVal *);
#endif /* _FSMID1WR_H */


