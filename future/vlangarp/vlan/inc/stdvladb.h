/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdvladb.h,v 1.8 2008/12/31 06:24:19 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDVLADB_H
#define _STDVLADB_H

UINT1 Dot1qFdbTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1qTpFdbTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot1qTpGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot1qForwardAllTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1qForwardUnregisteredTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1qStaticUnicastTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1qStaticMulticastTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1qVlanCurrentTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1qVlanStaticTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1qPortVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1qPortVlanStatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1qPortVlanHCStatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot1qLearningConstraintsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1vProtocolGroupTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Dot1vProtocolPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdvla [] ={1,3,6,1,2,1,17,7};
tSNMP_OID_TYPE stdvlaOID = {8, stdvla};


UINT4 Dot1qVlanVersionNumber [ ] ={1,3,6,1,2,1,17,7,1,1,1};
UINT4 Dot1qMaxVlanId [ ] ={1,3,6,1,2,1,17,7,1,1,2};
UINT4 Dot1qMaxSupportedVlans [ ] ={1,3,6,1,2,1,17,7,1,1,3};
UINT4 Dot1qNumVlans [ ] ={1,3,6,1,2,1,17,7,1,1,4};
UINT4 Dot1qGvrpStatus [ ] ={1,3,6,1,2,1,17,7,1,1,5};
UINT4 Dot1qFdbId [ ] ={1,3,6,1,2,1,17,7,1,2,1,1,1};
UINT4 Dot1qFdbDynamicCount [ ] ={1,3,6,1,2,1,17,7,1,2,1,1,2};
UINT4 Dot1qTpFdbAddress [ ] ={1,3,6,1,2,1,17,7,1,2,2,1,1};
UINT4 Dot1qTpFdbPort [ ] ={1,3,6,1,2,1,17,7,1,2,2,1,2};
UINT4 Dot1qTpFdbStatus [ ] ={1,3,6,1,2,1,17,7,1,2,2,1,3};
UINT4 Dot1qTpGroupAddress [ ] ={1,3,6,1,2,1,17,7,1,2,3,1,1};
UINT4 Dot1qTpGroupEgressPorts [ ] ={1,3,6,1,2,1,17,7,1,2,3,1,2};
UINT4 Dot1qTpGroupLearnt [ ] ={1,3,6,1,2,1,17,7,1,2,3,1,3};
UINT4 Dot1qForwardAllPorts [ ] ={1,3,6,1,2,1,17,7,1,2,4,1,1};
UINT4 Dot1qForwardAllStaticPorts [ ] ={1,3,6,1,2,1,17,7,1,2,4,1,2};
UINT4 Dot1qForwardAllForbiddenPorts [ ] ={1,3,6,1,2,1,17,7,1,2,4,1,3};
UINT4 Dot1qForwardUnregisteredPorts [ ] ={1,3,6,1,2,1,17,7,1,2,5,1,1};
UINT4 Dot1qForwardUnregisteredStaticPorts [ ] ={1,3,6,1,2,1,17,7,1,2,5,1,2};
UINT4 Dot1qForwardUnregisteredForbiddenPorts [ ] ={1,3,6,1,2,1,17,7,1,2,5,1,3};
UINT4 Dot1qStaticUnicastAddress [ ] ={1,3,6,1,2,1,17,7,1,3,1,1,1};
UINT4 Dot1qStaticUnicastReceivePort [ ] ={1,3,6,1,2,1,17,7,1,3,1,1,2};
UINT4 Dot1qStaticUnicastAllowedToGoTo [ ] ={1,3,6,1,2,1,17,7,1,3,1,1,3};
UINT4 Dot1qStaticUnicastStatus [ ] ={1,3,6,1,2,1,17,7,1,3,1,1,4};
UINT4 Dot1qStaticMulticastAddress [ ] ={1,3,6,1,2,1,17,7,1,3,2,1,1};
UINT4 Dot1qStaticMulticastReceivePort [ ] ={1,3,6,1,2,1,17,7,1,3,2,1,2};
UINT4 Dot1qStaticMulticastStaticEgressPorts [ ] ={1,3,6,1,2,1,17,7,1,3,2,1,3};
UINT4 Dot1qStaticMulticastForbiddenEgressPorts [ ] ={1,3,6,1,2,1,17,7,1,3,2,1,4};
UINT4 Dot1qStaticMulticastStatus [ ] ={1,3,6,1,2,1,17,7,1,3,2,1,5};
UINT4 Dot1qVlanNumDeletes [ ] ={1,3,6,1,2,1,17,7,1,4,1};
UINT4 Dot1qVlanTimeMark [ ] ={1,3,6,1,2,1,17,7,1,4,2,1,1};
UINT4 Dot1qVlanIndex [ ] ={1,3,6,1,2,1,17,7,1,4,2,1,2};
UINT4 Dot1qVlanFdbId [ ] ={1,3,6,1,2,1,17,7,1,4,2,1,3};
UINT4 Dot1qVlanCurrentEgressPorts [ ] ={1,3,6,1,2,1,17,7,1,4,2,1,4};
UINT4 Dot1qVlanCurrentUntaggedPorts [ ] ={1,3,6,1,2,1,17,7,1,4,2,1,5};
UINT4 Dot1qVlanStatus [ ] ={1,3,6,1,2,1,17,7,1,4,2,1,6};
UINT4 Dot1qVlanCreationTime [ ] ={1,3,6,1,2,1,17,7,1,4,2,1,7};
UINT4 Dot1qVlanStaticName [ ] ={1,3,6,1,2,1,17,7,1,4,3,1,1};
UINT4 Dot1qVlanStaticEgressPorts [ ] ={1,3,6,1,2,1,17,7,1,4,3,1,2};
UINT4 Dot1qVlanForbiddenEgressPorts [ ] ={1,3,6,1,2,1,17,7,1,4,3,1,3};
UINT4 Dot1qVlanStaticUntaggedPorts [ ] ={1,3,6,1,2,1,17,7,1,4,3,1,4};
UINT4 Dot1qVlanStaticRowStatus [ ] ={1,3,6,1,2,1,17,7,1,4,3,1,5};
UINT4 Dot1qNextFreeLocalVlanIndex [ ] ={1,3,6,1,2,1,17,7,1,4,4};
UINT4 Dot1qPvid [ ] ={1,3,6,1,2,1,17,7,1,4,5,1,1};
UINT4 Dot1qPortAcceptableFrameTypes [ ] ={1,3,6,1,2,1,17,7,1,4,5,1,2};
UINT4 Dot1qPortIngressFiltering [ ] ={1,3,6,1,2,1,17,7,1,4,5,1,3};
UINT4 Dot1qPortGvrpStatus [ ] ={1,3,6,1,2,1,17,7,1,4,5,1,4};
UINT4 Dot1qPortGvrpFailedRegistrations [ ] ={1,3,6,1,2,1,17,7,1,4,5,1,5};
UINT4 Dot1qPortGvrpLastPduOrigin [ ] ={1,3,6,1,2,1,17,7,1,4,5,1,6};
UINT4 Dot1qPortRestrictedVlanRegistration [ ] ={1,3,6,1,2,1,17,7,1,4,5,1,7};
UINT4 Dot1qTpVlanPortInFrames [ ] ={1,3,6,1,2,1,17,7,1,4,6,1,1};
UINT4 Dot1qTpVlanPortOutFrames [ ] ={1,3,6,1,2,1,17,7,1,4,6,1,2};
UINT4 Dot1qTpVlanPortInDiscards [ ] ={1,3,6,1,2,1,17,7,1,4,6,1,3};
UINT4 Dot1qTpVlanPortInOverflowFrames [ ] ={1,3,6,1,2,1,17,7,1,4,6,1,4};
UINT4 Dot1qTpVlanPortOutOverflowFrames [ ] ={1,3,6,1,2,1,17,7,1,4,6,1,5};
UINT4 Dot1qTpVlanPortInOverflowDiscards [ ] ={1,3,6,1,2,1,17,7,1,4,6,1,6};
UINT4 Dot1qTpVlanPortHCInFrames [ ] ={1,3,6,1,2,1,17,7,1,4,7,1,1};
UINT4 Dot1qTpVlanPortHCOutFrames [ ] ={1,3,6,1,2,1,17,7,1,4,7,1,2};
UINT4 Dot1qTpVlanPortHCInDiscards [ ] ={1,3,6,1,2,1,17,7,1,4,7,1,3};
UINT4 Dot1qConstraintVlan [ ] ={1,3,6,1,2,1,17,7,1,4,8,1,1};
UINT4 Dot1qConstraintSet [ ] ={1,3,6,1,2,1,17,7,1,4,8,1,2};
UINT4 Dot1qConstraintType [ ] ={1,3,6,1,2,1,17,7,1,4,8,1,3};
UINT4 Dot1qConstraintStatus [ ] ={1,3,6,1,2,1,17,7,1,4,8,1,4};
UINT4 Dot1qConstraintSetDefault [ ] ={1,3,6,1,2,1,17,7,1,4,9};
UINT4 Dot1qConstraintTypeDefault [ ] ={1,3,6,1,2,1,17,7,1,4,10};
UINT4 Dot1vProtocolTemplateFrameType [ ] ={1,3,6,1,2,1,17,7,1,5,1,1,1};
UINT4 Dot1vProtocolTemplateProtocolValue [ ] ={1,3,6,1,2,1,17,7,1,5,1,1,2};
UINT4 Dot1vProtocolGroupId [ ] ={1,3,6,1,2,1,17,7,1,5,1,1,3};
UINT4 Dot1vProtocolGroupRowStatus [ ] ={1,3,6,1,2,1,17,7,1,5,1,1,4};
UINT4 Dot1vProtocolPortGroupId [ ] ={1,3,6,1,2,1,17,7,1,5,2,1,1};
UINT4 Dot1vProtocolPortGroupVid [ ] ={1,3,6,1,2,1,17,7,1,5,2,1,2};
UINT4 Dot1vProtocolPortRowStatus [ ] ={1,3,6,1,2,1,17,7,1,5,2,1,3};


tMbDbEntry stdvlaMibEntry[]= {

{{11,Dot1qVlanVersionNumber}, NULL, Dot1qVlanVersionNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Dot1qMaxVlanId}, NULL, Dot1qMaxVlanIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Dot1qMaxSupportedVlans}, NULL, Dot1qMaxSupportedVlansGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Dot1qNumVlans}, NULL, Dot1qNumVlansGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Dot1qGvrpStatus}, NULL, Dot1qGvrpStatusGet, Dot1qGvrpStatusSet, Dot1qGvrpStatusTest, Dot1qGvrpStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,Dot1qFdbId}, GetNextIndexDot1qFdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1qFdbTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qFdbDynamicCount}, GetNextIndexDot1qFdbTable, Dot1qFdbDynamicCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qFdbTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qTpFdbAddress}, GetNextIndexDot1qTpFdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot1qTpFdbTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpFdbPort}, GetNextIndexDot1qTpFdbTable, Dot1qTpFdbPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1qTpFdbTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpFdbStatus}, GetNextIndexDot1qTpFdbTable, Dot1qTpFdbStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1qTpFdbTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpGroupAddress}, GetNextIndexDot1qTpGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot1qTpGroupTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpGroupEgressPorts}, GetNextIndexDot1qTpGroupTable, Dot1qTpGroupEgressPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1qTpGroupTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpGroupLearnt}, GetNextIndexDot1qTpGroupTable, Dot1qTpGroupLearntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1qTpGroupTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qForwardAllPorts}, GetNextIndexDot1qForwardAllTable, Dot1qForwardAllPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1qForwardAllTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qForwardAllStaticPorts}, GetNextIndexDot1qForwardAllTable, Dot1qForwardAllStaticPortsGet, Dot1qForwardAllStaticPortsSet, Dot1qForwardAllStaticPortsTest, Dot1qForwardAllTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qForwardAllTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qForwardAllForbiddenPorts}, GetNextIndexDot1qForwardAllTable, Dot1qForwardAllForbiddenPortsGet, Dot1qForwardAllForbiddenPortsSet, Dot1qForwardAllForbiddenPortsTest, Dot1qForwardAllTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qForwardAllTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qForwardUnregisteredPorts}, GetNextIndexDot1qForwardUnregisteredTable, Dot1qForwardUnregisteredPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1qForwardUnregisteredTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qForwardUnregisteredStaticPorts}, GetNextIndexDot1qForwardUnregisteredTable, Dot1qForwardUnregisteredStaticPortsGet, Dot1qForwardUnregisteredStaticPortsSet, Dot1qForwardUnregisteredStaticPortsTest, Dot1qForwardUnregisteredTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qForwardUnregisteredTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qForwardUnregisteredForbiddenPorts}, GetNextIndexDot1qForwardUnregisteredTable, Dot1qForwardUnregisteredForbiddenPortsGet, Dot1qForwardUnregisteredForbiddenPortsSet, Dot1qForwardUnregisteredForbiddenPortsTest, Dot1qForwardUnregisteredTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qForwardUnregisteredTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qStaticUnicastAddress}, GetNextIndexDot1qStaticUnicastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot1qStaticUnicastTableINDEX, 3, 0, 0, NULL},

{{13,Dot1qStaticUnicastReceivePort}, GetNextIndexDot1qStaticUnicastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1qStaticUnicastTableINDEX, 3, 0, 0, NULL},

{{13,Dot1qStaticUnicastAllowedToGoTo}, GetNextIndexDot1qStaticUnicastTable, Dot1qStaticUnicastAllowedToGoToGet, Dot1qStaticUnicastAllowedToGoToSet, Dot1qStaticUnicastAllowedToGoToTest, Dot1qStaticUnicastTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qStaticUnicastTableINDEX, 3, 0, 0, NULL},

{{13,Dot1qStaticUnicastStatus}, GetNextIndexDot1qStaticUnicastTable, Dot1qStaticUnicastStatusGet, Dot1qStaticUnicastStatusSet, Dot1qStaticUnicastStatusTest, Dot1qStaticUnicastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qStaticUnicastTableINDEX, 3, 0, 0, "3"},

{{13,Dot1qStaticMulticastAddress}, GetNextIndexDot1qStaticMulticastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot1qStaticMulticastTableINDEX, 3, 0, 0, NULL},

{{13,Dot1qStaticMulticastReceivePort}, GetNextIndexDot1qStaticMulticastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1qStaticMulticastTableINDEX, 3, 0, 0, NULL},

{{13,Dot1qStaticMulticastStaticEgressPorts}, GetNextIndexDot1qStaticMulticastTable, Dot1qStaticMulticastStaticEgressPortsGet, Dot1qStaticMulticastStaticEgressPortsSet, Dot1qStaticMulticastStaticEgressPortsTest, Dot1qStaticMulticastTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qStaticMulticastTableINDEX, 3, 0, 0, NULL},

{{13,Dot1qStaticMulticastForbiddenEgressPorts}, GetNextIndexDot1qStaticMulticastTable, Dot1qStaticMulticastForbiddenEgressPortsGet, Dot1qStaticMulticastForbiddenEgressPortsSet, Dot1qStaticMulticastForbiddenEgressPortsTest, Dot1qStaticMulticastTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qStaticMulticastTableINDEX, 3, 0, 0, NULL},

{{13,Dot1qStaticMulticastStatus}, GetNextIndexDot1qStaticMulticastTable, Dot1qStaticMulticastStatusGet, Dot1qStaticMulticastStatusSet, Dot1qStaticMulticastStatusTest, Dot1qStaticMulticastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qStaticMulticastTableINDEX, 3, 0, 0, "3"},

{{11,Dot1qVlanNumDeletes}, NULL, Dot1qVlanNumDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,Dot1qVlanTimeMark}, GetNextIndexDot1qVlanCurrentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1qVlanCurrentTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qVlanIndex}, GetNextIndexDot1qVlanCurrentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1qVlanCurrentTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qVlanFdbId}, GetNextIndexDot1qVlanCurrentTable, Dot1qVlanFdbIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot1qVlanCurrentTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qVlanCurrentEgressPorts}, GetNextIndexDot1qVlanCurrentTable, Dot1qVlanCurrentEgressPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1qVlanCurrentTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qVlanCurrentUntaggedPorts}, GetNextIndexDot1qVlanCurrentTable, Dot1qVlanCurrentUntaggedPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1qVlanCurrentTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qVlanStatus}, GetNextIndexDot1qVlanCurrentTable, Dot1qVlanStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1qVlanCurrentTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qVlanCreationTime}, GetNextIndexDot1qVlanCurrentTable, Dot1qVlanCreationTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Dot1qVlanCurrentTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qVlanStaticName}, GetNextIndexDot1qVlanStaticTable, Dot1qVlanStaticNameGet, Dot1qVlanStaticNameSet, Dot1qVlanStaticNameTest, Dot1qVlanStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qVlanStaticTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qVlanStaticEgressPorts}, GetNextIndexDot1qVlanStaticTable, Dot1qVlanStaticEgressPortsGet, Dot1qVlanStaticEgressPortsSet, Dot1qVlanStaticEgressPortsTest, Dot1qVlanStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qVlanStaticTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qVlanForbiddenEgressPorts}, GetNextIndexDot1qVlanStaticTable, Dot1qVlanForbiddenEgressPortsGet, Dot1qVlanForbiddenEgressPortsSet, Dot1qVlanForbiddenEgressPortsTest, Dot1qVlanStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qVlanStaticTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qVlanStaticUntaggedPorts}, GetNextIndexDot1qVlanStaticTable, Dot1qVlanStaticUntaggedPortsGet, Dot1qVlanStaticUntaggedPortsSet, Dot1qVlanStaticUntaggedPortsTest, Dot1qVlanStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1qVlanStaticTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qVlanStaticRowStatus}, GetNextIndexDot1qVlanStaticTable, Dot1qVlanStaticRowStatusGet, Dot1qVlanStaticRowStatusSet, Dot1qVlanStaticRowStatusTest, Dot1qVlanStaticTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qVlanStaticTableINDEX, 1, 0, 1, NULL},

{{11,Dot1qNextFreeLocalVlanIndex}, NULL, Dot1qNextFreeLocalVlanIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

	 /* Manually modified to store the default value of the Port Pvid  for
	  ** maintaing the same value after restoration  */
	
{{13,Dot1qPvid}, GetNextIndexDot1qPortVlanTable, Dot1qPvidGet, Dot1qPvidSet, Dot1qPvidTest, Dot1qPortVlanTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot1qPortVlanTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qPortAcceptableFrameTypes}, GetNextIndexDot1qPortVlanTable, Dot1qPortAcceptableFrameTypesGet, Dot1qPortAcceptableFrameTypesSet, Dot1qPortAcceptableFrameTypesTest, Dot1qPortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qPortVlanTableINDEX, 1, 0, 0, "1"},

{{13,Dot1qPortIngressFiltering}, GetNextIndexDot1qPortVlanTable, Dot1qPortIngressFilteringGet, Dot1qPortIngressFilteringSet, Dot1qPortIngressFilteringTest, Dot1qPortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qPortVlanTableINDEX, 1, 0, 0, "2"},

{{13,Dot1qPortGvrpStatus}, GetNextIndexDot1qPortVlanTable, Dot1qPortGvrpStatusGet, Dot1qPortGvrpStatusSet, Dot1qPortGvrpStatusTest, Dot1qPortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qPortVlanTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qPortGvrpFailedRegistrations}, GetNextIndexDot1qPortVlanTable, Dot1qPortGvrpFailedRegistrationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qPortVlanTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qPortGvrpLastPduOrigin}, GetNextIndexDot1qPortVlanTable, Dot1qPortGvrpLastPduOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1qPortVlanTableINDEX, 1, 0, 0, NULL},

{{13,Dot1qPortRestrictedVlanRegistration}, GetNextIndexDot1qPortVlanTable, Dot1qPortRestrictedVlanRegistrationGet, Dot1qPortRestrictedVlanRegistrationSet, Dot1qPortRestrictedVlanRegistrationTest, Dot1qPortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qPortVlanTableINDEX, 1, 0, 0, "2"},

{{13,Dot1qTpVlanPortInFrames}, GetNextIndexDot1qPortVlanStatisticsTable, Dot1qTpVlanPortInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpVlanPortOutFrames}, GetNextIndexDot1qPortVlanStatisticsTable, Dot1qTpVlanPortOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpVlanPortInDiscards}, GetNextIndexDot1qPortVlanStatisticsTable, Dot1qTpVlanPortInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpVlanPortInOverflowFrames}, GetNextIndexDot1qPortVlanStatisticsTable, Dot1qTpVlanPortInOverflowFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpVlanPortOutOverflowFrames}, GetNextIndexDot1qPortVlanStatisticsTable, Dot1qTpVlanPortOutOverflowFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpVlanPortInOverflowDiscards}, GetNextIndexDot1qPortVlanStatisticsTable, Dot1qTpVlanPortInOverflowDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1qPortVlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpVlanPortHCInFrames}, GetNextIndexDot1qPortVlanHCStatisticsTable, Dot1qTpVlanPortHCInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot1qPortVlanHCStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpVlanPortHCOutFrames}, GetNextIndexDot1qPortVlanHCStatisticsTable, Dot1qTpVlanPortHCOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot1qPortVlanHCStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qTpVlanPortHCInDiscards}, GetNextIndexDot1qPortVlanHCStatisticsTable, Dot1qTpVlanPortHCInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot1qPortVlanHCStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qConstraintVlan}, GetNextIndexDot1qLearningConstraintsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot1qLearningConstraintsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qConstraintSet}, GetNextIndexDot1qLearningConstraintsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1qLearningConstraintsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qConstraintType}, GetNextIndexDot1qLearningConstraintsTable, Dot1qConstraintTypeGet, Dot1qConstraintTypeSet, Dot1qConstraintTypeTest, Dot1qLearningConstraintsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qLearningConstraintsTableINDEX, 2, 0, 0, NULL},

{{13,Dot1qConstraintStatus}, GetNextIndexDot1qLearningConstraintsTable, Dot1qConstraintStatusGet, Dot1qConstraintStatusSet, Dot1qConstraintStatusTest, Dot1qLearningConstraintsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1qLearningConstraintsTableINDEX, 2, 0, 1, NULL},

{{11,Dot1qConstraintSetDefault}, NULL, Dot1qConstraintSetDefaultGet, Dot1qConstraintSetDefaultSet, Dot1qConstraintSetDefaultTest, Dot1qConstraintSetDefaultDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Dot1qConstraintTypeDefault}, NULL, Dot1qConstraintTypeDefaultGet, Dot1qConstraintTypeDefaultSet, Dot1qConstraintTypeDefaultTest, Dot1qConstraintTypeDefaultDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,Dot1vProtocolTemplateFrameType}, GetNextIndexDot1vProtocolGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1vProtocolGroupTableINDEX, 2, 0, 0, NULL},

{{13,Dot1vProtocolTemplateProtocolValue}, GetNextIndexDot1vProtocolGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Dot1vProtocolGroupTableINDEX, 2, 0, 0, NULL},

{{13,Dot1vProtocolGroupId}, GetNextIndexDot1vProtocolGroupTable, Dot1vProtocolGroupIdGet, Dot1vProtocolGroupIdSet, Dot1vProtocolGroupIdTest, Dot1vProtocolGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1vProtocolGroupTableINDEX, 2, 0, 0, NULL},

{{13,Dot1vProtocolGroupRowStatus}, GetNextIndexDot1vProtocolGroupTable, Dot1vProtocolGroupRowStatusGet, Dot1vProtocolGroupRowStatusSet, Dot1vProtocolGroupRowStatusTest, Dot1vProtocolGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1vProtocolGroupTableINDEX, 2, 0, 1, NULL},

{{13,Dot1vProtocolPortGroupId}, GetNextIndexDot1vProtocolPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1vProtocolPortTableINDEX, 2, 0, 0, NULL},

{{13,Dot1vProtocolPortGroupVid}, GetNextIndexDot1vProtocolPortTable, Dot1vProtocolPortGroupVidGet, Dot1vProtocolPortGroupVidSet, Dot1vProtocolPortGroupVidTest, Dot1vProtocolPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1vProtocolPortTableINDEX, 2, 0, 0, NULL},

{{13,Dot1vProtocolPortRowStatus}, GetNextIndexDot1vProtocolPortTable, Dot1vProtocolPortRowStatusGet, Dot1vProtocolPortRowStatusSet, Dot1vProtocolPortRowStatusTest, Dot1vProtocolPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1vProtocolPortTableINDEX, 2, 0, 1, NULL},
};
tMibData stdvlaEntry = { 71, stdvlaMibEntry };
#endif /* _STDVLADB_H */

