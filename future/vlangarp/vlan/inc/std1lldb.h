/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1lldb.h,v 1.2 2016/07/16 11:15:03 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STD1LLDB_H
#define _STD1LLDB_H

UINT1 LldpXdot1EvbConfigEvbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1EvbConfigCdcpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpV2Xdot1LocEvbTlvTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot1LocCdcpTlvTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot1RemEvbTlvTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpV2Xdot1RemCdcpTlvTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 std1ll [] ={1,3,111,2,802,1,1,13,1,5,32962,7,1};
tSNMP_OID_TYPE std1llOID = {13, std1ll};


UINT4 LldpXdot1EvbConfigEvbTxEnable [ ] ={1,3,111,2,802,1,1,13,1,5,32962,7,1,1,1,1,1,1,1};
UINT4 LldpXdot1EvbConfigCdcpTxEnable [ ] ={1,3,111,2,802,1,1,13,1,5,32962,7,1,1,1,1,2,1,1};
UINT4 LldpV2Xdot1LocEvbTlvString [ ] ={1,3,111,2,802,1,1,13,1,5,32962,7,1,1,1,2,1,1,1};
UINT4 LldpV2Xdot1LocCdcpTlvString [ ] ={1,3,111,2,802,1,1,13,1,5,32962,7,1,1,1,2,2,1,1};
UINT4 LldpV2Xdot1RemEvbTlvString [ ] ={1,3,111,2,802,1,1,13,1,5,32962,7,1,1,1,3,1,1,1};
UINT4 LldpV2Xdot1RemCdcpTlvString [ ] ={1,3,111,2,802,1,1,13,1,5,32962,7,1,1,1,3,2,1,1};




tMbDbEntry std1llMibEntry[]= {

{{19,LldpXdot1EvbConfigEvbTxEnable}, GetNextIndexLldpXdot1EvbConfigEvbTable, LldpXdot1EvbConfigEvbTxEnableGet, LldpXdot1EvbConfigEvbTxEnableSet, LldpXdot1EvbConfigEvbTxEnableTest, LldpXdot1EvbConfigEvbTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1EvbConfigEvbTableINDEX, 2, 0, 0, "2"},

{{19,LldpXdot1EvbConfigCdcpTxEnable}, GetNextIndexLldpXdot1EvbConfigCdcpTable, LldpXdot1EvbConfigCdcpTxEnableGet, LldpXdot1EvbConfigCdcpTxEnableSet, LldpXdot1EvbConfigCdcpTxEnableTest, LldpXdot1EvbConfigCdcpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1EvbConfigCdcpTableINDEX, 2, 0, 0, "2"},

{{19,LldpV2Xdot1LocEvbTlvString}, GetNextIndexLldpV2Xdot1LocEvbTlvTable, LldpV2Xdot1LocEvbTlvStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot1LocEvbTlvTableINDEX, 1, 0, 0, NULL},

{{19,LldpV2Xdot1LocCdcpTlvString}, GetNextIndexLldpV2Xdot1LocCdcpTlvTable, LldpV2Xdot1LocCdcpTlvStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot1LocCdcpTlvTableINDEX, 1, 0, 0, NULL},

{{19,LldpV2Xdot1RemEvbTlvString}, GetNextIndexLldpV2Xdot1RemEvbTlvTable, LldpV2Xdot1RemEvbTlvStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot1RemEvbTlvTableINDEX, 4, 0, 0, NULL},

{{19,LldpV2Xdot1RemCdcpTlvString}, GetNextIndexLldpV2Xdot1RemCdcpTlvTable, LldpV2Xdot1RemCdcpTlvStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, LldpV2Xdot1RemCdcpTlvTableINDEX, 4, 0, 0, NULL},
};
tMibData std1llEntry = { 6, std1llMibEntry };

#endif /* _STD1LLDB_H */



