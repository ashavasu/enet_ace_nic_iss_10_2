/* $Id: fsvlannc.h,v 1.1 2016/06/21 09:54:51 siva Exp $
    ISS Wrapper header
    module ARICENT-VLAN-MIB

 */

#ifndef _H_i_ARICENT_VLAN_MIB
#define _H_i_ARICENT_VLAN_MIB

/********************************************************************
* FUNCTION NcDot1qFutureVlanStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanStatusSet (
                INT4 i4Dot1qFutureVlanStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanMacBasedOnAllPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanMacBasedOnAllPortsSet (
                INT4 i4Dot1qFutureVlanMacBasedOnAllPorts );

/********************************************************************
* FUNCTION NcDot1qFutureVlanMacBasedOnAllPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanMacBasedOnAllPortsTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanMacBasedOnAllPorts );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortProtoBasedOnAllPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortProtoBasedOnAllPortsSet (
                INT4 i4Dot1qFutureVlanPortProtoBasedOnAllPorts );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortProtoBasedOnAllPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortProtoBasedOnAllPortsTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPortProtoBasedOnAllPorts );

/********************************************************************
* FUNCTION NcDot1qFutureVlanShutdownStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanShutdownStatusSet (
                INT4 i4Dot1qFutureVlanShutdownStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanShutdownStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanShutdownStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanShutdownStatus );

/********************************************************************
* FUNCTION NcDot1qFutureGarpShutdownStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureGarpShutdownStatusSet (
                INT4 i4Dot1qFutureGarpShutdownStatus );

/********************************************************************
* FUNCTION NcDot1qFutureGarpShutdownStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureGarpShutdownStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureGarpShutdownStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanDebugSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanDebugSet (
                INT4 i4Dot1qFutureVlanDebug );

/********************************************************************
* FUNCTION NcDot1qFutureVlanDebugTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanDebugTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanDebug );

/********************************************************************
* FUNCTION NcDot1qFutureVlanLearningModeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanLearningModeSet (
                INT4 i4Dot1qFutureVlanLearningMode );

/********************************************************************
* FUNCTION NcDot1qFutureVlanLearningModeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanLearningModeTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanLearningMode );

/********************************************************************
* FUNCTION NcDot1qFutureVlanHybridTypeDefaultSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanHybridTypeDefaultSet (
                INT4 i4Dot1qFutureVlanHybridTypeDefault );

/********************************************************************
* FUNCTION NcDot1qFutureVlanHybridTypeDefaultTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanHybridTypeDefaultTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanHybridTypeDefault );

/********************************************************************
* FUNCTION NcDot1qFutureVlanOperStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanOperStatusGet (
                INT4 *pi4Dot1qFutureVlanOperStatus );

/********************************************************************
* FUNCTION NcDot1qFutureGvrpOperStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureGvrpOperStatusGet (
                INT4 *pi4Dot1qFutureGvrpOperStatus );

/********************************************************************
* FUNCTION NcDot1qFutureGmrpOperStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureGmrpOperStatusGet (
                INT4 *pi4Dot1qFutureGmrpOperStatus );

/********************************************************************
* FUNCTION NcDot1qFutureGarpDebugSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureGarpDebugSet (
                INT4 i4Dot1qFutureGarpDebug );

/********************************************************************
* FUNCTION NcDot1qFutureGarpDebugTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureGarpDebugTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureGarpDebug );

/********************************************************************
* FUNCTION NcDot1qFutureUnicastMacLearningLimitSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureUnicastMacLearningLimitSet (
                UINT4 u4Dot1qFutureUnicastMacLearningLimit );

/********************************************************************
* FUNCTION NcDot1qFutureUnicastMacLearningLimitTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureUnicastMacLearningLimitTest (UINT4 *pu4Error,
                UINT4 u4Dot1qFutureUnicastMacLearningLimit );

/********************************************************************
* FUNCTION NcDot1qFutureVlanBaseBridgeModeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanBaseBridgeModeSet (
                INT4 i4Dot1qFutureVlanBaseBridgeMode );

/********************************************************************
* FUNCTION NcDot1qFutureVlanBaseBridgeModeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanBaseBridgeModeTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanBaseBridgeMode );

/********************************************************************
* FUNCTION NcDot1qFutureVlanSubnetBasedOnAllPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanSubnetBasedOnAllPortsSet (
                INT4 i4Dot1qFutureVlanSubnetBasedOnAllPorts );

/********************************************************************
* FUNCTION NcDot1qFutureVlanSubnetBasedOnAllPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanSubnetBasedOnAllPortsTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanSubnetBasedOnAllPorts );

/********************************************************************
* FUNCTION NcDot1qFutureVlanGlobalMacLearningStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanGlobalMacLearningStatusSet (
                INT4 i4Dot1qFutureVlanGlobalMacLearningStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanGlobalMacLearningStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanGlobalMacLearningStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanGlobalMacLearningStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanApplyEnhancedFilteringCriteriaSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanApplyEnhancedFilteringCriteriaSet (
                INT4 i4Dot1qFutureVlanApplyEnhancedFilteringCriteria );

/********************************************************************
* FUNCTION NcDot1qFutureVlanApplyEnhancedFilteringCriteriaTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanApplyEnhancedFilteringCriteriaTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanApplyEnhancedFilteringCriteria );

/********************************************************************
* FUNCTION NcDot1qFutureVlanSwStatsEnabledSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanSwStatsEnabledSet (
                INT4 i4Dot1qFutureVlanSwStatsEnabled );

/********************************************************************
* FUNCTION NcDot1qFutureVlanSwStatsEnabledTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanSwStatsEnabledTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanSwStatsEnabled );

/********************************************************************
* FUNCTION NcDot1qFutureVlanGlobalsFdbFlushSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanGlobalsFdbFlushSet (
                INT4 i4Dot1qFutureVlanGlobalsFdbFlush );

/********************************************************************
* FUNCTION NcDot1qFutureVlanGlobalsFdbFlushTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanGlobalsFdbFlushTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanGlobalsFdbFlush );

/********************************************************************
* FUNCTION NcDot1qFutureVlanUserDefinedTPIDSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanUserDefinedTPIDSet (
                INT4 i4Dot1qFutureVlanUserDefinedTPID );

/********************************************************************
* FUNCTION NcDot1qFutureVlanUserDefinedTPIDTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanUserDefinedTPIDTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanUserDefinedTPID );

/********************************************************************
* FUNCTION NcDot1qFutureVlanBridgeModeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanBridgeModeSet (
                INT4 i4Dot1qFutureVlanBridgeMode );

/********************************************************************
* FUNCTION NcDot1qFutureVlanBridgeModeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanBridgeModeTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanBridgeMode );

/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelBpduPriSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelBpduPriSet (
                INT4 i4Dot1qFutureVlanTunnelBpduPri );

/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelBpduPriTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelBpduPriTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanTunnelBpduPri );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortTypeSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortType );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortTypeTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortType );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortMacBasedClassificationSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortMacBasedClassificationSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortMacBasedClassification );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortMacBasedClassificationTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortMacBasedClassificationTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortMacBasedClassification );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortPortProtoBasedClassificationSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortPortProtoBasedClassificationSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortPortProtoBasedClassification );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortPortProtoBasedClassificationTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortPortProtoBasedClassificationTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortPortProtoBasedClassification );

/********************************************************************
* FUNCTION NcDot1qFutureVlanFilteringUtilityCriteriaSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanFilteringUtilityCriteriaSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanFilteringUtilityCriteria );

/********************************************************************
* FUNCTION NcDot1qFutureVlanFilteringUtilityCriteriaTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanFilteringUtilityCriteriaTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanFilteringUtilityCriteria );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortProtectedSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortProtectedSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortProtected );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortProtectedTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortProtectedTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortProtected );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetBasedClassificationSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetBasedClassificationSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortSubnetBasedClassification );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetBasedClassificationTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetBasedClassificationTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortSubnetBasedClassification );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortUnicastMacLearningSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortUnicastMacLearningSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortUnicastMacLearning );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortUnicastMacLearningTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortUnicastMacLearningTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortUnicastMacLearning );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortIngressEtherTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortIngressEtherTypeSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortIngressEtherType );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortIngressEtherTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortIngressEtherTypeTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortIngressEtherType );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortEgressEtherTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortEgressEtherTypeSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortEgressEtherType );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortEgressEtherTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortEgressEtherTypeTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortEgressEtherType );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortEgressTPIDTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortEgressTPIDTypeSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortEgressTPIDType );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortEgressTPIDTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortEgressTPIDTypeTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortEgressTPIDType );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortAllowableTPID1Set
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortAllowableTPID1Set (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortAllowableTPID1 );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortAllowableTPID1Test
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortAllowableTPID1Test (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortAllowableTPID1 );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortAllowableTPID2Set
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortAllowableTPID2Set (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortAllowableTPID2 );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortAllowableTPID2Test
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortAllowableTPID2Test (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortAllowableTPID2 );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortAllowableTPID3Set
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortAllowableTPID3Set (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortAllowableTPID3 );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortAllowableTPID3Test
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortAllowableTPID3Test (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortAllowableTPID3 );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortUnicastMacSecTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortUnicastMacSecTypeSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortUnicastMacSecType );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortUnicastMacSecTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortUnicastMacSecTypeTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanPortUnicastMacSecType );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortMacMapVidSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortMacMapVidSet (
                INT4 i4Dot1qFutureVlanPort,
                UINT1 *pDot1qFutureVlanPortMacMapAddr,
                INT4 i4Dot1qFutureVlanPortMacMapVid );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortMacMapVidTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortMacMapVidTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                UINT1 *pDot1qFutureVlanPortMacMapAddr,
                INT4 i4Dot1qFutureVlanPortMacMapVid );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortMacMapNameSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortMacMapNameSet (
                INT4 i4Dot1qFutureVlanPort,
                UINT1 *pDot1qFutureVlanPortMacMapAddr,
                UINT1 *pDot1qFutureVlanPortMacMapName );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortMacMapNameTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortMacMapNameTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                UINT1 *pDot1qFutureVlanPortMacMapAddr,
                UINT1 *pDot1qFutureVlanPortMacMapName );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortMacMapMcastBcastOptionSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortMacMapMcastBcastOptionSet (
                INT4 i4Dot1qFutureVlanPort,
                UINT1 *pDot1qFutureVlanPortMacMapAddr,
                INT4 i4Dot1qFutureVlanPortMacMapMcastBcastOption );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortMacMapMcastBcastOptionTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortMacMapMcastBcastOptionTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                UINT1 *pDot1qFutureVlanPortMacMapAddr,
                INT4 i4Dot1qFutureVlanPortMacMapMcastBcastOption );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortMacMapRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortMacMapRowStatusSet (
                INT4 i4Dot1qFutureVlanPort,
                UINT1 *pDot1qFutureVlanPortMacMapAddr,
                INT4 i4Dot1qFutureVlanPortMacMapRowStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortMacMapRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortMacMapRowStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                UINT1 *pDot1qFutureVlanPortMacMapAddr,
                INT4 i4Dot1qFutureVlanPortMacMapRowStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanFidSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanFidSet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 u4Dot1qFutureVlanFid );

/********************************************************************
* FUNCTION NcDot1qFutureVlanFidTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanFidTest (UINT4 *pu4Error,
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 u4Dot1qFutureVlanFid );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterRxUcastGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterRxUcastGet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 *pu4Dot1qFutureVlanCounterRxUcast );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterRxMcastBcastGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterRxMcastBcastGet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 *pu4Dot1qFutureVlanCounterRxMcastBcast );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterTxUnknUcastGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterTxUnknUcastGet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 *pu4Dot1qFutureVlanCounterTxUnknUcast );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterTxUcastGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterTxUcastGet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 *pu4Dot1qFutureVlanCounterTxUcast );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterTxBcastGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterTxBcastGet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 *pu4Dot1qFutureVlanCounterTxBcast );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterRxFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterRxFramesGet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 *pu4Dot1qFutureVlanCounterRxFrames );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterRxBytesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterRxBytesGet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 *pu4Dot1qFutureVlanCounterRxBytes );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterTxFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterTxFramesGet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 *pu4Dot1qFutureVlanCounterTxFrames );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterTxBytesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterTxBytesGet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 *pu4Dot1qFutureVlanCounterTxBytes );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterDiscardFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterDiscardFramesGet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 *pu4Dot1qFutureVlanCounterDiscardFrames );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterDiscardBytesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterDiscardBytesGet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 *pu4Dot1qFutureVlanCounterDiscardBytes );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterStatusSet (
                UINT4 u4Dot1qFutureVlanIndex,
                INT4 i4Dot1qFutureVlanCounterStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanCounterStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanCounterStatusTest (UINT4 *pu4Error,
                UINT4 u4Dot1qFutureVlanIndex,
                INT4 i4Dot1qFutureVlanCounterStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanUnicastMacLimitSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanUnicastMacLimitSet (
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 u4Dot1qFutureVlanUnicastMacLimit );

/********************************************************************
* FUNCTION NcDot1qFutureVlanUnicastMacLimitTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanUnicastMacLimitTest (UINT4 *pu4Error,
                UINT4 u4Dot1qFutureVlanIndex,
                UINT4 u4Dot1qFutureVlanUnicastMacLimit );

/********************************************************************
* FUNCTION NcDot1qFutureVlanAdminMacLearningStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanAdminMacLearningStatusSet (
                UINT4 u4Dot1qFutureVlanIndex,
                INT4 i4Dot1qFutureVlanAdminMacLearningStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanAdminMacLearningStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanAdminMacLearningStatusTest (UINT4 *pu4Error,
                UINT4 u4Dot1qFutureVlanIndex,
                INT4 i4Dot1qFutureVlanAdminMacLearningStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanOperMacLearningStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanOperMacLearningStatusGet (
                UINT4 u4Dot1qFutureVlanIndex,
                INT4 *pi4Dot1qFutureVlanOperMacLearningStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortFdbFlushSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortFdbFlushSet (
                UINT4 u4Dot1qFutureVlanIndex,
                INT4 i4Dot1qFutureVlanPortFdbFlush );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortFdbFlushTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortFdbFlushTest (UINT4 *pu4Error,
                UINT4 u4Dot1qFutureVlanIndex,
                INT4 i4Dot1qFutureVlanPortFdbFlush );

/********************************************************************
* FUNCTION NcDot1qFutureVlanWildCardEgressPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanWildCardEgressPortsSet (
                UINT1 *pDot1qFutureVlanWildCardMacAddress,
                UINT1 *pau1Dot1qFutureVlanWildCardEgressPorts );

/********************************************************************
* FUNCTION NcDot1qFutureVlanWildCardEgressPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanWildCardEgressPortsTest (UINT4 *pu4Error,
                UINT1 *pDot1qFutureVlanWildCardMacAddress,
                UINT1 *pau1Dot1qFutureVlanWildCardEgressPorts );

/********************************************************************
* FUNCTION NcDot1qFutureVlanWildCardRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanWildCardRowStatusSet (
                UINT1 *pDot1qFutureVlanWildCardMacAddress,
                INT4 i4Dot1qFutureVlanWildCardRowStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanWildCardRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanWildCardRowStatusTest (UINT4 *pu4Error,
                UINT1 *pDot1qFutureVlanWildCardMacAddress,
                INT4 i4Dot1qFutureVlanWildCardRowStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapVidSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapVidSet (
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapAddr,
                INT4 i4Dot1qFutureVlanPortSubnetMapVid );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapVidTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapVidTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapAddr,
                INT4 i4Dot1qFutureVlanPortSubnetMapVid );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapARPOptionSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapARPOptionSet (
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapAddr,
                INT4 i4Dot1qFutureVlanPortSubnetMapARPOption );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapARPOptionTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapARPOptionTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapAddr,
                INT4 i4Dot1qFutureVlanPortSubnetMapARPOption );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapRowStatusSet (
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapAddr,
                INT4 i4Dot1qFutureVlanPortSubnetMapRowStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapRowStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapAddr,
                INT4 i4Dot1qFutureVlanPortSubnetMapRowStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapExtVidSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapExtVidSet (
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapExtAddr,
                UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
                INT4 i4Dot1qFutureVlanPortSubnetMapExtVid );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapExtVidTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapExtVidTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapExtAddr,
                UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
                INT4 i4Dot1qFutureVlanPortSubnetMapExtVid );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapExtARPOptionSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapExtARPOptionSet (
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapExtAddr,
                UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
                INT4 i4Dot1qFutureVlanPortSubnetMapExtARPOption );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapExtARPOptionTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapExtARPOptionTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapExtAddr,
                UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
                INT4 i4Dot1qFutureVlanPortSubnetMapExtARPOption );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapExtRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapExtRowStatusSet (
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapExtAddr,
                UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
                INT4 i4Dot1qFutureVlanPortSubnetMapExtRowStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanPortSubnetMapExtRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanPortSubnetMapExtRowStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                UINT4 pDot1qFutureVlanPortSubnetMapExtAddr,
                UINT4 pDot1qFutureVlanPortSubnetMapExtMask,
                INT4 i4Dot1qFutureVlanPortSubnetMapExtRowStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanLoopbackStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanLoopbackStatusSet (
                UINT4 u4Dot1qFutureVlanIndex,
                INT4 i4Dot1qFutureVlanLoopbackStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanLoopbackStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanLoopbackStatusTest (UINT4 *pu4Error,
                UINT4 u4Dot1qFutureVlanIndex,
                INT4 i4Dot1qFutureVlanLoopbackStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelStatusSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanTunnelStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanTunnelStatus );

/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelStpPDUsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelStpPDUsSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanTunnelStpPDUs );

/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelStpPDUsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelStpPDUsTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanTunnelStpPDUs );


/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelGvrpPDUsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelGvrpPDUsSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanTunnelGvrpPDUs );

/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelGvrpPDUsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelGvrpPDUsTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanTunnelGvrpPDUs );

/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelGvrpPDUsRecvdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelGvrpPDUsRecvdSet (
                INT4 i4Dot1qFutureVlanPort,
                UINT4 u4Dot1qFutureVlanTunnelGvrpPDUsRecvd );


/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelGvrpPDUsSentTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelGvrpPDUsSentTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                UINT4 u4Dot1qFutureVlanTunnelGvrpPDUsSent );

/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelIgmpPktsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelIgmpPktsSet (
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanTunnelIgmpPkts );

/********************************************************************
* FUNCTION NcDot1qFutureVlanTunnelIgmpPktsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFutureVlanTunnelIgmpPktsTest (UINT4 *pu4Error,
                INT4 i4Dot1qFutureVlanPort,
                INT4 i4Dot1qFutureVlanTunnelIgmpPkts );



/* END i_ARICENT_VLAN_MIB.c */


#endif
