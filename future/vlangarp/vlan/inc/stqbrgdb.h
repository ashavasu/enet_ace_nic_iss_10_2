/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stqbrgdb.h,v 1.1 2008/11/12 09:22:47 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STQBRGDB_H
#define _STQBRGDB_H

UINT1 Ieee8021QBridgeTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgeCVlanPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgeFdbTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgeTpFdbTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Ieee8021QBridgeVlanCurrentTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgeTpGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Ieee8021QBridgeForwardAllTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgeForwardUnregisteredTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgeStaticUnicastTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgeStaticMulticastTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgeVlanStaticTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgeNextFreeLocalVlanTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgePortVlanTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgePortVlanStatisticsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgeLearningConstraintsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Ieee8021QBridgeLearningConstraintDefaultsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021QBridgeProtocolGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Ieee8021QBridgeProtocolPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stqbrg [] ={1,111,2,802,1,4};
tSNMP_OID_TYPE stqbrgOID = {6, stqbrg};


UINT4 Ieee8021QBridgeComponentId [ ] ={1,111,2,802,1,4,1,1,1,1,1};
UINT4 Ieee8021QBridgeVlanVersionNumber [ ] ={1,111,2,802,1,4,1,1,1,1,2};
UINT4 Ieee8021QBridgeMaxVlanId [ ] ={1,111,2,802,1,4,1,1,1,1,3};
UINT4 Ieee8021QBridgeMaxSupportedVlans [ ] ={1,111,2,802,1,4,1,1,1,1,4};
UINT4 Ieee8021QBridgeNumVlans [ ] ={1,111,2,802,1,4,1,1,1,1,5};
UINT4 Ieee8021QBridgeMvrpEnabledStatus [ ] ={1,111,2,802,1,4,1,1,1,1,6};
UINT4 Ieee8021QBridgeCVlanPortComponentId [ ] ={1,111,2,802,1,4,1,1,2,1,1};
UINT4 Ieee8021QBridgeCVlanPortNumber [ ] ={1,111,2,802,1,4,1,1,2,1,2};
UINT4 Ieee8021QBridgeCVlanPortRowStatus [ ] ={1,111,2,802,1,4,1,1,2,1,3};
UINT4 Ieee8021QBridgeFdbComponentId [ ] ={1,111,2,802,1,4,1,2,1,1,1};
UINT4 Ieee8021QBridgeFdbId [ ] ={1,111,2,802,1,4,1,2,1,1,2};
UINT4 Ieee8021QBridgeFdbDynamicCount [ ] ={1,111,2,802,1,4,1,2,1,1,3};
UINT4 Ieee8021QBridgeFdbLearnedEntryDiscards [ ] ={1,111,2,802,1,4,1,2,1,1,4};
UINT4 Ieee8021QBridgeFdbAgingTime [ ] ={1,111,2,802,1,4,1,2,1,1,5};
UINT4 Ieee8021QBridgeTpFdbAddress [ ] ={1,111,2,802,1,4,1,2,2,1,1};
UINT4 Ieee8021QBridgeTpFdbPort [ ] ={1,111,2,802,1,4,1,2,2,1,2};
UINT4 Ieee8021QBridgeTpFdbStatus [ ] ={1,111,2,802,1,4,1,2,2,1,3};
UINT4 Ieee8021QBridgeVlanNumDeletes [ ] ={1,111,2,802,1,4,1,4,1};
UINT4 Ieee8021QBridgeVlanTimeMark [ ] ={1,111,2,802,1,4,1,4,2,1,1};
UINT4 Ieee8021QBridgeVlanCurrentComponentId [ ] ={1,111,2,802,1,4,1,4,2,1,2};
UINT4 Ieee8021QBridgeVlanIndex [ ] ={1,111,2,802,1,4,1,4,2,1,3};
UINT4 Ieee8021QBridgeVlanFdbId [ ] ={1,111,2,802,1,4,1,4,2,1,4};
UINT4 Ieee8021QBridgeVlanCurrentEgressPorts [ ] ={1,111,2,802,1,4,1,4,2,1,5};
UINT4 Ieee8021QBridgeVlanCurrentUntaggedPorts [ ] ={1,111,2,802,1,4,1,4,2,1,6};
UINT4 Ieee8021QBridgeVlanStatus [ ] ={1,111,2,802,1,4,1,4,2,1,7};
UINT4 Ieee8021QBridgeVlanCreationTime [ ] ={1,111,2,802,1,4,1,4,2,1,8};
UINT4 Ieee8021QBridgeTpGroupAddress [ ] ={1,111,2,802,1,4,1,2,3,1,1};
UINT4 Ieee8021QBridgeTpGroupEgressPorts [ ] ={1,111,2,802,1,4,1,2,3,1,2};
UINT4 Ieee8021QBridgeTpGroupLearnt [ ] ={1,111,2,802,1,4,1,2,3,1,3};
UINT4 Ieee8021QBridgeForwardAllVlanIndex [ ] ={1,111,2,802,1,4,1,2,4,1,1};
UINT4 Ieee8021QBridgeForwardAllPorts [ ] ={1,111,2,802,1,4,1,2,4,1,2};
UINT4 Ieee8021QBridgeForwardAllStaticPorts [ ] ={1,111,2,802,1,4,1,2,4,1,3};
UINT4 Ieee8021QBridgeForwardAllForbiddenPorts [ ] ={1,111,2,802,1,4,1,2,4,1,4};
UINT4 Ieee8021QBridgeForwardUnregisteredVlanIndex [ ] ={1,111,2,802,1,4,1,2,5,1,1};
UINT4 Ieee8021QBridgeForwardUnregisteredPorts [ ] ={1,111,2,802,1,4,1,2,5,1,2};
UINT4 Ieee8021QBridgeForwardUnregisteredStaticPorts [ ] ={1,111,2,802,1,4,1,2,5,1,3};
UINT4 Ieee8021QBridgeForwardUnregisteredForbiddenPorts [ ] ={1,111,2,802,1,4,1,2,5,1,4};
UINT4 Ieee8021QBridgeStaticUnicastComponentId [ ] ={1,111,2,802,1,4,1,3,1,1,1};
UINT4 Ieee8021QBridgeStaticUnicastVlanIndex [ ] ={1,111,2,802,1,4,1,3,1,1,2};
UINT4 Ieee8021QBridgeStaticUnicastAddress [ ] ={1,111,2,802,1,4,1,3,1,1,3};
UINT4 Ieee8021QBridgeStaticUnicastReceivePort [ ] ={1,111,2,802,1,4,1,3,1,1,4};
UINT4 Ieee8021QBridgeStaticUnicastStaticEgressPorts [ ] ={1,111,2,802,1,4,1,3,1,1,5};
UINT4 Ieee8021QBridgeStaticUnicastForbiddenEgressPorts [ ] ={1,111,2,802,1,4,1,3,1,1,6};
UINT4 Ieee8021QBridgeStaticUnicastStorageType [ ] ={1,111,2,802,1,4,1,3,1,1,7};
UINT4 Ieee8021QBridgeStaticUnicastRowStatus [ ] ={1,111,2,802,1,4,1,3,1,1,8};
UINT4 Ieee8021QBridgeStaticMulticastAddress [ ] ={1,111,2,802,1,4,1,3,2,1,1};
UINT4 Ieee8021QBridgeStaticMulticastReceivePort [ ] ={1,111,2,802,1,4,1,3,2,1,2};
UINT4 Ieee8021QBridgeStaticMulticastStaticEgressPorts [ ] ={1,111,2,802,1,4,1,3,2,1,3};
UINT4 Ieee8021QBridgeStaticMulticastForbiddenEgressPorts [ ] ={1,111,2,802,1,4,1,3,2,1,4};
UINT4 Ieee8021QBridgeStaticMulticastStorageType [ ] ={1,111,2,802,1,4,1,3,2,1,5};
UINT4 Ieee8021QBridgeStaticMulticastRowStatus [ ] ={1,111,2,802,1,4,1,3,2,1,6};
UINT4 Ieee8021QBridgeVlanStaticComponentId [ ] ={1,111,2,802,1,4,1,4,3,1,1};
UINT4 Ieee8021QBridgeVlanStaticVlanIndex [ ] ={1,111,2,802,1,4,1,4,3,1,2};
UINT4 Ieee8021QBridgeVlanStaticName [ ] ={1,111,2,802,1,4,1,4,3,1,3};
UINT4 Ieee8021QBridgeVlanStaticEgressPorts [ ] ={1,111,2,802,1,4,1,4,3,1,4};
UINT4 Ieee8021QBridgeVlanForbiddenEgressPorts [ ] ={1,111,2,802,1,4,1,4,3,1,5};
UINT4 Ieee8021QBridgeVlanStaticUntaggedPorts [ ] ={1,111,2,802,1,4,1,4,3,1,6};
UINT4 Ieee8021QBridgeVlanStaticRowStatus [ ] ={1,111,2,802,1,4,1,4,3,1,7};
UINT4 Ieee8021QBridgeNextFreeLocalVlanComponentId [ ] ={1,111,2,802,1,4,1,4,4,1,1};
UINT4 Ieee8021QBridgeNextFreeLocalVlanIndex [ ] ={1,111,2,802,1,4,1,4,4,1,2};
UINT4 Ieee8021QBridgePvid [ ] ={1,111,2,802,1,4,1,4,5,1,1};
UINT4 Ieee8021QBridgePortAcceptableFrameTypes [ ] ={1,111,2,802,1,4,1,4,5,1,2};
UINT4 Ieee8021QBridgePortIngressFiltering [ ] ={1,111,2,802,1,4,1,4,5,1,3};
UINT4 Ieee8021QBridgePortMvrpEnabledStatus [ ] ={1,111,2,802,1,4,1,4,5,1,4};
UINT4 Ieee8021QBridgePortMvrpFailedRegistrations [ ] ={1,111,2,802,1,4,1,4,5,1,5};
UINT4 Ieee8021QBridgePortMvrpLastPduOrigin [ ] ={1,111,2,802,1,4,1,4,5,1,6};
UINT4 Ieee8021QBridgePortRestrictedVlanRegistration [ ] ={1,111,2,802,1,4,1,4,5,1,7};
UINT4 Ieee8021QBridgeTpVlanPortInFrames [ ] ={1,111,2,802,1,4,1,4,6,1,1};
UINT4 Ieee8021QBridgeTpVlanPortOutFrames [ ] ={1,111,2,802,1,4,1,4,6,1,2};
UINT4 Ieee8021QBridgeTpVlanPortInDiscards [ ] ={1,111,2,802,1,4,1,4,6,1,3};
UINT4 Ieee8021QBridgeLearningConstraintsComponentId [ ] ={1,111,2,802,1,4,1,4,8,1,1};
UINT4 Ieee8021QBridgeLearningConstraintsVlan [ ] ={1,111,2,802,1,4,1,4,8,1,2};
UINT4 Ieee8021QBridgeLearningConstraintsSet [ ] ={1,111,2,802,1,4,1,4,8,1,3};
UINT4 Ieee8021QBridgeLearningConstraintsType [ ] ={1,111,2,802,1,4,1,4,8,1,4};
UINT4 Ieee8021QBridgeLearningConstraintsStatus [ ] ={1,111,2,802,1,4,1,4,8,1,5};
UINT4 Ieee8021QBridgeLearningConstraintDefaultsComponentId [ ] ={1,111,2,802,1,4,1,4,9,1,1};
UINT4 Ieee8021QBridgeLearningConstraintDefaultsSet [ ] ={1,111,2,802,1,4,1,4,9,1,2};
UINT4 Ieee8021QBridgeLearningConstraintDefaultsType [ ] ={1,111,2,802,1,4,1,4,9,1,3};
UINT4 Ieee8021QBridgeProtocolGroupComponentId [ ] ={1,111,2,802,1,4,1,5,1,1,1};
UINT4 Ieee8021QBridgeProtocolTemplateFrameType [ ] ={1,111,2,802,1,4,1,5,1,1,2};
UINT4 Ieee8021QBridgeProtocolTemplateProtocolValue [ ] ={1,111,2,802,1,4,1,5,1,1,3};
UINT4 Ieee8021QBridgeProtocolGroupId [ ] ={1,111,2,802,1,4,1,5,1,1,4};
UINT4 Ieee8021QBridgeProtocolGroupRowStatus [ ] ={1,111,2,802,1,4,1,5,1,1,5};
UINT4 Ieee8021QBridgeProtocolPortGroupId [ ] ={1,111,2,802,1,4,1,5,2,1,1};
UINT4 Ieee8021QBridgeProtocolPortGroupVid [ ] ={1,111,2,802,1,4,1,5,2,1,2};
UINT4 Ieee8021QBridgeProtocolPortRowStatus [ ] ={1,111,2,802,1,4,1,5,2,1,3};


tMbDbEntry stqbrgMibEntry[]= {

{{11,Ieee8021QBridgeComponentId}, GetNextIndexIeee8021QBridgeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeTableINDEX, 1, 0, NULL},

{{11,Ieee8021QBridgeVlanVersionNumber}, GetNextIndexIeee8021QBridgeTable, Ieee8021QBridgeVlanVersionNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021QBridgeTableINDEX, 1, 0, NULL},

{{11,Ieee8021QBridgeMaxVlanId}, GetNextIndexIeee8021QBridgeTable, Ieee8021QBridgeMaxVlanIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021QBridgeTableINDEX, 1, 0, NULL},

{{11,Ieee8021QBridgeMaxSupportedVlans}, GetNextIndexIeee8021QBridgeTable, Ieee8021QBridgeMaxSupportedVlansGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021QBridgeTableINDEX, 1, 0, NULL},

{{11,Ieee8021QBridgeNumVlans}, GetNextIndexIeee8021QBridgeTable, Ieee8021QBridgeNumVlansGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Ieee8021QBridgeTableINDEX, 1, 0, NULL},

{{11,Ieee8021QBridgeMvrpEnabledStatus}, GetNextIndexIeee8021QBridgeTable, Ieee8021QBridgeMvrpEnabledStatusGet, Ieee8021QBridgeMvrpEnabledStatusSet, Ieee8021QBridgeMvrpEnabledStatusTest, Ieee8021QBridgeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeTableINDEX, 1, 0, "1"},

{{11,Ieee8021QBridgeCVlanPortComponentId}, GetNextIndexIeee8021QBridgeCVlanPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeCVlanPortTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeCVlanPortNumber}, GetNextIndexIeee8021QBridgeCVlanPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeCVlanPortTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeCVlanPortRowStatus}, GetNextIndexIeee8021QBridgeCVlanPortTable, Ieee8021QBridgeCVlanPortRowStatusGet, Ieee8021QBridgeCVlanPortRowStatusSet, Ieee8021QBridgeCVlanPortRowStatusTest, Ieee8021QBridgeCVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeCVlanPortTableINDEX, 2, 1, NULL},

{{11,Ieee8021QBridgeFdbComponentId}, GetNextIndexIeee8021QBridgeFdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeFdbTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeFdbId}, GetNextIndexIeee8021QBridgeFdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeFdbTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeFdbDynamicCount}, GetNextIndexIeee8021QBridgeFdbTable, Ieee8021QBridgeFdbDynamicCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Ieee8021QBridgeFdbTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeFdbLearnedEntryDiscards}, GetNextIndexIeee8021QBridgeFdbTable, Ieee8021QBridgeFdbLearnedEntryDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021QBridgeFdbTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeFdbAgingTime}, GetNextIndexIeee8021QBridgeFdbTable, Ieee8021QBridgeFdbAgingTimeGet, Ieee8021QBridgeFdbAgingTimeSet, Ieee8021QBridgeFdbAgingTimeTest, Ieee8021QBridgeFdbTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021QBridgeFdbTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeTpFdbAddress}, GetNextIndexIeee8021QBridgeTpFdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Ieee8021QBridgeTpFdbTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeTpFdbPort}, GetNextIndexIeee8021QBridgeTpFdbTable, Ieee8021QBridgeTpFdbPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021QBridgeTpFdbTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeTpFdbStatus}, GetNextIndexIeee8021QBridgeTpFdbTable, Ieee8021QBridgeTpFdbStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021QBridgeTpFdbTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeTpGroupAddress}, GetNextIndexIeee8021QBridgeTpGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Ieee8021QBridgeTpGroupTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeTpGroupEgressPorts}, GetNextIndexIeee8021QBridgeTpGroupTable, Ieee8021QBridgeTpGroupEgressPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021QBridgeTpGroupTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeTpGroupLearnt}, GetNextIndexIeee8021QBridgeTpGroupTable, Ieee8021QBridgeTpGroupLearntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021QBridgeTpGroupTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeForwardAllVlanIndex}, GetNextIndexIeee8021QBridgeForwardAllTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeForwardAllTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeForwardAllPorts}, GetNextIndexIeee8021QBridgeForwardAllTable, Ieee8021QBridgeForwardAllPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021QBridgeForwardAllTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeForwardAllStaticPorts}, GetNextIndexIeee8021QBridgeForwardAllTable, Ieee8021QBridgeForwardAllStaticPortsGet, Ieee8021QBridgeForwardAllStaticPortsSet, Ieee8021QBridgeForwardAllStaticPortsTest, Ieee8021QBridgeForwardAllTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeForwardAllTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeForwardAllForbiddenPorts}, GetNextIndexIeee8021QBridgeForwardAllTable, Ieee8021QBridgeForwardAllForbiddenPortsGet, Ieee8021QBridgeForwardAllForbiddenPortsSet, Ieee8021QBridgeForwardAllForbiddenPortsTest, Ieee8021QBridgeForwardAllTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeForwardAllTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeForwardUnregisteredVlanIndex}, GetNextIndexIeee8021QBridgeForwardUnregisteredTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeForwardUnregisteredTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeForwardUnregisteredPorts}, GetNextIndexIeee8021QBridgeForwardUnregisteredTable, Ieee8021QBridgeForwardUnregisteredPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021QBridgeForwardUnregisteredTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeForwardUnregisteredStaticPorts}, GetNextIndexIeee8021QBridgeForwardUnregisteredTable, Ieee8021QBridgeForwardUnregisteredStaticPortsGet, Ieee8021QBridgeForwardUnregisteredStaticPortsSet, Ieee8021QBridgeForwardUnregisteredStaticPortsTest, Ieee8021QBridgeForwardUnregisteredTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeForwardUnregisteredTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeForwardUnregisteredForbiddenPorts}, GetNextIndexIeee8021QBridgeForwardUnregisteredTable, Ieee8021QBridgeForwardUnregisteredForbiddenPortsGet, Ieee8021QBridgeForwardUnregisteredForbiddenPortsSet, Ieee8021QBridgeForwardUnregisteredForbiddenPortsTest, Ieee8021QBridgeForwardUnregisteredTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeForwardUnregisteredTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeStaticUnicastComponentId}, GetNextIndexIeee8021QBridgeStaticUnicastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeStaticUnicastTableINDEX, 4, 0, NULL},

{{11,Ieee8021QBridgeStaticUnicastVlanIndex}, GetNextIndexIeee8021QBridgeStaticUnicastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeStaticUnicastTableINDEX, 4, 0, NULL},

{{11,Ieee8021QBridgeStaticUnicastAddress}, GetNextIndexIeee8021QBridgeStaticUnicastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Ieee8021QBridgeStaticUnicastTableINDEX, 4, 0, NULL},

{{11,Ieee8021QBridgeStaticUnicastReceivePort}, GetNextIndexIeee8021QBridgeStaticUnicastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeStaticUnicastTableINDEX, 4, 0, NULL},

{{11,Ieee8021QBridgeStaticUnicastStaticEgressPorts}, GetNextIndexIeee8021QBridgeStaticUnicastTable, Ieee8021QBridgeStaticUnicastStaticEgressPortsGet, Ieee8021QBridgeStaticUnicastStaticEgressPortsSet, Ieee8021QBridgeStaticUnicastStaticEgressPortsTest, Ieee8021QBridgeStaticUnicastTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeStaticUnicastTableINDEX, 4, 0, "0"},

{{11,Ieee8021QBridgeStaticUnicastForbiddenEgressPorts}, GetNextIndexIeee8021QBridgeStaticUnicastTable, Ieee8021QBridgeStaticUnicastForbiddenEgressPortsGet, Ieee8021QBridgeStaticUnicastForbiddenEgressPortsSet, Ieee8021QBridgeStaticUnicastForbiddenEgressPortsTest, Ieee8021QBridgeStaticUnicastTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeStaticUnicastTableINDEX, 4, 0, "0"},

{{11,Ieee8021QBridgeStaticUnicastStorageType}, GetNextIndexIeee8021QBridgeStaticUnicastTable, Ieee8021QBridgeStaticUnicastStorageTypeGet, Ieee8021QBridgeStaticUnicastStorageTypeSet, Ieee8021QBridgeStaticUnicastStorageTypeTest, Ieee8021QBridgeStaticUnicastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeStaticUnicastTableINDEX, 4, 0, "3"},

{{11,Ieee8021QBridgeStaticUnicastRowStatus}, GetNextIndexIeee8021QBridgeStaticUnicastTable, Ieee8021QBridgeStaticUnicastRowStatusGet, Ieee8021QBridgeStaticUnicastRowStatusSet, Ieee8021QBridgeStaticUnicastRowStatusTest, Ieee8021QBridgeStaticUnicastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeStaticUnicastTableINDEX, 4, 1, NULL},

{{11,Ieee8021QBridgeStaticMulticastAddress}, GetNextIndexIeee8021QBridgeStaticMulticastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Ieee8021QBridgeStaticMulticastTableINDEX, 4, 0, NULL},

{{11,Ieee8021QBridgeStaticMulticastReceivePort}, GetNextIndexIeee8021QBridgeStaticMulticastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeStaticMulticastTableINDEX, 4, 0, NULL},

{{11,Ieee8021QBridgeStaticMulticastStaticEgressPorts}, GetNextIndexIeee8021QBridgeStaticMulticastTable, Ieee8021QBridgeStaticMulticastStaticEgressPortsGet, Ieee8021QBridgeStaticMulticastStaticEgressPortsSet, Ieee8021QBridgeStaticMulticastStaticEgressPortsTest, Ieee8021QBridgeStaticMulticastTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeStaticMulticastTableINDEX, 4, 0, "0"},

{{11,Ieee8021QBridgeStaticMulticastForbiddenEgressPorts}, GetNextIndexIeee8021QBridgeStaticMulticastTable, Ieee8021QBridgeStaticMulticastForbiddenEgressPortsGet, Ieee8021QBridgeStaticMulticastForbiddenEgressPortsSet, Ieee8021QBridgeStaticMulticastForbiddenEgressPortsTest, Ieee8021QBridgeStaticMulticastTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeStaticMulticastTableINDEX, 4, 0, "0"},

{{11,Ieee8021QBridgeStaticMulticastStorageType}, GetNextIndexIeee8021QBridgeStaticMulticastTable, Ieee8021QBridgeStaticMulticastStorageTypeGet, Ieee8021QBridgeStaticMulticastStorageTypeSet, Ieee8021QBridgeStaticMulticastStorageTypeTest, Ieee8021QBridgeStaticMulticastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeStaticMulticastTableINDEX, 4, 0, "3"},

{{11,Ieee8021QBridgeStaticMulticastRowStatus}, GetNextIndexIeee8021QBridgeStaticMulticastTable, Ieee8021QBridgeStaticMulticastRowStatusGet, Ieee8021QBridgeStaticMulticastRowStatusSet, Ieee8021QBridgeStaticMulticastRowStatusTest, Ieee8021QBridgeStaticMulticastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeStaticMulticastTableINDEX, 4, 1, NULL},

{{9,Ieee8021QBridgeVlanNumDeletes}, NULL, Ieee8021QBridgeVlanNumDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, NULL, 0, 0, NULL},

{{11,Ieee8021QBridgeVlanTimeMark}, GetNextIndexIeee8021QBridgeVlanCurrentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeVlanCurrentTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeVlanCurrentComponentId}, GetNextIndexIeee8021QBridgeVlanCurrentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeVlanCurrentTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeVlanIndex}, GetNextIndexIeee8021QBridgeVlanCurrentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeVlanCurrentTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeVlanFdbId}, GetNextIndexIeee8021QBridgeVlanCurrentTable, Ieee8021QBridgeVlanFdbIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021QBridgeVlanCurrentTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeVlanCurrentEgressPorts}, GetNextIndexIeee8021QBridgeVlanCurrentTable, Ieee8021QBridgeVlanCurrentEgressPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021QBridgeVlanCurrentTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeVlanCurrentUntaggedPorts}, GetNextIndexIeee8021QBridgeVlanCurrentTable, Ieee8021QBridgeVlanCurrentUntaggedPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021QBridgeVlanCurrentTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeVlanStatus}, GetNextIndexIeee8021QBridgeVlanCurrentTable, Ieee8021QBridgeVlanStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021QBridgeVlanCurrentTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeVlanCreationTime}, GetNextIndexIeee8021QBridgeVlanCurrentTable, Ieee8021QBridgeVlanCreationTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Ieee8021QBridgeVlanCurrentTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeVlanStaticComponentId}, GetNextIndexIeee8021QBridgeVlanStaticTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeVlanStaticTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeVlanStaticVlanIndex}, GetNextIndexIeee8021QBridgeVlanStaticTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeVlanStaticTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeVlanStaticName}, GetNextIndexIeee8021QBridgeVlanStaticTable, Ieee8021QBridgeVlanStaticNameGet, Ieee8021QBridgeVlanStaticNameSet, Ieee8021QBridgeVlanStaticNameTest, Ieee8021QBridgeVlanStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeVlanStaticTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeVlanStaticEgressPorts}, GetNextIndexIeee8021QBridgeVlanStaticTable, Ieee8021QBridgeVlanStaticEgressPortsGet, Ieee8021QBridgeVlanStaticEgressPortsSet, Ieee8021QBridgeVlanStaticEgressPortsTest, Ieee8021QBridgeVlanStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeVlanStaticTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeVlanForbiddenEgressPorts}, GetNextIndexIeee8021QBridgeVlanStaticTable, Ieee8021QBridgeVlanForbiddenEgressPortsGet, Ieee8021QBridgeVlanForbiddenEgressPortsSet, Ieee8021QBridgeVlanForbiddenEgressPortsTest, Ieee8021QBridgeVlanStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeVlanStaticTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeVlanStaticUntaggedPorts}, GetNextIndexIeee8021QBridgeVlanStaticTable, Ieee8021QBridgeVlanStaticUntaggedPortsGet, Ieee8021QBridgeVlanStaticUntaggedPortsSet, Ieee8021QBridgeVlanStaticUntaggedPortsTest, Ieee8021QBridgeVlanStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021QBridgeVlanStaticTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgeVlanStaticRowStatus}, GetNextIndexIeee8021QBridgeVlanStaticTable, Ieee8021QBridgeVlanStaticRowStatusGet, Ieee8021QBridgeVlanStaticRowStatusSet, Ieee8021QBridgeVlanStaticRowStatusTest, Ieee8021QBridgeVlanStaticTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeVlanStaticTableINDEX, 2, 1, NULL},

{{11,Ieee8021QBridgeNextFreeLocalVlanComponentId}, GetNextIndexIeee8021QBridgeNextFreeLocalVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeNextFreeLocalVlanTableINDEX, 1, 0, NULL},

{{11,Ieee8021QBridgeNextFreeLocalVlanIndex}, GetNextIndexIeee8021QBridgeNextFreeLocalVlanTable, Ieee8021QBridgeNextFreeLocalVlanIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021QBridgeNextFreeLocalVlanTableINDEX, 1, 0, NULL},

{{11,Ieee8021QBridgePvid}, GetNextIndexIeee8021QBridgePortVlanTable, Ieee8021QBridgePvidGet, Ieee8021QBridgePvidSet, Ieee8021QBridgePvidTest, Ieee8021QBridgePortVlanTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021QBridgePortVlanTableINDEX, 2, 0, "1"},

{{11,Ieee8021QBridgePortAcceptableFrameTypes}, GetNextIndexIeee8021QBridgePortVlanTable, Ieee8021QBridgePortAcceptableFrameTypesGet, Ieee8021QBridgePortAcceptableFrameTypesSet, Ieee8021QBridgePortAcceptableFrameTypesTest, Ieee8021QBridgePortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgePortVlanTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgePortIngressFiltering}, GetNextIndexIeee8021QBridgePortVlanTable, Ieee8021QBridgePortIngressFilteringGet, Ieee8021QBridgePortIngressFilteringSet, Ieee8021QBridgePortIngressFilteringTest, Ieee8021QBridgePortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgePortVlanTableINDEX, 2, 0, "2"},

{{11,Ieee8021QBridgePortMvrpEnabledStatus}, GetNextIndexIeee8021QBridgePortVlanTable, Ieee8021QBridgePortMvrpEnabledStatusGet, Ieee8021QBridgePortMvrpEnabledStatusSet, Ieee8021QBridgePortMvrpEnabledStatusTest, Ieee8021QBridgePortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgePortVlanTableINDEX, 2, 0, "1"},

{{11,Ieee8021QBridgePortMvrpFailedRegistrations}, GetNextIndexIeee8021QBridgePortVlanTable, Ieee8021QBridgePortMvrpFailedRegistrationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021QBridgePortVlanTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgePortMvrpLastPduOrigin}, GetNextIndexIeee8021QBridgePortVlanTable, Ieee8021QBridgePortMvrpLastPduOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Ieee8021QBridgePortVlanTableINDEX, 2, 0, NULL},

{{11,Ieee8021QBridgePortRestrictedVlanRegistration}, GetNextIndexIeee8021QBridgePortVlanTable, Ieee8021QBridgePortRestrictedVlanRegistrationGet, Ieee8021QBridgePortRestrictedVlanRegistrationSet, Ieee8021QBridgePortRestrictedVlanRegistrationTest, Ieee8021QBridgePortVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgePortVlanTableINDEX, 2, 0, "2"},

{{11,Ieee8021QBridgeTpVlanPortInFrames}, GetNextIndexIeee8021QBridgePortVlanStatisticsTable, Ieee8021QBridgeTpVlanPortInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021QBridgePortVlanStatisticsTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeTpVlanPortOutFrames}, GetNextIndexIeee8021QBridgePortVlanStatisticsTable, Ieee8021QBridgeTpVlanPortOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021QBridgePortVlanStatisticsTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeTpVlanPortInDiscards}, GetNextIndexIeee8021QBridgePortVlanStatisticsTable, Ieee8021QBridgeTpVlanPortInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021QBridgePortVlanStatisticsTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeLearningConstraintsComponentId}, GetNextIndexIeee8021QBridgeLearningConstraintsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeLearningConstraintsTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeLearningConstraintsVlan}, GetNextIndexIeee8021QBridgeLearningConstraintsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeLearningConstraintsTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeLearningConstraintsSet}, GetNextIndexIeee8021QBridgeLearningConstraintsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Ieee8021QBridgeLearningConstraintsTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeLearningConstraintsType}, GetNextIndexIeee8021QBridgeLearningConstraintsTable, Ieee8021QBridgeLearningConstraintsTypeGet, Ieee8021QBridgeLearningConstraintsTypeSet, Ieee8021QBridgeLearningConstraintsTypeTest, Ieee8021QBridgeLearningConstraintsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeLearningConstraintsTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeLearningConstraintsStatus}, GetNextIndexIeee8021QBridgeLearningConstraintsTable, Ieee8021QBridgeLearningConstraintsStatusGet, Ieee8021QBridgeLearningConstraintsStatusSet, Ieee8021QBridgeLearningConstraintsStatusTest, Ieee8021QBridgeLearningConstraintsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeLearningConstraintsTableINDEX, 3, 1, NULL},

{{11,Ieee8021QBridgeLearningConstraintDefaultsComponentId}, GetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeLearningConstraintDefaultsTableINDEX, 1, 0, NULL},

{{11,Ieee8021QBridgeLearningConstraintDefaultsSet}, GetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable, Ieee8021QBridgeLearningConstraintDefaultsSetGet, Ieee8021QBridgeLearningConstraintDefaultsSetSet, Ieee8021QBridgeLearningConstraintDefaultsSetTest, Ieee8021QBridgeLearningConstraintDefaultsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021QBridgeLearningConstraintDefaultsTableINDEX, 1, 0, NULL},

{{11,Ieee8021QBridgeLearningConstraintDefaultsType}, GetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable, Ieee8021QBridgeLearningConstraintDefaultsTypeGet, Ieee8021QBridgeLearningConstraintDefaultsTypeSet, Ieee8021QBridgeLearningConstraintDefaultsTypeTest, Ieee8021QBridgeLearningConstraintDefaultsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeLearningConstraintDefaultsTableINDEX, 1, 0, NULL},

{{11,Ieee8021QBridgeProtocolGroupComponentId}, GetNextIndexIeee8021QBridgeProtocolGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021QBridgeProtocolGroupTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeProtocolTemplateFrameType}, GetNextIndexIeee8021QBridgeProtocolGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021QBridgeProtocolGroupTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeProtocolTemplateProtocolValue}, GetNextIndexIeee8021QBridgeProtocolGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Ieee8021QBridgeProtocolGroupTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeProtocolGroupId}, GetNextIndexIeee8021QBridgeProtocolGroupTable, Ieee8021QBridgeProtocolGroupIdGet, Ieee8021QBridgeProtocolGroupIdSet, Ieee8021QBridgeProtocolGroupIdTest, Ieee8021QBridgeProtocolGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021QBridgeProtocolGroupTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeProtocolGroupRowStatus}, GetNextIndexIeee8021QBridgeProtocolGroupTable, Ieee8021QBridgeProtocolGroupRowStatusGet, Ieee8021QBridgeProtocolGroupRowStatusSet, Ieee8021QBridgeProtocolGroupRowStatusTest, Ieee8021QBridgeProtocolGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeProtocolGroupTableINDEX, 3, 1, NULL},

{{11,Ieee8021QBridgeProtocolPortGroupId}, GetNextIndexIeee8021QBridgeProtocolPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Ieee8021QBridgeProtocolPortTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeProtocolPortGroupVid}, GetNextIndexIeee8021QBridgeProtocolPortTable, Ieee8021QBridgeProtocolPortGroupVidGet, Ieee8021QBridgeProtocolPortGroupVidSet, Ieee8021QBridgeProtocolPortGroupVidTest, Ieee8021QBridgeProtocolPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeProtocolPortTableINDEX, 3, 0, NULL},

{{11,Ieee8021QBridgeProtocolPortRowStatus}, GetNextIndexIeee8021QBridgeProtocolPortTable, Ieee8021QBridgeProtocolPortRowStatusGet, Ieee8021QBridgeProtocolPortRowStatusSet, Ieee8021QBridgeProtocolPortRowStatusTest, Ieee8021QBridgeProtocolPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021QBridgeProtocolPortTableINDEX, 3, 1, NULL},
};
tMibData stqbrgEntry = { 86, stqbrgMibEntry };
#endif /* _STQBRGDB_H */

