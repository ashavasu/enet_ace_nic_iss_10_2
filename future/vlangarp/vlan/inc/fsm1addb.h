/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsm1addb.h,v 1.10 2013/12/05 12:44:52 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSM1ADDB_H
#define _FSM1ADDB_H

UINT1 Dot1adMIPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1adMIVidTranslationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1adMICVidRegistrationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1adMIPepTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1adMIServicePriorityRegenerationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1adMIPcpDecodingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot1adMIPcpEncodingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsm1ad [] ={1,3,6,1,4,1,2076,133};
tSNMP_OID_TYPE fsm1adOID = {8, fsm1ad};


UINT4 Dot1adMIPortNum [ ] ={1,3,6,1,4,1,2076,133,1,1,1,1};
UINT4 Dot1adMIPortPcpSelectionRow [ ] ={1,3,6,1,4,1,2076,133,1,1,1,2};
UINT4 Dot1adMIPortUseDei [ ] ={1,3,6,1,4,1,2076,133,1,1,1,3};
UINT4 Dot1adMIPortReqDropEncoding [ ] ={1,3,6,1,4,1,2076,133,1,1,1,4};
UINT4 Dot1adMIPortSVlanPriorityType [ ] ={1,3,6,1,4,1,2076,133,1,1,1,5};
UINT4 Dot1adMIPortSVlanPriority [ ] ={1,3,6,1,4,1,2076,133,1,1,1,6};
UINT4 Dot1adMIVidTranslationLocalVid [ ] ={1,3,6,1,4,1,2076,133,1,2,1,1};
UINT4 Dot1adMIVidTranslationRelayVid [ ] ={1,3,6,1,4,1,2076,133,1,2,1,2};
UINT4 Dot1adMIVidTranslationRowStatus [ ] ={1,3,6,1,4,1,2076,133,1,2,1,3};
UINT4 Dot1adMICVidRegistrationCVid [ ] ={1,3,6,1,4,1,2076,133,1,3,1,1};
UINT4 Dot1adMICVidRegistrationSVid [ ] ={1,3,6,1,4,1,2076,133,1,3,1,2};
UINT4 Dot1adMICVidRegistrationUntaggedPep [ ] ={1,3,6,1,4,1,2076,133,1,3,1,3};
UINT4 Dot1adMICVidRegistrationUntaggedCep [ ] ={1,3,6,1,4,1,2076,133,1,3,1,4};
UINT4 Dot1adMICVidRegistrationRowStatus [ ] ={1,3,6,1,4,1,2076,133,1,3,1,5};
UINT4 Dot1adMICVidRegistrationSVlanPriorityType [ ] ={1,3,6,1,4,1,2076,133,1,3,1,6};
UINT4 Dot1adMICVidRegistrationSVlanPriority [ ] ={1,3,6,1,4,1,2076,133,1,3,1,7};
UINT4 Dot1adMICVidRegistrationRelayCVid [ ] ={1,3,6,1,4,1,2076,133,1,3,1,8};
UINT4 Dot1adMIPepPvid [ ] ={1,3,6,1,4,1,2076,133,1,4,1,1};
UINT4 Dot1adMIPepDefaultUserPriority [ ] ={1,3,6,1,4,1,2076,133,1,4,1,2};
UINT4 Dot1adMIPepAccptableFrameTypes [ ] ={1,3,6,1,4,1,2076,133,1,4,1,3};
UINT4 Dot1adMIPepIngressFiltering [ ] ={1,3,6,1,4,1,2076,133,1,4,1,4};
UINT4 Dot1adMIServicePriorityRegenReceivedPriority [ ] ={1,3,6,1,4,1,2076,133,1,5,1,1};
UINT4 Dot1adMIServicePriorityRegenRegeneratedPriority [ ] ={1,3,6,1,4,1,2076,133,1,5,1,2};
UINT4 Dot1adMIPcpDecodingPcpSelRow [ ] ={1,3,6,1,4,1,2076,133,1,6,1,1};
UINT4 Dot1adMIPcpDecodingPcpValue [ ] ={1,3,6,1,4,1,2076,133,1,6,1,2};
UINT4 Dot1adMIPcpDecodingPriority [ ] ={1,3,6,1,4,1,2076,133,1,6,1,3};
UINT4 Dot1adMIPcpDecodingDropEligible [ ] ={1,3,6,1,4,1,2076,133,1,6,1,4};
UINT4 Dot1adMIPcpEncodingPcpSelRow [ ] ={1,3,6,1,4,1,2076,133,1,7,1,1};
UINT4 Dot1adMIPcpEncodingPriority [ ] ={1,3,6,1,4,1,2076,133,1,7,1,2};
UINT4 Dot1adMIPcpEncodingDropEligible [ ] ={1,3,6,1,4,1,2076,133,1,7,1,3};
UINT4 Dot1adMIPcpEncodingPcpValue [ ] ={1,3,6,1,4,1,2076,133,1,7,1,4};


tMbDbEntry fsm1adMibEntry[]= {

{{12,Dot1adMIPortNum}, GetNextIndexDot1adMIPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1adMIPortTableINDEX, 1, 0, 0, NULL},

{{12,Dot1adMIPortPcpSelectionRow}, GetNextIndexDot1adMIPortTable, Dot1adMIPortPcpSelectionRowGet, Dot1adMIPortPcpSelectionRowSet, Dot1adMIPortPcpSelectionRowTest, Dot1adMIPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPortTableINDEX, 1, 0, 0, "1"},

{{12,Dot1adMIPortUseDei}, GetNextIndexDot1adMIPortTable, Dot1adMIPortUseDeiGet, Dot1adMIPortUseDeiSet, Dot1adMIPortUseDeiTest, Dot1adMIPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPortTableINDEX, 1, 0, 0, "2"},

{{12,Dot1adMIPortReqDropEncoding}, GetNextIndexDot1adMIPortTable, Dot1adMIPortReqDropEncodingGet, Dot1adMIPortReqDropEncodingSet, Dot1adMIPortReqDropEncodingTest, Dot1adMIPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPortTableINDEX, 1, 0, 0, "2"},

{{12,Dot1adMIPortSVlanPriorityType}, GetNextIndexDot1adMIPortTable, Dot1adMIPortSVlanPriorityTypeGet, Dot1adMIPortSVlanPriorityTypeSet, Dot1adMIPortSVlanPriorityTypeTest, Dot1adMIPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPortTableINDEX, 1, 0, 0, "0"},

{{12,Dot1adMIPortSVlanPriority}, GetNextIndexDot1adMIPortTable, Dot1adMIPortSVlanPriorityGet, Dot1adMIPortSVlanPrioritySet, Dot1adMIPortSVlanPriorityTest, Dot1adMIPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPortTableINDEX, 1, 0, 0, "0"},

{{12,Dot1adMIVidTranslationLocalVid}, GetNextIndexDot1adMIVidTranslationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adMIVidTranslationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adMIVidTranslationRelayVid}, GetNextIndexDot1adMIVidTranslationTable, Dot1adMIVidTranslationRelayVidGet, Dot1adMIVidTranslationRelayVidSet, Dot1adMIVidTranslationRelayVidTest, Dot1adMIVidTranslationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIVidTranslationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adMIVidTranslationRowStatus}, GetNextIndexDot1adMIVidTranslationTable, Dot1adMIVidTranslationRowStatusGet, Dot1adMIVidTranslationRowStatusSet, Dot1adMIVidTranslationRowStatusTest, Dot1adMIVidTranslationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIVidTranslationTableINDEX, 2, 0, 1, NULL},

{{12,Dot1adMICVidRegistrationCVid}, GetNextIndexDot1adMICVidRegistrationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adMICVidRegistrationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adMICVidRegistrationSVid}, GetNextIndexDot1adMICVidRegistrationTable, Dot1adMICVidRegistrationSVidGet, Dot1adMICVidRegistrationSVidSet, Dot1adMICVidRegistrationSVidTest, Dot1adMICVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMICVidRegistrationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adMICVidRegistrationUntaggedPep}, GetNextIndexDot1adMICVidRegistrationTable, Dot1adMICVidRegistrationUntaggedPepGet, Dot1adMICVidRegistrationUntaggedPepSet, Dot1adMICVidRegistrationUntaggedPepTest, Dot1adMICVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMICVidRegistrationTableINDEX, 2, 0, 0, "2"},

{{12,Dot1adMICVidRegistrationUntaggedCep}, GetNextIndexDot1adMICVidRegistrationTable, Dot1adMICVidRegistrationUntaggedCepGet, Dot1adMICVidRegistrationUntaggedCepSet, Dot1adMICVidRegistrationUntaggedCepTest, Dot1adMICVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMICVidRegistrationTableINDEX, 2, 0, 0, "2"},

{{12,Dot1adMICVidRegistrationRowStatus}, GetNextIndexDot1adMICVidRegistrationTable, Dot1adMICVidRegistrationRowStatusGet, Dot1adMICVidRegistrationRowStatusSet, Dot1adMICVidRegistrationRowStatusTest, Dot1adMICVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMICVidRegistrationTableINDEX, 2, 0, 1, NULL},

{{12,Dot1adMICVidRegistrationSVlanPriorityType}, GetNextIndexDot1adMICVidRegistrationTable, Dot1adMICVidRegistrationSVlanPriorityTypeGet, Dot1adMICVidRegistrationSVlanPriorityTypeSet, Dot1adMICVidRegistrationSVlanPriorityTypeTest, Dot1adMICVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMICVidRegistrationTableINDEX, 2, 0, 0, "0"},

{{12,Dot1adMICVidRegistrationSVlanPriority}, GetNextIndexDot1adMICVidRegistrationTable, Dot1adMICVidRegistrationSVlanPriorityGet, Dot1adMICVidRegistrationSVlanPrioritySet, Dot1adMICVidRegistrationSVlanPriorityTest, Dot1adMICVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMICVidRegistrationTableINDEX, 2, 0, 0, "0"},

{{12,Dot1adMICVidRegistrationRelayCVid}, GetNextIndexDot1adMICVidRegistrationTable, Dot1adMICVidRegistrationRelayCVidGet, Dot1adMICVidRegistrationRelayCVidSet, Dot1adMICVidRegistrationRelayCVidTest, Dot1adMICVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMICVidRegistrationTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adMIPepPvid}, GetNextIndexDot1adMIPepTable, Dot1adMIPepPvidGet, Dot1adMIPepPvidSet, Dot1adMIPepPvidTest, Dot1adMIPepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPepTableINDEX, 2, 0, 0, NULL},

{{12,Dot1adMIPepDefaultUserPriority}, GetNextIndexDot1adMIPepTable, Dot1adMIPepDefaultUserPriorityGet, Dot1adMIPepDefaultUserPrioritySet, Dot1adMIPepDefaultUserPriorityTest, Dot1adMIPepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPepTableINDEX, 2, 0, 0, "0"},

{{12,Dot1adMIPepAccptableFrameTypes}, GetNextIndexDot1adMIPepTable, Dot1adMIPepAccptableFrameTypesGet, Dot1adMIPepAccptableFrameTypesSet, Dot1adMIPepAccptableFrameTypesTest, Dot1adMIPepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPepTableINDEX, 2, 0, 0, "1"},

{{12,Dot1adMIPepIngressFiltering}, GetNextIndexDot1adMIPepTable, Dot1adMIPepIngressFilteringGet, Dot1adMIPepIngressFilteringSet, Dot1adMIPepIngressFilteringTest, Dot1adMIPepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPepTableINDEX, 2, 0, 0, "2"},

{{12,Dot1adMIServicePriorityRegenReceivedPriority}, GetNextIndexDot1adMIServicePriorityRegenerationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adMIServicePriorityRegenerationTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adMIServicePriorityRegenRegeneratedPriority}, GetNextIndexDot1adMIServicePriorityRegenerationTable, Dot1adMIServicePriorityRegenRegeneratedPriorityGet, Dot1adMIServicePriorityRegenRegeneratedPrioritySet, Dot1adMIServicePriorityRegenRegeneratedPriorityTest, Dot1adMIServicePriorityRegenerationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIServicePriorityRegenerationTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adMIPcpDecodingPcpSelRow}, GetNextIndexDot1adMIPcpDecodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adMIPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adMIPcpDecodingPcpValue}, GetNextIndexDot1adMIPcpDecodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adMIPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adMIPcpDecodingPriority}, GetNextIndexDot1adMIPcpDecodingTable, Dot1adMIPcpDecodingPriorityGet, Dot1adMIPcpDecodingPrioritySet, Dot1adMIPcpDecodingPriorityTest, Dot1adMIPcpDecodingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adMIPcpDecodingDropEligible}, GetNextIndexDot1adMIPcpDecodingTable, Dot1adMIPcpDecodingDropEligibleGet, Dot1adMIPcpDecodingDropEligibleSet, Dot1adMIPcpDecodingDropEligibleTest, Dot1adMIPcpDecodingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{12,Dot1adMIPcpEncodingPcpSelRow}, GetNextIndexDot1adMIPcpEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adMIPcpEncodingTableINDEX, 4, 0, 0, NULL},

{{12,Dot1adMIPcpEncodingPriority}, GetNextIndexDot1adMIPcpEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adMIPcpEncodingTableINDEX, 4, 0, 0, NULL},

{{12,Dot1adMIPcpEncodingDropEligible}, GetNextIndexDot1adMIPcpEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot1adMIPcpEncodingTableINDEX, 4, 0, 0, NULL},

{{12,Dot1adMIPcpEncodingPcpValue}, GetNextIndexDot1adMIPcpEncodingTable, Dot1adMIPcpEncodingPcpValueGet, Dot1adMIPcpEncodingPcpValueSet, Dot1adMIPcpEncodingPcpValueTest, Dot1adMIPcpEncodingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1adMIPcpEncodingTableINDEX, 4, 0, 0, NULL},
};
tMibData fsm1adEntry = { 31, fsm1adMibEntry };

#endif /* _FSM1ADDB_H */

