/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: vlanmacs.h,v 1.107 2017/10/26 12:21:01 siva Exp $                     */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : vlanmacs.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN                                             */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains definitions of macros used    */
/*                          in the VLAN module.                              */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#ifndef _VLANMACS_H
#define _VLANMACS_H

#define VLAN_RB_CMP_EQUAL         0
#define VLAN_RB_CMP_LESSER        -1            
#define VLAN_RB_CMP_GREATER       1
#define VLAN_MAX_CONTEXT        VLAN_SIZING_CONTEXT_COUNT

#define VLAN_DEFAULT_BCONTEXT_ID  0xFFFFFFFF
#define VLAN_DEFAULT_ICONTEXT_ID  0x0
#define VLAN_DEFAULT_CONTEXT_ID   0x0

#define VLAN_INGRESS_ETHERTYPE  1
#define VLAN_EGRESS_ETHERTYPE   2

#define VLAN_CUSTOMER_VLAN_PORT     0
#define VLAN_PROVID_NETWORK_PORT    1
#define VLAN_CUSTOMER_NETWORK_PORT  2
#define VLAN_CUST_EDGE_PORT         3
#define VLAN_CUST_BACKBONE_PORT     4
#define VLAN_VIRT_INSTANCE_PORT     5
#define VLAN_BRIDGE_PORT            6
#define VLAN_IP_DST_ADDR_LEN_SIZE   16

#define VLAN_ENABLE_GLOBAL_TRIGGER        4
#define VLAN_DISABLE_GLOBAL_TRIGGER       5
#define VLAN_LEARNING_LIMIT_ZERO_TRIGGER  6 
#define VLAN_DEFAULT_SLOT_ID              0

#define VLAN_PLATFORM_LEARNING_MODE()  gu4VlanPlatformLearningMode
#define VLAN_SUBNET_GLOBAL_OPTION()  gu4SubnetGlobalOption

/*CLI*/
#define VLAN_DYN_MAX_CMDS 9
#define VLAN_AUDIT_FILE_ACTIVE "/tmp/vlan_output_file_active"
#define VLAN_AUDIT_FILE_STDBY "/tmp/vlan_output_file_stdby"
#define VLAN_CLI_EOF                  2
#define VLAN_CLI_NO_EOF                 1
#define VLAN_CLI_RDONLY               OSIX_FILE_RO
#define VLAN_CLI_WRONLY               OSIX_FILE_WO
#define VLAN_CLI_MAX_GROUPS_LINE_LEN  200

/* For VlanCliAddFidListScanAndPrint () */
#define VLAN_SCAN_PRINT_FDB_TABLE              1
#define VLAN_SCAN_PRINT_STUCAST_TABLE          2
#define VLAN_DOT1D_SCAN_PRINT_STUCAST_TABLE    3
#define VLAN_DOT1D_SCAN_PRINT_STMCAST_TABLE    4
#define VLAN_MBSM_MAX_PORTS_CFG                2
#define VLAN_MEMORY_TYPE()      MEM_DEFAULT_MEMORY_TYPE

#define VLAN_MIN(x,y) (x < y ? x : y)

#define VLAN_GET_PRIORITY(pTag) ((UINT1)(((pTag)->u2Tag & 0xE000) >> VLAN_TAG_PRIORITY_SHIFT)) 

#define VLAN_IS_PRIORITY_ENABLED() \
        ((gpVlanContextInfo->VlanInfo.u1TrfClassEnabled == VLAN_SNMP_TRUE) ? VLAN_TRUE : VLAN_FALSE)

#define VLAN_IS_PORT_VALID(u2Port) \
        (((u2Port > VLAN_MAX_PORTS) || (u2Port == 0)) ? VLAN_FALSE : VLAN_TRUE)

#define VLAN_IS_FDB_ID_VALID(u4Fid) \
        (((u4Fid == 0) || (u4Fid > VLAN_MAX_VLAN_ID)) ? VLAN_FALSE : VLAN_TRUE)

#define VLAN_IS_CONTEXT_VALID(i4ContextId) \
        (((i4ContextId < 0) || ((UINT4)i4ContextId >= VLAN_SIZING_CONTEXT_COUNT)) ? VLAN_FALSE : VLAN_TRUE)

#define VLAN_IS_GVRP_ENABLED_ON_PORT(pPortEntry) \
        ((pPortEntry->u1GvrpStatus == VLAN_ENABLED) ? VLAN_TRUE : VLAN_FALSE)
   
#define VLAN_IS_GMRP_ENABLED_ON_PORT(pPortEntry) \
        ((pPortEntry->u1GmrpStatus == VLAN_ENABLED) ? VLAN_TRUE : VLAN_FALSE)

#define VLAN_IS_MVRP_ENABLED_ON_PORT(pPortEntry) \
        ((pPortEntry->u1MvrpStatus == VLAN_ENABLED) ? VLAN_TRUE : VLAN_FALSE)

#define VLAN_IS_MMRP_ENABLED_ON_PORT(pPortEntry) \
        ((pPortEntry->u1MmrpStatus == VLAN_ENABLED) ? VLAN_TRUE : VLAN_FALSE)

#define VLAN_DESTADDR_IS_GVRPADDR(pDestAddr) \
         ((VLAN_MEMCMP(pDestAddr,gGvrpAddress,ETHERNET_ADDR_SIZE)) ? \
          VLAN_FALSE : VLAN_TRUE)
   
#define VLAN_DESTADDR_IS_GMRPADDR(pDestAddr) \
        ((VLAN_MEMCMP(pDestAddr,gGmrpAddress,ETHERNET_ADDR_SIZE)) ? \
          VLAN_FALSE : VLAN_TRUE)

#define VLAN_IS_SW_STATS_ENABLED() \
        ((gu1SwStatsEnabled == VLAN_SNMP_TRUE)? VLAN_TRUE: VLAN_FALSE)

#define VLAN_IS_PORT_EGRESS_TPID_TYPE_VALID(u1EgressTPIDType)\
    (((u1EgressTPIDType == VLAN_PORT_EGRESS_PORTBASED ) \
    || (u1EgressTPIDType == VLAN_PORT_EGRESS_VLANBASED)) \
    ? VLAN_TRUE : VLAN_FALSE)

/* This macro is used to convert the component Id
 * to context ID */
#define VLAN_CONVERT_COMP_ID_TO_CXT_ID(u4CompId) ((u4CompId) - 1)

/* This macro is used to convert the contextId 
 * to component Id*/
#define VLAN_CONVERT_CXT_ID_TO_COMP_ID(u4ContextId) ((u4ContextId) + 1)

#define VLAN_IS_VALID_COMP_ID(u4CompId) \
    (((u4CompId == VLAN_ZERO) || (u4CompId > (VLAN_MAX_CONTEXTS + 1) )) ? VLAN_FALSE : VLAN_TRUE)

#define VLAN_IS_ENH_FILTERING_ENABLED() \
        ((VLAN_CURR_CONTEXT_PTR()->u1ApplyEnhancedFilteringCriteria == VLAN_SNMP_TRUE)? VLAN_TRUE: VLAN_FALSE)

#define VLAN_DESTADDR_IS_MVRPADDR(pDestAddr) \
    ((VLAN_MEMCMP(pDestAddr,gMvrpAddress,ETHERNET_ADDR_SIZE)) ? \
                   VLAN_FALSE : VLAN_TRUE)

#define VLAN_DESTADDR_IS_MMRPADDR(pDestAddr) \
    ((VLAN_MEMCMP(pDestAddr,gMmrpAddress,ETHERNET_ADDR_SIZE)) ? \
               VLAN_FALSE : VLAN_TRUE)

         /* Checks whether the address is RSTP/MSTP destination address */
#define VLAN_DESTADDR_IS_STPADDR(pAddr) \
         (((VLAN_MEMCMP(pAddr,gReservedAddress,(ETHERNET_ADDR_SIZE-1)) == 0) \
           &&(pAddr[5] == 0)) ? \
          VLAN_TRUE : VLAN_FALSE)
         /*
          * This macro only checks for the GARP Block address between the range 
          * 01:80:c2:00:00:20 - 01:80:c2:00:00:2F 
          */
#define VLAN_IS_GARP_BLOCK_ADDRESS(pAddr) \
         (((VLAN_MEMCMP(pAddr,gReservedAddress,(ETHERNET_ADDR_SIZE-1)) == 0) \
           &&(pAddr[5] >= 0x20) && (pAddr[5] <= 0x2F)) ?\
          VLAN_TRUE : VLAN_FALSE)
 
#define VLAN_IS_MRP_BLOCK_ADDRESS(pAddr) \
         (((VLAN_MEMCMP(pAddr,gReservedAddress,(ETHERNET_ADDR_SIZE-1)) == 0) \
           &&(pAddr[5] >= 0x20) && (pAddr[5] <= 0x2F)) ?\
          VLAN_TRUE : VLAN_FALSE)

#define VLAN_IS_MCASTADDR(pMacAddr) \
    ((FS_UTIL_IS_MCAST_MAC(pMacAddr) == OSIX_TRUE) ? VLAN_TRUE : VLAN_FALSE)
       
#define VLAN_CPY_MAC_ADDR(pMacAddr1, pMacAddr2) \
        MEMCPY(pMacAddr1,pMacAddr2,ETHERNET_ADDR_SIZE) 
        
#define VLAN_ARE_MAC_ADDR_EQUAL(pMacAddr1, pMacAddr2) \
        ((VLAN_MEMCMP(pMacAddr1,pMacAddr2,ETHERNET_ADDR_SIZE)) ? \
          VLAN_FALSE : VLAN_TRUE)

#define VLAN_CPY_SUBNET_ADDR(SubnetAddr1, SubnetAddr2) \
        (SubnetAddr1 = SubnetAddr2);
   
#define VLAN_ARE_SUBNET_ADDR_EQUAL(SubnetAddr1, SubnetAddr2) \
        ((SubnetAddr1 == SubnetAddr2) ? VLAN_TRUE : VLAN_FALSE)

#define VLAN_GET_TRF_CLASS(u2Port, u1Pri) \
   gpVlanContextInfo->apVlanPortEntry[u2Port]->au1TrfClassMap[u1Pri]  

#define VLAN_LEARNING_MODE()   gpVlanContextInfo->VlanInfo.u1VlanLearningType
   /* 
    * This macro is used to scan thru the list of active ports.
    * This function starts scanning from START port index (gpVlanContextInfo->u2VlanStartPortInd).
    * Dont use this macro in Get NEXT routines.
    */
#define VLAN_SCAN_PORT_TABLE(u2Port)    \
   for (u2Port = gpVlanContextInfo->u2VlanStartPortInd; \
        u2Port <  VLAN_INVALID_PORT_INDEX; \
        u2Port = VLAN_GET_NEXT_PORT_INDEX (u2Port))

#define VLAN_START_VLAN_INDEX() gpVlanContextInfo->VlanStartVlanInd
#define VLAN_NUM_VID_TRANS_ENTS()  (gpVlanContextInfo->u2NumVidTransEntries)
   /* 
    * This macro is used to scan thru the list of active vlans.
    * This function starts scanning from START Vlan index (gpVlanContextInfo->VlanStartVlanInd).
    * As storage is done based on the sorted order, this macro can be
    * used for the GET NEXT routines
    */
#define VLAN_SCAN_VLAN_TABLE(VlanInd)    \
   for (VlanInd = gpVlanContextInfo->VlanStartVlanInd; \
        VlanInd <  VLAN_INVALID_VLAN_INDEX; \
        VlanInd = VLAN_GET_NEXT_VLAN_INDEX (VlanInd))

    /* 
     * This macro is used to scan thru the list of active vlans starting
     * from the given VLAN ID - VlanStartId
     * 
     * Before calling this macro VlanStartId should have been validated
     * to contain a non NULL entry.
     */

#define VLAN_SCAN_VLAN_TABLE_FROM_MID(VlanStartId, VlanId)    \
   for (VlanId = VlanStartId; \
        VlanId != VLAN_INVALID_VLAN_INDEX; \
        VlanId = VLAN_GET_NEXT_VLAN_INDEX (VlanId))

#define VLAN_IS_PORT_CHANNEL(pPortEntry) \
    ((pPortEntry->u1IfType == CFA_LAGG) ? VLAN_TRUE : VLAN_FALSE)

   /* Macros for accessing the port bit array */

   /* 
    * This macro is used to find whether the specified port u2Port is
    * a member of the specified port list.
    */
   /* Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port.*/
#define VLAN_IS_MEMBER_PORT(au1PortArray, u2Port, u1Result) \
        {\
           UINT2 u2PortBytePos;\
           UINT2 u2PortBitPos;\
           u2PortBytePos = (UINT2)(u2Port / VLAN_PORTS_PER_BYTE);\
           u2PortBitPos  = (UINT2)(u2Port % VLAN_PORTS_PER_BYTE);\
    if (u2PortBitPos  == 0) {u2PortBytePos --;} \
           \
           if ((u2PortBytePos < CONTEXT_PORT_LIST_SIZE) && \
               (au1PortArray[u2PortBytePos] \
                & gau1PortBitMaskMap[u2PortBitPos]) != 0) {\
           \
              u1Result = VLAN_TRUE;\
           }\
           else {\
           \
              u1Result = VLAN_FALSE; \
           } \
        }

#define VLAN_IS_MEMBER_PORT_LIST(au1PortArray, u2Port, u1Result) \
        {\
           UINT2 u2PortBytePos;\
           UINT2 u2PortBitPos;\
           u2PortBytePos = (UINT2)(u2Port / VLAN_PORTS_PER_BYTE);\
           u2PortBitPos  = (UINT2)(u2Port % VLAN_PORTS_PER_BYTE);\
    if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
           if ((u2PortBytePos < BRG_PORT_LIST_SIZE) && \
               (au1PortArray[u2PortBytePos] \
                & gau1PortBitMaskMap[u2PortBitPos]) != 0) {\
           \
              u1Result = VLAN_TRUE;\
           }\
           else {\
           \
              u1Result = VLAN_FALSE; \
           } \
        }

    /* This macro adds u2Port as a member port in the list pointed by
     * au1PortArray
     */

 /* Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port.*/
#define VLAN_SET_MEMBER_PORT(au1PortArray, u2Port) \
           {\
              UINT2 u2PortBytePos;\
              UINT2 u2PortBitPos;\
              u2PortBytePos = (UINT2)(u2Port / VLAN_PORTS_PER_BYTE);\
              u2PortBitPos  = (UINT2)(u2Port % VLAN_PORTS_PER_BYTE);\
       if (u2PortBitPos  == 0) {u2PortBytePos --;} \
           \
               if (u2PortBytePos < CONTEXT_PORT_LIST_SIZE) { \
              au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                             | gau1PortBitMaskMap[u2PortBitPos]);\
           }}
/* Pseudo wire visibility */
#define VLAN_SET_MEMBER_PORT_LIST(au1PortArray, u4Port) \
           {\
              UINT4 u4PortBytePos;\
              UINT4 u4PortBitPos;\
              u4PortBytePos = (UINT4)(u4Port / VLAN_PORTS_PER_BYTE);\
              u4PortBitPos  = (UINT4)(u4Port % VLAN_PORTS_PER_BYTE);\
       if (u4PortBitPos  == 0) {u4PortBytePos --;} \
           \
               if (u4PortBytePos < BRG_PORT_LIST_SIZE) { \
              au1PortArray[u4PortBytePos] = (UINT1)(au1PortArray[u4PortBytePos] \
                             | gau1PortBitMaskMap[u4PortBitPos]);\
           }}

#define VLAN_SET_VLAN_LIST(au1VlanList, u4Vlan)\
        VLAN_SET_MEMBER_PORT(au1VlanList, u4Vlan)

/* 
 * The macro VLAN_RESET_MEMBER_PORT (), removes u2Port from the member 
 * list of au1PortArray. 
 * Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port.
 */
#define VLAN_RESET_MEMBER_PORT(au1PortArray, u2Port) \
              {\
                 UINT2 u2PortBytePos;\
                 UINT2 u2PortBitPos;\
                 u2PortBytePos = (UINT2)(u2Port / VLAN_PORTS_PER_BYTE);\
                 u2PortBitPos  = (UINT2)(u2Port % VLAN_PORTS_PER_BYTE);\
   if (u2PortBitPos  == 0) {u2PortBytePos --;} \
                 \
                  if (u2PortBytePos < CONTEXT_PORT_LIST_SIZE) { \
                 au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                               & ~gau1PortBitMaskMap[u2PortBitPos]);\
              }}

/* new macro added for cfa index based array */
/* Pseudo wire visibility */
#define VLAN_RESET_MEMBER_PORT_LIST(au1PortArray, u4Port) \
              {\
                 UINT4 u4PortBytePos;\
                 UINT4 u4PortBitPos;\
                 u4PortBytePos = (UINT4)(u4Port / VLAN_PORTS_PER_BYTE);\
                 u4PortBitPos  = (UINT4)(u4Port % VLAN_PORTS_PER_BYTE);\
   if (u4PortBitPos  == 0) {u4PortBytePos -= 1;} \
                 \
                  if (u4PortBytePos < BRG_PORT_LIST_SIZE) { \
                 au1PortArray[u4PortBytePos] = (UINT1)(au1PortArray[u4PortBytePos] \
                               & ~gau1PortBitMaskMap[u4PortBitPos]);\
              }}
/*
 * The macro VLAN_IS_PORT_ONLY_MEMBER () checks if the given Port
 * is the ONLY member Port of the Port List. It MUST be called only 
 * if __u2Port is a member port of __PortList. If the macro is called
 * without the Port being a member, then the Port will be added as 
 * a member to the Portlist after execution of the macro.
 */
#define VLAN_IS_PORT_ONLY_MEMBER(__PortList, __u2Port, __u1Result)   \
        { \
            VLAN_RESET_MEMBER_PORT ((__PortList), (__u2Port)); \
            \
            if (MEMCMP ((__PortList), gNullPortList, sizeof (tLocalPortList)) == 0) \
            { \
                VLAN_SET_MEMBER_PORT ((__PortList), (__u2Port)); \
                __u1Result = VLAN_TRUE; \
            } \
            else \
            { \
                VLAN_SET_MEMBER_PORT ((__PortList), (__u2Port)); \
                __u1Result = VLAN_FALSE; \
            } \
        }

    /* Adds the port list au1List2 to au1List1 - ie adds members of 
     * au1List2 to au1List1*/             
#define VLAN_ADD_PORT_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < VLAN_PORT_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
                 }\
              }
              
             
/* au1List1 = au1List1 & au1List2 */
#define VLAN_AND_PORT_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < VLAN_PORT_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] &= au1List2[u2ByteIndex];\
                 }\
              }

/* au1ResultList = au1List1 ^ au1List2 */
#define VLAN_XOR_PORT_LIST(au1ResultList, au1List1, au1List2) \
{\
    UINT2 u2ByteIndex;\
    \
    for (u2ByteIndex = 0;\
            u2ByteIndex < VLAN_PORT_LIST_SIZE;\
            u2ByteIndex++) {\
        au1ResultList[u2ByteIndex] = au1List1[u2ByteIndex] ^ au1List2[u2ByteIndex];\
    }\
}

#define  VLAN_IS_NULL_PORTLIST(au1List) \
         ((MEMCMP (au1List, gNullPortList, sizeof (gNullPortList)) == 0) \
          ? VLAN_TRUE : VLAN_FALSE)

/* MACROS to access Global structures */
#define VLAN_GET_FID_ENTRY(index) gpVlanContextInfo->apVlanFidTable[index]              

#define VLAN_GET_FDB_ENTRY(fidindex, hashindex) \
        gpVlanContextInfo->apVlanFidTable[fidindex]->UcastHashTbl[hashindex]
              
#define VLAN_GET_CURR_ENTRY(index)    gpVlanContextInfo->apVlanCurrTable[index]
#define VLAN_GET_NEXT_VLAN_INDEX(index)\
 (gpVlanContextInfo->apVlanCurrTable[index]->u2NextVlanInd)
              
#define VLAN_GET_MAC_MAP_ENTRY(hashindex) \
gpVlanContextInfo->VlanMacMapTable[hashindex]
              
#define VLAN_GET_PORT_ENTRY(portindex)    \
gpVlanContextInfo->apVlanPortEntry[portindex]

#define VLAN_GET_BASE_PORT_ENTRY(portindex)    \
gpVlanContextInfo->apVlanBasePortEntry[portindex]


#define VLAN_GET_NEXT_PORT_INDEX(index) \
   gpVlanContextInfo->apVlanPortEntry[index]->u2NextPortInd
#define VLAN_PORT_PORT_PROTOCOL_BASED(portindex) \
   gpVlanContextInfo->apVlanPortEntry[portindex]->u1PortProtoBasedClassification
#define VLAN_PORT_MAC_BASED(portindex) \
             gpVlanContextInfo->apVlanPortEntry[portindex]->u1MacBasedClassification
#define VLAN_PORT_PROTO_BASED gpVlanContextInfo->VlanInfo.u1PortProtoBasedClassification

#define VLAN_PORT_SUBNET_BASED(portindex) \
((VLAN_SUBNET_GLOBAL_OPTION() == VLAN_TRUE) ? VLAN_SUBNET_BASED : (gpVlanContextInfo->apVlanPortEntry[portindex]->u1SubnetBasedClassification)) 

#define VLAN_SET_PORT_SUBNET_BASED(portindex,u2val) \
{                                                   \
    if(VLAN_SUBNET_GLOBAL_OPTION() == VLAN_TRUE)    \
    {                                               \
        VLAN_SUBNET_BASED = u2val;                  \
    }                                               \
    else                                            \
    {                                               \
        (gpVlanContextInfo->apVlanPortEntry[portindex]->u1SubnetBasedClassification) = u2val;\
    }                                               \
}

#define VLAN_MAC_BASED gpVlanContextInfo->VlanInfo.u1MacBasedClassification 
#define VLAN_PROTOCOL_GROUP_TBL  gpVlanContextInfo->VlanInfo.pProtGrpDb
#define VLAN_SUBNET_BASED gpVlanContextInfo->VlanInfo.u1SubnetBasedClassification 
/*Userdefined TPID allowable for a Port/EgressVlan */
#define VLAN_PORT_USER_DEFINED_TPID \
        gpVlanContextInfo->u2UserDefinedTPID

#define VLAN_ENET_IS_TYPE(u2Type)   (u2Type > 1500) 

#define VLAN_IS_ETHER_TYPE_VALID(u2EtherType) \
            (((u2EtherType < 1) || (u2EtherType > 65535)) ? VLAN_FALSE : VLAN_TRUE)
                 
/* VLAN port stats accessing macros */
#ifdef SYS_HC_WANTED        
#define VLAN_INC_IN_FRAMES(pPortStats) \
{ \
        (pPortStats)->u4InFrames++; \
        if ((pPortStats)->u4InFrames == 0xFFFFFFFF) {\
           (pPortStats)->u4InOverflow++; \
              (pPortStats)->u4InFrames = 0; \
        }\
}

#define VLAN_INC_OUT_FRAMES(pPortStats) \
{\
        (pPortStats)->u4OutFrames++; \
        if ((pPortStats)->u4OutFrames == 0xFFFFFFFF) {\
           (pPortStats)->u4OutOverflow++; \
              (pPortStats)->u4OutFrames = 0; \
        }\
}

#define VLAN_INC_IN_DISCARDS(pPortStats) \
{\
        (pPortStats)->u4InDiscards++; \
        if ((pPortStats)->u4InDiscards == 0xFFFFFFFF) {\
           (pPortStats)->u4InOverflowDiscards++; \
              (pPortStats)->u4InDiscards = 0; \
        }\
}
#else
#define VLAN_INC_IN_FRAMES(pPortStats)   (pPortStats)->u4InFrames++

#define VLAN_INC_OUT_FRAMES(pPortStats)  (pPortStats)->u4OutFrames++

#define VLAN_INC_IN_DISCARDS(pPortStats) (pPortStats)->u4InDiscards++
        
#endif
#define VLAN_INC_IN_UCAST(pVlanCounters) \
{\
        (pVlanCounters)->u4RxUcastCnt++; \
        if ((pVlanCounters)->u4RxUcastCnt == VLAN_MAX_COUNTER_SIZE) {\
              (pVlanCounters)->u4RxUcastCnt = 0; \
        }\
}

#define VLAN_INC_IN_MCAST_BCAST(pVlanCounters) \
{\
        (pVlanCounters)->u4RxMcastBcastCnt++; \
        if ((pVlanCounters)->u4RxMcastBcastCnt == VLAN_MAX_COUNTER_SIZE) {\
              (pVlanCounters)->u4RxMcastBcastCnt = 0; \
        }\
}

#define VLAN_INC_OUT_UNKNOWN_UCAST(pVlanCounters) \
{\
        (pVlanCounters)->u4TxUnknUcastCnt++; \
        if ((pVlanCounters)->u4TxUnknUcastCnt == VLAN_MAX_COUNTER_SIZE) {\
              (pVlanCounters)->u4TxUnknUcastCnt = 0; \
        }\
}

#define VLAN_INC_OUT_UCAST(pVlanCounters) \
{\
        (pVlanCounters)->u4TxUcastCnt++; \
        if ((pVlanCounters)->u4TxUcastCnt == VLAN_MAX_COUNTER_SIZE) {\
              (pVlanCounters)->u4TxUcastCnt = 0; \
        }\
}

#define VLAN_INC_OUT_BCAST(pVlanCounters) \
{\
        (pVlanCounters)->u4TxBcastCnt++; \
        if ((pVlanCounters)->u4TxBcastCnt == VLAN_MAX_COUNTER_SIZE) {\
              (pVlanCounters)->u4TxBcastCnt = 0; \
        }\
}


#define VLAN_MODULE_STATUS()   gau1VlanStatus[gpVlanContextInfo->u4ContextId]
#define VLAN_MODULE_STATUS_FOR_CONTEXT(u4CtxId)\
 ((gu1IsVlanInitialised == VLAN_FALSE)?VLAN_DISABLED:gau1VlanStatus[u4CtxId])

#define VLAN_SYSTEM_CONTROL_FOR_CONTEXT(u4CtxId)\
 ((gu1IsVlanInitialised == VLAN_FALSE)?VLAN_SNMP_TRUE:gau1VlanShutDownStatus[u4CtxId])

#define VLAN_SYSTEM_CONTROL()\
 gau1VlanShutDownStatus[gpVlanContextInfo->u4ContextId]

#define VLAN_BASE_BRIDGE_MODE()          gpVlanContextInfo->u4BaseBridgeMode
#define VLAN_BRIDGE_MODE()               gpVlanContextInfo->VlanInfo.u4BridgeMode
#define VLAN_HW_ING_TAG_SUPPORT()          gpVlanContextInfo->VlanInfo.u1HwIngTagSupport
#define VLAN_HW_EG_UNTAG_SUPPORT()         gpVlanContextInfo->VlanInfo.u1HwEgUnTagSupport
#define VLAN_TUNNEL_BPDU_PRIORITY()      gpVlanContextInfo->VlanInfo.u1L2BpduPriority
/* For accessing tunnel info on a port */
#define VLAN_TUNNEL_STATUS(pPortEntry)   (pPortEntry)->u1TunnelStatus

#define VLAN_IS_TUNNEL_ENABLED(pPortEntry) \
        ((VLAN_BRIDGE_MODE() != VLAN_PROVIDER_BRIDGE_MODE) ? \
    (((VLAN_DOT1X_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_DOT1X_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD) ||\
      (VLAN_LACP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_LACP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD) ||\
      (VLAN_IGMP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_IGMP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD) ||\
      (VLAN_STP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_STP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD) ||\
      (VLAN_GVRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_GVRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD) ||\
      (VLAN_GMRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_GMRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD) ||\
      (VLAN_MMRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD) ||\
      (VLAN_MVRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD)||\
      (VLAN_MVRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_MMRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_ELMI_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_ELMI_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD) ||\
      (VLAN_EOAM_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_EOAM_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD) ||\
      (VLAN_ECFM_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_ECFM_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD) ||\
      (VLAN_LLDP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_LLDP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_DISCARD)) ? \
       VLAN_TRUE: VLAN_FALSE): \
     ((VLAN_TUNNEL_STATUS(pPortEntry) == VLAN_ENABLED)? VLAN_TRUE: \
       VLAN_FALSE))\


#define VLAN_IS_TUNNEL_ENABLED_ON_PORT(pPortEntry) \
        ((VLAN_BRIDGE_MODE() != VLAN_PROVIDER_BRIDGE_MODE) ? \
    (((VLAN_DOT1X_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_LACP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_IGMP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_STP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_GVRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_GMRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_MVRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_MMRP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_ELMI_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_EOAM_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_ECFM_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||\
      (VLAN_LLDP_TUNNEL_STATUS(pPortEntry) == VLAN_TUNNEL_PROTOCOL_TUNNEL)) ? \
       VLAN_TRUE: VLAN_FALSE): \
     ((VLAN_TUNNEL_STATUS(pPortEntry) == VLAN_ENABLED)? VLAN_TRUE: \
       VLAN_FALSE))\


#define VLAN_GET_TUNNEL_PROTOCOL_ENTRY(pPortEntry) \
                      (&(pPortEntry)->L2ProtoTnlInfo)

#define VLAN_DOT1X_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1Dot1xTunnelStatus
#define VLAN_LACP_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1LacpTunnelStatus
#define VLAN_STP_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1StpTunnelStatus
#define VLAN_GVRP_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1GvrpTunnelStatus
#define VLAN_GMRP_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1GmrpTunnelStatus
#define VLAN_IGMP_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1IgmpTunnelStatus
#define VLAN_MVRP_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1MvrpTunnelStatus
#define VLAN_MMRP_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1MmrpTunnelStatus
#define VLAN_ELMI_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1ElmiTunnelStatus
#define VLAN_LLDP_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1LldpTunnelStatus
#define VLAN_ECFM_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1EcfmTunnelStatus
#define VLAN_EOAM_TUNNEL_STATUS(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoInfo.u1EoamTunnelStatus


#define VLAN_DOT1X_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1Dot1xTunnelStatus
#define VLAN_LACP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1LacpTunnelStatus
#define VLAN_STP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1StpTunnelStatus
#define VLAN_GVRP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1GvrpTunnelStatus
#define VLAN_GMRP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1GmrpTunnelStatus
#define VLAN_IGMP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1IgmpTunnelStatus
#define VLAN_MVRP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1MvrpTunnelStatus
#define VLAN_MMRP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1MmrpTunnelStatus
#define VLAN_ELMI_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1ElmiTunnelStatus
#define VLAN_LLDP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1LldpTunnelStatus
#define VLAN_ECFM_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1EcfmTunnelStatus
#define VLAN_EOAM_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1EoamTunnelStatus
#define VLAN_PTP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1PtpTunnelStatus
#define VLAN_OTHER_TUNNEL_STATUS_PER_VLAN(pVlanInfo, index) \
    (pVlanInfo)->VlanTunnelL2ProtoInfo.u1OtherTunnelStatus[index]

#define VLAN_TUNNEL_STP_BPDUS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4StpBpdusRecvd
#define VLAN_TUNNEL_STP_BPDUS_TX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4StpBpdusSent
#define VLAN_INCR_TUNNEL_STP_BPDUS_RX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4StpBpdusRecvd)++
#define VLAN_INCR_TUNNEL_STP_BPDUS_TX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4StpBpdusSent)++

#define VLAN_TUNNEL_GVRP_PDUS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4GvrpPktsRecvd
#define VLAN_TUNNEL_GVRP_PDUS_TX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4GvrpPktsSent
#define VLAN_INCR_TUNNEL_GVRP_PDUS_RX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4GvrpPktsRecvd)++
#define VLAN_INCR_TUNNEL_GVRP_PDUS_TX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4GvrpPktsSent)++

#define VLAN_TUNNEL_MVRP_PDUS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4MvrpPktsRecvd
#define VLAN_TUNNEL_MVRP_PDUS_TX(pPortEntry) \
        (pPortEntry)->VlanTunnelL2ProtoStats.u4MvrpPktsSent
#define VLAN_INCR_TUNNEL_MVRP_PDUS_RX(pPortEntry) \
            ((pPortEntry)->VlanTunnelL2ProtoStats.u4MvrpPktsRecvd)++
#define VLAN_INCR_TUNNEL_MVRP_PDUS_TX(pPortEntry) \
((pPortEntry)->VlanTunnelL2ProtoStats.u4MvrpPktsSent)++

#define VLAN_TUNNEL_IGMP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4IgmpPktsRecvd
#define VLAN_TUNNEL_IGMP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4IgmpPktsSent
#define VLAN_INCR_TUNNEL_IGMP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4IgmpPktsRecvd)++
#define VLAN_INCR_TUNNEL_IGMP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4IgmpPktsSent)++

#define VLAN_TUNNEL_GMRP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4GmrpPktsRecvd
#define VLAN_TUNNEL_GMRP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4GmrpPktsSent
#define VLAN_INCR_TUNNEL_GMRP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4GmrpPktsRecvd)++
#define VLAN_INCR_TUNNEL_GMRP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4GmrpPktsSent)++

#define VLAN_TUNNEL_MMRP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4MmrpPktsRecvd
#define VLAN_TUNNEL_MMRP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4MmrpPktsSent
#define VLAN_INCR_TUNNEL_MMRP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4MmrpPktsRecvd)++
#define VLAN_INCR_TUNNEL_MMRP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4MmrpPktsSent)++

#define VLAN_TUNNEL_LACP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4LacpPktsRecvd
#define VLAN_TUNNEL_LACP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4LacpPktsSent
#define VLAN_INCR_TUNNEL_LACP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4LacpPktsRecvd)++
#define VLAN_INCR_TUNNEL_LACP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4LacpPktsSent)++

#define VLAN_TUNNEL_DOT1X_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4Dot1xPktsRecvd
#define VLAN_TUNNEL_DOT1X_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4Dot1xPktsSent
#define VLAN_INCR_TUNNEL_DOT1X_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4Dot1xPktsRecvd)++
#define VLAN_INCR_TUNNEL_DOT1X_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4Dot1xPktsSent)++

#define VLAN_TUNNEL_ECFM_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4EcfmPktsRecvd
#define VLAN_TUNNEL_ECFM_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4EcfmPktsSent
#define VLAN_INCR_TUNNEL_ECFM_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4EcfmPktsRecvd)++
#define VLAN_INCR_TUNNEL_ECFM_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4EcfmPktsSent)++

#define VLAN_TUNNEL_EOAM_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4EoamPktsRecvd
#define VLAN_TUNNEL_EOAM_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4EoamPktsSent
#define VLAN_INCR_TUNNEL_EOAM_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4EoamPktsRecvd)++
#define VLAN_INCR_TUNNEL_EOAM_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4EoamPktsSent)++

#define VLAN_TUNNEL_LLDP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4LldpPktsRecvd
#define VLAN_TUNNEL_LLDP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4LldpPktsSent
#define VLAN_INCR_TUNNEL_LLDP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4LldpPktsRecvd)++
#define VLAN_INCR_TUNNEL_LLDP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4LldpPktsSent)++

#define VLAN_TUNNEL_ELMI_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4ElmiPktsRecvd
#define VLAN_TUNNEL_ELMI_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanTunnelL2ProtoStats.u4ElmiPktsSent
#define VLAN_INCR_TUNNEL_ELMI_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4ElmiPktsRecvd)++
#define VLAN_INCR_TUNNEL_ELMI_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanTunnelL2ProtoStats.u4ElmiPktsSent)++


#define VLAN_DISCARD_DOT1X_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscDot1xPktsRx
#define VLAN_DISCARD_DOT1X_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscDot1xPktsTx

#define VLAN_INCR_DISCARD_DOT1X_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscDot1xPktsRx)++
#define VLAN_INCR_DISCARD_DOT1X_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscDot1xPktsTx)++

#define VLAN_DISCARD_LACP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscLacpPktsRx
#define VLAN_DISCARD_LACP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscLacpPktsTx

#define VLAN_INCR_DISCARD_LACP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscLacpPktsRx)++
#define VLAN_INCR_DISCARD_LACP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscLacpPktsTx)++

#define VLAN_DISCARD_STP_BPDUS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscStpBpdusRx
#define VLAN_DISCARD_STP_BPDUS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscStpBpdusTx

#define VLAN_INCR_DISCARD_STP_BPDUS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscStpBpdusRx)++
#define VLAN_INCR_DISCARD_STP_BPDUS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscStpBpdusTx)++

#define VLAN_DISCARD_GVRP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscGvrpPktsRx
#define VLAN_DISCARD_GVRP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscGvrpPktsTx

#define VLAN_INCR_DISCARD_GVRP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscGvrpPktsRx)++
#define VLAN_INCR_DISCARD_GVRP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscGvrpPktsTx)++

#define VLAN_DISCARD_MVRP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscMvrpPktsRx
#define VLAN_DISCARD_MVRP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscMvrpPktsTx

#define VLAN_INCR_DISCARD_MVRP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscMvrpPktsRx)++
#define VLAN_INCR_DISCARD_MVRP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscMvrpPktsTx)++

#define VLAN_DISCARD_GMRP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscGmrpPktsRx
#define VLAN_DISCARD_GMRP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscGmrpPktsTx

#define VLAN_INCR_DISCARD_GMRP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscGmrpPktsRx)++
#define VLAN_INCR_DISCARD_GMRP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscGmrpPktsTx)++

#define VLAN_DISCARD_MMRP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscMmrpPktsRx
#define VLAN_DISCARD_MMRP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscMmrpPktsTx

#define VLAN_INCR_DISCARD_MMRP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscMmrpPktsRx)++
#define VLAN_INCR_DISCARD_MMRP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscMmrpPktsTx)++

#define VLAN_DISCARD_IGMP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscIgmpPktsRx
#define VLAN_DISCARD_IGMP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscIgmpPktsTx

#define VLAN_INCR_DISCARD_IGMP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscIgmpPktsRx)++
#define VLAN_INCR_DISCARD_IGMP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscIgmpPktsTx)++

#define VLAN_DISCARD_ELMI_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscElmiPktsRx
#define VLAN_DISCARD_ELMI_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscElmiPktsTx

#define VLAN_INCR_DISCARD_ELMI_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscElmiPktsRx)++
#define VLAN_INCR_DISCARD_ELMI_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscElmiPktsTx)++

#define VLAN_DISCARD_LLDP_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscLldpPktsRx
#define VLAN_DISCARD_LLDP_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscLldpPktsTx

#define VLAN_INCR_DISCARD_LLDP_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscLldpPktsRx)++
#define VLAN_INCR_DISCARD_LLDP_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscLldpPktsTx)++

#define VLAN_DISCARD_ECFM_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscEcfmPktsRx
#define VLAN_DISCARD_ECFM_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscEcfmPktsTx

#define VLAN_INCR_DISCARD_ECFM_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscEcfmPktsRx)++
#define VLAN_INCR_DISCARD_ECFM_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscEcfmPktsTx)++

#define VLAN_DISCARD_EOAM_PKTS_RX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscEoamPktsRx
#define VLAN_DISCARD_EOAM_PKTS_TX(pPortEntry) \
    (pPortEntry)->VlanDiscardL2ProtoStats.u4DiscEoamPktsTx

#define VLAN_INCR_DISCARD_EOAM_PKTS_RX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscEoamPktsRx)++
#define VLAN_INCR_DISCARD_EOAM_PKTS_TX(pPortEntry) \
    ((pPortEntry)->VlanDiscardL2ProtoStats.u4DiscEoamPktsTx)++

#define VLAN_TUNNEL_STP_BPDUS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4StpBpdusRecvd
#define VLAN_TUNNEL_STP_BPDUS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4StpBpdusSent
#define VLAN_INCR_TUNNEL_STP_BPDUS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4StpBpdusRecvd)++
#define VLAN_INCR_TUNNEL_STP_BPDUS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4StpBpdusSent)++

#define VLAN_TUNNEL_GVRP_PDUS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4GvrpPktsRecvd
#define VLAN_TUNNEL_GVRP_PDUS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4GvrpPktsSent
#define VLAN_INCR_TUNNEL_GVRP_PDUS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4GvrpPktsRecvd)++
#define VLAN_INCR_TUNNEL_GVRP_PDUS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4GvrpPktsSent)++

#define VLAN_TUNNEL_MVRP_PDUS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4MvrpPktsRecvd
#define VLAN_TUNNEL_MVRP_PDUS_TX_PER_VLAN(pVlanInfo) \
        (pVlanInfo)->VlanTunnelL2ProtoStats.u4MvrpPktsSent
#define VLAN_INCR_TUNNEL_MVRP_PDUS_RX_PER_VLAN(pVlanInfo) \
      ((pVlanInfo)->VlanTunnelL2ProtoStats.u4MvrpPktsRecvd)++
#define VLAN_INCR_TUNNEL_MVRP_PDUS_TX_PER_VLAN(pVlanInfo) \
 ((pVlanInfo)->VlanTunnelL2ProtoStats.u4MvrpPktsSent)++

#define VLAN_TUNNEL_IGMP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4IgmpPktsRecvd
#define VLAN_TUNNEL_IGMP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4IgmpPktsSent
#define VLAN_INCR_TUNNEL_IGMP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4IgmpPktsRecvd)++
#define VLAN_INCR_TUNNEL_IGMP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4IgmpPktsSent)++

#define VLAN_TUNNEL_GMRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4GmrpPktsRecvd
#define VLAN_TUNNEL_GMRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4GmrpPktsSent
#define VLAN_INCR_TUNNEL_GMRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4GmrpPktsRecvd)++
#define VLAN_INCR_TUNNEL_GMRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4GmrpPktsSent)++

#define VLAN_TUNNEL_MMRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4MmrpPktsRecvd
#define VLAN_TUNNEL_MMRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4MmrpPktsSent
#define VLAN_INCR_TUNNEL_MMRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4MmrpPktsRecvd)++
#define VLAN_INCR_TUNNEL_MMRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4MmrpPktsSent)++

#define VLAN_TUNNEL_LACP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4LacpPktsRecvd
#define VLAN_TUNNEL_LACP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4LacpPktsSent
#define VLAN_INCR_TUNNEL_LACP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4LacpPktsRecvd)++
#define VLAN_INCR_TUNNEL_LACP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4LacpPktsSent)++

#define VLAN_TUNNEL_DOT1X_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4Dot1xPktsRecvd
#define VLAN_TUNNEL_DOT1X_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4Dot1xPktsSent
#define VLAN_INCR_TUNNEL_DOT1X_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4Dot1xPktsRecvd)++
#define VLAN_INCR_TUNNEL_DOT1X_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4Dot1xPktsSent)++

#define VLAN_TUNNEL_ECFM_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4EcfmPktsRecvd
#define VLAN_TUNNEL_ECFM_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4EcfmPktsSent
#define VLAN_INCR_TUNNEL_ECFM_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4EcfmPktsRecvd)++
#define VLAN_INCR_TUNNEL_ECFM_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4EcfmPktsSent)++

#define VLAN_TUNNEL_LLDP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4LldpPktsRecvd
#define VLAN_TUNNEL_LLDP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4LldpPktsSent
#define VLAN_INCR_TUNNEL_LLDP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4LldpPktsRecvd)++
#define VLAN_INCR_TUNNEL_LLDP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4LldpPktsSent)++

#define VLAN_TUNNEL_ELMI_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4ElmiPktsRecvd
#define VLAN_TUNNEL_ELMI_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4ElmiPktsSent
#define VLAN_INCR_TUNNEL_ELMI_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4ElmiPktsRecvd)++
#define VLAN_INCR_TUNNEL_ELMI_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4ElmiPktsSent)++

#define VLAN_TUNNEL_EOAM_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4EoamPktsRecvd
#define VLAN_TUNNEL_EOAM_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4EoamPktsSent
#define VLAN_INCR_TUNNEL_EOAM_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4EoamPktsRecvd)++
#define VLAN_INCR_TUNNEL_EOAM_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4EoamPktsSent)++

#define VLAN_TUNNEL_PTP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4PtpPktsRecvd
#define VLAN_TUNNEL_PTP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanTunnelL2ProtoStats.u4PtpPktsSent
#define VLAN_INCR_TUNNEL_PTP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4PtpPktsRecvd)++
#define VLAN_INCR_TUNNEL_PTP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanTunnelL2ProtoStats.u4PtpPktsSent)++

#define VLAN_DISCARD_DOT1X_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscDot1xPktsRx
#define VLAN_DISCARD_DOT1X_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscDot1xPktsTx

#define VLAN_INCR_DISCARD_DOT1X_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscDot1xPktsRx)++
#define VLAN_INCR_DISCARD_DOT1X_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscDot1xPktsTx)++

#define VLAN_DISCARD_LACP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscLacpPktsRx
#define VLAN_DISCARD_LACP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscLacpPktsTx

#define VLAN_INCR_DISCARD_LACP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscLacpPktsRx)++
#define VLAN_INCR_DISCARD_LACP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscLacpPktsTx)++

#define VLAN_DISCARD_STP_BPDUS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscStpBpdusRx
#define VLAN_DISCARD_STP_BPDUS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscStpBpdusTx

#define VLAN_INCR_DISCARD_STP_BPDUS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscStpBpdusRx)++
#define VLAN_INCR_DISCARD_STP_BPDUS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscStpBpdusTx)++

#define VLAN_DISCARD_GVRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscGvrpPktsRx
#define VLAN_DISCARD_GVRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscGvrpPktsTx

#define VLAN_INCR_DISCARD_GVRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscGvrpPktsRx)++
#define VLAN_INCR_DISCARD_GVRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscGvrpPktsTx)++

#define VLAN_DISCARD_MVRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscMvrpPktsRx
#define VLAN_DISCARD_MVRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscMvrpPktsTx

#define VLAN_INCR_DISCARD_MVRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscMvrpPktsRx)++
#define VLAN_INCR_DISCARD_MVRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscMvrpPktsTx)++

#define VLAN_DISCARD_GMRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscGmrpPktsRx
#define VLAN_DISCARD_GMRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscGmrpPktsTx

#define VLAN_INCR_DISCARD_GMRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscGmrpPktsRx)++
#define VLAN_INCR_DISCARD_GMRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscGmrpPktsTx)++

#define VLAN_DISCARD_MMRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscMmrpPktsRx
#define VLAN_DISCARD_MMRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscMmrpPktsTx

#define VLAN_INCR_DISCARD_MMRP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscMmrpPktsRx)++
#define VLAN_INCR_DISCARD_MMRP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscMmrpPktsTx)++

#define VLAN_DISCARD_IGMP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscIgmpPktsRx
#define VLAN_DISCARD_IGMP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscIgmpPktsTx

#define VLAN_INCR_DISCARD_IGMP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscIgmpPktsRx)++
#define VLAN_INCR_DISCARD_IGMP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscIgmpPktsTx)++

#define VLAN_DISCARD_ELMI_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscElmiPktsRx
#define VLAN_DISCARD_ELMI_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscElmiPktsTx

#define VLAN_INCR_DISCARD_ELMI_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscElmiPktsRx)++
#define VLAN_INCR_DISCARD_ELMI_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscElmiPktsTx)++

#define VLAN_DISCARD_LLDP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscLldpPktsRx
#define VLAN_DISCARD_LLDP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscLldpPktsTx

#define VLAN_INCR_DISCARD_LLDP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscLldpPktsRx)++
#define VLAN_INCR_DISCARD_LLDP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscLldpPktsTx)++

#define VLAN_DISCARD_ECFM_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscEcfmPktsRx
#define VLAN_DISCARD_ECFM_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscEcfmPktsTx

#define VLAN_INCR_DISCARD_ECFM_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscEcfmPktsRx)++
#define VLAN_INCR_DISCARD_ECFM_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscEcfmPktsTx)++

#define VLAN_DISCARD_EOAM_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscEoamPktsRx
#define VLAN_DISCARD_EOAM_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscEoamPktsTx

#define VLAN_INCR_DISCARD_EOAM_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscEoamPktsRx)++
#define VLAN_INCR_DISCARD_EOAM_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscEoamPktsTx)++

#define VLAN_DISCARD_PTP_PKTS_RX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscPtpPktsRx
#define VLAN_DISCARD_PTP_PKTS_TX_PER_VLAN(pVlanInfo) \
    (pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscPtpPktsTx

#define VLAN_INCR_DISCARD_PTP_PKTS_RX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscPtpPktsRx)++
#define VLAN_INCR_DISCARD_PTP_PKTS_TX_PER_VLAN(pVlanInfo) \
    ((pVlanInfo)->VlanDiscardL2ProtoStats.u4DiscPtpPktsTx)++

#define VLAN_GET_PB_PORT_ENTRY(u2Port) \
   ((VLAN_GET_PORT_ENTRY(u2Port) != NULL) ? \
    (VLAN_GET_PORT_ENTRY(u2Port)->pVlanPbPortEntry) : NULL)

#define VLAN_PB_PORT_TYPE(u2port) VlanPbPortType(u2port) 

#define VLAN_PB_PORT_UNICAST_MAC_LIMIT(u2port) \
         VLAN_GET_PB_PORT_ENTRY(u2port)->u4MacLearningLimit

#define VLAN_PB_PORT_SERVICE_TYPE(u2port) \
         VLAN_GET_PB_PORT_ENTRY(u2port)->u1PortServiceType

/* Valid for both PB and PBB Bridge Mode */
#define VLAN_PB_802_1AD_AH_BRIDGE() \
   (((VLAN_BRIDGE_MODE () != VLAN_CUSTOMER_BRIDGE_MODE) && \
     (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_BRIDGE_MODE )) ? VLAN_TRUE : VLAN_FALSE)

/* Valid for PB Bridge Mode */
#define VLAN_PB_802_1AD_BRIDGE() \
   (((VLAN_BRIDGE_MODE () == VLAN_PROVIDER_CORE_BRIDGE_MODE) || \
     (VLAN_BRIDGE_MODE () == VLAN_PROVIDER_EDGE_BRIDGE_MODE)) ? VLAN_TRUE : VLAN_FALSE)

/* Valid for only PBB Bridge Mode */
#define VLAN_IS_802_1AH_BRIDGE() \
   (((VLAN_BRIDGE_MODE () != VLAN_CUSTOMER_BRIDGE_MODE) && \
     (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_BRIDGE_MODE ) && \
  (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_EDGE_BRIDGE_MODE ) && \
  (VLAN_BRIDGE_MODE () != VLAN_PROVIDER_CORE_BRIDGE_MODE ) && \
  (VLAN_BRIDGE_MODE () != VLAN_INVALID_BRIDGE_MODE )) ? VLAN_TRUE : VLAN_FALSE)


#define VLAN_PB_NETWORK_PORT(u2Port) \
   (((VLAN_PB_PORT_TYPE(u2Port) == VLAN_PROP_PROVIDER_NETWORK_PORT) ||\
    (VLAN_PB_PORT_TYPE(u2Port) == VLAN_PROVIDER_NETWORK_PORT) ||\
    (VLAN_PB_PORT_TYPE(u2Port) == VLAN_CNP_TAGGED_PORT))\
    ? VLAN_TRUE : VLAN_FALSE)

#define VLAN_PB_PROTO_TUNNEL_PORT(u2Port)\
   (((VLAN_PB_PORT_TYPE(u2Port) == VLAN_PROVIDER_NETWORK_PORT) ||\
    (VLAN_PB_PORT_TYPE(u2Port) == VLAN_CNP_TAGGED_PORT))\
    ? VLAN_FALSE : VLAN_TRUE)

#define VLAN_PB_CUSTOMER_PORT(u2Port) \
   (((VLAN_PB_PORT_TYPE(u2Port) == VLAN_PROP_CUSTOMER_EDGE_PORT) ||\
    (VLAN_PB_PORT_TYPE(u2Port) == VLAN_PROP_CUSTOMER_NETWORK_PORT) ||\
    (VLAN_PB_PORT_TYPE(u2Port) == VLAN_CUSTOMER_EDGE_PORT) ||\
    (VLAN_PB_PORT_TYPE(u2Port) == VLAN_CNP_PORTBASED_PORT))\
    ? VLAN_TRUE : VLAN_FALSE)


#define VLAN_INGRESS_VLAN_ETHER_TYPE(port) \
        VLAN_GET_PORT_ENTRY(port)->u2IngressEtherType
         
#define VLAN_EGRESS_VLAN_ETHER_TYPE(port) \
         VLAN_GET_PORT_ENTRY(port)->u2EgressEtherType

#define VLAN_PB_PORT_USE_DEI(port) VLAN_GET_PB_PORT_ENTRY(port)->u1UseDei

#define VLAN_QMSG_TYPE(pMsg) pMsg->u2MsgType

#define VLAN_TUNNEL_DOT1X_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderDot1xAddr
#define VLAN_TUNNEL_LACP_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderLacpAddr
#define VLAN_TUNNEL_STP_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderStpAddr
#define VLAN_TUNNEL_GVRP_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderGvrpAddr
#define VLAN_TUNNEL_GMRP_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderGmrpAddr
#define VLAN_TUNNEL_MVRP_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderMvrpAddr
#define VLAN_TUNNEL_MMRP_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderMmrpAddr
#define VLAN_TUNNEL_ELMI_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderElmiAddr
#define VLAN_TUNNEL_LLDP_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderLldpAddr
#define VLAN_TUNNEL_ECFM_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderEcfmAddr
#define VLAN_TUNNEL_EOAM_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderEoamAddr
#define VLAN_TUNNEL_IGMP_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderIgmpAddr
#define VLAN_TUNNEL_OTHER_ADDR() VLAN_CURR_CONTEXT_PTR()->PropProviderOtherAddr

/* For accessing Tunnel info on a vlan (Enable/Disable) */
#define VLAN_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
      (pVlanInfo)->u1TunnelStatus

/* Tunnel Mac address Per Vlan*/
#define VLAN_TUNNEL_DOT1X_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelDot1xAddr
#define VLAN_TUNNEL_LACP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelLacpAddr
#define VLAN_TUNNEL_STP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelStpAddr
#define VLAN_TUNNEL_GVRP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelGvrpAddr
#define VLAN_TUNNEL_GMRP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelGmrpAddr
#define VLAN_TUNNEL_IGMP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelIgmpAddr
#define VLAN_TUNNEL_MVRP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelMvrpAddr
#define VLAN_TUNNEL_MMRP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelMmrpAddr
#define VLAN_TUNNEL_ELMI_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelElmiAddr
#define VLAN_TUNNEL_LLDP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelLldpAddr
#define VLAN_TUNNEL_ECFM_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelEcfmAddr
#define VLAN_TUNNEL_EOAM_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelEoamAddr
#define VLAN_TUNNEL_PTP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelPtpAddr
#define VLAN_TUNNEL_OTHER_ADDR_PER_VLAN(pVlanInfo, index) \
     (pVlanInfo)->VlanL2ProtoTunnelMacAddr.ProviderTunnelOtherAddr[index]

/* Reserved Mac address per Vlan */
#define VLAN_RSVD_DOT1X_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdDot1xAddr
#define VLAN_RSVD_LACP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdLacpAddr
#define VLAN_RSVD_STP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdStpAddr
#define VLAN_RSVD_GVRP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdGvrpAddr
#define VLAN_RSVD_GMRP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdGmrpAddr
#define VLAN_RSVD_IGMP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdIgmpAddr
#define VLAN_RSVD_MVRP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdMvrpAddr
#define VLAN_RSVD_MMRP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdMmrpAddr
#define VLAN_RSVD_ELMI_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdElmiAddr
#define VLAN_RSVD_LLDP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdLldpAddr
#define VLAN_RSVD_ECFM_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdEcfmAddr
#define VLAN_RSVD_EOAM_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdEoamAddr
#define VLAN_RSVD_PTP_ADDR_PER_VLAN(pVlanInfo) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdPtpAddr
#define VLAN_RSVD_OTHER_ADDR_PER_VLAN(pVlanInfo, index) \
     (pVlanInfo)->VlanL2ProtoRsvdMacAddr.ProviderRsvdOtherAddr[index]

/* Macros for accessing the Vlan In Pkt Iffoe */
#define VLAN_IFMSG_SRC_ADDR(pVlanIf)     (pVlanIf)->SrcAddr
#define VLAN_IFMSG_DST_ADDR(pVlanIf)     (pVlanIf)->DestAddr
#define VLAN_IFMSG_PORT(pVlanIf)         (pVlanIf)->u2LocalPort
#define VLAN_IFMSG_LEN(pVlanIf)          (pVlanIf)->u2Length
#define VLAN_IFMSG_TIME_STAMP(pVlanIf)   (pVlanIf)->u4TimeStamp

#define VLAN_MODULE_ADMIN_STATUS() (gpVlanContextInfo->VlanInfo.u1ProtocolAdminStatus)

#define VLAN_GVRP_STATUS() (gpVlanContextInfo->VlanInfo.u1GvrpStatus)
#define VLAN_GMRP_STATUS() (gpVlanContextInfo->VlanInfo.u1GmrpStatus)

#define VLAN_MVRP_STATUS() (gpVlanContextInfo->VlanInfo.u1MvrpStatus)
#define VLAN_MMRP_STATUS() (gpVlanContextInfo->VlanInfo.u1MmrpStatus)

#define VLAN_SPLIT_AGEOUT_TIME(u4AgeOutInt) \
                           (u4AgeOutInt / VLAN_AGEING_SPLIT_INTERVAL);

#define VLAN_IS_MODULE_INITIALISED() \
        ((gu1IsVlanInitialised == VLAN_TRUE) ? VLAN_TRUE : VLAN_FALSE)

#define VLAN_CURR_CONTEXT_PTR()    gpVlanContextInfo
#define VLAN_CURR_CONTEXT_ID()     gpVlanContextInfo->u4ContextId
#define VLAN_CONTEXT_PTR(CtxId)    gapVlanContextInfo[CtxId]
#define VLAN_GLOBAL_PORT_RBTREE()  gVlanPortTable
#define VLAN_LOCAL_PORT_RBTREE()   (VLAN_CURR_CONTEXT_PTR ()->VlanPortList)
#define VLAN_CURR_CONTEXT_STR()     gpVlanContextInfo->au1ContextStr

#define VLAN_GET_PHY_PORT(u2Port) \
             ((u2Port == 0) || (u2Port > VLAN_MAX_PORTS)) ? 0 : ((VLAN_GET_PORT_ENTRY (u2Port))->u4PhyIfIndex)

#define VLAN_GET_IFINDEX(u2Port) \
             ((u2Port == 0) || (u2Port > VLAN_MAX_PORTS)) ? 0 :\
                    (VLAN_GET_PORT_ENTRY (u2Port) == 0 ) ? 0 : ((VLAN_GET_PORT_ENTRY (u2Port))->u4IfIndex)
    
#define VLAN_MEM_ALLOC             MEM_MALLOC
#define VLAN_MEM_FREE              MEM_FREE

#define VLAN_IS_VC_VALID(i4ContextId) \
            (((i4ContextId  < 0) || ((UINT4)i4ContextId >= VLAN_SIZING_CONTEXT_COUNT)) ? \
                  VLAN_FALSE :((gapVlanContextInfo[i4ContextId] != NULL) ? VLAN_TRUE : VLAN_FALSE))

#define VLAN_DEF_UNICAST_LEARNING_LIMIT \
        (VLAN_DEV_MAX_L2_TABLE_SIZE - FsVLANSizingParams[MAX_VLAN_ST_UCAST_ENTRIES_SIZING_ID].u4PreAllocatedUnits)

#define VLAN_DYNAMIC_UNICAST_SIZE \
        gpVlanContextInfo->VlanInfo.u4SwitchDynamicUnicastSize

#define VLAN_DYNAMIC_UNICAST_COUNT \
        gpVlanContextInfo->VlanInfo.u4SwitchDynamicUnicastCount

/* Current number of Static Unicast Entries configured */
#define VLAN_ST_UCAST_TABLE_COUNT \
        gpVlanContextInfo->VlanInfo.u4SwitchStaticUnicastCount

/* Current number of Static Multicast Entries configured */
#define VLAN_ST_MCAST_TABLE_COUNT \
        gpVlanContextInfo->VlanInfo.u4SwitchCurrentMulticastCount

#define VLAN_CONTROL_MAC_LEARNING_LIMIT(VlanId) \
        ((tVlanMacControl*) \
        (gpVlanContextInfo->apVlanCurrTable[VlanId]->pVlanMacControl))->\
        u4UnicastMacLearningLimit

#define VLAN_CONTROL_ADM_MAC_LEARNING_STATUS(VlanId) \
        ((tVlanMacControl*) \
        (gpVlanContextInfo->apVlanCurrTable[VlanId]->pVlanMacControl))->\
        u1AdminUnicastMacLearningStatus

#define VLAN_CONTROL_OPER_MAC_LEARNING_STATUS(VlanId) \
        ((tVlanMacControl*) \
        (gpVlanContextInfo->apVlanCurrTable[VlanId]->pVlanMacControl))->\
        u1OperationalUnicastMacLearningStatus

#define VLAN_GLOBAL_MAC_LEARNING_STATUS()  gpVlanContextInfo->u1GlobalMacLearningStatus

#define VLAN_CURR_CTX_ENHANCE_FILTERING_STATUS()  gpVlanContextInfo->u1ApplyEnhancedFilteringCriteria

#define VLAN_MISC_SIZE (VLAN_MAX_PORTS * sizeof (UINT4)) 

    /* If the Message IfIndex is zero, then the Packet is treated as it came 
     * from MPLS Module */
#define VLAN_IS_PKT_RCVD_FROM_MPLS(pVlanIf) \
           ((pVlanIf->u4IfIndex == 0)? VLAN_TRUE: VLAN_FALSE)

#define VLAN_TASK_ID            gVlanTaskInfo.VlanTaskId
#define VLAN_AUDIT_TASK_ID      gVlanRedTaskInfo.VlanAuditTaskId
#define VLAN_SEM_ID             gVlanTaskInfo.VlanSemId
#define VLAN_SHADOW_TBL_SEM_ID  gVlanTaskInfo.VlanShadowTblSemId

#ifdef PBBTE_PERFORM_TRACE_WANTED
#define VLAN_PERF_MARK_START_TIME()     gettimeofday(&gStartTime, NULL)
#define VLAN_PERF_MARK_END_TIME()      \
{\
    long long u4T1, u4T2;\
    gettimeofday(&gEndTime, NULL);\
    u4T1 = (gStartTime.tv_sec * 1000000 + gStartTime.tv_usec);\
    u4T2 = (gEndTime.tv_sec * 1000000 + gEndTime.tv_usec);\
    gu4TimeTaken += (u4T2 - u4T1);\
}
#else
#define VLAN_PERF_MARK_START_TIME()
#define VLAN_PERF_MARK_END_TIME()  
#endif

#define VLAN_IS_LOGICAL_BIT_LIST_SET(au1PortList, u2PortNum, i4Size, bResult)  \
    OSIX_BITLIST_IS_BIT_SET (au1PortList, u2PortNum, i4Size, bResult)

#define VLAN_MIN_SISP_IF_INDEX   CFA_MIN_SISP_IF_INDEX
#define VLAN_MAX_SISP_IF_INDEX   CFA_MAX_SISP_IF_INDEX

#ifdef PBB_PERFORMANCE_WANTED

#define VLAN_PBB_PERF_MARK_START_TIME()     gettimeofday(&gStTime, NULL);
#define VLAN_PBB_PERF_MARK_END_TIME(u4TimeTaken)      \
{\
    long long u4T1, u4T2;\
    gettimeofday(&gEnTime, NULL);\
    u4T1 = (gStTime.tv_sec * 1000000 + gStTime.tv_usec);\
    u4T2 = (gEnTime.tv_sec * 1000000 + gEnTime.tv_usec);\
    gu4TimeTaken = (u4T2 - u4T1);\
    u4TimeTaken += (u4T2 - u4T1);\
}
#define VLAN_PBB_PERF_PRINT_TIME(u4TimeTaken)      \
{\
    VLAN_PRINT(VLAN_ALL_FLAG, VLAN_ALL_FLAG, VLAN_NAME,"Time taken ->  micro-sec=%u\r\n",u4TimeTaken);\
}

#define VLAN_PBB_PERF_CURR_TIME()      \
{\
    long long u4T1;\
    gettimeofday(&gCurTime, NULL);\
    u4T1 = (gCurTime.tv_sec * 1000000 + gCurTime.tv_usec);\
    VLAN_PRINT(VLAN_ALL_FLAG, VLAN_ALL_FLAG, VLAN_NAME,"Current Time ->  micro-sec=%llu \r\n",u4T1);\
}

#define VLAN_PBB_PERF_PRINT_VLANTIME(u4TimeTaken)      \
{\
    VLAN_PRINT(VLAN_ALL_FLAG, VLAN_ALL_FLAG, VLAN_NAME,"VLAN Time taken ->  micro-sec=%u\r\n",u4TimeTaken);\
}
#define VLAN_PBB_PERF_PRINT_TOTTIME(u4Time1, u4Time2)      \
{\
    VLAN_PRINT(VLAN_ALL_FLAG, VLAN_ALL_FLAG, VLAN_NAME,"Total Time taken ->  micro-sec=%u\r\n",u4Time1 +\
            u4Time2);\
}
#define VLAN_PBB_PERF_PRINT_DELTOTTIME()      \
{\
    VLAN_PRINT(VLAN_ALL_FLAG, VLAN_ALL_FLAG, VLAN_NAME,"Total Time taken ->  micro-sec=%u\r\n",gu4ISIDDeleteTimeTaken +\
            gu4DelVlanIsidTimeTaken);\
}


#define VLAN_PBB_PERF_RESET(u4Time)     u4Time = 0;

#else
#define VLAN_PBB_PERF_MARK_START_TIME()
#define VLAN_PBB_PERF_MARK_END_TIME(u4TimeTaken)
#define VLAN_PBB_PERF_PRINT_TIME(u4TimeTaken)      
#define VLAN_PBB_PERF_CURR_TIME()
#define VLAN_PBB_PERF_PRINT_VLANTIME(u4TimeTaken)
#define VLAN_PBB_PERF_PRINT_TOTTIME(u4Time1, u4Time2)
#define VLAN_PBB_PERF_PRINT_DELTOTTIME()
#define VLAN_PBB_PERF_RESET(u4Time)
#endif
#define VLAN_CONVERT_COMP_ID_TO_CTXT_ID(u4ComponentId) \
    u4ComponentId = (u4ComponentId - 1)
#define VLAN_CONVERT_CTXT_ID_TO_COMP_ID(u4ContextId) \
    u4ContextId = (u4ContextId + 1)
#define IS_VLANLIST_NONEMPTY(pu1VlanList, i4IsResult) \
{\
    UINT2 u2Count = 0;\
    i4IsResult = VLAN_FALSE;\
    for (u2Count = 0; u2Count < VLAN_LIST_SIZE; u2Count++)\
    {\
 if (pu1VlanList [u2Count] != 0)\
        {\
     i4IsResult = VLAN_TRUE;\
     break;\
 }\
    }\
}

#ifdef VLAN_PORTLIST_RBTREE_WANTED
/* the following macro should be defined as
 * VLAN_MAX_PORTS * VLAN_DEV_MAX_NUM_VLAN.
 * This macro should not be used anywhere in the code 
 * for arrays. Because this is mapped to sizing variable
 * MAX_VLAN_PORT_VLAN_MAP_ENTRIES
 */
#define VLAN_PORT_VLAN_MAP_ENTRIES  4096

#else

/* the following macro is 1. Because tLocalPortList will
 * be used in the code and hence the data strucutre 
 * tPortVlanMapEntry will not be required and size is 1.
 * This macro should not be used anywhere in the code 
 * for arrays. Because this is mapped to sizing variable
 * MAX_VLAN_PORT_VLAN_MAP_ENTRIES.
 * NOTE: the reason to keep this memory size as 1 is, when pool creation is 
 * being done, the number of blocks should be atleast 1 otherwise, the pool
 * creation will fail.
 */
#define VLAN_PORT_VLAN_MAP_ENTRIES  1

#endif /* VLAN_PORTLIST_RBTREE_WANTED */


#ifdef VLAN_PORTLIST_RBTREE_WANTED
#define VLAN_GET_PORT_VLAN_MAP_ENTRY(VlanId, u2PortId)      \
        VlanGetPortVlanMapEntry ((UINT2) VlanId, u2PortId)
   
#define VLAN_SET_FDB_FLUSH(pEntry,u2PortId)       \
        VlanSetFdbFlushStatus (pEntry->VlanId, u2PortId)
   
#define VLAN_GET_FDB_FLUSH(pEntry,u2PortId,u1Result)       \
        VlanGetFdbFlushStatus (pEntry->VlanId, u2PortId, &u1Result)
   
#define VLAN_RESET_FDB_FLUSH(pEntry,u2PortId)       \
        VlanResetFdbFlushStatus ((UINT4) pEntry->VlanId, u2PortId)
   
    /* tPortVlanMapEntry -> RBTree utilities */
#define VLAN_IS_MAC_LEARN_PORT(pEntry,u2Port,u1Result)                        \
        VlanIsMemberPort ((UINT2)pEntry->u4Fid, u2Port,                       \
            VLAN_PORT_PROPERTY_MAC_LEARN, &u1Result);

#define VLAN_SET_SINGLE_MAC_LEARN_PORT(pEntry,u2Port)                         \
        VlanAddSinglePortInDB ((UINT2)pEntry->u4Fid, u2Port,                  \
            VLAN_PORT_PROPERTY_MAC_LEARN);

#define VLAN_SET_MAC_LEARN_PORTS(pEntry,LocalPortList)                        \
        VlanAddPortsInDB ((UINT2)pEntry->u4Fid,                               \
            VLAN_PORT_PROPERTY_MAC_LEARN, LocalPortList);

#define VLAN_RESET_SINGLE_MAC_LEARN_PORT(pEntry,u2Port)                       \
        VlanResetSinglePortInDB ((UINT2)pEntry->u4Fid, u2Port,                \
            VLAN_PORT_PROPERTY_MAC_LEARN);

#define VLAN_GET_ADD_DEL_MAC_LEARN_PORTS(pEntry,InputList,AddList,DelList)    \
        VlanGetAddedDeletedPorts ((UINT2)pEntry->u4Fid,                       \
            VLAN_PORT_PROPERTY_MAC_LEARN, InputList, AddList, DelList);

#define VLAN_RESET_MAC_LEARN_PORTS(pEntry,LocalPortList)                      \
        VlanResetPortsInDB ((UINT2)pEntry->u4Fid,                             \
            VLAN_PORT_PROPERTY_MAC_LEARN, LocalPortList);

#define VLAN_RESET_ALL_MAC_LEARN_PORTS(pEntry)                                \
        VlanResetAllPortInDB ((UINT2)pEntry->u4Fid,                           \
            VLAN_PORT_PROPERTY_MAC_LEARN);

/* CLASSIFICATION : CREATIONS/DELETIONS */
#define VLAN_SET_SINGLE_EGRESS_PORT(pEntry,u2Port)                            \
        VlanAddSinglePortInDB (pEntry->VlanId, u2Port,                        \
            VLAN_PORT_PROPERTY_EGRESS);


#define VLAN_SET_ALL_EGRESS_PORTS(pEntry)                                     \
        VlanWrSetAllPorts (pEntry->VlanId, VLAN_PORT_PROPERTY_EGRESS);

#define VLAN_RESET_SINGLE_EGRESS_PORT(pEntry,u2Port)                          \
        VlanResetSinglePortInDB (pEntry->VlanId, u2Port,                      \
               VLAN_PORT_PROPERTY_EGRESS);                                    \

#define VLAN_RESET_ALL_EGRESS_PORTS(pEntry)                                   \
        VlanResetAllPortInDB ((UINT2)pEntry->VlanId,                          \
            VLAN_PORT_PROPERTY_EGRESS);

#define VLAN_OVERWRITE_EGRESS_PORTS(pEntry,LocalPortList)                     \
        VlanAddDelPortsInDB (pEntry->VlanId, VLAN_PORT_PROPERTY_EGRESS,       \
                          LocalPortList);

/* CLASSIFICATION : GET/VALIDATIONS */
#define VLAN_IS_EGRESS_PORT(pEntry,u2Port,u1Result)                           \
        VlanIsMemberPort (pEntry->VlanId, u2Port, VLAN_PORT_PROPERTY_EGRESS,  \
                          &u1Result);

#define VLAN_IS_NO_EGRESS_PORTS_PRESENT(pEntry,u1Result)                      \
        VlanIsNoPortsPresent (pEntry->VlanId, VLAN_PORT_PROPERTY_EGRESS,      \
                              &u1Result);

#define VLAN_REMOVE_EGRESS_PORTS_FROM_PORTLIST(pEntry,LocalPortList)          \
        VlanResetPortsInLocalPortList (pEntry->VlanId,                        \
            VLAN_PORT_PROPERTY_EGRESS, LocalPortList);

#define VLAN_ADD_EGRESS_PORTS_TO_LIST(pEntry,LocalPortList)                   \
        VlanGetPortsFromDB (pEntry->VlanId, VLAN_PORT_PROPERTY_EGRESS,        \
                            LocalPortList);

#define VLAN_GET_ADD_DEL_EGRESS_PORTS(pEntry,InputList,AddList,DelList)       \
        VlanGetAddedDeletedPorts (pEntry->VlanId,                             \
            VLAN_PORT_PROPERTY_EGRESS, InputList, AddList, DelList);

#define VLAN_GET_EGRESS_PORTS(pEntry,LocalPortList)                           \
        MEMSET (LocalPortList, 0, sizeof (tLocalPortList));                   \
        VlanGetPortsFromDB (pEntry->VlanId, VLAN_PORT_PROPERTY_EGRESS,        \
                            LocalPortList);

#define VLAN_IS_PORTLIST_SUBSET_TO_EGRESS(pEntry,LocalPortList,u1Result)      \
        VlanIsPortListSubsetToDB (pEntry->VlanId, VLAN_PORT_PROPERTY_EGRESS,  \
                                  LocalPortList, &u1Result);

#define VLAN_IS_PORTLIST_SUBSET_TO_TRUNK(pEntry,LocalPortList,u1Result)      \
        VlanIsPortListSubsetToDB (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_TRUNK,  \
                                  LocalPortList, &u1Result);

#define VLAN_ARE_PORTS_EXCL_WITH_EGRESS(pEntry,LocalPortList,u1Result)        \
        VlanArePortsExclusive (pEntry->VlanId, VLAN_PORT_PROPERTY_EGRESS,     \
                      LocalPortList, &u1Result);

#define VLAN_IS_SEC_SUBSET_OF_PRIM(pPrimEntry,pSecEntry,u1Result)             \
        VlanIsDB2SubsetToDB1 (pPrimEntry->VlanId, VLAN_PORT_PROPERTY_EGRESS,  \
              pSecEntry->VlanId, VLAN_PORT_PROPERTY_EGRESS, &u1Result);

#define VLAN_IS_ALL_MATCHING_FOR_EGRESS(pEntry,LocalPortList,u1Result)        \
        VlanIsAllPortsMatching (pEntry->VlanId, VLAN_PORT_PROPERTY_EGRESS,    \
            LocalPortList, &u1Result);

#define VLAN_CONVERT_EGRESS_TO_IFPORTLIST(pEntry,PortList)                    \
        VlanConvertDBPortsToIfPortList (pEntry->VlanId,                       \
                 VLAN_PORT_PROPERTY_EGRESS, PortList);

/*UNTAGGED PORTS*/
/* Macros that are added for tStaticVlanEntry->UnTagPorts*/
#define VLAN_IS_UNTAGGED_PORT(pEntry,u2Port,u1Result)                           \
        VlanIsMemberPort (pEntry->VlanId, u2Port, VLAN_PORT_PROPERTY_UNTAGGED,  \
                         &u1Result);

#define VLAN_SET_SINGLE_UNTAGGED_PORT(pEntry, u2Port)                           \
        VlanAddSinglePortInDB (pEntry->VlanId, u2Port,                          \
            VLAN_PORT_PROPERTY_UNTAGGED);

#define VLAN_SET_ALL_UNTAGGED_PORTS(pEntry)                                     \
        VlanWrSetAllPorts (pEntry->VlanId, VLAN_PORT_PROPERTY_UNTAGGED);

#define VLAN_RESET_SINGLE_UNTAGGED_PORT(pEntry,u2Port)                          \
        VlanResetSinglePortInDB (pEntry->VlanId, u2Port,                        \
            VLAN_PORT_PROPERTY_UNTAGGED);

#define VLAN_RESET_ALL_UNTAGGED_PORTS(pEntry)                                   \
        VlanResetAllPortInDB ((UINT2)pEntry->VlanId,                            \
            VLAN_PORT_PROPERTY_UNTAGGED);

#define VLAN_REMOVE_UNTAGGED_PORTS_FROM_PORTLIST(pEntry, LocalPortList)         \
        VlanResetPortsInLocalPortList (pEntry->VlanId,                          \
            VLAN_PORT_PROPERTY_UNTAGGED, LocalPortList);

#define VLAN_GET_UNTAGGED_PORTS(pEntry,LocalPortList)                           \
{                                                                               \
    MEMSET (LocalPortList, 0, sizeof (tLocalPortList));                         \
    VlanGetPortsFromDB (pEntry->VlanId, VLAN_PORT_PROPERTY_UNTAGGED,            \
            LocalPortList);                                                     \
}

#define VLAN_IS_UNTAGGED_SUBSET_TO_PORTLIST(pEntry,LocalPortList,u1Result)          \
        VlanIsDBPortsSubsetToPortList (pEntry->VlanId, VLAN_PORT_PROPERTY_UNTAGGED, \
            LocalPortList, &u1Result);

#define VLAN_IS_ALL_MATCHING_FOR_UNTAGGED(pEntry,LocalPortList,u1Result)        \
        VlanIsAllPortsMatching (pEntry->VlanId, VLAN_PORT_PROPERTY_UNTAGGED,    \
            LocalPortList, &u1Result);

#define VLAN_ADD_UNTAGGED_PORTS_TO_LIST(pEntry,LocalPortList)                   \
        VlanGetPortsFromDB (pEntry->VlanId, VLAN_PORT_PROPERTY_UNTAGGED,        \
            LocalPortList);

#define VLAN_OVERWRITE_UNTAGGED_PORTS(pEntry,LocalPortList)                     \
        VlanAddDelPortsInDB (pEntry->VlanId, VLAN_PORT_PROPERTY_UNTAGGED,       \
            LocalPortList);

#define VLAN_CONVERT_UNTAGGED_TO_IFPORTLIST(pEntry,PortList)                    \
        VlanConvertDBPortsToIfPortList (pEntry->VlanId,                         \
            VLAN_PORT_PROPERTY_UNTAGGED, PortList);

/*FORBIDDEN PORTS*/
/* Macros that are added for tStaticVlanEntry->UnTagPorts*/
#define VLAN_IS_FORBIDDEN_PORT(pEntry,u2Port,u1Result)                          \
{                                                                               \
    {                                                                           \
        VlanIsMemberPort (pEntry->VlanId, u2Port, VLAN_PORT_PROPERTY_FORBIDDEN, \
                          &u1Result);                                           \
    }                                                                           \
}

#define VLAN_IS_NO_FORBIDDEN_PORTS_PRESENT(pEntry,u1Result)                     \
{                                                                               \
    {                                                                           \
        VlanIsNoPortsPresent (pEntry->VlanId, VLAN_PORT_PROPERTY_FORBIDDEN,     \
                              &u1Result);                                       \
    }                                                                           \
}

#define VLAN_SET_SINGLE_FORBIDDEN_PORT(pEntry,u2Port)                           \
{                                                                               \
    {                                                                           \
        VlanAddSinglePortInDB (pEntry->VlanId, u2Port,                          \
            VLAN_PORT_PROPERTY_FORBIDDEN);                                      \
    }                                                                           \
}


#define VLAN_RESET_SINGLE_FORBIDDEN_PORT(pEntry,u2Port)                         \
{                                                                               \
    {                                                                           \
        VlanResetSinglePortInDB (pEntry->VlanId, u2Port,                        \
               VLAN_PORT_PROPERTY_FORBIDDEN);                                   \
    }                                                                           \
}

#define VLAN_RESET_ALL_FORBIDDEN_PORT(pEntry)                                   \
{                                                                               \
    {                                                                           \
        VlanResetAllPortInDB (pEntry->VlanId, VLAN_PORT_PROPERTY_FORBIDDEN);    \
    }                                                                           \
}

#define VLAN_REMOVE_FORBIDDEN_PORTS_FROM_PORTLIST(pEntry,LocalPortList)         \
{                                                                               \
    {                                                                           \
        VlanResetPortsInLocalPortList (pEntry->VlanId,                          \
            VLAN_PORT_PROPERTY_FORBIDDEN, LocalPortList);                       \
    }                                                                           \
}

#define VLAN_GET_FORBIDDEN_PORTS(pEntry,LocalPortList)                          \
{                                                                               \
    {                                                                           \
        VlanGetPortsFromDB (pEntry->VlanId, VLAN_PORT_PROPERTY_FORBIDDEN,       \
                            LocalPortList);                                     \
    }                                                                           \
}

#define VLAN_IS_ALL_MATCHING_FOR_FORBIDDEN(pEntry,LocalPortList,u1Result)       \
{                                                                               \
        {                                                                       \
        VlanIsAllPortsMatching (pEntry->VlanId, VLAN_PORT_PROPERTY_FORBIDDEN,   \
            LocalPortList, &u1Result);                                          \
    }                                                                           \
} 

#define VLAN_ARE_PORTS_EXCL_WITH_FORBIDDEN(pEntry,LocalPortList,u1Result)       \
{                                                                               \
    {                                                                           \
        VlanArePortsExclusive (pEntry->VlanId, VLAN_PORT_PROPERTY_FORBIDDEN,    \
                      LocalPortList, &u1Result);                                \
    }                                                                           \
} 

#define VLAN_ADD_FORBIDDEN_PORTS_TO_LIST(pEntry,LocalPortList)                  \
{                                                                               \
    {                                                                           \
        VlanGetPortsFromDB (pEntry->VlanId, VLAN_PORT_PROPERTY_FORBIDDEN,       \
                            LocalPortList);                                     \
    }                                                                           \
}

#define VLAN_OVERWRITE_FORBIDDEN_PORTS(pEntry,LocalPortList)                    \
{                                                                               \
    {                                                                           \
        VlanAddDelPortsInDB (pEntry->VlanId, VLAN_PORT_PROPERTY_FORBIDDEN,      \
                          LocalPortList);                                       \
    }                                                                           \
}

#define VLAN_CONVERT_FORBIDDEN_TO_IFPORTLIST(pEntry,PortList)                   \
{                                                                               \
    {                                                                           \
        VlanConvertDBPortsToIfPortList (pEntry->VlanId,                         \
                 VLAN_PORT_PROPERTY_FORBIDDEN, PortList);                       \
    }                                                                           \
}

#define VLAN_GET_ADD_DEL_FORBIDDEN_PORTS(pEntry,InputList,AddList,DelList)      \
{                                                                               \
    {                                                                           \
        VlanGetAddedDeletedPorts (pEntry->VlanId,                               \
            VLAN_PORT_PROPERTY_FORBIDDEN, InputList, AddList, DelList);         \
    }                                                                           \
} 

/*CURR EGRESS PORTS*/
/* Macros that are added for tVlanCurrEntry->EgressPorts*/
#define VLAN_IS_CURR_EGRESS_PORT(pEntry,u2Port,u1Result)                        \
{                                                                               \
    {                                                                           \
        VlanIsMemberPort (pEntry->VlanId, u2Port, VLAN_PORT_PROPERTY_CUR_EGRESS,\
                          &u1Result);                                           \
    }                                                                           \
}

#define VLAN_IS_NO_CURR_EGRESS_PORTS_PRESENT(pEntry,u1Result)                   \
{                                                                               \
    {                                                                           \
        VlanIsNoPortsPresent (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_EGRESS,    \
                              &u1Result);                                       \
    }                                                                           \
}

#define VLAN_SET_SINGLE_CURR_EGRESS_PORT(pEntry,u2Port)                         \
{                                                                               \
    {                                                                           \
        VlanAddSinglePortInDB (pEntry->VlanId, u2Port,                          \
            VLAN_PORT_PROPERTY_CUR_EGRESS);                                     \
    }                                                                           \
}

#define VLAN_SET_CURR_EGRESS_PORTS(pEntry,LocalPortList)                        \
{                                                                               \
    {                                                                           \
        VlanAddPortsInDB (pEntry->VlanId,                                       \
            VLAN_PORT_PROPERTY_CUR_EGRESS,LocalPortList);                       \
    }                                                                           \
}

#define VLAN_RESET_SINGLE_CURR_EGRESS_PORT(pEntry,u2Port)                       \
{                                                                               \
    {                                                                           \
        VlanResetSinglePortInDB (pEntry->VlanId, u2Port,                        \
               VLAN_PORT_PROPERTY_CUR_EGRESS);                                  \
    }                                                                           \
}

#define VLAN_RESET_ALL_CURR_EGRESS_PORT(pEntry)                                 \
{                                                                               \
    {                                                                           \
        VlanResetAllPortInDB (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_EGRESS);   \
    }                                                                           \
}

#define VLAN_RESET_CURR_EGRESS_PORTS(pEntry,LocalPortList)                      \
{                                                                               \
    {                                                                           \
        VlanResetPortsInDB(pEntry->VlanId,                                      \
            VLAN_PORT_PROPERTY_CUR_EGRESS, LocalPortList);                      \
    }                                                                           \
}

#define VLAN_GET_CURR_EGRESS_PORTS(pEntry,LocalPortList)                        \
{                                                                               \
    {                                                                           \
        VlanGetPortsFromDB (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_EGRESS,      \
                            LocalPortList);                                     \
    }                                                                           \
}

#define VLAN_IS_ALL_MATCHING_FOR_CURR_EGRESS(pEntry,LocalPortList,u1Result)     \
{                                                                               \
    {                                                                           \
        VlanIsAllPortsMatching (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_EGRESS,  \
            LocalPortList, &u1Result);                                          \
    }                                                                           \
} 

#define VLAN_ARE_PORTS_EXCL_WITH_CURR_EGRESS(pEntry,LocalPortList,u1Result)     \
{                                                                               \
    {                                                                           \
        VlanArePortsExclusive (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_EGRESS,   \
                      LocalPortList, &u1Result);                                \
    }                                                                           \
} 

#define VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST(pEntry,LocalPortList)                \
{                                                                               \
    {                                                                           \
        VlanGetPortsFromDB (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_EGRESS,      \
                            LocalPortList);                                     \
    }                                                                           \
}

#define VLAN_OVERWRITE_CURR_EGRESS_PORTS(pEntry,LocalPortList)                  \
{                                                                               \
    {                                                                           \
        VlanAddDelPortsInDB (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_EGRESS,     \
                          LocalPortList);                                       \
    }                                                                           \
}

#define VLAN_CONVERT_CURR_EGRESS_TO_IFPORTLIST(pEntry,PortList)                 \
{                                                                               \
    {                                                                           \
        VlanConvertDBPortsToIfPortList (pEntry->VlanId,                         \
                 VLAN_PORT_PROPERTY_CUR_EGRESS, PortList);                      \
    }                                                                           \
}

#define VLAN_IS_PORTLIST_SUBSET_TO_CURR_EGRESS(pEntry,LocalPortList,u1Result)   \
{                                                                               \
    {                                                                           \
        VlanIsPortListSubsetToDB (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_EGRESS,\
                                          LocalPortList, &u1Result);            \
    }                                                                           \
}


#define VLAN_IS_PORT_ONLY_MEMBER_FOR_CURR_EGRESS(pEntry,u2Port,u1Result)        \
{                                                                               \
    {                                                                           \
        VlanIsPortOnlyMember(pEntry->VlanId, u2Port,                            \
                    VLAN_PORT_PROPERTY_CUR_EGRESS, &u1Result);                   \
    }                                                                           \
}

/*CURR LEARNT PORTS*/
/* Macros that are added for tVlanCurrEntry->LearntPorts*/
#define VLAN_IS_CURR_LEARNT_PORT(pEntry,u2Port,u1Result)                        \
{                                                                               \
    {                                                                           \
        VlanIsMemberPort (pEntry->VlanId, u2Port, VLAN_PORT_PROPERTY_CUR_LEARNT,\
                          &u1Result);                                           \
    }                                                                           \
}

#define VLAN_IS_NO_CURR_LEARNT_PORTS_PRESENT(pEntry,u1Result)                   \
{                                                                               \
    {                                                                           \
        VlanIsNoPortsPresent (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_LEARNT,    \
                              &u1Result);                                       \
    }                                                                           \
}

#define VLAN_SET_SINGLE_CURR_LEARNT_PORT(pEntry,u2Port)                         \
{                                                                               \
    {                                                                           \
        VlanAddSinglePortInDB (pEntry->VlanId, u2Port,                          \
            VLAN_PORT_PROPERTY_CUR_LEARNT);                                     \
    }                                                                           \
}


#define VLAN_RESET_SINGLE_CURR_LEARNT_PORT(pEntry,u2Port)                       \
{                                                                               \
    {                                                                           \
        VlanResetSinglePortInDB (pEntry->VlanId, u2Port,                        \
               VLAN_PORT_PROPERTY_CUR_LEARNT);                                  \
    }                                                                           \
}

#define VLAN_RESET_CURR_LEARNT_PORTS(pEntry,LocalPortList)                      \
{                                                                               \
    {                                                                           \
        VlanResetPortsInDB(pEntry->VlanId,                                      \
            VLAN_PORT_PROPERTY_CUR_LEARNT, LocalPortList);                      \
    }                                                                           \
}

#define VLAN_RESET_ALL_CURR_LEARNT_PORT(pEntry)                                 \
{                                                                               \
    {                                                                           \
        VlanResetAllPortInDB (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_LEARNT);   \
    }                                                                           \
}

#define VLAN_GET_CURR_LEARNT_PORTS(pEntry,LocalPortList)                        \
{                                                                               \
    {                                                                           \
        VlanGetPortsFromDB (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_LEARNT,      \
                            LocalPortList);                                     \
    }                                                                           \
}

#define VLAN_ADD_CURR_LEARNT_PORTS_TO_LIST(pEntry,LocalPortList)                \
{                                                                               \
    {                                                                           \
        VlanGetPortsFromDB (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_LEARNT,      \
                            LocalPortList);                                     \
    }                                                                           \
}

#define VLAN_CONVERT_CURR_LEARNT_TO_IFPORTLIST(pEntry,PortList)                 \
{                                                                               \
    {                                                                           \
        VlanConvertDBPortsToIfPortList (pEntry->VlanId,                         \
                 VLAN_PORT_PROPERTY_CUR_LEARNT, PortList);                      \
    }                                                                           \
}
/*This Macro will set u1Result to VLAN_TRUE when u2Port is a */
/*tagged member of the vlan*/

/*CURR TRUNK PORTS*/
/* Macros that are added for tVlanCurrEntry->TrunkPorts*/
#define VLAN_IS_CURR_TRUNK_PORT(pEntry,u2Port,u1Result)                         \
{                                                                               \
    {                                                                           \
        VlanIsMemberPort (pEntry->VlanId, u2Port, VLAN_PORT_PROPERTY_CUR_TRUNK, \
                          &u1Result);                                           \
    }                                                                           \
}

#define VLAN_SET_SINGLE_CURR_TRUNK_PORT(pEntry,u2Port)                          \
{                                                                               \
    {                                                                           \
        VlanAddSinglePortInDB (pEntry->VlanId, u2Port,                          \
            VLAN_PORT_PROPERTY_CUR_TRUNK);                                      \
    }                                                                           \
}


#define VLAN_RESET_SINGLE_CURR_TRUNK_PORT(pEntry,u2Port)                        \
{                                                                               \
    {                                                                           \
        VlanResetSinglePortInDB (pEntry->VlanId, u2Port,                        \
               VLAN_PORT_PROPERTY_CUR_TRUNK);                                   \
    }                                                                           \
}

#define VLAN_RESET_ALL_CURR_TRUNK_PORT(pEntry)                                  \
{                                                                               \
    {                                                                           \
        VlanResetAllPortInDB (pEntry->VlanId, VLAN_PORT_PROPERTY_CUR_TRUNK);    \
    }                                                                           \
}

#define VLAN_SET_CURR_TRUNK_PORTS(pEntry,LocalPortList)                         \
{                                                                               \
    {                                                                           \
        VlanAddPortsInDB (pEntry->VlanId,                                       \
            VLAN_PORT_PROPERTY_CUR_TRUNK, LocalPortList);                       \
    }                                                                           \
}
#else /* else for VLAN_PORTLIST_RBTREE_WANTED */
    /* tLocalPortList utilities */
#define VLAN_GET_PORT_VLAN_MAP_ENTRY(VlanId, u2PortId)  \
        VlanGetPortVlanFdbFlushEntry (VlanId, u2PortId)

#define VLAN_SET_FDB_FLUSH(pEntry,u2PortId)  \
        VLAN_SET_MEMBER_PORT (pEntry->FdbFlushStatus, u2PortId)

#define VLAN_GET_FDB_FLUSH(pEntry,u2PortId,u1Result)       \
        VLAN_IS_MEMBER_PORT (pEntry->FdbFlushStatus, u2PortId, u1Result)
   
#define VLAN_RESET_FDB_FLUSH(pEntry,u2PortId)       \
        VLAN_RESET_MEMBER_PORT (pEntry->FdbFlushStatus, u2PortId)
   

#define VLAN_IS_MAC_LEARN_PORT(pEntry,u2Port,u1Result)                        \
        VLAN_IS_MEMBER_PORT (pEntry->MacLearnPortList, u2Port, u1Result);

#define VLAN_SET_SINGLE_MAC_LEARN_PORT(pEntry,u2Port)                         \
        VLAN_SET_MEMBER_PORT (pEntry->MacLearnPortList, u2Port);

#define VLAN_SET_MAC_LEARN_PORTS(pEntry,LocalPortList)                        \
        VLAN_ADD_PORT_LIST (pEntry->MacLearnPortList, LocalPortList);

#define VLAN_RESET_SINGLE_MAC_LEARN_PORT(pEntry,u2Port)                       \
        VLAN_RESET_MEMBER_PORT (pEntry->MacLearnPortList, u2Port); 

#define VLAN_GET_ADD_DEL_MAC_LEARN_PORTS(pEntry,InputList,AddList,DelList)    \
{                                                                             \
    UINT2   u2ByteInd = 0;                                                    \
                                                                              \
        for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)     \
        {                                                                     \
            AddList[u2ByteInd] = (UINT1)                                      \
                     ((~(pEntry->MacLearnPortList[u2ByteInd])) &              \
                         (InputList[u2ByteInd]));                             \
                                                                              \
            DelList[u2ByteInd] = (UINT1)                                      \
                     ((~InputList[u2ByteInd]) &                               \
                         (pEntry->MacLearnPortList[u2ByteInd]));              \
        }                                                                     \
}

#define VLAN_RESET_MAC_LEARN_PORTS(pEntry,LocalPortList)                      \
        VLAN_RESET_PORT_LIST (pEntry->MacLearnPortList, LocalPortList);

#define VLAN_RESET_ALL_MAC_LEARN_PORTS(pEntry)                                \
    MEMSET (pEntry->MacLearnPortList, 0, sizeof (tLocalPortList));

/* Macros that are added for tStaticVlanEntry->EgressPorts */
#define VLAN_IS_EGRESS_PORT(pEntry,u2Port,u1Result)                           \
        VLAN_IS_MEMBER_PORT (pEntry->EgressPorts, u2Port, u1Result);

#define VLAN_IS_NO_EGRESS_PORTS_PRESENT(pEntry,u1Result)                      \
{                                                                             \
    if (MEMCMP (pEntry->EgressPorts, gNullPortList,                           \
                sizeof (tLocalPortList)) == 0)                                \
    {                                                                         \
        u1Result = VLAN_TRUE;                                                 \
    }                                                                         \
    else                                                                      \
    {                                                                         \
        u1Result = VLAN_FALSE;                                                \
    }                                                                         \
}

#define VLAN_SET_SINGLE_EGRESS_PORT(pEntry,u2Port)                            \
        VLAN_SET_MEMBER_PORT (pEntry->EgressPorts, u2Port);


#define VLAN_SET_ALL_EGRESS_PORTS(pEntry)                                     \
        VlanSetAllPorts (pEntry->EgressPorts);

#define VLAN_RESET_SINGLE_EGRESS_PORT(pEntry,u2Port)                          \
        VLAN_RESET_MEMBER_PORT (pEntry->EgressPorts, u2Port);

#define VLAN_RESET_ALL_EGRESS_PORTS(pEntry)                                   \
    MEMSET (pEntry->EgressPorts, 0, sizeof (tLocalPortList));

#define VLAN_REMOVE_EGRESS_PORTS_FROM_PORTLIST(pEntry,LocalPortList)          \
        VLAN_RESET_PORT_LIST (LocalPortList, pEntry->EgressPorts);

#define VLAN_GET_EGRESS_PORTS(pEntry,LocalPortList)                           \
        MEMCPY (LocalPortList, pEntry->EgressPorts, sizeof (tLocalPortList));

#define VLAN_IS_PORTLIST_SUBSET_TO_EGRESS(pEntry,LocalPortList,u1Result)      \
        VLAN_IS_PORT_LIST_A_SUBSET (pEntry->EgressPorts, LocalPortList,       \
                                    u1Result);

#define VLAN_IS_PORTLIST_SUBSET_TO_TRUNK(pEntry,LocalPortList,u1Result)      \
        VLAN_IS_PORT_LIST_A_SUBSET (pEntry->TrunkPorts, LocalPortList,       \
                                    u1Result);

#define VLAN_IS_SEC_SUBSET_OF_PRIM(pPrimEntry,pSecEntry,u1Result)             \
        VLAN_IS_PORT_LIST_A_SUBSET (pPrimEntry->EgressPorts,                  \
                pSecEntry->EgressPorts, u1Result);

#define VLAN_IS_ALL_MATCHING_FOR_EGRESS(pEntry,LocalPortList,u1Result)        \
    if (MEMCMP (pEntry->EgressPorts, LocalPortList,                           \
                sizeof (tLocalPortList)) == 0)                                \
{                                                                             \
    u1Result = VLAN_TRUE;                                                     \
}                                                                             \
else                                                                          \
{                                                                             \
    u1Result = VLAN_FALSE;                                                    \
}

#define VLAN_ARE_PORTS_EXCL_WITH_EGRESS(pEntry,LocalPortList,u1Result)        \
        VLAN_ARE_PORTS_EXCLUSIVE (pEntry->EgressPorts, LocalPortList,         \
                                         u1Result);

#define VLAN_ADD_EGRESS_PORTS_TO_LIST(pEntry,LocalPortList)                   \
        VLAN_ADD_PORT_LIST (LocalPortList, pEntry->EgressPorts);

#define VLAN_OVERWRITE_EGRESS_PORTS(pEntry,LocalPortList)                     \
        MEMCPY (pEntry->EgressPorts, LocalPortList, sizeof (tLocalPortList));

#define VLAN_CONVERT_EGRESS_TO_IFPORTLIST(pEntry,PortList)                    \
        VlanConvertToIfPortList (pEntry->EgressPorts, PortList);

#define VLAN_GET_ADD_DEL_EGRESS_PORTS(pEntry,InputList,AddList,DelList)       \
{                                                                             \
    UINT2   u2BytInd = 0;                                                     \
                                                                              \
    for (u2BytInd = 0; u2BytInd < VLAN_PORT_LIST_SIZE; u2BytInd++)            \
    {                                                                         \
        AddList[u2BytInd] = (UINT1)                                           \
        ((~(pEntry->EgressPorts[u2BytInd])) &                                 \
         (InputList[u2BytInd]));                                              \
                                                                              \
        DelList[u2BytInd] = (UINT1)                                           \
        ((~InputList[u2BytInd]) &                                             \
         (pEntry->EgressPorts[u2BytInd]));                                    \
    }                                                                         \
}
/* Macros that are added for tStaticVlanEntry->UnTagPorts*/
#define VLAN_IS_UNTAGGED_PORT(pEntry,u2Port,u1Result)                         \
        VLAN_IS_MEMBER_PORT (pEntry->UnTagPorts, u2Port, u1Result);

#define VLAN_SET_SINGLE_UNTAGGED_PORT(pEntry, u2Port)                         \
        VLAN_SET_MEMBER_PORT (pEntry->UnTagPorts, u2Port);

#define VLAN_SET_ALL_UNTAGGED_PORTS(pEntry)                                   \
         VlanSetAllPorts (pEntry->UnTagPorts);

#define VLAN_RESET_SINGLE_UNTAGGED_PORT(pEntry,u2Port)                        \
        VLAN_RESET_MEMBER_PORT (pEntry->UnTagPorts, u2Port);

#define VLAN_RESET_ALL_UNTAGGED_PORTS(pEntry)                                   \
    MEMSET (pEntry->UnTagPorts, 0, sizeof (tLocalPortList));

#define VLAN_REMOVE_UNTAGGED_PORTS_FROM_PORTLIST(pEntry,LocalPortList)        \
        VLAN_RESET_PORT_LIST (LocalPortList, pEntry->UnTagPorts);

#define VLAN_GET_UNTAGGED_PORTS(pEntry,LocalPortList)                         \
        MEMCPY (LocalPortList, pEntry->UnTagPorts, sizeof (tLocalPortList));

#define VLAN_IS_UNTAGGED_SUBSET_TO_PORTLIST(pEntry,LocalPortList,u1Result)    \
        VLAN_IS_PORT_LIST_A_SUBSET (LocalPortList,pEntry->UnTagPorts,         \
            u1Result);

#define VLAN_IS_ALL_MATCHING_FOR_UNTAGGED(pEntry,LocalPortList,u1Result)      \
{                                                                             \
    {                                                                         \
        if (MEMCMP (pEntry->UnTagPorts, LocalPortList,                        \
                    sizeof (tLocalPortList)) == 0)                            \
        {                                                                     \
            u1Result = VLAN_TRUE;                                             \
        }                                                                     \
        else                                                                    \
        {                                                                       \
            u1Result = VLAN_FALSE;                                              \
        }                                                                       \
    }                                                                         \
}

#define VLAN_ADD_UNTAGGED_PORTS_TO_LIST(pEntry,LocalPortList)                 \
        VLAN_ADD_PORT_LIST (LocalPortList, pEntry->UnTagPorts);

#define VLAN_OVERWRITE_UNTAGGED_PORTS(pEntry,LocalPortList)                   \
        MEMCPY (pEntry->UnTagPorts, LocalPortList, sizeof (tLocalPortList));

#define VLAN_CONVERT_UNTAGGED_TO_IFPORTLIST(pEntry,PortList)                  \
        VlanConvertToIfPortList (pEntry->UnTagPorts, PortList);


/*FORBIDDEN PORTS*/
/* Macros that are added for tStaticVlanEntry->UnTagPorts*/
#define VLAN_IS_FORBIDDEN_PORT(pEntry,u2Port,u1Result)                          \
{                                                                               \
    {                                                                           \
        VLAN_IS_MEMBER_PORT (pEntry->ForbiddenPorts, u2Port, u1Result);         \
    }                                                                           \
}

#define VLAN_IS_NO_FORBIDDEN_PORTS_PRESENT(pEntry,u1Result)                     \
{                                                                               \
    {                                                                           \
        if (MEMCMP (pEntry->ForbiddenPorts, gNullPortList,                      \
                            sizeof (tLocalPortList)) == 0)                      \
        {                                                                       \
            u1Result = VLAN_TRUE;                                               \
        }                                                                       \
        else                                                                    \
        {                                                                       \
            u1Result = VLAN_FALSE;                                              \
        }                                                                       \
    }                                                                           \
}

#define VLAN_SET_SINGLE_FORBIDDEN_PORT(pEntry,u2Port)                           \
{                                                                               \
    {                                                                           \
        VLAN_SET_MEMBER_PORT (pEntry->ForbiddenPorts, u2Port);                  \
    }                                                                           \
}


#define VLAN_RESET_SINGLE_FORBIDDEN_PORT(pEntry,u2Port)                         \
{                                                                               \
    {                                                                           \
        VLAN_RESET_MEMBER_PORT (pEntry->ForbiddenPorts, u2Port);                \
    }                                                                           \
}

#define VLAN_RESET_ALL_FORBIDDEN_PORT(pEntry)                                   \
{                                                                               \
    {                                                                           \
        MEMSET (pEntry->ForbiddenPorts, 0, sizeof (tLocalPortList));            \
    }                                                                           \
}

#define VLAN_REMOVE_FORBIDDEN_PORTS_FROM_PORTLIST(pEntry,LocalPortList)         \
{                                                                               \
    {                                                                           \
        VLAN_RESET_PORT_LIST (LocalPortList, pEntry->ForbiddenPorts);           \
    }                                                                           \
}

#define VLAN_GET_FORBIDDEN_PORTS(pEntry,LocalPortList)                          \
{                                                                               \
    {                                                                           \
        MEMCPY (LocalPortList, pEntry->ForbiddenPorts, sizeof(tLocalPortList)); \
    }                                                                           \
}

#define VLAN_IS_ALL_MATCHING_FOR_FORBIDDEN(pEntry,LocalPortList,u1Result)       \
{                                                                               \
    {                                                                           \
        if (MEMCMP (pEntry->ForbiddenPorts, LocalPortList,                      \
                sizeof (tLocalPortList)) == 0)                                  \
        {                                                                       \
            u1Result = VLAN_TRUE;                                               \
        }                                                                       \
        else                                                                    \
        {                                                                       \
            u1Result = VLAN_FALSE;                                              \
        }                                                                       \
    }                                                                           \
}

#define VLAN_ARE_PORTS_EXCL_WITH_FORBIDDEN(pEntry,LocalPortList,u1Result)       \
{                                                                               \
    {                                                                           \
        VLAN_ARE_PORTS_EXCLUSIVE (pEntry->ForbiddenPorts, LocalPortList,        \
                                         u1Result);                             \
    }                                                                           \
}

#define VLAN_ADD_FORBIDDEN_PORTS_TO_LIST(pEntry,LocalPortList)                  \
{                                                                               \
    {                                                                           \
        VLAN_ADD_PORT_LIST (LocalPortList, pEntry->ForbiddenPorts);             \
    }                                                                           \
}

#define VLAN_OVERWRITE_FORBIDDEN_PORTS(pEntry,LocalPortList)                    \
{                                                                               \
    {                                                                           \
        MEMCPY (pEntry->ForbiddenPorts, LocalPortList, sizeof(tLocalPortList)); \
    }                                                                           \
}
#define VLAN_CONVERT_FORBIDDEN_TO_IFPORTLIST(pEntry,PortList)                   \
{                                                                               \
    {                                                                           \
        VlanConvertToIfPortList (pEntry->ForbiddenPorts, PortList);             \
    }                                                                           \
}

#define VLAN_GET_ADD_DEL_FORBIDDEN_PORTS(pEntry,InputList,AddList,DelList)      \
{                                                                               \
    UINT2   u2BytInd = 0;                                                       \
                                                                                \
    {                                                                           \
        for (u2BytInd = 0; u2BytInd < VLAN_PORT_LIST_SIZE; u2BytInd++)          \
        {                                                                       \
            AddList[u2BytInd] = (UINT1)                                         \
                     ((~(pEntry->ForbiddenPorts[u2BytInd])) &                   \
                         (InputList[u2ByteInd]));                               \
                                                                                \
            DelList[u2BytInd] = (UINT1)                                         \
                     ((~InputList[u2BytInd]) &                                  \
                         (pEntry->ForbiddenPorts[u2BytInd]));                   \
        }                                                                       \
    }                                                                           \
}

/*CURR EGRESS PORTS*/
/* Macros that are added for tVlanCurrEntry->EgressPorts*/
#define VLAN_IS_CURR_EGRESS_PORT(pEntry,u2Port,u1Result)                        \
{                                                                               \
    {                                                                           \
        VLAN_IS_MEMBER_PORT (pEntry->EgressPorts, u2Port, u1Result);            \
    }                                                                           \
}

#define VLAN_IS_NO_CURR_EGRESS_PORTS_PRESENT(pEntry,u1Result)                   \
{                                                                               \
    {                                                                           \
        if (MEMCMP (pEntry->EgressPorts, gNullPortList,                         \
                            sizeof (tLocalPortList)) == 0)                      \
        {                                                                       \
            u1Result = VLAN_TRUE;                                               \
        }                                                                       \
        else                                                                    \
        {                                                                       \
            u1Result = VLAN_FALSE;                                              \
        }                                                                       \
    }                                                                           \
}
#define VLAN_IS_TAGGED_PORT(pEntry,u2Port,u1Result)                             \
{                                                                               \
    VLAN_IS_EGRESS_PORT(pEntry,u2Port,u1Result);                            \
    if (u1Result == VLAN_TRUE)                                              \
    {                                                                       \
            VLAN_IS_UNTAGGED_PORT(pEntry,u2Port,u1Result);                  \
            if (u1Result == VLAN_TRUE)                                      \
            {                                                               \
                    u1Result=VLAN_FALSE;                                    \
            }                                                               \
            else                                                            \
            {                                                               \
                    u1Result=VLAN_TRUE;                                     \
            }                                                               \
    }                                                                       \
    else                                                                    \
    {                                                                       \
            u1Result=VLAN_FALSE;                                            \
    }                                                                       \
}

#define VLAN_SET_SINGLE_CURR_EGRESS_PORT(pEntry,u2Port)                         \
{                                                                               \
    {                                                                           \
        VLAN_SET_MEMBER_PORT (pEntry->EgressPorts, u2Port);                     \
    }                                                                           \
}


#define VLAN_SET_CURR_EGRESS_PORTS(pEntry,LocalPortList)                        \
{                                                                               \
    {                                                                           \
        VLAN_ADD_PORT_LIST (pEntry->EgressPorts, LocalPortList);                \
    }                                                                           \
}

#define VLAN_RESET_SINGLE_CURR_EGRESS_PORT(pEntry,u2Port)                       \
{                                                                               \
    {                                                                           \
        VLAN_RESET_MEMBER_PORT (pEntry->EgressPorts, u2Port);                   \
    }                                                                           \
}

#define VLAN_RESET_ALL_CURR_EGRESS_PORT(pEntry)                                 \
{                                                                               \
    {                                                                           \
        MEMSET (pEntry->EgressPorts, 0, sizeof (tLocalPortList));               \
    }                                                                           \
}

#define VLAN_RESET_CURR_EGRESS_PORTS(pEntry,LocalPortList)                      \
{                                                                               \
    {                                                                           \
        VLAN_RESET_PORT_LIST (pEntry->EgressPorts, LocalPortList);              \
    }                                                                           \
}

#define VLAN_GET_CURR_EGRESS_PORTS(pEntry,LocalPortList)                        \
{                                                                               \
    {                                                                           \
        MEMCPY (LocalPortList, pEntry->EgressPorts, sizeof(tLocalPortList));    \
    }                                                                           \
}

#define VLAN_IS_ALL_MATCHING_FOR_CURR_EGRESS(pEntry,LocalPortList,u1Result)     \
{                                                                               \
    {                                                                           \
        if (MEMCMP (pEntry->EgressPorts, LocalPortList,                         \
                sizeof (tLocalPortList)) == 0)                                  \
        {                                                                       \
            u1Result = VLAN_TRUE;                                               \
        }                                                                       \
        else                                                                    \
        {                                                                       \
            u1Result = VLAN_FALSE;                                              \
        }                                                                       \
    }                                                                           \
}

#define VLAN_ARE_PORTS_EXCL_WITH_CURR_EGRESS(pEntry,LocalPortList,u1Result)     \
{                                                                               \
    {                                                                           \
        VLAN_ARE_PORTS_EXCLUSIVE (pEntry->EgressPorts, LocalPortList,           \
                                         u1Result);                             \
    }                                                                           \
}

#define VLAN_ADD_CURR_EGRESS_PORTS_TO_LIST(pEntry,LocalPortList)                \
{                                                                               \
    {                                                                           \
        VLAN_ADD_PORT_LIST (LocalPortList, pEntry->EgressPorts);                \
    }                                                                           \
}

#define VLAN_OVERWRITE_CURR_EGRESS_PORTS(pEntry,LocalPortList)                  \
{                                                                               \
    {                                                                           \
        MEMCPY (pEntry->EgressPorts, LocalPortList, sizeof(tLocalPortList));    \
    }                                                                           \
}
#define VLAN_CONVERT_CURR_EGRESS_TO_IFPORTLIST(pEntry,PortList)                 \
{                                                                               \
    {                                                                           \
        VlanConvertToIfPortList (pEntry->EgressPorts, PortList);                \
    }                                                                           \
}

#define VLAN_IS_PORTLIST_SUBSET_TO_CURR_EGRESS(pEntry,LocalPortList,u1Result)   \
{                                                                               \
    {                                                                           \
        VLAN_IS_PORT_LIST_A_SUBSET (pEntry->EgressPorts, LocalPortList,         \
                                    u1Result);                                  \
    }                                                                           \
}

#define VLAN_IS_PORT_ONLY_MEMBER_FOR_CURR_EGRESS(pEntry,u2Port,u1Result)        \
{                                                                               \
    {                                                                           \
        VLAN_IS_PORT_ONLY_MEMBER(pEntry->EgressPorts, u2Port,                   \
                                         u1Result);                             \
    }                                                                           \
}

/*CURR LEARNT PORTS*/
/* Macros that are added for tVlanCurrEntry->LearntPorts*/
#define VLAN_IS_CURR_LEARNT_PORT(pEntry,u2Port,u1Result)                        \
{                                                                               \
    {                                                                           \
        VLAN_IS_MEMBER_PORT (pEntry->LearntPorts, u2Port, u1Result);            \
    }                                                                           \
}

#define VLAN_IS_NO_CURR_LEARNT_PORTS_PRESENT(pEntry,u1Result)                   \
{                                                                               \
    {                                                                           \
        if (MEMCMP (pEntry->LearntPorts, gNullPortList,                         \
                            sizeof (tLocalPortList)) == 0)                      \
        {                                                                       \
            u1Result = VLAN_TRUE;                                               \
        }                                                                       \
        else                                                                    \
        {                                                                       \
            u1Result = VLAN_FALSE;                                              \
        }                                                                       \
    }                                                                           \
}

#define VLAN_SET_SINGLE_CURR_LEARNT_PORT(pEntry,u2Port)                         \
{                                                                               \
    {                                                                           \
        VLAN_SET_MEMBER_PORT (pEntry->LearntPorts, u2Port);                     \
    }                                                                           \
}

#define VLAN_RESET_SINGLE_CURR_LEARNT_PORT(pEntry,u2Port)                       \
{                                                                               \
    {                                                                           \
        VLAN_RESET_MEMBER_PORT (pEntry->LearntPorts, u2Port);                   \
    }                                                                           \
}

#define VLAN_RESET_CURR_LEARNT_PORTS(pEntry,LocalPortList)                      \
{                                                                               \
    {                                                                           \
        VLAN_RESET_PORT_LIST (pEntry->LearntPorts, LocalPortList);              \
    }                                                                           \
}

#define VLAN_RESET_ALL_CURR_LEARNT_PORT(pEntry)                                 \
{                                                                               \
    {                                                                           \
        MEMSET (pEntry->LearntPorts, 0, sizeof (tLocalPortList));               \
    }                                                                           \
}

#define VLAN_GET_CURR_LEARNT_PORTS(pEntry,LocalPortList)                        \
{                                                                               \
    {                                                                           \
        MEMCPY (LocalPortList, pEntry->LearntPorts, sizeof(tLocalPortList));    \
    }                                                                           \
}

#define VLAN_ADD_CURR_LEARNT_PORTS_TO_LIST(pEntry,LocalPortList)                \
{                                                                               \
    {                                                                           \
        VLAN_ADD_PORT_LIST (LocalPortList, pEntry->LearntPorts);                \
    }                                                                           \
}

#define VLAN_CONVERT_CURR_LEARNT_TO_IFPORTLIST(pEntry,PortList)                 \
{                                                                               \
    {                                                                           \
        VlanConvertToIfPortList (pEntry->LearntPorts, PortList);                \
    }                                                                           \
}

/*CURR TRUNK PORTS*/
/* Macros that are added for tVlanCurrEntry->TrunkPorts*/
#define VLAN_IS_CURR_TRUNK_PORT(pEntry,u2Port,u1Result)                         \
{                                                                               \
    {                                                                           \
        VLAN_IS_MEMBER_PORT (pEntry->TrunkPorts, u2Port, u1Result);             \
    }                                                                           \
}

#define VLAN_SET_SINGLE_CURR_TRUNK_PORT(pEntry,u2Port)                          \
{                                                                               \
    {                                                                           \
        VLAN_SET_MEMBER_PORT (pEntry->TrunkPorts, u2Port);                      \
    }                                                                           \
}

#define VLAN_RESET_SINGLE_CURR_TRUNK_PORT(pEntry,u2Port)                        \
{                                                                               \
    {                                                                           \
        VLAN_RESET_MEMBER_PORT (pEntry->TrunkPorts, u2Port);                    \
    }                                                                           \
}

#define VLAN_RESET_ALL_CURR_TRUNK_PORT(pEntry)                                  \
{                                                                               \
    {                                                                           \
        MEMSET (pEntry->TrunkPorts, 0, sizeof (tLocalPortList));                \
    }                                                                           \
}

#define VLAN_SET_CURR_TRUNK_PORTS(pEntry,LocalPortList)                         \
{                                                                               \
    {                                                                           \
        VLAN_ADD_PORT_LIST (pEntry->TrunkPorts, LocalPortList);                 \
    }                                                                           \
}
#endif /* VLAN_PORTLIST_RBTREE_WANTED */
#define VLAN_DEC_FDB_COUNT(pCurrEntry,u1EntryType)                              \
{                                                                               \
    {                                                                           \
        if (NULL != pCurrEntry)                                                 \
        {                                                                       \
            if(u1EntryType == VLAN_FDB_LEARNT)                                  \
            {                                                                   \
                if (pCurrEntry->u4UnicastDynamicCount > 0)                      \
                {                                                               \
                    pCurrEntry->u4UnicastDynamicCount--;                        \
                }                                                               \
            }                                                                   \
            else if (u1EntryType == VLAN_FDB_MGMT)                \
            {                                                                   \
                if (pCurrEntry->u4UnicastStaticCount > 0)                       \
                {                                                               \
                    pCurrEntry->u4UnicastStaticCount--;                         \
                }                                                               \
            }                                                                   \
        }                                                                       \
    }                                                                           \
}
#endif /* _VLANMACS_H */
