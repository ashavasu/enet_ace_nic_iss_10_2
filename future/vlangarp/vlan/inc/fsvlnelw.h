/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvlnelw.h,v 1.11 2015/06/05 09:40:36 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVlanBridgeMode ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVlanBridgeMode ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVlanBridgeMode ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVlanBridgeMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVlanTunnelBpduPri ARG_LIST((INT4 *));

INT1
nmhGetFsVlanTunnelStpAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsVlanTunnelLacpAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsVlanTunnelDot1xAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsVlanTunnelGvrpAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsVlanTunnelGmrpAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsVlanTunnelElmiAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsVlanTunnelLldpAddress ARG_LIST((tMacAddr * ));
INT1
nmhGetFsVlanTunnelEcfmAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsVlanTunnelEoamAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsVlanTunnelIgmpAddress ARG_LIST((tMacAddr * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVlanTunnelBpduPri ARG_LIST((INT4 ));

INT1
nmhSetFsVlanTunnelStpAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsVlanTunnelLacpAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsVlanTunnelDot1xAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsVlanTunnelGvrpAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsVlanTunnelGmrpAddress ARG_LIST((tMacAddr ));
INT1
nmhSetFsVlanTunnelElmiAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsVlanTunnelLldpAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsVlanTunnelEcfmAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsVlanTunnelEoamAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsVlanTunnelIgmpAddress ARG_LIST((tMacAddr ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVlanTunnelBpduPri ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelStpAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsVlanTunnelLacpAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsVlanTunnelDot1xAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsVlanTunnelGvrpAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsVlanTunnelGmrpAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsVlanTunnelElmiAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsVlanTunnelLldpAddress ARG_LIST((UINT4 *  ,tMacAddr ));
INT1
nmhTestv2FsVlanTunnelEcfmAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsVlanTunnelEoamAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsVlanTunnelIgmpAddress ARG_LIST((UINT4 *  ,tMacAddr ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVlanTunnelBpduPri ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVlanTunnelStpAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVlanTunnelLacpAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVlanTunnelDot1xAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVlanTunnelGvrpAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVlanTunnelGmrpAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhDepv2FsVlanTunnelElmiAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVlanTunnelLldpAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVlanTunnelEcfmAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVlanTunnelEoamAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVlanTunnelIgmpAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVlanTunnelTable. */
INT1
nmhValidateIndexInstanceFsVlanTunnelTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVlanTunnelTable  */

INT1
nmhGetFirstIndexFsVlanTunnelTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVlanTunnelTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVlanTunnelStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVlanTunnelStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVlanTunnelStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVlanTunnelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVlanTunnelProtocolTable. */
INT1
nmhValidateIndexInstanceFsVlanTunnelProtocolTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVlanTunnelProtocolTable  */

INT1
nmhGetFirstIndexFsVlanTunnelProtocolTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVlanTunnelProtocolTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVlanTunnelProtocolDot1x ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelProtocolLacp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelProtocolStp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelProtocolGvrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelProtocolGmrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelProtocolIgmp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelProtocolMvrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelProtocolMmrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelProtocolElmi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelProtocolLldp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelProtocolEcfm ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelOverrideOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVlanTunnelProtocolEoam ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVlanTunnelProtocolDot1x ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelProtocolLacp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelProtocolStp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelProtocolGvrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelProtocolGmrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelProtocolIgmp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelProtocolMvrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelProtocolMmrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelProtocolElmi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelProtocolLldp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelProtocolEcfm ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelOverrideOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVlanTunnelProtocolEoam ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVlanTunnelProtocolDot1x ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelProtocolLacp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelProtocolStp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelProtocolGvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelProtocolGmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelProtocolIgmp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelProtocolMvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelProtocolMmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelProtocolElmi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelProtocolLldp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelProtocolEcfm ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelOverrideOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVlanTunnelProtocolEoam ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVlanTunnelProtocolTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVlanTunnelProtocolStatsTable. */
INT1
nmhValidateIndexInstanceFsVlanTunnelProtocolStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVlanTunnelProtocolStatsTable  */

INT1
nmhGetFirstIndexFsVlanTunnelProtocolStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVlanTunnelProtocolStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVlanTunnelProtocolDot1xPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolDot1xPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolLacpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolLacpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolStpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolStpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolGvrpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolGvrpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolGmrpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolGmrpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolIgmpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolIgmpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolMvrpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolMvrpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolMmrpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolMmrpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolElmiPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolElmiPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolLldpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolLldpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolEcfmPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolEcfmPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolEoamPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanTunnelProtocolEoamPktsSent ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsVlanDiscardStatsTable. */
INT1
nmhValidateIndexInstanceFsVlanDiscardStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVlanDiscardStatsTable  */

INT1
nmhGetFirstIndexFsVlanDiscardStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVlanDiscardStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVlanDiscardDot1xPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardDot1xPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardLacpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardLacpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardStpPDUsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardStpPDUsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardGvrpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardGvrpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardGmrpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardGmrpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardIgmpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardIgmpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardMvrpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardMvrpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardMmrpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardMmrpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardElmiPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardElmiPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardLldpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardLldpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardEcfmPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardEcfmPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardEoamPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVlanDiscardEoamPktsTx ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVlanTunnelMvrpAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsVlanTunnelMmrpAddress ARG_LIST((tMacAddr * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVlanTunnelMvrpAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsVlanTunnelMmrpAddress ARG_LIST((tMacAddr ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVlanTunnelMvrpAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsVlanTunnelMmrpAddress ARG_LIST((UINT4 *  ,tMacAddr ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVlanTunnelMvrpAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVlanTunnelMmrpAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
VlanSetFsVlanTunnelStatus ARG_LIST ((INT4 , INT4 ));

/* Proto Validate Index Instance for FsServiceVlanTunnelProtocolTable. */
INT1
nmhValidateIndexInstanceFsServiceVlanTunnelProtocolTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsServiceVlanTunnelProtocolTable  */

INT1
nmhGetFirstIndexFsServiceVlanTunnelProtocolTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsServiceVlanTunnelProtocolTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsServiceVlanRsvdMacaddress ARG_LIST((INT4  , INT4 ,tMacAddr * ));

INT1
nmhGetFsServiceVlanTunnelMacaddress ARG_LIST((INT4  , INT4 ,tMacAddr * ));

INT1
nmhGetFsServiceVlanTunnelProtocolStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsServiceVlanTunnelPktsRecvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsServiceVlanTunnelPktsSent ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsServiceVlanDiscardPktsRx ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsServiceVlanDiscardPktsTx ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsServiceVlanRsvdMacaddress ARG_LIST((INT4  , INT4  ,tMacAddr ));

INT1
nmhSetFsServiceVlanTunnelMacaddress ARG_LIST((INT4  , INT4  ,tMacAddr ));

INT1
nmhSetFsServiceVlanTunnelProtocolStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsServiceVlanRsvdMacaddress ARG_LIST((UINT4 *  ,INT4  , INT4  ,tMacAddr ));

INT1
nmhTestv2FsServiceVlanTunnelMacaddress ARG_LIST((UINT4 *  ,INT4  , INT4  ,tMacAddr ));

INT1
nmhTestv2FsServiceVlanTunnelProtocolStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsServiceVlanTunnelProtocolTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
