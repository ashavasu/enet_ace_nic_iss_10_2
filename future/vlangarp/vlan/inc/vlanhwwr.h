/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanhwwr.h,v 1.34 2016/03/03 10:19:52 siva Exp $
 *
 * Description: This file contains wrappers used in VLAN module.
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2005-2006                        */
/*                                                                           */
/*  FILE NAME             : vlanhwwr.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN Module                                      */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Jul 2006                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains Prototype for                 */
/*                          VLAN H/W Wrapper routines.                       */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 1.0                                              */
/*  Date(DD/MM/YYYY)      : 26/07/2007                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/
#ifndef _VLANHWWR_H
#define _VLANHWWR_H


INT4 VlanHwSetAgingTime PROTO((UINT4 u4ContextId, INT4 i4AgingTime));

INT4 VlanHwSetShortAgingTime PROTO((UINT4 u4ContextId, UINT2 u2Port,
                                    INT4 i4AgingTime));

INT4 VlanHwResetShortAgingTime PROTO((UINT4 u4ContextId, UINT2 u2Port,
                                      INT4 i4AgingTime));

INT4 VlanGetDot1dStats PROTO((INT4 i4Port, INT1 i1StatType, UINT4 *pu4Value));

INT4 VlanGetStats PROTO((INT4 i4Port, tVlanId VlanId, INT1 i1StatType,
                         UINT4 *pu4Value));

INT4 VlanGetStats64 PROTO((INT4 i4Port, tVlanId VlanId, INT1 i1StatType,
                           tSNMP_COUNTER64_TYPE  *pu8Value));

INT4 VlanHwSetDefGroupInfo PROTO((UINT4 u4ContextId, tVlanId VlanId,
                                  UINT1 u1Type, tLocalPortList PortList));

INT4 VlanHwResetDefGroupInfo PROTO((UINT4 u4ContextId, tVlanId VlanId,
                                    UINT1 u1Type, tLocalPortList PortList));


INT4 VlanHwAddStMcastEntry PROTO((tVlanId VlanId, tMacAddr MacAddr,
                                  UINT4 u4RcvPort, tLocalPortList HwPortList));

INT4 VlanHwAddMcastEntry PROTO((UINT4 u4ContextId, tVlanId VlanId,
                                tMacAddr MacAddr, tLocalPortList HwPortList));

INT4 VlanHwAddVlanEntry PROTO((tVlanId VlanId,
                               tLocalPortList HwPortList,
                               tLocalPortList HwUnTagPorts));

INT4 VlanHwAddStaticUcastEntry PROTO((UINT4 u4Fid, tMacAddr MacAddress,
                                      UINT4 u4RcvPort,
                                      tLocalPortList AllowedToGoPort,
                                      UINT1 u1Status, tMacAddr ConnectionId));

INT4 VlanHwDelStMcastEntry PROTO((tVlanId VlanId,
                                  tMacAddr MacAddr, UINT4 u4RcvPort));

INT4 VlanHwDelMcastEntry PROTO((UINT4 u4ContextId, tVlanId VlanId,
                                tMacAddr MacAddr));

INT4 VlanHwDelVlanEntry PROTO((UINT4 u4ContextId, tVlanId VlanId));

INT4 VlanHwDelStaticUcastEntry PROTO((UINT4 u4Fid, tMacAddr MacAddr,
                                      UINT4 u4IfIndex));

INT4 VlanHwAddDynVlanEntry PROTO(( tVlanId VlanId, UINT2 u2Port));

INT4 VlanHwDelDynVlanEntry PROTO((tVlanId VlanId, UINT2 u2Port));

INT4 VlanHwSetVlanMemberPort PROTO((tVlanId VlanId, UINT2 u2Port,
                                    UINT1 u1IsTagged));

INT4 VlanHwResetVlanMemberPort PROTO((tVlanId VlanId,
                                      UINT2 u2Port));

INT4 VlanHwAddDynMcastEntry PROTO((tVlanId VlanId, tMacAddr MacAddr,
                                   UINT2 u2Port));

INT4 VlanHwSetMcastPort PROTO((tVlanId VlanId, tMacAddr MacAddr, UINT2 u2Port));

INT4 VlanHwResetMcastPort PROTO((tVlanId VlanId, tMacAddr MacAddr,
                                 UINT2 u2Port));

INT4 VlanHwDelDynMcastEntry PROTO((tVlanId VlanId, tMacAddr MacAddr,
                                   UINT2 u2Port));

INT4 VlanHwDelVlanProtocolMap PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                                     UINT4 u4GroupId,
                                     tVlanProtoTemplate * pProtoTemplate));

INT4 VlanHwAddVlanProtocolMap PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                                     UINT4 u4GroupId,
                                     tVlanProtoTemplate * pProtoTemplate,
                                     tVlanId VlanId));

INT4 VlanHwGetFdbCount PROTO((UINT4 u4ContextId, UINT4 u4FdbId,
                              UINT4 *pu4Count));

INT4 VlanHwFlushPort PROTO((UINT4 u4ContextId, UINT2 u2Port,
                            INT4 i4OptimizeFlag));

INT4 VlanHwFlushPortFdbId PROTO((UINT4 u4ContextId, UINT2 u2Port,
                                 UINT4 u4FdbId, INT4 i4OptimizeFlag)); 

INT4 VlanHwFlushFdbId PROTO((UINT4 u4ContextId, UINT4 u4FdbId)); 

INT4
VlanHwFlushFdbList (UINT4 u4ContextId,tVlanFlushInfo *pVlanFlushInfo);


INT4 VlanHwDeleteAllFdbEntries PROTO((UINT4 u4ContextId)); 

INT4 VlanHwGetFirstTpFdbEntry PROTO((UINT4 u4ContextId, UINT4 *pu4FdbId,
                                     tMacAddr MacAddr));

INT4 VlanHwGetNextTpFdbEntry PROTO((UINT4 u4ContextId, UINT4 u4FdbId,
                                    tMacAddr MacAddr, UINT4 *pu4NextContextId,
                                    UINT4 *pu4NextFdbId, tMacAddr NextMacAddr));
#ifdef NPAPI_WANTED

INT4 VlanHwGetFdbEntry PROTO((UINT4 u4ContextId, UINT4 u4FdbId,
                              tMacAddr MacAddr, tHwUnicastMacEntry * pEntry));

INT4 VlanHwGetVlanInfo PROTO((UINT4 u4ContextId, tVlanId VlanId,
                              tHwMiVlanEntry * pHwEntry));
#endif

INT4 VlanHwSetDefUserPriority PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                                     INT4 i4DefPriority));

INT4 VlanHwSetPortNumTrafClasses PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                                        INT4 i4NumTraffClass));

INT4 VlanHwSetRegenUserPriority PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                                       INT4 i4UserPriority,
                                       INT4 i4RegenPriority));

INT4 VlanHwSetTraffClassMap PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                                   INT4 i4UserPriority, INT4 i4TraffClass));


INT4 VlanEnablePriModule PROTO((VOID));

INT4 VlanDisablePriModule PROTO((VOID));

INT4 VlanHwDeInit PROTO((UINT4 u4ContextId));


INT4 VlanHwSetPortPvid PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                              tVlanId VlanId));

INT4 VlanHwSetPortAccFrameType PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                                      INT1 u1AccFrameType));

INT4 VlanHwSetPortIngFiltering PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                                      UINT1 u1IngFilterEnable));

INT4 VlanHwSetMacBasedStatusOnPort PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                                          UINT1 u1MacBasedVlanEnable));

INT4 VlanHwSetSubnetBasedStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                                       UINT1 u1SubnetBasedVlanEnable);

INT4 VlanHwEnableProtoVlanOnPort PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                                        UINT1 u1VlanProtoEnable));
INT4 VlanHwAddPortMacVlanEntry  (UINT4 u4ContextId, UINT4 u4Port,
                                 tMacAddr MacAddr,tVlanId VlanId,
                                 BOOL1 bSuppressOption);
INT4 VlanHwDeletePortMacVlanEntry (UINT4 u4ContextId, UINT4 u4Port,
                                   tMacAddr MacAddr);

INT4
VlanHwAddPortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4Port, UINT4 SubnetAddr,
                              UINT4 u4SubnetMask, tVlanId VlanId,
                              BOOL1 bArpOption);

INT4
VlanHwDeletePortSubnetVlanEntry (UINT4 u4ContextId, UINT4 u4Port,
                                 UINT4 SubnetAddr, UINT4 u4SubnetMask);
   
INT4 VlanHwSetPortTunnelMode PROTO((UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT4 u4Mode));

INT4 VlanHwSetTunnelFilter PROTO((UINT4 u4ContextId, INT4 i4BridgeMode));

INT4 VlanHwCheckTagAtEgress PROTO((UINT4 u4ContextId, UINT1 *pu1TagSet));

INT4 VlanHwCheckTagAtIngress PROTO((UINT4 u4ContextId, UINT1 *pu1TagSet));

#ifdef L2RED_WANTED
INT4 VlanHwGetVlanProtocolMap PROTO((UINT4 u4ContextId, UINT2 u2IfIndex,
                                     UINT4 u4GroupId,
                                     tVlanProtoTemplate * pProtoTemplate,
                                     tVlanId * pVlanId));
#ifdef NPAPI_WANTED
INT4 VlanHwScanProtocolVlanTbl PROTO((UINT4 u4ContextId, UINT4 u4Port,
                                      FsMiVlanHwProtoCb ProtoVlanCallBack));

INT4 VlanHwScanMulticastTbl PROTO((UINT4 u4ContextId,
                                   FsMiVlanHwMcastCb McastCallBack));

INT4 VlanHwScanUnicastTbl PROTO((UINT4 u4ContextId,
                                 FsMiVlanHwUcastCb UcastCallBack));
#endif

INT4 VlanHwGetStMcastEntry PROTO((UINT4 u4ContextId, tVlanId VlanId,
                                  tMacAddr MacAddr, UINT4 u4RcvPort,
                                  tLocalPortList HwPortList));


INT4 VlanHwGetStaticUcastEntry PROTO((UINT4 u4ContextId, UINT4 u4Fid,
                                      tMacAddr MacAddr, UINT2 u2Port,
                                      tLocalPortList HwPortList,
                                      UINT1 *pu1Status, 
                                      tMacAddr ConnectionId));

INT4 VlanHwGetMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                                 tMacAddr MacAddr, tLocalPortList PortList));

INT4  VlanHwSyncDefaultVlanId PROTO ((UINT4 u4ContextId, tVlanId VlanId));
INT4
VlanRedHwUpdateDBForDefaultVlanId (UINT4 u4ContextId, tVlanId VlanId);
#endif
INT4 VlanHwVlanDisable PROTO((UINT4 u4ContextId));

INT4 VlanHwInit PROTO((UINT4 u4ContextId));

INT4 VlanHwTraffClassMapInit PROTO((UINT4 u4ContextId, UINT1 u1Priority,
                                    INT4 i4CosqValue));

INT4 VlanHwVlanEnable PROTO((UINT4 u4ContextId));

INT4 VlanHwSetVlanLearningType PROTO((UINT4 u4ContextId, UINT1 u1LearningType));

INT4 VlanHwAssociateVlanFdb PROTO((UINT4 u4ContextId, UINT4 u4Fid,
                                   tVlanId VlanId));

INT4 VlanHwDisassociateVlanFdb PROTO((UINT4 u4ContextId, UINT4 u4Fid,
                                      tVlanId VlanId));

INT4 VlanHwCreateFdbId PROTO((UINT4 u4ContextId, UINT4 u4Fid));

INT4 VlanHwDeleteFdbId PROTO((UINT4 u4ContextId, UINT4 u4Fid));

INT4  VlanHwSetBrgMode PROTO ((UINT4 u4ContextId, UINT4 u4BridgeMode));

INT4 VlanHwSetBaseBrgMode PROTO ((UINT4 u4ContextId, UINT4 u4BaseBridgeMode));
    
INT4  VlanHwSetDefaultVlanId PROTO ((UINT4 u4ContextId, tVlanId u2DefaultVlanId));
INT4
VlanHwSetProviderBridgePortType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT4 u4PortType));

INT4
VlanHwSetPortIngressEtherType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                      UINT2 u2EtherType));

INT4
VlanHwSetPortEgressEtherType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT2 u2EtherType));

INT4
VlanHwSetPortSVlanTranslationStatus PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT1 u1Status));

INT4
VlanHwAddSVlanTranslationEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT2 u2LocalSVlan, UINT2 u2RelaySVlan));

INT4
VlanHwDelSVlanTranslationEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT2 u2LocalSVlan, UINT2 u2RelaySVlan));

INT4
VlanHwSetPortEtherTypeSwapStatus PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT1 u1Status));

INT4
VlanHwAddEtherTypeSwapEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2LocalEtherType,
                                    UINT2 u2RelayEtherType));

INT4
VlanHwDelEtherTypeSwapEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                             UINT2 u2LocalEtherType, UINT2 u2RelayEtherType));

INT4
VlanHwAddSVlanMap PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap));

INT4
VlanHwDeleteSVlanMap PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap));


INT4
VlanHwSetPortSVlanClassifyMethod PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                         UINT1 u1TableType));

INT4
VlanHwPortMacLearningStatus PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                             UINT1 u1Status));
INT4
VlanHwPortUnicastMacSecType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                   INT4 i4MacSecType));

INT4
VlanHwPortMacLearningLimit PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                   UINT4 u4MacLimit));

INT4
VlanHwFidMacLearningLimit PROTO ((UINT4 u4ContextId, UINT2 u2Fid,
                                  UINT4 u4MacLimit));

INT4
VlanHwMulticastMacTableLimit PROTO ((UINT4 u4ContextId, UINT4 u4MacLimit));

INT4
VlanHwSetPortCustomerVlan PROTO ((UINT4 u4ContextId, UINT2 u2Port,
                                  tVlanId CVlanId));

INT4
VlanHwResetPortCustomerVlan PROTO ((UINT4 u4ContextId, UINT2 u2Port));

INT4
VlanHwCreateProviderEdgePort PROTO ((UINT4 u4ContextId, UINT2 u2IfIndex,
                                     tVlanId SVlanId, 
                                     tHwVlanPbPepInfo PepConfig));

INT4
VlanHwDelProviderEdgePort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                  tVlanId SVlanId));
INT4
VlanHwSetPepPvid PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId SVlanId,
                  tVlanId Pvid));

INT4
VlanHwSetPepAccFrameType PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                 tVlanId SVlanId, UINT1 u1AccepFrameType));

INT4
VlanHwSetPepDefUserPriority PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                             tVlanId SVlanId, INT4 i4DefUsrPri));

INT4
VlanHwSetPepIngFiltering PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                 tVlanId SVlanId, UINT1 u1IngFilterEnable));

INT4
VlanHwSetPcpEncodTbl PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                             tHwVlanPbPcpInfo NpPbVlanPcpInfo));

INT4
VlanHwSetPcpDecodTbl PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                             tHwVlanPbPcpInfo NpPbVlanPcpInfo));

INT4
VlanHwSetPortUseDei PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                            UINT1 u1UseDei));

INT4
VlanHwSetPortReqDropEncoding PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                     UINT1 u1ReqDrpEncoding));

INT4
VlanHwSetPortPcpSelection PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                                  UINT2 u2PcpSelection));

INT4
VlanHwSetPortProperty PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                              tHwVlanPortProperty VlanPortProperty));

INT4
VlanHwSetServicePriRegenEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                               tVlanId SVlanId, INT4 i4RecvPriority,
                               INT4 i4RegenPriority));
INT4
VlanHwSetTunnelMacAddress PROTO ((UINT4 u4ContextId, tMacAddr MacAddr, 
                               UINT2 u2Protocol));
INT4
VlanHwSetCvidUntagPep PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap));

INT4
VlanHwSetCvidUntagCep PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap));

VOID VlanHandleMacBasedOnPort (UINT4 u4Port, UINT1 u1Flag);
VOID VlanHandleSubnetBasedOnPort (UINT4 u4Port, UINT1 u1Flag);
INT4 VlanHwGetVlanStats (INT4 u4ContextId, UINT4 i4VlanId, UINT1 i1StatType, 
                         UINT4 *pu4Value);
INT4 VlanHwMacLearningStatus (UINT4 u4ContextId, tVlanId VlanId,
                              tLocalPortList EgressPorts, UINT1 u1Status);
INT4 VlanHwMacLearningLimit (UINT4 u4ContextId, tVlanId VlanId,
                             UINT4 u4MacLimit);


INT4 FsMiWrVlanHwSetAllGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, tLocalPortList Ports));
INT4 FsMiWrVlanHwResetAllGroupsPorts  PROTO ((UINT4 u4ContextId, tVlanId, tLocalPortList));
INT4 FsMiWrVlanHwSetUnRegGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId, tLocalPortList Ports));
INT4 FsMiWrVlanHwResetUnRegGroupsPorts PROTO ((UINT4 u4ContextId, tVlanId, tLocalPortList));
INT4 FsMiWrVlanHwAddStaticUcastEntry PROTO ((UINT4 u4ContextId, UINT4 u4Fid, tMacAddr MacAddr, 
                                UINT4 u4RcvPort, tLocalPortList AllowedToGoPorts, 
    UINT1 u1Status, tMacAddr ConnectionId));
INT4 FsMiWrVlanHwAddMcastEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, 
                        tLocalPortList PortList));

INT4 FsMiWrVlanHwAddStMcastEntry PROTO ((UINT4 u4ContextId, tVlanId, tMacAddr , INT4, tLocalPortList));
INT4 FsMiWrVlanHwAddVlanEntry PROTO ((UINT4 u4ContextId, tVlanId VlanId, tLocalPortList HwPortList, 
                        tLocalPortList UnTagPorts));
INT4 FsMiWrVlanHwMacLearningStatus (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2FdbId,
                                    tLocalPortList PortBmp, UINT1 u1Status);
INT4 FsMiWrVlanHwSetBaseBridgeMode PROTO ((INT4, INT4));
INT4 FsMiWrVlanHwGetVlanInfo (UINT4 u4ContextId, tVlanId VlanId,
                              tHwMiVlanEntry * pHwEntry);
INT4
FsMiWrVlanHwGetMcastEntry (UINT4 u4ContextId, tVlanId VlanId, tMacAddr MacAddr, 
                           tLocalPortList PortList);
INT4
VlanHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid,
                                tLocalPortList HwPortList, UINT2 u2Port, 
                                UINT1 u1Action);

INT4
FsMiWrVlanHwSetUnRegGroupsPortsForSisp (UINT4 u4ContextId, tVlanId VlanId,
                                        tLocalPortList PortBmp, UINT4 u4PortId);

INT4
FsMiWrVlanHwSetAllGroupsPortsForSisp (UINT4 u4ContextId, tVlanId VlanId,
                                      tLocalPortList PortBmp, UINT4 u4PortId);

INT4
FsMiWrVlanHwResetAllGroupsPortsForSisp (UINT4 u4ContextId, tVlanId VlanId,
                                        tLocalPortList PortBmp,UINT4 u4PortId);

INT4
FsMiWrVlanHwResetUnRegGroupsPortsForSisp (UINT4 u4ContextId, tVlanId VlanId,
                                          tLocalPortList PortBmp, UINT4 u4PortId);

INT4
FsMiWrVlanHwEvbConfigSChIface (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo);

INT4
FsMiWrVlanHwSetBridgePortType (tVlanHwPortInfo *pVlanHwPortInfo);
#ifdef L2RED_WANTED
INT4 FsMiWrVlanHwGetStMcastEntry (UINT4 u4ContextId, tVlanId VlanId, 
                                  tMacAddr MacAddr, UINT4 u4RcvPort, 
                                  tLocalPortList HwPortList);
INT4 FsMiWrVlanHwGetStaticUcastEntry (UINT4 u4ContextId, UINT4 u4Fid, 
                                      tMacAddr MacAddr, UINT4 u4Port, 
                                      tLocalPortList AllowedToGoPortBmp,
                                      UINT1 *pu1Status);
INT4 FsMiWrVlanHwGetStaticUcastEntryEx (UINT4 u4ContextId, UINT4 u4Fid, 
                                      tMacAddr MacAddr, UINT4 u4Port, 
                                      tLocalPortList AllowedToGoPortBmp,
                                      UINT1 *pu1Status, tMacAddr ConnectionId);
INT4 VlanHwGetSyncedTnlProtocolMacAddr (UINT4 u4ContextId, UINT2 u2Protocol,
                                        tMacAddr MacAddr);

#endif
#ifdef MBSM_WANTED
INT4 FsMiWrVlanMbsmHwAddVlanEntry         (UINT4, tVlanId, tLocalPortList, 
                                           tLocalPortList, tMbsmSlotInfo *);
INT4 FsMiWrVlanMbsmHwAddStaticUcastEntry  (UINT4, UINT4, tMacAddr, UINT4, 
                                           tLocalPortList, UINT1, 
                                           tMbsmSlotInfo *, tMacAddr);
INT4 FsMiWrVlanMbsmHwAddStMcastEntry      (UINT4, tVlanId, tMacAddr, INT4, 
                                           tLocalPortList, tMbsmSlotInfo *);
INT4 FsMiWrVlanMbsmHwAddMcastEntry        (UINT4, tVlanId, tMacAddr, 
                                           tLocalPortList, tMbsmSlotInfo *);
INT4 FsMiWrVlanMbsmHwSetAllGroupsPorts    (UINT4, tVlanId, tLocalPortList, 
                                           tMbsmSlotInfo *);
INT4 FsMiWrVlanMbsmHwSetUnRegGroupsPorts  (UINT4, tVlanId, tLocalPortList, 
                                           tMbsmSlotInfo *);
INT4
FsMiWrVlanMbsmHwEvbConfigSChIface (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo,
                                 tMbsmSlotInfo * pSlotInfo);

INT4 
FsMiWrVlanMbsmHwSetBridgePortType   (tVlanHwPortInfo *pVlanHwPortInfo ,
                                         tMbsmSlotInfo * pSlotInfo);
#endif /* MBSM_WANTED */
INT4 VlanHwGetNextSVlanMap (UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap,
                            tVlanSVlanMap *pRetVlanSVlanMap);

INT4
VlanHwGetNextSVlanTranslationEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                    UINT2 u2LocalSVlan,
                                    tVidTransEntryInfo *pVidTransEntryInfo);

INT4
VlanHwSetEvcAttribute (INT4 i4FsEvcContextId, UINT4 u4Action, tEvcInfo * pIssEvcInfo);

INT4
FsMiWrVlanHwSetFidPortLearningStatus (UINT4 u4ContextId, UINT4 u4Fid,
                                      tPortList PortBmp, UINT4 u4Port,
                                      UINT1 u1Action);
INT4
VlanHwSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                                tVlanHwTunnelFilters ProtocolId,
                                               INT4 i4TunnelProtocolStp);
INT4
VlanHwSetProtectedStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                               INT4 i4SetValDot1qFutureVlanPortProtected);

INT4 
VlanHwSwitchMacLearningLimit (UINT4 u4ContextId, UINT4 u4MacLimit);

INT4
VlanHwResetDefGroupInfoForSisp (UINT4 u4ContextId, tVlanId VlanId,
                                UINT1 u1Type, tLocalPortList HwPortList,UINT4 u4DstPort);
INT4
VlanHwSetDefGroupInfoForSisp (UINT4 u4ContextId, tVlanId VlanId,
                              UINT1 u1Type, tLocalPortList HwPortList, UINT4 u4DstPort);
INT4 VlanHwResetDefGroupInfoForPort (UINT4 u4ContextId, tVlanId VlanId,
                                     UINT4 u4IfIndex, UINT1 u1Type);
INT4 VlanMiHwResetVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                                  UINT4 u4IfIndex);

INT4
FsMiWrVlanHwResetDefGroupInfoForPort (UINT4 u4ContextId, tVlanId VlanId,
                                      UINT4 u4IfIndex, UINT1 u1Type);

INT4
VlanHwForwardOnPortList (UINT4 u4ContextId, tVlanTag * pVlanTag, tCRU_BUF_CHAIN_HEADER * pFrame,
                         tLocalPortList PortList);
INT4
VlanHwSetMcastIndex (tMacAddr MacAddr,tVlanId VlanId);

INT4
VlanHwSetVlanLoopbackStatus PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                                     INT4 i4LoopbackStatus));

INT4
VlanHwGetCVlanStats (INT4 i4ContextId,INT4 u2Port ,UINT4 u4CVlanId,UINT1 i1StatType,
                     UINT4 *pu4Value);

INT4
VlanHwClearCVlanStats (INT4 i4ContextId, INT4 u2Port, UINT2 u2CVlanId);

INT4
VlanHwEvbConfigSChIface (tVlanEvbHwConfigInfo *pVlanEvbHwConfigInfo);
INT4
VlanHwSetBridgePortType (tVlanHwPortInfo  *pVlanHwPortInfo);

#endif /* VLANHWWR_H */
