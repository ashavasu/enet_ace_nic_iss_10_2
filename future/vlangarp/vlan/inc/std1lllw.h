/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1lllw.h,v 1.2 2016/07/16 11:15:03 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for LldpXdot1EvbConfigEvbTable. */
INT1
nmhValidateIndexInstanceLldpXdot1EvbConfigEvbTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1EvbConfigEvbTable  */

INT1
nmhGetFirstIndexLldpXdot1EvbConfigEvbTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1EvbConfigEvbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1EvbConfigEvbTxEnable ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1EvbConfigEvbTxEnable ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1EvbConfigEvbTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1EvbConfigEvbTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1EvbConfigCdcpTable. */
INT1
nmhValidateIndexInstanceLldpXdot1EvbConfigCdcpTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1EvbConfigCdcpTable  */

INT1
nmhGetFirstIndexLldpXdot1EvbConfigCdcpTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1EvbConfigCdcpTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1EvbConfigCdcpTxEnable ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1EvbConfigCdcpTxEnable ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1EvbConfigCdcpTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1EvbConfigCdcpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpV2Xdot1LocEvbTlvTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1LocEvbTlvTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1LocEvbTlvTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1LocEvbTlvTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1LocEvbTlvTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1LocEvbTlvString ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpV2Xdot1LocCdcpTlvTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1LocCdcpTlvTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1LocCdcpTlvTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1LocCdcpTlvTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1LocCdcpTlvTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1LocCdcpTlvString ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpV2Xdot1RemEvbTlvTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1RemEvbTlvTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1RemEvbTlvTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1RemEvbTlvTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1RemEvbTlvTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1RemEvbTlvString ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for LldpV2Xdot1RemCdcpTlvTable. */
INT1
nmhValidateIndexInstanceLldpV2Xdot1RemCdcpTlvTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpV2Xdot1RemCdcpTlvTable  */

INT1
nmhGetFirstIndexLldpV2Xdot1RemCdcpTlvTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpV2Xdot1RemCdcpTlvTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpV2Xdot1RemCdcpTlvString ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
