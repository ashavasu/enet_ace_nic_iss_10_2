/*************************************************************************    
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: std1llwr.h,v 1.1 2015/09/10 13:21:49 siva Exp $
* Description: This header file contains all prototype of the wrapper
*              routines in VLAN Modules.
****************************************************************************/

#ifndef _STD1LLWR_H
#define _STD1LLWR_H
INT4 GetNextIndexLldpXdot1EvbConfigEvbTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTD1LL(VOID);

VOID UnRegisterSTD1LL(VOID);
INT4 LldpXdot1EvbConfigEvbTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1EvbConfigEvbTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1EvbConfigEvbTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1EvbConfigEvbTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1EvbConfigCdcpTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1EvbConfigCdcpTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1EvbConfigCdcpTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1EvbConfigCdcpTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1EvbConfigCdcpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpV2Xdot1LocEvbTlvTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1LocEvbTlvStringGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1LocCdcpTlvTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1LocCdcpTlvStringGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1RemEvbTlvTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1RemEvbTlvStringGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpV2Xdot1RemCdcpTlvTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpV2Xdot1RemCdcpTlvStringGet(tSnmpIndex *, tRetVal *);
#endif /* _STD1LLWR_H */


