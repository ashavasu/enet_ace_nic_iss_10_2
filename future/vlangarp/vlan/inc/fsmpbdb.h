/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbdb.h,v 1.11 2016/01/11 12:56:20 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPBDB_H
#define _FSMPBDB_H

UINT1 FsMIPbContextInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPbPortInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPbSrcMacSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIPbDstMacSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIPbCVlanSrcMacSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIPbCVlanDstMacSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIPbDscpSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPbCVlanDscpSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPbSrcIpAddrSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIPbDstIpAddrSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIPbSrcDstIpSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIPbCVlanDstIpSVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIPbPortBasedCVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPbEtherTypeSwapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPbSVlanConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIPbTunnelProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPbTunnelProtocolStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIPbPepExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIPbPortCVlanCounterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmpb [] ={1,3,6,1,4,1,2076,127};
tSNMP_OID_TYPE fsmpbOID = {8, fsmpb};


UINT4 FsMIPbContextId [ ] ={1,3,6,1,4,1,2076,127,1,1,1,1};
UINT4 FsMIPbMulticastMacLimit [ ] ={1,3,6,1,4,1,2076,127,1,1,1,2};
UINT4 FsMIPbTunnelStpAddress [ ] ={1,3,6,1,4,1,2076,127,1,1,1,3};
UINT4 FsMIPbTunnelLacpAddress [ ] ={1,3,6,1,4,1,2076,127,1,1,1,4};
UINT4 FsMIPbTunnelDot1xAddress [ ] ={1,3,6,1,4,1,2076,127,1,1,1,5};
UINT4 FsMIPbTunnelGvrpAddress [ ] ={1,3,6,1,4,1,2076,127,1,1,1,6};
UINT4 FsMIPbTunnelGmrpAddress [ ] ={1,3,6,1,4,1,2076,127,1,1,1,7};
UINT4 FsMIPbPort [ ] ={1,3,6,1,4,1,2076,127,2,1,1,1};
UINT4 FsMIPbPortSVlanClassificationMethod [ ] ={1,3,6,1,4,1,2076,127,2,1,1,2};
UINT4 FsMIPbPortSVlanIngressEtherType [ ] ={1,3,6,1,4,1,2076,127,2,1,1,3};
UINT4 FsMIPbPortSVlanEgressEtherType [ ] ={1,3,6,1,4,1,2076,127,2,1,1,4};
UINT4 FsMIPbPortSVlanEtherTypeSwapStatus [ ] ={1,3,6,1,4,1,2076,127,2,1,1,5};
UINT4 FsMIPbPortSVlanTranslationStatus [ ] ={1,3,6,1,4,1,2076,127,2,1,1,6};
UINT4 FsMIPbPortUnicastMacLearning [ ] ={1,3,6,1,4,1,2076,127,2,1,1,7};
UINT4 FsMIPbPortUnicastMacLimit [ ] ={1,3,6,1,4,1,2076,127,2,1,1,8};
UINT4 FsMIPbPortBundleStatus [ ] ={1,3,6,1,4,1,2076,127,2,1,1,9};
UINT4 FsMIPbPortMultiplexStatus [ ] ={1,3,6,1,4,1,2076,127,2,1,1,10};
UINT4 FsMIPbSrcMacAddress [ ] ={1,3,6,1,4,1,2076,127,2,2,1,1};
UINT4 FsMIPbSrcMacSVlan [ ] ={1,3,6,1,4,1,2076,127,2,2,1,2};
UINT4 FsMIPbSrcMacRowStatus [ ] ={1,3,6,1,4,1,2076,127,2,2,1,3};
UINT4 FsMIPbDstMacAddress [ ] ={1,3,6,1,4,1,2076,127,2,3,1,1};
UINT4 FsMIPbDstMacSVlan [ ] ={1,3,6,1,4,1,2076,127,2,3,1,2};
UINT4 FsMIPbDstMacRowStatus [ ] ={1,3,6,1,4,1,2076,127,2,3,1,3};
UINT4 FsMIPbCVlanSrcMacCVlan [ ] ={1,3,6,1,4,1,2076,127,2,4,1,1};
UINT4 FsMIPbCVlanSrcMacAddr [ ] ={1,3,6,1,4,1,2076,127,2,4,1,2};
UINT4 FsMIPbCVlanSrcMacSVlan [ ] ={1,3,6,1,4,1,2076,127,2,4,1,3};
UINT4 FsMIPbCVlanSrcMacRowStatus [ ] ={1,3,6,1,4,1,2076,127,2,4,1,4};
UINT4 FsMIPbCVlanDstMacCVlan [ ] ={1,3,6,1,4,1,2076,127,2,5,1,1};
UINT4 FsMIPbCVlanDstMacAddr [ ] ={1,3,6,1,4,1,2076,127,2,5,1,2};
UINT4 FsMIPbCVlanDstMacSVlan [ ] ={1,3,6,1,4,1,2076,127,2,5,1,3};
UINT4 FsMIPbCVlanDstMacRowStatus [ ] ={1,3,6,1,4,1,2076,127,2,5,1,4};
UINT4 FsMIPbDscp [ ] ={1,3,6,1,4,1,2076,127,2,6,1,1};
UINT4 FsMIPbDscpSVlan [ ] ={1,3,6,1,4,1,2076,127,2,6,1,2};
UINT4 FsMIPbDscpRowStatus [ ] ={1,3,6,1,4,1,2076,127,2,6,1,3};
UINT4 FsMIPbCVlanDscpCVlan [ ] ={1,3,6,1,4,1,2076,127,2,7,1,1};
UINT4 FsMIPbCVlanDscp [ ] ={1,3,6,1,4,1,2076,127,2,7,1,2};
UINT4 FsMIPbCVlanDscpSVlan [ ] ={1,3,6,1,4,1,2076,127,2,7,1,3};
UINT4 FsMIPbCVlanDscpRowStatus [ ] ={1,3,6,1,4,1,2076,127,2,7,1,4};
UINT4 FsMIPbSrcIpAddr [ ] ={1,3,6,1,4,1,2076,127,2,8,1,1};
UINT4 FsMIPbSrcIpSVlan [ ] ={1,3,6,1,4,1,2076,127,2,8,1,2};
UINT4 FsMIPbSrcIpRowStatus [ ] ={1,3,6,1,4,1,2076,127,2,8,1,3};
UINT4 FsMIPbDstIpAddr [ ] ={1,3,6,1,4,1,2076,127,2,9,1,1};
UINT4 FsMIPbDstIpSVlan [ ] ={1,3,6,1,4,1,2076,127,2,9,1,2};
UINT4 FsMIPbDstIpRowStatus [ ] ={1,3,6,1,4,1,2076,127,2,9,1,3};
UINT4 FsMIPbSrcDstSrcIpAddr [ ] ={1,3,6,1,4,1,2076,127,2,10,1,1};
UINT4 FsMIPbSrcDstDstIpAddr [ ] ={1,3,6,1,4,1,2076,127,2,10,1,2};
UINT4 FsMIPbSrcDstIpSVlan [ ] ={1,3,6,1,4,1,2076,127,2,10,1,3};
UINT4 FsMIPbSrcDstIpRowStatus [ ] ={1,3,6,1,4,1,2076,127,2,10,1,4};
UINT4 FsMIPbCVlanDstIpCVlan [ ] ={1,3,6,1,4,1,2076,127,2,11,1,1};
UINT4 FsMIPbCVlanDstIp [ ] ={1,3,6,1,4,1,2076,127,2,11,1,2};
UINT4 FsMIPbCVlanDstIpSVlan [ ] ={1,3,6,1,4,1,2076,127,2,11,1,3};
UINT4 FsMIPbCVlanDstIpRowStatus [ ] ={1,3,6,1,4,1,2076,127,2,11,1,4};
UINT4 FsMIPbPortCVlan [ ] ={1,3,6,1,4,1,2076,127,2,12,1,1};
UINT4 FsMIPbPortCVlanClassifyStatus [ ] ={1,3,6,1,4,1,2076,127,2,12,1,2};
UINT4 FsMIPbPortEgressUntaggedStatus [ ] ={1,3,6,1,4,1,2076,127,2,12,1,3};
UINT4 FsMIPbLocalEtherType [ ] ={1,3,6,1,4,1,2076,127,2,13,1,1};
UINT4 FsMIPbRelayEtherType [ ] ={1,3,6,1,4,1,2076,127,2,13,1,2};
UINT4 FsMIPbEtherTypeSwapRowStatus [ ] ={1,3,6,1,4,1,2076,127,2,13,1,3};
UINT4 FsMIPbSVlanConfigServiceType [ ] ={1,3,6,1,4,1,2076,127,2,14,1,1};
UINT4 FsMIPbTunnelProtocolDot1x [ ] ={1,3,6,1,4,1,2076,127,2,15,1,1};
UINT4 FsMIPbTunnelProtocolLacp [ ] ={1,3,6,1,4,1,2076,127,2,15,1,2};
UINT4 FsMIPbTunnelProtocolStp [ ] ={1,3,6,1,4,1,2076,127,2,15,1,3};
UINT4 FsMIPbTunnelProtocolGvrp [ ] ={1,3,6,1,4,1,2076,127,2,15,1,4};
UINT4 FsMIPbTunnelProtocolGmrp [ ] ={1,3,6,1,4,1,2076,127,2,15,1,5};
UINT4 FsMIPbTunnelProtocolIgmp [ ] ={1,3,6,1,4,1,2076,127,2,15,1,6};
UINT4 FsMIPbTunnelProtocolDot1xPktsRecvd [ ] ={1,3,6,1,4,1,2076,127,2,16,1,1};
UINT4 FsMIPbTunnelProtocolDot1xPktsSent [ ] ={1,3,6,1,4,1,2076,127,2,16,1,2};
UINT4 FsMIPbTunnelProtocolLacpPktsRecvd [ ] ={1,3,6,1,4,1,2076,127,2,16,1,3};
UINT4 FsMIPbTunnelProtocolLacpPktsSent [ ] ={1,3,6,1,4,1,2076,127,2,16,1,4};
UINT4 FsMIPbTunnelProtocolStpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,127,2,16,1,5};
UINT4 FsMIPbTunnelProtocolStpPDUsSent [ ] ={1,3,6,1,4,1,2076,127,2,16,1,6};
UINT4 FsMIPbTunnelProtocolGvrpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,127,2,16,1,7};
UINT4 FsMIPbTunnelProtocolGvrpPDUsSent [ ] ={1,3,6,1,4,1,2076,127,2,16,1,8};
UINT4 FsMIPbTunnelProtocolGmrpPktsRecvd [ ] ={1,3,6,1,4,1,2076,127,2,16,1,9};
UINT4 FsMIPbTunnelProtocolGmrpPktsSent [ ] ={1,3,6,1,4,1,2076,127,2,16,1,10};
UINT4 FsMIPbTunnelProtocolIgmpPktsRecvd [ ] ={1,3,6,1,4,1,2076,127,2,16,1,11};
UINT4 FsMIPbTunnelProtocolIgmpPktsSent [ ] ={1,3,6,1,4,1,2076,127,2,16,1,12};
UINT4 FsMIPbPepExtCosPreservation [ ] ={1,3,6,1,4,1,2076,127,2,17,1,1};
UINT4 FsMIPbPortCVlanContextId [ ] ={1,3,6,1,4,1,2076,127,2,18,1,1};
UINT4 FsMIPbPortCVlanPort [ ] ={1,3,6,1,4,1,2076,127,2,18,1,2};
UINT4 FsMIPbPortCVlanIndex [ ] ={1,3,6,1,4,1,2076,127,2,18,1,3};
UINT4 FsMIPbPortCVlanCounterRxUcast [ ] ={1,3,6,1,4,1,2076,127,2,18,1,4};
UINT4 FsMIPbPortCVlanCounterRxFrames [ ] ={1,3,6,1,4,1,2076,127,2,18,1,5};
UINT4 FsMIPbPortCVlanCounterRxBytes [ ] ={1,3,6,1,4,1,2076,127,2,18,1,6};
UINT4 FsMIPbPortCVlanCounterStatus [ ] ={1,3,6,1,4,1,2076,127,2,18,1,7};
UINT4 FsMIPbPortCVlanClearCounter [ ] ={1,3,6,1,4,1,2076,127,2,18,1,8};




tMbDbEntry fsmpbMibEntry[]= {

{{12,FsMIPbContextId}, GetNextIndexFsMIPbContextInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPbContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbMulticastMacLimit}, GetNextIndexFsMIPbContextInfoTable, FsMIPbMulticastMacLimitGet, FsMIPbMulticastMacLimitSet, FsMIPbMulticastMacLimitTest, FsMIPbContextInfoTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIPbContextInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbTunnelStpAddress}, GetNextIndexFsMIPbContextInfoTable, FsMIPbTunnelStpAddressGet, FsMIPbTunnelStpAddressSet, FsMIPbTunnelStpAddressTest, FsMIPbContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIPbContextInfoTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelLacpAddress}, GetNextIndexFsMIPbContextInfoTable, FsMIPbTunnelLacpAddressGet, FsMIPbTunnelLacpAddressSet, FsMIPbTunnelLacpAddressTest, FsMIPbContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIPbContextInfoTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelDot1xAddress}, GetNextIndexFsMIPbContextInfoTable, FsMIPbTunnelDot1xAddressGet, FsMIPbTunnelDot1xAddressSet, FsMIPbTunnelDot1xAddressTest, FsMIPbContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIPbContextInfoTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelGvrpAddress}, GetNextIndexFsMIPbContextInfoTable, FsMIPbTunnelGvrpAddressGet, FsMIPbTunnelGvrpAddressSet, FsMIPbTunnelGvrpAddressTest, FsMIPbContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIPbContextInfoTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelGmrpAddress}, GetNextIndexFsMIPbContextInfoTable, FsMIPbTunnelGmrpAddressGet, FsMIPbTunnelGmrpAddressSet, FsMIPbTunnelGmrpAddressTest, FsMIPbContextInfoTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIPbContextInfoTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbPort}, GetNextIndexFsMIPbPortInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbPortSVlanClassificationMethod}, GetNextIndexFsMIPbPortInfoTable, FsMIPbPortSVlanClassificationMethodGet, FsMIPbPortSVlanClassificationMethodSet, FsMIPbPortSVlanClassificationMethodTest, FsMIPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbPortSVlanIngressEtherType}, GetNextIndexFsMIPbPortInfoTable, FsMIPbPortSVlanIngressEtherTypeGet, FsMIPbPortSVlanIngressEtherTypeSet, FsMIPbPortSVlanIngressEtherTypeTest, FsMIPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPbPortInfoTableINDEX, 1, 1, 0, "34984"},

{{12,FsMIPbPortSVlanEgressEtherType}, GetNextIndexFsMIPbPortInfoTable, FsMIPbPortSVlanEgressEtherTypeGet, FsMIPbPortSVlanEgressEtherTypeSet, FsMIPbPortSVlanEgressEtherTypeTest, FsMIPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPbPortInfoTableINDEX, 1, 1, 0, "34984"},

{{12,FsMIPbPortSVlanEtherTypeSwapStatus}, GetNextIndexFsMIPbPortInfoTable, FsMIPbPortSVlanEtherTypeSwapStatusGet, FsMIPbPortSVlanEtherTypeSwapStatusSet, FsMIPbPortSVlanEtherTypeSwapStatusTest, FsMIPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbPortSVlanTranslationStatus}, GetNextIndexFsMIPbPortInfoTable, FsMIPbPortSVlanTranslationStatusGet, FsMIPbPortSVlanTranslationStatusSet, FsMIPbPortSVlanTranslationStatusTest, FsMIPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbPortUnicastMacLearning}, GetNextIndexFsMIPbPortInfoTable, FsMIPbPortUnicastMacLearningGet, FsMIPbPortUnicastMacLearningSet, FsMIPbPortUnicastMacLearningTest, FsMIPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbPortUnicastMacLimit}, GetNextIndexFsMIPbPortInfoTable, FsMIPbPortUnicastMacLimitGet, FsMIPbPortUnicastMacLimitSet, FsMIPbPortUnicastMacLimitTest, FsMIPbPortInfoTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbPortBundleStatus}, GetNextIndexFsMIPbPortInfoTable, FsMIPbPortBundleStatusGet, FsMIPbPortBundleStatusSet, FsMIPbPortBundleStatusTest, FsMIPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbPortMultiplexStatus}, GetNextIndexFsMIPbPortInfoTable, FsMIPbPortMultiplexStatusGet, FsMIPbPortMultiplexStatusSet, FsMIPbPortMultiplexStatusTest, FsMIPbPortInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPortInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbSrcMacAddress}, GetNextIndexFsMIPbSrcMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIPbSrcMacSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbSrcMacSVlan}, GetNextIndexFsMIPbSrcMacSVlanTable, FsMIPbSrcMacSVlanGet, FsMIPbSrcMacSVlanSet, FsMIPbSrcMacSVlanTest, FsMIPbSrcMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbSrcMacSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbSrcMacRowStatus}, GetNextIndexFsMIPbSrcMacSVlanTable, FsMIPbSrcMacRowStatusGet, FsMIPbSrcMacRowStatusSet, FsMIPbSrcMacRowStatusTest, FsMIPbSrcMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbSrcMacSVlanTableINDEX, 2, 0, 1, NULL},

{{12,FsMIPbDstMacAddress}, GetNextIndexFsMIPbDstMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIPbDstMacSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbDstMacSVlan}, GetNextIndexFsMIPbDstMacSVlanTable, FsMIPbDstMacSVlanGet, FsMIPbDstMacSVlanSet, FsMIPbDstMacSVlanTest, FsMIPbDstMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbDstMacSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbDstMacRowStatus}, GetNextIndexFsMIPbDstMacSVlanTable, FsMIPbDstMacRowStatusGet, FsMIPbDstMacRowStatusSet, FsMIPbDstMacRowStatusTest, FsMIPbDstMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbDstMacSVlanTableINDEX, 2, 0, 1, NULL},

{{12,FsMIPbCVlanSrcMacCVlan}, GetNextIndexFsMIPbCVlanSrcMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIPbCVlanSrcMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanSrcMacAddr}, GetNextIndexFsMIPbCVlanSrcMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIPbCVlanSrcMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanSrcMacSVlan}, GetNextIndexFsMIPbCVlanSrcMacSVlanTable, FsMIPbCVlanSrcMacSVlanGet, FsMIPbCVlanSrcMacSVlanSet, FsMIPbCVlanSrcMacSVlanTest, FsMIPbCVlanSrcMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbCVlanSrcMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanSrcMacRowStatus}, GetNextIndexFsMIPbCVlanSrcMacSVlanTable, FsMIPbCVlanSrcMacRowStatusGet, FsMIPbCVlanSrcMacRowStatusSet, FsMIPbCVlanSrcMacRowStatusTest, FsMIPbCVlanSrcMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbCVlanSrcMacSVlanTableINDEX, 3, 0, 1, NULL},

{{12,FsMIPbCVlanDstMacCVlan}, GetNextIndexFsMIPbCVlanDstMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIPbCVlanDstMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanDstMacAddr}, GetNextIndexFsMIPbCVlanDstMacSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIPbCVlanDstMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanDstMacSVlan}, GetNextIndexFsMIPbCVlanDstMacSVlanTable, FsMIPbCVlanDstMacSVlanGet, FsMIPbCVlanDstMacSVlanSet, FsMIPbCVlanDstMacSVlanTest, FsMIPbCVlanDstMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbCVlanDstMacSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanDstMacRowStatus}, GetNextIndexFsMIPbCVlanDstMacSVlanTable, FsMIPbCVlanDstMacRowStatusGet, FsMIPbCVlanDstMacRowStatusSet, FsMIPbCVlanDstMacRowStatusTest, FsMIPbCVlanDstMacSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbCVlanDstMacSVlanTableINDEX, 3, 0, 1, NULL},

{{12,FsMIPbDscp}, GetNextIndexFsMIPbDscpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPbDscpSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbDscpSVlan}, GetNextIndexFsMIPbDscpSVlanTable, FsMIPbDscpSVlanGet, FsMIPbDscpSVlanSet, FsMIPbDscpSVlanTest, FsMIPbDscpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbDscpSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbDscpRowStatus}, GetNextIndexFsMIPbDscpSVlanTable, FsMIPbDscpRowStatusGet, FsMIPbDscpRowStatusSet, FsMIPbDscpRowStatusTest, FsMIPbDscpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbDscpSVlanTableINDEX, 2, 0, 1, NULL},

{{12,FsMIPbCVlanDscpCVlan}, GetNextIndexFsMIPbCVlanDscpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIPbCVlanDscpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanDscp}, GetNextIndexFsMIPbCVlanDscpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPbCVlanDscpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanDscpSVlan}, GetNextIndexFsMIPbCVlanDscpSVlanTable, FsMIPbCVlanDscpSVlanGet, FsMIPbCVlanDscpSVlanSet, FsMIPbCVlanDscpSVlanTest, FsMIPbCVlanDscpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbCVlanDscpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanDscpRowStatus}, GetNextIndexFsMIPbCVlanDscpSVlanTable, FsMIPbCVlanDscpRowStatusGet, FsMIPbCVlanDscpRowStatusSet, FsMIPbCVlanDscpRowStatusTest, FsMIPbCVlanDscpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbCVlanDscpSVlanTableINDEX, 3, 0, 1, NULL},

{{12,FsMIPbSrcIpAddr}, GetNextIndexFsMIPbSrcIpAddrSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIPbSrcIpAddrSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbSrcIpSVlan}, GetNextIndexFsMIPbSrcIpAddrSVlanTable, FsMIPbSrcIpSVlanGet, FsMIPbSrcIpSVlanSet, FsMIPbSrcIpSVlanTest, FsMIPbSrcIpAddrSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbSrcIpAddrSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbSrcIpRowStatus}, GetNextIndexFsMIPbSrcIpAddrSVlanTable, FsMIPbSrcIpRowStatusGet, FsMIPbSrcIpRowStatusSet, FsMIPbSrcIpRowStatusTest, FsMIPbSrcIpAddrSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbSrcIpAddrSVlanTableINDEX, 2, 0, 1, NULL},

{{12,FsMIPbDstIpAddr}, GetNextIndexFsMIPbDstIpAddrSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIPbDstIpAddrSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbDstIpSVlan}, GetNextIndexFsMIPbDstIpAddrSVlanTable, FsMIPbDstIpSVlanGet, FsMIPbDstIpSVlanSet, FsMIPbDstIpSVlanTest, FsMIPbDstIpAddrSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbDstIpAddrSVlanTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbDstIpRowStatus}, GetNextIndexFsMIPbDstIpAddrSVlanTable, FsMIPbDstIpRowStatusGet, FsMIPbDstIpRowStatusSet, FsMIPbDstIpRowStatusTest, FsMIPbDstIpAddrSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbDstIpAddrSVlanTableINDEX, 2, 0, 1, NULL},

{{12,FsMIPbSrcDstSrcIpAddr}, GetNextIndexFsMIPbSrcDstIpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIPbSrcDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbSrcDstDstIpAddr}, GetNextIndexFsMIPbSrcDstIpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIPbSrcDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbSrcDstIpSVlan}, GetNextIndexFsMIPbSrcDstIpSVlanTable, FsMIPbSrcDstIpSVlanGet, FsMIPbSrcDstIpSVlanSet, FsMIPbSrcDstIpSVlanTest, FsMIPbSrcDstIpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbSrcDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbSrcDstIpRowStatus}, GetNextIndexFsMIPbSrcDstIpSVlanTable, FsMIPbSrcDstIpRowStatusGet, FsMIPbSrcDstIpRowStatusSet, FsMIPbSrcDstIpRowStatusTest, FsMIPbSrcDstIpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbSrcDstIpSVlanTableINDEX, 3, 0, 1, NULL},

{{12,FsMIPbCVlanDstIpCVlan}, GetNextIndexFsMIPbCVlanDstIpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIPbCVlanDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanDstIp}, GetNextIndexFsMIPbCVlanDstIpSVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIPbCVlanDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanDstIpSVlan}, GetNextIndexFsMIPbCVlanDstIpSVlanTable, FsMIPbCVlanDstIpSVlanGet, FsMIPbCVlanDstIpSVlanSet, FsMIPbCVlanDstIpSVlanTest, FsMIPbCVlanDstIpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbCVlanDstIpSVlanTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbCVlanDstIpRowStatus}, GetNextIndexFsMIPbCVlanDstIpSVlanTable, FsMIPbCVlanDstIpRowStatusGet, FsMIPbCVlanDstIpRowStatusSet, FsMIPbCVlanDstIpRowStatusTest, FsMIPbCVlanDstIpSVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbCVlanDstIpSVlanTableINDEX, 3, 0, 1, NULL},

{{12,FsMIPbPortCVlan}, GetNextIndexFsMIPbPortBasedCVlanTable, FsMIPbPortCVlanGet, FsMIPbPortCVlanSet, FsMIPbPortCVlanTest, FsMIPbPortBasedCVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPortBasedCVlanTableINDEX, 1, 0, 0, NULL},

{{12,FsMIPbPortCVlanClassifyStatus}, GetNextIndexFsMIPbPortBasedCVlanTable, FsMIPbPortCVlanClassifyStatusGet, FsMIPbPortCVlanClassifyStatusSet, FsMIPbPortCVlanClassifyStatusTest, FsMIPbPortBasedCVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPortBasedCVlanTableINDEX, 1, 0, 0, NULL},
{{12,FsMIPbPortEgressUntaggedStatus}, GetNextIndexFsMIPbPortBasedCVlanTable, FsMIPbPortEgressUntaggedStatusGet, FsMIPbPortEgressUntaggedStatusSet, FsMIPbPortEgressUntaggedStatusTest, FsMIPbPortBasedCVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPortBasedCVlanTableINDEX, 1, 0, 0, "2"},

{{12,FsMIPbLocalEtherType}, GetNextIndexFsMIPbEtherTypeSwapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPbEtherTypeSwapTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbRelayEtherType}, GetNextIndexFsMIPbEtherTypeSwapTable, FsMIPbRelayEtherTypeGet, FsMIPbRelayEtherTypeSet, FsMIPbRelayEtherTypeTest, FsMIPbEtherTypeSwapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPbEtherTypeSwapTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbEtherTypeSwapRowStatus}, GetNextIndexFsMIPbEtherTypeSwapTable, FsMIPbEtherTypeSwapRowStatusGet, FsMIPbEtherTypeSwapRowStatusSet, FsMIPbEtherTypeSwapRowStatusTest, FsMIPbEtherTypeSwapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbEtherTypeSwapTableINDEX, 2, 0, 1, NULL},

{{12,FsMIPbSVlanConfigServiceType}, GetNextIndexFsMIPbSVlanConfigTable, FsMIPbSVlanConfigServiceTypeGet, FsMIPbSVlanConfigServiceTypeSet, FsMIPbSVlanConfigServiceTypeTest, FsMIPbSVlanConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbSVlanConfigTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbTunnelProtocolDot1x}, GetNextIndexFsMIPbTunnelProtocolTable, FsMIPbTunnelProtocolDot1xGet, FsMIPbTunnelProtocolDot1xSet, FsMIPbTunnelProtocolDot1xTest, FsMIPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolLacp}, GetNextIndexFsMIPbTunnelProtocolTable, FsMIPbTunnelProtocolLacpGet, FsMIPbTunnelProtocolLacpSet, FsMIPbTunnelProtocolLacpTest, FsMIPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolStp}, GetNextIndexFsMIPbTunnelProtocolTable, FsMIPbTunnelProtocolStpGet, FsMIPbTunnelProtocolStpSet, FsMIPbTunnelProtocolStpTest, FsMIPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolGvrp}, GetNextIndexFsMIPbTunnelProtocolTable, FsMIPbTunnelProtocolGvrpGet, FsMIPbTunnelProtocolGvrpSet, FsMIPbTunnelProtocolGvrpTest, FsMIPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolGmrp}, GetNextIndexFsMIPbTunnelProtocolTable, FsMIPbTunnelProtocolGmrpGet, FsMIPbTunnelProtocolGmrpSet, FsMIPbTunnelProtocolGmrpTest, FsMIPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolIgmp}, GetNextIndexFsMIPbTunnelProtocolTable, FsMIPbTunnelProtocolIgmpGet, FsMIPbTunnelProtocolIgmpSet, FsMIPbTunnelProtocolIgmpTest, FsMIPbTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolDot1xPktsRecvd}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolDot1xPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolDot1xPktsSent}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolDot1xPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolLacpPktsRecvd}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolLacpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolLacpPktsSent}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolLacpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolStpPDUsRecvd}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolStpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolStpPDUsSent}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolStpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolGvrpPDUsRecvd}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolGvrpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolGvrpPDUsSent}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolGvrpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolGmrpPktsRecvd}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolGmrpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolGmrpPktsSent}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolGmrpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolIgmpPktsRecvd}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolIgmpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbTunnelProtocolIgmpPktsSent}, GetNextIndexFsMIPbTunnelProtocolStatsTable, FsMIPbTunnelProtocolIgmpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbTunnelProtocolStatsTableINDEX, 1, 1, 0, NULL},

{{12,FsMIPbPepExtCosPreservation}, GetNextIndexFsMIPbPepExtTable, FsMIPbPepExtCosPreservationGet, FsMIPbPepExtCosPreservationSet, FsMIPbPepExtCosPreservationTest, FsMIPbPepExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPepExtTableINDEX, 2, 0, 0, NULL},

{{12,FsMIPbPortCVlanContextId}, GetNextIndexFsMIPbPortCVlanCounterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPbPortCVlanCounterTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbPortCVlanPort}, GetNextIndexFsMIPbPortCVlanCounterTable, FsMIPbPortCVlanPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPbPortCVlanCounterTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbPortCVlanIndex}, GetNextIndexFsMIPbPortCVlanCounterTable, FsMIPbPortCVlanIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIPbPortCVlanCounterTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbPortCVlanCounterRxUcast}, GetNextIndexFsMIPbPortCVlanCounterTable, FsMIPbPortCVlanCounterRxUcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbPortCVlanCounterTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbPortCVlanCounterRxFrames}, GetNextIndexFsMIPbPortCVlanCounterTable, FsMIPbPortCVlanCounterRxFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbPortCVlanCounterTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbPortCVlanCounterRxBytes}, GetNextIndexFsMIPbPortCVlanCounterTable, FsMIPbPortCVlanCounterRxBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPbPortCVlanCounterTableINDEX, 3, 0, 0, NULL},

{{12,FsMIPbPortCVlanCounterStatus}, GetNextIndexFsMIPbPortCVlanCounterTable, FsMIPbPortCVlanCounterStatusGet, FsMIPbPortCVlanCounterStatusSet, FsMIPbPortCVlanCounterStatusTest, FsMIPbPortCVlanCounterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPortCVlanCounterTableINDEX, 3, 0, 0, "2"},

{{12,FsMIPbPortCVlanClearCounter}, GetNextIndexFsMIPbPortCVlanCounterTable, FsMIPbPortCVlanClearCounterGet, FsMIPbPortCVlanClearCounterSet, FsMIPbPortCVlanClearCounterTest, FsMIPbPortCVlanCounterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPbPortCVlanCounterTableINDEX, 3, 0, 0, "2"},

};
tMibData fsmpbEntry = { 86, fsmpbMibEntry };

#endif /* _FSMPBDB_H */

