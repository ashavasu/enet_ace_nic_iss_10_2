
/* $Id: stdvlanc.h,v 1.3 2016/10/21 13:42:06 siva Exp $
    ISS Wrapper header
    module Q-BRIDGE-MIB

 */

#ifndef _H_i_Q_BRIDGE_MIB
#define _H_i_Q_BRIDGE_MIB

#define VLAN_STATIC_EGRESS_PORT 1
#define VLAN_STATIC_UNTAGGED_PORT 2
#define VLAN_STATIC_FORBIDDEN_PORT 3
#define VLAN_FORWARDALL_STATIC_PORT 4
#define VLAN_FORWARDALL_FORBIDDEN_PORT 5
#define VLAN_FORWARD_UNREG_STATIC_PORT 6
#define VLAN_FORWARD_UNREG_FORBIDDEN_PORT 7
#define VLAN_PORT_LENGTH 36
/********************************************************************
* FUNCTION NcDot1qVlanVersionNumberGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanVersionNumberGet (
                INT4 *pi4Dot1qVlanVersionNumber );

/********************************************************************
* FUNCTION NcDot1qMaxVlanIdGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qMaxVlanIdGet (
                INT4 *pi4Dot1qMaxVlanId );

/********************************************************************
* FUNCTION NcDot1qMaxSupportedVlansGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qMaxSupportedVlansGet (
                UINT4 *pu4Dot1qMaxSupportedVlans );

/********************************************************************
* FUNCTION NcDot1qNumVlansGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qNumVlansGet (
                UINT4 *pu4Dot1qNumVlans );

/********************************************************************
* FUNCTION NcDot1qGvrpStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qGvrpStatusSet (
                INT4 i4Dot1qGvrpStatus );

/********************************************************************
* FUNCTION NcDot1qGvrpStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qGvrpStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1qGvrpStatus );

/********************************************************************
* FUNCTION NcDot1qVlanNumDeletesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanNumDeletesGet (
                UINT4 *pu4Dot1qVlanNumDeletes );

/********************************************************************
* FUNCTION NcDot1qNextFreeLocalVlanIndexGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qNextFreeLocalVlanIndexGet (
                INT4 *pi4Dot1qNextFreeLocalVlanIndex );

/********************************************************************
* FUNCTION NcDot1qConstraintSetDefaultSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qConstraintSetDefaultSet (
                INT4 i4Dot1qConstraintSetDefault );

/********************************************************************
* FUNCTION NcDot1qConstraintSetDefaultTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qConstraintSetDefaultTest (UINT4 *pu4Error,
                INT4 i4Dot1qConstraintSetDefault );

/********************************************************************
* FUNCTION NcDot1qConstraintTypeDefaultSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qConstraintTypeDefaultSet (
                INT4 i4Dot1qConstraintTypeDefault );

/********************************************************************
* FUNCTION NcDot1qConstraintTypeDefaultTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qConstraintTypeDefaultTest (UINT4 *pu4Error,
                INT4 i4Dot1qConstraintTypeDefault );

/********************************************************************
* FUNCTION NcDot1qFdbDynamicCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qFdbDynamicCountGet (
                UINT4 u4Dot1qFdbId,
                UINT4 *pu4Dot1qFdbDynamicCount );

/********************************************************************
* FUNCTION NcDot1qTpFdbPortGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpFdbPortGet (
                UINT4 u4Dot1qFdbId,
                UINT1 *pDot1qTpFdbAddress,
                INT4 *pi4Dot1qTpFdbPort );

/********************************************************************
* FUNCTION NcDot1qTpFdbStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpFdbStatusGet (
                UINT4 u4Dot1qFdbId,
                UINT1 *pDot1qTpFdbAddress,
                INT4 *pi4Dot1qTpFdbStatus );

/********************************************************************
* FUNCTION NcDot1qTpGroupEgressPortsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpGroupEgressPortsGet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pDot1qTpGroupAddress,
                UINT1 *pau1Dot1qTpGroupEgressPorts );

/********************************************************************
* FUNCTION NcDot1qTpGroupLearntGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpGroupLearntGet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pDot1qTpGroupAddress,
                UINT1 *pau1Dot1qTpGroupLearnt );

/********************************************************************
* FUNCTION NcDot1qForwardAllPortsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qForwardAllPortsGet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qForwardAllPorts );

/********************************************************************
* FUNCTION NcDot1qForwardAllStaticPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qForwardAllStaticPortsSet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qForwardAllStaticPorts );

/********************************************************************
* FUNCTION NcDot1qForwardAllStaticPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qForwardAllStaticPortsTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qForwardAllStaticPorts );

/********************************************************************
* FUNCTION NcDot1qForwardAllForbiddenPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qForwardAllForbiddenPortsSet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qForwardAllForbiddenPorts );

/********************************************************************
* FUNCTION NcDot1qForwardAllForbiddenPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qForwardAllForbiddenPortsTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qForwardAllForbiddenPorts );

/********************************************************************
* FUNCTION NcDot1qForwardUnregisteredPortsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qForwardUnregisteredPortsGet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qForwardUnregisteredPorts );

/********************************************************************
* FUNCTION NcDot1qForwardUnregisteredStaticPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qForwardUnregisteredStaticPortsSet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qForwardUnregisteredStaticPorts );

/********************************************************************
* FUNCTION NcDot1qForwardUnregisteredStaticPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qForwardUnregisteredStaticPortsTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qForwardUnregisteredStaticPorts );

/********************************************************************
* FUNCTION NcDot1qForwardUnregisteredForbiddenPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qForwardUnregisteredForbiddenPortsSet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qForwardUnregisteredForbiddenPorts );

/********************************************************************
* FUNCTION NcDot1qForwardUnregisteredForbiddenPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qForwardUnregisteredForbiddenPortsTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qForwardUnregisteredForbiddenPorts );

/********************************************************************
* FUNCTION NcDot1qStaticUnicastAllowedToGoToSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qStaticUnicastAllowedToGoToSet (
                UINT4 u4Dot1qFdbId,
                UINT1 *pDot1qStaticUnicastAddress,
                INT4 i4Dot1qStaticUnicastReceivePort,
                UINT1 *pau1Dot1qStaticUnicastAllowedToGoTo );

/********************************************************************
* FUNCTION NcDot1qStaticUnicastAllowedToGoToTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qStaticUnicastAllowedToGoToTest (UINT4 *pu4Error,
                UINT4 u4Dot1qFdbId,
                UINT1 *pDot1qStaticUnicastAddress,
                INT4 i4Dot1qStaticUnicastReceivePort,
                UINT1 *pau1Dot1qStaticUnicastAllowedToGoTo );

/********************************************************************
* FUNCTION NcDot1qStaticUnicastStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qStaticUnicastStatusSet (
                UINT4 u4Dot1qFdbId,
                UINT1 *pDot1qStaticUnicastAddress,
                INT4 i4Dot1qStaticUnicastReceivePort,
                INT4 i4Dot1qStaticUnicastStatus );

/********************************************************************
* FUNCTION NcDot1qStaticUnicastStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qStaticUnicastStatusTest (UINT4 *pu4Error,
                UINT4 u4Dot1qFdbId,
                UINT1 *pDot1qStaticUnicastAddress,
                INT4 i4Dot1qStaticUnicastReceivePort,
                INT4 i4Dot1qStaticUnicastStatus );

/********************************************************************
* FUNCTION NcDot1qStaticMulticastStaticEgressPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qStaticMulticastStaticEgressPortsSet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pDot1qStaticMulticastAddress,
                INT4 i4Dot1qStaticMulticastReceivePort,
                UINT1 *pau1Dot1qStaticMulticastStaticEgressPorts );

/********************************************************************
* FUNCTION NcDot1qStaticMulticastStaticEgressPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qStaticMulticastStaticEgressPortsTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pDot1qStaticMulticastAddress,
                INT4 i4Dot1qStaticMulticastReceivePort,
                UINT1 *pau1Dot1qStaticMulticastStaticEgressPorts );

/********************************************************************
* FUNCTION NcDot1qStaticMulticastForbiddenEgressPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qStaticMulticastForbiddenEgressPortsSet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pDot1qStaticMulticastAddress,
                INT4 i4Dot1qStaticMulticastReceivePort,
                UINT1 *pau1Dot1qStaticMulticastForbiddenEgressPorts );

/********************************************************************
* FUNCTION NcDot1qStaticMulticastForbiddenEgressPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qStaticMulticastForbiddenEgressPortsTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pDot1qStaticMulticastAddress,
                INT4 i4Dot1qStaticMulticastReceivePort,
                UINT1 *pau1Dot1qStaticMulticastForbiddenEgressPorts );

/********************************************************************
* FUNCTION NcDot1qStaticMulticastStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qStaticMulticastStatusSet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pDot1qStaticMulticastAddress,
                INT4 i4Dot1qStaticMulticastReceivePort,
                INT4 i4Dot1qStaticMulticastStatus );

/********************************************************************
* FUNCTION NcDot1qStaticMulticastStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qStaticMulticastStatusTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pDot1qStaticMulticastAddress,
                INT4 i4Dot1qStaticMulticastReceivePort,
                INT4 i4Dot1qStaticMulticastStatus );

/********************************************************************
* FUNCTION NcDot1qVlanFdbIdGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanFdbIdGet (
                UINT4 u4Dot1qVlanTimeMark,
                UINT4 u4Dot1qVlanIndex,
                UINT4 *pu4Dot1qVlanFdbId );

/********************************************************************
* FUNCTION NcDot1qVlanCurrentEgressPortsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanCurrentEgressPortsGet (
                UINT4 u4Dot1qVlanTimeMark,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qVlanCurrentEgressPorts );

/********************************************************************
* FUNCTION NcDot1qVlanCurrentUntaggedPortsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanCurrentUntaggedPortsGet (
                UINT4 u4Dot1qVlanTimeMark,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qVlanCurrentUntaggedPorts );

/********************************************************************
* FUNCTION NcDot1qVlanStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanStatusGet (
                UINT4 u4Dot1qVlanTimeMark,
                UINT4 u4Dot1qVlanIndex,
                INT4 *pi4Dot1qVlanStatus );

/********************************************************************
* FUNCTION NcDot1qVlanCreationTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanCreationTimeGet (
                UINT4 u4Dot1qVlanTimeMark,
                UINT4 u4Dot1qVlanIndex,
                UINT4 *pu4Dot1qVlanCreationTime );

/********************************************************************
* FUNCTION NcDot1qVlanStaticNameSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanStaticNameSet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pDot1qVlanStaticName );

/********************************************************************
* FUNCTION NcDot1qVlanStaticNameTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanStaticNameTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pDot1qVlanStaticName );

/********************************************************************
* FUNCTION NcDot1qVlanStaticEgressPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanStaticEgressPortsSet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qVlanStaticEgressPorts,
                UINT4 u4Flag);

/********************************************************************
* FUNCTION NcDot1qVlanStaticEgressPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanStaticEgressPortsTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qVlanStaticEgressPorts );

/********************************************************************
* FUNCTION NcDot1qVlanForbiddenEgressPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanForbiddenEgressPortsSet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qVlanForbiddenEgressPorts );

/********************************************************************
* FUNCTION NcDot1qVlanForbiddenEgressPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanForbiddenEgressPortsTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qVlanForbiddenEgressPorts );

/********************************************************************
* FUNCTION NcDot1qVlanStaticUntaggedPortsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanStaticUntaggedPortsSet (
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qVlanStaticUntaggedPorts );

/********************************************************************
* FUNCTION NcDot1qVlanStaticUntaggedPortsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanStaticUntaggedPortsTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                UINT1 *pau1Dot1qVlanStaticUntaggedPorts );

/********************************************************************
* FUNCTION NcDot1qVlanStaticRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanStaticRowStatusSet (
                UINT4 u4Dot1qVlanIndex,
                INT4 i4Dot1qVlanStaticRowStatus );

/********************************************************************
* FUNCTION NcDot1qVlanStaticRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qVlanStaticRowStatusTest (UINT4 *pu4Error,
                UINT4 u4Dot1qVlanIndex,
                INT4 i4Dot1qVlanStaticRowStatus );

/********************************************************************
* FUNCTION NcDot1qTpVlanPortInFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpVlanPortInFramesGet (
                INT4 i4Dot1dBasePort,
                UINT4 u4Dot1qVlanIndex,
                UINT4 *pu4Dot1qTpVlanPortInFrames );

/********************************************************************
* FUNCTION NcDot1qTpVlanPortOutFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpVlanPortOutFramesGet (
                INT4 i4Dot1dBasePort,
                UINT4 u4Dot1qVlanIndex,
                UINT4 *pu4Dot1qTpVlanPortOutFrames );

/********************************************************************
* FUNCTION NcDot1qTpVlanPortInDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpVlanPortInDiscardsGet (
                INT4 i4Dot1dBasePort,
                UINT4 u4Dot1qVlanIndex,
                UINT4 *pu4Dot1qTpVlanPortInDiscards );

/********************************************************************
* FUNCTION NcDot1qTpVlanPortInOverflowFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpVlanPortInOverflowFramesGet (
                INT4 i4Dot1dBasePort,
                UINT4 u4Dot1qVlanIndex,
                UINT4 *pu4Dot1qTpVlanPortInOverflowFrames );

/********************************************************************
* FUNCTION NcDot1qTpVlanPortOutOverflowFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpVlanPortOutOverflowFramesGet (
                INT4 i4Dot1dBasePort,
                UINT4 u4Dot1qVlanIndex,
                UINT4 *pu4Dot1qTpVlanPortOutOverflowFrames );

/********************************************************************
* FUNCTION NcDot1qTpVlanPortInOverflowDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpVlanPortInOverflowDiscardsGet (
                INT4 i4Dot1dBasePort,
                UINT4 u4Dot1qVlanIndex,
                UINT4 *pu4Dot1qTpVlanPortInOverflowDiscards );

/********************************************************************
* FUNCTION NcDot1qTpVlanPortHCInFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpVlanPortHCInFramesGet (
                INT4 i4Dot1dBasePort,
                UINT4 u4Dot1qVlanIndex,
                unsigned long long *pu8Dot1qTpVlanPortHCInFrames );

/********************************************************************
* FUNCTION NcDot1qTpVlanPortHCOutFramesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpVlanPortHCOutFramesGet (
                INT4 i4Dot1dBasePort,
                UINT4 u4Dot1qVlanIndex,
                unsigned long long *pu8Dot1qTpVlanPortHCOutFrames );

/********************************************************************
* FUNCTION NcDot1qTpVlanPortHCInDiscardsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qTpVlanPortHCInDiscardsGet (
                INT4 i4Dot1dBasePort,
                UINT4 u4Dot1qVlanIndex,
                unsigned long long *pu8Dot1qTpVlanPortHCInDiscards );

/********************************************************************
* FUNCTION NcDot1qConstraintTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qConstraintTypeSet (
                UINT4 u4Dot1qConstraintVlan,
                INT4 i4Dot1qConstraintSet,
                INT4 i4Dot1qConstraintType );

/********************************************************************
* FUNCTION NcDot1qConstraintTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qConstraintTypeTest (UINT4 *pu4Error,
                UINT4 u4Dot1qConstraintVlan,
                INT4 i4Dot1qConstraintSet,
                INT4 i4Dot1qConstraintType );

/********************************************************************
* FUNCTION NcDot1qConstraintStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qConstraintStatusSet (
                UINT4 u4Dot1qConstraintVlan,
                INT4 i4Dot1qConstraintSet,
                INT4 i4Dot1qConstraintStatus );

/********************************************************************
* FUNCTION NcDot1qConstraintStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1qConstraintStatusTest (UINT4 *pu4Error,
                UINT4 u4Dot1qConstraintVlan,
                INT4 i4Dot1qConstraintSet,
                INT4 i4Dot1qConstraintStatus );

/********************************************************************
* FUNCTION NcDot1vProtocolGroupIdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1vProtocolGroupIdSet (
                INT4 i4Dot1vProtocolTemplateFrameType,
                UINT1 *au1Dot1vProtocolTemplateProtocolValue,
                INT4 i4Dot1vProtocolGroupId );

/********************************************************************
* FUNCTION NcDot1vProtocolGroupIdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1vProtocolGroupIdTest (UINT4 *pu4Error,
                INT4 i4Dot1vProtocolTemplateFrameType,
                UINT1 au1Dot1vProtocolTemplateProtocolValue,
                INT4 i4Dot1vProtocolGroupId );

/********************************************************************
* FUNCTION NcDot1vProtocolGroupRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1vProtocolGroupRowStatusSet (
                INT4 i4Dot1vProtocolTemplateFrameType,
                UINT1 *au1Dot1vProtocolTemplateProtocolValue,
                INT4 i4Dot1vProtocolGroupRowStatus );

/********************************************************************
* FUNCTION NcDot1vProtocolGroupRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1vProtocolGroupRowStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1vProtocolTemplateFrameType,
                UINT1 *au1Dot1vProtocolTemplateProtocolValue,
                INT4 i4Dot1vProtocolGroupRowStatus );

/********************************************************************
* FUNCTION NcDot1vProtocolPortGroupVidSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1vProtocolPortGroupVidSet (
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1vProtocolPortGroupId,
                INT4 i4Dot1vProtocolPortGroupVid );

/********************************************************************
* FUNCTION NcDot1vProtocolPortGroupVidTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1vProtocolPortGroupVidTest (UINT4 *pu4Error,
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1vProtocolPortGroupId,
                INT4 i4Dot1vProtocolPortGroupVid );

/********************************************************************
* FUNCTION NcDot1vProtocolPortRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1vProtocolPortRowStatusSet (
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1vProtocolPortGroupId,
                INT4 i4Dot1vProtocolPortRowStatus );

/********************************************************************
* FUNCTION NcDot1vProtocolPortRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcDot1vProtocolPortRowStatusTest (UINT4 *pu4Error,
                INT4 i4Dot1dBasePort,
                INT4 i4Dot1vProtocolPortGroupId,
                INT4 i4Dot1vProtocolPortRowStatus );

/* END i_Q_BRIDGE_MIB.c */

INT1 VlanConfigureStaticPorts (UINT4 u4Dot1qVlanIndex, UINT1 *pu1MemberPorts,
              UINT1 *pu1UntaggedPorts, UINT1 *pu1ForbiddenPorts,INT1 PortType );

INT1 VlanConfigureForwardPort (UINT1 *pu1EgressPorts,
                 UINT1 *pu1ForbiddenPorts, UINT4 u4EgressFlag,
                 UINT4 u4Type,UINT4 u4VlanId,INT1 PortType);




#endif
