/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmvlewr.h,v 1.10 2015/06/05 09:40:36 siva Exp $
 *
 *********************************************************************/

#ifndef _FSMVLEWR_H
#define _FSMVLEWR_H
INT4 GetNextIndexFsMIVlanBridgeInfoTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMVLE(VOID);

VOID UnRegisterFSMVLE(VOID);
INT4 FsMIVlanBridgeModeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanBridgeModeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanBridgeModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanBridgeInfoTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsMIVlanTunnelContextInfoTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIVlanTunnelBpduPriGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelStpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelLacpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelDot1xAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelGvrpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelGmrpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelMvrpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelMmrpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelElmiAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelLldpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelEcfmAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelEoamAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelIgmpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelBpduPriSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelStpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelLacpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelDot1xAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelGvrpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelGmrpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelMvrpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelMmrpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelElmiAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelLldpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelEcfmAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelEoamAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelIgmpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelBpduPriTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelStpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelLacpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelDot1xAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelGvrpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelGmrpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelMvrpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelMmrpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelElmiAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelLldpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelEcfmAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelEoamAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelIgmpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelContextInfoTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIVlanTunnelTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIVlanTunnelStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIVlanTunnelProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIVlanTunnelProtocolDot1xGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolLacpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolStpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolGvrpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolGmrpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolIgmpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolMvrpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolMmrpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolElmiGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolLldpGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolEcfmGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelOverrideOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolEoamGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolDot1xSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolLacpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolStpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolGvrpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolGmrpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolIgmpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolMvrpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolMmrpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolElmiSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolLldpSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolEcfmSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelOverrideOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolEoamSet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolDot1xTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolLacpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolStpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolGvrpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolGmrpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolIgmpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolMvrpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolMmrpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolElmiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolLldpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolEcfmTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelOverrideOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolEoamTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIVlanTunnelProtocolStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIVlanTunnelProtocolDot1xPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolDot1xPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolLacpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolLacpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolStpPDUsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolStpPDUsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolGvrpPDUsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolGvrpPDUsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolGmrpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolGmrpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolIgmpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolIgmpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolMvrpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolMvrpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolMmrpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolMmrpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolElmiPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolElmiPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolLldpPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolLldpPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolEcfmPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolEcfmPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolEoamPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanTunnelProtocolEoamPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIVlanDiscardStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIVlanDiscardDot1xPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardDot1xPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardLacpPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardLacpPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardStpPDUsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardStpPDUsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardGvrpPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardGvrpPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardGmrpPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardGmrpPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardIgmpPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardIgmpPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardMvrpPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardMvrpPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardMmrpPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardMmrpPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardElmiPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardElmiPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardLldpPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardLldpPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardEcfmPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardEcfmPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardEoamPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIVlanDiscardEoamPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIServiceVlanTunnelProtocolTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIServiceVlanRsvdMacaddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanTunnelMacaddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanTunnelProtocolStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanTunnelPktsRecvdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanTunnelPktsSentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanDiscardPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanDiscardPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanRsvdMacaddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanTunnelMacaddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanTunnelProtocolStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanRsvdMacaddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanTunnelMacaddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanTunnelProtocolStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIServiceVlanTunnelProtocolTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSMVLEWR_H */
