/* $Id: vlanicch.h,v 1.12 2016/01/20 10:57:16 siva Exp $     */
/*****************************************************************************/
/* Copyright (C) 2015 Aricent Inc . All Rights Reserved     */
/* Licensee Aricent Inc., 2006-2015                        */
/*****************************************************************************/
/*    FILE  NAME            : vlanicch.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : VLAN ICCH                                      */
/*    MODULE NAME           : VLAN/GARP                                      */
/*                                                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 02 Jan 2015                                   */
/*    AUTHOR                : ICCH TEAM                                      */
/*    DESCRIPTION           : This file contains all constants and typedefs  */
/*                            for VLAN ICCH module.                          */
/*---------------------------------------------------------------------------*/
#ifndef _VLANICCH_H_
#define _VLANICCH_H_

   

/* Type of synchronization messages used in VLAN */
enum
{
    VLAN_ICCH_SYNC_UCAST_FDB =5,
    VLAN_ICCH_SYNC_FDB_FLUSH,
    VLAN_ICCH_SYNC_PORT_FLUSH, 
    VLAN_ICCH_SYNC_VLAN_FLUSH, 
    VLAN_ICCH_SYNC_PORT_VLAN_FLUSH,
    VLAN_ICCH_HIT_BIT_REQUEST,
    VLAN_ICCH_HIT_BIT_REPLY,
    VLAN_ICCH_CHECK_PORT_STATUS,
    VLAN_ICCH_SYNC_PORT_DOWN
};

/* E,vent types processed in VLAN for ICCH */
enum {
    VLAN_ICCH_BULK_UPD_REQUEST = ICCH_BULK_UPDT_REQ_MSG,
    VLAN_ICCH_BULK_UPD_TAIL_MESSAGE = ICCH_BULK_UPDT_TAIL_MSG,
    VLAN_ICCH_UPDATE_MESSAGE
};

#define VLAN_MCLAG_FDB_ENTRY               3
#define VLAN_ICCH_MAX_MSG_SIZE              1500
#define VLAN_ICCH_BULK_UPD_TAIL_MSG_SIZE    3

#define VLAN_ICCH_TYPE_FIELD_SIZE 1
#define VLAN_ICCH_BULK_REQ_MSG_SIZE 3
#define VLAN_ICCH_LEN_FIELD_SIZE 2
#define VLAN_ICCH_CONTEXT_FIELD_SIZE 4
#define VLAN_ICCH_FDB_ID_LEN 2
#define VLAN_ICCH_FLAG_LEN 2
#define VLAN_ICCH_MAC_ADDR_LEN sizeof (tMacAddr)
#define VLAN_ICCH_FDBTYPE_LEN 1
#define VLAN_ICCH_FLUSH_FDB_LEN 7
#define VLAN_ICCH_PORT_FLUSH_MSG_LEN 11
#define VLAN_ICCH_VLAN_FLUSH_MSG_LEN 9
#define VLAN_ICCH_PORT_VLAN_FLUSH_LEN 13
#define VLAN_ICCH_FDB_DEL_REQ_MSG_LEN 7
#define VLAN_ICCH_LEN_NO_OF_ENTRIES 4
#define VLAN_ICCH_LEN_HIT_REQ_VALUE 8
#define VLAN_ICCH_LEN_HIT_REP_VALUE 9

/* Macros to write in to ICCH buffer. */
#define VLAN_ICCH_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    ICCH_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define VLAN_ICCH_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    ICCH_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define VLAN_ICCH_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    ICCH_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define VLAN_ICCH_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    ICCH_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define VLAN_ICCH_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    ICCH_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define VLAN_ICCH_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    ICCH_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define VLAN_ICCH_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    ICCH_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define VLAN_ICCH_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    ICCH_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size; \
}while (0)


/* tVlanIcchData - Used to enqueue message to Icch */
typedef struct _VlanIcchData {
    UINT4         u4Events;  /* Events from ICCH to VLAN */
    tIcchMsg     *pIcchMsg;  /* Message buffer sent from ICCH to VLAN */
    UINT2         u2DataLen; /* Buffer length */
    UINT1         au1Reserved[2];
}tVlanIcchData;


/* Unicast FDB sync format */
typedef struct _VlanIcchUcastFdbSync {
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH]; /* VLAN_IDENTIFIER */
    tMacAddr UcastAddr;                           /* Unicast FDB MAC address */
    tMacAddr ConnectionId;                        /* Connection Identifier */
    UINT4    u4Context;                           /* Context identifier */
    UINT4    u4IfIndex;                           /* Interface Index */
    UINT4    u4PwIndex;                           /* PW Index*/
    tVlanId  VlanId;                              /* FID index */
    UINT2    u2Flag;                              /* VLAN ADD/VLAN_DEL/VLAN_UPDATE */
}tVlanIcchUcastFdbSync;

/* Multicast FDB sync format */
typedef struct _VlanIcchMcastFdbSync {
    UINT1    au1IfName[CFA_MAX_PORT_NAME_LENGTH]; /* Interface name */
    tMacAddr McastAddr;                           /* Multicat MAC Address */
    tVlanId  VlanId;                              /* VLAN Identifier */
    UINT4    u4Context;                           /* Context identifier */
    UINT2    u2Flag;                              /* FLAG VLAN_ADD/VLAN_DEL/VLAN_UPDATE */
 UINT1    au1Pad[2];
}tVlanIcchMcastFdbSync;


#define MCAG_TASK_ID gMcagTaskInfo.McagTaskId
#define MCAG_SEM_ID gMcagTaskInfo.McagSemId
#define VLAN_MCAG_MAX_MAC_AGING_ENTRIES 30
#define VLAN_MAC_AGING_TIME 5

#define MCAG_SEM_NAME                ((const UINT1 *)"MAC1")
#define MCAG_TASK            ((const UINT1*)"MCAG")
#define MCAG_TASK_QUEUE      ((UINT1*)"MACQ")
#define MCAG_Q_DEPTH         50
#define MCAG_TASK_QUEUE_ID   gMcagTaskInfo.McagQId

/* Trace for MAC Aging Task */
/* All traces are set to crtical by default, so user is not required
 * to enable specfic traces for MAC Aging task */
#define MCAG_TRC_FLAG 0x1
#define CRITICAL_DEFAULT_TRC 0x1

#define MCAG_TRC(TraceType, Str)                                              \
            MOD_TRC(MCAG_TRC_FLAG, TraceType, MCAG_NAME, Str)


















/* Prototypes */
INT4 VlanIcchCheckAndSendMsg (tIcchMsg **ppIcchMsg, UINT2 u2MsgLen, UINT2 *pu2OffSet);
INT4 VlanIcchRegisterWithICCH  PROTO ((VOID));
INT4 VlanIcchHandleUpdateEvents (UINT1 u1Event , tIcchMsg * pData , UINT2 u2DataLen );
VOID VlanIcchHandleUpdates (tIcchMsg * pMsg , UINT2 u2DataLen );
VOID VlanIcchSendBulkUpdates  PROTO ((VOID));
INT4 VlanIcchSyncBulkVlanEntries (UINT4 u4Context);
INT4 VlanSyncVlanEntriesforIfIndex (tIcchMsg **ppMsg , UINT4 u4Context , UINT2 *pu2Offset,
                                    UINT4 u4IfIndex);
INT1 VlanIcchGetIfNameFromPortList (tLocalPortList PortList , UINT1 *au1IfName );
INT4 VlanIcchSyncUcastAddress (tVlanIcchUcastFdbSync *pVlanIcchFdbSync );
INT4 VlanIcchSendMsgToIcch (tIcchMsg * pMsg , UINT2 u2Len );
INT4 VlanIcchProcessUcastSyncMessage (tVlanIcchUcastFdbSync *pVlanIcchUcastFdbSync );
UINT4 VlanIcchRegisterProtocols (tIcchRegParams * pIcchReg);
UINT4 VlanIcchEnqMsgToIcchFromAppl (tIcchMsg * pIcchMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                          UINT4 u4DestEntId);
UINT1 VlanIcchHandleProtocolEvent (tIcchProtoEvt * pEvt);
INT4 VlanIcchSendBulkUpdTailMsg (VOID) ;
VOID VlanIcchSendBulkReqMsg (VOID);
VOID VlanIcchProcessPeerDown (VOID) ;
INT1 VlanIcchGetIfNameFromPortArray (tHwPortArray *pHwPortArray, UINT1 *au1IfName);
VOID VlanIcchSyncClearMacTable (VOID);
VOID VlanIcchSyncPortFlush (UINT4 u4IfIndex);
VOID VlanIcchSendPortDown (UINT4 u4IfIndex);
VOID VlanIcchCheckPortStatus (UINT4 u4IfIndex);
VOID VlanIcchSyncVlanFlush (tVlanId VlanId);
VOID VlanIcchSyncPortVlanFlush (tVlanId VlanId, UINT4 u4IfIndex);
INT4 VlanIcchFormHitBitRequest (VOID);
INT4 VlanIcchProcessHitBitRequest(tIcchMsg *pMsg,UINT2 u2OffSet,UINT4 u4Entries);
INT4 VlanIcchProcessHitBitReply(tIcchMsg *pMsg,UINT2 u2OffSet,UINT4 u4Entries);
VOID VlanIcchProcessMCLAGEnableStatus (VOID);
VOID VlanIcchProcessMCLAGDisableStatus (VOID);
VOID VlanDelIsolationTblForIccl (VOID);
VOID VlanIcchProcessCheckPortStatus (tVlanIcchUcastFdbSync *pVlanIcchFdbCheckReq);
VOID VlanIcchProcessMclagOperUp (UINT4 u4IfIndex);
VOID VlanIcchProcessMclagDown (UINT4 u4IfIndex, UINT1 u1Status);
INT4 VlanIcchCheckMacAddrOnMclag (tMacAddr MacAddr,tVlanId VlanId);
VOID VlanIcchHandleMclagOperStatusChange (tMacAddr MacAddr,tVlanId VlanId);

#endif /* _VLANICCH_H_ */
