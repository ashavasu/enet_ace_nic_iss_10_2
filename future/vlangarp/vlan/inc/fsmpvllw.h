/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpvllw.h,v 1.28.22.1 2018/03/15 12:59:54 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanGlobalTrace ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanGlobalTrace ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanGlobalTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanGlobalsTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanGlobalsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanGlobalsTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanGlobalsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanGlobalsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanMacBasedOnAllPorts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortProtoBasedOnAllPorts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanShutdownStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanDebug ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanLearningMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanHybridTypeDefault ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanContextName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIDot1qFutureGarpDebug ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureUnicastMacLearningLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureBaseBridgeMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanSubnetBasedOnAllPorts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanGlobalMacLearningStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanGlobalsFdbFlush ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanUserDefinedTPID ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanRemoteFdbFlush ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanMacBasedOnAllPorts ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortProtoBasedOnAllPorts ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanShutdownStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanDebug ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanLearningMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanHybridTypeDefault ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureGarpDebug ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureUnicastMacLearningLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIDot1qFutureBaseBridgeMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanSubnetBasedOnAllPorts ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanGlobalMacLearningStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanApplyEnhancedFilteringCriteria ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanGlobalsFdbFlush ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanUserDefinedTPID ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanRemoteFdbFlush ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanMacBasedOnAllPorts ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortProtoBasedOnAllPorts ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanShutdownStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanDebug ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanLearningMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanHybridTypeDefault ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureGarpDebug ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureUnicastMacLearningLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIDot1qFutureBaseBridgeMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanSubnetBasedOnAllPorts ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanGlobalMacLearningStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanApplyEnhancedFilteringCriteria ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanGlobalsFdbFlush ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanUserDefinedTPID ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanRemoteFdbFlush ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanGlobalsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanPortTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanPortTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanPortType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortMacBasedClassification ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortPortProtoBasedClassification ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanFilteringUtilityCriteria ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortProtected ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortSubnetBasedClassification ARG_LIST((INT4 ,INT4 *));

/* Low Level GET Routine for All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanPortUnicastMacLearning ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpJoinEmptyTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpJoinEmptyRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpJoinInTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpJoinInRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveInTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveInRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpEmptyTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpEmptyRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveAllTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveAllRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGmrpDiscardCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpJoinEmptyTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpJoinEmptyRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpJoinInTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpJoinInRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveInTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveInRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpEmptyTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpEmptyRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveAllTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveAllRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortGvrpDiscardCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortFdbFlush ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortIngressEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortEgressEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortEgressTPIDType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortAllowableTPID1 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortAllowableTPID2 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortAllowableTPID3 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortClearGarpStats ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFuturePortPacketReflectionStatus ARG_LIST((INT4 ,INT4 *));   

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanPortType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortMacBasedClassification ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortPortProtoBasedClassification ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanFilteringUtilityCriteria ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortProtected ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortSubnetBasedClassification ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortUnicastMacLearning ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortFdbFlush ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortIngressEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortEgressEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortEgressTPIDType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortAllowableTPID1 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortAllowableTPID2 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortAllowableTPID3 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortClearGarpStats ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFuturePortPacketReflectionStatus ARG_LIST((INT4  ,INT4 )); 

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanPortType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortMacBasedClassification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortPortProtoBasedClassification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanFilteringUtilityCriteria ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortProtected ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortSubnetBasedClassification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortUnicastMacLearning ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortFdbFlush ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortIngressEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortEgressEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortEgressTPIDType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID1 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID2 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortAllowableTPID3 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortClearGarpStats ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFuturePortPacketReflectionStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));                                                                                                                                              

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanPortMacMapTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanPortMacMapTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanPortMacMapTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanPortMacMapTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanPortMacMapTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanPortMacMapVid ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortMacMapName ARG_LIST((INT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIDot1qFutureVlanPortMacMapMcastBcastOption ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortMacMapRowStatus ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanPortMacMapVid ARG_LIST((INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortMacMapName ARG_LIST((INT4  , tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIDot1qFutureVlanPortMacMapMcastBcastOption ARG_LIST((INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortMacMapRowStatus ARG_LIST((INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanPortMacMapVid ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortMacMapName ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIDot1qFutureVlanPortMacMapMcastBcastOption ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortMacMapRowStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanPortMacMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanFidMapTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanFidMapTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanFidMapTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanFidMapTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanFidMapTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanFid ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanFid ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanFid ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanFidMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanTunnelConfigTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanTunnelConfigTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanTunnelConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanTunnelConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanBridgeMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanTunnelBpduPri ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanBridgeMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanTunnelBpduPri ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanBridgeMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanTunnelBpduPri ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanTunnelConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanTunnelTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanTunnelTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanTunnelTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanTunnelTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanTunnelStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanTunnelStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanTunnelStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanTunnelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanTunnelProtocolTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanTunnelProtocolTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanTunnelProtocolTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanTunnelProtocolTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanTunnelStpPDUs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanTunnelStpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanTunnelStpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanTunnelGvrpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanTunnelIgmpPkts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanTunnelIgmpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanTunnelIgmpPktsSent ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanTunnelStpPDUs ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanTunnelGvrpPDUs ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanTunnelIgmpPkts ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanTunnelStpPDUs ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanTunnelGvrpPDUs ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanTunnelIgmpPkts ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanTunnelProtocolTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanCounterTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanCounterTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanCounterTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanCounterTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanCounterTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanCounterRxUcast ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanCounterRxMcastBcast ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanCounterTxUnknUcast ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanCounterTxUcast ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanCounterTxBcast ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanCounterRxFrames ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanCounterRxBytes ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanCounterTxFrames ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanCounterTxBytes ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanCounterDiscardFrames ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanCounterDiscardBytes ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanCounterStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanCounterStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanCounterStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanCounterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanUnicastMacControlTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanUnicastMacControlTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanUnicastMacControlTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanUnicastMacControlTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanUnicastMacControlTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanUnicastMacLimit ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIDot1qFutureVlanAdminMacLearningStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanOperMacLearningStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanUnicastMacLimit ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIDot1qFutureVlanAdminMacLearningStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanUnicastMacLimit ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanAdminMacLearningStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanUnicastMacControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureGarpGlobalTrace ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureGarpGlobalTrace ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureGarpGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanTpFdbTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanTpFdbTable ARG_LIST((INT4  , UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanTpFdbTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanTpFdbTable ARG_LIST((INT4 * , UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanTpFdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanOldTpFdbPort ARG_LIST((INT4  , UINT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMIDot1qFutureConnectionIdentifier ARG_LIST((INT4  , UINT4  , tMacAddr ,tMacAddr * ));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanWildCardTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanWildCardTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanWildCardTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanWildCardTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanWildCardTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanWildCardRowStatus ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanWildCardRowStatus ARG_LIST((INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanWildCardRowStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanWildCardTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanWildCardPortTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanWildCardPortTable ARG_LIST((INT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanWildCardPortTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanWildCardPortTable ARG_LIST((INT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanWildCardPortTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanIsWildCardEgressPort ARG_LIST((INT4  , tMacAddr  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanIsWildCardEgressPort ARG_LIST((INT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanIsWildCardEgressPort ARG_LIST((UINT4 *  ,INT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanWildCardPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureStaticUnicastExtnTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureStaticUnicastExtnTable ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureStaticUnicastExtnTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureStaticUnicastExtnTable ARG_LIST((INT4 * , UINT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureStaticUnicastExtnTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureStaticConnectionIdentifier ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4 ,tMacAddr * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureStaticConnectionIdentifier ARG_LIST((INT4  , UINT4  , tMacAddr  , INT4  ,tMacAddr ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureStaticConnectionIdentifier ARG_LIST((UINT4 *  ,INT4  , UINT4  , tMacAddr  , INT4  ,tMacAddr ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureStaticUnicastExtnTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanPortSubnetMapTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanPortSubnetMapTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanPortSubnetMapTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanPortSubnetMapVid ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortSubnetMapARPOption ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanPortSubnetMapVid ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortSubnetMapARPOption ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanPortSubnetMapVid ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortSubnetMapARPOption ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanPortSubnetMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanPortSubnetMapExtTable.*/
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanPortSubnetMapExtTable ARG_LIST
((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanPortSubnetMapExtTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanPortSubnetMapExtTable ARG_LIST
((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable ARG_LIST
((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanPortSubnetMapExtVid ARG_LIST
((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortSubnetMapExtARPOption ARG_LIST
((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureVlanPortSubnetMapExtRowStatus ARG_LIST
((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanPortSubnetMapExtVid ARG_LIST
((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortSubnetMapExtARPOption ARG_LIST
((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureVlanPortSubnetMapExtRowStatus ARG_LIST
((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtVid ARG_LIST
((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtARPOption ARG_LIST
((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureVlanPortSubnetMapExtRowStatus ARG_LIST
((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanPortSubnetMapExtTable ARG_LIST
((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureStVlanExtTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureStVlanExtTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureStVlanExtTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureStVlanExtTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureStVlanExtTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanSwStatsEnabled ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanSwStatsEnabled ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanSwStatsEnabled ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanSwStatsEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFsMIDot1qFutureStVlanFdbFlush ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureStVlanEgressEthertype ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIDot1qFutureStVlanType ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIDot1qFutureStVlanVid ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhSetFsMIDot1qFutureStVlanFdbFlush ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIDot1qFutureStVlanEgressEthertype ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */


INT1
nmhTestv2FsMIDot1qFutureStVlanFdbFlush ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIDot1qFutureStVlanEgressEthertype ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureStVlanExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto Validate Index Instance for FsMIDot1qFuturePortVlanExtTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFuturePortVlanExtTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFuturePortVlanExtTable  */

INT1
nmhGetFirstIndexFsMIDot1qFuturePortVlanExtTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFuturePortVlanExtTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFuturePortVlanFdbFlush ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFuturePortVlanFdbFlush ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFuturePortVlanFdbFlush ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFuturePortVlanExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIDot1qFutureVlanLoopbackTable. */
INT1
nmhValidateIndexInstanceFsMIDot1qFutureVlanLoopbackTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIDot1qFutureVlanLoopbackTable  */

INT1
nmhGetFirstIndexFsMIDot1qFutureVlanLoopbackTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIDot1qFutureVlanLoopbackTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIDot1qFutureVlanLoopbackStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIDot1qFutureVlanLoopbackStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIDot1qFutureVlanLoopbackStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIDot1qFutureVlanLoopbackTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
