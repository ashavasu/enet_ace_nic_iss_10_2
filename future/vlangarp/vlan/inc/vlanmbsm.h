/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanmbsm.h,v 1.12 2016/06/01 09:50:57 siva Exp $
 *
 *******************************************************************/
#ifdef MBSM_WANTED


#define MBSM_VLAN    "MbsmVlan"

INT4 VlanMbsmUpdateOnCardInsertion (tMbsmPortInfo *pPortInfo, 
                          tMbsmSlotInfo *pSlotInfo);
INT4 VlanMbsmUpdtVlanEntriesToHw (tMbsmPortInfo * pPortInfo,
                                  tMbsmSlotInfo * pSlotInfo,
                                  tLocalPortList SlotLocalPorts);
INT4 VlanMbsmUpdtStUcastTblToHw (tMbsmPortInfo * pPortInfo,
                                 tMbsmSlotInfo * pSlotInfo,
                                 tLocalPortList SlotLocalPorts);
INT4 VlanMbsmUpdtMcastTblToHw (tMbsmPortInfo * pMBSMPortInfo,
                               tMbsmSlotInfo * pSlotInfo,
                               tLocalPortList SlotLocalPorts);
INT4 VlanMbsmUpdtPortPropertiesToHw (tMbsmPortInfo *pMbsmPortInfo, 
                                     tMbsmSlotInfo *pSlotInfo);

INT4 VlanMbsmSetPortTunnelModeToHw (tMbsmPortInfo *pMBSMPortInfo, 
                                    tLocalPortList  LocalPorts);

INT4 VlanMbsmUpdateOnCardRemoval (tMbsmPortInfo *pPortInfo, 
                                tMbsmSlotInfo *pSlotInfo);
INT4 VlanMbsmResetVlanTbl (tMbsmPortInfo *pPortInfo, 
                           tMbsmSlotInfo *pSlotInfo, 
                           tLocalPortList  LocalPorts);
INT4 VlanMbsmResetStUcastTbl (tMbsmPortInfo *pPortInfo, 
                              tMbsmSlotInfo *pSlotInfo, 
                              tLocalPortList  LocalPorts);
INT4 VlanMbsmResetMcastTbl (tMbsmPortInfo *pPortInfo, 
                            tMbsmSlotInfo *pSlotInfo, 
                            tLocalPortList  LocalPorts);


INT4 VlanMbsmProcessUpdateMessage PROTO ((tMbsmProtoMsg *, INT4)) ;

INT4 VlanMbsmUpdtVlanTblForLoadSharing (UINT1 u1Flag);
INT4 VlanMbsmUpdtL2McForLoadSharing (UINT1 u1Flag);
INT4
VlanMbsmUpdtWildCardTblToHw (tMbsmPortInfo * pMBSMPortInfo,
                             tMbsmSlotInfo * pSlotInfo,
                             tLocalPortList SlotLocalPorts);
INT4
VlanMbsmResetWildCardTbl (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo,
                          tLocalPortList LocalPorts);

INT4 VlanMbsmUpdatePortLearningStatus (tMbsmSlotInfo * pSlotInfo,
                                       UINT2 u2PortId,
                                       INT4 i4VlanFilteringUtilityCriteria);

INT4 VlanMbsmGetProtocolTunnelStatus (UINT1 u1ProtocolId, UINT2 u2PortNum, 
                                      UINT1 *pu1TunnelStatus);

VOID
VlanMbsmGetPortListForCurrContext (tMbsmPortInfo *pPortInfo,
       tLocalPortList LocalPortList);
INT4
VlanMbsmProgramDynamicFdbEntry(tMbsmSlotInfo * pSlotInfo);

INT4
VlanSendAckToMbsm(INT4 i4RetStatus);
#ifdef EVB_WANTED
INT4
VlanMbsmUpdtEvbConfig (tMbsmPortInfo * pMbsmPortInfo,
                       tMbsmSlotInfo * pSlotInfo);
#endif
#endif /* MBSM_WANTED */
