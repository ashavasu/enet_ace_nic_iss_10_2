/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdpbdb.h,v 1.1 2011/06/17 05:40:15 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDDOTDB_H
#define _STDDOTDB_H

UINT1 Ieee8021PbVidTranslationTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Ieee8021PbCVidRegistrationTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Ieee8021PbEdgePortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Ieee8021PbServicePriorityRegenerationTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021PbCnpTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021PbPnpTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021PbCepTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stddot [] ={1,3,111,2,802,1,1,5};
tSNMP_OID_TYPE stddotOID = {8, stddot};


UINT4 Ieee8021PbVidTranslationLocalVid [ ] ={1,3,111,2,802,1,1,5,1,1,1,1};
UINT4 Ieee8021PbVidTranslationRelayVid [ ] ={1,3,111,2,802,1,1,5,1,1,1,2};
UINT4 Ieee8021PbVidTranslationRowStatus [ ] ={1,3,111,2,802,1,1,5,1,1,1,3};
UINT4 Ieee8021PbCVidRegistrationCVid [ ] ={1,3,111,2,802,1,1,5,1,2,1,1};
UINT4 Ieee8021PbCVidRegistrationSVid [ ] ={1,3,111,2,802,1,1,5,1,2,1,2};
UINT4 Ieee8021PbCVidRegistrationUntaggedPep [ ] ={1,3,111,2,802,1,1,5,1,2,1,3};
UINT4 Ieee8021PbCVidRegistrationUntaggedCep [ ] ={1,3,111,2,802,1,1,5,1,2,1,4};
UINT4 Ieee8021PbCVidRegistrationRowStatus [ ] ={1,3,111,2,802,1,1,5,1,2,1,5};
UINT4 Ieee8021PbEdgePortSVid [ ] ={1,3,111,2,802,1,1,5,1,3,1,1};
UINT4 Ieee8021PbEdgePortPVID [ ] ={1,3,111,2,802,1,1,5,1,3,1,2};
UINT4 Ieee8021PbEdgePortDefaultUserPriority [ ] ={1,3,111,2,802,1,1,5,1,3,1,3};
UINT4 Ieee8021PbEdgePortAcceptableFrameTypes [ ] ={1,3,111,2,802,1,1,5,1,3,1,4};
UINT4 Ieee8021PbEdgePortEnableIngressFiltering [ ] ={1,3,111,2,802,1,1,5,1,3,1,5};
UINT4 Ieee8021PbServicePriorityRegenerationSVid [ ] ={1,3,111,2,802,1,1,5,1,4,1,1};
UINT4 Ieee8021PbServicePriorityRegenerationReceivedPriority [ ] ={1,3,111,2,802,1,1,5,1,4,1,2};
UINT4 Ieee8021PbServicePriorityRegenerationRegeneratedPriority [ ] ={1,3,111,2,802,1,1,5,1,4,1,3};
UINT4 Ieee8021PbCnpCComponentId [ ] ={1,3,111,2,802,1,1,5,1,5,1,1};
UINT4 Ieee8021PbCnpSVid [ ] ={1,3,111,2,802,1,1,5,1,5,1,2};
UINT4 Ieee8021PbCnpRowStatus [ ] ={1,3,111,2,802,1,1,5,1,5,1,3};
UINT4 Ieee8021PbPnpRowStatus [ ] ={1,3,111,2,802,1,1,5,1,6,1,1};
UINT4 Ieee8021PbCepCComponentId [ ] ={1,3,111,2,802,1,1,5,1,7,1,1};
UINT4 Ieee8021PbCepCepPortNumber [ ] ={1,3,111,2,802,1,1,5,1,7,1,2};
UINT4 Ieee8021PbCepRowStatus [ ] ={1,3,111,2,802,1,1,5,1,7,1,3};




tMbDbEntry stddotMibEntry[]= {

{{12,Ieee8021PbVidTranslationLocalVid}, GetNextIndexIeee8021PbVidTranslationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021PbVidTranslationTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021PbVidTranslationRelayVid}, GetNextIndexIeee8021PbVidTranslationTable, Ieee8021PbVidTranslationRelayVidGet, Ieee8021PbVidTranslationRelayVidSet, Ieee8021PbVidTranslationRelayVidTest, Ieee8021PbVidTranslationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbVidTranslationTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021PbVidTranslationRowStatus}, GetNextIndexIeee8021PbVidTranslationTable, Ieee8021PbVidTranslationRowStatusGet, Ieee8021PbVidTranslationRowStatusSet, Ieee8021PbVidTranslationRowStatusTest, Ieee8021PbVidTranslationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbVidTranslationTableINDEX, 3, 0, 1, NULL},

{{12,Ieee8021PbCVidRegistrationCVid}, GetNextIndexIeee8021PbCVidRegistrationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021PbCVidRegistrationTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021PbCVidRegistrationSVid}, GetNextIndexIeee8021PbCVidRegistrationTable, Ieee8021PbCVidRegistrationSVidGet, Ieee8021PbCVidRegistrationSVidSet, Ieee8021PbCVidRegistrationSVidTest, Ieee8021PbCVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbCVidRegistrationTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021PbCVidRegistrationUntaggedPep}, GetNextIndexIeee8021PbCVidRegistrationTable, Ieee8021PbCVidRegistrationUntaggedPepGet, Ieee8021PbCVidRegistrationUntaggedPepSet, Ieee8021PbCVidRegistrationUntaggedPepTest, Ieee8021PbCVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbCVidRegistrationTableINDEX, 3, 0, 0, "1"},

{{12,Ieee8021PbCVidRegistrationUntaggedCep}, GetNextIndexIeee8021PbCVidRegistrationTable, Ieee8021PbCVidRegistrationUntaggedCepGet, Ieee8021PbCVidRegistrationUntaggedCepSet, Ieee8021PbCVidRegistrationUntaggedCepTest, Ieee8021PbCVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbCVidRegistrationTableINDEX, 3, 0, 0, "1"},

{{12,Ieee8021PbCVidRegistrationRowStatus}, GetNextIndexIeee8021PbCVidRegistrationTable, Ieee8021PbCVidRegistrationRowStatusGet, Ieee8021PbCVidRegistrationRowStatusSet, Ieee8021PbCVidRegistrationRowStatusTest, Ieee8021PbCVidRegistrationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbCVidRegistrationTableINDEX, 3, 0, 1, NULL},

{{12,Ieee8021PbEdgePortSVid}, GetNextIndexIeee8021PbEdgePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021PbEdgePortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021PbEdgePortPVID}, GetNextIndexIeee8021PbEdgePortTable, Ieee8021PbEdgePortPVIDGet, Ieee8021PbEdgePortPVIDSet, Ieee8021PbEdgePortPVIDTest, Ieee8021PbEdgePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbEdgePortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021PbEdgePortDefaultUserPriority}, GetNextIndexIeee8021PbEdgePortTable, Ieee8021PbEdgePortDefaultUserPriorityGet, Ieee8021PbEdgePortDefaultUserPrioritySet, Ieee8021PbEdgePortDefaultUserPriorityTest, Ieee8021PbEdgePortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021PbEdgePortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021PbEdgePortAcceptableFrameTypes}, GetNextIndexIeee8021PbEdgePortTable, Ieee8021PbEdgePortAcceptableFrameTypesGet, Ieee8021PbEdgePortAcceptableFrameTypesSet, Ieee8021PbEdgePortAcceptableFrameTypesTest, Ieee8021PbEdgePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbEdgePortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021PbEdgePortEnableIngressFiltering}, GetNextIndexIeee8021PbEdgePortTable, Ieee8021PbEdgePortEnableIngressFilteringGet, Ieee8021PbEdgePortEnableIngressFilteringSet, Ieee8021PbEdgePortEnableIngressFilteringTest, Ieee8021PbEdgePortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbEdgePortTableINDEX, 3, 0, 0, "1"},

{{12,Ieee8021PbServicePriorityRegenerationSVid}, GetNextIndexIeee8021PbServicePriorityRegenerationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021PbServicePriorityRegenerationTableINDEX, 4, 0, 0, NULL},

{{12,Ieee8021PbServicePriorityRegenerationReceivedPriority}, GetNextIndexIeee8021PbServicePriorityRegenerationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021PbServicePriorityRegenerationTableINDEX, 4, 0, 0, NULL},

{{12,Ieee8021PbServicePriorityRegenerationRegeneratedPriority}, GetNextIndexIeee8021PbServicePriorityRegenerationTable, Ieee8021PbServicePriorityRegenerationRegeneratedPriorityGet, Ieee8021PbServicePriorityRegenerationRegeneratedPrioritySet, Ieee8021PbServicePriorityRegenerationRegeneratedPriorityTest, Ieee8021PbServicePriorityRegenerationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021PbServicePriorityRegenerationTableINDEX, 4, 0, 0, NULL},

{{12,Ieee8021PbCnpCComponentId}, GetNextIndexIeee8021PbCnpTable, Ieee8021PbCnpCComponentIdGet, Ieee8021PbCnpCComponentIdSet, Ieee8021PbCnpCComponentIdTest, Ieee8021PbCnpTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021PbCnpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021PbCnpSVid}, GetNextIndexIeee8021PbCnpTable, Ieee8021PbCnpSVidGet, Ieee8021PbCnpSVidSet, Ieee8021PbCnpSVidTest, Ieee8021PbCnpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021PbCnpTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021PbCnpRowStatus}, GetNextIndexIeee8021PbCnpTable, Ieee8021PbCnpRowStatusGet, Ieee8021PbCnpRowStatusSet, Ieee8021PbCnpRowStatusTest, Ieee8021PbCnpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbCnpTableINDEX, 2, 0, 1, NULL},

{{12,Ieee8021PbPnpRowStatus}, GetNextIndexIeee8021PbPnpTable, Ieee8021PbPnpRowStatusGet, Ieee8021PbPnpRowStatusSet, Ieee8021PbPnpRowStatusTest, Ieee8021PbPnpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbPnpTableINDEX, 2, 0, 1, NULL},

{{12,Ieee8021PbCepCComponentId}, GetNextIndexIeee8021PbCepTable, Ieee8021PbCepCComponentIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021PbCepTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021PbCepCepPortNumber}, GetNextIndexIeee8021PbCepTable, Ieee8021PbCepCepPortNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021PbCepTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021PbCepRowStatus}, GetNextIndexIeee8021PbCepTable, Ieee8021PbCepRowStatusGet, Ieee8021PbCepRowStatusSet, Ieee8021PbCepRowStatusTest, Ieee8021PbCepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbCepTableINDEX, 2, 0, 1, NULL},
};
tMibData stddotEntry = { 23, stddotMibEntry };

#endif /* _STDDOTDB_H */

