/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmsbrlw.h,v 1.5 2008/08/20 15:29:50 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsDot1dBaseTable. */
INT1
nmhValidateIndexInstanceFsDot1dBaseTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dBaseTable  */

INT1
nmhGetFirstIndexFsDot1dBaseTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dBaseTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dBaseBridgeAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsDot1dBaseNumPorts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dBaseType ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FsDot1dBasePortTable. */
INT1
nmhValidateIndexInstanceFsDot1dBasePortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dBasePortTable  */

INT1
nmhGetFirstIndexFsDot1dBasePortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dBasePortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dBasePortIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dBasePortCircuit ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsDot1dBasePortDelayExceededDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot1dBasePortMtuExceededDiscards ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsDot1dStpTable. */
INT1
nmhValidateIndexInstanceFsDot1dStpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dStpTable  */

INT1
nmhGetFirstIndexFsDot1dStpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dStpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dStpProtocolSpecification ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpTimeSinceTopologyChange ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot1dStpTopChanges ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot1dStpDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot1dStpRootCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpRootPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpMaxAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpHoldTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpForwardDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpBridgeMaxAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpBridgeHelloTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpBridgeForwardDelay ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1dStpPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1dStpBridgeMaxAge ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1dStpBridgeHelloTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1dStpBridgeForwardDelay ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1dStpPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dStpBridgeMaxAge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dStpBridgeHelloTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dStpBridgeForwardDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1dStpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1dStpPortTable. */
INT1
nmhValidateIndexInstanceFsDot1dStpPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dStpPortTable  */

INT1
nmhGetFirstIndexFsDot1dStpPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dStpPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dStpPortPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpPortState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpPortEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpPortPathCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpPortDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot1dStpPortDesignatedCost ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dStpPortDesignatedBridge ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot1dStpPortDesignatedPort ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot1dStpPortForwardTransitions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot1dStpPortPathCost32 ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1dStpPortPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1dStpPortEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1dStpPortPathCost ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot1dStpPortPathCost32 ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1dStpPortPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dStpPortEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dStpPortPathCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dStpPortPathCost32 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1dStpPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1dTpTable. */
INT1
nmhValidateIndexInstanceFsDot1dTpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dTpTable  */

INT1
nmhGetFirstIndexFsDot1dTpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dTpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dTpLearnedEntryDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot1dTpAgingTime ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1dTpAgingTime ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1dTpAgingTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1dTpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1dTpFdbTable. */
INT1
nmhValidateIndexInstanceFsDot1dTpFdbTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dTpFdbTable  */

INT1
nmhGetFirstIndexFsDot1dTpFdbTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dTpFdbTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dTpFdbPort ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsDot1dTpFdbStatus ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Proto Validate Index Instance for FsDot1dTpPortTable. */
INT1
nmhValidateIndexInstanceFsDot1dTpPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dTpPortTable  */

INT1
nmhGetFirstIndexFsDot1dTpPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dTpPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dTpPortMaxInfo ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot1dTpPortInFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot1dTpPortOutFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot1dTpPortInDiscards ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsDot1dStaticTable. */
INT1
nmhValidateIndexInstanceFsDot1dStaticTable ARG_LIST((INT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dStaticTable  */

INT1
nmhGetFirstIndexFsDot1dStaticTable ARG_LIST((INT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dStaticTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dStaticRowStatus ARG_LIST((INT4  , tMacAddr  , INT4 ,INT4 *));

INT1
nmhGetFsDot1dStaticStatus ARG_LIST((INT4  , tMacAddr  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1dStaticRowStatus ARG_LIST((INT4  , tMacAddr  , INT4  ,INT4 ));

INT1
nmhSetFsDot1dStaticStatus ARG_LIST((INT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1dStaticRowStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  , INT4  ,INT4 ));

INT1
nmhTestv2FsDot1dStaticStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1dStaticTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot1dStaticAllowedToGoTable. */
INT1
nmhValidateIndexInstanceFsDot1dStaticAllowedToGoTable ARG_LIST((INT4  , tMacAddr  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot1dStaticAllowedToGoTable  */

INT1
nmhGetFirstIndexFsDot1dStaticAllowedToGoTable ARG_LIST((INT4 * , tMacAddr *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot1dStaticAllowedToGoTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot1dStaticAllowedIsMember ARG_LIST((INT4  , tMacAddr  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot1dStaticAllowedIsMember ARG_LIST((INT4  , tMacAddr  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot1dStaticAllowedIsMember ARG_LIST((UINT4 *  ,INT4  , tMacAddr  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot1dStaticAllowedToGoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
