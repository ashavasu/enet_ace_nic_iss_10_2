/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlnl2vpn.h,v 1.8 2015/11/20 10:20:26 siva Exp $
 *
 * Description:This file Contains VLAN module prototype definitions.
 *
 *******************************************************************/


#ifndef _VLNL2VPN_H
#define _VLNL2VPN_H

INT4     VlanL2VpnInit   (VOID);
VOID     VlanL2VpnDeInit (VOID);
INT4     VlanL2VpnCmpPortBasedMapTbl (tRBElem *pNodeA, tRBElem *pNodeB);
INT4     VlanL2VpnCmpPortVlanBasedMapTbl (tRBElem *pNodeA, tRBElem *pNodeB);
UINT1*   VlanL2VpnGetMemBlock (INT4 i4BufType);
VOID     VlanL2VpnReleaseMemBlock (INT4 u1BufType, UINT1 *pu1Buf);
INT4     VlanL2VpnFreeTreeNode (tRBElem *pNode, UINT4 u4NodeType);
VOID     VlanL2VpnHandleAddL2VpnInfo (tVplsInfo *pL2VpnData);
VOID     VlanL2VpnHandleDelL2VpnInfo (tVplsInfo *pL2VpnData);
VOID     VlanL2VpnHandleMplsPktFwdOnPorts (tCRU_BUF_CHAIN_DESC * pFrame, 
                                          tVlanIfMsg * pVlanIf);
UINT4     VlanL2VpnPktFloodOnMplsPort(tCRU_BUF_CHAIN_DESC *pFrame,
                                     tVlanIfMsg *pVlanIf, tVlanCurrEntry 
                      *pVlanEntry, tVlanTag *pInVlanTag);
VOID     VlanL2VpnFwdLearntPktOnMplsPort (tCRU_BUF_CHAIN_DESC *pFrame,
                                          tVlanIfMsg  *pVlanIf,
                                          tVlanCurrEntry *pVlanEntry,
                                          tVlanTag *pInVlanTag, 
                                          UINT4 u4VplsIndex);
INT4     VlanL2VpnIsMplsIfPresent (tVlanId VlanId);
VOID     VlanVplsDelPwFdbEntries (UINT4 u4PwVcIndex);
INT4     VlanL2VpnIsVlanMapEntryPresent (tVlanId VlanId);
UINT4    VlanL2VpnGetVplsIndex (UINT4 u4ContextId, UINT4 u4VlanId);
VOID VlanCheckAndRaiseVplsAlarm (tVlanFdbEntry *pFdbEntry);
#endif  /*__VLNL2VPN_H*/
