/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vlanprot.h,v 1.251 2017/12/26 11:06:16 siva Exp $
 *
 * Description:This file Contains VLAN module prototype definitions.
 *
 *******************************************************************/

#ifndef _VLANPROT_H
#define _VLANPROT_H

/*********Protypes for VLAN Module *******************/

/************************* VLANSYS.C *************************************/
INT4 
VlanTaskInit PROTO ((VOID));

VOID 
VlanMainDeInit PROTO ((VOID));

INT4
VlanHandleStartModule PROTO ((VOID));

INT4 
VlanInitGlobalInfo PROTO ((VOID));

INT4
VlanDeInitGlobalInfo PROTO ((VOID));

INT4 
VlanInit PROTO ((VOID));

VOID
VlanAssignMempoolIds PROTO ((VOID));

VOID 
VlanDeInit PROTO ((VOID));

VOID VlanGetAllPorts (VOID);

VOID
VlanHwAddStaticInfo (VOID);

VOID
VlanHwDelStaticInfo (VOID);

INT4 
VlanEnableVlan PROTO ((VOID));

VOID 
VlanDeleteFidTable PROTO ((VOID));

VOID
VlanDeleteStats PROTO ((VOID));

VOID 
VlanDeleteCurrTable PROTO ((VOID));

VOID
VlanHandleDeleteAllDynamicMcastInfo PROTO ((UINT4 u4Port, UINT1 u1DisableVlan,
                                            UINT1 u1MacAddrType));
VOID
VlanHandleDeleteAllDynamicVlanInfo PROTO ((UINT4 u4ContextId, UINT2 u2Port));

VOID
VlanHandleDelDynamicMcastInfoForVlan PROTO ((tVlanId VlanId));

VOID
VlanHandleDeleteAllDynamicDefGroupInfo PROTO ((UINT4 u4ContextId,
                                               UINT2 u2Port));

VOID 
VlanHandleDelDynamicDefGroupInfoForVlan PROTO ((tVlanId VlanId));

VOID 
VlanDeleteStVlanTable PROTO ((VOID));

VOID 
VlanDeleteConstraintsTable PROTO ((VOID));

VOID 
VlanDeleteMacMapTable PROTO ((VOID));

VOID 
VlanDeleteSubnetMapTable PROTO ((VOID));

VOID 
VlanHandleMacMapDeleteOnPort (UINT4 u4Port);
    
VOID 
VlanHandleDeleteFdbEntry (tVlanId VlanId, tMacAddr MacAddress, UINT2 u2InstanceId);

VOID
VlanHandleDeleteAllFdbEntry (VOID);

VOID
VlanFdbRemoveEntryFromHashTable (tVlanId VlanId, tMacAddr MacAddr);

VOID
VlanFdbRemoveEntryFromKernel (tVlanId VlanId, tMacAddr MacAddress);

VOID 
VlanRemoveFdbEntryFromKernel PROTO ((VOID));

INT4 
VlanPortTblInit PROTO ((VOID));
   
VOID VlanInitializeTunnelCounters PROTO((tVlanPortEntry  *pPortEntry));

VOID VlanInitializeDiscardCounters PROTO((tVlanPortEntry  *pPortEntry));

INT4
VlanL2IwfSetOverrideOption (UINT2 u2Port, UINT1 u1OverrideOption);

VOID
VlanL2IwfGetOverrideOption (UINT2 u2Port, UINT1 *pu1OverrideOption);

INT4 
VlanIngressFiltering PROTO ((tVlanCurrEntry *pVlanRec, 
                             UINT2 u2InPort));

INT4 
VlanEgressFiltering PROTO ((UINT2 u2Port, 
                            tVlanCurrEntry *pVlanEntry, tMacAddr DestAddr));
#ifdef HVPLS_WANTED
INT4
VlanMplsProcessMplsPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                      UINT4 u4VfId, UINT1 u1PktType, UINT4 u4PwVcIndex);
#endif
VOID
VlanProcessUcastDataPkt PROTO ((tCRU_BUF_CHAIN_DESC *pFrame,
                                tVlanIfMsg *pVlanIf,
                                tVlanCurrEntry *pVlanRec,
                                tVlanTag *pVlanTag));
VOID
VlanProcessMcastDataPkt PROTO ((tCRU_BUF_CHAIN_DESC *pFrame,
                                tVlanIfMsg *pVlanIf,
                                tVlanCurrEntry * pVlanRec,
                                tVlanTag *pVlanTag));

VOID 
VlanFwdOnSinglePort PROTO ((tCRU_BUF_CHAIN_DESC *pFrame, 
                            tVlanIfMsg *pVlanIf,
                            UINT2                u2Port, 
                            tVlanCurrEntry      *pVlanEntry, 
                            tVlanTag            *pInVlanTag, 
                            UINT1                u1FrameType));

VOID 
VlanFwdOnPorts PROTO ((tCRU_BUF_CHAIN_DESC *pFrame, 
                       tVlanIfMsg          *pVlanIf, 
                       tLocalPortList       PortList, 
                       tVlanCurrEntry      *pVlanEntry, 
                       tVlanTag            *pInVlanTag,
                       UINT1                u1FrameType));

VOID 
VlanProcessCfgEvent PROTO ((tVlanQMsg *pVlanQMsg));

VOID 
VlanHandleProcessPkt PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, 
                             tVlanIfMsg * pVlanIf));
INT4 
VlanPostCfgMessage PROTO ((UINT2 u2MsgType, 
                           UINT4 u4Port));

INT4 
VlanHandleCreatePort PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
            UINT2 u2LocalPort));

INT4 
VlanHandleDeletePort PROTO ((UINT2 u2Port));

INT4 
VlanHandleDelPortAndCopyProperties PROTO ((UINT2 u2Port, UINT2 u2PoIndex));

INT4 
VlanDeletePortConfigurations PROTO ((UINT2 u2Port, UINT1 u1LldpFlg));

INT4 VlanDeletePortTables  PROTO (( UINT2 u2Port));

VOID VlanResetPortTnlStatusAndRelPbPortMem (UINT2 u2Port);

INT4 
VlanHandlePortOperInd PROTO ((UINT2 u2Port,
                              UINT1 u1OperStatus));

VOID
VlanHandleOutgoingPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                              tVlanOutIfMsg *pVlanOutIf));
INT4
VlanDisableVlan PROTO ((VOID));

VOID
VlanNotifyMembersChangeToL2Iwf PROTO ((tVlanId VlanId, 
                                       tLocalPortList AddedPorts,
                                       tLocalPortList DeletedPorts));
VOID
VlanNotifyUntagMembersChangeToL2Iwf (tVlanId VlanId, 
                                     tLocalPortList AddedPorts);

INT4 
VlanSispCopySispPortPropertiesToHw PROTO ((UINT4 u4DstPort, 
                                                      UINT2 u2SrcLocalPort));
INT4
VlanSispRemovePortPropertiesFromHw PROTO ((UINT4 u4DstPort, 
                                                      UINT2 u4SrcLocalPort));


INT4 
VlanSispCopyLogicalIntfPropToHw PROTO ((UINT4 u4DstPort, 
                                               UINT2 u4SrcLocalPort));


INT4
VlanSispHandleDelAndCopySispPortProp PROTO ((UINT4 u4DstPort,
                                                UINT4 u4SrcPort,
                                                UINT2 u2MsgType));
INT4
VlanSispUpdatePortVlanTable PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                                    tLocalPortList AddedPorts,
                                    tLocalPortList DeletedPorts));
INT4
VlanSispUpdatePortVlanTableOnPort PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                                          UINT2 u2Port, UINT1 u1Status));

VOID
VlanHandleDeleteFdbEntries PROTO ((UINT2 u2Port));

#ifndef NPAPI_WANTED

VOID
VlanHandleFlushFdbEntries PROTO ((UINT2 u2Port, UINT4 u4FdbId, UINT1 u1ModId));
VOID
VlanHandleFlushFdbId PROTO ((UINT4 u4FdbId));
#endif

INT4 VlanCounterReset (tVlanId VlanId);

VOID 
VlanPropagateStaticMacInfo PROTO ((VOID));

VOID 
VlanPropagateStaticVlanInfo PROTO ((VOID));

VOID 
VlanPropStaticMacInfoForPort PROTO ((UINT2 u2Port));

VOID
VlanPropStaticVlanInfoForPort PROTO ((UINT2 u2Port));

VOID 
VlanPropagateNewPortInfo PROTO ((UINT2 u2Port));

VOID 
VlanPropagateVlanInfoOnPort PROTO((UINT2 u2Port));

VOID 
VlanPropagateMACInfoOnPort PROTO((UINT2 u2Port));


INT4 
VlanHandleCopyPortUcastPropertiesToHw  PROTO ((UINT2 u2Port));

INT4 
VlanHandleRemovePortUcastPropertiesFromHw  PROTO ((UINT2 u2Port));

INT4
VlanHandleCopyPortPropertiesToHw PROTO ((UINT2 u2DstPort, UINT2 u2SrcPort,
                                         UINT1 u1InterfaceType));

INT4
VlanHandleRemovePortPropertiesFromHw (UINT4 u4DstIfIndex, UINT2 u2SrcPort,
          UINT1 u1HwDelDefVlan);

INT4
VlanHandleCopyPortMcastPropertiesToHw PROTO ((UINT2 u2Port));

INT4 
VlanHandleRemovePortMcastPropertiesFromHw PROTO ((UINT2 u2Port));
VOID VlanDeleteFdbEntriesForPort (UINT2 u2Port);
#ifdef NPAPI_WANTED
#ifdef SW_LEARNING
VOID
VlanFdbTableAddInCtxt (UINT2 u2Port, tMacAddr MacAddr, tVlanId VlanId,
                      UINT1 u1EntryType, tMacAddr ConnectionId,
                      UINT2 u2RemoteId, UINT2 u2PortChannel, UINT4 u4PwIndex);
VOID
VlanUtilPvlanFdbTableAdd (UINT2 u2Port, tMacAddr MacAddr, tVlanId VlanId,
                          UINT1 u1EntryType, tMacAddr ConnectionId);
VOID VlanFdbTableRemoveInCtxt (tVlanId VlanId, tMacAddr MacAddr, UINT2 u2RemoteId);
VOID VlanFdbRemoveLocalOrRemote (tVlanId VlanId, tMacAddr MacAddr);
VOID VlanFdbTableRemoveAllInCtxt (VOID);
INT4 VlanFdbEntryFreeFunc (tRBElem * pElem, UINT4 u4Arg);
VOID VlanDeleteAllFdbEntries PROTO ((UINT4 u4FidIndex));
VOID VlanDeleteFdbEntriesForPort (UINT2 u2Port);
#ifdef MBSM_WANTED
VOID VlanUpdateVlanMacTable (VOID);
#endif /* MBSM_WANTED */
#endif /* SW_LEARNING */
#endif /* NPAPI_WANTED */

VOID
VlanFwdUnicastDataPacket (tCRU_BUF_CHAIN_HEADER *pFrame,
                          tVlanIfMsg *pVlanIf, tVlanCurrEntry *pVlanRec, 
                          tVlanTag *pVlanTag);

VOID
VlanTunnelPnacControlPkt (tCRU_BUF_CHAIN_DESC *pFrame, UINT1 *pu1Data, 
                          tVlanPortEntry *pVlanOutPortEntry, UINT1 *pu1RetVal);

VOID
VlanTunnelLacpControlPkt (tCRU_BUF_CHAIN_DESC *pFrame, UINT1 *pu1Data, 
                          tVlanPortEntry *pVlanOutPortEntry, UINT1 *pu1RetVal);

VOID
VlanTunnelEoamControlPkt (tCRU_BUF_CHAIN_DESC *pFrame, UINT1 *pu1Data, 
                          tVlanPortEntry *pVlanOutPortEntry, UINT1 *pu1RetVal);
INT4
VlanIsLldpControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port, 
                         UINT1 *pu1Result);

VOID
VlanTunnelStpControlPkt  (tCRU_BUF_CHAIN_DESC *pFrame, UINT1 *pu1Data, 
                          tVlanPortEntry *pVlanOutPortEntry, UINT1 *pu1RetVal);

VOID
VlanTunnelGvrpControlPkt (tCRU_BUF_CHAIN_DESC *pFrame, UINT1 *pu1Data, 
                          tVlanPortEntry *pVlanOutPortEntry, UINT1 *pu1RetVal);

VOID
VlanTunnelGvrpControlPktPerVlan (tCRU_BUF_CHAIN_DESC * pFrame, UINT1 *pu1Data,
                                tVlanCurrEntry * pVlanOutCurrEntry, UINT4 u4Port,
                                UINT1 *pu1RetVal);

VOID
VlanTunnelMvrpControlPkt (tCRU_BUF_CHAIN_DESC *pFrame, UINT1 *pu1Data,
                          tVlanPortEntry *pVlanOutPortEntry, UINT1 *pu1RetVal);

VOID
VlanTunnelMvrpControlPktPerVlan (tCRU_BUF_CHAIN_DESC * pFrame, UINT1 *pu1Data,
                                tVlanCurrEntry * pVlanOutCurrEntry, UINT4 u4Port, UINT1 *pu1RetVal);

VOID
VlanTunnelGmrpControlPkt (tCRU_BUF_CHAIN_DESC *pFrame, UINT1 *pu1Data, 
                          tVlanPortEntry *pVlanOutPortEntry, UINT1 *pu1RetVal);

VOID
VlanTunnelGmrpControlPktPerVlan (tCRU_BUF_CHAIN_DESC * pFrame, UINT1 *pu1Data,
                                tVlanCurrEntry * pVlanOutCurrEntry, UINT4 u4Port,
                                UINT1 *pu1RetVal);

VOID
VlanTunnelMmrpControlPkt (tCRU_BUF_CHAIN_DESC *pFrame, UINT1 *pu1Data,
                          tVlanPortEntry *pVlanOutPortEntry, UINT1 *pu1RetVal);

VOID
VlanTunnelMmrpControlPktPerVlan (tCRU_BUF_CHAIN_DESC * pFrame, UINT1 *pu1Data,
                                tVlanCurrEntry * pVlanOutCurrEntry, UINT4 u4Port,
                                UINT1 *pu1RetVal);

VOID
VlanTunnelIgmpControlPkt (tCRU_BUF_CHAIN_DESC *pFrame, UINT1 *pu1Data, 
                          tVlanPortEntry *pVlanOutPortEntry, UINT1 *pu1RetVal);

VOID
VlanTunnelIgmpControlPktPerVlan (tCRU_BUF_CHAIN_DESC * pFrame, UINT1 *pu1Data,
                                tVlanPortEntry * pVlanOutPortEntry,
                                tVlanCurrEntry * pVlanOutCurrEntry,
                                UINT1 *pu1RetVal);

VOID
VlanTunnelEcfmControlPkt (tCRU_BUF_CHAIN_DESC *pFrame, UINT1 *pu1Data, 
                          tVlanPortEntry *pVlanOutPortEntry, UINT1 *pu1RetVal);

VOID
VlanTunnelEcfmControlPktPerVlan (tCRU_BUF_CHAIN_DESC * pFrame, UINT1 *pu1Data,
                          tVlanCurrEntry * pVlanOutCurrEntry, UINT4 u4Port, UINT1 *pu1RetVal);

VOID
VlanTunnelOtherControlPktPerVlan (tCRU_BUF_CHAIN_DESC * pFrame, UINT1 *pu1Data,
                          tVlanCurrEntry * pVlanOutCurrEntry, UINT4 u4Port, UINT1 *pu1RetVal);

INT4
VlanGlobalPortFreeFunc (tRBElem * pElem, UINT4 u4Arg);

INT4
VlanSendMacThresholdTrap PROTO ((UINT4 u4ContextId, UINT4 u4VlanId));

INT4
VlanSendSwitchMacThresholdTrap PROTO ((VOID));

tSNMP_OID_TYPE     *
VlanMakeObjIdFromDotNew (INT1 *pi1TextStr);

#ifdef SNMP_2_WANTED
VOID 
VlanRegisterMIB (VOID);

INT4
VlanSISendMacThresholdTrap (UINT4 u4VlanId);

INT4
VlanMISendMacThresholdTrap (UINT4 u4VlanId);

INT4
VlanSISendSrcRelearnTrap (tVlanSrcRelearnTrap * pVlanSrcRelearnTrap);

INT4
VlanMISendSrcRelearnTrap (tVlanSrcRelearnTrap * pVlanSrcRelearnTrap);

INT4
VlanSISendSwitchMacThresholdTrap (VOID);

INT4
VlanMISendSwitchMacThresholdTrap (VOID);

#endif

/************************* VLANUTIL.C *************************************/
INT4
VlanGetPortIsoFwdStatus (UINT4 u4IngressPort,UINT4 u4EgressPort,UINT2 tVlanId);

UINT1
VlanGetVlanLearningMode         PROTO ((VOID));

VOID 
VlanNotifyMemberAddToAttrRP PROTO ((tVlanCurrEntry *pVlanEntry, 
                                  UINT2 u2Port));

UINT1 
VlanGetPktType PROTO ((tMacAddr DestAddr));


VOID 
VlanFreeFidIndex PROTO ((UINT4 u4FidIndex));

INT4 
VlanIsValidMcastAddr PROTO ((tMacAddr MacAddr)); 

INT4 
VlanIsValidUcastAddr PROTO ((tMacAddr MacAddr)); 

tVlanCurrEntry *
VlanGetVlanEntry PROTO ((tVlanId VlanId));
VOID
VlanUpdateVlanTag (tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId, UINT1 u1Pcp,
                   UINT1 u1DropEligible,tVlanPortEntry *pVlanPortEntry);
VOID
VlanUpdateTagEtherType PROTO((tCRU_BUF_CHAIN_DESC * pFrame, 
                              UINT2 u2EtherType));

tVlanStUcastEntry *
VlanGetStaticUcastEntry PROTO ((UINT4 u4FidIndex, 
                                tMacAddr MacAddr, 
                                UINT2 u2RcvPort));

tVlanStMcastEntry *
VlanGetStMcastEntry PROTO ((tMacAddr MacAddr,
                            UINT2 u2InPort,
                            tVlanCurrEntry *pVlanRec));

tVlanFdbEntry *
VlanGetUcastEntry PROTO ((UINT4 u4FidIndex, 
                          tMacAddr MacAddr));

VOID 
VlanAddFdbEntry PROTO ((UINT4 u4FidIndex, 
                        tVlanFdbEntry *pFdbEntry));

tVlanMacMapEntry *
VlanGetMacMapEntry PROTO ((tMacAddr MacAddr, UINT4 u4Port));

INT4
VlanSubnetMapTableCmp PROTO ((tRBElem *pRBElem1, 
                              tRBElem *pRBElem2));

tVlanSubnetMapEntry   *
VlanGetSubnetMapEntry PROTO ((UINT4 SubnetAddr, UINT4 u4Port,
                              UINT4 u4SubnetMask));

INT4 VlanGetMacMapMcastBcastFilterOption(UINT2 u2InPort, tMacAddr SrcAddr);

INT4 VlanGetSubnetMapArpFilterOption (UINT2 u2InPort, UINT4 SubnetAddr);

VOID 
VlanDeleteMacMapEntry PROTO ((tVlanMacMapEntry *pMacMapEntry));

VOID 
VlanAddMacMapEntry PROTO ((tVlanMacMapEntry *pMacMapEntry));

VOID 
VlanDeleteSubnetMapEntry PROTO ((tVlanSubnetMapEntry *pSubnetMapEntry));

VOID 
VlanAddSubnetMapEntry PROTO ((tVlanSubnetMapEntry *pSubnetMapEntry));

VOID 
VlanLearn PROTO ((tMacAddr MacAddr, 
                  UINT2 u2InPort, 
                  tVlanCurrEntry *pVlanRec));

tVlanGroupEntry *
VlanGetGroupEntry PROTO ((tMacAddr MacAddr, 
                          tVlanCurrEntry *pVlanRec));

UINT1 
VlanRegenUserPriority PROTO ((UINT2 u2Port, 
                              UINT1 u1Priority));

UINT1 
VlanQoSRegenerateVlanPriority PROTO ((UINT2 u2InPort,UINT2 u2VlanId,
                                     UINT1 u1Priority));

tVlanId 
VlanGetVlanId PROTO ((tCRU_BUF_CHAIN_DESC *pFrame, 
                      tMacAddr DestAddr, 
                      UINT2 u2Port,
                      tVlanTag *pVlanTag));

VOID 
VlanDeleteGroupEntry PROTO ((tVlanCurrEntry *pVlanEntry, 
                             tVlanGroupEntry *pGroupEntry));

INT4 
VlanDeleteGroupPortInfo PROTO ((tVlanCurrEntry *pVlanEntry, 
                              tVlanGroupEntry *pGroupEntry, 
                              UINT2 u2Port));
INT4 
VlanGetNextPort (UINT2 u2CurrPort, UINT2 *pu2Port);

UINT1 
VlanCmpMacAddr PROTO ((tMacAddr Mac1, 
                       tMacAddr Mac2)); 

VOID 
VlanUpdateMulticastEgressPorts PROTO ((tStaticVlanEntry* pStVlanEntry));

INT4 
VlanCheckIfConstraintActiveForVlan PROTO ((tVlanConstraintEntry* pInConstEntry));

INT4
VlanRemapFidIndexForVlan PROTO ((tVlanCurrEntry *pVlanEntry, 
   UINT4 u4NewFidIndex));

VOID 
VlanGrpDelStMcastInfo PROTO ((tVlanCurrEntry *pVlanEntry, 
                              tVlanStMcastEntry *pStMcastEntry, 
                              UINT4 u4RemoveInHw));

INT4
VlanInitializeDefGroupInfo PROTO ((tVlanCurrEntry *pVlanEntry));
    
VOID 
VlanUpdtVlanEgressPorts PROTO ((tVlanCurrEntry *pVlanEntry));


VOID 
VlanDelVlanEntry PROTO ((tVlanCurrEntry *pVlanDelEntry));

VOID 
VlanDeleteStMcastEntry PROTO ((tVlanCurrEntry *pVlanEntry, 
                               tVlanStMcastEntry *pStDelMcastEntry, 
                               UINT4 u4RemoveInHw));

VOID 
VlanDeleteStUcastEntry PROTO ((tVlanFidEntry *pFidEntry, 
                               tVlanStUcastEntry *pStDelUcastEntry));

VOID 
VlanDeleteFdbEntry PROTO ((UINT4 u4FidIndex, 
                           tVlanFdbEntry *pFdbDelEntry));

INT4 
VlanAddCurrEntry PROTO ((tStaticVlanEntry *pStVlanEntry));

INT4 
VlanAddVlanEntry PROTO ((tVlanId VlanId, 
                         UINT2 u2Port, 
                         tStaticVlanEntry *pStVlanEntry));

VOID
VlanInitTunnelProtStatusPerVlan PROTO ((UINT4 u4VlanId));

INT4
VlanSetTunnelStatusPerVLAN PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                                UINT1 u1TunnelStatus));

INT4
VlanGetTunnelStatusPerVLAN PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                           UINT1 *pu1TunnelStatus));

VOID 
VlanDeleteStVlanEntry PROTO ((tStaticVlanEntry *pStDelEntry));

INT4 VlanSetDefaultPvidForAllPortsInHw (VOID);
INT4 
VlanDelStVlanInfo PROTO ((tStaticVlanEntry *pStVlanEntry));

tStaticVlanEntry *
VlanGetStaticVlanEntry PROTO ((tVlanId VlanId));

UINT4 
VlanGetFidEntryFromFdbId PROTO ((UINT4 u4FdbId));

tVlanConstraintEntry* 
VlanGetConstEntry PROTO ((tVlanId VlanId, 
                          UINT2   u2ConstSet));

tVlanConstraintEntry* 
VlanGetFreeConstEntry PROTO ((VOID));

INT4 
VlanCheckConstSetType PROTO ((UINT2 u2ConstSet, 
                              UINT1 u1ConstType));

INT4 
VlanCheckConstSetVlan PROTO ((tVlanConstraintEntry *pInConstEntry));
   
INT4 
VlanAreConstraintSetMembers PROTO ((UINT2 u2ConstSet, 
                                    tVlanId VlanIdA, 
                                    tVlanId VlanIdB));

INT4 
VlanDoMembersCoExist PROTO ((tVlanConstraintEntry *pConstEntryA, 
                             tVlanConstraintEntry *pConstEntryB));

INT4 
VlanCheckTypeInconsistency PROTO ((tVlanConstraintEntry *pConstEntry));
   
INT4 
VlanCheckInconsistencies PROTO ((tVlanConstraintEntry *pConstEntry));

VOID 
VlanGetFidIndexFromSet PROTO ((UINT2 u2ConstSet, UINT4 *pu4FidIndex));

tVlanStUcastEntry *
VlanGetStaticUcastEntryWithExactRcvPort PROTO ((UINT4 u4FidIndex, 
                                                tMacAddr MacAddr, 
                                                UINT2 u2RcvPort));

VOID 
VlanDelFidEntry PROTO ((UINT4 u4FidIndex, UINT1 u1DisableVlan));

VOID 
VlanUpdtConstraintEntries PROTO ((tVlanId VlanId, 
                                  UINT4 u4FidIndex));

tVlanStMcastEntry *
VlanGetStMcastEntryWithExactRcvPort PROTO ((tMacAddr MacAddr,
                                            UINT2 u2InPort,
                                            tVlanCurrEntry *pVlan));

UINT4 
VlanGetFidIndex PROTO ((tVlanId VlanId)); 

UINT4 
VlanGetFreeFidIndex PROTO ((UINT4 u4FdbId));

UINT4 
VlanGetMatchFidIndexFromSet PROTO ((UINT2 u2ConstSet));

UINT4 
VlanAllocFdbId PROTO ((VOID));

UINT4 
VlanGetAgeoutInt PROTO ((VOID));

VOID 
VlanEnablePreConfigInfo PROTO ((VOID));

VOID 
VlanRestoreConfigInfo PROTO ((VOID));

VOID 
VlanSaveConfigInfo PROTO ((VOID));

INT4 
VlanCreateDefaultVlanEntry PROTO ((VOID)); 

VOID 
VlanSetDefaultVlan PROTO ((VOID));

INT4 
VlanActivateDefaultVlanEntry PROTO ((VOID));

VOID 
VlanSetAllPorts PROTO ((tLocalPortList PortList));

VOID
VlanSetUnRegPorts PROTO ((tLocalPortList PortList));

INT4 
VlanDelMcastLearntPorts PROTO ((tVlanCurrEntry *pVlanEntry, 
                                tMacAddr MacAddr));

INT4 
VlanGetVlanIdFromFdbId PROTO ((UINT4 u4Fid, 
                               tVlanId *pVlanId));

VOID 
VlanAddTrunkPortToExistingVlans PROTO ((UINT2 u2Port));

INT4 
VlanAddTrunkPortsToNewVlan PROTO ((tVlanId VlanId));

INT4
VlanRemoveTrunkPortsForElineVlan PROTO ((tVlanId VlanId));

VOID 
VlanDeleteTrunkPortFromExistingVlan PROTO ((UINT2 u2Port));

INT4 
VlanUpdtVlanForVlanDeletion PROTO ((tVlanId VlanId));

INT4 
VlanSetMacBasedStatusOnAllPorts PROTO ((UINT1 u1Status));

INT4 
VlanSetSubnetBasedStatusOnAllPorts PROTO ((UINT1 u1Status));

INT4 
VlanSetPortProtocolBasedStatusOnAllPorts PROTO ((UINT1 u1Status));

INT4 
VlanChangeDestAddrAtIngress PROTO ((tCRU_BUF_CHAIN_DESC *pFrame, 
                                      tMacAddr DestAddr,
                                      UINT2 u2InPort, 
                                      tMacAddr OutDestAddr));

INT4
VlanChangeDestAddrAtIngressPerVlan PROTO ((tCRU_BUF_CHAIN_DESC * pFrame,
                                    UINT4 u4ContextId, tVlanId VlanId,
                                    UINT2 u2InPort, tMacAddr DestAddr,
                                    tMacAddr OutDestAddr));

INT4 
VlanChangeDestAddrAtEgress PROTO ((tCRU_BUF_CHAIN_DESC *pFrame, 
                             UINT2 u2Port));

INT4
VlanChangeDestAddrAtEgressPerVlan PROTO ((tCRU_BUF_CHAIN_DESC * pFrame,
                                   UINT4 u4ContextId, tVlanId VlanId,
                                   UINT2 u2Port));

VOID 
VlanAddVlanToCurrentTable PROTO ((tVlanId VlanId));
                                 
VOID 
VlanRemoveVlanFromCurrentTable PROTO ((tVlanId VlanId));

UINT1 
VlanCheckResAddrAndFrameType PROTO ((tCRU_BUF_CHAIN_DESC *pFrame, tMacAddr Addr, 
                                     UINT1 *pu1FrameType, UINT2 u2Port));

VOID
VlanCheckFrame PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, tMacAddr DestAddr, UINT1 *pu1FrameType));

INT4 VlanSetHwProtocolTunnelStatus (tVlanPortEntry *pPortEntry , UINT2 u2Port);


INT4 
VlanIsPortListValid PROTO ((UINT1 *pu1Ports, 
                            INT4 i4Len));
INT4
VlanIsPortListPortTypeValid PROTO ((UINT1* pu1Ports, INT4 i4Len,
                                    UINT1 u1PortType));

VOID VlanDeletePortInfo PROTO ((UINT2 u2Port)); 

INT4
VlanUtlRemPortFromStMcastTable PROTO ((tVlanCurrEntry *pCurrEntry, 
                                       UINT2 u2Port, UINT1 u1HwRemFlag));

INT4
VlanUtlRemPortFromGroupEntry PROTO ((tVlanCurrEntry *pCurrEntry, 
                                     tVlanGroupEntry *pGroupEntry, 
                                     UINT2 u2LocalPort));

VOID VlanResetAllPortList PROTO((tVlanCurrEntry *pVlanCurrEntry, 
                                 UINT2 u2Port));

INT4
VlanGetFdbIdFromVlanId PROTO ((tVlanId VlanId, 
                               UINT4 *pu4FdbId));


VOID
VlanSetVlanLearningMode PROTO ((UINT1 u1LearningMode));

INT4
VlanGetFdbInfo PROTO ((UINT4 u4FdbId, 
                       tMacAddr MacAddr, 
                       tVlanTempFdbInfo *pVlanTempFdbInfo));
INT4
VlanMacMapTableGetNextEntry(tMacMapIndex *pCurrMacMapIndex, tMacMapIndex *pNextMacMapIndex);

INT4
VlanMacMapTableIndexCompare(tMacMapIndex *MacMapIndex1,tMacMapIndex *MacMapIndex2);

INT4
VlanSubnetMapTableGetFirstEntry PROTO ((UINT4 *pu4Port, UINT4 *pu4SubnetAddr,
                                        UINT4 *pu4SubnetMask));
   
INT4
VlanSubnetMapTableGetNextEntry PROTO ((tVlanSubnetMapIndex * pCurrSubnetMapIndex,
                                       tVlanSubnetMapIndex * pNextSubnetMapIndex));

tVlanSubnetMapEntry *
VlanSubnetMapTableGetFirstEntryOnPort PROTO ((UINT4 u4Port));

tVlanSubnetMapEntry *
VlanSubnetMapTableGetNextEntryOnPort PROTO ((tVlanSubnetMapIndex CurrIndex));

UINT4
VlanGetDefMaskFromSrcIPAddr PROTO ((UINT4 u4SubnetAddr));
VOID 
VlanUpdatePriorityInFrame PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, 
                                  UINT2 u2Priority));
VOID
VlanDeleteMacMapEntryOnPort PROTO ((UINT4 u4Port));

VOID
VlanDeleteSubnetMapEntryOnPort PROTO ((UINT4 u4Port));

VOID VlanUpdateEnhanceFiltering (UINT2 u2PortId);
VOID VlanUpdateFidEnhanceFiltering PROTO ((UINT4 u4Fid, UINT2 u2EnhFltPort));
tVlanWildCardEntry * VlanGetWildCardEntry PROTO ((tMacAddr WildCardMacAddr));
INT4 VlanDeleteWildCardEntry PROTO ((tVlanWildCardEntry  *pWildCardDelEntry));
VOID VlanDeleteMacAddressInfo (tMacAddr MacAddr);
VOID VlanSetPortsLearningStatus (UINT4 u4Fid, tPortList PortList,
                                 UINT1 u1LearningStatus);
INT4 VlanUpdatePortLearningStatus (UINT2 u2PortId,
                                   INT4 i4VlanFilteringUtilityCriteria);

INT4 VlanNotifySetVlanMemberToL2IwfAndEnhFltCriteria (tVlanId VlanId,
                                                      UINT2 u2PortId,
                                                      INT4 i4PortType);

INT4 VlanNotifyResetVlanMemberToL2IwfAndEnhFltCriteria (tVlanId VlanId,
                                                       UINT2 u2PortId);

VOID VlanGetAddedAndDeletedPorts (tSNMP_OCTET_STRING_TYPE *,
                                  tSNMP_OCTET_STRING_TYPE *,
                                  tLocalPortList, tLocalPortList);
INT1 VlanIsUnRegForwardPortsConfigured (UINT4, UINT4);

INT4 VlanStaticMulticastStatus (UINT4 u4VlanIndex, tMacAddr McastAddr, 
                                INT4 i4RcvPort, INT4 i4Status);

INT4 VlanStaticMulticastStaticEgressPorts (UINT4 u4VlanIndex,
                                           tMacAddr McastAddr,
                                           INT4 i4RcvPort,
                                           tSNMP_OCTET_STRING_TYPE 
             *pStEgressPorts);

INT4 VlanStaticMulticastForbiddenEgressPorts (UINT4 u4VlanIndex,
                                              tMacAddr McastAddr,
                                              INT4 i4RcvPort,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pForbiddenPorts);

VOID
VlanGetAddedAndDeletedVlans (tSNMP_OCTET_STRING_TYPE * pOldVlanList,
                             tSNMP_OCTET_STRING_TYPE * pNewVlanList,
                             UINT1 *pAddedVlan,
                             UINT1 *pDeletedVlan);
INT4
VlanIsPbbPortTypeValid (UINT1 *pu1Ports, INT4 i4Len);

INT4
VlanIsPbbPortTypeInPortList (UINT1 *pu1FirstPorts, UINT1 *pu1SecondPorts,
        INT4 i4FirstLen, UINT1 u1PbPortType);
INT1
VlanGetPortTypePortList (UINT1 u1PbPortType, UINT1 *pu1Ports, INT4 i4Len,  
                         UINT1 *pu1RetPorts);

INT1 
VlanValidateVlanList ( UINT2 u2VipPort, tSNMP_OCTET_STRING_TYPE *pVlanIdList,
        UINT1 u1IsTagged);

INT1 
VlanGetMemberVlanList ( UINT2 u2VipPort, tSNMP_OCTET_STRING_TYPE *pVlanIdList,
      UINT1 u1IsTagged);

INT4 
VlanSetPbbPorts ( tSNMP_OCTET_STRING_TYPE *pVlanIdList, UINT2 u2VipPort,
      UINT1 u1IsTagged);

INT4 
VlanTestEgressMemberPort ( UINT1 *pAddedList, UINT1 *pDeletedList,
         UINT2 u2Port);

INT4 
VlanTestUntaggedMemberPort ( UINT1 *pAddedList, UINT1 *pDeletedList,
           UINT2 u2Port);

INT4 
VlanSetEgressMemberPort ( UINT1 *pAddedList, UINT1 *pDeletedList,
        UINT2 u2Port);

INT4 
VlanSetUntaggedMemberPort ( UINT1 *pAddedList, UINT1 *pDeletedList,
          UINT2 u2Port);

INT4
VlanPbbValidatePortType (UINT2 u2LocalPort, UINT1 u1BrgPortType);

INT4
VlanPbbUpdateInterfaceType (UINT1 u1InterfaceType);

VOID VlanUpdateCurrentMulticastDynamicMacCount (tVlanId VlanId ,UINT1 u1Flag);
VOID VlanUpdateCurrentMulticastStaticMacCount (tVlanId VlanId ,UINT1 u1Flag);

VOID VlanDeleteUCastEntry PROTO ((VOID));

INT4 VlanDeleteMcastEntry PROTO ((VOID));

INT1
VlanGetNextIndexDot1qTpFdbTable (UINT4 u4Dot1qFdbId,
                                UINT4 *pu4NextDot1qFdbId,
                                tMacAddr Dot1qTpFdbAddress,
                                tMacAddr * pNextDot1qTpFdbAddress);
INT1
VlanGetFirstIndexDot1qTpFdbTable (UINT4 *pu4Dot1qFdbId,
                                 tMacAddr * pDot1qTpFdbAddress);
INT1
VlanGetFirstIndexDot1qStaticUnicastTable (UINT4 *pu4Dot1qFdbId,
                                         tMacAddr *
                                         pDot1qStaticUnicastAddress,
                                         INT4 *pi4Dot1qStaticUnicastReceivePort);
INT1
VlanGetNextIndexDot1qStaticUnicastTable (UINT4 u4Dot1qFdbId,
                                        UINT4 *pu4NextDot1qFdbId,
                                        tMacAddr Dot1qStaticUnicastAddress,
                                        tMacAddr *
                                        pNextDot1qStaticUnicastAddress,
                                        INT4 i4Dot1qStaticUnicastReceivePort,
                                        INT4
                                        *pi4NextDot1qStaticUnicastReceivePort);
INT1
VlanGetFirstIndexDot1qStaticMulticastTable (UINT4 *pu4Dot1qVlanIndex,
                                           tMacAddr *
                                           pDot1qStaticMulticastAddress,
                                           INT4
                                           *pi4Dot1qStaticMulticastReceivePort);
INT1
VlanGetNextIndexDot1qStaticMulticastTable (UINT4 u4Dot1qVlanIndex,
                                          UINT4 *pu4NextDot1qVlanIndex,
                                          tMacAddr
                                          Dot1qStaticMulticastAddress,
                                          tMacAddr *
                                          pNextDot1qStaticMulticastAddress,
                                          INT4
                                          i4Dot1qStaticMulticastReceivePort,
                                          INT4
                                          *pi4NextDot1qStaticMulticastReceivePort);
INT1
VlanGetDot1qTpFdbPort (UINT4 u4Dot1qFdbId,
                      tMacAddr Dot1qTpFdbAddress, INT4 *pi4RetValDot1qTpFdbPort);

INT1
VlanGetDot1qTpFdbStatus (UINT4 u4Dot1qFdbId,
                        tMacAddr Dot1qTpFdbAddress,
                        INT4 *pi4RetValDot1qTpFdbStatus);

INT4
VlanGetVlanIdForSubnetVlan (UINT2 u2Port, UINT2 u2EtherType,
                            tCRU_BUF_CHAIN_DESC *pFrame, tVlanId * pVlanId);

UINT4
VlanGetSrcIpSubnet (UINT4 u4SrcIpAddress);
INT4
VlanSetDefPortTypeForPvlan (tVlanId VlanId);

INT4
VlanPbPortType (UINT2 u2Port);
#ifndef NPAPI_WANTED
INT4 
VlanAddPortStatsForAllVlans PROTO ((UINT2 u2Port));

VOID 
VlanDelPortStatsFromAllVlans PROTO ((UINT2 u2Port));

INT4 
VlanUpdtVlanPortStatsTbl PROTO ((tVlanCurrEntry *pVlanEntry));

tVlanPortInfoPerVlan* 
VlanGetPortStatsEntry PROTO ((tVlanCurrEntry *pEntry, UINT2 u2Port));

VOID 
VlanDelAllPortStatTbl PROTO ((tVlanCurrEntry *pVlanEntry));

#endif /*NPAPI_WANTED*/

#ifdef NPAPI_WANTED

#ifdef SW_LEARNING
INT4
VlanFdbTableCmp PROTO ((tRBElem *pRBElem1, 
                        tRBElem *pRBElem2));

INT4 
VlanGetFirstTpFdbEntry PROTO ((UINT4 *pu4FdbId, 
                               tMacAddr MacAddr));

INT4 
VlanGetNextTpFdbEntry PROTO ((UINT4 u4FdbId,
                              tMacAddr MacAddr,
                              UINT4 *pu4NextFdbId,
                              tMacAddr NextMacAddr));
INT4 
VlanGetFdbDynamicCount PROTO ((UINT4 u4FdbId, 
                        UINT4 *pu4FdbCount));

tVlanFdbInfo * 
VlanGetFdbEntry PROTO ((UINT4 u4FdbId, 
                        tMacAddr MacAddr));
#endif
VOID
VlanConfHwLearningProperties (tVlanId VlanId);

VOID
VlanSetOrSyncBrgModeFlag PROTO ((UINT4 u4ContextId, UINT1 u1Flag));
#endif /* ! NPAPI_WANTED */

INT4
VlanStaticUnicastTableCmp PROTO ((tRBElem *pRBElem1,
                        tRBElem *pRBElem2));


VOID
VlanHandleRemoveMacBasedOnPort PROTO ((UINT4 u4DstPort, UINT4 u4SrcPort));

INT4 VlanParseSubIdNew (UINT1 **tempPtr, UINT4 *pu4Value);

VOID
VlanUpdateVlanMacCount (tVlanCurrEntry * pVlanEntry, UINT1 u1Flag);
UINT1
VlanIsVlanMacLearningLimitExceeded (tVlanCurrEntry * pVlanEntry);
INT4
VlanUnicastMacTableSetDefault (tVlanId VlanId);

INT4
VlanAddPortToIfIndexTable (tVlanPortEntry *pPortEntry);

INT4
VlanRemovePortFromIfIndexTable (tVlanPortEntry *pPortEntry);



INT4
VlanGlobalPortTblCmp PROTO ((tRBElem *pRBElem1, 
                        tRBElem *pRBElem2));

INT4
VlanHandleCreateContext (UINT4 u4ContextId);
INT4
VlanHandleDeleteContext (UINT4 u4ContextId);
INT4
VlanHandleUpdateContextName (UINT4 u4ContextId);

INT4 VlanReleaseContext (VOID);

INT4 VlanSelectContext (UINT4 u4ContextId);
INT4 VlanGetContextInfoFromIfIndex (UINT4 u4Port, UINT4 *pu4ContextId, 
                                    UINT2 *pu2LocalPort);

INT4 VlanGetContextInfoFromLocalPort (UINT2 u2LocalPort, UINT4 *pu4ContextId, 
                                    UINT4 *pu4IfIndex);
INT4 
VlanPostPortCreateMessage (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2Port,
                           UINT2 u2MsgType);

INT4
VlanPostIntfChangeMessage (UINT4 u4ContextId, UINT1 u1IntfType,
                           UINT2 u2MsgType);

UINT1  *
VlanMiGetBuf (UINT1 u1BufType, UINT4 u4Size);

VOID
VlanMiReleaseBuf (UINT1 u1BufType, UINT1 *pu1Buf);
INT4
VlanConvertToLocalPortList (tPortList IfPortList, tLocalPortList LocalPortList);

INT4
VlanConvertIntfRangeToLocalPortList (tPortList IfPortList, tLocalPortList LocalPortList);

INT4
VlanConvertToOpenflowPortList (tPortList IfPortList, tLocalPortList LocalPortList);

VOID
VlanConvertToIfPortList (tLocalPortList LocalPortList, tPortList IfPortList);

INT4
VlanMIDelStVlanInfo (tStaticVlanEntry * pStVlanEntry);

INT4
VlanGetFirstActiveContext (UINT4 *pu4ContextId);

INT4
VlanGetNextActiveContext (UINT4 u4CurrContextId, UINT4 *pu4NextContextId);

INT4
VlanGetFirstPortInSystem (UINT4 *pu4IfIndex);

INT4
VlanGetNextPortInSystem (INT4 i4IfIndex, UINT4 *pu4NextIfIndex);

VOID
VlanConvertLocalPortListToArray (tLocalPortList LocalPortList, UINT4 *pu4IfPortArray,
                                 UINT2 *u2NumPorts);

INT4
VlanConvertPortArrayToLocalPortList (UINT4 *pu4IfPortArray, INT4 i4NumPorts,
                                     tLocalPortList LocalPortList);

INT4
VlanConfDefaultPortInfoInL2Iwf PROTO((UINT2 u2Port));

VOID
VlanGetPortListForCurrContext (tPortList IfPortList, 
                               tLocalPortList LocalPortList);


VOID
VlanGetPortChannelPortListForCurrContext (tMbsmPortInfo *pPortInfo,
                                          tLocalPortList LocalPortList);

VOID
VlanDelPortsFromPvlan (tL2PvlanMappingInfo *pL2PvlanMappingInfo,
                           UINT2 u2Port);
INT4
VlanTestConfigMembersOnPvlan (UINT4 u4Dot1qVlanIndex, UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE *pEgressPorts);
INT4 VlanChkPortsInPrimVlansAreExclusive(UINT4 ,UINT1 , tSNMP_OCTET_STRING_TYPE *);

VOID
VlanConfigPvlanPortToNormal (UINT2 u2Port);
INT4
VlanUtilMISetVlanStEgressPort (INT4 i4FsDot1qVlanContextId,
                               UINT4 u4FsDot1qVlanIndex, INT4 i4FsDot1qTpPort,
                               INT4 i4SetValFsDot1qVlanStaticPort);
INT4
VlanUtilSetPvid (INT4 i4Dot1dBasePort, UINT4 u4SetValDot1qPvid);
INT4
VlanUtilSetIngressFiltering (INT4 i4Dot1dBasePort, INT4 i4IngFiltering);
INT4
VlanUtilSetVlanPortType (INT4 i4Dot1qFutureVlanPort,
                         INT4 i4SetValDot1qFutureVlanPortType);
INT4
VlanSetVlanPvlanPortType (INT4 i4Dot1qFutureVlanPort,
                              INT4 *i4SetValDot1qFutureVlanPortType);
VOID
VlanSetPvlanMemberPorts (tVlanId VlanId,
                             tLocalPortList AddedPorts,
                             tLocalPortList DeletedPorts);
VOID
VlanSetPvlanProperties (tVlanPortEntry *pPortEntry,
                            tL2PvlanMappingInfo *pL2PvlanMappingInfo,
                            UINT2 u2PortInd, UINT1 u1Action);
VOID
VlanUtilGetAddedOrDeletedPorts (UINT4 u4VlanIndex,
                                tSNMP_OCTET_STRING_TYPE * pEgressPorts,
                                tLocalPortList *pAddedPorts,
                                tLocalPortList *pDeletedPorts);
INT4
VlanUpdatePortIsolation (tL2PvlanMappingInfo *pL2PvlanMappingInfo);
VOID
VlanPgmPortIsoTblForHostPorts (tLocalPortList HostPortList, 
                               UINT2 *pu2EgressPorts, 
                               UINT2 u2NumPorts, 
                               UINT1 u1Action);
VOID
VlanConstructPvlanPortList (tVlanCurrEntry *pVlanCurrEntry,
                                tLocalPortList HostPortList,
                                UINT4 *pu4EgressPorts, UINT2 *pu2NumPorts);
VOID
VlanUpdtIsolationTblForVlanDel (tVlanId VlanId);
INT4
VlanUtilHandlePortTypeDependency (INT4 i4Dot1qFutureVlanPort,
                                  INT4 i4SetValDot1qFutureVlanPortType);
VOID
VlanConstHostAndUpLinkPorts(tVlanCurrEntry *pVlanCurrEntry,
                            tLocalPortList HostPortList,
                            UINT2 *pu2EgressPorts, 
                            UINT2 *pu2NumPorts); 
VOID VlanRePgmPITableForTrunkPorts PROTO((UINT2));
VOID
VlanPvlanFdbTableRem (tVlanId VlanId,  tMacAddr MacAddr);
VOID
VlanConvertLPortArrayToPhyPorts (UINT2 *pu2EgressPorts, UINT4 *pu4UpLinkPorts,
                                 UINT2 u2NumPorts);

UINT1 VlanIsStaticUcastMacLimitExceeded PROTO ((VOID));

VOID VlanUpdateCurrentStaticUcastMacCount PROTO ((UINT1));


/************************* VLANTMR.C *************************************/
VOID 
VlanProcessAgeoutTimerExpiry PROTO ((VOID));

INT4 
VlanStartTimer PROTO ((UINT1 u1TimerType, 
                       UINT4 u4TimeOut));

INT4 
VlanStopTimer PROTO ((UINT1 u1TimerType));

VOID 
VlanProcessTimerEvent PROTO ((VOID));

VOID 
VlanAgeoutStMcastEntries PROTO ((tVlanCurrEntry *pVlanEntry, 
                                 UINT4 u4CurrTime, 
                                 UINT2 u2AgeoutInt));

VOID 
VlanAgeoutStUcastEntries PROTO ((UINT4 u4FidIndex, 
                                 UINT4 u4CurrTime, 
                                 UINT2 u2AgeoutInt));

VOID 
VlanAgeoutFdbEntries PROTO ((UINT4 u4CurrTime, 
                             UINT2 u2AgeoutInt));


INT4 VlanSetStaticEgressPorts PROTO((UINT4 u4VlanIndex,
        tSNMP_OCTET_STRING_TYPE * pEgressPorts));

INT4 VlanSetStaticForbiddenPorts PROTO((UINT4 u4VlanIndex,
        tSNMP_OCTET_STRING_TYPE * pForbiddenPorts));

INT4
VlanTestDot1qVlanStaticEgressPorts PROTO ((UINT4 *pu4ErrorCode, 
                                           UINT4 u4Dot1qVlanIndex,
                                           tSNMP_OCTET_STRING_TYPE *pEgressPorts));
INT4
VlanTestDot1qVlanStaticUntaggedPorts PROTO ((UINT4 *pu4ErrorCode, 
                                             UINT4 u4Dot1qVlanIndex,
                                             tSNMP_OCTET_STRING_TYPE *pUntagPorts));

INT4 VlanGetPortInfoFromIfIndexAndCtx PROTO ((UINT4 u4IfIndex, 
                                              UINT4 u4ContextId,
                                              UINT4 *pu4SispIfIndex, 
                                              UINT2 *pu2LocalPort));

INT4 VlanNotifySnoopOnUpdate PROTO ((tVlanId VlanId, tMacAddr MacAddr, 
                                     tLocalPortList EgressPorts, 
                                     tLocalPortList ForbiddenPorts, 
                                     UINT1 u1Type, UINT1 u1Action));

/************************* VLANPRI.C *************************************/

INT4 
VlanEnablePriorityModule PROTO ((VOID));

INT4 
VlanDisablePriorityModule PROTO ((VOID));

#ifndef NPAPI_WANTED
VOID 
VlanSendToPriorityQ PROTO ((tCRU_BUF_CHAIN_DESC *pBuf, 
                                 tVlanOutIfMsg *pVlanOutIf, 
                                 UINT1 u1TrfClass));

VOID 
VlanDrainAllPriQ PROTO ((VOID));

VOID 
VlanFreePriorityQ PROTO ((tVlanPortEntry *pPortEntry));
#endif
VOID 
VlanHwEnablePriorityModule PROTO ((VOID));

VOID 
VlanHwDisablePriorityModule PROTO ((VOID));

/************************* VLANGFGN.C *************************************/
tVlanFdbEntry* 
VlanGetFirstFdbEntry PROTO ((tVlanFdbHashTable FdbHashTable));

tVlanFdbEntry* 
VlanGetNextFdbEntry PROTO ((tVlanFdbHashTable  FdbHashTable, 
                            tMacAddr       MacAddr));

tVlanGroupEntry* 
VlanGetFirstGroupEntry PROTO ((tVlanGroupEntry *pGroupTable));

tVlanGroupEntry* 
VlanGetNextGroupEntry PROTO ((tVlanGroupEntry  *pGroupEntry, 
                              tMacAddr      MacAddr));

tVlanStUcastEntry* 
VlanGetFirstStUcastEntry PROTO ((tVlanStUcastEntry *pStUcastTable));

tVlanStUcastEntry* 
VlanGetNextStUcastEntry PROTO ((tVlanStUcastEntry *pStUcastTable,
                                tMacAddr       MacAddr,
                                UINT2          u2RcvPort));

tVlanStMcastEntry* 
VlanGetFirstStMcastEntry PROTO ((tVlanStMcastEntry *pStMcastTable));

tVlanStMcastEntry* 
VlanGetNextStMcastEntry PROTO ((tVlanStMcastEntry *pStMcastTable,
                                tMacAddr       MacAddr,
                                UINT2          u2RcvPort));

tVlanCurrEntry* 
VlanGetNextCurrVlanEntry PROTO ((UINT4 u4Dot1qVlanIndex, 
                                 UINT2 u2Port));

INT1 
VlanGetNextVlanId PROTO ((UINT4 u4VlanIndex, 
                          UINT4 *pu4NextVlanIndex));

INT1 
VlanGetNextLearningConstraintsTable PROTO ((UINT4 u4ConstraintVlan,
                                            UINT4* pu4NextDot1qConstraintVlan,
                                            INT4 i4Dot1qConstraintSet,
                                            INT4* pi4NextDot1qConstraintSet,
                                            UINT1 u1VlanSort));

INT4 
VlanConstraintSetCmp PROTO ((UINT4 u4Vlan1, 
                             INT4 i4Set1, 
                             UINT4 u4Vlan2, 
                             INT4 i4Set2, 
                             UINT1 u1VlanFlag));


/************************* VLANBUF.C *************************************/

UINT1 *
VlanGetBuf PROTO ((UINT1 u1BufType, 
                   UINT4 u4Size));

UINT1              *
VlanEvbGetBuf (UINT1 u1BufType, UINT4 u4Size);

VOID  
VlanReleaseBuf PROTO ((UINT1 u1BufType, 
                       UINT1 *pu1Buf));

VOID  
VlanEvbReleaseBuf PROTO ((UINT1 u1BufType, 
                       UINT1 *pu1Buf));

tCRU_BUF_CHAIN_DESC *
VlanDupBuf PROTO ((tCRU_BUF_CHAIN_DESC *pSrc));



/************************* VLANIVR.C *************************************/

VOID 
VlanIvrComputeVlanIfOperStatus (tVlanCurrEntry *pCurrEntry);

VOID 
VlanIvrUpdateVlanIfOperStatus (UINT2 u2Port, 
                               UINT1 u1OperStatus);

VOID 
VlanIvrGetOperStatus(tVlanId VlanId,
                     UINT1 *pu1OperStatus);


VOID
VlanIvrComputeVlanIfOperStatusForPort PROTO ((tVlanCurrEntry * pCurrEntry,
                                              UINT1 u1Flag));

/*MCLAG related functions */

VOID
VlanIvrUpdateMclagIfOperStatus (UINT2 u2Port,
                               UINT1 u1OperStatus);

VOID
VlanIvrComputeVlanMclagIfOperStatus (tVlanCurrEntry *pCurrEntry);

VOID
VlanProcessMclagStatus (tVlanQMsg * pVlanQMsg);

INT1
VlanCfaIvrNotifyMclagIfOperStatus (tVlanIfaceVlanId VlanId, UINT1 u1OperStatus);

/************************* VLANCFY.C *************************************/
tVlanProtGrpEntry *
VlanGetProtGrpEntry PROTO ((UINT1 u1FrameType, 
                            UINT1 u1Length, 
                            UINT1 *pu1Value));

tVlanProtGrpEntry  *
VlanGetActiveProtoGrpEntry PROTO((UINT4 u4GroupId));

INT1 
VlanCheckProtGrpEntryMappedToPort PROTO ((tVlanProtGrpEntry * pProtGrpEntry));

INT4 
VlanCheckAndDelProtGrpInfo PROTO ((tVlanProtGrpEntry * pVlanProtGrpEntry));

VOID 
VlanGetHashIndexProtocolValue PROTO ((UINT1 u1Length, 
                                      UINT1 *pu1Value, 
                                      UINT4 *pu4HashIndex));
VOID 
VlanStoreProtoValue PROTO ((UINT1 u1Length,
                            UINT1 *pu1Value,
                            tVlanProtGrpEntry *pVlanProtGrpEntry));
INT1 
VlanCompProtoValue PROTO ((UINT1 u1Length,
                           UINT1 *pu1Value,
                           tVlanProtGrpEntry *pVlanProtGrpEntry));

INT1 
VlanCompareFrameTypeAndProtoValue PROTO ((INT4 i4FrameType, 
                                          UINT1   u1ProtoLength, 
                                          UINT1 *pu1ProtoValue, 
                                          tVlanProtGrpEntry *pProtGrpEntry));

VOID 
VlanAddProtGrpEntry PROTO ((tVlanProtGrpEntry *pVlanProtGrpEntry));

VOID 
VlanDeleteProtGrpEntry PROTO ((tVlanProtGrpEntry *pVlanProtGrpEntry));


tVlanPortVidSet *
VlanGetPortProtoVidSetEntry PROTO ((UINT2 u2Port,
                                    UINT4 u4ProtoGrpId));

VOID 
VlanAddPortProtoVidSetEntry PROTO ((tVlanPortEntry     *pVlanPortEntry,
                                    tVlanPortVidSet *pVlanPortVidSet));

INT4 
VlanGetVlanIdForPortAndProtocol PROTO ((UINT2 u2Port, 
                                        UINT1 *pFrame,
                                        tVlanId *pVlanId));

INT4 
VlanGetProtocolTemplateInFrame PROTO ((UINT1  *pFrame,UINT2 u2Port, 
                                       tVlanProtoTemplate *pProtoTemplate));

INT4 
VlanGetGroupIdForProtoTemplate PROTO ((tVlanProtoTemplate ProtoTemplate,
                                       UINT4 *pu4GroupId));

INT4 
VlanGetVlanIdInPortProtoTbl PROTO ((UINT2 u2Port, 
                                    UINT4 u4GroupId,
                                    tVlanId *pVlanId));

VOID 
VlanReleaseProtocolGrpTbl PROTO ((VOID));

VOID 
VlanAddNewGroupInfoInHw  PROTO ((tVlanProtGrpEntry * pVlanProtGrpEntry));
VOID 
VlanUpdateGroupVlanMapInfoInHw PROTO ((UINT4 u4Port, UINT4 u4GroupId, 
                                       tVlanId VlanId, UINT1 u1Flag));

VOID
VlanDeleteGroupVlanMapInfoInHw PROTO ((UINT4 u4DstPort, UINT4 u4SrcPort));

/************************* VLANSET.C *************************************/
INT1
VlanSetDot1qFutureVlanTunnelStatus PROTO ((INT4 i4Port,
                                           INT4 i4TunnelStatus));
INT1
VlanSetDot1qFutureVlanTunnelStpPDUs PROTO ((INT4 i4Port,
                                            INT4 i4TunnelStpPDUs));

INT1
VlanSetDot1qFutureVlanTunnelGvrpPDUs PROTO ((INT4 i4Port,
                                            INT4 i4TunnelGvrpPDUs));

INT1
VlanSetDot1qFutureVlanTunnelIgmpPkts PROTO ((INT4 i4Port,
                                            INT4 i4TunnelIgmpPkts));

INT1
VlanSetDot1qPvid PROTO ((UINT2 u2Port, tVlanId VlanId));

/************************* VLANCLI.C *************************************/
INT4
VlanUtilGetProtocolValue (UINT4 u4Protocol, UINT1 *pu1ProtoVal, UINT1 *pu1Len);


INT4 
VlanSrcDisplayPortPvid (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4IfIndex,
                        INT4 i4LocalPort, UINT1 *pu1HeadFlag);

INT4 
VlanSrcDisplayPortType (tCliHandle CliHandle, INT4 i4LocalPort,
                        UINT1 u1PbPortType, UINT1 *pu1HeadFlag);

/************************* VLNRDSTB.c / VLANRED.C *************************/
INT4 
VlanRedRegisterWithRM (VOID);
INT4 
VlanRedDeInitGlobalInfo (VOID);
VOID 
VlanRedSendDelAllDynInfoUpdate (UINT1 u1Flag, UINT4 u4Port);
VOID 
VlanRedAddVlanDeletedEntry (tVlanId VlanId);
INT4 
VlanRedIsVlanPresentInLrntTable (tVlanId VlanId);
tVlanNodeStatus 
VlanRedGetNodeStatus (VOID);
INT4 
VlanRedInitGlobalInfo (VOID);
INT4 
VlanRedInitRedundancyInfo (VOID);
VOID 
VlanRedDeInitRedundancyInfo (VOID);
INT4 
VlanRedIsRelearningInProgress (VOID);
INT4 
VlanRedIsMcastRelrningInProgress (VOID);
VOID 
VlanRedDelGroupDeletedEntry (tVlanId VlanId, tMacAddr McastAddr);
VOID 
VlanRedSetGroupChangedFlag (tVlanCurrEntry *pVlanEntry, 
                                 tVlanGroupEntry *pGroupEntry);
VOID 
VlanRedAddGroupDeletedEntry (tVlanId VlanId, tMacAddr McastAddr);
VOID 
VlanRedDelVlanDeletedEntry (tVlanId VlanId);
VOID 
VlanRedSetVlanChangedFlag (tVlanCurrEntry *pVlanEntry);
INT4 
VlanRedSyncUcastAddressDeletion (UINT4 u4Context, UINT4 u4FidIndex,
                                 tMacAddr UcastAddr, UINT4 u4RcvPortId);
INT4
VlanRedSyncMcastAddressDeletion (UINT4 u4Context, UINT2 u2VlanId,
                                 tMacAddr MacAdd, UINT4 u4RcvPortId);
INT4 
VlanRedSyncPortOperStatus (UINT4 u4ContextId, UINT4 u4Port, UINT1 u1Status);

INT4 VlanAddVidTransEntryInHw (UINT2 u2Port, UINT2 u2DstPort, tVlanId LocalVid,
                               tVlanId RelayVid);

INT4 VlanDelVidTransEntryInHw (UINT2 u2Port, UINT2 u2DstPort, tVlanId LocalVid,
                               tVlanId RelayVid);
INT4 VlanIsThisInterfaceVlan (tVlanId VlanId);
INT4 VlanPbIsAnyCustomerPortPresent (UINT1 *pu1Ports, INT4 i4Len);
INT4 VlanPbIsPortTypePresentInPortList (UINT1 *pu1Ports, INT4 i4Len, 
                                        UINT1 u1PbPortType);
INT4
VlanEnhancedFilteringCriteria (UINT2 u2InPort, UINT4 u4InFdbId, 
                               tVlanCurrEntry *pVlanRec);
INT4 VlanCheckAccptFrameType (UINT2 u2Port, UINT1 u1TagType);

INT4 VlanClassifyFrame (tCRU_BUF_CHAIN_DESC *pFrame, UINT2 u2EtherType, 
                        UINT2 u2Port);
INT4
VlanIsFrameTagged PROTO ((tCRU_BUF_CHAIN_DESC *, UINT2, UINT2, UINT1 *));

VOID VlanDecodeVlanTag (tCRU_BUF_CHAIN_DESC *pFrame, UINT2 u2Port,
                        UINT1 u1FrameType,UINT1 *pu1TagType, tVlanTag *pVlanTag);
INT4
VlanHandleTagOutFrame (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port,
                       tVlanIfMsg *pVlanIf, UINT1 u1TagOut, 
                       tVlanTag * pInVlanTag);



VOID
VlanAddTagToFrame (tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId, UINT1 u1Priority,
              UINT1 u1DropEligible, tVlanPortEntry *pVlanPortEntry);

VOID
VlanGetTagLenInFrameForLocalPort (tCRU_BUF_CHAIN_DESC *pFrame, 
                                  UINT2 u2LocalPort, UINT4 *pu4VlanOffset);

VOID VlanFormTag (tVlanTag  VlanTag, tVlanPortEntry *pVlanPortEntry, 
                  UINT2 *pu2Tag);

INT4 VlanGetPcpAndLocalVid (tVlanTag VlanTag, tVlanPortEntry *pVlanPortEntry,
                            tVlanId *pLocalVid, UINT1 *pu1Pcp);


VOID VlanUnTagAllInFrame (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2LocalPort);

INT4
VlanIsIgmpControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port, 
                         UINT1 *pu1Result);
INT4
VlanIsPnacControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port, 
                         UINT1 *pu1Result);
INT4
VlanIsEcfmControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port, 
                         UINT1 *pu1Result);
INT4
VlanIsLaControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port, 
                         UINT1 *pu1Result);
INT4
VlanIsEoamControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port, 
                         UINT1 *pu1Result);
#ifdef MRVLLS
INT4
VlanIsDhcpControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port, 
                         UINT1 *pu1Result);
#endif


INT4 VlanCheckReservedGroup (tMacAddr Address);


/************************* VLANMICLI.C ************************************/
INT4
VlanGetNextTrafficClassTableIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex, 
                                   INT4 i4Priority, INT4 *pi4NextPriority);
INT1
VlanGetNextPortStatisticsTableIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                     UINT4 u4VlanId, UINT4 *pu4NextVlanId);
INT1
VlanGetNextProtocolPortTableIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                   INT4 i4GroupId, INT4 *pi4NextGroupId);
INT4
VlanGetFirstPortInContext (UINT4 u4ContextId, UINT4 *pu4IfIndex);

INT4
VlanGetNextPortInContext (UINT4 u4ContextId, UINT4 u4IfIndex,
                              UINT4 *pu4NextIfIndex);

INT4
VlanGetFirstPortInCurrContext (UINT4 *pu4IfIndex);

INT4
VlanGetNextPortInCurrContext (UINT4 u4IfIndex,
                              UINT4 *pu4NextIfIndex);


#ifdef MPLS_WANTED
INT4
VlanGetVplsFdbInfo ARG_LIST ((UINT4 u4FdbId, tMacAddr MacAddr,
                              UINT4 *pu4PwVcIndex, UINT1 *pu1Status));
#endif

INT4
VlanGetStartAndEndVlanIdFromRangeStr (CHR1 * c1RangeString, 
                                      tVlanId * pStartVlanId, 
                                      tVlanId * pLastVlanId);
VOID
VlanGetMcastFwdPortList (tMacAddr OutDestAddr, UINT2 u2LocalPort, 
                         tVlanCurrEntry *pVlanRec, tLocalPortList FwdPortList);
VOID
VlanDeleteWildCardTable (VOID);
INT4
VlanSendSrcRelearnTrap(tVlanSrcRelearnTrap *pVlanSrcRelearnTrap);

#ifdef  SYSLOG_WANTED
VOID
VlanSendSrcRelearnSyslog(tVlanSrcRelearnTrap *pVlanSrcRelearnTrap);
#endif


/************************* VLANPORT.C *************************************/

INT4
VlanVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias);

INT4
VlanVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId, 
                                  UINT2 *pu2LocalPortId);

INT4
VlanVcmGetSystemMode (UINT2 u2ProtocolId);

INT4
VlanVcmGetSystemModeExt (UINT2 u2ProtocolId);

INT4
VlanVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum);

INT4
VlanSnoopIsIgmpSnoopingEnabled (UINT4 u4ContextId);

INT4
VlanSnoopIsMldSnoopingEnabled (UINT4 u4ContextId);

INT4
VlanSnoopMiDelMcastFwdEntryForVlan (UINT4 u4ContextId, tVlanId VlanId);

INT4
VlanSnoopMiUpdatePortList (UINT4 u4ContextId, tVlanId VlanId,
                           tMacAddr McastAddr, tSnoopIfPortBmp AddPortBitmap,
                           tSnoopIfPortBmp DelPortBitmap, UINT1 u1PortType);

INT4
VlanSnoopUpdateVlanStatus (BOOL1 b1Status);

INT4
VlanPnacGetPnacEnableStatus (VOID);

INT4
VlanMstMiDeleteAllVlans (UINT4 u4ContextId);

INT4
VlanAttrRPFillBulkMessage (UINT4 u4ContextId, VOID  ** ppGarpQMsg,
      UINT1 u1MsgType, tVlanId VlanId, UINT2 u2Port,
      tLocalPortList Ports, UINT1 *pu1MacAddr);

VOID
VlanUpdatePortsToAttrRP (UINT4 u4ContextId, tLocalPortList AddPortList,
                         tLocalPortList DelPortList, UINT2 u2InfoPropId);

INT4
VlanGarpIsGarpEnabledInContext (UINT4 u4ContextId);

INT4
VlanMrpIsMrpStarted (UINT4 u4ContextId);

INT4
VlanMrpIsMvrpEnabled (UINT4 u4ContextId);

INT4
VlanMrpIsMmrpEnabled (UINT4 u4ContextId);

INT4
VlanGarpPortOperInd (UINT2 u2IfIndex, UINT1 u1OperStatus);

INT4
VlanAttrRPPostBulkCfgMessage (UINT4 u4ContextId, VOID * pAttrRpQMsg);

INT4
VlanGmrpIsGmrpEnabledInContext (UINT4 u4ContextId);

void
VlanPropagateDefGrpInfoToAttrRP (UINT4 u4ContextId, UINT1 u1Type,
                               tVlanId VlanId,
                               tLocalPortList AddPortList,
                               tLocalPortList DelPortList);

void
VlanPropagateMacInfoToAttrRP (UINT4 u4ContextId, tMacAddr MacAddr,
                            tVlanId VlanId, tLocalPortList AddPortList, 
                            tLocalPortList DelPortList);

void
VlanAttrRPSetDefGrpForbiddPorts (UINT4 u4ContextId, UINT1 u1Type,
                                   tVlanId VlanId,
                                   tLocalPortList AddPortList,
                                   tLocalPortList DelPortList);

void
VlanAttrRPSetMcastForbiddPorts (UINT4 u4ContextId, tMacAddr MacAddr,
                                tVlanId VlanId,
                                tLocalPortList AddPortList,
                                tLocalPortList DelPortList);

INT4
VlanGvrpIsGvrpEnabledInContext (UINT4 u4ContextId);

void
VlanPropagateVlanInfoToAttrRP (UINT4 u4ContextId, tVlanId VlanId,
                           tLocalPortList AddPortList, 
                           tLocalPortList DelPortList);

void
VlanAttrRPSetVlanForbiddenPorts (UINT4 u4ContextId, tVlanId VlanId,
                               tLocalPortList AddPortList,
                               tLocalPortList DelPortList);

INT4
VlanIssGetAggregatorMac (UINT2 u2AggIndex, tMacAddr AggrMac);

VOID
VlanIssGetContextMacAddress (UINT4 u4ContextId, tMacAddr pSwitchMac);

INT1
VlanIssGetRestoreFlagFromNvRam (VOID);

INT4
VlanAstIsMstStartedInContext (UINT4 u4ContextId);

INT4
VlanAstIsPvrstEnabledInContext (UINT4 u4ContextId);

INT4
VlanAstIsPvrstStartedInContext (UINT4 u4ContextId);

UINT4
VlanBrgGetAgeoutTime (VOID);

VOID
VlanBrgIncrFilterInDiscards (UINT4 u4Port);

INT4
VlanCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName);

INT4
VlanCfaCliGetIfList (INT1 *pi1IfName, INT1 *pi1IfListStr, UINT1 *pu1IfListArray,
                     UINT4 u4IfListArrayLen);

INT4
VlanCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName);

INT4
VlanCfaGetInterfaceBrgPortTypeString(UINT4 u4IfIndex, UINT1 *pu1IfBrgPortType);

INT4
VlanCfaGetInterfaceBrgPortType (UINT4 u4IfIndex, INT4 *pi4IfBrgPortType);


INT4
VlanCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo);

INT4
VlanOfcCreateOpenflowVlan (UINT4 u4VlanId);

INT4
VlanOfcDeleteOpenflowVlan (UINT4 u4VlanId);

BOOL1
VlanCfaIsThisOpenflowVlan (UINT4 u4VlanId);

INT4 VlanOfcVlanSetPorts (UINT4 u4VlanId , UINT1 *pu1MemberPorts ,
                  UINT1 *pu1UntaggedPorts , UINT4 u4Flag);

UINT1
VlanCfaGetIvrStatus (VOID);

VOID
VlanCfaGetSysMacAddress (tMacAddr SwitchMac);

INT1
VlanCfaGetVlanId (UINT4 u4IfIndex, tVlanIfaceVlanId * pVlanId);

UINT4
VlanCfaGetVlanInterfaceIndexInCxt (UINT4 u4ContextId, tVlanIfaceVlanId VlanId);

BOOL1
VlanCfaIsThisInterfaceVlan (UINT2 VlanId);

INT1
VlanCfaIvrNotifyVlanIfOperStatus (tVlanIfaceVlanId VlanId, UINT1 u1OperStatus);

INT4
VlanCfaSetIfInfo (INT4 i4CfaIfParam, UINT4 u4IfIndex, tCfaIfInfo * pIfInfo);

INT4
VlanL2IwfCreateVlan (UINT4 u4ContextId, tVlanId VlanId);

INT4
VlanL2IwfDeleteAllVlans (UINT4 u4ContextId);

INT4
VlanL2IwfDeleteVlan (UINT4 u4ContextId, tVlanId VlanId);

UINT4
VlanL2IwfGetAgeoutInt (VOID);

INT4
VlanL2IwfGetNextValidPortForContext (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                     UINT2 *pu2NextLocalPort, 
                                     UINT4 *pu4NextIfIndex);

INT4
VlanL2IwfGetPortChannelForPort (UINT4 u4IfIndex, UINT2 *pu2AggId);

INT4
VlanL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT4 u4IfIndex, 
                            UINT1 *pu1OperStatus);

UINT1
VlanL2IwfGetVlanPortState (tVlanId VlanId, UINT4 u4IfIndex);

INT4
VlanL2IwfGetVlanIdFromFdbId (UINT4 u4ContextId, UINT4 u4FdbId,
                             tVlanId *pVlanId);
INT4
VlanL2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT2 u2IfIndex, UINT4 u4PktSize, 
                                  UINT2 u2Protocol, UINT1 u1Encap);

INT4
VlanL2IwfHwPbPepCreate (UINT4 u4IfIndex, tVlanId SVlanId);

VOID
VlanL2IwfIncrFilterInDiscards (UINT2 u2Port);

INT4
VlanL2IwfIsPortInPortChannel (UINT4 u4IfIndex);

UINT2
VlanL2IwfMiGetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId);

INT4
VlanL2IwfPbCreateProviderEdgePort (UINT4 u4IfIndex, tVlanId SVlanId);

INT4
VlanL2IwfPbPepDeleteIndication (UINT4 u4IfIndex, tVlanId SVlanId);

VOID
VlanL2IwfPbPepHandleInCustBpdu (tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4IfIndex,
                                tVlanId SVlanId);

INT4
VlanL2IwfPbSetDefBrgPortTypeInCfa (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   UINT1 u1BrgModePortType,
                                   UINT1 u1IsCnpCheck);

VOID
VlanL2IwfPbUpdatePepOperStatus (UINT4 u4IfIndex, tVlanId SVlanId,
                                UINT1 u1OperStatus);

INT4
VlanL2IwfSetBridgeMode (UINT4 u4ContextId, UINT4 u4BridgeMode);

INT4
VlanL2IwfSetVlanPortPvid (UINT4 u4ContextId, UINT2 u2LocalPortId, 
                          tVlanId VlanId);

INT4
VlanL2IwfGetNextActiveVlan PROTO ((UINT2 u2VlanId, UINT2 *pu2NextVlanId));

INT4
VlanL2IwfSetVlanPortType (UINT4 u4ContextId, UINT2 u2LocalPortId, 
                          UINT1 u1PortType);

VOID
VlanL2IwfGetPortOperPointToPointStatus (UINT4 u4IfIndex,
                                        BOOL1 * pbOperPointToPoint);
VOID
VlanIssApiUpdtPortIsolationEntry (tIssUpdtPortIsolation *pPortIsolationEntry);

#ifdef MPLS_WANTED
INT4
VlanMplsProcessL2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                      UINT4 u4VfId, UINT1 u1PktType);
#endif

#ifdef L2RED_WANTED
#if defined (IGS_WANTED) || defined (MLDS_WANTED)    /* SNOOP_APPROACH */
INT4
VlanSnoopRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
#endif
#if defined (ECFM_WANTED)
INT4
VlanEcfmRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
#endif
#if defined (ELMI_WANTED)
INT4
VlanElmRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
#endif


#ifdef LLDP_WANTED
INT4
VlanLldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
#endif

VOID
VlanGarpRedSendMsg (UINT4 u4MessageType);

UINT4
VlanRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId, 
                          UINT4 u4DestEntId);

UINT4
VlanRmGetNodeState (VOID);

UINT1
VlanRmGetStandbyNodeCount (VOID);

UINT1
VlanRmGetStaticConfigStatus (VOID);

UINT1
VlanRmHandleProtocolEvent (tRmProtoEvt *pEvt);

UINT4
VlanRmRegisterProtocols (tRmRegParams * pRmReg);

UINT4
VlanRmDeRegisterProtocols (VOID);

UINT4
VlanRmReleaseMemoryForMsg (UINT1 *pu1Block);

INT4
VlanRmSetBulkUpdatesStatus (UINT4 u4AppId);

INT4
VlanPbbVlanAuditStatusInd (tPbbVlanAuditStatus *pPbbVlanStatus);
#endif
INT4
VlanRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));

INT4
VlanGetShowCmdOutputAndCalcChkSum (UINT2 *);
INT4
VlanCliGetShowCmdOutputToFile (UINT1 *);

INT4
VlanCliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
VlanCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);

INT4
VlanVcmSispIsPortVlanMappingValid (UINT4 u4ContextId, tVlanId  VlanId,
                                   tLocalPortList AddedPorts);

INT4
VlanVcmSispUpdatePortVlanMapping (UINT4 u4ContextId, tVlanId  VlanId,
                                  tLocalPortList AddedPorts,
                                  tLocalPortList DeletedPorts);
INT4
VlanVcmSispUpdatePortVlanMappingOnPort (UINT4 u4ContextId, tVlanId  VlanId,
                                        UINT4 u4IfIndex, UINT1 u1Status);

INT4
VlanVcmSispUpdatePortVlanMappingInHw (UINT4 u4ContextId, tVlanId VlanId,
                                      tLocalPortList PortList, UINT1 u1Action);

INT4 
VlanVcmSispGetPhysicalPortOfSispPort (UINT4 u4LogicalIfIndex, 
          UINT4 *pu4PhysicalIndex); 
INT4
VlanVcmSispGetSispPortOfPhysicalPortInCtx (UINT4 u4PhyIfIndex, 
                                           UINT4 u4ContextId,
                                           UINT4 *pu4SispPort, 
                                           UINT2 * pu2LocalPort);
INT4
VlanL2IwfGetSispPortCtrlStatus (UINT4 u4IfIndex, UINT1 *pu1Status);

INT4
VlanVcmSispGetSispPortsInfoOfPhysicalPort (UINT4 u4PhyIfIndex, 
        UINT1 u1RetLocalPorts,
        VOID *paSispPorts, 
        UINT4 *pu4PortCount);
INT4
VlanVcmSispDeleteAllPortVlanMapEntriesForPort (UINT4 u4IfIndex);

INT4
VlanMstSispValidateInstRestriction (UINT4 u4ContextId, UINT4 u4PortNum,
        tVlanId VlanId);

/************************* VLANL2WR.C *************************************/

INT4
VlanL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode);

UINT1
VlanL2IwfGetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId);

INT4
VlanL2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType);

INT4
VlanL2IwfGetProtocolEnabledStatusOnPort (UINT2 u2IfIndex, UINT2 u2Protocol,
                                         UINT1 *pu1Status);

INT4
VlanL2IwfGetVlanPortGvrpStatus (UINT2 u2IfIndex, UINT1 *pu1GvrpStatus);

INT4
VlanL2IwfGetVlanPortMvrpStatus (UINT4 u4IfIndex, UINT1 *pu1MvrpStatus);

BOOL1
VlanL2IwfIsVlanElan (UINT4 u4ContextId, tVlanId VlanId);

UINT4
VlanL2IwfMiGetVlanFdbId (UINT4 u4ContextId, tVlanId VlanId);

INT4
VlanL2IwfPbConfigVidTransEntry (UINT4 u4ContextId,
                                tVidTransEntryInfo VidTransEntryInfo);

INT4
VlanL2IwfPbCreateVidTransTable (UINT4 u4ContextId);

INT4
VlanL2IwfPbDeleteVidTransTable (UINT4 u4ContextId);

INT4
VlanL2IwfPbGetLocalVidFromRelayVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                    tVlanId RelayVid, tVlanId * pLocalVid);

INT4
VlanL2IwfPbGetNextVidTransTblEntry (UINT4 u4ContextId, UINT2 u2Port,
                                    tVlanId LocalVid,
                                    tVidTransEntryInfo * pOutVidTransEntryInfo);

INT4
VlanL2IwfPbGetRelayVidFromLocalVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                    tVlanId LocalVid, tVlanId * pRelayVid);

INT4
VlanL2IwfPbGetVidTransEntry (UINT4 u4ContextId, UINT2 u2LocalPort,
                             tVlanId SearchVid, UINT1 u1IsLocalVid,
                             tVidTransEntryInfo * pOutVidTransInfo);

INT4
VlanL2IwfPbGetVidTransStatus (UINT4 u4ContextId, UINT2 u2LocalPort,
                              UINT1 *pu1Status);

INT4
VlanL2IwfPbGetVlanServiceType (UINT4 u4ContextId, tVlanId VlanId,
                               UINT1 *pu1ServiceType);

INT4
VlanL2IwfPbSetPortType (UINT4 u4IfIndex, INT4 i4PortType);

INT4
VlanL2IwfPbSetVidTransStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                              UINT1 u1Status);

INT4
VlanL2IwfPbSetVlanServiceType (UINT4 u4ContextId, tVlanId VlanId,
                               UINT1 u1ServiceType);

VOID
VlanL2IwfRegisterSTDBRI (VOID);

INT4
VlanL2IwfResetVlanEgressPort (UINT4 u4ContextId, tVlanId VlanId,
                              UINT2 u2LocalPortId);

INT4
VlanL2IwfSetPortVlanTunnelStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                  BOOL1 bTunnelPort);

INT4
VlanL2IwfSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                                        UINT2 u2Protocol, UINT1 u1TunnelStatus);

INT4
VlanL2IwfSetProtocolTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                                        UINT2 u2Protocol, UINT1 u1TunnelStatus);

INT4
VlanL2IwfSetTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId, UINT1 u1TunnelStatus);

VOID
VlanL2IwfGetProtocolTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                                        UINT2 u2Protocol, UINT1 *pu1TunnelStatus);
INT4
VlanL2IwfUpdtVlanEgressPortList (UINT4 u4ContextId, tVlanId VlanId, 
                                 tLocalPortList AddedPorts,
                                 tLocalPortList DeletedPorts);

INT4
VlanL2IwfUpdtVlanUntagPortList (UINT4 u4ContextId, tVlanId VlanId, 
                                 tLocalPortList AddedPorts);

INT4
VlanL2IwfSetVlanEgressPort (UINT4 u4ContextId, tVlanId VlanId, 
                            UINT2 u2LocalPortId, INT4 i4PortType);
INT4
VlanL2IwfUpdateEnhFilterStatus (UINT4 u4ContextId, BOOL1 b1EnhFilterStatus);

INT4
VlanL2IwfUpdateVlanName (UINT4 u4ContextId, tVlanId VlanId, UINT1 *pVlanName);

INT4
VlanL2IwfSetVlanFdbId (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4FdbId);

VOID
VlanL2IwfStartHwAgeTimer (UINT4 u4ContextId, UINT4 u4AgeOutInt);

INT4 
VlanL2IwfUpdatePortDeiBit (UINT4 u4IfIndex,UINT1 u1DeiBitValue);
VOID
VlanLldpApiNotifyProtoVlanStatus (UINT2 u2Port, UINT1 u1ProtVlanStatus);

VOID
VlanLldpApiNotifyProtoVlanId (UINT2 u2Port, UINT2 u2VlanId, UINT1 u1ActionFlag,
                              UINT2 u2OldVlanId);
INT4 VlanL2IwfGetPortOperEdgeStatus PROTO ((UINT2 u2IfIndex,
                                            BOOL1 * pbOperEdge));

INT4
VlanSetForwardUnregStaticPorts (UINT4 u4VlanIndex,                         
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValDot1qForwardUnregisteredStaticPorts);
INT4
VlanGetVlanForwardAllStaticPorts (UINT4 u4VlanIndex,                
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValDot1qForwardAllStaticPorts);

INT4
VlanSetForwardAllStaticPorts (UINT4 u4VlanIndex,                
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValDot1qForwardAllStaticPorts);

INT4
VlanSetGlobalMacLearnLearningStatus (INT4
                                     i4SetValDot1qFutureVlanGlobalMacLearningStatus);

INT4
VlanSetVlanAdminMacLearnStatus (UINT4 u4Dot1qFutureVlanIndex,                  
                                  INT4
                                  i4SetValDot1qFutureVlanAdminMacLearningStatus);

INT4
VlanGetVlanAdminMacLearnStatus (UINT4 u4Dot1qFutureVlanIndex,                  
                                  INT4 *
                                  pi4SetValDot1qFutureVlanAdminMacLearningStatus);
INT4
VlanTestForwardUnregStaticPorts (UINT4 *pu4ErrorCode,     
                                       UINT4 u4Dot1qVlanIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValUnregPorts);

INT4
VlanTestForwardAllStaticPorts (UINT4 *pu4ErrorCode,               
                               UINT4 u4Dot1qVlanIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValDot1qForwardAllStaticPorts);

INT4
VlanTstVlanAdminMacLearnStatus (UINT4 *pu4ErrorCode,                           
                                   UINT4 u4Dot1qFutureVlanIndex,
                                   INT4
                                   i4TestValDot1qFutureVlanAdminMacLearningStatus);

INT4
VlanIsVlanAscToVpwsPw(UINT4 u4VlanId);


INT4
VlanTstGlobalMacLearnStatus (UINT4 *pu4ErrorCode,
                             INT4
                             i4TestValDot1qFutureVlanGlobalMacLearningStatus);

INT4
VlanTestStaticMulticastStatus (UINT4 *pu4ErrorCode,                     
                               UINT4 u4Dot1qVlanIndex,
                               tMacAddr Dot1qStaticMulticastAddress,
                               INT4 i4Dot1qStaticMulticastReceivePort,
                               INT4 i4TestValDot1qStaticMulticastStatus);

INT4
VlanTestStaticMcastEgressPorts (UINT4 *pu4ErrorCode, 
        UINT4 u4Dot1qVlanIndex,
                                    tMacAddr
                                    Dot1qStaticMulticastAddress,
                                    INT4
                                    i4Dot1qStaticMulticastReceivePort,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pEgressPorts);
INT4
VlanSetStaticMulticastStatus (UINT4 u4VlanIndex,            
                              tMacAddr McastAddr,
                              INT4 i4RcvPort, INT4 i4Status);
INT4
VlanSetStaticMcastEgressPorts (UINT4 u4VlanIndex, tMacAddr McastAddr,
       INT4 i4RcvPort,
       tSNMP_OCTET_STRING_TYPE *
                                   pStEgressPorts);
INT4
VlanGetStaticMcastEgressPorts (UINT4 u4VlanIndex, tMacAddr StMcastAddr,
                                   INT4 i4RcvPort, tSNMP_OCTET_STRING_TYPE *
                                   pEgressPorts);
INT4
VlanGetStaticMulticastStatus (UINT4 u4VlanIndex,              
                              tMacAddr StMcastAddr,
                              INT4 i4RcvPort, INT4 *pi4Status);
INT4
VlanDeleteStaticUnicastEntry (UINT4 u4FdbId, 
                              tConfigStUcastInfo *pStaticUnicastInfo,
         UINT1 *pu1ErrorCode);
INT4
VlanCreateStaticUnicastEntry (UINT4 u4FdbId, 
                              tConfigStUcastInfo *pStaticUnicastInfo,
         UINT1 *pu1ErrorCode);
INT4
VlanSetConnectionIdentifier (UINT4 u4Dot1qFdbId, tMacAddr                 
                             Dot1qStaticUnicastAddress,
                             INT4
                             i4Dot1qStaticUnicastReceivePort,
                             tMacAddr
                             SetValDot1qFutureStaticConnectionIdentifier);
INT4
VlanGetConnectionIdentifier (UINT4 u4Dot1qFdbId,
        tMacAddr Dot1qTpFdbAddress,
                             INT4 i4RecvPort,
                             tMacAddr * pRetValDot1qFutureConnectionIdentifier);
INT4
VlanTestConnectionIdentifier (UINT4 *pu4ErrorCode,                                  
                              UINT4 u4Dot1qFdbId,
                              tMacAddr Dot1qStaticUnicastAddress,
                              INT4 i4Dot1qStaticUnicastReceivePort,
                              tMacAddr 
                              TestValDot1qFutureStaticConnectionIdentifier);
INT4
VlanGetStaticUcastAllowedToGoTo (UINT4 u4Dot1qFdbId,                      
                                 tMacAddr StUcastAddr,
                                 INT4 i4RcvPort,
                                 tSNMP_OCTET_STRING_TYPE * pAllowedToGoTo);
INT4
VlanSetStaticUcastAllowedToGoTo (UINT4 u4Dot1qFdbId,                    
                                 tMacAddr Dot1qStaticUnicastAddress,
                                 INT4 i4Dot1qStaticUnicastReceivePort,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValDot1qStaticUnicastAllowedToGoTo);
INT4
VlanTestStaticUcastAllowedToGoTo (UINT4 *pu4ErrorCode, UINT4 u4Dot1qFdbId,
                                  tMacAddr Dot1qStaticUnicastAddress,
                                  INT4 i4Dot1qStaticUnicastReceivePort,
                                  tSNMP_OCTET_STRING_TYPE * pAllowedToGo);
INT4
VlanGetStaticUnicastStatus (UINT4 u4Dot1qFdbId,             
                            tMacAddr StUcastAddr,
                            INT4 i4RcvPort, INT4 *pi4Status);
INT4
VlanCreateStaticMcastEntry (tConfigStMcastInfo *pStaticMCastInfo,
       UINT1 * pu1ErrorCode);

INT4
VlanDeleteStaticMcastEntry (tConfigStMcastInfo *pStaticMCastInfo,
       UINT1 * pu1ErrorCode);

INT4
VlanSetStaticUnicastStatus (UINT4 u4Dot1qFdbId,                 
                     tMacAddr Dot1qStaticUnicastAddress,
                     INT4 i4Dot1qStaticUnicastReceivePort,
                     INT4 i4SetValDot1qStaticUnicastStatus);
INT4
VlanTestStaticUnicastStatus (UINT4 *pu4ErrorCode,
                             UINT4 u4Dot1qFdbId,
                             tMacAddr Dot1qStaticUnicastAddress,
                              INT4 i4Dot1qStaticUnicastReceivePort,
                              INT4 i4TestValDot1qStaticUnicastStatus);
INT4
VlanHandleStMcastDeletion (tVlanCurrEntry *, tVlanStMcastEntry *pStMcastEntry);
INT4
VlanHandleStUcastDeletion (tVlanCurrEntry *pVlanCurrEntry, 
                           tVlanStUcastEntry *pVlanStUcastEntry);

INT4
VlanL2IwfSetVlanFloodingStatus (UINT4 u4ContextId, tVlanId u2VlanId, 
    UINT1 u1FloodStatus);
INT4
VlanL2IwfGetVlanFloodingStatus (UINT4 u4ContextId, tVlanId u2VlanId, 
    UINT1 *pu1VlanFloodStatus);

INT4
VlanCheckPbbAccptFrameType PROTO ((UINT2 u2Port, tVlanTag * pVlanTag));

INT4
VlanL2IwfSetInterfaceType (UINT4 u4ContextId, UINT1 u1InterfaceType);

INT4
VlanL2IwfGetInterfaceType (UINT4 u4ContextId, UINT1 *pu1InterfaceType);

INT4
VlanL2IwfSetAdminIntfTypeFlag (UINT4 u4ContextId,
                               UINT1 u1AdminIntfTypeFlag);

INT4
VlanL2IwfGetAdminIntfTypeFlag (UINT4 u4ContextId,
                               UINT1 *pu1AdminIntfTypeFlag);

INT4
VlanL2IwfChangeBridgeModeChange (UINT4 u4ContextId);

INT4
VlanL2IwfGetCnpPortCount (UINT4 u4ContextId, UINT4 *pu4Count);

INT4
VlanL2IwfGetAllToOneBndlStatus (UINT4 u4ContextId, UINT1 *pu1Status);

INT1
VlanL2IwfIsBrgModeChangeAllowed (UINT4 u4ContextId, UINT4 u4BrgMode);

INT4
VlanPbbCliGetVipIndexOfIsid(UINT4 u4ContextId,UINT4 u4Isid,UINT4 *pu4IfIndex);

VOID
VlanNotifyProtocolShutdownStatus PROTO ((INT4));

INT4
VlanPbbGetTagLenInFrame (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4ContextId,
                         UINT2 u2LocalPort, UINT1 u1BrgPortType,
                         UINT4 *pu4VlanOffset);


INT4
VlanPbbTeVidIsEspVid PROTO ((UINT4 u4ContextId, tVlanId VlanId));

INT4
VlanEcfmEtherTypeChngInd(UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1EtherType, 
                         UINT2 u2EtherTypeValue);
INT4
VlanFillInfoToNotifyMrp (UINT4 u4ContextId, tVlanId VlanId,
                         tMacAddr MacAddr, tLocalPortList AddPortList,
                         tLocalPortList DelPortList, UINT1 u1MsgType);
INT4
DisplayDot1qVlanSubnetMapTableInfo (tCliHandle CliHandle,
                                    UINT4 u4NextSubnetAddr,
                                    INT4 i4LocalPort, UINT4 u4NextSubnetMask,
                                    UINT4 *pu4PagingStatus);
INT4
DisplayDot1qVlanMacMapTableInfo (tCliHandle CliHandle,
                                 tMacAddr NextMacAddr, INT4 i4LocalPort,
                                 UINT4 *pu4PagingStatus);
INT4
VlanGetSrcMacAddress (UINT4 u4IfIndex, tVlanTag *pVlanTag, tMacAddr SrcMacAddr);

INT4
VlanPbNpUpdateCvidAndPepInHw (UINT2 u2Port, tVlanId SVlanId, UINT2 u2DstPort, UINT1
                                                             u1Flag);
INT4
VlanGlobalMacLearningStatus (tCliHandle CliHandle, INT4 i4Status);
INT4
VlanL2IwfConfigPvlanMapping (UINT4 u4ContextId, tVlanId VlanId,
                             UINT1 u1PvlanType, tVlanId PrimaryVlanId,
                             UINT1 u1ConfigType);
VOID
VlanL2IwfGetPVlanMappingInfo (tL2PvlanMappingInfo *pL2PvlanMappingInfo);
INT4
VlanL2IwfDeletePvlanMapping (UINT4 u4ContextId, tVlanId VlanId);
INT4
VlanL2IwfGetVlanPortType (UINT4 u4IfIndex, UINT1 *pPortType);
INT4
VlanIssApiGetIsolationFwdStatus PROTO((UINT4,tVlanId ,UINT4 ));

VOID
VlanPgmPITableForAllVlans PROTO((VOID));

VOID
VlanPvlanFdbTableAdd PROTO ((UINT2 , tMacAddr , tVlanId ,
                             UINT1 , tMacAddr ));
UINT4
VlanL2iwfValidatePortType PROTO ((UINT4 u4BridgeMode, UINT1 u1BrgPortType));

VOID
VlanSnoopDeleteFwdAndGroupTableDynamicInfo(UINT4 u4ContextId,tMacAddr MacAddr);

INT1
VlanGetNextIndexAscendingFsDot1qVlanCurrentTable PROTO 
((INT4 i4FsDot1qVlanContextId, INT4 *pi4NextFsDot1qVlanContextId,
  UINT4 u4FsDot1qVlanTimeMark, UINT4 *pu4NextFsDot1qVlanTimeMark,
  UINT4 u4FsDot1qVlanIndex, UINT4 *pu4NextFsDot1qVlanIndex));


INT1
VlanGetFirstIndexAscendingDot1qVlanCurrentTable PROTO 
((UINT4 *pu4Dot1qVlanTimeMark, UINT4 *pu4Dot1qVlanIndex));

INT1
VlanGetNextIndexAscendingDot1qVlanCurrentTable PROTO 
((UINT4 u4Dot1qVlanTimeMark, UINT4 *pu4NextDot1qVlanTimeMark,
  UINT4 u4Dot1qVlanIndex, UINT4 *pu4NextDot1qVlanIndex));

INT1
VlanIsValidCompId PROTO ((UINT4 , UINT4 ));

INT1
VlanGetCompIdFromIfIndex PROTO ((UINT4 , UINT4 *));

INT4
VlanL2IwfSetVlanLearningType (UINT4 u4ContextId, UINT1 u1LearnMode);

INT4
VlanLaSetDefaultPropForAggregator (UINT4 u4AggId);

tStaticVlanEntry *
VlanCustGetBuf (tVlanId vlanId, UINT1 u1BufType, UINT4 u4Size);

VOID
VlanCustReleaseBuf (tVlanId vlanId, UINT1 u1BufType, UINT1 *pu1Buf);
INT4
VlanValidateComponentId(UINT4 u4ComponentId,UINT4 u4PortNum);

INT1
VlanGetNextIndexIEEE8021Dot1dBasePortTable (UINT4 u4Dot1dBasePort,
                                            UINT4 *pu4NextDot1dBasePort);

/* Pseudo wire visibility */
INT4
VlanCfaGetIfType (UINT4 u4IfIndex, UINT1 *pu1IfType);

INT4
VlanUtilIsPortListValid (UINT4 u4ContextId, UINT1 *pu1Ports, INT4 i4Len);

INT4
VlanVcmGetIfIndexFromLocalPort (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                UINT4 *pu4IfIndex);
INT4
VlanUtilCreateExtPorts (UINT4 u4ContextId, tLocalPortList AddPorts);

INT4 
  VlanUtilCreateExtPort (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                         UINT2 *pu2LocalPort); 
INT4
VlanUtilDeleteExtPorts (UINT4 u4ContextId, tLocalPortList DelPorts);

INT1
VlanValidateIndexIeee8021dBasePortTable (INT4 i4Dot1dBasePort);

VOID
VlanTunnelElmiControlPkt (tCRU_BUF_CHAIN_DESC * pFrame, UINT1 *pu1Data,
                          tVlanPortEntry * pVlanOutPortEntry, UINT1 *pu1RetVal);

VOID
VlanTunnelLldpControlPkt (tCRU_BUF_CHAIN_DESC * pFrame, UINT1 *pu1Data,
                          tVlanPortEntry * pVlanOutPortEntry, UINT1 *pu1RetVal);
INT4
VlanUpdateTunnelStatusforSTP (UINT4 u4Port, BOOL1 bSTPStatus);

INT4
VlanUtilGetPvid (UINT4 u4IfIndex , UINT4 *pu4RetValFsDot1qPvid);

INT4
PortVlanMapTblCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);

INT1
VlanCreatePortVlanMapEntry (UINT2 u2VlanId, UINT2 u2Port,
                            tPortVlanMapEntry **ppPortVlanMapEntry);

INT1
VlanDeletePortVlanMapEntry (tPortVlanMapEntry * pPortVlanMapEntry);

tPortVlanMapEntry *
VlanGetPortVlanMapEntry (UINT2 u2VlanId, UINT2 u2Port);

tPortVlanMapEntry *
VlanPortVlanMapTblGetFirstEntry (VOID);

tPortVlanMapEntry *
VlanPortVlanMapTblGetNextEntry (UINT2 u2VlanId, UINT2 u2Port);

VOID
VlanDeletePortVlanMapTable (VOID);

/* Added functions in vlanutl2.c */
INT1
VlanAddSinglePortInDB (UINT2 u2VlanId, UINT2 u2Port, UINT2 u2Flag);

INT1
VlanAddPortsInDB (UINT2 u2VlanId, UINT2 u2Flag, tLocalPortList LocalPortList);


VOID
VlanIsMemberPort (UINT2 u2VlanId, UINT2 u2Port, UINT2 u2Flag, UINT1 *pu1Result);

INT1
VlanResetSinglePortInDB (UINT2 u2VlanId, UINT2 u2Port, UINT2 u2Flag);

INT1
VlanResetPortsInDB (UINT2 u2VlanId, UINT2 u2Flag, tLocalPortList LocalPortList);

VOID
VlanGetAddedDeletedPorts(UINT2 u2VlanId, UINT2 u2Flag,
                         tLocalPortList InputPortList,
                         tLocalPortList AddedPortList,
                         tLocalPortList DeletedPortList);
VOID
VlanIsNoPortsPresent (UINT2 u2VlanId, UINT2 u2Flag, UINT1 *pu1Result);
INT1
VlanWrSetAllPorts (UINT2 u2VlanId, UINT2 u2Flag);
VOID
VlanResetPortsInLocalPortList (UINT2 u2VlanId, UINT2 u2Flag,
                               tLocalPortList LocalPortList);
VOID
VlanGetPortsFromDB (UINT2 u2VlanId, UINT2 u2Flag, tLocalPortList LocalPortList);
VOID
VlanIsPortListSubsetToDB (UINT2 u2VlanId, UINT2 u2Flag,
                          tLocalPortList LocalPortList,
                          UINT1 *pu1Result);
VOID
VlanIsDB2SubsetToDB1 (UINT2 u2VlanId1, UINT2 u2Flag1, UINT2 u2VlanId2,
                      UINT2 u2Flag2, UINT1 *pu1Result);
VOID
VlanIsDBPortsSubsetToPortList (UINT2 u2VlanId, UINT2 u2Flag,
                               tLocalPortList LocalPortList,
                               UINT1 *pu1Result);
VOID
VlanIsAllPortsMatching (UINT2 u2VlanId, UINT2 u2Flag,
                        tLocalPortList LocalPortList, UINT1 *pu1Result);
VOID
VlanArePortsExclusive (UINT2 u2VlanId, UINT2 u2Flag,
                       tLocalPortList LocalPortList, UINT1 *pu1Result);
VOID
VlanConvertDBPortsToIfPortList (UINT2 u2VlanId, UINT2 u2Flag,
                                tPortList PortList);
INT1
VlanAddDelPortsInDB (UINT2 u2VlanId, UINT2 u2Flag, tLocalPortList LocalPortList);

VOID
VlanIsPortOnlyMember (UINT2 u2VlanId, UINT2 u2Port, UINT2 u2Flag, UINT1 *pu1Result);

VOID
VlanResetAllPortInDB (UINT2 u2VlanId, UINT2 u2Flag);

VOID
VlanSetFdbFlushStatus (UINT4 u4VlanId, UINT2 u2Port);

VOID
VlanGetFdbFlushStatus (UINT4 u4VlanId, UINT2 u2Port, UINT1 *pu1Result);

VOID
VlanResetFdbFlushStatus (UINT4 u4VlanId, UINT2 u2Port);

tStaticVlanEntry *
VlanGetPortVlanFdbFlushEntry (UINT4 u4VlanId, UINT2 u2Port);

INT4
VlanUpdatePortMacVlanInfoInHw (UINT4 u4ContextId, UINT2 u2Port);

INT4
VlanGetSVlanId (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2Port, UINT4 u4ContextId, tVlanId *SVlanId);

VOID
VlanUpdtIsolationTblForIccl (tVlanId VlanId);
#ifdef VXLAN_WANTED
INT4
VlanSetVxlanNvePorts (UINT4 u4VlanId, UINT1 *pu1MemberPorts,
                      UINT1 *pu1UntaggedPorts, UINT1 *pu1ForbiddenPorts, UINT4 u4Flag);
#endif

#ifdef ICCH_WANTED
VOID VlanIcchProcessUpdateMsg (tVlanQMsg * pVlanQMsg);

INT4 McagTaskInit PROTO ((VOID));

VOID McagMainDeInit PROTO ((VOID));

VOID VlanMcagHandleAgingEvent PROTO ((tMcagQMsg *pMcagQMsg));

VOID VlanMcagHandleMacAgingTmrExpiry PROTO ((VOID));

INT4 MacAgingStartTimer PROTO ((UINT1 u1TimerType,UINT4 u4TimeOut));

INT4 MacAgingStopTimer PROTO ((UINT1 u1TimerType));

#endif /* ICCH_WANTED */
INT4 VlanLaIsMclagInterface (UINT4 u4IfIndex);

UINT1
VlanLaGetMCLAGSystemStatus (VOID);

INT4
VlanUpdateCommunityPortIsolation (tL2PvlanMappingInfo * pL2PvlanMappingInfo);

VOID
VlanFlushRemoteFdb (BOOL1 b1IsHwFlushReq);


VOID
VlanFlushLocalFdb (VOID);

VOID
VlanFlushLocalFdbId (tVlanId VlanId);

VOID
VlanFlushLocalPortFdbId (tVlanId VlanId, UINT2 u2Port);

VOID 
VlanFlushRemoteFdbforIfIndex PROTO ((UINT4 u4IfIndex));

VOID 
VlanFlushRemoteFdbforVlan PROTO ((tVlanId VlanId));

VOID
VlanFlushRemotePortFdbId PROTO ((tVlanId VlanId, UINT4 u4IfIndex));

VOID
VlanFlushLocalFdbForIfIndex PROTO ((UINT4 u4IfIndex));

INT4 VlanPrimaryPortTable(UINT4 u4Dot1qVlanIndex,INT4 i4SetValDot1qFutureStVlanPrimaryVid);

INT4
VlanCfaGetIfOperStatus PROTO ((UINT4 u4IfIndex, UINT1 *pu1OperStatus));
INT4
VlanAddIcclPortToNewVlan   (tVlanId VlanId);
VOID
VlanIcchGetIcclIfIndex (UINT4 *pu4IfIndex);

UINT4 VlanIcchApiGetIcclVlanId  (UINT2 u2Index);

UINT4 VlanIcchIsIcclVlan (UINT2 u2VlanId);

UINT4
VlanIcchGetPeerNodeState (VOID);
INT4 VlanIcchSetBulkUpdatesStatus (UINT4 u4AppId);
UINT4 VlanCfaGetInterfaceNameFromIndex (UINT4 u4Index, UINT1 *pu1Alias);
INT4
VlanLaGetLaEnableStatus (VOID);
VOID 
VlanMcagDelMacAgingEntries (VOID);
VOID
VlanFlushFdbForIfIndex PROTO ((UINT4 u4IfIndex, UINT1 u1FdbType));
INT4
VlanIcchMoveFdbtoIccl PROTO ((UINT4 u4IfIndex));
INT4
VlanGetFdbEntryWithPort PROTO ((tMacAddr MacAddr, tVlanId VlanId, UINT2 u2Port));
UINT4
VlanLaGetOperStatusonAggIndex PROTO ((UINT4 u4AggIndex, UINT1 *pu1Status));

INT4
VlanCliSetPortPacketReflectionStatus (tCliHandle CliHandle,UINT4 u4ContextId ,
                                      UINT2 u2PortId,INT4 i4ReflectionStatus);


/* EVB related porting functions - start*/
INT4
VlanPortCfaCreateSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4SChIfIndex , 
                                    UINT1 u1Status);
INT4
VlanPortCfaDeleteSChannelInterface (UINT4 u4SChIfIndex);
INT4                                         
VlanPortEvbL2IwfApplPortRequest (UINT4 u4UapIfIndex,
        tLldpAppPortMsg * pLldpAppPortMsg,
        UINT1 u1MsgType);
INT4
VlanPortCfaGetFreeSChIndexForUap (UINT4 u4UapIfIndex, UINT4 *pu4SChIfIndex);
INT4
VlanL2IwfGetBridgePortOperStatus (UINT4 u4IfIndex, UINT1 *pu1BridgeOperStatus);
VOID
VlanApiEvbCallBackFunc (tLldpAppTlv *pLldpAppTlv);
INT4
VlanPortLldpSetDestMac (UINT4 u4UapIfIndex, UINT1 *pu1MacAddr);
INT4
VlanPortLldpApiGetInstanceId (UINT1 *pu1MacAddr, UINT4 *pu4InstanceId);
INT4
VlanPortCfaSetIfMainBrgPortType (INT4 i4IfIndex, INT4 i4SetValIfMainBrgPortType);
VOID VlanUtilDumpPkt (UINT1 *pu1Pkt,  UINT2 u2PktLen);
INT4
VlanAstIsStpEnabled(INT4 i4IfIndex,UINT4 u4ContextId);
INT4
VlanUtilIsOnlyUntaggedPorts(tVlanId VlanId);
INT4
VlanUtilIsPvidVlanOfAnyPorts (tVlanId VlanId);
/* EVB related porting functions - end */

INT4
VlanCliFlushRemoteFdb (tCliHandle CliHandle);
INT4
VlanPortCfaCreateDynamicSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4SChIfIndex);
INT4
VlanPortCfaDeleteDynamicSChannelInterface (UINT4 u4SChIfIndex);
VOID VlanPortLldpApiUpdateSvidOnUap (UINT4, UINT4, UINT4);
INT4
VlanPortCfaUpdateSChannelOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus);
INT4
VlanValidateForL3Subinterface (UINT4 u4PortNum, UINT2 u2L2VlanId);

INT4
VlanCfaIsL3SubIfVlanId (UINT4 u4VlanId);

INT4
VlanCfaOperDownForSubIf (UINT4 u4VlanId);

#endif
