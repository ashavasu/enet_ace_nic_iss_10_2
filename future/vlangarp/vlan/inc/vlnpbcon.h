/* $Id: vlnpbcon.h,v 1.20 2013/12/05 12:44:53 siva Exp $ */
#ifndef _VLNPBCON_H
#define _VLNPBCON_H

/* Maximum priority value for S-VLAN classification tables */

#define VLAN_IS_DSCP_VALID(Dscp) \
            (((Dscp < 0) || (Dscp > 63)) ? VLAN_FALSE : VLAN_TRUE)

#define VLAN_CURR_MAX_STATIC_MCAST_ENTRIES  gVlanInfo.u4VlanNumStaticMulticast

#define VLAN_MAX_STATIC_MCAST_ENTRIES  gVlanInfo.u4VlanMulticastMacLimit


#define VLAN_PB_PCP_DECODE_PRIORITY(pVlanPbPortEntry,u1PcpSelRow,u1Pcp) \
   pVlanPbPortEntry->au1PcpDecoding[u1PcpSelRow-1][u1Pcp].u1Priority

#define VLAN_PB_PCP_DECODE_DROP_ELIGIBLE(pVlanPbPortEntry,u1PcpSelRow,u1Pcp) \
   pVlanPbPortEntry->au1PcpDecoding[u1PcpSelRow-1][u1Pcp].u1DropEligible

#define VLAN_PB_PCP_ENCODE_PCP(pVlanPbPortEntry,u1PcpSelRow,u1Prio,u1DropEligible) \
   pVlanPbPortEntry->au1PcpEncoding[u1PcpSelRow-1][u1Prio][u1DropEligible]


#define VLAN_PB_IS_TUNNELING_VALID(u2Port) \
   (((VLAN_PB_PORT_TYPE (u2Port) == VLAN_PROP_PROVIDER_NETWORK_PORT) || \
     (VLAN_PB_PORT_TYPE (u2Port) == VLAN_PROVIDER_NETWORK_PORT) || \
     (VLAN_PB_PORT_TYPE (u2Port) == VLAN_CNP_TAGGED_PORT)) ? \
    VLAN_FALSE : VLAN_TRUE)

#define VLAN_PB_IS_PORT_TYPE_VALID(u1PortType) \
   (((u1PortType == VLAN_PROVIDER_NETWORK_PORT) || \
     (u1PortType == VLAN_CNP_TAGGED_PORT) || \
     (u1PortType == VLAN_CNP_PORTBASED_PORT) || \
     (u1PortType == VLAN_CUSTOMER_EDGE_PORT) || \
     (u1PortType == VLAN_PROP_CUSTOMER_EDGE_PORT) || \
     (u1PortType == VLAN_PROP_CUSTOMER_NETWORK_PORT) || \
     (u1PortType == VLAN_PROP_PROVIDER_NETWORK_PORT))? VLAN_TRUE : \
     VLAN_FALSE)

#define VLAN_PB_IS_PCP_DECODING_NEEDED(u1PktType, u2port) \
   (((u1PktType == VLAN_UNTAGGED_PACKET) || \
     (VLAN_PB_CTAG_FRAME (u1PktType) && \
      ((VLAN_PB_PORT_TYPE (u2Port) != VLAN_CUSTOMER_EDGE_PORT) || \
       (VLAN_PB_PORT_TYPE (u2Port) != VLAN_PROP_PROVIDER_NETWORK_PORT)))) ? \
    VLAN_FALSE : VLAN_TRUE)

#define VLAN_PB_UNTAG_PEPSET(pPort) ((pPort)->u1UnTagPepSet)


/* Service VLAN classification - packet offset*/

#define VLAN_TYPE_LEN_OFFSET             12   /* Type/Len offset in untagged
                                                 frames.*/
    
#define VLAN_ST_TYPE_LEN_OFFSET          16  /*Type/Len offset in single tag
                                               frames.*/
#define VLAN_DT_TYPE_LEN_OFFSET          20  /* Type/Len offset in double tag 
                                                frames.*/

#define VLAN_ENETV2_IP_HEADER_OFFSET     14
#define VLAN_ENETV2_ST_IP_HEADER_OFFSET  18
#define VLAN_ENETV2_DT_IP_HEADER_OFFSET  22

#define VLAN_LLCSNAP_PROTOCOL_OFFSET         20  
#define VLAN_ST_LLCSNAP_PROTOCOL_OFFSET      24
#define VLAN_DT_LLCSNAP_PROTOCOL_OFFSET      28
#define VLAN_PROTOCOL_LEN                     2


#define VLAN_DT_LLCSNAP_IPHDR_OFFSET      30
#define VLAN_ST_LLCSNAP_IPHDR_OFFSET      26
#define VLAN_LLCSNAP_IPHDR_OFFSET         22

#define VLAN_ARP_HDR_LEN                     28
#define VLAN_IP_HDR_LEN                      20

#define   VLAN_IP_IS_ADDR_CLASS_A(u4Addr) \
    ((u4Addr &  0x80000000) == 0)
    
#define   VLAN_IP_IS_ADDR_CLASS_B(u4Addr) \
    ((u4Addr &  0xc0000000) == 0x80000000)
    
#define   VLAN_IP_IS_ADDR_CLASS_C(u4Addr) \
    ((u4Addr &  0xe0000000) == 0xc0000000)
    
#define   VLAN_IP_IS_ADDR_CLASS_D(u4Addr) \
    ((u4Addr &  0xf0000000) == 0xe0000000)
    
#define   VLAN_IP_IS_ADDR_CLASS_E(u4Addr) \
   ((u4Addr &  0xf0000000) == 0xf0000000)

#define VLAN_PB_DYNAMIC_UNICAST_SIZE \
        gpVlanContextInfo->VlanInfo.u4SwitchDynamicUnicastSize

/* Current multicast MAC Limit */
#define VLAN_PB_MULTICAST_TABLE_LIMIT \
        gpVlanContextInfo->VlanInfo.u4SwitchMulticastLimit
       
/* Current number of multicast MAC entries */
#define VLAN_PB_MULTICAST_TABLE_COUNT \
        gpVlanContextInfo->VlanInfo.u4SwitchCurrentMulticastCount

#define VLAN_IS_PCP_ROW_VALID(selectedrow) \
        (((selectedrow < VLAN_8P0D_SEL_ROW ) || (selectedrow > \
          VLAN_5P3D_SEL_ROW)) ? VLAN_FALSE : VLAN_TRUE)

#define VLAN_IS_PRIORITY_VALID(priority) \
        (((priority < VLAN_MIN_PRIORITY) || (priority > VLAN_HIGHEST_PRIORITY)) \
         ? VLAN_FALSE : VLAN_TRUE)
#define VLAN_IS_USE_DEI_VALID(useDEI) \
        (((useDEI != VLAN_SNMP_TRUE) && (useDEI != VLAN_SNMP_FALSE)) ? VLAN_FALSE : \
        VLAN_TRUE)
#define VLAN_IS_REQ_DROP_ENCODING_VALID(ReqDropEncoding) \
        (((ReqDropEncoding != VLAN_SNMP_TRUE) && (ReqDropEncoding != \
         VLAN_SNMP_FALSE)) ? VLAN_FALSE : VLAN_TRUE)

#define VLAN_PB_PORT_REQ_DROP_ENCODING(port) \
         VLAN_GET_PB_PORT_ENTRY(port)->u1ReqDropEncoding

#define VLAN_PB_PORT_ETHER_TYPE_SWAP_STATUS(port) \
         VLAN_GET_PB_PORT_ENTRY(port)->u1EtherTypeSwapStatus

#define VLAN_PB_PORT_PCP_SEL_ROW(port) VLAN_GET_PB_PORT_ENTRY(port)->u1PcpSelRow


#define VLAN_PB_PORT_MAC_LEARNING_STATUS(port)\
        VLAN_GET_PB_PORT_ENTRY(port)->u1MacLearningStatus
#define VLAN_PB_PORT_SVLAN_CLASSIFY(port) \
        VLAN_GET_PB_PORT_ENTRY(port)->u1SVlanTableType
#define VLAN_PB_PORT_DEF_CVLAN_CLASSIFY(port)\
        VLAN_GET_PB_PORT_ENTRY(port)->u1DefCVlanClassify

#define VLAN_PB_PORT_CVLAN_ID(port) \
        VLAN_GET_PB_PORT_ENTRY(port)->CVlanId
        

#define VLAN_IS_VID_TRANS_VALID_ONPORT(u2Port) \
         (\
          ((VLAN_PB_PORT_TYPE (u2Port) == VLAN_PROVIDER_NETWORK_PORT) || \
         (VLAN_PB_PORT_TYPE (u2Port) == VLAN_PROP_CUSTOMER_NETWORK_PORT) ||\
         (VLAN_PB_PORT_TYPE (u2Port) == VLAN_CNP_TAGGED_PORT)) \
          ? VLAN_TRUE : VLAN_FALSE \
          )

#define VLAN_PEP_MAKE_OPER_UP(pVlanPortEntry, pVlanPbLogicalPortEntry) \
          (((pVlanPortEntry->u1OperStatus == VLAN_OPER_UP)&& \
            (VlanPbIsSVlanActive (pVlanPbLogicalPortEntry->SVlanId) \
             == VLAN_TRUE) && \
            (pVlanPbLogicalPortEntry->u2NoOfActiveCvidEntries > 0))? \
            VLAN_TRUE: VLAN_FALSE)
#endif

          
