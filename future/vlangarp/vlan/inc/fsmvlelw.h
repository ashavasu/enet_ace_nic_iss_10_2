/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmvlelw.h,v 1.11 2015/06/05 09:40:35 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIVlanBridgeInfoTable. */
INT1
nmhValidateIndexInstanceFsMIVlanBridgeInfoTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIVlanBridgeInfoTable  */

INT1
nmhGetFirstIndexFsMIVlanBridgeInfoTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIVlanBridgeInfoTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIVlanBridgeMode ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIVlanBridgeMode ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIVlanBridgeMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIVlanBridgeInfoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIVlanTunnelContextInfoTable. */
INT1
nmhValidateIndexInstanceFsMIVlanTunnelContextInfoTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIVlanTunnelContextInfoTable  */

INT1
nmhGetFirstIndexFsMIVlanTunnelContextInfoTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIVlanTunnelContextInfoTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIVlanTunnelBpduPri ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelStpAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIVlanTunnelLacpAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIVlanTunnelDot1xAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIVlanTunnelGvrpAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIVlanTunnelGmrpAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIVlanTunnelMvrpAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIVlanTunnelMmrpAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIVlanTunnelElmiAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIVlanTunnelLldpAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIVlanTunnelEcfmAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIVlanTunnelEoamAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsMIVlanTunnelIgmpAddress ARG_LIST((INT4 ,tMacAddr * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIVlanTunnelBpduPri ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelStpAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIVlanTunnelLacpAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIVlanTunnelDot1xAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIVlanTunnelGvrpAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIVlanTunnelGmrpAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIVlanTunnelMvrpAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIVlanTunnelMmrpAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIVlanTunnelElmiAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIVlanTunnelLldpAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIVlanTunnelEcfmAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIVlanTunnelEoamAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsMIVlanTunnelIgmpAddress ARG_LIST((INT4  ,tMacAddr ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIVlanTunnelBpduPri ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelStpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIVlanTunnelLacpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIVlanTunnelDot1xAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIVlanTunnelGvrpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIVlanTunnelGmrpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIVlanTunnelMvrpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIVlanTunnelMmrpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIVlanTunnelElmiAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIVlanTunnelLldpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIVlanTunnelEcfmAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIVlanTunnelEoamAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsMIVlanTunnelIgmpAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIVlanTunnelContextInfoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIVlanTunnelTable. */
INT1
nmhValidateIndexInstanceFsMIVlanTunnelTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIVlanTunnelTable  */

INT1
nmhGetFirstIndexFsMIVlanTunnelTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIVlanTunnelTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIVlanTunnelStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIVlanTunnelStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIVlanTunnelStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIVlanTunnelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIVlanTunnelProtocolTable. */
INT1
nmhValidateIndexInstanceFsMIVlanTunnelProtocolTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIVlanTunnelProtocolTable  */

INT1
nmhGetFirstIndexFsMIVlanTunnelProtocolTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIVlanTunnelProtocolTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIVlanTunnelProtocolDot1x ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolLacp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolStp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolGvrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolGmrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolIgmp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolMvrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolMmrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolElmi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolLldp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolEcfm ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelOverrideOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolEoam ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIVlanTunnelProtocolDot1x ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelProtocolLacp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelProtocolStp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelProtocolGvrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelProtocolGmrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelProtocolIgmp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelProtocolMvrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelProtocolMmrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelProtocolElmi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelProtocolLldp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelProtocolEcfm ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelOverrideOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIVlanTunnelProtocolEoam ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIVlanTunnelProtocolDot1x ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelProtocolLacp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelProtocolStp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelProtocolGvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelProtocolGmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelProtocolIgmp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelProtocolMvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelProtocolMmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelProtocolElmi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelProtocolLldp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelProtocolEcfm ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelOverrideOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIVlanTunnelProtocolEoam ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIVlanTunnelProtocolTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIVlanTunnelProtocolStatsTable. */
INT1
nmhValidateIndexInstanceFsMIVlanTunnelProtocolStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIVlanTunnelProtocolStatsTable  */

INT1
nmhGetFirstIndexFsMIVlanTunnelProtocolStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIVlanTunnelProtocolStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIVlanTunnelProtocolDot1xPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolDot1xPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolLacpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolLacpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolStpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolStpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolGvrpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolGvrpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolGmrpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolGmrpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolIgmpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolIgmpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolMvrpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolMvrpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolMmrpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolMmrpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolElmiPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolElmiPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolLldpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolLldpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolEcfmPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolEcfmPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolEoamPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanTunnelProtocolEoamPktsSent ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIVlanDiscardStatsTable. */
INT1
nmhValidateIndexInstanceFsMIVlanDiscardStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIVlanDiscardStatsTable  */

INT1
nmhGetFirstIndexFsMIVlanDiscardStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIVlanDiscardStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIVlanDiscardDot1xPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardDot1xPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardLacpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardLacpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardStpPDUsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardStpPDUsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardGvrpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardGvrpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardGmrpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardGmrpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardIgmpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardIgmpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardMvrpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardMvrpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardMmrpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardMmrpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardElmiPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardElmiPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardLldpPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardLldpPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardEcfmPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardEcfmPktsTx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardEoamPktsRx ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIVlanDiscardEoamPktsTx ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIServiceVlanTunnelProtocolTable. */
INT1
nmhValidateIndexInstanceFsMIServiceVlanTunnelProtocolTable ARG_LIST((INT4, INT4, INT4));

/* Proto Type for Low Level GET FIRST fn for FsMIServiceVlanTunnelProtocolTable  */

INT1
nmhGetFirstIndexFsMIServiceVlanTunnelProtocolTable ARG_LIST((INT4 *, INT4 *, INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIServiceVlanTunnelProtocolTable ARG_LIST((INT4, INT4 *, INT4, INT4 *, INT4, INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIServiceVlanRsvdMacaddress ARG_LIST((INT4, INT4, INT4, tMacAddr *));

INT1
nmhGetFsMIServiceVlanTunnelMacaddress ARG_LIST((INT4, INT4, INT4, tMacAddr *));

INT1
nmhGetFsMIServiceVlanTunnelProtocolStatus ARG_LIST((INT4, INT4, INT4, INT4 *));

INT1
nmhGetFsMIServiceVlanTunnelPktsRecvd ARG_LIST((INT4, INT4, INT4, UINT4 *));

INT1
nmhGetFsMIServiceVlanTunnelPktsSent ARG_LIST((INT4, INT4, INT4, UINT4 *));

INT1
nmhGetFsMIServiceVlanDiscardPktsRx ARG_LIST((INT4, INT4, INT4, UINT4 *));

INT1
nmhGetFsMIServiceVlanDiscardPktsTx ARG_LIST((INT4, INT4, INT4, UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIServiceVlanRsvdMacaddress ARG_LIST((INT4, INT4, INT4, tMacAddr));

INT1
nmhSetFsMIServiceVlanTunnelMacaddress ARG_LIST((INT4, INT4, INT4, tMacAddr));

INT1
nmhSetFsMIServiceVlanTunnelProtocolStatus ARG_LIST((INT4, INT4, INT4, INT4));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIServiceVlanRsvdMacaddress ARG_LIST((UINT4 *, INT4, INT4, INT4, tMacAddr));

INT1
nmhTestv2FsMIServiceVlanTunnelMacaddress ARG_LIST((UINT4 *, INT4, INT4, INT4, tMacAddr));

INT1
nmhTestv2FsMIServiceVlanTunnelProtocolStatus ARG_LIST((UINT4 *, INT4, INT4, INT4, INT4));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIServiceVlanTunnelProtocolTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
