#ifndef _STDBRGWR_H
#define _STDBRGWR_H

VOID RegisterSTDBRG(VOID);

VOID UnRegisterSTDBRG(VOID);
INT4 Dot1dDeviceCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTrafficClassesEnabledGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dGmrpStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTrafficClassesEnabledSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dGmrpStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTrafficClassesEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dGmrpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dTrafficClassesEnabledDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1dGmrpStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 GetNextIndexDot1dPortCapabilitiesTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dPortCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1dPortPriorityTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dPortDefaultUserPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortNumTrafficClassesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortDefaultUserPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortNumTrafficClassesSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortDefaultUserPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dPortNumTrafficClassesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dPortPriorityTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1dUserPriorityRegenTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dRegenUserPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dRegenUserPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1dRegenUserPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dUserPriorityRegenTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexDot1dTrafficClassTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dTrafficClassGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTrafficClassSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTrafficClassTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dTrafficClassTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexDot1dPortOutboundAccessPriorityTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dPortOutboundAccessPriorityGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1dPortGarpTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dPortGarpJoinTimeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGarpLeaveTimeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGarpLeaveAllTimeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGarpJoinTimeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGarpLeaveTimeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGarpLeaveAllTimeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGarpJoinTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGarpLeaveTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGarpLeaveAllTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGarpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexDot1dPortGmrpTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dPortGmrpStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGmrpFailedRegistrationsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGmrpLastPduOriginGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortRestrictedGroupRegistrationGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGmrpStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortRestrictedGroupRegistrationSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGmrpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dPortRestrictedGroupRegistrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dPortGmrpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1dTpHCPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dTpHCPortInFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpHCPortOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpHCPortInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1dTpPortOverflowTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dTpPortInOverflowFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpPortOutOverflowFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpPortInOverflowDiscardsGet(tSnmpIndex *, tRetVal *);
#endif /* _STDBRGWR_H */
