/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvlnedb.h,v 1.11 2015/06/05 09:40:36 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSVLNEDB_H
#define _FSVLNEDB_H

UINT1 FsVlanTunnelTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsVlanTunnelProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsVlanTunnelProtocolStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsVlanDiscardStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsServiceVlanTunnelProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsvlne [] ={1,3,6,1,4,1,2076,137};
tSNMP_OID_TYPE fsvlneOID = {8, fsvlne};


UINT4 FsVlanBridgeMode [ ] ={1,3,6,1,4,1,2076,137,1,1};
UINT4 FsVlanTunnelBpduPri [ ] ={1,3,6,1,4,1,2076,137,2,1};
UINT4 FsVlanTunnelStpAddress [ ] ={1,3,6,1,4,1,2076,137,2,2};
UINT4 FsVlanTunnelLacpAddress [ ] ={1,3,6,1,4,1,2076,137,2,3};
UINT4 FsVlanTunnelDot1xAddress [ ] ={1,3,6,1,4,1,2076,137,2,4};
UINT4 FsVlanTunnelGvrpAddress [ ] ={1,3,6,1,4,1,2076,137,2,5};
UINT4 FsVlanTunnelGmrpAddress [ ] ={1,3,6,1,4,1,2076,137,2,6};
UINT4 FsVlanTunnelMvrpAddress [ ] ={1,3,6,1,4,1,2076,137,2,10};
UINT4 FsVlanTunnelMmrpAddress [ ] ={1,3,6,1,4,1,2076,137,2,11};
UINT4 FsVlanTunnelElmiAddress [ ] ={1,3,6,1,4,1,2076,137,2,12};
UINT4 FsVlanTunnelLldpAddress [ ] ={1,3,6,1,4,1,2076,137,2,13};
UINT4 FsVlanTunnelEcfmAddress [ ] ={1,3,6,1,4,1,2076,137,2,14};
UINT4 FsVlanTunnelEoamAddress [ ] ={1,3,6,1,4,1,2076,137,2,16};
UINT4 FsVlanTunnelIgmpAddress [ ] ={1,3,6,1,4,1,2076,137,2,17};
UINT4 FsVlanPort [ ] ={1,3,6,1,4,1,2076,137,2,7,1,1};
UINT4 FsVlanTunnelStatus [ ] ={1,3,6,1,4,1,2076,137,2,7,1,2};
UINT4 FsVlanTunnelProtocolDot1x [ ] ={1,3,6,1,4,1,2076,137,2,8,1,1};
UINT4 FsVlanTunnelProtocolLacp [ ] ={1,3,6,1,4,1,2076,137,2,8,1,2};
UINT4 FsVlanTunnelProtocolStp [ ] ={1,3,6,1,4,1,2076,137,2,8,1,3};
UINT4 FsVlanTunnelProtocolGvrp [ ] ={1,3,6,1,4,1,2076,137,2,8,1,4};
UINT4 FsVlanTunnelProtocolGmrp [ ] ={1,3,6,1,4,1,2076,137,2,8,1,5};
UINT4 FsVlanTunnelProtocolIgmp [ ] ={1,3,6,1,4,1,2076,137,2,8,1,6};
UINT4 FsVlanTunnelProtocolMvrp [ ] ={1,3,6,1,4,1,2076,137,2,8,1,7};
UINT4 FsVlanTunnelProtocolMmrp [ ] ={1,3,6,1,4,1,2076,137,2,8,1,8};
UINT4 FsVlanTunnelProtocolElmi [ ] ={1,3,6,1,4,1,2076,137,2,8,1,9};
UINT4 FsVlanTunnelProtocolLldp [ ] ={1,3,6,1,4,1,2076,137,2,8,1,10};
UINT4 FsVlanTunnelProtocolEcfm [ ] ={1,3,6,1,4,1,2076,137,2,8,1,11};
UINT4 FsVlanTunnelOverrideOption [ ] ={1,3,6,1,4,1,2076,137,2,8,1,12};
UINT4 FsVlanTunnelProtocolEoam [ ] ={1,3,6,1,4,1,2076,137,2,8,1,13};
UINT4 FsVlanTunnelProtocolDot1xPktsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,1};
UINT4 FsVlanTunnelProtocolDot1xPktsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,2};
UINT4 FsVlanTunnelProtocolLacpPktsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,3};
UINT4 FsVlanTunnelProtocolLacpPktsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,4};
UINT4 FsVlanTunnelProtocolStpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,5};
UINT4 FsVlanTunnelProtocolStpPDUsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,6};
UINT4 FsVlanTunnelProtocolGvrpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,7};
UINT4 FsVlanTunnelProtocolGvrpPDUsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,8};
UINT4 FsVlanTunnelProtocolGmrpPktsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,9};
UINT4 FsVlanTunnelProtocolGmrpPktsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,10};
UINT4 FsVlanTunnelProtocolIgmpPktsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,11};
UINT4 FsVlanTunnelProtocolIgmpPktsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,12};
UINT4 FsVlanTunnelProtocolMvrpPktsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,13};
UINT4 FsVlanTunnelProtocolMvrpPktsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,14};
UINT4 FsVlanTunnelProtocolMmrpPktsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,15};
UINT4 FsVlanTunnelProtocolMmrpPktsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,16};
UINT4 FsVlanTunnelProtocolElmiPktsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,17};
UINT4 FsVlanTunnelProtocolElmiPktsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,18};
UINT4 FsVlanTunnelProtocolLldpPktsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,19};
UINT4 FsVlanTunnelProtocolLldpPktsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,20};
UINT4 FsVlanTunnelProtocolEcfmPktsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,21};
UINT4 FsVlanTunnelProtocolEcfmPktsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,22};
UINT4 FsVlanTunnelProtocolEoamPktsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,9,1,23};
UINT4 FsVlanTunnelProtocolEoamPktsSent [ ] ={1,3,6,1,4,1,2076,137,2,9,1,24};
UINT4 FsVlanDiscardDot1xPktsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,1};
UINT4 FsVlanDiscardDot1xPktsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,2};
UINT4 FsVlanDiscardLacpPktsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,3};
UINT4 FsVlanDiscardLacpPktsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,4};
UINT4 FsVlanDiscardStpPDUsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,5};
UINT4 FsVlanDiscardStpPDUsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,6};
UINT4 FsVlanDiscardGvrpPktsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,7};
UINT4 FsVlanDiscardGvrpPktsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,8};
UINT4 FsVlanDiscardGmrpPktsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,9};
UINT4 FsVlanDiscardGmrpPktsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,10};
UINT4 FsVlanDiscardIgmpPktsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,11};
UINT4 FsVlanDiscardIgmpPktsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,12};
UINT4 FsVlanDiscardMvrpPktsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,13};
UINT4 FsVlanDiscardMvrpPktsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,14};
UINT4 FsVlanDiscardMmrpPktsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,15};
UINT4 FsVlanDiscardMmrpPktsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,16};
UINT4 FsVlanDiscardElmiPktsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,17};
UINT4 FsVlanDiscardElmiPktsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,18};
UINT4 FsVlanDiscardLldpPktsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,19};
UINT4 FsVlanDiscardLldpPktsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,20};
UINT4 FsVlanDiscardEcfmPktsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,21};
UINT4 FsVlanDiscardEcfmPktsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,22};
UINT4 FsVlanDiscardEoamPktsRx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,23};
UINT4 FsVlanDiscardEoamPktsTx [ ] ={1,3,6,1,4,1,2076,137,3,1,1,24};
UINT4 FsServiceVlanId [ ] ={1,3,6,1,4,1,2076,137,2,15,1,1};
UINT4 FsServiceProtocolEnum [ ] ={1,3,6,1,4,1,2076,137,2,15,1,2};
UINT4 FsServiceVlanRsvdMacaddress [ ] ={1,3,6,1,4,1,2076,137,2,15,1,3};
UINT4 FsServiceVlanTunnelMacaddress [ ] ={1,3,6,1,4,1,2076,137,2,15,1,4};
UINT4 FsServiceVlanTunnelProtocolStatus [ ] ={1,3,6,1,4,1,2076,137,2,15,1,5};
UINT4 FsServiceVlanTunnelPktsRecvd [ ] ={1,3,6,1,4,1,2076,137,2,15,1,6};
UINT4 FsServiceVlanTunnelPktsSent [ ] ={1,3,6,1,4,1,2076,137,2,15,1,7};
UINT4 FsServiceVlanDiscardPktsRx [ ] ={1,3,6,1,4,1,2076,137,2,15,1,8};
UINT4 FsServiceVlanDiscardPktsTx [ ] ={1,3,6,1,4,1,2076,137,2,15,1,9};





tMbDbEntry fsvlneMibEntry[]= {

{{10,FsVlanBridgeMode}, NULL, FsVlanBridgeModeGet, FsVlanBridgeModeSet, FsVlanBridgeModeTest, FsVlanBridgeModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsVlanTunnelBpduPri}, NULL, FsVlanTunnelBpduPriGet, FsVlanTunnelBpduPriSet, FsVlanTunnelBpduPriTest, FsVlanTunnelBpduPriDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "7"},

{{10,FsVlanTunnelStpAddress}, NULL, FsVlanTunnelStpAddressGet, FsVlanTunnelStpAddressSet, FsVlanTunnelStpAddressTest, FsVlanTunnelStpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsVlanTunnelLacpAddress}, NULL, FsVlanTunnelLacpAddressGet, FsVlanTunnelLacpAddressSet, FsVlanTunnelLacpAddressTest, FsVlanTunnelLacpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsVlanTunnelDot1xAddress}, NULL, FsVlanTunnelDot1xAddressGet, FsVlanTunnelDot1xAddressSet, FsVlanTunnelDot1xAddressTest, FsVlanTunnelDot1xAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsVlanTunnelGvrpAddress}, NULL, FsVlanTunnelGvrpAddressGet, FsVlanTunnelGvrpAddressSet, FsVlanTunnelGvrpAddressTest, FsVlanTunnelGvrpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsVlanTunnelGmrpAddress}, NULL, FsVlanTunnelGmrpAddressGet, FsVlanTunnelGmrpAddressSet, FsVlanTunnelGmrpAddressTest, FsVlanTunnelGmrpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsVlanPort}, GetNextIndexFsVlanTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsVlanTunnelTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelStatus}, GetNextIndexFsVlanTunnelTable, FsVlanTunnelStatusGet, FsVlanTunnelStatusSet, FsVlanTunnelStatusTest, FsVlanTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolDot1x}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolDot1xGet, FsVlanTunnelProtocolDot1xSet, FsVlanTunnelProtocolDot1xTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolLacp}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolLacpGet, FsVlanTunnelProtocolLacpSet, FsVlanTunnelProtocolLacpTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolStp}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolStpGet, FsVlanTunnelProtocolStpSet, FsVlanTunnelProtocolStpTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolGvrp}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolGvrpGet, FsVlanTunnelProtocolGvrpSet, FsVlanTunnelProtocolGvrpTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolGmrp}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolGmrpGet, FsVlanTunnelProtocolGmrpSet, FsVlanTunnelProtocolGmrpTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolIgmp}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolIgmpGet, FsVlanTunnelProtocolIgmpSet, FsVlanTunnelProtocolIgmpTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolMvrp}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolMvrpGet, FsVlanTunnelProtocolMvrpSet, FsVlanTunnelProtocolMvrpTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolMmrp}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolMmrpGet, FsVlanTunnelProtocolMmrpSet, FsVlanTunnelProtocolMmrpTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolElmi}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolElmiGet, FsVlanTunnelProtocolElmiSet, FsVlanTunnelProtocolElmiTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolLldp}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolLldpGet, FsVlanTunnelProtocolLldpSet, FsVlanTunnelProtocolLldpTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolEcfm}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolEcfmGet, FsVlanTunnelProtocolEcfmSet, FsVlanTunnelProtocolEcfmTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelOverrideOption}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelOverrideOptionGet, FsVlanTunnelOverrideOptionSet, FsVlanTunnelOverrideOptionTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, "2"},

{{12,FsVlanTunnelProtocolEoam}, GetNextIndexFsVlanTunnelProtocolTable, FsVlanTunnelProtocolEoamGet, FsVlanTunnelProtocolEoamSet, FsVlanTunnelProtocolEoamTest, FsVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanTunnelProtocolTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolDot1xPktsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolDot1xPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolDot1xPktsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolDot1xPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolLacpPktsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolLacpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolLacpPktsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolLacpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolStpPDUsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolStpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolStpPDUsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolStpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolGvrpPDUsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolGvrpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolGvrpPDUsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolGvrpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolGmrpPktsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolGmrpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolGmrpPktsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolGmrpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolIgmpPktsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolIgmpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolIgmpPktsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolIgmpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolMvrpPktsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolMvrpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolMvrpPktsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolMvrpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolMmrpPktsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolMmrpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolMmrpPktsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolMmrpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolElmiPktsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolElmiPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolElmiPktsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolElmiPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolLldpPktsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolLldpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolLldpPktsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolLldpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolEcfmPktsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolEcfmPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolEcfmPktsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolEcfmPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolEoamPktsRecvd}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolEoamPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanTunnelProtocolEoamPktsSent}, GetNextIndexFsVlanTunnelProtocolStatsTable, FsVlanTunnelProtocolEoamPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanTunnelProtocolStatsTableINDEX, 1, 0, 0, NULL},

{{10,FsVlanTunnelMvrpAddress}, NULL, FsVlanTunnelMvrpAddressGet, FsVlanTunnelMvrpAddressSet, FsVlanTunnelMvrpAddressTest, FsVlanTunnelMvrpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsVlanTunnelMmrpAddress}, NULL, FsVlanTunnelMmrpAddressGet, FsVlanTunnelMmrpAddressSet, FsVlanTunnelMmrpAddressTest, FsVlanTunnelMmrpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsVlanTunnelElmiAddress}, NULL, FsVlanTunnelElmiAddressGet, FsVlanTunnelElmiAddressSet, FsVlanTunnelElmiAddressTest, FsVlanTunnelElmiAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsVlanTunnelLldpAddress}, NULL, FsVlanTunnelLldpAddressGet, FsVlanTunnelLldpAddressSet, FsVlanTunnelLldpAddressTest, FsVlanTunnelLldpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsVlanTunnelEcfmAddress}, NULL, FsVlanTunnelEcfmAddressGet, FsVlanTunnelEcfmAddressSet, FsVlanTunnelEcfmAddressTest, FsVlanTunnelEcfmAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsServiceVlanId}, GetNextIndexFsServiceVlanTunnelProtocolTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsServiceVlanTunnelProtocolTableINDEX, 2, 0, 0, NULL},

{{12,FsServiceProtocolEnum}, GetNextIndexFsServiceVlanTunnelProtocolTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsServiceVlanTunnelProtocolTableINDEX, 2, 0, 0, NULL},

{{12,FsServiceVlanRsvdMacaddress}, GetNextIndexFsServiceVlanTunnelProtocolTable, FsServiceVlanRsvdMacaddressGet, FsServiceVlanRsvdMacaddressSet, FsServiceVlanRsvdMacaddressTest, FsServiceVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsServiceVlanTunnelProtocolTableINDEX, 2, 0, 0, NULL},

{{12,FsServiceVlanTunnelMacaddress}, GetNextIndexFsServiceVlanTunnelProtocolTable, FsServiceVlanTunnelMacaddressGet, FsServiceVlanTunnelMacaddressSet, FsServiceVlanTunnelMacaddressTest, FsServiceVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsServiceVlanTunnelProtocolTableINDEX, 2, 0, 0, NULL},

{{12,FsServiceVlanTunnelProtocolStatus}, GetNextIndexFsServiceVlanTunnelProtocolTable, FsServiceVlanTunnelProtocolStatusGet, FsServiceVlanTunnelProtocolStatusSet, FsServiceVlanTunnelProtocolStatusTest, FsServiceVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsServiceVlanTunnelProtocolTableINDEX, 2, 0, 0, NULL},

{{12,FsServiceVlanTunnelPktsRecvd}, GetNextIndexFsServiceVlanTunnelProtocolTable, FsServiceVlanTunnelPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsServiceVlanTunnelProtocolTableINDEX, 2, 0, 0, NULL},

{{12,FsServiceVlanTunnelPktsSent}, GetNextIndexFsServiceVlanTunnelProtocolTable, FsServiceVlanTunnelPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsServiceVlanTunnelProtocolTableINDEX, 2, 0, 0, NULL},

{{12,FsServiceVlanDiscardPktsRx}, GetNextIndexFsServiceVlanTunnelProtocolTable, FsServiceVlanDiscardPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsServiceVlanTunnelProtocolTableINDEX, 2, 0, 0, NULL},

{{12,FsServiceVlanDiscardPktsTx}, GetNextIndexFsServiceVlanTunnelProtocolTable, FsServiceVlanDiscardPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsServiceVlanTunnelProtocolTableINDEX, 2, 0, 0, NULL},

{{10,FsVlanTunnelEoamAddress}, NULL, FsVlanTunnelEoamAddressGet, FsVlanTunnelEoamAddressSet, FsVlanTunnelEoamAddressTest, FsVlanTunnelEoamAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsVlanTunnelIgmpAddress}, NULL, FsVlanTunnelIgmpAddressGet, FsVlanTunnelIgmpAddressSet, FsVlanTunnelIgmpAddressTest, FsVlanTunnelIgmpAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsVlanDiscardDot1xPktsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardDot1xPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardDot1xPktsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardDot1xPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardLacpPktsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardLacpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardLacpPktsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardLacpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardStpPDUsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardStpPDUsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardStpPDUsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardStpPDUsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardGvrpPktsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardGvrpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardGvrpPktsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardGvrpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardGmrpPktsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardGmrpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardGmrpPktsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardGmrpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardIgmpPktsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardIgmpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardIgmpPktsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardIgmpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardMvrpPktsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardMvrpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardMvrpPktsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardMvrpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardMmrpPktsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardMmrpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardMmrpPktsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardMmrpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardElmiPktsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardElmiPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardElmiPktsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardElmiPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardLldpPktsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardLldpPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardLldpPktsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardLldpPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardEcfmPktsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardEcfmPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardEcfmPktsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardEcfmPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardEoamPktsRx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardEoamPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsVlanDiscardEoamPktsTx}, GetNextIndexFsVlanDiscardStatsTable, FsVlanDiscardEoamPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVlanDiscardStatsTableINDEX, 1, 0, 0, NULL},
};
tMibData fsvlneEntry = { 86, fsvlneMibEntry };

#endif /* _FSVLNEDB_H */

