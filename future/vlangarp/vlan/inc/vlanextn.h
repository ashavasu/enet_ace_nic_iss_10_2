/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/* $Id: vlanextn.h,v 1.52 2016/07/07 14:17:25 siva Exp $                                                               */
/*  FILE NAME             : vlanextn.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : VLAN                                             */
/*  MODULE NAME           : VLAN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains extern declarations of        */
/*                          glabal data structures in VLAN module.           */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : VLAN TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#ifndef _VLANEXTN_H
#define _VLANEXTN_H

#ifdef ICCH_WANTED
extern tMcagTaskInfo gMcagTaskInfo;
extern tVLAN_SLL      gVlanMcagEntries;
extern tMcagTimer gMcagTimer;
#endif

/* Newly added for MI */
extern tVlanContextInfo   *gapVlanContextInfo [];
extern tVlanContextInfo *gpVlanContextInfo;

extern tRBTree   gpVlanLogicalPortTable;

extern tRBTree  gVlanPortTable; /* IfIndex based port RBTree  (tVlanPortEntry)*/
extern tVLAN_SLL gVlanTempPortList; /* Temp Portlist maintained by MI to updated
                                     the portlist. */
extern UINT4 gu4VlanGlobalTrace;

extern UINT4        gu4BaseBridgeMode;
extern UINT4 gu4SubnetGlobalOption;
#ifdef MPLS_WANTED
extern tRBTree        gpVlanL2VpnPortMapTbl;
extern tRBTree        gpVlanL2VpnPortVlanMapTbl;
#endif /*MPLS_WANTED*/

extern tVlanRegTbl    gaVlanRegTbl[VLAN_MAX_REG_MODULES];

extern UINT1 gau1VlanStatus[];
extern UINT1 gau1VlanShutDownStatus[];

extern tVlanTaskInfo         gVlanTaskInfo;

extern UINT1 gu1IsVlanInitialised; 

/* PRIORITY to Traffic class mapping table */
extern UINT1 gau1PriTrfClassMap[VLAN_MAX_PRIORITY][VLAN_MAX_PRIORITY];

/*Port types and their default L2CP tunnel status*/ 
extern UINT1 gau1PbDefPortProtoTunnelStatus[VLAN_MAX_PB_PORT_TYPES][VLAN_L2_PROTOCOLS];

extern UINT1 gau1PortBitMaskMap[VLAN_PORTS_PER_BYTE];
extern UINT1 gua1VlanDupBuff[VLAN_DEFAULT_PKT_SIZE];

extern tCVlanInfo gVlanPbCVlanInfo;
extern tVlanList gVlanNullVlanList;
extern tMacAddr gNullMacAddress;
extern tMacAddr gGmrpAddress;
extern tMacAddr gGvrpAddress;
extern tMacAddr gMmrpAddress;
extern tMacAddr gMvrpAddress;

extern tLocalPortList gNullPortList; 
extern tPortList gNullIfPortList; 
extern tLocalPortList gVlanLocalPortList;

extern tMacAddr  gVlanProviderStpAddr;
extern tMacAddr  gVlanProviderGvrpAddr;
extern tMacAddr  gVlanProviderGmrpAddr;
extern tMacAddr  gVlanProviderMvrpAddr; 
extern tMacAddr  gVlanProviderMmrpAddr; 
extern tMacAddr  gVlanProviderDot1xAddr;
extern tMacAddr  gVlanProviderLacpAddr;
extern tMacAddr  gVlanProviderElmiAddr;
extern tMacAddr  gVlanProviderLldpAddr;
extern tMacAddr  gVlanProviderEcfmAddr;
extern tMacAddr  gVlanProviderEoamAddr;
extern tMacAddr  gVlanProviderIgmpAddr;

extern UINT2 gu2VlanMacMapCount;
extern UINT2 gu2VlanSubnetMapCount;
#ifdef SYSLOG_WANTED
extern INT4 gi4VlanSysLogId;
#endif
#ifdef L2RED_WANTED 
extern tVlanRedTaskInfo    gVlanRedTaskInfo;
extern tVlanRedGlobalInfo  gVlanRedInfo;
#endif /* L2RED_WANTED */

extern INT4                gi4MibResStatus;
extern tVlanId             gVlanDefVlanId;

#ifdef PBBTE_PERFORM_TRACE_WANTED
extern struct timeval      gStartTime;
extern struct timeval      gEndTime;
extern UINT4               gu4TimeTaken;
#endif
#ifdef PBB_PERFORMANCE_WANTED
extern struct timeval      gCurTime;
extern struct timeval      gStTime;
extern struct timeval      gEnTime;
extern UINT4               gu4TimeTaken;
extern UINT4               gu4TotalTimeTaken;
extern UINT4               gu4ISIDCreateTimeTaken;
extern UINT4               gu4ISIDDeleteTimeTaken;
extern UINT4               gu4VlanIsidTimeTaken;
extern UINT4               gu4DelVlanIsidTimeTaken;
extern UINT4               gu4CreateBVLANTimeTaken;
extern UINT4               gu4DeleteBVLANTimeTaken;
extern UINT4               gu4CreateBVLANTunnelTimeTaken;
extern UINT4               gu4DeleteBVLANTunnelTimeTaken;
#endif


extern tFsModSizingParams gFsVlanSizingParams []; 
extern tFsModSizingInfo gFsVlanSizingInfo;
extern UINT4 gu4VlanCliContext;
extern UINT1 gu1SwStatsEnabled;
extern UINT4 gu4VlanPlatformLearningMode;

#ifdef MBSM_WANTED
extern INT4 gi4ProtoId;
extern UINT4 gu4ContextIdProcessed;
extern UINT2 gu2ByteIndex;
extern UINT2 gu2BitIndex;
extern UINT1 gu1IsBatchingStarted;
extern UINT1 gu1PortFlag;
extern tMbsmSlotInfo gMbsmSlotInfo;
extern tMbsmPortInfo gMbsmPortInfo;
#endif
#ifdef SW_LEARNING
#ifdef MBSM_WANTED
extern tRBTree             gVlanFDBInfo;
#endif /* MBSM_WANTD */
#endif /* SW_LEARNING */
extern VOID MefApiGetEvcInfo (UINT4, tEvcInfo *);
#endif

#ifdef PIM_WANTED
extern VOID PimVlanPortBmpChgNotify (tMacAddr MacAddr, tVlanId VlanId, UINT1 u1AddrType);
#endif
