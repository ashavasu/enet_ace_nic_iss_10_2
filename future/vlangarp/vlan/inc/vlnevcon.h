/*************************************************************************   
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: vlnevcon.h,v 1.7 2016/07/16 11:15:03 siva Exp $
*
* Description: This file contains VLAN EVB constant definitions.
*
*******************************************************************/
#ifndef _VLNEVCON_H
#define _VLNEVCON_H

/* EVB System Role */
#define VLAN_EVB_ROLE_BRIDGE                    1
#define VLAN_EVB_ROLE_STATION                   2

/* EVB System Status */
#define VLAN_EVB_SYSTEM_START                   1
#define VLAN_EVB_SYSTEM_SHUTDOWN                2
#define VLAN_EVB_DEF_SYSTEM_STATUS              VLAN_EVB_SYSTEM_SHUTDOWN

/* EVB Module Status */
#define VLAN_EVB_MODULE_ENABLE                  1
#define VLAN_EVB_MODULE_DISABLE                 2
#define VLAN_EVB_DEF_MODULE_STATUS              VLAN_EVB_MODULE_DISABLE

/* EVB Trap values */
#define VLAN_EVB_TRAP_ENABLE                    1
#define VLAN_EVB_TRAP_DISABLE                   2
#define VLAN_EVB_DEF_TRAP_STATUS                VLAN_EVB_TRAP_DISABLE
#define VLAN_EVB_SBP_CREATE_TRAP                1
#define VLAN_EVB_SBP_DELETE_TRAP                2
#define VLAN_EVB_SBP_CREATE_FAIL_TRAP           3
#define VLAN_EVB_SBP_DELETE_FAIL_TRAP           4
#define VLAN_EVB_SNMP_TRAP_OID_LEN              11
#define VLAN_EVB_TRAP_OID_LEN                   12
#define VLAN_EVB_SNMP_MAX_OID_LENGTH           257
#define VLAN_EVB_CFA_OBJECT_NAME_LEN           256

/* EVB RM values */
#define VLAN_EVB_BULK_SPLIT_MSG_SIZE             1500

/* EVB S-Channel L2 Filter Status */ 
#define VLAN_EVB_SCH_L2_FILTER_STATUS_ENABLE    1
#define VLAN_EVB_SCH_L2_FILTER_STATUS_DISABLE   2

/* EVB S-Channel Configuration Mode */
#define VLAN_EVB_SCH_MODE_AUTO                  1
#define VLAN_EVB_SCH_MODE_MANUAL                2
#define VLAN_EVB_DEF_SCH_MODE                   VLAN_EVB_SCH_MODE_AUTO

/* EVB UAP Entry Storage Types */
#define VLAN_EVB_STORAGE_TYPE_VOLATILE          2
#define VLAN_EVB_STORAGE_TYPE_NON_VOLATILE      3

/* EVB UAP CDCP Status either on local UAP or on remote UAP*/
#define VLAN_EVB_UAP_CDCP_ENABLE                1
#define VLAN_EVB_UAP_CDCP_DISABLE               2

/* EVB LLDP CDCP Enabling Status - as per MIB*/
#define VLAN_EVB_CDCP_LLDP_ENABLE               1
#define VLAN_EVB_CDCP_LLDP_DISABLE              0

/* EVB CDCP Admin Channel Capacity */
/* NOTE:Default value is not present in MIB. Check for 17 */
#define VLAN_EVB_DEF_CDCP_ADMIN_CHAN_CAP        17
#define VLAN_EVB_MIN_CDCP_ADMIN_CHAN_CAP        1
#define VLAN_EVB_MAX_CDCP_ADMIN_CHAN_CAP        17

/* EVB CDCP Oper Channel Capacity */
/* NOTE:Default value is not present in MIB. Check for 17 */
#define VLAN_EVB_DEF_CDCP_OPER_CHAN_CAP         17
#define VLAN_EVB_MIN_CDCP_OPER_CHAN_CAP         1
#define VLAN_EVB_MAX_CDCP_OPER_CHAN_CAP         17

/* EVB CDCP SVID Pool Values */
/* NOTE: check the range to be as (2-4093) */
#define VLAN_EVB_DEF_CDCP_SVID_POOL_LOW         1
#define VLAN_EVB_DEF_CDCP_SVID_POOL_HIGH        4094

/* EVB CDCP Running status on UAP */
#define VLAN_EVB_UAP_CDCP_RUNNING               1
#define VLAN_EVB_UAP_CDCP_NOT_RUNNING           2

/* EVB CDCP S-Channel entries mode */
#define VLAN_EVB_UAP_SCH_MODE_DYNAMIC           1
#define VLAN_EVB_UAP_SCH_MODE_HYBRID            2
#define VLAN_EVB_UAP_SCH_MODE_STATIC            0

/* EVB CDCP Default S-Channel SVID and SCID */
#define VLAN_EVB_DEF_SVID                       1
#define VLAN_EVB_DEF_SCID                       1
#define VLAN_EVB_MAX_SCID                    4094

/* EVB S-Channel Entries Allocation Status in Hybrid mode */
#define VLAN_EVB_SCH_FREE                       1
#define VLAN_EVB_SCH_NEGOTIATING                2
#define VLAN_EVB_SCH_CONFIRMED                  3

/* EVB HW Config Values */
#define VLAN_EVB_HW_SCH_IF_CREATE               1
#define VLAN_EVB_HW_SCH_IF_DELETE               2

/*LLDP message type */
#define L2IWF_LLDP_APPL_PORT_REREGISTER         1

#define VLAN_EVB_CDCP_SUBTYPE                   0x0e
#define VLAN_EVB_CDCP_ROLE_BRIDGE               0
#define VLAN_EVB_CDCP_ROLE_STATION              1
#define VLAN_EVB_CDCP_TLV_TYPE                  127
#define VLAN_EVB_CDCP_TLV_RES2                  0xff 
#define VLAN_EVB_CDCP_TLV_RES_CHNCAP            0xf 

#define VLAN_EVB_ZERO                           0
#define VLAN_EVB_CDCP_ONE                       1
#define VLAN_EVB_CDCP_TWO                       2
#define VLAN_EVB_CDCP_THREE                     3
#define VLAN_EVB_CDCP_FOUR                      4


#define VLAN_EVB_CDCP_TYPE_LEN_SIZE             2
#define VLAN_EVB_CDCP_TLV_OUI_LEN               3
#define VLAN_EVB_CDCP_TLV_SUBTYPE_LEN           1
/* Role (1bit) + Reserved1 (3bits) + SComp (1bit) +
 * Reserved2 (15bits) + ChanCap (12bits) */
#define VLAN_EVB_CDCP_TLV_RRSRC_LEN             4
#define VLAN_EVB_CDCP_SINGLE_PAIR_LEN           3
#define VLAN_EVB_MAX_CDCP_ENTRIES_PER_PORT      16
/* CDCP Packet Length in major division */
#define VLAN_EVB_CDCP_TLV_HDR_LEN               2
#define VLAN_EVB_CDCP_TLV_INFO_STR_LEN          510
#define VLAN_EVB_CDCP_SCID_SVID_PAIR_LEN        502
#define VLAN_EVB_CDCP_LEN_TILL_DEF_PAIR         (VLAN_EVB_CDCP_TLV_HDR_LEN +\
                                                 VLAN_EVB_CDCP_MANDATORY +\
                                                 VLAN_EVB_CDCP_SINGLE_PAIR_LEN)

#define VLAN_EVB_CDCP_TLV_RESERVED_LEN          1
#define VLAN_EVB_ROLE_STATION_LEN               1
#define VLAN_EVB_MAX_CDCP_ENTRIES_PER_PORT      16
#define VLAN_EVB_CDCP_TLV_LENGTH_IN_BITS        9
#define VLAN_EVB_CDCP_INFO_LENGTH_IN_BITS       31
#define VLAN_EVB_CDCP_RESERVED1                 1
#define VLAN_EVB_CDCP_RESERVED2                 1
#define VLAN_EVB_CDCP_TLV_LENGTH_IN_BITS        9
#define VLAN_EVB_CDCP_MANDATORY                 8
#define VLAN_EVB_CDCP_TABLE_ENTRY_LEN           3
#define VLAN_EVB_CDCP_CHN_CAP                   127
#define VLAN_EVB_CDCP_CHN_CAP_IN_BITS           12
#define VLAN_EVB_CDCP_LLDP_PORT_REG             1
#define VLAN_EVB_CDCP_LLDP_PORT_DEREG           2
#define VLAN_EVB_CDCP_LLDP_PORT_UPDATE          3

/*CDCP TLV construction */
#define VLAN_EVB_CDCP_TLV_ROLE_IN_BITS          7
#define VLAN_EVB_CDCP_TLV_RES1_IN_BITS          4
#define VLAN_EVB_CDCP_TLV_SCOMP_IN_BITS         3
#define VLAN_EVB_CDCP_TLV_RES2_IN_BITS          12
#define VLAN_EVB_CDCP_TLV_SCID_LEN_IN_BITS      12

#define VLAN_EVB_SCID_MASK                      0x00000fff
#define VLAN_EVB_SVID_MASK                      0xfff00000
#define VLAN_EVB_CDCP_CHNL_CAP_MASK             0x00000fff

#define VLAN_EVB_CDCP_ROLE_MASK                 0x80
#define VLAN_EVB_CDCP_SCOMP_MASK                0x08

#define VLAN_EVB_SCID_SVID_LEN                  3
#define VLAN_EVB_SVLAN_COMP                     1
#define VLAN_EVB_LLDP_ORG_SPEC_TLV              127

/* Trace related MACROs  */
#define VLAN_EVB_DEBUG_DEF_LVL_FLAG             1
#define VLAN_EVB_INIT_SHUT_TRC                  0x00000001
#define VLAN_EVB_MGMT_TRC                       0x00000002
#define VLAN_EVB_DATA_PATH_TRC                  0x00000004
#define VLAN_EVB_CONTROL_PATH_TRC               0x00000008
#define VLAN_EVB_PKT_DUMP_TRC                   0x00000010
#define VLAN_EVB_ALL_RESOURCE_TRC               0x00000020
#define VLAN_EVB_ALL_FAILURE_TRC                0x00000040
/* maximum trace level is obtained since we supporting until 7 bits 
 * Bit 8 is not allocated for any trace*/
#define VLAN_EVB_MAX_TRACE_LEVEL                0x7f

/* EVB Memory pool constants */
#define VLAN_EVB_CONTEXT_ENTRY                  1
#define VLAN_EVB_UAP_ENTRY                      2
#define VLAN_EVB_SCH_ENTRY                      3
#define VLAN_EVB_QUE_ENTRY                      4

#endif  /* _VLNEVCON_H */
