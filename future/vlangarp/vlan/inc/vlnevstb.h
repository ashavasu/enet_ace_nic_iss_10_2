/******************************************************************* 
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: vlnevstb.h,v 1.3 2016/06/01 09:50:58 siva Exp $
*
* Description: This file contains VLAN EVB constant definitions.
*
*******************************************************************/
#ifndef __VLNEVSTB_H__
#define __VLNEVSTB_H__

INT4
VlanEvbSChIfUpdateLocalPort (UINT4 u4SChIfIndex);
VOID
VlanEvbUapIfSetDefProperties (tVlanPortEntry *pVlanPortEntry);
INT4
VlanEvbUapIfWrAddEntry (UINT4 u4UapIfIndex);
INT4
VlanEvbUapSetOperStatus (UINT4 u4UapIfIndex, UINT1 u1OperStatus);
INT4
VlanEvbSChSetAdminStatus (UINT4 u4SChIfIndex, UINT1 u1Status);
INT4
VlanEvbCdcpProcessTlv (tEvbQueMsg * pMsg);
INT4
VlanEvbUapIfDelEntry (UINT4 u4UapIfIndex);
UINT1
VlanEvbGetSystemStatus (UINT4 u4ContextId);
INT4
VlanEvbStart (UINT4 u4ContextId);
VOID
VlanEvbShutdown (UINT4 u4ContextId);
VOID
VlanEvbPostCdcpMsg (tLldpAppTlv *pLldpAppTlv);
VOID
VlanEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex, tVlanEvbSbpArray  *pSbpArray);
INT4
VlanGetSChIfIndex (UINT4 u4UapIfIndex, UINT4 u4SVID, UINT4 *pu4SChIfIndex);
INT4
VlanGetSChInfoFromSChIfIndex (UINT4 u4SChIfIndex, UINT4 *pu4UapIfIndex,
                                  UINT2 *pu2SVID);
VOID
VlanEvbRedGetMsgLen (UINT1 u1MsgType, UINT2 *pu2MsgLen);
INT4
VlanEvbRedProcessCdcpTlvSyncUp (tRmMsg * pRmMsg, UINT2 *pu2Offset);
INT4
VlanEvbRedProcessRemoteRoleSyncUp (tRmMsg * pRmMsg, UINT2 *pu2Offset);
INT4
VlanEvbRedProcessSbpSyncUp (tRmMsg * pRmMsg,  UINT1 u1MsgType, UINT2 *pu2Offset);
VOID
VlanEvbRedSendSbpBulkSyncup (VOID);
VOID
VlanEvbRedSendCdcpTlvBulkSyncup (VOID);
VOID
VlanEvbRedSendRemoteRoleBulkSyncup (VOID);
INT4
VlanEvbSchIfDelDynamicEntry (UINT4 u4SbpIfIndex);

#endif /* __VLNEVSTB_H__ */
