/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1d1lw.h,v 1.3 2015/09/13 09:17:33 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Ieee8021BridgeBaseTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeBaseTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeBaseTable  */

INT1
nmhGetFirstIndexIeee8021BridgeBaseTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeBaseTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeBaseBridgeAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetIeee8021BridgeBaseNumPorts ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeBaseComponentType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeBaseDeviceCapabilities ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021BridgeBaseTrafficClassesEnabled ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeBaseMmrpEnabledStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeBaseRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeBaseBridgeAddress ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetIeee8021BridgeBaseComponentType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgeBaseDeviceCapabilities ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021BridgeBaseTrafficClassesEnabled ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgeBaseMmrpEnabledStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgeBaseRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeBaseBridgeAddress ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Ieee8021BridgeBaseComponentType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeBaseDeviceCapabilities ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021BridgeBaseTrafficClassesEnabled ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeBaseMmrpEnabledStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeBaseRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeBaseTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgeBasePortTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeBasePortTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeBasePortTable  */

INT1
nmhGetFirstIndexIeee8021BridgeBasePortTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeBasePortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeBasePortIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeBasePortDelayExceededDiscards ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021BridgeBasePortMtuExceededDiscards ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021BridgeBasePortCapabilities ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021BridgeBasePortTypeCapabilities ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021BridgeBasePortType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeBasePortExternal ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeBasePortAdminPointToPoint ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeBasePortOperPointToPoint ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeBasePortName ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeBasePortIfIndex ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgeBasePortAdminPointToPoint ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeBasePortIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgeBasePortAdminPointToPoint ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeBasePortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto Validate Index Instance for Ieee8021BridgeBaseIfToPortTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeBaseIfToPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeBaseIfToPortTable  */

INT1
nmhGetFirstIndexIeee8021BridgeBaseIfToPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeBaseIfToPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeBaseIfIndexComponentId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgeBaseIfIndexPort ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Ieee8021BridgePhyPortTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgePhyPortTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgePhyPortTable  */

INT1
nmhGetFirstIndexIeee8021BridgePhyPortTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgePhyPortTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgePhyPortIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgePhyMacAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetIeee8021BridgePhyPortToComponentId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgePhyPortToInternalPort ARG_LIST((UINT4 ,UINT4 *));



/* Proto Validate Index Instance for Ieee8021BridgeTpPortTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeTpPortTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeTpPortTable  */

INT1
nmhGetFirstIndexIeee8021BridgeTpPortTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeTpPortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeTpPortMaxInfo ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgeTpPortInFrames ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021BridgeTpPortOutFrames ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021BridgeTpPortInDiscards ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for Ieee8021BridgePortPriorityTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgePortPriorityTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgePortPriorityTable  */

INT1
nmhGetFirstIndexIeee8021BridgePortPriorityTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgePortPriorityTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgePortDefaultUserPriority ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgePortNumTrafficClasses ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgePortPriorityCodePointSelection ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgePortUseDEI ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgePortRequireDropEncoding ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgePortServiceAccessPrioritySelection ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgePortDefaultUserPriority ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021BridgePortNumTrafficClasses ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgePortPriorityCodePointSelection ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgePortUseDEI ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgePortRequireDropEncoding ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgePortServiceAccessPrioritySelection ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgePortDefaultUserPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021BridgePortNumTrafficClasses ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgePortPriorityCodePointSelection ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgePortUseDEI ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgePortRequireDropEncoding ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgePortServiceAccessPrioritySelection ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgePortPriorityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgeUserPriorityRegenTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeUserPriorityRegenTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeUserPriorityRegenTable  */

INT1
nmhGetFirstIndexIeee8021BridgeUserPriorityRegenTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeUserPriorityRegenTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeRegenUserPriority ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeRegenUserPriority ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeRegenUserPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeUserPriorityRegenTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgeTrafficClassTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeTrafficClassTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeTrafficClassTable  */

INT1
nmhGetFirstIndexIeee8021BridgeTrafficClassTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeTrafficClassTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeTrafficClass ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeTrafficClass ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeTrafficClass ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeTrafficClassTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgePortOutboundAccessPriorityTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgePortOutboundAccessPriorityTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgePortOutboundAccessPriorityTable  */

INT1
nmhGetFirstIndexIeee8021BridgePortOutboundAccessPriorityTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgePortOutboundAccessPriorityTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgePortOutboundAccessPriority ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Ieee8021BridgePortDecodingTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgePortDecodingTable ARG_LIST((UINT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgePortDecodingTable  */

INT1
nmhGetFirstIndexIeee8021BridgePortDecodingTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgePortDecodingTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgePortDecodingPriority ARG_LIST((UINT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetIeee8021BridgePortDecodingDropEligible ARG_LIST((UINT4  , UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgePortDecodingPriority ARG_LIST((UINT4  , UINT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetIeee8021BridgePortDecodingDropEligible ARG_LIST((UINT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgePortDecodingPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021BridgePortDecodingDropEligible ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgePortDecodingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgePortEncodingTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgePortEncodingTable ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgePortEncodingTable  */

INT1
nmhGetFirstIndexIeee8021BridgePortEncodingTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgePortEncodingTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgePortEncodingPriority ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgePortEncodingPriority ARG_LIST((UINT4  , UINT4  , INT4  , INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgePortEncodingPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgePortEncodingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgeServiceAccessPriorityTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeServiceAccessPriorityTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeServiceAccessPriorityTable  */

INT1
nmhGetFirstIndexIeee8021BridgeServiceAccessPriorityTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeServiceAccessPriorityTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeServiceAccessPriorityValue ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeServiceAccessPriorityValue ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeServiceAccessPriorityValue ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeServiceAccessPriorityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgeILanIfTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeILanIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeILanIfTable  */

INT1
nmhGetFirstIndexIeee8021BridgeILanIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeILanIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeILanIfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeILanIfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeILanIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeILanIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgeDot1dPortTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgeDot1dPortTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgeDot1dPortTable  */

INT1
nmhGetFirstIndexIeee8021BridgeDot1dPortTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgeDot1dPortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgeDot1dPortRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgeDot1dPortRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgeDot1dPortRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgeDot1dPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


