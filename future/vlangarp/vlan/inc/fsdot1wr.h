/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: fsdot1wr.h,v 1.6 2013/12/05 12:44:52 siva Exp $
* Description: This header file contains all prototype of the wrapper  
*              routines in VLAN Modules.
****************************************************************************/
#ifndef _FSDOT1WR_H
#define _FSDOT1WR_H
INT4 GetNextIndexDot1adPortTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSDOT1(VOID);

VOID UnRegisterFSDOT1(VOID);
INT4 Dot1adPortPcpSelectionRowGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPortUseDeiGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPortReqDropEncodingGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPortSVlanPriorityTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPortSVlanPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPortPcpSelectionRowSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPortUseDeiSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPortReqDropEncodingSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPortSVlanPriorityTypeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPortSVlanPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPortPcpSelectionRowTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPortUseDeiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPortReqDropEncodingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPortSVlanPriorityTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPortSVlanPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexDot1adVidTranslationTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adVidTranslationRelayVidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adVidTranslationRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adVidTranslationRelayVidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adVidTranslationRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adVidTranslationRelayVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adVidTranslationRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adVidTranslationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1adCVidRegistrationTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adCVidRegistrationSVidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationUntaggedPepGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationUntaggedCepGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationSVlanPriorityTypeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationSVlanPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVIdRegistrationRelayCVidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationSVidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationUntaggedPepSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationUntaggedCepSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationSVlanPriorityTypeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationSVlanPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVIdRegistrationRelayCVidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationSVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationUntaggedPepTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationUntaggedCepTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationSVlanPriorityTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationSVlanPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adCVIdRegistrationRelayCVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adCVidRegistrationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexDot1adPepTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adPepPvidGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPepDefaultUserPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPepAccptableFrameTypesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPepIngressFilteringGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPepPvidSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPepDefaultUserPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPepAccptableFrameTypesSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPepIngressFilteringSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPepPvidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPepDefaultUserPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPepAccptableFrameTypesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPepIngressFilteringTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPepTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexDot1adServicePriorityRegenerationTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adServicePriorityRegenRegeneratedPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adServicePriorityRegenRegeneratedPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1adServicePriorityRegenRegeneratedPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adServicePriorityRegenerationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexDot1adPcpDecodingTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adPcpDecodingPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPcpDecodingDropEligibleGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPcpDecodingPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPcpDecodingDropEligibleSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPcpDecodingPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPcpDecodingDropEligibleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPcpDecodingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexDot1adPcpEncodingTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1adPcpEncodingPcpValueGet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPcpEncodingPcpValueSet(tSnmpIndex *, tRetVal *);
INT4 Dot1adPcpEncodingPcpValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1adPcpEncodingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif /* _FSDOT1WR_H */
