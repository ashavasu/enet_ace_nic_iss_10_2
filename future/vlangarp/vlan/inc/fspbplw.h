/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbplw.h,v 1.3 2007/02/01 15:09:07 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbMulticastMacLimit ARG_LIST((UINT4 *));

INT1
nmhGetFsPbTunnelStpAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsPbTunnelLacpAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsPbTunnelDot1xAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsPbTunnelGvrpAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsPbTunnelGmrpAddress ARG_LIST((tMacAddr * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbMulticastMacLimit ARG_LIST((UINT4 ));

INT1
nmhSetFsPbTunnelStpAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsPbTunnelLacpAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsPbTunnelDot1xAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsPbTunnelGvrpAddress ARG_LIST((tMacAddr ));

INT1
nmhSetFsPbTunnelGmrpAddress ARG_LIST((tMacAddr ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbMulticastMacLimit ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsPbTunnelStpAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsPbTunnelLacpAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsPbTunnelDot1xAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsPbTunnelGvrpAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsPbTunnelGmrpAddress ARG_LIST((UINT4 *  ,tMacAddr ));

/* Proto Validate Index Instance for FsPbPortInfoTable. */
INT1
nmhValidateIndexInstanceFsPbPortInfoTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbPortInfoTable  */

INT1
nmhGetFirstIndexFsPbPortInfoTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbPortInfoTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbPortSVlanClassificationMethod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbPortSVlanIngressEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbPortSVlanEgressEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbPortSVlanEtherTypeSwapStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbPortSVlanTranslationStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbPortUnicastMacLearning ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbPortUnicastMacLimit ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbPortSVlanClassificationMethod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbPortSVlanIngressEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbPortSVlanEgressEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbPortSVlanEtherTypeSwapStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbPortSVlanTranslationStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbPortUnicastMacLearning ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbPortUnicastMacLimit ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbPortSVlanClassificationMethod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbPortSVlanIngressEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbPortSVlanEgressEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbPortSVlanEtherTypeSwapStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbPortSVlanTranslationStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbPortUnicastMacLearning ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbPortUnicastMacLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Proto Validate Index Instance for FsPbSrcMacSVlanTable. */
INT1
nmhValidateIndexInstanceFsPbSrcMacSVlanTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsPbSrcMacSVlanTable  */

INT1
nmhGetFirstIndexFsPbSrcMacSVlanTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbSrcMacSVlanTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbSrcMacSVlan ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsPbSrcMacRowStatus ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbSrcMacSVlan ARG_LIST((INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsPbSrcMacRowStatus ARG_LIST((INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbSrcMacSVlan ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsPbSrcMacRowStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

/* Proto Validate Index Instance for FsPbDstMacSVlanTable. */
INT1
nmhValidateIndexInstanceFsPbDstMacSVlanTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsPbDstMacSVlanTable  */

INT1
nmhGetFirstIndexFsPbDstMacSVlanTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbDstMacSVlanTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbDstMacSVlan ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsPbDstMacRowStatus ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbDstMacSVlan ARG_LIST((INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsPbDstMacRowStatus ARG_LIST((INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbDstMacSVlan ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsPbDstMacRowStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

/* Proto Validate Index Instance for FsPbCVlanSrcMacSVlanTable. */
INT1
nmhValidateIndexInstanceFsPbCVlanSrcMacSVlanTable ARG_LIST((INT4  , INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsPbCVlanSrcMacSVlanTable  */

INT1
nmhGetFirstIndexFsPbCVlanSrcMacSVlanTable ARG_LIST((INT4 * , INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbCVlanSrcMacSVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbCVlanSrcMacSVlan ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsPbCVlanSrcMacRowStatus ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbCVlanSrcMacSVlan ARG_LIST((INT4  , INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsPbCVlanSrcMacRowStatus ARG_LIST((INT4  , INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbCVlanSrcMacSVlan ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsPbCVlanSrcMacRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,INT4 ));

/* Proto Validate Index Instance for FsPbCVlanDstMacSVlanTable. */
INT1
nmhValidateIndexInstanceFsPbCVlanDstMacSVlanTable ARG_LIST((INT4  , INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsPbCVlanDstMacSVlanTable  */

INT1
nmhGetFirstIndexFsPbCVlanDstMacSVlanTable ARG_LIST((INT4 * , INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbCVlanDstMacSVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbCVlanDstMacSVlan ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsPbCVlanDstMacRowStatus ARG_LIST((INT4  , INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbCVlanDstMacSVlan ARG_LIST((INT4  , INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsPbCVlanDstMacRowStatus ARG_LIST((INT4  , INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbCVlanDstMacSVlan ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsPbCVlanDstMacRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tMacAddr  ,INT4 ));

/* Proto Validate Index Instance for FsPbDscpSVlanTable. */
INT1
nmhValidateIndexInstanceFsPbDscpSVlanTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbDscpSVlanTable  */

INT1
nmhGetFirstIndexFsPbDscpSVlanTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbDscpSVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbDscpSVlan ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbDscpRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbDscpSVlan ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPbDscpRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbDscpSVlan ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPbDscpRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for FsPbCVlanDscpSVlanTable. */
INT1
nmhValidateIndexInstanceFsPbCVlanDscpSVlanTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbCVlanDscpSVlanTable  */

INT1
nmhGetFirstIndexFsPbCVlanDscpSVlanTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbCVlanDscpSVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbCVlanDscpSVlan ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbCVlanDscpRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbCVlanDscpSVlan ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPbCVlanDscpRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbCVlanDscpSVlan ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPbCVlanDscpRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for FsPbSrcIpAddrSVlanTable. */
INT1
nmhValidateIndexInstanceFsPbSrcIpAddrSVlanTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbSrcIpAddrSVlanTable  */

INT1
nmhGetFirstIndexFsPbSrcIpAddrSVlanTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbSrcIpAddrSVlanTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbSrcIpSVlan ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPbSrcIpRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbSrcIpSVlan ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsPbSrcIpRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbSrcIpSVlan ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsPbSrcIpRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Proto Validate Index Instance for FsPbDstIpAddrSVlanTable. */
INT1
nmhValidateIndexInstanceFsPbDstIpAddrSVlanTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbDstIpAddrSVlanTable  */

INT1
nmhGetFirstIndexFsPbDstIpAddrSVlanTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbDstIpAddrSVlanTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbDstIpSVlan ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPbDstIpRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbDstIpSVlan ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsPbDstIpRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbDstIpSVlan ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsPbDstIpRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Proto Validate Index Instance for FsPbSrcDstIpSVlanTable. */
INT1
nmhValidateIndexInstanceFsPbSrcDstIpSVlanTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbSrcDstIpSVlanTable  */

INT1
nmhGetFirstIndexFsPbSrcDstIpSVlanTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbSrcDstIpSVlanTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbSrcDstIpSVlan ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPbSrcDstIpRowStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbSrcDstIpSVlan ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsPbSrcDstIpRowStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbSrcDstIpSVlan ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsPbSrcDstIpRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Proto Validate Index Instance for FsPbCVlanDstIpSVlanTable. */
INT1
nmhValidateIndexInstanceFsPbCVlanDstIpSVlanTable ARG_LIST((INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbCVlanDstIpSVlanTable  */

INT1
nmhGetFirstIndexFsPbCVlanDstIpSVlanTable ARG_LIST((INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbCVlanDstIpSVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbCVlanDstIpSVlan ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPbCVlanDstIpRowStatus ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbCVlanDstIpSVlan ARG_LIST((INT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsPbCVlanDstIpRowStatus ARG_LIST((INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbCVlanDstIpSVlan ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsPbCVlanDstIpRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,INT4 ));

/* Proto Validate Index Instance for FsPbPortBasedCVlanTable. */
INT1
nmhValidateIndexInstanceFsPbPortBasedCVlanTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbPortBasedCVlanTable  */

INT1
nmhGetFirstIndexFsPbPortBasedCVlanTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbPortBasedCVlanTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbPortCVlan ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbPortCVlanClassifyStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbPortCVlan ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbPortCVlanClassifyStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbPortCVlan ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbPortCVlanClassifyStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsPbFdbUnicastMacControlTable. */
INT1
nmhValidateIndexInstanceFsPbFdbUnicastMacControlTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbFdbUnicastMacControlTable  */

INT1
nmhGetFirstIndexFsPbFdbUnicastMacControlTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbFdbUnicastMacControlTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbFdbUnicastMacLimit ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsPbFdbAdminMacLearningStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsPbFdbOperMacLearningStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbFdbUnicastMacLimit ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsPbFdbAdminMacLearningStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbFdbUnicastMacLimit ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsPbFdbAdminMacLearningStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Proto Validate Index Instance for FsPbEtherTypeSwapTable. */
INT1
nmhValidateIndexInstanceFsPbEtherTypeSwapTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbEtherTypeSwapTable  */

INT1
nmhGetFirstIndexFsPbEtherTypeSwapTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbEtherTypeSwapTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbRelayEtherType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbEtherTypeSwapRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbRelayEtherType ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPbEtherTypeSwapRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbRelayEtherType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPbEtherTypeSwapRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for FsPbSVlanConfigTable. */
INT1
nmhValidateIndexInstanceFsPbSVlanConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbSVlanConfigTable  */

INT1
nmhGetFirstIndexFsPbSVlanConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbSVlanConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbSVlanConfigServiceType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbSVlanConfigServiceType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbSVlanConfigServiceType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Proto Validate Index Instance for FsPbTunnelProtocolTable. */
INT1
nmhValidateIndexInstanceFsPbTunnelProtocolTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbTunnelProtocolTable  */

INT1
nmhGetFirstIndexFsPbTunnelProtocolTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbTunnelProtocolTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbTunnelProtocolDot1x ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbTunnelProtocolLacp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbTunnelProtocolStp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbTunnelProtocolGvrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbTunnelProtocolGmrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbTunnelProtocolIgmp ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbTunnelProtocolDot1x ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbTunnelProtocolLacp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbTunnelProtocolStp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbTunnelProtocolGvrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbTunnelProtocolGmrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbTunnelProtocolIgmp ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbTunnelProtocolDot1x ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbTunnelProtocolLacp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbTunnelProtocolStp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbTunnelProtocolGvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbTunnelProtocolGmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbTunnelProtocolIgmp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsPbTunnelProtocolStatsTable. */
INT1
nmhValidateIndexInstanceFsPbTunnelProtocolStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbTunnelProtocolStatsTable  */

INT1
nmhGetFirstIndexFsPbTunnelProtocolStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbTunnelProtocolStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbTunnelProtocolDot1xPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbTunnelProtocolDot1xPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbTunnelProtocolLacpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbTunnelProtocolLacpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbTunnelProtocolStpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbTunnelProtocolStpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbTunnelProtocolGvrpPDUsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbTunnelProtocolGvrpPDUsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbTunnelProtocolGmrpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbTunnelProtocolGmrpPktsSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbTunnelProtocolIgmpPktsRecvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbTunnelProtocolIgmpPktsSent ARG_LIST((INT4 ,UINT4 *));
