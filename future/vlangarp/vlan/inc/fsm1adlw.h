/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsm1adlw.h,v 1.8 2013/12/05 12:44:52 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Dot1adMIPortTable. */
INT1
nmhValidateIndexInstanceDot1adMIPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adMIPortTable  */

INT1
nmhGetFirstIndexDot1adMIPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adMIPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adMIPortPcpSelectionRow ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1adMIPortUseDei ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1adMIPortReqDropEncoding ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1adMIPortSVlanPriorityType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1adMIPortSVlanPriority ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adMIPortPcpSelectionRow ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1adMIPortUseDei ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1adMIPortReqDropEncoding ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1adMIPortSVlanPriorityType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1adMIPortSVlanPriority ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adMIPortPcpSelectionRow ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMIPortUseDei ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMIPortReqDropEncoding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMIPortSVlanPriorityType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMIPortSVlanPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adMIPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adMIVidTranslationTable. */
INT1
nmhValidateIndexInstanceDot1adMIVidTranslationTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adMIVidTranslationTable  */

INT1
nmhGetFirstIndexDot1adMIVidTranslationTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adMIVidTranslationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adMIVidTranslationRelayVid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adMIVidTranslationRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adMIVidTranslationRelayVid ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adMIVidTranslationRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adMIVidTranslationRelayVid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMIVidTranslationRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adMIVidTranslationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adMICVidRegistrationTable. */
INT1
nmhValidateIndexInstanceDot1adMICVidRegistrationTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adMICVidRegistrationTable  */

INT1
nmhGetFirstIndexDot1adMICVidRegistrationTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adMICVidRegistrationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adMICVidRegistrationSVid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adMICVidRegistrationUntaggedPep ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adMICVidRegistrationUntaggedCep ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adMICVidRegistrationRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adMICVidRegistrationRelayCVid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adMICVidRegistrationSVlanPriorityType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adMICVidRegistrationSVlanPriority ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adMICVidRegistrationSVid ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adMICVidRegistrationUntaggedPep ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adMICVidRegistrationUntaggedCep ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adMICVidRegistrationRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adMICVidRegistrationRelayCVid ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adMICVidRegistrationSVlanPriorityType ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adMICVidRegistrationSVlanPriority ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adMICVidRegistrationSVid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMICVidRegistrationUntaggedPep ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMICVidRegistrationUntaggedCep ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMICVidRegistrationRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMICVidRegistrationRelayCVid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMICVidRegistrationSVlanPriorityType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMICVidRegistrationSVlanPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adMICVidRegistrationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adMIPepTable. */
INT1
nmhValidateIndexInstanceDot1adMIPepTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adMIPepTable  */

INT1
nmhGetFirstIndexDot1adMIPepTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adMIPepTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adMIPepPvid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adMIPepDefaultUserPriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adMIPepAccptableFrameTypes ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adMIPepIngressFiltering ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adMIPepPvid ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adMIPepDefaultUserPriority ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adMIPepAccptableFrameTypes ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adMIPepIngressFiltering ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adMIPepPvid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMIPepDefaultUserPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMIPepAccptableFrameTypes ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMIPepIngressFiltering ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adMIPepTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adMIServicePriorityRegenerationTable. */
INT1
nmhValidateIndexInstanceDot1adMIServicePriorityRegenerationTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adMIServicePriorityRegenerationTable  */

INT1
nmhGetFirstIndexDot1adMIServicePriorityRegenerationTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adMIServicePriorityRegenerationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adMIServicePriorityRegenRegeneratedPriority ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adMIServicePriorityRegenRegeneratedPriority ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adMIServicePriorityRegenRegeneratedPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adMIServicePriorityRegenerationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adMIPcpDecodingTable. */
INT1
nmhValidateIndexInstanceDot1adMIPcpDecodingTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adMIPcpDecodingTable  */

INT1
nmhGetFirstIndexDot1adMIPcpDecodingTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adMIPcpDecodingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adMIPcpDecodingPriority ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetDot1adMIPcpDecodingDropEligible ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adMIPcpDecodingPriority ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetDot1adMIPcpDecodingDropEligible ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adMIPcpDecodingPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Dot1adMIPcpDecodingDropEligible ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adMIPcpDecodingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot1adMIPcpEncodingTable. */
INT1
nmhValidateIndexInstanceDot1adMIPcpEncodingTable ARG_LIST((INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1adMIPcpEncodingTable  */

INT1
nmhGetFirstIndexDot1adMIPcpEncodingTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1adMIPcpEncodingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1adMIPcpEncodingPcpValue ARG_LIST((INT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1adMIPcpEncodingPcpValue ARG_LIST((INT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1adMIPcpEncodingPcpValue ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot1adMIPcpEncodingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
