/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpvldb.h,v 1.32.22.1 2018/03/15 12:59:53 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPVLDB_H
#define _FSMPVLDB_H

UINT1 FsMIDot1qFutureVlanGlobalsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIDot1qFutureVlanPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIDot1qFutureVlanPortMacMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIDot1qFutureVlanFidMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIDot1qFutureVlanTunnelConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIDot1qFutureVlanTunnelTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIDot1qFutureVlanTunnelProtocolTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIDot1qFutureVlanCounterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIDot1qFutureVlanUnicastMacControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIDot1qFutureVlanTpFdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIDot1qFutureVlanWildCardTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMIDot1qFutureVlanWildCardPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIDot1qFutureStaticUnicastExtnTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIDot1qFutureVlanPortSubnetMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIDot1qFutureStVlanExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIDot1qFutureVlanPortSubnetMapExtTableINDEX [] = 
{SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,
    SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIDot1qFuturePortVlanExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIDot1qFutureVlanLoopbackTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmpvl [] ={1,3,6,1,4,1,2076,120};
tSNMP_OID_TYPE fsmpvlOID = {8, fsmpvl};


UINT4 FsMIDot1qFutureVlanGlobalTrace [ ] ={1,3,6,1,4,1,2076,120,1,1};
UINT4 FsMIDot1qFutureVlanContextId [ ] ={1,3,6,1,4,1,2076,120,1,2,1,1};
UINT4 FsMIDot1qFutureVlanStatus [ ] ={1,3,6,1,4,1,2076,120,1,2,1,2};
UINT4 FsMIDot1qFutureVlanMacBasedOnAllPorts [ ] ={1,3,6,1,4,1,2076,120,1,2,1,3};
UINT4 FsMIDot1qFutureVlanPortProtoBasedOnAllPorts [ ] ={1,3,6,1,4,1,2076,120,1,2,1,4};
UINT4 FsMIDot1qFutureVlanShutdownStatus [ ] ={1,3,6,1,4,1,2076,120,1,2,1,5};
UINT4 FsMIDot1qFutureGarpShutdownStatus [ ] ={1,3,6,1,4,1,2076,120,1,2,1,6};
UINT4 FsMIDot1qFutureVlanDebug [ ] ={1,3,6,1,4,1,2076,120,1,2,1,7};
UINT4 FsMIDot1qFutureVlanLearningMode [ ] ={1,3,6,1,4,1,2076,120,1,2,1,8};
UINT4 FsMIDot1qFutureVlanHybridTypeDefault [ ] ={1,3,6,1,4,1,2076,120,1,2,1,9};
UINT4 FsMIDot1qFutureVlanOperStatus [ ] ={1,3,6,1,4,1,2076,120,1,2,1,10};
UINT4 FsMIDot1qFutureGvrpOperStatus [ ] ={1,3,6,1,4,1,2076,120,1,2,1,11};
UINT4 FsMIDot1qFutureGmrpOperStatus [ ] ={1,3,6,1,4,1,2076,120,1,2,1,12};
UINT4 FsMIDot1qFutureVlanContextName [ ] ={1,3,6,1,4,1,2076,120,1,2,1,13};
UINT4 FsMIDot1qFutureGarpDebug [ ] ={1,3,6,1,4,1,2076,120,1,2,1,14};
UINT4 FsMIDot1qFutureUnicastMacLearningLimit [ ] ={1,3,6,1,4,1,2076,120,1,2,1,15};
UINT4 FsMIDot1qFutureBaseBridgeMode [ ] ={1,3,6,1,4,1,2076,120,1,2,1,16};
UINT4 FsMIDot1qFutureVlanSubnetBasedOnAllPorts [ ] ={1,3,6,1,4,1,2076,120,1,2,1,17};
UINT4 FsMIDot1qFutureVlanGlobalMacLearningStatus [ ] ={1,3,6,1,4,1,2076,120,1,2,1,18};
UINT4 FsMIDot1qFutureVlanApplyEnhancedFilteringCriteria [ ] ={1,3,6,1,4,1,2076,120,1,2,1,19};
UINT4 FsMIDot1qFutureVlanGlobalsFdbFlush [ ] ={1,3,6,1,4,1,2076,120,1,2,1,20};
UINT4 FsMIDot1qFutureVlanUserDefinedTPID [ ] ={1,3,6,1,4,1,2076,120,1,2,1,21};
UINT4 FsMIDot1qFutureVlanRemoteFdbFlush [ ] ={1,3,6,1,4,1,2076,120,1,2,1,22};
UINT4 FsMIDot1qFutureVlanPort [ ] ={1,3,6,1,4,1,2076,120,1,3,1,1};
UINT4 FsMIDot1qFutureVlanPortType [ ] ={1,3,6,1,4,1,2076,120,1,3,1,2};
UINT4 FsMIDot1qFutureVlanPortMacBasedClassification [ ] ={1,3,6,1,4,1,2076,120,1,3,1,3};
UINT4 FsMIDot1qFutureVlanPortPortProtoBasedClassification [ ] ={1,3,6,1,4,1,2076,120,1,3,1,4};
UINT4 FsMIDot1qFutureVlanFilteringUtilityCriteria [ ] ={1,3,6,1,4,1,2076,120,1,3,1,5};
UINT4 FsMIDot1qFutureVlanPortProtected [ ] ={1,3,6,1,4,1,2076,120,1,3,1,6};
UINT4 FsMIDot1qFutureVlanPortSubnetBasedClassification [ ] ={1,3,6,1,4,1,2076,120,1,3,1,7};
UINT4 FsMIDot1qFutureVlanPortUnicastMacLearning [ ] ={1,3,6,1,4,1,2076,120,1,3,1,8};
UINT4 FsMIDot1qFutureVlanPortGmrpJoinEmptyTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,9};
UINT4 FsMIDot1qFutureVlanPortGmrpJoinEmptyRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,10};
UINT4 FsMIDot1qFutureVlanPortGmrpJoinInTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,11};
UINT4 FsMIDot1qFutureVlanPortGmrpJoinInRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,12};
UINT4 FsMIDot1qFutureVlanPortGmrpLeaveInTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,13};
UINT4 FsMIDot1qFutureVlanPortGmrpLeaveInRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,14};
UINT4 FsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,15};
UINT4 FsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,16};
UINT4 FsMIDot1qFutureVlanPortGmrpEmptyTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,17};
UINT4 FsMIDot1qFutureVlanPortGmrpEmptyRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,18};
UINT4 FsMIDot1qFutureVlanPortGmrpLeaveAllTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,19};
UINT4 FsMIDot1qFutureVlanPortGmrpLeaveAllRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,20};
UINT4 FsMIDot1qFutureVlanPortGmrpDiscardCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,21};
UINT4 FsMIDot1qFutureVlanPortGvrpJoinEmptyTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,22};
UINT4 FsMIDot1qFutureVlanPortGvrpJoinEmptyRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,23};
UINT4 FsMIDot1qFutureVlanPortGvrpJoinInTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,24};
UINT4 FsMIDot1qFutureVlanPortGvrpJoinInRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,25};
UINT4 FsMIDot1qFutureVlanPortGvrpLeaveInTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,26};
UINT4 FsMIDot1qFutureVlanPortGvrpLeaveInRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,27};
UINT4 FsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,28};
UINT4 FsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,29};
UINT4 FsMIDot1qFutureVlanPortGvrpEmptyTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,30};
UINT4 FsMIDot1qFutureVlanPortGvrpEmptyRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,31};
UINT4 FsMIDot1qFutureVlanPortGvrpLeaveAllTxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,32};
UINT4 FsMIDot1qFutureVlanPortGvrpLeaveAllRxCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,33};
UINT4 FsMIDot1qFutureVlanPortGvrpDiscardCount [ ] ={1,3,6,1,4,1,2076,120,1,3,1,34};
UINT4 FsMIDot1qFutureVlanPortFdbFlush [ ] ={1,3,6,1,4,1,2076,120,1,3,1,35};
UINT4 FsMIDot1qFutureVlanPortIngressEtherType [ ] ={1,3,6,1,4,1,2076,120,1,3,1,36};
UINT4 FsMIDot1qFutureVlanPortEgressEtherType [ ] ={1,3,6,1,4,1,2076,120,1,3,1,37};
UINT4 FsMIDot1qFutureVlanPortEgressTPIDType [ ] ={1,3,6,1,4,1,2076,120,1,3,1,38};
UINT4 FsMIDot1qFutureVlanPortAllowableTPID1 [ ] ={1,3,6,1,4,1,2076,120,1,3,1,39};
UINT4 FsMIDot1qFutureVlanPortAllowableTPID2 [ ] ={1,3,6,1,4,1,2076,120,1,3,1,40};
UINT4 FsMIDot1qFutureVlanPortAllowableTPID3 [ ] ={1,3,6,1,4,1,2076,120,1,3,1,41};
UINT4 FsMIDot1qFutureVlanPortClearGarpStats [ ] ={1,3,6,1,4,1,2076,120,1,3,1,42};
UINT4 FsMIDot1qFuturePortPacketReflectionStatus [ ] ={1,3,6,1,4,1,2076,120,1,3,1,44};
UINT4 FsMIDot1qFutureVlanPortMacMapAddr [ ] ={1,3,6,1,4,1,2076,120,1,4,1,1};
UINT4 FsMIDot1qFutureVlanPortMacMapVid [ ] ={1,3,6,1,4,1,2076,120,1,4,1,2};
UINT4 FsMIDot1qFutureVlanPortMacMapName [ ] ={1,3,6,1,4,1,2076,120,1,4,1,3};
UINT4 FsMIDot1qFutureVlanPortMacMapMcastBcastOption [ ] ={1,3,6,1,4,1,2076,120,1,4,1,4};
UINT4 FsMIDot1qFutureVlanPortMacMapRowStatus [ ] ={1,3,6,1,4,1,2076,120,1,4,1,5};
UINT4 FsMIDot1qFutureVlanIndex [ ] ={1,3,6,1,4,1,2076,120,1,5,1,1};
UINT4 FsMIDot1qFutureVlanFid [ ] ={1,3,6,1,4,1,2076,120,1,5,1,2};
UINT4 FsMIDot1qFutureVlanBridgeMode [ ] ={1,3,6,1,4,1,2076,120,2,1,1,1};
UINT4 FsMIDot1qFutureVlanTunnelBpduPri [ ] ={1,3,6,1,4,1,2076,120,2,1,1,2};
UINT4 FsMIDot1qFutureVlanTunnelStatus [ ] ={1,3,6,1,4,1,2076,120,2,2,1,1};
UINT4 FsMIDot1qFutureVlanTunnelStpPDUs [ ] ={1,3,6,1,4,1,2076,120,2,3,1,1};
UINT4 FsMIDot1qFutureVlanTunnelStpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,120,2,3,1,2};
UINT4 FsMIDot1qFutureVlanTunnelStpPDUsSent [ ] ={1,3,6,1,4,1,2076,120,2,3,1,3};
UINT4 FsMIDot1qFutureVlanTunnelGvrpPDUs [ ] ={1,3,6,1,4,1,2076,120,2,3,1,4};
UINT4 FsMIDot1qFutureVlanTunnelGvrpPDUsRecvd [ ] ={1,3,6,1,4,1,2076,120,2,3,1,5};
UINT4 FsMIDot1qFutureVlanTunnelGvrpPDUsSent [ ] ={1,3,6,1,4,1,2076,120,2,3,1,6};
UINT4 FsMIDot1qFutureVlanTunnelIgmpPkts [ ] ={1,3,6,1,4,1,2076,120,2,3,1,7};
UINT4 FsMIDot1qFutureVlanTunnelIgmpPktsRecvd [ ] ={1,3,6,1,4,1,2076,120,2,3,1,8};
UINT4 FsMIDot1qFutureVlanTunnelIgmpPktsSent [ ] ={1,3,6,1,4,1,2076,120,2,3,1,9};
UINT4 FsMIDot1qFutureVlanCounterRxUcast [ ] ={1,3,6,1,4,1,2076,120,1,6,1,1};
UINT4 FsMIDot1qFutureVlanCounterRxMcastBcast [ ] ={1,3,6,1,4,1,2076,120,1,6,1,2};
UINT4 FsMIDot1qFutureVlanCounterTxUnknUcast [ ] ={1,3,6,1,4,1,2076,120,1,6,1,3};
UINT4 FsMIDot1qFutureVlanCounterTxUcast [ ] ={1,3,6,1,4,1,2076,120,1,6,1,4};
UINT4 FsMIDot1qFutureVlanCounterTxBcast [ ] ={1,3,6,1,4,1,2076,120,1,6,1,5};
UINT4 FsMIDot1qFutureVlanCounterRxFrames [ ] ={1,3,6,1,4,1,2076,120,1,6,1,6};
UINT4 FsMIDot1qFutureVlanCounterRxBytes [ ] ={1,3,6,1,4,1,2076,120,1,6,1,7};
UINT4 FsMIDot1qFutureVlanCounterTxFrames [ ] ={1,3,6,1,4,1,2076,120,1,6,1,8};
UINT4 FsMIDot1qFutureVlanCounterTxBytes [ ] ={1,3,6,1,4,1,2076,120,1,6,1,9};
UINT4 FsMIDot1qFutureVlanCounterDiscardFrames [ ] ={1,3,6,1,4,1,2076,120,1,6,1,10};
UINT4 FsMIDot1qFutureVlanCounterDiscardBytes [ ] ={1,3,6,1,4,1,2076,120,1,6,1,11};
UINT4 FsMIDot1qFutureVlanCounterStatus [ ] ={1,3,6,1,4,1,2076,120,1,6,1,12};
UINT4 FsMIDot1qFutureVlanUnicastMacLimit [ ] ={1,3,6,1,4,1,2076,120,1,7,1,1};
UINT4 FsMIDot1qFutureVlanAdminMacLearningStatus [ ] ={1,3,6,1,4,1,2076,120,1,7,1,2};
UINT4 FsMIDot1qFutureVlanOperMacLearningStatus [ ] ={1,3,6,1,4,1,2076,120,1,7,1,3};
UINT4 FsMIDot1qFutureGarpGlobalTrace [ ] ={1,3,6,1,4,1,2076,120,1,8};
UINT4 FsMIDot1qFutureVlanOldTpFdbPort [ ] ={1,3,6,1,4,1,2076,120,1,9,1,1};
UINT4 FsMIDot1qFutureConnectionIdentifier [ ] ={1,3,6,1,4,1,2076,120,1,9,1,2};
UINT4 FsMIDot1qFutureVlanWildCardMacAddress [ ] ={1,3,6,1,4,1,2076,120,1,10,1,1};
UINT4 FsMIDot1qFutureVlanWildCardRowStatus [ ] ={1,3,6,1,4,1,2076,120,1,10,1,2};
UINT4 FsMIDot1qFutureVlanIsWildCardEgressPort [ ] ={1,3,6,1,4,1,2076,120,1,11,1,1};
UINT4 FsMIDot1qFutureStaticConnectionIdentifier [ ] ={1,3,6,1,4,1,2076,120,1,12,1,1};
UINT4 FsMIDot1qFutureVlanPortSubnetMapAddr [ ] ={1,3,6,1,4,1,2076,120,1,13,1,1};
UINT4 FsMIDot1qFutureVlanPortSubnetMapVid [ ] ={1,3,6,1,4,1,2076,120,1,13,1,2};
UINT4 FsMIDot1qFutureVlanPortSubnetMapARPOption [ ] ={1,3,6,1,4,1,2076,120,1,13,1,3};
UINT4 FsMIDot1qFutureVlanPortSubnetMapRowStatus [ ] ={1,3,6,1,4,1,2076,120,1,13,1,4};
UINT4 FsMIDot1qFutureVlanSwStatsEnabled [ ] ={1,3,6,1,4,1,2076,120,1,14};
UINT4 FsMIDot1qFutureStVlanType [ ] ={1,3,6,1,4,1,2076,120,1,15,1,1};
UINT4 FsMIDot1qFutureStVlanVid [ ] ={1,3,6,1,4,1,2076,120,1,15,1,2};
UINT4 FsMIDot1qFutureStVlanFdbFlush [ ] ={1,3,6,1,4,1,2076,120,1,15,1,3};
UINT4 FsMIDot1qFutureStVlanEgressEthertype [ ] ={1,3,6,1,4,1,2076,120,1,15,1,4};
UINT4 FsMIDot1qFutureVlanPortSubnetMapExtAddr [ ] =
{1,3,6,1,4,1,2076,120,1,16,1,1};
UINT4 FsMIDot1qFutureVlanPortSubnetMapExtMask [ ] =
{1,3,6,1,4,1,2076,120,1,16,1,2};
UINT4 FsMIDot1qFutureVlanPortSubnetMapExtVid [ ] =
{1,3,6,1,4,1,2076,120,1,16,1,3};
UINT4 FsMIDot1qFutureVlanPortSubnetMapExtARPOption [ ] =
{1,3,6,1,4,1,2076,120,1,16,1,4};
UINT4 FsMIDot1qFutureVlanPortSubnetMapExtRowStatus [ ] =
{1,3,6,1,4,1,2076,120,1,16,1,5};
UINT4 FsMIDot1qFuturePortVlanFdbFlush [ ] ={1,3,6,1,4,1,2076,120,1,17,1,1};
UINT4 FsMIDot1qFutureVlanLoopbackStatus [ ] ={1,3,6,1,4,1,2076,120,1,18,1,1};



tMbDbEntry fsmpvlMibEntry[]= {

{{10,FsMIDot1qFutureVlanGlobalTrace}, NULL, FsMIDot1qFutureVlanGlobalTraceGet, FsMIDot1qFutureVlanGlobalTraceSet, FsMIDot1qFutureVlanGlobalTraceTest, FsMIDot1qFutureVlanGlobalTraceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanContextId}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanStatus}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanStatusGet, FsMIDot1qFutureVlanStatusSet, FsMIDot1qFutureVlanStatusTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanMacBasedOnAllPorts}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanMacBasedOnAllPortsGet, FsMIDot1qFutureVlanMacBasedOnAllPortsSet, FsMIDot1qFutureVlanMacBasedOnAllPortsTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortProtoBasedOnAllPorts}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanPortProtoBasedOnAllPortsGet, FsMIDot1qFutureVlanPortProtoBasedOnAllPortsSet, FsMIDot1qFutureVlanPortProtoBasedOnAllPortsTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanShutdownStatus}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanShutdownStatusGet, FsMIDot1qFutureVlanShutdownStatusSet, FsMIDot1qFutureVlanShutdownStatusTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, "2"},

{{12,FsMIDot1qFutureGarpShutdownStatus}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureGarpShutdownStatusGet, FsMIDot1qFutureGarpShutdownStatusSet, FsMIDot1qFutureGarpShutdownStatusTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanDebug}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanDebugGet, FsMIDot1qFutureVlanDebugSet, FsMIDot1qFutureVlanDebugTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanLearningMode}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanLearningModeGet, FsMIDot1qFutureVlanLearningModeSet, FsMIDot1qFutureVlanLearningModeTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, "1"},

{{12,FsMIDot1qFutureVlanHybridTypeDefault}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanHybridTypeDefaultGet, FsMIDot1qFutureVlanHybridTypeDefaultSet, FsMIDot1qFutureVlanHybridTypeDefaultTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, "1"},

{{12,FsMIDot1qFutureVlanOperStatus}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureGvrpOperStatus}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureGvrpOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureGmrpOperStatus}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureGmrpOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanContextName}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanContextNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureGarpDebug}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureGarpDebugGet, FsMIDot1qFutureGarpDebugSet, FsMIDot1qFutureGarpDebugTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, "0"},

{{12,FsMIDot1qFutureUnicastMacLearningLimit}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureUnicastMacLearningLimitGet, FsMIDot1qFutureUnicastMacLearningLimitSet, FsMIDot1qFutureUnicastMacLearningLimitTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureBaseBridgeMode}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureBaseBridgeModeGet, FsMIDot1qFutureBaseBridgeModeSet, FsMIDot1qFutureBaseBridgeModeTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanSubnetBasedOnAllPorts}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanSubnetBasedOnAllPortsGet, FsMIDot1qFutureVlanSubnetBasedOnAllPortsSet, FsMIDot1qFutureVlanSubnetBasedOnAllPortsTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanGlobalMacLearningStatus}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanGlobalMacLearningStatusGet, FsMIDot1qFutureVlanGlobalMacLearningStatusSet, FsMIDot1qFutureVlanGlobalMacLearningStatusTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, "1"},

{{12,FsMIDot1qFutureVlanApplyEnhancedFilteringCriteria}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanApplyEnhancedFilteringCriteriaGet, FsMIDot1qFutureVlanApplyEnhancedFilteringCriteriaSet, FsMIDot1qFutureVlanApplyEnhancedFilteringCriteriaTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, "1"},

{{12,FsMIDot1qFutureVlanGlobalsFdbFlush}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanGlobalsFdbFlushGet, FsMIDot1qFutureVlanGlobalsFdbFlushSet, FsMIDot1qFutureVlanGlobalsFdbFlushTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, "2"},

{{12,FsMIDot1qFutureVlanUserDefinedTPID}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanUserDefinedTPIDGet, FsMIDot1qFutureVlanUserDefinedTPIDSet, FsMIDot1qFutureVlanUserDefinedTPIDTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, "0"},

{{12,FsMIDot1qFutureVlanRemoteFdbFlush}, GetNextIndexFsMIDot1qFutureVlanGlobalsTable, FsMIDot1qFutureVlanRemoteFdbFlushGet, FsMIDot1qFutureVlanRemoteFdbFlushSet, FsMIDot1qFutureVlanRemoteFdbFlushTest, FsMIDot1qFutureVlanGlobalsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanGlobalsTableINDEX, 1, 0, 0, "2"},

{{12,FsMIDot1qFutureVlanPort}, GetNextIndexFsMIDot1qFutureVlanPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortType}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortTypeGet, FsMIDot1qFutureVlanPortTypeSet, FsMIDot1qFutureVlanPortTypeTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, "3"},

{{12,FsMIDot1qFutureVlanPortMacBasedClassification}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortMacBasedClassificationGet, FsMIDot1qFutureVlanPortMacBasedClassificationSet, FsMIDot1qFutureVlanPortMacBasedClassificationTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortPortProtoBasedClassification}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortPortProtoBasedClassificationGet, FsMIDot1qFutureVlanPortPortProtoBasedClassificationSet, FsMIDot1qFutureVlanPortPortProtoBasedClassificationTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanFilteringUtilityCriteria}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanFilteringUtilityCriteriaGet, FsMIDot1qFutureVlanFilteringUtilityCriteriaSet, FsMIDot1qFutureVlanFilteringUtilityCriteriaTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMIDot1qFutureVlanPortProtected}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortProtectedGet, FsMIDot1qFutureVlanPortProtectedSet, FsMIDot1qFutureVlanPortProtectedTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIDot1qFutureVlanPortSubnetBasedClassification}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortSubnetBasedClassificationGet, FsMIDot1qFutureVlanPortSubnetBasedClassificationSet, FsMIDot1qFutureVlanPortSubnetBasedClassificationTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},
{{12,FsMIDot1qFutureVlanPortUnicastMacLearning}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortUnicastMacLearningGet, FsMIDot1qFutureVlanPortUnicastMacLearningSet, FsMIDot1qFutureVlanPortUnicastMacLearningTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMIDot1qFutureVlanPortGmrpJoinEmptyTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpJoinEmptyTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpJoinEmptyRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpJoinEmptyRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpJoinInTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpJoinInTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpJoinInRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpJoinInRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpLeaveInTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpLeaveInTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpLeaveInRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpLeaveInRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpEmptyTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpEmptyTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpEmptyRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpEmptyRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpLeaveAllTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpLeaveAllTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpLeaveAllRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpLeaveAllRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGmrpDiscardCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGmrpDiscardCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpJoinEmptyTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpJoinEmptyTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpJoinEmptyRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpJoinEmptyRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpJoinInTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpJoinInTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpJoinInRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpJoinInRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpLeaveInTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpLeaveInTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpLeaveInRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpLeaveInRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpEmptyTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpEmptyTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpEmptyRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpEmptyRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpLeaveAllTxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpLeaveAllTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpLeaveAllRxCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpLeaveAllRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortGvrpDiscardCount}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortGvrpDiscardCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortFdbFlush}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortFdbFlushGet, FsMIDot1qFutureVlanPortFdbFlushSet, FsMIDot1qFutureVlanPortFdbFlushTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIDot1qFutureVlanPortIngressEtherType}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortIngressEtherTypeGet, FsMIDot1qFutureVlanPortIngressEtherTypeSet, FsMIDot1qFutureVlanPortIngressEtherTypeTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortEgressEtherType}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortEgressEtherTypeGet, FsMIDot1qFutureVlanPortEgressEtherTypeSet, FsMIDot1qFutureVlanPortEgressEtherTypeTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortEgressTPIDType}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortEgressTPIDTypeGet, FsMIDot1qFutureVlanPortEgressTPIDTypeSet, FsMIDot1qFutureVlanPortEgressTPIDTypeTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, "1"},

{{12,FsMIDot1qFutureVlanPortAllowableTPID1}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortAllowableTPID1Get, FsMIDot1qFutureVlanPortAllowableTPID1Set, FsMIDot1qFutureVlanPortAllowableTPID1Test, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, "0"},

{{12,FsMIDot1qFutureVlanPortAllowableTPID2}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortAllowableTPID2Get, FsMIDot1qFutureVlanPortAllowableTPID2Set, FsMIDot1qFutureVlanPortAllowableTPID2Test, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, "0"},

{{12,FsMIDot1qFutureVlanPortAllowableTPID3}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortAllowableTPID3Get, FsMIDot1qFutureVlanPortAllowableTPID3Set, FsMIDot1qFutureVlanPortAllowableTPID3Test, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, "0"},
{{12,FsMIDot1qFutureVlanPortClearGarpStats}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFutureVlanPortClearGarpStatsGet, FsMIDot1qFutureVlanPortClearGarpStatsSet, FsMIDot1qFutureVlanPortClearGarpStatsTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, "2"},

{{12,FsMIDot1qFuturePortPacketReflectionStatus}, GetNextIndexFsMIDot1qFutureVlanPortTable, FsMIDot1qFuturePortPacketReflectionStatusGet, FsMIDot1qFuturePortPacketReflectionStatusSet, FsMIDot1qFuturePortPacketReflectionStatusTest, FsMIDot1qFutureVlanPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortTableINDEX, 1, 0, 0, "2"},  

{{12,FsMIDot1qFutureVlanPortMacMapAddr}, GetNextIndexFsMIDot1qFutureVlanPortMacMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIDot1qFutureVlanPortMacMapTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortMacMapVid}, GetNextIndexFsMIDot1qFutureVlanPortMacMapTable, FsMIDot1qFutureVlanPortMacMapVidGet, FsMIDot1qFutureVlanPortMacMapVidSet, FsMIDot1qFutureVlanPortMacMapVidTest, FsMIDot1qFutureVlanPortMacMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortMacMapTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortMacMapName}, GetNextIndexFsMIDot1qFutureVlanPortMacMapTable, FsMIDot1qFutureVlanPortMacMapNameGet, FsMIDot1qFutureVlanPortMacMapNameSet, FsMIDot1qFutureVlanPortMacMapNameTest, FsMIDot1qFutureVlanPortMacMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIDot1qFutureVlanPortMacMapTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortMacMapMcastBcastOption}, GetNextIndexFsMIDot1qFutureVlanPortMacMapTable, FsMIDot1qFutureVlanPortMacMapMcastBcastOptionGet, FsMIDot1qFutureVlanPortMacMapMcastBcastOptionSet, FsMIDot1qFutureVlanPortMacMapMcastBcastOptionTest, FsMIDot1qFutureVlanPortMacMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortMacMapTableINDEX, 2, 0, 0, "1"},

{{12,FsMIDot1qFutureVlanPortMacMapRowStatus}, GetNextIndexFsMIDot1qFutureVlanPortMacMapTable, FsMIDot1qFutureVlanPortMacMapRowStatusGet, FsMIDot1qFutureVlanPortMacMapRowStatusSet, FsMIDot1qFutureVlanPortMacMapRowStatusTest, FsMIDot1qFutureVlanPortMacMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortMacMapTableINDEX, 2, 0, 1, NULL},

{{12,FsMIDot1qFutureVlanIndex}, GetNextIndexFsMIDot1qFutureVlanFidMapTable, FsMIDot1qFutureVlanIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIDot1qFutureVlanFidMapTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanFid}, GetNextIndexFsMIDot1qFutureVlanFidMapTable, FsMIDot1qFutureVlanFidGet, FsMIDot1qFutureVlanFidSet, FsMIDot1qFutureVlanFidTest, FsMIDot1qFutureVlanFidMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIDot1qFutureVlanFidMapTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterRxUcast}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterRxUcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterRxMcastBcast}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterRxMcastBcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterTxUnknUcast}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterTxUnknUcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterTxUcast}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterTxUcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterTxBcast}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterTxBcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterRxFrames}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterRxFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterRxBytes}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterRxBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterTxFrames}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterTxFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterTxBytes}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterTxBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterDiscardFrames}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterDiscardFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterDiscardBytes}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterDiscardBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanCounterStatus}, GetNextIndexFsMIDot1qFutureVlanCounterTable, FsMIDot1qFutureVlanCounterStatusGet, FsMIDot1qFutureVlanCounterStatusSet, FsMIDot1qFutureVlanCounterStatusTest, FsMIDot1qFutureVlanCounterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanCounterTableINDEX, 2, 0, 0, "2"},

{{12,FsMIDot1qFutureVlanUnicastMacLimit}, GetNextIndexFsMIDot1qFutureVlanUnicastMacControlTable, FsMIDot1qFutureVlanUnicastMacLimitGet, FsMIDot1qFutureVlanUnicastMacLimitSet, FsMIDot1qFutureVlanUnicastMacLimitTest, FsMIDot1qFutureVlanUnicastMacControlTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIDot1qFutureVlanUnicastMacControlTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanAdminMacLearningStatus}, GetNextIndexFsMIDot1qFutureVlanUnicastMacControlTable, FsMIDot1qFutureVlanAdminMacLearningStatusGet, FsMIDot1qFutureVlanAdminMacLearningStatusSet, FsMIDot1qFutureVlanAdminMacLearningStatusTest, FsMIDot1qFutureVlanUnicastMacControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanUnicastMacControlTableINDEX, 2, 0, 0, "3"},

{{12,FsMIDot1qFutureVlanOperMacLearningStatus}, GetNextIndexFsMIDot1qFutureVlanUnicastMacControlTable, FsMIDot1qFutureVlanOperMacLearningStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1qFutureVlanUnicastMacControlTableINDEX, 2, 0, 0, NULL},

{{10,FsMIDot1qFutureGarpGlobalTrace}, NULL, FsMIDot1qFutureGarpGlobalTraceGet, FsMIDot1qFutureGarpGlobalTraceSet, FsMIDot1qFutureGarpGlobalTraceTest, FsMIDot1qFutureGarpGlobalTraceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanOldTpFdbPort}, GetNextIndexFsMIDot1qFutureVlanTpFdbTable, FsMIDot1qFutureVlanOldTpFdbPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIDot1qFutureVlanTpFdbTableINDEX, 3, 0, 0, NULL},

{{12,FsMIDot1qFutureConnectionIdentifier}, GetNextIndexFsMIDot1qFutureVlanTpFdbTable, FsMIDot1qFutureConnectionIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMIDot1qFutureVlanTpFdbTableINDEX, 3, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanWildCardMacAddress}, GetNextIndexFsMIDot1qFutureVlanWildCardTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMIDot1qFutureVlanWildCardTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanWildCardRowStatus}, GetNextIndexFsMIDot1qFutureVlanWildCardTable, FsMIDot1qFutureVlanWildCardRowStatusGet, FsMIDot1qFutureVlanWildCardRowStatusSet, FsMIDot1qFutureVlanWildCardRowStatusTest, FsMIDot1qFutureVlanWildCardTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanWildCardTableINDEX, 2, 0, 1, NULL},

{{12,FsMIDot1qFutureVlanIsWildCardEgressPort}, GetNextIndexFsMIDot1qFutureVlanWildCardPortTable, FsMIDot1qFutureVlanIsWildCardEgressPortGet, FsMIDot1qFutureVlanIsWildCardEgressPortSet, FsMIDot1qFutureVlanIsWildCardEgressPortTest, FsMIDot1qFutureVlanWildCardPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanWildCardPortTableINDEX, 3, 0, 0, NULL},

{{12,FsMIDot1qFutureStaticConnectionIdentifier}, GetNextIndexFsMIDot1qFutureStaticUnicastExtnTable, FsMIDot1qFutureStaticConnectionIdentifierGet, FsMIDot1qFutureStaticConnectionIdentifierSet, FsMIDot1qFutureStaticConnectionIdentifierTest, FsMIDot1qFutureStaticUnicastExtnTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMIDot1qFutureStaticUnicastExtnTableINDEX, 4, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortSubnetMapAddr}, GetNextIndexFsMIDot1qFutureVlanPortSubnetMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIDot1qFutureVlanPortSubnetMapTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortSubnetMapVid}, GetNextIndexFsMIDot1qFutureVlanPortSubnetMapTable, FsMIDot1qFutureVlanPortSubnetMapVidGet, FsMIDot1qFutureVlanPortSubnetMapVidSet, FsMIDot1qFutureVlanPortSubnetMapVidTest, FsMIDot1qFutureVlanPortSubnetMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortSubnetMapTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortSubnetMapARPOption}, GetNextIndexFsMIDot1qFutureVlanPortSubnetMapTable, FsMIDot1qFutureVlanPortSubnetMapARPOptionGet, FsMIDot1qFutureVlanPortSubnetMapARPOptionSet, FsMIDot1qFutureVlanPortSubnetMapARPOptionTest, FsMIDot1qFutureVlanPortSubnetMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortSubnetMapTableINDEX, 2, 0, 0, "1"},

{{12,FsMIDot1qFutureVlanPortSubnetMapRowStatus}, GetNextIndexFsMIDot1qFutureVlanPortSubnetMapTable, FsMIDot1qFutureVlanPortSubnetMapRowStatusGet, FsMIDot1qFutureVlanPortSubnetMapRowStatusSet, FsMIDot1qFutureVlanPortSubnetMapRowStatusTest, FsMIDot1qFutureVlanPortSubnetMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanPortSubnetMapTableINDEX, 2, 0, 1, NULL},

{{10,FsMIDot1qFutureVlanSwStatsEnabled}, NULL, FsMIDot1qFutureVlanSwStatsEnabledGet, FsMIDot1qFutureVlanSwStatsEnabledSet, FsMIDot1qFutureVlanSwStatsEnabledTest, FsMIDot1qFutureVlanSwStatsEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMIDot1qFutureStVlanType}, GetNextIndexFsMIDot1qFutureStVlanExtTable, FsMIDot1qFutureStVlanTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIDot1qFutureStVlanExtTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureStVlanVid}, GetNextIndexFsMIDot1qFutureStVlanExtTable, FsMIDot1qFutureStVlanVidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIDot1qFutureStVlanExtTableINDEX, 2, 0, 0, NULL},

{{12,FsMIDot1qFutureStVlanFdbFlush}, GetNextIndexFsMIDot1qFutureStVlanExtTable, FsMIDot1qFutureStVlanFdbFlushGet, FsMIDot1qFutureStVlanFdbFlushSet, FsMIDot1qFutureStVlanFdbFlushTest, FsMIDot1qFutureStVlanExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureStVlanExtTableINDEX, 2, 0, 0, "2"},

{{12,FsMIDot1qFutureStVlanEgressEthertype}, GetNextIndexFsMIDot1qFutureStVlanExtTable, FsMIDot1qFutureStVlanEgressEthertypeGet, FsMIDot1qFutureStVlanEgressEthertypeSet, FsMIDot1qFutureStVlanEgressEthertypeTest, FsMIDot1qFutureStVlanExtTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1qFutureStVlanExtTableINDEX, 2, 0, 0, "33024"},

{{12,FsMIDot1qFutureVlanPortSubnetMapExtAddr},
GetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable, NULL, NULL, NULL, NULL,
SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS,
FsMIDot1qFutureVlanPortSubnetMapExtTableINDEX, 3, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortSubnetMapExtMask},
GetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable, NULL, NULL, NULL, NULL,
SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS,
FsMIDot1qFutureVlanPortSubnetMapExtTableINDEX, 3, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortSubnetMapExtVid},
GetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable,
FsMIDot1qFutureVlanPortSubnetMapExtVidGet,
FsMIDot1qFutureVlanPortSubnetMapExtVidSet,
FsMIDot1qFutureVlanPortSubnetMapExtVidTest,
FsMIDot1qFutureVlanPortSubnetMapExtTableDep,
SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
FsMIDot1qFutureVlanPortSubnetMapExtTableINDEX, 3, 0, 0, NULL},

{{12,FsMIDot1qFutureVlanPortSubnetMapExtARPOption},
GetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable,
FsMIDot1qFutureVlanPortSubnetMapExtARPOptionGet,
FsMIDot1qFutureVlanPortSubnetMapExtARPOptionSet,
FsMIDot1qFutureVlanPortSubnetMapExtARPOptionTest,
FsMIDot1qFutureVlanPortSubnetMapExtTableDep, SNMP_DATA_TYPE_INTEGER,
SNMP_READWRITE, FsMIDot1qFutureVlanPortSubnetMapExtTableINDEX, 3, 0, 0, "1"},

{{12,FsMIDot1qFutureVlanPortSubnetMapExtRowStatus},
GetNextIndexFsMIDot1qFutureVlanPortSubnetMapExtTable,
FsMIDot1qFutureVlanPortSubnetMapExtRowStatusGet,
FsMIDot1qFutureVlanPortSubnetMapExtRowStatusSet,
FsMIDot1qFutureVlanPortSubnetMapExtRowStatusTest,
FsMIDot1qFutureVlanPortSubnetMapExtTableDep, SNMP_DATA_TYPE_INTEGER,
SNMP_READWRITE, FsMIDot1qFutureVlanPortSubnetMapExtTableINDEX, 3, 0, 1, NULL},

{{12,FsMIDot1qFuturePortVlanFdbFlush}, GetNextIndexFsMIDot1qFuturePortVlanExtTable, FsMIDot1qFuturePortVlanFdbFlushGet, FsMIDot1qFuturePortVlanFdbFlushSet, FsMIDot1qFuturePortVlanFdbFlushTest, FsMIDot1qFuturePortVlanExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFuturePortVlanExtTableINDEX, 3, 0, 0, "2"},

{{12,FsMIDot1qFutureVlanLoopbackStatus}, GetNextIndexFsMIDot1qFutureVlanLoopbackTable, FsMIDot1qFutureVlanLoopbackStatusGet, FsMIDot1qFutureVlanLoopbackStatusSet, FsMIDot1qFutureVlanLoopbackStatusTest, FsMIDot1qFutureVlanLoopbackTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanLoopbackTableINDEX, 2, 0, 0, "2"},

{{12,FsMIDot1qFutureVlanBridgeMode}, GetNextIndexFsMIDot1qFutureVlanTunnelConfigTable, FsMIDot1qFutureVlanBridgeModeGet, FsMIDot1qFutureVlanBridgeModeSet, FsMIDot1qFutureVlanBridgeModeTest, FsMIDot1qFutureVlanTunnelConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanTunnelConfigTableINDEX, 1, 1, 0, NULL},

{{12,FsMIDot1qFutureVlanTunnelBpduPri}, GetNextIndexFsMIDot1qFutureVlanTunnelConfigTable, FsMIDot1qFutureVlanTunnelBpduPriGet, FsMIDot1qFutureVlanTunnelBpduPriSet, FsMIDot1qFutureVlanTunnelBpduPriTest, FsMIDot1qFutureVlanTunnelConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIDot1qFutureVlanTunnelConfigTableINDEX, 1, 1, 0, "7"},

{{12,FsMIDot1qFutureVlanTunnelStatus}, GetNextIndexFsMIDot1qFutureVlanTunnelTable, FsMIDot1qFutureVlanTunnelStatusGet, FsMIDot1qFutureVlanTunnelStatusSet, FsMIDot1qFutureVlanTunnelStatusTest, FsMIDot1qFutureVlanTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanTunnelTableINDEX, 1, 1, 0, "2"},

{{12,FsMIDot1qFutureVlanTunnelStpPDUs}, GetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable, FsMIDot1qFutureVlanTunnelStpPDUsGet, FsMIDot1qFutureVlanTunnelStpPDUsSet, FsMIDot1qFutureVlanTunnelStpPDUsTest, FsMIDot1qFutureVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, "2"},

{{12,FsMIDot1qFutureVlanTunnelStpPDUsRecvd}, GetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable, FsMIDot1qFutureVlanTunnelStpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsMIDot1qFutureVlanTunnelStpPDUsSent}, GetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable, FsMIDot1qFutureVlanTunnelStpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsMIDot1qFutureVlanTunnelGvrpPDUs}, GetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable, FsMIDot1qFutureVlanTunnelGvrpPDUsGet, FsMIDot1qFutureVlanTunnelGvrpPDUsSet, FsMIDot1qFutureVlanTunnelGvrpPDUsTest, FsMIDot1qFutureVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, "1"},

{{12,FsMIDot1qFutureVlanTunnelGvrpPDUsRecvd}, GetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable, FsMIDot1qFutureVlanTunnelGvrpPDUsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsMIDot1qFutureVlanTunnelGvrpPDUsSent}, GetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable, FsMIDot1qFutureVlanTunnelGvrpPDUsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsMIDot1qFutureVlanTunnelIgmpPkts}, GetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable, FsMIDot1qFutureVlanTunnelIgmpPktsGet, FsMIDot1qFutureVlanTunnelIgmpPktsSet, FsMIDot1qFutureVlanTunnelIgmpPktsTest, FsMIDot1qFutureVlanTunnelProtocolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIDot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, "1"},

{{12,FsMIDot1qFutureVlanTunnelIgmpPktsRecvd}, GetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable, FsMIDot1qFutureVlanTunnelIgmpPktsRecvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},

{{12,FsMIDot1qFutureVlanTunnelIgmpPktsSent}, GetNextIndexFsMIDot1qFutureVlanTunnelProtocolTable, FsMIDot1qFutureVlanTunnelIgmpPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIDot1qFutureVlanTunnelProtocolTableINDEX, 1, 1, 0, NULL},
};
tMibData fsmpvlEntry = { 122, fsmpvlMibEntry };

#endif /* _FSMPVLDB_H */

