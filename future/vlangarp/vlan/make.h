#####################################################################
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved                    ####
# $Id: make.h,v 1.9 2015/10/09 06:19:48 siva Exp $#
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved          ####
#####################################################################
##|                                                               |##
##|###############################################################|##
##|                                                               |##
##|    FILE NAME               ::  make.h                         |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.      |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  VLAN                           |##
##|                                                               |##
##|    MODULE NAME             ::  VLAN                           |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  ANY                            |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  26 Mar 2002                    |##
##|                                                               |##
##|    DESCRIPTION             ::  Include file for the VLAN      |##
##|                                Makefile.                      |## 
##|                                                               |##
##|###############################################################|##


###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)
TOTAL_OPNS += -DOPTIMIZED_HW_AUDIT_WANTED

############################################################################
#                         Directories                                      #
############################################################################

GARP_BASE_DIR   = ${BASE_DIR}/vlangarp/garp
VLAN_BASE_DIR = ${BASE_DIR}/vlangarp/vlan
VLAN_SRC_DIR  = ${VLAN_BASE_DIR}/src
VLAN_INC_DIR  = ${VLAN_BASE_DIR}/inc
VLAN_OBJ_DIR  = ${VLAN_BASE_DIR}/obj
GARP_INCD     = ${GARP_BASE_DIR}/inc
VLAN_TEST_DIR = ${VLAN_BASE_DIR}/test


############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${VLAN_INC_DIR} -I${GARP_INCD} -I${VLAN_TEST_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS} 



#############################################################################
