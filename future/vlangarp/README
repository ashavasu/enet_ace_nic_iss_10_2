Copyright (C) 2006 Aricent Inc . All Rights Reserved
--------------------------------------------------


Introduction
============

AricentVLAN is a portable implementation of the VLAN and GARP features. It
provides complete management capabilities using SNMP and CLI. 

AricentVLAN conforms to the IEEE 802.1Q 1998 Edition, IEEE 802.1v 2001 Edition,
IEEE 802.1D 1998 Edition. It is extended to support Q in Q (VLAN Stacking)
from REL2-1-0-0. 
Support for default group service functionality code is controlled by the
compilation switch GARP_EXTENDED_FILTER. This switch is enabled by default.   

The software Release comprises of 2 modules:

1) VLAN Module
2) GARP Module


Contents
========

The contents of this distribution are as follows:

inc/              - Header files
src/              - Source files
Makefile          - Module makefile 
make.h            - This file contains variables required for building  
                    the module.
../mibs/          - MIB files.
   stdvlan.mib    - Standard VLAN Mib - RFC 2674 (Q-BRIDGE-MIB).
   stdbrgext.mib  - Standard Bridge Extension Mib - RFC 2674 (P-BRIDGE-MIB).
   fsvlan.mib     - Proprietary Mib for VLAN.
   fsdot1ad.mib
   fspb.mib
   fsvlnext.mib
   fsm1ad.mib
   fsmpb.mib
   fsmpvlan.mib
   fsmsvlan.mib
   fsmvlext.mib



The files in the following directories are needed for building the module.
o inc
o LR
o fsap2
o util
o l2iwf

Note:
=====

1) If AricentSNMP is NOT used, then the following files (if provided in this
   release) must NOT be used,

   o VLAN Module

      src/stdbrgwr.c
      src/stdvlawr.c
      src/fsvlanwr.c
      inc/stdbrgwr.h
      inc/stdvlawr.h
      inc/fsvlanwr.h
      inc/stdbrgdb.h
      inc/stdvladb.h
      inc/fsvlandb.h

2) To enable the multiple instantiation feature, the switch MI should be
   enabled in ../LR/make.h during compilation.

3) Following list of files ( if provided in this release ) are for future 
   implementations of Q bridge, these are not currently used 
   inc/stqbrgdb.h
   inc/stqbrglw.h
   inc/stqbrgwr.h
   src/stqbrglw.c
   src/stqbrgwr.c
  
Important Documents
===================

Please refer to the following documents for further information-

- VLANfs.pdf         - Feature Sheet
- VLANps.pdf         - Product Specification
- VLANpo.pdf         - Porting Guide
- VERSION            - Version of modules to be compiled to generate an executable   
- RELEASE            - Release notes

How to read documents
---------------------

   The product source code documents are best viewed using Acrobat Reader ver.
   6.x. To download ver. 6.x use the link,

   http://www.adobe.com/products/acrobat/readstep2.html.

          
END OF README
