/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: garptrc.h,v 1.8 2013/12/18 12:03:07 siva Exp $
 *
 * Description: This file contains VLAN CLI command constants, error
 *              strings and function prototypes.
 *
 *******************************************************************/

#ifndef _GARPTRC_H
#define _GARPTRC_H

/* Trace and debug flags */
#define   GARP_TRC_FLAG      (gpGarpContextInfo)->u4GarpTrcOption

#define   GARP_GBL_TRC_FLAG    gu1GarpGblTraceOption
#define  GARP_STR(x)              "%s"x, GARP_CURR_CONTEXT_STR()

extern UINT4 gu4GarpTrcLvl;
#define GARP_TRC_LVL        gu4GarpTrcLvl

/*Garp Trace definitions*/
#ifdef TRACE_WANTED

#define GARP_GLOBAL_TRC    GarpGblTrace
#define GARP_TRC(ModTrc, Mask, ModuleName, Fmt) \
    if (((GARP_TRC_FLAG & ModTrc) != 0)){\
        UtlTrcLog (GARP_TRC_FLAG, Mask, ModuleName, GARP_STR(Fmt)); \
    }
#define GARP_TRC_ARG1(ModTrc, Mask, ModuleName, Fmt, Arg1) \
   if (((GARP_TRC_FLAG & ModTrc) != 0)){\
    UtlTrcLog (GARP_TRC_FLAG, Mask,ModuleName, GARP_STR(Fmt), Arg1); \
}
#define GARP_TRC_ARG2(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2) \
   if (((GARP_TRC_FLAG & ModTrc) != 0)){\
    UtlTrcLog (GARP_TRC_FLAG, Mask,ModuleName, GARP_STR(Fmt), Arg1, Arg2); \
}
#define GARP_TRC_ARG3(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3) \
   if (((GARP_TRC_FLAG & ModTrc) != 0)){\
    UtlTrcLog (u4DbgVar, Mask,ModuleName, GARP_STR(Fmt), Arg1, Arg2, Arg3); \
}

#else
#define GARP_GLOBAL_TRC    GarpGblTrace 
#define GARP_TRC(ModTrc, Mask, ModuleName, Fmt) \
{\
   UNUSED_PARAM (ModTrc);\
   UNUSED_PARAM (ModuleName);\
   UNUSED_PARAM (Mask);\
   UNUSED_PARAM (Fmt);\
}
#define GARP_TRC_ARG1(ModTrc, Mask, ModuleName, Fmt, Arg1)\
{\
   UNUSED_PARAM (ModTrc);\
   UNUSED_PARAM (ModuleName);\
   UNUSED_PARAM (Mask);\
   UNUSED_PARAM (Fmt);\
   UNUSED_PARAM (Arg1);\
}
#define GARP_TRC_ARG2(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2)\
{\
   UNUSED_PARAM (ModTrc);\
   UNUSED_PARAM (ModuleName);\
   UNUSED_PARAM (Mask);\
   UNUSED_PARAM (Fmt);\
   UNUSED_PARAM (Arg1);\
   UNUSED_PARAM (Arg2);\
}
#define GARP_TRC_ARG3(ModTrc, Mask, ModuleName, Fmt, Arg1, Arg2, Arg3)\
{\
   UNUSED_PARAM (ModTrc);\
   UNUSED_PARAM (ModuleName);\
   UNUSED_PARAM (Mask);\
   UNUSED_PARAM (Fmt);\
   UNUSED_PARAM (Arg1);\
   UNUSED_PARAM (Arg2);\
   UNUSED_PARAM (Arg3);\
}
#endif

#ifdef TRACE_WANTED
#define GARP_PRINT_ATTRIBUTE(pAttr)     GarpPrintAttribute ((pAttr))
#define GARP_PRINT_MAC_ADDR(u4ModId, pMacAddr)   \
        GarpPrintMacAddr (u4ModId, (pMacAddr))
#define GARP_PRINT_PORT_LIST(u4ModId, PortList)  \
        GarpPrintPortList (u4ModId, (PortList), GARP_INVALID_PORT)
#else
#define GARP_PRINT_ATTRIBUTE(pAttr)
#define GARP_PRINT_MAC_ADDR(u4ModId, pMacAddr) 
#define GARP_PRINT_PORT_LIST(u4ModId, PortList)
#endif
#endif
