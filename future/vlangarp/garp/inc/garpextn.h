/* $Id: garpextn.h,v 1.19 2016/08/03 09:42:52 siva Exp $              */
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : garpextn.h                                  */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : GARP                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains the extern declaration   */
/*                          of the Global variables.                    */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#ifndef _GARPEXTN_H
#define _GARPEXTN_H

/*-------------- Extern Declarations of Global Variables -----------*/
extern tGarpContextInfo     *gpGarpContextInfo; 
extern tGarpContextInfo     *gapGarpContextInfo[]; 
extern tRBTree              gGarpPortTable; /* IfIndex based port RbTree */
extern UINT1                gau1GarpShutDownStatus[]; 
extern UINT1                gau1GvrpStatus[]; 
extern UINT1                gau1GmrpStatus[]; 
extern tGarpQMsg            *gapGarpRemapQMsg[]; 

extern tOsixTaskId          gGarpTaskId;
extern tOsixQId             gGarpQId;
extern tOsixQId             gGarpCfgQId;
extern tTimerListId         gGarpTmrListId; 
extern tGarpTimer           gGarpVlanTimer;
extern tGarpPoolIds         gGarpPoolIds;   
extern UINT1                gu1GarpIsInitComplete;
extern UINT1                gau1GarpBitMaskMap [];
extern tGarpAppSemEntry gaGarpAppSem [GARP_MAX_APP_EVENTS][GARP_MAX_APP_STATES];
extern tGarpRegSemEntry gaGarpRegSem [GARP_MAX_REG_EVENTS][GARP_MAX_REG_STATES];
extern tGarpNodeStatus      gGarpNodeStatus; 
extern UINT1                gu1GarpStandbyToActive;
extern tMacAddr            gGvrpAddr;
extern tMacAddr            gGmrpAddr;
extern tMacAddr            gCustomerGvrpAddr;
extern tMacAddr            gProviderGvrpAddr;
/* HITLESS RESTART */
extern UINT1               gu1StdyStReqRcvd;
extern VOID
GarpGblTrace (UINT4 u4ContextId, UINT4 u4ModeTrc ,UINT4 u4Mask, const char *ModuleName, const char*, ...);
extern tOsixSemId           gGarpSemId;
extern UINT1               gu1GarpGblTraceOption;

extern UINT4 gu4GarpCliContext;
extern UINT2                gu2ConfPorts[];
#endif
