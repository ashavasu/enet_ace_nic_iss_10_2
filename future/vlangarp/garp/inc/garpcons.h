/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: garpcons.h,v 1.35 2016/08/20 09:41:02 siva Exp $
 *
 * Description: This file contains the constant definitions.
 *
 *******************************************************************/

#ifndef _GARPCONS_H
#define _GARPCONS_H


/*      
 * The following macros define the length of the different attributes propagated
 * by GARP  
 */    
#define GARP_DEF_GROUP_ATTR_LEN    1
#define GARP_VLAN_ATTR_LEN         2
#define GARP_MAC_ATTR_LEN          6

#define GARP_MAC_ADDR_LEN 6
#define GARP_MAX_ATTR_LEN 6
#define GARP_NUM_ATTR_TYPES       2

#define GARP_ATTR_GVRP_TYPE       1
#define GARP_ATTR_GMRP_TYPE       2

#define GARP_SYS_BUFF             1
#define GARP_PORT_BUFF                  2
#define GARP_ATTR_BUFF                  3
#define GARP_GIP_BUFF                   4
#define GARP_HASH_TABLE_GVRP_BUFF       5
#define GARP_HASH_TABLE_GMRP_BUFF       15 
#define GARP_QMSG_BUFF             6
#define GARP_BULK_QMSG_BUFF        7
#define GARP_VLAN_MAP_MSG_BUFF     8
#define GARP_REMAP_ATTR_BUFF       9
#define GARP_APPGIP_HASH_BUFF      10
#define GARP_CONTEXT_INFO          11
#define GARP_GLOBPORT_ENTRY        12
#define GARP_SI_CONTEXT_INFO       13
#define GARP_REMAP_TXBUFF_INFO     14
#define GARP_PORT_STAT_BUFF     16
#define GARP_MAX_PORTS              (VLAN_MAX_PORTS - SYS_DEF_MAX_SBP_IFACES)   
#define GARP_MAX_GVRP_GIP_ENT_PER_PORT AST_MAX_MST_INSTANCES /* Number of STP 
         Context per Trunk Port
       */
#define GARP_MAX_GVRP_GIP_ENTRIES ((GARP_MAX_GVRP_GIP_ENT_PER_PORT) \
                     * (BRG_MAX_PHY_PLUS_LOG_PORTS))

/* If MRP = YES, then there is a split up of 
 * attributes between MRP and GARP. If MRP = NO then 
 * this MACRO should be defined as VLAN_DEV_MAX_NUM_VLAN */
#define GARP_VLAN_DEV_MAX_NUM_VLAN (VLAN_DEV_MAX_NUM_VLAN / 2)
#define GARP_MAX_GVRP_ATTR_ENT ((BRG_MAX_PHY_PLUS_LOG_PORTS) * \
                  (VLAN_MAX_CURR_ENTRIES))

    /* For GMRP Number of VLAN gives number of GIPs. Because of this mempool 
     * becomes huge. Hence for GMRP
     *   No. of GIPs per port = No. of VLANs / 2 and the memory type is 
     * changed to heap to accomodate GIPS more than the allocated one. */
#define GARP_MAX_GMRP_GIP_ENT_PER_PORT \
    ((GARP_VLAN_DEV_MAX_NUM_VLAN < VLAN_DEV_MAX_VLAN_ID)? \
GARP_VLAN_DEV_MAX_NUM_VLAN: VLAN_DEV_MAX_VLAN_ID)

#define GARP_MAX_GMRP_GIP_ENTRIES ((GARP_MAX_GMRP_GIP_ENT_PER_PORT) * \
                                   (BRG_MAX_PHY_PLUS_LOG_PORTS))
/* 
 * This macro defines the no. of valid attribute types for GMRP. In case of 
 * GMRP the two attribute types are Group and Service requirement attribute
 * types. 
 */
#define GMRP_MAX_ATTR_TYPES       2

#define GARP_MAX_GMRP_ATTR_ENT    (((VLAN_MAX_GROUP_ENTRIES) + \
               (GMRP_MAX_ATTR_TYPES)) * GARP_MAX_PORTS) 

#define GARP_MAX_GIP_ENTRIES           ((GARP_MAX_GMRP_GIP_ENTRIES) + \
                                        (GARP_MAX_GVRP_GIP_ENTRIES))          
 
#define GARP_MAX_APPS                  2

#define GARP_MAX_PORT_ENTRIES         (GARP_MAX_PORTS * GARP_MAX_APPS)
    /* No of GVRP Attribute entries + No of GMRP Attribute entries turns out 
     * to be a very big number. Hence divide that by 2 and during mempool 
     * allocation for Attribute entries set the memtype as Heap. */
#define GARP_MAX_ATTR_ENTRIES         (((GARP_MAX_GVRP_ATTR_ENT) + (GARP_MAX_GMRP_ATTR_ENT)) / 2)

#define GARP_PORTS_PER_BYTE       VLAN_PORTS_PER_BYTE

#define GARP_MAX_APP_STATES 11
#define GARP_MAX_APP_EVENTS 9

#define GARP_MAX_REG_STATES 3
#define GARP_MAX_REG_EVENTS 7 

#define GARP_END_MARK             0
#define GARP_INIT_VAL             0    

#define GARP_VALID   1
#define GARP_INVALID 2

#define GARP_ACTIVE     1
#define GARP_PASSIVE    2

#define GARP_EQUAL     1
#define GARP_LESSER    2
#define GARP_GREATER   3

#define GVRP_APP_ID         1
#define GMRP_APP_ID         2
#define GARP_INVALID_APP_ID 0
#define GARP_INVALID_PORT   0

#define GARP_LEAVE_ALL       0
#define GARP_JOIN_EMPTY      1 
#define GARP_JOIN_IN         2
#define GARP_LEAVE_EMPTY     3
#define GARP_LEAVE_IN        4
#define GARP_EMPTY           5
#define GARP_INVALID_EVENT 255

#define GARP_DST_ADDR_OFFSET 0
#define GARP_SRC_ADDR_OFFSET 6
#define GARP_TYPE_LEN_OFFSET 12
#define GARP_LLC_OFFSET      14
#define GARP_PID_OFFSET      17

#define GARP_HDR_SIZE       19
#define GARP_TYPE_LEN_SIZE   2
#define GARP_LLC_SIZE        3 
#define GARP_PID_SIZE        2

#define GARP_PID            1


#define GVRP_MAX_BUCKETS    512
#define GVRP_MAX_ATTRIBUTES   1
    
#define GMRP_MAX_BUCKETS     50
#define GMRP_MAX_ATTRIBUTES   2
/* 
 * Garp pdus can be transmitted as Tagged pdus (for GMRP). So Garp msg len
 * must be less than the ethernet MTU by 4 bytes (Vlan Tag len).
 */
#define GARP_MAX_MSG_LEN    1496
#define GARP_MAX_LEAVE_ALL_MSG_LEN    32 

#define GARP_INVALID_ATTR_TYPE 0

#define GARP_MAX_ATTR_SIZE          12 /* 
                                        * END MARK (1) + Type (1) + Len (1) 
                                        * + Event (1) + MacAddrLen (6) 
                                        * + 2 END MARKS (1+1) 
                                        */ 
#define GARP_MAX_LEAVE_ALL_ATTR_SIZE 5 /* 
                                        * Type (1) + Len (1) + Event (1)
                                        * + 2 END MARKS (1+1). 
                                        */ 

#define GARP_MIN_ATTR_LEN_VAL      2  /* Len (1) + Event (1) */
#define GARP_MAX_ATTR_LEN_VAL      8  /* Len (1) + Event (1) + 
                                         MacAddrLen (6) */

#define GARP_ATTR_TYPE_SIZE     1
#define GARP_ATTR_EVENT_SIZE    1
#define GARP_ATTR_LEN_SIZE      1
#define GARP_END_MARK_SIZE      1

#define GARP_REG_NORMAL    1
#define GARP_REG_FIXED     2
#define GARP_REG_FORBIDDEN 3

#define GARP_FORWARDING    1
#define GARP_BLOCKING      2

#define GARP_DEF_JOIN_TIME        20    /* In Centi seconds */
#define GARP_DEF_LEAVE_TIME       60    /* In Centi seconds */
#define GARP_DEF_LEAVE_ALL_TIME 1000    /* In Centi seconds */
#define GARP_LEAVEALL_DIV_INTERVAL 2    /* As per the standard the
        leave all should vary between
        LT < x < 1.5 * LT is the leave
               all time - Sec 12.10.2.3 
        IEEE Std 802.1D, 1998 Edition */  
#define GARP_LEAVE_ALL_TIMER    1
#define GARP_LEAVE_TIMER        2
#define GARP_JOIN_TIMER         3

/*
 * These two macros specify the allowed number of registration other
 * than the registration in the 
 * recieving port, for LEAVE messages to be propagated
 */ 
#define GARP_NO_REGS_FOR_LEAVEMSG        0 
#define GARP_MIN_REGS_FOR_LEAVEMSG       1

#define GARP_SEC_TO_CENT_SEC_UNIT 100

/* HITLESS RESTART */
#define GARP_CENT_SEC_TO_MILLI_SEC_UNIT 10

#define GARP_MIN_TIME_TO_START 1
#define GARP_VLAN_AGEOUT_TIMER  4

/*
 * GARP LeaveALL PDU's alone will be sent on blocked ports. The following
 * macros used to distinguish them.
 */
#define GARP_LEAVEALL_PDU       0
#define GARP_OTHER_PDU          1

#define GARP_APPGIP_HASH_BUCKET_SIZE 20
#define GARP_GVRP_GIP_HASH_BUCKET_SIZE 20
#define GARP_GMRP_GIP_HASH_BUCKET_SIZE 20
#define GARP_TRC_BUF_SIZE    500
#define GARP_INVALID_CONTEXT_ID  (GARP_SIZING_CONTEXT_COUNT + 1)

#define GARP_CLI_INVALID_CONTEXT   0xFFFFFFFF
#define GARP_SWITCH_ALIAS_LEN      33

#define GARP_MIN_TRACE_VAL           0
#define GARP_MAX_TRACE_VAL           0x000FFFFF

#define GARP_MAX_PVLAN_BLKS 1

#endif /* _GARPCONS_H */

