/* $Id: garpglob.h,v 1.20 2016/08/20 09:41:02 siva Exp $              */
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : garpglob.h                                  */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : GARP                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains the declaration of the   */
/*                          global variables.                           */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#ifndef _GARPGLOB_H
#define _GARPGLOB_H

/*-------------- GLOBAL DECLARATIONS -------------------------------*/
tGarpContextInfo     *gpGarpContextInfo = NULL;
tGarpContextInfo     *gapGarpContextInfo [SYS_DEF_MAX_NUM_CONTEXTS];
tRBTree              gGarpPortTable; /* IfIndex based port RbTree */

UINT1                gau1GarpShutDownStatus [SYS_DEF_MAX_NUM_CONTEXTS];
UINT1                gau1GmrpStatus [SYS_DEF_MAX_NUM_CONTEXTS];
UINT1                gau1GvrpStatus [SYS_DEF_MAX_NUM_CONTEXTS];
tGarpQMsg            *gapGarpRemapQMsg [SYS_DEF_MAX_NUM_CONTEXTS];

tOsixTaskId          gGarpTaskId;
tOsixQId             gGarpQId;
tTimerListId         gGarpTmrListId;
tOsixQId             gGarpCfgQId;
tOsixSemId           gGarpSemId;
tGarpPoolIds         gGarpPoolIds;
UINT1                gu1GarpIsInitComplete = GARP_FALSE;
tGarpNodeStatus      gGarpNodeStatus; /* Can be GARP_IDLE, GARP_ACTIVE
                                           or GARP_STANDBY */
UINT1                gu1GarpStandbyToActive = GARP_FALSE;
UINT1                gau1GarpBitMaskMap [GARP_PORTS_PER_BYTE] = { 0x01, 0x80,
                                   0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };
tMacAddr             gGvrpAddr = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x21 };
tMacAddr             gGmrpAddr = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x20 };
UINT1                gu1GarpGblTraceOption = GARP_DISABLED;
tMacAddr             gProviderGvrpAddr = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0D };
tMacAddr             gCustomerGvrpAddr = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x21 };

/* HITLESS RESTART */
UINT1                gu1StdyStReqRcvd = OSIX_FALSE;

/* To reduce stacksize in GarpRemapUpdateVlanRegCount */ 
UINT2                gu2ConfPorts[GARP_MAX_PORTS] = { 0 };

/*********************** Applicant Sem Definition ***************************/

tGarpAppSemEntry gaGarpAppSem[GARP_MAX_APP_EVENTS][GARP_MAX_APP_STATES] =
{
   { /* GARP_APP_TRANSMIT_PDU */
      /* GARP_VA */ {GARP_AA, GARP_SEND_JOIN,     GARP_TRUE},
      /* GARP_AA */ {GARP_QA, GARP_SEND_JOIN,     GARP_FALSE},
      /* GARP_QA */ {GARP_QA, GARP_SEND_NONE,     GARP_FALSE},
      /* GARP_LA */ {GARP_VO, GARP_SEND_LEAVE_MT, GARP_FALSE},
      /* GARP_VP */ {GARP_AA, GARP_SEND_JOIN,     GARP_TRUE},
      /* GARP_AP */ {GARP_QA, GARP_SEND_JOIN,     GARP_FALSE},
      /* GARP_QP */ {GARP_QP, GARP_SEND_NONE,     GARP_FALSE},
      /* GARP_VO */ {GARP_VO, GARP_SEND_NONE,     GARP_FALSE},
      /* GARP_AO */ {GARP_AO, GARP_SEND_NONE,     GARP_FALSE},
      /* GARP_QO */ {GARP_QO, GARP_SEND_NONE,     GARP_FALSE},
      /* GARP_LO */ {GARP_VO, GARP_SEND_MT,       GARP_FALSE}
   },
   { /* GARP_APP_RCV_JOIN_IN */
      /* GARP_VA */ {GARP_AA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AA */ {GARP_QA, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_QA */ {GARP_QA, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_LA */ {GARP_LA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_VP */ {GARP_AP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AP */ {GARP_QP, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_QP */ {GARP_QP, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_VO */ {GARP_AO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_AO */ {GARP_QO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_QO */ {GARP_QO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_LO */ {GARP_AO, GARP_SEND_NONE, GARP_FALSE}
   },
   { /* GARP_APP_RCV_JOIN_MT */
      /* GARP_VA */ {GARP_VA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AA */ {GARP_VA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QA */ {GARP_VA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_LA */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_VP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_VO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_AO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_QO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_LO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE}
   },
   { /* GARP_APP_RCV_MT */
      /* GARP_VA */ {GARP_VA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AA */ {GARP_VA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QA */ {GARP_VA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_LA */ {GARP_LA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_VP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_VO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_AO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_QO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_LO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE}
   },
   { /* GARP_APP_RCV_LEAVE_IN */
     /* GARP_VA */ {GARP_VA, GARP_SEND_NONE, GARP_TRUE},
     /* GARP_AA */ {GARP_VA, GARP_SEND_NONE, GARP_TRUE},
     /* GARP_QA */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
     /* GARP_LA */ {GARP_LA, GARP_SEND_NONE, GARP_TRUE},
     /* GARP_VP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
     /* GARP_AP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
     /* GARP_QP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
     /* GARP_VO */ {GARP_LO, GARP_SEND_NONE, GARP_TRUE},
     /* GARP_AO */ {GARP_LO, GARP_SEND_NONE, GARP_TRUE},
     /* GARP_QO */ {GARP_LO, GARP_SEND_NONE, GARP_TRUE},
     /* GARP_LO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE}
   },
   { /* GARP_APP_RCV_LEAVE_MT */
      /* GARP_VA */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AA */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QA */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_LA */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_VP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_VO */ {GARP_LO, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AO */ {GARP_LO, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QO */ {GARP_LO, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_LO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE}
   },
   { /* GARP_APP_LEAVE_ALL */
      /* GARP_VA */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AA */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QA */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_LA */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_VP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_VO */ {GARP_LO, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AO */ {GARP_LO, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QO */ {GARP_LO, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_LO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE}
   },
   { /* GARP_APP_REQ_JOIN */
      /* GARP_VA */ {GARP_VA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AA */ {GARP_AA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QA */ {GARP_QA, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_LA */ {GARP_VA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_VP */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AP */ {GARP_AP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QP */ {GARP_QP, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_VO */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AO */ {GARP_AP, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QO */ {GARP_QP, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_LO */ {GARP_VP, GARP_SEND_NONE, GARP_TRUE}
   },
   { /* GARP_APP_REQ_LEAVE */
      /* GARP_VA */ {GARP_LA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_AA */ {GARP_LA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_QA */ {GARP_LA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_LA */ {GARP_LA, GARP_SEND_NONE, GARP_TRUE},
      /* GARP_VP */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_AP */ {GARP_AO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_QP */ {GARP_QO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_VO */ {GARP_VO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_AO */ {GARP_AO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_QO */ {GARP_QO, GARP_SEND_NONE, GARP_FALSE},
      /* GARP_LO */ {GARP_LO, GARP_SEND_NONE, GARP_TRUE}
   }
};

/*********************** Registrar Sem Definition ***************************/

tGarpRegSemEntry gaGarpRegSem [GARP_MAX_REG_EVENTS][GARP_MAX_REG_STATES] =
{
   { /* GARP_REG_RCV_JOIN_IN */
      /* GARP_IN */ {GARP_IN, GARP_HL_IND_NONE, GARP_TIMER_NONE},
      /* GARP_LV */ {GARP_IN, GARP_HL_IND_NONE, GARP_TIMER_STOP},
      /* GARP_MT */ {GARP_IN, GARP_HL_IND_JOIN, GARP_TIMER_NONE}
   },
   { /* GARP_REG_RCV_JOIN_MT */
      /* GARP_IN */ {GARP_IN, GARP_HL_IND_NONE, GARP_TIMER_NONE},
      /* GARP_LV */ {GARP_IN, GARP_HL_IND_NONE, GARP_TIMER_STOP},
      /* GARP_MT */ {GARP_IN, GARP_HL_IND_JOIN, GARP_TIMER_NONE}
   },
   { /* GARP_REG_RCV_MT */
      /* GARP_IN */ {GARP_IN, GARP_HL_IND_NONE, GARP_TIMER_NONE},
      /* GARP_LV */ {GARP_LV, GARP_HL_IND_NONE, GARP_TIMER_NONE},
      /* GARP_MT */ {GARP_MT, GARP_HL_IND_NONE, GARP_TIMER_NONE}
   },
   { /* GARP_REG_RCV_LEAVE_IN */
      /* GARP_IN */ {GARP_LV, GARP_HL_IND_NONE, GARP_TIMER_START},
      /* GARP_LV */ {GARP_LV, GARP_HL_IND_NONE, GARP_TIMER_NONE},
      /* GARP_MT */ {GARP_MT, GARP_HL_IND_NONE, GARP_TIMER_NONE}
   },
   { /* GARP_REG_RCV_LEAVE_MT */
      /* GARP_IN */ {GARP_LV, GARP_HL_IND_NONE, GARP_TIMER_START},
      /* GARP_LV */ {GARP_LV, GARP_HL_IND_NONE, GARP_TIMER_NONE},
      /* GARP_MT */ {GARP_MT, GARP_HL_IND_NONE, GARP_TIMER_NONE}
   },
   { /* GARP_REG_LEAVE_ALL */
      /* GARP_IN */ {GARP_LV, GARP_HL_IND_NONE, GARP_TIMER_START},
      /* GARP_LV */ {GARP_LV, GARP_HL_IND_NONE, GARP_TIMER_NONE},
      /* GARP_MT */ {GARP_MT, GARP_HL_IND_NONE, GARP_TIMER_NONE}
   },
   { /* GARP_REG_LEAVE_TIMER_EXPIRY */
      /* GARP_IN */ {GARP_IN, GARP_HL_IND_NONE, GARP_TIMER_NONE},
      /* GARP_LV */ {GARP_MT, GARP_HL_IND_LEAVE, GARP_TIMER_NONE},
      /* GARP_MT */ {GARP_MT, GARP_HL_IND_NONE, GARP_TIMER_NONE}
   }
};

UINT4 gu4GarpCliContext=0;
#endif
