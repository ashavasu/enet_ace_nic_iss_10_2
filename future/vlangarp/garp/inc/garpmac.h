/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: garpmac.h,v 1.35 2013/05/28 14:50:25 siva Exp $
 *
 * Description     : This file contains macro routines forr GARP
 *
 *******************************************************************/

#ifndef _GARP_MAC_H
#define _GARP_MAC_H

#define GARP_ADD                       1
#define GARP_DELETE                    2

#define GARP_SIZING_CONTEXT_COUNT   FsGARPSizingParams[MAX_GARP_CONTEXT_INFO_SIZING_ID].u4PreAllocatedUnits
#define GARP_MAX_CONTEXTS           GARP_SIZING_CONTEXT_COUNT 
#define GARP_MEMORY_TYPE()      \
    ((VcmGetSystemMode (GARP_PROTOCOL_ID) == VCM_SI_MODE) \
     ? MEM_DEFAULT_MEMORY_TYPE : MEM_HEAP_MEMORY_TYPE)

#define GARP_GET_BUFF(u1BuffType, u4Size)\
        GarpGetBuff ((u1BuffType), (u4Size))

#define GARP_RELEASE_BUFF(u1BuffType, pBuf)\
        GarpReleaseBuff ((u1BuffType), ((UINT1 *)(pBuf)))
       
#define GARP_MAC_ADDR_TO_ARRAY(pu1Array, pMacAddr) \
          MEMCPY (pu1Array, pMacAddr, GARP_MAC_ADDR_LEN)
           
#define GARP_ARRAY_TO_MAC_ADDR(pMacAddr, pu1Array) \
          MEMCPY (pMacAddr, pu1Array, GARP_MAC_ADDR_LEN); 

#define GARP_IS_ATTR_LEN_VALID(u1AttrLen) \
        (((u1AttrLen < GARP_MIN_ATTR_LEN_VAL) || (u1AttrLen > GARP_MAX_ATTR_LEN_VAL)) ? \
        GARP_FALSE : GARP_TRUE)

#define GARP_HASH_MAX_GIP_BUCKETS(u1AppId,u4MaxBucketSize)   \
        {\
            if (u1AppId == GARP_CURR_CONTEXT_PTR()->u1GvrpAppId ) \
            {\
                u4MaxBucketSize = GARP_GVRP_GIP_HASH_BUCKET_SIZE; \
            }else {\
                u4MaxBucketSize = GARP_GMRP_GIP_HASH_BUCKET_SIZE; \
            }\
        }

#define GARP_GIP_HASH_SIZE(pHashTab)    (pHashTab)->u4_HashSize

/* SEM Related Macros */
#define GARP_SEM_NAME   ((const UINT1 *)"GARS")
#define GARP_CREATE_SEM() \
        OsixCreateSem (GARP_SEM_NAME, 1, OSIX_DEFAULT_SEM_MODE, \
                       (&gGarpSemId))
#define GARP_RELEASE_SEM(SemId)\
        OsixSemGive((SemId))

#define GARP_DELETE_SEM()     OsixSemDel(gGarpSemId)
   
#define GARP_TAKE_SEM(SemId)\
        OsixSemTake ((SemId))

#define GARP_GET_APP_ENTRY(u1AppId) \
        (((u1AppId > GARP_INVALID_APP_ID) && (u1AppId <= GARP_MAX_APPS) \
          && (GARP_CURR_CONTEXT_PTR()->aGarpAppTable[u1AppId].u1Status == GARP_VALID)) \
                 ? (&GARP_CURR_CONTEXT_PTR()->aGarpAppTable[u1AppId]) : NULL)

#define GARP_IS_PORT_CREATED(_u2Port) \
        (((GARP_CURR_CONTEXT_PTR()->apGarpGlobalPortEntry[_u2Port]) == NULL) \
         ? GARP_FALSE : GARP_TRUE)
    
#define GARP_IS_PORT_CHANNEL(u2Port) \
        (((u2Port > BRG_MAX_PHY_PORTS) && (u2Port <= BRG_MAX_PHY_PLUS_LOG_PORTS)) ? GARP_TRUE : GARP_FALSE)

/* Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port. */
#define GARP_IS_MEMBER_PORT(au1PortArray, u2Port, u1Result) \
        {\
           UINT2 u2PortBytePos;\
           UINT2 u2PortBitPos;\
           u2PortBytePos = (UINT2)(u2Port / GARP_PORTS_PER_BYTE);\
           u2PortBitPos  = (UINT2)(u2Port % GARP_PORTS_PER_BYTE);\
           if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
           if ((au1PortArray[u2PortBytePos] \
                & gau1GarpBitMaskMap [u2PortBitPos]) != 0) {\
           \
              u1Result = GARP_TRUE;\
           }\
           else {\
           \
              u1Result = GARP_FALSE; \
           } \
        }

/* Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port. */
#define GARP_SET_MEMBER_PORT(au1PortArray, u2Port, i4ArraySize) \
           {\
              UINT2 u2PortBytePos;\
              UINT2 u2PortBitPos;\
              u2PortBytePos = (UINT2)(u2Port / GARP_PORTS_PER_BYTE);\
              u2PortBitPos  = (UINT2)(u2Port % GARP_PORTS_PER_BYTE);\
               if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
              \
               if (u2PortBytePos < i4ArraySize) \
               {                                \
                 au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                             | gau1GarpBitMaskMap [u2PortBitPos]);\
               }                                \
           }

/* Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port. */
#define GARP_RESET_MEMBER_PORT(au1PortArray, u2Port) \
              {\
                 UINT2 u2PortBytePos;\
                 UINT2 u2PortBitPos;\
                 u2PortBytePos = (UINT2)(u2Port / GARP_PORTS_PER_BYTE);\
                 u2PortBitPos  = (UINT2)(u2Port % GARP_PORTS_PER_BYTE);\
                 if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
                 \
                 au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                               & ~gau1GarpBitMaskMap[u2PortBitPos]);\
              }

#define GARP_CMP_MAC_ADDR(pMacAddr1, pMacAddr2) \
       ((MEMCMP(pMacAddr1,pMacAddr2,GARP_MAC_ADDR_LEN)) ? \
   VLAN_FALSE : VLAN_TRUE)
#define GARP_IS_PORT_VALID(u2Port) \
        (((u2Port > GARP_MAX_PORTS_PER_CONTEXT) || (u2Port == 0)) ? GARP_FALSE : GARP_TRUE)


/* 
 * This macro is used by GARP applications (GVRP/GMRP) to check if a 
 * particular port is created and operationally UP.
 */
#define GARP_PORT_ENTRY(port) GARP_CURR_CONTEXT_PTR()->apGarpGlobalPortEntry[port]
    
#define GARP_IS_PORT_KNOWN(u2Port) \
((GARP_CURR_CONTEXT_PTR()->apGarpGlobalPortEntry[u2Port] == NULL)?GARP_FALSE: \
(GARP_CURR_CONTEXT_PTR()->apGarpGlobalPortEntry[u2Port]->u1OperStatus == GARP_OPER_UP) ? \
         GARP_TRUE : GARP_FALSE)

#define GARP_COPY_MAC_ADDR(pMacAddr1, pMacAddr2)\
       MEMCPY(pMacAddr1,pMacAddr2,GARP_MAC_ADDR_LEN) 

#define GARP_DESTADDR_IS_GVRPADDR(pDestAddr) \
         ((MEMCMP(pDestAddr,gGvrpAddress,ETHERNET_ADDR_SIZE)) ? \
   VLAN_FALSE : VLAN_TRUE)
   
#define GARP_DESTADDR_IS_GMRPADDR(pDestAddr) \
        ((MEMCMP(pDestAddr,gGmrpAddress,ETHERNET_ADDR_SIZE)) ? \
          VLAN_FALSE : VLAN_TRUE)
 
#define GARP_IS_GVRP_ENABLED_ON_PORT(u2Port) \
        ((GARP_CURR_CONTEXT_PTR()->aGvrpPortTable[u2Port] != GVRP_ENABLED) ? \
          GARP_TRUE : GARP_FALSE)

#define GARP_IS_GMRP_ENABLED_ON_PORT(u2Port) \
 ((GARP_CURR_CONTEXT_PTR()->aGmrpPortTable[u2Port].u1Status == GMRP_ENABLED) ? \
          GARP_TRUE : GARP_FALSE)

#define GARP_CONTEXT_PTR(u4ContextId)  gapGarpContextInfo[u4ContextId]    
#define GARP_CURR_CONTEXT_PTR()  gpGarpContextInfo    
#define GARP_CURR_CONTEXT_STR()  gpGarpContextInfo->au1ContextStr
#define GARP_CURR_CONTEXT_ID()  gpGarpContextInfo->u4ContextId    
#define GARP_BRIDGE_MODE() (GARP_CURR_CONTEXT_PTR()->u4BridgeMode)

#define GARP_IS_GARP_ENABLED()   GarpIsGarpEnabled ()
#define GVRP_IS_GVRP_ENABLED()   GvrpIsGvrpEnabled ()
/*
 * GARP Timer Relationship :
 * For efficient operation of GARP, Leave Time minus two Join Times and 
 * Leave All Period minus Leave Time should all be positive and non-zero value.
 * The time parameter values should be chosen in order to
 * ensure that these timer relationships are maintained.
 */       
 
#define GARP_IS_LEAVE_TIME_VALID(LeaveTime, LeaveAllTime, JoinTime) \
        (((LeaveTime < LeaveAllTime) && (LeaveTime > (2 * JoinTime))) ?\
           GARP_TRUE : GARP_FALSE)    

#define GARP_IS_JOIN_TIME_VALID(JoinTime, LeaveTime) \
        (((JoinTime > 0 ) &&((2 * JoinTime) < LeaveTime)) ?  GARP_TRUE : GARP_FALSE)    

#define GARP_IS_LEAVE_ALL_TIME_VALID(LeaveAllTime, LeaveTime) \
        (((LeaveAllTime > 0) && (LeaveAllTime > LeaveTime)) ?  GARP_TRUE : GARP_FALSE)    

#define GARP_ATTR_GIPID(pAttrEntry)  ((pAttrEntry)->pGipEntry)->u2GipId

#define GARP_QMSG_TYPE(pMsg) (pMsg)->u2MsgType

#define GARP_QMSG_COUNT(pMsg) pMsg->i4Count

#define GARP_QMSG_EVENT(pMsg) pMsg->u2MsgEvent
#define GARP_QMSG_EVENT_COUNT(pMsg) pMsg->i4EventCount

#define GARP_HASH_DYN_Scan_Bucket(pHashTab, u2HashKey, pNode, \
                pTempNode, TypeCast)\
                GARP_SLL_DYN_Scan (&((pHashTab)->HashList[u2HashKey]), \
                                 pNode, pTempNode, TypeCast)

#define GARP_SLL_DYN_Scan TMO_DYN_SLL_Scan

#define GARP_NODE_STATUS() (gGarpNodeStatus)

#define GVRP_ADMIN_STATUS() (GARP_CURR_CONTEXT_PTR()->u1GvrpAdminStatus)
#define GMRP_ADMIN_STATUS() (GARP_CURR_CONTEXT_PTR()->u1GmrpAdminStatus)
#define GVRP_MODULE_STATUS(u4ContextId)  gau1GvrpStatus[u4ContextId]
#define GMRP_MODULE_STATUS(u4ContextId)  gau1GmrpStatus[u4ContextId]

#define GARP_GET_PHY_PORT(u2Port)  \
    (GARP_CURR_CONTEXT_PTR()->apGarpGlobalPortEntry[u2Port])->u4PhyIfIndex
    
#define GARP_GET_IFINDEX(u2Port)  \
    (GARP_CURR_CONTEXT_PTR()->apGarpGlobalPortEntry[u2Port])->u4IfIndex
    
#define GARP_PB_PORT_TYPE(u2Port) \
    ((GARP_PORT_ENTRY (u2Port))->u2PbPortType)

#define GARP_CTXT_GVRP_ADDR() \
    ((GARP_CURR_CONTEXT_PTR ())->CtxtGvrpAddr)

#define GARP_IS_1AD_BRIDGE() \
    (((GARP_BRIDGE_MODE () == GARP_PROVIDER_EDGE_BRIDGE_MODE) || \
    (GARP_BRIDGE_MODE () == GARP_PROVIDER_CORE_BRIDGE_MODE)) ? GARP_TRUE \
    : GARP_FALSE )

#define GARP_IS_CUSTOMER_NETWORK_PORT(u2Port) \
    (((GARP_IS_1AD_BRIDGE () == GARP_TRUE) && \
     (GARP_PB_PORT_TYPE (u2Port) == GARP_CUSTOMER_NETWORK_PORT))? GARP_TRUE: \
     GARP_FALSE)

#define GARP_STAP_PORT_CHANGE(pAppEntry, pAppPortEntry, u2GipId, u1PortState) \
        ((u1PortState == GARP_FORWARDING)? \
         (GarpPortStateChangedToForwarding((pAppEntry),(pAppPortEntry), \
                                           (u2GipId))): \
         (GarpPortStateChangedToBlocking((pAppEntry), (pAppPortEntry), \
                                         (u2GipId))))
    
#define GARP_GET_TASK_ID            OsixGetTaskId
#define GARP_TASK_ID                gGarpTaskId
#define GARP_TASK_QUEUE_ID          gGarpQId
#define GARP_CFG_QUEUE_ID           gGarpCfgQId
#endif
