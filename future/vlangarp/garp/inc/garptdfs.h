/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: garptdfs.h,v 1.35 2013/02/14 13:42:07 siva Exp $
 *
 * Description: This file contains all the type definitions.
 *
 *******************************************************************/ 
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : garptdfs.h                                  */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : GARP                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains the type definitions.    */
/*                                                                      */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#ifndef _GARP_TDFS_H
#define _GARP_TDFS_H

typedef struct _tGarpGipEntry tGarpGipEntry;
typedef struct _tGarpContextInfo tGarpContextInfo;

typedef struct _tGarpTimer{
   tTmrAppTimer  TmrNode;
   void         *pRec;
   tGarpContextInfo *pGarpContextInfo; 
   UINT2         u2Port;
   UINT1         u1AppId;
   UINT1         u1TmrType;
   UINT1         u1IsTmrActive;
   UINT1         au1Reserved [3];   
} tGarpTimer;

typedef struct _tGarpAttr {
   UINT1   u1AttrType;
   UINT1   u1AttrLen;
   UINT1   au1AttrVal [GARP_MAX_ATTR_LEN];
} tGarpAttr;

typedef struct _tGarpRemapAttrEntry {
   struct _tGarpRemapAttrEntry   *pNextNode;
   tGarpAttr                      Attr;
   UINT4                          u4FixedRegCount;
} tGarpRemapAttrEntry;

typedef struct _tGarpAttrEntry{
   struct _tGarpAttrEntry *pNextHashNode;
   tGarpAttr               Attr; 
   tGarpTimer              LeaveTmr;
   tGarpGipEntry          *pGipEntry;
   UINT1                   u1RegSemState;
   UINT1                   u1AppSemState;
   UINT1                   u1AdmRegControl;
   UINT1                   u1Reserved;
} tGarpAttrEntry;

typedef struct _tGarpPortEntry{
   struct _tGarpPortEntry     *pNextNode;
   tGarpTimer                 LeaveAllTmr;
   tGarpTimer                 JoinTmr;
   tTMO_HASH_TABLE            *pGipTable;
   UINT4                      u4NumRegFailed;
   tMacAddr                   LastPduOrigin; 
   UINT2                      u2Port;  /*Local Port Identifier*/
   UINT1                      u1LeaveAllSemState;
   UINT1                      u1AppStatus; /* Indicates whether the corresponding
                                            * application is enabled/disabled on
                                            * this port. */
   UINT1                      u1RestrictedRegControl;
   UINT1                      au1Reserved [1];  
   UINT4                      u4NumGipEntries; /* Reflects no of gip entries */
} tGarpPortEntry;

struct _tGarpGipEntry {
   tTMO_SLL_NODE             NextNode;
   struct _tGarpGipEntry    *pNextPortGipEntry;
   struct _tGarpGipEntry    *pPrevPortGipEntry;
   struct _tGarpGipEntry    *pNextAppGipEntry;
   struct _tGarpGipEntry    *pPrevAppGipEntry;
   UINT4                     u4NumEntries;
   tGarpAttrEntry          **papAttrTable;
   tGarpPortEntry           *pGarpPortEntry;
   UINT2                     u2GipId;
   UINT1                     u1StapStatus; 
   UINT1                     u1AttrType;   
};

typedef INT4 (*tGarpAppIndFn) (UINT1, UINT1, UINT1 *, UINT2, UINT2);
typedef INT4 (*tGarpAppValidateFn) (UINT1);
typedef INT4 (*tGarpAppRegValidateFn) (tGarpAttr, UINT2 ,UINT2);
typedef INT4 (*tGarpAppWildCardValidateFn) (tGarpAttr, UINT2 ,UINT2);

typedef struct _tGarpAppnFn{

 tGarpAppIndFn pJoinIndFn;
 tGarpAppIndFn pLeaveIndFn;
 tGarpAppValidateFn pAttrTypeValidateFn;
 tGarpAppRegValidateFn pAttrRegValidateFn;
 tGarpAppWildCardValidateFn pAttrWildCardValidateFn;
}tGarpAppnFn;


typedef struct _tGarpAppEntry{
   tGarpAppIndFn       pLeaveIndFn;
   tGarpAppIndFn       pJoinIndFn;
   tGarpAppValidateFn  pAttrTypeValidateFn; /* 
                                             * This function pointer is
                                             * used to validate the received
                                             * attribute type 
                                             */
   tGarpAppRegValidateFn pAttrRegValidateFn; 
   tGarpAppWildCardValidateFn pAttrWildCardValidateFn; /*
                                                        This function pointer
                                                        is used to validate the
                                                        whether Mcast address 
                                                        can be learn on Port.
                                                        */
                                        
   tGarpPortEntry     *pPortTable;
   tGarpGipEntry     **papAppGipHashTable;
   tMacAddr            AppAddress;
   UINT1               u1AppId;
   UINT1               u1Status;
   UINT2               u2MaxBuckets;
   UINT2               u2Reserved;
} tGarpAppEntry;

/* GARP Port Statistics, counters for GMRP Join and Leave,
 * GVRP Join and Leave and Discard Counters for GARP, GVRP, and GMRP */
typedef struct _tGarpPortStats{
   UINT4                    u4GmrpJoinEmptyTxCount;
   UINT4                    u4GmrpJoinEmptyRxCount;
   UINT4                    u4GmrpJoinInTxCount;
   UINT4                    u4GmrpJoinInRxCount;
   UINT4                    u4GmrpLeaveInTxCount;
   UINT4                    u4GmrpLeaveInRxCount;
   UINT4                    u4GmrpLeaveEmptyTxCount;
   UINT4                    u4GmrpLeaveEmptyRxCount;
   UINT4                    u4GmrpEmptyTxCount;
   UINT4                    u4GmrpEmptyRxCount;
   UINT4                    u4GmrpLeaveAllTxCount;
   UINT4                    u4GmrpLeaveAllRxCount;
   UINT4                    u4GmrpDiscardCount;
   UINT4                    u4GvrpJoinEmptyTxCount;
   UINT4                    u4GvrpJoinEmptyRxCount;
   UINT4                    u4GvrpJoinInTxCount;
   UINT4                    u4GvrpJoinInRxCount;
   UINT4                    u4GvrpLeaveInTxCount;
   UINT4                    u4GvrpLeaveInRxCount;
   UINT4                    u4GvrpLeaveEmptyTxCount;
   UINT4                    u4GvrpLeaveEmptyRxCount;
   UINT4                    u4GvrpEmptyTxCount;
   UINT4                    u4GvrpEmptyRxCount;
   UINT4                    u4GvrpLeaveAllTxCount;
   UINT4                    u4GvrpLeaveAllRxCount;
   UINT4                    u4GvrpDiscardCount;
}tGarpPortStats;


typedef struct _tGarpGlobalPortEntry {
   tRBNodeEmbd  NextPortNode; /*Embedded RBTree next node */
   tRBNodeEmbd  RBNode; /* Embeded RBTree node */
   tGarpPortStats *pGarpPortStats; /* Pointer to GARP Port Statistics structure */
   UINT4    u4ContextId; /* Context Id for which this port belongs to */
   UINT4    u4IfIndex; /* Interface index  */
   UINT4    u4PhyIfIndex; /* Physical Interface index */
   UINT4    u4JoinTime;
   UINT4    u4LeaveTime;
   UINT4    u4LeaveAllTime;
   UINT2    u2PbPortType;
   UINT2    u2LocalPortId;
   UINT1    u1OperStatus;
   UINT1    au1Reserved[3];
} tGarpGlobalPortEntry;

typedef struct _tGarpIfMsg{
   tGarpAppEntry  *pAppEntry;
   UINT4           u4ContextId;
   tMacAddr     SrcMacAddr;
   UINT2           u2GipId;
   UINT2           u2Port; /*Interface Index*/
   UINT2           u2Len;
   } tGarpIfMsg;

typedef struct _tGarpPoolIds {
   tMemPoolId    GarpQMsgPoolId; 
   tMemPoolId    GarpQBulkMsgPoolId;
   tMemPoolId    GarpContextPoolId;
   tMemPoolId    PortPoolId; 
   tMemPoolId    AttrPoolId;
   tMemPoolId    AttrGvrpHashPoolId;
   tMemPoolId    AttrGmrpHashPoolId;
   tMemPoolId    GipPoolId;  
   tMemPoolId    GarpVlanMapMsgPoolId;
   tMemPoolId    AppGipPoolId;
   tMemPoolId    RemapAttrPoolId;
   tMemPoolId    GarpGlobPortPoolId;
   tMemPoolId    GarpRemapTxBufPoolId;
   tMemPoolId    GarpGlobPvlanPoolId;
   tMemPoolId  GarpPortStatsPoolId;
}tGarpPoolIds;

typedef enum {
   GARP_SEND_JOIN,
   GARP_SEND_LEAVE_MT,
   GARP_SEND_MT,
   GARP_SEND_NONE
} tGarpSendMsgInd;

typedef struct _tGarpAppSemEntry {
   UINT4           u4NextState;
   tGarpSendMsgInd SendMsgInd;
   UINT4           u4JoinTmrInd;  /* GARP_TRUE or GARP_FALSE */
} tGarpAppSemEntry;

typedef enum {
   GARP_VA,
   GARP_AA, 
   GARP_QA, 
   GARP_LA, 
   GARP_VP, 
   GARP_AP, 
   GARP_QP,
   GARP_VO,
   GARP_AO,
   GARP_QO,
   GARP_LO
} tGarpAppSemStates;

typedef enum {
   GARP_APP_TRANSMIT_PDU,
   GARP_APP_RCV_JOIN_IN, 
   GARP_APP_RCV_JOIN_MT, 
   GARP_APP_RCV_MT, 
   GARP_APP_RCV_LEAVE_IN, 
   GARP_APP_RCV_LEAVE_MT,
   GARP_APP_LEAVE_ALL,
   GARP_APP_REQ_JOIN,
   GARP_APP_REQ_LEAVE
} tGarpAppEvents;


typedef enum {
   GARP_HL_IND_JOIN,
   GARP_HL_IND_LEAVE,
   GARP_HL_IND_NONE
} tGarpHlMsgInd;

typedef enum {
   GARP_TIMER_START,
   GARP_TIMER_STOP,
   GARP_TIMER_NONE
} tGarpLeaveTmrInd;

typedef struct _tGarpRegSemEntry {
   UINT4            u4NextState;
   tGarpHlMsgInd    HlMsgInd;
   tGarpLeaveTmrInd LeaveTmrInd;
} tGarpRegSemEntry;

typedef enum {
   GARP_IN,
   GARP_LV,
   GARP_MT
} tGarpRegSemStates;

typedef enum {
   GARP_REG_RCV_JOIN_IN,
   GARP_REG_RCV_JOIN_MT, 
   GARP_REG_RCV_MT, 
   GARP_REG_RCV_LEAVE_IN, 
   GARP_REG_RCV_LEAVE_MT,
   GARP_REG_LEAVE_ALL,
   GARP_REG_LEAVE_TIMER_EXPIRY 
} tGarpRegEvents;

/* Represents the node status */
typedef enum {
    GARP_NODE_IDLE = 1,
    GARP_NODE_ACTIVE,
    GARP_NODE_STANDBY,
    GARP_NODE_FORCE_SWITCHOVER_INPROGRESS
}tGarpNodeStatus;

struct _tGarpContextInfo {
   tRBTree      GarpContextPortInfo; /* list of port entries, 
                                 sorted based on interface index */
   tGarpRemapAttrEntry *pRemapAttrList; 
   tGarpAppEntry        aGarpAppTable [GARP_MAX_APPS + 1];
   tGarpGlobalPortEntry *apGarpGlobalPortEntry[GARP_MAX_PORTS_PER_CONTEXT + 1];
                        /* array of pointer to port entry local port based 
                           table The following Port tables size is 4 bytes 
                           (with 2 byes padded)so It need not be array of 
                           pointers*/
   tGvrpPortTable       aGvrpPortTable[GARP_MAX_PORTS_PER_CONTEXT+1];
   tGmrpPortTable       aGmrpPortTable[GARP_MAX_PORTS_PER_CONTEXT+1];
   UINT4                u4ContextId; /* Context Identifier */
   UINT4                u4BridgeMode;/* Bridge mode filled when garp comes
                                      * up. */
   UINT4                u4GarpTrcOption;
   UINT1                *pu1RemapTxBuf;
   tMacAddr             CtxtGvrpAddr; /* Gvrp addr to be used in this 
                                       * context. */
   UINT1                u1GvrpAdminStatus;
   UINT1                u1GvrpAppId;
   UINT1                au1ContextStr[GARP_CONTEXT_ALIAS_LEN + 1];
   UINT1                u1GmrpAdminStatus;
   UINT1                u1GmrpAppId;
   UINT1                au1Pad[1];
};

/* BELOW STRUCTURE IS USED FOR MEMPOOL CREATION ONLY*/
/* START */
typedef struct GarpMaxBulkMsgSize{
    tGarpQMsg      GarpQMsg;
    tGarpBulkMsg   GarpBulkMsg[VLAN_NO_OF_MSG_PER_POST - 1];
}tGarpMaxBulkMsgSize;

typedef struct GarpVlanMapMsgSize{
    tGarpQMsg      GarpQMsg;
    tVlanMapInfo   VlanMapInfo[VLAN_MAP_INFO_PER_POST - 1];
}tGarpVlanMapMsgSize;

typedef struct GarpAppGipHashArray{
    tGarpGipEntry     *apGipEntry[GARP_APPGIP_HASH_BUCKET_SIZE];
}tGarpAppGipHashArray;

typedef struct GarpGlobPvlanSize{
    UINT2   PvlanInfo[VLAN_MAX_COMMUNITY_VLANS + 1];
}tGarpGlobPvlanSize;

/*END */

#endif
