
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2001-2002                                     */
/* $Id: garpprot.h,v 1.53 2015/07/13 04:42:40 siva Exp $              */
/*                                                                      */
/*  FILE NAME             : garpprot.h                                  */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*  SUBSYSTEM NAME        : GARP                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                                */
/*  DESCRIPTION           : This file contains the prototype of all the */
/*                          functions.                                  */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#ifndef _GARP_PROTO_H
#define _GARP_PROTO_H

INT4 GarpTaskInit PROTO ((VOID));
VOID GarpTaskDeInit PROTO ((void));
INT4 GarpInit PROTO ((void));
INT4 GarpHandleStartModule PROTO ((VOID));
VOID  GarpAppAdminEnable (VOID);
VOID  GarpAppModuleEnable (VOID);
void GarpProcessConfigMsg (tGarpQMsg *pGarpQMsg);
void GarpProcessTimerEvent (void);
void GarpProcessTimerExpiry PROTO ((tGarpTimer *pTimer));
void GarpProcessMsg PROTO ((tGarpIfMsg *pIfMsg, tCRU_BUF_CHAIN_HEADER *pMsg));
UINT1* GarpGetBuff PROTO ((UINT1 u1BufType, UINT4 u4Size));
void GarpReleaseBuff PROTO ((UINT1 u1BufType, UINT1 *pu1Buf));

void GarpPortStateChangedToBlocking (tGarpAppEntry  *pAppEntry, 
                                     tGarpPortEntry *pPortEntry,
                                     UINT2           u2GipId);
void GarpFormAndSendLeaveAllPdu (tGarpAttr     *pAttr, 
                                 tGarpAppEntry *pAppEntry, 
                                 UINT1          u1IsLastAttr,
                                 UINT2          u2Port, 
                                 UINT2          u2GipId);

void GarpFormAndSendPdu (tGarpAttr   *pAttr, 
                         UINT1        u1Event, 
                         tMacAddr     DstAddr, 
                         UINT1        u1IsLastAttr,
                         UINT2        u2Port, 
                         UINT2        u2GipId);

void GarpPortStateChangedToForwarding (tGarpAppEntry  *pAppEntry, 
                                       tGarpPortEntry *pPortEntry,
                                       UINT2           u2GipId);
tGarpAppEntry* GarpGetNewAppEntry (UINT1 *pu1AppId);

tGarpAppEntry* GarpGetAppEntryWithAddr (tMacAddr );

tGarpPortEntry* GarpGetNewPortEntry (UINT1);
   
void GarpFreePortEntry PROTO ((tGarpPortEntry *pPortEntry));

tGarpPortEntry* GarpGetPortEntry (tGarpAppEntry *pAppEntry, UINT2 u2Port);
   
void GarpDeletePortEntry (tGarpAppEntry *pAppEntry, tGarpPortEntry *pNode);

tGarpAttrEntry* GarpGetNewAttributeEntry PROTO ((void));

void GarpFreeAttributeEntry (tGarpAttrEntry *pAttrEntry);
   
void GarpInitAttributeEntry PROTO ((tGarpAttrEntry *pAttrEntry,
                             tGarpAttr *pAttr, 
                             UINT1 u1AppId, 
                             UINT2 u2Port, 
                             tGarpGipEntry *pGipEntry));

tGarpAttrEntry* GarpGetAttributeEntry PROTO ((tGarpAppEntry  *pAppEntry,
                                       tGarpGipEntry  *pGipEntry, 
                                       tGarpAttr      *pAttr));

tGarpGipEntry* GarpAddAttributeEntry PROTO ((tGarpAppEntry *pAppEntry, 
                            tGarpPortEntry *pPortEntry, 
                            tGarpGipEntry *pGipEntry,
                            tGarpAttrEntry *pAttrEntry,
                            UINT2 u2GipId));


void GarpDeleteAttributeEntry PROTO ((tGarpAppEntry  *pAppEntry,
                               tGarpPortEntry *pPortEntry, 
                               tGarpGipEntry **pGipEntry,
                               tGarpAttrEntry *pAttrEntry));

void GarpDeleteAllAttrEntries (tGarpAppEntry *pAppEntry, 
                               tGarpPortEntry *pPortEntry);

tGarpGipEntry* GarpGetNewGipEntry (tGarpAppEntry *pAppEntry);

void GarpFreeGipEntry (tGarpGipEntry *pGipEntry);

void GarpDeleteGipEntry (tGarpAppEntry* pAppEntry,
                         tGarpPortEntry *pPortEntry,
                         tGarpGipEntry *pGipEntry);

INT4
GarpGetGipAndAttributeEntry PROTO ((tGarpAppEntry * pAppEntry,
                             tGarpPortEntry * pPortEntry, tGarpAttr * pAttr,
                             UINT2 u2GipId, tGarpAttrEntry ** pAttrEntry,
                             tGarpGipEntry ** pGipEntry));




UINT4 GarpGetAttributeHashIndex (tGarpAppEntry *pAppEntry, tGarpAttr *pAttr);
   
INT4 GarpSkipToNextValidAttrType PROTO ((UINT1 *pu1Data, tGarpAppEntry *pAppEntry, UINT2 u2BufLen, UINT2 *pu2AttrOffset));

INT4 GarpGetFirstAttribute PROTO ((UINT1 *pu1Data, UINT1 *pu1Event,
                                   UINT2 *pu2Len,
                                   tGarpAttr *pAttr, tGarpAppEntry *pAppEntry));

INT4 GarpGetNextAttribute PROTO ((UINT1 *pu1Data,   UINT1     u1PrevAttrType,
                                  UINT1     *pu1Event, UINT2  *pu2Len,     
                                  tGarpAttr *pAttr, tGarpAppEntry *pAppEntry));

INT4 GarpIsAttrAdminRegFixed PROTO ((tGarpAppEntry *pAppEntry, 
                                     tGarpAttr *pAttr, UINT2 u2GipId, 
                                     UINT2 u2Port));

INT4 GarpAppDelPort PROTO ((UINT1 u1AppId, UINT2 u2Port));

INT4 GarpAppAddPort PROTO ((UINT1 u1AppId, UINT2 u2Port));

INT4 GarpAppDeEnrol PROTO ((UINT1 u1AppId));

INT4 GarpAppEnrol PROTO ((tMacAddr AppMacAddr,tGarpAppnFn AppnFn, UINT1 *pu1AppId));


void GarpTransmitPdu (UINT1 *pu1Data, UINT2 u2BufLen, tMacAddr DstAddr,
                      UINT2 u2Port, UINT2 u2GipId);


/* GARP _GID _GIP prototypes*/

void GarpGidGipAttributeLeaveReq (tGarpAttr      *pAttr,
                                  tGarpPortEntry *pPortEntry,
                                  tGarpIfMsg     *pIfMsg);
void GarpGidGipAttributeJoinReq (tGarpAttr      *pAttr,
                                 tGarpPortEntry *pPortEntry,
                                 tGarpIfMsg     *pIfMsg);

void GarpGipAppAttributeReqJoin (UINT1      u1AppId, 
                                 tGarpAttr *pAttr,
                                 UINT2      u2GipId,
                                 tLocalPortList  AddPorts);

void GarpGipAppAttributeReqLeave (UINT1      u1AppId, 
                                  tGarpAttr *pAttr,
                                  UINT2      u2GipId,
                                  tLocalPortList  DelPorts);

void GarpGipAppSetForbiddenPorts (UINT1      u1AppId, 
                                  tGarpAttr *pAttr,
                                  UINT2      u2GipId,
                                  tLocalPortList  AddPorts,
                                  tLocalPortList  DelPorts);

void GarpGipGidAttributeJoinInd (tGarpAttr *pAttr, tGarpIfMsg *pIfMsg);

void GarpGipGidAttributeLeaveInd (tGarpAttr *pAttr, tGarpIfMsg *pIfMsg);

void GarpGipPropagateOnAllPorts (tGarpAttr       *pAttr, 
                                 tGarpIfMsg      *pIfMsg,
                                 tGarpSendMsgInd  SendMsgInd);

void GarpGipGetPortList PROTO ((UINT1 u1AppId, UINT2 u2GipId, tLocalPortList PortList));

void
GarpHandleGipUpdateGipPorts PROTO ((UINT2 u2Port, UINT2 u2GipId, UINT1 u1Action));

VOID
GarpCleanAndDeleteGipEntry PROTO ((tGarpAppEntry * pAppEntry,
                            tGarpPortEntry * pPortEntry, UINT2 u2GipId));
/* GARP Timer related  Api's */

INT4 GarpStartTimer (tGarpTimer *pTimer, UINT4 u4Duration);

INT4 GarpStopTimer (tGarpTimer *pTimer);

void GarpJoinTimerExpiry PROTO ((UINT1 u1AppId, tGarpPortEntry *pPortEntry));

void GarpLeaveTimerExpiry PROTO ((UINT1           u1AppId, 
                           UINT2           u2Port, 
                           tGarpAttrEntry *pAttrEntry));

void GarpLeaveAllTimerExpiry PROTO ((tGarpPortEntry *pPortEntry));

INT4 GarpDisable PROTO ((VOID));
INT4 GarpEnable PROTO ((VOID));
INT4 GarpDeInit PROTO ((VOID));

void InitGarpAttributeEntry PROTO ((tGarpAttrEntry * pAttrEntry));



/* Garp Applicant sem handler prototypes */

tGarpSendMsgInd GarpApplicantSem PROTO ((tGarpAttrEntry *pAttrEntry, 
                                  tGarpPortEntry *pPortEntry,
                                  UINT1           u1SemEvent));

void GarpRegistrarSem PROTO ((tGarpAppEntry  *pAppEntry, 
                       tGarpAttrEntry *pAttrEntry, 
                       UINT2           u2InPort, 
                       UINT1           u1SemEvent));


INT4 GarpGetNumRegFailed (UINT1 u1AppId, UINT2 u2Port, UINT4 *pu4NumRegFailed);

INT4 GarpGetLastPduOrigin (UINT1 u1AppId, UINT2 u2Port,
                           tMacAddr MacAddr);

void GarpGidJoinInd PROTO ((tGarpAttr *pAttr, tGarpIfMsg *pIfMsg, tGarpPortEntry *pPortEntry, UINT1 u1MsgType));

void GarpGidLeaveInd PROTO ((tGarpAttr *pAttr, tGarpIfMsg *pIfMsg,
                             tGarpPortEntry *pPortEntry, UINT1 u1MsgType));

void GarpGidEmptyInd PROTO ((tGarpAttr *pAttr, tGarpIfMsg *pIfMsg, tGarpPortEntry *pPortEntry));


void GarpGidLeaveAllInd PROTO ((tGarpAttr *pAttr, tGarpIfMsg *pIfMsg, tGarpPortEntry *pPortEntry));

INT4 GvrpDisable PROTO ((VOID));

INT4 GmrpDisable PROTO ((VOID));

INT4 GvrpEnablePort PROTO ((UINT2 u2Port));

INT4 GvrpDisablePort PROTO ((UINT2 u2Port));

INT4 GmrpEnablePort PROTO ((UINT2 u2Port));

INT4 GmrpDisablePort PROTO ((UINT2 u2Port));

INT4 GarpAppEnablePort PROTO ((UINT1 u1AppId, UINT2 u2Port));

INT4 GarpAppDisablePort PROTO ((UINT1 u1AppId, UINT2 u2Port));

void
GmrpHandlePropagateMcastInfo (tMacAddr MacAddr, tVlanId VlanId,
                              tLocalPortList AddPortList, tLocalPortList DelPortList);
void
GmrpHandlePropagateDefGroupInfo (UINT1 u1Type, tVlanId VlanId,
                                 tLocalPortList AddPortList, tLocalPortList DelPortList);

void
GmrpHandleSetDefGroupForbiddenPorts (UINT1 u1Type, tVlanId VlanId,
                                     tLocalPortList AddPortList, tLocalPortList DelPortList);

void
GmrpHandleSetMcastForbiddenPorts (tMacAddr MacAddr, tVlanId VlanId,
                                  tLocalPortList AddPortList, tLocalPortList DelPortList);
void GarpSendLeaveAll PROTO ((tGarpAppEntry *pAppEntry, 
                              tGarpPortEntry *pPortEntry));

tGarpGipEntry* 
GarpGetGipEntry PROTO ((tGarpPortEntry *pPortEntry, UINT2 u2GipId));

INT4 GmrpJoinIndFn (UINT1   u1AttrType, 
                    UINT1   u1AttrLen, 
                    UINT1  *pu1AttrVal,
                    UINT2   u2Port,
                    UINT2   u2GipId);

INT4 GmrpLeaveIndFn (UINT1   u1AttrType, 
                     UINT1   u1AttrLen, 
                     UINT1  *pu1AttrVal,
                     UINT2   u2Port,
                     UINT2   u2GipId);

INT4 GmrpTypeValidateFn (UINT1 u1AttrType);

INT4 GvrpJoinIndFn (UINT1   u1AttrType, 
                    UINT1   u1AttrLen, 
                    UINT1  *pu1AttrVal,
                    UINT2   u2Port,
                    UINT2   u2GipId);

INT4 GvrpLeaveIndFn (UINT1   u1AttrType, 
                     UINT1   u1AttrLen, 
                     UINT1  *pu1AttrVal,
                     UINT2   u2Port,
                     UINT2   u2GipId);

INT4 GvrpTypeValidateFn (UINT1 u1AttrType);

INT4 GvrpAttrRegValidateFn (tGarpAttr Attr, UINT2 u2Port, UINT2 u2GipId);
INT4 GmrpAttrRegValidateFn (tGarpAttr Attr, UINT2 u2Port, UINT2 u2GipId);

INT4
GvrpWildCardAttrRegValidateFn (tGarpAttr Attr, UINT2 u2Port, UINT2 u2GipId);
INT4
GmrpWildCardAttrRegValidateFn (tGarpAttr Attr, UINT2 u2Port, UINT2 u2GipId);

void
GvrpHandlePropagateVlanInfo (tVlanId VlanId,
                             tLocalPortList AddPortList, tLocalPortList DelPortList);

void
GvrpHandleSetVlanForbiddenPorts (tVlanId VlanId,
                                 tLocalPortList AddPortList, tLocalPortList DelPortList);

INT4 GvrpInit                  PROTO ((VOID));

INT4 GvrpAddPort               PROTO ((UINT2 u2Port));

INT4 GvrpDelPort               PROTO ((UINT2 u2Port));

INT4 GvrpEnable(void);          

INT4 GvrpGetNumRegFailed (UINT2 u2Port, UINT4 *pu4NumRegFailed);

INT4 GvrpGetLastPduOrigin (UINT2 u2Port, tMacAddr MacAddr);


INT4 GmrpEnable(void);

INT4 GmrpGetNumRegFailed (UINT2 u2Port, UINT4 *pu4NumRegFailed);

INT4 GmrpGetLastPduOrigin (UINT2 u2Port, tMacAddr MacAddr);

INT4
GvrpEnablePortRestrictedRegControl (UINT2 u2Port);
INT4
GvrpDisablePortRestrictedRegControl (UINT2 u2Port);
INT4
GmrpEnablePortRestrictedRegControl (UINT2 u2Port);
INT4
GmrpDisablePortRestrictedRegControl (UINT2 u2Port);
INT4
GarpIsAttrRegisteredOnOtherPorts (tGarpAppEntry * pAppEntry, tGarpAttr * pAttr,
                                             UINT2 u2GipId, UINT2 u2Port);
INT4
GarpAppEnablePortRestrictedRegControl (UINT1 u1AppId, UINT2 u2Port);
INT4
GarpAppDisablePortRestrictedRegControl (UINT1 u1AppId, UINT2 u2Port);

INT4 GarpPostCfgMessage PROTO ((UINT1 u1MsgType, UINT2 u2Port, UINT4 u4ContextId, 
                                UINT4 u4IfIndex));
INT4 GarpHandleCreatePort PROTO ((UINT4 u4IfIndex, UINT2 u2LocalPortId));
INT4 GarpHandleDeletePort PROTO ((UINT2 u2Port));
INT4 GarpHandlePortOperInd PROTO ((UINT2 u2Port, UINT1 u1OperStatus));

VOID GarpProcessBulkCfgEvent (tGarpQMsg *pGarpQMsg);
VOID GarpProcessCfgEvent PROTO ((tGarpQMsg *pGarpQMsg));
VOID GarpHandleStapPortChange PROTO ((UINT2 u2Port, UINT1 u1State, UINT2 u2GipId));
        
INT4 GarpCheckAndDelAttrEntry PROTO ((tGarpAppEntry*  pAppEntry, 
                               tGarpPortEntry* pPortEntry,
                               tGarpGipEntry* pGipEntry,
                               tGarpAttrEntry* pAttrEntry));

void GarpSyncAttributeTable (tGarpAppEntry  *pAppEntry, 
                             tGarpPortEntry *pPortEntry);

UINT1
GarpCmpMacAddr (tMacAddr Mac1, tMacAddr Mac2);

void GarpPrintAttribute (tGarpAttr *pAttr);

VOID GarpSyncAttributeTableForGip (tGarpAppEntry *pAppEntry, 
                                   tGarpPortEntry *pPortEntry, 
                                   UINT2 u2GipId);


INT4 GarpIsValidMcastAddr (tGarpAttr *pAttr); 
INT4 GarpIsValidVlanId (tGarpAttr *pAttr);

INT4 GarpIsAttributeValid PROTO ((tGarpIfMsg * pGarpIfMsg, 
                                  tGarpAttr * pAttr));

VOID
GarpProcessVlanMapCfgEvent PROTO ((tGarpQMsg * pGarpQMsg));

VOID
GarpPostInstVlanMap PROTO ((tGarpQMsg *pGarpQMsg));

VOID
GarpHandleVlanInstanceMap PROTO ((tVlanId VlanId, UINT2 u2OldInstance, 
                                           UINT2 u2NewInstance));

tGarpAttrEntry *
GarpRemapRemoveAttrEntry PROTO ((tGarpAppEntry *pAppEntry, 
                                 tGarpGipEntry **ppGipEntry,
                                 tGarpPortEntry *pAppPortEntry, 
                                 tGarpAttr *pAttr));

UINT2
GarpRemapUpdateGvrpRegCount PROTO ((tGarpAppEntry *pAppEntry, 
                             tVlanId VlanId,
                             UINT2 u2GipId, UINT2 u2NewGipId));

UINT2 
GarpRemapUpdateVlanRegCount PROTO ((tVlanId VlanId, UINT2 u2NewInstance));

VOID
GarpCheckAndAddRemapAttrEntry PROTO ((tGarpAttrEntry* pAttrEntry, 
                                      UINT1 u1IsRegFixed));
   
VOID
GarpRemapBuildGmrpAttrList PROTO ((tGarpAppEntry* pAppEntry, 
                                   tGarpGipEntry* pGipEntry));
    
VOID
GarpRemapUpdateGmrpRegCount PROTO ((tGarpAppEntry *pAppEntry, 
                                    UINT2 u2GipId));
    
VOID
GarpRemapWithoutAttrInNewInstance PROTO ((tGarpAppEntry *pAppEntry,
                                   tVlanId VlanId,
                                   UINT2 u2Instance,
                                   tGarpAttr *pDelAttr));

VOID
GarpRemapUpdateAppSemAndTx PROTO ((tGarpAppEntry *pAppEntry, 
                                   tGarpGipEntry *pGipEntry,
                                   tGarpAttrEntry *pAttrEntry, 
                                   UINT2 *pu2Offset));

VOID 
GarpRemapTransmitPdu PROTO ((UINT2 u2Offset, tMacAddr AppAddress, 
                             UINT2 u2PortId, UINT2 u2GipId));

VOID
GarpRemapUpdateRegSem PROTO ((tGarpAppEntry *pAppEntry, 
                              tGarpGipEntry *pGipEntry,
                              tGarpAttrEntry *pAttrEntry, tVlanId VlanId,
                              UINT1 u1VlanUpdate));

VOID
GarpRemapWithAttrInNewInstance PROTO ((tGarpAppEntry *pAppEntry, UINT2 u2GipId,
                                       tGarpAttr *pAttr));

tGarpRemapAttrEntry*
GarpGetNewRemapAttrEntry (VOID);

VOID
GarpDelGipEntryFromAppGipTable (tGarpAppEntry *pAppEntry, 
                             tGarpGipEntry *pAppDelGipEntry);

VOID
GarpAddGipEntryFromAppGipTable (tGarpAppEntry *pAppEntry, 
                             tGarpGipEntry *pAppAddGipEntry);

UINT2 GarpGipGetHashIndex PROTO ((UINT2 , UINT2 ));
tTMO_HASH_TABLE *
GarpCreateAppPortGipHashTable PROTO ((UINT4 ));
INT4
GarpDeleteAppPortGipHashTable PROTO ((tGarpPortEntry *));
INT4 
GarpAddGipEntryToGipHashTable PROTO ((tTMO_HASH_TABLE *, tGarpGipEntry  *));
INT4 
GarpDelGipEntryFromGipHashTable PROTO ((tTMO_HASH_TABLE *, tGarpGipEntry *));

VOID GarpDeleteGipHashNode (tTMO_HASH_NODE  *pGipHashNode);


/* Function Prototypes for garplow.c */





INT1 GarpSnmpSetDot1qPortGvrpStatus PROTO ((INT4 i4Dot1dBasePort , 
                                       INT4 i4SetValDot1qPortGvrpStatus));







INT1 GarpSnmpSetDot1dPortGarpJoinTime PROTO ((INT4 i4Dot1dBasePort , 
                                        INT4 i4SetValDot1dPortGarpJoinTime));


INT1 GarpSnmpSetDot1dPortGarpLeaveTime PROTO ((INT4 i4Dot1dBasePort , 
                                         INT4 i4SetValDot1dPortGarpLeaveTime));


INT1 GarpSnmpSetDot1dPortGarpLeaveAllTime PROTO ((INT4 i4Dot1dBasePort , 
                                            INT4 i4SetValDot1dPortGarpLeaveAllTime));




INT1 GarpSnmpSetDot1dPortGmrpStatus PROTO ((INT4 i4Dot1dBasePort,
                           INT4 i4SetValDot1dPortGmrpStatus));

INT1 nmhSetDot1qFutureGarpStatus PROTO ((INT4 i4SetValDot1qFutureGarpStatus));
















INT1 nmhTestv2Dot1qFutureGarpStatus PROTO ((UINT4 *pu4ErrorCode , INT4 i4TestValDot1qFutureGarpStatus));





tGarpNodeStatus GarpGetNodeStatus (VOID);

INT4 GarpIsGarpEnabled PROTO ((VOID));

INT4 GvrpIsGvrpEnabled PROTO ((VOID));

INT4 GmrpIsGmrpEnabled PROTO ((VOID));

INT4 GarpGetContextInfoFromIfIndex PROTO ((UINT2 u2Port, 
                                          UINT4 *pu4ContextId, UINT2 *pu2LocalPort));

INT4
GarpPortTblCmpFn PROTO ((tRBElem * pPort1, tRBElem * pPort2));
    
INT4 GarpSelectContext PROTO ((UINT4 u4ContextId));

VOID GarpReleaseContext (void);

INT4 GarpGlobalMemoryInit (void);
INT4 GarpGlobalMemoryDeInit (void);
VOID GarpAssignMempoolIds (VOID);
    
tGarpGlobalPortEntry *
GarpGetIfIndexEntry (UINT4 u4IfIndex);
    
VOID
GarpRemovePortEntry (VOID);
INT4
GarpInitAndAddPortEntry (tGarpGlobalPortEntry *pGlobalPortEntry,
                         UINT2 u2LocalPortId, UINT4 u4IfIndex);
VOID GarpInitPort (tGarpGlobalPortEntry * pGlobalPortEntry,
                   UINT2 u2LocalPortId, UINT4 u4IfIndex);
UINT4
GarpHandleCreateContext (UINT4 u4ContextId);
UINT4
GarpHandleDeleteContext (UINT4 u4ContextId);
UINT4
GarpHandleUpdateContextName (UINT4 u4ContextId);

INT4
GarpFreeGlobPortEntry (tRBElem *pElem, UINT4 u4Arg);
VOID
GarpRemovePortFromIfIndexTable (tGarpGlobalPortEntry *pPortEntry);
void
GarpProcessRedConfigMsg (tGarpQMsg * pGarpQMsg);
VOID
GarpFlushContextsQueue (VOID);    
VOID
GarpPrintMacAddr (UINT4 u4ModId, tMacAddr MacAddr);
VOID
GarpPrintPortList (UINT4 u4ModId, tLocalPortList PortList, UINT2 u2InPort);   
    
INT4 GarpGetFirstPortInSystem (UINT4 *pu4IfIndex);
INT4 GarpGetNextPortInSystem (INT4 i4IfIndex, UINT4 *pu4IfIndex);
/* Function Prototypes for garpmicli.c */
INT4
GarpGetNextActiveContext (UINT4 u4CurrContextId, UINT4 *pu4NextContextId);
INT4
GarpGetFirstPortInContext (UINT4 u4ContextId, UINT4 *pu4IfIndex);
INT4
GarpGetNextPortInContext (UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 *pu4NextIfIndex);

/* Provider bridge related proto-types. */
INT4 GarpHandleBridgeMode (VOID);

INT4 GarpPbCheckPortTypeChange (UINT2 u2Port);

VOID GarpPbHandlePortTypeChange (UINT2 u2Port);


INT4 GarpPbTranslateAttr (UINT2  u2Port, tGarpAttr *pAttr, tVlanId *pVid);

VOID GarpPbEgrChangeGvrpAddrOnPpnp (UINT2 u2Port, tMacAddr DstAddr);

VOID GarpPbTransAttrValAndCopy (UINT2  u2Port, UINT1 *pu1Ptr, tGarpAttr *pAttr);

INT4 GvrpPbIsGvrpEnablePortValid (UINT2 u2Port);
INT4 GvrpPbIsPortValidForGvrpEnable (UINT2 u2Port);

INT4 GarpPbIsPortOkForGarpPktRcv (UINT2 u2Port, tMacAddr AppAddress);

UINT1  GarpGetInstPortState (UINT2 u2GipId, 
                             tGarpGlobalPortEntry *pGarpGlobalPortEntry);

UINT1 GarpGetVlanPortState (UINT2 u2GipId,
                            tGarpGlobalPortEntry *pGarpGlobalPortEntry);
/* Function Prototypes for garpport.c */
INT4 GarpCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName);

INT4 GarpCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName);

INT4 GarpCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo);
BOOL1 GarpCfaIsThisOpenflowVlan (UINT4 u4VlanId);

INT4 GarpSnoopIsIgmpSnoopingEnabled (UINT4 u4ContextId);

INT4 GarpSnoopIsMldSnoopingEnabled (UINT4 u4ContextId);

INT4 GarpVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias);

INT4 GarpVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId, 
                                       UINT2 *pu2LocalPortId);

INT4 GarpVcmGetSystemMode (UINT2 u2ProtocolId);
INT4 GarpVcmGetSystemModeExt (UINT2 u2ProtocolId);

INT4 GarpVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum);

INT4 GarpVlanCheckAndTagOutgoingGmrpFrame (UINT4 u4ContextId, 
                                           tCRU_BUF_CHAIN_DESC * pBuf,
                                           tVlanId VlanId, UINT2 u2Port);

VOID GarpVlanDeleteAllDynamicVlanInfo (UINT4 u4ContextId, UINT2 u2Port);

tVlanNodeStatus GarpVlanGetNodeStatus (VOID);

INT4 GarpVlanGetStartedStatus (UINT4 u4ContextId);

INT4 GarpIsPvrstStartedInContext(UINT4 u4ContextId);

VOID GarpVlanGmrpDisableInd (UINT4 u4ContextId);

VOID GarpVlanGmrpEnableInd (UINT4 u4ContextId);

VOID GarpVlanGvrpDisableInd (UINT4 u4ContextId);

VOID GarpVlanGvrpEnableInd (UINT4 u4ContextId);

INT4 GarpVlanIsMcastStaticAndRegNormal (UINT4 u4ContxtId, tMacAddr MacAddr,
                                        UINT2 u2VlanId, UINT2 u2Port);
VOID
GarpL2IwfGetPVlanMappingInfo (tL2PvlanMappingInfo *pL2PvlanMappingInfo);

#ifdef VLAN_EXTENDED_FILTER
VOID GarpVlanDeleteAllDynamicDefGroupInfo (UINT4 u4ContextId, UINT2 u2Port);

INT4 GarpVlanIsServiceAttrStaticAndRegNormal (UINT4 u4ContxtId, UINT1 u1Type,
                                              UINT2 u2VlanId, UINT2 u2Port);

VOID GarpVlanMiDelDynamicDefGroupInfoForVlan (UINT4 u4ContextId, 
                                              tVlanId VlanId);

INT4 GarpVlanUpdateDynamicDefGroupInfo (UINT4 u4ContextId, UINT1 u1Type, 
                                        tVlanId VlanId, UINT2 u2Port, 
                                        UINT1 u1Action);
#endif
INT4 GarpVlanIsVlanDynamic (UINT4 u4ContextId, UINT2 VlanId);

INT4 GarpVlanIsVlanEnabledInContext (UINT4 u4ContextId);

INT4 GarpVlanIsVlanStaticAndRegNormal (UINT4 u4ContxtId, UINT2 VlanId, 
                                       UINT2 u2Port);

VOID GarpVlanMiDelDynamicMcastInfoForVlan (UINT4 u4ContextId, tVlanId VlanId);

VOID GarpVlanMiDeleteAllDynamicMcastInfo (UINT4 u4ContextId, UINT2 u2Port,
                                      UINT1 u1MacAddrType);

INT4 GarpVlanMiUpdateDynamicMcastInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                                       tVlanId VlanId, UINT2 u2Port, 
                                       UINT1 u1Action);

INT4 GarpVlanUpdateDynamicVlanInfo (UINT4 u4ContextId, tVlanId VlanId, 
                                    UINT2 u2Port, UINT1 u1Action);

INT4 GarpL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode);

UINT1 GarpL2IwfGetInstPortState (UINT2 u2MstInst, UINT2 u2IfIndex);

INT4 GarpL2IwfGetNextValidPortForContext (UINT4 u4ContextId, 
                                          UINT2 u2LocalPortId,
                                          UINT2 *pu2NextLocalPort, 
                                          UINT4 *pu4NextIfIndex);

INT4 GarpL2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType);

INT4 GarpL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT2 u2IfIndex, 
                                 UINT1 *pu1OperStatus);

INT4 GarpL2IwfGetPortVlanTunnelStatus (UINT2 u2IfIndex, BOOL1 * pbTunnelPort);

VOID GarpL2IwfGetProtocolTunnelStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol,
                                             UINT1 *pu1TunnelStatus);

UINT1 GarpL2IwfGetVlanPortState (tVlanId VlanId, UINT2 u2IfIndex);

INT4 GarpL2IwfGetVlanPortType (UINT2 u2IfIndex, UINT1 *pPortType);

INT4 GarpL2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                                       UINT2 u2IfIndex, UINT4 u4PktSize, 
                                       UINT2 u2Protocol, UINT1 u1Encap);

INT4 GarpL2IwfIsPortInPortChannel (UINT4 u4IfIndex);

UINT2 GarpL2IwfMiGetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId);

INT4 GarpL2IwfMiGetVlanLocalEgressPorts (UINT4 u4ContextId, tVlanId VlanId,
                                         tLocalPortList LocalEgressPorts);

INT4 GarpL2IwfPbGetLocalVidFromRelayVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                         tVlanId RelayVid, tVlanId * pLocalVid);

INT4 GarpL2IwfPbGetRelayVidFromLocalVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                         tVlanId LocalVid, tVlanId * pRelayVid);

INT4 GarpL2IwfSetProtocolEnabledStatusOnPort (UINT4 u4ContextId, 
                                              UINT2 u2LocalPortId,
                                              UINT2 u2Protocol, UINT1 u1Status);
INT4
GmrpProcessPvlanJoinOrLeaveInd (UINT4 u4ContextId, tMacAddr MacAddr,
                                tVlanId GipId, UINT2 u2Port, UINT1 u1Action);
INT4 GarpRegisterWithPktHandler (VOID);

UINT4 GarpUpdateGarpAppStatusOnAllPorts(UINT4 u4ContextId, UINT1 u1Status,
                                        UINT4 u4AppId);
UINT1 GarpLaGetMCLAGSystemStatus (VOID);
VOID
GarpIcchGetIcclIfIndex (UINT4 *pu4IfIndex);
#endif
