/* $Id: garpred.h,v 1.7 2011/09/12 07:09:14 siva Exp $              */
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : garpred.h                                   */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : MAIN                                        */
/*  MODULE NAME           : GARP Redundancy module                      */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : GARP redundancy header file.                */
/************************************************************************/
#ifndef _GARPRED_H
#define _GARPRED_H

typedef struct _tGarpRedWalkArg
{
    tGarpIfMsg   *pGarpIfMsg;
    tGarpAttr    *pAttr;
}tGarpRedWalkMsg;

#define GARP_RED_TYPE_FIELD_SIZE     1
#define GARP_RED_LEN_FIELD_SIZE      2

/* HITLESS RESTART */
#define GARP_HR_STDY_ST_PKT_REQ        RM_HR_STDY_ST_PKT_REQ
#define GARP_HR_STDY_ST_PKT_MSG        RM_HR_STDY_ST_PKT_MSG
#define GARP_HR_STDY_ST_PKT_TAIL       RM_HR_STDY_ST_PKT_TAIL
#define GARP_HR_STATUS()               RmGetHRFlag()
#define GARP_HR_STATUS_DISABLE         RM_HR_STATUS_DISABLE
#define GARP_RM_OFFSET                 0  /* Offset in the RM buffer from where
                                         * protocols can start writing. */
#define GARP_HR_STDY_ST_REQ_RCVD()     gu1StdyStReqRcvd

VOID GarpRedProcessMsg (tGarpQMsg *pGarpQMsg);
VOID GarpRedProcessMcastAddMsg (tGarpQMsg *pGarpQMsg);
VOID GarpRedMakeNodeActiveFromIdle (VOID);
VOID GarpRedMakeNodeStandbyFromIdle (VOID);
VOID GarpRedMakeNodeActiveFromStandby (VOID);
VOID GarpRedMakeNodeStandbyFromActive (VOID);
VOID GarpRedApplyDynamicData (VOID);
VOID GarpProcessCfgQMsgs (VOID);

INT4 GarpRedApplyGmrpLearntPorts (tRBElem *pRBElem, eRBVisit visit, 
                                  UINT4 u4Level, VOID *pArg, VOID *pOut);
VOID GarpRedApplyLearntPortList (tGarpIfMsg *pIfMsg, tGarpAttr *pAttr,
                                 tLocalPortList PortList, UINT1 u1IsFoundInAudit);
VOID GarpRedSendLeaveALL (VOID);

/* HITLESS RESTART */
VOID GarpRedHRProcStdyStPktReq  (VOID);
INT1 GarpRedHRSendStdyStTailMsg (VOID);
INT1 GarpRedHRSendStdyStPkt     (UINT1 *pu1LinBuf, UINT4 u4PktLen, UINT2 u2Port,
                               UINT4 u4TimeOut);

#endif /* _GARPRED_H */
