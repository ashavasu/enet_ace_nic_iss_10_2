
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : gvrp.h                                      */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : GVRP                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains Gvrp related information */
/*                                                                      */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#ifndef _GVRP_H_
#define _GVRP_H_


typedef struct _tGvrpPortTable {
    UINT1 u1RestrictedRegControl;
    UINT1 u1Status;
    UINT2 u2Pad;
}tGvrpPortTable;

#define GVRP_GROUP_ATTR_TYPE        GARP_GROUP_ATTR_TYPE         
#define GVRP_VLAN_ID_LEN            2


#endif
