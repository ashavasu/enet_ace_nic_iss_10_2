/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: garpsz.h,v 1.6 2013/02/14 13:42:07 siva Exp $
 *
 * Description: This file contains prototypes of creation/deletion
 *              mempools.
 *
 *******************************************************************/
enum {
    MAX_GARP_ATTR_ENTRIES_SIZING_ID,
    MAX_GARP_BULK_MSG_COUNT_SIZING_ID,
    MAX_GARP_CONFIG_Q_MESG_SIZING_ID,
    MAX_GARP_CONTEXT_INFO_SIZING_ID,
    MAX_GARP_GIP_ENTRIES_SIZING_ID,
    MAX_GARP_GLOBAL_PORT_ENTRIES_SIZING_ID,
    MAX_GARP_GLOBAL_PVLAN_ENTRIES_SIZING_ID,
    MAX_GARP_GMRP_ATTR_HASH_BUF_SIZING_ID,
    MAX_GARP_GVRP_ATTR_HASH_BUF_SIZING_ID,
    MAX_GARP_MAX_APP_HASH_ARRAY_COUNT_SIZING_ID,
    MAX_GARP_MSG_COUNT_SIZING_ID,
    MAX_GARP_PORT_ENTRIES_SIZING_ID,
    MAX_GARP_PORT_STAT_ENTRIES_SIZING_ID,
    MAX_GARP_REMAP_ATTR_ENTRIES_SIZING_ID,
    MAX_GARP_VLAN_MAP_MSG_COUNT_SIZING_ID,
    GARP_MAX_SIZING_ID
};
#ifdef  _GARPSZ_C
tMemPoolId GARPMemPoolIds[ GARP_MAX_SIZING_ID];
INT4  GarpSizingMemCreateMemPools(VOID);
VOID  GarpSizingMemDeleteMemPools(VOID);
INT4  GarpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _GARPSZ_C  */
extern tMemPoolId GARPMemPoolIds[ ];
extern INT4  GarpSizingMemCreateMemPools(VOID);
extern VOID  GarpSizingMemDeleteMemPools(VOID);
extern INT4  GarpSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _GARPSZ_C  */


#ifdef  _GARPSZ_C
tFsModSizingParams FsGARPSizingParams [] = {
{ "tGarpAttrEntry", "MAX_GARP_ATTR_ENTRIES", sizeof(tGarpAttrEntry),MAX_GARP_ATTR_ENTRIES, MAX_GARP_ATTR_ENTRIES,0 },
{ "tGarpMaxBulkMsgSize", "MAX_GARP_BULK_MSG_COUNT", sizeof(tGarpMaxBulkMsgSize),MAX_GARP_BULK_MSG_COUNT, MAX_GARP_BULK_MSG_COUNT,0 },
{ "tGarpQMsg", "MAX_GARP_CONFIG_Q_MESG", sizeof(tGarpQMsg),MAX_GARP_CONFIG_Q_MESG, MAX_GARP_CONFIG_Q_MESG,0 },
{ "tGarpContextInfo", "MAX_GARP_CONTEXT_INFO", sizeof(tGarpContextInfo),MAX_GARP_CONTEXT_INFO, MAX_GARP_CONTEXT_INFO,0 },
{ "tGarpGipEntry", "MAX_GARP_GIP_ENTRIES", sizeof(tGarpGipEntry),MAX_GARP_GIP_ENTRIES, MAX_GARP_GIP_ENTRIES,0 },
{ "tGarpGlobalPortEntry", "MAX_GARP_GLOBAL_PORT_ENTRIES", sizeof(tGarpGlobalPortEntry),MAX_GARP_GLOBAL_PORT_ENTRIES, MAX_GARP_GLOBAL_PORT_ENTRIES,0 },
{ "tGarpGlobPvlanSize", "MAX_GARP_GLOBAL_PVLAN_ENTRIES", sizeof(tGarpGlobPvlanSize),MAX_GARP_GLOBAL_PVLAN_ENTRIES, MAX_GARP_GLOBAL_PVLAN_ENTRIES,0 },
{ "(tGarpAttrEntry*)", "MAX_GARP_GMRP_ATTR_HASH_BUF", sizeof(tGarpAttrEntry*) * GMRP_MAX_BUCKETS,MAX_GARP_GMRP_ATTR_HASH_BUF, MAX_GARP_GMRP_ATTR_HASH_BUF,0 },
{ "(tGarpAttrEntry*)", "MAX_GARP_GVRP_ATTR_HASH_BUF", sizeof(tGarpAttrEntry*) * GVRP_MAX_BUCKETS,MAX_GARP_GVRP_ATTR_HASH_BUF, MAX_GARP_GVRP_ATTR_HASH_BUF,0 },
{ "tGarpAppGipHashArray", "MAX_GARP_MAX_APP_HASH_ARRAY_COUNT", sizeof(tGarpAppGipHashArray),MAX_GARP_MAX_APP_HASH_ARRAY_COUNT, MAX_GARP_MAX_APP_HASH_ARRAY_COUNT,0 },
{ "UINT1[1496]", "MAX_GARP_MSG_COUNT", sizeof(UINT1[1496]),MAX_GARP_MSG_COUNT, MAX_GARP_MSG_COUNT,0 },
{ "tGarpPortEntry", "MAX_GARP_PORT_ENTRIES", sizeof(tGarpPortEntry),MAX_GARP_PORT_ENTRIES, MAX_GARP_PORT_ENTRIES,0 },
{ "tGarpPortStats", "MAX_GARP_PORT_STAT_ENTRIES", sizeof(tGarpPortStats),MAX_GARP_PORT_STAT_ENTRIES, MAX_GARP_PORT_STAT_ENTRIES,0 },
{ "tGarpRemapAttrEntry", "MAX_GARP_REMAP_ATTR_ENTRIES", sizeof(tGarpRemapAttrEntry),MAX_GARP_REMAP_ATTR_ENTRIES, MAX_GARP_REMAP_ATTR_ENTRIES,0 },
{ "tGarpVlanMapMsgSize", "MAX_GARP_VLAN_MAP_MSG_COUNT", sizeof(tGarpVlanMapMsgSize),MAX_GARP_VLAN_MAP_MSG_COUNT, MAX_GARP_VLAN_MAP_MSG_COUNT,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _GARPSZ_C  */
extern tFsModSizingParams FsGARPSizingParams [];
#endif /*  _GARPSZ_C  */


