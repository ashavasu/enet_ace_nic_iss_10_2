
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : gmrp.h                                      */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : GARP                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains GMRP related information */
/*                                                                      */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#ifndef _GMRP_H
#define _GMRP_H


typedef struct _tGmrpPortTable {
    UINT1 u1RestrictedRegControl;
    UINT1 u1Status;
    UINT2 u2Pad;
}tGmrpPortTable;

#define GMRP_GROUP_ATTR_TYPE        GARP_GROUP_ATTR_TYPE         
#define GMRP_SERVICE_REQ_ATTR_TYPE  GARP_SERVICE_REQ_ATTR_TYPE

#define GMRP_MAC_ADDR_LEN           6
#define GMRP_SERVICE_REQ_LEN        1

#define GMRP_ALL_GROUPS             0
#define GMRP_UNREG_GROUPS           1


#endif
