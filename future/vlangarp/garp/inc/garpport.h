
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : garpport.h                                  */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : GARP                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains porting related          */
/*                          information.                                */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#ifndef _GARP_PORT_H 
#define _GARP_PORT_H

#define GARP_TASK_PRIORITY   30
#define GARP_TASK            ((UINT1*)"GARP")
#define GARP_TASK_QUEUE      ((UINT1*)"GRPQ")
#define GARP_Q_DEPTH         \
        ((VcmGetSystemMode (GARP_PROTOCOL_ID) == VCM_SI_MODE) \
         ? 100 : (2 * GARP_MAX_PORTS))

#define GARP_TIMER_EXP_EVENT    0x0001 
#define GARP_MSG_ENQ_EVENT      0x0002
#define GARP_CFG_MSG_EVENT      0x0004 
#define GARP_CFG_QUEUE       ((UINT1*)"GPCQ")
#define GARP_CFG_Q_DEPTH        GARP_MAX_PORTS * 4
#define GARP_MAX_MSG_PER_PORT 4 /*One for GVRP ,two for service req attr,
                                  one for multicast MAC */
#define GARP_MAX_PORT_FOR_BULK_MSG 12 /* This value can be changed as per need.*/

#define GARP_MAX_BULK_CFG_MSG  (GARP_MAX_PORT_FOR_BULK_MSG * GARP_MAX_MSG_PER_PORT)

#define GARP_RANDOM_TIMEOUT(u4TimeOut, u4RandVal) \
        OsixGetSysTime((tOsixSysTime *)&u4RandVal); \
       (u4RandVal) = (u4RandVal) % (u4TimeOut) + 1

#define GARP_GET_RANDOM_LEAVEALL_TIME(u4TimeOut, u4RandVal) \
        OsixGetSysTime((tOsixSysTime *)&(u4RandVal)); \
        (u4RandVal) = (u4RandVal) % ((u4TimeOut) / GARP_LEAVEALL_DIV_INTERVAL);\
        if (u4RandVal == 0) \
        {\
	 (u4RandVal)++; \
        }\
        (u4RandVal) = (u4TimeOut) + (u4RandVal) 

#define GARP_GET_DEVICE_MAC_ADDR(u2Port, MacAddr) \
        CfaGetIfMacAddr ((u2Port), (MacAddr))

#define GARP_CREATE_TASK        OsixTskCrt

#define GARP_CREATE_QUEUE       OsixQueCrt 

#define GARP_DELETE_QUEUE       OsixQueDel

#define GARP_SEND_EVENT    OsixEvtSend

#define GARP_RECV_EVENT    OsixEvtRecv

#define GARP_SEND_TO_QUEUE    OsixQueSend

#define GARP_RECV_FROM_QUEUE  OsixQueRecv

#define GARP_TMR_START(TmrListId, pAppTimer, u4Duration) \
        TmrStartTimer ((TmrListId), (pAppTimer), (u4Duration))

#define GARP_TMR_STOP(TmrListId, pAppTimer) \
        TmrStopTimer ((TmrListId), (pAppTimer))

#define GARP_SYS_BUFF_ALLOC(u4Size, u4Offset) \
        CRU_BUF_Allocate_MsgBufChain (u4Size, u4Offset)

#define GARP_SYS_BUFF_RELEASE(pBuf, u1Flag) \
        CRU_BUF_Release_MsgBufChain (((tCRU_BUF_CHAIN_HEADER *)(pBuf)), u1Flag)

#define GARP_COPY_FROM_SYS_BUFF(pBuf, pu1Dst, u4Offset, u4Size) \
        CRU_BUF_Copy_FromBufChain (((tCRU_BUF_CHAIN_HEADER *)(pBuf)), \
                                   (UINT1*)(pu1Dst), u4Offset, u4Size)

#define GARP_COPY_TO_SYS_BUFF(pBuf, pu1Src, u4Offset, u4Size) \
        CRU_BUF_Copy_OverBufChain ((tCRU_BUF_CHAIN_HEADER *)(pBuf), \
                                   (UINT1 *)(pu1Src), (u4Offset), (u4Size))

#define GARP_SYS_BUFF_IF_LINEAR(pBuf, u4OffSet, u4Size) \
        CRU_BUF_Get_DataPtr_IfLinear ((tCRU_BUF_CHAIN_HEADER *)(pBuf), \
                                      (u4OffSet), (u4Size))

#define GARP_GET_BUF_LEN(pBuf) \
        CRU_BUF_Get_ChainValidByteCount(pBuf)  

#define GARP_SLL_SCAN  TMO_SLL_Scan
#define tGARP_SLL_NODE tTMO_SLL_NODE 

#define GARP_FILL_IF_MSG(pMsg, pIfMsg) \
        {\
           void *pPtr;  \
           pPtr \
           = (void *) CRU_BUF_Get_ModuleData ((tCRU_BUF_CHAIN_HEADER *)(pMsg)); \
           MEMCPY(pPtr, (void *)(pIfMsg), sizeof (tGarpIfMsg)); \
        }
        
#define GARP_GET_IF_MSG(pMsg, pIfMsg) \
        { \
           pIfMsg = (tGarpIfMsg *) \
              CRU_BUF_Get_ModuleData ((tCRU_BUF_CHAIN_HEADER *)(pMsg)); \
        }
#define GARP_L2IWF_GETNEXT_VALID_PORT  L2IwfGetNextValidPort
#define GARP_L2IWF_GETNEXT_VALID_PORT_FOR_CONTEXT  \
        GarpL2IwfGetNextValidPortForContext

#define GARP_INIT_COMPLETE(u4Status)	lrInitComplete(u4Status)
#define GARP_DEFAULT_CONTEXT_ID      L2IWF_DEFAULT_CONTEXT
#define GARP_CONTEXT_ALIAS_LEN       L2IWF_CONTEXT_ALIAS_LEN
#endif

