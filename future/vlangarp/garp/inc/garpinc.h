/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: garpinc.h,v 1.18 2014/01/30 12:22:07 siva Exp $
 *
 * Description: This file contains all the include files.
 *
 *******************************************************************/ 
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : garpinc.h                                   */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : GARP                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains all the include files.   */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#ifndef  GARPINC_H
#define  GARPINC_H

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "bridge.h"
#ifndef _VLANINC_H
 #include "vlaninc.h"
#endif

#include "snmccons.h"
#include "snmcdefn.h"

#include "fsvlan.h"
#include "vlantrc.h"
#include "igs.h"

#include "la.h"
#include "mstp.h" 
#include "rstp.h"
#include "vcm.h"

/* GARP related defines */
#include "garptrc.h"
#include "garpport.h"
#include "garpcons.h"
#include "garp.h"
#include "gmrp.h"
#include "gvrp.h"
#include "garptdfs.h"
#include "garpextn.h"
#include "garpmac.h"
#include "garpprot.h"

#include "garpcli.h"
#ifdef L2RED_WANTED 
#include "garpred.h"
#endif /* L2RED_WANTED */
#include "garpsz.h"
#endif   
