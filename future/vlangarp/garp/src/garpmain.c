
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2001-2002                                     */
/* $Id: garpmain.c,v 1.113 2015/06/19 10:28:59 siva Exp $              */
/*  FILE NAME             : garpmain.c                                  */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*  SUBSYSTEM NAME        : MAIN                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                                */
/*  DESCRIPTION           : This file contains major Garp routines      */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#include "garpinc.h"
#include "garpglob.h"
#include "mux.h"

#ifdef TRACE_WANTED
UINT1               gu1GarpTestSemDbg = 1;
#endif

/***************************************************************/
/*  Function Name   :GarpProcessMsg                            *
 *  Description     :This function performs the processing of  *
 *                   the incoming message and update the       *
 *                   Interface structure.                      *
 *                                                             *
 *  Input(s)        : pMsg -Pointer to Incoming message        * 
 *                    pIfMsg - Pointer to Interface structure  *
 *  Output(s)       : None                                     *
 *  Global Variables Referred : None                           *
 *  Global variables Modified : None                           *
 *  Exceptions or Operating System Error Handling : None       *
 *  Use of Recursion : None                                    * 
 *  Returns         : None                                     *
 ***************************************************************/
void
GarpProcessMsg (tGarpIfMsg * pIfMsg, tCRU_BUF_CHAIN_HEADER * pMsg)
{
    tGarpAttr           Attr;
    tGarpPortEntry     *pPortEntry = NULL;
    tGarpGlobalPortEntry *pGlobalPortEntry;
    tMacAddr            DstMacAddr;
    tMacAddr            MacAddr;
    tGarpAppEntry      *pAppEntry;
    INT4                i4IsAttributeValid = GARP_TRUE;
    INT4                i4Result;
    UINT2               u2OffSet = 0;
    UINT2               u2Len;
    UINT2               u2BufLen;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1Data = NULL;
    UINT1               u1PrevAttrType;
    UINT1               u1Event;
    UINT1               u1IsGarpStatsReq = GARP_TRUE;
    UINT1               u1IsGvrp = GARP_FALSE;

    MEMSET (MacAddr, 0, sizeof (tMacAddr));
    MEMSET (DstMacAddr, 0, sizeof (tMacAddr));

    if (pIfMsg->u2Port > GARP_MAX_PORTS_PER_CONTEXT)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Port unknown. Received Garp PDU dropped.\n");
        return;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (pIfMsg->u2Port);

    if (pGlobalPortEntry == NULL)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Port unknown. Received Garp PDU dropped.\n");
        return;
    }

    if (pGlobalPortEntry->pGarpPortStats == NULL)
    {
        u1IsGarpStatsReq = GARP_FALSE;
    }

    if (pGlobalPortEntry->u1OperStatus == GARP_OPER_DOWN)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Port %d Oper Status = DOWN. Garp PDU dropped.\n",
                       GARP_GET_IFINDEX (pIfMsg->u2Port));
        return;
    }

    GARP_COPY_FROM_SYS_BUFF (pMsg, &DstMacAddr, 0, ETHERNET_ADDR_SIZE);

    /* Check whether this packet can be processed on this port.
     * In case of GVRP packet received on a PPNP, the following
     * function changes the DstMacAddr to Provider Gvrp addr. */
    if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
    {
        /* Check whether this packet can be processed on this port.
         * In case of GVRP packet received on a PPNP, the following
         * function changes the DstMacAddr to Provider Gvrp addr. */
        if (GarpPbIsPortOkForGarpPktRcv (pIfMsg->u2Port, DstMacAddr) ==
            GARP_FALSE)
        {
            return;
        }
    }

    pAppEntry = GarpGetAppEntryWithAddr (DstMacAddr);

    if (pAppEntry == NULL)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Frame for unregistered Application received "
                       "on Port %d.\n", GARP_GET_IFINDEX (pIfMsg->u2Port));
        return;
    }

    /* Store the App Entry in IfMsg since below functions are using it */

    pIfMsg->pAppEntry = pAppEntry;

    /* Buffer len in the interface message includes header also. */

    u2BufLen = pIfMsg->u2Len;

    if (u2BufLen < GARP_HDR_SIZE)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                       "GarpProcessMsg:Received Pkt of length %d is invalid \n",
                       u2BufLen);
        return;
    }

    /* 
     * Check whether the corresponding application is enabled or not.
     * Discard the packet if the application is disabled.
     */
    pPortEntry = GarpGetPortEntry (pIfMsg->pAppEntry, pIfMsg->u2Port);

    if (pPortEntry == NULL)
    {

        GARP_TRC (GARP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  GARP_NAME, "PDU received on Unknown port\n");
        /* Port is not enabled for this application */
        return;
    }

    if (pPortEntry->u1AppStatus == GARP_DISABLED)
    {
        /* Application disabled on this Port */
        GARP_TRC (GARP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  GARP_NAME, "PDU received on application disabled port\n");

        /* Port is not enabled for this application */
        return;
    }

    u2BufLen = (UINT2) (u2BufLen - GARP_HDR_SIZE);

    pu1Data = GARP_SYS_BUFF_IF_LINEAR (pMsg, GARP_HDR_SIZE, u2BufLen);

    if (pu1Data == NULL)
    {
        /* Allocate memory for GARP message */
        pu1Buf = MemAllocMemBlk (gGarpPoolIds.GarpRemapTxBufPoolId);
        if (pu1Buf == NULL)
        {
            GARP_TRC (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                      "GarpProcessMsg:Memory allocation for GARP message failed\n");
            return;
        }

        GARP_COPY_FROM_SYS_BUFF (pMsg, pu1Buf, GARP_HDR_SIZE, u2BufLen);

        pu1Data = pu1Buf;
    }

    u2Len = u2BufLen;

    i4Result = GarpGetFirstAttribute (pu1Data, &u1Event, &u2Len, &Attr,
                                      pIfMsg->pAppEntry);
    if (i4Result == GARP_FAILURE)
    {
        GARP_TRC (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  "GarpProcessMsg: First Attribute is invalid \n");
        if (pu1Buf != NULL)
        {
            MemReleaseMemBlock (gGarpPoolIds.GarpRemapTxBufPoolId, pu1Buf);
        }
        return;
    }

    GARP_COPY_MAC_ADDR (MacAddr, pIfMsg->pAppEntry->AppAddress);

    if (VLAN_DESTADDR_IS_GVRPADDR (MacAddr) == VLAN_TRUE)
    {
        u1IsGvrp = GARP_TRUE;
    }

    while (i4Result == GARP_SUCCESS)
    {
        if (GARP_LEAVE_ALL != u1Event)
        {
            /* This function validates the obtained attr in the message. 
             * When its GVRP attr, updates the associated GIPID in pIfMsg */
            i4IsAttributeValid = GarpIsAttributeValid (pIfMsg, &Attr);
        }

        switch (u1Event)
        {

            case GARP_JOIN_EMPTY:

                GARP_TRC_ARG2 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                               "JOIN EMPTY PDU received on port %d for GIP %d\n",
                               GARP_GET_IFINDEX (pIfMsg->u2Port),
                               pIfMsg->u2GipId);

                GARP_PRINT_ATTRIBUTE (&Attr);

                if (GARP_FALSE == i4IsAttributeValid)
                {
                    if (u1IsGarpStatsReq == GARP_TRUE)
                    {
                        if (u1IsGvrp == GARP_TRUE)
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GvrpDiscardCount++;
                        }
                        else
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GmrpDiscardCount++;
                        }
                    }
                    break;
                }

                i4Result =
                    pIfMsg->pAppEntry->pAttrWildCardValidateFn
                    (Attr, pIfMsg->u2Port, pIfMsg->u2GipId);

                if (i4Result == OSIX_TRUE)
                {
                    if (pPortEntry->u1RestrictedRegControl == GARP_ENABLED)
                    {
                        i4Result =
                            pIfMsg->pAppEntry->pAttrRegValidateFn (Attr,
                                                                   pIfMsg->
                                                                   u2Port,
                                                                   pIfMsg->
                                                                   u2GipId);

                        if (i4Result == GARP_TRUE)
                        {
                            GarpGidJoinInd (&Attr, pIfMsg, pPortEntry,
                                            GARP_JOIN_EMPTY);

                        }
                    }
                    else
                    {
                        GarpGidJoinInd (&Attr, pIfMsg, pPortEntry,
                                        GARP_JOIN_EMPTY);

                    }
                }
                break;

            case GARP_JOIN_IN:

                GARP_TRC_ARG2 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                               "JOIN IN PDU received on port %d for GIP %d\n",
                               GARP_GET_IFINDEX (pIfMsg->u2Port),
                               pIfMsg->u2GipId);

                GARP_PRINT_ATTRIBUTE (&Attr);

                if (GARP_FALSE == i4IsAttributeValid)
                {
                    if (u1IsGarpStatsReq == GARP_TRUE)
                    {
                        if (u1IsGvrp == GARP_TRUE)
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GvrpDiscardCount++;
                        }
                        else
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GmrpDiscardCount++;
                        }
                    }
                    break;
                }
                i4Result =
                    pIfMsg->pAppEntry->pAttrWildCardValidateFn
                    (Attr, pIfMsg->u2Port, pIfMsg->u2GipId);

                if (i4Result == OSIX_TRUE)
                {

                    if (pPortEntry->u1RestrictedRegControl == GARP_ENABLED)
                    {
                        i4Result =
                            pIfMsg->pAppEntry->pAttrRegValidateFn (Attr,
                                                                   pIfMsg->
                                                                   u2Port,
                                                                   pIfMsg->
                                                                   u2GipId);

                        if (i4Result == GARP_TRUE)
                        {
                            GarpGidJoinInd (&Attr, pIfMsg, pPortEntry,
                                            GARP_JOIN_IN);
                        }
                    }
                    else
                    {
                        GarpGidJoinInd (&Attr, pIfMsg, pPortEntry,
                                        GARP_JOIN_IN);
                    }
                }
                if (u1IsGarpStatsReq == GARP_TRUE)
                {
                    if (u1IsGvrp == GARP_TRUE)
                    {
                        pGlobalPortEntry->pGarpPortStats->u4GvrpJoinInRxCount++;
                    }
                    else
                    {
                        pGlobalPortEntry->pGarpPortStats->u4GmrpJoinInRxCount++;
                    }
                }
                break;

            case GARP_LEAVE_IN:

                GARP_TRC_ARG2 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                               "LEAVE IN PDU received on port %d for GIP %d\n",
                               GARP_GET_IFINDEX (pIfMsg->u2Port),
                               pIfMsg->u2GipId);

                GARP_PRINT_ATTRIBUTE (&Attr);

                if (GARP_TRUE == i4IsAttributeValid)
                {
                    GarpGidLeaveInd (&Attr, pIfMsg, pPortEntry, GARP_LEAVE_IN);
                    if (u1IsGarpStatsReq == GARP_TRUE)
                    {
                        if (u1IsGvrp == GARP_TRUE)
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GvrpLeaveInRxCount++;
                        }
                        else
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GmrpLeaveInRxCount++;
                        }
                    }
                }
                else
                {
                    if (u1IsGarpStatsReq == GARP_TRUE)
                    {
                        if (u1IsGvrp == GARP_TRUE)
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GvrpDiscardCount++;
                        }
                        else
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GmrpDiscardCount++;
                        }
                    }
                }
                break;

            case GARP_LEAVE_EMPTY:

                GARP_TRC_ARG2 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                               "LEAVE EMPTY PDU received on port %d for GIP %d\n",
                               GARP_GET_IFINDEX (pIfMsg->u2Port),
                               pIfMsg->u2GipId);

                GARP_PRINT_ATTRIBUTE (&Attr);

                if (GARP_TRUE == i4IsAttributeValid)
                {
                    GarpGidLeaveInd (&Attr, pIfMsg, pPortEntry,
                                     GARP_LEAVE_EMPTY);
                    if (u1IsGarpStatsReq == GARP_TRUE)
                    {
                        if (u1IsGvrp == GARP_TRUE)
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GvrpLeaveEmptyRxCount++;
                        }
                        else
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GmrpLeaveEmptyRxCount++;
                        }
                    }
                }
                else
                {
                    if (u1IsGarpStatsReq == GARP_TRUE)
                    {
                        if (u1IsGvrp == GARP_TRUE)
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GvrpDiscardCount++;
                        }
                        else
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GmrpDiscardCount++;
                        }
                    }
                }
                break;

            case GARP_EMPTY:

                GARP_TRC_ARG2 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                               "EMPTY PDU received on port %d for GIP %d\n",
                               GARP_GET_IFINDEX (pIfMsg->u2Port),
                               pIfMsg->u2GipId);

                GARP_PRINT_ATTRIBUTE (&Attr);

                if (GARP_TRUE == i4IsAttributeValid)
                {
                    GarpGidEmptyInd (&Attr, pIfMsg, pPortEntry);
                    if (u1IsGarpStatsReq == GARP_TRUE)
                    {
                        if (u1IsGvrp == GARP_TRUE)
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GvrpEmptyRxCount++;
                        }
                        else
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GmrpEmptyRxCount++;
                        }
                    }
                }
                else
                {
                    if (u1IsGarpStatsReq == GARP_TRUE)
                    {
                        if (u1IsGvrp == GARP_TRUE)
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GvrpDiscardCount++;
                        }
                        else
                        {
                            pGlobalPortEntry->pGarpPortStats->
                                u4GmrpDiscardCount++;
                        }
                    }
                }
                break;

            case GARP_LEAVE_ALL:

                GARP_TRC_ARG2 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                               "LEAVE ALL PDU received on port %d for GIP %d\n",
                               GARP_GET_IFINDEX (pIfMsg->u2Port),
                               pIfMsg->u2GipId);

                GARP_PRINT_ATTRIBUTE (&Attr);

                GarpGidLeaveAllInd (&Attr, pIfMsg, pPortEntry);
                if (u1IsGarpStatsReq == GARP_TRUE)
                {
                    if (u1IsGvrp == GARP_TRUE)
                    {
                        pGlobalPortEntry->pGarpPortStats->
                            u4GvrpLeaveAllRxCount++;
                    }
                    else
                    {
                        pGlobalPortEntry->pGarpPortStats->
                            u4GmrpLeaveAllRxCount++;
                    }
                }
                break;

            default:

                GARP_TRC (GARP_MOD_TRC, (ALL_FAILURE_TRC | CONTROL_PLANE_TRC),
                          GARP_NAME,
                          "Invalid Attribute Event present in the PDU \n");

                if (u1IsGarpStatsReq == GARP_TRUE)
                {
                    if (u1IsGvrp == GARP_TRUE)
                    {
                        pGlobalPortEntry->pGarpPortStats->u4GvrpDiscardCount++;
                    }
                    else
                    {
                        pGlobalPortEntry->pGarpPortStats->u4GmrpDiscardCount++;
                    }
                }
                /* Proceed with the next attribute. */
                break;
        }

        u1PrevAttrType = Attr.u1AttrType;

        u2OffSet = (UINT2) (u2BufLen - u2Len);
        i4Result = GarpGetNextAttribute (pu1Data + u2OffSet, u1PrevAttrType,
                                         &u1Event, &u2Len, &Attr,
                                         pIfMsg->pAppEntry);
    }
    /* De-allocate memory for GARP message */
    if (pu1Buf != NULL)
    {
        MemReleaseMemBlock (gGarpPoolIds.GarpRemapTxBufPoolId, pu1Buf);
    }
}

/**************************************************************/
/*  Function Name   : GarpGidJoinInd                           */
/*  Description     : This function is for processing of an    * 
 *                    incoming join message                    */
/*  Input(s)        : pAttr -pointer to the GarpAttribute      *
 *                    structure                                */
/*                    pIfMsg-Pointer to the interface Message  *
 *                    structure                                */
/*                    pPortEntry-Application Specific port     */
/*                    Entry                                    */
/*                    u1MsgType-Incoming message Type          */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpGidJoinInd (tGarpAttr * pAttr, tGarpIfMsg * pIfMsg,
                tGarpPortEntry * pPortEntry, UINT1 u1MsgType)
{
    tGarpAppEntry      *pAppEntry;
    tGarpAttrEntry     *pAttrEntry = NULL;
    tGarpGipEntry      *pGipEntry;
    UINT2               u2InPort;
    UINT2               u2GipId;
    UINT1               u1RegSemEvent;
    UINT1               u1AppSemEvent;

    pAppEntry = pIfMsg->pAppEntry;
    u2GipId = pIfMsg->u2GipId;
    u2InPort = pIfMsg->u2Port;

    GARP_COPY_MAC_ADDR (&pPortEntry->LastPduOrigin, &pIfMsg->SrcMacAddr);

    pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

    if (NULL != pGipEntry)
    {
        pAttrEntry = GarpGetAttributeEntry (pAppEntry, pGipEntry, pAttr);
    }

    if (pAttrEntry == NULL)
    {
        pAttrEntry = GarpGetNewAttributeEntry ();

        if (pAttrEntry == NULL)
        {

            pPortEntry->u4NumRegFailed = pPortEntry->u4NumRegFailed + 1;

            return;
        }

        GarpInitAttributeEntry (pAttrEntry, pAttr,
                                pAppEntry->u1AppId, u2InPort, pGipEntry);

        pGipEntry = GarpAddAttributeEntry (pAppEntry, pPortEntry,
                                           pGipEntry, pAttrEntry, u2GipId);

        if (NULL == pGipEntry)
        {

            pPortEntry->u4NumRegFailed = pPortEntry->u4NumRegFailed + 1;

            GarpFreeAttributeEntry (pAttrEntry);

            return;
        }
    }
    else
    {

        if (pAttrEntry->u1AdmRegControl != GARP_REG_NORMAL)
        {

            GARP_TRC_ARG1 (GARP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                           GARP_NAME, "Admin Reg control = %d. Ignoring the "
                           "Attribute\n", pAttrEntry->u1AdmRegControl);

            return;
        }
    }

    if (u1MsgType == GARP_JOIN_EMPTY)
    {

        u1RegSemEvent = GARP_REG_RCV_JOIN_MT;
        u1AppSemEvent = GARP_APP_RCV_JOIN_MT;
    }
    else
    {

        u1RegSemEvent = GARP_REG_RCV_JOIN_IN;
        u1AppSemEvent = GARP_APP_RCV_JOIN_IN;
    }

    if (pAttrEntry->u1RegSemState == GARP_IN)
    {
        GarpApplicantSem (pAttrEntry, pPortEntry, u1AppSemEvent);
        /* 
         * Since the Registrar sem is in IN state, a JOIN
         * was received already and would have been propagated 
         * to other ports. So there is no need to propagate 
         * this JOIN.
         */
        return;
    }

    GarpRegistrarSem (pAppEntry, pAttrEntry, u2InPort, u1RegSemEvent);

    /*If updation of Vlan Dynamic information fails and Regsem state is
     * not changed to GARP_IN from GARP_MT the attribute should not be 
     * registered or propagated*/
    if (pAttrEntry->u1RegSemState == GARP_MT)
    {
        GarpCheckAndDelAttrEntry (pAppEntry, pPortEntry, pGipEntry, pAttrEntry);

        pPortEntry->u4NumRegFailed = pPortEntry->u4NumRegFailed + 1;

        return;
    }

    GarpApplicantSem (pAttrEntry, pPortEntry, u1AppSemEvent);

    if (pGipEntry->u1StapStatus == GARP_FORWARDING)
    {
        GarpGipGidAttributeJoinInd (pAttr, pIfMsg);
    }
    else
    {
        GARP_TRC (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  "Join message received on Port that is in BLOCKED State."
                  " NOT propagating the Join message.\n");
    }
}

/**************************************************************/
/*  Function Name   : GarpGidLeaveInd                          */
/*  Description     :                                           */
/*  Input(s)        : pAttr -pointer to the GarpAttribute      *
 *                    structure                                */
/*                    pIfMsg-Pointer to the interface Message  *
 *                    structure                                */
/*                    pPortEntry-Application Specific port     */
/*                    Entry                                    */
/*                    u1MsgType-Incoming message Type          */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpGidLeaveInd (tGarpAttr * pAttr, tGarpIfMsg * pIfMsg,
                 tGarpPortEntry * pPortEntry, UINT1 u1MsgType)
{
    tGarpAppEntry      *pAppEntry;
    tGarpAttrEntry     *pAttrEntry = NULL;
    tGarpGipEntry      *pGipEntry;
    UINT2               u2GipId;
    UINT1               u1RegSemEvent;
    UINT1               u1AppSemEvent;

    pAppEntry = pIfMsg->pAppEntry;
    u2GipId = pIfMsg->u2GipId;
    pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

    if (NULL == pGipEntry)
    {
        /* Gip Associated with attribute is not present */
        return;
    }

    pAttrEntry = GarpGetAttributeEntry (pAppEntry, pGipEntry, pAttr);

    if (pAttrEntry == NULL)
    {
        return;
    }

    if (pAttrEntry->u1AdmRegControl != GARP_REG_NORMAL)
    {

        GARP_TRC_ARG1 (GARP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                       GARP_NAME, "Admin Reg control = %d. Ignoring the "
                       "Attribute\n", pAttrEntry->u1AdmRegControl);

        return;
    }

    if (u1MsgType == GARP_LEAVE_EMPTY)
    {

        u1RegSemEvent = GARP_REG_RCV_LEAVE_MT;
        u1AppSemEvent = GARP_APP_RCV_LEAVE_MT;
    }
    else
    {

        u1RegSemEvent = GARP_REG_RCV_LEAVE_IN;
        u1AppSemEvent = GARP_APP_RCV_LEAVE_IN;
    }

    GarpRegistrarSem (pAppEntry, pAttrEntry, pPortEntry->u2Port, u1RegSemEvent);
    GarpApplicantSem (pAttrEntry, pPortEntry, u1AppSemEvent);

    GarpCheckAndDelAttrEntry (pAppEntry, pPortEntry, pGipEntry, pAttrEntry);

}

/**************************************************************/
/*  Function Name   : GarpGidEmptyInd                          */
/*  Description     :                                          */
/*  Input(s)        : pAttr -pointer to the GarpAttribute      *
 *                    structure                                */
/*                    pIfMsg-Pointer to the interface Message  */
/*                    structure                                */
/*                    pPortEntry-Application Specific port     */
/*                    Entry                                    */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpGidEmptyInd (tGarpAttr * pAttr, tGarpIfMsg * pIfMsg,
                 tGarpPortEntry * pPortEntry)
{
    tGarpAppEntry      *pAppEntry;
    tGarpAttrEntry     *pAttrEntry = NULL;
    tGarpGipEntry      *pGipEntry;
    UINT2               u2GipId;

    pAppEntry = pIfMsg->pAppEntry;
    u2GipId = pIfMsg->u2GipId;

    pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

    if (NULL == pGipEntry)
    {
        return;
    }

    pAttrEntry = GarpGetAttributeEntry (pAppEntry, pGipEntry, pAttr);

    if (pAttrEntry == NULL)
    {

        return;
    }

    if (pAttrEntry->u1AdmRegControl != GARP_REG_NORMAL)
    {

        GARP_TRC_ARG1 (GARP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                       GARP_NAME, "Admin Reg control = %d. Ignoring the "
                       "Attribute\n", pAttrEntry->u1AdmRegControl);

        return;
    }

    GarpRegistrarSem (pAppEntry, pAttrEntry, pPortEntry->u2Port,
                      GARP_REG_RCV_MT);
    GarpApplicantSem (pAttrEntry, pPortEntry, GARP_APP_RCV_MT);
}

/**************************************************************/
/*  Function Name   : GarpGidLeaveAllInd                       */
/*  Description     :                                          */
/*  Input(s)        : pAttr -pointer to the GarpAttribute      *
 *                    structure                                */
/*                    pIfMsg-Pointer to the interface Message  *
 *                    structure                                */
/*                    pPortEntry-Application Specific port     */
/*                    Entry                                    */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpGidLeaveAllInd (tGarpAttr * pAttr, tGarpIfMsg * pIfMsg,
                    tGarpPortEntry * pPortEntry)
{
    tGarpAppEntry      *pAppEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpAttrEntry     *pNextAttrEntry;
    tGarpGipEntry      *pGipEntry;
    tGarpGipEntry      *pNextGipEntry;
    UINT4               u4HashIndex = 0;
    UINT4               u4Index;
    UINT2               u2InPort;
    UINT4               u4LeaveAllTime;
    UINT4               u4RandLeaveAllTime;
    UINT4               u4NumAttrEntries;
    UINT4               u4DelAttrCount;

    pAppEntry = pIfMsg->pAppEntry;
    u2InPort = pIfMsg->u2Port;

    if (pPortEntry->pGipTable != NULL)
    {
        TMO_HASH_Scan_Table (pPortEntry->pGipTable, u4HashIndex)
        {
            GARP_HASH_DYN_Scan_Bucket (pPortEntry->pGipTable, u4HashIndex,
                                       pGipEntry, pNextGipEntry,
                                       tGarpGipEntry *)
            {
                u4NumAttrEntries = pGipEntry->u4NumEntries;
                u4DelAttrCount = 0;

                for (u4Index = 0; u4Index < pAppEntry->u2MaxBuckets; u4Index++)
                {
                    pAttrEntry = pGipEntry->papAttrTable[u4Index];

                    while (pAttrEntry != NULL)
                    {

                        pNextAttrEntry = pAttrEntry->pNextHashNode;

                        /* 
                         * Reg sem must be updated only if the Admin Reg 
                         * Control is GARP_REG_NORMAL. App sem must be
                         * updated irrespective of the Admin Reg 
                         * Control value.
                         */
                        if (pAttrEntry->Attr.u1AttrType == pAttr->u1AttrType)
                        {

                            if (pAttrEntry->u1AdmRegControl == GARP_REG_NORMAL)
                            {

                                GarpRegistrarSem (pAppEntry,
                                                  pAttrEntry, u2InPort,
                                                  GARP_REG_LEAVE_ALL);
                            }

                            GarpApplicantSem (pAttrEntry, pPortEntry,
                                              GARP_APP_LEAVE_ALL);

                            if (GarpCheckAndDelAttrEntry (pAppEntry,
                                                          pPortEntry,
                                                          pGipEntry,
                                                          pAttrEntry)
                                == GARP_TRUE)
                            {
                                u4DelAttrCount++;
                            }

                        }

                        pAttrEntry = pNextAttrEntry;
                    }

                    if (u4NumAttrEntries == u4DelAttrCount)
                    {
                        /* 
                         * All the entries in the Attribute Hash table are
                         * deleted. As a result of this, the Gip entry
                         * would be deleted. So we should not scan the remaining
                         * buckets
                         */

                        break;
                    }
                }
            }
        }
    }

    pPortEntry->u1LeaveAllSemState = GARP_PASSIVE;

    if (pPortEntry->LeaveAllTmr.u1IsTmrActive == GARP_TRUE)
    {

        GarpStopTimer (&pPortEntry->LeaveAllTmr);
    }

    u4LeaveAllTime =
        GARP_CURR_CONTEXT_PTR ()->apGarpGlobalPortEntry[pPortEntry->u2Port]->
        u4LeaveAllTime;

    /* The leave all timer expiry event should happen at some random time
     * in the interval between configured leave all time and 1.5 * configured
     * leave all time. 
     */

    GARP_GET_RANDOM_LEAVEALL_TIME (u4LeaveAllTime, u4RandLeaveAllTime);

    GarpStartTimer (&pPortEntry->LeaveAllTmr, u4RandLeaveAllTime);
}

/***************************************************************/
/*  Function Name   : GarpPortStateChangedToForwarding         */
/*  Description     :                                          */
/*  Input(s)        : pAppEntry -pointer to the GarpApplication*/
/*                    structure                                */
/*                    pPortEntry-Pointer to the PortTable      */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpPortStateChangedToForwarding (tGarpAppEntry * pAppEntry,
                                  tGarpPortEntry * pPortEntry, UINT2 u2GipId)
{
    tGarpAttrEntry     *pAttrEntry;
    tGarpGipEntry      *pGipEntry;
    tGarpIfMsg          IfMsg;
    UINT4               u4Index;

    pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

    if (pGipEntry != NULL)
    {
        /* 
         * Some attribute entries are there in this GIP.
         */
        if (pGipEntry->u1StapStatus == GARP_FORWARDING)
        {
            /*
             * Old STAP status equal to the new STAP status.
             */
            return;
        }

        /*
         * Set the STAP Status first ... This variable is accessed everywhere
         * in the code.
         */

        pGipEntry->u1StapStatus = GARP_FORWARDING;

        /*
         * Propagate the entries registered on this port on all 
         * other ports.
         */
        for (u4Index = 0; u4Index < pAppEntry->u2MaxBuckets; u4Index++)
        {
            pAttrEntry = pGipEntry->papAttrTable[u4Index];
            while (pAttrEntry != NULL)
            {
                if (pAttrEntry->u1RegSemState == GARP_IN)
                {

                    IfMsg.pAppEntry = pAppEntry;
                    IfMsg.u2Port = pPortEntry->u2Port;
                    IfMsg.u2GipId = pGipEntry->u2GipId;

                    GarpGipPropagateOnAllPorts (&pAttrEntry->Attr,
                                                &IfMsg, GARP_SEND_JOIN);

                    /* 
                     * If the Admin Reg control is Fixed, then we must
                     * make the Applicant, a member. This is because, when
                     * the port moves to Blocking state, all Member Applicants
                     * are made Observer Applicants.
                     */

                    /* Dont make it as a member if the applicant is disabled */
                    if ((pAttrEntry->u1AdmRegControl == GARP_REG_FIXED) &&
                        (pPortEntry->u1AppStatus == GARP_ENABLED))
                    {
                        GarpApplicantSem (pAttrEntry, pPortEntry,
                                          GARP_APP_REQ_JOIN);
                    }
                }
                pAttrEntry = pAttrEntry->pNextHashNode;
            }
        }

        if (pPortEntry->u1AppStatus == GARP_ENABLED)
        {
            /*
             * Declare on this port all the attributes registered on other
             * ports.
             */
            GarpSyncAttributeTableForGip (pAppEntry, pPortEntry, u2GipId);
        }
    }
    else
    {
        if (pPortEntry->u1AppStatus == GARP_ENABLED)
        {

            /*
             * No attribute entries are there in this GIP.
             * Propagate the attributes registered on other ports in 
             * this port.
             */
            GarpSyncAttributeTableForGip (pAppEntry, pPortEntry, u2GipId);

            /*
             * Set the STAP status to FORWARDING. GIP might not be present still.
             */
            pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);
            if (pGipEntry != NULL)
            {
                pGipEntry->u1StapStatus = GARP_FORWARDING;
            }
        }
    }
}

/***************************************************************/
/*  Function Name   : GarpPortStateChangedToBlocking           */
/*  Description     : Invoked whenever port state is changed   */
/*                    changed from Forwarding state to         */
/*                    Blocked state.                           */
/*  Input(s)        : pAppEntry -pointer to the GarpApplication*/
/*                    structure                                */
/*                    pPortEntry-Pointer to the PortTable      */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpPortStateChangedToBlocking (tGarpAppEntry * pAppEntry,
                                tGarpPortEntry * pPortEntry, UINT2 u2GipId)
{
    tGarpAttrEntry     *pAttrEntry;
    tGarpAttrEntry     *pTmpAttrEntry;
    tGarpGipEntry      *pGipEntry;
    tGarpIfMsg          IfMsg;
    UINT4               u4Index;
    INT4                i4RetVal = GARP_TRUE;
    UINT4               u4NumAttrEntries;
    UINT4               u4DelAttrCount;

    pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

    if (pGipEntry == NULL)
    {
        return;
    }

    if (pGipEntry->u1StapStatus == GARP_BLOCKING)
    {
        /*
         * Old STAP status equal to new STAP status.
         */
        return;
    }

    u4NumAttrEntries = pGipEntry->u4NumEntries;
    u4DelAttrCount = 0;

    pGipEntry->u1StapStatus = GARP_BLOCKING;

    for (u4Index = 0; u4Index < pAppEntry->u2MaxBuckets; u4Index++)
    {
        pAttrEntry = pGipEntry->papAttrTable[u4Index];

        while (pAttrEntry != NULL)
        {

            if (pAttrEntry->u1RegSemState == GARP_IN)
            {

                /*
                 * Previously when the STAP status of a port goes from FORWARDING
                 * to BLOCKING ,leave is propogated on to all ports only if no port
                 * has registration FIXED for that attribute.But as per IEEE802.1u
                 * implementation LEAVE should be propogated on all ports only if no 
                 * port has its Registrar State Machine to be IN state for that attribute .
                 * ie.,even if that attribute has been learnt dynamically on any one of the ports
                 * LEAVE should not be propogated */

                i4RetVal = GarpIsAttrAdminRegFixed (pAppEntry,
                                                    &pAttrEntry->Attr,
                                                    pGipEntry->u2GipId,
                                                    pPortEntry->u2Port);

                if (i4RetVal == GARP_FALSE)
                {

                    /* 
                     * This attribute entry is not registered in any other
                     * ports with Admin Reg Control as REG_FIXED. So send
                     * leave empty message on all ports for this attribute.
                     * Refer. 802.1D 1998, Sec. 12.3.3.d
                     */
                    IfMsg.pAppEntry = pAppEntry;
                    IfMsg.u2Port = pPortEntry->u2Port;
                    IfMsg.u2GipId = pGipEntry->u2GipId;

                    GarpGipPropagateOnAllPorts (&pAttrEntry->Attr,
                                                &IfMsg, GARP_SEND_LEAVE_MT);
                }
            }

            /* Making all the Applicants as Observers */
            GarpApplicantSem (pAttrEntry, pPortEntry, GARP_APP_REQ_LEAVE);

            pTmpAttrEntry = pAttrEntry;
            pAttrEntry = pAttrEntry->pNextHashNode;

            if (GarpCheckAndDelAttrEntry (pAppEntry, pPortEntry,
                                          pGipEntry,
                                          pTmpAttrEntry) == GARP_TRUE)
            {
                u4DelAttrCount++;
            }
        }

        if (u4NumAttrEntries == u4DelAttrCount)
        {
            /* 
             * All the entries in the Attribute Hash table are
             * deleted. As a result of this, the Gip entry
             * would be deleted. So we should not scan the remaining
             * buckets
             */

            break;
        }
    }
}

/**************************************************************/
/*  Function Name   : GarpSendLeaveAll                         */
/*  Description     : This function sends leaveall message to  *
 *                    all the available ports                  */
/*  Input(s)        : pAppEntry - Pointer to Application Entry */
/*                    pPortEntry - Pointer to the port entry   */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/

void
GarpSendLeaveAll (tGarpAppEntry * pAppEntry, tGarpPortEntry * pPortEntry)
{
    tGarpAttrEntry     *pAttrEntry;
    tGarpAttrEntry     *pNextAttrEntry;
    tGarpGipEntry      *pGipEntry;
    tGarpGipEntry      *pNextGipEntry;
    tGarpGlobalPortEntry *pGlobalPortEntry;
    tGarpAttrType       AttrType;
    tMacAddr            MacAddr;
    UINT4               u4Index;
    UINT4               u4NumAttrEntries;
    UINT4               u4DelAttrCount;
    UINT4               u4HashIndex = 0;
    UINT2               u2GipId;
    UINT1               au1Buf[GARP_MAX_LEAVE_ALL_MSG_LEN];
    UINT1              *pu1Ptr;
    UINT1               u1IsBuffEmpty = GARP_TRUE;
    UINT1               au1AttrTypesPresent[GARP_ATTR_TYPE_END];
    UINT1               au1AttrTypesSent[GARP_ATTR_TYPE_END];
    UINT1               u1IsGarpStatsReq = GARP_TRUE;
    UINT1               u1IsGvrp = GARP_FALSE;

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    pGlobalPortEntry = GARP_PORT_ENTRY (pPortEntry->u2Port);

    if (pGlobalPortEntry == NULL)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Port unknown. Received Garp PDU dropped.\n") return;
        return;
    }

    if (pGlobalPortEntry->pGarpPortStats == NULL)
    {
        u1IsGarpStatsReq = GARP_FALSE;
    }

    GARP_COPY_MAC_ADDR (MacAddr, pAppEntry->AppAddress);

    if (VLAN_DESTADDR_IS_GVRPADDR (MacAddr) == VLAN_TRUE)
    {
        u1IsGvrp = GARP_TRUE;
    }

    MEMSET (au1AttrTypesSent, 0, sizeof (au1AttrTypesSent));

    if (pPortEntry->pGipTable != NULL)
    {
        TMO_HASH_Scan_Table (pPortEntry->pGipTable, u4HashIndex)
        {
            GARP_HASH_DYN_Scan_Bucket (pPortEntry->pGipTable, u4HashIndex,
                                       pGipEntry, pNextGipEntry,
                                       tGarpGipEntry *)
            {
                u2GipId = pGipEntry->u2GipId;

                u4NumAttrEntries = pGipEntry->u4NumEntries;
                u4DelAttrCount = 0;

                pu1Ptr = au1Buf + GARP_HDR_SIZE;
                MEMSET (au1AttrTypesPresent, 0, sizeof (au1AttrTypesPresent));

                for (u4Index = 0; u4Index < pAppEntry->u2MaxBuckets; u4Index++)
                {
                    pAttrEntry = pGipEntry->papAttrTable[u4Index];

                    while (pAttrEntry != NULL)
                    {

                        pNextAttrEntry = pAttrEntry->pNextHashNode;

                        GarpApplicantSem (pAttrEntry, pPortEntry,
                                          GARP_APP_LEAVE_ALL);

                        if (pAttrEntry->u1AdmRegControl == GARP_REG_NORMAL)
                        {
                            au1AttrTypesPresent[pAttrEntry->Attr.u1AttrType] =
                                1;

                            GarpRegistrarSem (pAppEntry, pAttrEntry,
                                              pPortEntry->u2Port,
                                              GARP_REG_LEAVE_ALL);

                            if (GarpCheckAndDelAttrEntry (pAppEntry, pPortEntry,
                                                          pGipEntry,
                                                          pAttrEntry) ==
                                GARP_TRUE)
                            {
                                u4DelAttrCount++;
                            }
                        }
                        /*
                         * During Standby to Active transition, leave all 
                         * message needs to be transmitted through all the
                         * ports irrespective of the registration type. 
                         */
                        if (gu1GarpStandbyToActive == GARP_TRUE)
                        {
                            au1AttrTypesPresent[pAttrEntry->Attr.u1AttrType] =
                                1;
                        }

                        pAttrEntry = pNextAttrEntry;
                    }

                    if (u4NumAttrEntries == u4DelAttrCount)
                    {
                        /* 
                         * All the entries in the Attribute Hash table are
                         * deleted. As a result of this, the Gip entry
                         * would be deleted. So we should not scan the remaining
                         * buckets
                         */

                        break;
                    }
                }

                for (AttrType = GARP_GROUP_ATTR_TYPE;
                     AttrType < GARP_ATTR_TYPE_END; AttrType++)
                {
                    if ((au1AttrTypesPresent[AttrType] == 1) &&
                        (au1AttrTypesSent[AttrType] == 0))
                    {
                        u1IsBuffEmpty = GARP_FALSE;

                        *pu1Ptr = (UINT1) AttrType;
                        pu1Ptr++;

                        *pu1Ptr = GARP_ATTR_EVENT_SIZE + GARP_ATTR_LEN_SIZE;
                        pu1Ptr++;

                        *pu1Ptr = GARP_LEAVE_ALL;
                        pu1Ptr++;

                        *pu1Ptr = GARP_END_MARK;    /* End of Attribute List */
                        pu1Ptr++;

                        au1AttrTypesSent[AttrType] = 1;
                        if (u1IsGarpStatsReq == GARP_TRUE)
                        {
                            if (u1IsGvrp == GARP_TRUE)
                            {
                                pGlobalPortEntry->pGarpPortStats->
                                    u4GvrpLeaveAllTxCount++;
                            }
                            else if (AttrType == GARP_SERVICE_REQ_ATTR_TYPE)
                            {
                                pGlobalPortEntry->pGarpPortStats->
                                    u4GmrpLeaveAllTxCount++;
                            }
                        }
                    }
                }

                if (u1IsBuffEmpty == GARP_FALSE)
                {
                    *pu1Ptr = GARP_END_MARK;
                    pu1Ptr++;

                    GarpTransmitPdu (au1Buf, (UINT2) (pu1Ptr - au1Buf),
                                     pAppEntry->AppAddress,
                                     pPortEntry->u2Port, u2GipId);

                    u1IsBuffEmpty = GARP_TRUE;
                }
            }
        }
    }
}

/***************************************************************/
/*  Function Name   : GarpJoinTimerExpiry                      */
/*  Description     : Function to handle JoinTimer Expiry event*/
/*  Input(s)        : u1AppId - Application Id                 *
 *                    pPortEntry - Pointer to Port Entry       */
/*  Output(s)       : None                                     */
/*  <OPTIONAL Fields>:    None                                  */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion :None                                     */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpJoinTimerExpiry (UINT1 u1AppId, tGarpPortEntry * pPortEntry)
{
    tGarpAppEntry      *pAppEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpAttrEntry     *pNextAttrEntry;
    tGarpGipEntry      *pGipEntry;
    tGarpGipEntry      *pNextGipEntry;
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;
    tGarpSendMsgInd     u1SendMsgInd;
    tMacAddr            MacAddr;
    UINT4               u4Count;
    UINT4               u4NumAttrEntries;
    UINT4               u4NumGipEntries;
    UINT4               u4GipEntryCount = 0;
    UINT4               u4DelAttrCount;
    UINT4               u4HashIndex = 0;
    UINT2               u2Offset = GARP_HDR_SIZE;
    UINT2               u2Index;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1Ptr;
    UINT1               u1Event = GARP_INVALID_EVENT;
    UINT1               u1PrevAttrType = GARP_INVALID_ATTR_TYPE;
    UINT1               u1IsDataPresent = GARP_FALSE;
    UINT1               u1IsGarpStatsReq = GARP_TRUE;
    UINT1               u1IsGvrp = GARP_FALSE;

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    pGlobalPortEntry = GARP_PORT_ENTRY (pPortEntry->u2Port);

    if (pGlobalPortEntry == NULL)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Port unknown. Received Garp PDU dropped.\n") return;
        return;
    }
    if (pGlobalPortEntry->pGarpPortStats == NULL)
    {
        u1IsGarpStatsReq = GARP_FALSE;
    }

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {

        GARP_TRC (GARP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  GARP_NAME, "Join Timer Expiry : Unknown AppId.\n");

        return;
    }

    GARP_TRC_ARG1 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                   "Join Timer Expired for Port %d.\n",
                   GARP_GET_IFINDEX (pPortEntry->u2Port));

    if (pPortEntry->u1LeaveAllSemState == GARP_ACTIVE)
    {

        GarpSendLeaveAll (pAppEntry, pPortEntry);

        pPortEntry->u1LeaveAllSemState = GARP_PASSIVE;

        return;
    }

    /* Allocate memory for GARP message */
    pu1Buf = MemAllocMemBlk (gGarpPoolIds.GarpRemapTxBufPoolId);
    if (pu1Buf == NULL)
    {
        GARP_TRC (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  "Join Timer Expiry : Memory allocation for GARP message failed\n");
        return;
    }
    pu1Ptr = pu1Buf + GARP_HDR_SIZE;

    GARP_COPY_MAC_ADDR (MacAddr, pAppEntry->AppAddress);

    if (VLAN_DESTADDR_IS_GVRPADDR (MacAddr) == VLAN_TRUE)
    {
        u1IsGvrp = GARP_TRUE;
    }

    if (pPortEntry->pGipTable != NULL)
    {
        u4NumGipEntries = pPortEntry->u4NumGipEntries;

        TMO_HASH_Scan_Table (pPortEntry->pGipTable, u4HashIndex)
        {
            GARP_HASH_DYN_Scan_Bucket (pPortEntry->pGipTable, u4HashIndex,
                                       pGipEntry, pNextGipEntry,
                                       tGarpGipEntry *)
            {

                u4DelAttrCount = 0;
                u4NumAttrEntries = pGipEntry->u4NumEntries;
                u4Count = 0;
                u4GipEntryCount++;

                for (u2Index = 0; u2Index < pAppEntry->u2MaxBuckets; u2Index++)
                {

                    pAttrEntry = pGipEntry->papAttrTable[u2Index];

                    while (pAttrEntry != NULL)
                    {

                        pNextAttrEntry = pAttrEntry->pNextHashNode;

                        u4Count++;

                        u1SendMsgInd = GarpApplicantSem (pAttrEntry, pPortEntry,
                                                         GARP_APP_TRANSMIT_PDU);

                        switch (u1SendMsgInd)
                        {

                            case GARP_SEND_JOIN:

                                if (pAttrEntry->u1RegSemState == GARP_IN)
                                {

                                    GARP_TRC_ARG2 (GARP_MOD_TRC,
                                                   CONTROL_PLANE_TRC,
                                                   GARP_NAME,
                                                   "Sending JOIN IN on"
                                                   " the Port %d for  GIP %d.\n",
                                                   GARP_GET_IFINDEX
                                                   (pPortEntry->u2Port),
                                                   pGipEntry->u2GipId);

                                    u1Event = GARP_JOIN_IN;
                                    if (u1IsGarpStatsReq == GARP_TRUE)
                                    {
                                        if (u1IsGvrp == GARP_TRUE)
                                        {
                                            pGlobalPortEntry->pGarpPortStats->
                                                u4GvrpJoinInTxCount++;
                                        }
                                        else
                                        {
                                            pGlobalPortEntry->pGarpPortStats->
                                                u4GmrpJoinInTxCount++;
                                        }
                                    }
                                }
                                else
                                {

                                    GARP_TRC_ARG2 (GARP_MOD_TRC,
                                                   CONTROL_PLANE_TRC,
                                                   GARP_NAME,
                                                   "Sending JOIN EMPTY on"
                                                   " the Port %d for  GIP %d.\n",
                                                   GARP_GET_IFINDEX
                                                   (pPortEntry->u2Port),
                                                   pGipEntry->u2GipId);

                                    u1Event = GARP_JOIN_EMPTY;
                                    if (u1IsGarpStatsReq == GARP_TRUE)
                                    {
                                        if (u1IsGvrp == GARP_TRUE)
                                        {
                                            pGlobalPortEntry->pGarpPortStats->
                                                u4GvrpJoinEmptyTxCount++;
                                        }
                                        else
                                        {
                                            pGlobalPortEntry->pGarpPortStats->
                                                u4GmrpJoinEmptyTxCount++;
                                        }
                                    }
                                }

                                GARP_PRINT_ATTRIBUTE (&pAttrEntry->Attr);

                                break;

                            case GARP_SEND_LEAVE_MT:

                                GARP_TRC_ARG2 (GARP_MOD_TRC, CONTROL_PLANE_TRC,
                                               GARP_NAME,
                                               "Sending LEAVE EMPTY on"
                                               " the Port %d for  GIP %d.\n",
                                               GARP_GET_IFINDEX (pPortEntry->
                                                                 u2Port),
                                               pGipEntry->u2GipId);

                                GARP_PRINT_ATTRIBUTE (&pAttrEntry->Attr);

                                u1Event = GARP_LEAVE_EMPTY;
                                if (u1IsGarpStatsReq == GARP_TRUE)
                                {
                                    if (u1IsGvrp == GARP_TRUE)
                                    {
                                        pGlobalPortEntry->pGarpPortStats->
                                            u4GvrpLeaveEmptyTxCount++;
                                    }
                                    else
                                    {
                                        pGlobalPortEntry->pGarpPortStats->
                                            u4GmrpLeaveEmptyTxCount++;
                                    }
                                }
                                break;

                            case GARP_SEND_MT:

                                GARP_TRC_ARG2 (GARP_MOD_TRC, CONTROL_PLANE_TRC,
                                               GARP_NAME,
                                               "Sending EMPTY on"
                                               " the Port %d for GIP %d.\n",
                                               GARP_GET_IFINDEX (pPortEntry->
                                                                 u2Port),
                                               pGipEntry->u2GipId);

                                GARP_PRINT_ATTRIBUTE (&pAttrEntry->Attr);

                                u1Event = GARP_EMPTY;
                                if (u1IsGarpStatsReq == GARP_TRUE)
                                {
                                    if (u1IsGvrp == GARP_TRUE)
                                    {
                                        pGlobalPortEntry->pGarpPortStats->
                                            u4GvrpEmptyTxCount++;
                                    }
                                    else
                                    {
                                        pGlobalPortEntry->pGarpPortStats->
                                            u4GmrpEmptyTxCount++;
                                    }
                                }
                                break;

                            case GARP_SEND_NONE:
                                break;

                            default:

                                pAttrEntry = pNextAttrEntry;
                                continue;
                        }

                        if (pGipEntry->u1StapStatus == GARP_FORWARDING)
                        {

                            if (u1SendMsgInd != GARP_SEND_NONE)
                            {

                                if (u1PrevAttrType == GARP_INVALID_ATTR_TYPE)
                                {

                                    /* 
                                     * The current attribute is the first 
                                     * attribute for 
                                     * this GIP in the buffer.
                                     */
                                    *pu1Ptr = pAttrEntry->Attr.u1AttrType;
                                    pu1Ptr++;

                                    u1PrevAttrType =
                                        pAttrEntry->Attr.u1AttrType;
                                }
                                else
                                {

                                    if (u1PrevAttrType !=
                                        pAttrEntry->Attr.u1AttrType)
                                    {

                                        /* 
                                         * Current Attribute is a new attribute. 
                                         * Add an end mark to terminate the 
                                         * previous attribute list.
                                         */

                                        *pu1Ptr = GARP_END_MARK;
                                        pu1Ptr++;

                                        *pu1Ptr = pAttrEntry->Attr.u1AttrType;
                                        pu1Ptr++;

                                        u1PrevAttrType =
                                            pAttrEntry->Attr.u1AttrType;
                                    }
                                }

                                *pu1Ptr =
                                    (UINT1) (pAttrEntry->Attr.u1AttrLen +
                                             GARP_ATTR_EVENT_SIZE +
                                             GARP_ATTR_LEN_SIZE);

                                pu1Ptr++;

                                *pu1Ptr = u1Event;
                                pu1Ptr++;

                                if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
                                {
                                    GarpPbTransAttrValAndCopy (pPortEntry->
                                                               u2Port, pu1Ptr,
                                                               &(pAttrEntry->
                                                                 Attr));
                                }
                                else
                                {
                                    MEMCPY (pu1Ptr, pAttrEntry->Attr.au1AttrVal,
                                            pAttrEntry->Attr.u1AttrLen);
                                }

                                pu1Ptr = pu1Ptr + pAttrEntry->Attr.u1AttrLen;

                                u2Offset = (UINT2) (pu1Ptr - pu1Buf);

                                u1IsDataPresent = GARP_TRUE;
                            }
                        }        /* Port Status Check */

                        /* When Data is present, pkt has to be sent out either
                         * when the buffer is full or for GVRP when all gips are
                         * processed, for GMRP when all attrs are processed */
                        if ((u1IsDataPresent == GARP_TRUE)
                            &&
                            (((GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId == u1AppId
                               && u4Count == u4NumAttrEntries)
                              || (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId ==
                                  u1AppId && u4Count == u4NumAttrEntries
                                  && u4GipEntryCount == u4NumGipEntries))
                             || (u2Offset + GARP_MAX_ATTR_SIZE >
                                 GARP_MAX_MSG_LEN)))
                        {

                            /* 
                             * Either there are no more attributes for 
                             * this GIP Id or there is no space available 
                             * in this buffer for the next attribute. 
                             * Hence send the current buffer.
                             */

                            *pu1Ptr = GARP_END_MARK;    /* End of Attribute List */
                            pu1Ptr++;
                            *pu1Ptr = GARP_END_MARK;    /* End of Message List */
                            pu1Ptr++;

                            u2Offset = (UINT2) (pu1Ptr - pu1Buf);

                            GarpTransmitPdu (pu1Buf, u2Offset,
                                             pAppEntry->AppAddress,
                                             pPortEntry->u2Port,
                                             pGipEntry->u2GipId);

                            pu1Ptr = pu1Buf + GARP_HDR_SIZE;
                            u1PrevAttrType = GARP_INVALID_ATTR_TYPE;
                            u1IsDataPresent = GARP_FALSE;
                        }

                        if (GarpCheckAndDelAttrEntry (pAppEntry, pPortEntry,
                                                      pGipEntry,
                                                      pAttrEntry) == GARP_TRUE)
                        {
                            u4DelAttrCount++;
                        }

                        pAttrEntry = pNextAttrEntry;
                    }

                    if (u4NumAttrEntries == u4DelAttrCount)
                    {
                        /* 
                         * All the entries in the Attribute Hash table are
                         * deleted. As a result of this, the Gip entry
                         * would be deleted. So we should not scan the remaining
                         * buckets
                         */

                        break;
                    }
                }
            }
        }
    }
    /* De-allocate memory for GARP message */
    MemReleaseMemBlock (gGarpPoolIds.GarpRemapTxBufPoolId, pu1Buf);
}

/***************************************************************/
/*  Function Name   :GarpLeavTimerExpiry                       */
/*  Description     :This function is called whenever a leave  *
 *                   timer expiry even occurs                  */
/*  Input(s)        :u1AppId - Application Id                  *
 *                   u2Port - Port for which the leave timer   * 
 *                   had expired                               *
 *                   pAttrEntry - Pointer to the attribute     *
 *                   whose Leave Timer had expired.            *
 *                                                             */
/*  Output(s)       :None                                      */
/*  <OPTIONAL Fields>:None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion :None                                     */
/*  Returns         :None                                      */
/***************************************************************/
void
GarpLeaveTimerExpiry (UINT1 u1AppId, UINT2 u2Port, tGarpAttrEntry * pAttrEntry)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tGarpIfMsg          IfMsg;
    tGarpGipEntry      *pGipEntry = NULL;

    GARP_TRC_ARG2 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                   "Leave Timer Expired for AppId = %d, Port = %d.\n",
                   u1AppId, GARP_GET_IFINDEX (u2Port));

    GARP_PRINT_ATTRIBUTE (&pAttrEntry->Attr);

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {

        return;
    }

    pPortEntry = GarpGetPortEntry (pAppEntry, u2Port);

    if (pPortEntry == NULL)
    {

        return;
    }

    if (pAttrEntry->u1AdmRegControl == GARP_REG_NORMAL)
    {

        GarpRegistrarSem (pAppEntry,
                          pAttrEntry, u2Port, GARP_REG_LEAVE_TIMER_EXPIRY);

        /* 
         * Indicate the leave timer expiry for the attribute to
         * other ports. 
         */
        pGipEntry = pAttrEntry->pGipEntry;

        if (pGipEntry->u1StapStatus == GARP_FORWARDING)
        {

            IfMsg.pAppEntry = pAppEntry;
            IfMsg.u2GipId = pGipEntry->u2GipId;
            IfMsg.u2Port = u2Port;

            /* 
             * Propagate this deregistration message to other ports 
             */

            GarpGipGidAttributeLeaveInd (&pAttrEntry->Attr, &IfMsg);
        }

        GarpCheckAndDelAttrEntry (pAppEntry, pPortEntry, pGipEntry, pAttrEntry);
    }
}

/***************************************************************/
/*  Function Name   : GarpLeaveAllTimerExpiry                  */
/*  Description     : Thif function is called in the event of  *
 *                   LeaveAllTimer expiry                      */
/*  Input(s)        : pPortEntry - Pointer to the port entry   */
/*  Output(s)       : None                                     */
/*  <OPTIONAL Fields>: None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpLeaveAllTimerExpiry (tGarpPortEntry * pPortEntry)
{
    UINT4               u4LeaveAllTime;
    UINT4               u4JoinTime;
    UINT4               u4RandJoinTime;
    UINT4               u4RandLeaveAllTime;

    pPortEntry->u1LeaveAllSemState = GARP_ACTIVE;

    u4LeaveAllTime =
        (GARP_CURR_CONTEXT_PTR ()->apGarpGlobalPortEntry[pPortEntry->u2Port])->
        u4LeaveAllTime;

    /* The leave all timer expiry event should happen at some random time
     * in the interval between configured leave all time and 1.5 * configured
     * leave all time. 
     */
    GARP_GET_RANDOM_LEAVEALL_TIME (u4LeaveAllTime, u4RandLeaveAllTime);

    GarpStartTimer (&pPortEntry->LeaveAllTmr, u4RandLeaveAllTime);

    if (pPortEntry->JoinTmr.u1IsTmrActive == GARP_FALSE)
    {

        u4JoinTime =
            (GARP_CURR_CONTEXT_PTR ()->
             apGarpGlobalPortEntry[pPortEntry->u2Port])->u4JoinTime;

        GARP_RANDOM_TIMEOUT (u4JoinTime, u4RandJoinTime);

        GarpStartTimer (&pPortEntry->JoinTmr, u4RandJoinTime);
    }

    pPortEntry->LeaveAllTmr.u1IsTmrActive = GARP_TRUE;
}

/***************************************************************/
/*  Function Name   : GarpRegistrarSem                         */
/*  Description     : Thif function is called in the event of  *
 *                   incoming event and their is a change in   *
 *                   Registrar sem                             */
/*  Input(s)        : pAppEntry - Pointer to the Application   *
 *                   entry                                     * 
 *                    pAttrEntry - Pointer to the attribute    *
 *                   entry                                     *
 *                    u2InPort - Port whose registrar sem is to*
 *                   updated                                   *
 *                    u1SemEvent - Incoming Sem event          *
 *                                                             */
/*  Output(s)       : None                                     */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpRegistrarSem (tGarpAppEntry * pAppEntry,
                  tGarpAttrEntry * pAttrEntry, UINT2 u2InPort, UINT1 u1SemEvent)
{
    tGarpRegSemEntry   *pRegSemEntry;
    UINT1               u1TmpRegSemState;
    INT4                i4RetVal;

    pRegSemEntry = &gaGarpRegSem[u1SemEvent][pAttrEntry->u1RegSemState];
#ifdef TRACE_WANTED
    {
        UINT1               au1RegEvtName[GARP_MAX_REG_EVENTS][40] = {
            "RCV_JOIN_IN",
            "RCV_JOIN_MT",
            "RCV_MT",
            "RCV_LEAVE_IN",
            "RCV_LEAVE_MT",
            "LEAVE_ALL",
            "LEAVE_TIMER_EXPIRY"
        };

        UINT1               au1StateName[GARP_MAX_REG_STATES][10] = {
            "IN",
            "LV",
            "MT",
        };

        if (gu1GarpTestSemDbg == 1)
        {
            GARP_TRC (GARP_MOD_TRC, DATA_PATH_TRC, "", "\n");

            GARP_TRC (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                      "!!!!!!!!!!!!!!! REG Sem State Transition !!!!!!!!!!!!!!\n");

            if (u2InPort >= (GARP_MAX_PORTS_PER_CONTEXT + 1))
            {
                return;
            }
            GARP_TRC_ARG2 (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                           "\tGip Id = %d,\tPort = %d\n",
                           GARP_ATTR_GIPID (pAttrEntry),
                           GARP_GET_IFINDEX (u2InPort));

            GARP_PRINT_ATTRIBUTE (&pAttrEntry->Attr);

            GARP_TRC_ARG1 (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                           "\tEvent          = %s \n",
                           au1RegEvtName[u1SemEvent]);
            GARP_TRC_ARG1 (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                           "\tPrevious State = %s\n",
                           au1StateName[pAttrEntry->u1RegSemState]);

            GARP_TRC_ARG1 (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                           "\tNext State     = %s\n",
                           au1StateName[pRegSemEntry->u4NextState]);

            GARP_TRC (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                      "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
        }
    }
#endif

    u1TmpRegSemState = pAttrEntry->u1RegSemState;

    pAttrEntry->u1RegSemState = (UINT1) pRegSemEntry->u4NextState;

    switch (pRegSemEntry->LeaveTmrInd)
    {

        case GARP_TIMER_START:
            if (u2InPort >= (GARP_MAX_PORTS_PER_CONTEXT + 1))
            {
                return;
            }
            GarpStartTimer (&pAttrEntry->LeaveTmr,
                            GARP_CURR_CONTEXT_PTR ()->
                            apGarpGlobalPortEntry[u2InPort]->u4LeaveTime);

            break;

        case GARP_TIMER_STOP:
            GarpStopTimer (&pAttrEntry->LeaveTmr);
            break;

        default:
            break;
    }

    switch (pRegSemEntry->HlMsgInd)
    {
        case GARP_HL_IND_JOIN:

            i4RetVal = pAppEntry->pJoinIndFn (pAttrEntry->Attr.u1AttrType,
                                              pAttrEntry->Attr.u1AttrLen,
                                              pAttrEntry->Attr.au1AttrVal,
                                              u2InPort,
                                              GARP_ATTR_GIPID (pAttrEntry));

            if (i4RetVal == GARP_FAILURE)
            {
                pAttrEntry->u1RegSemState = u1TmpRegSemState;
            }

            break;

        case GARP_HL_IND_LEAVE:
            pAppEntry->pLeaveIndFn (pAttrEntry->Attr.u1AttrType,
                                    pAttrEntry->Attr.u1AttrLen,
                                    pAttrEntry->Attr.au1AttrVal,
                                    u2InPort, GARP_ATTR_GIPID (pAttrEntry));
            break;

        default:
            break;
    }
}

/***************************************************************/
/*  Function Name   : GarpApplicantSem                         */
/*  Description     : Thif function is called in the event of  *
 *                   incoming event and if their is a change in* 
 *                   Applicant sem                             */
/*  Input(s)        : pAppEntry - Pointer to the Application   *
 *                   entry                                     * 
 *                    pAttrEntry - Pointer to the attribute    *
 *                   entry                                     *
 *                    u2InPort - Port whose Applicant sem is to*
 *                   updated                                   *
 *                    u1SemEvent - Incoming Sem event          */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
tGarpSendMsgInd
GarpApplicantSem (tGarpAttrEntry * pAttrEntry,
                  tGarpPortEntry * pPortEntry, UINT1 u1SemEvent)
{
    tGarpAppSemEntry   *pAppSemEntry;
    UINT4               u4JoinTime;
    UINT4               u4RandJoinTime;

    pAppSemEntry = &gaGarpAppSem[u1SemEvent][pAttrEntry->u1AppSemState];

#ifdef TRACE_WANTED
    {
        UINT1               au1AppEvtName[GARP_MAX_APP_EVENTS][40] = {
            "TRANSMIT_PDU",
            "RCV_JOIN_IN",
            "RCV_JOIN_MT",
            "RCV_MT",
            "RCV_LEAVE_IN",
            "RCV_LEAVE_MT",
            "LEAVE_ALL",
            "REQ_JOIN",
            "REQ_LEAVE"
        };

        UINT1               au1StateName[GARP_MAX_APP_STATES][10] = {
            "VA",
            "AA",
            "QA",
            "LA",
            "VP",
            "AP",
            "QP",
            "VO",
            "AO",
            "QO",
            "LO"
        };

        if (gu1GarpTestSemDbg == 1)
        {

            GARP_TRC (GARP_MOD_TRC, DATA_PATH_TRC, "", "\n");

            GARP_TRC (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                      "*************** APP Sem State Transition **************\n");

            GARP_TRC_ARG2 (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                           "\tGip Id = %d,\tPort = %d\n",
                           GARP_ATTR_GIPID (pAttrEntry),
                           GARP_GET_IFINDEX (pPortEntry->u2Port));

            GARP_PRINT_ATTRIBUTE (&pAttrEntry->Attr);

            GARP_TRC_ARG1 (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                           "\tEvent          = %s\n",
                           au1AppEvtName[u1SemEvent]);

            GARP_TRC_ARG1 (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                           "\tPrevious State = %s\n",
                           au1StateName[pAttrEntry->u1AppSemState]);

            GARP_TRC_ARG1 (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                           "\tNext State     = %s\n",
                           au1StateName[pAppSemEntry->u4NextState]);

            GARP_TRC (GARP_MOD_TRC, DATA_PATH_TRC, GARP_NAME,
                      "*******************************************************\n\n");
        }
    }
#endif

    pAttrEntry->u1AppSemState = (UINT1) pAppSemEntry->u4NextState;

    if (pAppSemEntry->u4JoinTmrInd == GARP_TRUE)
    {

        if (pPortEntry->JoinTmr.u1IsTmrActive == GARP_FALSE)
        {

            u4JoinTime =
                GARP_CURR_CONTEXT_PTR ()->apGarpGlobalPortEntry[pPortEntry->
                                                                u2Port]->
                u4JoinTime;

            GARP_RANDOM_TIMEOUT (u4JoinTime, u4RandJoinTime);

            GarpStartTimer (&pPortEntry->JoinTmr, u4RandJoinTime);
#ifdef L2RED_WANTED
            /* HITLESS RESTART */
            if (u1SemEvent == GARP_APP_TRANSMIT_PDU)
            {
                if (GARP_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE)
                {
                    /* This periodic timer expired(timer value = 0) had happened when
                     * RM needs steady state packets.
                     * To avoid of sending more than 1 steady state pkt per port this
                     * check is added. It prevents starting the periodic timer with
                     * zero value  again and again*/

                    /* Since Join Packet should be sent to maintain steady state,
                     * return with GARP_SEND_JOIN as SendMsgInd 
                     */
                    return GARP_SEND_JOIN;
                }
            }
#endif

        }
    }
    return (pAppSemEntry->SendMsgInd);
}

/****************************************************************************/
/* Function Name    : GarpTransmitPdu ()                                    */
/*                                                                          */
/* Description      : Transmits the PDU after filling the header.           */
/*                    is returned                                           */
/*                                                                          */
/* Input(s)         : pu1Data - Pointer to the flat buffer containing       */
/*                              space for header.                           */
/*                    u2BufLen - Length of the PDU including the header     */
/*                    pDstAddr - The destination address.                   */
/*                    u2Port   - The port on which the PDU is to be sent.   */
/*                    u2GipId  - The Gip Id associated with the PDU.        */
/*                    u1PduType  The GARP PDU Type.                         */
/*                                                                          */
/* Output(s)        : PDU is transmitted.                                   */
/*                                                                          */
/* Global Variables                                                         */
/*   Referred       : None.                                                 */
/*                                                                          */
/* Global Variables                                                         */
/*   Modified       : None.                                                 */
/*                                                                          */
/* Exceptions or OS                                                         */
/*   Error Handling : None.                                                 */
/*                                                                          */
/* Returns          : None.                                                 */
/****************************************************************************/
void
GarpTransmitPdu (UINT1 *pu1Data, UINT2 u2BufLen, tMacAddr DstAddr,
                 UINT2 u2Port, UINT2 u2GipId)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT1               au1Llc[GARP_LLC_SIZE] = { 0x42, 0x42, 0x03 };
    UINT2               u2Pid;
    UINT2               u2TypeLen;
    UINT1              *pu1StartAddr;
    tCfaIfInfo          IfInfo;
    UINT2               u2Len;
    INT4                i4Result;
    tMacAddr            TempDstAddr;

    MEMSET (TempDstAddr, 0, sizeof (tMacAddr));
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    pGlobalPortEntry = GARP_PORT_ENTRY (u2Port);
    if (pGlobalPortEntry->u1OperStatus == GARP_OPER_DOWN)
    {
        return;
    }

    if (GarpCfaGetIfInfo (pGlobalPortEntry->u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return;
    }

    pBuf = (tCRU_BUF_CHAIN_HEADER *) (VOID *)
        GARP_GET_BUFF (GARP_SYS_BUFF, u2BufLen);

    if (pBuf == NULL)
    {

        return;
    }

    /*Since the DstAddr received here was AppEntry->AppMacAddress, we
     * should not change the AppMacAddress, So copy the AppMacAddress into
     * the temporary destination address and proceed*/
    MEMCPY (TempDstAddr, DstAddr, sizeof (tMacAddr));

    GarpPbEgrChangeGvrpAddrOnPpnp (u2Port, TempDstAddr);

    pu1StartAddr = pu1Data;

    /* 
     * The Parameter u2BufLen is the length of the total
     * Ethernet Frame including the Ethernet header. The Type Length
     * field set in the frame, must be the length of the Ethernet data
     * following the Type/Length field in the Ethernet Header. Hence 
     * u2TypeLen is subtracted by GARP_LLC_OFFSET (14) and stored in the 
     * variable "u2TypeLen".  
     */
    u2TypeLen = (UINT2) (u2BufLen - GARP_LLC_OFFSET);

    GARP_MAC_ADDR_TO_ARRAY (pu1Data, TempDstAddr);

    pu1Data = pu1Data + GARP_MAC_ADDR_LEN;

    MEMCPY (pu1Data, IfInfo.au1MacAddr, GARP_MAC_ADDR_LEN);

    pu1Data = pu1Data + GARP_MAC_ADDR_LEN;

    /* Type/Len field */
    u2TypeLen = (UINT2) (OSIX_HTONS (u2TypeLen));
    MEMCPY (pu1Data, &u2TypeLen, GARP_TYPE_LEN_SIZE);
    pu1Data = pu1Data + GARP_TYPE_LEN_SIZE;

    MEMCPY (pu1Data, au1Llc, GARP_LLC_SIZE);
    pu1Data = pu1Data + GARP_LLC_SIZE;

    u2Pid = OSIX_HTONS (GARP_PID);
    MEMCPY (pu1Data, &u2Pid, GARP_PID_SIZE);
    pu1Data = pu1Data + GARP_PID_SIZE;

    GARP_COPY_TO_SYS_BUFF (pBuf, pu1StartAddr, 0, u2BufLen);

    if (0 == (MEMCMP (TempDstAddr, gGmrpAddr, GARP_MAC_ADDR_LEN)))
    {

        i4Result =
            GarpVlanCheckAndTagOutgoingGmrpFrame (GARP_CURR_CONTEXT_PTR ()->
                                                  u4ContextId, pBuf,
                                                  (tVlanId) u2GipId, u2Port);

        if (i4Result == VLAN_NO_FORWARD)
        {
            GARP_RELEASE_BUFF (GARP_SYS_BUFF, pBuf);

            return;
        }
    }

    u2Len = (UINT2) GARP_GET_BUF_LEN (pBuf);

#ifdef L2RED_WANTED
    /* HITLESS RESTART */
    if (GARP_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE)
    {
        if (GarpRedHRSendStdyStPkt (pu1StartAddr,
                                    u2Len, u2Port,
                                    pGlobalPortEntry->u4JoinTime *
                                    GARP_CENT_SEC_TO_MILLI_SEC_UNIT)
            /* Join Time is converted from centi seconds to milliseconds */
            == OSIX_FAILURE)
        {
            GARP_TRC (GARP_RED_TRC, ALL_FAILURE_TRC, GARP_NAME,
                      "GARP HR: Sending steady state packet to RM failed\r\n");
        }
        GARP_RELEASE_BUFF (GARP_SYS_BUFF, pBuf);
        return;
    }
#endif
    if (GARP_NODE_STATUS () == GARP_NODE_ACTIVE)
    {
        GarpL2IwfHandleOutgoingPktOnPort (pBuf,
                                          (UINT2) pGlobalPortEntry->u4IfIndex,
                                          u2Len, 0, CFA_ENCAP_NONE);
    }
    else
    {
        GARP_RELEASE_BUFF (GARP_SYS_BUFF, pBuf);
    }

}

/************************************************************************/
/* Function Name    : GarpSyncAttributeTable ()                         */
/*                                                                      */
/* Description      : Transmits Join messages on the given port for all */
/*                    the Attributes registered on other ports.         */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the Application record.   */
/*                    pPortEntry - Pointer to the Port record.          */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
void
GarpSyncAttributeTable (tGarpAppEntry * pAppEntry, tGarpPortEntry * pPortEntry)
{
    UINT1              *pGipPortList = NULL;
    tGarpPortEntry     *pTmpPortEntry;
    tGarpGipEntry      *pGipEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpIfMsg          IfMsg;
    UINT4               u4Index;
    UINT1               u1Result;
    UINT4               u4HashIndex = 0;

    pTmpPortEntry = pAppEntry->pPortTable;
    pGipPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pGipPortList == NULL)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "GarpSyncAttributeTable: "
                  "Error in allocating memory for pGipPortList\r\n");
        return;
    }
    MEMSET (pGipPortList, 0, sizeof (tLocalPortList));

    while (pTmpPortEntry != NULL)
    {
        if ((pTmpPortEntry == pPortEntry) || (pTmpPortEntry->pGipTable == NULL))
        {
            pTmpPortEntry = pTmpPortEntry->pNextNode;
            continue;
        }

        TMO_HASH_Scan_Table (pTmpPortEntry->pGipTable, u4HashIndex)
        {
            TMO_HASH_Scan_Bucket (pTmpPortEntry->pGipTable, u4HashIndex,
                                  pGipEntry, tGarpGipEntry *)
            {
                if (pGipEntry->u1StapStatus == GARP_BLOCKING)
                {
                    /* If the Gip Status is blocking, no need to propagate
                     * attributes in this gip on the given port. */
                    continue;
                }

                /* 
                 * Propagate the Attributes on the new port, only if the new
                 * port belongs to the Gip context of the Attribute.
                 */
                GarpGipGetPortList (pAppEntry->u1AppId,
                                    pGipEntry->u2GipId, pGipPortList);

                GARP_IS_MEMBER_PORT (pGipPortList, pPortEntry->u2Port,
                                     u1Result);

                if (u1Result == GARP_TRUE)
                {

                    for (u4Index = 0; u4Index < pAppEntry->u2MaxBuckets;
                         u4Index++)
                    {
                        pAttrEntry = pGipEntry->papAttrTable[u4Index];

                        while (pAttrEntry != NULL)
                        {
                            if (pAttrEntry->u1RegSemState == GARP_IN)
                            {
                                IfMsg.pAppEntry = pAppEntry;
                                IfMsg.u2GipId = pGipEntry->u2GipId;
                                IfMsg.u2Port = pTmpPortEntry->u2Port;

                                GarpGidGipAttributeJoinReq (&pAttrEntry->Attr,
                                                            pPortEntry, &IfMsg);
                            }

                            pAttrEntry = pAttrEntry->pNextHashNode;
                        }
                    }
                }
            }
        }

        pTmpPortEntry = pTmpPortEntry->pNextNode;
    }
    UtilPlstReleaseLocalPortList (pGipPortList);
}

/************************************************************************/
/* Function Name    : GarpSyncAttributeTableForGip ()                   */
/*                                                                      */
/* Description      : Synchronises the attibute table for a given port  */
/*                    for a given GIP. This function will be called     */
/*                    whenever a port moves to forwarding state for a   */
/*                    particular GIP. This function transmits join      */
/*                    messages on the given port for the given GIP for  */
/*                    all the attributes registered on other ports.     */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the Application record.   */
/*                    pPortEntry - Pointer to the Port record.          */
/*                    pGipEntry - Pointer to the GIP record.            */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
VOID
GarpSyncAttributeTableForGip (tGarpAppEntry * pAppEntry,
                              tGarpPortEntry * pPortEntry, UINT2 u2GipId)
{

    UINT1              *pGipPortList = NULL;
    tGarpPortEntry     *pTmpPortEntry;
    tGarpGipEntry      *pTmpGipEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpIfMsg          IfMsg;
    UINT4               u4Index;
    UINT1               u1Result;

    pTmpPortEntry = pAppEntry->pPortTable;
    pGipPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pGipPortList == NULL)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "GarpSyncAttributeTableForGip: "
                  "Error in allocating memory for pGipPortList\r\n");
        return;
    }
    MEMSET (pGipPortList, 0, sizeof (tLocalPortList));

    while (pTmpPortEntry != NULL)
    {
        if (pTmpPortEntry == pPortEntry)
        {
            /* Same port ... Scan next one */
            pTmpPortEntry = pTmpPortEntry->pNextNode;
            continue;
        }

        pTmpGipEntry = GarpGetGipEntry (pTmpPortEntry, u2GipId);

        if ((pTmpGipEntry == NULL)
            || (pTmpGipEntry->u1StapStatus == GARP_BLOCKING))
        {
            /* Either GIP entry is not associated or the Ports status for
             * the GIP is in blocking state. So scan next port */
            pTmpPortEntry = pTmpPortEntry->pNextNode;
            continue;
        }

        /* 
         * Propagate the Attributes on the new port, only if the new
         * port belongs to the Gip context of the Attribute.
         */
        GarpGipGetPortList (pAppEntry->u1AppId,
                            pTmpGipEntry->u2GipId, pGipPortList);

        GARP_IS_MEMBER_PORT (pGipPortList, pPortEntry->u2Port, u1Result);

        if (u1Result == GARP_TRUE)
        {

            for (u4Index = 0; u4Index < pAppEntry->u2MaxBuckets; u4Index++)
            {

                pAttrEntry = pTmpGipEntry->papAttrTable[u4Index];

                while (pAttrEntry != NULL)
                {

                    if (pAttrEntry->u1RegSemState == GARP_IN)
                    {

                        IfMsg.pAppEntry = pAppEntry;
                        IfMsg.u2GipId = pTmpGipEntry->u2GipId;
                        IfMsg.u2Port = pTmpPortEntry->u2Port;

                        GarpGidGipAttributeJoinReq (&pAttrEntry->Attr,
                                                    pPortEntry, &IfMsg);
                    }
                    pAttrEntry = pAttrEntry->pNextHashNode;
                }
            }
        }

        pTmpPortEntry = pTmpPortEntry->pNextNode;
    }
    UtilPlstReleaseLocalPortList (pGipPortList);
}

/************************************************************************/
/* Function Name    : GarpInit ()                                       */
/*                                                                      */
/* Description      : Starts the Garp Module. All resource are created  */
/*                                                                      */
/* Input(s)         : u4ContextId: Context Identifier                   */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gpGarpContextInfo->u1GarpShutDownStatus           */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : gpGarpContextInfo->u1GarpShutDownStatus           */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : GARP_SUCCESS / GARP_FAILURE                       */
/************************************************************************/
INT4
GarpInit (void)
{
    INT4                i4RetVal;
    UINT2               u2Index;
    UINT4               u4IfIndex;
    UINT2               u2LocalPort;
    UINT2               u2PrevLocalPort;
    UINT1               u1OperStatus = 0;

    if (GARP_IS_GARP_ENABLED () == GARP_TRUE)
    {
        /* GARP is already enabled, so return */
        return GARP_SUCCESS;
    }

    MEMSET (GARP_CURR_CONTEXT_PTR ()->aGarpAppTable, 0,
            sizeof (tGarpAppEntry) * (GARP_MAX_APPS + 1));

    /* Entry 0 is not used */
    for (u2Index = 1; u2Index <= GARP_MAX_APPS; u2Index++)
    {

        GARP_CURR_CONTEXT_PTR ()->aGarpAppTable[u2Index].u1Status =
            GARP_INVALID;
        GARP_CURR_CONTEXT_PTR ()->aGarpAppTable[u2Index].u1AppId =
            GARP_INVALID_APP_ID;
    }

    if (GarpHandleBridgeMode () != GARP_SUCCESS)
    {
        return GARP_FAILURE;
    }

    gau1GarpShutDownStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] =
        GARP_SNMP_FALSE;

    u2PrevLocalPort = 0;

    while (GARP_L2IWF_GETNEXT_VALID_PORT_FOR_CONTEXT
           (GARP_CURR_CONTEXT_PTR ()->u4ContextId, u2PrevLocalPort,
            &u2LocalPort, &u4IfIndex) == L2IWF_SUCCESS)
    {

        if (GarpL2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
        {
            u2PrevLocalPort = u2LocalPort;
            continue;
        }

        i4RetVal = GarpHandleCreatePort (u4IfIndex, u2LocalPort);

        if (i4RetVal == GARP_FAILURE)
        {

            GARP_TRC_ARG1 (GARP_MOD_TRC, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                           GARP_NAME,
                           "Garp Port Creation failed for Port %d \n",
                           u4IfIndex);

            u2PrevLocalPort = u2LocalPort;
            continue;
        }

        GarpL2IwfGetPortOperStatus (GARP_MODULE,
                                    (UINT2) u4IfIndex, &u1OperStatus);

        if (u1OperStatus == CFA_IF_UP)
        {
            GarpHandlePortOperInd (u2LocalPort, GARP_OPER_UP);
        }
        u2PrevLocalPort = u2LocalPort;

    }

    if (GvrpInit () == GVRP_FAILURE)
    {
        return GARP_FAILURE;
    }

    if (GmrpInit () == GMRP_FAILURE)
    {
        return GARP_FAILURE;
    }

    if (GarpRegisterWithPktHandler () != GARP_SUCCESS)
    {
        return GARP_FAILURE;
    }

    GARP_TRC (GARP_MOD_TRC,
              INIT_SHUT_TRC, GARP_NAME, "GARP Module Init is successful.\n");

    return GARP_SUCCESS;
}

/************************************************************************/
/* Function Name    : GarpTaskDeInit ()                                 */
/*                                                                      */
/* Description      : Shutdown the Garp Module. All memory resources    */
/*                    are freed to the system                           */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gpGarpContextInfo->u1GarpShutDownStatus                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : gpGarpContextInfo->u1GarpShutDownStatus                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None.                                             */
/************************************************************************/
VOID
GarpTaskDeInit (void)
{
    GARP_DELETE_QUEUE (gGarpQId);
    GARP_DELETE_QUEUE (gGarpCfgQId);
    GARP_DELETE_SEM ();
}

/************************************************************************/
/* Function Name    : GarpDeInit ()                                     */
/*                                                                      */
/* Description      : Shutsdown the Garp Moduel. All memory resources   */
/*                    are freed to the system                           */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gpGarpContextInfo->u1GarpShutDownStatus                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : gpGarpContextInfo->u1GarpShutDownStatus                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : GARP_SUCCESS / GARP_FAILURE                       */
/************************************************************************/
INT4
GarpDeInit (void)
{
    tGarpAppEntry      *pAppEntry;
    UINT2               u2Index;

    if (GARP_CURR_CONTEXT_PTR () == NULL)
    {
        /* No valid context exists */
        return GARP_SUCCESS;
    }

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        /* GARP is already shut down so return */
        return GARP_SUCCESS;
    }

    for (u2Index = 1; u2Index <= GARP_MAX_APPS; u2Index++)
    {

        pAppEntry = &GARP_CURR_CONTEXT_PTR ()->aGarpAppTable[u2Index];

        if (pAppEntry->u1Status == GARP_VALID)
        {

            /* 
             * GARP can be disabled only when all the GARP 
             * applications are disabled 
             */
            return GARP_FAILURE;
        }
    }

    gau1GarpShutDownStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] =
        GARP_SNMP_TRUE;

    GarpRemovePortEntry ();

    GARP_BRIDGE_MODE () = GARP_INVALID_BRIDGE_MODE;

    GARP_TRC (GARP_MOD_TRC,
              INIT_SHUT_TRC, GARP_NAME, "GARP Shutdown is successful.\n");

    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_VRP);
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_MRP);

    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpTaskInit                             */
/*  Description     : This function initalise the Garp Task    */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on successful garp task     */
/*                  initialisation.Else GARP_FAILURE           */
/***************************************************************/
INT4
GarpTaskInit (VOID)
{
    gGarpQId = 0;
    gGarpCfgQId = 0;

    /* Create the Garp semaphore first. This semaphore will
     * not be destroyed even when garp is shutdown.
     * This semaphore is for protecting the data structures.
     */
    if (GARP_CREATE_SEM () != OSIX_SUCCESS)
    {
        return GARP_FAILURE;
    }

    MEMSET (&gGarpPoolIds, 0, sizeof (tGarpPoolIds));

    if (GARP_CREATE_QUEUE (GARP_TASK_QUEUE, OSIX_MAX_Q_MSG_LEN,
                           GARP_Q_DEPTH, &gGarpQId) != OSIX_SUCCESS)
    {
        return GARP_FAILURE;
    }

    if (GARP_CREATE_QUEUE (GARP_CFG_QUEUE, OSIX_MAX_Q_MSG_LEN,
                           GARP_CFG_Q_DEPTH, &gGarpCfgQId) != OSIX_SUCCESS)
    {
        return GARP_FAILURE;
    }

    return GARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : GarpHandleStartModule                                */
/*                                                                           */
/* Description        : This function handles the start of GARP module       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS/GARP_FAILURE.                           */
/*****************************************************************************/
INT4
GarpHandleStartModule ()
{
    UINT4               u4RetVal = 0;
    tGarpMaxBulkMsgSize *pGarpMaxBuldMsgPtr = NULL;
    tGarpVlanMapMsgSize *pGarpVlanMapMsgPtr = NULL;
    tGarpAppGipHashArray *pGarpAppGipHashPtr = NULL;
    tGarpGlobPvlanSize *pGarpGlobPvlanPtr = NULL;

    UNUSED_PARAM (pGarpMaxBuldMsgPtr);
    UNUSED_PARAM (pGarpVlanMapMsgPtr);
    UNUSED_PARAM (pGarpAppGipHashPtr);
    UNUSED_PARAM (pGarpGlobPvlanPtr);

    if (GarpGlobalMemoryInit () != GARP_SUCCESS)
    {
        return GARP_FAILURE;
    }

    u4RetVal = TmrCreateTimerList (GARP_TASK, GARP_TIMER_EXP_EVENT,
                                   NULL, &gGarpTmrListId);
    if (u4RetVal != TMR_SUCCESS)
    {
        GarpGlobalMemoryDeInit ();
        return GARP_FAILURE;
    }

    if (GarpSelectContext (GARP_DEFAULT_CONTEXT_ID) == GARP_FAILURE)
    {
        GarpGlobalMemoryDeInit ();
        TmrDeleteTimerList (gGarpTmrListId);
        gGarpTmrListId = 0;
        return GARP_FAILURE;
    }

    if (GarpInit () != GARP_SUCCESS)
    {
        GARP_TRC (GARP_RED_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "GARP shutdown failed during restart for applying "
                  "configuration database\n");
        GarpGlobalMemoryDeInit ();
        TmrDeleteTimerList (gGarpTmrListId);
        gGarpTmrListId = 0;
        GarpReleaseContext ();
        return GARP_FAILURE;
    }
    /* Make the admin status for the garp applications
     *      * as enabled. */
    GarpAppAdminEnable ();

    /*GVRP and GMRP will be enabled inside these functions */
#ifndef L2RED_WANTED
    GARP_NODE_STATUS () = GARP_NODE_ACTIVE;
    GarpAppModuleEnable ();
#else
    GARP_NODE_STATUS () = GARP_NODE_IDLE;
#endif
    gu1GarpIsInitComplete = GARP_TRUE;
    if (GarpL2IwfGetBridgeMode (GARP_CURR_CONTEXT_ID (),
                                &GARP_BRIDGE_MODE ()) != L2IWF_SUCCESS)
    {
        return GARP_FAILURE;
    }

    if ((GARP_BRIDGE_MODE () == GARP_INVALID_BRIDGE_MODE) ||
        (GARP_BRIDGE_MODE () == GARP_PBB_ICOMPONENT_BRIDGE_MODE) ||
        (GARP_BRIDGE_MODE () == GARP_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        nmhSetDot1qGvrpStatus (GVRP_DISABLED);
        nmhSetDot1dGmrpStatus (GMRP_DISABLED);
        nmhSetDot1qFutureGarpShutdownStatus (GARP_SNMP_TRUE);
        GarpReleaseContext ();
        return GARP_SUCCESS;
    }

    GarpReleaseContext ();
    return GARP_SUCCESS;
}

/**************************************************************/
/*  Function Name   : GarpMain                                 */
/*  Description     : This the main routine for the garp module*/
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpMain (INT1 *pi1Param)
{
    tCRU_BUF_CHAIN_HEADER *pMsg;
    UINT4               u4Events;
    tGarpIfMsg         *pGarpIfMsg;
    tGarpQMsg          *pGarpQMsg = NULL;

    UNUSED_PARAM (pi1Param);

    if (GarpTaskInit () != GARP_SUCCESS)
    {
        GarpTaskDeInit ();
        /* Indicate the status of initialization to the main routine */
        GARP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (GarpHandleStartModule () != GARP_SUCCESS)
    {
        GarpDeInit ();
        GarpTaskDeInit ();
        /* Indicate the status of initialization to the main routine */
        GARP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (GARP_GET_TASK_ID (SELF, GARP_TASK, &(GARP_TASK_ID)) != OSIX_SUCCESS)
    {
        GarpDeInit ();
        GarpTaskDeInit ();
        if (gGarpTmrListId != 0)
        {
            TmrDeleteTimerList (gGarpTmrListId);
            gGarpTmrListId = 0;
        }
        GarpGlobalMemoryDeInit ();
        /* Indicate the status of initialization to the main routine */
        GARP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    GARP_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        if ((GARP_RECV_EVENT (GARP_TASK_ID, (GARP_TIMER_EXP_EVENT
                                             | GARP_MSG_ENQ_EVENT
                                             | GARP_CFG_MSG_EVENT),
                              OSIX_WAIT, &u4Events)) == OSIX_SUCCESS)
        {

            if (u4Events & GARP_TIMER_EXP_EVENT)
            {
                GARP_LOCK ();

                GarpProcessTimerEvent ();

                GARP_UNLOCK ();
            }

            if (u4Events & GARP_MSG_ENQ_EVENT)
            {
                while (GARP_RECV_FROM_QUEUE (GARP_TASK_QUEUE_ID,
                                             (UINT1 *) &pMsg,
                                             OSIX_DEF_MSG_LEN,
                                             OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    GARP_GET_IF_MSG (pMsg, pGarpIfMsg);
                    GARP_LOCK ();

                    if (GarpSelectContext (pGarpIfMsg->u4ContextId) ==
                        GARP_SUCCESS)
                    {
                        if (GARP_IS_GARP_ENABLED () == GARP_TRUE)
                        {
                            GarpProcessMsg (pGarpIfMsg, pMsg);
                            GarpReleaseContext ();

                        }
                    }
                    GARP_UNLOCK ();
                    GARP_RELEASE_BUFF (GARP_SYS_BUFF, pMsg);
                }
            }

            if (u4Events & GARP_CFG_MSG_EVENT)
            {
                while (GARP_RECV_FROM_QUEUE (GARP_CFG_QUEUE_ID,
                                             (UINT1 *) &pGarpQMsg,
                                             OSIX_DEF_MSG_LEN,
                                             OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    GARP_LOCK ();

                    /* Buffer freed inside this function */
                    GarpProcessConfigMsg (pGarpQMsg);
                    /* 
                     * Give the Mutual Exclusion Sema4 on completion of
                     * Protocol Processing
                     */
                    GARP_UNLOCK ();
                }
            }
        }
    }
}

/***************************************************************/
/*  Function Name   : GarpProcessConfigMsg                     */
/*  Description     : This function is called to process the   */
/*                    received configuration message.          */
/*  Input(s)        : pGarpQMsg - Pointer to the configuration */
/*                    message.                                 */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpProcessConfigMsg (tGarpQMsg * pGarpQMsg)
{
    switch (pGarpQMsg->u2MsgType)
    {
        case GARP_VLAN_MAP_MSG:
        case GARP_VLAN_LIST_MAP_MSG:
            /* Buffer is released inside this function */
            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }
            GarpProcessVlanMapCfgEvent (pGarpQMsg);
            GarpReleaseContext ();
            break;

        case GARP_BULK_MSG:
            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }
            GarpProcessBulkCfgEvent (pGarpQMsg);
            GarpReleaseContext ();
            GARP_RELEASE_BUFF (GARP_BULK_QMSG_BUFF, pGarpQMsg);
            break;

#ifdef L2RED_WANTED
        case GARP_RED_MSG:

            GarpRedProcessMsg (pGarpQMsg);
            GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);
            break;

        case GARP_RED_MCAST_ADD_MSG:

            GarpRedProcessMcastAddMsg (pGarpQMsg);
            GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);
            break;
#endif
        default:
            GarpProcessCfgEvent (pGarpQMsg);
            GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);
            break;
    }                            /* Switch u2MsgType */
}

/**************************************************************/
/*  Function Name   : GarpProcessTimerExpiry                   */
/*  Description     : This function is called to process timer *
*                    expiry events                            */
/*  Input(s)        : pTimer - Pointer to the garp application *
*                    timer                                    */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpProcessTimerExpiry (tGarpTimer * pTimer)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    if (pTimer->u1IsTmrActive == GARP_FALSE)
    {

        return;
    }

    pTimer->u1IsTmrActive = GARP_FALSE;

    switch (pTimer->u1TmrType)
    {

        case GARP_JOIN_TIMER:

            if (GARP_IS_GARP_ENABLED () == GARP_TRUE)
            {

                GarpJoinTimerExpiry (pTimer->u1AppId,
                                     (tGarpPortEntry *) (pTimer->pRec));
            }
            break;

        case GARP_LEAVE_TIMER:

            if (GARP_IS_GARP_ENABLED () == GARP_TRUE)
            {

                GarpLeaveTimerExpiry (pTimer->u1AppId, pTimer->u2Port,
                                      (tGarpAttrEntry *) (pTimer->pRec));
            }
            break;

        case GARP_LEAVE_ALL_TIMER:

            if (GARP_IS_GARP_ENABLED () == GARP_TRUE)
            {
                pGlobalPortEntry = GARP_PORT_ENTRY (((tGarpPortEntry *)
                                                     (pTimer->pRec))->u2Port);

                GARP_TRC_ARG2 (GARP_MOD_TRC, CONTROL_PLANE_TRC,
                               GARP_NAME,
                               "Leave All Timer expired - App Id = %d,"
                               "\tPort = %d. \n", pTimer->u1AppId,
                               pGlobalPortEntry->u4IfIndex);

                GarpLeaveAllTimerExpiry ((tGarpPortEntry *) (pTimer->pRec));
            }
            break;
        default:
            GARP_TRC (GARP_MOD_TRC,
                      (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                      GARP_NAME,
                      " GARP Timer expiry for unknown timer type\r\n");
            break;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPostCfgMessage                               */
/*                                                                           */
/*    Description         : Posts the message to Garp Config Q.              */
/*                                                                           */
/*    Input(s)            : u1MsgType -  Meesage Type                        */
/*                          u2Port    -  Port Index.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpPostCfgMessage (UINT1 u1MsgType, UINT2 u2LocalPortId,
                    UINT4 u4ContextId, UINT4 u4IfIndex)
{
    tGarpQMsg          *pGarpQMsg = NULL;

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return GARP_FAILURE;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         GARP_NAME, " Message Allocation failed for"
                         "Garp Config Message Type %u \r\n", u1MsgType);
        return GARP_FAILURE;

    }

    pGarpQMsg = (tGarpQMsg *) (VOID *)
        GARP_GET_BUFF (GARP_QMSG_BUFF, sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         GARP_NAME, " Message Allocation failed for"
                         "Garp Config Message Type %u \r\n", u1MsgType);
        return GARP_FAILURE;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));
    pGarpQMsg->u2MsgType = GARP_MSG;

    pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = u1MsgType;
    pGarpQMsg->unGarpMsg.GarpMsg.u2Port = u2LocalPortId;
    pGarpQMsg->u4IfIndex = u4IfIndex;
    pGarpQMsg->u4ContextId = u4ContextId;

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         GARP_NAME,
                         " Send To Q failed for  Vlan Config Message "
                         "Type %u \r\n", u1MsgType);

        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);

        return GARP_FAILURE;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);

    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpHandleCreatePort                     */
/*  Description     : This function is for adding a port.      */
/*  Input(s)        : u2Port - Port to be added.               */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : gpGarpContextInfo->apGarpGlobalPortEntry          */
/*  Global variables Modified : gpGarpContextInfo->apGarpGlobalPortEntry          */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on successful addition of   */
/*                    port. GARP_FAILURE on failure            */
/***************************************************************/

INT4
GarpHandleCreatePort (UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1Status;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "GARP NOT enabled. Garp Port creation "
                       "failed for Port %d\n", u4IfIndex);

        return GARP_FAILURE;
    }

    if (GARP_IS_PORT_VALID (u2LocalPortId) == GARP_FALSE)
    {

        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Invalid Port. Port NOT created.\n");

        return GARP_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (u2LocalPortId);

    if (pGlobalPortEntry != NULL)
    {

        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Duplicate Port creation for Port %d.\n", u4IfIndex);

        return GARP_SUCCESS;
    }

    if (GarpCfaGetIfInfo (u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {

        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Failed to get Cfa Interface Parameters. \n");

        return GARP_FAILURE;
    }

    if (CfaIfInfo.u1BrgPortType == VLAN_PROVIDER_INSTANCE_PORT)
    {
        return GARP_SUCCESS;
    }

/* Allocate the port entry pointer here. */
    pGlobalPortEntry =
        (tGarpGlobalPortEntry *) (VOID *) GARP_GET_BUFF (GARP_GLOBPORT_ENTRY,
                                                         sizeof
                                                         (tGarpGlobalPortEntry));

    if (pGlobalPortEntry == NULL)
    {

        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Memory allocation failed for Port creation\n");

        return GARP_FAILURE;
    }

    /* The default Port Oper status for a created port is
     * set as down
     */
    if (GarpInitAndAddPortEntry (pGlobalPortEntry, u2LocalPortId, u4IfIndex)
        != GARP_SUCCESS)
    {
        return GARP_FAILURE;
    }
    if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
    {
        /* Set Gmrp disbaled for this port in l2iwf. */
        if (GarpL2IwfSetProtocolEnabledStatusOnPort
            (GARP_CURR_CONTEXT_ID (), u2LocalPortId, L2_PROTO_GMRP,
             OSIX_DISABLED) == L2IWF_FAILURE)
        {
            return GARP_FAILURE;
        }
    }
    else
    {
        if (GmrpIsGmrpEnabled () == GMRP_TRUE)
        {
            u1Status = GMRP_ENABLED;
            GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2LocalPortId].
                u1Status = GMRP_ENABLED;
        }
        else
        {
            u1Status = GMRP_DISABLED;
            GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2LocalPortId].
                u1Status = GMRP_DISABLED;
        }

        /* Set Gmrp disbaled for this port in l2iwf. */
        if (GarpL2IwfSetProtocolEnabledStatusOnPort
            (GARP_CURR_CONTEXT_ID (), u2LocalPortId, L2_PROTO_GMRP,
             u1Status) == L2IWF_FAILURE)
        {
            return GARP_FAILURE;
        }
    }

    /* In SISP enabled port, GVRP will be always disabled */
    VlanL2IwfGetSispPortCtrlStatus (GARP_GET_IFINDEX (u2LocalPortId),
                                    &u1Status);

    if (u1Status == L2IWF_ENABLED)
    {
        if (GarpL2IwfSetProtocolEnabledStatusOnPort (GARP_CURR_CONTEXT_ID (),
                                                     u2LocalPortId,
                                                     L2_PROTO_GVRP,
                                                     OSIX_DISABLED)
            == L2IWF_FAILURE)
        {
            return GARP_FAILURE;
        }
    }
    else
    {
        /* In case of 1ad bridge, do the need full Gvrp for present PB port 
         * type. */
        if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
        {
            GarpPbHandlePortTypeChange (u2LocalPortId);
        }
        else
        {
            if (GvrpIsGvrpEnabled () == GARP_TRUE)
            {
                u1Status =
                    GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2LocalPortId].
                    u1Status;
            }
            else
            {
                GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2LocalPortId].
                    u1Status = OSIX_DISABLED;
                u1Status = OSIX_DISABLED;
            }

            if (GarpL2IwfSetProtocolEnabledStatusOnPort
                (GARP_CURR_CONTEXT_ID (), u2LocalPortId, L2_PROTO_GVRP,
                 u1Status) == L2IWF_FAILURE)
            {
                return GARP_FAILURE;
            }
        }
    }
    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpHandleDeletePort                     */
/*  Description     : This function is for deleting a port.    */
/*  Input(s)        : u2Port - Port to be deleted              */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on successful deletion  of  */
/*                    port. GARP_FAILURE on failure            */
/***************************************************************/

INT4
GarpHandleDeletePort (UINT2 u2Port)
{
    tGarpGlobalPortEntry *pGlobPortEntry = NULL;
    tGarpAppEntry      *pAppEntry;
    UINT1               u1AppId;

    if (GARP_IS_PORT_VALID (u2Port) == GARP_FALSE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Invalid Port number. Garp Port Deletion failed.\n");

        return GARP_FAILURE;
    }

    GvrpDelPort (u2Port);

    GmrpDelPort (u2Port);

    for (u1AppId = 1; u1AppId <= GARP_MAX_APPS; u1AppId++)
    {
        pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

        if (pAppEntry != NULL)
        {
            GarpAppDelPort (u1AppId, u2Port);
        }
    }
    pGlobPortEntry = GARP_PORT_ENTRY (u2Port);

    if (pGlobPortEntry)
    {
        GarpRemovePortFromIfIndexTable (pGlobPortEntry);
        GARP_RELEASE_BUFF (GARP_GLOBPORT_ENTRY, (UINT1 *) pGlobPortEntry);
        GARP_PORT_ENTRY (u2Port) = NULL;
    }
    return GARP_SUCCESS;

}

/************************************************************************/
/*  Function Name    : GarpHandlePortOperInd ()                         */
/*  Description      : Invoked by VLAN whenever Port Oper Status is     */
/*                     changed. If Link Aggregation is enabled, then    */
/*                     Oper Down indication is given for each port      */
/*                     initially. When the Link Aggregation Group is    */
/*                     formed, then Oper Up indication is given.        */
/*                     In the Oper Down state, frames will neither      */
/*                     be transmitted nor be received.                  */
/*  Input(s)         : u2Port - Port for which Oper Status is changed.  */
/*                     u1OperStatus - GAARP_OPER_UP/GARP_OPER_DOWN      */
/*  Output(s)        : None                                             */
/*  Global Variables                                                    */
/*  Referred         : None                                             */
/*  Global Variables                                                    */
/*  Modified         : None                                             */
/*  Exceptions or OS                                                    */
/*  Error Handling   : None                                             */
/*  Use of Recursion : None                                             */
/*  Returns          : GARP_SUCCESS / GARP_FAILURE                      */
/************************************************************************/

INT4
GarpHandlePortOperInd (UINT2 u2Port, UINT1 u1OperStatus)
{
    INT4                i4RetVal;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        /* GARP Module is shutdown...Ignore the oper status indications */
        return GARP_SUCCESS;
    }

    if (GARP_IS_PORT_VALID (u2Port) == GARP_FALSE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Invalid Port number. Oper Status Indication failed.\n");

        return GARP_FAILURE;
    }

    if (GARP_PORT_ENTRY (u2Port) == NULL)
    {
        /* Port not created in GARP */
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Port not created in GARP Module. \n");

        return GARP_FAILURE;
    }

    if (GARP_CURR_CONTEXT_PTR ()->apGarpGlobalPortEntry[u2Port]->u1OperStatus
        == u1OperStatus)
    {
        /* No change in oper status */
        return GARP_SUCCESS;
    }

    GARP_CURR_CONTEXT_PTR ()->apGarpGlobalPortEntry[u2Port]->u1OperStatus =
        u1OperStatus;

    switch (u1OperStatus)
    {
        case GARP_OPER_UP:
            /* Give Port create indications to all GARP applications */
            i4RetVal = GvrpAddPort (u2Port);
            if (i4RetVal != GVRP_SUCCESS)
            {
                GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                          "Port registration in GVRP Module failed. \n");
            }

            i4RetVal = GmrpAddPort (u2Port);

            if (i4RetVal != GMRP_SUCCESS)
            {
                GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                          "Port registration in GMRP Module failed. \n");
            }
            /* Vlan module will now propagate the attribute entries */
            break;

        case GARP_OPER_DOWN:
            /* Give port delete indications to all GARP applications */
            GvrpDelPort (u2Port);
            GmrpDelPort (u2Port);
            break;

        default:
            return GARP_FAILURE;
    }

    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpProcessCfgEvent                              */
/*                                                                           */
/*    Description         : Function Process the received configuration event*/
/*                                                                           */
/*    Input(s)            : pGarpQMsg - Pointer to the Message                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
GarpProcessCfgEvent (tGarpQMsg * pGarpQMsg)
{
    INT4                i4RetVal;
    UINT2               u2Port;
    BOOL1               bResult;

    switch (GARP_QMSG_TYPE (&(pGarpQMsg->unGarpMsg.GarpMsg)))
    {

        case GARP_PORT_CREATE_MSG:
            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                break;
            }

            u2Port = pGarpQMsg->unGarpMsg.GarpMsg.u2Port;

            i4RetVal = GarpHandleCreatePort (pGarpQMsg->u4IfIndex, u2Port);

            if (i4RetVal == GARP_FAILURE)
            {

                GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                               "GARP Port Creation failed for Port %d \n",
                               pGarpQMsg->u4IfIndex);
            }
            else
            {
                GarpPbCheckPortTypeChange (u2Port);
            }

            GarpReleaseContext ();

            L2_SYNC_GIVE_SEM ();

            break;

        case GARP_PORT_DELETE_MSG:
            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }

            i4RetVal =
                GarpHandleDeletePort (pGarpQMsg->unGarpMsg.GarpMsg.u2Port);
            if (i4RetVal == GARP_FAILURE)
            {

                GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                               "GARP Port Deletion failed for Port %d \n",
                               pGarpQMsg->u4IfIndex);
            }
            GarpReleaseContext ();
            break;

        case GARP_PORT_MAP_MSG:
            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                L2MI_SYNC_GIVE_SEM ();
                break;
            }

            i4RetVal = GarpHandleCreatePort (pGarpQMsg->u4IfIndex,
                                             pGarpQMsg->unGarpMsg.GarpMsg.
                                             u2Port);

            if (i4RetVal == GARP_FAILURE)
            {

                GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                               "GARP Port Creation failed for Port %d \n",
                               pGarpQMsg->u4IfIndex);
            }
            GarpReleaseContext ();

            L2MI_SYNC_GIVE_SEM ();

            break;

        case GARP_PORT_UNMAP_MSG:
            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                L2MI_SYNC_GIVE_SEM ();
                break;
            }

            i4RetVal =
                GarpHandleDeletePort (pGarpQMsg->unGarpMsg.GarpMsg.u2Port);
            if (i4RetVal == GARP_FAILURE)
            {

                GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                               "GARP Port Deletion failed for Port %d \n",
                               pGarpQMsg->u4IfIndex);
            }
            GarpReleaseContext ();
            L2MI_SYNC_GIVE_SEM ();

            break;

        case GARP_PORT_OPER_UP_MSG:

            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }
            i4RetVal =
                GarpHandlePortOperInd (pGarpQMsg->unGarpMsg.GarpMsg.u2Port,
                                       GARP_OPER_UP);
            if (i4RetVal == GARP_FAILURE)
            {

                GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                               "GARP Port Oper Ind failed for Port %d \n",
                               pGarpQMsg->u4IfIndex);
            }

            GarpReleaseContext ();
            break;

        case GARP_PORT_OPER_DOWN_MSG:

            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }
            i4RetVal =
                GarpHandlePortOperInd (pGarpQMsg->unGarpMsg.GarpMsg.u2Port,
                                       GARP_OPER_DOWN);
            if (i4RetVal == GARP_FAILURE)
            {

                GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                               "GARP Port Oper Ind failed for Port %d \n",
                               pGarpQMsg->u4IfIndex);
            }

            GarpReleaseContext ();
            break;

        case GARP_STAP_PORT_FWD_MSG:

            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }
            GarpHandleStapPortChange ((UINT2) pGarpQMsg->u4IfIndex,
                                      GARP_FORWARDING,
                                      pGarpQMsg->unGarpMsg.GarpMsg.u2GipId);
            GarpReleaseContext ();
            break;

        case GARP_STAP_PORT_BLK_MSG:

            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }
            GarpHandleStapPortChange ((UINT2) pGarpQMsg->u4IfIndex,
                                      GARP_BLOCKING,
                                      pGarpQMsg->unGarpMsg.GarpMsg.u2GipId);
            GarpReleaseContext ();
            break;

        case GARP_PROP_VLAN_INFO_MSG:

            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }
            GvrpHandlePropagateVlanInfo (pGarpQMsg->unGarpMsg.GarpMsg.u2GipId,
                                         pGarpQMsg->unGarpMsg.GarpMsg.
                                         AddedPorts,
                                         pGarpQMsg->unGarpMsg.GarpMsg.
                                         DeletedPorts);
            GarpReleaseContext ();
            break;

        case GARP_PROP_MCAST_INFO_MSG:
            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }

            GmrpHandlePropagateMcastInfo (pGarpQMsg->unGarpMsg.GarpMsg.MacAddr,
                                          pGarpQMsg->unGarpMsg.GarpMsg.u2GipId,
                                          pGarpQMsg->unGarpMsg.GarpMsg.
                                          AddedPorts,
                                          pGarpQMsg->unGarpMsg.GarpMsg.
                                          DeletedPorts);

            GarpReleaseContext ();
            break;

        case GARP_PROP_FWDALL_INFO_MSG:

            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }
            GmrpHandlePropagateDefGroupInfo (VLAN_ALL_GROUPS,
                                             pGarpQMsg->unGarpMsg.GarpMsg.
                                             u2GipId,
                                             pGarpQMsg->unGarpMsg.GarpMsg.
                                             AddedPorts,
                                             pGarpQMsg->unGarpMsg.GarpMsg.
                                             DeletedPorts);

            GarpReleaseContext ();
            break;

        case GARP_PROP_FWDUNREG_INFO_MSG:

            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }
            GmrpHandlePropagateDefGroupInfo (VLAN_UNREG_GROUPS,
                                             pGarpQMsg->unGarpMsg.GarpMsg.
                                             u2GipId,
                                             pGarpQMsg->unGarpMsg.GarpMsg.
                                             AddedPorts,
                                             pGarpQMsg->unGarpMsg.GarpMsg.
                                             DeletedPorts);
            GarpReleaseContext ();
            break;

        case GARP_SET_VLAN_FORBID_MSG:

            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }
            GvrpHandleSetVlanForbiddenPorts (pGarpQMsg->unGarpMsg.GarpMsg.
                                             u2GipId,
                                             pGarpQMsg->unGarpMsg.GarpMsg.
                                             AddedPorts,
                                             pGarpQMsg->unGarpMsg.GarpMsg.
                                             DeletedPorts);

            GarpReleaseContext ();
            break;

        case GARP_SET_MCAST_FORBID_MSG:

            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }
            GmrpHandleSetMcastForbiddenPorts (pGarpQMsg->unGarpMsg.GarpMsg.
                                              MacAddr,
                                              pGarpQMsg->unGarpMsg.GarpMsg.
                                              u2GipId,
                                              pGarpQMsg->unGarpMsg.GarpMsg.
                                              AddedPorts,
                                              pGarpQMsg->unGarpMsg.GarpMsg.
                                              DeletedPorts);

            GarpReleaseContext ();
            break;

        case GARP_SET_FWDALL_FORBID_MSG:
            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }

            GmrpHandleSetDefGroupForbiddenPorts (VLAN_ALL_GROUPS,
                                                 pGarpQMsg->unGarpMsg.GarpMsg.
                                                 u2GipId,
                                                 pGarpQMsg->unGarpMsg.GarpMsg.
                                                 AddedPorts,
                                                 pGarpQMsg->unGarpMsg.GarpMsg.
                                                 DeletedPorts);

            GarpReleaseContext ();
            break;

        case GARP_SET_FWDUNREG_FORBID_MSG:
            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }

            GmrpHandleSetDefGroupForbiddenPorts (VLAN_UNREG_GROUPS,
                                                 pGarpQMsg->unGarpMsg.GarpMsg.
                                                 u2GipId,
                                                 pGarpQMsg->unGarpMsg.GarpMsg.
                                                 AddedPorts,
                                                 pGarpQMsg->unGarpMsg.GarpMsg.
                                                 DeletedPorts);
            GarpReleaseContext ();
            break;

        case GARP_GIP_UPDATE_GIP_PORTS_MSG:
            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                break;
            }

            for (u2Port = 1; u2Port < GARP_MAX_PORTS_PER_CONTEXT; u2Port++)
            {
                OSIX_BITLIST_IS_BIT_SET (pGarpQMsg->unGarpMsg.GarpMsg.
                                         AddedPorts, u2Port,
                                         sizeof (tLocalPortList), bResult);
                if (bResult == OSIX_TRUE)
                {
                    GarpHandleGipUpdateGipPorts (u2Port,
                                                 pGarpQMsg->unGarpMsg.GarpMsg.
                                                 u2GipId, GARP_ADD);

                }

                OSIX_BITLIST_IS_BIT_SET (pGarpQMsg->unGarpMsg.GarpMsg.
                                         DeletedPorts, u2Port,
                                         sizeof (tLocalPortList), bResult);
                if (bResult == OSIX_TRUE)
                {
                    GarpHandleGipUpdateGipPorts (u2Port,
                                                 pGarpQMsg->unGarpMsg.GarpMsg.
                                                 u2GipId, GARP_DELETE);

                }
            }
            GarpReleaseContext ();
            break;
        case GARP_CREATE_CONTEXT_MSG:

            i4RetVal = GarpHandleCreateContext (pGarpQMsg->u4ContextId);

            if (i4RetVal == GARP_FAILURE)
            {

                GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_MOD_TRC,
                                 ALL_FAILURE_TRC, GARP_NAME,
                                 "GARP context Creation failed for  %d \n",
                                 pGarpQMsg->u4ContextId);
            }
            L2MI_SYNC_GIVE_SEM ();

            break;
        case GARP_DELETE_CONTEXT_MSG:

            if (GarpSelectContext (pGarpQMsg->u4ContextId) == GARP_FAILURE)
            {
                L2MI_SYNC_GIVE_SEM ();
                break;
            }

            i4RetVal = GarpHandleDeleteContext (pGarpQMsg->u4ContextId);

            if (i4RetVal == GARP_FAILURE)
            {

                GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                               "GARP context Deletion failed for  %d \n",
                               pGarpQMsg->u4ContextId);
            }

            GarpReleaseContext ();
            L2MI_SYNC_GIVE_SEM ();

            break;
        case GARP_UPDATE_CONTEXT_NAME:

            i4RetVal = GarpHandleUpdateContextName (pGarpQMsg->u4ContextId);

            if (i4RetVal == GARP_FAILURE)
            {

                GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_MOD_TRC,
                                 ALL_FAILURE_TRC, GARP_NAME,
                                 "GARP context name updation failed for  %d \n",
                                 pGarpQMsg->u4ContextId);
            }

            break;
        default:
            break;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpProcessBulkCfgEvent                          */
/*                                                                           */
/*    Description         : Function Process the received bulk configuration */
/*                          event                                            */
/*                                                                           */
/*    Input(s)            : pGarpQMsg - Pointer to the Q Message             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
GarpProcessBulkCfgEvent (tGarpQMsg * pGarpQMsg)
{
    INT4                i4MsgIndex;
    tGarpBulkMsg       *pGarpBulkMsg;

    for (i4MsgIndex = 0; i4MsgIndex < GARP_QMSG_COUNT (pGarpQMsg); i4MsgIndex++)
    {
        pGarpBulkMsg = &(pGarpQMsg->unGarpMsg.GarpBulkMsg[i4MsgIndex]);

        switch (GARP_QMSG_TYPE (pGarpBulkMsg))
        {
            case GARP_PROP_VLAN_INFO_MSG:

                GvrpHandlePropagateVlanInfo (pGarpBulkMsg->VlanId,
                                             pGarpBulkMsg->Ports,
                                             gNullPortList);
                break;

            case GARP_PROP_MCAST_INFO_MSG:

                GmrpHandlePropagateMcastInfo (pGarpBulkMsg->MacAddr,
                                              pGarpBulkMsg->VlanId,
                                              pGarpBulkMsg->Ports,
                                              gNullPortList);

                break;

            case GARP_PROP_FWDALL_INFO_MSG:

                GmrpHandlePropagateDefGroupInfo (VLAN_ALL_GROUPS,
                                                 pGarpBulkMsg->VlanId,
                                                 pGarpBulkMsg->Ports,
                                                 gNullPortList);

                break;

            case GARP_PROP_FWDUNREG_INFO_MSG:

                GmrpHandlePropagateDefGroupInfo (VLAN_UNREG_GROUPS,
                                                 pGarpBulkMsg->VlanId,
                                                 pGarpBulkMsg->Ports,
                                                 gNullPortList);
                break;

            case GARP_SET_VLAN_FORBID_MSG:

                GvrpHandleSetVlanForbiddenPorts (pGarpBulkMsg->VlanId,
                                                 pGarpBulkMsg->Ports,
                                                 gNullPortList);

                break;

            case GARP_SET_MCAST_FORBID_MSG:

                GmrpHandleSetMcastForbiddenPorts (pGarpBulkMsg->MacAddr,
                                                  pGarpBulkMsg->VlanId,
                                                  pGarpBulkMsg->Ports,
                                                  gNullPortList);

                break;

            case GARP_SET_FWDALL_FORBID_MSG:

                GmrpHandleSetDefGroupForbiddenPorts (VLAN_ALL_GROUPS,
                                                     pGarpBulkMsg->VlanId,
                                                     pGarpBulkMsg->Ports,
                                                     gNullPortList);

                break;

            case GARP_SET_FWDUNREG_FORBID_MSG:

                GmrpHandleSetDefGroupForbiddenPorts (VLAN_UNREG_GROUPS,
                                                     pGarpBulkMsg->VlanId,
                                                     pGarpBulkMsg->Ports,
                                                     gNullPortList);
                break;
            default:
                break;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpProcessVlanMapCfgEvent                       */
/*                                                                           */
/*    Description         : Function Process the received VLAN Map           */
/*                          configuration event                              */
/*                                                                           */
/*    Input(s)            : pGarpQMsg - Pointer to the Q Message             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/

VOID
GarpProcessVlanMapCfgEvent (tGarpQMsg * pGarpQMsg)
{
    tGarpVidMapMsg     *pGarpVidMapMsg;
    tVlanMapInfo       *pVlanMapInfo;
    INT4                i4MsgIndex;
    INT4                i4EventIndex;
    UINT2               u2NewInstance;
    UINT1               u1BuffType;

    if (GARP_VLAN_MAP_MSG == GARP_QMSG_TYPE (pGarpQMsg))
    {
        u1BuffType = GARP_QMSG_BUFF;
    }
    else
    {
        u1BuffType = GARP_VLAN_MAP_MSG_BUFF;
    }

    /* Currently the loop executes only once */
    for (i4MsgIndex = 0; i4MsgIndex < GARP_QMSG_COUNT (pGarpQMsg); i4MsgIndex++)
    {
        pGarpVidMapMsg = &(pGarpQMsg->unGarpMsg.GarpVidMapMsg[i4MsgIndex]);

        u2NewInstance = pGarpVidMapMsg->u2NewMapId;

        switch (GARP_QMSG_EVENT (pGarpVidMapMsg))
        {
            case GARP_MAP_INST_VLAN_MSG:
                /* Fall Through */
            case GARP_UNMAP_INST_VLAN_MSG:

                for (i4EventIndex = 0;
                     i4EventIndex < GARP_QMSG_EVENT_COUNT (pGarpVidMapMsg);
                     i4EventIndex++)
                {
                    pVlanMapInfo = &(pGarpVidMapMsg->VlanMapInfo[i4EventIndex]);

                    GarpHandleVlanInstanceMap (pVlanMapInfo->VlanId,
                                               pVlanMapInfo->u2MapId,
                                               u2NewInstance);
                }

                break;

            case GARP_MAP_INST_VLAN_LIST_MSG:
                /* Fall Through */
            case GARP_UNMAP_INST_VLAN_LIST_MSG:

                for (i4EventIndex = 0;
                     i4EventIndex < GARP_QMSG_EVENT_COUNT (pGarpVidMapMsg);
                     i4EventIndex++)
                {
                    pVlanMapInfo = &(pGarpVidMapMsg->VlanMapInfo[i4EventIndex]);

                    /* Take care to validate the presence of GVRP/GMRP before 
                     * actually handing the respective application changes */
                    GarpHandleVlanInstanceMap (pVlanMapInfo->VlanId,
                                               pVlanMapInfo->u2MapId,
                                               u2NewInstance);
                }

                break;

            default:
                break;
        }
    }

    GARP_RELEASE_BUFF (u1BuffType, (UINT1 *) pGarpQMsg);

    return;
}

/***************************************************************/
/*  Function Name   : GarpHandleStapPortStateChange            */
/*  Description     : This function changes the state of the   */
/*                    stap for the given port                  */
/*  Input(s)        : u2port - Port number whose state as      */
/*                   changed                                   */
/*                    u1State - New state of the port          */
/*                    u2GipId - Group Information propagation ID*/
/*  Output(s)       : State of the port gets updated           */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/

VOID
GarpHandleStapPortChange (UINT2 u2IfIndex, UINT1 u1State, UINT2 u2GipId)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tStpInstanceInfo    StpInstInfo;
    UINT1               u1AppId;
    tVlanId             VlanId;
    tGarpGlobalPortEntry *pGlobPortEntry = NULL;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return;
    }

    pGlobPortEntry = GarpGetIfIndexEntry (u2IfIndex);
    if (pGlobPortEntry == NULL)
    {
        return;
    }

    for (u1AppId = 1; u1AppId <= GARP_MAX_APPS; u1AppId++)
    {
        pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

        if (pAppEntry == NULL)
        {
            continue;
        }
        /* 
         * Check whether the GIP Id is valid for this application
         */
        if (pAppEntry->u1AppId == GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId)
        {
            for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
            {
                if (u2GipId != ISS_ALL_STP_INSTANCES)
                {
                    if (GarpL2IwfMiGetVlanInstMapping
                        (pGlobPortEntry->u4ContextId, VlanId) != u2GipId)
                    {
                        continue;
                    }
                }

                pPortEntry = GarpGetPortEntry (pAppEntry,
                                               pGlobPortEntry->u2LocalPortId);

                if (pPortEntry != NULL)
                {
                    GARP_STAP_PORT_CHANGE (pAppEntry, pPortEntry, VlanId,
                                           u1State);
                }                /* Invalid GIP for the APP GVRP */
            }
        }
        else if (pAppEntry->u1AppId == GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId)
        {
            pPortEntry = GarpGetPortEntry (pAppEntry,
                                           pGlobPortEntry->u2LocalPortId);

            if (pPortEntry != NULL)
            {

                if (u2GipId == ISS_ALL_STP_INSTANCES)
                {
                    StpInstInfo.u2NextValidInstance = RST_DEFAULT_INSTANCE;
                    StpInstInfo.u1RequestFlag = L2IWF_REQ_NEXT_INSTANCE_ID;

                    GARP_STAP_PORT_CHANGE (pAppEntry, pPortEntry,
                                           RST_DEFAULT_INSTANCE, u1State);

                    while (L2IwfGetInstanceInfo (GARP_CURR_CONTEXT_ID (),
                                                 StpInstInfo.
                                                 u2NextValidInstance,
                                                 &StpInstInfo) != L2IWF_FAILURE)
                    {
                        GARP_STAP_PORT_CHANGE (pAppEntry, pPortEntry,
                                               StpInstInfo.u2NextValidInstance,
                                               u1State);

                    }

                }
                else
                {
                    GARP_STAP_PORT_CHANGE (pAppEntry, pPortEntry, u2GipId,
                                           u1State);
                }
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : GarpAppModuleEnable                                  */
/*                                                                           */
/* Description        : This function enables GVRP/GMRP modules. This        */
/*                      function will be called from GARP root task          */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpAppModuleEnable (VOID)
{
    INT4                i4RetVal;

    if (GARP_IS_GARP_ENABLED () == GARP_TRUE)
    {
        if (GVRP_ADMIN_STATUS () == GVRP_ENABLED)
        {
            i4RetVal = GvrpEnable ();

            if (i4RetVal == GVRP_FAILURE)
            {
                GARP_TRC (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                          "Enabling GVRP failed when the node is"
                          "made ACTIVE \n");
            }
        }

        if (GMRP_ADMIN_STATUS () == GMRP_ENABLED)
        {
            i4RetVal = GmrpEnable ();

            if (i4RetVal == GMRP_FAILURE)
            {
                GARP_TRC (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                          "Enabling GMRP failed when the node is"
                          "made ACTIVE \n");
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : GarpGlobalMemoryInit                                  */
/*                                                                           */
/* Description        : This function initialises the global non-context-    */
/*                      specific memory pertaining to the Garp Module        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS/GARP_FAILURE                            */
/*****************************************************************************/
INT4
GarpGlobalMemoryInit (VOID)
{
    UINT4               u4ContextId;

    for (u4ContextId = 0; u4ContextId < GARP_MAX_CONTEXTS; u4ContextId++)
    {
        gau1GarpShutDownStatus[u4ContextId] = GARP_SNMP_TRUE;
    }

    /* To Create all Mempools for the Module */
    if (GarpSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        GarpGlobalMemoryDeInit ();
        return GARP_FAILURE;
    }
    /*Assigning respective mempool Ids */
    GarpAssignMempoolIds ();

    gGarpPortTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tGarpGlobalPortEntry, RBNode),
                              (tRBCompareFn) GarpPortTblCmpFn);
    if (gGarpPortTable == NULL)
    {
        GarpGlobalMemoryDeInit ();
        return GARP_FAILURE;
    }

    GARP_CURR_CONTEXT_PTR () = (tGarpContextInfo *) (VOID *) GARP_GET_BUFF
        (GARP_CONTEXT_INFO, sizeof (tGarpContextInfo));

    if (GARP_CURR_CONTEXT_PTR () == NULL)
    {
        GarpGlobalMemoryDeInit ();
        return GARP_FAILURE;
    }

    /* Create and initialize te default Context */

    /* store the context pointer in the global array  */
    GARP_CONTEXT_PTR (GARP_DEFAULT_CONTEXT_ID) = GARP_CURR_CONTEXT_PTR ();

    MEMSET (GARP_CURR_CONTEXT_PTR (), 0, sizeof (tGarpContextInfo));
    GARP_CURR_CONTEXT_PTR ()->GarpContextPortInfo =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tGarpGlobalPortEntry, NextPortNode),
                              (tRBCompareFn) GarpPortTblCmpFn);
    if (GARP_CURR_CONTEXT_PTR ()->GarpContextPortInfo == NULL)
    {
        GARP_RELEASE_BUFF (GARP_CONTEXT_INFO,
                           (UINT1 *) GARP_CURR_CONTEXT_PTR ());
        GARP_CURR_CONTEXT_PTR () = NULL;
        GARP_CONTEXT_PTR (GARP_DEFAULT_CONTEXT_ID) = GARP_CURR_CONTEXT_PTR ();
        GarpGlobalMemoryDeInit ();
        return GARP_FAILURE;
    }

    GARP_CURR_CONTEXT_PTR ()->u1GvrpAdminStatus = GVRP_DISABLED;
    gau1GvrpStatus[GARP_DEFAULT_CONTEXT_ID] = GVRP_DISABLED;
    GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId = GARP_INVALID_APP_ID;
    GARP_CURR_CONTEXT_PTR ()->u1GmrpAdminStatus = GMRP_DISABLED;
    gau1GmrpStatus[GARP_DEFAULT_CONTEXT_ID] = GMRP_DISABLED;
    GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId = GARP_INVALID_APP_ID;
    GARP_CURR_CONTEXT_PTR ()->u4ContextId = GARP_DEFAULT_CONTEXT_ID;
    GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf =
        GARP_GET_BUFF (GARP_REMAP_TXBUFF_INFO, GARP_MAX_MSG_LEN);

    if (GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf == NULL)
    {
        RBTreeDelete (GARP_CURR_CONTEXT_PTR ()->GarpContextPortInfo);
        GARP_CURR_CONTEXT_PTR ()->GarpContextPortInfo = NULL;
        GARP_RELEASE_BUFF (GARP_CONTEXT_INFO,
                           (UINT1 *) GARP_CURR_CONTEXT_PTR ());
        GARP_CURR_CONTEXT_PTR () = NULL;
        GARP_CONTEXT_PTR (GARP_DEFAULT_CONTEXT_ID) = GARP_CURR_CONTEXT_PTR ();
        GarpReleaseContext ();
        GarpGlobalMemoryDeInit ();
        return GARP_FAILURE;
    }
    MEMSET (GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf, 0, GARP_MAX_MSG_LEN);

    MEMSET (GARP_CURR_CONTEXT_STR (), 0, GARP_CONTEXT_ALIAS_LEN + 1);
    if (GarpVcmGetSystemModeExt (GARP_PROTOCOL_ID) == VCM_MI_MODE)
    {
        GarpVcmGetAliasName (GARP_CURR_CONTEXT_ID (), GARP_CURR_CONTEXT_STR ());
        STRCAT (GARP_CURR_CONTEXT_STR (), ":");
    }

    GarpReleaseContext ();
    return GARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : GarpGlobalMemoryDeInit                               */
/*                                                                           */
/* Description        : This function De-initialises the global non-context- */
/*                      specific memory pertaining to the Garp Module        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS/GARP_FAILURE                            */
/*****************************************************************************/
INT4
GarpGlobalMemoryDeInit (VOID)
{

    if (gGarpPortTable != NULL)
    {
        RBTreeDestroy (gGarpPortTable, (tRBKeyFreeFn) GarpFreeGlobPortEntry, 0);
        gGarpPortTable = NULL;
    }
    /*Deleting all mempools */
    GarpSizingMemDeleteMemPools ();

    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpSelectContext                                */
/*                                                                           */
/*    Description         : This function switches to given context          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : gapGarpContextInfo,gpGarpContextInfo       */
/*                                                                           */
/*    Global Variables Modified : gapGarpContextInfo      .                  */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE.                      */
/*****************************************************************************/

INT4
GarpSelectContext (UINT4 u4ContextId)
{
    if (u4ContextId >= GARP_MAX_CONTEXTS)
    {
        return GARP_FAILURE;
    }
    GARP_CURR_CONTEXT_PTR () = GARP_CONTEXT_PTR (u4ContextId);
    if (GARP_CURR_CONTEXT_PTR () == NULL)
    {
        return GARP_FAILURE;
    }
    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpReleaseContext                              */
/*                                                                           */
/*    Description         : This function makes the switched context to NULL */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified :                                            */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                       .                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
GarpReleaseContext (VOID)
{
    GARP_CURR_CONTEXT_PTR () = NULL;
}

/***************************************************************/
/*  Function Name   : GarpAppEnrol                             */
/*  Description     : The applications (GVRP or GMRP) enrol    */
/*                   with Garp using this function.            */
/*                    This function is invoked whenever        */
/*                    Gvrp and/or Gmrp protocols wanted to     */
/*                    resgiter the GARp layer                  */
/*  Input(s)        : pAppMacAddr - Pointer to the Application */
/*                            reserved MAC address             */
/*                    pJoinIndFn - Pointer to the join Indicate*/
/*                            call back function registered for*/
/*                            the application                  */
/*                            app entry                        */
/*                    pLeaveIndFn - Pointer to the leave       */
/*                            indicate call back function      */
/*                            registered for the application   */
/*  Output(s)       : None                                     */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
INT4
GarpAppEnrol (tMacAddr AppMacAddr, tGarpAppnFn AppnFn, UINT1 *pu1AppId)
{
    tGarpAppEntry      *pAppEntry;
    tGarpGipEntry     **ppAppGipEntry;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Garp is disabled. App Enroll failed ....\n");

        return GARP_FAILURE;
    }

    pAppEntry = GarpGetAppEntryWithAddr (AppMacAddr);

    if (pAppEntry != NULL)
    {
        /* Application already registered */
        GARP_TRC (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  "Application already enrolled.\n");
        *pu1AppId = pAppEntry->u1AppId;
        return GARP_SUCCESS;
    }

    pAppEntry = GarpGetNewAppEntry (pu1AppId);

    if (pAppEntry == NULL)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Free App entry is NOT available. App Enroll failed ....\n");

        *pu1AppId = GARP_INVALID_APP_ID;
        return GARP_FAILURE;
    }

    ppAppGipEntry = (tGarpGipEntry **) (VOID *)
        GARP_GET_BUFF (GARP_APPGIP_HASH_BUFF,
                       GARP_APPGIP_HASH_BUCKET_SIZE * sizeof (tGarpGipEntry *));

    if (ppAppGipEntry == NULL)
    {
        *pu1AppId = GARP_INVALID_APP_ID;
        return GARP_FAILURE;
    }
    pAppEntry->papAppGipHashTable = ppAppGipEntry;

    MEMSET (ppAppGipEntry, 0, GARP_APPGIP_HASH_BUCKET_SIZE *
            sizeof (tGarpGipEntry *));

    if (MEMCMP (AppMacAddr, GARP_CTXT_GVRP_ADDR (), GARP_MAC_ADDR_LEN) == 0)
    {
        pAppEntry->u2MaxBuckets = GVRP_MAX_BUCKETS;
    }
    else
    {
        /* Currently, the Application is GMRP, if it is not GVRP. */
        pAppEntry->u2MaxBuckets = GMRP_MAX_BUCKETS;
    }

    pAppEntry->u1AppId = *pu1AppId;
    MEMCPY (pAppEntry->AppAddress, AppMacAddr, GARP_MAC_ADDR_LEN);

    pAppEntry->pJoinIndFn = AppnFn.pJoinIndFn;
    pAppEntry->pLeaveIndFn = AppnFn.pLeaveIndFn;
    pAppEntry->pAttrTypeValidateFn = AppnFn.pAttrTypeValidateFn;
    pAppEntry->pAttrRegValidateFn = AppnFn.pAttrRegValidateFn;
    pAppEntry->pAttrWildCardValidateFn = AppnFn.pAttrWildCardValidateFn;

    pAppEntry->pPortTable = NULL;
    pAppEntry->u1Status = GARP_VALID;

    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpAppDeEnrol                           */
/*  Description     : The applications (GVRP or GMRP) deenrol  */
/*             with Garp using this function.                  */
/*                    This function is invoked whenever               */
/*                    Gvrp and/or Gmrp protocols wanted to deregister */
/*             from the garp layer                             */
/*  Input(s)        : AppId- Id of the application to be       */
/*                    deregistered                             */
/*                            reserved MAC address             */
/*  Output(s)       : None                                     */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
INT4
GarpAppDeEnrol (UINT1 u1AppId)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tGarpPortEntry     *pNextPortEntry;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Garp is disabled. App De-Enroll failed ....\n");

        return GARP_FAILURE;
    }

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Unregistered Application. App De-Enroll failed ....\n");

        return GARP_FAILURE;
    }

    pPortEntry = pAppEntry->pPortTable;

    while (pPortEntry != NULL)
    {

        pNextPortEntry = pPortEntry->pNextNode;

        GarpAppDelPort (u1AppId, pPortEntry->u2Port);

        pPortEntry = pNextPortEntry;
    }

    if (NULL != pAppEntry->papAppGipHashTable)
    {
        GARP_RELEASE_BUFF (GARP_APPGIP_HASH_BUFF,
                           (UINT1 *) pAppEntry->papAppGipHashTable);

        pAppEntry->papAppGipHashTable = NULL;
    }

    pAppEntry->u1Status = GARP_INVALID;
    pAppEntry->u1AppId = GARP_INVALID_APP_ID;

    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpAppAddPort                           */
/*  Description     : This function is for adding a port to a  */
/*                    given Garp application specified by AppId*/
/*  Input(s)        : u1AppId - Application AppId              */
/*                    u2LocalPortId - Port number to be added to a    */
/*                    application AppId                        */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on successful addition of   */
/*                    port. GARP_FAILURE on failure            */
/***************************************************************/
INT4
GarpAppAddPort (UINT1 u1AppId, UINT2 u2LocalPortId)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tGarpTimer         *pLeaveAllTmr;
    tGarpTimer         *pJoinTmr;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Garp is disabled. App Port addition failed for port %d\n",
                       GARP_GET_IFINDEX (u2LocalPortId));

        return GARP_FAILURE;
    }

    if (GARP_IS_PORT_VALID (u2LocalPortId) == GARP_FALSE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Invalid Port number. App Port addition failed.\n");

        return GARP_FAILURE;
    }

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {

        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Unknown application. App Port addition failed "
                       "for port %d.\n", GARP_GET_IFINDEX (u2LocalPortId));

        return GARP_FAILURE;
    }

    pPortEntry = GarpGetPortEntry (pAppEntry, u2LocalPortId);

    if (pPortEntry != NULL)
    {

        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Port %d already added.\n",
                       GARP_GET_IFINDEX (u2LocalPortId));

        return GARP_SUCCESS;
    }

    pPortEntry = GarpGetNewPortEntry (u1AppId);

    if (pPortEntry == NULL)
    {

        return GARP_FAILURE;
    }

    pPortEntry->u2Port = u2LocalPortId;
    pLeaveAllTmr = &pPortEntry->LeaveAllTmr;

    pLeaveAllTmr->pRec = (void *) pPortEntry;
    pLeaveAllTmr->u1AppId = u1AppId;
    pLeaveAllTmr->u1TmrType = GARP_LEAVE_ALL_TIMER;
    pLeaveAllTmr->u1IsTmrActive = GARP_FALSE;
    pLeaveAllTmr->u2Port = u2LocalPortId;
    pLeaveAllTmr->pGarpContextInfo = GARP_CURR_CONTEXT_PTR ();

    pPortEntry->u1LeaveAllSemState = GARP_PASSIVE;

    pJoinTmr = &pPortEntry->JoinTmr;

    pJoinTmr->pRec = (void *) pPortEntry;
    pJoinTmr->u1AppId = u1AppId;
    pJoinTmr->u1TmrType = GARP_JOIN_TIMER;
    pJoinTmr->u1IsTmrActive = GARP_FALSE;
    pJoinTmr->u2Port = u2LocalPortId;
    pJoinTmr->pGarpContextInfo = GARP_CURR_CONTEXT_PTR ();

    /* 
     * Set the app status on the port as disabled. The application should
     * call GarpAppEnablePort to enable the application on that port 
     */
    pPortEntry->u1AppStatus = GARP_DISABLED;

    pPortEntry->pNextNode = pAppEntry->pPortTable;
    pAppEntry->pPortTable = pPortEntry;

    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpAppDelPort                           */
/*  Description     : This function is for deleting port from a*/
/*                    given Garp application specified by AppId*/
/*  Input(s)        : u1AppId - Application AppId              */
/*                    u2LocalPortId - Port number to be deleted*/
/*                    from the given application AppId          */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on successful deletion  of  */
/*                    port. GARP_FAILURE on failure            */
/***************************************************************/
INT4
GarpAppDelPort (UINT1 u1AppId, UINT2 u2LocalPortId)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tGarpGipEntry      *pGipEntry;    /* MST_CHANGE-Added */
    tGarpGipEntry      *pNextGipEntry = NULL;
    UINT4               u4HashIndex = 0;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "GARP NOT enabled. Port %d deletion failed.\n",
                       GARP_GET_IFINDEX (u2LocalPortId));

        return GARP_FAILURE;
    }

    if (GARP_IS_PORT_VALID (u2LocalPortId) == GARP_FALSE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Invalid Port. Port not deleted.\n");

        return GARP_FAILURE;
    }

    if (GARP_PORT_ENTRY (u2LocalPortId) == NULL)
    {

        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Port unknown. Port deletion failed.\n");
        return GARP_FAILURE;
    }
    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {

        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Invalid App Id. Port %d deletion failed.\n",
                       GARP_GET_IFINDEX (u2LocalPortId));

        return GARP_FAILURE;
    }

    pPortEntry = GarpGetPortEntry (pAppEntry, u2LocalPortId);

    if (pPortEntry == NULL)
    {

        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Port %d NOT created. Port deletion failed.\n",
                       GARP_GET_IFINDEX (u2LocalPortId));

        return GARP_FAILURE;
    }

    if (pPortEntry->pGipTable != NULL)
    {
        TMO_HASH_Scan_Table (pPortEntry->pGipTable, u4HashIndex)
        {
            GARP_HASH_DYN_Scan_Bucket (pPortEntry->pGipTable, u4HashIndex,
                                       pGipEntry, pNextGipEntry,
                                       tGarpGipEntry *)
            {
                if (pGipEntry->u1StapStatus == GARP_FORWARDING)
                {
                    /* 
                     * App Delete Port can be treated as Port being moved to
                     * Blocking state.
                     */
                    GarpPortStateChangedToBlocking (pAppEntry, pPortEntry,
                                                    pGipEntry->u2GipId);
                }
            }
        }
    }

    GarpDeleteAllAttrEntries (pAppEntry, pPortEntry);

    if (pPortEntry->JoinTmr.u1IsTmrActive == GARP_TRUE)
    {

        GarpStopTimer (&pPortEntry->JoinTmr);
        pPortEntry->JoinTmr.u1IsTmrActive = GARP_FALSE;
    }

    if (pPortEntry->LeaveAllTmr.u1IsTmrActive == GARP_TRUE)
    {

        GarpStopTimer (&pPortEntry->LeaveAllTmr);
        pPortEntry->LeaveAllTmr.u1IsTmrActive = GARP_FALSE;
    }

    GarpDeletePortEntry (pAppEntry, pPortEntry);

    GarpFreePortEntry (pPortEntry);

    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpAppEnablePort                        */
/*  Description     : This function is for enabling a GARP     */
/*                    application on the specified port.       */
/*                    GarpAppAddPort should be called before   */
/*                    calling this function.                   */
/*  Input(s)        : u1AppId - Application AppId              */
/*                    u2LocalPortId - Port number on which the */
/*                    application needs to be enabled.         */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns          : GARP_SUCCESS on successful addition of  */
/*                     port. GARP_FAILURE on failure           */
/***************************************************************/
INT4
GarpAppEnablePort (UINT1 u1AppId, UINT2 u2LocalPortId)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tGarpGipEntry      *pGipEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpGlobalPortEntry *pGlobPortEntry;
    tGarpTimer         *pLeaveAllTmr;
    UINT4               u4RandLeaveAllTime;
    UINT2               u2Index;
    UINT4               u4HashIndex = 0;

    pGlobPortEntry = GARP_PORT_ENTRY (u2LocalPortId);

    if (pGlobPortEntry == NULL)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Port unknown. Enabling Application failed.\n");
        return GARP_FAILURE;
    }

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Garp is disabled. Enabling Application on Port %d "
                       "failed \n", GARP_GET_IFINDEX (u2LocalPortId));

        return GARP_FAILURE;
    }

    if (GARP_IS_PORT_VALID (u2LocalPortId) == GARP_FALSE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Invalid Port number. Enabling Application  " "failed \n");
        return GARP_FAILURE;
    }

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Unknown application. Enabling application failed "
                       "for port %d.\n", GARP_GET_IFINDEX (u2LocalPortId));
        return GARP_FAILURE;
    }

    pPortEntry = GarpGetPortEntry (pAppEntry, u2LocalPortId);

    if (pPortEntry == NULL)
    {
        /* Seems GarpAppAddPort is not called before this */
        return GARP_FAILURE;
    }

    /* The leave all timer expiry event should happen at some random time
     * in the interval between configured leave all time and 1.5 * configured
     * leave all time. 
     */

    GARP_GET_RANDOM_LEAVEALL_TIME (pGlobPortEntry->u4LeaveAllTime,
                                   u4RandLeaveAllTime);

    pLeaveAllTmr = &pPortEntry->LeaveAllTmr;

    if (GarpStartTimer (&pPortEntry->LeaveAllTmr, u4RandLeaveAllTime)
        == GARP_FAILURE)
    {

        pLeaveAllTmr->u1IsTmrActive = GARP_FALSE;

        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Starting LeaveALL timer failed.  Enabling Application"
                       "on Port %d failed.\n",
                       GARP_GET_IFINDEX (u2LocalPortId));

        return GARP_FAILURE;
    }

    pPortEntry->u1LeaveAllSemState = GARP_PASSIVE;

    pPortEntry->u1AppStatus = GARP_ENABLED;

    if (pPortEntry->pGipTable != NULL)
    {
        TMO_HASH_Scan_Table (pPortEntry->pGipTable, u4HashIndex)
        {
            TMO_HASH_Scan_Bucket (pPortEntry->pGipTable, u4HashIndex,
                                  pGipEntry, tGarpGipEntry *)
            {
                for (u2Index = 0; u2Index < pAppEntry->u2MaxBuckets; u2Index++)
                {
                    pAttrEntry = pGipEntry->papAttrTable[u2Index];

                    while (pAttrEntry != NULL)
                    {
                        if (pAttrEntry->u1AdmRegControl == GARP_REG_FIXED)
                        {
                            /*
                             * Make Observers as Members. We propagate 
                             * JOIN IN's on ports
                             * in which static registrations exist.
                             */
                            GarpApplicantSem (pAttrEntry, pPortEntry,
                                              GARP_APP_REQ_JOIN);
                        }

                        pAttrEntry = pAttrEntry->pNextHashNode;
                    }
                }
            }
        }
    }

    /* 
     * Attributes registered on other ports must be propagated 
     * on this port.
     */
    GarpSyncAttributeTable (pAppEntry, pPortEntry);

    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpAppDisablePort                       */
/*  Description     : This function disables the given GARP    */
/*                    application on the specified port.       */
/*                    AppId                                    */
/*  Input(s)        : u1AppId - Application AppId              */
/*                    u2Port - Port number in which the GARP   */
/*                    application is to be disabled.           */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on successful deletion  of  */
/*                    port. GARP_FAILURE on failure            */
/***************************************************************/
INT4
GarpAppDisablePort (UINT1 u1AppId, UINT2 u2LocalPortId)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tGarpGipEntry      *pGipEntry;
    tGarpGipEntry      *pNextGipEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpAttrEntry     *pNextAttrEntry;
    tGarpIfMsg          GarpIfMsg;
    UINT2               u2Index;
    UINT4               u4NumAttrEntries;
    UINT4               u4DelAttrCount;
    UINT4               u4HashIndex = 0;

    if (GARP_PORT_ENTRY (u2LocalPortId) == NULL)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Port unknown. Disabling of Application failed.\n");
        return GARP_FAILURE;
    }
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "GARP NOT enabled. Disabling of Application on Port"
                       "%d failed.\n", GARP_GET_IFINDEX (u2LocalPortId));
        return GARP_FAILURE;
    }

    if (GARP_IS_PORT_VALID (u2LocalPortId) == GARP_FALSE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Invalid Port. Disabling of application failed.\n");
        return GARP_FAILURE;
    }

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Invalid App Id. Disabling Application on Port %d "
                       "failed.\n", GARP_GET_IFINDEX (u2LocalPortId));
        return GARP_FAILURE;
    }

    pPortEntry = GarpGetPortEntry (pAppEntry, u2LocalPortId);

    if (pPortEntry == NULL)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Port %d NOT created. Disabling application failed.\n",
                       GARP_GET_IFINDEX (u2LocalPortId));
        return GARP_FAILURE;
    }

    /* Stop Join timer and LeaveALL timer on this Port */
    if (pPortEntry->JoinTmr.u1IsTmrActive == GARP_TRUE)
    {
        GarpStopTimer (&pPortEntry->JoinTmr);
        pPortEntry->JoinTmr.u1IsTmrActive = GARP_FALSE;
    }

    if (pPortEntry->LeaveAllTmr.u1IsTmrActive == GARP_TRUE)
    {
        GarpStopTimer (&pPortEntry->LeaveAllTmr);
        pPortEntry->LeaveAllTmr.u1IsTmrActive = GARP_FALSE;
    }

    /* Propagate LEAVE on all ports for those attributes which are dynamically
     * registered on this Port */
    /* Delete ALL attribute entries whose admin reg control is REG_NORMAL on 
     * this Port */

    if (pPortEntry->pGipTable == NULL)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "There is no Gip context associated with"
                       "the port %d ", GARP_GET_IFINDEX (u2LocalPortId));
        return GARP_FAILURE;
    }

    TMO_HASH_Scan_Table (pPortEntry->pGipTable, u4HashIndex)
    {
        GARP_HASH_DYN_Scan_Bucket (pPortEntry->pGipTable, u4HashIndex,
                                   pGipEntry, pNextGipEntry, tGarpGipEntry *)
        {
            u4NumAttrEntries = pGipEntry->u4NumEntries;
            u4DelAttrCount = 0;

            /* Scan through the indices */

            for (u2Index = 0; u2Index < pAppEntry->u2MaxBuckets; u2Index++)
            {
                if (pGipEntry == NULL)
                {
                    break;
                }

                pAttrEntry = pGipEntry->papAttrTable[u2Index];
                while (pAttrEntry != NULL)
                {
                    pNextAttrEntry = pAttrEntry->pNextHashNode;

                    /* Propagate LEAVE message for ATTRIBUTES which 
                     * are dynamically registered on this port */

                    if (pAttrEntry->u1AdmRegControl == GARP_REG_NORMAL)
                    {
                        /* Either this attribute entry is dynamically 
                         * registered on this port or we had propagated 
                         * the registration of this attribute from some 
                         * other port or this port might have received 
                         * a leave for the attribute */

                        if ((pAttrEntry->u1RegSemState != GARP_MT)
                            && (pGipEntry != NULL))
                        {
                            /* Attribute dynamically registered on this port...
                             * Propagate LEAVE to all other ports */

                            GarpIfMsg.pAppEntry = pAppEntry;
                            GarpIfMsg.u2GipId = pGipEntry->u2GipId;
                            GarpIfMsg.u2Port = u2LocalPortId;

                            GarpGipGidAttributeLeaveInd (&(pAttrEntry->Attr),
                                                         &GarpIfMsg);
                        }

                        /* Delete This attribute entry anyway */
                        if (pAttrEntry->LeaveTmr.u1IsTmrActive == GARP_TRUE)
                        {
                            GarpStopTimer (&pAttrEntry->LeaveTmr);
                            pAttrEntry->LeaveTmr.u1IsTmrActive = GARP_FALSE;
                        }

                        u4DelAttrCount++;

                        GarpDeleteAttributeEntry (pAppEntry, pPortEntry,
                                                  &pGipEntry, pAttrEntry);
                    }
                    else if (pAttrEntry->u1AdmRegControl == GARP_REG_FIXED)
                    {
                        /*
                         * Make Partipants as observers 
                         */
                        GarpApplicantSem (pAttrEntry, pPortEntry,
                                          GARP_APP_REQ_LEAVE);
                    }

                    pAttrEntry = pNextAttrEntry;
                }

                if (u4DelAttrCount == u4NumAttrEntries)
                {
                    /* 
                     * All attribute entries are deleted and hence Gip 
                     * entry will be deleted. So to avoid accessing Gip 
                     * entry we need to come out of the Bucket scan'for'
                     * loop.
                     */
                    break;
                }
            }
        }
    }

    pPortEntry->u1AppStatus = GARP_DISABLED;

    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpAppEnablePortRestrictedRegControl     */
/*  Description     : This function is for enabling Restricted */
/*                    Registration Control according to IEEE   */
/*                    802.1U/1t on the specified port          */
/*  Input(s)        : u1AppId - Application AppId              */
/*                    u2LocalPortId - Port number on which the */
/*                    application needs to be enabled.         */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns          : GARP_SUCCESS on successful addition of  */
/*                     port. GARP_FAILURE on failure           */
/***************************************************************/
INT4
GarpAppEnablePortRestrictedRegControl (UINT1 u1AppId, UINT2 u2LocalPortId)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tGarpGlobalPortEntry *pGlobPortEntry;

    pGlobPortEntry = GARP_PORT_ENTRY (u2LocalPortId);

    if (pGlobPortEntry == NULL)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Port unknown. Configuring Restricted Registration failed.\n");
        return GARP_FAILURE;
    }
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Garp is disabled. Configuring Restricted Registration on Port %d "
                       "failed \n", GARP_GET_IFINDEX (u2LocalPortId));

        return GARP_FAILURE;
    }

    if (GARP_IS_PORT_VALID (u2LocalPortId) == GARP_FALSE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Invalid Port number. Configuring Restricted Registration  "
                  "failed \n");
        return GARP_FAILURE;
    }

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Unknown application. Configuring Restricted Registration failed "
                       "for port %d.\n", GARP_GET_IFINDEX (u2LocalPortId));
        return GARP_FAILURE;
    }

    pPortEntry = GarpGetPortEntry (pAppEntry, u2LocalPortId);

    if (pPortEntry == NULL)
    {
        /* Seems GarpAppAddPort is not called before this */
        return GARP_FAILURE;
    }

    /* IEEE 802.1u Restricted Registration control.Initially Restricted Registration control
     * is set as GARP_SNMP_FALSE for all ports */
    pPortEntry->u1RestrictedRegControl = GARP_ENABLED;
    /* IEEE 802.1u ends */

    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpAppDisablePortRestrictedRegControl    */
/*  Description     : This function is for disabling Restricted */
/*                    Registration Control according to IEEE   */
/*                    802.1U/1t on the specified port          */
/*  Input(s)        : u1AppId - Application AppId              */
/*                    u2LocalPortId - Port number on which the */
/*                    application needs to be enabled.         */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns          : GARP_SUCCESS on successful addition of  */
/*                     port. GARP_FAILURE on failure           */
/***************************************************************/
INT4
GarpAppDisablePortRestrictedRegControl (UINT1 u1AppId, UINT2 u2LocalPortId)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Garp is disabled. Disabling Restricted Registration on Port %d "
                       "failed \n", GARP_GET_IFINDEX (u2LocalPortId));

        return GARP_FAILURE;
    }

    if (GARP_IS_PORT_VALID (u2LocalPortId) == GARP_FALSE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Invalid Port number. Disabling Restricted Registration "
                  "failed \n");
        return GARP_FAILURE;
    }

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                       "Unknown application. Disabling Restricted Registration  failed "
                       "for port %d.\n", GARP_GET_IFINDEX (u2LocalPortId));
        return GARP_FAILURE;
    }

    pPortEntry = GarpGetPortEntry (pAppEntry, u2LocalPortId);

    if (pPortEntry == NULL)
    {
        /* Seems GarpAppAddPort is not called before this */
        return GARP_FAILURE;
    }

    /* IEEE 802.1u Restricted Registration control.Initially Restricted Registration control
     * is set as GARP_SNMP_FALSE for all ports */
    pPortEntry->u1RestrictedRegControl = GARP_DISABLED;
    /* IEEE 802.1u ends */

    return GARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : GarpRegisterWithPktHandler                           */
/*                                                                           */
/* Description        : This function registers with the packet handler for  */
/*                      GVRP/GMRP control packets.                           */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS/GARP_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpRegisterWithPktHandler ()
{
    static tMacAddr     GvrpMac = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x21 };
    static tMacAddr     GmrpMac = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x20 };

    tProtocolInfo       GarpProtocolInfo;

    /* Register with the packet handler for receiving packets */
    MEMSET (&GarpProtocolInfo, 0, sizeof (tProtocolInfo));

    /* The Function pointer and sento flag are commmon to both the
     * packet types so they are not repeated*/
    GarpProtocolInfo.ProtocolFnPtr = NULL;
    GarpProtocolInfo.u1SendToFlag = MUX_REG_SEND_TO_ISS;

    GarpProtocolInfo.i4Command = MUX_PROT_REG_HDLR;

    MEMCPY (&(GarpProtocolInfo.ProtRegTuple.MacInfo.MacAddr), GvrpMac,
            ETHERNET_ADDR_SIZE);
    MEMSET (GarpProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0xFF,
            ETHERNET_ADDR_SIZE);

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_VRP, &GarpProtocolInfo) != CFA_SUCCESS)
    {
        return GARP_FAILURE;
    }

    /* Register with the packet handler for receiving packets */
    MEMSET (&GarpProtocolInfo, 0, sizeof (tProtocolInfo));

    /* The same call back function has to be called for all STP packets
     * so, we need not copy this each time */
    GarpProtocolInfo.ProtocolFnPtr = NULL;
    GarpProtocolInfo.i4Command = MUX_PROT_REG_HDLR;
    GarpProtocolInfo.u1SendToFlag = MUX_REG_SEND_TO_ISS;

    MEMCPY (&(GarpProtocolInfo.ProtRegTuple.MacInfo.MacAddr), GmrpMac,
            ETHERNET_ADDR_SIZE);
    MEMSET (GarpProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0xFF,
            ETHERNET_ADDR_SIZE);

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_MRP, &GarpProtocolInfo) != CFA_SUCCESS)
    {
        return GARP_FAILURE;
    }

    return GARP_SUCCESS;
}

/* Function Name      : GarpAssignMempoolIds                                 */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's                                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
GarpAssignMempoolIds (VOID)
{
    /*tGarpQMsg */
    gGarpPoolIds.GarpQMsgPoolId =
        GARPMemPoolIds[MAX_GARP_CONFIG_Q_MESG_SIZING_ID];

    /*tGarpMaxBulkMsgSize */
    gGarpPoolIds.GarpQBulkMsgPoolId =
        GARPMemPoolIds[MAX_GARP_BULK_MSG_COUNT_SIZING_ID];

    /*tGarpContextInfo */
    gGarpPoolIds.GarpContextPoolId =
        GARPMemPoolIds[MAX_GARP_CONTEXT_INFO_SIZING_ID];

    /*tGarpPortEntry */
    gGarpPoolIds.PortPoolId = GARPMemPoolIds[MAX_GARP_PORT_ENTRIES_SIZING_ID];

    /*tGarpAttrEntry */
    gGarpPoolIds.AttrPoolId = GARPMemPoolIds[MAX_GARP_ATTR_ENTRIES_SIZING_ID];

    /*tGarpAttrEntry */
    gGarpPoolIds.AttrGvrpHashPoolId =
        GARPMemPoolIds[MAX_GARP_GVRP_ATTR_HASH_BUF_SIZING_ID];

    /*tGarpAttrEntry */
    gGarpPoolIds.AttrGmrpHashPoolId =
        GARPMemPoolIds[MAX_GARP_GMRP_ATTR_HASH_BUF_SIZING_ID];

    /*tGarpGipEntry */
    gGarpPoolIds.GipPoolId = GARPMemPoolIds[MAX_GARP_GIP_ENTRIES_SIZING_ID];

    /*tGarpVlanMapMsgSize */
    gGarpPoolIds.GarpVlanMapMsgPoolId =
        GARPMemPoolIds[MAX_GARP_VLAN_MAP_MSG_COUNT_SIZING_ID];

    /*tGarpAppGipHashArray */
    gGarpPoolIds.AppGipPoolId =
        GARPMemPoolIds[MAX_GARP_MAX_APP_HASH_ARRAY_COUNT_SIZING_ID];

    /*tGarpRemapAttrEntry */
    gGarpPoolIds.RemapAttrPoolId =
        GARPMemPoolIds[MAX_GARP_REMAP_ATTR_ENTRIES_SIZING_ID];

    /*tGarpGlobalPortEntry */
    gGarpPoolIds.GarpGlobPortPoolId =
        GARPMemPoolIds[MAX_GARP_GLOBAL_PORT_ENTRIES_SIZING_ID];

    /*GARP_MAX_MSG_LEN */
    gGarpPoolIds.GarpRemapTxBufPoolId =
        GARPMemPoolIds[MAX_GARP_MSG_COUNT_SIZING_ID];

    /*tGarpGlobPvlanSize */
    gGarpPoolIds.GarpGlobPvlanPoolId =
        GARPMemPoolIds[MAX_GARP_GLOBAL_PVLAN_ENTRIES_SIZING_ID];
    /* tGarpPortStats */
    gGarpPoolIds.GarpPortStatsPoolId =
        GARPMemPoolIds[MAX_GARP_PORT_STAT_ENTRIES_SIZING_ID];
}

#ifdef FSAP_SEM_DEBUG
/*****************************************************************************/
/* Function Name      : GarpLockDbg                                          */
/*                                                                           */
/* Description        : This function is used to take the GARP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : pu1File - File from where the call to this function  */
/*                                is made                                    */
/*                      u4Line  - Line number in pu1File from where the      */
/*                                call to this function is made              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS or GARP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
GarpLockDbg (UINT1 *pu1File, UINT4 u4Line)
{
    if (OsixTakeSemDbg (SELF, GARP_SEM_NAME,
                        OSIX_WAIT, 0, pu1File, u4Line) != OSIX_SUCCESS)
    {
        GARP_GLOBAL_TRC (GARP_MOD_TRC, (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         GARP_NAME,
                         "MSG: Failed to Take GARP Mutual Exclusion Sema4 \n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

#endif /* FSAP_SEM_DEBUG */
