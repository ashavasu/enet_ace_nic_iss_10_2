/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2001-2002                                     */
/* $Id: garpbuf.c,v 1.18 2013/02/14 13:42:09 siva Exp $               */
/*                                                                      */
/*  FILE NAME             : garpbuf.c                                   */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*  SUBSYSTEM NAME        : BUF                                         */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                                */
/*  DESCRIPTION           : This file contains buffer routines          */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#include "garpinc.h"
/************************************************************************/
/* Function Name    : GarpGetBuff ()                                    */
/*                                                                      */
/* Description      : Allocates a buffer of specified type              */
/*                                                                      */
/* Input(s)         : u1BufType - Type of the buffer to be allocated    */
/*                    u4Size    - Size of the buffer. This is used      */
/*                                only when the size of the requested   */
/*                                buffer is a variable.                 */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Pointer to the buffer of the requested type       */
/*                        if buffer is available                        */
/*                    NULL otherwise                                    */
/************************************************************************/
UINT1              *
GarpGetBuff (UINT1 u1BufType, UINT4 u4Size)
{
    UINT1              *pu1Buf = NULL;

    switch (u1BufType)
    {

        case GARP_PORT_BUFF:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.PortPoolId);
            break;

        case GARP_ATTR_BUFF:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.AttrPoolId);
            break;

        case GARP_GIP_BUFF:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.GipPoolId);
            break;

        case GARP_HASH_TABLE_GVRP_BUFF:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.AttrGvrpHashPoolId);
            break;

        case GARP_HASH_TABLE_GMRP_BUFF:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.AttrGmrpHashPoolId);
            break;

        case GARP_SYS_BUFF:
            pu1Buf = (UINT1 *) GARP_SYS_BUFF_ALLOC (u4Size, VLAN_TAG_LEN);
            break;

        case GARP_QMSG_BUFF:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.GarpQMsgPoolId);
            break;

        case GARP_BULK_QMSG_BUFF:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.GarpQBulkMsgPoolId);
            break;

        case GARP_VLAN_MAP_MSG_BUFF:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.GarpVlanMapMsgPoolId);
            break;

        case GARP_REMAP_ATTR_BUFF:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.RemapAttrPoolId);
            break;

        case GARP_APPGIP_HASH_BUFF:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.AppGipPoolId);
            break;

        case GARP_CONTEXT_INFO:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.GarpContextPoolId);
            break;
        case GARP_GLOBPORT_ENTRY:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.GarpGlobPortPoolId);
            break;
        case GARP_REMAP_TXBUFF_INFO:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.GarpRemapTxBufPoolId);
            break;
        case GARP_PORT_STAT_BUFF:
            pu1Buf = MemAllocMemBlk (gGarpPoolIds.GarpPortStatsPoolId);
            break;
        default:
            GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             GARP_NAME,
                             " Memory Allocation failed for Garp \r\n");

            return pu1Buf;
    }

    return pu1Buf;
}

/************************************************************************/
/* Function Name    : GarpReleaseBuff ()                                */
/*                                                                      */
/* Description      : Releases the given buffer.                        */
/*                                                                      */
/* Input(s)         : u1BufType - Type of the buffer to be released     */
/*                    pu1Buf    - Pointer to the buffer to be released  */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
void
GarpReleaseBuff (UINT1 u1BufType, UINT1 *pu1Buf)
{
    switch (u1BufType)
    {

        case GARP_PORT_BUFF:
            MemReleaseMemBlock (gGarpPoolIds.PortPoolId, pu1Buf);
            break;

        case GARP_ATTR_BUFF:
            MemReleaseMemBlock (gGarpPoolIds.AttrPoolId, pu1Buf);
            break;

        case GARP_GIP_BUFF:
            MemReleaseMemBlock (gGarpPoolIds.GipPoolId, pu1Buf);
            break;

        case GARP_HASH_TABLE_GVRP_BUFF:
            MemReleaseMemBlock (gGarpPoolIds.AttrGvrpHashPoolId, pu1Buf);
            break;

        case GARP_HASH_TABLE_GMRP_BUFF:
            MemReleaseMemBlock (gGarpPoolIds.AttrGmrpHashPoolId, pu1Buf);
            break;

        case GARP_SYS_BUFF:
            GARP_SYS_BUFF_RELEASE ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pu1Buf,
                                   0);
            pu1Buf = NULL;
            break;
        case GARP_QMSG_BUFF:

            MemReleaseMemBlock (gGarpPoolIds.GarpQMsgPoolId, pu1Buf);
            break;

        case GARP_BULK_QMSG_BUFF:
            MemReleaseMemBlock (gGarpPoolIds.GarpQBulkMsgPoolId, pu1Buf);
            break;

        case GARP_VLAN_MAP_MSG_BUFF:
            MemReleaseMemBlock (gGarpPoolIds.GarpVlanMapMsgPoolId, pu1Buf);
            break;

        case GARP_REMAP_ATTR_BUFF:
            MemReleaseMemBlock (gGarpPoolIds.RemapAttrPoolId, pu1Buf);
            break;

        case GARP_APPGIP_HASH_BUFF:
            MemReleaseMemBlock (gGarpPoolIds.AppGipPoolId, pu1Buf);
            break;

        case GARP_CONTEXT_INFO:
            MemReleaseMemBlock (gGarpPoolIds.GarpContextPoolId, pu1Buf);
            break;

        case GARP_GLOBPORT_ENTRY:
            MemReleaseMemBlock (gGarpPoolIds.GarpGlobPortPoolId, pu1Buf);
            break;

        case GARP_REMAP_TXBUFF_INFO:
            MemReleaseMemBlock (gGarpPoolIds.GarpRemapTxBufPoolId, pu1Buf);
            break;

        default:
            GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             GARP_NAME, " Memory Release failed for Garp \r\n");
            break;
    }
}
