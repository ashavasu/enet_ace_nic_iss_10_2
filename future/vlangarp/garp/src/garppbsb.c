
#include "garpinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbTransAttrValAndCopy                        */
/*                                                                           */
/*    Description         : This function in case of gvrp, gets the local    */
/*                          vid from L2Iwf for the relay vid in pAttr and    */
/*                          copies the local vid in pu1Ptr.                  */
/*                                                                           */
/*    Input(s)            : u2Port - Port over which the gvrp pkt is to be   */
/*                                   sent.                                   */
/*                          pu1Ptr - buffer where the tranlated vid needs to */
/*                                   to be written.                          */
/*                          pAttr - Pointer to the  GarpAttribute structure  */
/*                                                                           */
/*    Output(s)           : pu1Ptr - In this buffer the translated vid is    */
/*                                   is written.                             */
/*                                                                           */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Return the result of comparision.                 */
/*                         GARP_TRUE                                         */
/*                         GARP_FALSE                                        */
/*                                                                           */
/*****************************************************************************/

VOID
GarpPbTransAttrValAndCopy (UINT2 u2Port, UINT1 *pu1Ptr, tGarpAttr * pAttr)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (pu1Ptr);
    UNUSED_PARAM (pAttr);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbEgrChangeGvrpAddrOnPpnp                    */
/*                                                                           */
/*    Description         : This function will be called by GarpTransmitPdu  */
/*                          funtion. This fuction replaces the dstaddr to    */
/*                          provider gvrp addr, if the port is PPNP and the  */
/*                          given DstAddr is customer gvrp address.          */
/*                                                                           */
/*    Input(s)            : u2Port - Port over which the gvrp pkt is to be   */
/*                                   sent.                                   */
/*                          DstAddr - Dst addr to be filled in the gvrp pkt. */
/*                                                                           */
/*    Output(s)           : DstAddr - Updated value to be filled in the gvrp */
/*                                    to be sent.                            */
/*                                                                           */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/

VOID
GarpPbEgrChangeGvrpAddrOnPpnp (UINT2 u2Port, tMacAddr DstAddr)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (DstAddr);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbCheckPortTypeChange                        */
/*                                                                           */
/*    Description         : This function will be called when a port is      */
/*                          created in Garp. This function get the new port  */
/*                          type from L2IWF and updates it in Garp. If there */
/*                          is a port type change then it handles that.      */
/*                                                                           */
/*    Input(s)            : u2Port - Port that is newly created in Garp.     */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : GARP_SUCCESS / GARP_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
GarpPbCheckPortTypeChange (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);

    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbHandlePortTypeChange                       */
/*                                                                           */
/*    Description         : This function update the port Gvrp status based  */
/*                          PB port type.                                    */
/*                                                                           */
/*    Input(s)            : u2Port - Port that is newly created in Garp.     */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
GarpPbHandlePortTypeChange (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GvrpPbIsPortValidForGvrpEnable                   */
/*                                                                           */
/*    Description         : This function checks whether gvrp can be enabled */
/*                          on the given port.                               */
/*                                                                           */
/*    Input(s)            : u2Port - Port Id.                                */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : Return GVRP_TRUE if gvrp can be enabled on */
/*                                the given  port otherwise GVRP_FALSE.      */
/*                                                                           */
/*****************************************************************************/

INT4
GvrpPbIsPortValidForGvrpEnable (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return GARP_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbIsPortOkForGarpPktRcv                      */
/*                                                                           */
/*    Description         : This function will be called by GarpProcessMsg   */
/*                          funtion. This fuction checks the garp pkt rvd on */
/*                          the given port can be accepeted or not. Gvrp pkts*/
/*                          received cannot be accepted on ports other than  */
/*                          PNP and PPNPs. When a customer gvrp pkt is rcvd  */
/*                          on a PPNP, its is dst addr will be changed to    */
/*                          to the current context gvrp addr.                */
/*                                                                           */
/*    Input(s)            : u2Port - Port over which the gvrp pkt is rcvd.   */
/*                          AppAddress - Dst addr of the rcvd gvrp pkt.      */
/*                                                                           */
/*    Output(s)           : AppAddress - Updated value to be used for        */
/*                                       further processing.                 */
/*                                                                           */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : GARP_TRUE if the pkt can be accepted       */
/*                                otherwise GARP_FALSE.                      */
/*                                                                           */
/*****************************************************************************/

INT4
GarpPbIsPortOkForGarpPktRcv (UINT2 u2Port, tMacAddr AppAddress)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (AppAddress);
    return GARP_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbTranslateAttr                              */
/*                                                                           */
/*    Description         : This function translates the local vid to relay  */
/*                          vid and fills the pAttr accordingly.             */
/*                                                                           */
/*    Input(s)            : u2Port - Port on which the gvrp pkt is received  */
/*                          pAttr - Pointer to the  GarpAttribute structure  */
/*                          pvid  - vid value present in pAttr.              */
/*                                                                           */
/*    Output(s)           : pvid - Translated vid value.                     */
/*                                                                           */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Return the result of comparision.                 */
/*                         GARP_TRUE                                         */
/*                         GARP_FALSE                                        */
/*                                                                           */
/*****************************************************************************/

INT4
GarpPbTranslateAttr (UINT2 u2Port, tGarpAttr * pAttr, tVlanId * pVid)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (pAttr);
    UNUSED_PARAM (pVid);
    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpGetInstPortState                             */
/*                                                                           */
/*    Description         : This function will get the per instance port     */
/*                          state from the L2IWF module. In case of CEP it   */
/*                          returns forwarding                               */
/*                                                                           */
/*    Input(s)            : u2GipId- Instance Id.                            */
/*                          pGlobPortEntry- Global Port Entry                */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : AST_PORT_STATE_FORWARDING/                       */
/*                          AST_PORT_STATE_DISCARDING                        */
/*                                                                           */
/*****************************************************************************/

UINT1
GarpGetInstPortState (UINT2 u2GipId, tGarpGlobalPortEntry * pGlobalPortEntry)
{
    return (GarpL2IwfGetInstPortState
            (u2GipId, (UINT2) pGlobalPortEntry->u4IfIndex));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpGetVlanPortState                             */
/*                                                                           */
/*    Description         : This function will get the per vlan port         */
/*                          state from the L2IWF module. In case of CEP it   */
/*                          returns forwarding                               */
/*                                                                           */
/*    Input(s)            : u2GipId- Instance Id.                            */
/*                          pGlobPortEntry- Global Port Entry                */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : AST_PORT_STATE_FORWARDING/                       */
/*                          AST_PORT_STATE_DISCARDING                        */
/*                                                                           */
/*****************************************************************************/

UINT1
GarpGetVlanPortState (UINT2 u2GipId, tGarpGlobalPortEntry * pGlobalPortEntry)
{
    return (GarpL2IwfGetVlanPortState
            (u2GipId, (UINT2) pGlobalPortEntry->u4IfIndex));
}
