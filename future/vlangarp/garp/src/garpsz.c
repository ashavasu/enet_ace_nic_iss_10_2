/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: garpsz.c,v 1.3 2013/11/29 11:04:16 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _GARPSZ_C
#include "garpinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
GarpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < GARP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsGARPSizingParams[i4SizingId].u4StructSize,
                              FsGARPSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(GARPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            GarpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
GarpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsGARPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, GARPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
GarpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < GARP_MAX_SIZING_ID; i4SizingId++)
    {
        if (GARPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (GARPMemPoolIds[i4SizingId]);
            GARPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
