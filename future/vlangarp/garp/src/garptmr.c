/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: garptmr.c,v 1.19 2013/09/03 15:03:09 siva Exp $
 *
 * Description     : This file contains Timer routines for GARP
 *
 *******************************************************************/

#include "garpinc.h"
/************************************************************************/
/* Function Name    : GarpStartTimer ()                                 */
/*                                                                      */
/* Description      : Starts a timer of the given duration              */
/*                                                                      */
/* Input(s)         : pTimer     - Pointer to the timer structure       */
/*                    u4Duration - The duration of the timer in         */
/*                                 centi seconds                        */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gGarpTmrListId                                    */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : GARP_SUCCESS, if timer is started successfully    */
/*                    GARP_FAILURE otherwise                            */
/************************************************************************/
INT4
GarpStartTimer (tGarpTimer * pTimer, UINT4 u4Duration)
{
    UINT4               u4RetVal = TMR_SUCCESS;
    UINT4               u4TicksSec = 0;
    UINT4               u4TicksCentSec = 0;

    if (GARP_NODE_STATUS () != GARP_NODE_ACTIVE)
    {
        /* 
         * Timers are started only when the node is active. This is 
         * done to avoid sending multiple joins during switchover. During 
         * switchover if timer is started, then join timers will be started
         * for propagating each static/dynamic regs. To avoid this
         * timer will not be started till the relearning leaveALL is sent.
         */
        pTimer->u1IsTmrActive = GARP_FALSE;

        return GARP_SUCCESS;
    }

    /* Donot allow to start for 0 duration */
#ifdef L2RED_WANTED
    if (GARP_HR_STATUS () == GARP_HR_STATUS_DISABLE)
    {
        if (0 == u4Duration)
        {
            return GARP_FAILURE;
        }
    }
#else
    if (0 == u4Duration)
    {
        return GARP_FAILURE;
    }
#endif
    /* GARP Timer values are in centi seconds. Convert the value into centi
       second.
       * If the available system granularity is greater than one centi second,
       the
       * timer will be started in the next possible value. For example, if the
       granularity
       * available is 2 centi seconds and if the timer is started for 1 cs or
       3 cs or 5 cs,
       * the timer will be started for 2 cs, 4cs and 6cs respectively.
       * */

    u4TicksSec = (u4Duration * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
    u4TicksCentSec = u4TicksSec / GARP_SEC_TO_CENT_SEC_UNIT;

    /* Check the num of ticks in the timerval. If it is non multiple of 100,
       add 1 to it. */
    if ((u4TicksSec % GARP_SEC_TO_CENT_SEC_UNIT) != 0)
    {
        /* Granularity does not suit the value. Add one and start timer to
           the next possible value. */
        u4TicksCentSec += GARP_MIN_TIME_TO_START;
    }

    pTimer->pGarpContextInfo = GARP_CURR_CONTEXT_PTR ();

    u4RetVal
        = GARP_TMR_START (gGarpTmrListId, &(pTimer->TmrNode), u4TicksCentSec);

    if (u4RetVal == TMR_SUCCESS)
    {

        pTimer->u1IsTmrActive = GARP_TRUE;
        return GARP_SUCCESS;
    }

#ifdef TRACE_WANTED
    switch (pTimer->u1TmrType)
    {

        case GARP_LEAVE_ALL_TIMER:
            GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                      "LEAVE_ALL Timer Start Failed.\n");
            break;

        case GARP_LEAVE_TIMER:
            GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                      "LEAVE Timer Start Failed.\n");
            break;

        case GARP_JOIN_TIMER:
            GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                      "JOIN Timer Start Failed.\n");
            break;
    }
#endif

    pTimer->u1IsTmrActive = GARP_FALSE;
    return GARP_FAILURE;
}

/************************************************************************/
/* Function Name    : GarpStopTimer ()                                  */
/*                                                                      */
/* Description      : Stops the given timer                             */
/*                                                                      */
/* Input(s)         : pTimer - Pointer to the timer structure           */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gGarpTmrListId                                    */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : GARP_SUCCESS if timer is stopped successfully     */
/*                    GARP_FAILURE otherwise                            */
/************************************************************************/
INT4
GarpStopTimer (tGarpTimer * pTimer)
{
    UINT4               u4RetVal;

    if ((GARP_NODE_STATUS () != GARP_NODE_ACTIVE) &&
        (GARP_NODE_STATUS () != GARP_NODE_FORCE_SWITCHOVER_INPROGRESS))
    {
        /* 
         * Timers are started only when the node is active. Hence if the
         * node is not active, the timer will not be running and there is 
         * no need to stop the timer.                                      
         */
        pTimer->u1IsTmrActive = GARP_FALSE;

        return GARP_SUCCESS;
    }

    pTimer->pGarpContextInfo = NULL;
    u4RetVal = GARP_TMR_STOP (gGarpTmrListId, &(pTimer->TmrNode));

    if (u4RetVal == TMR_SUCCESS)
    {

        pTimer->u1IsTmrActive = GARP_FALSE;

        return GARP_SUCCESS;
    }

#ifdef TRACE_WANTED
    switch (pTimer->u1TmrType)
    {

        case GARP_LEAVE_ALL_TIMER:
            GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                      "LEAVE_ALL Timer Stop Failed.\n");
            break;

        case GARP_LEAVE_TIMER:
            GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                      "LEAVE Timer Stop Failed.\n");
            break;

        case GARP_JOIN_TIMER:
            GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                      "JOIN Timer Stop Failed.\n");
            break;
    }
#endif

    return GARP_FAILURE;
}

/************************************************************************/
/* Function Name    : GarpProcessTimerEvent ()                          */
/*                                                                      */
/* Description      : Process the timer expiry event                    */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gGarpTmrListId                                    */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
void
GarpProcessTimerEvent (void)
{
    tTmrAppTimer       *pTmrBlock;
    tGarpTimer         *pGarpTimer;
#ifdef L2RED_WANTED
    /* HITLESS RESTART */
    UINT1               u1HRTmrFlag = 0;
#endif
    if (gu1GarpIsInitComplete != TRUE)
    {
        return;
    }
    pTmrBlock = TmrGetNextExpiredTimer (gGarpTmrListId);

    while (pTmrBlock != NULL)
    {

        pGarpTimer = (tGarpTimer *) pTmrBlock;
        if ((pGarpTimer != NULL) && (pGarpTimer->pGarpContextInfo != NULL))
        {
            if (GarpSelectContext (pGarpTimer->pGarpContextInfo->u4ContextId) ==
                GARP_FAILURE)
            {
                pTmrBlock = TmrGetNextExpiredTimer (gGarpTmrListId);
                continue;
            }

            GarpProcessTimerExpiry (pGarpTimer);

#ifdef L2RED_WANTED
            /* HITLESS RESTART */
            if ((pGarpTimer->u1TmrType == GARP_JOIN_TIMER)
                && (GARP_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE)
                && (u1HRTmrFlag == 0))
            {
                /* Periodic timer has been made to expire after processing
                 * steady state pkt request from RM. This flag indicates
                 * to send steady tail msg after sending all the steady
                 * state packets of ERPS to RM. */
                u1HRTmrFlag = 1;
            }
#endif
            GarpReleaseContext ();
        }
        pTmrBlock = TmrGetNextExpiredTimer (gGarpTmrListId);
    }
#ifdef L2RED_WANTED
    /* HITLESS RESTART */
    if (u1HRTmrFlag == 1)
    {
        if (GarpSelectContext (GARP_DEFAULT_CONTEXT_ID) != GARP_FAILURE)
        {
            /* sending steady state tail msg to RM */
            GarpRedHRSendStdyStTailMsg ();
            GARP_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
            GarpReleaseContext ();
        }
    }
#endif
}
