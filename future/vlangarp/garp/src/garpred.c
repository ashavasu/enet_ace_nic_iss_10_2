/* $Id: garpred.c,v 1.29 2013/12/07 11:09:14 siva Exp $ */
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : garpred.c                                   */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : MAIN                                        */
/*  MODULE NAME           : GARP Redundancy module                      */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains routines added for       */
/*                          supporting GARP redundancy.                 */
/************************************************************************/
#include "garpinc.h"

#ifdef GARP_WANTED
/*****************************************************************************/
/* Function Name      : GarpRedProcessMsg                                    */
/*                                                                           */
/* Description        : This function handles the redundancy related         */
/*                      messages from the RM module.                         */
/*                                                                           */
/* Input(s)           : pGarpQMsg - Pointer to the Q message.                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedProcessMsg (tGarpQMsg * pGarpQMsg)
{
    UINT4               u4Events;

    u4Events = pGarpQMsg->unGarpMsg.RedMsg.u4Events;

    switch (u4Events)
    {
        case GO_ACTIVE:

            if ((GARP_NODE_STATUS () == GARP_NODE_ACTIVE) ||
                (GARP_NODE_STATUS () == GARP_NODE_FORCE_SWITCHOVER_INPROGRESS))
            {
                GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_RED_TRC,
                                 CONTROL_PLANE_TRC, GARP_NAME,
                                 "GARP module already ACTIVE or becoming ACTIVE. !!!\n");
            }
            else if (GARP_NODE_STATUS () == GARP_NODE_IDLE)
            {
                GarpRedMakeNodeActiveFromIdle ();
            }
            else if (GARP_NODE_STATUS () == GARP_NODE_STANDBY)
            {
                GarpRedMakeNodeActiveFromStandby ();
            }
            else
            {
                GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_RED_TRC,
                                 CONTROL_PLANE_TRC, GARP_NAME,
                                 "GARP module status unknown. !!!\n");
            }
            break;

        case GO_STANDBY:
            if (GARP_NODE_STATUS () == GARP_NODE_STANDBY)
            {
                GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_RED_TRC,
                                 CONTROL_PLANE_TRC, GARP_NAME,
                                 "GARP module already in STANDBY state. !!!\n");
            }
            else if (GARP_NODE_STATUS () == GARP_NODE_IDLE)
            {
                GarpRedMakeNodeStandbyFromIdle ();
            }
            else if ((GARP_NODE_STATUS () == GARP_NODE_ACTIVE) ||
                     (GARP_NODE_STATUS () ==
                      GARP_NODE_FORCE_SWITCHOVER_INPROGRESS))
            {
                GarpRedMakeNodeStandbyFromActive ();
            }
            else
            {
                GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_RED_TRC,
                                 CONTROL_PLANE_TRC, GARP_NAME,
                                 "GARP module status unknown. !!!\n");
            }
            break;

        case RM_INIT_HW_AUDIT:
            /* HW Audit is not implemented for Garp Module */
            break;

/* HITLESS RESTART */
        case GARP_HR_STDY_ST_PKT_REQ:
            GarpRedHRProcStdyStPktReq ();
            break;

        default:

            GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_RED_TRC,
                             CONTROL_PLANE_TRC, GARP_NAME,
                             "GARP Module received unknown redundancy message. !!!\n");

            break;
    }
}

/*****************************************************************************/
/* Function Name      : GarpRedMakeNodeActiveFromIdle                        */
/*                                                                           */
/* Description        : This function makes the node active from the IDLE    */
/*                      state. This function enables the GVRP/GMRP module    */
/*                      if their admin status is enabled.                    */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedMakeNodeActiveFromIdle (VOID)
{
    INT4                i4RetVal;
    UINT4               u4ContextId;

    GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_RED_TRC, CONTROL_PLANE_TRC,
                     GARP_NAME, "Moving node from IDLE to ACTIVE \n");

    /* Set the node status first to ACTIVE so that the timers
     * will be started asap */
    GARP_NODE_STATUS () = GARP_NODE_ACTIVE;

    for (u4ContextId = 0; u4ContextId < GARP_MAX_CONTEXTS; u4ContextId++)
    {
        if (GarpSelectContext (u4ContextId) == GARP_FAILURE)
        {
            continue;
        }

        if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
        {
            GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                      "GARP is shutdown. Ignoring GO_ACTIVE trigger from VLAN.\n");
            GarpReleaseContext ();
            continue;
        }

        if (GVRP_ADMIN_STATUS () == GVRP_ENABLED)
        {
            i4RetVal = GvrpEnable ();

            if (i4RetVal == GVRP_FAILURE)
            {
                GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                          "Enabling GVRP failed when the node is"
                          "made ACTIVE \n");

            }
        }

        if (GMRP_ADMIN_STATUS () == GMRP_ENABLED)
        {
            i4RetVal = GmrpEnable ();

            if (i4RetVal == GMRP_FAILURE)
            {
                GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                          "Enabling GMRP failed when the node is"
                          "made ACTIVE \n");
            }
        }
        GarpReleaseContext ();
    }
}

/*****************************************************************************/
/* Function Name      : GarpRedMakeNodeStandbyFromIdle                       */
/*                                                                           */
/* Description        : This function makes the node standby from the IDLE   */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedMakeNodeStandbyFromIdle (VOID)
{
    INT4                i4RetVal;
    UINT4               u4ContextId;

    GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_RED_TRC, CONTROL_PLANE_TRC,
                     GARP_NAME, "Moving node from IDLE to STANDBY \n");

    GARP_NODE_STATUS () = GARP_NODE_STANDBY;

    for (u4ContextId = 0; u4ContextId < GARP_MAX_CONTEXTS; u4ContextId++)
    {
        if (GarpSelectContext (u4ContextId) == GARP_FAILURE)
        {
            continue;
        }

        if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
        {
            GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                      "GARP is shutdown state.\n");
            GarpReleaseContext ();
            continue;
        }

        if (GVRP_ADMIN_STATUS () == GVRP_ENABLED)
        {
            i4RetVal = GvrpEnable ();

            if (i4RetVal == GVRP_FAILURE)
            {
                GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                          "Enabling GVRP failed when the node is"
                          "made STANDBY\n");
            }
        }

        if (GMRP_ADMIN_STATUS () == GMRP_ENABLED)
        {
            i4RetVal = GmrpEnable ();

            if (i4RetVal == GMRP_FAILURE)
            {
                GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                          "Enabling GMRP failed when the node is"
                          "made STANDBY\n");

            }
        }
        GarpReleaseContext ();
    }
}

/*****************************************************************************/
/* Function Name      : GarpRedMakeNodeActiveFromStandby                     */
/*                                                                           */
/* Description        : This function makes the node active from the standby */
/*                      state. This function invokes function to read GVRP   */
/*                      and GMRP learnt ports table and constructs the GARP  */
/*                      application database.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedMakeNodeActiveFromStandby (VOID)
{
    UINT4               u4ContextId;

    gu1GarpStandbyToActive = GARP_TRUE;
    /* Make node active here since sending leaveALL will make attributes
     * anxious and so we need to start the join timer */

    GARP_NODE_STATUS () = GARP_NODE_ACTIVE;

    for (u4ContextId = 0; u4ContextId < GARP_MAX_CONTEXTS; u4ContextId++)
    {
        if (GarpSelectContext (u4ContextId) == GARP_FAILURE)
        {
            continue;
        }

        if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
        {
            GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC,
                      GARP_NAME, "GARP is in shutdown state.\n");

            GarpReleaseContext ();
            continue;
        }

        if ((GVRP_IS_GVRP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_TRUE) ||
            (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_TRUE))
        {
            GarpRedSendLeaveALL ();
        }
        GarpReleaseContext ();
    }
    gu1GarpStandbyToActive = GARP_FALSE;
}

/*****************************************************************************/
/* Function Name      : GarpRedMakeNodeStandbyFromActive                     */
/*                                                                           */
/* Description        : This function makes the node standby from the active */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedMakeNodeStandbyFromActive (VOID)
{
    UINT4               u4ContextId;

    GARP_NODE_STATUS () = GARP_NODE_FORCE_SWITCHOVER_INPROGRESS;

    for (u4ContextId = 0; u4ContextId < GARP_MAX_CONTEXTS; u4ContextId++)
    {
        if (GarpSelectContext (u4ContextId) == GARP_FAILURE)
        {
            continue;
        }

        if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
        {
            GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                      "GARP is shutdown. Ignoring GO_STANDBY trigger from VLAN.\n");
            GarpReleaseContext ();
            continue;;
        }

        if (GVRP_IS_GVRP_ENABLED_IN_CONTEXT (u4ContextId) == GVRP_TRUE)
        {
            if (GvrpDisable () != GVRP_SUCCESS)
            {
                GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                          "GVRP Disable FAILED during Active to Standby Transition\n");
            }
            if (GvrpEnable () != GVRP_SUCCESS)
            {
                GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                          "GVRP Enable FAILED during Active to Standby Transition\n");
            }
        }

        if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (u4ContextId) == GMRP_TRUE)
        {
            if (GmrpDisable () != GMRP_SUCCESS)
            {
                GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                          "GMRP Disable FAILED during Active to Standby Transition\n");

            }
            if (GmrpEnable () != GVRP_SUCCESS)
            {
                GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                          "GMRP Enable FAILED during Active to Standby Transition\n");

            }
        }

        GarpReleaseContext ();
    }

    GARP_NODE_STATUS () = GARP_NODE_STANDBY;
}

/*****************************************************************************/
/* Function Name      : GarpRedApplyDynamicData                              */
/*                                                                           */
/* Description        : This function reads GVRP and GMRP learnt ports table */
/*                      and issues GID join indications for the ports set    */
/*                      in the learnt port list.                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedApplyDynamicData (VOID)
{
    tVlanRedGvrpLearntEntry *pGvrpEntry;
    tGarpAppEntry      *pAppEntry;
    tGarpAttr           Attr;
    tGarpIfMsg          GarpIfMsg;
    tGarpRedWalkMsg     GarpWalkMsg;
    tVlanId             VlanId;

    GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
              "Applying Dynamic Data \n");

    pAppEntry = GarpGetAppEntryWithAddr (gGvrpAddr);

    if (pAppEntry != NULL)
    {
        /* GVRP is enabled */
        GarpIfMsg.pAppEntry = pAppEntry;
        MEMSET (GarpIfMsg.SrcMacAddr, 0, sizeof (tMacAddr));
        GarpIfMsg.u2GipId = 0;
        GarpIfMsg.u2Len = 0;    /* not checked from GarpGidJoinInd */

        Attr.u1AttrType = GARP_GROUP_ATTR_TYPE;
        Attr.u1AttrLen = GVRP_VLAN_ID_LEN;

        VLAN_RED_LOCK ();

        GARP_SLL_SCAN (&VLAN_GVRP_LRNT_PORT_LIST (),
                       pGvrpEntry, tVlanRedGvrpLearntEntry *)
        {
            VlanId = OSIX_HTONS (pGvrpEntry->VlanId);
            MEMCPY (Attr.au1AttrVal, (UINT1 *) &VlanId, GVRP_VLAN_ID_LEN);

            GarpIfMsg.u2GipId =
                GarpL2IwfMiGetVlanInstMapping (GARP_CURR_CONTEXT_PTR ()->
                                               u4ContextId, pGvrpEntry->VlanId);

            GARP_TRC_ARG1 (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                           "Applying GVRP Dynamic Data for VLAN %d "
                           "with Ports - ", pGvrpEntry->VlanId);

            GARP_PRINT_PORT_LIST (GARP_RED_TRC, pGvrpEntry->LearntPortList);

            GarpRedApplyLearntPortList (&GarpIfMsg, &Attr,
                                        pGvrpEntry->LearntPortList, GARP_FALSE);
        }

        VLAN_RED_UNLOCK ();
    }
    else
    {
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  "GVRP Disabled. Ignoring GVRP learnt table \n");
    }

    pAppEntry = GarpGetAppEntryWithAddr (gGmrpAddr);

    if (pAppEntry != NULL)
    {
        /* GVRP is enabled */
        GarpIfMsg.pAppEntry = pAppEntry;
        MEMSET (GarpIfMsg.SrcMacAddr, 0, sizeof (tMacAddr));
        /* GIP Id will be filled later */
        GarpIfMsg.u2Len = 0;    /* not checked from GarpGidJoinInd */

        Attr.u1AttrType = GARP_GROUP_ATTR_TYPE;
        Attr.u1AttrLen = ETHERNET_ADDR_SIZE;

        GarpWalkMsg.pGarpIfMsg = &GarpIfMsg;
        GarpWalkMsg.pAttr = &Attr;

        VLAN_RED_LOCK ();

        /* RBTreeWalk will handle NULL Tree pointer also */
        RBTreeWalk (VLAN_GMRP_LRNT_PORTS_TBL (),
                    GarpRedApplyGmrpLearntPorts, (VOID *) &GarpWalkMsg, NULL);

        VLAN_RED_UNLOCK ();
    }
    else
    {
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  "GMRP Disabled. Ignoring GMRP learnt table \n");
    }
}

/*****************************************************************************/
/* Function Name      : GarpRedApplyGmrpLearntPorts                          */
/*                                                                           */
/* Description        : This function will be invoked for each entry in the  */
/*                      GMRP learnt ports tree. This function fills the GIP  */
/*                      ID and attribute value and then invokes another fn.  */
/*                      to issue Join indications.                           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
GarpRedApplyGmrpLearntPorts (tRBElem * pRBElem, eRBVisit visit,
                             UINT4 u4Level, VOID *pArg, VOID *pOut)
{
    tVlanRedGmrpLearntEntry *pGmrpEntry;
    tGarpRedWalkMsg    *pGarpWalkMsg;
    tGarpIfMsg         *pIfMsg;
    tGarpAttr          *pAttr;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pOut);

    pGmrpEntry = (tVlanRedGmrpLearntEntry *) pRBElem;

    if (visit == leaf || visit == postorder)
    {
        pGarpWalkMsg = (tGarpRedWalkMsg *) pArg;

        pIfMsg = pGarpWalkMsg->pGarpIfMsg;

        pAttr = pGarpWalkMsg->pAttr;

        /* Fill GarpIfMsg */
        pIfMsg->u2GipId = pGmrpEntry->VlanId;

        /* Fill Attr */
        MEMCPY (pAttr->au1AttrVal, pGmrpEntry->McastAddr, ETHERNET_ADDR_SIZE);

        GARP_TRC_ARG1 (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                       "Applying GMRP Dynamic Data for VLAN %d, Mcast Address - ",
                       pGmrpEntry->VlanId);

        GARP_PRINT_MAC_ADDR (GARP_RED_TRC, pGmrpEntry->McastAddr);

        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME, "Port List - ");

        GARP_PRINT_PORT_LIST (GARP_RED_TRC, pGmrpEntry->LearntPortList);

        GarpRedApplyLearntPortList (pIfMsg, pAttr, pGmrpEntry->LearntPortList,
                                    GARP_FALSE);
    }

    return RB_WALK_CONT;
}

/*****************************************************************************/
/* Function Name      : GarpRedApplyLearntPortList                           */
/*                                                                           */
/* Description        : This function scans the given learnt port list and   */
/*                      issues joins for the ports set in the learnt port    */
/*                      list.                                                */
/*                                                                           */
/* Input(s)           : pIfMsg - Pointer to the GARP Interface message.      */
/*                      pAttr - Pointer to the GARP Attribute.               */
/*                      PortList - Portlist to br programmed.                */
/*                      u1IsFoundInAudit - Indicates whether this entry is   */
/*                      found during AUDIT or not.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedApplyLearntPortList (tGarpIfMsg * pIfMsg, tGarpAttr * pAttr,
                            tLocalPortList PortList, UINT1 u1IsFoundInAudit)
{
    UINT1              *pFailedPortList = NULL;
    INT4                i4Result;
    INT4                i4RetVal;
    tGarpPortEntry     *pPortEntry;
    UINT2               u2Port;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag = 0;

    pFailedPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pFailedPortList == NULL)
    {
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  "GarpRedApplyLearntPortList: "
                  "Error in allocating memory for pFailedPortList\r\n");
        return;
    }
    MEMSET (pFailedPortList, 0, sizeof (tLocalPortList));

    for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
    {
        if (PortList[u2ByteInd] != 0)
        {
            u1PortFlag = PortList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {
                if ((u1PortFlag & VLAN_BIT8) != 0)
                {
                    u2Port =
                        (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                 u2BitIndex + 1);

                    /* L2RED_VLAN_ST */
                    pIfMsg->u2Port = u2Port;

                    pPortEntry = GarpGetPortEntry (pIfMsg->pAppEntry, u2Port);

                    if (pPortEntry == NULL)
                    {

                        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                                  "Lrnt port set for an unknown port\n");

                        GARP_SET_MEMBER_PORT (pFailedPortList, u2Port,
                                              sizeof (tLocalPortList));
                        u1PortFlag = (UINT1) (u1PortFlag << 1);

                        continue;
                    }

                    if (pPortEntry->u1AppStatus == GARP_DISABLED)
                    {
                        /* Application disabled on this Port */
                        GARP_TRC (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                                  "GVRP Learnt port set on a disabled port\n");

                        /* Port is not enabled for this application */
                        GARP_SET_MEMBER_PORT (pFailedPortList, u2Port,
                                              sizeof (tLocalPortList));
                        u1PortFlag = (UINT1) (u1PortFlag << 1);

                        continue;
                    }

                    i4Result = GARP_TRUE;

                    if (pPortEntry->u1RestrictedRegControl == GARP_TRUE)
                    {
                        /* Restricted reg control enabled on this port */
                        i4RetVal =
                            pIfMsg->pAppEntry->pAttrRegValidateFn (*pAttr,
                                                                   u2Port,
                                                                   pIfMsg->
                                                                   u2GipId);

                        if (i4RetVal == GARP_FALSE)
                        {
                            i4Result = GARP_FALSE;

                            GARP_TRC (GARP_MOD_TRC, CONTROL_PLANE_TRC,
                                      GARP_NAME,
                                      "GVRP Learnt port set with no static"
                                      "VLAN \n");
                            GARP_SET_MEMBER_PORT (pFailedPortList, u2Port,
                                                  sizeof (tLocalPortList));
                        }
                    }            /* Restricted Reg Control */

                    if (i4Result == GARP_TRUE)
                    {
                        GarpGidJoinInd (pAttr, pIfMsg, pPortEntry,
                                        GARP_JOIN_EMPTY);
                    }
                }                /* if (u1PortFlag & VLAN_BIT8) */

                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }                    /* scanning for 8 bits */
        }                        /* Some bit set in the port byte */
    }                            /* For each byte in the port list */

#ifdef NPAPI_WANTED
    if (u1IsFoundInAudit == GARP_TRUE)
    {
        /* will be true only for mcast adds detected during audit */
        if (MEMCMP (pFailedPortList, gNullPortList, sizeof (tLocalPortList)) !=
            0)
        {
            VlanRedNotifyMcastAddFailure (pIfMsg->u2GipId,
                                          pAttr->au1AttrVal,
                                          PortList, pFailedPortList);
        }
    }
#else
    UNUSED_PARAM (u1IsFoundInAudit);
#endif /* NPAPI_WANTED */
    UtilPlstReleaseLocalPortList (pFailedPortList);
}

/*****************************************************************************/
/* Function Name      : GarpProcessCfgQMsgs                                  */
/*                                                                           */
/* Description        : This function processes the config queue messages.   */
/*                      This function should be used in case if VLAN posts   */
/*                      static configurations to GARP queue.                 */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpProcessCfgQMsgs (VOID)
{
    tGarpQMsg          *pGarpQMsg;

    while (GARP_RECV_FROM_QUEUE (GARP_CFG_QUEUE_ID,
                                 (UINT1 *) &pGarpQMsg,
                                 OSIX_DEF_MSG_LEN,
                                 OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        /* Buffer freed inside this function */
        GarpProcessConfigMsg (pGarpQMsg);
    }
}

/*****************************************************************************/
/* Function Name      : GarpRedSendLeaveALL                                  */
/*                                                                           */
/* Description        : This function sends GVRP and GMRP leaveALL messages  */
/*                      on all ports for their corresponding GIP context.    */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedSendLeaveALL (VOID)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    UINT4               u4LeaveAllTime;
    UINT4               u4RandLeaveAllTime;
    UINT2               u2Index;
    tGarpGlobalPortEntry *pGlobalPortEntry;

    /* Scan thru all the enrolled applications */
    for (u2Index = 1; u2Index <= GARP_MAX_APPS; u2Index++)
    {
        pAppEntry = &GARP_CURR_CONTEXT_PTR ()->aGarpAppTable[u2Index];

        if (pAppEntry->u1Status != GARP_VALID)
        {
            GARP_TRC_ARG1 (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                           "AppId - %d is not enabled \n", u2Index);

            continue;
        }

        for (pPortEntry = pAppEntry->pPortTable;
             pPortEntry != NULL; pPortEntry = pPortEntry->pNextNode)
        {
            pGlobalPortEntry = GARP_PORT_ENTRY (pPortEntry->u2Port);

            if (pPortEntry->u1AppStatus != GARP_ENABLED)
            {
                GARP_TRC_ARG2 (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                               "GARP not enabled on port %d for app %d."
                               "LeaveALL not sent. \n", pAppEntry->u1AppId,
                               pGlobalPortEntry->u4IfIndex);
                continue;
            }

            /* Start leaveALL timer for this port */
            pPortEntry->u1LeaveAllSemState = GARP_PASSIVE;

            u4LeaveAllTime =
                GARP_CURR_CONTEXT_PTR ()->apGarpGlobalPortEntry[pPortEntry->
                                                                u2Port]->
                u4LeaveAllTime;
            GARP_GET_RANDOM_LEAVEALL_TIME (u4LeaveAllTime, u4RandLeaveAllTime);

            if (GarpStartTimer (&pPortEntry->LeaveAllTmr, u4RandLeaveAllTime)
                == GARP_FAILURE)
            {

                GARP_TRC_ARG1 (GARP_RED_TRC, ALL_FAILURE_TRC, GARP_NAME,
                               "Starting LeaveALL timer failed.\n",
                               GARP_GET_IFINDEX (pPortEntry->u2Port));

            }

            /* Invoke function to send leaveALL */
            GarpSendLeaveAll (pAppEntry, pPortEntry);
        }                        /* for (pPortEntry)... */
    }                            /* For each application */
}

/*****************************************************************************/
/* Function Name      : GarpRedSendMsg                                       */
/*                                                                           */
/* Description        : This function sends GO_ACTIVE and GO_STANDBY msgs    */
/*                      to GARP module. This fn. will be invoked by VLAN.    */
/*                                                                           */
/* Input(s)           : u4MessageType                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedSendMsg (UINT4 u4MessageType)
{
    tGarpQMsg          *pGarpMsg;

    if (gu1GarpIsInitComplete != GARP_TRUE)
    {
        GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_RED_TRC,
                         CONTROL_PLANE_TRC | ALL_FAILURE_TRC, GARP_NAME,
                         "GARP not yet initialized. Ignoring "
                         "the redundancy message from VLAN \n");
        return;
    }

    pGarpMsg =
        (tGarpQMsg *) (VOID *) GARP_GET_BUFF (GARP_QMSG_BUFF,
                                              sizeof (tGarpQMsg));

    if (pGarpMsg == NULL)
    {
        GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_RED_TRC,
                         CONTROL_PLANE_TRC | ALL_FAILURE_TRC, GARP_NAME,
                         "No memory to send message to GARP \n");
        return;
    }

    MEMSET (pGarpMsg, 0, sizeof (tGarpQMsg));

    pGarpMsg->u2MsgType = GARP_RED_MSG;

    pGarpMsg->unGarpMsg.RedMsg.u4Events = u4MessageType;

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpMsg,
                            OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_RED_TRC,
                         ALL_FAILURE_TRC, GARP_NAME,
                         "RM Message Enqueue to GARP Q FAILED\n");

        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, (UINT1 *) pGarpMsg);

        return;
    }

    if (GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT) != OSIX_SUCCESS)
    {
        GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_RED_TRC,
                         ALL_FAILURE_TRC, GARP_NAME, "RM Event send FAILED\n");

        return;
    }
}

/*****************************************************************************/
/* Function Name      : GarpRedProcessMcastAddMsg                            */
/*                                                                           */
/* Description        : This function will be invoked to add a new attribute */
/*                      in the GMRP attribute table.                         */
/*                      This function will be invoked when a multicast entry */
/*                      is present in hardware but not present in software.  */
/*                                                                           */
/* Input(s)           : pGarpQMsg - GARP Q message.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedProcessMcastAddMsg (tGarpQMsg * pGarpQMsg)
{
    tGarpAppEntry      *pAppEntry;
    tGarpIfMsg          GarpIfMsg;
    tGarpAttr           Attr;

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (pGarpQMsg->u4ContextId) == GARP_FALSE)
    {
        return;
    }

    MEMSET (&GarpIfMsg, 0, sizeof (tGarpIfMsg));
    MEMSET (&Attr, 0, sizeof (tGarpAttr));

    pAppEntry = GarpGetAppEntryWithAddr (gGmrpAddr);

    if (pAppEntry != NULL)
    {
        /* GMRP is enabled */
        GarpIfMsg.pAppEntry = pAppEntry;
        GarpIfMsg.u2GipId = pGarpQMsg->unGarpMsg.RedAddMsg.VlanId;
        GarpIfMsg.u2Len = 0;    /* not checked from GarpGidJoinInd */

        Attr.u1AttrType = GARP_GROUP_ATTR_TYPE;
        Attr.u1AttrLen = ETHERNET_ADDR_SIZE;
        MEMCPY (Attr.au1AttrVal, pGarpQMsg->unGarpMsg.RedAddMsg.MacAddr,
                ETHERNET_ADDR_SIZE);

        GarpRedApplyLearntPortList (&GarpIfMsg, &Attr,
                                    pGarpQMsg->unGarpMsg.RedAddMsg.PortList,
                                    GARP_TRUE);
    }
}

/*****************************************************************************/
/* Function Name      : GarpRestartModule                                    */
/*                                                                           */
/* Description        : This function will restart GARP module               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS/GARP_FAILURE.                           */
/*****************************************************************************/
INT4
GarpRestartModule ()
{
    UINT4               u4ContextId;

    GARP_LOCK ();

    for (u4ContextId = 0; u4ContextId < GARP_MAX_CONTEXTS; u4ContextId++)
    {
        if (GarpSelectContext (u4ContextId) == GARP_FAILURE)
        {
            continue;
        }

        if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
        {
            GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                      "GARP is shutdown. Ignoring GO_ACTIVE trigger from VLAN.\n");
            GarpReleaseContext ();
            continue;
        }

        if (GarpDeInit () != GARP_SUCCESS)
        {
            GARP_TRC (GARP_RED_TRC, ALL_FAILURE_TRC, GARP_NAME,
                      "GARP shutdown failed during restart for applying "
                      "configuration database\n");
            GarpReleaseContext ();
            continue;
        }

        if (GarpInit () != GARP_SUCCESS)
        {
            GARP_TRC (GARP_RED_TRC, ALL_FAILURE_TRC, GARP_NAME,
                      "GARP start failed during restart for applying "
                      "configuration database\n");
            GarpReleaseContext ();
            continue;
        }
        GarpReleaseContext ();

    }
    GARP_NODE_STATUS () = GARP_NODE_IDLE;

    GARP_UNLOCK ();

    return GARP_SUCCESS;
}

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : GarpRedHRProcStdyStPktReq                            */
/*                                                                           */
/* Description        : This function triggers the steady state packet from  */
/*                      GARP to the RM by setting the periodic timeout value */
/*                      as zero                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedHRProcStdyStPktReq (VOID)
{
    tGarpAppEntry      *pAppEntry = NULL;
    tGarpPortEntry     *pPortEntry = NULL;
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2Index = 0;
    UINT1               u1TailFlag = 0;

    if (GARP_NODE_STATUS () != GARP_NODE_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  " Node is not active. "
                  "Processing of steady state pkt request failed.\n");
        return;
    }
    if (GARP_HR_STATUS () == GARP_HR_STATUS_DISABLE)
    {
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  " Hitless restart is not enabled."
                  " Processing of steady state pkt request failed.\n");
        return;
    }
    if (GARP_CURR_CONTEXT_PTR () == NULL)
    {
        return;
    }

    GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
              "starts sending steady state packets to RM.\n");

    /* This flag is set to indicate the GARP timer expiry handler that steady  
     * state request had received from RM */

    GARP_HR_STDY_ST_REQ_RCVD () = OSIX_TRUE;

    for (u4ContextId = 0; u4ContextId < GARP_MAX_CONTEXTS; u4ContextId++)
    {
        if ((GarpSelectContext (u4ContextId) == GARP_FAILURE)
            || (GarpIsGarpEnabled () == GARP_DISABLED))
        {
            continue;
        }
        for (u2Index = 1; u2Index <= GARP_MAX_APPS; u2Index++)
        {
            pAppEntry = GARP_GET_APP_ENTRY (u2Index);
            if (pAppEntry == NULL)
            {
                GARP_TRC_ARG1 (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                               "AppId - %d is not enabled \n", u2Index);

                continue;
            }
            for (pPortEntry = pAppEntry->pPortTable;
                 pPortEntry != NULL; pPortEntry = pPortEntry->pNextNode)
            {
                pGlobalPortEntry = GARP_PORT_ENTRY (pPortEntry->u2Port);

                if (pPortEntry->u1AppStatus != GARP_ENABLED)
                {
                    GARP_TRC_ARG2 (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                                   "GARP not enabled on port %d for app %d."
                                   "LeaveALL not sent. \n", pAppEntry->u1AppId,
                                   pGlobalPortEntry->u4IfIndex);
                    continue;
                }
                GarpStopTimer (&pPortEntry->JoinTmr);
                if (GarpStartTimer (&pPortEntry->JoinTmr, 0 /* Duration */ )
                    == GARP_FAILURE)
                {

                    GARP_TRC_ARG1 (GARP_RED_TRC, ALL_FAILURE_TRC, GARP_NAME,
                                   "Starting Join timer failed.\n",
                                   GARP_GET_IFINDEX (pPortEntry->u2Port));
                }
                if (u1TailFlag == 0)
                {
                    u1TailFlag = GARP_HR_STDY_ST_PKT_TAIL;
                }

            }

        }
    }
    /* This steady state tail msg sending is done in case of GARP periodic
     *      * timers are not running. */
    if (u1TailFlag != GARP_HR_STDY_ST_PKT_TAIL)
    {
        if (GarpSelectContext (GARP_DEFAULT_CONTEXT_ID) != GARP_FAILURE)
        {
            GarpRedHRSendStdyStTailMsg ();

            /* This flag is set to indicate the GARP timer expiry handler 
             * that steady state request had received from RM */
            GARP_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;

            GarpReleaseContext ();
        }

    }

    return;
}

/*****************************************************************************/
/* Function Name      : GarpRedHRSendStdyStPkt                               */
/*                                                                           */
/* Description        : This function sends steady state packet to RM.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS/FAILURE.                                     */
/*****************************************************************************/
PUBLIC INT1
GarpRedHRSendStdyStPkt (UINT1 *pu1LinBuf, UINT4 u4PktLen, UINT2 u2Port,
                        UINT4 u4TimeOut)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2BufSize = 0;

    if (GARP_NODE_STATUS () != GARP_NODE_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  " Node is not active. "
                  "Sending of steady state pkt failed.\n");
        return GARP_SUCCESS;
    }
    if (GARP_HR_STATUS () == GARP_HR_STATUS_DISABLE)
    {
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  " Hitless restart is not enabled."
                  " Sending of steady state pkt failed.\n");
        return GARP_SUCCESS;
    }

    /* Forming  steady state packet
     *
     *    <- 24B --><---- 1B -------------><-- 2B -->< 2B -><-- 4B -->< 60 B >
     *    ____________________________________________________________________
     *   |        |                       |         |      |         |        |
     *   | RM Hdr | RM_HR_STDY_ST_PKT_MSG | Msg Len | PORT | TimeOut | Buffer |
     *   |________|_______________________|_________|______|_________|________|
     * 
     * The RM Hdr shall be included by RM.
     * */

    u2BufSize = GARP_RED_TYPE_FIELD_SIZE + GARP_RED_LEN_FIELD_SIZE + sizeof (UINT2)    /* for port */
        + sizeof (UINT4) /* Timeout */  + u4PktLen;
    /* Allocate memory for data to be sent to RM. This memory will be freed by
     *      * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  "RM Alloc failed\n");
        return (GARP_FAILURE);
    }

    /* Fill the message type. */
    u2MsgLen = u4PktLen + sizeof (UINT4) + sizeof (UINT2);

    VLAN_RM_PUT_1_BYTE (pMsg, &u4Offset, RM_HR_STDY_ST_PKT_MSG);
    VLAN_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);
    VLAN_RM_PUT_2_BYTE (pMsg, &u4Offset, u2Port);
    VLAN_RM_PUT_4_BYTE (pMsg, &u4Offset, u4TimeOut);

    /* copying the packet into the RM steady state msg */
    VLAN_RM_PUT_N_BYTE (pMsg, pu1LinBuf, &u4Offset, u4PktLen);

    if (VlanRedSendMsgToRm (pMsg, u2BufSize) == OSIX_FAILURE)
    {
        GARP_TRC (GARP_RED_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  " steady state message sending is failed\n");
    }

    return GARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : GarpRedHRSendStdyStTailMsg                           */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
GarpRedHRSendStdyStTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = GARP_RM_OFFSET;
    UINT2               u2BufSize = 0;

    if (GARP_NODE_STATUS () != GARP_NODE_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  " Node is not active. "
                  "Steady state tail message is not sent to RM.\n");
        return GARP_SUCCESS;
    }
    if (GARP_HR_STATUS () == GARP_HR_STATUS_DISABLE)
    {
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  " Hitless restart is not enabled."
                  " Steady state tail message is not sent to RM.\n");
        return GARP_SUCCESS;
    }

    GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
              " sending steady state tail message to RM.\n");

    u2BufSize = GARP_RED_TYPE_FIELD_SIZE + GARP_RED_LEN_FIELD_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        GARP_TRC (GARP_RED_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Rm alloc failed\n");
        return (OSIX_FAILURE);
    }

    /* Form a steady state tail message.
     *
     *             <--------1 Byte----------><---2 Byte--->
     *   __________________________________________________
     *   |        |                          |             |
     *   | RM Hdr | GARP_HR_STDY_ST_PKT_TAIL | Msg Length  |
     *   |________|__________________________|_____________|
     *
     *  The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    VLAN_RM_PUT_1_BYTE (pMsg, &u4Offset, GARP_HR_STDY_ST_PKT_TAIL);
    VLAN_RM_PUT_2_BYTE (pMsg, &u4Offset, u2BufSize);

    if (VlanRedSendMsgToRm (pMsg, u2BufSize) == OSIX_FAILURE)
    {
        GARP_TRC (GARP_RED_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  " steady state tail message sending is failed\n");
    }
    return GARP_SUCCESS;
}

#endif /* GARP_WANTED */
