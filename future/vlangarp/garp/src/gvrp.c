
/************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gvrp.c,v 1.40 2014/07/16 12:48:46 siva Exp $
 *
 * DESCRIPTION           : This file contains GVRP related routines
 *
 ***********************************************************************/

/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : gvrp.c                                      */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : GVRP                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains GVRP routines.           */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#include "garpinc.h"

/****************************************************************************/
/*  Function Name   :GvrpHandlePropagateVlanInfo                            */
/*  Description     :depending on u1action we will add a static             */
/*                   entry or delete it                                     */
/*  Input(s)        :u1AttrType -The attribute type received                */
/*                   AddPortList - Ports to be added                        */
/*                   DelPortList - ports to be deleted                      */
/*                   u1Action - depending on u1Action we will add a static  */
/*                   Vlan entry or delete it                                */
/*  Output(s)       :  None                                                 */
/*  Global Variables Referred : None                                        */
/*  Global variables Modified : None                                        */
/*  Returns         : None                                                  */
/****************************************************************************/
void
GvrpHandlePropagateVlanInfo (tVlanId VlanId,
                             tLocalPortList AddPortList,
                             tLocalPortList DelPortList)
{
    UINT1              *pNullPortList = NULL;
    tGarpAttr           Attr;
    UINT2               u2GipId;

    if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
    {

        GARP_TRC_ARG1 (GVRP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                       GVRP_NAME, "GVRP is NOT enabled. Cannot Propagate "
                       "the Vlan Id %d.\n", VlanId);

        return;
    }

    GARP_TRC_ARG1 (GVRP_MOD_TRC, CONTROL_PLANE_TRC, GVRP_NAME,
                   "Received Propagation request for Vlan Id %d.\n", VlanId);

    /* The MST instance to which the VLAN is mapped is
     * the GIP Id of the VLAN.*/
    u2GipId =
        GarpL2IwfMiGetVlanInstMapping (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                       VlanId);

    Attr.u1AttrType = GVRP_GROUP_ATTR_TYPE;
    Attr.u1AttrLen = GVRP_VLAN_ID_LEN;

    VlanId = (tVlanId) OSIX_HTONS (VlanId);

    if (GarpCfaIsThisOpenflowVlan (VlanId) == OSIX_SUCCESS)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "Vlan is Openflow vlan.So It should not be propagated via GVRP\r\n");
        return;
    }

    MEMCPY (Attr.au1AttrVal, &VlanId, GVRP_VLAN_ID_LEN);
    pNullPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pNullPortList == NULL)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "GvrpHandlePropagateVlanInfo: "
                  "Error in allocating memory for pNullPortList\r\n");
        return;
    }
    MEMSET (pNullPortList, 0, sizeof (tLocalPortList));

    if (MEMCMP (AddPortList, pNullPortList, sizeof (tLocalPortList)) != 0)
    {

        GARP_TRC (GVRP_MOD_TRC, CONTROL_PLANE_TRC, GVRP_NAME,
                  "\tAdded Ports = ");

        GARP_PRINT_PORT_LIST (GVRP_MOD_TRC, AddPortList);

        GarpGipAppAttributeReqJoin (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId,
                                    &Attr, u2GipId, AddPortList);
    }

    if (MEMCMP (DelPortList, pNullPortList, sizeof (tLocalPortList)) != 0)
    {

        GARP_TRC (GVRP_MOD_TRC,
                  CONTROL_PLANE_TRC, GVRP_NAME, "\tDeleted Ports = ");

        GARP_PRINT_PORT_LIST (GVRP_MOD_TRC, DelPortList);

        GarpGipAppAttributeReqLeave (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId,
                                     &Attr, u2GipId, DelPortList);
    }
    UtilPlstReleaseLocalPortList (pNullPortList);
}

/****************************************************************************/
/*  Function Name   :GvrpHandleSetVlanForbiddenPorts                        */
/*  Description     :This function sets forbidden ports for a specific      */
/*                    VlanId                                                */
/*  Input(s)        :VlanId - VlanId for which Forbidden ports to be updated*/
/*                   AddPortList - Ports to be added                        */
/*                   DelPortList - ports to be deleted                      */
/*  Output(s)       :  None                                                 */
/*  Global Variables Referred : None                                        */
/*  Global variables Modified : None                                        */
/*  Returns         : None                                                  */
/****************************************************************************/
void
GvrpHandleSetVlanForbiddenPorts (tVlanId VlanId,
                                 tLocalPortList AddPortList,
                                 tLocalPortList DelPortList)
{
    tGarpAttr           Attr;
    UINT2               u2GipId;

    if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
    {

        GARP_TRC_ARG1 (GVRP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                       GVRP_NAME, "GVRP is NOT enabled. Cannot Set Forbidden "
                       "Ports for the Vlan Id %d.\n", VlanId);

        return;
    }

    GARP_TRC_ARG1 (GVRP_MOD_TRC, CONTROL_PLANE_TRC, GVRP_NAME,
                   "Received Forbidden Ports Set Request for Vlan Id %d.\n",
                   VlanId);

    GARP_TRC (GVRP_MOD_TRC, CONTROL_PLANE_TRC, GVRP_NAME, "\tAdded Ports = ");
    GARP_PRINT_PORT_LIST (GVRP_MOD_TRC, AddPortList);

    GARP_TRC (GVRP_MOD_TRC, CONTROL_PLANE_TRC, GVRP_NAME, "\tDeleted Ports = ");
    GARP_PRINT_PORT_LIST (GVRP_MOD_TRC, DelPortList);

    /* The MST instance to which the VLAN is mapped is
     * the GIP Id of the VLAN.*/

    u2GipId =
        GarpL2IwfMiGetVlanInstMapping (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                       VlanId);

    Attr.u1AttrType = GVRP_GROUP_ATTR_TYPE;
    Attr.u1AttrLen = GVRP_VLAN_ID_LEN;

    VlanId = (tVlanId) OSIX_HTONS (VlanId);

    MEMCPY (Attr.au1AttrVal, &VlanId, GVRP_VLAN_ID_LEN);

    GarpGipAppSetForbiddenPorts (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId, &Attr,
                                 u2GipId, AddPortList, DelPortList);
}

/***************************************************************/
/*  Function Name   :GvrpInit ()                                      */
/*  Description     :This function enables Gvrp application.   */
/*  Input(s)        :void                                      */
/*  Output(s)       :INT4                                      */
/*  Global Variables Referred :                                */
/*  Global variables Modified :                                */
/*  Exceptions or Operating System Error Handling :            */
/*  Use of Recursion : None                                    */
/*  Returns         : GVRP_SUCCESS, if gvrp is enabled.        */
/*                    GVRP_FAILURE otherwise                   */
/***************************************************************/
INT4
GvrpInit (void)
{
    UINT2               u2Port;
    UINT1               u1PortType = 0;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        GARP_TRC (GVRP_MOD_TRC, INIT_SHUT_TRC, GVRP_NAME,
                  "GVRP initialization failed.  GARP is not enabled \n");

        return GVRP_FAILURE;
    }

    if (GVRP_ADMIN_STATUS () == GVRP_ENABLED)
    {
        GARP_TRC (GVRP_MOD_TRC, INIT_SHUT_TRC, GVRP_NAME,
                  "GVRP is already enabled \n");
        return GVRP_SUCCESS;
    }

    for (u2Port = 1; u2Port <= GARP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {

        if ((GARP_IS_1AD_BRIDGE () == GARP_TRUE) &&
            (GARP_PORT_ENTRY (u2Port) != NULL))
        {
            /* In case of 1ad bridge, Gvrp cannot be enabled on CEP/
             * CNP(Port-Based)/PCEP/PCNP. So check that and initialize. */
            if (GvrpPbIsPortValidForGvrpEnable (u2Port) == GVRP_TRUE)
            {
                GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status =
                    GVRP_ENABLED;
            }
            else
            {
                GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status =
                    GVRP_DISABLED;
            }
        }
        else
        {
            GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status =
                GVRP_ENABLED;
        }

        if (GARP_PORT_ENTRY (u2Port) != NULL)
        {
            GarpL2IwfGetVlanPortType ((UINT2) GARP_GET_IFINDEX (u2Port),
                                      &u1PortType);

            /* In case of Access Port, GVRP should not be enabled on the port */
            if (u1PortType == VLAN_ACCESS_PORT)
            {
                GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status =
                    GVRP_DISABLED;
            }
        }

        /* As setting the protocol enabled status in L2Iwf is done in Garp
         * Handle Create port, no need to do that here. */
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].
            u1RestrictedRegControl = GVRP_DISABLED;
    }

    /* Make the admin status as disabled. For non-default contexts, garp will
     * not be started and gvrp/gmrp will not be enabled on context creation.
     * Gvrp/ Gmrp will be enabled only when they are enabled explcitly for
     * non-default contexts. If admin status is set to enabled, then the snmp
     * routine for gvrp/gmrp enable will not enable the application. So set
     * the admin status as disabled. */
    GVRP_ADMIN_STATUS () = GVRP_DISABLED;

    gau1GvrpStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] = GVRP_DISABLED;

    return GVRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   :GvrpEnable                                       */
/*  Description     :This function enables Gvrp application.   */
/*  Input(s)        :void                                      */
/*  Output(s)       :INT4                                      */
/*  Global Variables Referred :gpGarpContextInfo->u1GvrpStatus                   */
/*  Global variables Modified :gpGarpContextInfo->u1GvrpStatus                   */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : GVRP_SUCCESS, if gvrp is enabled.        */
/*                    GVRP_FAILURE otherwise                   */
/***************************************************************/
INT4
GvrpEnable (void)
{
    INT4                i4RetVal;
    UINT2               u2Port;

    tGarpAppnFn         AppnFn =
        { GvrpJoinIndFn, GvrpLeaveIndFn, GvrpTypeValidateFn,
        GvrpAttrRegValidateFn, GvrpWildCardAttrRegValidateFn
    };

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        return GVRP_FAILURE;
    }

    if (GVRP_IS_GVRP_ENABLED () == GVRP_TRUE)
    {

        return GVRP_SUCCESS;
    }

    i4RetVal =
        GarpAppEnrol (GARP_CTXT_GVRP_ADDR (), AppnFn,
                      &GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId);

    if (i4RetVal == GARP_FAILURE)
    {

        GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId = GARP_INVALID_APP_ID;
        return GVRP_FAILURE;
    }

    GarpUpdateGarpAppStatusOnAllPorts (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                       GVRP_ENABLED, GVRP_APP_ID);
    for (u2Port = 1; u2Port <= GARP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        if (GARP_IS_PORT_KNOWN (u2Port) == GARP_TRUE)
        {
            /* GVRP will not run on customer edge ports, 
             * proprietary CNP/CEP. So don't add
             * those ports to GVRP application port table. */
            /*
             * Create Application specific port entries in GARP iff GARP
             * is aware of that port.
             */
            i4RetVal =
                GarpAppAddPort (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId, u2Port);

            if (i4RetVal == GARP_FAILURE)
            {
                GARP_TRC (GVRP_MOD_TRC, ALL_FAILURE_TRC, GVRP_NAME,
                          "DeEnrolling GVRP with GARP \n");
                GarpAppDeEnrol (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId);

                GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId = GARP_INVALID_APP_ID;

                return GVRP_FAILURE;
            }

            if (GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status ==
                GVRP_ENABLED)
            {
                /* Enable GVRP on this Port */
                i4RetVal =
                    GarpAppEnablePort (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId,
                                       u2Port);

                if (i4RetVal == GARP_FAILURE)
                {
                    GARP_TRC (GVRP_MOD_TRC, ALL_FAILURE_TRC, GVRP_NAME,
                              "DeEnrolling GVRP with GARP \n");
                    GarpAppDeEnrol (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId);

                    GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId = GARP_INVALID_APP_ID;

                    return GVRP_FAILURE;
                }
            }

            if (GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].
                u1RestrictedRegControl == GVRP_ENABLED)
            {
                i4RetVal = GarpAppEnablePortRestrictedRegControl
                    (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId, u2Port);

                if (i4RetVal == GARP_FAILURE)
                {
                    GARP_TRC_ARG1 (GVRP_MOD_TRC, ALL_FAILURE_TRC, GVRP_NAME,
                                   "Restricted VLAN Registration on Port %d failed\n",
                                   GARP_GET_IFINDEX (u2Port));
                    return GVRP_FAILURE;
                }
            }
        }
    }

    gau1GvrpStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] = GVRP_ENABLED;

    GARP_TRC (GARP_MOD_TRC, INIT_SHUT_TRC, GARP_NAME,
              "GVRP Registered with GARP successfully \n");

    /* 
     * Static Vlan information might be present in
     * Vlan module, before GVRP is enabled. So need to
     * indicate Vlan to propagate the Vlan information.
     */
    GarpVlanGvrpEnableInd (GARP_CURR_CONTEXT_PTR ()->u4ContextId);
    return GVRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   :GvrpDisable                                      */
/*  Description     :This function disable Gvrp application.   */
/*  Input(s)        :void                                      */
/*  Output(s)       :INT4                                      */
/*  Global Variables Referred :GARP_CURR_CONTEXT_PTR()->u1GvrpStatus */
/*  Global variables Modified :gpGarpContextInfo->u1GvrpStatus */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : GVRP_SUCCESS, if gvrp is disabled        */
/*                    Else GVRP_FAILURE                        */
/***************************************************************/
INT4
GvrpDisable (void)
{

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        return GVRP_SUCCESS;
    }

    if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
    {

        return GVRP_SUCCESS;
    }

    GarpUpdateGarpAppStatusOnAllPorts (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                       GVRP_DISABLED, GVRP_APP_ID);
    GarpVlanDeleteAllDynamicVlanInfo (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                      VLAN_INVALID_PORT);

    GarpAppDeEnrol (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId);

    GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId = GARP_INVALID_APP_ID;
    gau1GvrpStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] = GVRP_DISABLED;

    /* Should be invoked only after invoking VlanDeleteAllDynamicVlanInfo
     * so that the Delete ALL update message is sent */
    GarpVlanGvrpDisableInd (GARP_CURR_CONTEXT_PTR ()->u4ContextId);
    return GVRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GvrpAddPort                              */
/*  Description     : A port is to be added to gvrp Application*/
/*  Input(s)        : u2port - port number of port to be added */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : GARP_CURR_CONTEXT_PTR()->u1GvrpStatus                  */
/*  Global variables Modified : gpGarpContextInfo->u1GvrpStatus                  */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
INT4
GvrpAddPort (UINT2 u2Port)
{
    INT4                i4RetVal;

    if ((u2Port == 0) || (u2Port > GARP_MAX_PORTS_PER_CONTEXT))
    {

        return GVRP_FAILURE;
    }

    if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
    {

        return GVRP_SUCCESS;
    }

    /* Port would have been already created in GARP */
    i4RetVal = GarpAppAddPort (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId, u2Port);

    if (i4RetVal == GARP_FAILURE)
    {

        return GVRP_FAILURE;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status ==
        GVRP_ENABLED)
    {
        /* GVRP is enabled on this port */
        i4RetVal =
            GarpAppEnablePort (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId, u2Port);
        if (i4RetVal == GARP_FAILURE)
        {
            GARP_TRC_ARG1 (GVRP_MOD_TRC, ALL_FAILURE_TRC, GVRP_NAME,
                           "Enabling GVRP on Port %d failed\n",
                           GARP_GET_IFINDEX (u2Port));
            return GVRP_FAILURE;
        }
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].
        u1RestrictedRegControl == GVRP_ENABLED)
    {

        i4RetVal =
            GarpAppEnablePortRestrictedRegControl (GARP_CURR_CONTEXT_PTR ()->
                                                   u1GvrpAppId, u2Port);
        if (i4RetVal == GARP_FAILURE)
        {
            GARP_TRC_ARG1 (GVRP_MOD_TRC, ALL_FAILURE_TRC, GVRP_NAME,
                           "Restricted VLAN Registration on Port %d failed\n",
                           GARP_GET_IFINDEX (u2Port));
            return GVRP_FAILURE;
        }

    }

    return GVRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GvrpDelPort                              */
/*  Description     : This function is for deleting port from a*/
/*                    given Gvrp application                   */
/*  Input(s)        :                                          */
/*                    u2Port - Port number to be deleted from  */
/*                    the given gvrp application               */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         :                                          */
/***************************************************************/
INT4
GvrpDelPort (UINT2 u2Port)
{
    if ((u2Port == 0) || (u2Port > GARP_MAX_PORTS_PER_CONTEXT))
    {

        return GVRP_FAILURE;
    }

    if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
    {
        return GVRP_SUCCESS;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status ==
        GVRP_DISABLED)
    {

        return GVRP_SUCCESS;
    }

    GarpVlanDeleteAllDynamicVlanInfo (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                      u2Port);

    GarpAppDelPort (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId, u2Port);

    return GVRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GvrpEnablePort                           */
/*  Description     : A port is to be added to gvrp Application*/
/*  Input(s)        : u2port - port number of port to be added */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : gpGarpContextInfo->u1GvrpStatus                  */
/*  Global variables Modified : gpGarpContextInfo->u1GvrpStatus                  */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
INT4
GvrpEnablePort (UINT2 u2Port)
{
    INT4                i4RetVal;

    if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
    {
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status =
            GVRP_ENABLED;

        /* Update VLAN port table with port Gvrp status
         * because port type should not be set as access port
         * on Gvrp enabled ports.*/
        GarpL2IwfSetProtocolEnabledStatusOnPort
            (GARP_CURR_CONTEXT_ID (), u2Port, L2_PROTO_GVRP, OSIX_ENABLED);
        return GVRP_SUCCESS;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status ==
        GVRP_ENABLED)
    {
        return GVRP_SUCCESS;
    }

    if (GARP_IS_PORT_KNOWN (u2Port) == GARP_FALSE)
    {
        /* GARP is not aware of this port */
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status =
            GVRP_ENABLED;

        /* GARP is not aware of this port */
        /* Update VLAN port table with port Gvrp status
         * because port type should not be set as access port
         * on Gvrp enabled ports.*/
        GarpL2IwfSetProtocolEnabledStatusOnPort
            (GARP_CURR_CONTEXT_ID (), u2Port, L2_PROTO_GVRP, OSIX_ENABLED);

        return GVRP_SUCCESS;
    }

    i4RetVal =
        GarpAppEnablePort (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId, u2Port);

    if (i4RetVal == GARP_FAILURE)
    {
        GARP_TRC_ARG1 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                       "Enabling GVRP on Port %d failed\n",
                       GARP_GET_IFINDEX (u2Port));
        return GVRP_FAILURE;
    }

    GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status = GVRP_ENABLED;
    GarpL2IwfSetProtocolEnabledStatusOnPort
        (GARP_CURR_CONTEXT_ID (), u2Port, L2_PROTO_GVRP, OSIX_ENABLED);
    return GVRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GvrpDisablePort                          */
/*  Description     : This function is for disabling port from */
/*                    a given Gvrp application                 */
/*  Input(s)        :                                          */
/*                    u2Port - Port number to be deleted from  */
/*                    the given gvrp application               */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         :                                          */
/***************************************************************/
INT4
GvrpDisablePort (UINT2 u2Port)
{
    if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
    {
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status =
            GVRP_DISABLED;

        /* Update VLAN port table with port Gvrp status
         * because port type should not be set as access port
         * on Gvrp enabled ports.*/
        GarpL2IwfSetProtocolEnabledStatusOnPort
            (GARP_CURR_CONTEXT_ID (), u2Port, L2_PROTO_GVRP, OSIX_DISABLED);

        return GVRP_SUCCESS;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status ==
        GVRP_DISABLED)
    {
        return GVRP_SUCCESS;
    }

    GarpVlanDeleteAllDynamicVlanInfo (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                      u2Port);

    GarpAppDisablePort (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId, u2Port);

    GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status = GVRP_DISABLED;

    /* Update VLAN port table with port Gvrp status
     * because port type should not be set as access port
     * on Gvrp enabled ports.*/
    GarpL2IwfSetProtocolEnabledStatusOnPort
        (GARP_CURR_CONTEXT_ID (), u2Port, L2_PROTO_GVRP, OSIX_DISABLED);

    return GVRP_SUCCESS;
}

/*********************************************************************/
/*  Function Name   : GvrpGetNumRegFailed                            */
/*  Description     :This function returns the number of failed Gvrp */
/*                   registrations on a given port.                           */
/*  Input(s)        : u2Port - Port in which the number of failed    */
/*                    registrations is received                      */
/*                    pu4NumRegFailed - pointer to the value which   */
/*                    contain the value of failed registrations      */
/*  Output(s)       : Pointer to the value which isthe number of     */
/*                   failed registrations or Pointer to NULL on      */
/*                   failure                                         */
/*  Global Variables Referred : None                                 */
/*  Global variables Modified : None                                 */
/*  Exceptions or Operating System Error Handling :None              */
/*  Use of Recursion : None                                          */
/*  Returns         : GVRP_SUCCESS if pu4NumRegFailed is not NULL.   */
/*                    Otherwise pointer to NULL                      */
/*********************************************************************/
INT4
GvrpGetNumRegFailed (UINT2 u2Port, UINT4 *pu4NumRegFailed)
{
    if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
    {

        return GVRP_FAILURE;
    }

    if (GarpGetNumRegFailed
        (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId, u2Port,
         pu4NumRegFailed) == GARP_FAILURE)
    {

        return GVRP_FAILURE;
    }

    return GVRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GvrpGetLastPduOrigin                     */
/*  Description     :This function returns the source address  */
/*                   of the  last packet recieved for a GVRP   */
/*                   application                               */
/*  Input(s)        :                                          */
/*                   u2Port - Port number of the incoming port */
/*                   pMacAddr - Pointer to the output mac      */
/*                   Address of this function                  */
/*  Output(s)       : pMacAddr is initialised with the result  */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : GVRP_SUCCESS on succesful updation of    */
/*                   lastpdu else GVRP_FAILURE*/
/***************************************************************/
INT4
GvrpGetLastPduOrigin (UINT2 u2Port, tMacAddr MacAddr)
{
    if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
    {

        return GVRP_FAILURE;
    }

    if (GarpGetLastPduOrigin
        (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId, u2Port,
         MacAddr) == GARP_FAILURE)
    {

        return GVRP_FAILURE;
    }

    return GVRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GvrpJoinIndFn                            */
/*  Description     :Registered function for JoinInd Event from*/
/*                        upper layer                          */
/*  Input(s)        :u1AttrType - Attribute type               */
/*                   u2Port - Port number of the incoming port */
/*                   u1AttrLen - Length of the attribute value * 
 *                   pu1AttrVal - Pointer to attribute value   */
/*                   u2Port - Port number of the incoming port */
/*                   u2GipId - GipId                           */
/*  Output(s)       : pMacAddr is initialised with the result  */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/***************************************************************/
INT4
GvrpJoinIndFn (UINT1 u1AttrType,
               UINT1 u1AttrLen, UINT1 *pu1AttrVal, UINT2 u2Port, UINT2 u2GipId)
{
    tVlanId             VlanId;
    INT4                i4RetVal = VLAN_FAILURE;
    UNUSED_PARAM (u2GipId);

    if (u1AttrType == GVRP_GROUP_ATTR_TYPE)
    {
        if (u1AttrLen == GVRP_VLAN_ID_LEN)
        {
            MEMCPY (&VlanId, pu1AttrVal, GVRP_VLAN_ID_LEN);

            VlanId = (tVlanId) OSIX_NTOHS (VlanId);

            if (GARP_CURR_CONTEXT_PTR () != NULL)
            {
                GARP_TRC_ARG2 (GVRP_MOD_TRC, CONTROL_PLANE_TRC, GVRP_NAME,
                               "Join Ind received on Port %d for VLAN %d\n",
                               GARP_GET_IFINDEX (u2Port), VlanId);

                if (GarpCfaIsThisOpenflowVlan (VlanId) == OSIX_SUCCESS)
                {
                    GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                              "Vlan is Openflow vlan.So we do not learn via GVRP\r\n");
                    return GARP_FAILURE;
                }

                i4RetVal = GarpVlanUpdateDynamicVlanInfo
                    (GARP_CURR_CONTEXT_PTR ()->u4ContextId, VlanId,
                     u2Port, VLAN_ADD);

                if (i4RetVal == VLAN_SUCCESS)
                {
                    GarpHandleGipUpdateGipPorts (u2Port, VlanId, GARP_ADD);
                }
                else
                {
                    GARP_TRC_ARG2 (GVRP_MOD_TRC, ALL_FAILURE_TRC, GVRP_NAME,
                                   "Failed to update dynamic vlan information"
                                   "for vlan %d on Port %d\n", VlanId,
                                   GARP_GET_IFINDEX (u2Port));

                    return GARP_FAILURE;
                }
            }
        }
    }
    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GvrpLeaveIndFn                           */
/*  Description     :Registered function for LeaveInd Event    */
/*                   from upper layer                          */
/*  Input(s)        :u1AttrType - Attribute type               */
/*                   u2Port - Port number of the incoming port */
/*                   u1AttrLen - Length of the attribute value * 
 *                   pu1AttrVal - Pointer to attribute value   */
/*                   u2Port - Port number of the incoming port */
/*                   u2GipId - GipId                           */
/*  Output(s)       : pMacAddr is initialised with the result  */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/***************************************************************/
INT4
GvrpLeaveIndFn (UINT1 u1AttrType,
                UINT1 u1AttrLen, UINT1 *pu1AttrVal, UINT2 u2Port, UINT2 u2GipId)
{
    tVlanId             VlanId;
    INT4                i4RetVal = VLAN_FAILURE;
    UNUSED_PARAM (u2GipId);

    if (u1AttrType == GVRP_GROUP_ATTR_TYPE)
    {

        if (u1AttrLen == GVRP_VLAN_ID_LEN)
        {

            MEMCPY (&VlanId, pu1AttrVal, GVRP_VLAN_ID_LEN);

            VlanId = (tVlanId) OSIX_NTOHS (VlanId);

            GARP_TRC_ARG2 (GVRP_MOD_TRC, CONTROL_PLANE_TRC, GVRP_NAME,
                           "Leave Ind received on Port %d for VLAN %d\n",
                           GARP_GET_IFINDEX (u2Port), VlanId);

            GarpHandleGipUpdateGipPorts (u2Port, VlanId, GARP_DELETE);

            i4RetVal =
                GarpVlanUpdateDynamicVlanInfo (GARP_CURR_CONTEXT_PTR ()->
                                               u4ContextId, VlanId, u2Port,
                                               VLAN_DELETE);

            if (i4RetVal == VLAN_FAILURE)
            {
                GARP_TRC_ARG2 (GVRP_MOD_TRC, ALL_FAILURE_TRC, GVRP_NAME,
                               "Failed to update Dynamic Vlan information"
                               "for Vlan %d on Port %d\n", VlanId,
                               GARP_GET_IFINDEX (u2Port));

                return GARP_FAILURE;

            }
        }
    }
    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GvrpTypeValidateFn                       */
/*  Description     : This function is called by GARP module   */
/*                    to check whether the attribute type is a */
/*                    valid GVRP attribute type.               */
/*  Input(s)        :u1AttrType - Attribute type               */
/*  Output(s)       : None.                                    */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_TRUE / GARP_FALSE                   */
/***************************************************************/
INT4
GvrpTypeValidateFn (UINT1 u1AttrType)
{

    if (u1AttrType == GVRP_GROUP_ATTR_TYPE)
    {

        return GARP_TRUE;
    }

    return GARP_FALSE;
}

/***************************************************************/
/*  Function Name   : GvrpEnablePortRestrictedRegControl        */
/*  Description     : The port becomes Restricted VLAN enabled */
/*  Input(s)        : u2port - port number of port to be added */
/*                                                             */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : gpGarpContextInfo->u1GvrpStatus                  */
/*  Global variables Modified : gpGarpContextInfo->u1GvrpStatus                  */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
INT4
GvrpEnablePortRestrictedRegControl (UINT2 u2Port)
{
    INT4                i4RetVal;

    if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
    {
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].
            u1RestrictedRegControl = GVRP_ENABLED;
        return GVRP_SUCCESS;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].
        u1RestrictedRegControl == GVRP_ENABLED)
    {
        return GVRP_SUCCESS;
    }

    if (GARP_IS_PORT_KNOWN (u2Port) == GARP_FALSE)
    {
        /* GARP is not aware of this port */
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].
            u1RestrictedRegControl = GVRP_ENABLED;
        return GVRP_SUCCESS;
    }

    i4RetVal =
        GarpAppEnablePortRestrictedRegControl (GARP_CURR_CONTEXT_PTR ()->
                                               u1GvrpAppId, u2Port);

    if (i4RetVal == GARP_FAILURE)
    {
        GARP_TRC_ARG1 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                       "Enabling Restricted VLAN Registration Control on Port %d failed\n",
                       GARP_GET_IFINDEX (u2Port));
        return GVRP_FAILURE;
    }

    GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1RestrictedRegControl =
        GVRP_ENABLED;

    return GVRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GvrpDisablePortRestrictedRegControl       */
/*  Description     : This function is for disabling Restricted*/
/*                    VLAN Registration for a port             */
/*  Input(s)        :                                          */
/*                    u2Port - Port number to be deleted from  */
/*                    the given gvrp application               */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         :                                          */
/***************************************************************/
INT4
GvrpDisablePortRestrictedRegControl (UINT2 u2Port)
{
    INT4                i4RetVal;

    if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
    {
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].
            u1RestrictedRegControl = GVRP_DISABLED;
        return GVRP_SUCCESS;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].
        u1RestrictedRegControl == GVRP_DISABLED)
    {
        return GVRP_SUCCESS;
    }

    if (GARP_IS_PORT_KNOWN (u2Port) == GARP_FALSE)
    {
        /* GARP is not aware of this port */
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].
            u1RestrictedRegControl = GVRP_DISABLED;
        return GVRP_SUCCESS;
    }

    i4RetVal =
        GarpAppDisablePortRestrictedRegControl (GARP_CURR_CONTEXT_PTR ()->
                                                u1GvrpAppId, u2Port);

    if (i4RetVal == GARP_FAILURE)
    {
        GARP_TRC_ARG1 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                       "Disabling Restricted VLAN Registration Control on Port %d failed\n",
                       GARP_GET_IFINDEX (u2Port));
        return GVRP_FAILURE;
    }

    GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1RestrictedRegControl = GVRP_DISABLED;    /* IEEE 802.1U */

    return GVRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GvrpAttrRegValidateFn                       */
/*  Description     : This function calls a function in VLAN   */
/*                    module and checks if the attribute is    */
/*                    already registered statically and if     */
/*                    registration is NORMAL for the attribute */
/*                    on u2Port                                */
/*                                                             */
/*  Input(s)        : Attr - Structure containing attribute    */
/*                    type,value,length                        */
/*                                                             */
/*                    u2Port - Port Number on which            */
/*                    Registration NORMAL for attribute        */
/*                    contained in Attr to be checked          */
/*                                                             */
/*                    u2GipId - GIP Id                         */
/*                                                             */
/*  Output(s)       : None                                     */
/*                                                             */
/*  Global Variables Referred : None                           */
/*                                                             */
/*  Global variables Modified : None                           */
/*                                                             */
/*  Exceptions or Operating System Error Handling : None       */
/*                                                             */
/*  Use of Recursion : None                                    */
/*                                                             */
/*  Returns         : GVRP_TRUE/GVRP_FALSE                     */
/***************************************************************/

INT4
GvrpAttrRegValidateFn (tGarpAttr Attr, UINT2 u2Port, UINT2 u2GipId)
{
    UINT2               VlanId;
    INT4                i4RetVal = GVRP_FALSE;
    UNUSED_PARAM (u2GipId);

    if (Attr.u1AttrType == GVRP_GROUP_ATTR_TYPE)
    {

        if (Attr.u1AttrLen == GVRP_VLAN_ID_LEN)
        {

            MEMCPY (&VlanId, Attr.au1AttrVal, GVRP_VLAN_ID_LEN);

            VlanId = OSIX_NTOHS (VlanId);

            i4RetVal =
                GarpVlanIsVlanStaticAndRegNormal (GARP_CURR_CONTEXT_PTR ()->
                                                  u4ContextId, VlanId, u2Port);
            if (i4RetVal == VLAN_TRUE)
            {
                i4RetVal = GVRP_TRUE;
            }
            else
            {
                i4RetVal = GVRP_FALSE;
            }

        }
    }
    return i4RetVal;
}

/***************************************************************/
/*  Function Name   : GvrpIsGvrpEnabled                        */
/*  Description     : Returns the GVRP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Global Variables Referred : gpGarpContextInfo->u1GvrpStatus                  */
/*  Global variables Modified :                                */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GVRP_ENABLED /GVRP_DISABLED              */
/***************************************************************/

INT4
GvrpIsGvrpEnabled ()
{
    return ((gau1GvrpStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] ==
             GVRP_ENABLED) ? GVRP_TRUE : GVRP_FALSE);
}

/***************************************************************/
/*  Function Name   : GvrpWildCardAttrRegValidateFn            */
/*  Description     : This function calls a function in VLAN   */
/*                    module and checks if the attribute can   */
/*                    be registered                            */
/*                                                             */
/*  Input(s)        : Attr - Structure containing attribute    */
/*                    type,value,length                        */
/*                                                             */
/*                    u2Port - Port Number on which            */
/*                    Registration NORMAL for attribute        */
/*                    contained in Attr to be checked          */
/*                                                             */
/*                    u2GipId - GIP Id                         */
/*                                                             */
/*  Output(s)       : None                                     */
/*                                                             */
/*  Global Variables Referred : None                           */
/*                                                             */
/*  Global variables Modified : None                           */
/*                                                             */
/*  Exceptions or Operating System Error Handling : None       */
/*                                                             */
/*  Use of Recursion : None                                    */
/*                                                             */
/*  Returns         : GVRP_TRUE/GVRP_FALSE                     */
/***************************************************************/

INT4
GvrpWildCardAttrRegValidateFn (tGarpAttr Attr, UINT2 u2Port, UINT2 u2GipId)
{
    UNUSED_PARAM (Attr);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u2GipId);

    return OSIX_TRUE;
}
