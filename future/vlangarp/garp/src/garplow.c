
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2001-2002                                       */
/* $Id: garplow.c,v 1.73 2015/07/13 04:42:41 siva Exp $              */
/*  FILE NAME             : garplow.c                                   */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*  SUBSYSTEM NAME        : SNMP                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains SNMP low level routines  */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#include "garpinc.h"
#include "fsmsvlcli.h"
#include "fsmsbecli.h"
#include "fsmpvlcli.h"

/****************************************************************************
 Function    :  nmhSetDot1qGvrpStatus
 Input       :  The Indices

                The Object 
                setValDot1qGvrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qGvrpStatus (INT4 i4SetValDot1qGvrpStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (GARP_NODE_STATUS () == GARP_NODE_IDLE)
    {
        GVRP_ADMIN_STATUS () = (UINT1) i4SetValDot1qGvrpStatus;

        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValDot1qGvrpStatus)
    {

        case GVRP_ENABLED:

            if (GVRP_IS_GVRP_ENABLED () == GVRP_FALSE)
            {

                if (GvrpEnable () == GVRP_SUCCESS)
                {
                    GVRP_ADMIN_STATUS () = (UINT1) i4SetValDot1qGvrpStatus;

                    i1RetVal = SNMP_SUCCESS;
                }
            }
            else
            {
                i1RetVal = SNMP_SUCCESS;
            }

            break;

        case GVRP_DISABLED:

            if (GvrpDisable () == GVRP_SUCCESS)
            {
                GVRP_ADMIN_STATUS () = (UINT1) i4SetValDot1qGvrpStatus;
                i1RetVal = SNMP_SUCCESS;
            }

            break;

        default:
            break;
    }

    /* Sending Trigger to MSR */

    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qGvrpStatus, u4SeqNum,
                          FALSE, GarpLock, GarpUnLock, 1, i1RetVal);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", GARP_CURR_CONTEXT_ID (),
                      i4SetValDot1qGvrpStatus));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetDot1qPortGvrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                setValDot1qPortGvrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qPortGvrpStatus (INT4 i4Dot1dBasePort,
                           INT4 i4SetValDot1qPortGvrpStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[i4Dot1dBasePort].u1Status ==
        (UINT1) i4SetValDot1qPortGvrpStatus)
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValDot1qPortGvrpStatus)
    {

        case GVRP_ENABLED:

            /* If the port is a CEP/CNP(Port-Based)/PCEP/PCNP then GVRP will be always disabled
             * on that port. */
            if (GvrpPbIsPortValidForGvrpEnable ((UINT2) i4Dot1dBasePort)
                == GVRP_FALSE)
            {
                /* Sending Trigger to MSR */

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qPortGvrpStatus,
                                      u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                                      SNMP_FAILURE);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  GARP_GET_IFINDEX (i4Dot1dBasePort),
                                  i4SetValDot1qPortGvrpStatus));
                if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                return SNMP_FAILURE;
            }

            if (GvrpEnablePort ((UINT2) i4Dot1dBasePort) == GVRP_SUCCESS)
            {
                /* Sending Trigger to MSR */

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qPortGvrpStatus,
                                      u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  GARP_GET_IFINDEX (i4Dot1dBasePort),
                                  i4SetValDot1qPortGvrpStatus));
                if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;
            }
            break;

        case GVRP_DISABLED:

            if (GvrpDisablePort ((UINT2) i4Dot1dBasePort) == GVRP_SUCCESS)
            {
                /* Sending Trigger to MSR */

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qPortGvrpStatus,
                                      u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  GARP_GET_IFINDEX (i4Dot1dBasePort),
                                  i4SetValDot1qPortGvrpStatus));
                if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;
            }
            break;

        default:
            break;
    }

    /* Sending Trigger to MSR */

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qPortGvrpStatus, u4SeqNum,
                          FALSE, GarpLock, GarpUnLock, 1, SNMP_FAILURE);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      GARP_GET_IFINDEX (i4Dot1dBasePort),
                      i4SetValDot1qPortGvrpStatus));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1dGmrpStatus
 Input       :  The Indices

                The Object 
                setValDot1dGmrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dGmrpStatus (INT4 i4SetValDot1dGmrpStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (GARP_NODE_STATUS () == GARP_NODE_IDLE)
    {
        GMRP_ADMIN_STATUS () = (UINT1) i4SetValDot1dGmrpStatus;

        return SNMP_SUCCESS;
    }

    switch (i4SetValDot1dGmrpStatus)
    {

        case GMRP_ENABLED:

            /* Enable GMRP status only when the IGS is disabled */
            if (SNOOP_ENABLED ==
                GarpSnoopIsIgmpSnoopingEnabled (GARP_CURR_CONTEXT_ID ()))
            {

                return SNMP_FAILURE;
            }

            /* Enable GMRP status only when the MLDS is disabled */
            if (SNOOP_ENABLED ==
                GarpSnoopIsMldSnoopingEnabled (GARP_CURR_CONTEXT_ID ()))
            {

                return SNMP_FAILURE;
            }

            if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
            {
                /* Gmrp cannot be enabled on provider core and edge
                 * bridges. */
                return SNMP_FAILURE;
            }

            if (GmrpIsGmrpEnabled () == GMRP_FALSE)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);
                if (GmrpEnable () == GMRP_SUCCESS)
                {
                    GMRP_ADMIN_STATUS () = (UINT1) i4SetValDot1dGmrpStatus;

                    i1RetVal = SNMP_SUCCESS;
                }
            }
            /* GMRP is already enabled. So just return success */
            else
            {
                return SNMP_SUCCESS;
            }
            break;

        case GMRP_DISABLED:

            RM_GET_SEQ_NUM (&u4SeqNum);
            if (GmrpDisable () == GMRP_SUCCESS)
            {
                GMRP_ADMIN_STATUS () = (UINT1) i4SetValDot1dGmrpStatus;
                i1RetVal = SNMP_SUCCESS;
            }

            break;

        default:
            break;
    }

    /* Sending Trigger to MSR */

    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dGmrpStatus, u4SeqNum,
                          FALSE, GarpLock, GarpUnLock, 1, i1RetVal);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", GARP_CURR_CONTEXT_ID (),
                      i4SetValDot1dGmrpStatus));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetDot1dPortGarpJoinTime
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                setValDot1dPortGarpJoinTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortGarpJoinTime (INT4 i4Dot1dBasePort,
                             INT4 i4SetValDot1dPortGarpJoinTime)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1dBasePort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    pGlobalPortEntry->u4JoinTime = (UINT4) i4SetValDot1dPortGarpJoinTime;

    /* Sending Trigger to MSR */

    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dPortGarpJoinTime, u4SeqNum,
                          FALSE, GarpLock, GarpUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      GARP_GET_IFINDEX (i4Dot1dBasePort),
                      i4SetValDot1dPortGarpJoinTime));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dPortGarpLeaveTime
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                setValDot1dPortGarpLeaveTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortGarpLeaveTime (INT4 i4Dot1dBasePort,
                              INT4 i4SetValDot1dPortGarpLeaveTime)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1dBasePort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    pGlobalPortEntry->u4LeaveTime = (UINT4) i4SetValDot1dPortGarpLeaveTime;

    /* Sending Trigger to MSR */

    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dPortGarpLeaveTime, u4SeqNum,
                          FALSE, GarpLock, GarpUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      GARP_GET_IFINDEX (i4Dot1dBasePort),
                      i4SetValDot1dPortGarpLeaveTime));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dPortGarpLeaveAllTime
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                setValDot1dPortGarpLeaveAllTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortGarpLeaveAllTime (INT4 i4Dot1dBasePort,
                                 INT4 i4SetValDot1dPortGarpLeaveAllTime)
{

    tGarpGlobalPortEntry *pGlobalPortEntry;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1dBasePort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    pGlobalPortEntry->u4LeaveAllTime =
        (UINT4) i4SetValDot1dPortGarpLeaveAllTime;

    /* Sending Trigger to MSR */

    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dPortGarpLeaveAllTime, u4SeqNum,
                          FALSE, GarpLock, GarpUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      GARP_GET_IFINDEX (i4Dot1dBasePort),
                      i4SetValDot1dPortGarpLeaveAllTime));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dPortGmrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                setValDot1dPortGmrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortGmrpStatus (INT4 i4Dot1dBasePort,
                           INT4 i4SetValDot1dPortGmrpStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    if (GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[i4Dot1dBasePort].u1Status ==
        (UINT1) i4SetValDot1dPortGmrpStatus)
    {
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValDot1dPortGmrpStatus)
    {

        case GMRP_ENABLED:

            if (GmrpEnablePort ((UINT2) i4Dot1dBasePort) == GMRP_SUCCESS)
            {
                /* Sending Trigger to MSR */

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dPortGmrpStatus,
                                      u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  GARP_GET_IFINDEX (i4Dot1dBasePort),
                                  i4SetValDot1dPortGmrpStatus));
                if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;
            }

            break;

        case GMRP_DISABLED:

            if (GmrpDisablePort ((UINT2) i4Dot1dBasePort) == GMRP_SUCCESS)
            {
                /* Sending Trigger to MSR */

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dPortGmrpStatus,
                                      u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  GARP_GET_IFINDEX (i4Dot1dBasePort),
                                  i4SetValDot1dPortGmrpStatus));
                if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

                return SNMP_SUCCESS;
            }

            break;

        default:
            break;
    }

    /* Sending Trigger to MSR */

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1dPortGmrpStatus, u4SeqNum,
                          FALSE, GarpLock, GarpUnLock, 1, SNMP_FAILURE);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      GARP_GET_IFINDEX (i4Dot1dBasePort),
                      i4SetValDot1dPortGmrpStatus));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureGarpShutdownStatus
 Input       :  The Indices

                The Object 
                setValDot1qFutureGarpShutdownStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureGarpShutdownStatus (INT4 i4SetValDot1qFutureGarpShutdownStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValDot1qFutureGarpShutdownStatus)
    {
        case GARP_SNMP_TRUE:

            if (GarpDeInit () == GARP_SUCCESS)
            {
                /* Sending Trigger to MSR */

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIDot1qFutureGarpShutdownStatus,
                                      u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  GARP_CURR_CONTEXT_ID (),
                                  i4SetValDot1qFutureGarpShutdownStatus));
                if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;
            }
            break;

        case GARP_SNMP_FALSE:

            if (GarpInit () == GARP_SUCCESS)
            {
                /* Sending Trigger to MSR */

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIDot1qFutureGarpShutdownStatus,
                                      u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  GARP_CURR_CONTEXT_ID (),
                                  i4SetValDot1qFutureGarpShutdownStatus));
                if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                GARP_NODE_STATUS () = GarpGetNodeStatus ();
                return SNMP_SUCCESS;
            }
            /* Garp Init failed. So de-init. */
            GarpDeInit ();
            break;

        default:
            break;
    }

    /* Sending Trigger to MSR */
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureGarpShutdownStatus,
                          u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                          SNMP_FAILURE);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", GARP_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureGarpShutdownStatus));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qGvrpStatus
 Input       :  The Indices

                The Object 
                retValDot1qGvrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qGvrpStatus (INT4 *pi4RetValDot1qGvrpStatus)
{
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        *pi4RetValDot1qGvrpStatus = GVRP_DISABLED;

        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qGvrpStatus = GVRP_ADMIN_STATUS ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureGvrpOperStatus
 Input       :  The Indices

                The Object
                retValDot1qFutureGvrpOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureGvrpOperStatus (INT4 *pi4RetValDot1qFutureGvrpOperStatus)
{
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        *pi4RetValDot1qFutureGvrpOperStatus = GVRP_DISABLED;

        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qFutureGvrpOperStatus =
        gau1GvrpStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId];
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureGmrpOperStatus
 Input       :  The Indices

                The Object
                retValDot1qFutureGmrpOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureGmrpOperStatus (INT4 *pi4RetValDot1qFutureGmrpOperStatus)
{
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        *pi4RetValDot1qFutureGmrpOperStatus = GMRP_DISABLED;

        return SNMP_SUCCESS;
    }

    *pi4RetValDot1qFutureGmrpOperStatus = GmrpIsGmrpEnabled ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qPortGvrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1qPortGvrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qPortGvrpStatus (INT4 i4Dot1dBasePort,
                           INT4 *pi4RetValDot1qPortGvrpStatus)
{
    UINT2               u2Port = (UINT2) i4Dot1dBasePort;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        *pi4RetValDot1qPortGvrpStatus = GARP_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    if ((u2Port == 0) || (u2Port > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (u2Port) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1qPortGvrpStatus =
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qPortGvrpFailedRegistrations
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1qPortGvrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qPortGvrpFailedRegistrations (INT4 i4Dot1dBasePort,
                                        UINT4 *pu4GvrpNumFailedReg)
{

    *pu4GvrpNumFailedReg = 0;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if ((i4Dot1dBasePort == 0)
        || (i4Dot1dBasePort > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (i4Dot1dBasePort) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    GvrpGetNumRegFailed ((UINT2) i4Dot1dBasePort, pu4GvrpNumFailedReg);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qPortGvrpLastPduOrigin
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1qPortGvrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qPortGvrpLastPduOrigin (INT4 i4Dot1dBasePort,
                                  tMacAddr * pLastPduOrigin)
{
    MEMSET (pLastPduOrigin, 0, sizeof (tMacAddr));

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if ((i4Dot1dBasePort == 0)
        || (i4Dot1dBasePort > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (i4Dot1dBasePort) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    GvrpGetLastPduOrigin ((UINT2) i4Dot1dBasePort, *pLastPduOrigin);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dGmrpStatus
 Input       :  The Indices

                The Object 
                retValDot1dGmrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dGmrpStatus (INT4 *pi4RetValDot1dGmrpStatus)
{
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        *pi4RetValDot1dGmrpStatus = GMRP_DISABLED;

        return SNMP_SUCCESS;
    }

    *pi4RetValDot1dGmrpStatus = GMRP_ADMIN_STATUS ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGarpJoinTime
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dPortGarpJoinTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGarpJoinTime (INT4 i4Dot1dBasePort,
                             INT4 *pi4RetValDot1dPortGarpJoinTime)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;

    *pi4RetValDot1dPortGarpJoinTime = 0;
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if ((i4Dot1dBasePort == 0)
        || (i4Dot1dBasePort > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (i4Dot1dBasePort) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1dBasePort);

    *pi4RetValDot1dPortGarpJoinTime = (INT4) pGlobalPortEntry->u4JoinTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGarpLeaveTime
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dPortGarpLeaveTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGarpLeaveTime (INT4 i4Dot1dBasePort,
                              INT4 *pi4RetValDot1dPortGarpLeaveTime)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;

    *pi4RetValDot1dPortGarpLeaveTime = 0;
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if ((i4Dot1dBasePort == 0)
        || (i4Dot1dBasePort > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (i4Dot1dBasePort) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1dBasePort);

    *pi4RetValDot1dPortGarpLeaveTime = (INT4) pGlobalPortEntry->u4LeaveTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGarpLeaveAllTime
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dPortGarpLeaveAllTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGarpLeaveAllTime (INT4 i4Dot1dBasePort,
                                 INT4 *pi4RetValDot1dPortGarpLeaveAllTime)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;

    *pi4RetValDot1dPortGarpLeaveAllTime = 0;
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if ((i4Dot1dBasePort == 0)
        || (i4Dot1dBasePort > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (i4Dot1dBasePort) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1dBasePort);

    *pi4RetValDot1dPortGarpLeaveAllTime =
        (INT4) pGlobalPortEntry->u4LeaveAllTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGmrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dPortGmrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGmrpStatus (INT4 i4Dot1dBasePort,
                           INT4 *pi4RetValDot1dPortGmrpStatus)
{
    UINT2               u2Port = (UINT2) i4Dot1dBasePort;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        *pi4RetValDot1dPortGmrpStatus = GMRP_DISABLED;
        return SNMP_SUCCESS;
    }

    if ((u2Port == 0) || (u2Port > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (u2Port) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1dPortGmrpStatus =
        GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGmrpFailedRegistrations
 Input       :  The Indices
                Dot1dBasePort

                The Object 
               
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGmrpFailedRegistrations (INT4 i4Dot1dBasePort,
                                        UINT4
                                        *pu4RetValDot1dPortGmrpFailedRegistrations)
{
    *pu4RetValDot1dPortGmrpFailedRegistrations = 0;
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if ((i4Dot1dBasePort == 0)
        || (i4Dot1dBasePort > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (i4Dot1dBasePort) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    GmrpGetNumRegFailed ((UINT2) i4Dot1dBasePort,
                         pu4RetValDot1dPortGmrpFailedRegistrations);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGmrpLastPduOrigin
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dPortGmrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGmrpLastPduOrigin (INT4 i4Dot1dBasePort,
                                  tMacAddr * pRetValDot1dPortGmrpLastPduOrigin)
{
    MEMSET (pRetValDot1dPortGmrpLastPduOrigin, 0, sizeof (tMacAddr));
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if ((i4Dot1dBasePort == 0)
        || (i4Dot1dBasePort > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (i4Dot1dBasePort) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    GmrpGetLastPduOrigin ((UINT2) i4Dot1dBasePort,
                          *pRetValDot1dPortGmrpLastPduOrigin);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureGarpShutdownStatus
 Input       :  The Indices

                The Object 
                retValDot1qFutureGarpShutdownStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureGarpShutdownStatus (INT4
                                     *pi4RetValDot1qFutureGarpShutdownStatus)
{
    *pi4RetValDot1qFutureGarpShutdownStatus =
        gau1GarpShutDownStatus[(GARP_CURR_CONTEXT_PTR ())->u4ContextId];

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qGvrpStatus
 Input       :  The Indices

                The Object 
                testValDot1qGvrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qGvrpStatus (UINT4 *pu4ErrorCode, INT4 i4TestValDot1qGvrpStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (GarpL2IwfGetBridgeMode (GARP_CURR_CONTEXT_ID (),
                                &GARP_BRIDGE_MODE ()) != L2IWF_SUCCESS)
    {
        return GARP_FAILURE;
    }

    if ((GARP_BRIDGE_MODE () == GARP_INVALID_BRIDGE_MODE) ||
        (GARP_BRIDGE_MODE () == GARP_PBB_ICOMPONENT_BRIDGE_MODE) ||
        (GARP_BRIDGE_MODE () == GARP_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
        CLI_SET_ERR (CLI_GARP_PBB_MODE_GVRP_STARTED_ERR);
        i1RetVal = SNMP_FAILURE;

    }

    if (((GARP_BRIDGE_MODE () == GARP_PBB_ICOMPONENT_BRIDGE_MODE) ||
         (GARP_BRIDGE_MODE () == GARP_PBB_BCOMPONENT_BRIDGE_MODE)) &&
        (i4TestValDot1qGvrpStatus == GVRP_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
        CLI_SET_ERR (CLI_GARP_PBB_MODE_STARTED_ERR);
        return SNMP_FAILURE;
    }

    switch (i4TestValDot1qGvrpStatus)
    {

        case GVRP_ENABLED:
            if (GarpVlanIsVlanEnabledInContext
                (GARP_CURR_CONTEXT_ID ()) == VLAN_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_GARP_VLAN_DISABLED_ERR);
                i1RetVal = SNMP_FAILURE;
            }
            if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_GARP_DISABLED_ERR);
                i1RetVal = SNMP_FAILURE;
            }
            if (GarpIsPvrstStartedInContext (GARP_CURR_CONTEXT_ID ()) ==
                PVRST_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_GARP_PVRST_STARTED_ERR);
                i1RetVal = SNMP_FAILURE;
            }
            break;

        case GVRP_DISABLED:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpJoinEmptyTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpJoinEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpJoinEmptyTxCount (INT4 i4Dot1qFutureVlanPort,
                                               UINT4
                                               *pu4RetValDot1qFutureVlanPortGmrpJoinEmptyTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpJoinEmptyTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpJoinEmptyTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpJoinEmptyTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpJoinEmptyRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpJoinEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpJoinEmptyRxCount (INT4 i4Dot1qFutureVlanPort,
                                               UINT4
                                               *pu4RetValDot1qFutureVlanPortGmrpJoinEmptyRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpJoinEmptyRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpJoinEmptyRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpJoinEmptyRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpJoinInTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpJoinInTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpJoinInTxCount (INT4 i4Dot1qFutureVlanPort,
                                            UINT4
                                            *pu4RetValDot1qFutureVlanPortGmrpJoinInTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpJoinInTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpJoinInTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpJoinInTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpJoinInRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpJoinInRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpJoinInRxCount (INT4 i4Dot1qFutureVlanPort,
                                            UINT4
                                            *pu4RetValDot1qFutureVlanPortGmrpJoinInRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpJoinInRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpJoinInRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpJoinInRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpLeaveInTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpLeaveInTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpLeaveInTxCount (INT4 i4Dot1qFutureVlanPort,
                                             UINT4
                                             *pu4RetValDot1qFutureVlanPortGmrpLeaveInTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpLeaveInTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpLeaveInTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpLeaveInTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpLeaveInRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpLeaveInRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpLeaveInRxCount (INT4 i4Dot1qFutureVlanPort,
                                             UINT4
                                             *pu4RetValDot1qFutureVlanPortGmrpLeaveInRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpLeaveInRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpLeaveInRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpLeaveInRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpLeaveEmptyTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpLeaveEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpLeaveEmptyTxCount (INT4 i4Dot1qFutureVlanPort,
                                                UINT4
                                                *pu4RetValDot1qFutureVlanPortGmrpLeaveEmptyTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpLeaveEmptyTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpLeaveEmptyTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpLeaveEmptyTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpLeaveEmptyRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpLeaveEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpLeaveEmptyRxCount (INT4 i4Dot1qFutureVlanPort,
                                                UINT4
                                                *pu4RetValDot1qFutureVlanPortGmrpLeaveEmptyRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpLeaveEmptyRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpLeaveEmptyRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpLeaveEmptyRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpEmptyTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpEmptyTxCount (INT4 i4Dot1qFutureVlanPort,
                                           UINT4
                                           *pu4RetValDot1qFutureVlanPortGmrpEmptyTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpEmptyTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpEmptyTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpEmptyTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpEmptyRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpEmptyRxCount (INT4 i4Dot1qFutureVlanPort,
                                           UINT4
                                           *pu4RetValDot1qFutureVlanPortGmrpEmptyRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpEmptyRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpEmptyRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpEmptyRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpLeaveAllTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpLeaveAllTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpLeaveAllTxCount (INT4 i4Dot1qFutureVlanPort,
                                              UINT4
                                              *pu4RetValDot1qFutureVlanPortGmrpLeaveAllTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpLeaveAllTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpLeaveAllTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpLeaveAllTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpLeaveAllRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpLeaveAllRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpLeaveAllRxCount (INT4 i4Dot1qFutureVlanPort,
                                              UINT4
                                              *pu4RetValDot1qFutureVlanPortGmrpLeaveAllRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpLeaveAllRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpLeaveAllRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpLeaveAllRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGmrpDiscardCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGmrpDiscardCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGmrpDiscardCount (INT4 i4Dot1qFutureVlanPort,
                                           UINT4
                                           *pu4RetValDot1qFutureVlanPortGmrpDiscardCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGmrpDiscardCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGmrpDiscardCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GmrpDiscardCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpJoinEmptyTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpJoinEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpJoinEmptyTxCount (INT4 i4Dot1qFutureVlanPort,
                                               UINT4
                                               *pu4RetValDot1qFutureVlanPortGvrpJoinEmptyTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpJoinEmptyTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpJoinEmptyTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpJoinEmptyTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpJoinEmptyRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpJoinEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpJoinEmptyRxCount (INT4 i4Dot1qFutureVlanPort,
                                               UINT4
                                               *pu4RetValDot1qFutureVlanPortGvrpJoinEmptyRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpJoinEmptyRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpJoinEmptyRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpJoinEmptyRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpJoinInTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpJoinInTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpJoinInTxCount (INT4 i4Dot1qFutureVlanPort,
                                            UINT4
                                            *pu4RetValDot1qFutureVlanPortGvrpJoinInTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpJoinInTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpJoinInTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpJoinInTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpJoinInRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpJoinInRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpJoinInRxCount (INT4 i4Dot1qFutureVlanPort,
                                            UINT4
                                            *pu4RetValDot1qFutureVlanPortGvrpJoinInRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpJoinInRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpJoinInRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpJoinInRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpLeaveInTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpLeaveInTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpLeaveInTxCount (INT4 i4Dot1qFutureVlanPort,
                                             UINT4
                                             *pu4RetValDot1qFutureVlanPortGvrpLeaveInTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpLeaveInTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpLeaveInTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpLeaveInTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpLeaveInRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpLeaveInRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpLeaveInRxCount (INT4 i4Dot1qFutureVlanPort,
                                             UINT4
                                             *pu4RetValDot1qFutureVlanPortGvrpLeaveInRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpLeaveInRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpLeaveInRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpLeaveInRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpLeaveEmptyTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpLeaveEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpLeaveEmptyTxCount (INT4 i4Dot1qFutureVlanPort,
                                                UINT4
                                                *pu4RetValDot1qFutureVlanPortGvrpLeaveEmptyTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpLeaveEmptyTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpLeaveEmptyTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpLeaveEmptyTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpLeaveEmptyRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpLeaveEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpLeaveEmptyRxCount (INT4 i4Dot1qFutureVlanPort,
                                                UINT4
                                                *pu4RetValDot1qFutureVlanPortGvrpLeaveEmptyRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpLeaveEmptyRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpLeaveEmptyRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpLeaveEmptyRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpEmptyTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpEmptyTxCount (INT4 i4Dot1qFutureVlanPort,
                                           UINT4
                                           *pu4RetValDot1qFutureVlanPortGvrpEmptyTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpEmptyTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpEmptyTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpEmptyTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpEmptyRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpEmptyRxCount (INT4 i4Dot1qFutureVlanPort,
                                           UINT4
                                           *pu4RetValDot1qFutureVlanPortGvrpEmptyRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpEmptyRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpEmptyRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpEmptyRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpLeaveAllTxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpLeaveAllTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpLeaveAllTxCount (INT4 i4Dot1qFutureVlanPort,
                                              UINT4
                                              *pu4RetValDot1qFutureVlanPortGvrpLeaveAllTxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpLeaveAllTxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpLeaveAllTxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpLeaveAllTxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpLeaveAllRxCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpLeaveAllRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpLeaveAllRxCount (INT4 i4Dot1qFutureVlanPort,
                                              UINT4
                                              *pu4RetValDot1qFutureVlanPortGvrpLeaveAllRxCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpLeaveAllRxCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpLeaveAllRxCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpLeaveAllRxCount;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureVlanPortGvrpDiscardCount
 Input       :  The Indices
                i4Dot1qFutureVlanPort
                The Object
                pu4RetValDot1qFutureVlanPortGvrpDiscardCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureVlanPortGvrpDiscardCount (INT4 i4Dot1qFutureVlanPort,
                                           UINT4
                                           *pu4RetValDot1qFutureVlanPortGvrpDiscardCount)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    *pu4RetValDot1qFutureVlanPortGvrpDiscardCount = 0;

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1qFutureVlanPort);

    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        *pu4RetValDot1qFutureVlanPortGvrpDiscardCount =
            (INT4) pGlobalPortEntry->pGarpPortStats->u4GvrpDiscardCount;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qGvrpStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qGvrpStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qPortGvrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                testValDot1qPortGvrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qPortGvrpStatus (UINT4 *pu4ErrorCode,
                              INT4 i4Dot1dBasePort,
                              INT4 i4TestValDot1qPortGvrpStatus)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;
    UINT4               u4IcclIfIndex = 0;
    UINT1               u1PortType;
    UINT1               u1TunnelStatus = 0;
    UINT1               u1Status;

    if (GARP_IS_PORT_VALID (i4Dot1dBasePort) == GARP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (i4Dot1dBasePort) == GARP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_GARP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qPortGvrpStatus != GVRP_ENABLED) &&
        (i4TestValDot1qPortGvrpStatus != GVRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1dBasePort);
    if (pGlobalPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4TestValDot1qPortGvrpStatus == GVRP_ENABLED)
    {
        if ((GarpL2IwfGetVlanPortType ((UINT2) pGlobalPortEntry->u4IfIndex,
                                       &u1PortType)) == L2IWF_SUCCESS)
        {
            if (u1PortType == VLAN_ACCESS_PORT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_GARP_ACCESS_PORT_ERR);
                return SNMP_FAILURE;
            }
        }
        /* If the port is a CEP/CNP(Port-Based)/PCEP/PCNP then GVRP will be 
         * always disabled on that port. */
        if (GvrpPbIsPortValidForGvrpEnable ((UINT2) i4Dot1dBasePort)
            == GVRP_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_GARP_PB_GVRP_PBPORT_ERR);
            return SNMP_FAILURE;
        }

        if (GARP_BRIDGE_MODE () == VLAN_CUSTOMER_BRIDGE_MODE)
        {
            GarpL2IwfGetProtocolTunnelStatusOnPort
                (GARP_GET_IFINDEX (i4Dot1dBasePort),
                 L2_PROTO_GVRP, &u1TunnelStatus);

            if ((u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||
                (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_GVRP_TUNNEL_ENABLED_ERR);
                return SNMP_FAILURE;
            }
        }
        if (SISP_IS_LOGICAL_PORT (GARP_GET_IFINDEX (i4Dot1dBasePort))
            == VCM_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_GVRP_SISP_PORT_ERR);
            return SNMP_FAILURE;
        }
        else
        {
            VlanL2IwfGetSispPortCtrlStatus (GARP_GET_IFINDEX (i4Dot1dBasePort),
                                            &u1Status);
            if (u1Status == L2IWF_ENABLED)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_GVRP_SISP_PORT_ERR);
                return SNMP_FAILURE;
            }
        }
    }

    if (i4TestValDot1qPortGvrpStatus == GVRP_ENABLED)
    {
        GarpIcchGetIcclIfIndex (&u4IcclIfIndex);

        if (u4IcclIfIndex == GARP_GET_IFINDEX (i4Dot1dBasePort))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_GVRP_DISABLED_ON_ICCL_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dPortGarpTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDot1dPortGarpTable (INT4 i4Dot1dBasePort)
{
    tGarpGlobalPortEntry *pGlobPortEntry;
    UINT2               u2Port = (UINT2) i4Dot1dBasePort;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_VALID (u2Port) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    pGlobPortEntry = GARP_PORT_ENTRY (u2Port);

    if (pGlobPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (GARP_IS_PORT_CREATED (u2Port) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dPortGmrpTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1dPortGmrpTable (INT4 i4Dot1dBasePort)
{
    UINT2               u2Port = (UINT2) i4Dot1dBasePort;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    if ((u2Port == 0) || (u2Port > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (u2Port) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dGmrpStatus
 Input       :  The Indices

                The Object 
                testValDot1dGmrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dGmrpStatus (UINT4 *pu4ErrorCode, INT4 i4TestValDot1dGmrpStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    if (GarpL2IwfGetBridgeMode (GARP_CURR_CONTEXT_ID (),
                                &GARP_BRIDGE_MODE ()) != L2IWF_SUCCESS)
    {
        return GARP_FAILURE;
    }

    if ((GARP_BRIDGE_MODE () == GARP_INVALID_BRIDGE_MODE) ||
        (GARP_BRIDGE_MODE () == GARP_PBB_ICOMPONENT_BRIDGE_MODE) ||
        (GARP_BRIDGE_MODE () == GARP_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
        CLI_SET_ERR (CLI_GARP_PBB_MODE_GMRP_STARTED_ERR);
        i1RetVal = SNMP_FAILURE;

    }

    if (((GARP_BRIDGE_MODE () == GARP_PBB_ICOMPONENT_BRIDGE_MODE) ||
         (GARP_BRIDGE_MODE () == GARP_PBB_BCOMPONENT_BRIDGE_MODE)) &&
        (i4TestValDot1dGmrpStatus == GMRP_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
        CLI_SET_ERR (CLI_GARP_PBB_MODE_STARTED_ERR);
        return SNMP_FAILURE;
    }

    switch (i4TestValDot1dGmrpStatus)
    {

        case GMRP_ENABLED:
            if (GarpVlanIsVlanEnabledInContext
                (GARP_CURR_CONTEXT_ID ()) == VLAN_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_GARP_VLAN_DISABLED_ERR);
                i1RetVal = SNMP_FAILURE;
            }
            if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_GARP_DISABLED_ERR);
                i1RetVal = SNMP_FAILURE;
            }

            /* Enable GMRP status only when the IGS is disabled */
            if (SNOOP_ENABLED ==
                GarpSnoopIsIgmpSnoopingEnabled (GARP_CURR_CONTEXT_ID ()))
            {
                CLI_SET_ERR (CLI_GARP_IGS_ENABLED_ERR);
                return SNMP_FAILURE;
            }

            if (SNOOP_ENABLED ==
                GarpSnoopIsMldSnoopingEnabled (GARP_CURR_CONTEXT_ID ()))
            {
                CLI_SET_ERR (CLI_GARP_MLDS_ENABLED_ERR);
                return SNMP_FAILURE;
            }

            if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
            {
                CLI_SET_ERR (CLI_GARP_PB_GMRP_ENABLED_ERR);
                return SNMP_FAILURE;
            }
            break;

        case GMRP_DISABLED:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1dGmrpStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dGmrpStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dPortGarpJoinTime
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                testValDot1dPortGarpJoinTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortGarpJoinTime (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                                INT4 i4TestValDot1dPortGarpJoinTime)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        CLI_SET_ERR (CLI_GARP_DISABLED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_VALID (i4Dot1dBasePort) == GARP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1dBasePort);

    if (pGlobalPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_GARP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if (GARP_IS_JOIN_TIME_VALID ((UINT4) i4TestValDot1dPortGarpJoinTime,
                                 pGlobalPortEntry->u4LeaveTime) == GARP_FALSE)
    {
        CLI_SET_ERR (CLI_GARP_TIMER_VAL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dPortGarpLeaveTime
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                testValDot1dPortGarpLeaveTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortGarpLeaveTime (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                                 INT4 i4TestValDot1dPortGarpLeaveTime)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        CLI_SET_ERR (CLI_GARP_DISABLED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_VALID (i4Dot1dBasePort) == GARP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1dBasePort);

    if (pGlobalPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_GARP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if (GARP_IS_LEAVE_TIME_VALID ((UINT4) i4TestValDot1dPortGarpLeaveTime,
                                  pGlobalPortEntry->u4LeaveAllTime,
                                  pGlobalPortEntry->u4JoinTime) == GARP_FALSE)
    {
        CLI_SET_ERR (CLI_GARP_TIMER_VAL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dPortGarpLeaveAllTime
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                testValDot1dPortGarpLeaveAllTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortGarpLeaveAllTime (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                                    INT4 i4TestValDot1dPortGarpLeaveAllTime)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        CLI_SET_ERR (CLI_GARP_DISABLED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_VALID (i4Dot1dBasePort) == GARP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (i4Dot1dBasePort);

    if (pGlobalPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_GARP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if (GARP_IS_LEAVE_ALL_TIME_VALID (i4TestValDot1dPortGarpLeaveAllTime,
                                      (INT4) pGlobalPortEntry->u4LeaveTime)
        == GARP_FALSE)
    {
        CLI_SET_ERR (CLI_GARP_TIMER_VAL_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dPortGarpTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dPortGarpTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dPortGmrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                testValDot1dPortGmrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortGmrpStatus (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                              INT4 i4TestValDot1dPortGmrpStatus)
{
    UINT4               u4IcclIfIndex = 0;
    UINT1               u1TunnelStatus = 0;

    if (GARP_IS_PORT_VALID (i4Dot1dBasePort) == GARP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1dPortGmrpStatus != GMRP_DISABLED)
        && (i4TestValDot1dPortGmrpStatus != GMRP_ENABLED))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValDot1dPortGmrpStatus == GMRP_ENABLED)
    {
        if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_GARP_PB_GMRP_ENABLED_ERR);
            return SNMP_FAILURE;
        }
    }

    if (GARP_IS_PORT_CREATED (i4Dot1dBasePort) == GARP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_GARP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValDot1dPortGmrpStatus == GMRP_ENABLED)
    {
        if (GARP_BRIDGE_MODE () == GARP_PROVIDER_BRIDGE_MODE)
        {
            GarpL2IwfGetPortVlanTunnelStatus
                ((UINT2) GARP_GET_IFINDEX (i4Dot1dBasePort),
                 (BOOL1 *) & u1TunnelStatus);

            if (u1TunnelStatus == OSIX_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_GMRP_PROTO_ENA_ERR);
                return SNMP_FAILURE;
            }
        }

        /*In case of 1ad bridges, GMRP is always disabled so no need to
         * check the protocol tunnel status on the port*/
        if (GARP_BRIDGE_MODE () == GARP_CUSTOMER_BRIDGE_MODE)
        {
            GarpL2IwfGetProtocolTunnelStatusOnPort
                (GARP_GET_IFINDEX (i4Dot1dBasePort), L2_PROTO_GMRP,
                 &u1TunnelStatus);

            if ((u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||
                (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_GMRP_TUNNEL_ENABLED_ERR);
                return SNMP_FAILURE;
            }
        }
    }
    
	if (i4TestValDot1dPortGmrpStatus == GMRP_ENABLED)
    {
        GarpIcchGetIcclIfIndex (&u4IcclIfIndex);

        if (u4IcclIfIndex == GARP_GET_IFINDEX (i4Dot1dBasePort))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_GMRP_DISABLED_ON_ICCL_ERR);
            return SNMP_FAILURE;
        }
    }
    
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureGarpShutdownStatus
 Input       :  The Indices

                The Object 
                testValDot1qFutureGarpShutdownStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureGarpShutdownStatus (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValDot1qFutureGarpShutdownStatus)
{
    tGarpAppEntry      *pAppEntry;
    UINT2               u2Index;

    if ((gau1GarpShutDownStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId])
        == i4TestValDot1qFutureGarpShutdownStatus)
    {
        /* Garp module is already in the same state. So 
         * return success*/
        return SNMP_SUCCESS;
    }

    if (GarpL2IwfGetBridgeMode (GARP_CURR_CONTEXT_ID (),
                                &GARP_BRIDGE_MODE ()) != L2IWF_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((GARP_BRIDGE_MODE () == GARP_INVALID_BRIDGE_MODE) ||
        (GARP_BRIDGE_MODE () == GARP_PBB_ICOMPONENT_BRIDGE_MODE) ||
        (GARP_BRIDGE_MODE () == GARP_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
        CLI_SET_ERR (CLI_GARP_PBB_MODE_STARTED_ERR);
        return SNMP_FAILURE;

    }

    if ((i4TestValDot1qFutureGarpShutdownStatus != GARP_SNMP_TRUE) &&
        (i4TestValDot1qFutureGarpShutdownStatus != GARP_SNMP_FALSE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureGarpShutdownStatus == GARP_SNMP_FALSE)
        && (GarpVlanGetStartedStatus (GARP_CURR_CONTEXT_PTR ()->u4ContextId)
            == VLAN_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_GARP_NOSHUT_VLAN_ENABLED_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qFutureGarpShutdownStatus == GARP_SNMP_FALSE)
        && (GarpLaGetMCLAGSystemStatus () == LA_MCLAG_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
         CLI_SET_ERR (CLI_GARP_NOSHUT_MCLAG_ENABLED_ERR);
         return SNMP_FAILURE;
    }
        
    if (MrpApiIsMrpStarted (GARP_CURR_CONTEXT_PTR ()->u4ContextId) == OSIX_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_GARP_NOSHUT_MRP_START_ERR);
        return SNMP_FAILURE;
    }

    if (VlanGetBaseBridgeMode () == DOT_1D_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_GARP_BASE_BRIDGE_GARP_ENABLED);
        return SNMP_FAILURE;
    }

    if (i4TestValDot1qFutureGarpShutdownStatus == GARP_SNMP_TRUE)
    {
        for (u2Index = 1; u2Index <= GARP_MAX_APPS; u2Index++)
        {
            pAppEntry = &GARP_CURR_CONTEXT_PTR ()->aGarpAppTable[u2Index];

            if (pAppEntry->u1Status == GARP_VALID)
            {
                /* 
                 * GARP can be disabled only when all the GARP 
                 * applications are diabled 
                 */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_GARP_SHUT_APP_ENABLED_ERR);
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureGarpShutdownStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureGarpShutdownStatus (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dPortGarpTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1dPortGarpTable (INT4 *pi4Dot1dBasePort)
{
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1dPortGarpTable (0, pi4Dot1dBasePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dPortGarpTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1dPortGarpTable (INT4 i4Dot1dBasePort,
                                   INT4 *pi4NextDot1dBasePort)
{
    UINT4               u4Index;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort < 0)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort > GARP_MAX_PORTS_PER_CONTEXT)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort <= 0)
    {
        i4Dot1dBasePort = 0;
    }

    for (u4Index = (UINT4) i4Dot1dBasePort + 1;
         u4Index <= GARP_MAX_PORTS_PER_CONTEXT; u4Index++)
    {
        if (GARP_PORT_ENTRY (u4Index) != NULL)
        {
            *pi4NextDot1dBasePort = (INT4) u4Index;

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dPortGmrpTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1dPortGmrpTable (INT4 *pi4Dot1dBasePort)
{
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        return SNMP_FAILURE;
    }

    return (nmhGetNextIndexDot1dPortGmrpTable (0, pi4Dot1dBasePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dPortGmrpTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1dPortGmrpTable (INT4 i4Dot1dBasePort,
                                   INT4 *pi4NextDot1dBasePort)
{
    UINT4               u4Index;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort < 0)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort > GARP_MAX_PORTS_PER_CONTEXT)
    {
        return SNMP_FAILURE;
    }

    if (i4Dot1dBasePort <= 0)
    {
        i4Dot1dBasePort = 0;
    }

    for (u4Index = (UINT4) i4Dot1dBasePort + 1;
         u4Index <= GARP_MAX_PORTS_PER_CONTEXT; u4Index++)
    {
        if (GARP_PORT_ENTRY (u4Index) != NULL)
        {
            *pi4NextDot1dBasePort = (INT4) u4Index;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  nmhGetDot1qPortRestrictedVlanRegistration
 *  Input       :  The Indices
 *                 Dot1dBasePort
 *                 The Object
 *                 retValDot1qPortRestrictedVlanRegistration
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qPortRestrictedVlanRegistration (INT4 i4Dot1dBasePort,
                                           INT4
                                           *pi4RetValDot1qPortRestrictedVlanRegistration)
{

    UINT2               u2Port = (UINT2) i4Dot1dBasePort;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        *pi4RetValDot1qPortRestrictedVlanRegistration = GARP_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    if ((u2Port == 0) || (u2Port > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (u2Port) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1qPortRestrictedVlanRegistration =
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1RestrictedRegControl;

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function   :  nmhSetDot1qPortRestrictedVlanRegistration
 *  Input      :  The Indices
 *                Dot1dBasePort
 *                The Object
 *                setValDot1qPortRestrictedVlanRegistration
 *  Output     :  The Set Low Lev Routine Take the Indices &
 *                Sets the Value accordingly.
 *  Returns    :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qPortRestrictedVlanRegistration (INT4 i4Dot1dBasePort,
                                           INT4
                                           i4SetValDot1qPortRestrictedVlanRegistration)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4CurrContextId = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValDot1qPortRestrictedVlanRegistration)
    {

        case GVRP_ENABLED:
            /* Set Restricted VLAN Registration Control for that port */

            if (GvrpEnablePortRestrictedRegControl ((UINT2) i4Dot1dBasePort) ==
                GVRP_SUCCESS)
            {
                /* Sending Trigger to MSR */

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsDot1qPortRestrictedVlanRegistration,
                                      u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  GARP_GET_IFINDEX (i4Dot1dBasePort),
                                  i4SetValDot1qPortRestrictedVlanRegistration));
                if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;
            }
            break;

        case GVRP_DISABLED:
            /* Reset Restricted VLAN Registration Control for that port */

            if (GvrpDisablePortRestrictedRegControl ((UINT2) i4Dot1dBasePort) ==
                GVRP_SUCCESS)
            {
                /* Sending Trigger to MSR */

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsDot1qPortRestrictedVlanRegistration,
                                      u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  GARP_GET_IFINDEX (i4Dot1dBasePort),
                                  i4SetValDot1qPortRestrictedVlanRegistration));
                if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;
            }
            break;

        default:
            break;
    }

    /* Sending Trigger to MSR */

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsDot1qPortRestrictedVlanRegistration,
                          u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                          SNMP_FAILURE);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      GARP_GET_IFINDEX (i4Dot1dBasePort),
                      i4SetValDot1qPortRestrictedVlanRegistration));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function   :  nmhTestv2Dot1qPortRestrictedVlanRegistration
 *  Input      :  The Indices
 *                Dot1dBasePort
 *                The Object
 *                testValDot1qPortRestrictedVlanRegistration
 *  Output     :  The Test Low Lev Routine Take the Indices &
 *                Test whether that Value is Valid Input for Set.
 *                Stores the value of error code in the Return val
 * Error Codes :  The following error codes are to be returned
 *                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *                                                                                                                             *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qPortRestrictedVlanRegistration (UINT4 *pu4ErrorCode,
                                              INT4 i4Dot1dBasePort,
                                              INT4
                                              i4TestValDot1qPortRestrictedVlanRegistration)
{

    if (GARP_IS_PORT_VALID (i4Dot1dBasePort) == GARP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (i4Dot1dBasePort) == GARP_FALSE)
    {
        CLI_SET_ERR (CLI_GARP_INVALID_PORT_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1qPortRestrictedVlanRegistration == GARP_ENABLED) ||
        (i4TestValDot1qPortRestrictedVlanRegistration == GARP_DISABLED))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  nmhTestv2Dot1dPortRestrictedGroupRegistration
 *  Input       :  The Indices
 *                 Dot1dBasePort
 *                 The Object
 *                 testValDot1dPortRestrictedGroupRegistration
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *
 * Returns      :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortRestrictedGroupRegistration (UINT4 *pu4ErrorCode,
                                               INT4 i4Dot1dBasePort,
                                               INT4
                                               i4TestValDot1dPortRestrictedGroupRegistration)
{

    if (GARP_IS_PORT_VALID (i4Dot1dBasePort) == GARP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (i4Dot1dBasePort) == GARP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_GARP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1dPortRestrictedGroupRegistration == GARP_ENABLED) ||
        (i4TestValDot1dPortRestrictedGroupRegistration == GARP_DISABLED))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dPortGmrpTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dPortGmrpTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetDot1dPortRestrictedGroupRegistration
 *  Input       :  The Indices
 *                 Dot1dBasePort
 *                 The Object
 *                 retValDot1dPortRestrictedGroupRegistration
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortRestrictedGroupRegistration (INT4 i4Dot1dBasePort,
                                            INT4
                                            *pi4RetValDot1dPortRestrictedGroupRegistration)
{

    UINT2               u2Port = (UINT2) i4Dot1dBasePort;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        *pi4RetValDot1dPortRestrictedGroupRegistration = GARP_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    if ((u2Port == 0) || (u2Port > GARP_MAX_PORTS_PER_CONTEXT))
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_PORT_CREATED (u2Port) == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValDot1dPortRestrictedGroupRegistration =
        GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1RestrictedRegControl;

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhSetDot1dPortRestrictedGroupRegistration
 *  Input       :  The Indices
 *                 Dot1dBasePort
 *                 The Object
 *                 setValDot1dPortRestrictedGroupRegistration
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortRestrictedGroupRegistration (INT4 i4Dot1dBasePort,
                                            INT4
                                            i4SetValDot1dPortRestrictedGroupRegistration)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValDot1dPortRestrictedGroupRegistration)
    {
        case GMRP_ENABLED:
            /* Set Restricted Group Registration for that port */

            if (GmrpEnablePortRestrictedRegControl ((UINT2) i4Dot1dBasePort) ==
                GVRP_SUCCESS)
            {
                /* Sending Trigger to MSR */

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsDot1dPortRestrictedGroupRegistration,
                                      u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  GARP_GET_IFINDEX (i4Dot1dBasePort),
                                  i4SetValDot1dPortRestrictedGroupRegistration));
                if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;
            }
            break;

        case GMRP_DISABLED:

            /*Reset Restricted Group Registration for that port */
            if (GmrpDisablePortRestrictedRegControl ((UINT2) i4Dot1dBasePort) ==
                GVRP_SUCCESS)
            {
                /* Sending Trigger to MSR */

                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsDot1dPortRestrictedGroupRegistration,
                                      u4SeqNum, FALSE, GarpLock, GarpUnLock, 1,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  GARP_GET_IFINDEX (i4Dot1dBasePort),
                                  i4SetValDot1dPortRestrictedGroupRegistration));
                if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;

            }
            break;

        default:
            break;
    }

    /* Sending Trigger to MSR */

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsDot1dPortRestrictedGroupRegistration, u4SeqNum,
                          FALSE, GarpLock, GarpUnLock, 1, SNMP_FAILURE);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                      GARP_GET_IFINDEX (i4Dot1dBasePort),
                      i4SetValDot1dPortRestrictedGroupRegistration));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureGarpDebug
 Input       :  The Indices

                The Object
                retValDot1qFutureGarpDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS
****************************************************************************/
INT1
nmhGetDot1qFutureGarpDebug (INT4 *pi4RetValDot1qFutureGarpDebug)
{
#ifdef TRACE_WANTED
    *pi4RetValDot1qFutureGarpDebug = (INT4) gpGarpContextInfo->u4GarpTrcOption;
    return SNMP_SUCCESS;
#else
    *pi4RetValDot1qFutureGarpDebug = 0;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureGarpDebug
 Input       :  The Indices

                The Object
                setValDot1qFutureGarpDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureGarpDebug (INT4 i4SetValDot1qFutureGarpDebug)
{
#ifdef TRACE_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    gpGarpContextInfo->u4GarpTrcOption = (UINT4) i4SetValDot1qFutureGarpDebug;

    /* Sending Trigger to MSR */

    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureGarpDebug, u4SeqNum,
                          FALSE, GarpLock, GarpUnLock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", GARP_CURR_CONTEXT_ID (),
                      i4SetValDot1qFutureGarpDebug));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValDot1qFutureGarpDebug);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureGarpDebug
 Input       :  The Indices

                The Object
                testValDot1qFutureGarpDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureGarpDebug (UINT4 *pu4ErrorCode,
                               INT4 i4TestValDot1qFutureGarpDebug)
{
#ifdef TRACE_WANTED
    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        CLI_SET_ERR (CLI_GARP_DISABLED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValDot1qFutureGarpDebug < GARP_MIN_TRACE_VAL ||
        i4TestValDot1qFutureGarpDebug > GARP_MAX_TRACE_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValDot1qFutureGarpDebug);
    return SNMP_FAILURE;
#endif

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureGarpDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureGarpDebug (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
