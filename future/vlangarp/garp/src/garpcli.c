/* SOURCE FILE HEADER :
 *
 ***********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                 *
 * Licensee Aricent Inc., 2001-2002                                     *
 * $Id: garpcli.c,v 1.46 2017/01/24 13:23:02 siva Exp $                *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : garpcli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : ISS TEAM                                         |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : GARP                                             |
 * |                                                                           |
 * |  MODULE NAME           : Garp configuration                               |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for set/get objects in           | 
 * |                          stdbrgext.mib, stdvlan.mib, fsvlan.mib,          |
 * |                          fsmpvlan.mib, fsmsvlan.mib and fsmsbext.mib      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */
#ifndef __GARPCLI_C__
#define __GARPCLI_C__

#include "garpinc.h"

PRIVATE INT4        GarpLockAndSelCliContext (VOID);
UINT4               gu4GarpTrcLvl;
INT4
cli_process_garp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[VLAN_MAX_ARGS];
    UINT1              *pu1Inst = NULL;
    INT1                argno = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4ErrCode;
    UINT4               u4ContextId;
    UINT4               u4TempContextId = GARP_CLI_INVALID_CONTEXT;
    UINT2               u2LocalPortId;
#ifdef TRACE_WANTED
    INT4                i4Args = 0;
#endif

    CliRegisterLock (CliHandle, GarpLock, GarpUnLock);

    GARP_LOCK ();

    if (GarpCliSelectContextOnMode
        (CliHandle, u4Command, &u4ContextId, &u2LocalPortId) == GARP_FAILURE)
    {
        GARP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);

    if (pu1Inst != 0)
    {
        u4IfIndex = CLI_PTR_TO_U4 (pu1Inst);
    }

    /* Walk through the rest of the arguments and store in args array. 
     * Store 5 arguements at the max. This is because garp commands do not
     * take more than five inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == GARP_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_GARP_SHUT_GARP:

            i4RetStatus = GarpShutdown (CliHandle, GARP_SNMP_TRUE);
            break;

        case CLI_GARP_NO_SHUT_GARP:

            i4RetStatus = GarpShutdown (CliHandle, GARP_SNMP_FALSE);
            break;

        case CLI_GARP_GVRP:
            /* args[0] - GVRP_ENABLED/DISABLED to enable/disable */

            i4RetStatus = GarpSetGvrp (CliHandle, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_GARP_PORT_GVRP:
            /* args[0] - GVRP_ENABLED/DISABLED to enable/disable */
            /* args[1] - Interface index */

            if (GarpVcmGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                  &u4TempContextId,
                                                  &u2LocalPortId) !=
                VCM_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Interface not mapped to any of the context.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (u4TempContextId != GARP_CURR_CONTEXT_ID ())
            {
                CliPrintf (CliHandle,
                           "\r%% Interface not belongs to this Context.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (GARP_IS_GARP_ENABLED () != GVRP_TRUE)
            {
                CliPrintf (CliHandle, "\r%% GARP is shutdown.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            i4RetStatus = GarpSetPortGvrp (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                           u2LocalPortId);

            break;

        case CLI_GARP_GMRP:
            /* args[0] - GMRP_ENABLED/DISABLED to enable/disable */

            i4RetStatus = GarpSetGmrp (CliHandle, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_GARP_PORT_GMRP:
            /* args[0] - GVRP_ENABLED/DISABLED to enable/disable */
            /* args[1] - Interface index */
            if (GarpVcmGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                  &u4TempContextId,
                                                  &u2LocalPortId) !=
                VCM_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Interface not mapped to any of the context.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (u4TempContextId != GARP_CURR_CONTEXT_ID ())
            {
                CliPrintf (CliHandle,
                           "\r%% Interface not belongs to this Context.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (GARP_IS_GARP_ENABLED () != GVRP_TRUE)
            {
                CliPrintf (CliHandle, "\r%% GARP is shutdown.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = GarpSetPortGmrp (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                           u2LocalPortId);
            break;

        case CLI_GARP_CLEAR_STATISTICS:

            if (u4IfIndex != 0)
            {
                if (GarpVcmGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                      &u4TempContextId,
                                                      &u2LocalPortId) !=
                    OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Interface not mapped to any of the "
                               "context.\r\n ");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

            }
            else
            {
                u2LocalPortId = 0;
            }
            i4RetStatus = GarpCliClearStatisticsPort(CliHandle, u4ContextId,
                                                     u4IfIndex);
            break;



        case CLI_GARP_JOIN_TIMER:
        case CLI_GARP_LEAVE_TIMER:
        case CLI_GARP_LEAVE_ALL_TIMER:
            /* args[0] - Timer value */

            if ((*(args[0]) % 10) != 0)
            {
                CliPrintf (CliHandle, "\r%% Invalid Timer Value. Enter in"
                           "multiples of 10 \r\n");

                GarpReleaseContext ();
                GARP_UNLOCK ();

                CliUnRegisterLock (CliHandle);

                return CLI_FAILURE;
            }

            i4RetStatus = GarpSetGarpTimers (CliHandle, u4Command,
                                             *args[0], (UINT4) u2LocalPortId);
            break;

        case CLI_GARP_RESTRICTED_VLAN:
            /* args[0] - GVRP_ENABLED/DISABLED to enable/disable */

            i4RetStatus = GarpSetRestrictedVlanReg (CliHandle,
                                                    CLI_PTR_TO_U4 (args[0]),
                                                    (UINT4) u2LocalPortId);
            break;

        case CLI_GARP_RESTRICTED_GROUP:
            /* args[0] - GMRP_ENABLED/DISABLED to enable/disable */

            i4RetStatus = GarpSetRestrictedGroupReg
                (CliHandle, CLI_PTR_TO_U4 (args[0]), (UINT4) u2LocalPortId);
            break;

#ifdef TRACE_WANTED
        case CLI_GARP_DEBUGS:
            /* args[0] - Switch Name */
            /* args[1] - Debug Module */
            /* args[2] - Debug Type */
            if (args[3] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[3]);
                GarpCliSetDebugLevel (CliHandle, i4Args);
            }

            if (args[0] != NULL)
            {
                if (GarpVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId) !=
                    VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = GARP_DEFAULT_CONTEXT_ID;
            }

            if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = GarpSetDebugs (CliHandle, CLI_PTR_TO_U4 (args[1]),
                                         CLI_PTR_TO_U4 (args[2]), GARP_SET_CMD);
            break;

        case CLI_GARP_NO_DEBUGS:
            /* args[0] - Switch Name */
            /* args[1] - Debug Value */
            /* args[2] - Debug Module */

            if (args[0] != NULL)
            {
                if (GarpVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId)
                    != VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = GARP_DEFAULT_CONTEXT_ID;
            }

            if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = GarpSetDebugs (CliHandle, CLI_PTR_TO_U4 (args[1]),
                                         CLI_PTR_TO_U4 (args[2]), GARP_NO_CMD);
            break;

        case CLI_GARP_GBL_DEBUG:

            i4RetStatus = GarpSetGlobalDebug (CliHandle, GARP_ENABLED);
            break;

        case CLI_GARP_NO_GBL_DEBUG:

            i4RetStatus = GarpSetGlobalDebug (CliHandle, GARP_DISABLED);
            break;

#else
        case CLI_GARP_DEBUGS:
        case CLI_GARP_NO_DEBUGS:
        case CLI_GARP_GBL_DEBUG:
            /* fall through */
        case CLI_GARP_NO_GBL_DEBUG:
            CliPrintf (CliHandle, "\r%% Debug not supported \r\n");
            GARP_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
#endif

        default:
            /* Given command does not match with any SET command. */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            GarpReleaseContext ();
            GARP_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    GarpReleaseContext ();

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_GARP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", GarpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    GARP_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return i4RetStatus;
}

INT4
cli_process_garp_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = 0;
    INT4                i4Inst = 0;
    UINT4               u4ErrCode;
    UINT4               u4ContextId;
    UINT4               u4TempContextId = GARP_CLI_INVALID_CONTEXT;
    UINT2               u2LocalPortId;
    UINT1              *pu1ContextName = NULL;

    CliRegisterLock (CliHandle, GarpLock, GarpUnLock);

    GARP_LOCK ();

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     * After the u4IfIndex. (ie) In all the cli commands we are passing 
     * IfIndex as the first argument in variable argument list. Like that 
     * as the second argument we have to pass context-name*/
    pu1ContextName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguments and store in args array. 
     * Store 5 arguements at the max. This is because garp commands do not
     * take more than five inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    va_end (ap);

    while (GarpCliGetContextForShowCmd
           (CliHandle, pu1ContextName, u4IfIndex, u4TempContextId,
            &u4ContextId, &u2LocalPortId) == GARP_SUCCESS)
    {
        switch (u4Command)
        {
            case CLI_GARP_SHOW_GARP_TIMER:

                i4RetStatus = ShowGarpTimers (CliHandle, u4ContextId,
                                              u4IfIndex);
                break;
            case CLI_GARP_SHOW_GMRP_STATS:
                i4RetStatus = ShowGmrpStatistics (CliHandle, u4ContextId,
                                                  u4IfIndex);
                break;
            case CLI_GARP_SHOW_GVRP_STATS:
                i4RetStatus = ShowGvrpStatistics (CliHandle, u4ContextId,
                                                  u4IfIndex);
                break;
            default:
                /* Given command does not match with any GET command. */
                CliPrintf (CliHandle, "\r%% Unknown command \r\n");

                GARP_UNLOCK ();

                CliUnRegisterLock (CliHandle);

                return CLI_FAILURE;
        }
        /* If SwitchName or Interface is given as input for show command 
         * then we have to come out of the Loop */
        if ((pu1ContextName != NULL) || (u4IfIndex != 0))
        {
            break;
        }
        u4TempContextId = u4ContextId;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_GARP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", GarpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    GARP_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  GarpCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
GarpCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    gu4GarpTrcLvl = 0;

    UNUSED_PARAM (CliHandle);
    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        gu4GarpTrcLvl =
            MGMT_TRC | BUFFER_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC |
            OS_RESOURCE_TRC | CONTROL_PLANE_TRC | DATA_PATH_TRC | DUMP_TRC;

    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        gu4GarpTrcLvl = CONTROL_PLANE_TRC | DATA_PATH_TRC | DUMP_TRC
            | INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        gu4GarpTrcLvl = INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        gu4GarpTrcLvl = INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        gu4GarpTrcLvl = INIT_SHUT_TRC | ALL_FAILURE_TRC;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        gu4GarpTrcLvl = INIT_SHUT_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : GarpShutdown                                      */
/*                                                                          */
/*     DESCRIPTION      : This function will start/shut Garp module         */
/*                                                                          */
/*     INPUT            : i4Status - Garp Module status                     */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
GarpShutdown (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode;

    if (nmhTestv2Dot1qFutureGarpShutdownStatus
        (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureGarpShutdownStatus (i4Status) == SNMP_FAILURE)
        /* if i4Status == VLAN_SNMP_FALSE, then the VLAN module will
         * be started and enabled */
    {
        CliPrintf (CliHandle, "\r%% GARP cannot be shutdown while other"
                   " GARP applications are running\r\n");
        return (CLI_FAILURE);
    }
/* If "no shutdown garp" is executed, GARP is started.
 * However GVRP and GMRP need to be enable explicitly */
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : GarpSetGvrp                                        */
/*                                                                           */
/*     DESCRIPTION      : This function will enable/disable GVRP on the      */
/*                        on the device.                                     */
/*                                                                           */
/*     INPUT            : u4Action  - Enable/Disable Gbl GVRP on the device  */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpSetGvrp (tCliHandle CliHandle, UINT4 u4Action)
{
    UINT4               u4ErrCode;

    if ((u4Action == GARP_ENABLED) && (GARP_IS_GARP_ENABLED () == GARP_FALSE))
        /* if GARP module is shutdown, then the GARP module will
         * be started and enabled */
    {
        if (nmhTestv2Dot1qFutureGarpShutdownStatus
            (&u4ErrCode, GARP_SNMP_FALSE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetDot1qFutureGarpShutdownStatus (GARP_SNMP_FALSE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    if (nmhTestv2Dot1qGvrpStatus (&u4ErrCode, u4Action) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qGvrpStatus (u4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : GarpSetPortGvrp                                    */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables GVRP on port        */
/*                                                                           */
/*     INPUT            : u4Action  - Enable/Disable GVRP on the port        */
/*                        u4PortId  - Port identifier                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpSetPortGvrp (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1qPortGvrpStatus (&u4ErrCode, u4PortId, u4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qPortGvrpStatus (u4PortId, u4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : GarpSetGmrp                                        */
/*                                                                           */
/*     DESCRIPTION      : This function will enable/disable GMRP on the      */
/*                        on the device.                                     */
/*                                                                           */
/*     INPUT            : u4Action  - Enable/Disable Gbl GMRP on the device  */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpSetGmrp (tCliHandle CliHandle, UINT4 u4Action)
{
    UINT4               u4ErrCode;

    if ((u4Action == GARP_ENABLED) && (GARP_IS_GARP_ENABLED () == GARP_FALSE))
        /* if GARP module is shutdown, then the GARP module will
         * be started and enabled */
    {
        if (nmhTestv2Dot1qFutureGarpShutdownStatus
            (&u4ErrCode, GARP_SNMP_FALSE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetDot1qFutureGarpShutdownStatus (GARP_SNMP_FALSE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    if (nmhTestv2Dot1dGmrpStatus (&u4ErrCode, u4Action) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1dGmrpStatus (u4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : GarpSetPortGmrp                                    */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables GMRP on port        */
/*                                                                           */
/*     INPUT            : u4Action  - Enable/Disable GMRP on the port        */
/*                        u4PortId  - Port identifier                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpSetPortGmrp (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    if (nmhTestv2Dot1dPortGmrpStatus (&u4ErrCode, u4PortId, u4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1dPortGmrpStatus (u4PortId, u4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : GarpSetGarpTimers                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets garp timer values               */
/*                                                                           */
/*     INPUT            : u4TimerType   - Type of timer to be configured     */
/*                        u4TimerValue  - Timer value configured             */
/*                        u4PortId      - Port identifier                    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpSetGarpTimers (tCliHandle CliHandle, UINT4 u4TimerType, UINT4 u4TimerValue,
                   UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    u4TimerValue = VLAN_GARP_TIMER_CLI_TO_PROTOCOL (u4TimerValue);

    switch (u4TimerType)
    {
        case CLI_GARP_JOIN_TIMER:
            if (nmhTestv2Dot1dPortGarpJoinTime (&u4ErrCode, u4PortId,
                                                u4TimerValue) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetDot1dPortGarpJoinTime (u4PortId, u4TimerValue)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;

        case CLI_GARP_LEAVE_TIMER:
            if (nmhTestv2Dot1dPortGarpLeaveTime (&u4ErrCode, u4PortId,
                                                 u4TimerValue) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetDot1dPortGarpLeaveTime (u4PortId, u4TimerValue)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;

        case CLI_GARP_LEAVE_ALL_TIMER:
            if (nmhTestv2Dot1dPortGarpLeaveAllTime (&u4ErrCode, u4PortId,
                                                    u4TimerValue)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetDot1dPortGarpLeaveAllTime (u4PortId, u4TimerValue)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : GarpSetRestrictedVlanReg                           */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables a port to be part   */
/*                        of a restriced VLAN.                               */
/*                                                                           */
/*     INPUT            : u4Action   - Action to be performed Set/Re-set     */
/*                        u4PortId   - Port identifier                       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpSetRestrictedVlanReg (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        CliPrintf (CliHandle, "\r%% GARP is shutdown\r\n");
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1qPortRestrictedVlanRegistration (&u4ErrCode, u4PortId,
                                                      u4Action) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qPortRestrictedVlanRegistration (u4PortId, u4Action)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : GarpSetRestrictedGroupReg                          */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables restricted group    */
/*                        registeration on a port                            */
/*                                                                           */
/*     INPUT            : u4Action   - Action to be performed Set/Re-set     */
/*                        u4PortId   - Port identifier                       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpSetRestrictedGroupReg (tCliHandle CliHandle, UINT4 u4Action, UINT4 u4PortId)
{
    UINT4               u4ErrCode;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        CliPrintf (CliHandle, "\r%% GARP is shutdown\r\n");
        return (CLI_FAILURE);
    }

    if (nmhTestv2Dot1dPortRestrictedGroupRegistration (&u4ErrCode, u4PortId,
                                                       u4Action) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1dPortRestrictedGroupRegistration (u4PortId, u4Action)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ShowGarpTimers                                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays VLAN GARP timer values      */
/*                                                                           */
/*     INPUT            : u4ContextId - Context identifier                   */
/*                        u4PortId - Port identifier                         */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowGarpTimers (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId)
{
    INT4                i4JoinTimerVal = 0;
    INT4                i4LeaveTimerVal = 0;
    INT4                i4LeaveAllTimerVal = 0;
    INT4                i4CurrentIndex = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        CliPrintf (CliHandle, "\r%% GARP is shutdown\r\n");
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nGarp Port Timer Info (in milli seconds)\r\n");

    CliPrintf (CliHandle, "---------------------------------------\r\n\r\n");

    CliPrintf (CliHandle,
               "Port     Join-time      Leave-time      Leave-all-time\r\n");

    CliPrintf (CliHandle,
               "-----    ---------      ----------      --------------\r\n");

    if (GarpGetFirstPortInContext (u4ContextId, (UINT4 *) &i4NextIndex) ==
        GARP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (u4PortId != 0)
        {
            if (nmhValidateIndexInstanceFsDot1dPortGarpTable
                ((INT4) u4PortId) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            i4NextIndex = (INT4) u4PortId;
            u1isShowAll = FALSE;
        }

        CLI_MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
        /* base port index */
        GarpCfaCliGetIfName ((UINT4) i4NextIndex, (INT1 *) au1NameStr);

        /* garp join time */
        nmhGetFsDot1dPortGarpJoinTime (i4NextIndex, &i4JoinTimerVal);
        i4JoinTimerVal = VLAN_GARP_TIMER_PROTOCOL_TO_CLI (i4JoinTimerVal);

        /* garp leave time */

        nmhGetFsDot1dPortGarpLeaveTime (i4NextIndex, &i4LeaveTimerVal);
        i4LeaveTimerVal = VLAN_GARP_TIMER_PROTOCOL_TO_CLI (i4LeaveTimerVal);

        /*garp leave all time */

        nmhGetFsDot1dPortGarpLeaveAllTime (i4NextIndex, &i4LeaveAllTimerVal);
        i4LeaveAllTimerVal =
            VLAN_GARP_TIMER_PROTOCOL_TO_CLI (i4LeaveAllTimerVal);

        CliPrintf (CliHandle, "%-9s", au1NameStr);
        CliPrintf (CliHandle, "%-15d", i4JoinTimerVal);
        CliPrintf (CliHandle, "%-16d", i4LeaveTimerVal);
        u4PagingStatus = CliPrintf (CliHandle, "%-d\r\n", i4LeaveAllTimerVal);
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = FALSE;
        }

        i4CurrentIndex = i4NextIndex;

        if (GarpGetNextPortInContext (u4ContextId, (UINT4) i4CurrentIndex,
                                      (UINT4 *) &i4NextIndex) == GARP_FAILURE)
        {
            u1isShowAll = FALSE;
        }

    }
    while (u1isShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : ShowGmrpStatistics                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays current GMRP Statistics     */
/*                                                                           */
/*     INPUT            : u4ContextId - Context identifier                   */
/*                          u4PortId - Port identifier                       */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowGmrpStatistics (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId)
{

    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;
    INT4                i4CurrentIndex = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4GmrpJoinEmptyTxCount = 0;
    UINT4               u4GmrpJoinEmptyRxCount = 0;
    UINT4               u4GmrpJoinInTxCount = 0;
    UINT4               u4GmrpJoinInRxCount = 0;
    UINT4               u4GmrpLeaveInTxCount = 0;
    UINT4               u4GmrpLeaveInRxCount = 0;
    UINT4               u4GmrpLeaveEmptyTxCount = 0;
    UINT4               u4GmrpLeaveEmptyRxCount = 0;
    UINT4               u4GmrpEmptyTxCount = 0;
    UINT4               u4GmrpEmptyRxCount = 0;
    UINT4               u4GmrpLeaveAllTxCount = 0;
    UINT4               u4GmrpLeaveAllRxCount = 0;
    UINT4               u4GmrpRxCount;
    UINT4               u4GmrpTxCount;
    UINT4               u4GmrpDiscardCount;
    UINT4               u4GarpContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        CliPrintf (CliHandle, "\r%% GARP is shutdown\r\n");
        return CLI_SUCCESS;
    }

    if (GarpGetFirstPortInContext (u4ContextId, (UINT4 *) &i4NextIndex) ==
        GARP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    /* Check if GARP Statistics are accumulated. If pGarpPortStats is allocated memory for second port
     * then GARP Stats are being accumulated */

    GarpGetNextPortInContext (u4ContextId, (UINT4) i4CurrentIndex,
                              (UINT4 *) &i4NextIndex);

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4NextIndex,
                                       &u4GarpContextId,
                                       &u2LocalPort) != GARP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (GarpSelectContext (u4GarpContextId) != GARP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (u2LocalPort);

    if (pGlobalPortEntry->pGarpPortStats == NULL)
    {
        CliPrintf (CliHandle, "\r%% GARP Statistics not accumulated\r\n");
        GarpReleaseContext ();
        return CLI_SUCCESS;
    }

    do 
    {
        if (u4PortId != 0)
        {
            if (nmhValidateIndexInstanceFsDot1dPortGarpTable
                ((INT4) u4PortId) == SNMP_FAILURE)
            { 
                GarpReleaseContext ();
                return CLI_FAILURE;
            }
            i4NextIndex = (INT4) u4PortId;
            u1isShowAll = FALSE;
        }

        CLI_MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH); 
        /* base port index */
        GarpCfaCliGetIfName ((UINT4) i4NextIndex, (INT1 *) au1NameStr);

        nmhGetFsMIDot1qFutureVlanPortGmrpJoinEmptyTxCount (i4NextIndex,
                                                           &u4GmrpJoinEmptyTxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpJoinEmptyRxCount (i4NextIndex,
                                                           &u4GmrpJoinEmptyRxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpJoinInTxCount (i4NextIndex,
                                                        &u4GmrpJoinInTxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpJoinInRxCount (i4NextIndex,
                                                        &u4GmrpJoinInRxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpLeaveInTxCount (i4NextIndex,
                                                         &u4GmrpLeaveInTxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpLeaveInRxCount (i4NextIndex,
                                                         &u4GmrpLeaveInRxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCount (i4NextIndex,
                                                            &u4GmrpLeaveEmptyTxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCount (i4NextIndex,
                                                            &u4GmrpLeaveEmptyRxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpEmptyTxCount (i4NextIndex,
                                                       &u4GmrpEmptyTxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpEmptyRxCount (i4NextIndex,
                                                       &u4GmrpEmptyRxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpLeaveAllTxCount (i4NextIndex,
                                                          &u4GmrpLeaveAllTxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpLeaveAllRxCount (i4NextIndex,
                                                          &u4GmrpLeaveAllRxCount);

        nmhGetFsMIDot1qFutureVlanPortGmrpDiscardCount (i4NextIndex,
                                                       &u4GmrpDiscardCount);

        u4GmrpRxCount =
            u4GmrpJoinEmptyRxCount + u4GmrpJoinInRxCount + u4GmrpLeaveInRxCount +
            u4GmrpLeaveAllRxCount + u4GmrpLeaveEmptyRxCount + u4GmrpEmptyRxCount;
        u4GmrpTxCount =
            u4GmrpJoinEmptyTxCount + u4GmrpJoinInTxCount + u4GmrpLeaveInTxCount +
            u4GmrpLeaveAllTxCount + u4GmrpLeaveEmptyTxCount + u4GmrpEmptyTxCount;

        CliPrintf (CliHandle, "\r\nGMRP Statistics for Port %-9s\r\n", au1NameStr);

        CliPrintf (CliHandle, "---------------------------------------\r\n");

        CliPrintf (CliHandle, "Total valid GMRP Packets Received %d:\r\n",
               u4GmrpRxCount);

        CliPrintf (CliHandle, "Join Emptys             %d\r\n",
               u4GmrpJoinEmptyRxCount);

        CliPrintf (CliHandle, "Join In                 %d\r\n",
               u4GmrpJoinInRxCount);

        CliPrintf (CliHandle, "Leave In                %d\r\n",
               u4GmrpLeaveInRxCount);

        CliPrintf (CliHandle, "Leave All               %d\r\n",
               u4GmrpLeaveAllRxCount);

        CliPrintf (CliHandle, "Leave Empty             %d\r\n",
               u4GmrpLeaveEmptyRxCount);

        CliPrintf (CliHandle, "Empty                   %d\r\n", u4GmrpEmptyRxCount);

        CliPrintf (CliHandle, "Total valid GMRP Packets Transmitted:%d \r\n",
               u4GmrpTxCount);

        CliPrintf (CliHandle, "Join Emptys             %d\r\n",
               u4GmrpJoinEmptyTxCount);

        CliPrintf (CliHandle, "Join In                 %d\r\n",
               u4GmrpJoinInTxCount);

        CliPrintf (CliHandle, "Leave In                %d\r\n",
               u4GmrpLeaveInTxCount);

        CliPrintf (CliHandle, "Leave All               %d\r\n",
               u4GmrpLeaveAllTxCount);

        CliPrintf (CliHandle, "Leave Empty             %d\r\n",
               u4GmrpLeaveEmptyTxCount);

        CliPrintf (CliHandle, "Empty                   %d\r\n", u4GmrpEmptyTxCount);

        CliPrintf (CliHandle, "\r\n");

        i4CurrentIndex = i4NextIndex;

        if (GarpGetNextPortInContext (u4ContextId, (UINT4) i4CurrentIndex,
                                          (UINT4 *) &i4NextIndex)
                == GARP_FAILURE)
        {
            u1isShowAll = FALSE;
        } 

    }
    while(u1isShowAll);

    GarpReleaseContext ();
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : ShowGvrpStatistics                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays current GVRP Statistics     */
/*                                                                           */
/*     INPUT            : u4ContextId - Context identifier                   */
/*                          u4PortId - Port identifier                       */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ShowGvrpStatistics (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId)
{

    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;
    INT4                i4CurrentIndex = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4GvrpJoinEmptyTxCount = 0;
    UINT4               u4GvrpJoinEmptyRxCount = 0;
    UINT4               u4GvrpJoinInTxCount = 0;
    UINT4               u4GvrpJoinInRxCount = 0;
    UINT4               u4GvrpLeaveInTxCount = 0;
    UINT4               u4GvrpLeaveInRxCount = 0;
    UINT4               u4GvrpLeaveEmptyTxCount = 0;
    UINT4               u4GvrpLeaveEmptyRxCount = 0;
    UINT4               u4GvrpEmptyTxCount = 0;
    UINT4               u4GvrpEmptyRxCount = 0;
    UINT4               u4GvrpLeaveAllTxCount = 0;
    UINT4               u4GvrpLeaveAllRxCount = 0;
    UINT4               u4GvrpDiscardCount;
    UINT4               u4GvrpRxCount;
    UINT4               u4GvrpTxCount;
    UINT4               u4GarpContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        CliPrintf (CliHandle, "\r%% GARP is shutdown\r\n");
        return CLI_SUCCESS;
    }

    if (GarpGetFirstPortInContext (u4ContextId, (UINT4 *) &i4NextIndex) ==
        GARP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    /* Check if GARP Statistics are accumulated. If pGarpPortStats is allocated memory for second port
     * then GARP Stats are being accumulated */

    GarpGetNextPortInContext (u4ContextId, (UINT4) i4CurrentIndex,
                              (UINT4 *) &i4NextIndex);

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4NextIndex,
                                       &u4GarpContextId,
                                       &u2LocalPort) != GARP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (GarpSelectContext (u4GarpContextId) != GARP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (u2LocalPort);

    if (pGlobalPortEntry->pGarpPortStats == NULL)
    {
        CliPrintf (CliHandle, "\r%% GARP Statistics not accumulated\r\n");
        GarpReleaseContext ();
        return CLI_SUCCESS;
    }
   
    do 
    {
        if (u4PortId != 0)
        {
            if (nmhValidateIndexInstanceFsDot1dPortGarpTable
                ((INT4) u4PortId) == SNMP_FAILURE)
            {
                GarpReleaseContext ();
                return CLI_FAILURE;
            }
            i4NextIndex = (INT4) u4PortId;
            u1isShowAll = FALSE;
        }

        CLI_MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
        /* base port index */
        GarpCfaCliGetIfName ((UINT4) i4NextIndex, (INT1 *) au1NameStr);

        nmhGetFsMIDot1qFutureVlanPortGvrpJoinEmptyTxCount (i4NextIndex,
                                                           &u4GvrpJoinEmptyTxCount);

        nmhGetFsMIDot1qFutureVlanPortGvrpJoinEmptyRxCount (i4NextIndex,
                                                           &u4GvrpJoinEmptyRxCount);

        nmhGetFsMIDot1qFutureVlanPortGvrpJoinInTxCount (i4NextIndex,
                                                        &u4GvrpJoinInTxCount);

        nmhGetFsMIDot1qFutureVlanPortGvrpJoinInRxCount (i4NextIndex,
                                                        &u4GvrpJoinInRxCount);

        nmhGetFsMIDot1qFutureVlanPortGvrpLeaveInTxCount (i4NextIndex,
                                                         &u4GvrpLeaveInTxCount);

        nmhGetFsMIDot1qFutureVlanPortGvrpLeaveInRxCount (i4NextIndex,
                                                         &u4GvrpLeaveInRxCount);

        nmhGetFsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCount (i4NextIndex,
                                                            &u4GvrpLeaveEmptyTxCount);

        nmhGetFsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCount (i4NextIndex,
                                                            &u4GvrpLeaveEmptyRxCount);

        nmhGetFsMIDot1qFutureVlanPortGvrpEmptyTxCount (i4NextIndex,
                                                       &u4GvrpEmptyTxCount);

        nmhGetFsMIDot1qFutureVlanPortGvrpEmptyRxCount (i4NextIndex,
                                                       &u4GvrpEmptyRxCount);

        nmhGetFsMIDot1qFutureVlanPortGvrpLeaveAllTxCount (i4NextIndex,
                                                          &u4GvrpLeaveAllTxCount);

        nmhGetFsMIDot1qFutureVlanPortGvrpLeaveAllRxCount (i4NextIndex,
                                                          &u4GvrpLeaveAllRxCount);

       nmhGetFsMIDot1qFutureVlanPortGvrpDiscardCount (i4NextIndex,
                                                      &u4GvrpDiscardCount);

       u4GvrpRxCount =
            u4GvrpJoinEmptyRxCount + u4GvrpJoinInRxCount + u4GvrpLeaveInRxCount +
            u4GvrpLeaveAllRxCount + u4GvrpLeaveEmptyRxCount + u4GvrpEmptyRxCount;
       u4GvrpTxCount =
            u4GvrpJoinEmptyTxCount + u4GvrpJoinInTxCount + u4GvrpLeaveInTxCount +
            u4GvrpLeaveAllTxCount + u4GvrpLeaveEmptyTxCount + u4GvrpEmptyTxCount;

       CliPrintf (CliHandle, "\r\nGVRP Statistics for Port %-9s\r\n", au1NameStr);

       CliPrintf (CliHandle, "---------------------------------------\r\n");

       CliPrintf (CliHandle, "Total valid GVRP Packets Received:  %d\r\n",
               u4GvrpRxCount);

       CliPrintf (CliHandle, "Join Emptys             %d\r\n",
               u4GvrpJoinEmptyRxCount);

       CliPrintf (CliHandle, "Join In                 %d\r\n",
               u4GvrpJoinInRxCount);

       CliPrintf (CliHandle, "Leave In                %d\r\n",
               u4GvrpLeaveInRxCount);

       CliPrintf (CliHandle, "Leave All               %d\r\n",
               u4GvrpLeaveAllRxCount);

       CliPrintf (CliHandle, "Leave Empty             %d\r\n",
               u4GvrpLeaveEmptyRxCount);

       CliPrintf (CliHandle, "Empty                   %d\r\n", u4GvrpEmptyRxCount);

       CliPrintf (CliHandle, "Total valid GVRP Packets Transmitted: %d\r\n",
               u4GvrpTxCount);

       CliPrintf (CliHandle, "Join Emptys             %d\r\n",
               u4GvrpJoinEmptyTxCount);

       CliPrintf (CliHandle, "Join In                 %d\r\n",
               u4GvrpJoinInTxCount);

       CliPrintf (CliHandle, "Leave In                %d\r\n",
               u4GvrpLeaveInTxCount);

       CliPrintf (CliHandle, "Leave All               %d\r\n",
               u4GvrpLeaveAllTxCount);

       CliPrintf (CliHandle, "Leave Empty             %d\r\n",
               u4GvrpLeaveEmptyTxCount);

       CliPrintf (CliHandle, "Empty                   %d\r\n", u4GvrpEmptyTxCount);

       CliPrintf (CliHandle, "\r\n");
       
       i4CurrentIndex = i4NextIndex;

       if (GarpGetNextPortInContext (u4ContextId, (UINT4) i4CurrentIndex,
                                          (UINT4 *) &i4NextIndex)
                == GARP_FAILURE)
       {
            u1isShowAll = FALSE;
       }

    }
    while(u1isShowAll);

    GarpReleaseContext ();
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : GarpShowRunningConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays current garp configuration  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
GarpShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT1               u1HeadFlag = VLAN_FALSE;
    CliRegisterLock (CliHandle, GarpLock, GarpUnLock);
    GARP_LOCK ();


    if (GarpShowRunningConfigScalars (CliHandle, u4ContextId, &u1HeadFlag) == CLI_SUCCESS)
    {
        GarpShowRunningConfigTables (CliHandle, u4ContextId);
	if (u1HeadFlag == VLAN_TRUE)
	{
                CliPrintf (CliHandle, "! \r\n");
	}
    }
    GARP_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : GarpShowRunningConfigScalars                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays current garp configuration  */
/*                        for Scalars objects                                */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - Context Identifier                   */
/*                        pu1HeadFlag - Flag to indicate whether switch      */
/*                                      mode is entered or not               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
GarpShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4ContextId, 
                              UINT1 *pu1HeadFlag)
{
    INT4               u4SysMode = 0;
    UINT1               au1ContextName[GARP_SWITCH_ALIAS_LEN + 4];
    INT4                i4RetValGvrp = 0;
    INT4                i4RetValGmrp = 0;
    INT4                i4RetValGarpShutdown = 0;
    INT1                i1StatusGvrp = 0;
    INT1                i1StatusGmrp= 0;
    INT1                i1StatusGarpShutdown = 0;

    u4SysMode = GarpVcmGetSystemModeExt (GARP_PROTOCOL_ID);
    /* These command are not applicable for PBB mode and thus should not
     * be shown as part of PBB bridge.
     */

    i1StatusGvrp = nmhGetFsDot1qGvrpStatus ((INT4)u4ContextId, &i4RetValGvrp);
    i1StatusGmrp = nmhGetFsDot1dGmrpStatus ((INT4)u4ContextId, &i4RetValGmrp);
    i1StatusGarpShutdown = nmhGetFsMIDot1qFutureGarpShutdownStatus ((INT4)u4ContextId, &i4RetValGarpShutdown);
    if ((u4SysMode == VCM_MI_MODE) && (*pu1HeadFlag == VLAN_FALSE))
    {

    	if((((i1StatusGvrp == SNMP_SUCCESS) && (i4RetValGvrp != GVRP_ENABLED) && (u4ContextId == GARP_DEFAULT_CONTEXT_ID)) 
		|| ((i4RetValGvrp != GVRP_DISABLED) && (u4ContextId != GARP_DEFAULT_CONTEXT_ID))) ||
		(((i1StatusGmrp == SNMP_SUCCESS) && (i4RetValGmrp != GMRP_ENABLED) && (u4ContextId == GARP_DEFAULT_CONTEXT_ID)) 
		|| ((i4RetValGmrp != GMRP_DISABLED) && (u4ContextId != GARP_DEFAULT_CONTEXT_ID))) ||
		(((i1StatusGarpShutdown == SNMP_SUCCESS) && (i4RetValGarpShutdown == GARP_SNMP_TRUE) && 
                  (u4ContextId == GARP_DEFAULT_CONTEXT_ID)) ||
		 ((i4RetValGarpShutdown == GARP_SNMP_FALSE) && (u4ContextId != GARP_DEFAULT_CONTEXT_ID))))
		{
        		MEMSET (au1ContextName, 0, (GARP_SWITCH_ALIAS_LEN + 4));

        		GarpVcmGetAliasName (u4ContextId, au1ContextName);
        		if (STRLEN (au1ContextName) != 0)
        		{
        			CliPrintf (CliHandle, "\rswitch  %s \r\n", au1ContextName);
				*pu1HeadFlag = VLAN_TRUE;
    			}
    		}
    		else
    		{
			return CLI_SUCCESS;
    		}
    }
		
    if ((i1StatusGvrp == SNMP_SUCCESS) && (i4RetValGvrp != GVRP_ENABLED))
    {
	if ((u4ContextId == GARP_DEFAULT_CONTEXT_ID) && ((i4RetValGarpShutdown != GARP_SNMP_TRUE)))
	{
        	CliPrintf (CliHandle, "set gvrp disable\r\n");
	}
    }
    else
    {
        if (u4ContextId != GARP_DEFAULT_CONTEXT_ID)
        {
            CliPrintf (CliHandle, "set gvrp enable\r\n");
        }
    }

    if ((i1StatusGmrp == SNMP_SUCCESS) && (i4RetValGmrp != GMRP_ENABLED))
    {
        if ((u4ContextId == GARP_DEFAULT_CONTEXT_ID) && (i4RetValGarpShutdown != GARP_SNMP_TRUE))
        {
        	CliPrintf (CliHandle, "set gmrp disable\r\n");
	}
    }
    else
    {
        if (u4ContextId != GARP_DEFAULT_CONTEXT_ID)
        {
            CliPrintf (CliHandle, "set gmrp enable\r\n");
        }
    }

    if ((i1StatusGarpShutdown == SNMP_SUCCESS) && (i4RetValGarpShutdown == GARP_SNMP_TRUE))
    {
        if (u4ContextId == GARP_DEFAULT_CONTEXT_ID)
        {
        	CliPrintf (CliHandle, "shutdown garp\r\n");
    	}
    }
    else
    {
        if (u4ContextId != GARP_DEFAULT_CONTEXT_ID)
        {
            CliPrintf (CliHandle, "no shutdown garp\r\n");
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : GarpShowRunningConfigInterfaceDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays current garp configuration  */
/*                        for Interface                                      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - context id
 *                        i4LocalPort - Local port of a particular context   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
GarpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                                       INT4 i4LocalPort, UINT1 *pu1HeadFlag)
{
    INT4                i4RetValGarpJoinTime = 0;
    INT4                i4RetValGarpLeaveTime = 0;
    INT4                i4RetValGarpLeaveAllTime = 0;
    INT4                i4RetValVlanRegistration = 0;
    INT4                i4RetValGroupRegistration = 0;
    INT1                i1StatusGarpJoinTime = 0;
    INT1                i1StatusGarpLeaveTime = 0;
    INT1                i1StatusGarpLeaveAllTime = 0;
    INT1                i1StatusVlanRegistration = 0;
    INT1                i1StatusGroupRegistration = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    CliRegisterLock (CliHandle, GarpLockAndSelCliContext, GarpUnLock);
    GARP_LOCK ();
    if (GarpSelectContext (u4ContextId) == GARP_FAILURE)
    {
        GARP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    /* Store the selected virtual context in a global variable so that the same
     * can be restored after doing VLAN-LOCK in CliPrintf
     * */

    gu4GarpCliContext = u4ContextId;
    if (nmhValidateIndexInstanceDot1dPortGarpTable (i4LocalPort) ==
        SNMP_SUCCESS)
    {
        i1StatusGarpJoinTime = nmhGetDot1dPortGarpJoinTime (i4LocalPort, &i4RetValGarpJoinTime);
        i1StatusGarpLeaveTime = nmhGetDot1dPortGarpLeaveTime (i4LocalPort, &i4RetValGarpLeaveTime);
        i1StatusGarpLeaveAllTime = nmhGetDot1dPortGarpLeaveAllTime (i4LocalPort, &i4RetValGarpLeaveAllTime);
        i1StatusVlanRegistration =
            nmhGetDot1qPortRestrictedVlanRegistration (i4LocalPort, &i4RetValVlanRegistration);
        i1StatusGroupRegistration =
            nmhGetDot1dPortRestrictedGroupRegistration (i4LocalPort, &i4RetValGroupRegistration);

        if (*pu1HeadFlag == VLAN_FALSE)
        {
            if (((i1StatusGarpJoinTime == SNMP_SUCCESS) && (i4RetValGarpJoinTime != GARP_DEF_JOIN_TIME)) ||
		    ((i1StatusGarpLeaveTime == SNMP_SUCCESS) && (i4RetValGarpLeaveTime != GARP_DEF_LEAVE_TIME)) ||
		    ((i1StatusGarpLeaveAllTime == SNMP_SUCCESS) && (i4RetValGarpLeaveAllTime != GARP_DEF_LEAVE_ALL_TIME)) ||
		    ((i1StatusVlanRegistration == SNMP_SUCCESS) && (i4RetValVlanRegistration != GVRP_DISABLED)) ||
		    ((i1StatusGroupRegistration == SNMP_SUCCESS) && (i4RetValGroupRegistration != GMRP_DISABLED)))
            {
                VlanCfaCliConfGetIfName (GARP_GET_IFINDEX(i4LocalPort), (INT1 *) au1NameStr);

                CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);
        	    *pu1HeadFlag = VLAN_TRUE;
	        }
        }
        
	if ((i1StatusGarpJoinTime == SNMP_SUCCESS) && (i4RetValGarpJoinTime != GARP_DEF_JOIN_TIME))
        {
            i4RetValGarpJoinTime = VLAN_GARP_TIMER_PROTOCOL_TO_CLI (i4RetValGarpJoinTime);
            CliPrintf (CliHandle, "set garp timer join %d\r\n", i4RetValGarpJoinTime);
        }

        if ((i1StatusGarpLeaveTime == SNMP_SUCCESS) && (i4RetValGarpLeaveTime != GARP_DEF_LEAVE_TIME))
        {
            i4RetValGarpLeaveTime = VLAN_GARP_TIMER_PROTOCOL_TO_CLI (i4RetValGarpLeaveTime);
            CliPrintf (CliHandle, "set garp timer leave %d\r\n", i4RetValGarpLeaveTime);
        }

        if ((i1StatusGarpLeaveAllTime == SNMP_SUCCESS) && (i4RetValGarpLeaveAllTime != GARP_DEF_LEAVE_ALL_TIME))
        {
            i4RetValGarpLeaveAllTime = VLAN_GARP_TIMER_PROTOCOL_TO_CLI (i4RetValGarpLeaveAllTime);
            CliPrintf (CliHandle, "set garp timer leaveall %d\r\n", i4RetValGarpLeaveAllTime);
        }

        if ((i1StatusVlanRegistration == SNMP_SUCCESS) && (i4RetValVlanRegistration != GVRP_DISABLED))
        {

            CliPrintf (CliHandle, "vlan restricted enable\r\n");
        }

        if ((i1StatusGroupRegistration == SNMP_SUCCESS) && (i4RetValGroupRegistration != GMRP_DISABLED))
        {

            CliPrintf (CliHandle, "group restricted enable\r\n");
        }
    }
    GarpReleaseContext ();
    gu4GarpCliContext = 0;
    GARP_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : GarpShowRunningConfigTables                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays current garp configuration  */
/*                        for Tables                                         */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
GarpShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4RetValGvrp = 0;
    INT4                i4RetValGmrp = 0;
    INT4                i4RetVal = 0;
    INT4                i4Status;
    UINT1               u1isShowAll = TRUE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               i4NextIndex;
    UINT4               i4CurrentIndex;
    UINT4               u4PagingStatus = CLI_SUCCESS;

    if (GarpGetFirstPortInContext (u4ContextId, (UINT4 *) &i4NextIndex)
        == GARP_SUCCESS)
    {
        do
        {
            CLI_MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
            i4Status = nmhGetFsDot1qPortGvrpStatus (i4NextIndex, &i4RetVal);
    	    nmhGetFsDot1qGvrpStatus ((INT4)u4ContextId, &i4RetValGvrp);

            if ((i4Status == SNMP_SUCCESS) && (i4RetVal != GVRP_ENABLED) && 
                (i4RetValGvrp == GVRP_ENABLED))
            {
                GarpCfaCliConfGetIfName (i4NextIndex, (INT1 *) au1NameStr);
                u4PagingStatus = CliPrintf (CliHandle,
                                            "set port gvrp %s disable\r\n",
                                            au1NameStr);
            }

            CLI_MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
            i4Status = nmhGetFsDot1dPortGmrpStatus (i4NextIndex, &i4RetVal);
    	    nmhGetFsDot1dGmrpStatus ((INT4)u4ContextId, &i4RetValGmrp);

            if ((i4Status == SNMP_SUCCESS) && (i4RetVal != GMRP_ENABLED) && (i4RetValGmrp == GMRP_ENABLED))
            {
                GarpCfaCliConfGetIfName (i4NextIndex, (INT1 *) au1NameStr);
                u4PagingStatus = CliPrintf (CliHandle,
                                            "set port gmrp %s disable\r\n",
                                            au1NameStr);
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            i4CurrentIndex = i4NextIndex;

            if (GarpGetNextPortInContext (u4ContextId, (UINT4) i4CurrentIndex,
                                          (UINT4 *) &i4NextIndex)
                == GARP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

        }
        while (u1isShowAll);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpGetNextActiveContext                         */
/*                                                                           */
/*    Description         : This function is used to get the next Active     */
/*                          context present in the system.                   */
/*                                                                           */
/*    Input(s)            : u4CurrContextId - Current Context Id.            */
/*                                                                           */
/*    Output(s)           : pu4NextContextId - Next Context Id.              */
/*                                                                           */
/*    Global Variables Referred : gapGarpContextInfo                         */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS/GARP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpGetNextActiveContext (UINT4 u4CurrContextId, UINT4 *pu4NextContextId)
{
    UINT4               u4ContextId;

    for (u4ContextId = u4CurrContextId + 1;
         u4ContextId < GARP_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        if (gapGarpContextInfo[u4ContextId] != NULL)
        {
            *pu4NextContextId = u4ContextId;
            return GARP_SUCCESS;
        }
    }
    return GARP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpGetFirstPortInContext                        */
/*                                                                           */
/*    Description         : This function is used to get the first port      */
/*                          mapped to this context.                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context identifier                 */
/*                                                                           */
/*    Output(s)           : pu4IfIndex - Interface Index.                    */
/*                                                                           */
/*    Global Variables Referred : gpGarpContextInfo->GarpContextPortInfo.    */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS/GARP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpGetFirstPortInContext (UINT4 u4ContextId, UINT4 *pu4IfIndex)
{
    tGarpGlobalPortEntry *pPortEntry = NULL;

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return GARP_FAILURE;
    }

    pPortEntry = (tGarpGlobalPortEntry *) RBTreeGetFirst
        (gpGarpContextInfo->GarpContextPortInfo);

    if (pPortEntry != NULL)
    {
        *pu4IfIndex = pPortEntry->u4IfIndex;
        GarpReleaseContext ();
        return GARP_SUCCESS;
    }
    GarpReleaseContext ();
    return GARP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpGetNextPortInContext                         */
/*                                                                           */
/*    Description         : This function is used to get the next port       */
/*                          mapped to this context.                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context identifier                 */
/*                          u4IfIndex - Current IfIndex.                     */
/*                                                                           */
/*    Output(s)           : pu4NextIfIndex - Next IfIndex.                   */
/*                                                                           */
/*    Global Variables Referred : gpGarpContextInfo->GarpContextPortInfo.    */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS/GARP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpGetNextPortInContext (UINT4 u4ContextId, UINT4 u4IfIndex,
                          UINT4 *pu4NextIfIndex)
{
    tGarpGlobalPortEntry *pPortEntry = NULL;
    tGarpGlobalPortEntry Dummy;

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return GARP_FAILURE;
    }

    Dummy.u4IfIndex = u4IfIndex;

    pPortEntry = RBTreeGetNext (gpGarpContextInfo->GarpContextPortInfo,
                                &Dummy, NULL);
    if (pPortEntry == NULL)
    {
        GarpReleaseContext ();
        return GARP_FAILURE;
    }

    *pu4NextIfIndex = pPortEntry->u4IfIndex;
    GarpReleaseContext ();
    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpCliSelectContextOnMode                       */
/*                                                                           */
/*    Description         : This function is used to check the Mode of       */
/*                          the command and also if it a Config Mode         */
/*                          command it will do SelectContext for the         */
/*                          Context.                                         */
/*                                                                           */
/*    Input(s)            : u4Cmd - CLI Command.                             */
/*                                                                           */
/*    Output(s)           : CliHandle - Contains error messages.             */
/*                          pu4ContextId - Context Id.                       */
/*                          pu2LocalPort - Local Port Number.                */
/*                          pu2Flag      - Show command or Not.              */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS/GARP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
GarpCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                            UINT4 *pu4Context, UINT2 *pu2LocalPort)
{
    UINT4               u4ContextId;
    INT4                i4PortId = 0;
    UINT1               u1IntfCmdFlag = GARP_FALSE;    /* This flag is used in MI 
                                                       case, to know whether 
                                                       the command is an
                                                       a interface mode 
                                                       command or garp mode 
                                                       command (for future 
                                                       reference). */

    /* For debug commands the context-name will be present in args[0], so 
     * the select context will be done seperately within the 
     * switch statement*/
    if ((u4Cmd == CLI_GARP_GBL_DEBUG) || (u4Cmd == CLI_GARP_NO_GBL_DEBUG) ||
        (u4Cmd == CLI_GARP_DEBUGS) || (u4Cmd == CLI_GARP_NO_DEBUGS))
    {
        return GARP_SUCCESS;
    }

    *pu4Context = GARP_DEFAULT_CONTEXT_ID;

    /* Get the Context-Id from the pCliContext structure */
    u4ContextId = CLI_GET_CXT_ID ();

    /* Check if the command is a switch mode command */
    if (u4ContextId != GARP_CLI_INVALID_CONTEXT)
    {
        /* Switch-mode Command */
        *pu4Context = u4ContextId;
    }
    else
    {
        /* This flag (u1IntfCmdFlag) is used only in case of MI.
         * If the command is a garp mode command (future reference)then the 
         * context-id won't be GARP_CLI_INVALID_CONTEXT, So this is an 
         * interface mode command. Now by refering this flag we have to 
         * get the context-id and local port number from the 
         * IfIndex (CLI_GET_IFINDEX). */
        u1IntfCmdFlag = GARP_TRUE;
    }

    i4PortId = CLI_GET_IFINDEX ();

    /* In SI both the i4PortId and Localport are same. So no need to call
     * GarpVcmGetContextInfoFromIfIndex. */
    *pu2LocalPort = (UINT2) i4PortId;

    if (GarpVcmGetSystemMode (GARP_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (u1IntfCmdFlag == GARP_TRUE)
        {
            /* This is an Interface Mode command
             * Get the context Id from the IfIndex */
            if (i4PortId != -1)
            {
                if (GarpVcmGetContextInfoFromIfIndex
                    ((UINT2) i4PortId, pu4Context, pu2LocalPort) != VCM_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    return GARP_FAILURE;
                }
                if (GARP_IS_GARP_ENABLED_IN_CONTEXT (*pu4Context) != GARP_TRUE)
                {
                    CliPrintf (CliHandle, "\r%% GARP is shutdown\r\n ");
                    return GARP_FAILURE;
                }
            }
        }
    }

    /* Since we are calling SI nmh routine for Configuration commands
     * we have to do SelectContext for Config commands*/
    if (GarpSelectContext (*pu4Context) != GARP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
        return GARP_FAILURE;
    }

    if ((GarpVlanGetStartedStatus (GARP_CURR_CONTEXT_PTR ()->u4ContextId)
         == VLAN_FALSE) && (u4Cmd != CLI_GARP_SHUT_GARP))
    {
        CliPrintf (CliHandle, "\r%% VLAN switching is shutdown\r\n");
        GarpReleaseContext ();
        return GARP_FAILURE;
    }

    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpCliGetContextForShowCmd                      */
/*                                                                           */
/*    Description         : This function is called only when the show       */
/*                          command is given. It handles 3 different         */
/*                          ways of show commands.                           */
/*                            1. If switch name is given, we have to get     */
/*                               the context Id from it.                     */
/*                            2. If Interface Index is present, from that    */
/*                               we have to get the context Id.              */
/*                            3. Else we have to go for each and every       */
/*                               context.                                    */
/*                          For the above all thing we have to check for     */
/*                          the Module status of the context.                */
/*                                                                           */
/*    Input(s)            : u4CurrContext    - Context-Id.                   */
/*                          pu1Name          - Switch-Name.                  */
/*                          u4IfIndex        - Interface Index.              */
/*                                                                           */
/*    Output(s)           : CliHandle        - Contains error messages.      */
/*                          pu4NextContextId - Context Id.                   */
/*                          pu2LocalPort     - Local Port Number.            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS/GARP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpCliGetContextForShowCmd (tCliHandle CliHandle, UINT1 *pu1Name,
                             UINT4 u4IfIndex, UINT4 u4CurrContext,
                             UINT4 *pu4NextContext, UINT2 *pu2LocalPort)
{
    UINT4               u4ContextId;
    UINT1               au1ContextName[GARP_SWITCH_ALIAS_LEN + 4];

    *pu2LocalPort = 0;

    /* If Switch-name is given then get the Context-Id from it */
    if (pu1Name != NULL)
    {
        if (GarpVcmIsSwitchExist (pu1Name, &u4ContextId) != VCM_TRUE)
        {
            CliPrintf (CliHandle, "Switch %s Does not exist.\r\n", pu1Name);
            return GARP_FAILURE;
        }
    }
    /* If IfIndex is given then get the Context-Id from it */
    else if (u4IfIndex != 0)
    {
        if (GarpVcmGetContextInfoFromIfIndex ((UINT2) u4IfIndex, &u4ContextId,
                                              pu2LocalPort) != VCM_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Interface not mapped to "
                       "any of the context.\r\n ");
            return GARP_FAILURE;
        }
    }
    else
    {
        /* Case 1: At first entry for this funtion, If Switch-name is not given, 
           then get the first active Context. For this if we give 
           0xffffffff as current context for GarpGetNextActiveContext 
           function it will return the First Active context. 
           Case 2: At the Next frequent entries it will as per we want. */
        if (GarpGetNextActiveContext (u4CurrContext,
                                      &u4ContextId) != GARP_SUCCESS)
        {
            return GARP_FAILURE;
        }
    }

    do
    {
        u4CurrContext = u4ContextId;

        /* To avoid "Switch <switch-name>" print in SI */
        if (GarpVcmGetSystemModeExt (GARP_PROTOCOL_ID) == VCM_MI_MODE)
        {
            /* if switch name is not given we have to get the switch-name 
             * from context-id for Display purpose */
            CLI_MEMSET (au1ContextName, 0, (GARP_SWITCH_ALIAS_LEN + 4));

            if (pu1Name == NULL)
            {
                GarpVcmGetAliasName (u4ContextId, au1ContextName);
            }
            else
            {
                CLI_MEMCPY (au1ContextName, pu1Name, STRLEN (pu1Name));
            }
            CliPrintf (CliHandle, "\r\n\rSwitch %s\r\n", au1ContextName);
        }

        if (GarpVlanGetStartedStatus (u4ContextId) == VLAN_FALSE)
        {
            CliPrintf (CliHandle, "\r\n\r%% VLAN switching is shutdown\r\n");
            if ((pu1Name != NULL) || (u4IfIndex != 0))
            {
                /* We have to come out of the loop when the switch is 
                 * shutdown, if the command is for specific switch else
                 * continue the Loop. */
                break;
            }
            continue;
        }
        *pu4NextContext = u4ContextId;
        return GARP_SUCCESS;
    }
    while (GarpGetNextActiveContext (u4CurrContext,
                                     &u4ContextId) == GARP_SUCCESS);

    return GARP_FAILURE;
}

#ifdef TRACE_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : GarpSetDebugs                                      */
/*                                                                           */
/*     DESCRIPTION      : This function configures/deconfigures debug level  */
/*                                                                           */
/*     INPUT            : u4DbgMod   - Submodule for which Trace is modified */
/*                        u4DbgValue - Type of the Trace that is modified    */
/*                        u1Action   - Set/Re-set action                     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpSetDebugs (tCliHandle CliHandle, UINT4 u4DbgMod, UINT4 u4DbgValue,
               UINT1 u1Action)
{
    UINT4               u4CurDbgValue;
    UINT4               u4NewDbgValue = 0;
    UINT4               u4NewDbgType;
    UINT4               u4ErrCode;

    if (nmhGetDot1qFutureGarpDebug ((INT4 *) &u4CurDbgValue) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (u1Action == GARP_SET_CMD)
    {
        u4NewDbgValue = u4DbgMod | u4DbgValue;
        u4NewDbgValue = u4NewDbgValue | u4CurDbgValue;
    }
    else
    {
        /* No debug command */
        if ((u4DbgMod & u4CurDbgValue) == 0)
        {
            /* Debug for the specific module was not enabled */
            return CLI_SUCCESS;
        }

        if (u4DbgValue == GARP_TRC_TYPE_ALL)
        {
            /* Resetting the bit corresponding to the specific module */
            u4NewDbgValue = u4CurDbgValue & (~u4DbgMod);
        }
        else
        {                        /* reset the trace value bits */
            u4NewDbgType = u4CurDbgValue & 0x0000FFFF;
            u4NewDbgType = ~u4DbgValue & u4NewDbgType;

            u4NewDbgType = 0xFFFF0000 | u4NewDbgType;
            u4NewDbgValue = u4CurDbgValue & u4NewDbgType;
        }
    }

    if (nmhTestv2Dot1qFutureGarpDebug (&u4ErrCode, (INT4) u4NewDbgValue)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetDot1qFutureGarpDebug ((INT4) u4NewDbgValue) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : GarpSetGlobalDebug                                 */
/*                                                                           */
/*     DESCRIPTION      : This function configures/deconfigures debug level  */
/*                                                                           */
/*     INPUT            : u1Action   - Set/Re-set action                     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpSetGlobalDebug (tCliHandle CliHandle, UINT1 u1Action)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsMIDot1qFutureGarpGlobalTrace (&u4ErrCode, u1Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIDot1qFutureGarpGlobalTrace (u1Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}
#endif /* TRACE_WANTED */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssGarpShowDebugging                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints the GARP debug level          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssGarpShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;
    INT4                i4GblDbgLevel = GARP_DISABLED;

    if (GarpSelectContext (L2IWF_DEFAULT_CONTEXT) != GARP_SUCCESS)
    {
        UNUSED_PARAM (CliHandle);
        return;
    }

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        UNUSED_PARAM (CliHandle);
        GarpReleaseContext ();
        return;
    }

    nmhGetDot1qFutureGarpDebug (&i4DbgLevel);
    nmhGetFsMIDot1qFutureGarpGlobalTrace (&i4GblDbgLevel);
    GarpReleaseContext ();

    if ((i4DbgLevel == 0) && (i4GblDbgLevel == GARP_DISABLED))
    {
        return;
    }
    CliPrintf (CliHandle, "\rGARP :");

    if (i4GblDbgLevel == GARP_ENABLED)
    {
        CliPrintf (CliHandle, "\r\n  GARP Global debugging is on");
    }
    if ((i4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GARP init and shutdown debugging is on");
    }
    if ((i4DbgLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GARP management debugging is on");
    }
    if ((i4DbgLevel & DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GARP data path debugging is on");
    }
    if ((i4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GARP control path debugging is on");
    }
    if ((i4DbgLevel & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GARP packet dump debugging is on");
    }
    if ((i4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GARP resources debugging is on");
    }
    if ((i4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GARP error debugging is on");
    }
    if ((i4DbgLevel & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GARP buffer debugging is on");
    }
    if ((i4DbgLevel & GARP_MOD_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GARP debugging is on");
    }
    if ((i4DbgLevel & GMRP_MOD_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GMRP debugging is on");
    }
    if ((i4DbgLevel & GVRP_MOD_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GVRP debugging is on");
    }
    if ((i4DbgLevel & GARP_RED_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  GARP redundancy is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : GarpLockAndSelCliContext                             */
/*                                                                           */
/* Description        : This function is used to take the GARP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread, this funtion also         */
/*                      restored the virtual context last selected           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
GarpLockAndSelCliContext (VOID)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    i4RetVal = GARP_LOCK ();
    if (GarpSelectContext (gu4GarpCliContext) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return i4RetVal;
}


 /*****************************************************************************/
 /*                                                                           */
/*     FUNCTION NAME    : GarpCliClearStatisticsPort                         */
/*                                                                           */
/*     DESCRIPTION      : This function clears garp counter values           */
/*                                                                           */
/*     INPUT            : u4Context    -  Context identifier                 */
/*                        u4PortId      - Port identifier                    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpCliClearStatisticsPort( tCliHandle CliHandle, UINT4 u4ContextId,
        UINT4 u4PortId)
{
    UINT4                   u4ErrorCode = 0;

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        CliPrintf (CliHandle, "\r%% GARP is shutdown\r\n");
        return CLI_SUCCESS;
    }

    if (GVRP_IS_GVRP_ENABLED_IN_CONTEXT (u4ContextId) == GVRP_FALSE)
    {
        CliPrintf (CliHandle, "\r%% GVRP is shutdown\r\n");
        return CLI_SUCCESS;
    }

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (u4ContextId) == GMRP_FALSE)
    {
        CliPrintf (CliHandle, "\r%% GMRP is shutdown\r\n");
        return CLI_SUCCESS;
    }
    if (nmhTestv2FsMIDot1qFutureVlanPortClearGarpStats (&u4ErrorCode, (INT4) u4PortId,
                                                        GARP_ENABLED) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIDot1qFutureVlanPortClearGarpStats ((INT4) u4PortId, GARP_ENABLED) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}



#endif
