
#include "garpinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbCheckPortTypeChange                        */
/*                                                                           */
/*    Description         : This function will be called when a port is      */
/*                          created in Garp. This function get the new port  */
/*                          type from L2IWF and updates it in Garp. If there */
/*                          is a port type change then it handles that.      */
/*                                                                           */
/*    Input(s)            : u2Port - Port that is newly created in Garp.     */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : GARP_SUCCESS / GARP_FAILURE                */
/*                                                                           */
/*****************************************************************************/
INT4
GarpPbCheckPortTypeChange (UINT2 u2Port)
{
    UINT1               u1PortType = GARP_PROVIDER_NETWORK_PORT;

    if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
    {
        /* Get the port type from L2Iwf. */
        if (GarpL2IwfGetPbPortType (GARP_GET_IFINDEX (u2Port), &u1PortType)
            == L2IWF_FAILURE)
        {
            /* Port type not found.  So take the default port type. */
            u1PortType = GARP_PROVIDER_NETWORK_PORT;
        }

        GARP_PB_PORT_TYPE (u2Port) = u1PortType;

        GarpPbHandlePortTypeChange (u2Port);

    }
    else
    {
        GARP_PB_PORT_TYPE (u2Port) = GARP_CUSTOMER_BRIDGE_PORT;
    }

    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbHandlePortTypeChange                       */
/*                                                                           */
/*    Description         : This function update the port Gvrp status based  */
/*                          PB port type.                                    */
/*                                                                           */
/*    Input(s)            : u2Port - Port that is newly created in Garp.     */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
GarpPbHandlePortTypeChange (UINT2 u2Port)
{
    /* GVRP will be disabled on CEP/CNP(Port-Based)/PCEP/PCNPs */
    if (GvrpPbIsPortValidForGvrpEnable (u2Port) == GVRP_FALSE)
    {
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status =
            GVRP_DISABLED;

        GarpL2IwfSetProtocolEnabledStatusOnPort (GARP_CURR_CONTEXT_ID (),
                                                 u2Port, L2_PROTO_GVRP,
                                                 OSIX_DISABLED);

    }
    else
    {
        /* Gvrp can be enabled on this port. */
        GARP_CURR_CONTEXT_PTR ()->aGvrpPortTable[u2Port].u1Status =
            GVRP_ENABLED;
        GarpL2IwfSetProtocolEnabledStatusOnPort (GARP_CURR_CONTEXT_ID (),
                                                 u2Port, L2_PROTO_GVRP,
                                                 OSIX_ENABLED);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbTranslateAttr                              */
/*                                                                           */
/*    Description         : This function translates the local vid to relay  */
/*                          vid and fills the pAttr accordingly.             */
/*                                                                           */
/*    Input(s)            : u2Port - Port on which the gvrp pkt is received  */
/*                          pAttr - Pointer to the  GarpAttribute structure  */
/*                          pvid  - vid value present in pAttr.              */
/*                                                                           */
/*    Output(s)           : pvid - Translated vid value.                     */
/*                                                                           */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Return the result of comparision.                 */
/*                         GARP_TRUE                                         */
/*                         GARP_FALSE                                        */
/*                                                                           */
/*****************************************************************************/
INT4
GarpPbTranslateAttr (UINT2 u2Port, tGarpAttr * pAttr, tVlanId * pVid)
{

    INT4                i4RetVal = L2IWF_SUCCESS;
    tVlanId             RelayVid;

    /* Get the translated value for given vid. */
    i4RetVal = GarpL2IwfPbGetRelayVidFromLocalVid (GARP_CURR_CONTEXT_ID (),
                                                   u2Port, *pVid, &RelayVid);

    if (i4RetVal == L2IWF_FAILURE)
    {
        return GARP_FAILURE;
    }

    /* Change the given value to relay vid. */
    *pVid = RelayVid;

    RelayVid = (tVlanId) OSIX_HTONS (RelayVid);

    MEMCPY (pAttr->au1AttrVal, &RelayVid, GVRP_VLAN_ID_LEN);

    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbIsPortOkForGarpPktRcv                      */
/*                                                                           */
/*    Description         : This function will be called by GarpProcessMsg   */
/*                          funtion. This fuction checks the garp pkt rvd on */
/*                          the given port can be accepeted or not. Gvrp pkts*/
/*                          received cannot be accepted on ports other than  */
/*                          PNP and PPNPs. When a customer gvrp pkt is rcvd  */
/*                          on a PPNP, its is dst addr will be changed to    */
/*                          to the current context gvrp addr.                */
/*                                                                           */
/*    Input(s)            : u2Port - Port over which the gvrp pkt is rcvd.   */
/*                          AppAddress - Dst addr of the rcvd gvrp pkt.      */
/*                                                                           */
/*    Output(s)           : AppAddress - Updated value to be used for        */
/*                                       further processing.                 */
/*                                                                           */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : GARP_TRUE if the pkt can be accepted       */
/*                                otherwise GARP_FALSE.                      */
/*                                                                           */
/*****************************************************************************/
INT4
GarpPbIsPortOkForGarpPktRcv (UINT2 u2Port, tMacAddr AppAddress)
{

    if (MEMCMP (AppAddress, gGmrpAddr, sizeof (tMacAddr)) == 0)
    {
        /* Gmrp is disabled on 1ad bridge. */
        return GARP_FALSE;
    }

    if ((GARP_PB_PORT_TYPE (u2Port) != GARP_PROVIDER_NETWORK_PORT) &&
        (GARP_PB_PORT_TYPE (u2Port) != GARP_PROP_PROVIDER_NETWORK_PORT))
    {
        /* As for all GVRP attributes the registration is fixed/forbidden 
         * on customer network ports, don't process the incoming gvrp 
         * packets on customer network ports. */

        /* Also don't process garp packets on CEP/PCEP/PCNP. */
        /* Even though gvrp is not enabled on CEP/PCNP/PCEP, it is better 
         * to check here, other wise it will print in the debug msg as port 
         * unknown for the application. */
        return GARP_FALSE;
    }

    if ((GARP_PB_PORT_TYPE (u2Port) == GARP_PROP_PROVIDER_NETWORK_PORT) &&
        (MEMCMP (AppAddress, gCustomerGvrpAddr, sizeof (tMacAddr)) == 0))
    {
        /* As propritary customer network ports are connected to Q-in-Q 
         * bridge, we have to process the GVRP packets received on this 
         * port with destination address as customer GVRP address in the 
         * S-VLAN component. */

        /* This we achieve by changing the AppAddress to gProviderGvrpAddr,
         * so the remaining thread thinks that a Gvrp packet is received 
         * with provider gvrp address on this port. */

        MEMCPY (AppAddress, GARP_CTXT_GVRP_ADDR (), sizeof (tMacAddr));

    }

    return GARP_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbEgrChangeGvrpAddrOnPpnp                    */
/*                                                                           */
/*    Description         : This function will be called by GarpTransmitPdu  */
/*                          funtion. This fuction replaces the dstaddr to    */
/*                          provider gvrp addr, if the port is PPNP and the  */
/*                          given DstAddr is customer gvrp address.          */
/*                                                                           */
/*    Input(s)            : u2Port - Port over which the gvrp pkt is to be   */
/*                                   sent.                                   */
/*                          DstAddr - Dst addr to be filled in the gvrp pkt. */
/*                                                                           */
/*    Output(s)           : DstAddr - Updated value to be filled in the gvrp */
/*                                    to be sent.                            */
/*                                                                           */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
GarpPbEgrChangeGvrpAddrOnPpnp (UINT2 u2Port, tMacAddr DstAddr)
{
    /* This function will be when a garp packet is to be transmitted on a port. 
     * Before transmitting a Gvrp packet on a PPNP, change the dst mac
     * address to customer gvrp mac. */
    if ((GARP_PB_PORT_TYPE (u2Port) == GARP_PROP_PROVIDER_NETWORK_PORT) &&
        (MEMCMP (DstAddr, GARP_CTXT_GVRP_ADDR (), sizeof (tMacAddr)) == 0))
    {
        MEMCPY (DstAddr, gCustomerGvrpAddr, sizeof (tMacAddr));
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPbTransAttrValAndCopy                        */
/*                                                                           */
/*    Description         : This function in case of gvrp, gets the local    */
/*                          vid from L2Iwf for the relay vid in pAttr and    */
/*                          copies the local vid in pu1Ptr.                  */
/*                                                                           */
/*    Input(s)            : u2Port - Port over which the gvrp pkt is to be   */
/*                                   sent.                                   */
/*                          pu1Ptr - buffer where the tranlated vid needs to */
/*                                   to be written.                          */
/*                          pAttr - Pointer to the  GarpAttribute structure  */
/*                                                                           */
/*    Output(s)           : pu1Ptr - In this buffer the translated vid is    */
/*                                   is written.                             */
/*                                                                           */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Return the result of comparision.                 */
/*                         GARP_TRUE                                         */
/*                         GARP_FALSE                                        */
/*                                                                           */
/*****************************************************************************/
VOID
GarpPbTransAttrValAndCopy (UINT2 u2Port, UINT1 *pu1Ptr, tGarpAttr * pAttr)
{
    INT4                i4RetVal = L2IWF_SUCCESS;
    tVlanId             RelayVid;
    tVlanId             LocalVid;

    /* Only in provider bridge attribute transaltion is supported.
     * In PPNP vid translation is not supported.
     * Translation is valid only for gvrp packet.
     * So check and act accordingly. */
    if ((GARP_PB_PORT_TYPE (u2Port) == GARP_PROP_PROVIDER_NETWORK_PORT) ||
        (pAttr->u1AttrType != GVRP_GROUP_ATTR_TYPE))
    {
        MEMCPY (pu1Ptr, pAttr->au1AttrVal, pAttr->u1AttrLen);
        return;
    }

    if (pAttr->u1AttrLen == GVRP_VLAN_ID_LEN)
    {
        MEMCPY (&RelayVid, pAttr->au1AttrVal, GVRP_VLAN_ID_LEN);
        RelayVid = (tVlanId) OSIX_NTOHS (RelayVid);

        /* Get the translated value for given vid. */
        i4RetVal = GarpL2IwfPbGetLocalVidFromRelayVid
            (GARP_CURR_CONTEXT_ID (), u2Port, RelayVid, &LocalVid);

        if (i4RetVal == L2IWF_SUCCESS)
        {
            /* Change the given value to relay vid. */
            LocalVid = (tVlanId) OSIX_HTONS (LocalVid);
            MEMCPY (pu1Ptr, &LocalVid, pAttr->u1AttrLen);
        }
        else
        {
            MEMCPY (pu1Ptr, pAttr->au1AttrVal, pAttr->u1AttrLen);
        }
        return;
    }

    MEMCPY (pu1Ptr, pAttr->au1AttrVal, pAttr->u1AttrLen);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GvrpPbIsPortValidForGvrpEnable                   */
/*                                                                           */
/*    Description         : This function checks whether gvrp can be enabled */
/*                          on the given port.                               */
/*                                                                           */
/*    Input(s)            : u2Port - Port Id.                                */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : Return GVRP_TRUE if gvrp can be enabled on */
/*                                the given  port otherwise GVRP_FALSE.      */
/*                                                                           */
/*****************************************************************************/
INT4
GvrpPbIsPortValidForGvrpEnable (UINT2 u2Port)
{
    UINT1               u1PbPortType;

    if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
    {
        u1PbPortType = (UINT1) GARP_PB_PORT_TYPE (u2Port);

        /* On CEP/CNP(Port-Based)/PCEP/PCNP Gvrp cannot be enabled. */
        if ((u1PbPortType != GARP_PROVIDER_NETWORK_PORT) &&
            (u1PbPortType != GARP_CNP_STAGGED_PORT) &&
            (u1PbPortType != GARP_PROP_PROVIDER_NETWORK_PORT))
        {
            return GVRP_FALSE;
        }
    }

    return GVRP_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpGetInstPortState                             */
/*                                                                           */
/*    Description         : This function will get the per instance port     */
/*                          state from the L2IWF module. In case of CEP it   */
/*                          returns forwarding                               */
/*                                                                           */
/*    Input(s)            : u2GipId- Instance Id.                            */
/*                          pGlobPortEntry- Global Port Entry                */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : AST_PORT_STATE_FORWARDING/                       */
/*                          AST_PORT_STATE_DISCARDING                        */
/*                                                                           */
/*****************************************************************************/

UINT1
GarpGetInstPortState (UINT2 u2GipId, tGarpGlobalPortEntry * pGlobalPortEntry)
{
    /*Since CEP is a part of CVLAN component, GARP Protocol assumes CEP will
     * be always forwarding */
    if (pGlobalPortEntry->u2PbPortType == GARP_CUSTOMER_EDGE_PORT)
    {
        return (UINT1) AST_PORT_STATE_FORWARDING;
    }

    return (GarpL2IwfGetInstPortState
            (u2GipId, (UINT2) pGlobalPortEntry->u4IfIndex));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpGetVlanPortState                             */
/*                                                                           */
/*    Description         : This function will get the per vlan port         */
/*                          state from the L2IWF module. In case of CEP it   */
/*                          returns forwarding                               */
/*                                                                           */
/*    Input(s)            : u2GipId- Instance Id.                            */
/*                          pGlobPortEntry- Global Port Entry                */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : AST_PORT_STATE_FORWARDING/                       */
/*                          AST_PORT_STATE_DISCARDING                        */
/*                                                                           */
/*****************************************************************************/

UINT1
GarpGetVlanPortState (UINT2 u2GipId, tGarpGlobalPortEntry * pGlobalPortEntry)
{
    /*Since CEP is a part of CVLAN component, GARP Protocol assumes CEP will
     * be always forwarding */
    if (pGlobalPortEntry->u2PbPortType == GARP_CUSTOMER_EDGE_PORT)
    {
        return (UINT1) AST_PORT_STATE_FORWARDING;
    }

    return (GarpL2IwfGetVlanPortState
            (u2GipId, (UINT2) pGlobalPortEntry->u4IfIndex));

}
