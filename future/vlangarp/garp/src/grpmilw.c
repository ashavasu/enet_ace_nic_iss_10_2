
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2001-2002                                     */
/* $Id: grpmilw.c,v 1.21 2015/07/02 06:43:51 siva Exp $               */
/*  FILE NAME             : garplow.c                                   */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*  SUBSYSTEM NAME        : SNMP                                        */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains SNMP low level routines  */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : MI Team                                     */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#include "garpinc.h"
#include "vcm.h"
#include "fsmpvlcli.h"

/****************************************************************************
 Function    :  nmhGetFsDot1qGvrpStatus
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                retValFsDot1qGvrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qGvrpStatus (INT4 i4FsDot1qVlanContextId,
                         INT4 *pi4RetValFsDot1qGvrpStatus)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsDot1qVlanContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qGvrpStatus (pi4RetValFsDot1qGvrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qGvrpStatus
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                setValFsDot1qGvrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qGvrpStatus (INT4 i4FsDot1qVlanContextId,
                         INT4 i4SetValFsDot1qGvrpStatus)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsDot1qVlanContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qGvrpStatus (i4SetValFsDot1qGvrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qGvrpStatus
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                testValFsDot1qGvrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qGvrpStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1qVlanContextId,
                            INT4 i4TestValFsDot1qGvrpStatus)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsDot1qVlanContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qGvrpStatus (pu4ErrorCode,
                                         i4TestValFsDot1qGvrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qPortGvrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1qPortGvrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qPortGvrpStatus (INT4 i4FsDot1dBasePort,
                             INT4 *pi4RetValFsDot1qPortGvrpStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        *pi4RetValFsDot1qPortGvrpStatus = GVRP_DISABLED;
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qPortGvrpStatus ((INT4) u2LocalPortId,
                                          pi4RetValFsDot1qPortGvrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qPortGvrpFailedRegistrations
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1qPortGvrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qPortGvrpFailedRegistrations (INT4 i4FsDot1dBasePort,
                                          UINT4
                                          *pu4RetValFsDot1qPortGvrpFailedRegistrations)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        *pu4RetValFsDot1qPortGvrpFailedRegistrations = GARP_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qPortGvrpFailedRegistrations ((INT4) u2LocalPortId,
                                                       pu4RetValFsDot1qPortGvrpFailedRegistrations);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qPortGvrpLastPduOrigin
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1qPortGvrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qPortGvrpLastPduOrigin (INT4 i4FsDot1dBasePort,
                                    tMacAddr *
                                    pRetValFsDot1qPortGvrpLastPduOrigin)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        MEMSET (pRetValFsDot1qPortGvrpLastPduOrigin, 0, sizeof (tMacAddr));
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qPortGvrpLastPduOrigin ((INT4) u2LocalPortId,
                                                 pRetValFsDot1qPortGvrpLastPduOrigin);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qPortRestrictedVlanRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1qPortRestrictedVlanRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qPortRestrictedVlanRegistration (INT4 i4FsDot1dBasePort,
                                             INT4
                                             *pi4RetValFsDot1qPortRestrictedVlanRegistration)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        *pi4RetValFsDot1qPortRestrictedVlanRegistration = GARP_SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qPortRestrictedVlanRegistration ((INT4) u2LocalPortId,
                                                          pi4RetValFsDot1qPortRestrictedVlanRegistration);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qPortGvrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                setValFsDot1qPortGvrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qPortGvrpStatus (INT4 i4FsDot1dBasePort,
                             INT4 i4SetValFsDot1qPortGvrpStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qPortGvrpStatus
        ((INT4) u2LocalPortId, i4SetValFsDot1qPortGvrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qPortRestrictedVlanRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                setValFsDot1qPortRestrictedVlanRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qPortRestrictedVlanRegistration (INT4 i4FsDot1dBasePort,
                                             INT4
                                             i4SetValFsDot1qPortRestrictedVlanRegistration)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qPortRestrictedVlanRegistration ((INT4) u2LocalPortId,
                                                          i4SetValFsDot1qPortRestrictedVlanRegistration);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qPortGvrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                testValFsDot1qPortGvrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qPortGvrpStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                INT4 i4TestValFsDot1qPortGvrpStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qPortGvrpStatus (pu4ErrorCode, (INT4) u2LocalPortId,
                                             i4TestValFsDot1qPortGvrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qPortRestrictedVlanRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                testValFsDot1qPortRestrictedVlanRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qPortRestrictedVlanRegistration (UINT4 *pu4ErrorCode,
                                                INT4 i4FsDot1dBasePort,
                                                INT4
                                                i4TestValFsDot1qPortRestrictedVlanRegistration)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qPortRestrictedVlanRegistration
        (pu4ErrorCode, (INT4) u2LocalPortId,
         i4TestValFsDot1qPortRestrictedVlanRegistration);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureGarpShutdownStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureGarpShutdownStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureGarpShutdownStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                         INT4
                                         *pi4RetValFsMIDot1qFutureGarpShutdownStatus)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsMIDot1qFutureVlanContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureGarpShutdownStatus
        (pi4RetValFsMIDot1qFutureGarpShutdownStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureGarpShutdownStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureGarpShutdownStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureGarpShutdownStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                         INT4
                                         i4SetValFsMIDot1qFutureGarpShutdownStatus)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsMIDot1qFutureVlanContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureGarpShutdownStatus
        (i4SetValFsMIDot1qFutureGarpShutdownStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureGarpShutdownStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureGarpShutdownStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureGarpShutdownStatus (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIDot1qFutureVlanContextId,
                                            INT4
                                            i4TestValFsMIDot1qFutureGarpShutdownStatus)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsMIDot1qFutureVlanContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureGarpShutdownStatus (pu4ErrorCode,
                                                       i4TestValFsMIDot1qFutureGarpShutdownStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureGvrpOperStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureGvrpOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureGvrpOperStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                     INT4
                                     *pi4RetValFsMIDot1qFutureGvrpOperStatus)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsMIDot1qFutureVlanContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureGvrpOperStatus
        (pi4RetValFsMIDot1qFutureGvrpOperStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureGmrpOperStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureGmrpOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureGmrpOperStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                     INT4
                                     *pi4RetValFsMIDot1qFutureGmrpOperStatus)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsMIDot1qFutureVlanContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureGmrpOperStatus
        (pi4RetValFsMIDot1qFutureGmrpOperStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/************************** fsmsbext MIB Get Routines ***********************/

/****************************************************************************
 Function    :  nmhGetFsDot1dGmrpStatus
 Input       :  The Indices
                FsDot1dBridgeContextId

                The Object
                retValFsDot1dGmrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dGmrpStatus (INT4 i4FsDot1dBridgeContextId,
                         INT4 *pi4RetValFsDot1dGmrpStatus)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsDot1dBridgeContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dGmrpStatus (pi4RetValFsDot1dGmrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dGmrpStatus
 Input       :  The Indices
                FsDot1dBridgeContextId

                The Object
                setValFsDot1dGmrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dGmrpStatus (INT4 i4FsDot1dBridgeContextId,
                         INT4 i4SetValFsDot1dGmrpStatus)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsDot1dBridgeContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dGmrpStatus (i4SetValFsDot1dGmrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dGmrpStatus
 Input       :  The Indices
                FsDot1dBridgeContextId

                The Object
                testValFsDot1dGmrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dGmrpStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBridgeContextId,
                            INT4 i4TestValFsDot1dGmrpStatus)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsDot1dBridgeContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dGmrpStatus (pu4ErrorCode,
                                         i4TestValFsDot1dGmrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dExtBaseTable
 Input       :  The Indices
                FsDot1dBridgeContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dExtBaseTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureGarpDebug
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                retValFsMIDot1qFutureGarpDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureGarpDebug (INT4 i4FsMIDot1qFutureVlanContextId,
                                INT4 *pi4RetValFsMIDot1qFutureGarpDebug)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsMIDot1qFutureVlanContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureGarpDebug (pi4RetValFsMIDot1qFutureGarpDebug);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureGarpDebug
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                setValFsMIDot1qFutureGarpDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureGarpDebug (INT4 i4FsMIDot1qFutureVlanContextId,
                                INT4 i4SetValFsMIDot1qFutureGarpDebug)
{
    INT1                i1RetVal;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (i4FsMIDot1qFutureVlanContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1qFutureGarpDebug (i4SetValFsMIDot1qFutureGarpDebug);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureGarpDebug
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object
                testValFsMIDot1qFutureGarpDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureGarpDebug (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIDot1qFutureVlanContextId,
                                   INT4 i4TestValFsMIDot1qFutureGarpDebug)
{
    INT1                i1RetVal;

    if (GarpSelectContext (i4FsMIDot1qFutureVlanContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1qFutureGarpDebug
        (pu4ErrorCode, i4TestValFsMIDot1qFutureGarpDebug);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dPortGarpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dPortGarpTable (INT4 i4FsDot1dBasePort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1dPortGarpTable
        ((INT4) u2LocalPortId);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dPortGarpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dPortGarpTable (INT4 *pi4FsDot1dBasePort)
{
    UINT4               u4IfIndex;

    if (GarpGetFirstPortInSystem (&u4IfIndex) == GARP_SUCCESS)
    {
        *pi4FsDot1dBasePort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dPortGarpTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dPortGarpTable (INT4 i4FsDot1dBasePort,
                                     INT4 *pi4NextFsDot1dBasePort)
{
    UINT4               u4IfIndex;

    if (GarpGetNextPortInSystem (i4FsDot1dBasePort, &u4IfIndex) == GARP_SUCCESS)
    {
        *pi4NextFsDot1dBasePort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGarpJoinTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGarpJoinTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGarpJoinTime (INT4 i4FsDot1dBasePort,
                               INT4 *pi4RetValFsDot1dPortGarpJoinTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dPortGarpJoinTime ((INT4) u2LocalPortId,
                                            pi4RetValFsDot1dPortGarpJoinTime);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGarpLeaveTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGarpLeaveTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGarpLeaveTime (INT4 i4FsDot1dBasePort,
                                INT4 *pi4RetValFsDot1dPortGarpLeaveTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dPortGarpLeaveTime ((INT4) u2LocalPortId,
                                             pi4RetValFsDot1dPortGarpLeaveTime);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGarpLeaveAllTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGarpLeaveAllTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGarpLeaveAllTime (INT4 i4FsDot1dBasePort,
                                   INT4 *pi4RetValFsDot1dPortGarpLeaveAllTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dPortGarpLeaveAllTime ((INT4) u2LocalPortId,
                                                pi4RetValFsDot1dPortGarpLeaveAllTime);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortGarpJoinTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortGarpJoinTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortGarpJoinTime (INT4 i4FsDot1dBasePort,
                               INT4 i4SetValFsDot1dPortGarpJoinTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dPortGarpJoinTime
        ((INT4) u2LocalPortId, i4SetValFsDot1dPortGarpJoinTime);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortGarpLeaveTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortGarpLeaveTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortGarpLeaveTime (INT4 i4FsDot1dBasePort,
                                INT4 i4SetValFsDot1dPortGarpLeaveTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dPortGarpLeaveTime
        ((INT4) u2LocalPortId, i4SetValFsDot1dPortGarpLeaveTime);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortGarpLeaveAllTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortGarpLeaveAllTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortGarpLeaveAllTime (INT4 i4FsDot1dBasePort,
                                   INT4 i4SetValFsDot1dPortGarpLeaveAllTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dPortGarpLeaveAllTime
        ((INT4) u2LocalPortId, i4SetValFsDot1dPortGarpLeaveAllTime);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortGarpJoinTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortGarpJoinTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortGarpJoinTime (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                  INT4 i4TestValFsDot1dPortGarpJoinTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dPortGarpJoinTime (pu4ErrorCode,
                                               (INT4) u2LocalPortId,
                                               i4TestValFsDot1dPortGarpJoinTime);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortGarpLeaveTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortGarpLeaveTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortGarpLeaveTime (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                   INT4 i4TestValFsDot1dPortGarpLeaveTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dPortGarpLeaveTime (pu4ErrorCode,
                                                (INT4) u2LocalPortId,
                                                i4TestValFsDot1dPortGarpLeaveTime);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortGarpLeaveAllTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortGarpLeaveAllTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortGarpLeaveAllTime (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDot1dBasePort,
                                      INT4 i4TestValFsDot1dPortGarpLeaveAllTime)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dPortGarpLeaveAllTime (pu4ErrorCode,
                                                   (INT4) u2LocalPortId,
                                                   i4TestValFsDot1dPortGarpLeaveAllTime);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpJoinEmptyTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpJoinEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpJoinEmptyTxCount (INT4
                                                   i4FsMIDot1qFutureVlanPort,
                                                   UINT4
                                                   *pu4RetValFsMIDot1qFutureVlanPortGmrpJoinEmptyTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGmrpJoinEmptyTxCount ((INT4) u2LocalPortId,
                                                       pu4RetValFsMIDot1qFutureVlanPortGmrpJoinEmptyTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpJoinEmptyRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpJoinEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpJoinEmptyRxCount (INT4
                                                   i4FsMIDot1qFutureVlanPort,
                                                   UINT4
                                                   *pu4RetValFsMIDot1qFutureVlanPortGmrpJoinEmptyRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGmrpJoinEmptyRxCount ((INT4) u2LocalPortId,
                                                       pu4RetValFsMIDot1qFutureVlanPortGmrpJoinEmptyRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpJoinInTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpJoinInTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpJoinInTxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                                UINT4
                                                *pu4RetValFsMIDot1qFutureVlanPortGmrpJoinInTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortGmrpJoinInTxCount ((INT4) u2LocalPortId,
                                                           pu4RetValFsMIDot1qFutureVlanPortGmrpJoinInTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpJoinInRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpJoinInRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpJoinInRxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                                UINT4
                                                *pu4RetValFsMIDot1qFutureVlanPortGmrpJoinInRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortGmrpJoinInRxCount ((INT4) u2LocalPortId,
                                                           pu4RetValFsMIDot1qFutureVlanPortGmrpJoinInRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpLeaveInTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpLeaveInTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveInTxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                                 UINT4
                                                 *pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveInTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGmrpLeaveInTxCount ((INT4) u2LocalPortId,
                                                     pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveInTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpLeaveInRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpLeaveInRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveInRxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                                 UINT4
                                                 *pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveInRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }
    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGmrpLeaveInRxCount ((INT4) u2LocalPortId,
                                                     pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveInRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCount (INT4
                                                    i4FsMIDot1qFutureVlanPort,
                                                    UINT4
                                                    *pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGmrpLeaveEmptyTxCount ((INT4) u2LocalPortId,
                                                        pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveEmptyTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCount (INT4
                                                    i4FsMIDot1qFutureVlanPort,
                                                    UINT4
                                                    *pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGmrpLeaveEmptyRxCount ((INT4) u2LocalPortId,
                                                        pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveEmptyRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpEmptyTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpEmptyTxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                               UINT4
                                               *pu4RetValFsMIDot1qFutureVlanPortGmrpEmptyTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortGmrpEmptyTxCount ((INT4) u2LocalPortId,
                                                          pu4RetValFsMIDot1qFutureVlanPortGmrpEmptyTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpEmptyRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpEmptyRxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                               UINT4
                                               *pu4RetValFsMIDot1qFutureVlanPortGmrpEmptyRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortGmrpEmptyRxCount ((INT4) u2LocalPortId,
                                                          pu4RetValFsMIDot1qFutureVlanPortGmrpEmptyRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpLeaveAllTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpLeaveAllTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveAllTxCount (INT4
                                                  i4FsMIDot1qFutureVlanPort,
                                                  UINT4
                                                  *pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveAllTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGmrpLeaveAllTxCount ((INT4) u2LocalPortId,
                                                      pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveAllTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpLeaveAllRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpLeaveAllRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpLeaveAllRxCount (INT4
                                                  i4FsMIDot1qFutureVlanPort,
                                                  UINT4
                                                  *pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveAllRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGmrpLeaveAllRxCount ((INT4) u2LocalPortId,
                                                      pu4RetValFsMIDot1qFutureVlanPortGmrpLeaveAllRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGmrpDiscardCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGmrpDiscardCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGmrpDiscardCount (INT4 i4FsMIDot1qFutureVlanPort,
                                               UINT4
                                               *pu4RetValFsMIDot1qFutureVlanPortGmrpDiscardCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortGmrpDiscardCount ((INT4) u2LocalPortId,
                                                          pu4RetValFsMIDot1qFutureVlanPortGmrpDiscardCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpJoinEmptyTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpJoinEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpJoinEmptyTxCount (INT4
                                                   i4FsMIDot1qFutureVlanPort,
                                                   UINT4
                                                   *pu4RetValFsMIDot1qFutureVlanPortGvrpJoinEmptyTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGvrpJoinEmptyTxCount ((INT4) u2LocalPortId,
                                                       pu4RetValFsMIDot1qFutureVlanPortGvrpJoinEmptyTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpJoinEmptyRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpJoinEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpJoinEmptyRxCount (INT4
                                                   i4FsMIDot1qFutureVlanPort,
                                                   UINT4
                                                   *pu4RetValFsMIDot1qFutureVlanPortGvrpJoinEmptyRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGvrpJoinEmptyRxCount ((INT4) u2LocalPortId,
                                                       pu4RetValFsMIDot1qFutureVlanPortGvrpJoinEmptyRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpJoinInTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpJoinInTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpJoinInTxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                                UINT4
                                                *pu4RetValFsMIDot1qFutureVlanPortGvrpJoinInTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }
    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortGvrpJoinInTxCount ((INT4) u2LocalPortId,
                                                           pu4RetValFsMIDot1qFutureVlanPortGvrpJoinInTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpJoinInRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpJoinInRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpJoinInRxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                                UINT4
                                                *pu4RetValFsMIDot1qFutureVlanPortGvrpJoinInRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }
    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortGvrpJoinInRxCount ((INT4) u2LocalPortId,
                                                           pu4RetValFsMIDot1qFutureVlanPortGvrpJoinInRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpLeaveInTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpLeaveInTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveInTxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                                 UINT4
                                                 *pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveInTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }
    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGvrpLeaveInTxCount ((INT4) u2LocalPortId,
                                                     pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveInTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpLeaveInRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpLeaveInRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveInRxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                                 UINT4
                                                 *pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveInRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGvrpLeaveInRxCount ((INT4) u2LocalPortId,
                                                     pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveInRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCount (INT4
                                                    i4FsMIDot1qFutureVlanPort,
                                                    UINT4
                                                    *pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGvrpLeaveEmptyTxCount ((INT4) u2LocalPortId,
                                                        pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveEmptyTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCount (INT4
                                                    i4FsMIDot1qFutureVlanPort,
                                                    UINT4
                                                    *pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGvrpLeaveEmptyRxCount ((INT4) u2LocalPortId,
                                                        pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveEmptyRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpEmptyTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpEmptyTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpEmptyTxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                               UINT4
                                               *pu4RetValFsMIDot1qFutureVlanPortGvrpEmptyTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortGvrpEmptyTxCount ((INT4) u2LocalPortId,
                                                          pu4RetValFsMIDot1qFutureVlanPortGvrpEmptyTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpEmptyRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpEmptyRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpEmptyRxCount (INT4 i4FsMIDot1qFutureVlanPort,
                                               UINT4
                                               *pu4RetValFsMIDot1qFutureVlanPortGvrpEmptyRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortGvrpEmptyRxCount ((INT4) u2LocalPortId,
                                                          pu4RetValFsMIDot1qFutureVlanPortGvrpEmptyRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpLeaveAllTxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpLeaveAllTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveAllTxCount (INT4
                                                  i4FsMIDot1qFutureVlanPort,
                                                  UINT4
                                                  *pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveAllTxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGvrpLeaveAllTxCount ((INT4) u2LocalPortId,
                                                      pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveAllTxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpLeaveAllRxCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpLeaveAllRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpLeaveAllRxCount (INT4
                                                  i4FsMIDot1qFutureVlanPort,
                                                  UINT4
                                                  *pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveAllRxCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhGetDot1qFutureVlanPortGvrpLeaveAllRxCount ((INT4) u2LocalPortId,
                                                      pu4RetValFsMIDot1qFutureVlanPortGvrpLeaveAllRxCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortGvrpDiscardCount
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortGvrpDiscardCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureVlanPortGvrpDiscardCount (INT4 i4FsMIDot1qFutureVlanPort,
                                               UINT4
                                               *pu4RetValFsMIDot1qFutureVlanPortGvrpDiscardCount)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1qFutureVlanPortGvrpDiscardCount ((INT4) u2LocalPortId,
                                                          pu4RetValFsMIDot1qFutureVlanPortGvrpDiscardCount);

    GarpReleaseContext ();
    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dPortGarpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dPortGarpTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dPortGmrpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dPortGmrpTable (INT4 i4FsDot1dBasePort)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceDot1dPortGmrpTable
        ((INT4) u2LocalPortId);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dPortGmrpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot1dPortGmrpTable (INT4 *pi4FsDot1dBasePort)
{
    UINT4               u4IfIndex;

    if (GarpGetFirstPortInSystem (&u4IfIndex) == GARP_SUCCESS)
    {
        *pi4FsDot1dBasePort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dPortGmrpTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dPortGmrpTable (INT4 i4FsDot1dBasePort,
                                     INT4 *pi4NextFsDot1dBasePort)
{
    UINT4               u4IfIndex;

    if (GarpGetNextPortInSystem (i4FsDot1dBasePort, &u4IfIndex) == GARP_SUCCESS)
    {
        *pi4NextFsDot1dBasePort = (INT4) u4IfIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGmrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGmrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGmrpStatus (INT4 i4FsDot1dBasePort,
                             INT4 *pi4RetValFsDot1dPortGmrpStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        *pi4RetValFsDot1dPortGmrpStatus = GMRP_DISABLED;
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dPortGmrpStatus ((INT4) u2LocalPortId,
                                          pi4RetValFsDot1dPortGmrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGmrpFailedRegistrations
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGmrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGmrpFailedRegistrations (INT4 i4FsDot1dBasePort,
                                          UINT4
                                          *pu4RetValFsDot1dPortGmrpFailedRegistrations)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dPortGmrpFailedRegistrations ((INT4) u2LocalPortId,
                                                       pu4RetValFsDot1dPortGmrpFailedRegistrations);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGmrpLastPduOrigin
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGmrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGmrpLastPduOrigin (INT4 i4FsDot1dBasePort,
                                    tMacAddr *
                                    pRetValFsDot1dPortGmrpLastPduOrigin)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dPortGmrpLastPduOrigin ((INT4) u2LocalPortId,
                                                 pRetValFsDot1dPortGmrpLastPduOrigin);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortRestrictedGroupRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortRestrictedGroupRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortRestrictedGroupRegistration (INT4 i4FsDot1dBasePort,
                                              INT4
                                              *pi4RetValFsDot1dPortRestrictedGroupRegistration)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetDot1dPortRestrictedGroupRegistration ((INT4) u2LocalPortId,
                                                           pi4RetValFsDot1dPortRestrictedGroupRegistration);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortGmrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortGmrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortGmrpStatus (INT4 i4FsDot1dBasePort,
                             INT4 i4SetValFsDot1dPortGmrpStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dPortGmrpStatus
        ((INT4) u2LocalPortId, i4SetValFsDot1dPortGmrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortRestrictedGroupRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortRestrictedGroupRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortRestrictedGroupRegistration (INT4 i4FsDot1dBasePort,
                                              INT4
                                              i4SetValFsDot1dPortRestrictedGroupRegistration)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhSetDot1dPortRestrictedGroupRegistration
        ((INT4) u2LocalPortId, i4SetValFsDot1dPortRestrictedGroupRegistration);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortGmrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortGmrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortGmrpStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                INT4 i4TestValFsDot1dPortGmrpStatus)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dPortGmrpStatus (pu4ErrorCode,
                                             (INT4) u2LocalPortId,
                                             i4TestValFsDot1dPortGmrpStatus);

    GarpReleaseContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortRestrictedGroupRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortRestrictedGroupRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortRestrictedGroupRegistration (UINT4 *pu4ErrorCode,
                                                 INT4 i4FsDot1dBasePort,
                                                 INT4
                                                 i4TestValFsDot1dPortRestrictedGroupRegistration)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsDot1dBasePort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhTestv2Dot1dPortRestrictedGroupRegistration (pu4ErrorCode,
                                                              (INT4)
                                                              u2LocalPortId,
                                                              i4TestValFsDot1dPortRestrictedGroupRegistration);

    GarpReleaseContext ();
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dPortGmrpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dPortGmrpTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureGarpGlobalTrace
 Input       :  The Indices

                The Object
                retValFsMIDot1qFutureGarpGlobalTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureGarpGlobalTrace (INT4
                                      *pi4RetValFsMIDot1qFutureGarpGlobalTrace)
{
#ifdef TRACE_WANTED
    *pi4RetValFsMIDot1qFutureGarpGlobalTrace = (INT4) gu1GarpGblTraceOption;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsMIDot1qFutureGarpGlobalTrace);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureGarpGlobalTrace
 Input       :  The Indices

                The Object
                setValFsMIDot1qFutureGarpGlobalTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureGarpGlobalTrace (INT4
                                      i4SetValFsMIDot1qFutureGarpGlobalTrace)
{
#ifdef TRACE_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4CurrContextId = 0;
    UINT4               u4SeqNum = 0;

    VLAN_MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    gu1GarpGblTraceOption = (UINT1) i4SetValFsMIDot1qFutureGarpGlobalTrace;

    /* Sending Trigger to MSR */

    u4CurrContextId = GARP_CURR_CONTEXT_ID ();

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIDot1qFutureGarpGlobalTrace,
                          u4SeqNum, FALSE, GarpLock, GarpUnLock, 0,
                          SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFsMIDot1qFutureGarpGlobalTrace));
    if (GarpSelectContext (u4CurrContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsMIDot1qFutureGarpGlobalTrace);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureGarpGlobalTrace
 Input       :  The Indices

                The Object
                testValFsMIDot1qFutureGarpGlobalTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureGarpGlobalTrace (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFsMIDot1qFutureGarpGlobalTrace)
{
#ifdef TRACE_WANTED
    if ((i4TestValFsMIDot1qFutureGarpGlobalTrace != GARP_ENABLED) &&
        (i4TestValFsMIDot1qFutureGarpGlobalTrace != GARP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFsMIDot1qFutureGarpGlobalTrace);
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureGarpGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureGarpGlobalTrace (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureVlanPortClearGarpStats
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                retValFsMIDot1qFutureVlanPortClearGarpStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIDot1qFutureVlanPortClearGarpStats(INT4 i4FsMIDot1qFutureVlanPort , INT4 *pi4RetValFsMIDot1qFutureVlanPortClearGarpStats)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
                                       &u4ContextId,
                                       &u2LocalPortId) != GARP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIDot1qFutureVlanPortClearGarpStats = GARP_FALSE;

    GarpReleaseContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureVlanPortClearGarpStats
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                testValFsMIDot1qFutureVlanPortClearGarpStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureVlanPortClearGarpStats(UINT4 *pu4ErrorCode , INT4 i4FsMIDot1qFutureVlanPort ,
                                               INT4 i4TestValFsMIDot1qFutureVlanPortClearGarpStats)

{
    INT1                i1RetVal = SNMP_SUCCESS;
    UNUSED_PARAM (i4FsMIDot1qFutureVlanPort);

    if ((i4TestValFsMIDot1qFutureVlanPortClearGarpStats != GARP_TRUE)
        && (i4TestValFsMIDot1qFutureVlanPortClearGarpStats != GARP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureVlanPortClearGarpStats
 Input       :  The Indices
                FsMIDot1qFutureVlanPort

                The Object
                setValFsMIDot1qFutureVlanPortClearGarpStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureVlanPortClearGarpStats(INT4 i4FsMIDot1qFutureVlanPort , INT4 i4SetValFsMIDot1qFutureVlanPortClearGarpStats)
{

	UINT4                 u4ContextId;
	UINT4                 u4CurrentIndex = 0;
	UINT4                 u4NextIndex = 0;
	UINT2                 u2LocalPortId = 0;
	INT1                  i1RetVal = SNMP_SUCCESS;
	tGarpGlobalPortEntry *pGlobalPortEntry = NULL;
	tGarpPortStats       *pGarpPortClearStats =NULL;


	if (i4SetValFsMIDot1qFutureVlanPortClearGarpStats != GARP_ENABLED)
	{
		return i1RetVal;
	}

	if (i4FsMIDot1qFutureVlanPort != 0)
	{
		if (GarpGetContextInfoFromIfIndex ((UINT2) i4FsMIDot1qFutureVlanPort,
					&u4ContextId,
					&u2LocalPortId) != GARP_SUCCESS)
		{
			return SNMP_FAILURE;
		}

		if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
		{
			return SNMP_SUCCESS;
		}
		if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
		{
			return SNMP_FAILURE;
		}

		pGlobalPortEntry = GARP_PORT_ENTRY ((INT4)u2LocalPortId);

		if (pGlobalPortEntry == NULL)
		{
			GarpReleaseContext ();
			return SNMP_FAILURE;
		}

		pGarpPortClearStats = pGlobalPortEntry->pGarpPortStats;

		MEMSET(pGarpPortClearStats,0,sizeof(tGarpPortStats));

		GarpReleaseContext ();
		return i1RetVal;
	}

	else
	{
		if (GarpGetNextPortInSystem ((INT4)u4CurrentIndex, &u4NextIndex) == GARP_FAILURE)
		{
			return i1RetVal;
		}
		do
		{
			u4CurrentIndex = u4NextIndex;

			if (GarpGetContextInfoFromIfIndex ((UINT2) u4NextIndex,
						&u4ContextId,
						&u2LocalPortId) != GARP_SUCCESS)
			{
				continue;
			}

			if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
			{
				continue;
			}

			pGlobalPortEntry = GARP_PORT_ENTRY ((INT4)u2LocalPortId);

			if (pGlobalPortEntry == NULL)
			{
				u4CurrentIndex = u4NextIndex;
				GarpReleaseContext ();
				continue;
			}

			pGarpPortClearStats = pGlobalPortEntry->pGarpPortStats;

			MEMSET(pGarpPortClearStats,0,sizeof(tGarpPortStats));
			GarpReleaseContext ();

		} while (GarpGetNextPortInSystem ((INT4)u4CurrentIndex, &u4NextIndex) != GARP_FAILURE);

	}

	return i1RetVal;
}



