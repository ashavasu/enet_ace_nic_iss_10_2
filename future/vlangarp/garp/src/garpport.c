/* $Id: garpport.c,v 1.11 2015/07/13 04:42:41 siva Exp $              */
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2001-2002                                     */
/*                                                                      */
/*  FILE NAME             : garpport.c                                  */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*  SUBSYSTEM NAME        : API                                         */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 12 Nov 2007                                 */
/*  AUTHOR                : Aricent Inc.                                */
/*  DESCRIPTION           : This file contains the external APIs        */
/*                          called by GARP                              */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 12 Nov 2007                                 */
/*  Modified by           : L2 Team                                     */
/*  Description of change : Initial Creation                            */
/************************************************************************/
#ifdef GARP_WANTED

#include "garpinc.h"

/*****************************************************************************/
/* Function Name      : GarpCfaCliConfGetIfName                              */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pi1IfName - Interface name of the given IfIndex.     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    return (CfaCliConfGetIfName (u4IfIndex, pi1IfName));
}

/*****************************************************************************/
/* Function Name      : GarpCfaCliGetIfName                                  */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pi1IfName - Interface name of the given IfIndex.     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
#ifdef CLI_WANTED
    return (CfaCliGetIfName (u4IfIndex, pi1IfName));
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi1IfName);
    return CLI_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : GarpCfaGetIfInfo                                     */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pIfInfo   - Interface information.                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaGetIfInfo (u4IfIndex, pIfInfo));
}

/*****************************************************************************/
/* Function Name      : GarpCfaIsThisOpenflowVlan                            */
/*                                                                           */
/* Description        : This function checks whether the given Vlan is an    */
/*                      Openflow Vlan or not.                                */
/*                                                                           */
/* Input(s)           : VlanId                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_TRUE/ CFA_FALSE                                  */
/*****************************************************************************/

BOOL1
GarpCfaIsThisOpenflowVlan (UINT4 u4VlanId)
{
    return (CfaIsThisOpenflowVlan (u4VlanId));
}

/*****************************************************************************/
/* Function Name      : GarpIsPvrstStartedInContext                          */
/*                                                                           */
/* Description        : This function calls STP Module to get PVRST          */
/*                      Module status in the given context.                  */
/*                                                                           */
/* Input(s)           : u4ContextId -Current Context Id                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpIsPvrstStartedInContext (UINT4 u4ContextId)
{
    return (AstIsPvrstStartedInContext (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : GarpSnoopIsIgmpSnoopingEnabled                       */
/*                                                                           */
/* Description        : This function checks if IGMP snooping feature is     */
/*                      enabled in the system or not.                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
GarpSnoopIsIgmpSnoopingEnabled (UINT4 u4ContextId)
{
    return (SnoopIsIgmpSnoopingEnabled (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : GarpSnoopIsMldSnoopingEnabled                        */
/*                                                                           */
/* Description        : This function checks if MLD snooping feature is      */
/*                      enabled in the system or not.                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
GarpSnoopIsMldSnoopingEnabled (UINT4 u4ContextId)
{
    return (SnoopIsMldSnoopingEnabled (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : GarpVcmGetAliasName                                  */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Alias name of the given context.                     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : pu1Alias    - Alias Name                             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    return (VcmGetAliasName (u4ContextId, pu1Alias));
}

/*****************************************************************************/
/* Function Name      : GarpVcmGetContextInfoFromIfIndex                     */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Context-Id and the Localport number.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier.                    */
/*                                                                           */
/* Output(s)          : pu4ContextId - Context Identifier.                   */
/*                      pu2LocalPortId - Local port number.                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                  UINT2 *pu2LocalPortId)
{
    return (VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                          pu2LocalPortId));
}

/*****************************************************************************/
/* Function Name      : GarpVcmGetSystemMode                                 */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpVcmGetSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : GarpVcmGetSystemModeExt                              */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpVcmGetSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : GarpVcmIsSwitchExist                                 */
/*                                                                           */
/* Description        : This function calls the VCM Module to check whether  */
/*                      the context exist or not.                            */
/*                                                                           */
/* Input(s)           : pu1Alias - Alias Name                                */
/*                                                                           */
/* Output(s)          : pu4VcNum - Context Identifier                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    return (VcmIsSwitchExist (pu1Alias, pu4VcNum));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanCheckAndTagOutgoingGmrpFrame             */
/*                                                                           */
/*    Description         : This function is called by GARP module to        */
/*                          transmit MRP PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanId   - VlanId                                */
/*                          u2Port   - Port on which the GARP PDU is to be   */
/*                                     transmitted.                          */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : VLAN_FORWARD/                                     */
/*                         VLAN_NO_FORWARD                                   */
/*                                                                           */
/*****************************************************************************/
INT4
GarpVlanCheckAndTagOutgoingGmrpFrame (UINT4 u4ContextId,
                                      tCRU_BUF_CHAIN_DESC * pBuf,
                                      tVlanId VlanId, UINT2 u2Port)
{
    return (VlanCheckAndTagOutgoingGmrpFrame (u4ContextId, pBuf,
                                              VlanId, u2Port));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanDeleteAllDynamicVlanInfo                 */
/*                                                                           */
/*    Description         : Removes all the dynamic vlan information         */
/*                          from the Current table.                          */
/*                                                                           */
/*    Input(s)            : u2Port - Port Number of the input port.          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
GarpVlanDeleteAllDynamicVlanInfo (UINT4 u4ContextId, UINT2 u2Port)
{
    VlanDeleteAllDynamicVlanInfo (u4ContextId, u2Port);
}

/******************************************************************************/
/*  Function Name   : GarpVlanGetNodeStatus                                   */
/*                                                                            */
/*  Description     : This function will be invoked by GARP module to get     */
/*                    the node status from the VLAN module. This function     */
/*                    will be invoked when the GARP module is started.        */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
tVlanNodeStatus
GarpVlanGetNodeStatus (VOID)
{
    return (VlanGetNodeStatus ());
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanGetStartedStatus                         */
/*                                                                           */
/*    Description         : This function returns whether VLAN is started or */
/*                          shutdown                                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_TRUE or VLAN_FALSE                           */
/*                                                                           */
/*****************************************************************************/

INT4
GarpVlanGetStartedStatus (UINT4 u4ContextId)
{
    return (VlanGetStartedStatus (u4ContextId));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanGmrpDisableInd                           */
/*                                                                           */
/*    Description         : Gmrp indicates Vlan, that Gmrp is disabled.      */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
GarpVlanGmrpDisableInd (UINT4 u4ContextId)
{
    VlanGmrpDisableInd (u4ContextId);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanGmrpEnableInd ()                         */
/*                                                                           */
/*    Description         : Gmrp indicates Vlan, that Gmrp is enabled.       */
/*                          If there are any Static Mcast entry configured   */
/*                          then, they must be propagated.                   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
GarpVlanGmrpEnableInd (UINT4 u4ContextId)
{
    VlanGmrpEnableInd (u4ContextId);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanGvrpDisableInd                           */
/*                                                                           */
/*    Description         : Gvrp indicates Vlan, that Gvrp is disabled.      */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
GarpVlanGvrpDisableInd (UINT4 u4ContextId)
{
    VlanGvrpDisableInd (u4ContextId);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanGvrpEnableInd ()                         */
/*                                                                           */
/*    Description         : Gvrp indicates Vlan, that Gvrp is enabled.       */
/*                          If there are any Static Vlan entry configured    */
/*                          then, they must be propagated.                   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
GarpVlanGvrpEnableInd (UINT4 u4ContextId)
{
    VlanGvrpEnableInd (u4ContextId);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanIsMcastStaticAndRegNormal                */
/*                                                                           */
/*    Description         : This function checks if the Multicast MacAddress */
/*                          is registered statically for the given VlanId    */
/*                          and checks if the MacAddress is registered NORMAL*/
/*                          on u2Port                                        */
/*                                                                           */
/*    Input(s)            : MacAddr -  MAC address                           */
/*                          u2VlanId - VlanId                                */
/*                          u2Port   - Port Number                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*****************************************************************************/
INT4
GarpVlanIsMcastStaticAndRegNormal (UINT4 u4ContxtId, tMacAddr MacAddr,
                                   UINT2 u2VlanId, UINT2 u2Port)
{
    return (VlanIsMcastStaticAndRegNormal (u4ContxtId, MacAddr,
                                           u2VlanId, u2Port));
}

#ifdef GARP_EXTENDED_FILTER
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanDeleteAllDynamicDefGroupInfo             */
/*                                                                           */
/*    Description         : This function removes all the dynamic service req*/
/*                          information from the table.                      */
/*                                                                           */
/*    Input(s)            : u2Port - This is currently called from GARP, so  */
/*                          the port is local port                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
GarpVlanDeleteAllDynamicDefGroupInfo (UINT4 u4ContextId, UINT2 u2Port)
{
    VlanDeleteAllDynamicDefGroupInfo (u4ContextId, u2Port);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanIsServiceAttrStaticAndRegNormal          */
/*                                                                           */
/*    Description         : This function checks if the Service Attribute    */
/*                          is registered statically for the given VlanId    */
/*                          and checks if the MacAddress is registered NORMAL*/
/*                          on u2Port                                        */
/*                                                                           */
/*    Input(s)            : u1Type   -  Service Attribute Type               */
/*                          u2VlanId - VlanId                                */
/*                          u2Port   - Port Number                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*****************************************************************************/
INT4
GarpVlanIsServiceAttrStaticAndRegNormal (UINT4 u4ContxtId, UINT1 u1Type,
                                         UINT2 u2VlanId, UINT2 u2Port)
{
    return (VlanIsServiceAttrStaticAndRegNormal (u4ContxtId, u1Type,
                                                 u2VlanId, u2Port));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanMiDelDynamicDefGroupInfoForVlan          */
/*                                                                           */
/*    Description         : This function removes all the dynamic default    */
/*                          group information from the table for the         */
/*                          specified Vlan                                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
GarpVlanMiDelDynamicDefGroupInfoForVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    VlanMiDelDynamicDefGroupInfoForVlan (u4ContextId, VlanId);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanUpdateDynamicDefGroupInfo                */
/*                                                                           */
/*    Description         : This function updates the dynamic entries in the */
/*                          Default GroupInfo table.                         */
/*                                                                           */
/*    Input(s)            : u1Type   - The Type of group has to be updated   */
/*                          VlanId   - The VlanId                            */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the port will be added */
/*                                     into the PortList.                    */
/*                                     VLAN_DELETE, then the port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*****************************************************************************/
INT4
GarpVlanUpdateDynamicDefGroupInfo (UINT4 u4ContextId, UINT1 u1Type,
                                   tVlanId VlanId, UINT2 u2Port, UINT1 u1Action)
{
    return (VlanUpdateDynamicDefGroupInfo (u4ContextId, u1Type, VlanId,
                                           u2Port, u1Action));
}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanIsVlanDynamic                            */
/*                                                                           */
/*    Description         : This function checks if the VLAN is dynamically  */
/*    Input(s)            : VlanId - Vlan ID                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*                                                                           */
/*****************************************************************************/
INT4
GarpVlanIsVlanDynamic (UINT4 u4ContextId, UINT2 VlanId)
{
    return (VlanIsVlanDynamic (u4ContextId, VlanId));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanIsVlanEnabledInContext                   */
/*                                                                           */
/*    Description         : This function checks the VLAN module is enabled. */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*                                                                           */
/*****************************************************************************/
INT4
GarpVlanIsVlanEnabledInContext (UINT4 u4ContextId)
{
    return (VlanIsVlanEnabledInContext (u4ContextId));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanIsVlanStaticAndRegNormal                 */
/*                                                                           */
/*    Description         : This function checks if a static Vlan Entry is   */
/*                          available for the given Vlan Id and if the       */
/*                          VlanId is registered NORMAL in u2Port.           */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan ID                                 */
/*                          u2Port - Port Number for which Reg NORMAL is to  */
/*                          be checked.                                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*                                                                           */
/*****************************************************************************/
INT4
GarpVlanIsVlanStaticAndRegNormal (UINT4 u4ContxtId, UINT2 VlanId, UINT2 u2Port)
{
    return (VlanIsVlanStaticAndRegNormal (u4ContxtId, VlanId, u2Port));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanMiDelDynamicMcastInfoForVlan             */
/*                                                                           */
/*    Description         : This function removes all the dynamic multicast  */
/*                          information from the table for the specified     */
/*                          Vlan for the given context                       */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
GarpVlanMiDelDynamicMcastInfoForVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    VlanMiDelDynamicMcastInfoForVlan (u4ContextId, VlanId);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanMiDeleteAllDynamicMcastInfo              */
/*                                                                           */
/*    Description         : This function removes all the dynamic multicast  */
/*                          information from the table.                      */
/*                                                                           */
/*    Input(s)            : u4ContextID - Context Id                         */
/*                          u2Port - Port Number of the input port           */
/*                          u1MacAddrType - Mac Address type of the dynamic  */
/*                          mcast entries to be deleted (IPV4 or IPV6 mac    */
/*                          address type).Value 1 indicates ipv4 mac address */
/*                          type, value 2 indicates ipv6 mac address type and*/
/*                          value 3 indicates both type of mac address.      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
GarpVlanMiDeleteAllDynamicMcastInfo (UINT4 u4ContextId, UINT2 u2Port,
                                     UINT1 u1MacAddrType)
{
    VlanMiDeleteAllDynamicMcastInfo (u4ContextId, u2Port, u1MacAddrType);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpVlanMiUpdateDynamicMcastInfo                 */
/*                                                                           */
/*    Description         : This function updated the Multicast table.       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          MacAddr -  MacAddress                            */
/*                          VlanId   - VlanId associated with the multicast  */
/*                                     MacAddress                            */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the port will be added */
/*                                     into the PortList.                    */
/*                                                                           */
/*                                     VLAN_DELETE, then the port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*****************************************************************************/
INT4
GarpVlanMiUpdateDynamicMcastInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                                  tVlanId VlanId, UINT2 u2Port, UINT1 u1Action)
{
    return (VlanMiUpdateDynamicMcastInfo (u4ContextId, MacAddr, VlanId,
                                          u2Port, u1Action));
}

/*****************************************************************************/
/*    Function Name       : GarpVlanUpdateDynamicVlanInfo                    */
/*                                                                           */
/*    Description         : This function updates the Current vlan table.    */
/*                                                                           */
/*    Input(s)            : VlanId   - The VlanId which is learnt by GVRP    */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the Port will be added */
/*                                     into the PortList.                    */
/*                                                                           */
/*                                     VLAN_DELETE, then the Port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*****************************************************************************/
INT4
GarpVlanUpdateDynamicVlanInfo (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2Port,
                               UINT1 u1Action)
{
    return (VlanUpdateDynamicVlanInfo (u4ContextId, VlanId, u2Port, u1Action));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfGetBridgeMode                               */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      bridge mode.                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : pu4BridgeMode  - bridge mode.                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode)
{
    return (L2IwfGetBridgeMode (u4ContextId, pu4BridgeMode));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfGetInstPortState                            */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      instance port state on a port.                       */
/*                                                                           */
/* Input(s)           : u2MstInst  - InstanceId for which the port state is  */
/*                      to be obtained.                                      */
/*                      u2IfIndex  - Global IfIndex of the port whose port   */
/*                      state is to be obtained.                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
GarpL2IwfGetInstPortState (UINT2 u2MstInst, UINT2 u2IfIndex)
{
    return (L2IwfGetInstPortState (u2MstInst, u2IfIndex));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfGetNextValidPortForContext                  */
/*                                                                           */
/* Description        : This routine returns the next active port in the     */
/*                      context as ordered by the HL Port ID. This is used   */
/*                      by the L2 modules to know the active ports in the    */
/*                      system when they are started from the shutdown state */
/*                                                                           */
/* Input(s)           : u2LocalPortId - HL Port Index whose next port is to  */
/*                                      be determined                        */
/*                                                                           */
/* Output(s)          : u2NextLocalPort - The next active HL Port ID         */
/*                      u2NextIfIndex - IfIndex of the next HL Port          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
GarpL2IwfGetNextValidPortForContext (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                     UINT2 *pu2NextLocalPort,
                                     UINT4 *pu4NextIfIndex)
{
    tL2NextPortInfo     L2NextPortInfo;

    L2NextPortInfo.u4ContextId = u4ContextId;
    L2NextPortInfo.u2LocalPortId = u2LocalPortId;
    L2NextPortInfo.pu2NextLocalPort = pu2NextLocalPort;
    L2NextPortInfo.pu4NextIfIndex = pu4NextIfIndex;
    L2NextPortInfo.u4ModuleId = L2IWF_PROTOCOL_ID_GARP;

    return (L2IwfGetNextValidPortForProtoCxt (&L2NextPortInfo));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfGetPbPortType                               */
/*                                                                           */
/* Description        : This function is used to get the port type of a      */
/*                      Physical port                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                      u4IfIndex   - IfIndex of the port.                   */
/*                                                                           */
/* Output(s)          : *pu1PortType - CEP/CNP/PNP/PCEP/PCNP/PPNP            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
GarpL2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType)
{
    return (L2IwfGetPbPortType (u4IfIndex, pu1PortType));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfGetPortOperStatus                           */
/*                                                                           */
/* Description        : This routine determines the operational status of a  */
/*                      port indicated to a given module by it's lower layer */
/*                      modules (determines the lower layer port oper status)*/
/*                                                                           */
/* Input(s)           : i4ModuleId - Module for which the lower layer        */
/*                                   oper status is to be determined         */
/*                      u2IfIndex - Global IfIndex of the port whose lower   */
/*                                  layer oper status is to be determined    */
/*                                                                           */
/* Output(s)          : u1OperStatus - Lower layer port oper status for a    */
/*                                     given module                          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
GarpL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT2 u2IfIndex,
                            UINT1 *pu1OperStatus)
{
    return (L2IwfGetPortOperStatus (i4ModuleId, u2IfIndex, pu1OperStatus));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfGetPortVlanTunnelStatus                     */
/*                                                                           */
/* Description        : This routine returns the port's tunnel status        */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2IfIndex - Global Index of the port whose tunnel    */
/*                                    status is to be obtained.              */
/*                                                                           */
/* Output(s)          : bTunnelPort - Boolean value indicating whether       */
/*                                    Tunnelling is enabled on the port      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
GarpL2IwfGetPortVlanTunnelStatus (UINT2 u2IfIndex, BOOL1 * pbTunnelPort)
{
    return (L2IwfGetPortVlanTunnelStatus (u2IfIndex, pbTunnelPort));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpL2IwfGetProtocolTunnelStatusOnPort           */
/*                                                                           */
/*    Description         : This function returns the tunnel status (Tunnel/ */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) on a port        */
/*                                                                           */
/*    Input(s)            : u2Port - Port number                             */
/*                          u2Protocol     - L2 Protocol                     */
/*                                                                           */
/*    Output(s)           : u1TunnelStatus - Protocol Tunnel status          */
/*                                                                           */
/*    Returns             : None.                                            */
/*                                                                           */
/*****************************************************************************/
VOID
GarpL2IwfGetProtocolTunnelStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol,
                                        UINT1 *pu1TunnelStatus)
{
    L2IwfGetProtocolTunnelStatusOnPort (u4IfIndex, u2Protocol, pu1TunnelStatus);
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfGetVlanPortState                            */
/*                                                                           */
/* Description        : This routine returns the port state given the        */
/*                      Vlan Id and the Port Index. It accesses the L2Iwf    */
/*                      common database to find the Mst Instance to which    */
/*                      the Vlan is mapped and then gets the ports state for */
/*                      that instance.                                       */
/*                                                                           */
/* Input(s)           : VlanId - VlanId for which the port state is to be    */
/*                               obtained.                                   */
/*                    : u2IfIndex - Global IfIndex of the port whose state   */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Port State                                           */
/*****************************************************************************/
UINT1
GarpL2IwfGetVlanPortState (tVlanId VlanId, UINT2 u2IfIndex)
{
    return (L2IwfGetVlanPortState (VlanId, u2IfIndex));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfGetVlanPortType                             */
/*                                                                           */
/* Description        : This routine returns the port tunnel type            */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2IfIndex - Global Port Index of the port whose port */
/*                               type is to be obtained.                     */
/*                                                                           */
/* Output(s)          : pPortType   - Port Type VLAN_ACCESS_PORT/            */
/*                                              VLAN_HYBRID_PORT/            */
/*                                              VLAN_TRUNK_PORT              */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
GarpL2IwfGetVlanPortType (UINT2 u2IfIndex, UINT1 *pPortType)
{
    return (L2IwfGetVlanPortType (u2IfIndex, pPortType));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfHandleOutgoingPktOnPort                     */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to send out the */
/*                      PNAC packet.                                         */
/*                                                                           */
/* Input(s)           : pBuf - pointer to the outgoing packet                */
/*                      TaggedPortBitmap - Port list                         */
/*                      u4PktSize        - Packet size                       */
/*                      u2Protocol       - Protocol type                     */
/*                      u1Encap          - Encapsulation Type                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpL2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT2 u2IfIndex, UINT4 u4PktSize,
                                  UINT2 u2Protocol, UINT1 u1Encap)
{
    return (L2IwfHandleOutgoingPktOnPort (pBuf, u2IfIndex, u4PktSize,
                                          u2Protocol, u1Encap));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfIsPortInPortChannel                         */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check        */
/*                      whether the port is in port-channel.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
GarpL2IwfIsPortInPortChannel (UINT4 u4IfIndex)
{
    return (L2IwfIsPortInPortChannel (u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfMiGetVlanInstMapping                        */
/*                                                                           */
/* Description        : This routine returns the Instance to which the given */
/*                      Vlan Id is mapped. It accesses the L2Iwf common      */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Mstp Instance                                        */
/*****************************************************************************/
UINT2
GarpL2IwfMiGetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfMiGetVlanInstMapping (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfMiGetVlanLocalEgressPorts                   */
/*                                                                           */
/* Description        : This routine returns the Egress ports for the given  */
/*                      VlanId as Local port IDs. It accesses the L2Iwf      */
/*                      common database for the given context                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Egress ports are to be      */
/*                                    obtained                               */
/*                                                                           */
/* Output(s)          : EgressPorts - Egress Port List for the given Vlan    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
GarpL2IwfMiGetVlanLocalEgressPorts (UINT4 u4ContextId, tVlanId VlanId,
                                    tLocalPortList LocalEgressPorts)
{
    return (L2IwfMiGetVlanLocalEgressPorts (u4ContextId, VlanId,
                                            LocalEgressPorts));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfPbGetLocalVidFromRelayVid                   */
/*                                                                           */
/* Description        : This routine gets the local vid for the corresponding*/
/*                      relay vid.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                      u2LocalPort - Per Context Port Index.                */
/*                      RelayVid    - Relay Vlan Id                          */
/*                                                                           */
/* Output(s)          : pLocalVid - LocalVid value for the given rely vid    */
/*                      Value.                                               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
GarpL2IwfPbGetLocalVidFromRelayVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                    tVlanId RelayVid, tVlanId * pLocalVid)
{
    return (L2IwfPbGetLocalVidFromRelayVid (u4ContextId, u2LocalPort,
                                            RelayVid, pLocalVid));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfPbGetRelayVidFromLocalVid                   */
/*                                                                           */
/* Description        : This routine gets the relay vid for the corresponding*/
/*                      local vid.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                      u2LocalPort - Per Context Port Index.                */
/*                      LocalVid    - Local Vlan Id                          */
/*                                                                           */
/* Output(s)          : pRelayVid - RelayVid value for the given local vid   */
/*                      Value.                                               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
GarpL2IwfPbGetRelayVidFromLocalVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                    tVlanId LocalVid, tVlanId * pRelayVid)
{
    return (L2IwfPbGetRelayVidFromLocalVid (u4ContextId, u2LocalPort,
                                            LocalVid, pRelayVid));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfSetProtocolEnabledStatusOnPort              */
/*                                                                           */
/* Description        : This routine will be called by protocol routines to  */
/*                      update the corresponding protocol's enabled status   */
/*                      on a port.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId   - Virtual Switch ID                    */
/*                      u2LocalPortId - Local port Index of the port whose   */
/*                                      protocols enabled status is to be    */
/*                                      updated.                             */
/*                      u2Protocol    - Protocol whose port status needs to  */
/*                                      updated.                             */
/*                      u1Status      - Protocol status (enabled/disable)    */
/*                                      status for the port.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
GarpL2IwfSetProtocolEnabledStatusOnPort (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                         UINT2 u2Protocol, UINT1 u1Status)
{
    return (L2IwfSetProtocolEnabledStatusOnPort (u4ContextId, u2LocalPortId,
                                                 u2Protocol, u1Status));
}

/*****************************************************************************/
/* Function Name      : GarpL2IwfGetPVlanMappingInfo                         */
/*                      This function gives the mapping between primary and  */
/*                      secondary vlan. This function will be invoked by     */
/*                      Spanning tree, GMRP, MMRP, IGS to know the type of   */
/*                      vlan and to get the primary to seconday vlan mapping */
/*                      and vice versa.                                      */
/*                                                                           */
/* Input(s)           : pL2PvlanMappingInfo - Pointer to the structure       */
/*                                            containing Private vlan mapping*/
/*                                            information. The following are */
/*                                            the fields in this structure   */
/*                                            that are input to this API.    */
/*                      u1RequestType - Type of request. It can take the     */
/*                                      following values:                    */
/*                                        1.L2IWF_VLAN_TYPE                  */
/*                                        2.L2IWF_MAPPED_VLANS               */
/*                                        3.L2IWF_NUM_MAPPED_VLANS           */
/*                                                                           */
/*                                      If L2IWF_VLAN_TYPE is passed, then   */
/*                                      pMappedVlans will not be filled, but */
/*                                      only value for pVlanType is filled.  */
/*                                                                           */
/*                                      If L2IWF_MAPPED_VLANS is passed,     */
/*                                      then both pVlanMapped Vlans and      */
/*                                      pVlanType are filled.                */
/*                                                                           */
/*                                      If L2IWF_NUM_MAPPED_VLANS is passed, */
/*                                      the the number of asscoiated vlans   */
/*                                      for the given vlan will be filled.   */
/*                      InVlanId - Id of the given Vlan.                     */
/*                                                                           */
/* Output(s)          : pL2PvlanMappingInfo - pointer to the structure       */
/*                                            containing Private vlan mapping*/
/*                                            information. The following are */
/*                                            the fields in this structure   */
/*                                            that are output to this API.   */
/*                                                                           */
/*                      pMappedVlans - If the InVlanId is a primary vlan,    */
/*                                     then this pointer points to an array  */
/*                                     containing the list of secondary vlans*/
/*                                     associated with that primary vlan.    */
/*                                                                           */
/*                                     If the InVlanId is a secondary vlan,  */
/*                                     this *pMappedVlans will contain the ID*/
/*                                     of primary vlan associated with the   */
/*                                     given secondary vlan (InVlanId).      */
/*                                                                           */
/*                                     If u1RequestTyps is                   */
/*                                     L2IWF_MAPPED_VLANS, then only this    */
/*                                     array will be filled. Otherwise this  */
/*                                     will not filled.                      */
/*                                                                           */
/*                      pu1VlanType - This gives the type of vlan of the     */
/*                                    given input vlan (InVlanId). Possible  */
/*                                    values for this parameter are:         */
/*                                      1.L2IWF_VLAN_TYPE_NORMAL,            */
/*                                      2.L2IWF_VLAN_TYPE_PRIMARY,           */
/*                                      3.L2IWF_VLAN_TYPE_ISOLATED,          */
/*                                      4.L2IWF_VLAN_TYPE_COMMUNITY          */
/*                                                                           */
/*                      pu2NumMappedVlans - Number of vlans present in the   */
/*                                          array pointed by pMappedVlans.   */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
GarpL2IwfGetPVlanMappingInfo (tL2PvlanMappingInfo * pL2PvlanMappingInfo)
{
    L2IwfGetPVlanMappingInfo (pL2PvlanMappingInfo);
}

/*****************************************************************************/
/* Function Name      : GarpLaGetMCLAGSystemStatus                           */
/*                                                                           */
/* Description        : This function is used to get the MCLAG system status */
/*                      whether MCLAG is enabled/disbaled in the system      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu1MCLAGSystemStatus                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_DISABLED - If MCLAG is disabled in the system     */
/*                      LA_ENABLED  - If MCLAG is enabled in the system      */
/*                                                                           */
/*****************************************************************************/
UINT1
GarpLaGetMCLAGSystemStatus (VOID)
{
#ifdef LA_WANTED
    return LaGetMCLAGSystemStatus ();
#else
    return LA_MCLAG_DISABLED;
#endif
}

/*****************************************************************************/
/* Function Name      : GarpIcchGetIcclIfIndex                               */
/*                                                                           */
/* Description        : This function is used to fetch the ICCL interface    */
/*                      index.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : *pu4IfIndex - ICCL interface index.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
GarpIcchGetIcclIfIndex (UINT4 *pu4IfIndex)
{
#ifdef ICCH_WANTED
    IcchGetIcclIfIndex (pu4IfIndex);
#else
	UNUSED_PARAM (pu4IfIndex);
#endif
	return ;    	
}
#endif
