/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2001-2002                                     */
/*   $Id: garpgip.c,v 1.25 2013/02/01 12:38:23 siva Exp $               */
/*                                                                      */
/*  FILE NAME             : garpgip.c                                   */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*  SUBSYSTEM NAME        : GIP                                         */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                                */
/*  DESCRIPTION           : This file contains Gip related routines     */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#include "garpinc.h"

/***************************************************************/
/*  Function Name   : GarpGipAppAttributeReqJoin               */
/*  Description     : This function is called when a VLAN_ADD  */
/*                   is encountered from Application should be */
/*                   treated as ReqJoin Event                  */
/*  Input(s)        : u1AppId - Application app id             */
/*                    pAttr - pointer to Garp Attribute        */
/*                     Structure                               */
/*                    u2GipId - GipId is vlan id for GMRP and  */
/*                     for GVRP it is equal to zero in base    */
/*                     spanning tree context                   */
/*                    AddPorts - Ports to be added             */
/*  Output(s)       : None                                     */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpGipAppAttributeReqJoin (UINT1 u1AppId,
                            tGarpAttr * pAttr,
                            UINT2 u2GipId, tLocalPortList AddPorts)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpGipEntry      *pGipEntry;
    tGarpIfMsg          IfMsg;
    INT4                i4Result;
    UINT1               u1Propagate = GARP_FALSE;

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {

        return;
    }

    for (pPortEntry = pAppEntry->pPortTable;
         pPortEntry != NULL; pPortEntry = pPortEntry->pNextNode)
    {
        GARP_IS_MEMBER_PORT (AddPorts, pPortEntry->u2Port, i4Result);

        /* 
         * Add a attribute entry if the port is a static port (i4Result == TRUE)
         * irrespective of whether the port is in Forwarding/Blocking state.
         */
        if (i4Result == GARP_TRUE)
        {
            pAttrEntry = NULL;

            pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

            if (NULL != pGipEntry)
            {
                pAttrEntry =
                    GarpGetAttributeEntry (pAppEntry, pGipEntry, pAttr);
            }

            if (pAttrEntry == NULL)
            {

                pAttrEntry = GarpGetNewAttributeEntry ();

                if (pAttrEntry == NULL)
                {

                    continue;
                }

                GarpInitAttributeEntry (pAttrEntry, pAttr,
                                        u1AppId, pPortEntry->u2Port, pGipEntry);

                pGipEntry =
                    GarpAddAttributeEntry (pAppEntry, pPortEntry, pGipEntry,
                                           pAttrEntry, u2GipId);

                if (NULL == pGipEntry)
                {

                    GarpFreeAttributeEntry (pAttrEntry);

                    continue;
                }
            }
            else
            {
                /* The Port is a member of the Added Port list */
                if (pAttrEntry->u1RegSemState == GARP_LV)
                {
                    GarpStopTimer (&pAttrEntry->LeaveTmr);

                    pAttrEntry->LeaveTmr.u1IsTmrActive = GARP_FALSE;
                }
            }

            pAttrEntry->u1AdmRegControl = GARP_REG_FIXED;
            pAttrEntry->u1RegSemState = GARP_IN;

            if (pGipEntry->u1StapStatus == GARP_FORWARDING)
            {
                /* 
                 * Atleast one of the added ports must be in Forwarding state
                 * to propagate the attribute. 
                 * 
                 * Otherwise, if all the static ports are not in Forwarding 
                 * state, propagating the attribute is useless, since frames
                 * received for that attribute from other bridges will never 
                 * be forwarded on these static ports.
                 *
                 * When these static ports move to Forwarding state, the 
                 * attribute will be propagated on all active ports.
                 */

                u1Propagate = GARP_TRUE;
            }
        }
    }

    if (u1Propagate == GARP_TRUE)
    {

        /* 
         * Some ports in the static port list is in Forwarding state.
         * Hence, propagate this attribute.
         */

        IfMsg.pAppEntry = pAppEntry;
        IfMsg.u2GipId = u2GipId;
        /* 
         * Set the port to invalid port for propagating the attribute on
         * all ports.
         */
        IfMsg.u2Port = GARP_INVALID_PORT;

        GarpGipPropagateOnAllPorts (pAttr, &IfMsg, GARP_SEND_JOIN);
    }
}

/******************************************************************/
/*  Function Name   : GarpGipAppAttributeReqLeave                 */
/*  Description     : This function is called when a VLAN_DELETE  */
/*                   is encountered from Application. Should be   */
/*                   treated as ReqLeave Event                    */
/*  Input(s)        : u1AppId - Application app id                */
/*                    pAttr - pointer to Garp Attribute           */
/*                     Structure                                  */
/*                    u2GipId - GipId is vlan id for GMRP and     */
/*                     for GVRP it is equal to zero in base       */
/*                     spanning tree context                      */
/*                    DelPorts - Ports to be deleted              */
/*  Output(s)       : None                                        */
/*  <OPTIONAL Fields>: None                                       */
/*  Global Variables Referred : None                              */
/*  Global variables Modified : None                              */
/*  Exceptions or Operating System Error Handling : None          */
/*  Use of Recursion : None                                       */
/*  Returns         : None                                        */
/******************************************************************/
void
GarpGipAppAttributeReqLeave (UINT1 u1AppId,
                             tGarpAttr * pAttr,
                             UINT2 u2GipId, tLocalPortList DelPorts)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tGarpAttrEntry     *pAttrEntry = NULL;
    tGarpGipEntry      *pGipEntry = NULL;
    UINT1               u1Result;
    UINT1               u1SendLeaveFlag = GARP_TRUE;
    UINT4               u4DelPortCount = 0;
    UINT4               u4DelPortBlockCount = 0;

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {

        return;
    }

    for (pPortEntry = pAppEntry->pPortTable;
         pPortEntry != NULL; pPortEntry = pPortEntry->pNextNode)
    {

        /* 
         * Propagate the attribute only on ports that belong 
         * to the given Gip Context.
         */

        pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

        if (pGipEntry == NULL)
        {
            /* This GIP is not associated with this port. */
            continue;
        }

        pAttrEntry = GarpGetAttributeEntry (pAppEntry, pGipEntry, pAttr);

        if (pAttrEntry != NULL)
        {

            GARP_IS_MEMBER_PORT (DelPorts, pPortEntry->u2Port, u1Result);

            if (u1Result == GARP_TRUE)
            {

                if (pAttrEntry->u1AdmRegControl != GARP_REG_NORMAL)
                {

                    pAttrEntry->u1AdmRegControl = GARP_REG_NORMAL;
                    pAttrEntry->u1RegSemState = GARP_MT;
                }

                u4DelPortCount++;

                if (pGipEntry->u1StapStatus == GARP_BLOCKING)
                {

                    u4DelPortBlockCount++;
                }
            }

            /* 
             * We need to check, if this attribute is associated statically
             * with any other port.
             */
            if ((pAttrEntry->u1AdmRegControl == GARP_REG_FIXED)
                && (pGipEntry->u1StapStatus == GARP_FORWARDING))
            {

                u1SendLeaveFlag = GARP_FALSE;
            }
        }
    }

    /* 
     * If all the deleted ports are in Blocking state, we dont need
     * to send Leave message on other ports. This is because, we would
     * not have propagated the messages, if all the deleted ports 
     * are in Blocking state.
     */
    if ((u4DelPortBlockCount != u4DelPortCount) &&
        (u1SendLeaveFlag == GARP_TRUE))
    {
        for (pPortEntry = pAppEntry->pPortTable;
             pPortEntry != NULL; pPortEntry = pPortEntry->pNextNode)
        {
            pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

            if (NULL == pGipEntry)
            {
                continue;
            }

            pAttrEntry = GarpGetAttributeEntry (pAppEntry, pGipEntry, pAttr);

            if (pAttrEntry != NULL)
            {

                GarpApplicantSem (pAttrEntry, pPortEntry, GARP_APP_REQ_LEAVE);

                GarpCheckAndDelAttrEntry (pAppEntry, pPortEntry,
                                          pGipEntry, pAttrEntry);
            }
        }
    }
}

/***************************************************************/
/*  Function Name   : GarpGipAppSetForbiddenPorts              */
/*  Description     :                                          */
/*  Input(s)        : u1AppId - Application id                 */
/*                    pAttr - Pointer to garp attribute table  */
/*                    u2GipId - GipId                          */
/*                    AddPorts - Ports to be added             */
/*                    DelPorts - ports to be deleted           */
/*  Output(s)       : Set application forbidden ports          */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpGipAppSetForbiddenPorts (UINT1 u1AppId,
                             tGarpAttr * pAttr,
                             UINT2 u2GipId,
                             tLocalPortList AddPorts, tLocalPortList DelPorts)
{
    UINT1              *pTmpDelPorts = NULL;
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tGarpAttrEntry     *pAttrEntry = NULL;
    tGarpGipEntry      *pGipEntry;
    INT4                i4Result;

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (pAppEntry == NULL)
    {

        return;
    }

    for (pPortEntry = pAppEntry->pPortTable;
         pPortEntry != NULL; pPortEntry = pPortEntry->pNextNode)
    {
        pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

        GARP_IS_MEMBER_PORT (AddPorts, pPortEntry->u2Port, i4Result);

        if (i4Result == GARP_TRUE)
        {
            /* Port is present in the Added list */
            pAttrEntry = NULL;

            if (NULL != pGipEntry)
            {
                pAttrEntry = GarpGetAttributeEntry (pAppEntry,
                                                    pGipEntry, pAttr);
            }

            if (pAttrEntry == NULL)
            {
                pAttrEntry = GarpGetNewAttributeEntry ();

                if (pAttrEntry == NULL)
                {
                    continue;
                }

                GarpInitAttributeEntry (pAttrEntry, pAttr,
                                        u1AppId, pPortEntry->u2Port, pGipEntry);

                pGipEntry = GarpAddAttributeEntry (pAppEntry, pPortEntry,
                                                   pGipEntry, pAttrEntry,
                                                   u2GipId);

                if (NULL == pGipEntry)
                {
                    GarpFreeAttributeEntry (pAttrEntry);

                    continue;
                }
            }

            if (pAttrEntry->u1RegSemState != GARP_MT)
            {
                if (pAttrEntry->u1RegSemState == GARP_LV)
                {
                    GarpStopTimer (&pAttrEntry->LeaveTmr);
                    pAttrEntry->LeaveTmr.u1IsTmrActive = GARP_FALSE;
                }

                /*
                 * If the Attribute was registered on this port,
                 * then give the Leave Indication to the Application.
                 */
                pAppEntry->pLeaveIndFn (pAttrEntry->Attr.u1AttrType,
                                        pAttrEntry->Attr.u1AttrLen,
                                        pAttrEntry->Attr.au1AttrVal,
                                        pPortEntry->u2Port, pGipEntry->u2GipId);

                if (pGipEntry->u1StapStatus == GARP_FORWARDING)
                {
                    /*
                     * If the Attribute was registered previously,
                     * then other ports would have been made Members. Since
                     * When the Port is made a Forbidden Port, we must
                     * indicate this to other ports, to make then Observers.
                     * GarpGipAppAttributeReqLeave (), will make other ports
                     * Observers, if there is no other port, that is statically
                     * configured.
                     */
                    pTmpDelPorts =
                        UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
                    if (pTmpDelPorts == NULL)
                    {
                        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                                  "GarpGipAppSetForbiddenPorts: "
                                  "Error in allocating memory for pTmpDelPorts\r\n");
                        return;
                    }
                    MEMSET (pTmpDelPorts, 0, sizeof (tLocalPortList));
                    GARP_SET_MEMBER_PORT (pTmpDelPorts, pPortEntry->u2Port,
                                          sizeof (tLocalPortList));

                    GarpGipAppAttributeReqLeave (pAppEntry->u1AppId,
                                                 &pAttrEntry->Attr,
                                                 pGipEntry->u2GipId,
                                                 pTmpDelPorts);
                    UtilPlstReleaseLocalPortList (pTmpDelPorts);
                }
            }

            pAttrEntry->u1AdmRegControl = GARP_REG_FORBIDDEN;
            pAttrEntry->u1RegSemState = GARP_MT;
        }
        else
        {

            GARP_IS_MEMBER_PORT (DelPorts, pPortEntry->u2Port, i4Result);

            if (i4Result == GARP_TRUE)
            {
                if (NULL == pGipEntry)
                {
                    continue;
                }

                pAttrEntry = GarpGetAttributeEntry (pAppEntry,
                                                    pGipEntry, pAttr);

                if (pAttrEntry != NULL)
                {

                    pAttrEntry->u1AdmRegControl = GARP_REG_NORMAL;

                    GarpCheckAndDelAttrEntry (pAppEntry, pPortEntry,
                                              pGipEntry, pAttrEntry);
                }
            }
        }
    }
}

/***************************************************************/
/*  Function Name   : GarpGipGidAttributeJoinInd               */
/*  Description     : This function indicates the Join message */
/*                    to the ports other than the source port  */
/*  Input(s)        :  pAttr - pointer to the Garp Attrribute  */
/*                     pIfMsg - Pointer to IfMsg Structure     */
/*  Output(s)       :  None                                    */
/*  <OPTIONAL Fields>: None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/

void
GarpGipGidAttributeJoinInd (tGarpAttr * pAttr, tGarpIfMsg * pIfMsg)
{
    UINT1              *pPortList = NULL;
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    UINT2               u2GipId;
    UINT1               u1Result = GARP_FAILURE;

    pAppEntry = pIfMsg->pAppEntry;
    u2GipId = pIfMsg->u2GipId;

    /* Propagate this Join on all other belonging to the given Gip. */

    if (pAppEntry->u1AppId == GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId)
    {
        pPortEntry = pAppEntry->pPortTable;

        while (pPortEntry != NULL)
        {
            if (pPortEntry->u2Port != pIfMsg->u2Port)
            {
                GarpGidGipAttributeJoinReq (pAttr, pPortEntry, pIfMsg);
            }

            pPortEntry = pPortEntry->pNextNode;
        }
    }
    else
    {
        pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pPortList == NULL)
        {
            GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                      "GarpGipGidAttributeJoinInd: Error in allocating memory "
                      "for pPortList\r\n");
            return;
        }
        MEMSET (pPortList, 0, sizeof (tLocalPortList));
        /* This is Vlan Context Gip */
        GarpGipGetPortList (pIfMsg->pAppEntry->u1AppId, u2GipId, pPortList);

        pPortEntry = pAppEntry->pPortTable;

        while (pPortEntry != NULL)
        {

            if (pPortEntry->u2Port != pIfMsg->u2Port)
            {

                GARP_IS_MEMBER_PORT (pPortList, pPortEntry->u2Port, u1Result);

                if (u1Result == GARP_TRUE)
                {

                    GarpGidGipAttributeJoinReq (pAttr, pPortEntry, pIfMsg);
                }
            }

            pPortEntry = pPortEntry->pNextNode;
        }
        UtilPlstReleaseLocalPortList (pPortList);
    }
}

/***************************************************************/
/*  Function Name   : GarpGipGidAttributeLeaveInd              */
/*  Description     : This function indicates the Leave message*/
/*                    to the ports other than the source port  */
/*  Input(s)        :  pAttr - pointer to the Garp Attrribute  */
/*                     pIfMsg - Pointer to IfMsg Structure     */
/*  Output(s)       :  None                                    */
/*  <OPTIONAL Fields>: None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpGipGidAttributeLeaveInd (tGarpAttr * pAttr, tGarpIfMsg * pIfMsg)
{
    UINT1              *pPortList = NULL;
    tGarpAppEntry      *pAppEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpAttrEntry     *pPrevAttrEntry = NULL;
    tGarpPortEntry     *pPortEntry;
    tGarpPortEntry     *pPrevPortEntry = NULL;
    tGarpGipEntry      *pGipEntry;
    INT4                i4RetVal;
    UINT2               u2GipId;
    UINT1               u1Result = GARP_FALSE;
    UINT1               u1RegCount = GARP_NO_REGS_FOR_LEAVEMSG;

    /* 
     * We will propagate this Leave only if one other port other than the 
     * source port has seen this registration. In this case we will send a 
     * LEAVE_EMPTY message on the other port alone. This is done to change 
     * the state of the participant to Observer. In case of no other port
     * having seen the registration we propagate LEAVE_EMPTY to all other
     * ports.
     */
    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList == NULL)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "GarpGipGidAttributeLeaveInd: Error in allocating memory "
                  "for pPortList\r\n");
        return;
    }

    MEMSET (pPortList, 0, sizeof (tLocalPortList));
    pAppEntry = pIfMsg->pAppEntry;
    u2GipId = pIfMsg->u2GipId;
    GarpGipGetPortList (pIfMsg->pAppEntry->u1AppId, u2GipId, pPortList);

    pPortEntry = pAppEntry->pPortTable;

    while (pPortEntry != NULL)
    {

        if (pPortEntry->u2Port != pIfMsg->u2Port)
        {
            GARP_IS_MEMBER_PORT (pPortList, pPortEntry->u2Port, u1Result);

            if (u1Result == GARP_TRUE)
            {
                i4RetVal
                    = GarpGetGipAndAttributeEntry (pAppEntry, pPortEntry,
                                                   pAttr, u2GipId,
                                                   &pAttrEntry, &pGipEntry);
                /*
                 * Checking out for the registration for the attributes
                 * in other ports and the counter is incremented
                 * accordingly
                 */
                if (i4RetVal == GARP_SUCCESS)
                {
                    /* Attribute exists on the GIP */
                    if ((pAttrEntry->u1RegSemState == GARP_IN) &&
                        (pGipEntry->u1StapStatus == GARP_FORWARDING))
                    {
                        /* Increment the no of registrations for that attribute */
                        u1RegCount++;

                        /* Store the port entry which had registration for the same 
                         * attribute. This will be used if u1RegCount == 1 */
                        pPrevPortEntry = pPortEntry;
                        pPrevAttrEntry = pAttrEntry;
                    }

                    if (u1RegCount > GARP_MIN_REGS_FOR_LEAVEMSG)
                    {
                        /* we are not bothered...we are going to ignore this LEAVE */
                        break;
                    }

                }
            }
        }

        pPortEntry = pPortEntry->pNextNode;
    }

    /*
     * if u1RegCount >= 2 - No need to propagate LEAVE
     * if u1RegCount == 1 - Propagate on other port which had seen that 
     *                      registration
     * if u1RegCount == 0 - Propagate LEAVE on all other ports
     */
    if (u1RegCount == GARP_NO_REGS_FOR_LEAVEMSG)
    {
        /* 
         * This attribute is not registered on any other ports...
         * So propagate LEAVE message to all other ports to make them
         *  Observers from Members 
         */
        pPortEntry = pAppEntry->pPortTable;

        while (pPortEntry != NULL)
        {
            if (pPortEntry->u2Port != pIfMsg->u2Port)
            {
                GARP_IS_MEMBER_PORT (pPortList, pPortEntry->u2Port, u1Result);
                if (u1Result == GARP_TRUE)
                {
                    GarpGidGipAttributeLeaveReq (pAttr, pPortEntry, pIfMsg);
                }
            }

            pPortEntry = pPortEntry->pNextNode;
        }
    }
    else if (u1RegCount == GARP_MIN_REGS_FOR_LEAVEMSG)
    {
        /* 
         * This attribute is registered on one port other than the port which 
         * received the LEAVE message. So propagate the LEAVE message on this
         * port if the admin reg control equals NORMAL. This is becos we will 
         * be a member (sending join ins for attribute reg on the same port)
         * on statically configured ports.
         */
        if (pPrevAttrEntry->u1AdmRegControl == GARP_REG_NORMAL)
        {
            GarpGidGipAttributeLeaveReq (pAttr, pPrevPortEntry, pIfMsg);
        }
    }
    UtilPlstReleaseLocalPortList (pPortList);
}

/***************************************************************/
/*  Function Name   : GarpGipPropagateOnAllPorts               */
/*  Description     : Propagate the information on all ports   */
/*  Input(s)        : pAttr - Pointer to garp attribute table  */
/*                    pIfMsg -Pointer to interface message     */
/*                            Structure                        */
/*                    SendMsgInd - Message to be Propagated    */
/*  Output(s)       : None                                     */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpGipPropagateOnAllPorts (tGarpAttr * pAttr,
                            tGarpIfMsg * pIfMsg, tGarpSendMsgInd SendMsgInd)
{
    tGarpPortEntry     *pPortEntry;
    UINT1              *pGipPortList = NULL;
    UINT1               u1Result;

    if ((pIfMsg == NULL) || (pAttr == NULL))
    {
        return;
    }
    pGipPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pGipPortList == NULL)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "GarpGipPropagateOnAllPorts: "
                  "Error in allocating memory for pGipPortList\r\n");
        return;
    }
    MEMSET (pGipPortList, 0, sizeof (tLocalPortList));

    GarpGipGetPortList (pIfMsg->pAppEntry->u1AppId,
                        pIfMsg->u2GipId, pGipPortList);

    pPortEntry = pIfMsg->pAppEntry->pPortTable;

    while (pPortEntry != NULL)
    {

        /* 
         * Propagate the attribute only on ports that belong 
         * to the given Gip Context.
         */

        GARP_IS_MEMBER_PORT (pGipPortList, pPortEntry->u2Port, u1Result);

        if (u1Result == GARP_TRUE)
        {

            if (pPortEntry->u2Port != pIfMsg->u2Port)
            {

                if (SendMsgInd == GARP_SEND_JOIN)
                {

                    GarpGidGipAttributeJoinReq (pAttr, pPortEntry, pIfMsg);
                }
                else
                {

                    GarpGidGipAttributeLeaveReq (pAttr, pPortEntry, pIfMsg);
                }
            }
        }

        pPortEntry = pPortEntry->pNextNode;
    }
    UtilPlstReleaseLocalPortList (pGipPortList);
}

/***************************************************************/
/*  Function Name   : GarpGipGetPortList                       */
/*  Description     : Get the port list for a given GipId      */
/*  Input(s)        : u1AppId - Application Index              */
/*                    u2GipId - GipId                          */
/*                    PortList - Port list structure to be     */
/*                            for the GIpId                    */
/*  Output(s)       : None                                     */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpGipGetPortList (UINT1 u1AppId, UINT2 u2GipId, tLocalPortList PortList)
{
    if (u1AppId == GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId)
    {
        MEMSET (PortList, 0xFF, sizeof (tLocalPortList));
    }
    else
    {
        MEMSET (PortList, 0, sizeof (tLocalPortList));
        GarpL2IwfMiGetVlanLocalEgressPorts
            (GARP_CURR_CONTEXT_PTR ()->u4ContextId, u2GipId, PortList);
    }
}

/**************************************************************************/
/*  Function Name    : GarpHandleGipUpdateGipPorts ()                     */
/*  Description      : This function transmits Join messages on the given */
/*                     port, for all the Attributes registered for the    */
/*                     given GipId.                                       */
/*                     This function is called by GVRP whenever a port    */
/*                     is learnt for a Vlan.                              */
/*                     Updation of Gip Port list affects only GMRP.       */
/*  Input(s)         : u2Port - The Port that is added to the             */
/*                              given GIP context.                        */
/*                     u2Gip  - Id of the GIP context for which           */
/*                              new port is added.                        */
/*  Output(s)        : None                                               */
/*  Global Variables                                                      */
/*  Referred         : None                                               */
/*  Global variables                                                      */
/*  Modified         : None                                               */
/*  Exceptions or OS                                                      */
/*  Error handling   : None                                               */
/*  Use of Recursion : None                                               */
/*  Returns          : None                                               */
/**************************************************************************/
VOID
GarpHandleGipUpdateGipPorts (UINT2 u2Port, UINT2 u2GipId, UINT1 u1Action)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;
    tGarpPortEntry     *pNewPortEntry;
    tGarpGipEntry      *pGipEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpIfMsg          IfMsg;
    UINT4               u4Index;

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {
        return;
    }

    pAppEntry = GARP_GET_APP_ENTRY (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId);

    if (pAppEntry == NULL)
    {
        return;
    }

    pNewPortEntry = GarpGetPortEntry (pAppEntry, u2Port);

    if (pNewPortEntry == NULL)
    {
        return;
    }

    if (u1Action == GARP_ADD)
    {
        pPortEntry = pAppEntry->pPortTable;

        while (pPortEntry != NULL)
        {
            pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

            if (pGipEntry != NULL)
            {
                if ((pPortEntry->u2Port == u2Port) ||
                    (pGipEntry->u1StapStatus == GARP_BLOCKING))
                {
                    pPortEntry = pPortEntry->pNextNode;
                    continue;
                }

                for (u4Index = 0; u4Index < pAppEntry->u2MaxBuckets; u4Index++)
                {
                    pAttrEntry = pGipEntry->papAttrTable[u4Index];
                    while (pAttrEntry != NULL)
                    {
                        if (pAttrEntry->u1RegSemState == GARP_IN)
                        {
                            IfMsg.pAppEntry = pAppEntry;
                            IfMsg.u2GipId = pGipEntry->u2GipId;
                            IfMsg.u2Port = pPortEntry->u2Port;

                            GarpGidGipAttributeJoinReq (&pAttrEntry->Attr,
                                                        pNewPortEntry, &IfMsg);
                        }

                        pAttrEntry = pAttrEntry->pNextHashNode;
                    }
                }
            }

            pPortEntry = pPortEntry->pNextNode;
        }
    }
    else if (u1Action == GARP_DELETE)
    {
        /* First do the steps that is done when a port moves 
         * to blocking state. */

        GarpPortStateChangedToBlocking (pAppEntry, pNewPortEntry, u2GipId);

        /* Remove all the attribute present in port for 
         * that Gip and finally remove the Gip from that port. */
        GarpCleanAndDeleteGipEntry (pAppEntry, pNewPortEntry, u2GipId);
    }
}
