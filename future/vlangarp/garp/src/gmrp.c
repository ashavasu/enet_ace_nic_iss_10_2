
/************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                 
 *
 * $Id: gmrp.c,v 1.52 2014/07/16 12:48:46 siva Exp $
 *
 * DESCRIPTION           : This file contains GMRP related routines     
 *
 ***********************************************************************/

/************************************************************************/
/* FILE NAME             : gmrp.c                                       */
/* PRINCIPAL AUTHOR      : Aricent Inc.                                 */
/* SUBSYSTEM NAME        : GMRP                                         */
/* MODULE NAME           : GARP                                         */
/* LANGUAGE              : C                                            */
/* TARGET ENVIRONMENT    : Any                                          */
/* DATE OF FIRST RELEASE : 26 Mar 2002                                  */
/* AUTHOR                : Aricent Inc.                                 */
/************************************************************************/
/*                                                                      */
/* Change History                                                       */
/* Version               : Initial                                      */
/* Date(DD/MM/YYYY)      : 26 Nov 2001                                  */
/* Modified by           : Vlan Team                                    */
/* Description of change : Initial Creation                             */
/************************************************************************/

#include "garpinc.h"
/****************************************************************/
/*  Function Name   :GmrpHandlePropagateMcastInfo               */
/*  Description     :depending on u1action we will add a static */
/*                   Multicast entry or delete an entry         */
/*  Input(s)        :pMacAddr - Pointer to mac address          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpHandlePropagateMcastInfo (tMacAddr MacAddr, tVlanId VlanId,
                              tLocalPortList AddPortList,
                              tLocalPortList DelPortList)
{
    UINT1              *pNullPortList = NULL;
    tGarpAttr           Attr;

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {
        GARP_TRC_ARG1 (GMRP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                       GMRP_NAME, "GMRP is NOT enabled. Cannot Propagate "
                       "for the Vlan Id %d, the Group Address ", VlanId);
        GARP_PRINT_MAC_ADDR (GMRP_MOD_TRC, MacAddr);

        return;
    }

    GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                   "Received Propagation Request: Vlan Id = %d, Group Address = ",
                   VlanId);
    GARP_PRINT_MAC_ADDR (GMRP_MOD_TRC, MacAddr);

    Attr.u1AttrType = GMRP_GROUP_ATTR_TYPE;
    Attr.u1AttrLen = GMRP_MAC_ADDR_LEN;

    MEMCPY (Attr.au1AttrVal, MacAddr, GMRP_MAC_ADDR_LEN);
    pNullPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pNullPortList == NULL)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "GmrpHandlePropagateMcastInfo: "
                  "Error in allocating memory for pNullPortList\r\n");
        return;
    }
    MEMSET (pNullPortList, 0, sizeof (tLocalPortList));
    if (MEMCMP (AddPortList, pNullPortList, sizeof (tLocalPortList)) != 0)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "\tAdded Ports = ");
        GARP_PRINT_PORT_LIST (GMRP_MOD_TRC, AddPortList);

        GarpGipAppAttributeReqJoin (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId,
                                    &Attr, VlanId, AddPortList);
    }

    if (MEMCMP (DelPortList, pNullPortList, sizeof (tLocalPortList)) != 0)
    {

        GARP_TRC (GMRP_MOD_TRC,
                  CONTROL_PLANE_TRC, GMRP_NAME, "\tDeleted Ports = ");
        GARP_PRINT_PORT_LIST (GMRP_MOD_TRC, DelPortList);

        GarpGipAppAttributeReqLeave (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId,
                                     &Attr, VlanId, DelPortList);
    }
    UtilPlstReleaseLocalPortList (pNullPortList);
}

/****************************************************************/
/*  Function Name   :GmrpHandleSetMcastForbiddenPorts           */
/*  Description     :This function set the forbidden ports for a*/
/*                   given multicast entry                      */
/*  Input(s)        :pMacAddr - Pointer to multicast mac address*/
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpHandleSetMcastForbiddenPorts (tMacAddr MacAddr, tVlanId VlanId,
                                  tLocalPortList AddPortList,
                                  tLocalPortList DelPortList)
{
    tGarpAttr           Attr;

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {

        GARP_TRC_ARG1 (GMRP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                       GMRP_NAME, "GMRP is NOT enabled. Cannot Set Forbidden "
                       "Ports for the Vlan Id %d and the Group Address ",
                       VlanId);
        GARP_PRINT_MAC_ADDR (GMRP_MOD_TRC, MacAddr);

        return;
    }

    GARP_TRC_ARG1 (GMRP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                   GMRP_NAME, "Received Forbidden Ports Set Request "
                   "for the Vlan Id %d and the Group Address ", VlanId);
    GARP_PRINT_MAC_ADDR (GMRP_MOD_TRC, MacAddr);

    GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME, "\tAdded Ports = ");
    GARP_PRINT_PORT_LIST (GMRP_MOD_TRC, AddPortList);

    GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME, "\tDeleted Ports = ");
    GARP_PRINT_PORT_LIST (GMRP_MOD_TRC, DelPortList);

    Attr.u1AttrType = GMRP_GROUP_ATTR_TYPE;
    Attr.u1AttrLen = GMRP_MAC_ADDR_LEN;

    MEMCPY (Attr.au1AttrVal, MacAddr, GMRP_MAC_ADDR_LEN);

    GarpGipAppSetForbiddenPorts (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId,
                                 &Attr, VlanId, AddPortList, DelPortList);
}

/****************************************************************/
/*  Function Name   :GmrpHandlePropagateDefGroupInfo            */
/*  Description     :This function propagates the default Group */
/*                   info to all the ports during initialization*/
/*  Input(s)        :u1Type - Whether it is VLAN_ALL_GROUPS or  */
/*                   VLAN_UNREG_GROUPS                          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpHandlePropagateDefGroupInfo (UINT1 u1Type, tVlanId VlanId,
                                 tLocalPortList AddPortList,
                                 tLocalPortList DelPortList)
{
#ifdef GARP_EXTENDED_FILTER
    UINT1              *pNullPortList = NULL;
    tGarpAttr           Attr;

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {

        GARP_TRC_ARG2 (GMRP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                       GMRP_NAME, "GMRP is NOT enabled. Cannot Propagate "
                       "Default Groups (Value = %d) behavior for the "
                       "Vlan Id %d\n", u1Type, VlanId);
        return;
    }

    Attr.u1AttrType = GMRP_SERVICE_REQ_ATTR_TYPE;
    Attr.u1AttrLen = GMRP_SERVICE_REQ_LEN;

    switch (u1Type)
    {

        case VLAN_ALL_GROUPS:

            GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                           "Received ALL Groups Propagation Request for "
                           "Vlan Id = %d\n", VlanId);

            Attr.au1AttrVal[0] = GMRP_ALL_GROUPS;
            break;

        case VLAN_UNREG_GROUPS:

            GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                           "Received UNREG Groups Propagation Request for "
                           "Vlan Id = %d\n", VlanId);

            Attr.au1AttrVal[0] = GMRP_UNREG_GROUPS;
            break;

        default:
            return;
    }

    pNullPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pNullPortList == NULL)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "GmrpHandlePropagateDefGroupInfo: "
                  "Error in allocating memory for pNullPortList\r\n");
        return;
    }
    MEMSET (pNullPortList, 0, sizeof (tLocalPortList));

    if (MEMCMP (AddPortList, pNullPortList, sizeof (tLocalPortList)) != 0)
    {

        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "\tAdded Ports = ");
        GARP_PRINT_PORT_LIST (GMRP_MOD_TRC, AddPortList);

        GarpGipAppAttributeReqJoin (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId,
                                    &Attr, VlanId, AddPortList);
    }

    if (MEMCMP (DelPortList, pNullPortList, sizeof (tLocalPortList)) != 0)
    {

        GARP_TRC (GMRP_MOD_TRC,
                  CONTROL_PLANE_TRC, GMRP_NAME, "\tDeleted Ports = ");
        GARP_PRINT_PORT_LIST (GMRP_MOD_TRC, DelPortList);

        GarpGipAppAttributeReqLeave (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId,
                                     &Attr, VlanId, DelPortList);
    }
#else
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
#endif /* EXTENDED_FILTERING_CHANGE */
    UtilPlstReleaseLocalPortList (pNullPortList);
}

/****************************************************************/
/*  Function Name   :GmrpHandleSetDefGroupForbiddenPorts        */
/*  Description     :depending on u1action we will add a static */
/*                   entry or delete it                         */
/*  Input(s)        :pMacAddr - Pointer to Mac address          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpHandleSetDefGroupForbiddenPorts (UINT1 u1Type, tVlanId VlanId,
                                     tLocalPortList AddPortList,
                                     tLocalPortList DelPortList)
{
#ifdef GARP_EXTENDED_FILTER
    tGarpAttr           Attr;

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {

        GARP_TRC_ARG2 (GMRP_MOD_TRC, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                       GMRP_NAME, "GMRP is NOT enabled. Cannot Set Default "
                       "Groups (Value = %d) Forbidden Ports for the "
                       "Vlan Id %d\n", u1Type, VlanId);
        return;
    }

    Attr.u1AttrType = GMRP_SERVICE_REQ_ATTR_TYPE;
    Attr.u1AttrLen = GMRP_SERVICE_REQ_LEN;

    switch (u1Type)
    {

        case VLAN_ALL_GROUPS:

            GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                           "Rcvd ALL Groups Forbidden Ports Set Request for "
                           "Vlan Id = %d\n", VlanId);

            Attr.au1AttrVal[0] = GMRP_ALL_GROUPS;
            break;

        case VLAN_UNREG_GROUPS:

            GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                           "Received UNREG Groups Forbidden Ports Set Request for "
                           "Vlan Id = %d\n", VlanId);

            Attr.au1AttrVal[0] = GMRP_UNREG_GROUPS;
            break;

        default:
            return;
    }

    GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME, "\tAdded Ports = ");
    GARP_PRINT_PORT_LIST (GMRP_MOD_TRC, AddPortList);

    GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME, "\tDeleted Ports = ");
    GARP_PRINT_PORT_LIST (GMRP_MOD_TRC, DelPortList);

    GarpGipAppSetForbiddenPorts (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId,
                                 &Attr, VlanId, AddPortList, DelPortList);
#else
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
#endif /* EXTENDED_FILTERING_CHANGE */
}

/***************************************************************/
/*  Function Name   :GmrpInit ()                                      */
/*  Description     :This function Initialises Gmrp Application*/
/*  Input(s)        :void                                      */
/*  Output(s)       :INT4                                      */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GMRP_SUCCESS, if gmrp is Initialised     */
/*                   succesfully,Otherwise GMRP_FAILURE        */
/***************************************************************/
INT4
GmrpInit (void)
{
    UINT2               u2Port;
    UINT1               u1TunnelStatus = 0;

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        GARP_TRC (GMRP_MOD_TRC, INIT_SHUT_TRC, GMRP_NAME,
                  "GMRP initialization failed. GARP is not enabled \n");

        return GMRP_FAILURE;
    }

    if (GMRP_ADMIN_STATUS () == GMRP_ENABLED)
    {
        GARP_TRC (GMRP_MOD_TRC, INIT_SHUT_TRC, GMRP_NAME,
                  "GMRP is already enabled \n");
        return GMRP_SUCCESS;
    }

    if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
    {
        /* GMRP is disabled in provider core and edge bridges. */
        GMRP_ADMIN_STATUS () = GMRP_DISABLED;

        for (u2Port = 1; u2Port <= GARP_MAX_PORTS_PER_CONTEXT; u2Port++)
        {
            /*
             * Disable Gmrp on all ports. 
             */
            GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status =
                GMRP_DISABLED;

            GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].
                u1RestrictedRegControl = GMRP_DISABLED;

        }

        gau1GmrpStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] = GMRP_DISABLED;
        /* oper status will be enabled when the node becomes active */

        return GMRP_SUCCESS;

    }

    /* Make the admin status as disabled. For non-default contexts, garp will
     * not be started and gvrp/gmrp will not be enabled on context creation.
     * Gvrp/ Gmrp will be enabled only when they are enabled explcitly for
     * non-default contexts. If admin status is set to enabled, then the snmp
     * routine for gvrp/gmrp enable will not enable the application. So set
     * the admin status as disabled. */
    GMRP_ADMIN_STATUS () = GMRP_DISABLED;

    for (u2Port = 1; u2Port <= GARP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        /*
         * By default, GMRP is enabled on all Ports.
         * Actual enabled indication will be given when the port is created.
         */

        GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status =
            GMRP_ENABLED;

        if (GARP_PORT_ENTRY (u2Port) != NULL)
        {
            /* In case of customer bridge mode, If the Protocol tunnel 
             * status is tunnel/discard, GMRP should be disabled on that port */
            if (GARP_BRIDGE_MODE () == GARP_CUSTOMER_BRIDGE_MODE)
            {
                GarpL2IwfGetProtocolTunnelStatusOnPort
                    (GARP_GET_IFINDEX (u2Port), L2_PROTO_GMRP, &u1TunnelStatus);

                if (u1TunnelStatus != VLAN_TUNNEL_PROTOCOL_PEER)
                {
                    GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status =
                        GMRP_DISABLED;
                }
            }

            /* In case of provider bridge mode, If the port vlan tunnel 
             * status is enabled, GMRP should be disabled on that port */
            if (GARP_BRIDGE_MODE () == GARP_PROVIDER_BRIDGE_MODE)
            {
                GarpL2IwfGetPortVlanTunnelStatus ((UINT2)
                                                  GARP_GET_IFINDEX (u2Port),
                                                  (BOOL1 *) & u1TunnelStatus);

                if (u1TunnelStatus == OSIX_TRUE)
                {
                    GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status =
                        GMRP_DISABLED;
                }
            }

        }

        GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].
            u1RestrictedRegControl = GMRP_DISABLED;

    }

    gau1GmrpStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] = GMRP_DISABLED;
    /* oper status will be enabled when the node becomes active */

    return GMRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   :GmrpEnable                                       */
/*  Description     :This function enables Gmrp application.   */
/*  Input(s)        :void                                      */
/*  Output(s)       :INT4                                      */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : GMRP_SUCCESS, if gmrp is enabled.        */
/*                    GMRP_FAILURE otherwise                   */
/***************************************************************/
INT4
GmrpEnable (void)
{
    INT4                i4RetVal;
    UINT2               u2Port;
    tGarpGlobalPortEntry *pGlobalPortEntry;

    tGarpAppnFn         AppnFn =
        { GmrpJoinIndFn, GmrpLeaveIndFn, GmrpTypeValidateFn,
        GmrpAttrRegValidateFn, GmrpWildCardAttrRegValidateFn
    };

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        return GMRP_FAILURE;
    }

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_TRUE)
    {

        return GMRP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (GARP_NODE_STATUS () == GARP_NODE_ACTIVE)
    {
        if (VlanFsMiVlanHwGmrpEnable (GARP_CURR_CONTEXT_PTR ()->u4ContextId) !=
            FNP_SUCCESS)
        {
            return GMRP_FAILURE;
        }
    }
#endif

    i4RetVal =
        GarpAppEnrol (gGmrpAddr, AppnFn,
                      &GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId);

    if (i4RetVal == GARP_FAILURE)
    {

        GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId = GARP_INVALID_APP_ID;

        return GMRP_FAILURE;
    }

    GarpUpdateGarpAppStatusOnAllPorts (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                       GMRP_ENABLED, GMRP_APP_ID);
    for (u2Port = 1; u2Port <= GARP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pGlobalPortEntry = GARP_PORT_ENTRY (u2Port);
        if (GARP_IS_PORT_KNOWN (u2Port) == GARP_TRUE)
        {
            /*
             * Create Application specific port entries in GARP iff GARP
             * is aware of that port.
             */
            i4RetVal =
                GarpAppAddPort (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId, u2Port);

            if (i4RetVal == GARP_FAILURE)
            {
                GARP_TRC (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                          "DeEnrolling GMRP with GARP \n");
                GarpAppDeEnrol (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId);

                GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId = GARP_INVALID_APP_ID;

                return GMRP_FAILURE;
            }

            if (GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status ==
                GMRP_ENABLED)
            {
                /* Enable GMRP on this Port */
                i4RetVal =
                    GarpAppEnablePort (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId,
                                       u2Port);

                if (i4RetVal == GARP_FAILURE)
                {
                    GARP_TRC (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                              "DeEnrolling GMRP with GARP \n");
                    GarpAppDeEnrol (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId);

                    GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId = GARP_INVALID_APP_ID;

                    return GMRP_FAILURE;
                }
            }

            if (GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].
                u1RestrictedRegControl == GMRP_ENABLED)
            {
                i4RetVal =
                    GarpAppEnablePortRestrictedRegControl (GARP_CURR_CONTEXT_PTR
                                                           ()->u1GmrpAppId,
                                                           u2Port);
                if (i4RetVal == GARP_FAILURE)
                {
                    GARP_TRC_ARG1 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                                   "Enabling Restricted Group Registration on Port %d failed\n",
                                   pGlobalPortEntry->u4IfIndex);
                    return GMRP_FAILURE;
                }
            }
        }
    }

    gau1GmrpStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] = GMRP_ENABLED;

    GARP_TRC (GARP_MOD_TRC, INIT_SHUT_TRC, GARP_NAME,
              "GMRP Registered with GARP successfully \n");

    /* 
     * Static multicast information might be present in
     * Vlan module, before Gmrp is enabled. So need to
     * indicate Vlan to propagate the multicast information.
     */
    GarpVlanGmrpEnableInd (GARP_CURR_CONTEXT_PTR ()->u4ContextId);
    return GMRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   :GmrpDisable                                      */
/*  Description     :This function disables Gmrp Application   */
/*  Input(s)        :void                                      */
/*  Output(s)       :INT4                                      */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : GMRP_SUCCESS, if gmrp is disabled.       */
/*                    GMRP_FAILURE otherwise                   */
/***************************************************************/
INT4
GmrpDisable (void)
{

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        return GVRP_SUCCESS;
    }

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {

        return GMRP_SUCCESS;
    }

    GarpUpdateGarpAppStatusOnAllPorts (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                       GMRP_DISABLED, GMRP_APP_ID);
#ifdef NPAPI_WANTED
    if (VlanFsMiVlanHwGmrpDisable (GARP_CURR_CONTEXT_PTR ()->u4ContextId)
        != FNP_SUCCESS)
    {
        return GMRP_FAILURE;
    }
#endif
    GarpVlanMiDeleteAllDynamicMcastInfo
        (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
         VLAN_INVALID_PORT, VLAN_ADDR_FMLY_ALL);
#ifdef GARP_EXTENDED_FILTER
    GarpVlanDeleteAllDynamicDefGroupInfo (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                          VLAN_INVALID_PORT);
#endif /*  GARP_EXTENDED_FILTER */

    GarpAppDeEnrol (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId);

    GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId = GARP_INVALID_APP_ID;
    gau1GmrpStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] = GMRP_DISABLED;

    /* Should be invoked only after invoking VlanMiDeleteAllDynamicMcastInfo
     * so that the Delete ALL update message is sent */
    GarpVlanGmrpDisableInd (GARP_CURR_CONTEXT_PTR ()->u4ContextId);
    return GMRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GmrpAddPort                              */
/*  Description     : A port is to be added to gmrp Application*/
/*  Input(s)        : u2port - port number of port to be added */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : gpGarpContextInfo->u1GmrpStatus                  */
/*  Global variables Modified : gpGarpContextInfo->u1GmrpStatus                  */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
INT4
GmrpAddPort (UINT2 u2Port)
{
    INT4                i4RetVal;

    if ((u2Port == 0) || (u2Port > GARP_MAX_PORTS_PER_CONTEXT))
    {

        return GMRP_FAILURE;
    }

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {

        return GMRP_SUCCESS;
    }

    /* 
     * Port would have been already created in GARP.
     * Port entry for this APP will be created even if GMRP on this port
     * is disabled.
     */

    i4RetVal = GarpAppAddPort (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId, u2Port);

    if (i4RetVal == GARP_FAILURE)
    {

        return GMRP_FAILURE;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status ==
        GMRP_ENABLED)
    {
        /* GMRP is enabled on this port */
        i4RetVal =
            GarpAppEnablePort (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId, u2Port);
        if (i4RetVal == GARP_FAILURE)
        {
            GARP_TRC_ARG1 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                           "Enabling GMRP on Port %d failed\n",
                           GARP_GET_IFINDEX (u2Port));
            return GMRP_FAILURE;
        }
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].
        u1RestrictedRegControl == GMRP_ENABLED)
    {
        i4RetVal =
            GarpAppEnablePortRestrictedRegControl (GARP_CURR_CONTEXT_PTR ()->
                                                   u1GmrpAppId, u2Port);
        if (i4RetVal == GARP_FAILURE)
        {
            GARP_TRC_ARG1 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                           "Enabling Restricted Group Registration on Port %d failed\n",
                           GARP_GET_IFINDEX (u2Port));
            return GMRP_FAILURE;
        }
    }

    return GMRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GmrpDelPort ()                           */
/*  Description     : A port is to be added to gmrp Application*/
/*  Input(s)        : u2port - port number of port to be added */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : gpGarpContextInfo->u1GmrpStatus                  */
/*  Global variables Modified : gpGarpContextInfo->u1GmrpStatus                  */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
INT4
GmrpDelPort (UINT2 u2Port)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;

    if ((u2Port == 0) || (u2Port > GARP_MAX_PORTS_PER_CONTEXT))
    {

        return GMRP_FAILURE;
    }

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {
        return GMRP_SUCCESS;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status ==
        GMRP_DISABLED)
    {

        return GMRP_SUCCESS;
    }

    pGlobalPortEntry = GARP_PORT_ENTRY (u2Port);

    if (pGlobalPortEntry == NULL)
    {
        return GMRP_SUCCESS;
    }

    GarpVlanMiDeleteAllDynamicMcastInfo (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                         (UINT2) pGlobalPortEntry->u4IfIndex,
                                         VLAN_ADDR_FMLY_ALL);
#ifdef GARP_EXTENDED_FILTER
    GarpVlanDeleteAllDynamicDefGroupInfo (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                          u2Port);
#endif /* GARP_EXTENDED_FILTER */

    GarpAppDelPort (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId, u2Port);

    return GMRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GmrpEnablePort                           */
/*  Description     : A port is to be enabled for gmrp         */
/*                    Application                              */
/*  Input(s)        : u2port - port number of port to be added */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : gpGarpContextInfo->u1GmrpStatus                  */
/*  Global variables Modified : gpGarpContextInfo->u1GmrpStatus                  */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
INT4
GmrpEnablePort (UINT2 u2Port)
{
    INT4                i4RetVal;

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {
        GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status =
            GMRP_ENABLED;

        /* Inform L2Iwf, that gmrp is enabled on this port. */
        GarpL2IwfSetProtocolEnabledStatusOnPort (GARP_CURR_CONTEXT_ID (),
                                                 u2Port, L2_PROTO_GMRP,
                                                 OSIX_ENABLED);

        return GMRP_SUCCESS;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status ==
        GMRP_ENABLED)
    {
        return GMRP_SUCCESS;
    }

    if (GARP_IS_PORT_KNOWN (u2Port) == GARP_FALSE)
    {
        /* GARP is not aware of this port */
        GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status =
            GMRP_ENABLED;
        /* Inform L2Iwf, that gmrp is enabled on this port. */
        GarpL2IwfSetProtocolEnabledStatusOnPort (GARP_CURR_CONTEXT_ID (),
                                                 u2Port, L2_PROTO_GMRP,
                                                 OSIX_ENABLED);
        return GMRP_SUCCESS;
    }

    i4RetVal =
        GarpAppEnablePort (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId, u2Port);

    if (i4RetVal == GARP_FAILURE)
    {
        GARP_TRC_ARG1 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                       "Enabling GMRP on Port %d failed\n",
                       GARP_GET_IFINDEX (u2Port));
        return GMRP_FAILURE;
    }

    GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status = GMRP_ENABLED;
    /* Inform L2Iwf, that gmrp is enabled on this port. */
    GarpL2IwfSetProtocolEnabledStatusOnPort (GARP_CURR_CONTEXT_ID (),
                                             u2Port, L2_PROTO_GMRP,
                                             OSIX_ENABLED);

    return GMRP_SUCCESS;
}

/****************************************************************/
/*  Function Name   : GmrpDisablePort ()                        */
/*  Description     : A port has to be disabled from the gmrp   */
/*                    Application                               */
/*  Input(s)        : u2port - port number of port to be added  */
/*  Output(s)       : None                                      */
/*  Global Variables Referred : gpGarpContextInfo->u1GmrpStatus                   */
/*  Global variables Modified : gpGarpContextInfo->u1GmrpStatus                   */
/*  Exceptions or Operating System Error Handling : None        */
/*  Use of Recursion : None                                     */
/*  Returns         : None                                      */
/****************************************************************/
INT4
GmrpDisablePort (UINT2 u2Port)
{
    tGarpGlobalPortEntry *pGlobalPortEntry;

    pGlobalPortEntry = GARP_PORT_ENTRY (u2Port);

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {
        GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status =
            GMRP_DISABLED;
        GarpL2IwfSetProtocolEnabledStatusOnPort (GARP_CURR_CONTEXT_ID (),
                                                 u2Port, L2_PROTO_GMRP,
                                                 OSIX_DISABLED);
        return GMRP_SUCCESS;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status ==
        GMRP_DISABLED)
    {
        return GMRP_SUCCESS;
    }
    GarpVlanMiDeleteAllDynamicMcastInfo (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                         (UINT2) pGlobalPortEntry->u4IfIndex,
                                         VLAN_ADDR_FMLY_ALL);
#ifdef GARP_EXTENDED_FILTER
    GarpVlanDeleteAllDynamicDefGroupInfo (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                          u2Port);
#endif /* GARP_EXTENDED_FILTER */

    GarpAppDisablePort (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId, u2Port);

    GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1Status = GMRP_DISABLED;
    /* Inform L2Iwf, that gmrp is disabled on this port. */
    GarpL2IwfSetProtocolEnabledStatusOnPort (GARP_CURR_CONTEXT_ID (),
                                             u2Port, L2_PROTO_GMRP,
                                             OSIX_DISABLED);

    return GMRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GmrpGetNumRegFailed                      */
/*  Description     :This function returns the number of failed*/
/*                          Gmrp registrations on a given port.      */
/*  Input(s)        :                                          */
/*                    u2port - Port Number for which the number*/
/*                    of failed registration is to be got      */
/*                    pu4NumRegFailed - pointer to the value of*/
/*                    number of failed registrations           */
/*  Output(s)       : pu4NumRegFailed is initialised to the    */
/*                    number of failed regitration on sucess   */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GMRP_SUCCESS on successful extraction of */
/*                    number of registration failures.Else     */
/*                    return GMRP_FAILURE                      */
/***************************************************************/
INT4
GmrpGetNumRegFailed (UINT2 u2Port, UINT4 *pu4NumRegFailed)
{
    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {

        return GMRP_FAILURE;
    }

    if (GarpGetNumRegFailed
        (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId, u2Port,
         pu4NumRegFailed) == GARP_FAILURE)
    {

        return GMRP_FAILURE;
    }

    return GMRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GmrpGetLastPduOrigin                     */
/*  Description     :This function returns the source address  */
/*                   of the  last packet recieved              */
/*  Input(s)        :                                          */
/*                   u2Port - Port number of the incoming port */
/*                   pMacAddr - Pointer to the output mac      */
/*                   Address of this function                  */
/*  Output(s)       : pMacAddr is initialised with the result  */
/*  <OPTIONAL Fields>:                                         */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : GMRP_SUCCESS on succesful updation of    */
/*                    LastPdu mac address else GMRP_FAILURE    */
/***************************************************************/
INT4
GmrpGetLastPduOrigin (UINT2 u2Port, tMacAddr MacAddr)
{
    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {

        return GMRP_FAILURE;
    }

    if (GarpGetLastPduOrigin
        (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId, u2Port,
         MacAddr) == GARP_FAILURE)
    {

        return GMRP_FAILURE;
    }

    return GMRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GmrpJoinIndFn                            */
/*  Description     :Registered function for JoinInd Event from*/
/*                        upper layer                          */
/*  Input(s)        :u1AttrType - Attribute type               */
/*                   u2Port - Port number of the incoming port */
/*                   u1AttrLen - Length of the attribute value * 
 *                   pu1AttrVal - Pointer to attribute value   */
/*                   u2Port - Port number of the incoming port */
/*                   u2GipId - GipId                           */
/*  Output(s)       : pMacAddr is initialised with the result  */
/*  <OPTIONAL Fields>:                                         */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
INT4
GmrpJoinIndFn (UINT1 u1AttrType,
               UINT1 u1AttrLen, UINT1 *pu1AttrVal, UINT2 u2Port, UINT2 u2GipId)
{
    tMacAddr            MacAddr;
#ifdef GARP_EXTENDED_FILTER
    UINT1               u1Type;
#endif
    INT4                i4RetVal = VLAN_FAILURE;

    switch (u1AttrType)
    {

        case GMRP_GROUP_ATTR_TYPE:

            if (u1AttrLen == GMRP_MAC_ADDR_LEN)
            {

                MEMCPY (MacAddr, pu1AttrVal, GMRP_MAC_ADDR_LEN);

                i4RetVal = GarpVlanMiUpdateDynamicMcastInfo
                    (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                     MacAddr, (tVlanId) u2GipId,
                     (UINT2) GARP_GET_IFINDEX (u2Port), VLAN_ADD);

                if (i4RetVal == VLAN_FAILURE)
                {

                    GARP_TRC_ARG2 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                                   "Failed to update Dynamic Multicast"
                                   "Information for vlan %d on Port %d\n",
                                   (tVlanId) u2GipId,
                                   GARP_GET_IFINDEX (u2Port));

                    return GARP_FAILURE;
                }
                /* Update vlan database for pvlan also */
                if (GmrpProcessPvlanJoinOrLeaveInd
                    (GARP_CURR_CONTEXT_PTR ()->u4ContextId, MacAddr,
                     (tVlanId) u2GipId, (UINT2) GARP_GET_IFINDEX (u2Port),
                     VLAN_ADD) == GARP_FAILURE)
                {
                    return GARP_FAILURE;
                }

            }

            GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                           "Join Ind received on Port %d for Group Address.\n",
                           GARP_GET_IFINDEX (u2Port));
            GARP_PRINT_MAC_ADDR (GMRP_MOD_TRC, MacAddr);

            break;

#ifdef GARP_EXTENDED_FILTER

        case GMRP_SERVICE_REQ_ATTR_TYPE:

            if (u1AttrLen == GMRP_SERVICE_REQ_LEN)
            {

                if (*pu1AttrVal == GMRP_ALL_GROUPS)
                {

                    u1Type = VLAN_ALL_GROUPS;

                    GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                                   "Join Ind received for ALL Groups on Port %d\n",
                                   GARP_GET_IFINDEX (u2Port));
                }
                else
                {

                    u1Type = VLAN_UNREG_GROUPS;

                    GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                                   "Join Ind received for ALL UnReg Groups on "
                                   "Port %d\n", GARP_GET_IFINDEX (u2Port));
                }

                i4RetVal = GarpVlanUpdateDynamicDefGroupInfo
                    (GARP_CURR_CONTEXT_PTR ()->u4ContextId, u1Type,
                     (tVlanId) u2GipId, u2Port, VLAN_ADD);
                if (i4RetVal == VLAN_FAILURE)
                {
                    GARP_TRC_ARG2 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                                   "Failed to update Dynamic Default Group"
                                   "information for Vlan %d on Port %d\n",
                                   (tVlanId) u2GipId,
                                   GARP_GET_IFINDEX (u2Port));

                    return GARP_FAILURE;
                }
            }

            break;
#endif /* GARP_EXTENDED_FILTER */

        default:
            break;
    }
    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GmrpLeaveIndFn                           */
/*  Description     :Registered function for LeaveInd Event    */
/*                   from upper layer                          */
/*  Input(s)        :u1AttrType - Attribute type               */
/*                   u2Port - Port number of the incoming port */
/*                   u1AttrLen - Length of the attribute value * 
 *                   pu1AttrVal - Pointer to attribute value   */
/*                   u2Port - Port number of the incoming port */
/*                   u2GipId - GipId                           */
/*  Output(s)       : pMacAddr is initialised with the result  */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
INT4
GmrpLeaveIndFn (UINT1 u1AttrType,
                UINT1 u1AttrLen, UINT1 *pu1AttrVal, UINT2 u2Port, UINT2 u2GipId)
{
    tMacAddr            MacAddr;
#ifdef GARP_EXTENDED_FILTER
    UINT1               u1Type;
#endif
    INT4                i4RetVal = VLAN_FAILURE;

    switch (u1AttrType)
    {

        case GMRP_GROUP_ATTR_TYPE:

            if (u1AttrLen == GMRP_MAC_ADDR_LEN)
            {

                MEMCPY (MacAddr, pu1AttrVal, GMRP_MAC_ADDR_LEN);

                GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                               "Leave Ind received on Port %d for Group Address.\n",
                               GARP_GET_IFINDEX (u2Port));
                GARP_PRINT_MAC_ADDR (GMRP_MOD_TRC, MacAddr);

                i4RetVal = GarpVlanMiUpdateDynamicMcastInfo
                    (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                     MacAddr, (tVlanId) u2GipId,
                     (UINT2) GARP_GET_IFINDEX (u2Port), VLAN_DELETE);

                if (i4RetVal == VLAN_FAILURE)
                {
                    GARP_TRC_ARG2 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                                   "Failed to update Dynamic Multicast"
                                   "information for vlan %d on Port %d\n",
                                   (tVlanId) u2GipId,
                                   GARP_GET_IFINDEX (u2Port));

                    return GARP_FAILURE;
                }
                /* Update vlan database for pvlan also */
                if (GmrpProcessPvlanJoinOrLeaveInd
                    (GARP_CURR_CONTEXT_PTR ()->u4ContextId, MacAddr,
                     (tVlanId) u2GipId, (UINT2) GARP_GET_IFINDEX (u2Port),
                     VLAN_DELETE) == GARP_FAILURE)
                {
                    return GARP_FAILURE;
                }

            }
            break;

#ifdef GARP_EXTENDED_FILTER

        case GMRP_SERVICE_REQ_ATTR_TYPE:

            if (u1AttrLen == GMRP_SERVICE_REQ_LEN)
            {

                if (*pu1AttrVal == GMRP_ALL_GROUPS)
                {

                    u1Type = VLAN_ALL_GROUPS;

                    GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                                   "Leave Ind received for ALL Groups on Port %d\n",
                                   GARP_GET_IFINDEX (u2Port));
                }
                else
                {

                    u1Type = VLAN_UNREG_GROUPS;

                    GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                                   "Leave Ind received for ALL UnReg Groups on Port %d\n",
                                   GARP_GET_IFINDEX (u2Port));
                }

                i4RetVal = GarpVlanUpdateDynamicDefGroupInfo
                    (GARP_CURR_CONTEXT_PTR ()->u4ContextId, u1Type,
                     (tVlanId) u2GipId, u2Port, VLAN_DELETE);

                if (i4RetVal == VLAN_FAILURE)
                {
                    GARP_TRC_ARG2 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                                   "Failed to update Dynamic Default Group"
                                   "information for Vlan %d on Port %d\n",
                                   (tVlanId) u2GipId,
                                   GARP_GET_IFINDEX (u2Port));

                    return GARP_FAILURE;

                }
            }
            break;
#endif /* GARP_EXTENDED_FILTER */

        default:

            break;
    }
    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GmrpTypeValidateFn                       */
/*  Description     : This function is called by GARP module   */
/*                    to check whether the attribute type is a */
/*                    valid GMRP attribute type.               */
/*  Input(s)        :u1AttrType - Attribute type               */
/*  Output(s)       : None.                                    */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_TRUE / GARP_FALSE                   */
/***************************************************************/
INT4
GmrpTypeValidateFn (UINT1 u1AttrType)
{

    if ((u1AttrType == GMRP_GROUP_ATTR_TYPE) ||
        (u1AttrType == GMRP_SERVICE_REQ_ATTR_TYPE))
    {

        return GARP_TRUE;
    }

    return GARP_FALSE;
}

/***************************************************************/
/*  Function Name   : GmrpEnablePortRestrictedRegControl        */
/*  Description     : A port is to be enabled for Restricted   */
/*                    Group Registration Control               */
/*  Input(s)        : u2port - port number of port to be added */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : GARP_CURR_CONTEXT_PTR()->u1GmrpStatus                  */
/*  Global variables Modified : gpGarpContextInfo->aGmrpPortTable                */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
INT4
GmrpEnablePortRestrictedRegControl (UINT2 u2Port)
{
    INT4                i4RetVal;

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {
        GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].
            u1RestrictedRegControl = GMRP_ENABLED;
        return GMRP_SUCCESS;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].
        u1RestrictedRegControl == GMRP_ENABLED)
    {
        return GMRP_SUCCESS;
    }

    if (GARP_IS_PORT_KNOWN (u2Port) == GARP_FALSE)
    {
        /* GARP is not aware of this port */
        GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].
            u1RestrictedRegControl = GMRP_ENABLED;
        return GMRP_SUCCESS;
    }

    i4RetVal =
        GarpAppEnablePortRestrictedRegControl (GARP_CURR_CONTEXT_PTR ()->
                                               u1GmrpAppId, u2Port);

    if (i4RetVal == GARP_FAILURE)
    {
        GARP_TRC_ARG1 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                       "Enabling Restricted Group Registration on Port %d failed\n",
                       GARP_GET_IFINDEX (u2Port));
        return GMRP_FAILURE;
    }

    GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1RestrictedRegControl =
        GMRP_ENABLED;

    return GMRP_SUCCESS;
}

/****************************************************************/
/*  Function Name   : GmrpDisablePortRestrictedRegControl ()     */
/*  Description     : A port has to be disabled of Registration */
/*                    Control                                   */
/*  Input(s)        : u2port - port number of port to be added  */
/*  Output(s)       : None                                      */
/*  Global Variables Referred : GARP_CURR_CONTEXT_PTR()->u1GmrpStatus                   */
/*  Global variables Modified : gpGarpContextInfo->aGmrpPortTable                 */
/*  Exceptions or Operating System Error Handling : None        */
/*  Use of Recursion : None                                     */
/*  Returns         : None                                      */
/****************************************************************/
INT4
GmrpDisablePortRestrictedRegControl (UINT2 u2Port)
{

    INT4                i4RetVal;
    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (GARP_CURR_CONTEXT_ID ()) == GMRP_FALSE)
    {
        GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].
            u1RestrictedRegControl = GMRP_DISABLED;
        return GMRP_SUCCESS;
    }

    if (GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].
        u1RestrictedRegControl == GMRP_DISABLED)
    {
        return GMRP_SUCCESS;
    }

    if (GARP_IS_PORT_KNOWN (u2Port) == GARP_FALSE)
    {
        /* GARP is not aware of this port */
        GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].
            u1RestrictedRegControl = GMRP_DISABLED;
        return GMRP_SUCCESS;
    }

    i4RetVal =
        GarpAppDisablePortRestrictedRegControl (GARP_CURR_CONTEXT_PTR ()->
                                                u1GmrpAppId, u2Port);

    if (i4RetVal == GARP_FAILURE)
    {
        GARP_TRC_ARG1 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                       "Disabling Restricted Group Registration on Port %d failed\n",
                       GARP_GET_IFINDEX (u2Port));
        return GMRP_FAILURE;
    }

    GARP_CURR_CONTEXT_PTR ()->aGmrpPortTable[u2Port].u1RestrictedRegControl =
        GMRP_DISABLED;

    return GMRP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GmrpAttrRegValidateFn                       */
/*  Description     : This function calls a function in VLAN   */
/*                    module and checks if the attribute is    */
/*                    already registered statically and if     */
/*                    registration is NORMAL for the attribute */
/*                    on u2Port                                */
/*                                                             */
/*  Input(s)        : Attr - Structure containing attribute    */
/*                    type,value,length                        */
/*                                                             */
/*                    u2Port - Port Number on which            */
/*                    Registration NORMAL for attribute        */
/*                    contained in Attr to be checked          */
/*                                                             */
/*                    u2GipId - GIP Id                         */
/*                                                             */
/*  Output(s)       : None                                     */
/*                                                             */
/*  Global Variables Referred : None                           */
/*                                                             */
/*  Global variables Modified : None                           */
/*                                                             */
/*  Exceptions or Operating System Error Handling : None       */
/*                                                             */
/*  Use of Recursion : None                                    */
/*                                                             */
/*  Returns         : GMRP_TRUE/GMRP_FALSE                     */
/***************************************************************/

INT4
GmrpAttrRegValidateFn (tGarpAttr Attr, UINT2 u2Port, UINT2 u2GipId)
{
    tMacAddr            MacAddr;
#ifdef GARP_EXTENDED_FILTER
    UINT1               u1Type;
    UINT1               u1ServiceAttrVal;
#endif
    INT4                i4RetVal = GMRP_FALSE;

    switch (Attr.u1AttrType)
    {

        case GMRP_GROUP_ATTR_TYPE:

            if (Attr.u1AttrLen == GMRP_MAC_ADDR_LEN)
            {

                MEMCPY (MacAddr, Attr.au1AttrVal, GMRP_MAC_ADDR_LEN);
                i4RetVal =
                    GarpVlanIsMcastStaticAndRegNormal
                    (GARP_CURR_CONTEXT_PTR ()->u4ContextId, MacAddr,
                     u2GipId, u2Port);
                if (i4RetVal == VLAN_TRUE)
                {
                    i4RetVal = GMRP_TRUE;
                }
                else
                {
                    i4RetVal = GMRP_FALSE;
                }

            }

            break;
#ifdef GARP_EXTENDED_FILTER
        case GMRP_SERVICE_REQ_ATTR_TYPE:

            if (Attr.u1AttrLen == GMRP_SERVICE_REQ_LEN)
            {
                MEMCPY (&u1ServiceAttrVal, Attr.au1AttrVal,
                        GMRP_SERVICE_REQ_LEN);

                if (u1ServiceAttrVal == GMRP_ALL_GROUPS)
                {

                    u1Type = VLAN_ALL_GROUPS;

                    GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                                   "Join Ind received for ALL Groups on Port %d\n",
                                   GARP_GET_IFINDEX (u2Port));
                }
                else
                {

                    u1Type = VLAN_UNREG_GROUPS;

                    GARP_TRC_ARG1 (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                                   "Join Ind received for ALL UnReg Groups on "
                                   "Port %d\n", GARP_GET_IFINDEX (u2Port));
                }
                i4RetVal =
                    GarpVlanIsServiceAttrStaticAndRegNormal
                    (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                     u1Type, u2GipId, u2Port);
                if (i4RetVal == VLAN_TRUE)
                {
                    i4RetVal = GMRP_TRUE;
                }
                else
                {
                    i4RetVal = GMRP_FALSE;
                }

            }
            break;
#endif
        default:
            break;
    }
    return i4RetVal;
}

/***************************************************************/
/*  Function Name   : GmrpIsGmrpEnabled                        */
/*  Description     : Returns the GMRP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Global Variables Referred : gpGarpContextInfo->u1GmrpStatus                  */
/*  Global variables Modified :                                */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GMRP_ENABLED /GMRP_DISABLED              */
/***************************************************************/
INT4
GmrpIsGmrpEnabled ()
{
    return ((gau1GmrpStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] ==
             GMRP_ENABLED) ? GMRP_TRUE : GMRP_FALSE);
}

/***************************************************************/
/*  Function Name   : GmrpWildCardAttrRegValidateFn            */
/*  Description     : This function calls a function in VLAN   */
/*                    module and checks if the attribute is    */
/*                    already registered as wild card entry    */
/*                    Leraning will not be allowed for Mcast   */
/*                    Mac address,if wild card entry is present*/
/*                                                             */
/*                    Learning allowed : GMRP_TRUE             */
/*                    Learning not allowed : GMRP_FALSE        */
/*                                                             */
/*  Input(s)        : Attr - Structure containing attribute    */
/*                    type,value,length                        */
/*                                                             */
/*                    u2Port - Port Number on which            */
/*                    Registration for attribute               */
/*                    contained in Attr to be checked          */
/*                                                             */
/*                    u2GipId - GIP Id                         */
/*                                                             */
/*  Output(s)       : None                                     */
/*                                                             */
/*  Global Variables Referred : None                           */
/*                                                             */
/*  Global variables Modified : None                           */
/*                                                             */
/*  Exceptions or Operating System Error Handling : None       */
/*                                                             */
/*  Use of Recursion : None                                    */
/*                                                             */
/*  Returns         : GMRP_TRUE/GMRP_FALSE                     */
/***************************************************************/

INT4
GmrpWildCardAttrRegValidateFn (tGarpAttr Attr, UINT2 u2Port, UINT2 u2GipId)
{
    tMacAddr            MacAddr;
    INT4                i4RetVal = OSIX_TRUE;

    UNUSED_PARAM (u2Port);

    if (Attr.u1AttrType == GMRP_GROUP_ATTR_TYPE)
    {
        if (Attr.u1AttrLen == GMRP_MAC_ADDR_LEN)
        {

            MEMCPY (MacAddr, Attr.au1AttrVal, GMRP_MAC_ADDR_LEN);

            i4RetVal = VlanIsMcastWildCardEntryPresent
                (GARP_CURR_CONTEXT_PTR ()->u4ContextId, u2GipId, MacAddr);

        }
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GmrpProcessPvlanJoinOrLeaveInd                   */
/*                                                                           */
/*    Description         : When Join comes for multicast group in primary   */
/*                          vlan, then this function will be invoked to      */
/*                          program that multicast group in all associated   */
/*                          secondary vlans.                                 */
/*                          Similary when a multicast group is removed from  */
/*                          a primary vlan, this function will be invoked    */
/*                          to remove the same group from all associated     */
/*                          secondary vlans.                                 */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          MacAddr -  MacAddress                            */
/*                          VlanId   - VlanId associated with the multicast  */
/*                                     MacAddress                            */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the port will be added */
/*                                     into the PortList.                    */
/*                                                                           */
/*                                     VLAN_DELETE, then the port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : GARP_SUCCESS                                      */
/*                         GARP_FAILURE                                      */
/*****************************************************************************/
INT4
GmrpProcessPvlanJoinOrLeaveInd (UINT4 u4ContextId, tMacAddr MacAddr,
                                tVlanId GipId, UINT2 u2Port, UINT1 u1Action)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT2              *pu2SecVlan = NULL;
    UINT2               u2VlanId = 0;

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    pu2SecVlan = MemAllocMemBlk (gGarpPoolIds.GarpGlobPvlanPoolId);

    if (pu2SecVlan == NULL)
    {
        return GARP_FAILURE;
    }
    MEMSET (pu2SecVlan, 0, (VLAN_MAX_COMMUNITY_VLANS + 1));
    L2PvlanMappingInfo.u4ContextId = u4ContextId;
    L2PvlanMappingInfo.InVlanId = GipId;
    L2PvlanMappingInfo.pMappedVlans = pu2SecVlan;
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    GarpL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if (L2PvlanMappingInfo.u1VlanType != L2IWF_NORMAL_VLAN)
    {
        for (u2VlanId = 0; u2VlanId < L2PvlanMappingInfo.u2NumMappedVlans;
             u2VlanId++)
        {
            if (GarpVlanMiUpdateDynamicMcastInfo
                (u4ContextId, MacAddr,
                 L2PvlanMappingInfo.pMappedVlans[u2VlanId],
                 u2Port, u1Action) == VLAN_FAILURE)
            {
                MemReleaseMemBlock (gGarpPoolIds.GarpGlobPvlanPoolId,
                                    (UINT1 *) pu2SecVlan);
                pu2SecVlan = NULL;
                return GARP_FAILURE;
            }
        }
    }

    MemReleaseMemBlock (gGarpPoolIds.GarpGlobPvlanPoolId, (UINT1 *) pu2SecVlan);
    pu2SecVlan = NULL;

    return GARP_SUCCESS;
}
