/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: garputl.c,v 1.64 2016/08/20 09:41:02 siva Exp $
 *
 * Description     : This file contains util routines for GARP 
 *
 *******************************************************************/

#include "garpinc.h"

/************************************************************************/
/* Function Name    : GarpGetNewAppEntry ()                             */
/*                                                                      */
/* Description      : Obtains the new Application Entry                 */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : pu1AppId - Pointer to the App Id value            */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gpGarpContextInfo->aGarpAppTable                                    */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Pointer to the App Entry if new entry is found    */
/*                    NULL otherwise                                    */
/************************************************************************/
tGarpAppEntry      *
GarpGetNewAppEntry (UINT1 *pu1AppId)
{
    tGarpAppEntry      *pAppEntry;
    UINT1               u1Index;

    for (u1Index = 1; u1Index <= GARP_MAX_APPS; u1Index++)
    {

        pAppEntry = &GARP_CURR_CONTEXT_PTR ()->aGarpAppTable[u1Index];

        if (pAppEntry->u1Status == GARP_INVALID)
        {

            *pu1AppId = u1Index;
            return pAppEntry;
        }
    }

    return NULL;
}

/************************************************************************/
/* Function Name    : GarpGetAppEntryWithAddr ()                        */
/*                                                                      */
/* Description      : Retrieves the App Entry with the matching         */
/*                    Application address                               */
/*                                                                      */
/* Input(s)         : pAppAddress - Pointer to the Application Address  */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gpGarpContextInfo->aGarpAppTable                                    */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Pointer to the App Entry if entry is found        */
/*                    NULL otherwise                                    */
/************************************************************************/
tGarpAppEntry      *
GarpGetAppEntryWithAddr (tMacAddr AppAddress)
{
    tGarpAppEntry      *pAppEntry = NULL;
    UINT1               u1Index;

    for (u1Index = 1; u1Index <= GARP_MAX_APPS; u1Index++)
    {

        pAppEntry = &GARP_CURR_CONTEXT_PTR ()->aGarpAppTable[u1Index];

        if (pAppEntry->u1Status == GARP_VALID)
        {
            if (GarpCmpMacAddr (pAppEntry->AppAddress, AppAddress)
                == GARP_EQUAL)
            {
                return pAppEntry;
            }
        }
    }

    return NULL;
}

/************************************************************************/
/* Function Name    : GarpGetNewPortEntry                               */
/*                                                                      */
/* Description      : Gets a new Port Entry.                            */
/*                                                                      */
/* Input(s)         : None.                                             */
/*                                                                      */
/* Output(s)        : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : pPorEntry - Pointer to the free Port entry.       */
/*                    NULL on failure.                                  */
/************************************************************************/
tGarpPortEntry     *
GarpGetNewPortEntry (UINT1 u1AppId)
{
    tGarpPortEntry     *pPortEntry;
    UINT4               u4MaxSize;

    pPortEntry = (tGarpPortEntry *) (VOID *) GARP_GET_BUFF (GARP_PORT_BUFF,
                                                            sizeof
                                                            (tGarpPortEntry));

    if (pPortEntry == NULL)
    {

        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "\n\n\n!!!!!!!!!!! Free port entry NOT available "
                  "!!!!!!!!!! \n\n\n\n");

        return NULL;
    }

    MEMSET (pPortEntry, 0, sizeof (tGarpPortEntry));

    GARP_HASH_MAX_GIP_BUCKETS (u1AppId, u4MaxSize);

    pPortEntry->pGipTable = GarpCreateAppPortGipHashTable (u4MaxSize);

    if (pPortEntry->pGipTable == NULL)
    {
        GarpFreePortEntry (pPortEntry);
        return NULL;
    }

    return pPortEntry;
}

/************************************************************************/
/* Function Name    : GarpFreePortEntry ()                              */
/*                                                                      */
/* Description      : Releases the Port Entry to the free pool          */
/*                                                                      */
/* Input(s)         : Pointer to the Port entry to be released.         */
/*                                                                      */
/* Output(s)        : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None.                                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None.                                             */
/*                                                                      */
/* Use of Recursion : None.                                             */
/*                                                                      */
/* Returns          : VOID                                              */
/************************************************************************/
VOID
GarpFreePortEntry (tGarpPortEntry * pPortEntry)
{
    GARP_RELEASE_BUFF (GARP_PORT_BUFF, (UINT1 *) pPortEntry);
}

/************************************************************************/
/* Function Name    : GarpGetPortEntry ()                               */
/*                                                                      */
/* Description      : Obtains the Port entry matching the given Port.   */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to the App Entry.             */
/*                    u2Port    - The port number                       */
/*                                                                      */
/* Output(s)        : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None.                                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None.                                             */
/*                                                                      */
/* Use of Recursion : None.                                             */
/*                                                                      */
/* Returns          : Port entry pointer if successful                  */
/*                    NULL otherwise.                                   */
/************************************************************************/
tGarpPortEntry     *
GarpGetPortEntry (tGarpAppEntry * pAppEntry, UINT2 u2Port)
{
    tGarpPortEntry     *pPortEntry;

    pPortEntry = pAppEntry->pPortTable;

    while (pPortEntry != NULL)
    {

        if (pPortEntry->u2Port == u2Port)
        {

            return pPortEntry;
        }

        pPortEntry = pPortEntry->pNextNode;
    }

    return NULL;
}

/************************************************************************/
/* Function Name    : GarpDeletePortEntry ()                            */
/*                                                                      */
/* Description      : Deletes the given Port entry from the Port table  */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to the App Entry              */
/*                    pNode     - Pointer to the Port Entry             */
/*                                                                      */
/* Output(s)        : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None.                                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None.                                             */
/*                                                                      */
/* Use of Recursion : None.                                             */
/*                                                                      */
/* Returns          : VOID                                              */
/************************************************************************/
VOID
GarpDeletePortEntry (tGarpAppEntry * pAppEntry, tGarpPortEntry * pNode)
{
    tGarpPortEntry     *pCurrNode;

    if (pAppEntry->pPortTable == pNode)
    {

        pAppEntry->pPortTable = pNode->pNextNode;

        return;
    }

    pCurrNode = pAppEntry->pPortTable;

    while (pCurrNode != NULL)
    {

        if (pCurrNode->pNextNode == pNode)
        {

            pCurrNode->pNextNode = pNode->pNextNode;
            return;
        }

        pCurrNode = pCurrNode->pNextNode;
    }
}

/************************************************************************/
/* Function Name    : GarpGetNewAttributeEntry ()                       */
/*                                                                      */
/* Description      : Obtains a new Attribute Entry                     */
/*                                                                      */
/* Input(s)         : None.                                             */
/*                                                                      */
/* Output(s)        : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None.                                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None.                                             */
/*                                                                      */
/* Use of Recursion : None.                                             */
/*                                                                      */
/* Returns          : Pointer to the new attribute entry if successful  */
/*                    NULL otherwise                                    */
/************************************************************************/
tGarpAttrEntry     *
GarpGetNewAttributeEntry (VOID)
{
    tGarpAttrEntry     *pAttrEntry;

    pAttrEntry = (tGarpAttrEntry *) (VOID *)
        GARP_GET_BUFF (GARP_ATTR_BUFF, sizeof (tGarpAttrEntry));

    if (pAttrEntry == NULL)
    {

        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "\n\n\n!!!!!!!!!!! Free Attribute entry NOT available "
                  "!!!!!!!!!! \n\n\n\n");

        return NULL;
    }

    MEMSET (pAttrEntry, 0, sizeof (tGarpAttrEntry));

    return pAttrEntry;
}

/************************************************************************/
/* Function Name    : GarpFreeAttributeEntry ()                         */
/*                                                                      */
/* Description      : Releases the Attribute Entry to the free pool     */
/*                                                                      */
/* Input(s)         : pAttrEntry - Pointer to the Attribute Entry       */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : VOID                                              */
/************************************************************************/
VOID
GarpFreeAttributeEntry (tGarpAttrEntry * pAttrEntry)
{
    GARP_RELEASE_BUFF (GARP_ATTR_BUFF, (UINT1 *) pAttrEntry);
}

/************************************************************************/
/* Function Name    : GarpInitAttributeEntry ()                         */
/*                                                                      */
/* Description      : Initialises the Attribute Entry                   */
/*                                                                      */
/* Input(s)         : pAttrEntry - Pointer to the Attribute Entry       */
/*                    pAttr      - Pointer the Attribute information    */
/*                    u1AppId    - Application Identifier               */
/*                    u2Port     - The Port on which this Attribute     */
/*                                 is registered                        */
/*                    pGipEntry  - Pointer to the Gip Entry             */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : VOID                                              */
/************************************************************************/
VOID
GarpInitAttributeEntry (tGarpAttrEntry * pAttrEntry,
                        tGarpAttr * pAttr,
                        UINT1 u1AppId, UINT2 u2Port, tGarpGipEntry * pGipEntry)
{
    pAttrEntry->Attr.u1AttrType = pAttr->u1AttrType;
    pAttrEntry->Attr.u1AttrLen = pAttr->u1AttrLen;

    MEMCPY (pAttrEntry->Attr.au1AttrVal, pAttr->au1AttrVal, pAttr->u1AttrLen);

    pAttrEntry->LeaveTmr.pRec = (void *) pAttrEntry;
    pAttrEntry->LeaveTmr.u2Port = u2Port;
    pAttrEntry->LeaveTmr.u1AppId = u1AppId;
    pAttrEntry->LeaveTmr.u1TmrType = GARP_LEAVE_TIMER;
    pAttrEntry->LeaveTmr.u1IsTmrActive = GARP_FALSE;

    pAttrEntry->pGipEntry = pGipEntry;
    pAttrEntry->u1RegSemState = GARP_MT;
    pAttrEntry->u1AppSemState = GARP_VO;
    pAttrEntry->u1AdmRegControl = GARP_REG_NORMAL;
}

/************************************************************************/
/* Function Name    : GarpGetAttributeEntry ()                          */
/*                                                                      */
/* Description      : Obtains the Attribute Entry matching the given    */
/*                    Attribute and the given GipId.                    */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the Application Entry     */
/*                    pGipEntry  - Pointer to the Gip Entry             */
/*                    pAttr      - Pointer to the Attribute Information */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Attribute Entry pointer if entry is found         */
/*                    NULL otherwise.                                   */
/************************************************************************/
tGarpAttrEntry     *
GarpGetAttributeEntry (tGarpAppEntry * pAppEntry, tGarpGipEntry * pGipEntry,
                       tGarpAttr * pAttr)
{
    tGarpAttrEntry     *pAttrEntry;
    UINT4               u4HashIndex;

    u4HashIndex = GarpGetAttributeHashIndex (pAppEntry, pAttr);

    pAttrEntry = pGipEntry->papAttrTable[u4HashIndex];

    while (pAttrEntry != NULL)
    {

        if ((pAttrEntry->Attr.u1AttrType == pAttr->u1AttrType)
            && (pAttrEntry->Attr.u1AttrLen == pAttr->u1AttrLen)
            && (MEMCMP (pAttrEntry->Attr.au1AttrVal,
                        pAttr->au1AttrVal, pAttr->u1AttrLen) == 0))
        {

            return pAttrEntry;
        }

        pAttrEntry = pAttrEntry->pNextHashNode;
    }

    return NULL;
}

/************************************************************************/
/* Function Name    : GarpAddAttributeEntry ()                          */
/*                                                                      */
/* Description      : Adds the given Attribute Entry in the Attribute   */
/*                    Hash Table.                                       */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the Application Entry     */
/*                  : pPortEntry - Pointer to the Port Entry            */
/*                    pGipEntry  - Pointer to the Gip Entry             */
/*                    pAttrEntry - Pointer to the Attribute Entry       */
/*                    u2GipId    - GipId                                */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : pGipEntry to which the attribute entry is added   */
/*                    NULL if the attribute cannot be added             */
/************************************************************************/
tGarpGipEntry      *
GarpAddAttributeEntry (tGarpAppEntry * pAppEntry, tGarpPortEntry * pPortEntry,
                       tGarpGipEntry * pGipEntry, tGarpAttrEntry * pAttrEntry,
                       UINT2 u2GipId)
{
    UINT4               u4HashIndex;
    UINT1               u1PortState;
    tGarpGlobalPortEntry *pGlobalPortEntry;

    if (pGipEntry == NULL)
    {

        pGipEntry = GarpGetNewGipEntry (pAppEntry);

        if (pGipEntry == NULL)
        {

            return NULL;
        }

        pGipEntry->u4NumEntries = 0;
        /* 
         * The contents of pGipEntry already initialsed with 0's.
         * So dont memset here. papAttrTable in pGipEntry will be lost if
         * u memset here !!!
         */

        pGipEntry->u2GipId = u2GipId;

        /* Default STAP status is made as FORWARDING */
        pGipEntry->pGarpPortEntry = pPortEntry;
        pGlobalPortEntry = GARP_PORT_ENTRY (pPortEntry->u2Port);

        if (pAppEntry->u1AppId == GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId)
        {
            u1PortState = GarpGetInstPortState (u2GipId, pGlobalPortEntry);
        }
        else
        {
            u1PortState = GarpGetVlanPortState (u2GipId, pGlobalPortEntry);
        }

        if (u1PortState == AST_PORT_STATE_FORWARDING)
        {
            u1PortState = GARP_FORWARDING;
        }
        else
        {
            u1PortState = GARP_BLOCKING;
        }

        pGipEntry->u1StapStatus = u1PortState;

        /* Add Gip entry to the Port Gip Hash Table */
        if (GARP_SUCCESS == GarpAddGipEntryToGipHashTable
            (pPortEntry->pGipTable, pGipEntry))
        {
            pPortEntry->u4NumGipEntries++;

            /* Add Gip entry to App Gip Hash TAble */
            GarpAddGipEntryFromAppGipTable (pAppEntry, pGipEntry);
        }
    }

    u4HashIndex = GarpGetAttributeHashIndex (pAppEntry, &pAttrEntry->Attr);

    pAttrEntry->pGipEntry = pGipEntry;
    pAttrEntry->pNextHashNode = pGipEntry->papAttrTable[u4HashIndex];
    pGipEntry->papAttrTable[u4HashIndex] = pAttrEntry;

    pGipEntry->u4NumEntries++;

    return pGipEntry;
}

/************************************************************************/
/* Function Name    : GarpCheckAndDelAttrEntry ()                       */
/*                                                                      */
/* Description      : Deletes the Attribute Entry from the Attribute    */
/*                    Table if the Applicant is an Observer and the     */
/*                    Registrar sem is in GARP_MT state.                */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the Application Entry     */
/*                  : pPortEntry - Pointer to the Port Entry            */
/*                    pGipEntry  - Pointer to the Gip Entry             */
/*                    pAttrEntry - Pointer to the Attribute Entry       */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
INT4
GarpCheckAndDelAttrEntry (tGarpAppEntry * pAppEntry,
                          tGarpPortEntry * pPortEntry,
                          tGarpGipEntry * pGipEntry,
                          tGarpAttrEntry * pAttrEntry)
{

    if (pAttrEntry->u1AdmRegControl == GARP_REG_NORMAL)
    {

        /* 
         * The Attribute entry must be deleted only when the Reg sem state 
         * is in GARP_MT state and the App sem is in GARP_VO state
         */

        if ((pAttrEntry->u1RegSemState == GARP_MT)
            && (pAttrEntry->u1AppSemState == GARP_VO))
        {
            GarpDeleteAttributeEntry (pAppEntry, pPortEntry,
                                      &pGipEntry, pAttrEntry);

            return GARP_TRUE;
        }
    }
    return GARP_FALSE;
}

/************************************************************************/
/* Function Name    : GarpDeleteAttributeEntry ()                       */
/*                                                                      */
/* Description      : Deletes the Attribute Entry from the Attribute    */
/*                    Table                                             */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the Application Entry     */
/*                  : pPortEntry - Pointer to the Port Entry            */
/*                    pGipEntry  - Pointer to the Gip Entry             */
/*                    pAttrEntry - Pointer to the Attribute Entry       */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
void
GarpDeleteAttributeEntry (tGarpAppEntry * pAppEntry,
                          tGarpPortEntry * pPortEntry,
                          tGarpGipEntry ** ppGipEntry,
                          tGarpAttrEntry * pAttrEntry)
{
    tGarpGipEntry      *pTempGipEntry;
    tGarpAttrEntry     *pCurrAttrEntry;
    tGarpAttrEntry     *pPrevAttrEntry = NULL;
    UINT4               u4HashIndex;

    pTempGipEntry = *ppGipEntry;

    if (pTempGipEntry == NULL)
    {
        return;
    }
    u4HashIndex = GarpGetAttributeHashIndex (pAppEntry, &pAttrEntry->Attr);

    pCurrAttrEntry = pTempGipEntry->papAttrTable[u4HashIndex];

    while (pCurrAttrEntry != NULL)
    {

        if (pCurrAttrEntry == pAttrEntry)
        {

            if (pPrevAttrEntry == NULL)
            {
                pTempGipEntry->papAttrTable[u4HashIndex] =
                    pAttrEntry->pNextHashNode;
            }
            else
            {

                pPrevAttrEntry->pNextHashNode = pAttrEntry->pNextHashNode;
            }

            pTempGipEntry->u4NumEntries--;

            if (pTempGipEntry->u4NumEntries == 0)
            {
                GarpDeleteGipEntry (pAppEntry, pPortEntry, pTempGipEntry);

                *ppGipEntry = NULL;
            }

            GarpFreeAttributeEntry (pAttrEntry);

            break;
        }

        pPrevAttrEntry = pCurrAttrEntry;
        pCurrAttrEntry = pCurrAttrEntry->pNextHashNode;
    }
}

/************************************************************************/
/* Function Name    : GarpDeleteAllAttrEntries ()                       */
/*                                                                      */
/* Description      : Deletes all attribute entries present in the      */
/*                    Attribute table.                                  */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the Application Entry     */
/*                  : pPortEntry - Pointer to the Port Entry            */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
VOID
GarpDeleteAllAttrEntries (tGarpAppEntry * pAppEntry,
                          tGarpPortEntry * pPortEntry)
{
    UINT4               u4Index;
    tGarpGipEntry      *pGipEntry;
    tGarpGipEntry      *pTempGipEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpAttrEntry     *pTempAttrEntry;
    UINT4               u4HashIndex = 0;

    if (pPortEntry->pGipTable != NULL)
    {
        TMO_HASH_Scan_Table (pPortEntry->pGipTable, u4HashIndex)
        {
            GARP_HASH_DYN_Scan_Bucket (pPortEntry->pGipTable, u4HashIndex,
                                       pGipEntry, pTempGipEntry,
                                       tGarpGipEntry *)
            {
                for (u4Index = 0; u4Index < pAppEntry->u2MaxBuckets; u4Index++)
                {
                    pAttrEntry = pGipEntry->papAttrTable[u4Index];

                    while (pAttrEntry != NULL)
                    {
                        if (pAttrEntry->u1RegSemState == GARP_LV)
                        {
                            GarpStopTimer (&pAttrEntry->LeaveTmr);
                            pAttrEntry->LeaveTmr.u1IsTmrActive = GARP_FALSE;
                        }

                        pTempAttrEntry = pAttrEntry;
                        pAttrEntry = pAttrEntry->pNextHashNode;

                        GarpFreeAttributeEntry (pTempAttrEntry);
                    }
                    pGipEntry->papAttrTable[u4Index] = NULL;
                }

                GarpDelGipEntryFromAppGipTable (pAppEntry, pGipEntry);
            }
        }
    }
    /* Now free the Gip entries and the Gip Hash Table */
    GarpDeleteAppPortGipHashTable (pPortEntry);
}

/************************************************************************/
/* Function Name    : GarpGetNewGipEntry ()                             */
/*                                                                      */
/* Description      : Gets a new Gip Entry.                             */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to the Application Entry.     */
/*                  : pPortEntry - Pointer to the Port Entry.           */
/*                  : u2GipId    - Id of the Gip Context.               */
/*                                                                      */
/* Output(s)        : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Pointer to the new Gip Entry.                     */
/*                    NULL on failure.                                  */
/************************************************************************/
tGarpGipEntry      *
GarpGetNewGipEntry (tGarpAppEntry * pAppEntry)
{
    tGarpGipEntry      *pGipEntry = NULL;
    tGarpAttrEntry    **ppAttrEntry = NULL;

    pGipEntry =
        (tGarpGipEntry *) (VOID *)
        GARP_GET_BUFF (GARP_GIP_BUFF, sizeof (tGarpGipEntry));

    if (pGipEntry == NULL)
    {

        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "\n\n\n!!!!!!!!!!! Free Gip entry NOT available "
                  "!!!!!!!!!! \n\n\n\n");
        return NULL;
    }

    MEMSET (pGipEntry, 0, sizeof (tGarpGipEntry));

    if (pAppEntry->u2MaxBuckets == GVRP_MAX_BUCKETS)
    {
        ppAttrEntry = (tGarpAttrEntry **) (VOID *)
            GARP_GET_BUFF (GARP_HASH_TABLE_GVRP_BUFF,
                           (pAppEntry->u2MaxBuckets *
                            sizeof (tGarpAttrEntry *)));

        pGipEntry->u1AttrType = GARP_ATTR_GVRP_TYPE;
    }

    else if (pAppEntry->u2MaxBuckets == GMRP_MAX_BUCKETS)
    {
        ppAttrEntry = (tGarpAttrEntry **) (VOID *)
            GARP_GET_BUFF (GARP_HASH_TABLE_GMRP_BUFF,
                           (pAppEntry->u2MaxBuckets *
                            sizeof (tGarpAttrEntry *)));

        pGipEntry->u1AttrType = GARP_ATTR_GMRP_TYPE;
    }

    if (ppAttrEntry == NULL)
    {
        GARP_RELEASE_BUFF (GARP_GIP_BUFF, (UINT1 *) pGipEntry);

        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "\n\n\n!!!!!!!!!!! Hash Table allocation failed "
                  "!!!!!!!!!! \n\n\n\n");
        return NULL;
    }

    MEMSET (ppAttrEntry, 0,
            (pAppEntry->u2MaxBuckets * sizeof (tGarpAttrEntry *)));

    pGipEntry->papAttrTable = ppAttrEntry;

    return pGipEntry;
}

/************************************************************************/
/* Function Name    : GarpFreeGipEntry ()                               */
/*                                                                      */
/* Description      : Releases the Gip Entry to the free pool           */
/*                                                                      */
/* Input(s)         : Pointer to the Gip Entry to be released.          */
/*                                                                      */
/* Output(s)        : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None.                                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None.                                             */
/*                                                                      */
/* Use of Recursion : None.                                             */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
void
GarpFreeGipEntry (tGarpGipEntry * pGipEntry)
{
    if (NULL != pGipEntry->papAttrTable)
    {
        if (pGipEntry->u1AttrType == GARP_ATTR_GVRP_TYPE)
        {
            GARP_RELEASE_BUFF (GARP_HASH_TABLE_GVRP_BUFF,
                               (UINT1 *) pGipEntry->papAttrTable);

            pGipEntry->papAttrTable = NULL;
        }
        else if (pGipEntry->u1AttrType == GARP_ATTR_GMRP_TYPE)
        {
            GARP_RELEASE_BUFF (GARP_HASH_TABLE_GMRP_BUFF,
                               (UINT1 *) pGipEntry->papAttrTable);

            pGipEntry->papAttrTable = NULL;
        }
    }
    GARP_RELEASE_BUFF (GARP_GIP_BUFF, (UINT1 *) pGipEntry);
}

/************************************************************************/
/* Function Name    : GarpGetGipEntry ()                                */
/*                                                                      */
/* Description      : Retrieves the Gip Entry matching the Gip Id       */
/*                                                                      */
/* Input(s)         : pPortEntry - Pointer to the Port Entry            */
/*                    u2GipId    - Gip Identifier                       */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Pointer to the Gip Entry if entry is found        */
/*                    NULL otherwise.                                   */
/************************************************************************/
tGarpGipEntry      *
GarpGetGipEntry (tGarpPortEntry * pPortEntry, UINT2 u2GipId)
{
    tGarpGipEntry      *pGipEntry = NULL;
    UINT2               u2HashIndex;

    u2HashIndex =
        GarpGipGetHashIndex (u2GipId,
                             (UINT2) GARP_GIP_HASH_SIZE (pPortEntry->
                                                         pGipTable));

    TMO_HASH_Scan_Bucket (pPortEntry->pGipTable, u2HashIndex, pGipEntry,
                          tGarpGipEntry *)
    {
        if (pGipEntry->u2GipId == u2GipId)
        {
            return pGipEntry;
        }
    }

    return NULL;
}

/************************************************************************/
/* Function Name    : GarpGetNewRemapAttrEntry                          */
/*                                                                      */
/* Description      : Gets new buffer entry for Remap attributes        */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Pointer to the new remap attribute Entry          */
/************************************************************************/
tGarpRemapAttrEntry *
GarpGetNewRemapAttrEntry (VOID)
{
    tGarpRemapAttrEntry *pRemapAttrEntry;

    pRemapAttrEntry =
        (tGarpRemapAttrEntry *) (VOID *) GARP_GET_BUFF (GARP_REMAP_ATTR_BUFF,
                                                        sizeof
                                                        (tGarpRemapAttrEntry));

    if (pRemapAttrEntry == NULL)
    {

        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "\n\n\n!!!!!!!!!!! Free Remap Attribute entry NOT available "
                  "!!!!!!!!!! \n\n\n\n");
        return NULL;
    }

    MEMSET (pRemapAttrEntry, 0, sizeof (tGarpRemapAttrEntry));

    return pRemapAttrEntry;
}

/************************************************************************/
/* Function Name    : GarpGetGipAndAttributeEntry                       */
/*                                                                      */
/* Description      : Gets the gip and attribute entry for the given    */
/*                    port entry and Gip Id                             */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the Application Entry.    */
/*                  : pPortEntry - Pointer to the Port Entry.           */
/*                    pAttr - Pointer to the attribute.                 */
/*                    u2GipId - GIP Id.                                 */
/*                                                                      */
/* Output(s)        : pAttrEntry - Pointer to the attribute entry.      */
/*                    pGipEntry - Pointer to the GIP entry.             */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : GARP_SUCCESS or GARP_FAILURE                      */
/************************************************************************/
INT4
GarpGetGipAndAttributeEntry (tGarpAppEntry * pAppEntry,
                             tGarpPortEntry * pPortEntry, tGarpAttr * pAttr,
                             UINT2 u2GipId, tGarpAttrEntry ** pAttrEntry,
                             tGarpGipEntry ** pGipEntry)
{
    tGarpGipEntry      *pTmpGipEntry;
    tGarpAttrEntry     *pTmpAttrEntry;
    UINT4               u4HashIndex;

    pTmpGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

    if (pTmpGipEntry == NULL)
    {
        return GARP_FAILURE;
    }

    *pGipEntry = pTmpGipEntry;

    u4HashIndex = GarpGetAttributeHashIndex (pAppEntry, pAttr);

    pTmpAttrEntry = pTmpGipEntry->papAttrTable[u4HashIndex];

    while (pTmpAttrEntry != NULL)
    {
        if ((pTmpAttrEntry->Attr.u1AttrType == pAttr->u1AttrType)
            && (pTmpAttrEntry->Attr.u1AttrLen == pAttr->u1AttrLen)
            && (MEMCMP (pTmpAttrEntry->Attr.au1AttrVal,
                        pAttr->au1AttrVal, pAttr->u1AttrLen) == 0))
        {
            *pAttrEntry = pTmpAttrEntry;
            return GARP_SUCCESS;
        }

        pTmpAttrEntry = pTmpAttrEntry->pNextHashNode;
    }

    return GARP_FAILURE;
}

/************************************************************************/
/* Function Name    : GarpDeleteGipEntry ()                             */
/*                                                                      */
/* Description      : Deletes the given Gip entry from the Gip table    */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the App Entry             */
/*                    pPortEntry - Pointer to the Port Entry            */
/*                    pGipEntry  - Pointer to the Gip Entry             */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
VOID
GarpDeleteGipEntry (tGarpAppEntry * pAppEntry, tGarpPortEntry * pPortEntry,
                    tGarpGipEntry * pGipEntry)
{
    GarpDelGipEntryFromAppGipTable (pAppEntry, pGipEntry);

    /* Gip Entry should not be NULL when this function is called */
    if (GARP_SUCCESS == GarpDelGipEntryFromGipHashTable
        (pPortEntry->pGipTable, pGipEntry))
    {
        pPortEntry->u4NumGipEntries--;
    }

    return;
}

/************************************************************************/
/* Function Name    : GarpDelGipEntryFromAppGipTable ()                 */
/*                                                                      */
/* Description      : Deletes the given Gip entry from the GIP hash     */
/*                    table maintained in App entry.                    */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the App Entry             */
/*                    pAppDelGipEntry  - Pointer to the to be           */
/*                    deleted Gip Entry                                 */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
VOID
GarpDelGipEntryFromAppGipTable (tGarpAppEntry * pAppEntry,
                                tGarpGipEntry * pAppDelGipEntry)
{
    UINT2               u2HashIndex;
    tGarpGipEntry      *pScanAppGipEntry = NULL;
    tGarpGipEntry      *pPrevAppGipEntry = NULL;
    tGarpGipEntry      *pPortScanGipEntry = NULL;
    tGarpGipEntry      *pPrevPortScanGipEntry = NULL;

    u2HashIndex = GarpGipGetHashIndex (pAppDelGipEntry->u2GipId,
                                       GARP_APPGIP_HASH_BUCKET_SIZE);

    pPrevAppGipEntry = NULL;

    /* GIP Entry cannot be NULL here. Otherwise delete wouldn't be called */
    pScanAppGipEntry = pAppEntry->papAppGipHashTable[u2HashIndex];

    while (NULL != pScanAppGipEntry)
    {
        if (pScanAppGipEntry->u2GipId == pAppDelGipEntry->u2GipId)
        {
            if (pScanAppGipEntry->pGarpPortEntry->u2Port ==
                pAppDelGipEntry->pGarpPortEntry->u2Port)
            {
                if ((NULL == pScanAppGipEntry->pNextPortGipEntry)
                    && (NULL == pScanAppGipEntry->pNextAppGipEntry))
                {
                    if (NULL == pPrevAppGipEntry)
                    {
                        pAppEntry->papAppGipHashTable[u2HashIndex] = NULL;
                    }
                    else
                    {
                        pPrevAppGipEntry->pNextAppGipEntry = NULL;
                    }
                    return;
                }
                else
                {
                    if (NULL == pScanAppGipEntry->pNextPortGipEntry)
                    {
                        if (NULL == pPrevAppGipEntry)
                        {
                            pAppEntry->papAppGipHashTable[u2HashIndex] =
                                pScanAppGipEntry->pNextAppGipEntry;
                        }
                        else
                        {
                            pPrevAppGipEntry->pNextAppGipEntry =
                                pScanAppGipEntry->pNextAppGipEntry;
                        }

                        /* Scan's NextApp cannot be NULL here */
                        (pScanAppGipEntry->pNextAppGipEntry)->pPrevAppGipEntry =
                            pScanAppGipEntry->pPrevAppGipEntry;
                    }
                    else if (NULL == pScanAppGipEntry->pNextAppGipEntry)
                    {
                        if (NULL == pPrevAppGipEntry)
                        {
                            pAppEntry->papAppGipHashTable[u2HashIndex] =
                                pScanAppGipEntry->pNextPortGipEntry;
                        }
                        else
                        {
                            pPrevAppGipEntry->pNextAppGipEntry =
                                pScanAppGipEntry->pNextPortGipEntry;
                        }

                        (pScanAppGipEntry->pNextPortGipEntry)->
                            pPrevAppGipEntry =
                            pScanAppGipEntry->pPrevAppGipEntry;

                        (pScanAppGipEntry->pNextPortGipEntry)->
                            pPrevPortGipEntry = NULL;
                    }
                    else
                    {
                        if (NULL == pPrevAppGipEntry)
                        {
                            pAppEntry->papAppGipHashTable[u2HashIndex] =
                                pScanAppGipEntry->pNextPortGipEntry;
                        }
                        else
                        {
                            pPrevAppGipEntry->pNextAppGipEntry =
                                pScanAppGipEntry->pNextPortGipEntry;
                        }

                        (pScanAppGipEntry->pNextPortGipEntry)->
                            pPrevAppGipEntry =
                            pScanAppGipEntry->pPrevAppGipEntry;

                        (pScanAppGipEntry->pNextAppGipEntry)->pPrevAppGipEntry =
                            pScanAppGipEntry->pNextPortGipEntry;

                        (pScanAppGipEntry->pNextPortGipEntry)->
                            pNextAppGipEntry =
                            pScanAppGipEntry->pNextAppGipEntry;

                        (pScanAppGipEntry->pNextPortGipEntry)->
                            pPrevPortGipEntry =
                            pScanAppGipEntry->pPrevPortGipEntry;

                        if (NULL != pScanAppGipEntry->pPrevPortGipEntry)
                        {
                            (pScanAppGipEntry->pPrevPortGipEntry)->
                                pNextPortGipEntry =
                                pScanAppGipEntry->pNextPortGipEntry;
                        }
                    }

                    return;
                }
            }
            else                /* GIP matches, search for the port */
            {
                /* Port Id doesn't match in this entry, so start with next
                   port entry in GIP  */
                pPrevPortScanGipEntry = pScanAppGipEntry;

                pPortScanGipEntry = pScanAppGipEntry->pNextPortGipEntry;

                while (NULL != pPortScanGipEntry)
                {
                    if (pPortScanGipEntry->pGarpPortEntry->u2Port ==
                        pAppDelGipEntry->pGarpPortEntry->u2Port)
                    {
                        pPrevPortScanGipEntry->pNextPortGipEntry =
                            pPortScanGipEntry->pNextPortGipEntry;

                        if (NULL != pPortScanGipEntry->pNextPortGipEntry)
                        {
                            (pPortScanGipEntry->pNextPortGipEntry)->
                                pPrevPortGipEntry = pPrevPortScanGipEntry;
                        }
                        return;
                    }
                    pPrevPortScanGipEntry = pPortScanGipEntry;

                    pPortScanGipEntry = pPortScanGipEntry->pNextPortGipEntry;
                }
                break;            /* From GIP while loop - no further chance of FIND */
            }
        }

        pPrevAppGipEntry = pScanAppGipEntry;

        pScanAppGipEntry = pScanAppGipEntry->pNextAppGipEntry;
    }

    return;
}

/************************************************************************/
/* Function Name    : GarpGetAttributeHashIndex ()                      */
/*                                                                      */
/* Description      : Calculates the Hash Index using the given         */
/*                    Attribution information.                          */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to the Application Entry.     */
/*                  : pAttr - Pointer to the Attribution Information    */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Hash Index in the range                           */
/*                    [0, pAppEntry->u2MaxBuckets]                      */
/************************************************************************/
UINT4
GarpGetAttributeHashIndex (tGarpAppEntry * pAppEntry, tGarpAttr * pAttr)
{
    UINT4               u4HashVal = 0;
    UINT4               u4LoopIndex;
    UINT2               u2VlanId;

    switch (pAttr->u1AttrLen)
    {
        case GARP_DEF_GROUP_ATTR_LEN:
            /* 
             * This is Service Requirement Attribute Type. Bucket 0
             * is reserved for this type.
             */
            u4HashVal = 0;
            break;

        case GARP_VLAN_ATTR_LEN:
            /* This is Group Attribute Type for GVRP */
            MEMCPY (&u2VlanId, pAttr->au1AttrVal, pAttr->u1AttrLen);
            u2VlanId = OSIX_NTOHS (u2VlanId);
            u4HashVal = u2VlanId % pAppEntry->u2MaxBuckets;
            break;

        case GARP_MAC_ATTR_LEN:
            for (u4LoopIndex = 0; u4LoopIndex < pAttr->u1AttrLen; u4LoopIndex++)
            {
                u4HashVal = u4HashVal ^ pAttr->au1AttrVal[u4LoopIndex];
            }
            /* 
             * Bucket 0 is reserved for Service Requirement Attribute Type.
             * Hence the Valid buckets for Mac Address Attribute Type fall
             * in the range [1, (pAppEntry->u2MaxBuckets - 1)] both inclusive.
             */
            u4HashVal = (u4HashVal % (pAppEntry->u2MaxBuckets - 1)) + 1;
            break;

        default:
            GARP_TRC_ARG1 (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                           "\n\n\n!!!!!!!!!!! Invalid Attribute Length %d"
                           "!!!!!!!!!! \n\n\n\n", pAttr->u1AttrLen);
            break;
    }
    return u4HashVal;
}

/****************************************************************************/
/* Function Name    : GarpSkipToNextValidAttrType ()                        */
/*                                                                          */
/* Description      : Obtains the offset of the next Valid Attribute.       */
/*                    If the attribute in the current location is invalid,  */
/*                    then the next Valid Attribute is obtained.            */
/*                                                                          */
/* Input(s)         : pu1Data   - Pointer to the flat buffer                */
/*                    pAppEntry - Pointer to the Application entry          */
/*                    u2BufLen  - Length of the buffer.                     */
/*                    pu2AttrOffset - Pass by reference parameter to hold   */
/*                                    the offset of the next valid attribute*/
/*                                                                          */
/* Output(s)        : The Offset of the next Valid attribute.               */
/*                                                                          */
/* Global Variables                                                         */
/*   Referred       : None.                                                 */
/*                                                                          */
/* Global Variables                                                         */
/*   Modified       : None.                                                 */
/*                                                                          */
/* Exceptions or OS                                                         */
/*   Error Handling : None.                                                 */
/*                                                                          */
/* Returns          : GARP_SUCCESS/GARP_FAILURE.                            */
/****************************************************************************/
INT4
GarpSkipToNextValidAttrType (UINT1 *pu1Data, tGarpAppEntry * pAppEntry,
                             UINT2 u2BufLen, UINT2 *pu2AttrOffset)
{
    UINT2               u2Len = 0;
    UINT1               u1IsAttrValid = GARP_FALSE;
    UINT1               u1AttrLen;

    while (u1IsAttrValid == GARP_FALSE)
    {
        /* Check for invalid attribute type */
        if (pAppEntry->pAttrTypeValidateFn (pu1Data[u2Len]) == GARP_FALSE)
        {

            u2Len++;

            if (u2Len >= u2BufLen)
            {
                /* Cannot be processed due to invalid buffer length. */
                return GARP_FAILURE;
            }

            /*
             * Skip the buffer to get the next attribute type..this is added
             * for backward combatibility...REF 12.11.3.3 802.1D 1998...
             */
            while (u2Len < u2BufLen)
            {
                /* We will go to the next attribute type based on the
                 * value in the length field of the packet.*/

                u2Len = u2Len + pu1Data[u2Len];

                if (u2Len >= u2BufLen)
                {
                    /* Cannot be processed due to invalid buffer length. */
                    return GARP_FAILURE;
                }

                if ((pu1Data[u2Len] == GARP_END_MARK) &&
                    (pu1Data[u2Len + 1] == GARP_END_MARK))
                {
                    /* Garp PDU ended with invalid attribute type */
                    return GARP_FAILURE;
                }

                if ((pu1Data[u2Len] == GARP_END_MARK) &&
                    (pu1Data[u2Len + 1] != GARP_END_MARK))
                {
                    /* Point to the next attribute type */
                    u2Len++;
                    break;
                }
            }
        }
        else
        {
            u1IsAttrValid = GARP_TRUE;
        }
    }

    /* 
     * Buffer is now pointing to atribute type...following is the attribute
     * length. checking whether a single attribute exists after this attribute 
     * type. Two end marks will exist at the end of the buffer.
     */
    u1AttrLen =
        (UINT1) (pu1Data[u2Len + 1] + GARP_END_MARK_SIZE + GARP_END_MARK_SIZE);

    if ((u2Len + u1AttrLen) >= u2BufLen)
    {

        /* Buffer doesnot even contains single attribute */
        return GARP_FAILURE;
    }

    /* 
     * In GarpGetNextAttributeType the offset should have been moved once.
     * So increment len with the old offset value.
     */

    *pu2AttrOffset = (UINT2) (*pu2AttrOffset + u2Len);
    return GARP_SUCCESS;
}

/****************************************************************************/
/* Function Name    : GarpGetFirstAttribute ()                              */
/*                                                                          */
/* Description      : The first Garp attribute present in the flat buffer   */
/*                    is obtained                                           */
/*                                                                          */
/* Input(s)         : pu1Data   - Pointer to the flat buffer                */
/*                    pu4Offset - The offset to be returned                 */
/*                    pu1Event  - The event associated with the attribute   */
/*                    u4Len     - The length of the buffer                  */
/*                    pAttr     - Pointer to the memory where the first     */
/*                                Attribute is to be filled.                */
/*                                                                          */
/* Output(s)        : The First Attribute in the flat buffer.               */
/*                                                                          */
/* Global Variables                                                         */
/*   Referred       : None.                                                 */
/*                                                                          */
/* Global Variables                                                         */
/*   Modified       : None.                                                 */
/*                                                                          */
/* Exceptions or OS                                                         */
/*   Error Handling : None.                                                 */
/*                                                                          */
/* Returns          : GARP_SUCCESS/GARP_FAILURE.                            */
/****************************************************************************/
INT4
GarpGetFirstAttribute (UINT1 *pu1Data, UINT1 *pu1Event, UINT2 *pu2Len,
                       tGarpAttr * pAttr, tGarpAppEntry * pAppEntry)
{
    UINT1              *pu1Ptr = NULL;
    UINT2               u2Offset = 0;
    INT4                i4RetVal;

    pu1Ptr = pu1Data;

    i4RetVal =
        GarpSkipToNextValidAttrType (pu1Ptr, pAppEntry, *pu2Len, &u2Offset);

    if (i4RetVal == GARP_FAILURE)
    {

        return GARP_FAILURE;
    }

    pAttr->u1AttrType = pu1Ptr[u2Offset];
    u2Offset++;

    /* 
     * The length value in the message includes the event and the length field 
     * also. But the attribute length in the structure tGarpAttr indicates
     * the length of the attribute value alone.
     */
    if (GARP_IS_ATTR_LEN_VALID (pu1Ptr[u2Offset]) == GARP_FALSE)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                       "GarpGetFirstAttribute:Received Pkt of length %d is invalid \n",
                       pu1Ptr[u2Offset]);
        return GARP_FAILURE;
    }

    if ((pu1Ptr[u2Offset] == GARP_MIN_ATTR_LEN_VAL) &&
        (pu1Ptr[u2Offset + 1] != GARP_LEAVE_ALL))
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                       "GarpGetNextAttribute:Received Pkt of length %d"
                       "is invalid \n", pu1Data[u2Offset]);
        return GARP_FAILURE;

    }

    pAttr->u1AttrLen
        =
        (UINT1) (pu1Ptr[u2Offset] -
                 (GARP_ATTR_EVENT_SIZE + GARP_ATTR_LEN_SIZE));
    u2Offset++;

    *pu1Event = pu1Ptr[u2Offset];
    u2Offset++;

    if (*pu1Event != GARP_LEAVE_ALL)
    {

        MEMCPY (pAttr->au1AttrVal, pu1Ptr + u2Offset, pAttr->u1AttrLen);

        u2Offset = (UINT2) (u2Offset + pAttr->u1AttrLen);
    }
    if (*pu2Len < u2Offset)
    {
        GARP_TRC_ARG1 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                       "GarpGetFirstAttribute:Received Pkt of length %d is invalid \n",
                       *pu2Len);
        return GARP_FAILURE;
    }
    *pu2Len = (UINT2) (*pu2Len - u2Offset);

    return GARP_SUCCESS;
}

/****************************************************************************/
/* Function Name    : GarpGetNextAttribute ()                               */
/*                                                                          */
/* Description      : The next Garp attribute present in the flat buffer    */
/*                    is returned                                           */
/*                                                                          */
/* Input(s)         : pu1Data        - Pointer to the flat buffer           */
/*                    u1PrevAttrType - The type of the previous Attribute   */
/*                    pu1Event       - The event associated with the        */
/*                                     Attribute information                */
/*                    u2Len          - The length of the buffer             */
/*                    pAttr          - Pointer the memory where the next    */
/*                                     Attribute is to be filled            */
/*                                                                          */
/* Output(s)        : The next attribute in the flat buffer.                */
/*                                                                          */
/* Global Variables                                                         */
/*   Referred       : None.                                                 */
/*                                                                          */
/* Global Variables                                                         */
/*   Modified       : None.                                                 */
/*                                                                          */
/* Exceptions or OS                                                         */
/*   Error Handling : None.                                                 */
/*                                                                          */
/* Returns          : GARP_SUCCESS/GARP_FAILURE.                            */
/****************************************************************************/
INT4
GarpGetNextAttribute (UINT1 *pu1Data,
                      UINT1 u1PrevAttrType,
                      UINT1 *pu1Event,
                      UINT2 *pu2Len,
                      tGarpAttr * pAttr, tGarpAppEntry * pAppEntry)
{
    INT4                i4RetVal;
    UINT2               u2Offset = 0;

    if (pu1Data[u2Offset] == GARP_END_MARK)
    {

        /* End of Attribute List */
        u2Offset++;

        if (pu1Data[u2Offset] == GARP_END_MARK)
        {

            /* End of GARP Message */

            return GARP_FAILURE;
        }

        i4RetVal = GarpSkipToNextValidAttrType (pu1Data + u2Offset,
                                                pAppEntry,
                                                (UINT2) (*pu2Len - 1),
                                                &u2Offset);

        if (i4RetVal == GARP_FAILURE)
        {

            return GARP_FAILURE;
        }

        pAttr->u1AttrType = pu1Data[u2Offset];
        u2Offset++;

        if (GARP_IS_ATTR_LEN_VALID (pu1Data[u2Offset]) == GARP_FALSE)
        {
            GARP_TRC_ARG1 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                           "GarpGetNextAttribute:Received Pkt of length %d"
                           "is invalid \n", pu1Data[u2Offset]);
            return GARP_FAILURE;
        }

        if ((pu1Data[u2Offset] == GARP_MIN_ATTR_LEN_VAL) &&
            (pu1Data[u2Offset + 1] != GARP_LEAVE_ALL))
        {
            GARP_TRC_ARG1 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                           "GarpGetNextAttribute:Received Pkt of length %d"
                           "is invalid \n", pu1Data[u2Offset]);
            return GARP_FAILURE;

        }

        /* The length value includes the event and the length field itself */
        pAttr->u1AttrLen
            =
            (UINT1) (pu1Data[u2Offset] -
                     (GARP_ATTR_EVENT_SIZE + GARP_ATTR_LEN_SIZE));
        u2Offset++;

        *pu1Event = pu1Data[u2Offset];
        u2Offset++;

        if (*pu1Event != GARP_LEAVE_ALL)
        {

            MEMCPY (pAttr->au1AttrVal, pu1Data + u2Offset, pAttr->u1AttrLen);

            u2Offset = (UINT2) (u2Offset + pAttr->u1AttrLen);
        }
    }
    else
    {
        if (GARP_IS_ATTR_LEN_VALID (pu1Data[u2Offset]) == GARP_FALSE)
        {
            GARP_TRC_ARG1 (GARP_MOD_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                           "GarpGetNextAttribute:Received Pkt of length %d"
                           "is invalid \n", pu1Data[u2Offset]);
            return GARP_FAILURE;
        }

        pAttr->u1AttrType = u1PrevAttrType;

        /* The length value includes the event and the length field itself */
        pAttr->u1AttrLen
            =
            (UINT1) (pu1Data[u2Offset] -
                     (GARP_ATTR_EVENT_SIZE + GARP_ATTR_LEN_SIZE));
        u2Offset++;

        if ((u2Offset + pAttr->u1AttrLen) >= *pu2Len)
        {

            return GARP_FAILURE;
        }

        *pu1Event = pu1Data[u2Offset];
        u2Offset++;

        if (*pu1Event != GARP_LEAVE_ALL)
        {

            MEMCPY (pAttr->au1AttrVal, pu1Data + u2Offset, pAttr->u1AttrLen);

            u2Offset = (UINT2) (u2Offset + pAttr->u1AttrLen);
        }
    }

    *pu2Len = (UINT2) (*pu2Len - u2Offset);
    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpIsAttrAdminRegFixed ()                       */
/*                                                                           */
/*    Description         : This function checks whether this attribute is   */
/*                          registered in any other port with admin reg.     */
/*                          control as GARP_REG_FIXED. This function will be */
/*                          called to decide, if leave message has to be     */
/*                          sent for this attribute on all ports.            */
/*                                                                           */
/*    Input(s)            : pAppEntry - Pointer to the application entry.    */
/*                          pAttr     - Pointer to the attribute.            */
/*                          u2GipId   - GIP Id.                              */
/*                          u2Port    - Port whose STAP state changed or     */
/*                                      port on which application is         */
/*                                      disabled.                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_TRUE / GARP_FALSE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpIsAttrAdminRegFixed (tGarpAppEntry * pAppEntry, tGarpAttr * pAttr,
                         UINT2 u2GipId, UINT2 u2Port)
{
    UINT1              *pGipPortList = NULL;
    tGarpPortEntry     *pPortEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpGipEntry      *pGipEntry;
    INT4                i4Result;

    pGipPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pGipPortList == NULL)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "GarpIsAttrAdminRegFixed: "
                  "Error in allocating memory for pGipPortList\r\n");
        return GARP_FALSE;
    }
    MEMSET (pGipPortList, 0, sizeof (tLocalPortList));

    GarpGipGetPortList (pAppEntry->u1AppId, u2GipId, pGipPortList);

    /* 
     * Need to search in all the Ports other than 'u2Port'.
     * Hence clearing the bit corresponding to 'u2Port'.
     */
    GARP_RESET_MEMBER_PORT (pGipPortList, u2Port);

    for (pPortEntry = pAppEntry->pPortTable;
         pPortEntry != NULL; pPortEntry = pPortEntry->pNextNode)
    {
        pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);
        if (pGipEntry == NULL)
        {

            continue;
        }
        if (pGipEntry->u1StapStatus == GARP_FORWARDING)
        {

            GARP_IS_MEMBER_PORT (pGipPortList, pPortEntry->u2Port, i4Result);

            if (i4Result == GARP_TRUE)
            {
                pAttrEntry = GarpGetAttributeEntry (pAppEntry,
                                                    pGipEntry, pAttr);

                if ((pAttrEntry != NULL) &&
                    (pAttrEntry->u1AdmRegControl == GARP_REG_FIXED))
                {
                    UtilPlstReleaseLocalPortList (pGipPortList);
                    return GARP_TRUE;
                }
            }
        }
    }

    UtilPlstReleaseLocalPortList (pGipPortList);
    return GARP_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpCmpMacAddr                                   */
/*                                                                           */
/*    Description         : This function compares the given mac addresses.  */
/*                                                                           */
/*    Input(s)            : pMac1 - Pointer to the MAC address.              */
/*                          pMac2 - Pointer to the MAC address.              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Return the result of comparision.                 */
/*                         VLAN_GREATER if pMac1 is greater than pMac2       */
/*                         VLAN_LESSER  if pMac1 is lesser than pMac2        */
/*                         VLAN_EQUAL if both the pMac1 and pMac2 are equal  */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
UINT1
GarpCmpMacAddr (tMacAddr Mac1, tMacAddr Mac2)
{
    UINT1               u1RetVal;

    if (MEMCMP (Mac1, Mac2, ETHERNET_ADDR_SIZE) > 0)
    {
        u1RetVal = GARP_GREATER;
    }
    else if (MEMCMP (Mac1, Mac2, ETHERNET_ADDR_SIZE) < 0)
    {
        u1RetVal = GARP_LESSER;
    }
    else
    {
        u1RetVal = GARP_EQUAL;
    }

    return u1RetVal;
}

#ifdef TRACE_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPrintAttribute ()                            */
/*                                                                           */
/*    Description         : This function prints the given Attribute.        */
/*                                                                           */
/*    Input(s)            : pAttr - Pointer to the Attribute structure.      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
void
GarpPrintAttribute (tGarpAttr * pAttr)
{
    UINT4               u4Index;

    GARP_TRC_ARG1 (GARP_MOD_TRC, DUMP_TRC,
                   GARP_NAME, "\t\tAttribute Type  = 0x%x\n",
                   pAttr->u1AttrType);

    if (pAttr->u1AttrLen != 0)
    {

        GARP_TRC (GARP_MOD_TRC, DUMP_TRC, GARP_NAME, "\t\t Attribute Value = ");

        for (u4Index = 0; u4Index < pAttr->u1AttrLen; u4Index++)
        {

            GARP_TRC_ARG1 (GARP_MOD_TRC,
                           DUMP_TRC, "", "0x%x ", pAttr->au1AttrVal[u4Index]);
        }

        GARP_TRC (GARP_MOD_TRC, DUMP_TRC, "", "\n");
    }
}
#endif /* TRACE_WANTED */
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpIsValidMcastAddr                             */
/*                                                                           */
/*    Description         : This function validates the mcast addresses      */
/*                          of a GMRP JOIN IN PDUs                           */
/*    Input(s)            : pAttr - Pointer to the  GarpAttribute structure  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Return the result of comparision.                 */
/*                         GARP_SUCCESS if pMacAddr is a valid mcast addr    */
/*                         GARP_FAILURE if pMacAddr is a invalid mcast addr  */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
GarpIsValidMcastAddr (tGarpAttr * pAttr)
{
    tMacAddr            MacAddr;

    GARP_COPY_MAC_ADDR (MacAddr, pAttr->au1AttrVal);

    if (pAttr->u1AttrType == GVRP_GROUP_ATTR_TYPE)
    {
        if (pAttr->u1AttrLen == ETHERNET_ADDR_SIZE)
        {
            if (VLAN_IS_MCASTADDR (MacAddr) == VLAN_FALSE)
            {
                return GARP_FAILURE;
            }
            if (VLAN_IS_BCASTADDR (MacAddr) == VLAN_TRUE)
            {
                return GARP_FAILURE;
            }
            /* Check for Vlan Reserved Address */
            if (VLAN_IS_RESERVED_ADDR (MacAddr) == VLAN_TRUE)
            {
                return GARP_FAILURE;
            }
            /* Check for GVRP Reserved Address */
            if (VLAN_DESTADDR_IS_GVRPADDR (MacAddr) == VLAN_TRUE)
            {
                return GARP_FAILURE;
            }
            /* Check for GMRP Reserved Address */
            if (VLAN_DESTADDR_IS_GMRPADDR (MacAddr) == VLAN_TRUE)
            {
                return GARP_FAILURE;
            }
        }
    }
    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpIsValidVlanId                                */
/*                                                                           */
/*    Description         : This function validates the VlanID               */
/*                          in a GVRP PDUs                                   */
/*    Input(s)            : pAttr - Pointer to the  GarpAttribute structure  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Return the result of comparision.                 */
/*                         GARP_SUCCESS if the given VlanId is a valid one   */
/*                         otherwise GARP_FAILURE                            */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
GarpIsValidVlanId (tGarpAttr * pAttr)
{
    tVlanId             VlanId;

    if (pAttr->u1AttrType == GVRP_GROUP_ATTR_TYPE)
    {
        if (pAttr->u1AttrLen == GVRP_VLAN_ID_LEN)
        {
            MEMCPY (&VlanId, pAttr->au1AttrVal, GVRP_VLAN_ID_LEN);
            VlanId = (tVlanId) OSIX_NTOHS (VlanId);

            if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
            {
                return GARP_FAILURE;
            }
        }
    }
    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpIsAttrRegisteredOnOtherPorts ()                            */
/*                                                                           */
/*    Description         : This function checks whether this attribute is   */
/*                          registered in any other port with RegistrarState */
/*                          machine as GARP_IN. This function will be        */
/*                          called to decide, if leave message has to be     */
/*                          sent for this attribute on all ports.            */
/*                                                                           */
/*    Input(s)            : pAppEntry - Pointer to the application entry.    */
/*                          pAttr     - Pointer to the attribute.            */
/*                          u2GipId   - GIP Id.                              */
/*                          u2Port    - Port whose STAP state changed or     */
/*                                      port on which application is         */
/*                                      disabled.                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_TRUE / GARP_FALSE                            */
/*                                                                           */
/*****************************************************************************/
INT4
GarpIsAttrRegisteredOnOtherPorts (tGarpAppEntry * pAppEntry, tGarpAttr * pAttr,
                                  UINT2 u2GipId, UINT2 u2Port)
{
    tGarpPortEntry     *pPortEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpGipEntry      *pGipEntry;
    UINT1              *pGipPortList = NULL;
    INT4                i4Result;

    pGipPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pGipPortList == NULL)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "GarpIsAttrRegisteredOnOtherPorts: "
                  "Error in allocating memory for pGipPortList\r\n");
        return GARP_FALSE;
    }
    MEMSET (pGipPortList, 0, sizeof (tLocalPortList));

    GarpGipGetPortList (pAppEntry->u1AppId, u2GipId, pGipPortList);

    /* 
     * Need to search in all the Ports other than 'u2Port'.
     * Hence clearing the bit corresponding to 'u2Port'.
     */
    GARP_RESET_MEMBER_PORT (pGipPortList, u2Port);

    for (pPortEntry = pAppEntry->pPortTable;
         pPortEntry != NULL; pPortEntry = pPortEntry->pNextNode)
    {
        pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);
        if (pGipEntry == NULL)
        {

            continue;
        }
        if (pGipEntry->u1StapStatus == GARP_FORWARDING)
        {

            GARP_IS_MEMBER_PORT (pGipPortList, pPortEntry->u2Port, i4Result);

            if (i4Result == GARP_TRUE)
            {
                pAttrEntry = GarpGetAttributeEntry (pAppEntry,
                                                    pGipEntry, pAttr);

                if ((pAttrEntry != NULL) &&
                    (pAttrEntry->u1RegSemState == GARP_IN))
                {
                    UtilPlstReleaseLocalPortList (pGipPortList);
                    return GARP_TRUE;
                }
            }
        }
    }
    UtilPlstReleaseLocalPortList (pGipPortList);
    return GARP_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpIsAttributeValid                             */
/*                                                                           */
/*    Description         : This function validates attributes in            */
/*                          GVRP,GMRP JOIN  PDUs and updates GipId for GVRP  */
/*                                                                           */
/*    Input(s)            : pAttr - Pointer to the  GarpAttribute structure  */
/*                          pGarpIfMsg -  Pointer to the message structure   */
/*                                                                           */
/*    Output(s)           : pGarpIfMsg->u2GipId for GVRP Application         */
/*                                                                           */
/*    Global Variables Referred : gpGarpContextInfo->u1GvrpAppId                               */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Return the result of comparision.                 */
/*                         GARP_TRUE                                         */
/*                         GARP_FALSE                                        */
/*                                                                           */
/*****************************************************************************/

INT4
GarpIsAttributeValid (tGarpIfMsg * pGarpIfMsg, tGarpAttr * pAttr)
{
    tVlanId             VlanId;
    tMacAddr            MacAddr;
    UINT1               u1ServiceAttr;
    INT4                i4RetVal = GARP_TRUE;
    UINT1               u1AppId;

    u1AppId = pGarpIfMsg->pAppEntry->u1AppId;

    if (u1AppId == GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId)
    {
        if (pAttr->u1AttrType == GVRP_GROUP_ATTR_TYPE)
        {
            if (pAttr->u1AttrLen == GVRP_VLAN_ID_LEN)
            {
                MEMCPY (&VlanId, pAttr->au1AttrVal, GVRP_VLAN_ID_LEN);
                VlanId = (tVlanId) OSIX_NTOHS (VlanId);

                if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
                {
                    GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                              "\r\n!! Got Invalid VlanId !!\r\n");

                    i4RetVal = GARP_FALSE;
                }
                else
                {
                    /* If Vid translation is enabled, the get thre relay
                     * vid and work on that relay vid. The following
                     * function will change the vid in pAttr as well as
                     * VlanId if the vid translation is enabled on that 
                     * port.*/
                    GarpPbTranslateAttr (pGarpIfMsg->u2Port, pAttr, &VlanId);
                    /* Update the GVRP attr's GIPID from MSTI-VID mapping */
                    pGarpIfMsg->u2GipId =
                        GarpL2IwfMiGetVlanInstMapping
                        (GARP_CURR_CONTEXT_PTR ()->u4ContextId, VlanId);
                }
            }
            else
            {
                GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                          "\r\n!! VlanId Length is Incorrect !!\r\n");

                i4RetVal = GARP_FALSE;
            }

        }
        else
        {
            GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                      "\r\n!! Attribute Type is Invalid for the Appn !!\r\n");

            i4RetVal = GARP_FALSE;
        }
    }
    else
    {
        if (pAttr->u1AttrType == GMRP_GROUP_ATTR_TYPE)
        {
            if (pAttr->u1AttrLen == ETHERNET_ADDR_SIZE)
            {
                GARP_COPY_MAC_ADDR (MacAddr, pAttr->au1AttrVal);

                if (VLAN_IS_BCASTADDR (MacAddr) == VLAN_TRUE)
                {
                    GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                              "\r\n!! Got Broadcast Address for Attribute in GMRP !!\r\n");

                    i4RetVal = GARP_FALSE;
                }

                if (VLAN_IS_MCASTADDR (MacAddr) == VLAN_FALSE)
                {
                    GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                              "\r\n!! Address not Multicast Address !!\r\n");

                    i4RetVal = GARP_FALSE;
                }

                /* Check for Vlan Reserved Address */
                if (VLAN_IS_RESERVED_ADDR (MacAddr) == VLAN_TRUE)
                {
                    GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                              "\r\n!! Address is Reserved Mac Address for Attribute !!\r\n");

                    i4RetVal = GARP_FALSE;
                }
                /* Check for GVRP Reserved Address */
                if (VLAN_DESTADDR_IS_GVRPADDR (MacAddr) == VLAN_TRUE)
                {
                    GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                              "\r\n!! Address is GVRP Mac Address for Attribute !!\r\n");

                    i4RetVal = GARP_FALSE;
                }
                /* Check for GMRP Reserved Address */
                if (VLAN_DESTADDR_IS_GMRPADDR (MacAddr) == VLAN_TRUE)
                {
                    GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                              "\r\n!! Address is GMRP Mac Address for Attribute !!\r\n");

                    i4RetVal = GARP_FALSE;
                }
            }
            else
            {
                GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                          "\r\n!! Address Length not Mac Address Length  for "
                          "GMRP GROUP Attribute !!\r\n");
                i4RetVal = FALSE;
            }

        }
        else if (pAttr->u1AttrType == GMRP_SERVICE_REQ_ATTR_TYPE)
        {

            u1ServiceAttr = pAttr->au1AttrVal[0];
            if (u1ServiceAttr > GMRP_UNREG_GROUPS)
            {
                GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                          "\r\n!! Attribute is not Service Requirement Attribute !!\r\n");

                i4RetVal = GARP_FALSE;
            }
        }
        else
        {
            GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                      "\r\n!! Attribute Type is invalid for Application !!\r\n");
            i4RetVal = GARP_FALSE;
        }
    }
    return i4RetVal;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpGipGetHashIndex                              */
/*                                                                           */
/*    Description         : This function returns Hashindex for the given gip*/
/*                                                                           */
/*    Input(s)            : u2GipId -  Gip Id                                */
/*                          u2BucketSize - Bucket size of the hash table for */
/*                          that application.                                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : u2HashIndex                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
UINT2
GarpGipGetHashIndex (UINT2 u2GipId, UINT2 u2BucketSize)
{
    return ((UINT2) (u2GipId % u2BucketSize));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpCreateAppPortGipHashTable                    */
/*                                                                           */
/*    Description         : This function creates a Hash table of given size */
/*                                                                           */
/*    Input(s)            : u4HashSize - Hash Size                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the hash table.                        */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
tTMO_HASH_TABLE    *
GarpCreateAppPortGipHashTable (UINT4 u4HashSize)
{
    tTMO_HASH_TABLE    *pHashTbl = NULL;

    /* HASH TBL creation for App Port Gip List */
    pHashTbl = TMO_HASH_Create_Table (u4HashSize, NULL, TRUE);

    return (pHashTbl);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpDeleteAppPortGipHashTable                    */
/*                                                                           */
/*    Description         : This function deletes the gip table for the given*/
/*                          port.                                            */
/*                                                                           */
/*    Input(s)            : pPortEntry - Port Entry pointer                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : GARP_SUCCESS / GARP_FAILURE.               */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpDeleteAppPortGipHashTable (tGarpPortEntry * pPortEntry)
{
    tTMO_HASH_TABLE    *pHashTbl = pPortEntry->pGipTable;

    if (pHashTbl != NULL)
    {
        TMO_HASH_Delete_Table (pHashTbl, GarpDeleteGipHashNode);
        pPortEntry->pGipTable = NULL;
        pPortEntry->u4NumGipEntries = 0;
        return GARP_SUCCESS;
    }

    return GARP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpDeleteGipHashNode                            */
/*                                                                           */
/*    Description         : This function deletes the given gip node form    */
/*                          gip hash table.                                  */
/*                                                                           */
/*    Input(s)            : pGipHashNode - Gip hash node to be removed.      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
GarpDeleteGipHashNode (tTMO_HASH_NODE * pGipHashNode)
{
    tGarpGipEntry      *pGipEntry;

    pGipEntry = (tGarpGipEntry *) pGipHashNode;

    GarpFreeGipEntry (pGipEntry);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpAddGipEntryToGipHashTable                    */
/*                                                                           */
/*    Description         : This function adds the gipentry in the given     */
/*                          gip hash table.                                  */
/*                                                                           */
/*    Input(s)            : pGipHashNode - Gip hash node to be removed.      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : GARP_SUCCESS / GARP_FAILURE.               */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpAddGipEntryToGipHashTable (tTMO_HASH_TABLE * pHashTab,
                               tGarpGipEntry * pGipEntry)
{
    UINT2               u2HashIndex = 0;

    u2HashIndex = GarpGipGetHashIndex (pGipEntry->u2GipId,
                                       (UINT2) GARP_GIP_HASH_SIZE (pHashTab));

    TMO_HASH_Add_Node (pHashTab, &(pGipEntry->NextNode),
                       (UINT4) u2HashIndex, NULL);
    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpDelGipEntryFromGipHashTable                  */
/*                                                                           */
/*    Description         : This function deletes the given gip entry form   */
/*                          gip hash table.                                  */
/*                                                                           */
/*    Input(s)            : pGipEntry - Gip Entry to be removed from the     */
/*                                      given hash table.                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : GARP_SUCCESS / GARP_FAILURE.               */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpDelGipEntryFromGipHashTable (tTMO_HASH_TABLE * pHashTab,
                                 tGarpGipEntry * pGipEntry)
{
    UINT2               u2HashIndex = 0;

    u2HashIndex = GarpGipGetHashIndex (pGipEntry->u2GipId,
                                       (UINT2) GARP_GIP_HASH_SIZE (pHashTab));
    TMO_HASH_Delete_Node (pHashTab, &(pGipEntry->NextNode),
                          (UINT4) u2HashIndex);
    GarpFreeGipEntry (pGipEntry);
    return GARP_SUCCESS;
}

/************************************************************************/
/* Function Name    : GarpCleanAndDeleteGipEntry ()                     */
/*                                                                      */
/* Description      : Deletes all the attribures from the given Gip and */
/*                    then deletes the given Gip from the given port.   */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the AppEntry              */
/*                    pPortEntry - Pointer to the Port Entry            */
/*                    u2GipId    - Gip Identifier                       */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : VOID                                              */
/************************************************************************/
VOID
GarpCleanAndDeleteGipEntry (tGarpAppEntry * pAppEntry,
                            tGarpPortEntry * pPortEntry, UINT2 u2GipId)
{
    tGarpGipEntry      *pGipEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpAttrEntry     *pTempAttrEntry;
    UINT4               u4Index;

    pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

    if (NULL == pGipEntry)
    {
        return;
    }

    if (NULL == pGipEntry->papAttrTable)
    {
        return;
    }

    for (u4Index = 0; u4Index < pAppEntry->u2MaxBuckets; u4Index++)
    {
        pAttrEntry = pGipEntry->papAttrTable[u4Index];

        while (pAttrEntry != NULL)
        {
            if (pAttrEntry->u1RegSemState == GARP_LV)
            {
                GarpStopTimer (&pAttrEntry->LeaveTmr);
            }
            pTempAttrEntry = pAttrEntry;
            pAttrEntry = pAttrEntry->pNextHashNode;

            GarpDeleteAttributeEntry (pAppEntry, pPortEntry, &pGipEntry,
                                      pTempAttrEntry);
        }

        if (NULL == pGipEntry)
        {
            break;
        }
    }
    return;
}

/************************************************************************/
/* Function Name    : GarpAddGipEntryFromAppGipTable                    */
/*                                                                      */
/* Description      : Adds Gip Entry to the GIP Table maintained in     */
/*                    App Table.                                        */
/*                                                                      */
/* Input(s)         : pAppEntry  - Pointer to the AppEntry              */
/*                    pGipEntry  - Pointer to the GIP Entry             */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : VOID                                              */
/************************************************************************/

VOID
GarpAddGipEntryFromAppGipTable (tGarpAppEntry * pAppEntry,
                                tGarpGipEntry * pGipEntry)
{
    UINT2               u2AppGipHashIndex;
    tGarpGipEntry      *pScanAppGipEntry = NULL;
    tGarpGipEntry      *pPrevScanAppGipEntry = NULL;

    u2AppGipHashIndex =
        GarpGipGetHashIndex (pGipEntry->u2GipId, GARP_APPGIP_HASH_BUCKET_SIZE);
    /* First Node addition */
    if (pAppEntry->papAppGipHashTable[u2AppGipHashIndex] == NULL)
    {
        pAppEntry->papAppGipHashTable[u2AppGipHashIndex] = pGipEntry;

        pGipEntry->pPrevAppGipEntry = NULL;
        pGipEntry->pNextAppGipEntry = NULL;
        pGipEntry->pNextPortGipEntry = NULL;
        pGipEntry->pPrevPortGipEntry = NULL;
    }
    else
    {
        pScanAppGipEntry = pAppEntry->papAppGipHashTable[u2AppGipHashIndex];

        while (NULL != pScanAppGipEntry)
        {
            pPrevScanAppGipEntry = pScanAppGipEntry;
            /* Found a GIP in the hash table hence add 
             * the GIP as the head and push the existing GIP
             * down.*/
            if (pScanAppGipEntry->u2GipId == pGipEntry->u2GipId)
            {
                if (pScanAppGipEntry->pPrevAppGipEntry == NULL)
                {
                    pAppEntry->papAppGipHashTable[u2AppGipHashIndex] =
                        pGipEntry;

                    pGipEntry->pPrevAppGipEntry = NULL;
                }
                else
                {
                    pGipEntry->pPrevAppGipEntry =
                        pScanAppGipEntry->pPrevAppGipEntry;

                    (pScanAppGipEntry->pPrevAppGipEntry)->pNextAppGipEntry =
                        pGipEntry;
                }

                pGipEntry->pNextAppGipEntry =
                    pScanAppGipEntry->pNextAppGipEntry;

                if (NULL != pScanAppGipEntry->pNextAppGipEntry)
                {
                    (pScanAppGipEntry->pNextAppGipEntry)->pPrevAppGipEntry =
                        pGipEntry;
                }

                pGipEntry->pNextPortGipEntry = pScanAppGipEntry;

                pScanAppGipEntry->pPrevPortGipEntry = pGipEntry;

                /* Always added on head, so PrevPort is NULL */
                pGipEntry->pPrevPortGipEntry = NULL;

                pScanAppGipEntry->pNextAppGipEntry = NULL;
                pScanAppGipEntry->pPrevAppGipEntry = NULL;

                break;
            }

            pScanAppGipEntry = pScanAppGipEntry->pNextAppGipEntry;

            /* New GIP in the bucket.Add the GIP as a fresh node. */

            if (pScanAppGipEntry == NULL)
            {
                pPrevScanAppGipEntry->pNextAppGipEntry = pGipEntry;

                pGipEntry->pPrevAppGipEntry = pPrevScanAppGipEntry;
                pGipEntry->pNextAppGipEntry = NULL;
                pGipEntry->pNextPortGipEntry = NULL;
                pGipEntry->pPrevPortGipEntry = NULL;
            }
        }
    }
    return;
}

/* Remap Functions */
/***************************************************************/
/*  Function Name   : GarpPostInstVlanMap                      */
/*                                                             */
/*  Description     : This function posts message to GARP      */
/*                    to inform Instance-VLAN map Updation     */
/*                                                             */
/*  Input(s)        : pGarpQMsg - Pointer to buffer to post    */
/*                                                             */
/*  Output(s)       : Updation of MST-VLAN map to GARP         */
/*                                                             */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/

VOID
GarpPostInstVlanMap (tGarpQMsg * pGarpQMsg)
{
    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return;
    }

    /* GarpProcessMsg should take care of freeing the pGarpQMsg buffer */
    if (OSIX_FAILURE == GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                                            (UINT1 *) &pGarpQMsg,
                                            OSIX_DEF_MSG_LEN))
    {
        GARP_GLOBAL_TRC (pGarpQMsg->u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         GARP_NAME,
                         " Send To Q failed for VLAN Map Config Message\r\n");

        if (GARP_VLAN_MAP_MSG == pGarpQMsg->u2MsgType)
        {
            GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);
        }
        else
        {
            GARP_RELEASE_BUFF (GARP_VLAN_MAP_MSG_BUFF, pGarpQMsg);
        }

        return;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);

    GARP_GLOBAL_TRC (pGarpQMsg->u4ContextId, GARP_MOD_TRC,
                     (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                     GARP_NAME, "VLAN Map Config Message posted to GARP \r\n");
    return;
}

/************************************************************************/
/* Function Name    : GarpHandleVlanInstanceMap                         */
/*                                                                      */
/* Description      : Handles VLAN mapping to a new instance.           */
/*                                                                      */
/* Input(s)         : VlanId - VLAN Identifier                          */
/*                    u2MapId - Old Instance Identifier                 */
/*                    u2NewInstance - New Instance Identifier           */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : gpGarpContextInfo->pRemapAttrList                                   */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : VOID                                              */
/************************************************************************/

VOID
GarpHandleVlanInstanceMap (tVlanId VlanId, UINT2 u2MapId, UINT2 u2NewInstance)
{
    UINT2               u2RegCount = 0;
    tGarpAppEntry      *pGvrpAppEntry = NULL;
    tGarpAppEntry      *pGmrpAppEntry = NULL;
    tGarpRemapAttrEntry *pRemapAttrEntry = NULL;
    tGarpRemapAttrEntry *pNextRemapAttrEntry = NULL;

    /* GARP Applications need to be present for handling remap */
    pGvrpAppEntry = GarpGetAppEntryWithAddr (GARP_CTXT_GVRP_ADDR ());

    pGmrpAppEntry = GarpGetAppEntryWithAddr (gGmrpAddr);

    if ((NULL == pGvrpAppEntry) && (NULL == pGmrpAppEntry))
    {
        return;
    }

    if (VLAN_TRUE == GarpVlanIsVlanDynamic
        (GARP_CURR_CONTEXT_PTR ()->u4ContextId, VlanId))
    {
        if (NULL != pGmrpAppEntry)
        {
            GarpRemapWithoutAttrInNewInstance (pGmrpAppEntry, VlanId,
                                               u2MapId, NULL);
        }

        if (NULL != pGvrpAppEntry)
        {
            GarpRemapWithoutAttrInNewInstance (pGvrpAppEntry, VlanId,
                                               u2MapId, NULL);
        }
    }
    else
    {
        /* First loop the existing information to obtain the RegCount.
         * Then loop again to act according to the count */
        if (NULL != pGvrpAppEntry)
        {
            u2RegCount = GarpRemapUpdateGvrpRegCount (pGvrpAppEntry, VlanId,
                                                      u2MapId, u2NewInstance);
        }
        else
        {
            u2RegCount = GarpRemapUpdateVlanRegCount (VlanId, u2NewInstance);
        }

        if (0 == u2RegCount)
        {
            /* This means static reg present but port is in BLK state 
             * So handle like Dynamic thread */

            if (NULL != pGmrpAppEntry)
            {
                GarpRemapWithoutAttrInNewInstance (pGmrpAppEntry, VlanId,
                                                   u2NewInstance, NULL);
            }

            if (NULL != pGvrpAppEntry)
            {
                GarpRemapWithoutAttrInNewInstance (pGvrpAppEntry, VlanId,
                                                   u2NewInstance, NULL);
            }
        }
        else
        {
            if (NULL != pGmrpAppEntry)
            {
                /* Construct GMRP registrations and corresponding count */
                GarpRemapUpdateGmrpRegCount (pGmrpAppEntry, VlanId);

                pRemapAttrEntry = GARP_CURR_CONTEXT_PTR ()->pRemapAttrList;

                while (NULL != pRemapAttrEntry)
                {
                    pNextRemapAttrEntry = pRemapAttrEntry->pNextNode;

                    if (0 == pRemapAttrEntry->u4FixedRegCount)
                    {
                        GarpRemapWithoutAttrInNewInstance (pGmrpAppEntry,
                                                           VlanId,
                                                           u2NewInstance,
                                                           &(pRemapAttrEntry->
                                                             Attr));
                    }
                    else
                    {
                        GarpRemapWithAttrInNewInstance (pGmrpAppEntry, VlanId,
                                                        &(pRemapAttrEntry->
                                                          Attr));
                    }

                    GARP_RELEASE_BUFF (GARP_REMAP_ATTR_BUFF, pRemapAttrEntry);

                    pRemapAttrEntry = pNextRemapAttrEntry;
                }

                /* All the entries are processed to back to NULL. Take care
                 * of releasing the RemapAttrEntry before this */
                GARP_CURR_CONTEXT_PTR ()->pRemapAttrList = NULL;
            }

            if (NULL != pGvrpAppEntry)
            {
                pRemapAttrEntry = GarpGetNewRemapAttrEntry ();

                if (NULL != pRemapAttrEntry)
                {
                    pRemapAttrEntry->Attr.u1AttrType = GVRP_GROUP_ATTR_TYPE;

                    pRemapAttrEntry->Attr.u1AttrLen = GVRP_VLAN_ID_LEN;

                    VlanId = (tVlanId) OSIX_HTONS (VlanId);
                    MEMCPY (pRemapAttrEntry->Attr.au1AttrVal, &VlanId,
                            pRemapAttrEntry->Attr.u1AttrLen);

                    pRemapAttrEntry->pNextNode = NULL;

                    pRemapAttrEntry->u4FixedRegCount = u2RegCount;

                    GarpRemapWithAttrInNewInstance (pGvrpAppEntry,
                                                    u2NewInstance,
                                                    &(pRemapAttrEntry->Attr));

                    GARP_RELEASE_BUFF (GARP_REMAP_ATTR_BUFF, pRemapAttrEntry);
                }
            }
        }
    }

    return;
}

/************************************************************************/
/* Function Name    : GarpRemapRemoveAttrEntry                          */
/*                                                                      */
/* Description      : Removes attribute from Old Gip                    */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to Application Entry          */
/*                    ppGipEntry - Double pointer to Gip Entry          */
/*                    pAppPortEntry - Port Entry Pointer                */
/*                    pAttr - Pointer to attribute                      */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Removed attribute entry pointer                   */
/************************************************************************/

tGarpAttrEntry     *
GarpRemapRemoveAttrEntry (tGarpAppEntry * pAppEntry,
                          tGarpGipEntry ** ppGipEntry,
                          tGarpPortEntry * pAppPortEntry, tGarpAttr * pAttr)
{
    tGarpGipEntry      *pTempGipEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpAttrEntry     *pPrevAttrEntry = NULL;
    UINT4               u4HashIndex;

    pTempGipEntry = *ppGipEntry;

    u4HashIndex = GarpGetAttributeHashIndex (pAppEntry, pAttr);

    pAttrEntry = pTempGipEntry->papAttrTable[u4HashIndex];

    while (NULL != pAttrEntry)
    {
        if ((pAttrEntry->Attr.u1AttrType == pAttr->u1AttrType)
            && (pAttrEntry->Attr.u1AttrLen == pAttr->u1AttrLen)
            && (MEMCMP (pAttrEntry->Attr.au1AttrVal,
                        pAttr->au1AttrVal, pAttr->u1AttrLen) == 0))
        {
            if (pPrevAttrEntry == NULL)
            {
                pTempGipEntry->papAttrTable[u4HashIndex] =
                    pAttrEntry->pNextHashNode;
            }
            else
            {
                pPrevAttrEntry->pNextHashNode = pAttrEntry->pNextHashNode;
            }

            pTempGipEntry->u4NumEntries--;

            if (0 == pTempGipEntry->u4NumEntries)
            {
                GarpDeleteGipEntry (pAppEntry, pAppPortEntry, pTempGipEntry);

                *ppGipEntry = NULL;
            }

            break;
        }

        pPrevAttrEntry = pAttrEntry;
        pAttrEntry = pAttrEntry->pNextHashNode;
    }

    return pAttrEntry;
}

/************************************************************************/
/* Function Name    : GarpRemapUpdateGvrpRegCount                       */
/*                                                                      */
/* Description      : Calculate GVRP Registration count for VlanId      */
/*                    specified and moves the attribute to New GIP      */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to Application Entry          */
/*                    VlanId - VLAN Identifier                          */
/*                    u2GipId - Old GIP Identifier                      */
/*                    u2NewGipId - New GIP Identifier                   */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Registration count                                */
/************************************************************************/

UINT2
GarpRemapUpdateGvrpRegCount (tGarpAppEntry * pAppEntry, tVlanId VlanId,
                             UINT2 u2GipId, UINT2 u2NewGipId)
{
    UINT2               u2RegCount = 0;
    tGarpAttr           Attr;
    tGarpGipEntry      *pAppGipEntry = NULL;
    tGarpGipEntry      *pNewGipEntry = NULL;
    tGarpAttrEntry     *pAttrEntry = NULL;
    tGarpPortEntry     *pAppPortEntry = NULL;

    pAppPortEntry = pAppEntry->pPortTable;

    /* Form the attribute of the given VLAN to be retrieved */
    MEMSET (&Attr, 0, sizeof (tGarpAttr));

    Attr.u1AttrType = GVRP_GROUP_ATTR_TYPE;
    Attr.u1AttrLen = GVRP_VLAN_ID_LEN;

    VlanId = (tVlanId) OSIX_HTONS (VlanId);
    MEMCPY (Attr.au1AttrVal, &VlanId, Attr.u1AttrLen);

    while (NULL != pAppPortEntry)
    {
        pAppGipEntry = GarpGetGipEntry (pAppPortEntry, u2GipId);

        if (NULL != pAppGipEntry)
        {
            /* Delete the attribute from old GIP. Don't use 
             * GarpDeleteAttributeEntry coz that frees the attribute entry */
            pAttrEntry = GarpRemapRemoveAttrEntry (pAppEntry, &pAppGipEntry,
                                                   pAppPortEntry, &Attr);

            if (NULL != pAttrEntry)
            {
                pNewGipEntry = GarpGetGipEntry (pAppPortEntry, u2NewGipId);

                pNewGipEntry = GarpAddAttributeEntry (pAppEntry, pAppPortEntry,
                                                      pNewGipEntry, pAttrEntry,
                                                      u2NewGipId);

                if (NULL != pNewGipEntry)
                {
                    if (pAttrEntry->u1RegSemState == GARP_LV)
                    {
                        GarpStopTimer (&pAttrEntry->LeaveTmr);
                    }

                    /* Check Stap Status and Attribute Reg Control to
                     * update count */
                    if ((GARP_FORWARDING == pNewGipEntry->u1StapStatus)
                        && (GARP_REG_FIXED == pAttrEntry->u1AdmRegControl))
                    {
                        u2RegCount++;
                    }
                }
            }
        }

        pAppPortEntry = pAppPortEntry->pNextNode;
    }
    return u2RegCount;
}

/************************************************************************/
/* Function Name    : GarpRemapUpdateVlanRegCount                       */
/*                                                                      */
/* Description      : Calculates VLAN Registration count in New GIP     */
/*                    in the absence of GVRP                            */
/*                                                                      */
/* Input(s)         : VlanId - VLAN Identifier                          */
/*                    u2NewGipId - New GIP Identifier                   */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Registration count                                */
/************************************************************************/
UINT2
GarpRemapUpdateVlanRegCount (tVlanId VlanId, UINT2 u2NewGipId)
{
    UINT1              *pPortList = NULL;
    UINT2               u2RegCount = 0;
    UINT2               u2NumPorts = 0;
    UINT2               u2TempPort;
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    /* When GVRP is not there in the system, Egress Ports can be only
       static ports */
    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));
    if (pPortList == NULL)
    {
        GARP_TRC (GMRP_MOD_TRC, CONTROL_PLANE_TRC, GMRP_NAME,
                  "GarpRemapUpdateVlanRegCount: "
                  "Error in allocating memory for pPortList\r\n");
        return u2RegCount;
    }
    MEMSET (pPortList, 0, sizeof (tLocalPortList));
    MEMSET (gu2ConfPorts,0, GARP_MAX_PORTS);

    GarpL2IwfMiGetVlanLocalEgressPorts (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                                        VlanId, pPortList);

    UtilGetPortArrayFromPortList (pPortList, sizeof (tLocalPortList),
                                  GARP_MAX_PORTS, gu2ConfPorts, &u2NumPorts);
    UtilPlstReleaseLocalPortList (pPortList);
    for (u2TempPort = 0; ((u2TempPort < u2NumPorts) &&
                          (u2TempPort < GARP_MAX_PORTS)); u2TempPort++)
    {
        pGlobalPortEntry = GARP_PORT_ENTRY (gu2ConfPorts[u2TempPort]);

        if (AST_PORT_STATE_FORWARDING ==
            (INT4) GarpGetInstPortState (u2NewGipId, pGlobalPortEntry))
        {
            u2RegCount++;
        }
    }

    return u2RegCount;
}

/************************************************************************/
/* Function Name    : GarpCheckAndAddRemapAttrEntry                     */
/*                                                                      */
/* Description      : Forms the list of attributes to be handled for    */
/*                    VLAN Instance Remap                               */
/*                                                                      */
/* Input(s)         : pAttrEtnry - Pointer to Attribute Entry           */
/*                    u1IsRegFixed - Indicates Registration Type        */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : gpGarpContextInfo->pRemapAttrList                                   */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
VOID
GarpCheckAndAddRemapAttrEntry (tGarpAttrEntry * pAttrEntry, UINT1 u1IsRegFixed)
{
    tGarpRemapAttrEntry *pScanRemapAttrEntry = NULL;
    tGarpRemapAttrEntry *pRemapAttrEntry = NULL;

    /* If List is NULL, add as first node */
    if (GARP_CURR_CONTEXT_PTR ()->pRemapAttrList == NULL)
    {
        pRemapAttrEntry = GarpGetNewRemapAttrEntry ();

        if (pRemapAttrEntry != NULL)
        {
            pRemapAttrEntry->Attr.u1AttrType = pAttrEntry->Attr.u1AttrType;

            pRemapAttrEntry->Attr.u1AttrLen = pAttrEntry->Attr.u1AttrLen;

            MEMCPY (pRemapAttrEntry->Attr.au1AttrVal,
                    pAttrEntry->Attr.au1AttrVal,
                    pRemapAttrEntry->Attr.u1AttrLen);

            pRemapAttrEntry->pNextNode = NULL;

            if (u1IsRegFixed == GARP_TRUE)
            {
                pRemapAttrEntry->u4FixedRegCount++;
            }

            GARP_CURR_CONTEXT_PTR ()->pRemapAttrList = pRemapAttrEntry;
        }

        return;
    }

    pScanRemapAttrEntry = GARP_CURR_CONTEXT_PTR ()->pRemapAttrList;

    while (NULL != pScanRemapAttrEntry)
    {
        /* Increment count if there is a match */
        if (pAttrEntry->Attr.u1AttrType == pScanRemapAttrEntry->Attr.u1AttrType)
        {
            if (pAttrEntry->Attr.u1AttrType == GMRP_GROUP_ATTR_TYPE)
            {
                if (0 == (MEMCMP (pAttrEntry->Attr.au1AttrVal,
                                  pScanRemapAttrEntry->Attr.au1AttrVal,
                                  GARP_MAC_ATTR_LEN)))
                {
                    if (u1IsRegFixed == GARP_TRUE)
                    {
                        pScanRemapAttrEntry->u4FixedRegCount++;
                    }
                    break;
                }
            }
            else
            {
                if (0 == (MEMCMP (pAttrEntry->Attr.au1AttrVal,
                                  pScanRemapAttrEntry->Attr.au1AttrVal,
                                  GMRP_SERVICE_REQ_LEN)))
                {
                    if (u1IsRegFixed == GARP_TRUE)
                    {
                        pScanRemapAttrEntry->u4FixedRegCount++;
                    }
                    break;
                }
            }
        }

        /* If nothing matches till the end, add a new node */
        if (pScanRemapAttrEntry->pNextNode == NULL)
        {
            pRemapAttrEntry = GarpGetNewRemapAttrEntry ();

            if (NULL != pRemapAttrEntry)
            {
                pScanRemapAttrEntry->pNextNode = pRemapAttrEntry;

                pRemapAttrEntry->Attr.u1AttrType = pAttrEntry->Attr.u1AttrType;

                pRemapAttrEntry->Attr.u1AttrLen = pAttrEntry->Attr.u1AttrLen;

                MEMCPY (pRemapAttrEntry->Attr.au1AttrVal,
                        pAttrEntry->Attr.au1AttrVal,
                        pRemapAttrEntry->Attr.u1AttrLen);

                if (u1IsRegFixed == GARP_TRUE)
                {
                    pRemapAttrEntry->u4FixedRegCount++;
                }

                pRemapAttrEntry->pNextNode = NULL;
            }

            return;
        }

        pScanRemapAttrEntry = pScanRemapAttrEntry->pNextNode;
    }

    return;
}

/************************************************************************/
/* Function Name    : GarpRemapBuildGmrpAttrList                        */
/*                                                                      */
/* Description      : Updates the list of GMRP attributes to be         */
/*                     handled for VLAN Instance Remap                  */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to Application Entry          */
/*                    pGipEntry - Pointer to Gip Entry                  */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/

VOID
GarpRemapBuildGmrpAttrList (tGarpAppEntry * pAppEntry,
                            tGarpGipEntry * pGipEntry)
{
    UINT4               u4HashIndex;
    UINT1               u1IsRegFixed = GARP_FALSE;
    tGarpAttrEntry     *pGmrpAttrEntry = NULL;

    for (u4HashIndex = 0; u4HashIndex < pAppEntry->u2MaxBuckets; u4HashIndex++)
    {
        pGmrpAttrEntry = pGipEntry->papAttrTable[u4HashIndex];

        while (NULL != pGmrpAttrEntry)
        {
            if (pGmrpAttrEntry->u1RegSemState == GARP_LV)
            {
                GarpStopTimer (&(pGmrpAttrEntry->LeaveTmr));
            }

            if ((pGipEntry->u1StapStatus == GARP_FORWARDING) &&
                (pGmrpAttrEntry->u1AdmRegControl == GARP_REG_FIXED))
            {
                u1IsRegFixed = GARP_TRUE;
            }

            /* This function updates the Global "gpGarpContextInfo->pRemapAttrList" to 
             * contain the list of attr to be processed */
            GarpCheckAndAddRemapAttrEntry (pGmrpAttrEntry, u1IsRegFixed);

            pGmrpAttrEntry = pGmrpAttrEntry->pNextHashNode;
        }
    }

    return;
}

/************************************************************************/
/* Function Name    : GarpRempUpdateGmrpRegCount                        */
/*                                                                      */
/* Description      : Calculates Registration count for GMRP attributes */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to Application Entry          */
/*                    u2GipId  - Gip Identifier                         */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
VOID
GarpRemapUpdateGmrpRegCount (tGarpAppEntry * pAppEntry, UINT2 u2GipId)
{
    UINT2               u2AppGipHashIndex;
    UINT1               u1PortState;
    tGarpGipEntry      *pAppGipEntry = NULL;
    tGarpGipEntry      *pAppNextPortGipEntry = NULL;
    tGarpGlobalPortEntry *pGlobalPortEntry;

    /* Retrieve the GipEntry from AppGipHashTable */
    u2AppGipHashIndex =
        GarpGipGetHashIndex (u2GipId, GARP_APPGIP_HASH_BUCKET_SIZE);

    pAppGipEntry = pAppEntry->papAppGipHashTable[u2AppGipHashIndex];

    while (pAppGipEntry)
    {
        if (pAppGipEntry->u2GipId == u2GipId)
        {
            break;
        }

        pAppGipEntry = pAppGipEntry->pNextAppGipEntry;
    }

    while (NULL != pAppGipEntry)
    {
        pGlobalPortEntry = GARP_PORT_ENTRY (pAppGipEntry->pGarpPortEntry->
                                            u2Port);
        pAppNextPortGipEntry = pAppGipEntry->pNextPortGipEntry;

        u1PortState = GarpGetVlanPortState (u2GipId, pGlobalPortEntry);

        if (u1PortState == AST_PORT_STATE_FORWARDING)
        {
            pAppGipEntry->u1StapStatus = GARP_FORWARDING;
        }
        else
        {
            pAppGipEntry->u1StapStatus = GARP_BLOCKING;
        }

        GarpRemapBuildGmrpAttrList (pAppEntry, pAppGipEntry);

        /* Process next GipEntry */
        pAppGipEntry = pAppNextPortGipEntry;
    }

    return;
}

/************************************************************************/
/* Function Name    : GarpRemapWithoutAttrInNewInstance                 */
/*                                                                      */
/* Description      : Handles Remap when no static Registration is      */
/*                    present                                           */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to Application Entry          */
/*                    u2VlanId  - VLAN Identifier                       */
/*                    u2Instance - Instance Identifier                  */
/*                    pDelAttr - pointer to attribute                   */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
VOID
GarpRemapWithoutAttrInNewInstance (tGarpAppEntry * pAppEntry,
                                   tVlanId VlanId,
                                   UINT2 u2Instance, tGarpAttr * pDelAttr)
{
    /* The function is called when the RegCount of the attr in new instance 
     * is zero */
    UINT4               u4StartHashIndex;
    UINT4               u4EndHashIndex;
    UINT4               u4HashIndex;
    UINT2               u2AppGipHashIndex;
    UINT2               u2GipId;
    UINT2               u2Offset = 0;
    UINT1               u1PortState;
    UINT4               u4NumAttrEntries = 0;
    tGarpAttr           Attr;
    tGarpGipEntry      *pAppGipEntry = NULL;
    tGarpGipEntry      *pAppNextPortGipEntry = NULL;
    tGarpAttrEntry     *pTempAttrEntry = NULL;
    tGarpAttrEntry     *pNextAttrEntry = NULL;
    tGarpPortEntry     *pTempPortEntry = NULL;
    tGarpGlobalPortEntry *pGlobalPortEntry;

    MEMSET (&Attr, 0, sizeof (tGarpAttr));

    /* Retrieve GIP and Attribute Indices to work for each Application */
    if (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId == pAppEntry->u1AppId)
    {
        u2GipId = u2Instance;

        Attr.u1AttrType = GVRP_GROUP_ATTR_TYPE;
        Attr.u1AttrLen = GVRP_VLAN_ID_LEN;

        VlanId = (tVlanId) OSIX_HTONS (VlanId);
        MEMCPY (Attr.au1AttrVal, &VlanId, Attr.u1AttrLen);

        u4StartHashIndex = u4EndHashIndex =
            GarpGetAttributeHashIndex (pAppEntry, &Attr);
    }
    else if (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId == pAppEntry->u1AppId)
    {
        u2GipId = VlanId;

        /* Func can be called for specific MCAST attr or all mcast */
        if (NULL == pDelAttr)
        {
            u4StartHashIndex = 0;

            u4EndHashIndex = pAppEntry->u2MaxBuckets - 1;
        }
        else
        {
            Attr.u1AttrType = GMRP_GROUP_ATTR_TYPE;
            Attr.u1AttrLen = GMRP_MAC_ADDR_LEN;

            MEMCPY (Attr.au1AttrVal, (pDelAttr->au1AttrVal), Attr.u1AttrLen);

            u4StartHashIndex = u4EndHashIndex =
                GarpGetAttributeHashIndex (pAppEntry, &Attr);
        }
    }
    else
    {
        return;
    }

    /* Retrieve the GipEntry from AppGipHashTable */
    u2AppGipHashIndex =
        GarpGipGetHashIndex (u2GipId, GARP_APPGIP_HASH_BUCKET_SIZE);

    pAppGipEntry = pAppEntry->papAppGipHashTable[u2AppGipHashIndex];

    while (pAppGipEntry)
    {
        if (pAppGipEntry->u2GipId == u2GipId)
        {
            break;
        }

        pAppGipEntry = pAppGipEntry->pNextAppGipEntry;
    }

    /* Check for Attributes in the GIP Entry of Ports */
    while (NULL != pAppGipEntry)
    {
        /* Store the number of attribute entries in this Gip */
        u4NumAttrEntries = pAppGipEntry->u4NumEntries;

        pGlobalPortEntry = GARP_PORT_ENTRY (pAppGipEntry->pGarpPortEntry->
                                            u2Port);
        if ((pAppEntry->u1AppId == GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId)
            && (pDelAttr == NULL))
        {
            u1PortState = GarpGetVlanPortState (VlanId, pGlobalPortEntry);

            if (u1PortState == AST_PORT_STATE_FORWARDING)
            {
                pAppGipEntry->u1StapStatus = GARP_FORWARDING;
            }
            else
            {
                pAppGipEntry->u1StapStatus = GARP_BLOCKING;
            }
        }

        pAppNextPortGipEntry = pAppGipEntry->pNextPortGipEntry;

        for (u4HashIndex = u4StartHashIndex;
             (u4HashIndex <= u4EndHashIndex) && (u4NumAttrEntries > 0);
             u4HashIndex++)
        {
            pTempAttrEntry = pAppGipEntry->papAttrTable[u4HashIndex];

            while ((NULL != pTempAttrEntry) && (u4NumAttrEntries > 0))
            {
                pNextAttrEntry = pTempAttrEntry->pNextHashNode;

                /* Update Applicant SEM, Tx LEAVE/EMPTY */
                GarpRemapUpdateAppSemAndTx (pAppEntry, pAppGipEntry,
                                            pTempAttrEntry, &u2Offset);

                /* Check Registrar SEM, update VLAN DB */
                /* The last parameter is to indicate whether to delete
                 * specific MCAST or all MCAST in VLAN */
                if (NULL == pDelAttr)
                {
                    GarpRemapUpdateRegSem (pAppEntry, pAppGipEntry,
                                           pTempAttrEntry, VlanId, GARP_FALSE);
                }
                else
                {
                    GarpRemapUpdateRegSem (pAppEntry, pAppGipEntry,
                                           pTempAttrEntry, VlanId, GARP_TRUE);
                }

                pTempPortEntry = pAppGipEntry->pGarpPortEntry;

                if (GarpCheckAndDelAttrEntry (pAppEntry,
                                              pAppGipEntry->pGarpPortEntry,
                                              pAppGipEntry, pTempAttrEntry)
                    == GARP_TRUE)
                {
                    /* An attribute has been deleted - decrement the count */
                    u4NumAttrEntries--;
                }

                /* Process next attribute */
                pTempAttrEntry = pNextAttrEntry;
            }
        }

        /* Before moving to next port Tx if filled buffer available */
        if (0 != u2Offset)
        {
            GarpRemapTransmitPdu (u2Offset, pAppEntry->AppAddress,
                                  pTempPortEntry->u2Port, u2GipId);

            u2Offset = 0;
        }

        /* Process next GipEntry */
        pAppGipEntry = pAppNextPortGipEntry;
    }

    return;
}

/************************************************************************/
/* Function Name    : GarpRemapUpdateAppSemAndTx                        */
/*                                                                      */
/* Description      : Updates App SEM and Transmits GARP Msg            */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to Application Entry          */
/*                    pGipEntry - pointer to GIP Entry                  */
/*                    pAttrEntry - pointer to attribute                 */
/*                    pu2Offset - pointer to packet offset              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : gpGarpContextInfo->au1RemapTxBuf                                    */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
VOID
GarpRemapUpdateAppSemAndTx (tGarpAppEntry * pAppEntry,
                            tGarpGipEntry * pGipEntry,
                            tGarpAttrEntry * pAttrEntry, UINT2 *pu2Offset)
{
    PRIVATE UINT1       u1PrevAttrType = GARP_INVALID_ATTR_TYPE;
    PRIVATE UINT1       u1DataPresent = GARP_FALSE;
    UINT2               u2Offset = 0;
    UINT1               u1SendMsg = GARP_INVALID_EVENT;
    UINT1              *pu1Ptr = NULL;

    /* Determine what message to send */
    if ((pAttrEntry->u1AppSemState == GARP_VA) ||
        (pAttrEntry->u1AppSemState == GARP_AA) ||
        (pAttrEntry->u1AppSemState == GARP_QA))
    {
        u1SendMsg = GARP_LEAVE_EMPTY;
    }
    else if ((pAttrEntry->u1AdmRegControl == GARP_REG_NORMAL) &&
             (pAttrEntry->u1RegSemState == GARP_IN))
    {
        u1SendMsg = GARP_EMPTY;
    }

    if (u1SendMsg != GARP_INVALID_EVENT)
    {
        /* Update App SEM to VO */
        pAttrEntry->u1AppSemState = GARP_VO;

        /* Determine buffer offset */
        if (0 == *pu2Offset)
        {
            u2Offset = GARP_HDR_SIZE;
            u1PrevAttrType = GARP_INVALID_ATTR_TYPE;
            u1DataPresent = GARP_FALSE;
        }
        else
        {
            u2Offset = *pu2Offset;
        }

        pu1Ptr = GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf + u2Offset;

        /* Update Attribute Type */
        if (u1PrevAttrType == GARP_INVALID_ATTR_TYPE)
        {
            *pu1Ptr = pAttrEntry->Attr.u1AttrType;
            pu1Ptr++;
        }
        else
        {
            /* When the attribute type changes, update */
            if (u1PrevAttrType != pAttrEntry->Attr.u1AttrType)
            {
                *pu1Ptr = GARP_END_MARK;
                pu1Ptr++;

                *pu1Ptr = pAttrEntry->Attr.u1AttrType;
                pu1Ptr++;
            }
        }

        u1PrevAttrType = pAttrEntry->Attr.u1AttrType;

        /* Update Length, Event and Attribute Value */
        *pu1Ptr = (UINT1) (pAttrEntry->Attr.u1AttrLen +
                           GARP_ATTR_EVENT_SIZE + GARP_ATTR_LEN_SIZE);

        pu1Ptr++;

        *pu1Ptr = u1SendMsg;
        pu1Ptr++;

        MEMCPY (pu1Ptr, pAttrEntry->Attr.au1AttrVal,
                pAttrEntry->Attr.u1AttrLen);

        pu1Ptr = pu1Ptr + pAttrEntry->Attr.u1AttrLen;

        u1DataPresent = GARP_TRUE;

        /* Update total data buffer offset */
        u2Offset = (UINT2) (pu1Ptr - GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf);

        if ((u1DataPresent == GARP_TRUE) &&
            ((u2Offset + GARP_MAX_ATTR_SIZE) > GARP_MAX_MSG_LEN))
        {
            GarpRemapTransmitPdu (u2Offset, pAppEntry->AppAddress,
                                  pGipEntry->pGarpPortEntry->u2Port,
                                  pGipEntry->u2GipId);

            u2Offset = 0;
        }

        /* Update Offset size for the calling function */
        *pu2Offset = u2Offset;
    }
    return;
}

/************************************************************************/
/* Function Name    : GarpRemapTransmitPdu                              */
/*                                                                      */
/* Description      : Transmits GARP PDU                                */
/*                                                                      */
/* Input(s)         : pu2Offset - pointer to packet offset              */
/*                    AppAddress - Application MAC address              */
/*                    u2PortId - Port Identifier                        */
/*                    u2GipId - GIP Identifier                          */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gpGarpContextInfo->au1RemapTxBuf                                    */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
VOID
GarpRemapTransmitPdu (UINT2 u2Offset, tMacAddr AppAddress, UINT2 u2PortId,
                      UINT2 u2GipId)
{
    UINT2               u2DataLen = 0;
    UINT1              *pu1Buffer = NULL;

    pu1Buffer = GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf + u2Offset;

    *pu1Buffer = GARP_END_MARK;
    pu1Buffer++;

    *pu1Buffer = GARP_END_MARK;
    pu1Buffer++;

    u2DataLen = (UINT2) (pu1Buffer - GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf);

    GarpTransmitPdu (GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf, u2DataLen,
                     AppAddress, u2PortId, u2GipId);

    /* Reset the transmit buffer for the next remap usage. */
    MEMSET (GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf, 0, GARP_MAX_MSG_LEN);

    pu1Buffer = NULL;

    u2DataLen = 0;
    UNUSED_PARAM(pu1Buffer);
    UNUSED_PARAM(u2DataLen);

    return;
}

/************************************************************************/
/* Function Name    : GarpRemapUpdateRegSem                             */
/*                                                                      */
/* Description      : Updates Registration SEM and VLAN Database        */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to Application Entry          */
/*                    pGipEntry - pointer to GIP Entry                  */
/*                    pAttrEntry - pointer to attribute                 */
/*                    u2VlanId - VLAN Identifier                        */
/*                    u1VlanIpdate - To update VLAN Database            */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
VOID
GarpRemapUpdateRegSem (tGarpAppEntry * pAppEntry, tGarpGipEntry * pGipEntry,
                       tGarpAttrEntry * pAttrEntry, tVlanId VlanId,
                       UINT1 u1VlanUpdate)
{
    UINT1               u1ServiceAttrType;
    INT4                i4RetVal = VLAN_FAILURE;
    tGarpGlobalPortEntry *pGlobalPortEntry;

    pGlobalPortEntry = GARP_PORT_ENTRY (pGipEntry->pGarpPortEntry->u2Port);

    if ((pAttrEntry->u1AdmRegControl == GARP_REG_NORMAL) &&
        (pAttrEntry->u1RegSemState != GARP_MT))
    {
        if (GARP_LV == pAttrEntry->u1RegSemState)
        {
            /* u1TmrActive need not be set to GARP_FALSE.
             * Its already done inside GarpStopTimer */
            GarpStopTimer (&(pAttrEntry->LeaveTmr));
        }

        pAttrEntry->u1RegSemState = GARP_MT;

        if (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId == pAppEntry->u1AppId)
        {
            i4RetVal =
                GarpVlanUpdateDynamicVlanInfo
                (GARP_CURR_CONTEXT_PTR ()->u4ContextId, VlanId,
                 pGipEntry->pGarpPortEntry->u2Port, VLAN_DELETE);

            if (i4RetVal == VLAN_FAILURE)
            {
                GARP_TRC_ARG2 (GVRP_MOD_TRC, ALL_FAILURE_TRC, GVRP_NAME,
                               "Failed to update Dynamic Vlan Information"
                               "for Vlan %d on Port %d\n", VlanId,
                               pGlobalPortEntry->u4IfIndex);
            }

        }
        else if (GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId == pAppEntry->u1AppId)
        {
            if (GARP_TRUE == u1VlanUpdate)
            {
                if (GMRP_GROUP_ATTR_TYPE == pAttrEntry->Attr.u1AttrType)
                {

                    i4RetVal = GarpVlanMiUpdateDynamicMcastInfo
                        (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                         pAttrEntry->Attr.au1AttrVal, VlanId,
                         (UINT2) pGlobalPortEntry->u4IfIndex, VLAN_DELETE);

                    if (i4RetVal == VLAN_FAILURE)
                    {
                        GARP_TRC_ARG2 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                                       "Failed to Update Dynamic Multicast "
                                       "Information for Vlan %d on Port %d\n",
                                       VlanId, pGlobalPortEntry->u4IfIndex);
                    }

                }
                else
                {
                    u1ServiceAttrType = pAttrEntry->Attr.au1AttrVal[0];
#ifdef GARP_EXTENDED_FILTER

                    i4RetVal = GarpVlanUpdateDynamicDefGroupInfo
                        (GARP_CURR_CONTEXT_PTR ()->u4ContextId,
                         u1ServiceAttrType,
                         VlanId,
                         pGipEntry->pGarpPortEntry->u2Port, VLAN_DELETE);

                    if (i4RetVal == VLAN_FAILURE)
                    {
                        GARP_TRC_ARG2 (GMRP_MOD_TRC, ALL_FAILURE_TRC, GMRP_NAME,
                                       "Failed to Update Dynamic Default group"
                                       "Information for Vlan %d on Port %d\n",
                                       VlanId, pGlobalPortEntry->u4IfIndex);
                    }
#endif /* GARP_EXTENDED_FILTER */
                }
            }
            else
            {
                if (GMRP_GROUP_ATTR_TYPE == pAttrEntry->Attr.u1AttrType)
                {
                    GarpVlanMiDelDynamicMcastInfoForVlan
                        (GARP_CURR_CONTEXT_PTR ()->u4ContextId, VlanId);
                }
                else
                {
                    u1ServiceAttrType = pAttrEntry->Attr.au1AttrVal[0];
#ifdef GARP_EXTENDED_FILTER
                    GarpVlanMiDelDynamicDefGroupInfoForVlan
                        (GARP_CURR_CONTEXT_PTR ()->u4ContextId, VlanId);
#endif
                }
            }
        }
    }

    return;
}

/************************************************************************/
/* Function Name    : GarpRemapWithAttrInNewInstance                    */
/*                                                                      */
/* Description      : Handles Remap when static Registration is present */
/*                                                                      */
/* Input(s)         : pAppEntry - Pointer to Application Entry          */
/*                    u2GipId  - GIP Identifier                         */
/*                    pAttr - pointer to attribute                      */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/

VOID
GarpRemapWithAttrInNewInstance (tGarpAppEntry * pAppEntry, UINT2 u2GipId,
                                tGarpAttr * pAttr)
{
    /* The function is called when the RegCount of the attr in new instance 
     * is not zero */
    UINT2               u2Offset = 0;
    tVlanId             VlanId;
    UINT1               u1PortState;
    tGarpPortEntry     *pAppPortEntry = NULL;
    tGarpGipEntry      *pGipEntry = NULL;
    tGarpAttrEntry     *pTempAttrEntry;
    tGarpGlobalPortEntry *pGlobalPortEntry;

    pAppPortEntry = pAppEntry->pPortTable;

    while (NULL != pAppPortEntry)
    {
        pTempAttrEntry = NULL;
        pGlobalPortEntry = GARP_PORT_ENTRY (pAppPortEntry->u2Port);

        if (GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId == pAppEntry->u1AppId)
        {
            u1PortState = GarpGetInstPortState (u2GipId, pGlobalPortEntry);
            MEMCPY (&VlanId, pAttr->au1AttrVal, pAttr->u1AttrLen);
            VlanId = (tVlanId) OSIX_NTOHS (VlanId);
        }
        else
        {
            u1PortState = GarpGetVlanPortState (u2GipId, pGlobalPortEntry);
            VlanId = u2GipId;
        }

        if (u1PortState == AST_PORT_STATE_FORWARDING)
        {
            u1PortState = GARP_FORWARDING;
        }
        else
        {
            u1PortState = GARP_BLOCKING;
        }

        pGipEntry = GarpGetGipEntry (pAppPortEntry, u2GipId);

        /* Get Attribute and Gip Entry */
        if (NULL != pGipEntry)
        {
            pTempAttrEntry = GarpGetAttributeEntry (pAppEntry, pGipEntry,
                                                    pAttr);
        }

        if (GARP_FORWARDING == u1PortState)
        {
            if (NULL == pTempAttrEntry)
            {
                pTempAttrEntry = GarpGetNewAttributeEntry ();

                if (NULL == pTempAttrEntry)
                {
                    pAppPortEntry = pAppPortEntry->pNextNode;
                    continue;
                }

                GarpInitAttributeEntry (pTempAttrEntry, pAttr,
                                        pAppEntry->u1AppId,
                                        pAppPortEntry->u2Port, pGipEntry);

                pGipEntry =
                    GarpAddAttributeEntry (pAppEntry, pAppPortEntry, pGipEntry,
                                           pTempAttrEntry, u2GipId);

                if (NULL == pGipEntry)
                {
                    GarpFreeAttributeEntry (pTempAttrEntry);

                    pAppPortEntry = pAppPortEntry->pNextNode;
                    continue;
                }
            }

            /* Update App SEM to VO - so that the next loop does not
             * transmit LEAVE */
            pTempAttrEntry->u1AppSemState = GARP_VO;
        }

        if (NULL != pTempAttrEntry)
        {
            GarpRemapUpdateAppSemAndTx (pAppEntry, pGipEntry,
                                        pTempAttrEntry, &u2Offset);

            GarpRemapUpdateRegSem (pAppEntry, pGipEntry, pTempAttrEntry,
                                   VlanId, GARP_TRUE);

            /* If Port is in Forwarding apply REQ_JOIN */
            if (GARP_FORWARDING == u1PortState)
            {
                GarpApplicantSem (pTempAttrEntry, pAppPortEntry,
                                  GARP_APP_REQ_JOIN);
            }

            GarpCheckAndDelAttrEntry (pAppEntry,
                                      pGipEntry->pGarpPortEntry,
                                      pGipEntry, pTempAttrEntry);
        }

        /* Before moving to next port Tx if filled buffer available */
        if (0 != u2Offset)
        {
            GarpRemapTransmitPdu (u2Offset, pAppEntry->AppAddress,
                                  pAppPortEntry->u2Port, u2GipId);

            u2Offset = 0;
        }

        pAppPortEntry = pAppPortEntry->pNextNode;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPrintPortList                                */
/*                                                                           */
/*    Description         : This function prints the port numbers set in the */
/*                          given port list other than the 'u2Port'.         */
/*                                                                           */
/*    Input(s)            : PortList - Ports to be printed.                  */
/*                          u2InPort - The Port which must not be printed.   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
GarpPrintPortList (UINT4 u4ModId, tLocalPortList PortList, UINT2 u2InPort)
{
#ifdef TRACE_WANTED
    UINT2               u2Port;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag = 0;
    UINT1               u1Flag = GARP_FALSE;
    tGarpGlobalPortEntry *pPortEntry;

    if ((GARP_TRC_FLAG & u4ModId) == 0)
    {
        return;
    }

    for (u2ByteInd = 0; u2ByteInd < VLAN_PORT_LIST_SIZE; u2ByteInd++)
    {

        if (PortList[u2ByteInd] != 0)
        {

            u1PortFlag = PortList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {

                if ((u1PortFlag & VLAN_BIT8) != 0)
                {

                    u2Port =
                        (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                 u2BitIndex + 1);
                    u2Port =
                        MEM_MAX_BYTES (u2Port, (GARP_MAX_PORTS_PER_CONTEXT));

                    pPortEntry = GARP_PORT_ENTRY (u2Port);

                    if ((pPortEntry != NULL) && (u2Port != u2InPort))
                    {

                        u1Flag = GARP_TRUE;
                        GARP_TRC_ARG1 ((GARP_MOD_TRC | VLAN_RED_TRC |
                                        GARP_RED_TRC),
                                       DATA_PATH_TRC, "", "%d ", u2Port);
                    }
                }

                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }

    if (u1Flag == GARP_FALSE)
    {
        GARP_TRC ((GARP_MOD_TRC | VLAN_RED_TRC | GARP_RED_TRC),
                  DATA_PATH_TRC, "", "NULL Port List\n");
    }
    else
    {
        GARP_TRC ((GARP_MOD_TRC | VLAN_RED_TRC | GARP_RED_TRC),
                  DATA_PATH_TRC, "", "\n");
    }
#else

    UNUSED_PARAM (u4ModId);
    UNUSED_PARAM (PortList);
    UNUSED_PARAM (u2InPort);
#endif
}

/************************************************************************/
/* Function Name    : GarpInitPort                                      */
/*                                                                      */
/* Description      : This function initializes port entry.             */
/*                                                                      */
/* Input(s)         : pGlobalPortEntry - Global port entry.             */
/*                    u2LocalPortId    - Hl port id of the given port.  */
/*                    u4IfIndex        - IfIndex of the given port.     */
/*                                                                      */
/* Output(s)        : Free Pools are created if operation is successful */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None.                                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None.                                             */
/************************************************************************/
VOID
GarpInitPort (tGarpGlobalPortEntry * pGlobalPortEntry,
              UINT2 u2LocalPortId, UINT4 u4IfIndex)
{
    pGlobalPortEntry->u4ContextId = GARP_CURR_CONTEXT_ID ();
    pGlobalPortEntry->u2LocalPortId = u2LocalPortId;

    /* In SI case need not be filled, as currently it is used 
     * in transmit thread */
    pGlobalPortEntry->u4IfIndex = u4IfIndex;
    GARP_PORT_ENTRY (u2LocalPortId) = pGlobalPortEntry;

    pGlobalPortEntry->u1OperStatus = GARP_OPER_DOWN;

    pGlobalPortEntry->u4JoinTime = GARP_DEF_JOIN_TIME;
    pGlobalPortEntry->u4LeaveTime = GARP_DEF_LEAVE_TIME;
    pGlobalPortEntry->u4LeaveAllTime = GARP_DEF_LEAVE_ALL_TIME;

    pGlobalPortEntry->pGarpPortStats =
        (tGarpPortStats *) (VOID *) GARP_GET_BUFF (GARP_PORT_STAT_BUFF,
                                                   sizeof (tGarpPortStats));

    /* If GARP Stats are not needed to be accumulated, only the first port will be allocated memory,
     * in that case, for all other port GARP_GET_BUFF will return NULL, since it is a requirement,
     * function proceeds without returing FAILURE */
    if (pGlobalPortEntry->pGarpPortStats != NULL)
    {
        MEMSET ((pGlobalPortEntry->pGarpPortStats), 0, sizeof (tGarpPortStats));
    }
    /* In case of 1 ad bridge, default pb port type is PNP.
     * In case of customer or provider bridge, the default pb
     * port type is customer port. */
    if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
    {
        GARP_PB_PORT_TYPE (u2LocalPortId) = GARP_PROVIDER_NETWORK_PORT;
    }
    else
    {
        GARP_PB_PORT_TYPE (u2LocalPortId) = GARP_CUSTOMER_BRIDGE_PORT;
    }
    if (SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_TRUE)
    {
        VlanVcmSispGetPhysicalPortOfSispPort (u4IfIndex,
                                              &(pGlobalPortEntry->
                                                u4PhyIfIndex));
    }
    else
    {
        pGlobalPortEntry->u4PhyIfIndex = u4IfIndex;
    }
}

/***************************************************************/
/*  Function Name   : GarpGetNumRegFailed                      */
/*  Description     : Geting the number of application         */
/*                   registration failure for a given          */
/*                   application                               */
/*  Input(s)        : u1AppId - Application App Id             */
/*                    u2LocalPortId - Port Number for which the 
 *                    number*/
/*                    of failed registration is to be got      */
/*                    pu4NumRegFailed - pointer to the value of*/
/*                    number of failed registrations           */
/*  Output(s)       : pu4NumRegFailed is initialised to the    */
/*                    number of failed regitration on sucess   */
/*  <OPTIONAL Fields>: None                                    */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on successful extraction of */
/*                    number of registration failures.Else     */
/*                    return GARP_FAILURE                      */
/***************************************************************/
INT4
GarpGetNumRegFailed (UINT1 u1AppId, UINT2 u2LocalPortId, UINT4 *pu4NumRegFailed)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        return GARP_FAILURE;
    }

    if (pAppEntry == NULL)
    {

        return GARP_FAILURE;
    }

    pPortEntry = GarpGetPortEntry (pAppEntry, u2LocalPortId);

    if (pPortEntry == NULL)
    {

        return GARP_FAILURE;
    }

    *pu4NumRegFailed = pPortEntry->u4NumRegFailed;

    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpGetLastPduOrigin                     */
/*  Description     :This function returns the source address  */
/*                   of the  last packet recieved              */
/*  Input(s)        :u1AppId - Application App id              */
/*                   u2LocalPortId - Port number of the        */
/*                   incoming port                             */
/*                   pMacAddr - Pointer to the output mac      */
/*                   Address of this function                  */
/*  Output(s)       : pMacAddr is initialised with the result  */
/*  <OPTIONAL Fields>:                                         */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on succesful updation of    */
/*                    LastPdu mac address else GARP_FAILURE    */
/***************************************************************/
INT4
GarpGetLastPduOrigin (UINT1 u1AppId, UINT2 u2LocalPortId, tMacAddr MacAddr)
{
    tGarpAppEntry      *pAppEntry;
    tGarpPortEntry     *pPortEntry;

    pAppEntry = GARP_GET_APP_ENTRY (u1AppId);

    if (GARP_IS_GARP_ENABLED () == GARP_FALSE)
    {

        return GARP_FAILURE;
    }

    if (pAppEntry == NULL)
    {

        return GARP_FAILURE;
    }

    pPortEntry = GarpGetPortEntry (pAppEntry, u2LocalPortId);

    if (pPortEntry == NULL)
    {

        return GARP_FAILURE;
    }

    MEMCPY (MacAddr, pPortEntry->LastPduOrigin, GARP_MAC_ADDR_LEN);

    return GARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : GarpGetNodeStatus                                    */
/*                                                                           */
/* Description        : This function function gets the node status from     */
/*                      VLAN module.                                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
tGarpNodeStatus
GarpGetNodeStatus (VOID)
{
    tVlanNodeStatus     VlanNodeStatus;
    tGarpNodeStatus     GarpNodeStatus;

    VlanNodeStatus = GarpVlanGetNodeStatus ();

    switch (VlanNodeStatus)
    {
        case VLAN_NODE_IDLE:
            GarpNodeStatus = GARP_NODE_IDLE;
            break;

        case VLAN_NODE_ACTIVE:
            GarpNodeStatus = GARP_NODE_ACTIVE;
            break;

        case VLAN_NODE_STANDBY:
            GarpNodeStatus = GARP_NODE_STANDBY;
            break;

        case VLAN_NODE_SWITCHOVER_IN_PROGRESS:
            GarpNodeStatus = GARP_NODE_ACTIVE;
            break;

        default:
            GarpNodeStatus = GARP_NODE_IDLE;
            break;
    }

    return GarpNodeStatus;
}

/************************************************************************/
/* Function Name    : GarpHandleBridgeMode                              */
/*                                                                      */
/* Description      : This function assigns bridge mode in GARP module. */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None.                                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : GARP_SUCCESS / GARP_FAILURE.                      */
/************************************************************************/

INT4
GarpHandleBridgeMode (VOID)
{
    if (GarpL2IwfGetBridgeMode (GARP_CURR_CONTEXT_ID (),
                                &GARP_BRIDGE_MODE ()) != L2IWF_SUCCESS)
    {
        return GARP_FAILURE;
    }

    if (GARP_BRIDGE_MODE () == GARP_INVALID_BRIDGE_MODE)
    {
        return GARP_FAILURE;
    }

    switch (GARP_BRIDGE_MODE ())
    {
        case GARP_PROVIDER_CORE_BRIDGE_MODE:
            /* Delibrate fall through. */
        case GARP_PROVIDER_EDGE_BRIDGE_MODE:
            MEMCPY (GARP_CTXT_GVRP_ADDR (), gProviderGvrpAddr,
                    sizeof (tMacAddr));
            break;
        case GARP_PROVIDER_BRIDGE_MODE:
            /* Delibrate fall through. */
        case GARP_CUSTOMER_BRIDGE_MODE:
            MEMCPY (GARP_CTXT_GVRP_ADDR (), gCustomerGvrpAddr,
                    sizeof (tMacAddr));
            break;
        case GARP_PBB_ICOMPONENT_BRIDGE_MODE:
        case GARP_PBB_BCOMPONENT_BRIDGE_MODE:
            break;

        default:
            return GARP_FAILURE;
    }

    return GARP_SUCCESS;
}

/************************************************************************/
/* Function Name    : GarpAppAdminEnable                                */
/*                                                                      */
/* Description      : This function sets the admin status for garp      */
/*                    applications as enabled. This function will be    */
/*                    called only during context creation if the bridge */
/*                    mode is available.                                */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None.                                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None.                                             */
/************************************************************************/
VOID
GarpAppAdminEnable (VOID)
{
    GVRP_ADMIN_STATUS () = GVRP_ENABLED;

    if (GARP_IS_1AD_BRIDGE () == GARP_TRUE)
    {
        GMRP_ADMIN_STATUS () = GMRP_DISABLED;
    }
    else
    {
        GMRP_ADMIN_STATUS () = GMRP_ENABLED;
    }
    return;
}

/***************************************************************/
/*  Function Name   : GarpIsGarpEnabled                        */
/*  Description     : Returns the GARP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Global Variables Referred : gpGarpContextInfo->u1GarpShutDownStatus          */
/*  Global variables Modified :                                */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_ENABLED /GARP_DISABLED              */
/***************************************************************/
INT4
GarpIsGarpEnabled ()
{
    return ((gau1GarpShutDownStatus[GARP_CURR_CONTEXT_PTR ()->u4ContextId] ==
             GARP_SNMP_FALSE) ? GARP_TRUE : GARP_FALSE);
}

/*****************************************************************************/
/* Function Name      : GarpPortTblCmpFn                                     */
/*                                                                           */
/* Description        : This routine will be invoked by the RB Tree library  */
/*                      to traverse through the global port based RB         */
/*                      tree.                                                */
/*                                                                           */
/* Input(s)           : pPort1 - Pointer to the first port entry.            */
/*                      pPort2 - Pointer to the second port entry.           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1, if pPort1 is greater than pPort2                  */
/*                     -1, if pPort2 is greater than pPort1                  */
/*                      0, if pPort2 and pPort1 are equal.                   */
/*****************************************************************************/
INT4
GarpPortTblCmpFn (tRBElem * pPort1, tRBElem * pPort2)
{
    tGarpGlobalPortEntry *pGarpGlobalPortEntry1 =
        (tGarpGlobalPortEntry *) pPort1;
    tGarpGlobalPortEntry *pGarpGlobalPortEntry2 =
        (tGarpGlobalPortEntry *) pPort2;

    if (pGarpGlobalPortEntry1->u4IfIndex > pGarpGlobalPortEntry2->u4IfIndex)
    {
        return 1;
    }
    else if (pGarpGlobalPortEntry1->u4IfIndex <
             pGarpGlobalPortEntry2->u4IfIndex)
    {
        return -1;
    }
    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpGetContextInfoFromIfIndex                    */
/*                                                                           */
/*    Description         : This function gets the context information for an*/
/*                          IfIndex                                          */
/*                                                                           */
/*    Input(s)            : u4IfIndex- Interface Index                       */
/*                                                                           */
/*    Output(s)           : pu4ContextId- Pointer to context Identifier      */
/*                          pu2LocalPortId - Pointer to local port           */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified :                                            */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS/GARP_FAILURE                       . */
/*****************************************************************************/

INT4
GarpGetContextInfoFromIfIndex (UINT2 u2IfIndex, UINT4 *pu4ContextId,
                               UINT2 *pu2LocalPortId)
{
    tGarpGlobalPortEntry *pGarpGlobalPortEntry = NULL;

    *pu4ContextId = 0;
    *pu2LocalPortId = GARP_INVALID_PORT;

    pGarpGlobalPortEntry = GarpGetIfIndexEntry (u2IfIndex);
    if (pGarpGlobalPortEntry == NULL)
    {
        return GARP_FAILURE;
    }
    *pu4ContextId = pGarpGlobalPortEntry->u4ContextId;
    *pu2LocalPortId = pGarpGlobalPortEntry->u2LocalPortId;
    return GARP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : GarpGetIfIndexEntry                                  */
/*                                                                           */
/* Description        : Returns a pointer to a port Entry for given IfIndex. */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the PortEntry if key found.               */
/*                      NULL if not found                                    */
/*****************************************************************************/

tGarpGlobalPortEntry *
GarpGetIfIndexEntry (UINT4 u4IfIndex)
{
    tGarpGlobalPortEntry GlobalPortEntry;

    GlobalPortEntry.u4IfIndex = u4IfIndex;

    return (tGarpGlobalPortEntry *) RBTreeGet (gGarpPortTable,
                                               (tRBElem *) & GlobalPortEntry);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpGetFirstPortInSystem                         */
/*                                                                           */
/*    Description         : This function is used to get the first port      */
/*                          in the system.                                   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pu4IfIndex - Interface Index.                    */
/*                                                                           */
/*    Global Variables Referred : gGarpPortTable                             */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS/GARP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpGetFirstPortInSystem (UINT4 *pu4IfIndex)
{
    tGarpGlobalPortEntry *pPortEntry = NULL;

    pPortEntry = (tGarpGlobalPortEntry *) RBTreeGetFirst (gGarpPortTable);

    if (pPortEntry != NULL)
    {
        *pu4IfIndex = pPortEntry->u4IfIndex;
        return GARP_SUCCESS;
    }
    return GARP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpGetNextPortInSystem                          */
/*                                                                           */
/*    Description         : This function is used to get the next port       */
/*                          in the system.                                   */
/*                                                                           */
/*    Input(s)            : i4IfIndex - Current IfIndex.                     */
/*                                                                           */
/*    Output(s)           : pu4IfIndex - Next Interface Index.               */
/*                                                                           */
/*    Global Variables Referred : gGarpPortTable                             */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS/GARP_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpGetNextPortInSystem (INT4 i4IfIndex, UINT4 *pu4IfIndex)
{
    tGarpGlobalPortEntry *pPortEntry = NULL;
    tGarpGlobalPortEntry Dummy;

    Dummy.u4IfIndex = (UINT4) i4IfIndex;

    pPortEntry = RBTreeGetNext (gGarpPortTable, &Dummy, NULL);

    if (pPortEntry == NULL)
    {
        return GARP_FAILURE;
    }

    *pu4IfIndex = pPortEntry->u4IfIndex;
    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpHandleCreateContext                          */
/*                                                                           */
/*    Description         : This function creates given context              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE.                      */
/*                                                                           */
/*****************************************************************************/
UINT4
GarpHandleCreateContext (UINT4 u4ContextId)
{

    /*Check whether the context exists
     *If already exits no need to create */
    if (NULL != GARP_CONTEXT_PTR (u4ContextId))
    {
        return GARP_SUCCESS;
    }

    GARP_CURR_CONTEXT_PTR () = (tGarpContextInfo *) (VOID *) GARP_GET_BUFF
        (GARP_CONTEXT_INFO, sizeof (tGarpContextInfo));

    if (GARP_CURR_CONTEXT_PTR () == NULL)
    {
        return GARP_FAILURE;
    }
    /* store the context pointer in the global array  */
    GARP_CONTEXT_PTR (u4ContextId) = GARP_CURR_CONTEXT_PTR ();
    /* Initialise per context default values */
    /*Initialise per context global variables */

    if (GarpSelectContext (u4ContextId) == GARP_FAILURE)
    {
        return GARP_FAILURE;
    }
    GARP_CURR_CONTEXT_PTR ()->GarpContextPortInfo =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tGarpGlobalPortEntry, NextPortNode),
                              (tRBCompareFn) GarpPortTblCmpFn);

    GARP_CURR_CONTEXT_PTR ()->u1GvrpAdminStatus = GVRP_DISABLED;
    gau1GvrpStatus[u4ContextId] = GVRP_DISABLED;
    GARP_CURR_CONTEXT_PTR ()->u1GvrpAppId = GARP_INVALID_APP_ID;
    GARP_CURR_CONTEXT_PTR ()->u1GmrpAdminStatus = GMRP_DISABLED;
    gau1GmrpStatus[u4ContextId] = GMRP_DISABLED;
    GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId = GARP_INVALID_APP_ID;
    GARP_CURR_CONTEXT_PTR ()->u4GarpTrcOption = 0;
    GARP_CURR_CONTEXT_PTR ()->u4ContextId = u4ContextId;
    GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf =
        GARP_GET_BUFF (GARP_REMAP_TXBUFF_INFO, GARP_MAX_MSG_LEN);

    MEMSET (GARP_CURR_CONTEXT_STR (), 0, GARP_CONTEXT_ALIAS_LEN + 1);
    GarpVcmGetAliasName (u4ContextId, GARP_CURR_CONTEXT_STR ());
    STRCAT (GARP_CURR_CONTEXT_STR (), ":");
    if (GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf == NULL)
    {
        GARP_RELEASE_BUFF (GARP_CONTEXT_INFO,
                           (UINT1 *) GARP_CURR_CONTEXT_PTR ());
        GarpReleaseContext ();
        return GARP_FAILURE;
    }

    if (GarpL2IwfGetBridgeMode (GARP_CURR_CONTEXT_ID (),
                                &(GARP_BRIDGE_MODE ())) != L2IWF_SUCCESS)
    {
        GARP_RELEASE_BUFF (GARP_CONTEXT_INFO,
                           (UINT1 *) GARP_CURR_CONTEXT_PTR ());
        GarpReleaseContext ();
        return GARP_FAILURE;
    }

    if ((GARP_BRIDGE_MODE () == GARP_INVALID_BRIDGE_MODE))
    {
        GarpReleaseContext ();
        return GARP_SUCCESS;
    }

    if (GarpInit () == GARP_FAILURE)
    {
        GarpDeInit ();
        GARP_RELEASE_BUFF (GARP_REMAP_TXBUFF_INFO,
                           GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf);
        GARP_RELEASE_BUFF (GARP_CONTEXT_INFO,
                           (UINT1 *) GARP_CURR_CONTEXT_PTR ());
        GarpReleaseContext ();
        gau1GarpShutDownStatus[u4ContextId] = GARP_SNMP_TRUE;
        GARP_CONTEXT_PTR (u4ContextId) = GARP_CURR_CONTEXT_PTR ();

        return GARP_FAILURE;
    }

    /* Make the admin status for garp applications as
     * enabled. */
    GarpAppAdminEnable ();

    /*GVRP and GMRP will be enabled inside this function */
    GarpAppModuleEnable ();

    gu1GarpIsInitComplete = GARP_TRUE;
    GarpReleaseContext ();
    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpHandleDeleteContext                          */
/*                                                                           */
/*    Description         : This function deletes given context              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE.                      */
/*                                                                           */
/*****************************************************************************/
UINT4
GarpHandleDeleteContext (UINT4 u4ContextId)
{

    if (GvrpDisable () != GVRP_SUCCESS)
    {
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  "GVRP Disable FAILED during Active to Standby Transition\n");
    }

    if (GmrpDisable () != GMRP_SUCCESS)
    {
        GARP_TRC (GARP_RED_TRC, CONTROL_PLANE_TRC, GARP_NAME,
                  "GMRP Disable FAILED during Active to Standby Transition\n");
    }

    if (GarpDeInit () == GARP_FAILURE)
    {
        GarpReleaseContext ();
        return GARP_FAILURE;
    }

    GARP_RELEASE_BUFF (GARP_REMAP_TXBUFF_INFO,
                       GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf);
    GARP_CURR_CONTEXT_PTR ()->pu1RemapTxBuf = NULL;

    if (GARP_CURR_CONTEXT_PTR ()->GarpContextPortInfo != NULL)
    {
        RBTreeDestroy (GARP_CURR_CONTEXT_PTR ()->GarpContextPortInfo,
                       (tRBKeyFreeFn) GarpFreeGlobPortEntry, 0);
    }
    GARP_RELEASE_BUFF (GARP_CONTEXT_INFO, GARP_CURR_CONTEXT_PTR ());
    GarpReleaseContext ();
    GARP_CONTEXT_PTR (u4ContextId) = NULL;

    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpHandleUpdateContextName                      */
/*                                                                           */
/*    Description         : This function updated the name of the context.   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE.                      */
/*                                                                           */
/*****************************************************************************/
UINT4
GarpHandleUpdateContextName (UINT4 u4ContextId)
{
    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return GARP_FAILURE;
    }

    MEMSET (GARP_CURR_CONTEXT_STR (), 0, GARP_CONTEXT_ALIAS_LEN + 1);

    if (GarpVcmGetAliasName (u4ContextId,
                             GARP_CURR_CONTEXT_STR ()) != VCM_SUCCESS)
    {
        return GARP_FAILURE;
    }
    STRCAT (GARP_CURR_CONTEXT_STR (), ":");

    GarpReleaseContext ();
    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpFlushContextsQueue                           */
/*                                                                           */
/*    Description         : This function flushes the queue                  */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE.                      */
/*                                                                           */
/*****************************************************************************/
VOID
GarpFlushContextsQueue (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pMsg;
    tGarpQMsg          *pGarpQMsg = NULL;
    /* Flush All Q Messages */

    if (gGarpQId != 0)
    {
        while (GARP_RECV_FROM_QUEUE (GARP_TASK_QUEUE_ID,
                                     (UINT1 *) &pMsg,
                                     OSIX_DEF_MSG_LEN,
                                     OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            GARP_RELEASE_BUFF (GARP_SYS_BUFF, pMsg);
        }
    }

    if (gGarpCfgQId != 0)
    {
        while (GARP_RECV_FROM_QUEUE (GARP_CFG_QUEUE_ID,
                                     (UINT1 *) &pGarpQMsg,
                                     OSIX_DEF_MSG_LEN,
                                     OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);
        }
    }
    return;
}

/************************************************************************/
/* Function Name    : GarpInitAndAddPortEntry                           */
/*                                                                      */
/* Description      : Adds the portentry to global and local RBTree.    */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : GARP_SUCCESS/GARP_FAILURE                         */
/************************************************************************/
INT4
GarpInitAndAddPortEntry (tGarpGlobalPortEntry * pGlobalPortEntry,
                         UINT2 u2LocalPortId, UINT4 u4IfIndex)
{
    /* Init pGlobalPortEntry. */
    GarpInitPort (pGlobalPortEntry, u2LocalPortId, u4IfIndex);

    if (RBTreeAdd (GARP_CURR_CONTEXT_PTR ()->GarpContextPortInfo,
                   (tRBElem *) pGlobalPortEntry) == RB_FAILURE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Unable to add port to global RBTree \n");
        GARP_RELEASE_BUFF (GARP_GLOBPORT_ENTRY, (UINT1 *) pGlobalPortEntry);
        GARP_PORT_ENTRY (u2LocalPortId) = NULL;
        return GARP_FAILURE;
    }
    /* Add port entry to global RBTree  */
    if (RBTreeAdd (gGarpPortTable, (tRBElem *) pGlobalPortEntry) == RB_FAILURE)
    {
        GARP_TRC (GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                  "Unable to add port to global RBTree \n");
        RBTreeRemove (GARP_CURR_CONTEXT_PTR ()->GarpContextPortInfo,
                      pGlobalPortEntry);
        GARP_RELEASE_BUFF (GARP_GLOBPORT_ENTRY, (UINT1 *) pGlobalPortEntry);
        GARP_PORT_ENTRY (u2LocalPortId) = NULL;
        return GARP_FAILURE;
    }
    return GARP_SUCCESS;
}

/************************************************************************/
/* Function Name    : GarpRemovePortEntry                               */
/*                                                                      */
/* Description      : Removes the portentry from local and global RBTree*/
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
VOID
GarpRemovePortEntry (VOID)
{
    UINT2               u2LocalPortId;
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;

    for (u2LocalPortId = 1; u2LocalPortId <= GARP_MAX_PORTS_PER_CONTEXT;
         u2LocalPortId++)
    {

        /* Set Gvrp and Gmrp disbaled for this port in l2iwf. */
        GarpL2IwfSetProtocolEnabledStatusOnPort (GARP_CURR_CONTEXT_ID (),
                                                 u2LocalPortId,
                                                 L2_PROTO_GVRP, OSIX_DISABLED);
        GarpL2IwfSetProtocolEnabledStatusOnPort (GARP_CURR_CONTEXT_ID (),
                                                 u2LocalPortId,
                                                 L2_PROTO_GMRP, OSIX_DISABLED);

        pGlobalPortEntry = GARP_PORT_ENTRY (u2LocalPortId);
        if (pGlobalPortEntry)
        {
            /* Remove the node from global RBTree */
            RBTreeRemove (gGarpPortTable, pGlobalPortEntry);
            /* delete the node from per context RBTree */
            RBTreeRemove (GARP_CURR_CONTEXT_PTR ()->GarpContextPortInfo,
                          pGlobalPortEntry);
            GARP_RELEASE_BUFF (GARP_GLOBPORT_ENTRY, (UINT1 *) pGlobalPortEntry);
            GARP_PORT_ENTRY (u2LocalPortId) = NULL;
        }
    }

    return;
}

/************************************************************************/
/* Function Name    : GarpRemovePortFromIfIndexTable                    */
/*                                                                      */
/* Description      : Removes the port entry from both the Global and   */
/*                    local RBTree.                                     */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gGarpPortTable                                    */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
VOID
GarpRemovePortFromIfIndexTable (tGarpGlobalPortEntry * pGlobPortEntry)
{
    /* delete the node from per context RBTree */
    RBTreeRemove (GARP_CURR_CONTEXT_PTR ()->GarpContextPortInfo,
                  pGlobPortEntry);
    /* Remove the node from global RBTree */
    RBTreeRemove (gGarpPortTable, pGlobPortEntry);
}

/************************************************************************/
/* Function Name    : GarpFreeGlobPortEntry                             */
/*                                                                      */
/* Description      : Releases the Port Entry to the free pool          */
/*                                                                      */
/* Input(s)         : Pointer to the Port entry to be released.         */
/*                                                                      */
/* Output(s)        : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None.                                             */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None.                                             */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None.                                             */
/*                                                                      */
/* Use of Recursion : None.                                             */
/*                                                                      */
/* Returns          : VOID                                              */
/************************************************************************/
INT4
GarpFreeGlobPortEntry (tRBElem * pElem, UINT4 u4Arg)
{
    GARP_RELEASE_BUFF (GARP_GLOBPORT_ENTRY, (tGarpGlobalPortEntry *) pElem);
    UNUSED_PARAM (u4Arg);
    return 1;
}

/**************************************************************************/
/* Function Name       : GarpGblTrace                                     */
/*                                                                        */
/* Description         : This function prints the trace message, the      */
/*                       u4ModTrc and u4Mask will be ignore and           */
/*                       u4Mask is set to 1, and will be AND with         */
/*                       GARP_GBL_TRC_FLAG to decide toprint the buffer or*/
/*                       not                                              */
/*                                                                        */
/* Input(s)            : u4ContextId - context for whcich the trace is    */
/*                                     enabled                            */
/*                       u4ModTrc - Which module trace needs to be        */
/*                                    enabled, currently UNUSED           */
/*                       ModuleName - Which module trcaes needs to be     */
/*                                    enabled, like GARP, GVRP , GMRP     */
/*                                 , UNUSED                               */
/*                       u4Mask - Which path trcaes needs to be enabled   */
/*                                 , UNUSED                               */
/*                       Fmt - print string                               */
/*                                                                        */
/* Output(s)           :None                                              */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/
VOID
GarpGblTrace (UINT4 u4ContextId, UINT4 u4ModTrc, UINT4 u4Mask,
              const char *ModuleName, const char *Fmt, ...)
{
    va_list             ap;
    static CHR1         Str[GARP_TRC_BUF_SIZE];
    UINT4               i;

    MEMSET (&ap, 0, sizeof (va_list));
    u4Mask = 1;
    UNUSED_PARAM (u4ModTrc);

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        return;
    }

    if (u4ContextId == GARP_INVALID_CONTEXT_ID)
    {
        va_start (ap, Fmt);
        vsprintf (&Str[0], Fmt, ap);
        va_end (ap);
        UtlTrcLog (GARP_GBL_TRC_FLAG, u4Mask, ModuleName, Str);
    }
    else
    {
        va_start (ap, Fmt);
        i = sprintf (&Str[0], "ContextId-%u:", u4ContextId);
        vsprintf (&Str[i], Fmt, ap);
        va_end (ap);
        UtlTrcLog (GARP_GBL_TRC_FLAG, u4Mask, ModuleName, Str);
    }
}

#ifdef TRACE_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPrintMacAddr                                 */
/*                                                                           */
/*    Description         : This function prints the given mac address.      */
/*                                                                           */
/*    Input(s)            : u4ModId - Module identifier.                     */
/*                          pMacAddr - Pointer to the mac address to be      */
/*                          printed.                                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
GarpPrintMacAddr (UINT4 u4ModId, tMacAddr MacAddr)
{
    UINT1               u1MacLen;
    UINT1               au1MacAddr[VLAN_MAC_ADDR_LEN];

    if ((GARP_TRC_FLAG & u4ModId) == 0)
    {
        return;
    }

    MEMCPY (au1MacAddr, MacAddr, ETHERNET_ADDR_SIZE);

    for (u1MacLen = 0; u1MacLen < VLAN_MAC_ADDR_LEN; u1MacLen++)
    {

        GARP_TRC_ARG1 ((GARP_MOD_TRC | VLAN_RED_TRC | GARP_RED_TRC),
                       CONTROL_PLANE_TRC | DATA_PATH_TRC, "",
                       "%x", au1MacAddr[u1MacLen]);

        if (u1MacLen != VLAN_MAC_ADDR_LEN - 1)
        {

            GARP_TRC ((GARP_MOD_TRC | VLAN_RED_TRC | GARP_RED_TRC),
                      CONTROL_PLANE_TRC | DATA_PATH_TRC, "", ":");
        }
    }

    GARP_TRC ((GARP_MOD_TRC | VLAN_RED_TRC | GARP_RED_TRC),
              CONTROL_PLANE_TRC | DATA_PATH_TRC, "", "\n");
}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpUpdateGarpAppsStatusOnAllPorts               */
/*                                                                           */
/*    Description         : This function sets the GVRP status on all ports  */
/*                          in the given to given value.                     */
/*    Input(s)            : u4ContextId                                      */
/*                          u1Status - GVRP Enable/disable                   */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE.                      */
/*                                                                           */
/*****************************************************************************/
UINT4
GarpUpdateGarpAppStatusOnAllPorts (UINT4 u4ContextId, UINT1 u1Status,
                                   UINT4 u4AppId)
{
    tGarpGlobalPortEntry *pGlobalPortEntry = NULL;
    UINT2               u2Port = 1;

    if (GarpSelectContext (u4ContextId) != GARP_SUCCESS)
    {
        return GARP_FAILURE;
    }

    do
    {
        pGlobalPortEntry = GARP_PORT_ENTRY (u2Port);

        if (pGlobalPortEntry != NULL)
        {
            if (GARP_ENABLED == u1Status)
            {
                if (u4AppId == GVRP_APP_ID)
                {
                    GvrpEnablePort (u2Port);
                }
                else
                {
                    GmrpEnablePort (u2Port);
                }
            }
            else
            {
                if (u4AppId == GVRP_APP_ID)
                {
                    GvrpDisablePort (u2Port);
                }
                else
                {
                    GmrpDisablePort (u2Port);
                }
            }
        }

        u2Port++;

    }
    while (u2Port <= GARP_MAX_PORTS_PER_CONTEXT);

    return GARP_SUCCESS;
}
