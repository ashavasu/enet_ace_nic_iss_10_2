
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : garpgid.c                                   */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : GID                                         */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains the Gid routines         */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/

#include "garpinc.h"

/***************************************************************/
/*  Function Name   : GarpGidGipAttributeJoinReq               */
/*  Description     : This gip function is called by GID to    */
/*                    indicate the Join req to other ports     */
/*  Input(s)        : pAttr -pointer to the GarpAttribute      */
/*                    structure                                */
/*                    pPortEntry - Pointer to the port entry   */
/*                    pIfMsg - Pointer to the IfMsg Structure  */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpGidGipAttributeJoinReq (tGarpAttr * pAttr,
                            tGarpPortEntry * pPortEntry, tGarpIfMsg * pIfMsg)
{
    tGarpAppEntry      *pAppEntry;
    tGarpAttrEntry     *pAttrEntry = NULL;
    tGarpGipEntry      *pGipEntry;
    UINT2               u2GipId;
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;
    tGarpGlobalPortEntry *pGlobalPortEntry;

    if (pPortEntry->u1AppStatus == GARP_DISABLED)
    {
        /* Application is disabled on this port. Ignore the JOIN now. */
        return;
    }
    pAppEntry = pIfMsg->pAppEntry;
    u2GipId = pIfMsg->u2GipId;

    pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);
    pGlobalPortEntry = GARP_PORT_ENTRY (pPortEntry->u2Port);

    if (pGlobalPortEntry == NULL)
    {
        return;
    }

    if (pGipEntry == NULL)
    {
        if (pIfMsg->pAppEntry->u1AppId == GARP_CURR_CONTEXT_PTR ()->u1GmrpAppId)
        {
            u1PortState = GarpGetVlanPortState (u2GipId, pGlobalPortEntry);
        }
        else
        {
            u1PortState = GarpGetInstPortState (u2GipId, pGlobalPortEntry);
        }

        if (u1PortState != AST_PORT_STATE_FORWARDING)
        {
            return;
        }
    }

    if ((u1PortState == AST_PORT_STATE_FORWARDING) ||
        (pGipEntry->u1StapStatus == GARP_FORWARDING))
    {
        if (NULL != pGipEntry)
        {
            pAttrEntry = GarpGetAttributeEntry (pAppEntry, pGipEntry, pAttr);
        }

        if (pAttrEntry == NULL)
        {

            pAttrEntry = GarpGetNewAttributeEntry ();

            if (pAttrEntry == NULL)
            {

                return;
            }

            GarpInitAttributeEntry (pAttrEntry, pAttr, pAppEntry->u1AppId,
                                    pPortEntry->u2Port, pGipEntry);

            /* Pass GipId here as the GipEntry can be NULL. In this case
               new Gip will be created and added inside AddAttributeEntry */
            pGipEntry = GarpAddAttributeEntry (pAppEntry, pPortEntry,
                                               pGipEntry, pAttrEntry, u2GipId);

            if (NULL == pGipEntry)
            {

                GarpFreeAttributeEntry (pAttrEntry);

                return;
            }
        }

        GarpApplicantSem (pAttrEntry, pPortEntry, GARP_APP_REQ_JOIN);
    }
}

/***************************************************************/
/*  Function Name   : GarpGidGipAttributeLeaveReq              */
/*  Description     : This gip function is called by GID to    */
/*                    indicate the LeaveReq to other ports     */
/*  Input(s)        : pAttr -pointer to the GarpAttribute      *
 *                    structure                                */
/*                    pPortEntry - Pointer to the port entry   */
/*                    pIfMsg - Pointer to the IfMsg Structure  */
/*  Output(s)       : None                                     */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpGidGipAttributeLeaveReq (tGarpAttr * pAttr,
                             tGarpPortEntry * pPortEntry, tGarpIfMsg * pIfMsg)
{
    tGarpAppEntry      *pAppEntry;
    tGarpAttrEntry     *pAttrEntry;
    tGarpGipEntry      *pGipEntry = NULL;
    UINT2               u2GipId;

    if (pPortEntry->u1AppStatus == GARP_DISABLED)
    {
        /* GARP Application disabled on this port...Ignore LEAVE now... */
        return;
    }
    pAppEntry = pIfMsg->pAppEntry;
    u2GipId = pIfMsg->u2GipId;

    pGipEntry = GarpGetGipEntry (pPortEntry, u2GipId);

    if (pGipEntry == NULL)
    {
        /* Gip Associated with Attr is not present. So return here. */
        return;
    }

    if (pGipEntry->u1StapStatus == GARP_FORWARDING)
    {
        pAttrEntry = GarpGetAttributeEntry (pAppEntry, pGipEntry, pAttr);

        if (pAttrEntry == NULL)
        {

            return;
        }

        GarpApplicantSem (pAttrEntry, pPortEntry, GARP_APP_REQ_LEAVE);

        GarpCheckAndDelAttrEntry (pAppEntry, pPortEntry, pGipEntry, pAttrEntry);
    }
}
