/* $Id: garpapi.c,v 1.46 2014/01/24 12:26:18 siva Exp $              */
/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc., 2001-2002                   */
/*                                                                      */
/*  FILE NAME             : garpapi.c                                   */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                             */
/*  SUBSYSTEM NAME        : API                                         */
/*  MODULE NAME           : GARP                                        */
/*  LANGUAGE              : C                                           */
/*  TARGET ENVIRONMENT    : Any                                         */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                 */
/*  AUTHOR                : Aricent Inc.                             */
/*  DESCRIPTION           : This file contains the API routines         */
/*                          offered by GARP                             */
/************************************************************************/
/*                                                                      */
/*  Change History                                                      */
/*  Version               : Initial                                     */
/*  Date(DD/MM/YYYY)      : 26 Nov 2001                                 */
/*  Modified by           : Vlan Team                                   */
/*  Description of change : Initial Creation                            */
/************************************************************************/
#ifdef GARP_WANTED

#include "garpinc.h"

/***************************************************************/
/*  Function Name   : GarpCreatePort                           */
/*  Description     : This function is for adding a port.      */
/*  Input(s)        : u2Port - Port to be added.               */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : gpGarpContextInfo->apGarpGlobalPortEntry          */
/*  Global variables Modified : gpGarpContextInfo->apGarpGlobalPortEntry          */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on successful addition of   */
/*                    port. GARP_FAILURE on failure            */
/***************************************************************/
INT4
GarpCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    INT4                i4RetVal;

    i4RetVal = GarpPostCfgMessage (GARP_PORT_CREATE_MSG, u2LocalPortId,
                                   u4ContextId, u4IfIndex);

    if (i4RetVal == GARP_SUCCESS)
    {
        L2_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/***************************************************************/
/*  Function Name   : GarpDeletePort                           */
/*  Description     : This function is for deleting a port.    */
/*  Input(s)        : u2Port - Port to be deleted              */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on successful deletion  of  */
/*                    port. GARP_FAILURE on failure            */
/***************************************************************/
INT4
GarpDeletePort (UINT2 u2IfIndex)
{
    INT4                i4RetVal;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    /*Assumption is VCM still has the context Info when delete 
     * port indication comes to GARP*/
    if (GarpVcmGetContextInfoFromIfIndex (u2IfIndex, &u4ContextId,
                                          &u2LocalPortId) == VCM_FAILURE)
    {
        return GARP_FAILURE;
    }
    i4RetVal = GarpPostCfgMessage (GARP_PORT_DELETE_MSG,
                                   u2LocalPortId, u4ContextId, u2IfIndex);

    return i4RetVal;
}

/************************************************************************/
/*  Function Name    : GarpPortOperInd ()                               */
/*  Description      : Invoked by VLAN whenever Port Oper Status is     */
/*                     changed. If Link Aggregation is enabled, then    */
/*                     Oper Down indication is given for each port      */
/*                     initially. When the Link Aggregation Group is    */
/*                     formed, then Oper Up indication is given.        */
/*                     In the Oper Down state, frames will neither      */
/*                     be transmitted nor be received.                  */
/*  Input(s)         : u2Port - Port for which Oper Status is changed.  */
/*                     u1OperStatus - GAARP_OPER_UP/GARP_OPER_DOWN      */
/*  Output(s)        : None                                             */
/*  Global Variables                                                    */
/*  Referred         : None                                             */
/*  Global Variables                                                    */
/*  Modified         : None                                             */
/*  Exceptions or OS                                                    */
/*  Error Handling   : None                                             */
/*  Use of Recursion : None                                             */
/*  Returns          : GARP_SUCCESS / GARP_FAILURE                      */
/************************************************************************/
INT4
GarpPortOperInd (UINT2 u2IfIndex, UINT1 u1OperStatus)
{
    tL2IwfContextInfo   L2IwfContextInfo;

    /* Get the Context Information before calling GarpWrPortOperInd ()
     * for backward compatibility */
    if (VcmGetContextInfoFromIfIndex
        ((UINT4) u2IfIndex, &L2IwfContextInfo.u4ContextId,
         &L2IwfContextInfo.u2LocalPortId) == VCM_FAILURE)
    {
        return GARP_FAILURE;
    }

    return (GarpWrPortOperInd (u2IfIndex, u1OperStatus, &L2IwfContextInfo));
}

/************************************************************************/
/*  Function Name    : GarpWrPortOperInd ()                               */
/*  Description      : Invoked by VLAN whenever Port Oper Status is     */
/*                     changed. If Link Aggregation is enabled, then    */
/*                     Oper Down indication is given for each port      */
/*                     initially. When the Link Aggregation Group is    */
/*                     formed, then Oper Up indication is given.        */
/*                     In the Oper Down state, frames will neither      */
/*                     be transmitted nor be received.                  */
/*  Input(s)         : u2Port - Port for which Oper Status is changed.  */
/*                     u1OperStatus - GAARP_OPER_UP/GARP_OPER_DOWN      */
/*                     pL2IwfContextInfo - Interface Context Information*/
/*  Output(s)        : None                                             */
/*  Global Variables                                                    */
/*  Referred         : None                                             */
/*  Global Variables                                                    */
/*  Modified         : None                                             */
/*  Exceptions or OS                                                    */
/*  Error Handling   : None                                             */
/*  Use of Recursion : None                                             */
/*  Returns          : GARP_SUCCESS / GARP_FAILURE                      */
/************************************************************************/
INT4
GarpWrPortOperInd (UINT2 u2IfIndex, UINT1 u1OperStatus,
                   tL2IwfContextInfo * pContextInfo)
{
    INT4                i4RetVal;

    if (u1OperStatus == GARP_OPER_UP)
    {
        i4RetVal = GarpPostCfgMessage (GARP_PORT_OPER_UP_MSG,
                                       pContextInfo->u2LocalPortId,
                                       pContextInfo->u4ContextId, u2IfIndex);
    }
    else
    {
        i4RetVal = GarpPostCfgMessage (GARP_PORT_OPER_DOWN_MSG,
                                       pContextInfo->u2LocalPortId,
                                       pContextInfo->u4ContextId, u2IfIndex);
    }

    return i4RetVal;
}

/***************************************************************/
/*  Function Name   : GarpStapPortStateChange                  */
/*  Description     : This function changes the state of the   */
/*                    stap for the given port                  */
/*  Input(s)        : u2port - Port number whose state as      */
/*                   changed                                   */
/*                    u1State - New state of the port          */
/*                    u2GipId - Group Information propagation ID*/
/*  Output(s)       : State of the port gets updated           */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/

void
GarpStapPortStateChange (UINT2 u2IfIndex, UINT1 u1State, UINT2 u2GipId)
{
    tGarpQMsg          *pGarpQMsg = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return;
    }

    if (GarpVcmGetContextInfoFromIfIndex (u2IfIndex, &u4ContextId,
                                          &u2LocalPortId) == VCM_FAILURE)
    {
        return;
    }

    UNUSED_PARAM (u2LocalPortId);

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        return;
    }

    /* Here u2GipId is MstInstance Id */
    pGarpQMsg = (tGarpQMsg *) (VOID *)
        GARP_GET_BUFF (GARP_QMSG_BUFF, sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         GARP_NAME,
                         "Message Allocation failed for Garp Config Message\r\n");
        return;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));

    pGarpQMsg->u2MsgType = GARP_MSG;
    pGarpQMsg->u4ContextId = u4ContextId;

    /*KNOWN ISSUE: 
       In a Provider Edge Bridge,
       When a CEP is configured as only member port of an S-VLAN and 
       there exists a Provider Edge Port  (CEP,  S-VLAN). 
       then GVRP propagation in Provider network happens and this 
       results in learning of SVLANs in the provider network 
       irrespective of the port state of C-VLAN component ports. 

       When CEP becomes an alternate port in the C-VLAN component, 
       the data path between the customer and provider breaks. 
       But the S-VLANs propagation will continue in the provider 
       network (as the internal CNPs are in forwarding state). 

       This results in unnecessary traffic load on the above S-VLAN 
       path connecting the above CEP and other customer points of 
       attachments. */

    if (u1State == AST_PORT_STATE_FORWARDING)
    {
        pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_STAP_PORT_FWD_MSG;
    }
    else
    {
        pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_STAP_PORT_BLK_MSG;
    }

    pGarpQMsg->u4IfIndex = u2IfIndex;
    pGarpQMsg->unGarpMsg.GarpMsg.u2GipId = u2GipId;

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                         GARP_NAME, "Send To Q failed for"
                         "Garp Config Message\r\n");

        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);

        return;
    }

    if (GARP_TASK_ID == GARP_INIT_VAL)
    {
        if (GARP_GET_TASK_ID (SELF, GARP_TASK, &(GARP_TASK_ID)) != OSIX_SUCCESS)
        {
            GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             GARP_NAME,
                             " Unable to get task id to send event\r\n");

            return;
        }

    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);
}

/***************************************************************/
/*  Function Name   : GarpMiUpdateInstVlanMap                  */
/*                                                             */
/*  Description     : This function updates GARP of Instance   */
/*                    to VLAN map Updation                     */
/*                                                             */
/*  Input(s)        : u1Action - Update / Post Message         */
/*                    u4ContextId - Context Identifier          */
/*                    u2NewMapId - New Map Identifier          */
/*                    u2OldMapId - Old Map Identifier          */
/*                                                             */
/*                    VlanId - VLAN Identifier of mapped VLAN  */
/*                    mapped to the instance                   */
/*                                                             */
/*                    u2MapEvent - Indicates whether it is a   */
/*                    single VLAN to MSTI or list of VLAN      */
/*                    map/umap                                 */
/*                                                             */
/*                    pu1Result - output to indicate message   */
/*                    post or updation                         */
/*                                                             */
/*  Output(s)       : Buffer to be posted to GARP              */
/*                                                             */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/

VOID
GarpMiUpdateInstVlanMap (UINT4 u4ContextId, UINT1 u1Action, UINT2 u2NewMapId,
                         UINT2 u2OldMapId, tVlanId VlanId,
                         UINT2 u2MapEvent, UINT1 *pu1Result)
{
    tGarpVidMapMsg     *pGarpVidMapMsg;
    tVlanMapInfo       *pVlanMapInfo;
    tGarpQMsg          *pGarpQMsg;
    INT4                i4EventIndex;
    UINT4               u4Size;
    UINT2               u2MsgType;
    UINT1               u1BufAlloc = GARP_FALSE;

    pGarpQMsg = gapGarpRemapQMsg[u4ContextId];

    /* Should Post the message if max array is reached and re-initialise 
     * buffer pointer to NULL on posting */

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {
        if (pGarpQMsg != NULL)
        {
            if ((GARP_MAP_INST_VLAN_LIST_MSG == u2MapEvent) ||
                (GARP_UNMAP_INST_VLAN_LIST_MSG == u2MapEvent))
            {

                GARP_RELEASE_BUFF (GARP_VLAN_MAP_MSG_BUFF, pGarpQMsg);
            }
            else
            {
                GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);
            }
        }
        gapGarpRemapQMsg[u4ContextId] = NULL;
        return;
    }

    if (GARP_POST_MAP_MSG != u1Action)
    {
        if (NULL == pGarpQMsg)
        {
            if ((GARP_MAP_INST_VLAN_LIST_MSG == u2MapEvent) ||
                (GARP_UNMAP_INST_VLAN_LIST_MSG == u2MapEvent))
            {
                u4Size = (sizeof (tGarpQMsg) +
                          (sizeof (tVlanMapInfo) *
                           (VLAN_MAP_INFO_PER_POST - 1)));

                pGarpQMsg = (tGarpQMsg *) (VOID *)
                    GARP_GET_BUFF (GARP_VLAN_MAP_MSG_BUFF, u4Size);

                u2MsgType = GARP_VLAN_LIST_MAP_MSG;
            }
            else
            {
                u4Size = sizeof (tGarpQMsg);

                pGarpQMsg =
                    (tGarpQMsg *) (VOID *) GARP_GET_BUFF (GARP_QMSG_BUFF,
                                                          u4Size);

                u2MsgType = GARP_VLAN_MAP_MSG;
            }

            if (NULL == pGarpQMsg)
            {
                GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                                 (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                                 " Message Allocation failed for VLAN Map "
                                 "Config Message\r\n");
                return;
            }

            u1BufAlloc = GARP_TRUE;

            MEMSET (pGarpQMsg, 0, u4Size);

            gapGarpRemapQMsg[u4ContextId] = pGarpQMsg;

            (pGarpQMsg)->i4Count++;
            (pGarpQMsg)->u2MsgType = u2MsgType;
            (pGarpQMsg)->u4ContextId = u4ContextId;
        }

        /* The below part of updating the message to be sent to GARP needs to 
         * be modified when MST-VLAN map is to be operated upon COMMIT. 
         * Take care of change the ProcessMsg thread also. 
         * Operate similar to GarpFillBulkMsg, post and process like BulkMsg */

        /* As of now GarpVidMapMsg is indexed with 0 as only one msg is present */
        pGarpVidMapMsg = &((pGarpQMsg)->unGarpMsg.GarpVidMapMsg[0]);

        i4EventIndex = pGarpVidMapMsg->i4EventCount;

        pVlanMapInfo = &(pGarpVidMapMsg->VlanMapInfo[i4EventIndex]);

        /* Fill the below only when the buffer is freshly allocated */
        if (GARP_TRUE == u1BufAlloc)
        {
            pGarpVidMapMsg->u2NewMapId = u2NewMapId;
            pGarpVidMapMsg->u2MsgEvent = u2MapEvent;
        }

        pVlanMapInfo->u2MapId = u2OldMapId;
        pVlanMapInfo->VlanId = VlanId;

        pGarpVidMapMsg->i4EventCount++;

        *pu1Result = GARP_MAP_MSG_UPDATED;
    }                            /* GARP_POST_MAP_MSG */
    else
    {
        /* When u1Action is post map message, pGarpQMsg cannot be NULL */
        pGarpVidMapMsg = &((pGarpQMsg)->unGarpMsg.GarpVidMapMsg[0]);
    }

    if (GARP_POST_MAP_MSG == u1Action || GARP_UPDNPOST_MAP_MSG == u1Action ||
        (((pGarpVidMapMsg)->i4EventCount) == VLAN_MAP_INFO_PER_POST))
    {
        GarpPostInstVlanMap (pGarpQMsg);
        gapGarpRemapQMsg[u4ContextId] = NULL;
        *pu1Result = GARP_MAP_MSG_POSTED;
    }

    return;
}

/************************************************************************/
/*  Function Name   : GarpMessageInd                                    */
/*  Description     : Garp process the received frame                   */
/*  Input(s)        : pFrame -the GMRP or GVRP pdu frame received       */
/*                    from a vlan aware external bridge or participant  */
/*                    u2Port - Src port in which the packet is received */
/*                    GipId -- It is 0 then a GVRP packet .             */
/*                    GipId is the vlanid if it is a GMRP packet        */
/*  Output(s)       : None                                              */
/*  Global Variables Referred : None                                    */
/*  Global variables Modified : None                                    */
/*  Exceptions or Operating System Error Handling : None                */
/*  Use of Recursion : None                                             */
/*  Returns         : GARP_SUCCESS on successfule message Indication    */
/*                    else GARP_FAILURE                                 */
/************************************************************************/
INT4
GarpMessageInd (tCRU_BUF_CHAIN_DESC * pFrame, UINT2 u2IfIndex, UINT2 u2GipId)
{
    tGarpIfMsg          GarpIfMsg;
    UINT4               u4RetValue;
    UINT2               u2Pid;
    UINT2               u2Len;
    UINT1               au1GarpHdr[GARP_HDR_SIZE];
    UINT1              *pu1Ptr = NULL;
    UINT1               au1Llc[GARP_LLC_SIZE] = { 0x42, 0x42, 0x3 };
    UINT2               u2LocalPortId;
    UINT4               u4ContextId;

    MEMSET (&GarpIfMsg, 0, sizeof (tGarpIfMsg));
    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return GARP_SUCCESS;
    }

    /*For SI case contextId should be default Context ID */
    if (GarpVcmGetContextInfoFromIfIndex (u2IfIndex, &u4ContextId,
                                          &u2LocalPortId) == VCM_FAILURE)
    {
        return GARP_FAILURE;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {

        GARP_RELEASE_BUFF (GARP_SYS_BUFF, pFrame);

        return GARP_FAILURE;
    }

    pu1Ptr = GARP_SYS_BUFF_IF_LINEAR (pFrame, 0, GARP_HDR_SIZE);

    if (pu1Ptr == NULL)
    {

        GARP_COPY_FROM_SYS_BUFF (pFrame, au1GarpHdr, 0, GARP_HDR_SIZE);

        pu1Ptr = au1GarpHdr;
    }

    GARP_ARRAY_TO_MAC_ADDR (GarpIfMsg.SrcMacAddr,
                            &pu1Ptr[GARP_SRC_ADDR_OFFSET]);

    MEMCPY (&u2Len, &pu1Ptr[GARP_TYPE_LEN_OFFSET], GARP_TYPE_LEN_SIZE);
    u2Len = (UINT2) (OSIX_NTOHS (u2Len));

    /* 
     * The Type/Length field of the Ethernet Header gives
     * the length of the Ethernet data following the Type/Length field.
     * The variable "u2Len" used here, must specify the length of the
     * entire Ethernet Frame. Hence "u2Len" is incremented by 
     * GARP_LLC_OFFSET (14).
     */
    u2Len = (UINT2) (u2Len + GARP_LLC_OFFSET);

    if (MEMCMP (&pu1Ptr[GARP_LLC_OFFSET], au1Llc, GARP_LLC_SIZE) != 0)
    {

        GARP_RELEASE_BUFF (GARP_SYS_BUFF, pFrame);

        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                         "Frame with Invalid LLC header received on Port %d.\n",
                         u2IfIndex);

        return GARP_FAILURE;
    }

    MEMCPY (&u2Pid, &pu1Ptr[GARP_PID_OFFSET], GARP_PID_SIZE);
    u2Pid = (UINT2) (OSIX_NTOHS (u2Pid));

    if (GARP_PID != u2Pid)
    {

        GARP_RELEASE_BUFF (GARP_SYS_BUFF, pFrame);

        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                         "Frame with Invalid PID received "
                         "on Port %d.\n", u2IfIndex);

        return GARP_FAILURE;
    }

    GarpIfMsg.u2GipId = u2GipId;
    GarpIfMsg.u2Port = u2LocalPortId;
    GarpIfMsg.u4ContextId = u4ContextId;
    GarpIfMsg.u2Len = u2Len;

    GARP_FILL_IF_MSG (pFrame, &GarpIfMsg);

    u4RetValue
        = GARP_SEND_TO_QUEUE (GARP_TASK_QUEUE_ID, (UINT1 *) &pFrame,
                              OSIX_DEF_MSG_LEN);

    if (u4RetValue == OSIX_SUCCESS)
    {
        if (GARP_TASK_ID == GARP_INIT_VAL)
        {
            if (GARP_GET_TASK_ID (SELF, GARP_TASK, &(GARP_TASK_ID)) !=
                OSIX_SUCCESS)
            {
                GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                                 (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                                 GARP_NAME,
                                 " Unable to get task id to send event\r\n");
                return GARP_FAILURE;
            }

        }

        GARP_SEND_EVENT (GARP_TASK_ID, GARP_MSG_ENQ_EVENT);

        return GARP_SUCCESS;
    }

    GARP_RELEASE_BUFF (GARP_SYS_BUFF, pFrame);

    GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC, ALL_FAILURE_TRC, GARP_NAME,
                     "Frame received on Port %d is NOT enqueued to "
                     "GARP message Queue.\n", u2IfIndex);

    return GARP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : GarpLock                                             */
/*                                                                           */
/* Description        : This function is used to take the GARP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS or GARP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
GarpLock (VOID)
{
    if (GARP_TAKE_SEM (gGarpSemId) != OSIX_SUCCESS)
    {
        GARP_GLOBAL_TRC (GARP_INVALID_CONTEXT_ID, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         "MSG: Failed to Take GARP Mutual Exclusion Semaphore4 \n");
        return GARP_FAILURE;
    }

    /* GarpSelectContext and GarpReleaseContext does not have stubs in case of
     * SI mode. Also context selection will not be done implicitly even in SI
     * mode. 
     * Therefore in SI mode also before calling SI nmh we have to do select 
     * context. 
     * Registering select and release context with SNMP is not possible as, 
     * Vlan and Garp shares same mibs. Putting select and release contexts in 
     * SI mib wrapper routines is painful. So it is better to do 
     * select context for default context in Lock function. Since we have 
     * opted the above way in vlan we have to follow the same here too. */

    if (GarpSelectContext (GARP_DEFAULT_CONTEXT_ID) == GARP_FAILURE)
    {
        return GARP_FAILURE;
    }
    return GARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : GarpUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the GARP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS or GARP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
GarpUnLock (VOID)
{
    GarpReleaseContext ();
    GARP_RELEASE_SEM (gGarpSemId);
    return GARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : GarpFillBulkMessage                                  */
/*                                                                           */
/* Description        : This function is used to fill the Vlan / Mcast /     */
/*                      Def Group Info in the Message. This funtion is called*/
/*                      whenever a Gvrp and Gmrp is enabled. This function   */
/*                      also post the message if the max no of message is    */
/*                      filled.                                              */
/*                                                                           */
/* Input(s)           : ppGarpQMsg - Double Pointer to store GarpQMsg        */
/*                      u1MsgType  - Type of Message - Vlan / Mcast / Def Grp*/
/*                      VlanId     - VlanId                                  */
/*                      Ports      - Added Portlist                          */
/*                      pu1MacAddr - MacAddres                               */
/*                                                                           */
/* Output(s)          : ppGarpQMsg - pointer to Allocated Message            */
/*                      pi4Count   - No of Message Filled                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS or GARP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
GarpFillBulkMessage (UINT4 u4ContextId, tGarpQMsg ** ppGarpQMsg,
                     UINT1 u1MsgType, tVlanId VlanId, tLocalPortList Ports,
                     UINT1 *pu1MacAddr)
{
    tGarpBulkMsg       *pGarpBulkMsg;
    INT4                i4MsgIndex;

    /*  This allocation is done only once for each message post to GARP. 
     *  As the function is called in loop the check here is essential */

    if (*ppGarpQMsg == NULL)
    {
        if ((*ppGarpQMsg =
             (tGarpQMsg *) (VOID *) GARP_GET_BUFF (GARP_BULK_QMSG_BUFF,
                                                   sizeof (tGarpQMsg))) == NULL)
        {
            return GARP_FAILURE;
        }

        MEMSET (*ppGarpQMsg, 0, sizeof (tGarpQMsg) +
                (sizeof (tGarpBulkMsg) * (VLAN_NO_OF_MSG_PER_POST - 1)));
    }

    i4MsgIndex = (*ppGarpQMsg)->i4Count;
    (*ppGarpQMsg)->u4ContextId = u4ContextId;

    pGarpBulkMsg = &((*ppGarpQMsg)->unGarpMsg.GarpBulkMsg[i4MsgIndex]);

    pGarpBulkMsg->VlanId = VlanId;

    pGarpBulkMsg->u2MsgType = u1MsgType;

    MEMCPY (pGarpBulkMsg->Ports, Ports, sizeof (tLocalPortList));

    if (pu1MacAddr != NULL)
    {
        MEMCPY (pGarpBulkMsg->MacAddr, pu1MacAddr, ETHERNET_ADDR_SIZE);
    }

    (*ppGarpQMsg)->i4Count++;

    if (((*ppGarpQMsg)->i4Count) == VLAN_NO_OF_MSG_PER_POST)
    {
        GarpPostBulkCfgMessage (*ppGarpQMsg);
        *ppGarpQMsg = NULL;
    }

    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPostBulkCfgMessage                           */
/*                                                                           */
/*    Description         : Posts the message to Garp Config Q.              */
/*                                                                           */
/*    Input(s)            : pGarpQMsg -  Pointer to GarpQMsg                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpPostBulkCfgMessage (tGarpQMsg * pGarpQMsg)
{
    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return GARP_SUCCESS;
    }

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (pGarpQMsg->u4ContextId) == GARP_FALSE)
    {
        GARP_RELEASE_BUFF (GARP_BULK_QMSG_BUFF, pGarpQMsg);
        return GARP_FAILURE;
    }

    pGarpQMsg->u2MsgType = GARP_BULK_MSG;

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_GLOBAL_TRC (pGarpQMsg->u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         " Send To Q failed for Bulk Vlan Config Message "
                         "\r\n");

        GARP_RELEASE_BUFF (GARP_BULK_QMSG_BUFF, pGarpQMsg);

        return GARP_FAILURE;
    }

    if (GARP_TASK_ID == GARP_INIT_VAL)
    {
        if (GARP_GET_TASK_ID (SELF, GARP_TASK, &(GARP_TASK_ID)) != OSIX_SUCCESS)
        {
            GARP_GLOBAL_TRC (pGarpQMsg->u4ContextId, GARP_MOD_TRC,
                             (OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                             GARP_NAME,
                             " Unable to get task id to send event\r\n");

            return GARP_FAILURE;
        }

    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);

    return GARP_SUCCESS;

}

/***************************************************************/
/*  Function Name   : GarpIsGarpEnabledInContext               */
/*  Description     : Returns the GARP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Global Variables Referred : gau1GarpShutDownStatus         */
/*  Global variables Modified :                                */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_ENABLED /GARP_DISABLED              */
/***************************************************************/
INT4
GarpIsGarpEnabledInContext (UINT4 u4ContextId)
{
    return ((gau1GarpShutDownStatus[u4ContextId] ==
             GARP_SNMP_FALSE) ? GARP_TRUE : GARP_FALSE);
}

/*****************************************************************************/
/* Function Name      : GarpStartModule                                      */
/*                                                                           */
/* Description        : This function starts the GARP module                 */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS/GARP_FAILURE.                           */
/*****************************************************************************/
INT4
GarpStartModule ()
{
    GARP_LOCK ();
    if (GarpHandleStartModule () == GARP_FAILURE)
    {
        GARP_UNLOCK ();
        return GARP_FAILURE;
    }
    GARP_UNLOCK ();
    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   :GarpAppReleaseMemory                      */
/*  Description     :This function releases memory allocated   */
/*                   for garp applications and in garp.        */
/*  Input(s)        :None                                      */
/*  Output(s)       :None                                      */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
VOID
GarpAppReleaseMemory (void)
{
    UINT4               u4ContextId;
    GARP_LOCK ();
    gu1GarpIsInitComplete = GARP_FALSE;
    for (u4ContextId = 0; u4ContextId < GARP_MAX_CONTEXTS; u4ContextId++)
    {
        if (GarpSelectContext (u4ContextId) == GARP_FAILURE)
        {
            continue;
        }
        GarpHandleDeleteContext (u4ContextId);
        GarpReleaseContext ();
    }
    GarpFlushContextsQueue ();
    GarpGlobalMemoryDeInit ();
    if (gGarpTmrListId != NULL)
    {
        TmrDeleteTimerList (gGarpTmrListId);
        gGarpTmrListId = NULL;
    }
#ifdef L2RED_WANTED
    GARP_NODE_STATUS () = GARP_NODE_IDLE;
    /* HITLESS RESTART */
    GARP_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
#endif
    GARP_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpCreateContext                                */
/*                                                                           */
/*    Description         : This function creates given context              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE.                      */
/*                                                                           */
/*****************************************************************************/
UINT4
GarpCreateContext (UINT4 u4ContextId)
{
    tGarpQMsg          *pGarpQMsg = NULL;

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return GARP_SUCCESS;
    }

    pGarpQMsg = (tGarpQMsg *) (VOID *)
        GARP_GET_BUFF (GARP_QMSG_BUFF, sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        return GARP_FAILURE;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));
    pGarpQMsg->u2MsgType = GARP_MSG;
    pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_CREATE_CONTEXT_MSG;
    pGarpQMsg->u4ContextId = u4ContextId;

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);
        return GARP_FAILURE;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);
    L2MI_SYNC_TAKE_SEM ();

    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpDeleteContext                                */
/*                                                                           */
/*    Description         : This function deletes given context              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
UINT4
GarpDeleteContext (UINT4 u4ContextId)
{
    tGarpQMsg          *pGarpQMsg = NULL;

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return GARP_SUCCESS;
    }

    pGarpQMsg = (tGarpQMsg *) (VOID *)
        GARP_GET_BUFF (GARP_QMSG_BUFF, sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        return GARP_FAILURE;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));
    pGarpQMsg->u2MsgType = GARP_MSG;
    pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_DELETE_CONTEXT_MSG;
    pGarpQMsg->u4ContextId = u4ContextId;

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);
        return GARP_FAILURE;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);
    L2MI_SYNC_TAKE_SEM ();

    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpUpdateContextName                            */
/*                                                                           */
/*    Description         : This function updates the name of the context    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE.                      */
/*                                                                           */
/*****************************************************************************/
UINT4
GarpUpdateContextName (UINT4 u4ContextId)
{
    tGarpQMsg          *pGarpQMsg = NULL;

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return GARP_SUCCESS;
    }

    pGarpQMsg = (tGarpQMsg *) (VOID *)
        GARP_GET_BUFF (GARP_QMSG_BUFF, sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        return GARP_FAILURE;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));
    pGarpQMsg->u2MsgType = GARP_MSG;
    pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_UPDATE_CONTEXT_NAME;
    pGarpQMsg->u4ContextId = u4ContextId;

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);
        return GARP_FAILURE;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);

    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpMapPort                              */
/*  Description     : This function is for mapping a port to a */
/*                    context.                                 */
/*  Input(s)        : u2Port - Port to be mapped.              */
/*  Output(s)       :None                                      */
/*  Global Variables Referred :None                            */
/*  Global variables Modified :None                            */
/*  Exceptions or Operating System Error Handling :None        */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS or GARP_FAILURE             */
/***************************************************************/
INT4
GarpMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    INT4                i4RetVal;

    i4RetVal = GarpPostCfgMessage (GARP_PORT_MAP_MSG, u2LocalPortId,
                                   u4ContextId, u4IfIndex);

    if (GARP_IS_GARP_ENABLED_IN_CONTEXT (u4ContextId) == GARP_FALSE)
    {

        GarpL2IwfSetProtocolEnabledStatusOnPort (u4ContextId,
                                                 u2LocalPortId,
                                                 L2_PROTO_GVRP, OSIX_DISABLED);
        GarpL2IwfSetProtocolEnabledStatusOnPort (u4ContextId,
                                                 u2LocalPortId,
                                                 L2_PROTO_GMRP, OSIX_DISABLED);

    }
    if (i4RetVal == GARP_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/***************************************************************/
/*  Function Name   : GarpUnmapPort                           */
/*  Description     : This function is for unmapping a port   */
/*                    from a context                          */
/*  Input(s)        : u2Port - Port to be deleted             */
/*  Output(s)       : None                                    */
/*  Global Variables Referred : None                          */
/*  Global variables Modified : None                          */
/*  Exceptions or Operating System Error Handling : None      */
/*  Use of Recursion : None                                   */
/*  Returns         : GARP_SUCCESS or GARP_FAILURE            */
/***************************************************************/
INT4
GarpUnmapPort (UINT2 u2IfIndex)
{
    INT4                i4RetVal;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    /*Assumption is VCM still has the context Info when delete 
     * port indication comes to GARP*/
    if (GarpVcmGetContextInfoFromIfIndex (u2IfIndex, &u4ContextId,
                                          &u2LocalPortId) == VCM_FAILURE)
    {
        return GARP_FAILURE;
    }
    i4RetVal = GarpPostCfgMessage (GARP_PORT_UNMAP_MSG,
                                   u2LocalPortId, u4ContextId, u2IfIndex);
    if (i4RetVal == GARP_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/***************************************************************/
/*  Function Name   : GvrpIsGvrpEnabledInContext               */
/*  Description     : Returns the GVRP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Global Variables Referred : gpGarpContextInfo->u1GvrpStatus                  */
/*  Global variables Modified :                                */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GVRP_ENABLED /GVRP_DISABLED              */
/***************************************************************/

INT4
GvrpIsGvrpEnabledInContext (UINT4 u4ContextId)
{
    return ((gau1GvrpStatus[u4ContextId] ==
             GVRP_ENABLED) ? GVRP_TRUE : GVRP_FALSE);
}

/****************************************************************************/
/*  Function Name   :GvrpPropagateVlanInfo                                  */
/*  Description     :depending on u1action we will add a static             */
/*                   entry or delete it                                     */
/*  Input(s)        :u1AttrType -The attribute type received                */
/*                   AddPortList - Ports to be added                        */
/*                   DelPortList - ports to be deleted                      */
/*                   u1Action - depending on u1Action we will add a static  */
/*                              vlan  entry or delete it                    */
/*  Output(s)       :  None                                                 */
/*  Global Variables Referred : None                                        */
/*  Global variables Modified : None                                        */
/*  Returns         : None                                                  */
/****************************************************************************/
void
GvrpPropagateVlanInfo (UINT4 u4ContextId, tVlanId VlanId,
                       tLocalPortList AddPortList, tLocalPortList DelPortList)
{
    tGarpQMsg          *pGarpQMsg = NULL;

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return;
    }

    if (GVRP_IS_GVRP_ENABLED_IN_CONTEXT (u4ContextId) == GVRP_FALSE)
    {

        GARP_GLOBAL_TRC (u4ContextId, GVRP_MOD_TRC,
                         (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), GVRP_NAME,
                         "GVRP is NOT enabled. Cannot Propagate "
                         "the Vlan Id %d.\n", VlanId);

        return;
    }

    GARP_GLOBAL_TRC (u4ContextId, GVRP_MOD_TRC, CONTROL_PLANE_TRC, GVRP_NAME,
                     "Received Propagation request for Vlan Id %d.\n", VlanId);

    pGarpQMsg = (tGarpQMsg *) (VOID *)
        GARP_GET_BUFF (GARP_QMSG_BUFF, sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         "Message Allocation failed for Garp Config Message\r\n");
        return;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));

    pGarpQMsg->u2MsgType = GARP_MSG;

    pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_PROP_VLAN_INFO_MSG;
    pGarpQMsg->unGarpMsg.GarpMsg.u2GipId = VlanId;
    pGarpQMsg->u4ContextId = u4ContextId;
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         "Send To Q failed for Garp Config Message\r\n");

        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);

        return;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);

    return;
}

/****************************************************************************/
/*  Function Name   :GvrpSetVlanForbiddenPorts                              */
/*  Description     :This function sets forbidden ports for a specific      */
/*                    VlanId                                                */
/*  Input(s)        :VlanId - VlanId for which Forbidden ports to be updated*/
/*                   AddPortList - Ports to be added                        */
/*                   DelPortList - ports to be deleted                      */
/*  Output(s)       :  None                                                 */
/*  Global Variables Referred : None                                        */
/*  Global variables Modified : None                                        */
/*  Returns         : None                                                  */
/****************************************************************************/
void
GvrpSetVlanForbiddenPorts (UINT4 u4ContextId, tVlanId VlanId,
                           tLocalPortList AddPortList,
                           tLocalPortList DelPortList)
{
    tGarpQMsg          *pGarpQMsg = NULL;

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return;
    }

    if (GVRP_IS_GVRP_ENABLED_IN_CONTEXT (u4ContextId) == GVRP_FALSE)
    {

        GARP_GLOBAL_TRC (u4ContextId, GVRP_MOD_TRC,
                         (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), GVRP_NAME,
                         "GVRP is NOT enabled. Cannot Set Forbidden "
                         "Ports for the Vlan Id %d.\n", VlanId);

        return;
    }

    GARP_GLOBAL_TRC (u4ContextId, GVRP_MOD_TRC, CONTROL_PLANE_TRC, GVRP_NAME,
                     "Received Forbidden Ports Set Request for Vlan Id %d.\n",
                     VlanId);
    pGarpQMsg =
        (tGarpQMsg *) (VOID *) GARP_GET_BUFF (GARP_QMSG_BUFF,
                                              sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         "Message Allocation failed for Garp Config Message\r\n");
        return;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));

    pGarpQMsg->u2MsgType = GARP_MSG;

    pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_SET_VLAN_FORBID_MSG;
    pGarpQMsg->unGarpMsg.GarpMsg.u2GipId = VlanId;
    pGarpQMsg->u4ContextId = u4ContextId;
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         "Send To Q failed for Garp Config Message\r\n");

        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);

        return;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);

    return;
}

/***************************************************************/
/*  Function Name   : GmrpIsGmrpEnabledInContext               */
/*  Description     : Returns the GMRP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Global Variables Referred : gpGarpContextInfo->u1GmrpStatus                  */
/*  Global variables Modified :                                */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GMRP_ENABLED /GMRP_DISABLED              */
/***************************************************************/
INT4
GmrpIsGmrpEnabledInContext (UINT4 u4ContextId)
{
    return ((gau1GmrpStatus[u4ContextId] ==
             GMRP_ENABLED) ? GMRP_TRUE : GMRP_FALSE);
}

/****************************************************************/
/*  Function Name   :GmrpPropagateMcastInfo                     */
/*  Description     :depending on u1action we will add a static */
/*                   Multicast entry or delete an entry         */
/*  Input(s)        :pMacAddr - Pointer to mac address          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpPropagateMcastInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                        tVlanId VlanId,
                        tLocalPortList AddPortList, tLocalPortList DelPortList)
{

    tGarpQMsg          *pGarpQMsg = NULL;
    UINT1               au1Str[22];

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return;
    }

    MEMSET (au1Str, 0, 22);
    CliMacToStr (MacAddr, au1Str);

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (u4ContextId) == GMRP_FALSE)
    {

        GARP_GLOBAL_TRC (u4ContextId, GMRP_MOD_TRC,
                         (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                         GMRP_NAME, "GMRP is NOT enabled. Cannot Propagate "
                         "for the Vlan Id %d, the Group Address %s", VlanId,
                         au1Str);
        return;
    }

    GARP_GLOBAL_TRC (u4ContextId, GMRP_MOD_TRC,
                     (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), GMRP_NAME,
                     "Received Forbidden Ports Set Request "
                     "for the Vlan Id %d and the Group Address %s", VlanId,
                     au1Str);

    pGarpQMsg = (tGarpQMsg *) (VOID *)
        GARP_GET_BUFF (GARP_QMSG_BUFF, sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         " Message Allocation failed for Garp"
                         "Config Message");
        return;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));

    pGarpQMsg->u2MsgType = GARP_MSG;

    pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_PROP_MCAST_INFO_MSG;
    pGarpQMsg->unGarpMsg.GarpMsg.u2GipId = VlanId;
    pGarpQMsg->u4ContextId = u4ContextId;
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.MacAddr, MacAddr, ETHERNET_ADDR_SIZE);
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         " Send To Q failed for  Garp Config Message\r\n");

        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);

        return;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);

    return;

}

/****************************************************************/
/*  Function Name   :GmrpSetMcastForbiddenPorts                 */
/*  Description     :This function set the forbidden ports for a*/
/*                   given multicast entry                      */
/*  Input(s)        :pMacAddr - Pointer to multicast mac address*/
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpSetMcastForbiddenPorts (UINT4 u4ContextId, tMacAddr MacAddr,
                            tVlanId VlanId,
                            tLocalPortList AddPortList,
                            tLocalPortList DelPortList)
{
    tGarpQMsg          *pGarpQMsg = NULL;
    UINT1               au1Str[22];

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return;
    }

    MEMSET (au1Str, 0, 22);
    CliMacToStr (MacAddr, au1Str);

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (u4ContextId) == GMRP_FALSE)
    {

        GARP_GLOBAL_TRC (u4ContextId, GMRP_MOD_TRC,
                         (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                         GMRP_NAME, "GMRP is NOT enabled. Cannot Set Forbidden "
                         "Ports for the Vlan Id %d and the Group Address %s",
                         VlanId, au1Str);

        return;
    }

    GARP_GLOBAL_TRC (u4ContextId, GMRP_MOD_TRC,
                     (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), GMRP_NAME,
                     "Received Forbidden Ports Set Request "
                     "for the Vlan Id %d and the Group Address %s", VlanId,
                     au1Str);

    pGarpQMsg = (tGarpQMsg *) (VOID *)
        GARP_GET_BUFF (GARP_QMSG_BUFF, sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         " Message Allocation failed for Garp Config Message");
        return;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));

    pGarpQMsg->u2MsgType = GARP_MSG;

    pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_SET_MCAST_FORBID_MSG;
    pGarpQMsg->unGarpMsg.GarpMsg.u2GipId = VlanId;
    pGarpQMsg->u4ContextId = u4ContextId;
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.MacAddr, MacAddr, ETHERNET_ADDR_SIZE);
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         " Send To Q failed for  Garp Config Message\r\n");

        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);

        return;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);

    return;

}

/****************************************************************/
/*  Function Name   :GmrpPropagateDefGroupInfo                  */
/*  Description     :This function propagates the default Group */
/*                   info to all the ports during initialisation*/
/*  Input(s)        :u1Type - Whether it is VLAN_ALL_GROUPS or  */
/*                   VLAN_UNREG_GROUPS                          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpPropagateDefGroupInfo (UINT4 u4ContextId, UINT1 u1Type,
                           tVlanId VlanId,
                           tLocalPortList AddPortList,
                           tLocalPortList DelPortList)
{
#ifdef GARP_EXTENDED_FILTER
    tGarpQMsg          *pGarpQMsg = NULL;
#endif

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return;
    }
#ifdef GARP_EXTENDED_FILTER
    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (u4ContextId) == GMRP_FALSE)
    {

        GARP_GLOBAL_TRC (u4ContextId, GMRP_MOD_TRC,
                         (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), GMRP_NAME,
                         "GMRP is NOT enabled. Cannot Propagate "
                         "Default Groups (Value = %d) behavior for the "
                         "Vlan Id %d\n", u1Type, VlanId);
        return;
    }

    pGarpQMsg = (tGarpQMsg *) (VOID *)
        GARP_GET_BUFF (GARP_QMSG_BUFF, sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         " Message Allocation failed for"
                         "Garp Config Message");
        return;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));

    pGarpQMsg->u2MsgType = GARP_MSG;

    if (u1Type == VLAN_ALL_GROUPS)
    {
        pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_PROP_FWDALL_INFO_MSG;
    }
    else
    {
        pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_PROP_FWDUNREG_INFO_MSG;
    }

    pGarpQMsg->unGarpMsg.GarpMsg.u2GipId = VlanId;
    pGarpQMsg->u4ContextId = u4ContextId;

    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         " Send To Q failed for  Garp Config Message\r\n");

        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);

        return;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);

    return;

#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
#endif /* EXTENDED_FILTERING_CHANGE */
}

/****************************************************************/
/*  Function Name   :GmrpSetDefGroupForbiddenPorts              */
/*  Description     :depending on u1action we will add a static */
/*                   entry or delete it                         */
/*  Input(s)        :pMacAddr - Pointer to Mac address          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpSetDefGroupForbiddenPorts (UINT4 u4ContextId, UINT1 u1Type,
                               tVlanId VlanId,
                               tLocalPortList AddPortList,
                               tLocalPortList DelPortList)
{
#ifdef GARP_EXTENDED_FILTER
    tGarpQMsg          *pGarpQMsg = NULL;
#endif

    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return;
    }
#ifdef GARP_EXTENDED_FILTER
    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (u4ContextId) == GMRP_FALSE)
    {

        GARP_GLOBAL_TRC (u4ContextId, GMRP_MOD_TRC,
                         (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), GMRP_NAME,
                         "GMRP is NOT enabled. Cannot Set Default "
                         "Groups (Value = %d) Forbidden Ports for the "
                         "Vlan Id %d\n", u1Type, VlanId);
        return;
    }

    pGarpQMsg = (tGarpQMsg *) (VOID *)
        GARP_GET_BUFF (GARP_QMSG_BUFF, sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         " Message Allocation failed for Garp Config Message");
        return;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));

    pGarpQMsg->u2MsgType = GARP_MSG;

    if (u1Type == VLAN_ALL_GROUPS)
    {
        pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_SET_FWDALL_FORBID_MSG;
    }
    else
    {
        pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_SET_FWDUNREG_FORBID_MSG;
    }

    pGarpQMsg->unGarpMsg.GarpMsg.u2GipId = VlanId;
    pGarpQMsg->u4ContextId = u4ContextId;

    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));
    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         " Send To Q failed for  Garp Config Message\r\n");

        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);

        return;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);

    return;

#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
#endif /* EXTENDED_FILTERING_CHANGE */
}

/**************************************************************************/
/*  Function Name    : GarpGipUpdateGipPorts ()                           */
/*  Description      : This function transmits Join messages on the given */
/*                     port, for all the Attributes registered for the    */
/*                     given GipId.                                       */
/*                     This function is called by VLAN whenever static    */
/*                     member ports are added.                            */
/*                     Updation of Gip Port list affects only GMRP.       */
/*  Input(s)         : AddPortList - Ports that is added to the           */
/*                              given GIP context.                        */
/*                     u2Gip  - Id of the GIP context for which           */
/*                              new port is added.                        */
/*  Output(s)        : None                                               */
/*  Global Variables                                                      */
/*  Referred         : None                                               */
/*  Global variables                                                      */
/*  Modified         : None                                               */
/*  Exceptions or OS                                                      */
/*  Error handling   : None                                               */
/*  Use of Recursion : None                                               */
/*  Returns          : None                                               */
/**************************************************************************/
VOID
GarpGipUpdateGipPorts (UINT4 u4ContextId, tLocalPortList AddPortList,
                       tLocalPortList DelPortList, UINT2 u2GipId)
{
    tGarpQMsg          *pGarpQMsg = NULL;
    if (gu1GarpIsInitComplete == GARP_FALSE)
    {
        /*GARP task is not initialized */
        return;
    }

    if (GMRP_IS_GMRP_ENABLED_IN_CONTEXT (u4ContextId) == GMRP_FALSE)
    {

        GARP_GLOBAL_TRC (u4ContextId, GMRP_MOD_TRC,
                         (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), GMRP_NAME,
                         "GMRP is NOT enabled. Cannot Update Gip"
                         "Ports for GipId %d \r\n", u2GipId);
        return;
    }

    pGarpQMsg = (tGarpQMsg *) (VOID *)
        GARP_GET_BUFF (GARP_QMSG_BUFF, sizeof (tGarpQMsg));

    if (NULL == pGarpQMsg)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         " Message Allocation failed for Garp Config"
                         "Message \r\n");
        return;
    }

    MEMSET (pGarpQMsg, 0, sizeof (tGarpQMsg));

    pGarpQMsg->u2MsgType = GARP_MSG;

    pGarpQMsg->unGarpMsg.GarpMsg.u2MsgType = GARP_GIP_UPDATE_GIP_PORTS_MSG;
    pGarpQMsg->unGarpMsg.GarpMsg.u2GipId = u2GipId;
    pGarpQMsg->u4ContextId = u4ContextId;

    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));

    MEMCPY (pGarpQMsg->unGarpMsg.GarpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    if (GARP_SEND_TO_QUEUE (GARP_CFG_QUEUE_ID,
                            (UINT1 *) &pGarpQMsg,
                            OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        GARP_GLOBAL_TRC (u4ContextId, GARP_MOD_TRC,
                         (OS_RESOURCE_TRC | ALL_FAILURE_TRC), GARP_NAME,
                         " Send To Q failed for  Garp " "Config Message\r\n");

        GARP_RELEASE_BUFF (GARP_QMSG_BUFF, pGarpQMsg);

        return;
    }

    GARP_SEND_EVENT (GARP_TASK_ID, GARP_CFG_MSG_EVENT);

    return;

}
#endif
