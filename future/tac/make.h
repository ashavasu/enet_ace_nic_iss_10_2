#
# Copyright (C) 2008 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 30 April 2008                                 |
# |                                                                          |
# |   DESCRIPTION            : This file contains all the warning options    |
# |                            that are used for building this module        |
# +--------------------------------------------------------------------------+

#include the LR make.h and make.rule
include ../LR/make.h
include ../LR/make.rule

# TAC base directories
TACM_NAME        = FutureTAC
TACM_BASE_DIR    = ${BASE_DIR}/tac
TACM_SRC_DIR     = $(TACM_BASE_DIR)/src
TACM_INC_DIR     = $(TACM_BASE_DIR)/inc
TACM_OBJ_DIR     = $(TACM_BASE_DIR)/obj


# TAC include directories
TACM_INCLUDES    =  -I$(TACM_INC_DIR) \
                      ${COMMON_INCLUDE_DIRS}


# TAC compilation switches
TACM_COMPILATION_SWITCHES = ${GENERAL_COMPILATION_SWITCHES} \
                            ${SYSTEM_COMPILATION_SWITCHES}

# TAC compilation flags
TACM_C_FLAGS     =  ${CC_FLAGS} ${TACM_COMPILATION_SWITCHES} \
                    ${TACM_INCLUDES}

# TAC Dependencies
TACM_DEPENDENCIES  = $(TACM_BASE_DIR)/make.h \
                     $(TACM_BASE_DIR)/Makefile \
                     ${COMMON_DEPENDENCIES}
