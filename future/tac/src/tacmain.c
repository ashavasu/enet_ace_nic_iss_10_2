/*****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2007                                               */
/* $Id: tacmain.c,v 1.6 2010/05/24 10:37:02 prabuc Exp $               */
/*****************************************************************************/
/* FILE  NAME            : TACmain.c                                         */
/* PRINCIPAL AUTHOR      : Aricent Inc.                                      */
/* SUBSYSTEM NAME        : Transmission and Admission Control (TAC)          */
/* MODULE NAME           : TAC Main Module                                   */
/* LANGUAGE              : C                                                 */
/* TARGET ENVIRONMENT    : Any                                               */
/* DATE OF FIRST RELEASE :                                                   */
/* AUTHOR                : Aricent Inc.                                      */
/* DESCRIPTION           : This file contains init, deinit funtions          */
/*                         for TAC module                                    */
/*---------------------------------------------------------------------------*/

#include "tacinc.h"
#include "tacglob.h"

/*****************************************************************************/
/* Function Name      : TACMain                                              */
/*                                                                           */
/* Description        : This function is the main entry point function for   */
/*                      the TAC module. Since TAC module is just a database  */
/*                      no task is spawned for the same.                     */
/*                                                                           */
/* Input(s)           : pi1Param - unused                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
TACMain (INT1 *pi1Param)
{
    UNUSED_PARAM (pi1Param);

    if (TacMainInit () == TACM_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    lrInitComplete (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : TacMainInit                                          */
/*                                                                           */
/* Description        : This initializes all the memory-pools and semaphores */
/*                      needed for this module                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacMainInit (VOID)
{
    /* Clear the global structure */
    MEMSET (&gTacGlobalConfig, 0, sizeof (tTacGlobalConfig));
    MEMSET (&gTacGlobalInfo, 0, sizeof (tTacGlobalInfo));

    MEMSET (&gFsTacSizingInfo, 0, sizeof (tFsModSizingInfo));
    MEMCPY (gFsTacSizingInfo.ModName, "TAC", STRLEN ("TAC"));
    gFsTacSizingInfo.u4ModMemPreAllocated = 0;
    gFsTacSizingInfo.ModSizingParams = gFsTacSizingParams;
    gTacGlobalConfig.i4TacStatus = TAC_ENABLED;

    /* Create the semaphore for the module */
    if (TACM_CREATE_SEM (TACM_SEM_NAME, &(TACM_SEM_ID)) != OSIX_SUCCESS)
    {
        TACM_TRC (OS_RESOURCE_TRC | INIT_SHUT_TRC,
                  "TacMainInit : Semaphore creation failed\r\n");
        return TACM_FAILURE;
    }

    TACM_UNLOCK ();

    /* Create the Memory Pool for Profile table */
    if (TACM_CREATE_MEM_POOL
        (TACM_PROFILE_MEMBLK_SIZE,
         gFsTacSizingParams[TACM_PROFILE_SIZING_ID].u4PreAllocatedUnits,
         MEM_DEFAULT_MEMORY_TYPE, &(TACM_PROFILE_MEMPOOL_ID)) != MEM_SUCCESS)
    {
        TACM_TRC (OS_RESOURCE_TRC | INIT_SHUT_TRC,
                  "TacMainInit : Profile Allocation failed\r\n");
        TacMainDeInit ();
        return TACM_FAILURE;
    }

    /* Create Memory pool for filter entries of each profile entry */
    if (TACM_CREATE_MEM_POOL
        (TACM_PRF_FILTER_MEMBLK_SIZE,
         gFsTacSizingParams[TACM_PRF_FLTR_SIZING_ID].u4PreAllocatedUnits,
         MEM_DEFAULT_MEMORY_TYPE, &(TACM_PRF_FILTER_MEMPOOL_ID)) != MEM_SUCCESS)
    {
        TACM_TRC (OS_RESOURCE_TRC | INIT_SHUT_TRC,
                  "TacMainInit : Filter Allocation failed\r\n");
        TacMainDeInit ();
        return TACM_FAILURE;
    }

    /* Create the Memory Pool for Hash Table Entries for Filters */
    if (TACM_CREATE_MEM_POOL
        (TACM_PRF_FILTER_HASH_MEMBLK_SIZE,
         gFsTacSizingParams[TACM_PRF_FLTR_HASH_SIZING_ID].u4PreAllocatedUnits,
         MEM_DEFAULT_MEMORY_TYPE,
         &(TACM_PRF_FILTER_HASH_MEMPOOL_ID)) != MEM_SUCCESS)
    {
        TACM_TRC (OS_RESOURCE_TRC | INIT_SHUT_TRC,
                  "TacMainInit : Filter Hash List allocation failed\r\n");
        TacMainDeInit ();
        return TACM_FAILURE;
    }

    /* Create the profile embedded RB Tree */
    gTacGlobalConfig.pProfileRBRoot = RBTreeCreateEmbedded
        ((TACM_OFFSET (tTacProfileEntry, RbNode)), TacUtilComparePrfInfo);

    if (gTacGlobalConfig.pProfileRBRoot == NULL)
    {
        TACM_TRC (OS_RESOURCE_TRC | INIT_SHUT_TRC,
                  "TacMainInit : Profile RB Tree creation failed\r\n");
        TacMainDeInit ();
        return TACM_FAILURE;
    }

    /* Register the MIBs with SNMP */
    RegisterFSTAC ();

    TACM_TRC (INIT_SHUT_TRC, "TacMainInit : Tac inititialisation "
              "successful\r\n");

    FsUtlSzCalculateModulePreAllocatedMemory (&gFsTacSizingInfo);
    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacMainDeInit                                        */
/*                                                                           */
/* Description        : This frees all the memory-pools and deletes the      */
/*                      semaphores for this module                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacMainDeInit (VOID)
{
    /* Delete the semaphore */
    if (TACM_SEM_ID != NULL)
    {
        TACM_DELETE_SEM (TACM_SEM_ID);
    }

    /* Delete the profile memory pool */
    TACM_DELETE_MEM_POOL (TACM_PROFILE_MEMPOOL_ID);

    /* Delete the memory pool for filter entries */
    TACM_DELETE_MEM_POOL (TACM_PRF_FILTER_MEMPOOL_ID);

    /* Delete the memory pool for Hash lists used by filter entries */
    TACM_DELETE_MEM_POOL (TACM_PRF_FILTER_HASH_MEMPOOL_ID);

    return TACM_SUCCESS;
}
