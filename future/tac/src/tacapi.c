/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* License Aricent Inc., 2008                                                */
/* $Id: tacapi.c,v 1.11 2011/07/04 09:24:34 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : tacapi.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC API Module                                 */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains APIs provided by the        */
/*                          : TAC Module                                     */
/*---------------------------------------------------------------------------*/

#include "tacinc.h"

#include "npapi.h"
/*****************************************************************************/
/* Function Name      : TacApiFilterChannel                                  */
/*                                                                           */
/* Description        : This function scans the filter database and checks   */
/*                      whether the channeh (S,G) needs to be filterd or not */
/*                      If the profile action is permit, then the matching   */
/*                      entry should be found in the database to be allowed  */
/*                      If the profile action is deny, then the matching     */
/*                      entry should not be found in the database to be      */
/*                      allowed                                              */
/*                                                                           */
/* Input(s)           : pTacFilterPkt    - pointer to tTacFilterPkt          */
/*                      pu1SrcList       - Pointer to list of sources        */
/*                                                                           */
/* Output(s)          : pu1SrcList       - Pointer to the list of sources    */
/*                                                                           */
/* Return Value(s)    : TACM_PERMIT/TACM_DENY                                */
/*****************************************************************************/
INT4
TacApiFilterChannel (tTacFilterPkt * pTacFilterPkt, UINT1 *pu1SrcList)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4HashIndex = 0;
    INT4                i4RetVal = TACM_PERMIT;
    UINT2               u2Count = 0;
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1AddrLen = 0;
    UINT1               u1Mode = 0;
    INT1                i1MatchFound = TACM_FALSE;
    INT1                i1Flag = TACM_FALSE;

    TACM_LOCK ();

    TACM_TRC (TACM_FN_ENTRY, "TacApiFilterChannel Entered\r\n");

    if (gTacGlobalConfig.i4TacStatus != TAC_ENABLED)
    {
        /* If TAC module is not enabled globally then dont 
         * apply the filter profiles */
        TACM_TRC (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                  "TacApiFilterChannel : "
                  "TAC module is not enabled, so all groups "
                  "are permitted\r\n");
        TACM_TRC (TACM_FN_EXIT, "TacApiFilterChannel Exited\r\n");
        TACM_UNLOCK ();
        return TACM_PERMIT;
    }

    if (pTacFilterPkt->GroupAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
        /* This function acts as a stub for IPv6 multicast filter */
        TACM_TRC (TACM_FN_EXIT, "TacApiFilterChannel Exited\r\n");
        TACM_UNLOCK ();
        return TACM_PERMIT;
    }

    /*  This API currently acts as a stub function for query messages.
     *  Leave messages need not be filtered since it will not add any
     *  new entry. Only the reports are processed
     */
    if (pTacFilterPkt->u1PktType != TACM_IGMP_REPORT)
    {
        TACM_TRC (TACM_FN_EXIT, "TacApiFilterChannel Exited\r\n");
        TACM_UNLOCK ();
        return TACM_PERMIT;
    }

    /*  If the allow or block packet is arrived and the number of sources
     *  is 0, allow the packet. This will not affect the database. If
     *  include report is received with empty source list, allow the
     *  packet as it is a leave message
     */
    if ((pTacFilterPkt->u4NumSrc == 0) &&
        ((pTacFilterPkt->u1Mode == TACM_IGMP_BLOCK) ||
         (pTacFilterPkt->u1Mode == TACM_IGMP_ALLOW) ||
         (pTacFilterPkt->u1Mode == TACM_IGMP_INCLUDE)))
    {
        if (pTacFilterPkt->u1Mode == TACM_IGMP_INCLUDE)
        {
            TACM_TRC_ARG1 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                           "TacApiFilterChannel : "
                           "Profile %u : Include report with "
                           "empty source list is allowed\r\n",
                           pTacFilterPkt->u4ProfileId);
        }
        TACM_TRC (TACM_FN_EXIT, "TacApiFilterChannel Exited\r\n");
        TACM_UNLOCK ();
        return TACM_PERMIT;
    }

    /*  If the given multicast address belongs to reserved address range,
     *  return permit
     */
    MEMCPY (&u4GrpAddr, pTacFilterPkt->GroupAddr.au1Addr, u1AddrLen);

    if (TACM_IS_RSVD_IPV4_MCAST_ADDRESS (u4GrpAddr))
    {
        TACM_TRC_ARG1 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                       "TacApiFilterChannel : "
                       "Profile %u : Report with "
                       "reserved multicast address is allowed\r\n",
                       pTacFilterPkt->u4ProfileId);
        TACM_TRC (TACM_FN_EXIT, "TacApiFilterChannel Exited\r\n");
        TACM_UNLOCK ();
        return TACM_PERMIT;
    }

    /* Set the Mode according to the packet */
    if ((pTacFilterPkt->u1Version == TACM_IGMP_VERSION1) ||
        (pTacFilterPkt->u1Version == TACM_IGMP_VERSION2) ||
        (pTacFilterPkt->u1Mode == TACM_IGMP_EXCLUDE) ||
        (pTacFilterPkt->u1Mode == TACM_IGMP_BLOCK))
    {
        u1Mode = TACM_EXCLUDE;
    }
    else
    {
        u1Mode = TACM_INCLUDE;
    }

    /* Get the profile Id using the Profile Id and address type */
    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = pTacFilterPkt->u4ProfileId;
    TacProfileEntry.u1AddressType = pTacFilterPkt->GroupAddr.u1Afi;

    if (((pTacProfileEntry = (tTacProfileEntry *)
          RBTreeGet (TACM_PROFILE_TREE,
                     (tRBElem *) & TacProfileEntry)) == NULL) ||
        (pTacProfileEntry->u1RowStatus != TACM_ACTIVE))
    {
        /* The corresponding profile is either not present or not active */
        TACM_TRC_ARG1 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                       "TacApiFilterChannel : Profile %u is "
                       "not available or not active\r\n",
                       pTacFilterPkt->u4ProfileId);
        TACM_TRC (TACM_FN_EXIT, "TacApiFilterChannel Exited\r\n");
        TACM_UNLOCK ();
        return TACM_PERMIT;
    }

    /* Get the Hash bucket index */
    u4HashIndex = TacUtilGetHashIndex (pTacProfileEntry->u1NoOfBuckets,
                                       pTacProfileEntry->u4MinGrpAddr,
                                       pTacProfileEntry->u4MaxGrpAddr,
                                       u4GrpAddr);

    if (pTacFilterPkt->u4NumSrc == 0)
    {
        /*  If the packet to be filtered does not contain specific source list
         *  identified with the number of sources in tTacFilterPkt structure,
         *  Scan the hash bucket for the generated index and the default hash
         *  bucket. Check for given group address and wild card source
         */

        TACM_TRC_ARG2 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                       "TacApiFilterChannel : Profile %u : "
                       "Searching for Group %x\r\n",
                       pTacFilterPkt->u4ProfileId, u4GrpAddr);

        MEMSET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
        i1MatchFound = TacUtilSearchHashBucket (pTacProfileEntry,
                                                pTacFilterPkt->GroupAddr,
                                                au1SrcAddr, u4HashIndex,
                                                u1Mode);

        if ((i1MatchFound == TACM_FALSE) && (u4HashIndex != 0))
        {
            /* Since a macth is not found, search the default hash bucket */
            i1MatchFound = TacUtilSearchHashBucket (pTacProfileEntry,
                                                    pTacFilterPkt->GroupAddr,
                                                    au1SrcAddr, 0, u1Mode);
        }

        if (i1MatchFound == TACM_TRUE)
        {
            if (pTacProfileEntry->u1ProfileAction == TACM_DENY)
            {
                TACM_TRC_ARG2 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                               "TacApiFilterChannel : "
                               "Profile %u : Group %x is denied\r\n",
                               pTacFilterPkt->u4ProfileId, u4GrpAddr);
                i4RetVal = TACM_DENY;
            }
        }
        else
        {
            if (pTacProfileEntry->u1ProfileAction == TACM_PERMIT)
            {
                TACM_TRC_ARG2 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                               "TacApiFilterChannel : "
                               "Profile %u : Group %x is denied\r\n",
                               pTacFilterPkt->u4ProfileId, u4GrpAddr);
                i4RetVal = TACM_DENY;
            }
        }

        if (i4RetVal == TACM_PERMIT)
        {
            TACM_TRC_ARG2 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                           "TacApiFilterChannel : "
                           "Profile %u : Group %x is allowed\r\n",
                           pTacFilterPkt->u4ProfileId, u4GrpAddr);
        }
    }
    else
    {
        /*  If the packet to be filtered contains specific sources, the
         *  source list is sent as a pointer. The sources that are denied are
         *  memset to 0 and the same pointer is sent back to module
         */
        for (u2Count = 0; u2Count < pTacFilterPkt->u4NumSrc; u2Count++)
        {
            MEMCPY (&u4SrcAddr, (pu1SrcList + (u2Count * u1AddrLen)),
                    u1AddrLen);
            u4SrcAddr = OSIX_NTOHL (u4SrcAddr);

            MEMSET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
            MEMCPY (au1SrcAddr, &u4SrcAddr, u1AddrLen);

            i1MatchFound = TacUtilSearchHashBucket (pTacProfileEntry,
                                                    pTacFilterPkt->GroupAddr,
                                                    au1SrcAddr, u4HashIndex,
                                                    u1Mode);

            if ((i1MatchFound == TACM_FALSE) && (u4HashIndex != 0))
            {
                /* Since a macth is not found, search the default hash bucket */
                i1MatchFound = TacUtilSearchHashBucket
                    (pTacProfileEntry,
                     pTacFilterPkt->GroupAddr, au1SrcAddr, 0, u1Mode);
            }

            if (i1MatchFound == TACM_TRUE)
            {
                if (pTacProfileEntry->u1ProfileAction == TACM_DENY)
                {
                    TACM_TRC_ARG3 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                                   "TacApiFilterChannel : "
                                   "Profile %u : Group %x and Source %x is "
                                   "denied\r\n", pTacFilterPkt->u4ProfileId,
                                   u4GrpAddr, u4SrcAddr);
                    MEMSET ((pu1SrcList + (u2Count * u1AddrLen)), 0, u1AddrLen);
                }
                else
                {
                    TACM_TRC_ARG3 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                                   "TacApiFilterChannel : "
                                   "Profile %u : Group %x and Source %x is "
                                   "allowed\r\n", pTacFilterPkt->u4ProfileId,
                                   u4GrpAddr, u4SrcAddr);
                    i1Flag = TACM_TRUE;
                }
            }
            else
            {
                if (pTacProfileEntry->u1ProfileAction == TACM_DENY)
                {
                    TACM_TRC_ARG3 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                                   "TacApiFilterChannel : "
                                   "Profile %u : Group %x and Source %x is "
                                   "allowed\r\n", pTacFilterPkt->u4ProfileId,
                                   u4GrpAddr, u4SrcAddr);
                    i1Flag = TACM_TRUE;
                }
                else
                {
                    TACM_TRC_ARG3 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                                   "TacApiFilterChannel : "
                                   "Profile %u : Group %x and Source %x is "
                                   "denied\r\n", pTacFilterPkt->u4ProfileId,
                                   u4GrpAddr, u4SrcAddr);
                    MEMSET ((pu1SrcList + (u2Count * u1AddrLen)), 0, u1AddrLen);
                }
            }
        }

        if (i1Flag == TACM_FALSE)
        {
            /* This case occurs when all the sources are denied */
            i4RetVal = TACM_DENY;
        }
    }

    TACM_TRC (TACM_FN_EXIT, "TacApiFilterChannel Exited\r\n");

    TACM_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : TacApiUpdatePortRefCount                             */
/*                                                                           */
/* Description        : This function updates the port reference count for   */
/*                      the particular profile. A profile can be deleted     */
/*                      only if the port reference count is 0                */
/*                                                                           */
/* Input(s)           : u4ProfileId      - Profile ID                        */
/*                      u1AddressType    - Internet address type             */
/*                      u1UpdtFlag       - TACM_INTF_MAP/TACM_INTF_UNMAP     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacApiUpdatePortRefCount (UINT4 u4ProfileId, UINT1 u1AddressType,
                          UINT1 u1UpdtFlag)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    TACM_LOCK ();

    TACM_TRC (TACM_FN_ENTRY, "TacApiUpdatePortRefCount Entered\r\n");

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4ProfileId;
    TacProfileEntry.u1AddressType = u1AddressType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_UNLOCK ();
        return TACM_FAILURE;
    }

    if (u1UpdtFlag == TACM_MAP)
    {
        pTacProfileEntry->u4IntfRefCnt++;
    }
    else
    {
        pTacProfileEntry->u4IntfRefCnt--;
    }

    TACM_TRC (TACM_FN_EXIT, "TacApiUpdatePortRefCount Exited\r\n");

    TACM_UNLOCK ();
    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacApiUpdateVlanRefCount                             */
/*                                                                           */
/* Description        : This function updates the VLAN reference count for   */
/*                      the particular profile. A profile can be deleted     */
/*                      only if the VLAN reference count is 0                */
/*                                                                           */
/* Input(s)           : u4Instance       - Context ID                        */
/*                      u4ProfileId      - Profile ID                        */
/*                      u1AddressType    - Internet address type             */
/*                      u1UpdtFlag       - TACM_INTF_MAP/TACM_INTF_UNMAP     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacApiUpdateVlanRefCount (UINT4 u4Instance, UINT4 u4ProfileId,
                          UINT1 u1AddressType, UINT1 u1UpdtFlag)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    TACM_LOCK ();

    TACM_TRC (TACM_FN_ENTRY, "TacApiUpdateVlanRefCount Entered\r\n");

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4ProfileId;
    TacProfileEntry.u1AddressType = u1AddressType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_UNLOCK ();
        return TACM_FAILURE;
    }

    if (u1UpdtFlag == TACM_MAP)
    {
        pTacProfileEntry->u4VlanRefCnt++;
        OSIX_BITLIST_SET_BIT (pTacProfileEntry->au1VlanInstBitMap,
                              (u4Instance + 1), TACM_MAX_CXT_BYTES);
    }
    else
    {
        pTacProfileEntry->u4VlanRefCnt--;
        OSIX_BITLIST_RESET_BIT (pTacProfileEntry->au1VlanInstBitMap,
                                (u4Instance + 1), TACM_MAX_CXT_BYTES);
    }

    TACM_TRC (TACM_FN_EXIT, "TacApiUpdateVlanRefCount Exited\r\n");

    TACM_UNLOCK ();
    return TACM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : TacApiGetPrfStats                                    */
/*                                                                           */
/* Description        : This function gets the reference count for this      */
/*                      profile. Returns reference counts of both port       */
/*                      and VLAN.                                            */
/*                                                                           */
/* Input(s)           : u4Instance       - Context ID                        */
/*                      u4ProfileId      - Profile ID                        */
/*                      u1AddressType    - Internet address type             */
/*                      pTacmPrfStats    - Structure to update the reference */
/*                      counts of both Interface & VLAN reference count      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacApiGetPrfStats (UINT4 u4Instance, UINT4 u4ProfileId,
                   UINT1 u1AddressType, tTacmPrfStats * pTacmPrfStats)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    TACM_LOCK ();

    TACM_TRC (TACM_FN_ENTRY, "TacApiGetPrfStats Entered\r\n");

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    MEMSET (pTacmPrfStats, 0, sizeof (tTacmPrfStats));

    TacProfileEntry.u4ProfileId = u4ProfileId;
    TacProfileEntry.u1AddressType = u1AddressType;

    if ((pTacProfileEntry = (tTacProfileEntry *) RBTreeGet (TACM_PROFILE_TREE,
                                                            (tRBElem *) &
                                                            TacProfileEntry)) ==
        NULL)
    {
        TACM_UNLOCK ();
        TACM_TRC (TACM_FN_EXIT, "TacApiGetPrfStats : No entry exists\r\n");
        return TACM_FAILURE;
    }

    pTacmPrfStats->u4VlanRefCnt = pTacProfileEntry->u4VlanRefCnt;
    pTacmPrfStats->u4IntfRefCnt = pTacProfileEntry->u4IntfRefCnt;
    OSIX_BITLIST_IS_BIT_SET
        (pTacProfileEntry->au1VlanInstBitMap, (u4Instance + 1),
         TACM_MAX_CXT_BYTES, pTacmPrfStats->u1VlanInstRefCnt);

    TACM_TRC (TACM_FN_EXIT, "TacApiGetPrfStats Exited\r\n");

    TACM_UNLOCK ();
    return TACM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : TacSearchGrpSrcRecordInProfile                       */
/*                                                                           */
/* Description        : This function searches for the Group/Source(Channel) */
/*                      passed through the SLL. If the Channel is present    */
/*                      in the particular profile, then it will update the   */
/*                      status flag as TRUE                                  */
/*                                                                           */
/* Input(s)           : u4ProfileId      - Profile ID                        */
/*                      pGrpRecords      - Ptr to Group/Source records SLL   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacSearchGrpSrcRecordInProfile (UINT4 u4ProfileId, tGrpRecords * pGrpRecords)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;
    tGrpSrcStatusEntry *pGrpSrcStatusNode = NULL;
    tIPvXAddr           GrpAddr;
    UINT4               u4HashIndex = 0;
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    INT1                i1MatchFound = TACM_FALSE;

    pGrpRecords->u4NumOfMatchFound = 0;

    TACM_LOCK ();

    TACM_TRC (TACM_FN_ENTRY, "TacSearchGrpSrcRecordInProfile Entered\r\n");

    /* Get the profile Id using the Profile Id and address type */
    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4ProfileId;
    TacProfileEntry.u1AddressType = IPVX_ADDR_FMLY_IPV4;

    if (((pTacProfileEntry = (tTacProfileEntry *)
          RBTreeGet (TACM_PROFILE_TREE,
                     (tRBElem *) & TacProfileEntry)) == NULL) ||
        (pTacProfileEntry->u1RowStatus != TACM_ACTIVE))
    {
        /* The corresponding profile is either not present or not active */
        TACM_TRC_ARG1 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                       "TacSearchGrpSrcRecordInProfile : "
                       "Profile %u is not available or not active\r\n",
                       u4ProfileId);
        TACM_TRC (TACM_FN_EXIT, "TacSearchGrpSrcRecordInProfile Exited\r\n");
        TACM_UNLOCK ();
        return TACM_FAILURE;
    }

    TACM_SLL_SCAN ((pGrpRecords->pGrpRecList), pGrpSrcStatusNode,
                   tGrpSrcStatusEntry *)
    {

        if (TACM_IS_RSVD_IPV4_MCAST_ADDRESS (pGrpSrcStatusNode->u4GroupAddress))
        {
            TACM_TRC_ARG1 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                           "TacSearchGrpSrcRecordInProfile : "
                           "Profile %u : "
                           "Reserved multicast address is not searched\r\n",
                           u4ProfileId);
            continue;
        }

        u4HashIndex = TacUtilGetHashIndex (pTacProfileEntry->u1NoOfBuckets,
                                           pTacProfileEntry->u4MinGrpAddr,
                                           pTacProfileEntry->u4MaxGrpAddr,
                                           pGrpSrcStatusNode->u4GroupAddress);

        TACM_TRC_ARG2 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                       "TacSearchGrpSrcRecordInProfil : Profile %u : "
                       "Searching for Group %x\r\n", u4ProfileId,
                       pGrpSrcStatusNode->u4GroupAddress);

        MEMSET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
        MEMCPY (au1SrcAddr, &pGrpSrcStatusNode->u4SourceAddress,
                IPVX_IPV4_ADDR_LEN);

        IPVX_ADDR_INIT_IPV4 (GrpAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &pGrpSrcStatusNode->u4GroupAddress);

        i1MatchFound = TacUtilSearchHashBucket (pTacProfileEntry, GrpAddr,
                                                au1SrcAddr, u4HashIndex,
                                                pGrpSrcStatusNode->
                                                u1RecordType);

        if ((i1MatchFound == TACM_FALSE) && (u4HashIndex != 0))
        {
            /* Since a macth is not found, search the default hash bucket */
            i1MatchFound = TacUtilSearchHashBucket (pTacProfileEntry, GrpAddr,
                                                    au1SrcAddr, 0,
                                                    pGrpSrcStatusNode->
                                                    u1RecordType);
        }
        if (i1MatchFound == TACM_TRUE)
        {
            pGrpSrcStatusNode->u1Status = TACM_TRUE;
            pGrpRecords->u4NumOfMatchFound++;
        }
    }
    TACM_TRC (TACM_FN_EXIT, "TacSearchGrpSrcRecordInProfile Exited\r\n");

    TACM_UNLOCK ();
    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacSearchGrpAddressInProfile                         */
/*                                                                           */
/* Description        : This function searches for the particular Group      */
/*                      address in the specified Profile. If match is found  */
/*                      it will return TRUE.                                 */
/*                                                                           */
/* Input(s)           : u4ProfileId      - Profile ID                        */
/*                      u4GrpAddr        - Group address to be searched      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_TRUE/TACM_FALSE                                 */
/*****************************************************************************/
INT4
TacSearchGrpAddressInProfile (UINT4 u4ProfileId, UINT4 u4GrpAddr)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;
    tIPvXAddr           GrpAddr;
    UINT4               u4HashIndex = 0;
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    INT1                i1MatchFound = TACM_FALSE;

    TACM_LOCK ();

    TACM_TRC (TACM_FN_ENTRY, "TacSearchGrpAddressInProfile Entered\r\n");

    if (TACM_IS_RSVD_IPV4_MCAST_ADDRESS (u4GrpAddr))
    {
        TACM_TRC_ARG1 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                       "TacSearchGrpAddressInProfile : "
                       "Profile %u : "
                       "Reserved multicast address is not searched\r\n",
                       u4ProfileId);
        TACM_TRC (TACM_FN_EXIT, "TacSearchGrpAddressInProfile Exited\r\n");
        TACM_UNLOCK ();
        return TACM_FALSE;
    }

    /* Get the profile Id using the Profile Id and address type */
    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4ProfileId;
    TacProfileEntry.u1AddressType = IPVX_ADDR_FMLY_IPV4;

    if (((pTacProfileEntry = (tTacProfileEntry *)
          RBTreeGet (TACM_PROFILE_TREE,
                     (tRBElem *) & TacProfileEntry)) == NULL) ||
        (pTacProfileEntry->u1RowStatus != TACM_ACTIVE))
    {
        /* The corresponding profile is either not present or not active */
        TACM_TRC_ARG1 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                       "TacSearchGrpAddressInProfile : "
                       "Profile %u is not available or not active\r\n",
                       u4ProfileId);

        TACM_TRC (TACM_FN_EXIT, "TacSearchGrpAddressInProfile Exited\r\n");
        TACM_UNLOCK ();
        return TACM_FALSE;
    }

    u4HashIndex = TacUtilGetHashIndex (pTacProfileEntry->u1NoOfBuckets,
                                       pTacProfileEntry->u4MinGrpAddr,
                                       pTacProfileEntry->u4MaxGrpAddr,
                                       u4GrpAddr);

    TACM_TRC_ARG2 (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
                   "TacSearchGrpAddressInProfile : "
                   "Profile %u : Searching for Group %x\r\n",
                   u4ProfileId, u4GrpAddr);

    MEMSET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    IPVX_ADDR_INIT_IPV4 (GrpAddr, IPVX_ADDR_FMLY_IPV4, (UINT1 *) &u4GrpAddr);

    i1MatchFound = TacUtilSearchHashBucket (pTacProfileEntry, GrpAddr,
                                            au1SrcAddr, u4HashIndex,
                                            TACM_EXCLUDE);

    if ((i1MatchFound == TACM_FALSE) && (u4HashIndex != 0))
    {
        /* Since a macth is not found, search the default hash bucket */
        i1MatchFound = TacUtilSearchHashBucket (pTacProfileEntry, GrpAddr,
                                                au1SrcAddr, 0, TACM_EXCLUDE);
    }

    TACM_TRC (TACM_FN_EXIT, "TacSearchGrpAddressInProfile Exited\r\n");

    TACM_UNLOCK ();

    return i1MatchFound;
}

/*****************************************************************************/
/* Function Name      : TacApiGetPrfAction                                   */
/*                                                                           */
/* Description        : This function gets the Profile action  for this      */
/*                      profile.                                             */
/*                                                                           */
/* Input(s)           : u4ProfileId      - Profile ID                        */
/*                      u1AddressType    - Internet address type             */
/*                                                                           */
/* Output(s)          : pu1ProfileAction - Profile action                    */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacApiGetPrfAction (UINT4 u4ProfileId, UINT1 u1AddressType,
                    UINT1 *pu1ProfileAction)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    TACM_LOCK ();

    TACM_TRC (TACM_FN_ENTRY, "TacApiGetPrfAction Entered\r\n");

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));

    TacProfileEntry.u4ProfileId = u4ProfileId;
    TacProfileEntry.u1AddressType = u1AddressType;

    if ((pTacProfileEntry = (tTacProfileEntry *) RBTreeGet (TACM_PROFILE_TREE,
                                                            (tRBElem *) &
                                                            TacProfileEntry)) ==
        NULL)
    {
        TACM_UNLOCK ();
        TACM_TRC (TACM_FN_EXIT, "TacApiGetPrfAction :No entry exists\r\n");
        return TACM_FAILURE;
    }

    *pu1ProfileAction = pTacProfileEntry->u1ProfileAction;

    TACM_TRC (TACM_FN_EXIT, "TacApiGetPrfAction Exited\r\n");

    TACM_UNLOCK ();
    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacStatusUpdate.                                     */
/*                                                                           */
/* Description        : This function will be called to enable/disable       */
/*                        the TAC module                                     */
/*                                                                           */
/* Input(s)           : i4TacStatus.                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS - On success                            */
/*                      TACM_FAILURE - On failure                            */
/*****************************************************************************/
INT4
TacStatusUpdate (INT4 i4TacStatus)
{
#ifdef NPAPI_WANTED
    if (FsTacHwUpdateStatus (i4TacStatus) != FNP_SUCCESS)
    {
        TACM_TRC (TACM_FN_EXIT, "TacStatusUpdate Failed\r\n");
        return TACM_FAILURE;
    }
#else
    UNUSED_PARAM (i4TacStatus);
#endif

    return TACM_SUCCESS;
}
