/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: tacprf.c,v 1.5 2009/12/07 15:47:03 prabuc Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : tacprf.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC Profile Module                             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains multicast profile related   */
/*                          : functions for TAC Module. This module contains */
/*                          : creation and deletion or profiles and filters  */
/*---------------------------------------------------------------------------*/

#include "tacinc.h"

/* Function prototypes */
PRIVATE INT4        TacPrfDeleteHashNodes
PROTO ((tTacProfileEntry * pTacProfileEntry));

/*****************************************************************************/
/* Function Name      : TacPrfCreateProfile                                  */
/*                                                                           */
/* Description        : This function creates a profile entry                */
/*                                                                           */
/* Input(s)           : u4ProfileId      - Profile Identifier                */
/*                      u1AddressType    - Internet address type             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : Pointer to Profile entry, if successful; else NULL   */
/*****************************************************************************/
tTacProfileEntry   *
TacPrfCreateProfile (UINT4 u4ProfileId, UINT1 u1AddressType)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;

    TACM_TRC (TACM_FN_ENTRY, "TacPrfCreateProfile Entered\r\n");

    pTacProfileEntry = TACM_ALLOC_MEM_BLOCK (tTacProfileEntry,
                                             TACM_PROFILE_MEMPOOL_ID);

    if (pTacProfileEntry == NULL)
    {
        TACM_TRC (TACM_CRITICAL_TRC | MGMT_TRC |
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "TacPrfCreateProfile : Profile memory " "pool exhausted\r\n");
        return NULL;
    }

    MEMSET (pTacProfileEntry, 0, sizeof (tTacProfileEntry));

    pTacProfileEntry->u4ProfileId = u4ProfileId;
    pTacProfileEntry->u1AddressType = u1AddressType;
    pTacProfileEntry->u1ProfileAction = TACM_DENY;
    pTacProfileEntry->u1FilterAddOrDelete = TACM_FALSE;

    /* Add the profile entry to the profile RB Tree */
    if (RBTreeAdd (TACM_PROFILE_TREE, (tRBElem *) pTacProfileEntry)
        == RB_FAILURE)
    {
        TACM_TRC_ARG2 (TACM_CRITICAL_TRC | MGMT_TRC | OS_RESOURCE_TRC,
                       "TacPrfCreateProfile : Profile %u "
                       "and Address type %d RB Tree addition failed\r\n",
                       u4ProfileId, u1AddressType);
        TACM_RELEASE_MEM_BLOCK (TACM_PROFILE_MEMPOOL_ID, pTacProfileEntry);
        return NULL;
    }

    /* Create the embedded RB Tree for the filter entries */
    pTacProfileEntry->pPrfFilterRoot = RBTreeCreateEmbedded
        (TACM_OFFSET (tTacPrfFilterEntry, RbNode), TacUtilComparePrfFilterInfo);

    if (pTacProfileEntry->pPrfFilterRoot == NULL)
    {
        /*  RB Tree creation failed for filter entries. Remove the profile
         *  entry from profile RB Tree and free the memory
         */
        TACM_TRC_ARG2 (OS_RESOURCE_TRC | MGMT_TRC |
                       ALL_FAILURE_TRC | TACM_CRITICAL_TRC,
                       "TacPrfCreateProfile : Profile %u "
                       "and Address type %d Filter RB Tree creation "
                       "failed\r\n", u4ProfileId, u1AddressType);
        RBTreeRemove (TACM_PROFILE_TREE, pTacProfileEntry);
        TACM_RELEASE_MEM_BLOCK (TACM_PROFILE_MEMPOOL_ID, pTacProfileEntry);
        return NULL;
    }

    /* Create the Hash Table for filter rules */
    pTacProfileEntry->pPrfFilterHashTable =
        TMO_HASH_Create_Table (TACM_MAX_BUCKETS + 1, NULL, FALSE);

    if (pTacProfileEntry->pPrfFilterHashTable == NULL)
    {
        /* Hash Table creation failed for the filter entries */
        TACM_TRC_ARG2 (OS_RESOURCE_TRC | MGMT_TRC |
                       ALL_FAILURE_TRC | TACM_CRITICAL_TRC,
                       "TacPrfCreateProfile : Profile %u "
                       "and Address type %d Filter Hash List creation "
                       "failed\r\n", u4ProfileId, u1AddressType);

        RBTreeDestroy (pTacProfileEntry->pPrfFilterRoot, TacUtilFreePrfFilter,
                       0);
        RBTreeRemove (TACM_PROFILE_TREE, pTacProfileEntry);
        TACM_RELEASE_MEM_BLOCK (TACM_PROFILE_MEMPOOL_ID, pTacProfileEntry);
        return NULL;
    }

    TACM_TRC (TACM_FN_EXIT, "TacPrfCreateProfile Exited\r\n");

    return pTacProfileEntry;
}

/*****************************************************************************/
/* Function Name      : TacPrfDeleteProfile                                  */
/*                                                                           */
/* Description        : This function deletes a profile entry. The rules     */
/*                      created for this profile are also deleted.           */
/*                      Profiles can be deleted only if there is no          */
/*                      reference count.                                     */
/*                                                                           */
/* Input(s)           : pTacProfileEntry - Profile Entry                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacPrfDeleteProfile (tTacProfileEntry * pTacProfileEntry)
{
    TACM_TRC (TACM_FN_ENTRY, "TacPrfDeleteProfile Entered\r\n");

    if (pTacProfileEntry == NULL)
    {
        return TACM_SUCCESS;
    }

    /*  The profile can be deleted only when there is no mapping
     *  across all the modules. The profile cannot be deleted if
     *  the reference count is not 0
     */
    if ((pTacProfileEntry->u4IntfRefCnt != 0) ||
        (pTacProfileEntry->u4VlanRefCnt != 0))
    {
        TACM_TRC_ARG3 (TACM_CRITICAL_TRC | MGMT_TRC | ALL_FAILURE_TRC,
                       "TacPrfDeleteProfile : Cannot "
                       "be deleted !!! Profile %u and Address type %d has "
                       "%d port or vlan mappings in other modules\r\n",
                       pTacProfileEntry->u4ProfileId,
                       pTacProfileEntry->u1AddressType,
                       pTacProfileEntry->u4IntfRefCnt);
        return TACM_FAILURE;
    }

    /* Delete the Hash nodes used for the filter rules in the profile */
    TacPrfDeleteHashNodes (pTacProfileEntry);

    /* Delete all the rules from the RB Tree */
    RBTreeDestroy (pTacProfileEntry->pPrfFilterRoot, TacUtilFreePrfFilter, 0);
    RBTreeRemove (TACM_PROFILE_TREE, pTacProfileEntry);

    /* Delete th hash table and clear the memory */
    TMO_HASH_Delete_Table (pTacProfileEntry->pPrfFilterHashTable, NULL);

    TACM_RELEASE_MEM_BLOCK (TACM_PROFILE_MEMPOOL_ID, pTacProfileEntry);

    TACM_TRC (TACM_FN_EXIT, "TacPrfDeleteProfile Exited\r\n");

    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacPrfCreateFilter                                   */
/*                                                                           */
/* Description        : This function creates a new rule for a particular    */
/*                      profile, The filter rules are created based on the   */
/*                      group and source address range. The rules are        */
/*                      created and added in a RB Tree. The memory reserved  */
/*                      for the rules are common for all the profiles.       */
/*                                                                           */
/* Input(s)           : pTacProfileEntry - Profile Entry.                    */
/*                      pu1GrpStartAddr  - Start address of group range.     */
/*                      pu1GrpEndAddr    - End address of group range.       */
/*                      pu1SrcStartAddr  - Start address of source range.    */
/*                      pu1SrcEndAddr    - End address of source range.      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : Filter entry, if successful; else NULL               */
/*****************************************************************************/
tTacPrfFilterEntry *
TacPrfCreateFilter (tTacProfileEntry * pTacProfileEntry,
                    UINT1 *pu1GrpStartAddr, UINT1 *pu1GrpEndAddr,
                    UINT1 *pu1SrcStartAddr, UINT1 *pu1SrcEndAddr)
{
    tTacPrfFilterEntry *pTacPrfFilterEntry = NULL;
    UINT1               u1AddrLen = 0;

    TACM_TRC (TACM_FN_ENTRY, "TacPrfCreateFilter Entered\r\n");

    pTacPrfFilterEntry = TACM_ALLOC_MEM_BLOCK (tTacPrfFilterEntry,
                                               TACM_PRF_FILTER_MEMPOOL_ID);

    if (pTacPrfFilterEntry == NULL)
    {
        TACM_TRC (TACM_CRITICAL_TRC | MGMT_TRC |
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "TacPrfCreateFilter : Filter memory pool exhausted\r\n");
        return NULL;
    }

    /*  If any filter rule is added, it will result in creation of Hash Table
     *  since the number of filter rules and the maximum and minimum group can
     *  be changed. Hence the Hash nodes can be deleted if this is the first
     *  rule added to the filter table after the profile entry has been changed
     *  to not in service. In this case, no need to free the memory for Hash Table
     *  as it will be used again once the profile is made active.
     */
    if (pTacProfileEntry->u1FilterAddOrDelete == TACM_FALSE)
    {
        TACM_TRC (TACM_FILTER_TRC | MGMT_TRC,
                  "TacPrfCreateFilter : Deleting the hash table\r\n");
        TacPrfDeleteHashNodes (pTacProfileEntry);
        pTacProfileEntry->u1FilterAddOrDelete = TACM_TRUE;
    }

    MEMSET (pTacPrfFilterEntry, 0, sizeof (tTacPrfFilterEntry));

    /* Store the group and source address range */
    if (pTacProfileEntry->u1AddressType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }

    MEMCPY (pTacPrfFilterEntry->GrpStartAddr, pu1GrpStartAddr, u1AddrLen);
    MEMCPY (pTacPrfFilterEntry->GrpEndAddr, pu1GrpEndAddr, u1AddrLen);
    MEMCPY (pTacPrfFilterEntry->SrcStartAddr, pu1SrcStartAddr, u1AddrLen);
    MEMCPY (pTacPrfFilterEntry->SrcEndAddr, pu1SrcEndAddr, u1AddrLen);

    /* Set the default filter mode as any */
    pTacPrfFilterEntry->u1FilterMode = TACM_ANY;

    /*  Add the node to the RB Tree. Addition to the Hash Table is done
     *  after making th profile entry as active
     */
    if (RBTreeAdd (pTacProfileEntry->pPrfFilterRoot,
                   (tRBElem *) pTacPrfFilterEntry) == RB_FAILURE)
    {
        TACM_TRC_ARG4 (TACM_CRITICAL_TRC | MGMT_TRC | OS_RESOURCE_TRC,
                       "TacPrfCreateFilter : Filter RB Tree "
                       "addition failed for group start %x group end %x source "
                       "start %x source end %x\r\n",
                       *((UINT4 *) (VOID *) (pu1GrpStartAddr)),
                       *((UINT4 *) (VOID *) (pu1GrpEndAddr)),
                       *((UINT4 *) (VOID *) (pu1SrcStartAddr)),
                       *((UINT4 *) (VOID *) (pu1SrcEndAddr)));
        TACM_RELEASE_MEM_BLOCK (TACM_PRF_FILTER_MEMPOOL_ID, pTacPrfFilterEntry);
        return NULL;
    }

    TACM_TRC (TACM_FN_EXIT, "TacPrfCreateFilter Exited\r\n");

    return pTacPrfFilterEntry;
}

/*****************************************************************************/
/* Function Name      : TacPrfDeleteFilter                                   */
/*                                                                           */
/* Description        : This function deletes a filter entry from a profile. */
/*                      The filter is deleted based on the group and the     */
/*                      source ranges                                        */
/*                                                                           */
/* Input(s)           : pTacProfileEntry   - Profile Entry                   */
/*                      pTacPrfFilterEntry - Filter Entry                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacPrfDeleteFilter (tTacProfileEntry * pTacProfileEntry,
                    tTacPrfFilterEntry * pTacPrfFilterEntry)
{
    TACM_TRC (TACM_FN_ENTRY, "TacPrfDeleteFilter Entered\r\n");

    if ((RBTreeRemove (pTacProfileEntry->pPrfFilterRoot, pTacPrfFilterEntry))
        == RB_SUCCESS)
    {
        /*  If any filter rule is deleted, it will result in creation of Hash
         *  Table since the number of filter rules and the maximum and minimum
         *  group can be changed. Hence the Hash Table can be deleted if this is
         *  the first rule deleted from the filter table after the profile entry
         *  has been changed to not in service
         */
        if (pTacProfileEntry->u1FilterAddOrDelete == TACM_FALSE)
        {
            TacPrfDeleteHashNodes (pTacProfileEntry);
            pTacProfileEntry->u1FilterAddOrDelete = TACM_TRUE;
        }
    }
    else
    {
        return TACM_FAILURE;
    }

    /* Release the memory */
    TACM_RELEASE_MEM_BLOCK (TACM_PRF_FILTER_MEMPOOL_ID, pTacPrfFilterEntry);

    TACM_TRC (TACM_FN_EXIT, "TacPrfDeleteFilter Exited\r\n");

    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacPrfDeleteHashNodes                                */
/*                                                                           */
/* Description        : This function deletes a hash table containing the    */
/*                      filter entries for a particular profile              */
/*                                                                           */
/* Input(s)           : pTacProfileEntry - Profile Entry.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
PRIVATE INT4
TacPrfDeleteHashNodes (tTacProfileEntry * pTacProfileEntry)
{
    UINT4               u4Hindex = 0;
    tTacPrfFilterHashEntry *pTacPrfFilterHashEntry = NULL;

    TACM_TRC (TACM_FN_ENTRY, "TacPrfDeleteHashNodes Entered\r\n");

    TMO_HASH_Scan_Table (pTacProfileEntry->pPrfFilterHashTable, u4Hindex)
    {
        while ((pTacPrfFilterHashEntry =
                (tTacPrfFilterHashEntry *) TMO_HASH_Get_First_Bucket_Node
                (pTacProfileEntry->pPrfFilterHashTable, u4Hindex)) != NULL)
        {
            TMO_HASH_Delete_Node (pTacProfileEntry->pPrfFilterHashTable,
                                  &(pTacPrfFilterHashEntry->PrfFilterHashNode),
                                  u4Hindex);
            TACM_RELEASE_MEM_BLOCK (TACM_PRF_FILTER_HASH_MEMPOOL_ID,
                                    pTacPrfFilterHashEntry);
        }
    }

    TACM_TRC (TACM_FN_EXIT, "TacPrfDeleteHashNodes Exited\r\n");

    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacPrfCreateHashTable                                */
/*                                                                           */
/* Description        : This function creates a hash table and stores all    */
/*                      the filter rules in the Hash. The hash table is      */
/*                      created when a profile entry is made active after    */
/*                      creation or deletion of atleast one filter           */
/*                                                                           */
/* Input(s)           : pTacProfileEntry - Profile Entry.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacPrfCreateHashTable (tTacProfileEntry * pTacProfileEntry)
{
    tTacPrfFilterEntry *pTacPrfFilterEntry = NULL;
    UINT4               u4GrpStartAddr = 0;
    UINT4               u4GrpEndAddr = 0;
    UINT4               u4NoOfFilters = 0;
    UINT4               u4GrpStartHashIndex = 0;
    UINT4               u4GrpEndHashIndex = 0;
    UINT4               u4HashIndex = 0;
    UINT1               u1NoOfBuckets = 0;

    TACM_TRC (TACM_FN_ENTRY, "TacPrfCreateHashTable Entered\r\n");

    /* Get the Total number of filter entries created in this profile */
    if (RBTreeCount (pTacProfileEntry->pPrfFilterRoot, &u4NoOfFilters)
        == RB_FAILURE)
    {
        return TACM_FAILURE;
    }

    if (u4NoOfFilters == 0)
    {
        /* No filters in this profile */
        return TACM_SUCCESS;
    }

    /* Get the total number of buckets to be used */
    u1NoOfBuckets = TacUtilGetNoOfHashBuckets (u4NoOfFilters);
    pTacProfileEntry->u1NoOfBuckets = u1NoOfBuckets;

    /*  Get the minimum and maximum group address configured in the
     *  profile
     */
    TacUtilGetMinMaxGrpAddr (pTacProfileEntry);

    /* Scan the filters RB Tree and add the entries to the Hash Table */
    pTacPrfFilterEntry = (tTacPrfFilterEntry *)
        RBTreeGetFirst (pTacProfileEntry->pPrfFilterRoot);

    while (pTacPrfFilterEntry != NULL)
    {
        /*  Copy the start and end IPv4 multicast address and get the hash
         *  index
         */
        MEMCPY (&u4GrpStartAddr, pTacPrfFilterEntry->GrpStartAddr,
                IPVX_IPV4_ADDR_LEN);
        MEMCPY (&u4GrpEndAddr, pTacPrfFilterEntry->GrpEndAddr,
                IPVX_IPV4_ADDR_LEN);
        u4GrpStartAddr = OSIX_NTOHL (u4GrpStartAddr);
        u4GrpEndAddr = OSIX_NTOHL (u4GrpEndAddr);

        u4GrpStartHashIndex =
            TacUtilGetHashIndex (u1NoOfBuckets,
                                 pTacProfileEntry->u4MinGrpAddr,
                                 pTacProfileEntry->u4MaxGrpAddr,
                                 u4GrpStartAddr);

        u4GrpEndHashIndex =
            TacUtilGetHashIndex (u1NoOfBuckets,
                                 pTacProfileEntry->u4MinGrpAddr,
                                 pTacProfileEntry->u4MaxGrpAddr, u4GrpEndAddr);

        /*  The difference between the two hash index gives the number
         *  of buckets to which the filter entry is hashed. If this
         *  number is greater than pre-defined number of buckets that
         *  a filter entry can be mapped, the filter is added to the
         *  default hash bucket. This is done to avoid many duplications
         *  The pre-defined number is identified by TACM_MAX_BUCKETS_PER_FILTER
         */
        if ((u4GrpEndHashIndex - u4GrpStartHashIndex + 1) >
            TACM_MAX_BUCKETS_PER_FILTER)
        {
            TacUtilAddHashNode (pTacProfileEntry, pTacPrfFilterEntry, 0);
        }
        else
        {
            for (u4HashIndex = u4GrpStartHashIndex;
                 u4HashIndex <= u4GrpEndHashIndex; u4HashIndex++)
            {
                TacUtilAddHashNode (pTacProfileEntry, pTacPrfFilterEntry,
                                    u4HashIndex);
            }
        }

        pTacPrfFilterEntry = (tTacPrfFilterEntry *)
            RBTreeGetNext (pTacProfileEntry->pPrfFilterRoot,
                           (tRBElem *) pTacPrfFilterEntry, NULL);
    }

    TACM_TRC (TACM_FN_EXIT, "TacPrfCreateHashTable Exited\r\n");

    return TACM_SUCCESS;
}
