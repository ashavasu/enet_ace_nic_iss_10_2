/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstaclw.c,v 1.16 2014/01/07 10:35:55 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "tacinc.h"
#include  "taccli.h"

extern UINT4        FsTacStatus[11];

PRIVATE INT4        ValidateTacFilterConfig
PROTO ((tTacPrfFilterEntry * pTacPrfFilterEntry, UINT1 u1AddressType));

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTacMcastChannelDefaultBandwidth
 Input       :  The Indices

                The Object 
                retValFsTacMcastChannelDefaultBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsTacMcastChannelDefaultBandwidth
    (UINT4 *pu4RetValFsTacMcastChannelDefaultBandwidth)
{
    UNUSED_PARAM (pu4RetValFsTacMcastChannelDefaultBandwidth);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTacTraceOption
 Input       :  The Indices

                The Object
                retValFsTacTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacTraceOption (UINT4 *pu4RetValFsTacTraceOption)
{
    *pu4RetValFsTacTraceOption = gTacGlobalConfig.u4TacmTrace;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTacStatus
 Input       :  The Indices

                     The Object
                retValFsTacStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacStatus (INT4 *pi4RetValFsTacStatus)
{
    *pi4RetValFsTacStatus = gTacGlobalConfig.i4TacStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTacMcastChannelDefaultBandwidth
 Input       :  The Indices

                The Object 
                setValFsTacMcastChannelDefaultBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsTacMcastChannelDefaultBandwidth
    (UINT4 u4SetValFsTacMcastChannelDefaultBandwidth)
{
    UNUSED_PARAM (u4SetValFsTacMcastChannelDefaultBandwidth);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTacTraceOption
 Input       :  The Indices

                The Object
                setValFsTacTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacTraceOption (UINT4 u4SetValFsTacTraceOption)
{
    gTacGlobalConfig.u4TacmTrace = u4SetValFsTacTraceOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTacStatus
 Input       :  The Indices

                The Object
                setValFsTacStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacStatus (INT4 i4SetValFsTacStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (i4SetValFsTacStatus == gTacGlobalConfig.i4TacStatus)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhSetFsTacStatus : "
                  "SNMP: IGMP Profiling Module already has the same "
                  "status\r\n");
        return SNMP_SUCCESS;
    }

    if (TacStatusUpdate (i4SetValFsTacStatus) != TACM_SUCCESS)
    {
        if (i4SetValFsTacStatus == TAC_ENABLED)
        {
            TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                      "SNMP: IGMP Profiling module Enable FAILED\n");
        }
        else
        {
            TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                      "SNMP: IGMP Profiling module Disable FAILED\n");
        }
        return SNMP_FAILURE;
    }

    gTacGlobalConfig.i4TacStatus = i4SetValFsTacStatus;

    SnmpNotifyInfo.pu4ObjectId = FsTacStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsTacStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 0;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsTacStatus));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTacMcastChannelDefaultBandwidth
 Input       :  The Indices

                The Object 
                testValFsTacMcastChannelDefaultBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsTacMcastChannelDefaultBandwidth
    (UINT4 *pu4ErrorCode, UINT4 u4TestValFsTacMcastChannelDefaultBandwidth)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValFsTacMcastChannelDefaultBandwidth);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTacTraceOption
 Input       :  The Indices

                The Object
                testValFsTacTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacTraceOption (UINT4 *pu4ErrorCode, UINT4 u4TestValFsTacTraceOption)
{
    if (u4TestValFsTacTraceOption > TACM_MAX_TRACE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTacStatus
 Input       :  The Indices

                The Object
                testValFsTacStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsTacStatus)
{
    if ((i4TestValFsTacStatus == TAC_ENABLED) ||
        (i4TestValFsTacStatus == TAC_DISABLED))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/* LOW LEVEL Routines for Table : FsTacMcastProfileTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTacMcastProfileTable
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1                nmhValidateIndexInstanceFsTacMcastProfileTable
    (UINT4 u4FsTacMcastProfileId, INT4 i4FsTacMcastProfileAddrType)
{
    tTacProfileEntry    TacProfileEntry;

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhValidateIndexInstanceFsTacMcastProfileTable : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTacMcastProfileTable
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsTacMcastProfileTable (UINT4 *pu4FsTacMcastProfileId,
                                        INT4 *pi4FsTacMcastProfileAddrType)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGetFirst (TACM_PROFILE_TREE)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4FsTacMcastProfileId = pTacProfileEntry->u4ProfileId;
    *pi4FsTacMcastProfileAddrType = (INT4) pTacProfileEntry->u1AddressType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTacMcastProfileTable
 Input       :  The Indices
                FsTacMcastProfileId
                nextFsTacMcastProfileId
                FsTacMcastProfileAddrType
                nextFsTacMcastProfileAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsTacMcastProfileTable
    (UINT4 u4FsTacMcastProfileId,
     UINT4 *pu4NextFsTacMcastProfileId,
     INT4 i4FsTacMcastProfileAddrType, INT4 *pi4NextFsTacMcastProfileAddrType)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGetNext (TACM_PROFILE_TREE,
                        (tRBElem *) & TacProfileEntry, NULL)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsTacMcastProfileId = pTacProfileEntry->u4ProfileId;
    *pi4NextFsTacMcastProfileAddrType = pTacProfileEntry->u1AddressType;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTacMcastProfileAction
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType

                The Object 
                retValFsTacMcastProfileAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacMcastProfileAction (UINT4 u4FsTacMcastProfileId,
                               INT4 i4FsTacMcastProfileAddrType,
                               INT4 *pi4RetValFsTacMcastProfileAction)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC, "nmhGetFsTacMcastProfileAction : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsTacMcastProfileAction =
        (INT4) pTacProfileEntry->u1ProfileAction;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTacMcastProfileDescription
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType

                The Object 
                retValFsTacMcastProfileDescription
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacMcastProfileDescription (UINT4 u4FsTacMcastProfileId,
                                    INT4 i4FsTacMcastProfileAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsTacMcastProfileDescription)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhGetFsTacMcastProfileDescription : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    pRetValFsTacMcastProfileDescription->i4_Length =
        (INT4) STRLEN (pTacProfileEntry->au1Description);
    MEMCPY (pRetValFsTacMcastProfileDescription->pu1_OctetList,
            pTacProfileEntry->au1Description,
            STRLEN (pTacProfileEntry->au1Description));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTacMcastProfileStatus
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType

                The Object 
                retValFsTacMcastProfileStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacMcastProfileStatus (UINT4 u4FsTacMcastProfileId,
                               INT4 i4FsTacMcastProfileAddrType,
                               INT4 *pi4RetValFsTacMcastProfileStatus)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC, "nmhGetFsTacMcastProfileStatus : "
                  "Profile does not exist\r\n");

        return SNMP_FAILURE;
    }

    *pi4RetValFsTacMcastProfileStatus = pTacProfileEntry->u1RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTacMcastProfileAction
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType

                The Object 
                setValFsTacMcastProfileAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacMcastProfileAction (UINT4 u4FsTacMcastProfileId,
                               INT4 i4FsTacMcastProfileAddrType,
                               INT4 i4SetValFsTacMcastProfileAction)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhSetFsTacMcastProfileAction : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    if (pTacProfileEntry->u1ProfileAction !=
        (UINT1) i4SetValFsTacMcastProfileAction)
    {
        pTacProfileEntry->u1ProfileAction =
            (UINT1) i4SetValFsTacMcastProfileAction;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTacMcastProfileDescription
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType

                The Object 
                setValFsTacMcastProfileDescription
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacMcastProfileDescription (UINT4 u4FsTacMcastProfileId,
                                    INT4 i4FsTacMcastProfileAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValFsTacMcastProfileDescription)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhSetFsTacMcastProfileDescription : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    if (pSetValFsTacMcastProfileDescription->i4_Length == 0)
    {
        MEMSET (pTacProfileEntry->au1Description, 0, TACM_MAX_DESCR_ARRAY_LEN);
    }

    if ((STRLEN (pTacProfileEntry->au1Description) !=
         (UINT4) pSetValFsTacMcastProfileDescription->i4_Length) ||
        ((MEMCMP (pTacProfileEntry->au1Description,
                  pSetValFsTacMcastProfileDescription->pu1_OctetList,
                  pSetValFsTacMcastProfileDescription->i4_Length)) != 0))
    {
        MEMSET (pTacProfileEntry->au1Description, 0, TACM_MAX_DESCR_ARRAY_LEN);
        MEMCPY (pTacProfileEntry->au1Description,
                pSetValFsTacMcastProfileDescription->pu1_OctetList,
                pSetValFsTacMcastProfileDescription->i4_Length);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTacMcastProfileStatus
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType

                The Object 
                setValFsTacMcastProfileStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacMcastProfileStatus (UINT4 u4FsTacMcastProfileId,
                               INT4 i4FsTacMcastProfileAddrType,
                               INT4 i4SetValFsTacMcastProfileStatus)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    pTacProfileEntry = (tTacProfileEntry *) RBTreeGet (TACM_PROFILE_TREE,
                                                       (tRBElem *)
                                                       & TacProfileEntry);

    if ((pTacProfileEntry != NULL) &&
        (pTacProfileEntry->u1RowStatus ==
         (UINT1) i4SetValFsTacMcastProfileStatus))
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValFsTacMcastProfileStatus)
    {
        case TACM_CREATE_AND_WAIT:
        case TACM_CREATE_AND_GO:
        {
            pTacProfileEntry = TacPrfCreateProfile
                (u4FsTacMcastProfileId, (UINT1) i4FsTacMcastProfileAddrType);

            if (pTacProfileEntry == NULL)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhSetFsTacMcastProfileStatus : "
                          "Profile creation failed\r\n");
                return SNMP_FAILURE;
            }

            if (i4SetValFsTacMcastProfileStatus == TACM_CREATE_AND_WAIT)
            {
                pTacProfileEntry->u1RowStatus = (UINT1) TACM_NOT_IN_SERVICE;
            }
            else
            {
                pTacProfileEntry->u1RowStatus = (UINT1) TACM_ACTIVE;
            }
            break;
        }
        case TACM_NOT_IN_SERVICE:
        {
            if (pTacProfileEntry == NULL)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhSetFsTacMcastProfileStatus : "
                          "Profile does not exist\r\n");
                return SNMP_FAILURE;
            }

            pTacProfileEntry->u1RowStatus = (UINT1) TACM_NOT_IN_SERVICE;
            pTacProfileEntry->u1FilterAddOrDelete = (UINT1) TACM_FALSE;
            break;
        }
        case TACM_ACTIVE:
        {
            if (pTacProfileEntry == NULL)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhSetFsTacMcastProfileStatus : "
                          "Profile does not exist\r\n");
                return SNMP_FAILURE;
            }

            if (pTacProfileEntry->u1FilterAddOrDelete == (UINT1) TACM_TRUE)
            {
                TacPrfCreateHashTable (pTacProfileEntry);
            }

            pTacProfileEntry->u1FilterAddOrDelete = (UINT1) TACM_FALSE;
            pTacProfileEntry->u1RowStatus = (UINT1) TACM_ACTIVE;
            break;
        }
        case TACM_DESTROY:
        {
            if ((TacPrfDeleteProfile (pTacProfileEntry)) == TACM_FAILURE)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhSetFsTacMcastProfileStatus : "
                          "Profile deletion failed\r\n");
                return SNMP_FAILURE;
            }
            break;
        }
        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTacMcastProfileAction
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType

                The Object 
                testValFsTacMcastProfileAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacMcastProfileAction (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsTacMcastProfileId,
                                  INT4 i4FsTacMcastProfileAddrType,
                                  INT4 i4TestValFsTacMcastProfileAction)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    if ((i4TestValFsTacMcastProfileAction != TACM_PERMIT) &&
        (i4TestValFsTacMcastProfileAction != TACM_DENY))
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhTestv2FsTacMcastProfileAction : "
                  "Profile action should be either permit or deny\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    pTacProfileEntry = (tTacProfileEntry *) RBTreeGet (TACM_PROFILE_TREE,
                                                       (tRBElem *)
                                                       & TacProfileEntry);

    if ((pTacProfileEntry != NULL) &&
        (pTacProfileEntry->u1RowStatus == TACM_NOT_IN_SERVICE))
    {
        return SNMP_SUCCESS;
    }

    if (pTacProfileEntry == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhTestv2FsTacMcastProfileAction : "
                  "Profile does not exist\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }
    else
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhTestv2FsTacMcastProfileAction : "
                  "Profile entry is active\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsTacMcastProfileDescription
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType

                The Object 
                testValFsTacMcastProfileDescription
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsTacMcastProfileDescription
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsTacMcastProfileId,
     INT4 i4FsTacMcastProfileAddrType,
     tSNMP_OCTET_STRING_TYPE * pTestValFsTacMcastProfileDescription)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    if ((pTestValFsTacMcastProfileDescription->i4_Length < 0) ||
        (pTestValFsTacMcastProfileDescription->i4_Length > TACM_MAX_DESCR_LEN))
    {
        TACM_TRC_ARG1 (MGMT_TRC | ALL_FAILURE_TRC,
                       "nmhTestv2FsTacMcastProfileDescription : "
                       "Profile description should be within %d characters\r\n",
                       TACM_MAX_DESCR_LEN);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars
        (pTestValFsTacMcastProfileDescription->pu1_OctetList,
         pTestValFsTacMcastProfileDescription->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    pTacProfileEntry = (tTacProfileEntry *) RBTreeGet (TACM_PROFILE_TREE,
                                                       (tRBElem *)
                                                       & TacProfileEntry);

    if ((pTacProfileEntry != NULL) &&
        (pTacProfileEntry->u1RowStatus == TACM_NOT_IN_SERVICE))
    {
        return SNMP_SUCCESS;
    }

    if (pTacProfileEntry == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhTestv2FsTacMcastProfileDescription : "
                  "Profile does not exist\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }
    else
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhTestv2FsTacMcastProfileDescription : "
                  "Profile is active\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsTacMcastProfileStatus
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType

                The Object 
                testValFsTacMcastProfileStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacMcastProfileStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsTacMcastProfileId,
                                  INT4 i4FsTacMcastProfileAddrType,
                                  INT4 i4TestValFsTacMcastProfileStatus)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    /* If profile Id is 0 or if the address type is not valid, return invalid */
    if ((u4FsTacMcastProfileId == 0) ||
        ((i4FsTacMcastProfileAddrType != IPVX_ADDR_FMLY_IPV4) &&
         (i4FsTacMcastProfileAddrType != IPVX_ADDR_FMLY_IPV6)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    pTacProfileEntry = (tTacProfileEntry *) RBTreeGet (TACM_PROFILE_TREE,
                                                       (tRBElem *)
                                                       & TacProfileEntry);

    switch (i4TestValFsTacMcastProfileStatus)
    {
        case TACM_CREATE_AND_WAIT:
        case TACM_CREATE_AND_GO:
        {
            if (pTacProfileEntry != NULL)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhTestv2FsTacMcastProfileStatus : "
                          "Profile already exists\r\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TACM_NOT_IN_SERVICE:
        {
            if (pTacProfileEntry == NULL)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhTestv2FsTacMcastProfileStatus : "
                          "Profile does not exist\r\n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            }
            else if ((pTacProfileEntry->u4IntfRefCnt != 0) ||
                     (pTacProfileEntry->u4VlanRefCnt != 0))
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhTestv2FsTacMcastProfileStatus : "
                          "Profile has mappings in other modules\r\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TACM_ACTIVE:
        {
            if (pTacProfileEntry == NULL)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhTestv2FsTacMcastProfileStatus : "
                          "Profile does not exist\r\n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
        }
        case TACM_NOT_READY:
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        case TACM_DESTROY:
        {
            if ((pTacProfileEntry != NULL) &&
                ((pTacProfileEntry->u4IntfRefCnt != 0) ||
                 (pTacProfileEntry->u4VlanRefCnt != 0)))
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhTestv2FsTacMcastProfileStatus : "
                          "Profile has mappings in other modules\r\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        default:
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsTacMcastPrfFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTacMcastPrfFilterTable
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
                FsTacMcastPrfFilterGrpStartAddr
                FsTacMcastPrfFilterGrpEndAddr
                FsTacMcastPrfFilterSrcStartAddr
                FsTacMcastPrfFilterSrcEndAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsTacMcastPrfFilterTable
    (UINT4 u4FsTacMcastProfileId,
     INT4 i4FsTacMcastProfileAddrType,
     tSNMP_OCTET_STRING_TYPE *
     pFsTacMcastPrfFilterGrpStartAddr,
     tSNMP_OCTET_STRING_TYPE *
     pFsTacMcastPrfFilterGrpEndAddr,
     tSNMP_OCTET_STRING_TYPE *
     pFsTacMcastPrfFilterSrcStartAddr,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterSrcEndAddr)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;
    tTacPrfFilterEntry  TacPrfFilterEntry;
    UINT1               u1AddrLen = 0;

    if (i4FsTacMcastProfileAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = (UINT1) IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = (UINT1) IPVX_IPV6_ADDR_LEN;
    }

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhValidateIndexInstanceFsTacMcastPrfFilterTable : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&TacPrfFilterEntry, 0, sizeof (tTacPrfFilterEntry));
    MEMCPY (TacPrfFilterEntry.GrpStartAddr,
            pFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.GrpEndAddr,
            pFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcStartAddr,
            pFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcEndAddr,
            pFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList, u1AddrLen);

    if ((RBTreeGet (pTacProfileEntry->pPrfFilterRoot,
                    (tRBElem *) & TacPrfFilterEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhValidateIndexInstanceFsTacMcastPrfFilterTable : "
                  "Filter entry does not exist\r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTacMcastPrfFilterTable
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
                FsTacMcastPrfFilterGrpStartAddr
                FsTacMcastPrfFilterGrpEndAddr
                FsTacMcastPrfFilterSrcStartAddr
                FsTacMcastPrfFilterSrcEndAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsTacMcastPrfFilterTable
    (UINT4 *pu4FsTacMcastProfileId,
     INT4 *pi4FsTacMcastProfileAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterGrpStartAddr,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterGrpEndAddr,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterSrcStartAddr,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterSrcEndAddr)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacPrfFilterEntry *pTacPrfFilterEntry = NULL;
    UINT1               u1AddrLen = 0;

    pTacProfileEntry = (tTacProfileEntry *) RBTreeGetFirst (TACM_PROFILE_TREE);

    while (pTacProfileEntry != NULL)
    {
        pTacPrfFilterEntry = (tTacPrfFilterEntry *)
            RBTreeGetFirst (pTacProfileEntry->pPrfFilterRoot);

        if (pTacPrfFilterEntry != NULL)
        {
            if (pTacProfileEntry->u1AddressType == IPVX_ADDR_FMLY_IPV4)
            {
                u1AddrLen = IPVX_IPV4_ADDR_LEN;
            }
            else
            {
                u1AddrLen = IPVX_IPV6_ADDR_LEN;
            }

            *pu4FsTacMcastProfileId = pTacProfileEntry->u4ProfileId;
            *pi4FsTacMcastProfileAddrType = pTacProfileEntry->u1AddressType;
            MEMCPY (pFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList,
                    pTacPrfFilterEntry->GrpStartAddr, u1AddrLen);
            pFsTacMcastPrfFilterGrpStartAddr->i4_Length = (INT4) u1AddrLen;
            MEMCPY (pFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList,
                    pTacPrfFilterEntry->GrpEndAddr, u1AddrLen);
            pFsTacMcastPrfFilterGrpEndAddr->i4_Length = (INT4) u1AddrLen;
            MEMCPY (pFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList,
                    pTacPrfFilterEntry->SrcStartAddr, u1AddrLen);
            pFsTacMcastPrfFilterSrcStartAddr->i4_Length = (INT4) u1AddrLen;
            MEMCPY (pFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList,
                    pTacPrfFilterEntry->SrcEndAddr, u1AddrLen);
            pFsTacMcastPrfFilterSrcEndAddr->i4_Length = (INT4) u1AddrLen;

            return SNMP_SUCCESS;
        }

        pTacProfileEntry = (tTacProfileEntry *)
            RBTreeGetNext (TACM_PROFILE_TREE, pTacProfileEntry, NULL);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTacMcastPrfFilterTable
 Input       :  The Indices
                FsTacMcastProfileId
                nextFsTacMcastProfileId
                FsTacMcastProfileAddrType
                nextFsTacMcastProfileAddrType
                FsTacMcastPrfFilterGrpStartAddr
                nextFsTacMcastPrfFilterGrpStartAddr
                FsTacMcastPrfFilterGrpEndAddr
                nextFsTacMcastPrfFilterGrpEndAddr
                FsTacMcastPrfFilterSrcStartAddr
                nextFsTacMcastPrfFilterSrcStartAddr
                FsTacMcastPrfFilterSrcEndAddr
                nextFsTacMcastPrfFilterSrcEndAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsTacMcastPrfFilterTable
    (UINT4 u4FsTacMcastProfileId,
     UINT4 *pu4NextFsTacMcastProfileId,
     INT4 i4FsTacMcastProfileAddrType,
     INT4 *pi4NextFsTacMcastProfileAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterGrpStartAddr,
     tSNMP_OCTET_STRING_TYPE *
     pNextFsTacMcastPrfFilterGrpStartAddr,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterGrpEndAddr,
     tSNMP_OCTET_STRING_TYPE *
     pNextFsTacMcastPrfFilterGrpEndAddr,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterSrcStartAddr,
     tSNMP_OCTET_STRING_TYPE *
     pNextFsTacMcastPrfFilterSrcStartAddr,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterSrcEndAddr,
     tSNMP_OCTET_STRING_TYPE * pNextFsTacMcastPrfFilterSrcEndAddr)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;
    tTacPrfFilterEntry *pTacPrfFilterEntry = NULL;
    tTacPrfFilterEntry  TacPrfFilterEntry;
    UINT1               u1AddrLen = 0;

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    MEMSET (&TacPrfFilterEntry, 0, sizeof (tTacPrfFilterEntry));

    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    pTacProfileEntry = (tTacProfileEntry *)
        RBTreeGet (TACM_PROFILE_TREE, &TacProfileEntry);

    if (pTacProfileEntry != NULL)
    {
        if (pTacProfileEntry->u1AddressType == IPVX_ADDR_FMLY_IPV4)
        {
            u1AddrLen = IPVX_IPV4_ADDR_LEN;
        }
        else
        {
            u1AddrLen = IPVX_IPV6_ADDR_LEN;
        }

        MEMCPY (TacPrfFilterEntry.GrpStartAddr,
                pFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (TacPrfFilterEntry.GrpEndAddr,
                pFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (TacPrfFilterEntry.SrcStartAddr,
                pFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (TacPrfFilterEntry.SrcEndAddr,
                pFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList, u1AddrLen);

        pTacPrfFilterEntry = (tTacPrfFilterEntry *)
            RBTreeGetNext (pTacProfileEntry->pPrfFilterRoot,
                           &TacPrfFilterEntry, NULL);

        if (pTacPrfFilterEntry != NULL)
        {
            *pu4NextFsTacMcastProfileId = pTacProfileEntry->u4ProfileId;
            *pi4NextFsTacMcastProfileAddrType = pTacProfileEntry->u1AddressType;
            MEMCPY (pNextFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList,
                    pTacPrfFilterEntry->GrpStartAddr, u1AddrLen);
            pNextFsTacMcastPrfFilterGrpStartAddr->i4_Length = (INT4) u1AddrLen;
            MEMCPY (pNextFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList,
                    pTacPrfFilterEntry->GrpEndAddr, u1AddrLen);
            pNextFsTacMcastPrfFilterGrpEndAddr->i4_Length = (INT4) u1AddrLen;
            MEMCPY (pNextFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList,
                    pTacPrfFilterEntry->SrcStartAddr, u1AddrLen);
            pNextFsTacMcastPrfFilterSrcStartAddr->i4_Length = (INT4) u1AddrLen;
            MEMCPY (pNextFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList,
                    pTacPrfFilterEntry->SrcEndAddr, u1AddrLen);
            pNextFsTacMcastPrfFilterSrcEndAddr->i4_Length = (INT4) u1AddrLen;

            return SNMP_SUCCESS;
        }

    }

    pTacProfileEntry = (tTacProfileEntry *)
        RBTreeGetNext (TACM_PROFILE_TREE, &TacProfileEntry, NULL);

    while (pTacProfileEntry != NULL)
    {
        pTacPrfFilterEntry = (tTacPrfFilterEntry *)
            RBTreeGetFirst (pTacProfileEntry->pPrfFilterRoot);

        if (pTacPrfFilterEntry != NULL)
        {
            if (pTacProfileEntry->u1AddressType == IPVX_ADDR_FMLY_IPV4)
            {
                u1AddrLen = IPVX_IPV4_ADDR_LEN;
            }
            else
            {
                u1AddrLen = IPVX_IPV6_ADDR_LEN;
            }

            *pu4NextFsTacMcastProfileId = pTacProfileEntry->u4ProfileId;
            *pi4NextFsTacMcastProfileAddrType = pTacProfileEntry->u1AddressType;
            MEMCPY (pNextFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList,
                    pTacPrfFilterEntry->GrpStartAddr, u1AddrLen);
            pNextFsTacMcastPrfFilterGrpStartAddr->i4_Length = (INT4) u1AddrLen;
            MEMCPY (pNextFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList,
                    pTacPrfFilterEntry->GrpEndAddr, u1AddrLen);
            pNextFsTacMcastPrfFilterGrpEndAddr->i4_Length = (INT4) u1AddrLen;
            MEMCPY (pNextFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList,
                    pTacPrfFilterEntry->SrcStartAddr, u1AddrLen);
            pNextFsTacMcastPrfFilterSrcStartAddr->i4_Length = (INT4) u1AddrLen;
            MEMCPY (pNextFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList,
                    pTacPrfFilterEntry->SrcEndAddr, u1AddrLen);
            pNextFsTacMcastPrfFilterSrcEndAddr->i4_Length = (INT4) u1AddrLen;

            return SNMP_SUCCESS;
        }

        pTacProfileEntry = (tTacProfileEntry *)
            RBTreeGetNext (TACM_PROFILE_TREE, pTacProfileEntry, NULL);
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTacMcastPrfFilterMode
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
                FsTacMcastPrfFilterGrpStartAddr
                FsTacMcastPrfFilterGrpEndAddr
                FsTacMcastPrfFilterSrcStartAddr
                FsTacMcastPrfFilterSrcEndAddr

                The Object 
                retValFsTacMcastPrfFilterMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacMcastPrfFilterMode (UINT4 u4FsTacMcastProfileId,
                               INT4 i4FsTacMcastProfileAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTacMcastPrfFilterGrpStartAddr,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTacMcastPrfFilterGrpEndAddr,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTacMcastPrfFilterSrcStartAddr,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTacMcastPrfFilterSrcEndAddr,
                               INT4 *pi4RetValFsTacMcastPrfFilterMode)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;
    tTacPrfFilterEntry *pTacPrfFilterEntry = NULL;
    tTacPrfFilterEntry  TacPrfFilterEntry;
    UINT1               u1AddrLen = 0;

    if (i4FsTacMcastProfileAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhGetFsTacMcastPrfFilterMode : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&TacPrfFilterEntry, 0, sizeof (tTacPrfFilterEntry));
    MEMCPY (TacPrfFilterEntry.GrpStartAddr,
            pFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.GrpEndAddr,
            pFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcStartAddr,
            pFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcEndAddr,
            pFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList, u1AddrLen);

    if ((pTacPrfFilterEntry = (tTacPrfFilterEntry *)
         RBTreeGet (pTacProfileEntry->pPrfFilterRoot,
                    (tRBElem *) & TacPrfFilterEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhGetFsTacMcastPrfFilterMode : "
                  "Filter entry does not exist\r\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsTacMcastPrfFilterMode = (INT4) pTacPrfFilterEntry->u1FilterMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTacMcastPrfFilterStatus
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
                FsTacMcastPrfFilterGrpStartAddr
                FsTacMcastPrfFilterGrpEndAddr
                FsTacMcastPrfFilterSrcStartAddr
                FsTacMcastPrfFilterSrcEndAddr

                The Object 
                retValFsTacMcastPrfFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacMcastPrfFilterStatus (UINT4 u4FsTacMcastProfileId,
                                 INT4 i4FsTacMcastProfileAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsTacMcastPrfFilterGrpStartAddr,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsTacMcastPrfFilterGrpEndAddr,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsTacMcastPrfFilterSrcStartAddr,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsTacMcastPrfFilterSrcEndAddr,
                                 INT4 *pi4RetValFsTacMcastPrfFilterStatus)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;
    tTacPrfFilterEntry *pTacPrfFilterEntry = NULL;
    tTacPrfFilterEntry  TacPrfFilterEntry;
    UINT1               u1AddrLen = 0;

    if (i4FsTacMcastProfileAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhGetFsTacMcastPrfFilterStatus : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&TacPrfFilterEntry, 0, sizeof (tTacPrfFilterEntry));
    MEMCPY (TacPrfFilterEntry.GrpStartAddr,
            pFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.GrpEndAddr,
            pFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcStartAddr,
            pFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcEndAddr,
            pFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList, u1AddrLen);

    if ((pTacPrfFilterEntry = (tTacPrfFilterEntry *)
         RBTreeGet (pTacProfileEntry->pPrfFilterRoot,
                    (tRBElem *) & TacPrfFilterEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhGetFsTacMcastPrfFilterStatus : "
                  "Filter entry does not exist\r\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsTacMcastPrfFilterStatus =
        (INT4) pTacPrfFilterEntry->u1RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTacMcastPrfFilterMode
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
                FsTacMcastPrfFilterGrpStartAddr
                FsTacMcastPrfFilterGrpEndAddr
                FsTacMcastPrfFilterSrcStartAddr
                FsTacMcastPrfFilterSrcEndAddr

                The Object 
                setValFsTacMcastPrfFilterMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacMcastPrfFilterMode (UINT4 u4FsTacMcastProfileId,
                               INT4 i4FsTacMcastProfileAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTacMcastPrfFilterGrpStartAddr,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTacMcastPrfFilterGrpEndAddr,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTacMcastPrfFilterSrcStartAddr,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsTacMcastPrfFilterSrcEndAddr,
                               INT4 i4SetValFsTacMcastPrfFilterMode)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;
    tTacPrfFilterEntry *pTacPrfFilterEntry = NULL;
    tTacPrfFilterEntry  TacPrfFilterEntry;
    UINT1               u1AddrLen = 0;

    if (i4FsTacMcastProfileAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhSetFsTacMcastPrfFilterMode : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&TacPrfFilterEntry, 0, sizeof (tTacPrfFilterEntry));
    MEMCPY (TacPrfFilterEntry.GrpStartAddr,
            pFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.GrpEndAddr,
            pFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcStartAddr,
            pFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcEndAddr,
            pFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList, u1AddrLen);

    if ((pTacPrfFilterEntry = (tTacPrfFilterEntry *)
         RBTreeGet (pTacProfileEntry->pPrfFilterRoot,
                    (tRBElem *) & TacPrfFilterEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhSetFsTacMcastPrfFilterMode : "
                  "Filter entry does not exist\r\n");
        return SNMP_FAILURE;
    }

    pTacPrfFilterEntry->u1FilterMode = (UINT1) i4SetValFsTacMcastPrfFilterMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTacMcastPrfFilterStatus
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
                FsTacMcastPrfFilterGrpStartAddr
                FsTacMcastPrfFilterGrpEndAddr
                FsTacMcastPrfFilterSrcStartAddr
                FsTacMcastPrfFilterSrcEndAddr

                The Object 
                setValFsTacMcastPrfFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacMcastPrfFilterStatus (UINT4 u4FsTacMcastProfileId,
                                 INT4 i4FsTacMcastProfileAddrType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsTacMcastPrfFilterGrpStartAddr,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsTacMcastPrfFilterGrpEndAddr,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsTacMcastPrfFilterSrcStartAddr,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsTacMcastPrfFilterSrcEndAddr,
                                 INT4 i4SetValFsTacMcastPrfFilterStatus)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;
    tTacPrfFilterEntry *pTacPrfFilterEntry = NULL;
    tTacPrfFilterEntry  TacPrfFilterEntry;
    UINT1               u1AddrLen = 0;

    if (i4FsTacMcastProfileAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    pTacProfileEntry = (tTacProfileEntry *) RBTreeGet (TACM_PROFILE_TREE,
                                                       (tRBElem *)
                                                       & TacProfileEntry);

    if (pTacProfileEntry == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhSetFsTacMcastPrfFilterStatus : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&TacPrfFilterEntry, 0, sizeof (tTacPrfFilterEntry));
    MEMCPY (TacPrfFilterEntry.GrpStartAddr,
            pFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.GrpEndAddr,
            pFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcStartAddr,
            pFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcEndAddr,
            pFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList, u1AddrLen);

    pTacPrfFilterEntry = (tTacPrfFilterEntry *) RBTreeGet
        (pTacProfileEntry->pPrfFilterRoot, (tRBElem *) & TacPrfFilterEntry);

    switch (i4SetValFsTacMcastPrfFilterStatus)
    {
        case TACM_CREATE_AND_WAIT:
        case TACM_CREATE_AND_GO:
        {
            pTacPrfFilterEntry =
                TacPrfCreateFilter
                (pTacProfileEntry,
                 pFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList,
                 pFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList,
                 pFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList,
                 pFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList);

            if (pTacPrfFilterEntry == NULL)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhSetFsTacMcastPrfFilterStatus : "
                          "Filter creation failed\r\n");
                return SNMP_FAILURE;
            }

            if (i4SetValFsTacMcastPrfFilterStatus == TACM_CREATE_AND_WAIT)
            {
                pTacPrfFilterEntry->u1RowStatus = TACM_NOT_IN_SERVICE;
            }
            else
            {
                pTacPrfFilterEntry->u1RowStatus = TACM_ACTIVE;
            }
            break;
        }
        case TACM_NOT_IN_SERVICE:
        case TACM_ACTIVE:
        {
            if (pTacPrfFilterEntry == NULL)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhSetFsTacMcastPrfFilterStatus : "
                          "Filter does not exist\r\n");
                return SNMP_FAILURE;
            }

            pTacPrfFilterEntry->u1RowStatus =
                (UINT1) i4SetValFsTacMcastPrfFilterStatus;
            break;
        }
        case TACM_NOT_READY:
        {
            return SNMP_FAILURE;
        }
        case TACM_DESTROY:
        {
            if (pTacPrfFilterEntry != NULL)
            {
                TacPrfDeleteFilter (pTacProfileEntry, pTacPrfFilterEntry);
            }
            break;
        }
        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTacMcastPrfFilterMode
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
                FsTacMcastPrfFilterGrpStartAddr
                FsTacMcastPrfFilterGrpEndAddr
                FsTacMcastPrfFilterSrcStartAddr
                FsTacMcastPrfFilterSrcEndAddr

                The Object 
                testValFsTacMcastPrfFilterMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacMcastPrfFilterMode (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsTacMcastProfileId,
                                  INT4 i4FsTacMcastProfileAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsTacMcastPrfFilterGrpStartAddr,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsTacMcastPrfFilterGrpEndAddr,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsTacMcastPrfFilterSrcStartAddr,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsTacMcastPrfFilterSrcEndAddr,
                                  INT4 i4TestValFsTacMcastPrfFilterMode)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;
    tTacPrfFilterEntry *pTacPrfFilterEntry = NULL;
    tTacPrfFilterEntry  TacPrfFilterEntry;
    UINT1               u1AddrLen = 0;

    if ((i4TestValFsTacMcastPrfFilterMode != TACM_INCLUDE) &&
        (i4TestValFsTacMcastPrfFilterMode != TACM_EXCLUDE) &&
        (i4TestValFsTacMcastPrfFilterMode != TACM_ANY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsTacMcastProfileAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = (UINT1) IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = (UINT1) IPVX_IPV6_ADDR_LEN;
    }

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhTestv2FsTacMcastPrfFilterMode : "
                  "Profile does not exist\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    MEMSET (&TacPrfFilterEntry, 0, sizeof (tTacPrfFilterEntry));
    MEMCPY (TacPrfFilterEntry.GrpStartAddr,
            pFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.GrpEndAddr,
            pFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcStartAddr,
            pFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcEndAddr,
            pFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList, u1AddrLen);

    pTacPrfFilterEntry = (tTacPrfFilterEntry *) RBTreeGet
        (pTacProfileEntry->pPrfFilterRoot, (tRBElem *) & TacPrfFilterEntry);

    if ((pTacPrfFilterEntry != NULL) &&
        (pTacPrfFilterEntry->u1RowStatus == TACM_NOT_IN_SERVICE))
    {
        return SNMP_SUCCESS;
    }

    if (pTacPrfFilterEntry == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhTestv2FsTacMcastPrfFilterMode : "
                  "Filter entry does not exist\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }
    else
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhTestv2FsTacMcastPrfFilterMode : "
                  "Filter entry is active\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsTacMcastPrfFilterStatus
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
                FsTacMcastPrfFilterGrpStartAddr
                FsTacMcastPrfFilterGrpEndAddr
                FsTacMcastPrfFilterSrcStartAddr
                FsTacMcastPrfFilterSrcEndAddr

                The Object 
                testValFsTacMcastPrfFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsTacMcastPrfFilterStatus
    (UINT4 *pu4ErrorCode, UINT4 u4FsTacMcastProfileId,
     INT4 i4FsTacMcastProfileAddrType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterGrpStartAddr,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterGrpEndAddr,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterSrcStartAddr,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastPrfFilterSrcEndAddr,
     INT4 i4TestValFsTacMcastPrfFilterStatus)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;
    tTacPrfFilterEntry *pTacPrfFilterEntry = NULL;
    tTacPrfFilterEntry  TacPrfFilterEntry;
    UINT1               u1AddrLen = 0;

    if (i4FsTacMcastProfileAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    pTacProfileEntry = (tTacProfileEntry *)
        RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry);

    if (pTacProfileEntry == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhTestv2FsTacMcastPrfFilterStatus : "
                  "Profile does not exist\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (TACM_CLI_ERR_PRF_ACTIVE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsTacMcastPrfFilterStatus >= TACM_ACTIVE) &&
        (i4TestValFsTacMcastPrfFilterStatus <= TACM_DESTROY) &&
        (pTacProfileEntry->u1RowStatus != TACM_NOT_IN_SERVICE))
    {

        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhTestv2FsTacMcastPrfFilterStatus : "
                  "Profile is active\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (TACM_CLI_ERR_PRF_ACTIVE);

        return SNMP_FAILURE;
    }

    MEMSET (&TacPrfFilterEntry, 0, sizeof (tTacPrfFilterEntry));
    MEMCPY (TacPrfFilterEntry.GrpStartAddr,
            pFsTacMcastPrfFilterGrpStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.GrpEndAddr,
            pFsTacMcastPrfFilterGrpEndAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcStartAddr,
            pFsTacMcastPrfFilterSrcStartAddr->pu1_OctetList, u1AddrLen);
    MEMCPY (TacPrfFilterEntry.SrcEndAddr,
            pFsTacMcastPrfFilterSrcEndAddr->pu1_OctetList, u1AddrLen);

    pTacPrfFilterEntry = (tTacPrfFilterEntry *) RBTreeGet
        (pTacProfileEntry->pPrfFilterRoot, (tRBElem *) & TacPrfFilterEntry);

    switch (i4TestValFsTacMcastPrfFilterStatus)
    {
        case TACM_CREATE_AND_WAIT:
        case TACM_CREATE_AND_GO:
        {
            if (pTacPrfFilterEntry != NULL)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhTestv2FsTacMcastPrfFilterStatus : "
                          "Filter entry already exist\r\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (ValidateTacFilterConfig (&TacPrfFilterEntry,
                                         (UINT1) i4FsTacMcastProfileAddrType)
                == TACM_FALSE)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhTestv2FsTacMcastPrfFilterStatus : "
                          "Invalid filter configuration\r\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TACM_NOT_IN_SERVICE:
        case TACM_ACTIVE:
        {
            if (pTacPrfFilterEntry == NULL)
            {
                TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                          "nmhTestv2FsTacMcastPrfFilterStatus : "
                          "Filter entry does not exist\r\n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;
        }
        case TACM_NOT_READY:
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        case TACM_DESTROY:
        {
            break;
        }
        default:
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsTacMcastChannelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTacMcastChannelTable
 Input       :  The Indices
                FsTacMcastChannelAddressType
                FsTacMcastChannelGrpAddress
                FsTacMcastChannelSrcAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsTacMcastChannelTable
    (INT4 i4FsTacMcastChannelAddressType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelGrpAddress,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelSrcAddress)
{
    UNUSED_PARAM (i4FsTacMcastChannelAddressType);
    UNUSED_PARAM (pFsTacMcastChannelGrpAddress);
    UNUSED_PARAM (pFsTacMcastChannelSrcAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTacMcastChannelTable
 Input       :  The Indices
                FsTacMcastChannelAddressType
                FsTacMcastChannelGrpAddress
                FsTacMcastChannelSrcAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsTacMcastChannelTable
    (INT4 *pi4FsTacMcastChannelAddressType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelGrpAddress,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelSrcAddress)
{
    UNUSED_PARAM (pi4FsTacMcastChannelAddressType);
    UNUSED_PARAM (pFsTacMcastChannelGrpAddress);
    UNUSED_PARAM (pFsTacMcastChannelSrcAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTacMcastChannelTable
 Input       :  The Indices
                FsTacMcastChannelAddressType
                nextFsTacMcastChannelAddressType
                FsTacMcastChannelGrpAddress
                nextFsTacMcastChannelGrpAddress
                FsTacMcastChannelSrcAddress
                nextFsTacMcastChannelSrcAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsTacMcastChannelTable
    (INT4 i4FsTacMcastChannelAddressType,
     INT4 *pi4NextFsTacMcastChannelAddressType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelGrpAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsTacMcastChannelGrpAddress,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelSrcAddress,
     tSNMP_OCTET_STRING_TYPE * pNextFsTacMcastChannelSrcAddress)
{
    UNUSED_PARAM (i4FsTacMcastChannelAddressType);
    UNUSED_PARAM (pi4NextFsTacMcastChannelAddressType);
    UNUSED_PARAM (pFsTacMcastChannelGrpAddress);
    UNUSED_PARAM (pNextFsTacMcastChannelGrpAddress);
    UNUSED_PARAM (pFsTacMcastChannelSrcAddress);
    UNUSED_PARAM (pNextFsTacMcastChannelSrcAddress);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTacMcastChannelBandWidth
 Input       :  The Indices
                FsTacMcastChannelAddressType
                FsTacMcastChannelGrpAddress
                FsTacMcastChannelSrcAddress

                The Object 
                retValFsTacMcastChannelBandWidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsTacMcastChannelBandWidth
    (INT4 i4FsTacMcastChannelAddressType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelGrpAddress,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelSrcAddress,
     UINT4 *pu4RetValFsTacMcastChannelBandWidth)
{
    UNUSED_PARAM (i4FsTacMcastChannelAddressType);
    UNUSED_PARAM (pFsTacMcastChannelGrpAddress);
    UNUSED_PARAM (pFsTacMcastChannelSrcAddress);
    UNUSED_PARAM (pu4RetValFsTacMcastChannelBandWidth);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsTacMcastChannelRowStatus
 Input       :  The Indices
                FsTacMcastChannelAddressType
                FsTacMcastChannelGrpAddress
                FsTacMcastChannelSrcAddress

                The Object 
                retValFsTacMcastChannelRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsTacMcastChannelRowStatus
    (INT4 i4FsTacMcastChannelAddressType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelGrpAddress,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelSrcAddress,
     INT4 *pi4RetValFsTacMcastChannelRowStatus)
{
    UNUSED_PARAM (i4FsTacMcastChannelAddressType);
    UNUSED_PARAM (pFsTacMcastChannelGrpAddress);
    UNUSED_PARAM (pFsTacMcastChannelSrcAddress);
    UNUSED_PARAM (pi4RetValFsTacMcastChannelRowStatus);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTacMcastChannelBandWidth
 Input       :  The Indices
                FsTacMcastChannelAddressType
                FsTacMcastChannelGrpAddress
                FsTacMcastChannelSrcAddress

                The Object 
                setValFsTacMcastChannelBandWidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsTacMcastChannelBandWidth
    (INT4 i4FsTacMcastChannelAddressType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelGrpAddress,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelSrcAddress,
     UINT4 u4SetValFsTacMcastChannelBandWidth)
{
    UNUSED_PARAM (i4FsTacMcastChannelAddressType);
    UNUSED_PARAM (pFsTacMcastChannelGrpAddress);
    UNUSED_PARAM (pFsTacMcastChannelSrcAddress);
    UNUSED_PARAM (u4SetValFsTacMcastChannelBandWidth);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsTacMcastChannelRowStatus
 Input       :  The Indices
                FsTacMcastChannelAddressType
                FsTacMcastChannelGrpAddress
                FsTacMcastChannelSrcAddress

                The Object 
                setValFsTacMcastChannelRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsTacMcastChannelRowStatus
    (INT4 i4FsTacMcastChannelAddressType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelGrpAddress,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelSrcAddress,
     INT4 i4SetValFsTacMcastChannelRowStatus)
{
    UNUSED_PARAM (i4FsTacMcastChannelAddressType);
    UNUSED_PARAM (pFsTacMcastChannelGrpAddress);
    UNUSED_PARAM (pFsTacMcastChannelSrcAddress);
    UNUSED_PARAM (i4SetValFsTacMcastChannelRowStatus);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTacMcastChannelBandWidth
 Input       :  The Indices
                FsTacMcastChannelAddressType
                FsTacMcastChannelGrpAddress
                FsTacMcastChannelSrcAddress

                The Object 
                testValFsTacMcastChannelBandWidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsTacMcastChannelBandWidth
    (UINT4 *pu4ErrorCode, INT4 i4FsTacMcastChannelAddressType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelGrpAddress,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelSrcAddress,
     UINT4 u4TestValFsTacMcastChannelBandWidth)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsTacMcastChannelAddressType);
    UNUSED_PARAM (pFsTacMcastChannelGrpAddress);
    UNUSED_PARAM (pFsTacMcastChannelSrcAddress);
    UNUSED_PARAM (u4TestValFsTacMcastChannelBandWidth);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsTacMcastChannelRowStatus
 Input       :  The Indices
                FsTacMcastChannelAddressType
                FsTacMcastChannelGrpAddress
                FsTacMcastChannelSrcAddress

                The Object 
                testValFsTacMcastChannelRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsTacMcastChannelRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4FsTacMcastChannelAddressType,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelGrpAddress,
     tSNMP_OCTET_STRING_TYPE * pFsTacMcastChannelSrcAddress,
     INT4 i4TestValFsTacMcastChannelRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsTacMcastChannelAddressType);
    UNUSED_PARAM (pFsTacMcastChannelGrpAddress);
    UNUSED_PARAM (pFsTacMcastChannelSrcAddress);
    UNUSED_PARAM (i4TestValFsTacMcastChannelRowStatus);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsTacMcastPrfStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTacMcastPrfStatsTable
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsTacMcastPrfStatsTable
    (UINT4 u4FsTacMcastProfileId, INT4 i4FsTacMcastProfileAddrType)
{
    return (nmhValidateIndexInstanceFsTacMcastProfileTable
            (u4FsTacMcastProfileId, i4FsTacMcastProfileAddrType));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTacMcastPrfStatsTable
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsTacMcastPrfStatsTable
    (UINT4 *pu4FsTacMcastProfileId, INT4 *pi4FsTacMcastProfileAddrType)
{
    return (nmhGetFirstIndexFsTacMcastProfileTable
            (pu4FsTacMcastProfileId, pi4FsTacMcastProfileAddrType));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTacMcastPrfStatsTable
 Input       :  The Indices
                FsTacMcastProfileId
                nextFsTacMcastProfileId
                FsTacMcastProfileAddrType
                nextFsTacMcastProfileAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsTacMcastPrfStatsTable (UINT4 u4FsTacMcastProfileId,
                                        UINT4 *pu4NextFsTacMcastProfileId,
                                        INT4 i4FsTacMcastProfileAddrType,
                                        INT4 *pi4NextFsTacMcastProfileAddrType)
{
    return (nmhGetNextIndexFsTacMcastProfileTable
            (u4FsTacMcastProfileId, pu4NextFsTacMcastProfileId,
             i4FsTacMcastProfileAddrType, pi4NextFsTacMcastProfileAddrType));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTacMcastPrfStatsPortRefCnt
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType

                The Object 
                retValFsTacMcastPrfStatsRefCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsTacMcastPrfStatsPortRefCnt
    (UINT4 u4FsTacMcastProfileId,
     INT4 i4FsTacMcastProfileAddrType,
     UINT4 *pu4RetValFsTacMcastPrfStatsPortRefCnt)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhGetFsTacMcastPrfStatsPortRefCnt : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    *pu4RetValFsTacMcastPrfStatsPortRefCnt = pTacProfileEntry->u4IntfRefCnt;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTacMcastPrfStatsVlanRefCnt
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType

                The Object 
                retValFsTacMcastPrfStatsVlanRefCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacMcastPrfStatsVlanRefCnt (UINT4 u4FsTacMcastProfileId,
                                    INT4 i4FsTacMcastProfileAddrType,
                                    UINT4
                                    *pu4RetValFsTacMcastPrfStatsVlanRefCnt)
{
    tTacProfileEntry   *pTacProfileEntry = NULL;
    tTacProfileEntry    TacProfileEntry;

    MEMSET (&TacProfileEntry, 0, sizeof (tTacProfileEntry));
    TacProfileEntry.u4ProfileId = u4FsTacMcastProfileId;
    TacProfileEntry.u1AddressType = (UINT1) i4FsTacMcastProfileAddrType;

    if ((pTacProfileEntry = (tTacProfileEntry *)
         RBTreeGet (TACM_PROFILE_TREE, (tRBElem *) & TacProfileEntry)) == NULL)
    {
        TACM_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                  "nmhGetFsTacMcastPrfStatsPortRefCnt : "
                  "Profile does not exist\r\n");
        return SNMP_FAILURE;
    }

    *pu4RetValFsTacMcastPrfStatsVlanRefCnt = pTacProfileEntry->u4VlanRefCnt;

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ValidateTacFilterConfig                              */
/*                                                                           */
/* Description        : This function checks whether the filter configured   */
/*                      has valid source and group address range             */
/*                                                                           */
/* Input(s)           : pTacPrfFilterEntry -  Filter entry                   */
/*                      u1AddressType      -  Internet address type          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_TRUE/TACM_FALSE                                 */
/*****************************************************************************/
PRIVATE INT4
ValidateTacFilterConfig (tTacPrfFilterEntry * pTacPrfFilterEntry,
                         UINT1 u1AddressType)
{
    UINT4               u4GrpStartAddr = 0;
    UINT4               u4GrpEndAddr = 0;
    UINT1               au1NullIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1AddrLen = 0;

    TACM_TRC (TACM_FN_ENTRY, "ValidateTacFilterConfig Entered\r\n");

    MEMSET (&au1NullIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    /*  If the configured multicast address is not within the specific
     *  range, then return false
     */
    if (u1AddressType == (UINT1) IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = (UINT1) IPVX_IPV4_ADDR_LEN;
        MEMCPY (&u4GrpStartAddr, pTacPrfFilterEntry->GrpStartAddr, u1AddrLen);
        u4GrpStartAddr = OSIX_NTOHL (u4GrpStartAddr);

        if ((u4GrpStartAddr != 0) &&
            (!(TACM_IS_VALID_IPV4_MCAST_ADDRESS (u4GrpStartAddr))))
        {
            TACM_TRC (TACM_FILTER_TRC | MGMT_TRC,
                      "ValidateTacFilterConfig : "
                      "Invalid start Group address\r\n");
            CLI_SET_ERR (TACM_CLI_ERR_INV_MCAST_ADDR);
            return TACM_FALSE;
        }

        if (TACM_IS_RSVD_IPV4_MCAST_ADDRESS (u4GrpStartAddr))
        {
            TACM_TRC (TACM_FILTER_TRC | MGMT_TRC,
                      "ValidateTacFilterConfig : "
                      "Start Group address is a reserved multicast "
                      "address\r\n");
            CLI_SET_ERR (TACM_CLI_ERR_RSVD_MCAST_ADDR);
            return TACM_FALSE;
        }

        MEMCPY (&u4GrpEndAddr, pTacPrfFilterEntry->GrpEndAddr, u1AddrLen);
        u4GrpEndAddr = OSIX_NTOHL (u4GrpEndAddr);

        if ((u4GrpEndAddr != 0) &&
            (!(TACM_IS_VALID_IPV4_MCAST_ADDRESS (u4GrpEndAddr))))
        {
            TACM_TRC (TACM_FILTER_TRC | MGMT_TRC,
                      "ValidateTacFilterConfig : "
                      "Invalid end Group address\r\n");
            CLI_SET_ERR (TACM_CLI_ERR_INV_MCAST_ADDR);
            return TACM_FALSE;
        }

        if (TACM_IS_RSVD_IPV4_MCAST_ADDRESS (u4GrpEndAddr))
        {
            TACM_TRC (TACM_FILTER_TRC | MGMT_TRC,
                      "ValidateTacFilterConfig : "
                      "End Group address is a reserved multicast "
                      "address\r\n");
            CLI_SET_ERR (TACM_CLI_ERR_RSVD_MCAST_ADDR);
            return TACM_FALSE;
        }
    }
    else
    {
        u1AddrLen = (UINT1) IPVX_IPV6_ADDR_LEN;
    }

    /*  If the start group or source address is wild card (0.0.0.0), then
     *  end address should also be a wild card
     */
    if ((MEMCMP (pTacPrfFilterEntry->GrpStartAddr, au1NullIpAddr,
                 u1AddrLen) == 0) &&
        (MEMCMP (pTacPrfFilterEntry->GrpEndAddr, au1NullIpAddr,
                 u1AddrLen) != 0))
    {
        TACM_TRC (TACM_FILTER_TRC | MGMT_TRC,
                  "ValidateTacFilterConfig : Invalid !!! "
                  "Group end address should be a wild card if group "
                  "start address is a wild card\r\n");
        CLI_SET_ERR (TACM_CLI_ERR_WCARD_MCAST_ADDR);
        return TACM_FALSE;
    }

    if ((MEMCMP (pTacPrfFilterEntry->SrcStartAddr, au1NullIpAddr,
                 u1AddrLen) == 0) &&
        (MEMCMP (pTacPrfFilterEntry->SrcEndAddr, au1NullIpAddr,
                 u1AddrLen) != 0))
    {
        TACM_TRC (TACM_FILTER_TRC | MGMT_TRC,
                  "ValidateTacFilterConfig : Invalid !!! "
                  "Source end address should be a wild card if Source "
                  "start address is a wild card\r\n");
        CLI_SET_ERR (TACM_CLI_ERR_WCARD_SRC_ADDR);
        return TACM_FALSE;
    }

    if (u1AddressType == (UINT1) IPVX_ADDR_FMLY_IPV4)
    {
        /* If the end address is less than the start address, return false */
        if (u4GrpStartAddr > u4GrpEndAddr)
        {
            TACM_TRC (TACM_FILTER_TRC | MGMT_TRC,
                      "ValidateTacFilterConfig : Invalid !!! "
                      "Group end address is less than group start address\r\n");
            CLI_SET_ERR (TACM_CLI_ERR_INV_MCAST_RANGE);
            return TACM_FALSE;
        }

        if (MEMCMP (pTacPrfFilterEntry->SrcStartAddr,
                    pTacPrfFilterEntry->SrcEndAddr, u1AddrLen) > 0)
        {
            TACM_TRC (TACM_FILTER_TRC | MGMT_TRC,
                      "ValidateTacFilterConfig : Invalid !!! "
                      "Source end address is less than Source start "
                      "address\r\n");
            CLI_SET_ERR (TACM_CLI_ERR_INV_SRC_RANGE);
            return TACM_FALSE;
        }
    }

    TACM_TRC (TACM_FN_EXIT, "ValidateTacFilterConfig Exited\r\n");

    return TACM_TRUE;
}

/****************************************************************************
 Function    :  nmhDepv2FsTacMcastChannelDefaultBandwidth
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacMcastChannelDefaultBandwidth (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTacTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacTraceOption (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTacStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTacMcastProfileTable
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacMcastProfileTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTacMcastPrfFilterTable
 Input       :  The Indices
                FsTacMcastProfileId
                FsTacMcastProfileAddrType
                FsTacMcastPrfFilterGrpStartAddr
                FsTacMcastPrfFilterGrpEndAddr
                FsTacMcastPrfFilterSrcStartAddr
                FsTacMcastPrfFilterSrcEndAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacMcastPrfFilterTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTacMcastChannelTable
 Input       :  The Indices
                FsTacMcastChannelAddressType
                FsTacMcastChannelGrpAddress
                FsTacMcastChannelSrcAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacMcastChannelTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
