/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: taccli.c,v 1.23 2015/02/10 10:11:00 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : taccli.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC CLI Module                                 */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains CLI routines used for       */
/*                            TAC Module                                     */
/*---------------------------------------------------------------------------*/

#ifndef __TACCLI_C__
#define __TACCLI_C__

#include "tacinc.h"
#include "cli.h"
#include "taccli.h"
#include "snmputil.h"
#include "fstaccli.h"

PRIVATE INT4        TacCliSetProfileEntry
PROTO ((tCliHandle CliHandle, INT4 i4ProfileStatus,
        UINT4 u4ProfileId, UINT1 *pu1Description, UINT1 u1AddressType));

PRIVATE INT4        TacCliSetProfileAction
PROTO ((tCliHandle CliHandle, UINT4 u4ProfileId, UINT1 u1AddressType,
        UINT1 u1Action));

PRIVATE INT4        TacCliSetPrfFilter
PROTO ((tCliHandle CliHandle, UINT4 u4ProfileId,
        UINT1 u1AddressType, INT4 i4PrfFilterStatus,
        UINT1 *pu1GrpStartAddr, UINT1 *pu1GrpEndAddr,
        UINT1 *pu1SrcStartAddr, UINT1 *pu1SrcEndAddr, UINT1 u1FilterMode));

PRIVATE INT4        TacCliSetProfileStatus
PROTO ((tCliHandle CliHandle, UINT4 u4ProfileId, UINT1 u1AddressType,
        INT4 i4ProfileStstus));

PRIVATE INT4        TacCliShowMcastProfile
PROTO ((tCliHandle CliHandle, UINT1 u1DisplayFormat,
        UINT4 u4ProfileId, UINT1 u1AddressType));

PRIVATE INT4        TacCliShowFilterInfo
PROTO ((tCliHandle CliHandle, UINT1 u1DisplayFormat,
        UINT4 u4ShowProfileId, UINT1 u1AddressType));

PRIVATE INT4        TacCliShowPrfStatsInfo
PROTO ((tCliHandle CliHandle, UINT1 u1DisplayFormat,
        UINT4 u4ShowProfileId, UINT1 u1AddressType));

PRIVATE VOID        TacCliConvertIpToStr
PROTO ((CHR1 * pu1DestIpAddr, UINT1 *pu1SrcIpAddr, UINT1 u1AddressType));

PRIVATE INT4        TacCliSetDebug
PROTO ((tCliHandle CliHandle, UINT1 u1DebugStatus, UINT4 u4DebugOption));

PRIVATE INT4        TacCliShowRunningConfigFilters
PROTO ((tCliHandle CliHandle, UINT4 u4ProfileId, INT4 i4AddressType));

/*****************************************************************************/
/* Function Name      : cli_process_tacm_cmd                                 */
/*                                                                           */
/* Description        : This function takes in variable no. of arguments     */
/*                      and process the commands                             */
/*                                                                           */
/* Input(s)           : CliHandle -  CLIHandler                              */
/*                      u4Command -  Command Identifier                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
cli_process_tacm_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *pu1Args[TACM_CLI_MAX_ARGS];
    UINT4               u4ErrCode = 0;
    UINT4               u4ProfileId = 0;
    INT4                i4RetStat = CLI_SUCCESS;
    UINT1               u1AddressType = 0;
    INT1                i1ArgNo = 0;

    CliRegisterLock (CliHandle, TacLock, TacUnLock);

    TACM_LOCK ();

    CLI_SET_ERR (0);
    va_start (ap, u4Command);

    /* The third argument in CLI process command is always interface index */
    va_arg (ap, UINT4);

    /*  Walk through the rest of the arguements and store in args array */
    MEMSET (pu1Args, 0, TACM_CLI_MAX_ARGS);
    while (1)
    {
        pu1Args[i1ArgNo++] = va_arg (ap, UINT1 *);
        if (i1ArgNo == TACM_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    switch (u4Command)
    {
        case TACM_CLI_IP_PROFILE:
        {
            i4RetStat = TacCliSetProfileEntry
                (CliHandle, TACM_PROFILE_CREATE,
                 *(UINT4 *) (VOID *) pu1Args[0], pu1Args[1],
                 (UINT1) CLI_PTR_TO_U4 (pu1Args[2]));
            break;
        }
        case TACM_CLI_NO_IP_PROFILE:
        {
            i4RetStat = TacCliSetProfileEntry
                (CliHandle, TACM_PROFILE_DELETE,
                 *(UINT4 *) (VOID *) pu1Args[0], NULL,
                 (UINT1) CLI_PTR_TO_U4 (pu1Args[1]));
            break;
        }
        case TACM_CLI_STATUS:
        {
            i4RetStat =
                TacCliSetStatus (CliHandle, (INT4) CLI_PTR_TO_U4 (pu1Args[0]));
            break;
        }
        case TACM_CLI_PROFILE_ACTION:
        {
            u4ProfileId = CLI_GET_TACM_PROFILE_ID ();
            u1AddressType = (UINT1) CLI_GET_TACM_ADDRESS_TYPE ();
            i4RetStat = TacCliSetProfileAction
                (CliHandle, u4ProfileId, u1AddressType,
                 (UINT1) CLI_PTR_TO_U4 (pu1Args[0]));
            break;
        }
        case TACM_CLI_FILTER:
        {
            u4ProfileId = CLI_GET_TACM_PROFILE_ID ();
            u1AddressType = (UINT1) CLI_GET_TACM_ADDRESS_TYPE ();
            i4RetStat = TacCliSetPrfFilter
                (CliHandle, u4ProfileId, u1AddressType,
                 TACM_FILTER_CREATE,
                 pu1Args[0], pu1Args[1],
                 pu1Args[2], pu1Args[3], (UINT1) CLI_PTR_TO_U4 (pu1Args[4]));
            break;
        }
        case TACM_CLI_NO_FILTER:
        {
            u4ProfileId = CLI_GET_TACM_PROFILE_ID ();
            u1AddressType = (UINT1) CLI_GET_TACM_ADDRESS_TYPE ();
            i4RetStat = TacCliSetPrfFilter
                (CliHandle, u4ProfileId, u1AddressType,
                 TACM_FILTER_DELETE,
                 pu1Args[0], pu1Args[1], pu1Args[2], pu1Args[3], 0);
            break;
        }
        case TACM_CLI_PROFILE_ACTIVE:
        {
            u4ProfileId = CLI_GET_TACM_PROFILE_ID ();
            u1AddressType = (UINT1) CLI_GET_TACM_ADDRESS_TYPE ();
            i4RetStat = TacCliSetProfileStatus (CliHandle, u4ProfileId,
                                                u1AddressType, TACM_ACTIVE);
            break;
        }
        case TACM_CLI_NO_PROFILE_ACTIVE:
        {
            u4ProfileId = CLI_GET_TACM_PROFILE_ID ();
            u1AddressType = (UINT1) CLI_GET_TACM_ADDRESS_TYPE ();
            i4RetStat = TacCliSetProfileStatus (CliHandle, u4ProfileId,
                                                u1AddressType,
                                                TACM_NOT_IN_SERVICE);
            break;
        }
        case TACM_CLI_SHOW_PROFILE:
        {
            if (pu1Args[1] == NULL)
            {
                u4ProfileId = 0;
            }
            else
            {
                u4ProfileId = *(UINT4 *) (VOID *) pu1Args[1];
            }

            i4RetStat = TacCliShowMcastProfile
                (CliHandle,
                 (UINT1) CLI_PTR_TO_U4 (pu1Args[0]),
                 u4ProfileId, (UINT1) CLI_PTR_TO_U4 (pu1Args[2]));
            break;
        }
        case TACM_CLI_DEBUG:
        {
            i4RetStat = TacCliSetDebug (CliHandle,
                                        (UINT1) TACM_CLI_ENABLE_DEBUG,
                                        CLI_PTR_TO_U4 (pu1Args[0]));
            break;
        }
        case TACM_CLI_NO_DEBUG:
        {
            i4RetStat = TacCliSetDebug (CliHandle,
                                        (UINT1) TACM_CLI_DISABLE_DEBUG,
                                        CLI_PTR_TO_U4 (pu1Args[0]));
            break;
        }
        default:
        {
            CliPrintf (CliHandle, "%% Invalid command\r\n");
            break;
        }
    }

    if ((i4RetStat == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < TACM_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s", gapc1TacCliErrorString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS ((UINT4) i4RetStat);

    TACM_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return i4RetStat;
}

/*****************************************************************************/
/* Function Name      : TacCliSetProfileEntry                               */
/*                                                                           */
/* Description        : This function creates, deletes or updates a profile  */
/*                      entry                                                */
/*                                                                           */
/* Input(s)           : CliHandle       - Cli Context                        */
/*                      i4ProfileStatus - TACM_PROFILE_CREATE or             */
/*                                        TACM_PROFILE_DELETE                */
/*                      u4ProfileId     - Profile Identifier                 */
/*                      pu1Description  - Profile Description                */
/*                      u1AddressType   - Internet address Type              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
TacCliSetProfileEntry (tCliHandle CliHandle, INT4 i4ProfileStatus,
                       UINT4 u4ProfileId, UINT1 *pu1Description,
                       UINT1 u1AddressType)
{
    tSNMP_OCTET_STRING_TYPE prfDescription;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;
    CHR1                ac1Cmd[MAX_PROMPT_LEN];

    MEMSET (ac1Cmd, 0, MAX_PROMPT_LEN);

    if (pu1Description != NULL)
    {
        MEMSET (&prfDescription, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        prfDescription.pu1_OctetList = pu1Description;
        prfDescription.i4_Length = (INT4) STRLEN (pu1Description);
    }

    if (i4ProfileStatus == TACM_PROFILE_CREATE)
    {

        /*  If the profile entry is already created, set the status as
         *  not in service else create a new entry and set the status as
         *  not in service
         */
        if (nmhValidateIndexInstanceFsTacMcastProfileTable
            (u4ProfileId, (INT4) u1AddressType) == SNMP_SUCCESS)
        {
            i4RowStatus = TACM_NOT_IN_SERVICE;
        }
        else
        {
            i4RowStatus = TACM_CREATE_AND_WAIT;
        }
    }
    else
    {
        i4RowStatus = TACM_DESTROY;
    }

    if (nmhTestv2FsTacMcastProfileStatus (&u4ErrCode, u4ProfileId,
                                          (INT4) u1AddressType,
                                          i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% !!! Profile is mapped to some port or vlan "
                   "in other modules\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTacMcastProfileStatus (u4ProfileId, (INT4) u1AddressType,
                                       i4RowStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (pu1Description == NULL)
    {
        if (i4ProfileStatus == TACM_PROFILE_CREATE)
        {
            /*  Set the profile id and address type in CLI context structure
             *  and change the config mode
             */
            SNPRINTF (ac1Cmd, MAX_PROMPT_LEN, "%s%u %u", TACM_CLI_MODE_PROFILE,
                      u4ProfileId, u1AddressType);
            CliChangePath ((CONST CHR1 *) ac1Cmd);
        }
        return CLI_SUCCESS;
    }

    if (nmhTestv2FsTacMcastProfileDescription (&u4ErrCode, u4ProfileId,
                                               (INT4) u1AddressType,
                                               &prfDescription) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% !!! Profile Entry is active\r\n");

        if (nmhSetFsTacMcastProfileStatus (u4ProfileId, (INT4) u1AddressType,
                                           TACM_DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        return CLI_FAILURE;
    }

    if (nmhSetFsTacMcastProfileDescription (u4ProfileId, (INT4) u1AddressType,
                                            &prfDescription) == SNMP_FAILURE)
    {
        if (nmhSetFsTacMcastProfileStatus (u4ProfileId, (INT4) u1AddressType,
                                           TACM_DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (i4ProfileStatus == TACM_PROFILE_CREATE)
    {
        /*  Set the profile id and address type in CLI context structure
         *  and change the config mode
         */
        SNPRINTF (ac1Cmd, MAX_PROMPT_LEN, "%s%u %u", TACM_CLI_MODE_PROFILE,
                  u4ProfileId, u1AddressType);
        CliChangePath ((CONST CHR1 *) ac1Cmd);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacCliSetProfileAction                              */
/*                                                                           */
/* Description        : This function sets the action for the particular     */
/*                      profile                                              */
/*                                                                           */
/* Input(s)           : CliHandle       - Cli Context                        */
/*                      u4ProfileId     - Profile Identifier                 */
/*                      u1AddressType   - Internet Address Type              */
/*                      u1Action        - Profile Action                     */
/*                                        TACM_PERMIT or TACM_DENY           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
PRIVATE INT4
TacCliSetProfileAction (tCliHandle CliHandle, UINT4 u4ProfileId,
                        UINT1 u1AddressType, UINT1 u1Action)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsTacMcastProfileAction (&u4ErrCode, u4ProfileId,
                                          u1AddressType, u1Action)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% !!! Profile Entry is active\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTacMcastProfileAction (u4ProfileId, u1AddressType,
                                       u1Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacCliSetPrfFilter                                  */
/*                                                                           */
/* Description        : This function adds or remove or updates a filter     */
/*                                                                           */
/* Input(s)           : CliHandle          - Cli Context                     */
/*                      u4ProfileId        - Profile Identifier              */
/*                      u1AddressType      - Internet Address Type           */
/*                      i4PrfFilterStatus  - create or delete                */
/*                      pu1GrpStartAddr    - Group Start address             */
/*                      pu1GrpEndAddr      - Group end address               */
/*                      pu1SrcStartAddr    - Source start address            */
/*                      pu1SrcEndAddr      - Source end address              */
/*                      u1FilterMode       - Filter mode                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
PRIVATE INT4
TacCliSetPrfFilter (tCliHandle CliHandle, UINT4 u4ProfileId,
                    UINT1 u1AddressType, INT4 i4PrfFilterStatus,
                    UINT1 *pu1GrpStartAddr, UINT1 *pu1GrpEndAddr,
                    UINT1 *pu1SrcStartAddr, UINT1 *pu1SrcEndAddr,
                    UINT1 u1FilterMode)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;
    tSNMP_OCTET_STRING_TYPE *pGrpStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcEndAddr = NULL;
    UINT1               au1NullIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1AddrLen = IPVX_IPV6_ADDR_LEN;

    MEMSET (au1NullIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    if (u1AddressType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = (UINT1) IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = (UINT1) IPVX_IPV6_ADDR_LEN;
    }

    if (pu1GrpStartAddr != NULL)
    {
        /* Specific group address configured */
        pGrpStartAddr = SNMP_AGT_FormOctetString (pu1GrpStartAddr, u1AddrLen);

        if (pu1GrpEndAddr != NULL)
        {
            pGrpEndAddr = SNMP_AGT_FormOctetString (pu1GrpEndAddr, u1AddrLen);
        }
        else
        {
            pGrpEndAddr = SNMP_AGT_FormOctetString (pu1GrpStartAddr, u1AddrLen);
        }
    }

    if (pGrpStartAddr == NULL)
    {
        free_octetstring (pGrpEndAddr);
        return CLI_FAILURE;
    }
    if (pGrpEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        return CLI_FAILURE;
    }

    if (pu1SrcStartAddr == NULL)
    {
        pSrcStartAddr = SNMP_AGT_FormOctetString (au1NullIpAddr, u1AddrLen);
        pSrcEndAddr = SNMP_AGT_FormOctetString (au1NullIpAddr, u1AddrLen);
    }
    else if (pu1SrcStartAddr != NULL)
    {
        /* Specific source address configured */
        pSrcStartAddr = SNMP_AGT_FormOctetString (pu1SrcStartAddr, u1AddrLen);
        if (pu1SrcEndAddr != NULL)
        {
            pSrcEndAddr = SNMP_AGT_FormOctetString (pu1SrcEndAddr, u1AddrLen);
        }
        else
        {
            pSrcEndAddr = SNMP_AGT_FormOctetString (pu1SrcStartAddr, u1AddrLen);
        }
    }

    if ((pSrcStartAddr == NULL) || (pSrcEndAddr == NULL))
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        return CLI_FAILURE;
    }

    TAC_INET_NTOHL (pGrpStartAddr->pu1_OctetList, u1AddrLen);
    TAC_INET_NTOHL (pGrpEndAddr->pu1_OctetList, u1AddrLen);
    TAC_INET_NTOHL (pSrcStartAddr->pu1_OctetList, u1AddrLen);
    TAC_INET_NTOHL (pSrcEndAddr->pu1_OctetList, u1AddrLen);

    if (i4PrfFilterStatus == TACM_FILTER_CREATE)
    {
        /*  If the filter  entry is already created, set the status as
         *  not in service else create a new entry and set the status as
         *  not in service
         */
        if (nmhValidateIndexInstanceFsTacMcastPrfFilterTable
            (u4ProfileId, u1AddressType, pGrpStartAddr,
             pGrpEndAddr, pSrcStartAddr, pSrcEndAddr) == SNMP_FAILURE)
        {
            i4RowStatus = TACM_CREATE_AND_WAIT;
        }
        else
        {
            i4RowStatus = TACM_NOT_IN_SERVICE;
        }
    }
    else
    {
        i4RowStatus = TACM_DESTROY;
    }

    if (nmhTestv2FsTacMcastPrfFilterStatus
        (&u4ErrCode, u4ProfileId, u1AddressType, pGrpStartAddr,
         pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, i4RowStatus) == SNMP_FAILURE)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        return CLI_FAILURE;
    }

    if (nmhSetFsTacMcastPrfFilterStatus
        (u4ProfileId, u1AddressType, pGrpStartAddr,
         pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, i4RowStatus) == SNMP_FAILURE)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (i4PrfFilterStatus == TACM_FILTER_DELETE)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        return CLI_SUCCESS;
    }

    /* Set the filter mode */
    if (nmhTestv2FsTacMcastPrfFilterMode
        (&u4ErrCode, u4ProfileId, u1AddressType, pGrpStartAddr,
         pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, u1FilterMode) == SNMP_FAILURE)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        return CLI_FAILURE;
    }

    if (nmhSetFsTacMcastPrfFilterMode
        (u4ProfileId, u1AddressType, pGrpStartAddr,
         pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, u1FilterMode) == SNMP_FAILURE)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Set the filter entry as active */
    if (nmhTestv2FsTacMcastPrfFilterStatus
        (&u4ErrCode, u4ProfileId, u1AddressType, pGrpStartAddr,
         pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, TACM_ACTIVE) == SNMP_FAILURE)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        return CLI_FAILURE;
    }

    if (nmhSetFsTacMcastPrfFilterStatus
        (u4ProfileId, u1AddressType, pGrpStartAddr,
         pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, TACM_ACTIVE) == SNMP_FAILURE)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    free_octetstring (pGrpStartAddr);
    free_octetstring (pGrpEndAddr);
    free_octetstring (pSrcStartAddr);
    free_octetstring (pSrcEndAddr);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacCliSetProfileStatus                               */
/*                                                                           */
/* Description        : This function exits from the profile mode            */
/*                      While exiting the profile mode, the corresponding    */
/*                      profile enrty is set as active                       */
/*                                                                           */
/* Input(s)           : CliHandle          - Cli Context                     */
/*                      u4ProfileId        - Profile Identifier              */
/*                      u1AddressType      - Internet Address Type           */
/*                      i4ProfileStatus    - Profile status                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
PRIVATE INT4
TacCliSetProfileStatus (tCliHandle CliHandle, UINT4 u4ProfileId,
                        UINT1 u1AddressType, INT4 i4ProfileStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsTacMcastProfileStatus (&u4ErrCode, u4ProfileId,
                                          (INT4) u1AddressType,
                                          i4ProfileStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% !!! Profile is mapped to some port or vlan "
                   "in other modules\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTacMcastProfileStatus (u4ProfileId, (INT4) u1AddressType,
                                       i4ProfileStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : TacCliShowMcastProfile                               */
/*                                                                           */
/* Description        : This function displays the multicast profile(s)      */
/*                                                                           */
/* Input(s)           : CliHandle       - CLIHandler                         */
/*                      u1DisplayFormat - Display format                     */
/*                      u4ProfileId     - Profile Identifier                 */
/*                      u1AddressType   - Internet address type              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
PRIVATE INT4
TacCliShowMcastProfile (tCliHandle CliHandle, UINT1 u1DisplayFormat,
                        UINT4 u4ProfileId, UINT1 u1AddressType)
{
    if ((u1DisplayFormat == (UINT1) TACM_CLI_SHOW_FILTER) ||
        (u1DisplayFormat == (UINT1) TACM_CLI_SHOW_FILTER_PROFILE))
    {
        TacCliShowFilterInfo (CliHandle, u1DisplayFormat,
                              u4ProfileId, u1AddressType);
    }
    else
    {
        TacCliShowPrfStatsInfo (CliHandle, u1DisplayFormat,
                                u4ProfileId, u1AddressType);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacCliShowFilterInfo                                 */
/*                                                                           */
/* Description        : This function displays the filters present in all    */
/*                      the profiles or given profile for the given internet */
/*                      address type.                                        */
/*                                                                           */
/*                      The profile action will not be displayed if the      */
/*                      action is deny                                       */
/*                      Source range will not be displayed if it is a wild   */
/*                      card                                                 */
/*                      The filter mode will not be displayed if the mode    */
/*                      is 'any'                                             */
/*                                                                           */
/* Input(s)           : CliHandle       - CLIHandler                         */
/*                      u1DisplayFormat - Display format                     */
/*                      u4ProfileId     - Profile Identifier                 */
/*                      u1AddressType   - Internet address type              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
PRIVATE INT4
TacCliShowFilterInfo (tCliHandle CliHandle, UINT1 u1DisplayFormat,
                      UINT4 u4ShowProfileId, UINT1 u1AddressType)
{
    tSNMP_OCTET_STRING_TYPE *pGrpStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpNextStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpNextEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcNextStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcNextEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pProfileDescription = NULL;
    UINT4               u4ProfileId = 0;
    UINT4               u4NextProfileId = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4ProfileAction = TACM_DENY;
    INT4                i4FilterMode = TACM_ANY;
    UINT1               u1AddrLen = 0;
    CHR1                ac1IpAddr[IPVX_MAX_INET_ADDR_LEN];

    if (u1AddressType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }

    /* Allocate the memory for the octet string */

    pGrpStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pGrpStartAddr == NULL)
    {
        return CLI_FAILURE;
    }
    pGrpEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pGrpEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        return CLI_FAILURE;
    }

    pSrcStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pSrcStartAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        return CLI_FAILURE;
    }

    pSrcEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pSrcEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        return CLI_FAILURE;
    }
    pGrpNextStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pGrpNextStartAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        return CLI_FAILURE;
    }

    pGrpNextEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pGrpNextEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        free_octetstring (pGrpNextStartAddr);
        return CLI_FAILURE;
    }
    pSrcNextStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pSrcNextStartAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        free_octetstring (pGrpNextStartAddr);
        free_octetstring (pGrpNextEndAddr);
        return CLI_FAILURE;
    }
    pSrcNextEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pSrcNextEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        free_octetstring (pGrpNextStartAddr);
        free_octetstring (pGrpNextEndAddr);
        free_octetstring (pSrcNextStartAddr);
        return CLI_FAILURE;
    }
    pProfileDescription = allocmem_octetstring (TACM_MAX_DESCR_ARRAY_LEN);

    if (pProfileDescription == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        free_octetstring (pGrpNextStartAddr);
        free_octetstring (pGrpNextEndAddr);
        free_octetstring (pSrcNextEndAddr);
        free_octetstring (pSrcNextStartAddr);

        return CLI_FAILURE;
    }

    MEMSET (pGrpStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpStartAddr->i4_Length = 0;

    MEMSET (pGrpEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpEndAddr->i4_Length = 0;

    MEMSET (pSrcStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcStartAddr->i4_Length = 0;

    MEMSET (pSrcEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcEndAddr->i4_Length = 0;

    MEMSET (pGrpNextStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpNextStartAddr->i4_Length = 0;

    MEMSET (pGrpNextEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpNextEndAddr->i4_Length = 0;

    MEMSET (pSrcNextStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcNextStartAddr->i4_Length = 0;

    MEMSET (pSrcNextEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcNextEndAddr->i4_Length = 0;

    MEMSET (pProfileDescription->pu1_OctetList, 0, TACM_MAX_DESCR_ARRAY_LEN);
    pProfileDescription->i4_Length = 0;

    TacCliGetStatus (CliHandle);

    /*  If the display option is to show all the filters in a particular
     *  profile, get next is called with given profile id and rest as 0
     */
    if (u1DisplayFormat == (UINT1) TACM_CLI_SHOW_FILTER_PROFILE)
    {
        i4RetVal = nmhGetNextIndexFsTacMcastPrfFilterTable
            (u4ShowProfileId, &u4NextProfileId,
             i4AddrType, &i4NextAddrType,
             pGrpStartAddr, pGrpNextStartAddr,
             pGrpEndAddr, pGrpNextEndAddr,
             pSrcStartAddr, pSrcNextStartAddr, pSrcEndAddr, pSrcNextEndAddr);
    }
    else
    {
        i4RetVal = nmhGetFirstIndexFsTacMcastPrfFilterTable
            (&u4NextProfileId, &i4NextAddrType,
             pGrpNextStartAddr, pGrpNextEndAddr,
             pSrcNextStartAddr, pSrcNextEndAddr);
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpNextStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pGrpNextEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcNextStartAddr);
        free_octetstring (pSrcEndAddr);
        free_octetstring (pSrcNextEndAddr);
        free_octetstring (pProfileDescription);

        return CLI_SUCCESS;
    }

    do
    {
        if ((u1DisplayFormat == (UINT1) TACM_CLI_SHOW_FILTER_PROFILE) &&
            (u4NextProfileId != u4ShowProfileId))
        {
            break;
        }

        if (i4NextAddrType != (INT4) u1AddressType)
        {
            continue;
        }

        if (u4ProfileId != u4NextProfileId)
        {
            CliPrintf (CliHandle, "\r\nProfile %u", u4NextProfileId);

            MEMSET (pProfileDescription->pu1_OctetList, 0,
                    TACM_MAX_DESCR_ARRAY_LEN);
            pProfileDescription->i4_Length = 0;

            if (nmhGetFsTacMcastProfileDescription (u4NextProfileId,
                                                    i4NextAddrType,
                                                    pProfileDescription)
                == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, " %s",
                           pProfileDescription->pu1_OctetList);
            }

            CliPrintf (CliHandle, "\r\n");

            if (nmhGetFsTacMcastProfileAction (u4NextProfileId,
                                               i4NextAddrType,
                                               &i4ProfileAction)
                == SNMP_FAILURE)
            {
                if (u1DisplayFormat == (UINT1) TACM_CLI_SHOW_FILTER_PROFILE)
                {
                    free_octetstring (pGrpStartAddr);
                    free_octetstring (pGrpNextStartAddr);
                    free_octetstring (pGrpEndAddr);
                    free_octetstring (pGrpNextEndAddr);
                    free_octetstring (pSrcStartAddr);
                    free_octetstring (pSrcNextStartAddr);
                    free_octetstring (pSrcEndAddr);
                    free_octetstring (pSrcNextEndAddr);
                    free_octetstring (pProfileDescription);

                    return CLI_SUCCESS;
                }
                else
                {
                    continue;
                }
            }

            if (i4ProfileAction == TACM_PERMIT)
            {
                CliPrintf (CliHandle, "    permit\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "    deny\r\n");
            }
        }

        u4ProfileId = u4NextProfileId;
        i4AddrType = i4NextAddrType;

        MEMSET (pGrpStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
        MEMSET (pGrpEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
        MEMSET (pSrcStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
        MEMSET (pSrcEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);

        MEMCPY (pGrpStartAddr->pu1_OctetList,
                pGrpNextStartAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (pGrpEndAddr->pu1_OctetList,
                pGrpNextEndAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (pSrcStartAddr->pu1_OctetList,
                pSrcNextStartAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (pSrcEndAddr->pu1_OctetList,
                pSrcNextEndAddr->pu1_OctetList, u1AddrLen);

        pGrpStartAddr->i4_Length = pGrpNextStartAddr->i4_Length;
        pGrpEndAddr->i4_Length = pGrpNextEndAddr->i4_Length;
        pSrcStartAddr->i4_Length = pSrcNextStartAddr->i4_Length;
        pSrcEndAddr->i4_Length = pSrcNextStartAddr->i4_Length;

        if (nmhGetFsTacMcastPrfFilterMode
            (u4ProfileId, i4AddrType, pGrpStartAddr,
             pGrpEndAddr, pSrcStartAddr, pSrcEndAddr, &i4FilterMode)
            == SNMP_FAILURE)
        {
            continue;
        }

        CliPrintf (CliHandle, "    range ");

        MEMSET (ac1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
        TacCliConvertIpToStr (ac1IpAddr, pGrpStartAddr->pu1_OctetList,
                              u1AddressType);
        CliPrintf (CliHandle, "%s ", ac1IpAddr);

        MEMSET (ac1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
        TacCliConvertIpToStr (ac1IpAddr, pGrpEndAddr->pu1_OctetList,
                              u1AddressType);
        CliPrintf (CliHandle, "%s", ac1IpAddr);

        /* Display the source range if it is not a wild card */
        MEMSET (ac1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
        if ((MEMCMP (ac1IpAddr, pSrcStartAddr->pu1_OctetList, u1AddrLen)) != 0)
        {
            TacCliConvertIpToStr (ac1IpAddr, pSrcStartAddr->pu1_OctetList,
                                  u1AddressType);
            CliPrintf (CliHandle, " source %s ", ac1IpAddr);

            MEMSET (ac1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
            TacCliConvertIpToStr (ac1IpAddr, pSrcEndAddr->pu1_OctetList,
                                  u1AddressType);
            CliPrintf (CliHandle, "%s", ac1IpAddr);
        }

        if (i4FilterMode == TACM_INCLUDE)
        {
            CliPrintf (CliHandle, " mode include");
        }
        else if (i4FilterMode == TACM_EXCLUDE)
        {
            CliPrintf (CliHandle, " mode exclude");
        }

        CliPrintf (CliHandle, "\r\n");

    }
    while (nmhGetNextIndexFsTacMcastPrfFilterTable
           (u4ProfileId, &u4NextProfileId, i4AddrType, &i4NextAddrType,
            pGrpStartAddr, pGrpNextStartAddr, pGrpEndAddr, pGrpNextEndAddr,
            pSrcStartAddr, pSrcNextStartAddr, pSrcEndAddr, pSrcNextEndAddr)
           == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    free_octetstring (pGrpStartAddr);
    free_octetstring (pGrpNextStartAddr);
    free_octetstring (pGrpEndAddr);
    free_octetstring (pGrpNextEndAddr);
    free_octetstring (pSrcStartAddr);
    free_octetstring (pSrcNextStartAddr);
    free_octetstring (pSrcEndAddr);
    free_octetstring (pSrcNextEndAddr);
    free_octetstring (pProfileDescription);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacCliShowPrfStatsInfo                               */
/*                                                                           */
/* Description        : This function displays the statistics of all the     */
/*                      the profiles or given profile for the given internet */
/*                      address type.                                        */
/*                                                                           */
/*                      The statistic mentions the number of port-profile    */
/*                      mappings done across all the modules.                */
/*                                                                           */
/* Input(s)           : CliHandle       - CLIHandler                         */
/*                      u1DisplayFormat - Display format                     */
/*                      u4ProfileId     - Profile Identifier                 */
/*                      u1AddressType   - Internet address type              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
PRIVATE INT4
TacCliShowPrfStatsInfo (tCliHandle CliHandle,
                        UINT1 u1DisplayFormat,
                        UINT4 u4ShowProfileId, UINT1 u1AddressType)
{
    tSNMP_OCTET_STRING_TYPE *pProfileDescription = NULL;
    UINT4               u4ProfileId = 0;
    UINT4               u4NextProfileId = 0;
    UINT4               u4PortRefCnt = 0;
    UINT4               u4VlanRefCnt = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;

    pProfileDescription = allocmem_octetstring (TACM_MAX_DESCR_ARRAY_LEN);
    if (pProfileDescription == NULL)
    {
        return CLI_FAILURE;
    }

    MEMSET (pProfileDescription->pu1_OctetList, 0, TACM_MAX_DESCR_ARRAY_LEN);
    pProfileDescription->i4_Length = 0;

    /*  If the display option is to show all the filters in a particular
     *  profile, get next is called with given profile id and address type 0
     */
    if (u1DisplayFormat == (UINT1) TACM_CLI_SHOW_STATS_PROFILE)
    {
        i4RetVal = nmhGetNextIndexFsTacMcastPrfStatsTable
            (u4ShowProfileId, &u4NextProfileId, i4AddrType, &i4NextAddrType);
    }
    else
    {
        i4RetVal = nmhGetFirstIndexFsTacMcastPrfStatsTable
            (&u4NextProfileId, &i4NextAddrType);
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        free_octetstring (pProfileDescription);
        return CLI_SUCCESS;
    }

    do
    {
        if ((u1DisplayFormat == (UINT1) TACM_CLI_SHOW_STATS_PROFILE) &&
            (u4NextProfileId != u4ShowProfileId))
        {
            break;
        }

        if ((u4ProfileId != u4NextProfileId) &&
            (i4NextAddrType == (INT4) u1AddressType))
        {
            CliPrintf (CliHandle, "Profile %u", u4NextProfileId);

            MEMSET (pProfileDescription->pu1_OctetList, 0,
                    TACM_MAX_DESCR_ARRAY_LEN);
            pProfileDescription->i4_Length = 0;

            if (nmhGetFsTacMcastProfileDescription (u4NextProfileId,
                                                    i4NextAddrType,
                                                    pProfileDescription)
                == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, " %s",
                           pProfileDescription->pu1_OctetList);
            }

            CliPrintf (CliHandle, "\r\n");

        }

        u4ProfileId = u4NextProfileId;
        i4AddrType = i4NextAddrType;

        if (i4NextAddrType != (INT4) u1AddressType)
        {
            continue;
        }

        if (nmhGetFsTacMcastPrfStatsPortRefCnt (u4ProfileId,
                                                i4AddrType,
                                                &u4PortRefCnt) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsTacMcastPrfStatsVlanRefCnt (u4ProfileId,
                                                i4AddrType,
                                                &u4VlanRefCnt) == SNMP_FAILURE)
        {
            continue;
        }

        CliPrintf (CliHandle, "    Port Reference count %u\r\n", u4PortRefCnt);

        CliPrintf (CliHandle, "    Vlan Reference count %u\r\n", u4VlanRefCnt);

        CliPrintf (CliHandle, "\r\n");

    }
    while (nmhGetNextIndexFsTacMcastPrfStatsTable
           (u4ProfileId, &u4NextProfileId, i4AddrType,
            &i4NextAddrType) == SNMP_SUCCESS);

    free_octetstring (pProfileDescription);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacCliGetProfCfgPrompt                               */
/*                                                                           */
/* Description        : This function is used to change the CLI prompt       */
/*                      description and to store any data to be used         */
/*                      inside the mode                                      */
/*                                                                           */
/* Input(s)           : pi1ModeName - Command Mode                           */
/*                                                                           */
/* Output(s)          : pi1DispStr  - New Prompt                             */
/*                                                                           */
/* Return Value(s)    : TRUE or FALSE                                        */
/*****************************************************************************/
INT1
TacCliGetProfCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = 0;
    UINT4               u4ProfileId = 0;
    INT4                i4AddressType = 0;

    u4Len = STRLEN (TACM_CLI_MODE_PROFILE);

    /*  The first part if mode name contains the display string and
     *  second part contains the profile id followed by address tye
     */
    if (STRNCMP (pi1ModeName, TACM_CLI_MODE_PROFILE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;

    SSCANF ((CHR1 *) pi1ModeName, "%u %d", &u4ProfileId,
            (UINT4 *) &i4AddressType);

    CLI_SET_TACM_PROFILE_ID (u4ProfileId);
    CLI_SET_TACM_ADDRESS_TYPE (i4AddressType);

    STRNCPY (pi1DispStr, TACM_CLI_MODE_PROFILE, STRLEN(TACM_CLI_MODE_PROFILE));
    pi1DispStr[STRLEN(TACM_CLI_MODE_PROFILE)] = '\0';
    return TRUE;
}

/*****************************************************************************/
/* Function Name      : TacCliConvertIpToStr                                 */
/*                                                                           */
/* Description        : This function converts the given IP adress to        */
/*                      dotted form based on the internet adddress type      */
/*                                                                           */
/* Input(s)           : pu1SrcIpAddr  -  IPv4/IPv6 address                   */
/*                      u1AddressType -  Internet address type               */
/*                                                                           */
/* Output(s)          : pc1DestIpAddr -  IP address in dotted format         */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PRIVATE VOID
TacCliConvertIpToStr (CHR1 * pc1DestIpAddr, UINT1 *pu1SrcIpAddr,
                      UINT1 u1AddressType)
{
    if (u1AddressType == IPVX_ADDR_FMLY_IPV4)
    {
        SPRINTF (pc1DestIpAddr, "%d.%d.%d.%d", pu1SrcIpAddr[0],
                 pu1SrcIpAddr[1], pu1SrcIpAddr[2], pu1SrcIpAddr[3]);
    }
}

/*****************************************************************************/
/* Function Name      : TacCliSetDebug                                       */
/*                                                                           */
/* Description        : This function enables or disables the debug option   */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                      u1DebugStatus -  Enable or disable debug             */
/*                      u4DebugOption -  Debug options                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
PRIVATE INT4
TacCliSetDebug (tCliHandle CliHandle, UINT1 u1DebugStatus, UINT4 u4DebugOption)
{
    UINT4               u4Trace = 0;
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);

    nmhGetFsTacTraceOption (&u4Trace);

    if (u1DebugStatus == TACM_CLI_ENABLE_DEBUG)
    {
        u4Trace |= u4DebugOption;
    }
    else
    {
        u4Trace &= ~u4DebugOption;
    }

    if (nmhTestv2FsTacTraceOption (&u4ErrCode, u4Trace) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetFsTacTraceOption (u4Trace);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacShowRunningConfig                                 */
/*                                                                           */
/* Description        : Displays configurations done in TAC module           */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
INT4
TacShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE *pProfileDescription = NULL;
    UINT4               u4ProfileId = 0;
    UINT4               u4NextProfileId = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4AddressType = 0;
    INT4                i4NextAddressType = 0;
    INT4                i4ProfileAction = TACM_DENY;
    INT4                i4Status = 0;

    /* Allocate the memory for the octet string */
    pProfileDescription = allocmem_octetstring (TACM_MAX_DESCR_ARRAY_LEN);
    if (pProfileDescription == NULL)
    {
        return CLI_FAILURE;
    }

    MEMSET (pProfileDescription->pu1_OctetList, 0, TACM_MAX_DESCR_ARRAY_LEN);
    pProfileDescription->i4_Length = 0;

    /* Get the first profile from the profile Table */
    i4RetVal = nmhGetFirstIndexFsTacMcastProfileTable
        (&u4NextProfileId, &i4NextAddressType);

    if (i4RetVal == SNMP_FAILURE)
    {
        /* No Profile exists */
        free_octetstring (pProfileDescription);
        return CLI_SUCCESS;
    }

    do
    {
        u4ProfileId = u4NextProfileId;
        i4AddressType = i4NextAddressType;

        MEMSET (pProfileDescription->pu1_OctetList, 0,
                TACM_MAX_DESCR_ARRAY_LEN);
        pProfileDescription->i4_Length = 0;
        nmhGetFsTacMcastProfileDescription (u4ProfileId,
                                            i4AddressType, pProfileDescription);

        /* Get the profile actions */
        nmhGetFsTacMcastProfileAction (u4ProfileId, i4AddressType,
                                       &i4ProfileAction);

        /* Get the row status */
        nmhGetFsTacMcastProfileStatus (u4ProfileId, i4AddressType, &i4Status);

        if (i4AddressType == IPVX_ADDR_FMLY_IPV4)
        {
            CliPrintf (CliHandle, "ip mcast profile %u", u4ProfileId);
        }
        else
        {
            continue;
        }

        if (pProfileDescription->i4_Length != 0)
        {
            CliPrintf (CliHandle, " %s", pProfileDescription->pu1_OctetList);
        }

        CliPrintf (CliHandle, "\r\n");

        if (i4ProfileAction == TACM_PERMIT)
        {
            CliPrintf (CliHandle, "    permit\r\n");
        }

        /* Display the filter rules configured in this profile */
        TacCliShowRunningConfigFilters (CliHandle, u4ProfileId, i4AddressType);

        if (i4Status == TACM_ACTIVE)
        {
            CliPrintf (CliHandle, "    profile active\r\n");
        }

        CliPrintf (CliHandle, "!\r\n");

    }
    while (nmhGetNextIndexFsTacMcastProfileTable
           (u4ProfileId, &u4NextProfileId,
            i4AddressType, &i4NextAddressType) == SNMP_SUCCESS);

    free_octetstring (pProfileDescription);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacCliShowRunningConfigFilters                       */
/*                                                                           */
/* Description        : Displays configurations done in the given profile    */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                      u4ProfileId     - Profile Identifier                 */
/*                      i4AddressType   - Internet address type              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
PRIVATE INT4
TacCliShowRunningConfigFilters (tCliHandle CliHandle, UINT4 u4ProfileId,
                                INT4 i4AddressType)
{
    tSNMP_OCTET_STRING_TYPE *pGrpStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpNextStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpNextEndAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcNextStartAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcNextEndAddr = NULL;
    UINT4               u4NextProfileId = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4NextAddressType = 0;
    INT4                i4FilterMode = 0;
    UINT1               u1AddrLen = 0;
    CHR1                ac1IpAddr[IPVX_MAX_INET_ADDR_LEN];

    if (i4AddressType == IPVX_ADDR_FMLY_IPV4)
    {
        u1AddrLen = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        u1AddrLen = IPVX_IPV6_ADDR_LEN;
    }

    /* Allocate the memory for the octet string */
    pGrpStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pGrpStartAddr == NULL)
    {
        return CLI_FAILURE;
    }
    pGrpEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pGrpEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        return CLI_FAILURE;
    }

    pSrcStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pSrcStartAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        return CLI_FAILURE;
    }

    pSrcEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pSrcEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        return CLI_FAILURE;
    }
    pGrpNextStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pGrpNextStartAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        return CLI_FAILURE;
    }

    pGrpNextEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pGrpNextEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        free_octetstring (pGrpNextStartAddr);
        return CLI_FAILURE;
    }
    pSrcNextStartAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pSrcNextStartAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        free_octetstring (pGrpNextStartAddr);
        free_octetstring (pGrpNextEndAddr);
        return CLI_FAILURE;
    }
    pSrcNextEndAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);

    if (pSrcNextEndAddr == NULL)
    {
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcEndAddr);
        free_octetstring (pGrpNextStartAddr);
        free_octetstring (pGrpNextEndAddr);
        free_octetstring (pSrcNextStartAddr);
        return CLI_FAILURE;
    }

    MEMSET (pGrpStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpStartAddr->i4_Length = 0;

    MEMSET (pGrpEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpEndAddr->i4_Length = 0;

    MEMSET (pSrcStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcStartAddr->i4_Length = 0;

    MEMSET (pSrcEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcEndAddr->i4_Length = 0;

    MEMSET (pGrpNextStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpNextStartAddr->i4_Length = 0;

    MEMSET (pGrpNextEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pGrpNextEndAddr->i4_Length = 0;

    MEMSET (pSrcNextStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcNextStartAddr->i4_Length = 0;

    MEMSET (pSrcNextEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    pSrcNextEndAddr->i4_Length = 0;

    i4RetVal = nmhGetNextIndexFsTacMcastPrfFilterTable
        (u4ProfileId, &u4NextProfileId, i4AddressType,
         &i4NextAddressType, pGrpStartAddr,
         pGrpNextStartAddr, pGrpEndAddr, pGrpNextEndAddr,
         pSrcStartAddr, pSrcNextStartAddr, pSrcEndAddr, pSrcNextEndAddr);

    if (i4RetVal == SNMP_FAILURE)
    {
        /* No filter rules configured in this profile */
        free_octetstring (pGrpStartAddr);
        free_octetstring (pGrpNextStartAddr);
        free_octetstring (pGrpEndAddr);
        free_octetstring (pGrpNextEndAddr);
        free_octetstring (pSrcStartAddr);
        free_octetstring (pSrcNextStartAddr);
        free_octetstring (pSrcEndAddr);
        free_octetstring (pSrcNextEndAddr);
        return CLI_SUCCESS;
    }

    do
    {
        if ((u4ProfileId != u4NextProfileId) ||
            (i4AddressType != i4NextAddressType))
        {
            break;
        }

        u4ProfileId = u4NextProfileId;
        i4AddressType = i4NextAddressType;

        MEMSET (pGrpStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
        MEMSET (pGrpEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
        MEMSET (pSrcStartAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
        MEMSET (pSrcEndAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);

        MEMCPY (pGrpStartAddr->pu1_OctetList,
                pGrpNextStartAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (pGrpEndAddr->pu1_OctetList,
                pGrpNextEndAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (pSrcStartAddr->pu1_OctetList,
                pSrcNextStartAddr->pu1_OctetList, u1AddrLen);
        MEMCPY (pSrcEndAddr->pu1_OctetList,
                pSrcNextEndAddr->pu1_OctetList, u1AddrLen);

        pGrpStartAddr->i4_Length = pGrpNextStartAddr->i4_Length;
        pGrpEndAddr->i4_Length = pGrpNextEndAddr->i4_Length;
        pSrcStartAddr->i4_Length = pSrcNextStartAddr->i4_Length;
        pSrcEndAddr->i4_Length = pSrcNextStartAddr->i4_Length;

        nmhGetFsTacMcastPrfFilterMode
            (u4ProfileId, i4AddressType, pGrpStartAddr, pGrpEndAddr,
             pSrcStartAddr, pSrcEndAddr, &i4FilterMode);

        CliPrintf (CliHandle, "    range ");

        MEMSET (ac1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
        TacCliConvertIpToStr (ac1IpAddr, pGrpStartAddr->pu1_OctetList,
                              (UINT1) i4AddressType);
        CliPrintf (CliHandle, "%s", ac1IpAddr);

        if (MEMCMP (pGrpStartAddr->pu1_OctetList,
                    pGrpEndAddr->pu1_OctetList, u1AddrLen) != 0)
        {
            MEMSET (ac1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
            TacCliConvertIpToStr (ac1IpAddr, pGrpEndAddr->pu1_OctetList,
                                  (UINT1) i4AddressType);
            CliPrintf (CliHandle, " %s", ac1IpAddr);
        }

        /* Display the source range if it is not a wild card */
        MEMSET (ac1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
        if ((MEMCMP (ac1IpAddr, pSrcStartAddr->pu1_OctetList, u1AddrLen)) != 0)
        {
            TacCliConvertIpToStr (ac1IpAddr, pSrcStartAddr->pu1_OctetList,
                                  (UINT1) i4AddressType);
            CliPrintf (CliHandle, " source %s", ac1IpAddr);

            if (MEMCMP (pSrcStartAddr->pu1_OctetList,
                        pSrcEndAddr->pu1_OctetList, u1AddrLen) != 0)
            {
                MEMSET (ac1IpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
                TacCliConvertIpToStr (ac1IpAddr, pSrcEndAddr->pu1_OctetList,
                                      (UINT1) i4AddressType);
                CliPrintf (CliHandle, " %s", ac1IpAddr);
            }
        }

        if (i4FilterMode == TACM_INCLUDE)
        {
            CliPrintf (CliHandle, " filter-mode include");
        }
        else if (i4FilterMode == TACM_EXCLUDE)
        {
            CliPrintf (CliHandle, " filter-mode exclude");
        }

        CliPrintf (CliHandle, "\r\n");

    }
    while (nmhGetNextIndexFsTacMcastPrfFilterTable
           (u4ProfileId, &u4NextProfileId, i4AddressType,
            &i4NextAddressType, pGrpStartAddr, pGrpNextStartAddr,
            pGrpEndAddr, pGrpNextEndAddr, pSrcStartAddr, pSrcNextStartAddr,
            pSrcEndAddr, pSrcNextEndAddr) == SNMP_SUCCESS);

    free_octetstring (pGrpStartAddr);
    free_octetstring (pGrpNextStartAddr);
    free_octetstring (pGrpEndAddr);
    free_octetstring (pGrpNextEndAddr);
    free_octetstring (pSrcStartAddr);
    free_octetstring (pSrcNextStartAddr);
    free_octetstring (pSrcEndAddr);
    free_octetstring (pSrcNextEndAddr);

    return CLI_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*     FUNCTION NAME    : TacCliSetStatus                                   */
/*                                                                          */
/*     DESCRIPTION      : This function sets the TAC module status          */
/*                                                                          */
/*     INPUT            : i4LaStatus - TAC_ENABLED/TAC_DISABLED             */
/*                        CliHandle  - CLI Handler                          */
/*                                                                          */
/*     OUTPUT           : NONE                                              */
/*                                                                          */
/*     RETURNS          : Success/Failure                                   */
/*                                                                          */
/****************************************************************************/
INT4
TacCliSetStatus (tCliHandle CliHandle, INT4 i4TacStatus)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsTacStatus (&u4ErrCode, i4TacStatus) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsTacStatus (i4TacStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : TacCliGetStatus                                      */
/*                                                                           */
/* Description        : This function Shows the  Status of TAC Moduletion    */
/*                                                                           */
/* Input(s)           : CliHandle    -  CLIHandler                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_FAILURE/CLI_SUCCESS                              */
/*****************************************************************************/
INT4
TacCliGetStatus (tCliHandle CliHandle)
{
    INT4                i4TacStatus = 0;

    if (nmhGetFsTacStatus (&i4TacStatus) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (i4TacStatus == TAC_ENABLED)
    {
        CliPrintf (CliHandle, "\r\nIGMP Profiling Module is : %-12s\n",
                   "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nIGMP Profiling Module is : %-12s\n",
                   "Disabled");
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : TacCliShowDebugging                                  */
/*                                                                           */
/* Description        : Prints the debug level    */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                           */
/*****************************************************************************/

VOID
TacCliShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4DbgLevel = 0;

    nmhGetFsTacTraceOption (&u4DbgLevel);

    if (u4DbgLevel == 0)
    {
        return;
    }
    CliPrintf (CliHandle, "\rTAC :");
    if ((u4DbgLevel & TACM_FN_ENTRY) != 0)
    {
        CliPrintf (CliHandle, "\r\n  TAC function entry debugging is on");
    }
    if ((u4DbgLevel & TACM_FN_EXIT) != 0)
    {
        CliPrintf (CliHandle, "\r\n  TAC function exit debugging is on");
    }
    if ((u4DbgLevel & TACM_FILTER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  TAC filter debugging is on");
    }
    if ((u4DbgLevel & TACM_CRITICAL_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  TAC Critical debugging is on");
    }
    if ((u4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  TAC init and shutdown debugging is on");
    }
    if ((u4DbgLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  TAC management debugging is on");
    }
    if ((u4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  TAC control plane debugging is on");
    }
    if ((u4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  TAC resource debugging is on");
    }
    if ((u4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  TAC failure debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
    return;
}

#endif
