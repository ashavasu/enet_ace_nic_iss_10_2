/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: tacutil.c,v 1.5 2010/12/23 06:22:38 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : tacutil.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC UTIL Module                                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains util functions used for     */
/*                          : TAC Module                                     */
/*---------------------------------------------------------------------------*/

#include "tacinc.h"

/*****************************************************************************/
/* Function Name      : TacUtilComparePrfInfo                                */
/*                                                                           */
/* Description        : Comparison function for profile entries              */
/*                      Index: Profile Id and Address type                   */
/*                                                                           */
/* Input(s)           : pRBElemFirst  - Pointer to Profile info              */
/*                      pRBElemSecond - Pointer to Profile info              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : TACM_LESSER   - pRBElemFirst is less                 */
/*                      TACM_GREATER  - pRBElemFirst is greater              */
/*                      TACM_EQUAL    - Both are equal                       */
/*****************************************************************************/
INT4
TacUtilComparePrfInfo (tRBElem * pRBElemFirst, tRBElem * pRBElemSecond)
{
    tTacProfileEntry   *pTacProfileEntryFirst =
        (tTacProfileEntry *) pRBElemFirst;
    tTacProfileEntry   *pTacProfileEntrySecond =
        (tTacProfileEntry *) pRBElemSecond;

    if (pTacProfileEntryFirst->u4ProfileId <
        pTacProfileEntrySecond->u4ProfileId)
    {
        return TACM_LESSER;
    }
    else if (pTacProfileEntryFirst->u4ProfileId >
             pTacProfileEntrySecond->u4ProfileId)
    {
        return TACM_GREATER;
    }

    if (pTacProfileEntryFirst->u1AddressType <
        pTacProfileEntrySecond->u1AddressType)
    {
        return TACM_LESSER;
    }
    else if (pTacProfileEntryFirst->u1AddressType >
             pTacProfileEntrySecond->u1AddressType)
    {
        return TACM_GREATER;
    }

    return TACM_EQUAL;
}

/*****************************************************************************/
/* Function Name      : TacUtilComparePrfFilterInfo                          */
/*                                                                           */
/* Description        : Comparison function for filter entries               */
/*                      Index: Group start address, Group end address        */
/*                             Source start address, Source end address      */
/*                                                                           */
/* Input(s)           : pRBElemFirst  - Pointer to filter info               */
/*                      pRBElemSecond - Pointer to filter info               */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : TACM_LESSER   - pRBElemFirst is less                 */
/*                      TACM_GREATER  - pRBElemFirst is greater              */
/*                      TACM_EQUAL    - Both are equal                       */
/*****************************************************************************/
INT4
TacUtilComparePrfFilterInfo (tRBElem * pRBElemFirst, tRBElem * pRBElemSecond)
{
    tTacPrfFilterEntry *pTacProfileEntryFirst =
        (tTacPrfFilterEntry *) pRBElemFirst;
    tTacPrfFilterEntry *pTacProfileEntrySecond =
        (tTacPrfFilterEntry *) pRBElemSecond;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pTacProfileEntryFirst->GrpStartAddr,
                       pTacProfileEntrySecond->GrpStartAddr,
                       IPVX_MAX_INET_ADDR_LEN);

    if (i4RetVal < 0)
    {
        return TACM_LESSER;
    }
    else if (i4RetVal > 0)
    {
        return TACM_GREATER;
    }

    i4RetVal = MEMCMP (pTacProfileEntryFirst->GrpEndAddr,
                       pTacProfileEntrySecond->GrpEndAddr,
                       IPVX_MAX_INET_ADDR_LEN);

    if (i4RetVal < 0)
    {
        return TACM_LESSER;
    }
    else if (i4RetVal > 0)
    {
        return TACM_GREATER;
    }

    i4RetVal = MEMCMP (pTacProfileEntryFirst->SrcStartAddr,
                       pTacProfileEntrySecond->SrcStartAddr,
                       IPVX_MAX_INET_ADDR_LEN);

    if (i4RetVal < 0)
    {
        return TACM_LESSER;
    }
    else if (i4RetVal > 0)
    {
        return TACM_GREATER;
    }

    i4RetVal = MEMCMP (pTacProfileEntryFirst->SrcEndAddr,
                       pTacProfileEntrySecond->SrcEndAddr,
                       IPVX_MAX_INET_ADDR_LEN);

    if (i4RetVal < 0)
    {
        return TACM_LESSER;
    }
    else if (i4RetVal > 0)
    {
        return TACM_GREATER;
    }

    return TACM_EQUAL;
}

/*****************************************************************************/
/* Function Name      : TacUtilFreePrfFilter                                 */
/*                                                                           */
/* Description        : This module is called while destroying the           */
/*                      filter RB Tree in a profile. This is used to         */
/*                      free the memory used for the filter                  */
/*                      After deleting each node from the RB Tree, this      */
/*                      call back function is triggered                      */
/*                                                                           */
/* Input(s)           : pRBElem       - Pointer to filter info               */
/*                      u4Arg         - Unused param                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS                                         */
/*****************************************************************************/
INT4
TacUtilFreePrfFilter (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    TACM_RELEASE_MEM_BLOCK (TACM_PRF_FILTER_MEMPOOL_ID, pRBElem);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacLock                                              */
/*                                                                           */
/* Description        : This function is to take the protocol lock for the   */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
TacLock (VOID)
{
    if (OsixSemTake (TACM_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacUnLock                                            */
/*                                                                           */
/* Description        : This function is to release the protocol lock for    */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
TacUnLock (VOID)
{
    OsixSemGive (TACM_SEM_ID);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacUtilGetNoOfHashBuckets                            */
/*                                                                           */
/* Description        : This function is get the number of hash buckets      */
/*                      to be used                                           */
/*                                                                           */
/* Input(s)           : u4NoOfFilters - Number of filter entries in profile  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : The number of Hash buckets required                  */
/*****************************************************************************/
UINT1
TacUtilGetNoOfHashBuckets (UINT4 u4NoOfFilters)
{
    UINT1               u1NoOfBuckets = 0;
    UINT4               u4Reminder = 0;
    UINT4               u4DivideValue = TACM_MAX_NODES_PER_BUCKET;

    TACM_TRC (TACM_FN_ENTRY, "TacUtilGetNoOfHashBuckets Entered\r\n");

    /*  The default number of nodes per bucket is defined by
     *  TACM_MAX_NODES_PER_BUCKET. The total number of hash buckets to be
     *  used is identified by dividing the total number of filters by
     *  the number of hash nodes per bucket.
     *  Note: The number of nodes per hash bucket is used to calculate the
     *        total number of buckets to be used such that the filter entries
     *        are equally distributed. Since a single filter entry can belong
     *        to more than one hash bucket, there can be scenarios where a
     *        single hash bucket can have more than the defined number of nodes
     *        per bucket. It is valid for a bucket to have more than
     *        TACM_MAX_NODES_PER_BUCKET
     */

    u1NoOfBuckets = (UINT1) (u4NoOfFilters / u4DivideValue);

    /*  If the number of filter is not an exact multiple of
     *  TACM_MAX_NODES_PER_BUCKET, add one to the number of buckets
     *  If the number of buckets to be used is greater than the maximum number
     *  of buckets defined by TACM_MAX_BUCKETS, then the maximum value is
     *  considered
     */
    u4Reminder = (UINT4) (u4NoOfFilters % u4DivideValue);
    if (u4Reminder != 0)
    {
        u1NoOfBuckets++;
    }

    if (u1NoOfBuckets > TACM_MAX_BUCKETS)
    {
        u1NoOfBuckets = TACM_MAX_BUCKETS;
    }

    TACM_TRC (TACM_FN_EXIT, "TacUtilGetNoOfHashBuckets Exited\r\n");

    return u1NoOfBuckets;
}

/*****************************************************************************/
/* Function Name      : TacUtilGetHashIndex                                  */
/*                                                                           */
/* Description        : This function is used to get the Hash bucket for     */
/*                      the given Group address. This Hash function          */
/*                      is applicable only for IPV4 multicast address        */
/*                                                                           */
/* Input(s)           : u1Total       - Number of buckets used               */
/*                      u4Minimum     - Minimum IPv4 multicast address       */
/*                                      configured in the profile            */
/*                      u4Maximum     - Maximum IPv4 multicast address       */
/*                                      configured in the profile            */
/*                      u4Value       - IPv4 multicast address for which     */
/*                                      hash index needs to be identified    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Hash Index                                           */
/*****************************************************************************/
UINT4
TacUtilGetHashIndex (UINT1 u1Total, UINT4 u4Minimum, UINT4 u4Maximum,
                     UINT4 u4Value)
{
    UINT4               u4OffSet = 0;    /* Contains the difference between
                                         * given value and minimum value  */
    UINT4               u4Range = 0;    /* (max - min)/buckets. This value
                                         * is used to compute the hash
                                         * index. This value gives the
                                         * range of value that are mapped
                                         * to hash bucket                 */
    UINT4               u4HashIndex = 0;    /* The Hash index value to be
                                             * returned                       */

    TACM_TRC (TACM_FN_ENTRY, "TacUtilGetHashIndex Entered\r\n");

    /*  Here, Hash bucket 0 is the default bucket. If the value to be hashed
     *  is 0 or greater than maximum or less than the minimum, then the
     *  index is the default bucket
     */
    if ((u4Value == 0) || (u4Value < u4Minimum) || (u4Value > u4Maximum))
    {
        TACM_TRC_ARG1 (TACM_FILTER_TRC, "TacUtilGetHashIndex : %x hashed "
                       "to bucket 0\r\n", u4Value);
        TACM_TRC (TACM_FN_EXIT, "TacUtilGetHashIndex Exited\r\n");
        return 0;
    }

    /*  Since the hash index is computed based on the difference between
     *  the value to be hashed and the minimum value, if the given value
     *  is equal to minimum, then the hash index will be 0 which is the
     *  default hash bucket. Since it should not be added to the default
     *  bucket, this is handled separately
     */
    if (u4Value == u4Minimum)
    {
        TACM_TRC_ARG1 (TACM_FILTER_TRC | MGMT_TRC | CONTROL_PLANE_TRC,
                       "TacUtilGetHashIndex : %x hashed "
                       "to bucket 1\r\n", u4Value);
        TACM_TRC (TACM_FN_EXIT, "TacUtilGetHashIndex Exited\r\n");
        return 1;
    }

    /*  Calculate the difference between the given value and minimum value
     *  configured in this profile
     */
    u4OffSet = u4Value - u4Minimum;

    /*  Calculate the range that are mapped to each bucket. Range is
     *  approximately equal to difference between the maximum and minimum
     *  value that is hashed to each bucket
     */
    u4Range = (u4Maximum - u4Minimum) / u1Total;

    /*  If the difference between the maximum and minimum is not an exact
     *  multiple of total, then add one to the range
     */
    if (((u4Maximum - u4Minimum) % u1Total) != 0)
    {
        u4Range++;
    }

    /* Calculate the hash index */
    u4HashIndex = u4OffSet / u4Range;

    /*  If the difference between the value to be hashed and the minimum
     *  value is not an exact multiple of range,
     *  add one to the hash index
     */
    if ((u4OffSet % u4Range) != 0)
    {
        u4HashIndex++;
    }

    TACM_TRC_ARG2 (TACM_FILTER_TRC | MGMT_TRC | CONTROL_PLANE_TRC,
                   "TacUtilGetHashIndex : %x hashed "
                   "to bucket %d\r\n", u4Value, u4HashIndex);

    TACM_TRC (TACM_FN_EXIT, "TacUtilGetHashIndex Exited\r\n");
    return u4HashIndex;
}

/*****************************************************************************/
/* Function Name      : TacUtilGetMinMaxGrpAddr                              */
/*                                                                           */
/* Description        : This function is used to get the minimum and the     */
/*                      maximum IPv4 multicast address configured for        */
/*                      the profile                                          */
/*                                                                           */
/* Input(s)           : pTacProfileEntry - Profile Entry                     */
/*                                                                           */
/* Output(s)          : pTacProfileEntry - Profile Entry. The minimum        */
/*                                         and maximum address is populated  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
TacUtilGetMinMaxGrpAddr (tTacProfileEntry * pTacProfileEntry)
{
    tTacPrfFilterEntry *pTacPrfFilterEntry = NULL;
    UINT4               u4MinGrpAddr = 0;
    UINT4               u4MaxGrpAddr = 0;
    UINT4               u4GrpStartAddr = 0;
    UINT4               u4GrpEndAddr = 0;

    TACM_TRC (TACM_FN_ENTRY, "TacUtilGetMinMaxGrpAddr Entered\r\n");

    /*  The wild card (0.0.0.0) is not considered in the calculation of
     *  minimum and maximum address
     */

    /* Scan the filters RB Tree and add the entries to the Hash Table */
    pTacPrfFilterEntry = (tTacPrfFilterEntry *)
        RBTreeGetFirst (pTacProfileEntry->pPrfFilterRoot);

    while (pTacPrfFilterEntry != NULL)
    {
        /*  Copy the start and end IPv4 multicast address and get the hash
         *  index
         */
        MEMCPY (&u4GrpStartAddr, pTacPrfFilterEntry->GrpStartAddr,
                IPVX_IPV4_ADDR_LEN);
        MEMCPY (&u4GrpEndAddr, pTacPrfFilterEntry->GrpEndAddr,
                IPVX_IPV4_ADDR_LEN);
        u4GrpStartAddr = OSIX_NTOHL (u4GrpStartAddr);
        u4GrpEndAddr = OSIX_NTOHL (u4GrpEndAddr);

        if (u4MinGrpAddr == 0)
        {
            u4MinGrpAddr = u4GrpStartAddr;
        }
        else
        {
            if ((u4GrpStartAddr != 0) && (u4GrpStartAddr < u4MinGrpAddr))
            {
                u4MinGrpAddr = u4GrpStartAddr;
            }
        }

        if (u4MaxGrpAddr == 0)
        {
            u4MaxGrpAddr = u4GrpEndAddr;
        }
        else
        {
            if ((u4GrpEndAddr != 0) && (u4GrpEndAddr > u4MaxGrpAddr))
            {
                u4MaxGrpAddr = u4GrpEndAddr;
            }
        }

        pTacPrfFilterEntry = (tTacPrfFilterEntry *)
            RBTreeGetNext (pTacProfileEntry->pPrfFilterRoot,
                           (tRBElem *) pTacPrfFilterEntry, NULL);
    }

    /* Update the minimum and maximum group address in the profile entry */
    pTacProfileEntry->u4MinGrpAddr = u4MinGrpAddr;
    pTacProfileEntry->u4MaxGrpAddr = u4MaxGrpAddr;

    TACM_TRC (TACM_FN_EXIT, "TacUtilGetMinMaxGrpAddr Exited\r\n");
}

/*****************************************************************************/
/* Function Name      : TacUtilAddHashNode                                   */
/*                                                                           */
/* Description        : This function adds a node in the Hash Table.         */
/*                      The nodes in each Hash bucket are added in the       */
/*                      increasing order of source start address             */
/*                                                                           */
/* Input(s)           : pTacProfileEntry   -  Profile Entry                  */
/*                      pTacPrfFilterEntry -  Filter entry                   */
/*                      u4HashIndex        -  Hash Index                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacUtilAddHashNode (tTacProfileEntry * pTacProfileEntry,
                    tTacPrfFilterEntry * pTacPrfFilterEntry, UINT4 u4HashIndex)
{
    tTacPrfFilterHashEntry *pTacPrfFilterHashEntry = NULL;
    tTacPrfFilterHashEntry *pScanTacHashList = NULL;
    tTacPrfFilterHashEntry *pPrevTacHashList = NULL;
    UINT4               u4SrcStartAddr = 0;    /*  Source start address
                                             *  of filter to be added
                                             */
    UINT4               u4SrcHashStartAddr = 0;    /*  Source start address
                                                 *  of filter in the hash
                                                 *  table
                                                 */

    TACM_TRC (TACM_FN_ENTRY, "TacUtilAddHashNode Entered\r\n");

    /* Allocate a memory block for the Hash list Node */
    pTacPrfFilterHashEntry = TACM_ALLOC_MEM_BLOCK
        (tTacPrfFilterHashEntry, TACM_PRF_FILTER_HASH_MEMPOOL_ID);

    if (pTacPrfFilterHashEntry == NULL)
    {
        /* All the blocks in the memory pools Id has been used */
        TACM_TRC (TACM_CRITICAL_TRC | TACM_FILTER_TRC | MGMT_TRC,
                  "TacUtilAddHashNode : Hash list memory exhausted\r\n");
        return TACM_FAILURE;
    }

    MEMSET (pTacPrfFilterHashEntry, 0, sizeof (tTacPrfFilterHashEntry));
    pTacPrfFilterHashEntry->pTacPrfFilterEntry = pTacPrfFilterEntry;

    /*  Scan the Hash bucket and insert the node in the increaing order
     *  of source start address
     */
    TMO_HASH_Scan_Bucket (pTacProfileEntry->pPrfFilterHashTable,
                          u4HashIndex, pScanTacHashList,
                          tTacPrfFilterHashEntry *)
    {
        MEMCPY (&u4SrcStartAddr, pTacPrfFilterEntry->SrcStartAddr,
                IPVX_IPV4_ADDR_LEN);
        MEMCPY (&u4SrcHashStartAddr,
                pScanTacHashList->pTacPrfFilterEntry->SrcStartAddr,
                IPVX_IPV4_ADDR_LEN);
        u4SrcStartAddr = OSIX_NTOHL (u4SrcStartAddr);
        u4SrcHashStartAddr = OSIX_NTOHL (u4SrcHashStartAddr);

        if (u4SrcHashStartAddr > u4SrcStartAddr)
        {
            break;
        }
        pPrevTacHashList = pScanTacHashList;
    }

    if (pPrevTacHashList == NULL)
    {
        TMO_HASH_Insert_Bucket (pTacProfileEntry->pPrfFilterHashTable,
                                u4HashIndex, NULL,
                                &(pTacPrfFilterHashEntry->PrfFilterHashNode));
    }
    else
    {
        TMO_HASH_Insert_Bucket (pTacProfileEntry->pPrfFilterHashTable,
                                u4HashIndex,
                                &(pPrevTacHashList->PrfFilterHashNode),
                                &(pTacPrfFilterHashEntry->PrfFilterHashNode));
    }

    TACM_TRC (TACM_FN_EXIT, "TacUtilAddHashNode Exited\r\n");

    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacUtilIsFilterMatch                                 */
/*                                                                           */
/* Description        : This function checks whether the given group,        */
/*                      source address and filter mode matches the filter    */
/*                      entry                                                */
/*                                                                           */
/* Input(s)           : pTacPrfFilterEntry -  Filter entry                   */
/*                      GroupAddr          -  Group address                  */
/*                      u4SrcAddr          -  source address                 */
/*                      u1Mode             -  Filter mode                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_TRUE/TACM_FALSE                                 */
/*****************************************************************************/
INT1
TacUtilIsFilterMatch (tTacPrfFilterEntry * pTacPrfFilterEntry,
                      tIPvXAddr GroupAddr, UINT4 u4SrcAddr, UINT1 u1Mode)
{
    UINT4               u4GrpStartAddr = 0;    /* Group start address  */
    UINT4               u4GrpEndAddr = 0;    /* Group end address    */
    UINT4               u4SrcStartAddr = 0;    /* Source start address */
    UINT4               u4SrcEndAddr = 0;    /* Source end address   */
    UINT4               u4GrpAddr = 0;    /* Given Group address  */

    TACM_TRC (TACM_FN_ENTRY, "TacUtilIsFilterMatch Entered\r\n");

    if (pTacPrfFilterEntry->u1RowStatus != TACM_ACTIVE)
    {
        return TACM_FALSE;
    }

    MEMCPY (&u4GrpStartAddr, pTacPrfFilterEntry->GrpStartAddr,
            IPVX_IPV4_ADDR_LEN);
    u4GrpStartAddr = OSIX_NTOHL (u4GrpStartAddr);
    MEMCPY (&u4GrpEndAddr, pTacPrfFilterEntry->GrpEndAddr, IPVX_IPV4_ADDR_LEN);
    u4GrpEndAddr = OSIX_NTOHL (u4GrpEndAddr);
    MEMCPY (&u4SrcStartAddr, pTacPrfFilterEntry->SrcStartAddr,
            IPVX_IPV4_ADDR_LEN);
    u4SrcStartAddr = OSIX_NTOHL (u4SrcStartAddr);
    MEMCPY (&u4SrcEndAddr, pTacPrfFilterEntry->SrcEndAddr, IPVX_IPV4_ADDR_LEN);
    u4SrcEndAddr = OSIX_NTOHL (u4SrcEndAddr);
    MEMCPY (&u4GrpAddr, GroupAddr.au1Addr, IPVX_IPV4_ADDR_LEN);

    if ((u4GrpStartAddr == 0) ||
        ((u4GrpAddr >= u4GrpStartAddr) && (u4GrpAddr <= u4GrpEndAddr)))
    {
        if ((u4SrcStartAddr == 0) ||
            ((u4SrcAddr >= u4SrcStartAddr) && (u4SrcAddr <= u4SrcEndAddr)))
        {
            if ((pTacPrfFilterEntry->u1FilterMode == TACM_ANY) ||
                (pTacPrfFilterEntry->u1FilterMode == u1Mode))
            {
                TACM_TRC (TACM_FN_EXIT, "TacUtilIsFilterMatch Exited\r\n");
                return TACM_TRUE;
            }
        }
    }

    TACM_TRC (TACM_FILTER_TRC | CONTROL_PLANE_TRC,
              "TacUtilIsFilterMatch : Match  not found\r\n");

    return TACM_FALSE;
}

/*****************************************************************************/
/* Function Name      : TacUtilSearchHashBucket                              */
/*                                                                           */
/* Description        : This function scans the hash bucket based on the     */
/*                      given hash index for a matching filter entry         */
/*                                                                           */
/* Input(s)           : pTacProfileEntry   -  Profile entry                  */
/*                      GroupAddr          -  Group address                  */
/*                      pu1SrcAddr         -  source address                 */
/*                      u4HashIndex        -  Hash BUcket index              */
/*                      u1Mode             -  Filter mode                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_TRUE/TACM_FALSE                                 */
/*****************************************************************************/
INT1
TacUtilSearchHashBucket (tTacProfileEntry * pTacProfileEntry,
                         tIPvXAddr GroupAddr,
                         UINT1 *pu1SrcAddr, UINT4 u4HashIndex, UINT1 u1Mode)
{
    tTacPrfFilterHashEntry *pTacPrfFilterHashEntry = NULL;
    UINT4               u4SrcStartAddr = 0;
    UINT4               u4SrcAddr = 0;
    INT1                i1MatchFound = TACM_FALSE;

    TACM_TRC (TACM_FN_ENTRY, "TacUtilSearchHashBucket Entered\r\n");

    /*  The Nodes in the buckets are stored in the increasing order
     *  of source start address. Hence search the list till the
     *  source start address is greater than the given source address
     */
    TMO_HASH_Scan_Bucket (pTacProfileEntry->pPrfFilterHashTable,
                          u4HashIndex, pTacPrfFilterHashEntry,
                          tTacPrfFilterHashEntry *)
    {
        MEMCPY (&u4SrcStartAddr,
                pTacPrfFilterHashEntry->pTacPrfFilterEntry->SrcStartAddr,
                IPVX_IPV4_ADDR_LEN);
        u4SrcStartAddr = OSIX_HTONL (u4SrcStartAddr);

        MEMCPY (&u4SrcAddr, pu1SrcAddr, IPVX_IPV4_ADDR_LEN);

        if (u4SrcStartAddr > u4SrcAddr)
        {
            break;
        }

        i1MatchFound =
            TacUtilIsFilterMatch (pTacPrfFilterHashEntry->pTacPrfFilterEntry,
                                  GroupAddr, u4SrcAddr, u1Mode);

        if (i1MatchFound == TACM_TRUE)
        {
            break;
        }
    }

    TACM_TRC (TACM_FN_EXIT, "TacUtilSearchHashBucket Exited\r\n");

    return i1MatchFound;
}
