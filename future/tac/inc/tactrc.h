/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* License Aricent Inc., 2008                                                */
/* $Id: tactrc.h,v 1.3 2008/06/04 15:34:07 iss Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : tactrc.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC module definitions                         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains trace related macros        */
/*                            and functions                                  */
/*---------------------------------------------------------------------------*/
#ifndef _TACTRC_H
#define _TACTRC_H

/* Module name */
#define TACM_NAME                   "TACM "

/* Trace variable */
#define TACM_TRACE                  gTacGlobalConfig.u4TacmTrace

/* Trace Related definitions */
#ifdef TRACE_WANTED
#define TACM_TRC(TraceValue, Fmt) \
        UtlTrcLog(TACM_TRACE, TraceValue, TACM_NAME, Fmt)

#define TACM_TRC_ARG1(TraceValue, Fmt, Arg1) \
        UtlTrcLog(TACM_TRACE, TraceValue, TACM_NAME, Fmt, Arg1)

#define TACM_TRC_ARG2(TraceValue, Fmt, Arg1, Arg2) \
        UtlTrcLog(TACM_TRACE, TraceValue, TACM_NAME, Fmt, Arg1, Arg2)

#define TACM_TRC_ARG3(TraceValue, Fmt, Arg1, Arg2, Arg3) \
        UtlTrcLog(TACM_TRACE, TraceValue, TACM_NAME, Fmt, Arg1, Arg2, Arg3)

#define TACM_TRC_ARG4(TraceValue, Fmt, Arg1, Arg2, Arg3, Arg4) \
        UtlTrcLog(TACM_TRACE, TraceValue, TACM_NAME, Fmt, Arg1, Arg2, Arg3, \
                  Arg4)

#define TACM_TRC_ARG5(TraceValue, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
        UtlTrcLog(TACM_TRACE, TraceValue, TACM_NAME, Fmt, Arg1, Arg2, Arg3, \
                  Arg4, Arg5)


#else
#define TACM_TRC(TraceValue, Fmt)
#define TACM_TRC_ARG1(TraceValue, Fmt, Arg1)
#define TACM_TRC_ARG2(TraceValue, Fmt, Arg1, Arg2)
#define TACM_TRC_ARG3(TraceValue, Fmt, Arg1, Arg2, Arg3)
#define TACM_TRC_ARG4(TraceValue, Fmt, Arg1, Arg2, Arg3, Arg4)
#define TACM_TRC_ARG5(TraceValue, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#endif

#endif
