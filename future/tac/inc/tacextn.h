/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: tacextn.h,v 1.4 2009/12/02 15:35:51 prabuc Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : tacextn.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC module externs                             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains external variables          */
/*                            for TAC module                                 */
/*---------------------------------------------------------------------------*/

#ifndef _TACEXTN_H
#define _TACEXTN_H

/* Global configurations */
extern tTacGlobalConfig gTacGlobalConfig;

/* Global memory pools */
extern tTacGlobalInfo   gTacGlobalInfo;

/* Sizing Params Structure */ 
extern tFsModSizingParams gFsTacSizingParams [];
extern tFsModSizingInfo gFsTacSizingInfo; 
#endif
