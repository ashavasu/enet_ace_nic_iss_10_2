/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstacdb.h,v 1.7 2010/05/19 07:14:35 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSTACDB_H
#define _FSTACDB_H

UINT1 FsTacMcastProfileTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsTacMcastPrfFilterTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsTacMcastChannelTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsTacMcastPrfStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fstac [] ={1,3,6,1,4,1,29601,2,8};
tSNMP_OID_TYPE fstacOID = {9, fstac};


UINT4 FsTacMcastChannelDefaultBandwidth [ ] ={1,3,6,1,4,1,29601,2,8,1,1};
UINT4 FsTacTraceOption [ ] ={1,3,6,1,4,1,29601,2,8,1,2};
UINT4 FsTacStatus [ ] ={1,3,6,1,4,1,29601,2,8,1,3};
UINT4 FsTacMcastProfileId [ ] ={1,3,6,1,4,1,29601,2,8,2,1,1,1};
UINT4 FsTacMcastProfileAddrType [ ] ={1,3,6,1,4,1,29601,2,8,2,1,1,2};
UINT4 FsTacMcastProfileAction [ ] ={1,3,6,1,4,1,29601,2,8,2,1,1,3};
UINT4 FsTacMcastProfileDescription [ ] ={1,3,6,1,4,1,29601,2,8,2,1,1,4};
UINT4 FsTacMcastProfileStatus [ ] ={1,3,6,1,4,1,29601,2,8,2,1,1,5};
UINT4 FsTacMcastPrfFilterGrpStartAddr [ ] ={1,3,6,1,4,1,29601,2,8,2,2,1,1};
UINT4 FsTacMcastPrfFilterGrpEndAddr [ ] ={1,3,6,1,4,1,29601,2,8,2,2,1,2};
UINT4 FsTacMcastPrfFilterSrcStartAddr [ ] ={1,3,6,1,4,1,29601,2,8,2,2,1,3};
UINT4 FsTacMcastPrfFilterSrcEndAddr [ ] ={1,3,6,1,4,1,29601,2,8,2,2,1,4};
UINT4 FsTacMcastPrfFilterMode [ ] ={1,3,6,1,4,1,29601,2,8,2,2,1,5};
UINT4 FsTacMcastPrfFilterStatus [ ] ={1,3,6,1,4,1,29601,2,8,2,2,1,6};
UINT4 FsTacMcastChannelAddressType [ ] ={1,3,6,1,4,1,29601,2,8,3,1,1,1};
UINT4 FsTacMcastChannelGrpAddress [ ] ={1,3,6,1,4,1,29601,2,8,3,1,1,2};
UINT4 FsTacMcastChannelSrcAddress [ ] ={1,3,6,1,4,1,29601,2,8,3,1,1,3};
UINT4 FsTacMcastChannelBandWidth [ ] ={1,3,6,1,4,1,29601,2,8,3,1,1,4};
UINT4 FsTacMcastChannelRowStatus [ ] ={1,3,6,1,4,1,29601,2,8,3,1,1,5};
UINT4 FsTacMcastPrfStatsPortRefCnt [ ] ={1,3,6,1,4,1,29601,2,8,4,1,1,1};
UINT4 FsTacMcastPrfStatsVlanRefCnt [ ] ={1,3,6,1,4,1,29601,2,8,4,1,1,2};


tMbDbEntry fstacMibEntry[]= {

{{11,FsTacMcastChannelDefaultBandwidth}, NULL, FsTacMcastChannelDefaultBandwidthGet, FsTacMcastChannelDefaultBandwidthSet, FsTacMcastChannelDefaultBandwidthTest, FsTacMcastChannelDefaultBandwidthDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "2000"},

{{11,FsTacTraceOption}, NULL, FsTacTraceOptionGet, FsTacTraceOptionSet, FsTacTraceOptionTest, FsTacTraceOptionDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsTacStatus}, NULL, FsTacStatusGet, FsTacStatusSet, FsTacStatusTest, FsTacStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsTacMcastProfileId}, GetNextIndexFsTacMcastProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsTacMcastProfileTableINDEX, 2, 0, 0, NULL},

{{13,FsTacMcastProfileAddrType}, GetNextIndexFsTacMcastProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsTacMcastProfileTableINDEX, 2, 0, 0, NULL},

{{13,FsTacMcastProfileAction}, GetNextIndexFsTacMcastProfileTable, FsTacMcastProfileActionGet, FsTacMcastProfileActionSet, FsTacMcastProfileActionTest, FsTacMcastProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTacMcastProfileTableINDEX, 2, 0, 0, "2"},

{{13,FsTacMcastProfileDescription}, GetNextIndexFsTacMcastProfileTable, FsTacMcastProfileDescriptionGet, FsTacMcastProfileDescriptionSet, FsTacMcastProfileDescriptionTest, FsTacMcastProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsTacMcastProfileTableINDEX, 2, 0, 0, NULL},

{{13,FsTacMcastProfileStatus}, GetNextIndexFsTacMcastProfileTable, FsTacMcastProfileStatusGet, FsTacMcastProfileStatusSet, FsTacMcastProfileStatusTest, FsTacMcastProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTacMcastProfileTableINDEX, 2, 0, 1, NULL},

{{13,FsTacMcastPrfFilterGrpStartAddr}, GetNextIndexFsTacMcastPrfFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsTacMcastPrfFilterTableINDEX, 6, 0, 0, NULL},

{{13,FsTacMcastPrfFilterGrpEndAddr}, GetNextIndexFsTacMcastPrfFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsTacMcastPrfFilterTableINDEX, 6, 0, 0, NULL},

{{13,FsTacMcastPrfFilterSrcStartAddr}, GetNextIndexFsTacMcastPrfFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsTacMcastPrfFilterTableINDEX, 6, 0, 0, NULL},

{{13,FsTacMcastPrfFilterSrcEndAddr}, GetNextIndexFsTacMcastPrfFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsTacMcastPrfFilterTableINDEX, 6, 0, 0, NULL},

{{13,FsTacMcastPrfFilterMode}, GetNextIndexFsTacMcastPrfFilterTable, FsTacMcastPrfFilterModeGet, FsTacMcastPrfFilterModeSet, FsTacMcastPrfFilterModeTest, FsTacMcastPrfFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTacMcastPrfFilterTableINDEX, 6, 0, 0, "3"},

{{13,FsTacMcastPrfFilterStatus}, GetNextIndexFsTacMcastPrfFilterTable, FsTacMcastPrfFilterStatusGet, FsTacMcastPrfFilterStatusSet, FsTacMcastPrfFilterStatusTest, FsTacMcastPrfFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTacMcastPrfFilterTableINDEX, 6, 0, 1, NULL},

{{13,FsTacMcastChannelAddressType}, GetNextIndexFsTacMcastChannelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsTacMcastChannelTableINDEX, 3, 0, 0, NULL},

{{13,FsTacMcastChannelGrpAddress}, GetNextIndexFsTacMcastChannelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsTacMcastChannelTableINDEX, 3, 0, 0, NULL},

{{13,FsTacMcastChannelSrcAddress}, GetNextIndexFsTacMcastChannelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsTacMcastChannelTableINDEX, 3, 0, 0, NULL},

{{13,FsTacMcastChannelBandWidth}, GetNextIndexFsTacMcastChannelTable, FsTacMcastChannelBandWidthGet, FsTacMcastChannelBandWidthSet, FsTacMcastChannelBandWidthTest, FsTacMcastChannelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsTacMcastChannelTableINDEX, 3, 0, 0, "2000"},

{{13,FsTacMcastChannelRowStatus}, GetNextIndexFsTacMcastChannelTable, FsTacMcastChannelRowStatusGet, FsTacMcastChannelRowStatusSet, FsTacMcastChannelRowStatusTest, FsTacMcastChannelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTacMcastChannelTableINDEX, 3, 0, 1, NULL},

{{13,FsTacMcastPrfStatsPortRefCnt}, GetNextIndexFsTacMcastPrfStatsTable, FsTacMcastPrfStatsPortRefCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsTacMcastPrfStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsTacMcastPrfStatsVlanRefCnt}, GetNextIndexFsTacMcastPrfStatsTable, FsTacMcastPrfStatsVlanRefCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsTacMcastPrfStatsTableINDEX, 2, 0, 0, NULL},
};
tMibData fstacEntry = { 21, fstacMibEntry };
#endif /* _FSTACDB_H */

