/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstaclw.h,v 1.6 2010/05/19 07:14:35 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTacMcastChannelDefaultBandwidth ARG_LIST((UINT4 *));

INT1
nmhGetFsTacTraceOption ARG_LIST((UINT4 *));

INT1
nmhGetFsTacStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTacMcastChannelDefaultBandwidth ARG_LIST((UINT4 ));

INT1
nmhSetFsTacTraceOption ARG_LIST((UINT4 ));

INT1
nmhSetFsTacStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTacMcastChannelDefaultBandwidth ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsTacTraceOption ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsTacStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTacMcastChannelDefaultBandwidth ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTacTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTacStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTacMcastProfileTable. */
INT1
nmhValidateIndexInstanceFsTacMcastProfileTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTacMcastProfileTable  */

INT1
nmhGetFirstIndexFsTacMcastProfileTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTacMcastProfileTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTacMcastProfileAction ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsTacMcastProfileDescription ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsTacMcastProfileStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTacMcastProfileAction ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsTacMcastProfileDescription ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsTacMcastProfileStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTacMcastProfileAction ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsTacMcastProfileDescription ARG_LIST((UINT4 *  ,UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsTacMcastProfileStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTacMcastProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTacMcastPrfFilterTable. */
INT1
nmhValidateIndexInstanceFsTacMcastPrfFilterTable ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsTacMcastPrfFilterTable  */

INT1
nmhGetFirstIndexFsTacMcastPrfFilterTable ARG_LIST((UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTacMcastPrfFilterTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTacMcastPrfFilterMode ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsTacMcastPrfFilterStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTacMcastPrfFilterMode ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsTacMcastPrfFilterStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTacMcastPrfFilterMode ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsTacMcastPrfFilterStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTacMcastPrfFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTacMcastChannelTable. */
INT1
nmhValidateIndexInstanceFsTacMcastChannelTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsTacMcastChannelTable  */

INT1
nmhGetFirstIndexFsTacMcastChannelTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTacMcastChannelTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTacMcastChannelBandWidth ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsTacMcastChannelRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTacMcastChannelBandWidth ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsTacMcastChannelRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTacMcastChannelBandWidth ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsTacMcastChannelRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTacMcastChannelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTacMcastPrfStatsTable. */
INT1
nmhValidateIndexInstanceFsTacMcastPrfStatsTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTacMcastPrfStatsTable  */

INT1
nmhGetFirstIndexFsTacMcastPrfStatsTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTacMcastPrfStatsTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTacMcastPrfStatsPortRefCnt ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsTacMcastPrfStatsVlanRefCnt ARG_LIST((UINT4  , INT4 ,UINT4 *));
