/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* License  Aricent Inc., 2008                                               */
/* $Id: tacprot.h,v 1.5 2010/05/19 07:14:35 prabuc Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : tacprot.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC module prototypes                          */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains function prototypes         */
/*                            for TAC module                                 */
/*---------------------------------------------------------------------------*/
#ifndef _TACPROT_H
#define _TACPROT_H

/************************** tacmain.c ***************************************/
INT4 TacMainInit PROTO ((VOID));

INT4 TacMainDeInit PROTO ((VOID));

/************************** tacprf.c ****************************************/
tTacProfileEntry * TacPrfCreateProfile
PROTO ((UINT4 u4ProfileId, UINT1 u1AddressType));

INT4 TacPrfDeleteProfile
PROTO ((tTacProfileEntry * pTacProfileEntry));

tTacPrfFilterEntry * TacPrfCreateFilter
PROTO ((tTacProfileEntry * pTacProfileEntry,
        UINT1 * pu1GrpStartAddr, UINT1 * pu1GrpEndAddr,
        UINT1 * pu1SrcStartAddr, UINT1 * pu1SrcEndAddr));

INT4 TacPrfDeleteFilter
PROTO ((tTacProfileEntry * pTacProfileEntry,
        tTacPrfFilterEntry * pTacPrfFilterEntry));

INT4 TacPrfCreateHashTable
PROTO ((tTacProfileEntry * pTacProfileEntry));

/************************** tacutil.c ***************************************/
INT4 TacUtilComparePrfInfo
PROTO ((tRBElem * pRBElemFirst, tRBElem * pRBElemSecond));

INT4 TacUtilComparePrfFilterInfo
PROTO ((tRBElem * pRBElemFirst, tRBElem * pRBElemSecond));

INT4 TacUtilFreePrfFilter
PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

UINT1 TacUtilGetNoOfHashBuckets PROTO ((UINT4 u4NoOfFilters));

UINT4 TacUtilGetHashIndex
PROTO ((UINT1 u1Total, UINT4 u4Minimum, UINT4 u4Maximum, UINT4 u4Value));

VOID TacUtilGetMinMaxGrpAddr PROTO ((tTacProfileEntry * pTacProfileEntry));

INT4 TacUtilAddHashNode
PROTO ((tTacProfileEntry * pTacProfileEntry,
        tTacPrfFilterEntry * pTacPrfFilterEntry, UINT4 u4HashIndex));

INT1 TacUtilIsFilterMatch
PROTO ((tTacPrfFilterEntry * pTacPrfFilterEntry,
        tIPvXAddr GroupAddr, UINT4 u4SrcAddr, UINT1 u1Mode));

INT1 TacUtilSearchHashBucket
PROTO ((tTacProfileEntry * pTacProfileEntry,
        tIPvXAddr GroupAddr, UINT1 * pu1SrcAddr, UINT4 u4HashIndex,
        UINT1 u1Mode));

INT4 TacStatusUpdate
PROTO ((INT4 i4TacStatus));

#endif
