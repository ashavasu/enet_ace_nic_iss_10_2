/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: tacinc.h,v 1.4 2011/07/04 13:42:13 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : tacinc.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC module header files                        */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains include files               */
/*                            for TAC module                                 */
/*---------------------------------------------------------------------------*/

#ifndef _TACINC_H
#define _TACINC_H

#include "lr.h"
#include "fssnmp.h"
#include "fsvlan.h"
#include "l2iwf.h"

#include "tac.h"
#include "tacdefn.h"
#include "tactdfs.h"
#include "tacextn.h"
#include "tacmacs.h"
#include "tacprot.h"
#include "fstaclw.h"
#include "fstacwr.h"
#include "tactrc.h"
#include "tacnp.h"

#endif /*_TACINC_H */

