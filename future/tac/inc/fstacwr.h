#ifndef _FSTACWR_H
#define _FSTACWR_H

VOID RegisterFSTAC(VOID);

VOID UnRegisterFSTAC(VOID);
INT4 FsTacMcastChannelDefaultBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 FsTacTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsTacStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastChannelDefaultBandwidthSet(tSnmpIndex *, tRetVal *);
INT4 FsTacTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsTacStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastChannelDefaultBandwidthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacMcastChannelDefaultBandwidthDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsTacTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsTacStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsTacMcastProfileTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsTacMcastProfileActionGet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastProfileDescriptionGet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastProfileStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastProfileActionSet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastProfileDescriptionSet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastProfileStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastProfileActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacMcastProfileDescriptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacMcastProfileStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacMcastProfileTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsTacMcastPrfFilterTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsTacMcastPrfFilterModeGet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastPrfFilterStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastPrfFilterModeSet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastPrfFilterStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastPrfFilterModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacMcastPrfFilterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacMcastPrfFilterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsTacMcastChannelTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsTacMcastChannelBandWidthGet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastChannelRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastChannelBandWidthSet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastChannelRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastChannelBandWidthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacMcastChannelRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacMcastChannelTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsTacMcastPrfStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsTacMcastPrfStatsPortRefCntGet(tSnmpIndex *, tRetVal *);
INT4 FsTacMcastPrfStatsVlanRefCntGet(tSnmpIndex *, tRetVal *);
#endif /* _FSTACWR_H */
