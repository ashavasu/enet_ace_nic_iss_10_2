/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: tacdefn.h,v 1.5 2013/04/29 13:22:35 siva Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : tacdefn.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC module definitions                         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains constants defined           */
/*                            for TAC module                                 */
/*---------------------------------------------------------------------------*/
#ifndef _TACDEFN_H
#define _TACDEFN_H

/*  Maxuimum number of bytes required to maintain the instance bitmap
 */
#define TACM_MAX_CXT_BYTES                ((SYS_DEF_MAX_NUM_CONTEXTS + 31)/32 * 4)

/* Macros related to Filter Hash Table */
#define TACM_MAX_BUCKETS                  100   /* Maximum buckets allowed   */
#define TACM_MAX_NODES_PER_BUCKET         TACM_MAX_FILTERS / TACM_MAX_BUCKETS
                                                /* Maximum filters allowed
                                                 * per bucket                */
#define TACM_MAX_BUCKETS_PER_FILTER       10    /* Maximum buckets which the
                                                 * filter can overlap        */
#define TACM_MAX_PRF_FILTER_HASH_ENTRIES  TACM_MAX_FILTERS * TACM_MAX_BUCKETS_PER_FILTER
                                                /* Maximum hash nodes. Each
                                                 * rule can be in
                                                 * TACM_MAX_BUCKETS_PER_FILTER
                                                 * buckets. Hence maximum
                                                 * number of hash nodes is a
                                                 * multiple of maximum number
                                                 * of filters                */

/* Multicast ranges */                    
#define TACM_MIN_IPv4_MCAST_ADDR          0xE0000001
#define TACM_MAX_IPv4_MCAST_ADDR          0xEFFFFFFF

/* Reserved Multicast address ranges */
#define TACM_MIN_IPV4_RSVD_MCAST_ADDR     0xE0000001
#define TACM_MAX_IPV4_RSVD_MCAST_ADDR     0xE00000FF
                                          
/* comparison macros */                   
#define TACM_LESSER                      -1
#define TACM_EQUAL                        0
#define TACM_GREATER                      1
                                          
/* Rowstatus Macros */                    
#define TACM_ACTIVE                       ACTIVE
#define TACM_NOT_IN_SERVICE               NOT_IN_SERVICE
#define TACM_NOT_READY                    NOT_READY
#define TACM_CREATE_AND_WAIT              CREATE_AND_WAIT
#define TACM_CREATE_AND_GO                CREATE_AND_GO
#define TACM_DESTROY                      DESTROY
                                          
/* Semaphore related definition */
#define TACM_SEM_NAME                     ((UINT1 *) "TACM")
#define TACM_MODULE_NAME                  "TAC"
#define TACM_SEM_COUNT                    1
#define TACM_SEM_FLAGS                    OSIX_DEFAULT_SEM_MODE

/* Trace related macros */
#define TACM_MAX_TRACE                    TACM_ALL_TRC

#endif
