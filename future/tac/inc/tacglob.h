/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: tacglob.h,v 1.4 2009/12/02 15:35:51 prabuc Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : tacglob.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC module global definitions                  */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains global definitions          */
/*                            for TAC module                                 */
/*---------------------------------------------------------------------------*/
#ifndef _TACGLOB_H
#define _TACGLOB_H

/* Global configurations */
tTacGlobalConfig        gTacGlobalConfig;

/* Global memory pools */
tTacGlobalInfo          gTacGlobalInfo;

/* Sizing Params Structure */
tFsModSizingParams gFsTacSizingParams [] =
{
    {"TACM_PROFILE_MEMBLK_SIZE", "TACM_MAX_PROFILES", TACM_PROFILE_MEMBLK_SIZE,
     TACM_MAX_PROFILES, TACM_MAX_PROFILES, 0},

    {"TACM_PRF_FILTER_MEMBLK_SIZE", "TACM_MAX_FILTERS", TACM_PRF_FILTER_MEMBLK_SIZE,
     TACM_MAX_FILTERS, TACM_MAX_FILTERS, 0},

    {"TACM_PRF_FILTER_HASH_MEMBLK_SIZE", "TACM_MAX_PRF_FILTER_HASH_ENTRIES",
     TACM_PRF_FILTER_HASH_MEMBLK_SIZE, TACM_MAX_PRF_FILTER_HASH_ENTRIES,
     TACM_MAX_PRF_FILTER_HASH_ENTRIES, 0},

    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo gFsTacSizingInfo;


#endif
