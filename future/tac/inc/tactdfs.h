/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: tactdfs.h,v 1.6 2011/04/28 11:57:21 siva Exp $                                                                    */
/*****************************************************************************/
/* FILE  NAME            : tactdfs.h                                         */
/* PRINCIPAL AUTHOR      : Aricent Inc.                                      */
/* SUBSYSTEM NAME        : Transmission and Admission Control (TAC)          */
/* MODULE NAME           : TAC Data Structures                               */
/* LANGUAGE              : C                                                 */
/* TARGET ENVIRONMENT    : Any                                               */
/* DATE OF FIRST RELEASE :                                                   */
/* AUTHOR                : Aricent Inc.                                      */
/* DESCRIPTION           : This file contains Data Structures                */
/*                         for TAC module                                    */
/*---------------------------------------------------------------------------*/
#ifndef _TACTDFS_H
#define _TACTDFS_H

/* TAC module Profile structure */
typedef struct _TacProfileEntry {
    tRBNodeEmbd         RbNode;             /* The profile node in the
                                             * global RB Tree                 */
    tRBTree             pPrfFilterRoot;     /* Pointer to the root of filter
                                             * rules RB Tree                  */
    tTMO_HASH_TABLE    *pPrfFilterHashTable;/* Pointer to filter rules Hash
                                             * Table                          */
    UINT4               u4ProfileId;        /* The unique profile Id for each
                                             * profile entry                  */
    UINT4               u4MinGrpAddr;       /* The minimum IPv4 group address
                                             * configured in the filter rules.
                                             * This does not include the wild
                                             * card (0.0.0.0)                 */
    UINT4               u4MaxGrpAddr;       /* The maximum IPv4 group address
                                             * configured in the filter rules.*/
    UINT4               u4IntfRefCnt;       /* The number of registrations done
                                             * for this profile across all the
                                             * modules. The profile can be
                                             * deleted only after removing
                                             * the registrations in all the
                                             * modules                        */
    UINT4               u4VlanRefCnt;       /* The number of registrations done
                                             * for this profile with the VLAN
                                             * across all instances.
                                             * The profile can be
                                             * deleted only after removing
                                             * the registrations in all the
                                             * modules                        */
    UINT1               au1VlanInstBitMap [TACM_MAX_CXT_BYTES];
                                            /* Contains the bitmap of instances
                                             * to indicate whether a profile is
                                             * mapped to any VLAN in this
                                             * instance or not                */
    UINT1               au1Description [TACM_MAX_DESCR_ARRAY_LEN];
                                            /* An optional description for
                                             * each profile entry             */
    UINT1               u1AddressType;      /* The internet address type of
                                             * the rules maintained in the
                                             * profile                        */
    UINT1               u1ProfileAction;    /* Action configured for the
                                             * profile entry
                                             * Macro         Value
                                             * -------------------
                                             * TACM_PERMIT     1
                                             * TACM_DENY       2              */
    UINT1               u1FilterAddOrDelete;/* The flag used to indicate
                                             * whether a filter is added or
                                             * deleted. This can be used to
                                             * check whether the hash table
                                             * needs to be re-constructed
                                             * or not (TACM_TRUE/TACM_FALSE)  */
    UINT1               u1NoOfBuckets;      /* Number of buckets used by this
                                             * profile                        */
    UINT1               u1RowStatus;        /* Row status                     */
    UINT1               au1Reserved[3];     /* Included for 4 byte alignment  */
}tTacProfileEntry;


/* Filter rules structure for each profile. Each entry in a filter table
 * corresponds to a range of channels (source, group).
 * 0.0.0.0 for source and group address denotes a wild card */
typedef struct _TacPrfFilterEntry {
    tRBNodeEmbd         RbNode;             /* The filter rule node in the
                                             * profile RB Tree                */
    UINT1               GrpStartAddr[IPVX_MAX_INET_ADDR_LEN];
                                            /* Low Multicast address          */
    UINT1               GrpEndAddr[IPVX_MAX_INET_ADDR_LEN];
                                            /* High Multicast address         */
    UINT1               SrcStartAddr[IPVX_MAX_INET_ADDR_LEN];
                                            /* Low Source address             */
    UINT1               SrcEndAddr[IPVX_MAX_INET_ADDR_LEN];
                                            /* High Source address            */
    UINT1               u1FilterMode;       /* Type of packets for which the
                                             * the filter is to be applied
                                             * Macro          Value
                                             * --------------------
                                             * TACM_INCLUDE     1
                                             * TACM_EXCLUDE     2
                                             * TACM_ANY         3             */
    UINT1               u1RowStatus;        /* Row status                     */
    UINT1               au1Reserved[2];     /* Included for 4 byte alignment  */
}tTacPrfFilterEntry;


/* Filter Hash Structure. Each rule in a profile can belong to multiple
 * Hash buckets. This structure contains the hash nodes for each bucket.
 * the address of the first bucket node is maintained in the filter entry     */
typedef struct _TacPrfFilterHashEntry {
    tTMO_HASH_NODE      PrfFilterHashNode;  /* Pointer to next node in Hash
                                             * Bucket                         */
    tTacPrfFilterEntry *pTacPrfFilterEntry; /* Pointer to the filter entry to
                                             * which this node belongs        */
}tTacPrfFilterHashEntry;


/* TAC module Global configurations */
typedef struct TacGlobalConfig {
    tRBTree             pProfileRBRoot;     /* Pointer to the RB Tree root of
                                             * profile entries                */
    tOsixSemId          TacmSemId;          /* Semapore for the module        */
    UINT4               u4TacmTrace;        /* Trace flags                    */
    INT4                i4TacStatus;        /* TAC Module Global Status       */
}tTacGlobalConfig;


/* TAC module Global memory pools */
typedef struct TacGlobalInfo {
    tMemPoolId          ProfilePoolId;      /* Memory pool for the profile
                                               entries                        */
    tMemPoolId          PrfFilterPoolId;    /* Memory pool for the filter rule
                                               entries of each profile entry  */
    tMemPoolId          FilterHashPoolId;   /* Memory pool for storing the
                                             * hash nodes for each rule       */
}tTacGlobalInfo;


/* Sizing Params enum.*/
enum
{
   TACM_PROFILE_SIZING_ID,
   TACM_PRF_FLTR_SIZING_ID,
   TACM_PRF_FLTR_HASH_SIZING_ID,
   TACM_NO_SIZING_ID
};
#endif
