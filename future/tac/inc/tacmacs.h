/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/* $Id: tacmacs.h,v 1.6 2010/01/07 12:43:59 prabuc Exp $                                                                    */
/*****************************************************************************/
/*    FILE  NAME            : TACmacs.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Transmission and Admission Control (TAC)       */
/*    MODULE NAME           : TAC Module related Macros                      */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains macro definitions           */
/*                            for TAC Module                                 */
/*---------------------------------------------------------------------------*/
#ifndef _TACMACS_H
#define _TACMACS_H

/* Trace related Macro */
#define TACM_TRC_FLAG                     gTacGlobalConfig.u4TacmTrace
                                          
/* RBTree related macros */
#define TACM_PROFILE_TREE                 gTacGlobalConfig.pProfileRBRoot
                                          
/* Semapahore */                          
#define TACM_SEM_ID                       gTacGlobalConfig.TacmSemId

/* OSIX related definition */
#define TACM_CREATE_MEM_POOL              MemCreateMemPool
#define TACM_CREATE_SEM                   OsixSemCrt
#define TACM_DELETE_SEM                   OsixSemDel

#define TACM_DELETE_MEM_POOL(PoolId) \
{\
    if (PoolId != 0)\
    {\
        (VOID) MemDeleteMemPool(PoolId);\
    }\
}

#define TACM_ALLOC_MEM_BLOCK(type,PoolId) \
    ((type *)MemAllocMemBlk(PoolId))

#define TACM_RELEASE_MEM_BLOCK(PoolId, pu1Msg) \
{\
    if (pu1Msg != NULL) \
    { \
        MemReleaseMemBlock(PoolId, (UINT1 *) pu1Msg); \
        pu1Msg = NULL; \
    } \
}

/* Memory block size */
#define TACM_PROFILE_MEMPOOL_ID           gTacGlobalInfo.ProfilePoolId
#define TACM_PROFILE_MEMBLK_SIZE          sizeof (tTacProfileEntry)
                                          
#define TACM_PRF_FILTER_MEMPOOL_ID        gTacGlobalInfo.PrfFilterPoolId
#define TACM_PRF_FILTER_MEMBLK_SIZE       sizeof (tTacPrfFilterEntry)

#define TACM_PRF_FILTER_HASH_MEMPOOL_ID   gTacGlobalInfo.FilterHashPoolId
#define TACM_PRF_FILTER_HASH_MEMBLK_SIZE  sizeof (tTacPrfFilterHashEntry)

/* Offset calculation */
#define  TACM_OFFSET(x,y)                 FSAP_OFFSETOF(x,y)

/* Checking the validity of multicast address */
#define TACM_IS_VALID_IPV4_MCAST_ADDRESS(addr) \
    ((addr >= TACM_MIN_IPv4_MCAST_ADDR) && \
     (addr <= TACM_MAX_IPv4_MCAST_ADDR))

/* Checking the reserved multicast address */
#define TACM_IS_RSVD_IPV4_MCAST_ADDRESS(addr) \
    ((addr >= TACM_MIN_IPV4_RSVD_MCAST_ADDR) && \
     (addr <= TACM_MAX_IPV4_RSVD_MCAST_ADDR))


/* Byte order change */
#define TAC_INET_NTOHL(Addr,addrlen)\
{\
    UINT4   u4TmpAddr = 0;\
    UINT4   u4Count = 0;\
    UINT1   u1Index = 0;\
    UINT1   au1TmpAddr[addrlen];\
        \
    MEMSET (au1TmpAddr, 0, addrlen);\
    \
    for (u4Count = 0, u1Index = 0; u4Count < addrlen;\
                      u1Index++, u4Count = u4Count + 4)\
    {\
        MEMCPY (&u4TmpAddr, (Addr + u4Count), sizeof(UINT4));\
        u4TmpAddr = OSIX_NTOHL(u4TmpAddr);\
        MEMCPY (&(au1TmpAddr[u4Count]), &u4TmpAddr, sizeof(UINT4));\
    }\
    MEMCPY (Addr, au1TmpAddr, addrlen);\
}


#define  TACM_SLL_SCAN(pList, pNode, Typecast) \
         TMO_SLL_Scan((tTacmSll *)pList, pNode, Typecast)

#endif
