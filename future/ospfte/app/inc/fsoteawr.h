#ifndef _FSOTEAWR_H
#define _FSOTEAWR_H
INT4 GetNextIndexFsOspfTeSasConstraintTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSOTEA(VOID);

VOID UnRegisterFSOTEA(VOID);
INT4 FsOspfTeSasConstraintSourceIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintDestinationIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintWPSourceIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintWPDestinationIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintMaxPathMetricGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintMaxHopsInPathGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintBwGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintIncludeAllSetGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintIncludeAnySetGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintExcludeAnySetGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintExplicitRouteGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintSwitchingCapabilityGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintEncodingTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintLinkProtectionTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintDiversityGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintIndicationGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintSourceIpAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintDestinationIpAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintWPSourceIpAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintWPDestinationIpAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintMaxPathMetricSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintMaxHopsInPathSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintBwSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintIncludeAllSetSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintIncludeAnySetSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintExcludeAnySetSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintExplicitRouteSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintSwitchingCapabilitySet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintEncodingTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintLinkProtectionTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintDiversitySet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintIndicationSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintSourceIpAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintDestinationIpAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintWPSourceIpAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintWPDestinationIpAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintMaxPathMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintMaxHopsInPathTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintBwTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintIncludeAllSetTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintIncludeAnySetTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintExcludeAnySetTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintExplicitRouteTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintSwitchingCapabilityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintEncodingTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintLinkProtectionTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintDiversityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintIndicationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasConstraintTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



















INT4 GetNextIndexFsOspfTeSasCspfPathTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsOspfTeSasCspfPathNumHopsGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasCspfPathRouterIdGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasCspfPathNextHopIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsOspfTeSasCspfPathLocalIdentifierGet(tSnmpIndex *, tRetVal *);
#endif /* _FSOTEAWR_H */
