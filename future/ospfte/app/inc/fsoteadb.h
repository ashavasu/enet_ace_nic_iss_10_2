/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsoteadb.h,v 1.3 2008/08/20 15:22:11 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSOTEADB_H
#define _FSOTEADB_H

UINT1 FsOspfTeSasConstraintTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsOspfTeSasCspfPathTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsotea [] ={1,3,6,1,4,1,2076,72,20};
tSNMP_OID_TYPE fsoteaOID = {9, fsotea};


UINT4 FsOspfTeSasConstraintId [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,1};
UINT4 FsOspfTeSasConstraintSourceIpAddr [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,2};
UINT4 FsOspfTeSasConstraintDestinationIpAddr [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,3};
UINT4 FsOspfTeSasConstraintWPSourceIpAddr [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,4};
UINT4 FsOspfTeSasConstraintWPDestinationIpAddr [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,5};
UINT4 FsOspfTeSasConstraintMaxPathMetric [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,6};
UINT4 FsOspfTeSasConstraintMaxHopsInPath [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,7};
UINT4 FsOspfTeSasConstraintBw [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,8};
UINT4 FsOspfTeSasConstraintIncludeAllSet [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,9};
UINT4 FsOspfTeSasConstraintIncludeAnySet [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,10};
UINT4 FsOspfTeSasConstraintExcludeAnySet [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,11};
UINT4 FsOspfTeSasConstraintPriority [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,12};
UINT4 FsOspfTeSasConstraintExplicitRoute [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,13};
UINT4 FsOspfTeSasConstraintSwitchingCapability [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,14};
UINT4 FsOspfTeSasConstraintEncodingType [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,15};
UINT4 FsOspfTeSasConstraintLinkProtectionType [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,16};
UINT4 FsOspfTeSasConstraintDiversity [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,17};
UINT4 FsOspfTeSasConstraintIndication [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,18};
UINT4 FsOspfTeSasConstraintFlag [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,19};
UINT4 FsOspfTeSasConstraintStatus [ ] ={1,3,6,1,4,1,2076,72,20,2,1,1,20};
UINT4 FsOspfTeSasCspfPathConstraintId [ ] ={1,3,6,1,4,1,2076,72,20,2,2,1,1};
UINT4 FsOspfTeSasCspfPathType [ ] ={1,3,6,1,4,1,2076,72,20,2,2,1,2};
UINT4 FsOspfTeSasCspfPathNumHops [ ] ={1,3,6,1,4,1,2076,72,20,2,2,1,3};
UINT4 FsOspfTeSasCspfPathRouterId [ ] ={1,3,6,1,4,1,2076,72,20,2,2,1,4};
UINT4 FsOspfTeSasCspfPathNextHopIpAddress [ ] ={1,3,6,1,4,1,2076,72,20,2,2,1,5};
UINT4 FsOspfTeSasCspfPathLocalIdentifier [ ] ={1,3,6,1,4,1,2076,72,20,2,2,1,6};


tMbDbEntry fsoteaMibEntry[]= {

{{13,FsOspfTeSasConstraintId}, GetNextIndexFsOspfTeSasConstraintTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintSourceIpAddr}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintSourceIpAddrGet, FsOspfTeSasConstraintSourceIpAddrSet, FsOspfTeSasConstraintSourceIpAddrTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintDestinationIpAddr}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintDestinationIpAddrGet, FsOspfTeSasConstraintDestinationIpAddrSet, FsOspfTeSasConstraintDestinationIpAddrTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintWPSourceIpAddr}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintWPSourceIpAddrGet, FsOspfTeSasConstraintWPSourceIpAddrSet, FsOspfTeSasConstraintWPSourceIpAddrTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintWPDestinationIpAddr}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintWPDestinationIpAddrGet, FsOspfTeSasConstraintWPDestinationIpAddrSet, FsOspfTeSasConstraintWPDestinationIpAddrTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintMaxPathMetric}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintMaxPathMetricGet, FsOspfTeSasConstraintMaxPathMetricSet, FsOspfTeSasConstraintMaxPathMetricTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintMaxHopsInPath}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintMaxHopsInPathGet, FsOspfTeSasConstraintMaxHopsInPathSet, FsOspfTeSasConstraintMaxHopsInPathTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintBw}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintBwGet, FsOspfTeSasConstraintBwSet, FsOspfTeSasConstraintBwTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintIncludeAllSet}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintIncludeAllSetGet, FsOspfTeSasConstraintIncludeAllSetSet, FsOspfTeSasConstraintIncludeAllSetTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintIncludeAnySet}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintIncludeAnySetGet, FsOspfTeSasConstraintIncludeAnySetSet, FsOspfTeSasConstraintIncludeAnySetTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintExcludeAnySet}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintExcludeAnySetGet, FsOspfTeSasConstraintExcludeAnySetSet, FsOspfTeSasConstraintExcludeAnySetTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintPriority}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintPriorityGet, FsOspfTeSasConstraintPrioritySet, FsOspfTeSasConstraintPriorityTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintExplicitRoute}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintExplicitRouteGet, FsOspfTeSasConstraintExplicitRouteSet, FsOspfTeSasConstraintExplicitRouteTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintSwitchingCapability}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintSwitchingCapabilityGet, FsOspfTeSasConstraintSwitchingCapabilitySet, FsOspfTeSasConstraintSwitchingCapabilityTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintEncodingType}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintEncodingTypeGet, FsOspfTeSasConstraintEncodingTypeSet, FsOspfTeSasConstraintEncodingTypeTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintLinkProtectionType}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintLinkProtectionTypeGet, FsOspfTeSasConstraintLinkProtectionTypeSet, FsOspfTeSasConstraintLinkProtectionTypeTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintDiversity}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintDiversityGet, FsOspfTeSasConstraintDiversitySet, FsOspfTeSasConstraintDiversityTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintIndication}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintIndicationGet, FsOspfTeSasConstraintIndicationSet, FsOspfTeSasConstraintIndicationTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintFlag}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintFlagGet, FsOspfTeSasConstraintFlagSet, FsOspfTeSasConstraintFlagTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 0, NULL},

{{13,FsOspfTeSasConstraintStatus}, GetNextIndexFsOspfTeSasConstraintTable, FsOspfTeSasConstraintStatusGet, FsOspfTeSasConstraintStatusSet, FsOspfTeSasConstraintStatusTest, FsOspfTeSasConstraintTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsOspfTeSasConstraintTableINDEX, 1, 0, 1, NULL},

{{13,FsOspfTeSasCspfPathConstraintId}, GetNextIndexFsOspfTeSasCspfPathTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsOspfTeSasCspfPathTableINDEX, 2, 0, 0, NULL},

{{13,FsOspfTeSasCspfPathType}, GetNextIndexFsOspfTeSasCspfPathTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsOspfTeSasCspfPathTableINDEX, 2, 0, 0, NULL},

{{13,FsOspfTeSasCspfPathNumHops}, GetNextIndexFsOspfTeSasCspfPathTable, FsOspfTeSasCspfPathNumHopsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsOspfTeSasCspfPathTableINDEX, 2, 0, 0, NULL},

{{13,FsOspfTeSasCspfPathRouterId}, GetNextIndexFsOspfTeSasCspfPathTable, FsOspfTeSasCspfPathRouterIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsOspfTeSasCspfPathTableINDEX, 2, 0, 0, NULL},

{{13,FsOspfTeSasCspfPathNextHopIpAddress}, GetNextIndexFsOspfTeSasCspfPathTable, FsOspfTeSasCspfPathNextHopIpAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsOspfTeSasCspfPathTableINDEX, 2, 0, 0, NULL},

{{13,FsOspfTeSasCspfPathLocalIdentifier}, GetNextIndexFsOspfTeSasCspfPathTable, FsOspfTeSasCspfPathLocalIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsOspfTeSasCspfPathTableINDEX, 2, 0, 0, NULL},
};
tMibData fsoteaEntry = { 26, fsoteaMibEntry };
#endif /* _FSOTEADB_H */

