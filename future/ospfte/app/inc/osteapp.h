
/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: osteapp.h,v 1.5 2014/03/03 12:22:26 siva Exp $
 *
 * Description: This file contains constants , typedefs & macros
 *              related to App module.
 *********************************************************************/
#include "ospfte.h"
#include "ostecmn.h"

/*******************************************************
 * Application constraint data structure.
 *****************************************************/

tTMO_SLL            gOsTeAppConsLst;

#define IS_VALID_TE_METRIC(x)     (x > 0)
#define IS_VALID_MAX_HOPS_IN_PATH(x) ((x > 0) && (x <= 16))
#define IS_VALID_FLAG_VALUE(x) ((x > 0) && (x <= 63))
#define IS_VALID_DIVERSITY_VALUE(x) ((x > 0) && (x <= 7))
#define IS_VALID_CONSTRAINT_PRIORITY(x)     ((x >= 0) && (x <= 7))
#define IS_VALID_SWITCH_CAPABLE(x)     (((x >= 1) && (x <= 4)) || (x == 51)  \
                                                || (x == 100) \
             || (x == 150) \
             || (x == 200))
#define IS_VALID_ENCODING_TYPE(x)     (((x >= 1) && (x <= 11)) && ((x != 4)|| \
                                                        (x != 6)|| \
                     (x != 10))) 
#define IS_VALID_PROTECTION_TYPE(x)     ((x == 0x01) || \
                              (x == 0x02) || \
         (x == 0x04) || \
         (x == 0x08) || \
         (x == 0x10) || \
         (x == 0x20) || \
         (x == 0x40) || \
         (x == 0x80))
#define IS_VALID_CONSTRAINT_INDICATION(x) ((x == 0) || (x == 1))


#define LTD_B_CAST_ADDR       0xffffffff     /* Limited BroadCast address */

#define IS_CLASS_A_ADDR(u4Addr) \
                               ((u4Addr & 0x80000000) == 0)

#define IS_CLASS_B_ADDR(u4Addr) \
                               ((u4Addr & 0xc0000000) == 0x80000000)

#define IS_CLASS_C_ADDR(u4Addr) \
                               ((u4Addr & 0xe0000000) == 0xc0000000)
#define LOOP_BACK_ADDR_MASK   0xff000000     /* Loop back address                                                   127.x.x.x range */
#define LOOP_BACK_ADDRESES    0x7f000000


PUBLIC tOsTeCspfCons * OspfTeGetConstrainInfo (UINT4 u4ConstraintId);
PUBLIC  UINT1 OspfTeIsValidIpAddress (UINT4 u4IpAddress);

#define APP_SUCCESS  1
#define APP_FAILURE  0

#define OSPF_TE_APP_MEMCPY      MEMCPY
#define OSPF_TE_APP_MEMSET      MEMSET

#define OSPF_TE_APP_NULL        0
#define OSPF_TE_APP_TRUE        1
#define OSPF_TE_APP_FALSE       2

#define OSPFTE_APP_THOUSAND_BYTES    1000
