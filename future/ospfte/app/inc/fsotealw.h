/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: fsotealw.h,v 1.3 2008/08/20 15:22:11 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsOspfTeSasConstraintTable. */
INT1
nmhValidateIndexInstanceFsOspfTeSasConstraintTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsOspfTeSasConstraintTable  */

INT1
nmhGetFirstIndexFsOspfTeSasConstraintTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsOspfTeSasConstraintTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsOspfTeSasConstraintSourceIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsOspfTeSasConstraintDestinationIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsOspfTeSasConstraintWPSourceIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsOspfTeSasConstraintWPDestinationIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsOspfTeSasConstraintMaxPathMetric ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintMaxHopsInPath ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintBw ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintIncludeAllSet ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintIncludeAnySet ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintExcludeAnySet ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintExplicitRoute ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsOspfTeSasConstraintSwitchingCapability ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintEncodingType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintLinkProtectionType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintDiversity ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintIndication ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasConstraintStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsOspfTeSasConstraintSourceIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsOspfTeSasConstraintDestinationIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsOspfTeSasConstraintWPSourceIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsOspfTeSasConstraintWPDestinationIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsOspfTeSasConstraintMaxPathMetric ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintMaxHopsInPath ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintBw ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintIncludeAllSet ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintIncludeAnySet ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintExcludeAnySet ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintExplicitRoute ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsOspfTeSasConstraintSwitchingCapability ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintEncodingType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintLinkProtectionType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintDiversity ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintIndication ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsOspfTeSasConstraintStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsOspfTeSasConstraintSourceIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintDestinationIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintWPSourceIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintWPDestinationIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintMaxPathMetric ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintMaxHopsInPath ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintBw ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintIncludeAllSet ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintIncludeAnySet ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintExcludeAnySet ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintExplicitRoute ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsOspfTeSasConstraintSwitchingCapability ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintEncodingType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintLinkProtectionType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintDiversity ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintIndication ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsOspfTeSasConstraintStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsOspfTeSasConstraintTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsOspfTeSasCspfPathTable. */
INT1
nmhValidateIndexInstanceFsOspfTeSasCspfPathTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsOspfTeSasCspfPathTable  */

INT1
nmhGetFirstIndexFsOspfTeSasCspfPathTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsOspfTeSasCspfPathTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsOspfTeSasCspfPathNumHops ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsOspfTeSasCspfPathRouterId ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsOspfTeSasCspfPathNextHopIpAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsOspfTeSasCspfPathLocalIdentifier ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
