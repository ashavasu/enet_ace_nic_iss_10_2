/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: fsotealw.c,v 1.11 2014/02/14 14:07:36 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsotealw.h"
#include   "snmccons.h"
# include  "osteinc.h"
# include  "osteapp.h"

#define OSPF_CRU_BMC_DWTOPDU(pu1PduAddr,u4Value) \
        OspfTeUtilDwordToPdu (pu1PduAddr,u4Value)

#define  LGET1BYTE(addr)  *((UINT1 *)addr); addr += 1;
#define  LGET2BYTE(addr)  OSPF_TE_CRU_BMC_WFROMPDU(addr); addr += 2;
#define  LGET3BYTE(addr)  THREE_BYTE_FROM_PDU(addr); addr += 3;
#define  LGET4BYTE(addr)  OSPF_TE_CRU_BMC_DWFROMPDU(addr); addr += 4;

/* LOW LEVEL Routines for Table : FsOspfTeSasConstraintTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsOspfTeSasConstraintTable
 Input       :  The Indices
                FsOspfTeSasConstraintId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsOspfTeSasConstraintTable (INT4
                                                    i4FsOspfTeSasConstraintId)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsOspfTeSasConstraintTable
 Input       :  The Indices
                FsOspfTeSasConstraintId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsOspfTeSasConstraintTable (INT4 *pi4FsOspfTeSasConstraintId)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    pOsTeCspfCons = (tOsTeCspfCons *) TMO_SLL_First (&(gOsTeAppConsLst));
    if (pOsTeCspfCons != NULL)
    {
        *pi4FsOspfTeSasConstraintId = (INT4) pOsTeCspfCons->u4ConstraintId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsOspfTeSasConstraintTable
 Input       :  The Indices
                FsOspfTeSasConstraintId
                nextFsOspfTeSasConstraintId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsOspfTeSasConstraintTable (INT4 i4FsOspfTeSasConstraintId,
                                           INT4 *pi4NextFsOspfTeSasConstraintId)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    UINT1               u1Flag = APP_FAILURE;
    TMO_SLL_Scan (&(gOsTeAppConsLst), pOsTeCspfCons, tOsTeCspfCons *)
    {
        if ((INT4) pOsTeCspfCons->u4ConstraintId == i4FsOspfTeSasConstraintId)
        {
            u1Flag = APP_SUCCESS;
            continue;
        }
        else if (u1Flag == APP_SUCCESS)
        {
            *pi4NextFsOspfTeSasConstraintId =
                (INT4) pOsTeCspfCons->u4ConstraintId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintSourceIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintSourceIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintSourceIpAddr (INT4 i4FsOspfTeSasConstraintId,
                                         UINT4
                                         *pu4RetValFsOspfTeSasConstraintSourceIpAddr)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pu4RetValFsOspfTeSasConstraintSourceIpAddr =
            pOsTeCspfCons->pCspfReq->u4SrcIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintDestinationIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintDestinationIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintDestinationIpAddr (INT4 i4FsOspfTeSasConstraintId,
                                              UINT4
                                              *pu4RetValFsOspfTeSasConstraintDestinationIpAddr)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pu4RetValFsOspfTeSasConstraintDestinationIpAddr =
            pOsTeCspfCons->pCspfReq->u4DestIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintWPSourceIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintWPSourceIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintWPSourceIpAddr (INT4 i4FsOspfTeSasConstraintId,
                                           UINT4
                                           *pu4RetValFsOspfTeSasConstraintWPSourceIpAddr)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pu4RetValFsOspfTeSasConstraintWPSourceIpAddr =
            pOsTeCspfCons->pCspfReq->u4WPSrcIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintWPDestinationIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintWPDestinationIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintWPDestinationIpAddr (INT4 i4FsOspfTeSasConstraintId,
                                                UINT4
                                                *pu4RetValFsOspfTeSasConstraintWPDestinationIpAddr)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pu4RetValFsOspfTeSasConstraintWPDestinationIpAddr =
            pOsTeCspfCons->pCspfReq->u4WPDestIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintMaxPathMetric
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintMaxPathMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintMaxPathMetric (INT4 i4FsOspfTeSasConstraintId,
                                          INT4
                                          *pi4RetValFsOspfTeSasConstraintMaxPathMetric)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintMaxPathMetric =
            (INT4) pOsTeCspfCons->pCspfReq->u4MaxPathMetric;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintMaxHopsInPath
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintMaxHopsInPath
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintMaxHopsInPath (INT4 i4FsOspfTeSasConstraintId,
                                          INT4
                                          *pi4RetValFsOspfTeSasConstraintMaxHopsInPath)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintMaxHopsInPath =
            (INT4) pOsTeCspfCons->pCspfReq->u2HopCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintBw
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintBw
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintBw (INT4 i4FsOspfTeSasConstraintId,
                               INT4 *pi4RetValFsOspfTeSasConstraintBw)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintBw =
            (INT4) pOsTeCspfCons->pCspfReq->bandwidth;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintIncludeAllSet
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintIncludeAllSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintIncludeAllSet (INT4 i4FsOspfTeSasConstraintId,
                                          INT4
                                          *pi4RetValFsOspfTeSasConstraintIncludeAllSet)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintIncludeAllSet =
            (INT4) pOsTeCspfCons->pCspfReq->u4IncludeAllSet;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintIncludeAnySet
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintIncludeAnySet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintIncludeAnySet (INT4 i4FsOspfTeSasConstraintId,
                                          INT4
                                          *pi4RetValFsOspfTeSasConstraintIncludeAnySet)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintIncludeAnySet =
            (INT4) pOsTeCspfCons->pCspfReq->u4IncludeAnySet;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintExcludeAnySet
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintExcludeAnySet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintExcludeAnySet (INT4 i4FsOspfTeSasConstraintId,
                                          INT4
                                          *pi4RetValFsOspfTeSasConstraintExcludeAnySet)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintExcludeAnySet =
            (INT4) pOsTeCspfCons->pCspfReq->u4ExcludeAnySet;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintPriority
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintPriority (INT4 i4FsOspfTeSasConstraintId,
                                     INT4
                                     *pi4RetValFsOspfTeSasConstraintPriority)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintPriority =
            (INT4) pOsTeCspfCons->pCspfReq->u1Priority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintExplicitRoute
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintExplicitRoute
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintExplicitRoute (INT4 i4FsOspfTeSasConstraintId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsOspfTeSasConstraintExplicitRoute)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    UINT1              *pExpRoute = pRetValFsOspfTeSasConstraintExplicitRoute->
        pu1_OctetList;

    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        if (pOsTeCspfCons->pCspfReq->osTeExpRoute.u2HopCount != 0)
        {
            OSPF_TE_APP_MEMCPY (pExpRoute,
                                &pOsTeCspfCons->pCspfReq->osTeExpRoute.u2Info,
                                2);
            OSPF_TE_APP_MEMCPY ((pExpRoute + 2),
                                &pOsTeCspfCons->pCspfReq->osTeExpRoute.
                                u2HopCount, 2);
            OSPF_TE_APP_MEMCPY ((pExpRoute + 4),
                                pOsTeCspfCons->pCspfReq->osTeExpRoute.
                                aNextHops,
                                pOsTeCspfCons->pCspfReq->osTeExpRoute.
                                u2HopCount * sizeof (tOsTeNextHop));
            pRetValFsOspfTeSasConstraintExplicitRoute->i4_Length =
                (INT4) (4 +
                        (pOsTeCspfCons->pCspfReq->osTeExpRoute.u2HopCount *
                         sizeof (tOsTeNextHop)));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintSwitchingCapability
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintSwitchingCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintSwitchingCapability (INT4 i4FsOspfTeSasConstraintId,
                                                INT4
                                                *pi4RetValFsOspfTeSasConstraintSwitchingCapability)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintSwitchingCapability =
            (INT4) pOsTeCspfCons->pCspfReq->u1SwitchingCap;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintEncodingType
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintEncodingType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintEncodingType (INT4 i4FsOspfTeSasConstraintId,
                                         INT4
                                         *pi4RetValFsOspfTeSasConstraintEncodingType)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintEncodingType =
            (INT4) pOsTeCspfCons->pCspfReq->u1EncodingType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintLinkProtectionType
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintLinkProtectionType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintLinkProtectionType (INT4 i4FsOspfTeSasConstraintId,
                                               INT4
                                               *pi4RetValFsOspfTeSasConstraintLinkProtectionType)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintLinkProtectionType =
            (INT4) pOsTeCspfCons->pCspfReq->u1ProtectionType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintDiversity
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintDiversity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintDiversity (INT4 i4FsOspfTeSasConstraintId,
                                      INT4
                                      *pi4RetValFsOspfTeSasConstraintDiversity)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintDiversity =
            (INT4) pOsTeCspfCons->pCspfReq->u1Diversity;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintIndication
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintIndication
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintIndication (INT4 i4FsOspfTeSasConstraintId,
                                       INT4
                                       *pi4RetValFsOspfTeSasConstraintIndication)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintIndication =
            (INT4) pOsTeCspfCons->pCspfReq->u1Indication;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintFlag
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintFlag (INT4 i4FsOspfTeSasConstraintId,
                                 INT4 *pi4RetValFsOspfTeSasConstraintFlag)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintFlag =
            (INT4) pOsTeCspfCons->pCspfReq->u1Flag;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasConstraintStatus
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                retValFsOspfTeSasConstraintStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasConstraintStatus (INT4 i4FsOspfTeSasConstraintId,
                                   INT4 *pi4RetValFsOspfTeSasConstraintStatus)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if ((pOsTeCspfCons = OspfTeGetConstrainInfo
         ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
    {
        *pi4RetValFsOspfTeSasConstraintStatus =
            (INT4) pOsTeCspfCons->u4ConsStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintSourceIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintSourceIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintSourceIpAddr (INT4 i4FsOspfTeSasConstraintId,
                                         UINT4
                                         u4SetValFsOspfTeSasConstraintSourceIpAddr)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u4SrcIpAddr =
            u4SetValFsOspfTeSasConstraintSourceIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintDestinationIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintDestinationIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintDestinationIpAddr (INT4 i4FsOspfTeSasConstraintId,
                                              UINT4
                                              u4SetValFsOspfTeSasConstraintDestinationIpAddr)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u4DestIpAddr =
            u4SetValFsOspfTeSasConstraintDestinationIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintWPSourceIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintWPSourceIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintWPSourceIpAddr (INT4 i4FsOspfTeSasConstraintId,
                                           UINT4
                                           u4SetValFsOspfTeSasConstraintWPSourceIpAddr)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u4WPSrcIpAddr =
            u4SetValFsOspfTeSasConstraintWPSourceIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintWPDestinationIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintWPDestinationIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintWPDestinationIpAddr (INT4
                                                i4FsOspfTeSasConstraintId,
                                                UINT4
                                                u4SetValFsOspfTeSasConstraintWPDestinationIpAddr)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u4WPDestIpAddr =
            u4SetValFsOspfTeSasConstraintWPDestinationIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintMaxPathMetric
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintMaxPathMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintMaxPathMetric (INT4 i4FsOspfTeSasConstraintId,
                                          INT4
                                          i4SetValFsOspfTeSasConstraintMaxPathMetric)
{

    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u4MaxPathMetric =
            (UINT4) i4SetValFsOspfTeSasConstraintMaxPathMetric;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintMaxHopsInPath
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintMaxHopsInPath
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintMaxHopsInPath (INT4 i4FsOspfTeSasConstraintId,
                                          INT4
                                          i4SetValFsOspfTeSasConstraintMaxHopsInPath)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u2HopCount =
            (UINT2) i4SetValFsOspfTeSasConstraintMaxHopsInPath;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintBw
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintBw
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintBw (INT4 i4FsOspfTeSasConstraintId,
                               INT4 i4SetValFsOspfTeSasConstraintBw)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->bandwidth =
            (float) i4SetValFsOspfTeSasConstraintBw *OSPFTE_APP_THOUSAND_BYTES;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintIncludeAllSet
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintIncludeAllSet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintIncludeAllSet (INT4 i4FsOspfTeSasConstraintId,
                                          INT4
                                          i4SetValFsOspfTeSasConstraintIncludeAllSet)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u4IncludeAllSet =
            (UINT4) i4SetValFsOspfTeSasConstraintIncludeAllSet;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintIncludeAnySet
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintIncludeAnySet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintIncludeAnySet (INT4 i4FsOspfTeSasConstraintId,
                                          INT4
                                          i4SetValFsOspfTeSasConstraintIncludeAnySet)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u4IncludeAnySet =
            (UINT4) i4SetValFsOspfTeSasConstraintIncludeAnySet;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintExcludeAnySet
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintExcludeAnySet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintExcludeAnySet (INT4 i4FsOspfTeSasConstraintId,
                                          INT4
                                          i4SetValFsOspfTeSasConstraintExcludeAnySet)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u4ExcludeAnySet =
            (UINT4) i4SetValFsOspfTeSasConstraintExcludeAnySet;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintPriority
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintPriority (INT4 i4FsOspfTeSasConstraintId,
                                     INT4 i4SetValFsOspfTeSasConstraintPriority)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u1Priority =
            (UINT1) i4SetValFsOspfTeSasConstraintPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintExplicitRoute
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintExplicitRoute
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintExplicitRoute (INT4 i4FsOspfTeSasConstraintId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSetValFsOspfTeSasConstraintExplicitRoute)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    UINT1              *pCurrent = NULL;
    UINT1               u1Count;

    pCurrent = pSetValFsOspfTeSasConstraintExplicitRoute->pu1_OctetList;

    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->osTeExpRoute.u2Info = LGET2BYTE (pCurrent);

        pOsTeCspfCons->pCspfReq->osTeExpRoute.u2HopCount = LGET2BYTE (pCurrent);

        for (u1Count = 0;
             u1Count < pOsTeCspfCons->pCspfReq->osTeExpRoute.u2HopCount;
             u1Count++)
        {

            if (u1Count >= 16)
            {
                break;
            }
            pOsTeCspfCons->pCspfReq->osTeExpRoute.aNextHops[u1Count].
                u4RouterId = LGET4BYTE (pCurrent);
            pOsTeCspfCons->pCspfReq->osTeExpRoute.aNextHops[u1Count].
                u4NextHopIpAddr = LGET4BYTE (pCurrent);
            pOsTeCspfCons->pCspfReq->osTeExpRoute.aNextHops[u1Count].
                u4RemoteIdentifier = LGET4BYTE (pCurrent);
        }

        pOsTeCspfCons->pCspfReq->osTeExpRoute.u2HopCount = u1Count;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintSwitchingCapability
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintSwitchingCapability
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintSwitchingCapability (INT4
                                                i4FsOspfTeSasConstraintId,
                                                INT4
                                                i4SetValFsOspfTeSasConstraintSwitchingCapability)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u1SwitchingCap =
            (UINT1) i4SetValFsOspfTeSasConstraintSwitchingCapability;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintEncodingType
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintEncodingType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintEncodingType (INT4 i4FsOspfTeSasConstraintId,
                                         INT4
                                         i4SetValFsOspfTeSasConstraintEncodingType)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u1EncodingType =
            (UINT1) i4SetValFsOspfTeSasConstraintEncodingType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintLinkProtectionType
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintLinkProtectionType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintLinkProtectionType (INT4 i4FsOspfTeSasConstraintId,
                                               INT4
                                               i4SetValFsOspfTeSasConstraintLinkProtectionType)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u1ProtectionType =
            (UINT1) i4SetValFsOspfTeSasConstraintLinkProtectionType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintDiversity
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintDiversity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintDiversity (INT4 i4FsOspfTeSasConstraintId,
                                      INT4
                                      i4SetValFsOspfTeSasConstraintDiversity)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u1Diversity =
            (UINT1) i4SetValFsOspfTeSasConstraintDiversity;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintIndication
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintIndication
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintIndication (INT4 i4FsOspfTeSasConstraintId,
                                       INT4
                                       i4SetValFsOspfTeSasConstraintIndication)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u1Indication =
            (UINT1) i4SetValFsOspfTeSasConstraintIndication;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintFlag
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintFlag (INT4 i4FsOspfTeSasConstraintId,
                                 INT4 i4SetValFsOspfTeSasConstraintFlag)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == NOT_READY))
    {
        pOsTeCspfCons->pCspfReq->u1Flag =
            (UINT1) i4SetValFsOspfTeSasConstraintFlag;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsOspfTeSasConstraintStatus
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                setValFsOspfTeSasConstraintStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsOspfTeSasConstraintStatus (INT4 i4FsOspfTeSasConstraintId,
                                   INT4 i4SetValFsOspfTeSasConstraintStatus)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    tOsTeCspfReq       *pOsTeCspfReq = NULL;

    switch ((UINT1) i4SetValFsOspfTeSasConstraintStatus)
    {
        case CREATE_AND_GO:
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:
            if (OspfTeGetConstrainInfo
                ((UINT4) i4FsOspfTeSasConstraintId) == NULL)
            {
                if (OSPF_TE_CSPF_CONS_ALLOC (pOsTeCspfCons, tOsTeCspfCons) ==
                    NULL)
                {
                    OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC,
                                 "CspfCons Allocation Failure\n");
                    return SNMP_FAILURE;
                }
                if (OSPF_TE_CSPF_REQ_ALLOC (pOsTeCspfReq, tOsTeCspfReq) == NULL)
                {
                    OSPF_TE_CSPF_CONS_FREE (pOsTeCspfCons);
                    OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC,
                                 "CspfReq Allocation Failure\n");
                    return SNMP_FAILURE;
                }
                OSPF_TE_APP_MEMSET (pOsTeCspfCons, 0, sizeof (tOsTeCspfCons));
                OSPF_TE_APP_MEMSET (pOsTeCspfReq, 0, sizeof (tOsTeCspfReq));
                pOsTeCspfReq->u1Indication = 255;
                pOsTeCspfCons->pCspfReq = pOsTeCspfReq;
                pOsTeCspfCons->u4ConstraintId =
                    (UINT4) i4FsOspfTeSasConstraintId;
                pOsTeCspfCons->u4ConsStatus = NOT_READY;
                TMO_SLL_Add (&gOsTeAppConsLst, &(pOsTeCspfCons->NextConsNode));
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;
        case ACTIVE:
            /* Call the function provided by CSPF API */
            if (((pOsTeCspfCons = OspfTeGetConstrainInfo
                  ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
                (pOsTeCspfCons->u4ConsStatus == NOT_READY))
            {
                pOsTeCspfCons->u4ConsStatus =
                    (UINT4) i4SetValFsOspfTeSasConstraintStatus;
                OspfTeCspfFindPaths (pOsTeCspfCons->pCspfReq,
                                     pOsTeCspfCons->aPath);
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;
        case DESTROY:
            if ((pOsTeCspfCons = OspfTeGetConstrainInfo
                 ((UINT4) i4FsOspfTeSasConstraintId)) != NULL)
            {

                TMO_SLL_Delete (&gOsTeAppConsLst,
                                &(pOsTeCspfCons->NextConsNode));
                OSPF_TE_CSPF_REQ_FREE (pOsTeCspfCons->pCspfReq);
                OSPF_TE_CSPF_CONS_FREE (pOsTeCspfCons);
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintSourceIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintSourceIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintSourceIpAddr (UINT4 *pu4ErrorCode,
                                            INT4 i4FsOspfTeSasConstraintId,
                                            UINT4
                                            u4TestValFsOspfTeSasConstraintSourceIpAddr)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (OspfTeIsValidIpAddress
            (u4TestValFsOspfTeSasConstraintSourceIpAddr) == OSPF_TE_APP_TRUE)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintDestinationIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintDestinationIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintDestinationIpAddr (UINT4 *pu4ErrorCode,
                                                 INT4
                                                 i4FsOspfTeSasConstraintId,
                                                 UINT4
                                                 u4TestValFsOspfTeSasConstraintDestinationIpAddr)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (OspfTeIsValidIpAddress
            (u4TestValFsOspfTeSasConstraintDestinationIpAddr)
            == OSPF_TE_APP_TRUE)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintWPSourceIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintWPSourceIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintWPSourceIpAddr (UINT4 *pu4ErrorCode,
                                              INT4 i4FsOspfTeSasConstraintId,
                                              UINT4
                                              u4TestValFsOspfTeSasConstraintWPSourceIpAddr)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (OspfTeIsValidIpAddress
            (u4TestValFsOspfTeSasConstraintWPSourceIpAddr) == OSPF_TE_APP_TRUE)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintWPDestinationIpAddr
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintWPDestinationIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintWPDestinationIpAddr (UINT4 *pu4ErrorCode,
                                                   INT4
                                                   i4FsOspfTeSasConstraintId,
                                                   UINT4
                                                   u4TestValFsOspfTeSasConstraintWPDestinationIpAddr)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (OspfTeIsValidIpAddress
            (u4TestValFsOspfTeSasConstraintWPDestinationIpAddr)
            == OSPF_TE_APP_TRUE)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintMaxPathMetric
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintMaxPathMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintMaxPathMetric (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsOspfTeSasConstraintId,
                                             INT4
                                             i4TestValFsOspfTeSasConstraintMaxPathMetric)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (IS_VALID_TE_METRIC (i4TestValFsOspfTeSasConstraintMaxPathMetric))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintMaxHopsInPath
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintMaxHopsInPath
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintMaxHopsInPath (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsOspfTeSasConstraintId,
                                             INT4
                                             i4TestValFsOspfTeSasConstraintMaxHopsInPath)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (IS_VALID_MAX_HOPS_IN_PATH
            (i4TestValFsOspfTeSasConstraintMaxHopsInPath))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintBw
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintBw
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintBw (UINT4 *pu4ErrorCode,
                                  INT4 i4FsOspfTeSasConstraintId,
                                  INT4 i4TestValFsOspfTeSasConstraintBw)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (i4TestValFsOspfTeSasConstraintBw != OSPF_TE_APP_NULL)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintIncludeAllSet
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintIncludeAllSet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintIncludeAllSet (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsOspfTeSasConstraintId,
                                             INT4
                                             i4TestValFsOspfTeSasConstraintIncludeAllSet)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (i4TestValFsOspfTeSasConstraintIncludeAllSet != OSPF_TE_APP_NULL)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintIncludeAnySet
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintIncludeAnySet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintIncludeAnySet (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsOspfTeSasConstraintId,
                                             INT4
                                             i4TestValFsOspfTeSasConstraintIncludeAnySet)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (i4TestValFsOspfTeSasConstraintIncludeAnySet != OSPF_TE_APP_NULL)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintExcludeAnySet
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintExcludeAnySet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintExcludeAnySet (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsOspfTeSasConstraintId,
                                             INT4
                                             i4TestValFsOspfTeSasConstraintExcludeAnySet)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (i4TestValFsOspfTeSasConstraintExcludeAnySet != OSPF_TE_APP_NULL)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintPriority
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintPriority (UINT4 *pu4ErrorCode,
                                        INT4 i4FsOspfTeSasConstraintId,
                                        INT4
                                        i4TestValFsOspfTeSasConstraintPriority)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (IS_VALID_CONSTRAINT_PRIORITY
            (i4TestValFsOspfTeSasConstraintPriority))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintExplicitRoute
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintExplicitRoute
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintExplicitRoute (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4FsOspfTeSasConstraintId,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pTestValFsOspfTeSasConstraintExplicitRoute)
{
    UNUSED_PARAM (pTestValFsOspfTeSasConstraintExplicitRoute);
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintSwitchingCapability
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintSwitchingCapability
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintSwitchingCapability (UINT4 *pu4ErrorCode,
                                                   INT4
                                                   i4FsOspfTeSasConstraintId,
                                                   INT4
                                                   i4TestValFsOspfTeSasConstraintSwitchingCapability)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (IS_VALID_SWITCH_CAPABLE
            (i4TestValFsOspfTeSasConstraintSwitchingCapability))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintEncodingType
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintEncodingType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintEncodingType (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4FsOspfTeSasConstraintId,
                                            INT4
                                            i4TestValFsOspfTeSasConstraintEncodingType)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (IS_VALID_ENCODING_TYPE (i4TestValFsOspfTeSasConstraintEncodingType))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintLinkProtectionType
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintLinkProtectionType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintLinkProtectionType (UINT4 *pu4ErrorCode,
                                                  INT4
                                                  i4FsOspfTeSasConstraintId,
                                                  INT4
                                                  i4TestValFsOspfTeSasConstraintLinkProtectionType)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (IS_VALID_PROTECTION_TYPE
            (i4TestValFsOspfTeSasConstraintLinkProtectionType))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintDiversity
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintDiversity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintDiversity (UINT4 *pu4ErrorCode,
                                         INT4 i4FsOspfTeSasConstraintId,
                                         INT4
                                         i4TestValFsOspfTeSasConstraintDiversity)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (IS_VALID_DIVERSITY_VALUE (i4TestValFsOspfTeSasConstraintDiversity))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintIndication
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintIndication
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintIndication (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4FsOspfTeSasConstraintId,
                                          INT4
                                          i4TestValFsOspfTeSasConstraintIndication)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (IS_VALID_CONSTRAINT_INDICATION
            (i4TestValFsOspfTeSasConstraintIndication))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintFlag
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintFlag (UINT4 *pu4ErrorCode,
                                    INT4 i4FsOspfTeSasConstraintId,
                                    INT4 i4TestValFsOspfTeSasConstraintFlag)
{
    if (OspfTeGetConstrainInfo ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
    {
        if (IS_VALID_FLAG_VALUE (i4TestValFsOspfTeSasConstraintFlag))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsOspfTeSasConstraintStatus
 Input       :  The Indices
                FsOspfTeSasConstraintId

                The Object 
                testValFsOspfTeSasConstraintStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsOspfTeSasConstraintStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4FsOspfTeSasConstraintId,
                                      INT4 i4TestValFsOspfTeSasConstraintStatus)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;

    switch ((UINT1) i4TestValFsOspfTeSasConstraintStatus)
    {
        case CREATE_AND_GO:
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:
            if (((pOsTeCspfCons = OspfTeGetConstrainInfo
                  ((UINT4) i4FsOspfTeSasConstraintId)) == NULL))
            {
                return SNMP_SUCCESS;
            }
            UNUSED_PARAM (pOsTeCspfCons);
            break;
        case ACTIVE:
            if (((pOsTeCspfCons = OspfTeGetConstrainInfo
                  ((UINT4) i4FsOspfTeSasConstraintId)) != NULL) &&
                (pOsTeCspfCons->u4ConsStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            break;
        case DESTROY:
            if (OspfTeGetConstrainInfo
                ((UINT4) i4FsOspfTeSasConstraintId) != NULL)
            {
                return SNMP_SUCCESS;
            }
            break;
        case NOT_IN_SERVICE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsOspfTeSasConstraintTable
 Input       :  The Indices
                FsOspfTeSasConstraintId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsOspfTeSasConstraintTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsOspfTeSasCspfPathTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsOspfTeSasCspfPathTable
 Input       :  The Indices
                FsOspfTeSasCspfPathConstraintId
                FsOspfTeSasCspfPathType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsOspfTeSasCspfPathTable (INT4
                                                  i4FsOspfTeSasCspfPathConstraintId,
                                                  INT4
                                                  i4FsOspfTeSasCspfPathType)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    UINT1               u1Count;

    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasCspfPathConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == ACTIVE))
    {
        for (u1Count = 0; u1Count < 2; u1Count++)
        {
            if ((&pOsTeCspfCons->aPath[u1Count] != NULL) &&
                (pOsTeCspfCons->aPath[u1Count].u2PathType ==
                 i4FsOspfTeSasCspfPathType))
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsOspfTeSasCspfPathTable
 Input       :  The Indices
                FsOspfTeSasCspfPathConstraintId
                FsOspfTeSasCspfPathType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsOspfTeSasCspfPathTable (INT4
                                          *pi4FsOspfTeSasCspfPathConstraintId,
                                          INT4 *pi4FsOspfTeSasCspfPathType)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    UINT1               u1Count;

    TMO_SLL_Scan (&(gOsTeAppConsLst), pOsTeCspfCons, tOsTeCspfCons *)
    {
        for (u1Count = 0; u1Count < 2; u1Count++)
        {
            if (&pOsTeCspfCons->aPath[u1Count] != NULL)
            {
                *pi4FsOspfTeSasCspfPathConstraintId =
                    (INT4) pOsTeCspfCons->u4ConstraintId;
                *pi4FsOspfTeSasCspfPathType =
                    pOsTeCspfCons->aPath[u1Count].u2PathType;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsOspfTeSasCspfPathTable
 Input       :  The Indices
                FsOspfTeSasCspfPathConstraintId
                nextFsOspfTeSasCspfPathConstraintId
                FsOspfTeSasCspfPathType
                nextFsOspfTeSasCspfPathType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsOspfTeSasCspfPathTable (INT4
                                         i4FsOspfTeSasCspfPathConstraintId,
                                         INT4
                                         *pi4NextFsOspfTeSasCspfPathConstraintId,
                                         INT4 i4FsOspfTeSasCspfPathType,
                                         INT4 *pi4NextFsOspfTeSasCspfPathType)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    UINT1               u1Count;
    UINT1               u1Flag = OSPF_TE_APP_FALSE;

    TMO_SLL_Scan (&(gOsTeAppConsLst), pOsTeCspfCons, tOsTeCspfCons *)
    {
        for (u1Count = 0; u1Count < 2; u1Count++)
        {
            if (&pOsTeCspfCons->aPath[u1Count] != NULL)
            {
                if ((i4FsOspfTeSasCspfPathConstraintId ==
                     (INT4) pOsTeCspfCons->u4ConstraintId) &&
                    (i4FsOspfTeSasCspfPathType ==
                     (INT4) pOsTeCspfCons->aPath[u1Count].u2PathType))
                {
                    u1Flag = OSPF_TE_APP_TRUE;
                }
                else if ((u1Flag == OSPF_TE_APP_TRUE)
                         && (pOsTeCspfCons->aPath[u1Count].u2PathType != 0))
                {
                    *pi4NextFsOspfTeSasCspfPathConstraintId =
                        (INT4) pOsTeCspfCons->u4ConstraintId;
                    *pi4NextFsOspfTeSasCspfPathType =
                        (INT4) pOsTeCspfCons->aPath[u1Count].u2PathType;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasCspfPathNumHops
 Input       :  The Indices
                FsOspfTeSasCspfPathConstraintId
                FsOspfTeSasCspfPathType

                The Object 
                retValFsOspfTeSasCspfPathNumHops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasCspfPathNumHops (INT4
                                  i4FsOspfTeSasCspfPathConstraintId,
                                  INT4 i4FsOspfTeSasCspfPathType,
                                  INT4 *pi4RetValFsOspfTeSasCspfPathNumHops)
{

    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    UINT1               u1Count;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasCspfPathConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == ACTIVE))
    {
        for (u1Count = 0; u1Count < 2; u1Count++)
        {
            if ((&pOsTeCspfCons->aPath[u1Count] != NULL) &&
                (pOsTeCspfCons->aPath[u1Count].u2PathType ==
                 i4FsOspfTeSasCspfPathType))
            {
                *pi4RetValFsOspfTeSasCspfPathNumHops =
                    (INT4) pOsTeCspfCons->aPath[u1Count].u2HopCount;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasCspfPathRouterId
 Input       :  The Indices
                FsOspfTeSasCspfPathConstraintId
                FsOspfTeSasCspfPathType

                The Object 
                retValFsOspfTeSasCspfPathRouterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasCspfPathRouterId (INT4
                                   i4FsOspfTeSasCspfPathConstraintId,
                                   INT4 i4FsOspfTeSasCspfPathType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsOspfTeSasCspfPathRouterId)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    UINT1               u1Count;
    UINT2               u2Count;
    UINT1              *pPath =
        pRetValFsOspfTeSasCspfPathRouterId->pu1_OctetList;
    tIPADDR             routerId;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasCspfPathConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == ACTIVE))
    {
        for (u1Count = 0; u1Count < 2; u1Count++)
        {
            if ((&pOsTeCspfCons->aPath[u1Count] != NULL) &&
                (pOsTeCspfCons->aPath[u1Count].u2PathType ==
                 i4FsOspfTeSasCspfPathType))
            {
                for (u2Count = 0; u2Count < pOsTeCspfCons->aPath[u1Count].
                     u2HopCount; u2Count++)
                {
                    OSPF_CRU_BMC_DWTOPDU (routerId,
                                          pOsTeCspfCons->aPath[u1Count].
                                          aNextHops[u2Count].u4RouterId);
                    OSPF_TE_APP_MEMCPY ((pPath + (u2Count * 4)), routerId, 4);
                }
                pRetValFsOspfTeSasCspfPathRouterId->i4_Length =
                    ((pOsTeCspfCons->aPath[u1Count].u2HopCount) * 4);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasCspfPathNextHopIpAddress
 Input       :  The Indices
                FsOspfTeSasCspfPathConstraintId
                FsOspfTeSasCspfPathType

                The Object 
                retValFsOspfTeSasCspfPathNextHopIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasCspfPathNextHopIpAddress (INT4
                                           i4FsOspfTeSasCspfPathConstraintId,
                                           INT4
                                           i4FsOspfTeSasCspfPathType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pRetValFsOspfTeSasCspfPathNextHopIpAddress)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    UINT1               u1Count;
    UINT2               u2Count;
    UINT1              *pPath =
        pRetValFsOspfTeSasCspfPathNextHopIpAddress->pu1_OctetList;
    tIPADDR             routerId;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasCspfPathConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == ACTIVE))
    {
        for (u1Count = 0; u1Count < 2; u1Count++)
        {
            if ((&pOsTeCspfCons->aPath[u1Count] != NULL) &&
                (pOsTeCspfCons->aPath[u1Count].u2PathType ==
                 i4FsOspfTeSasCspfPathType))
            {
                for (u2Count = 0; u2Count < pOsTeCspfCons->aPath[u1Count].
                     u2HopCount; u2Count++)
                {
                    OSPF_CRU_BMC_DWTOPDU (routerId,
                                          pOsTeCspfCons->aPath[u1Count].
                                          aNextHops[u2Count].u4NextHopIpAddr);
                    OSPF_TE_APP_MEMCPY ((pPath + (u2Count * 4)), routerId, 4);
                }
                pRetValFsOspfTeSasCspfPathNextHopIpAddress->i4_Length =
                    ((pOsTeCspfCons->aPath[u1Count].u2HopCount) * 4);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsOspfTeSasCspfPathLocalIdentifier
 Input       :  The Indices
                FsOspfTeSasCspfPathConstraintId
                FsOspfTeSasCspfPathType

                The Object 
                retValFsOspfTeSasCspfPathLocalIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsOspfTeSasCspfPathLocalIdentifier (INT4
                                          i4FsOspfTeSasCspfPathConstraintId,
                                          INT4
                                          i4FsOspfTeSasCspfPathType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsOspfTeSasCspfPathLocalIdentifier)
{

    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    UINT1               u1Count;
    UINT2               u2Count;
    UINT1              *pPath =
        pRetValFsOspfTeSasCspfPathLocalIdentifier->pu1_OctetList;
    tIPADDR             routerId;
    if (((pOsTeCspfCons = OspfTeGetConstrainInfo
          ((UINT4) i4FsOspfTeSasCspfPathConstraintId)) != NULL) &&
        (pOsTeCspfCons->u4ConsStatus == ACTIVE))
    {
        for (u1Count = 0; u1Count < 2; u1Count++)
        {
            if ((&pOsTeCspfCons->aPath[u1Count] != NULL) &&
                (pOsTeCspfCons->aPath[u1Count].u2PathType ==
                 i4FsOspfTeSasCspfPathType))
            {
                for (u2Count = 0; u2Count < pOsTeCspfCons->aPath[u1Count].
                     u2HopCount; u2Count++)
                {
                    OSPF_CRU_BMC_DWTOPDU (routerId,
                                          pOsTeCspfCons->aPath[u1Count].
                                          aNextHops[u2Count].
                                          u4RemoteIdentifier);
                    OSPF_TE_APP_MEMCPY ((pPath + (u2Count * 4)), routerId, 4);
                    /*OSPF_TE_APP_MEMCPY ((pPath + (u2Count * 4)),
                       &pOsTeCspfCons->aPath[u1Count].
                       aNextHops[u2Count].u4RemoteIdentifier, 4); */
                }
                pRetValFsOspfTeSasCspfPathLocalIdentifier->i4_Length =
                    ((pOsTeCspfCons->aPath[u1Count].u2HopCount) * 4);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

PUBLIC tOsTeCspfCons *
OspfTeGetConstrainInfo (UINT4 u4ConstraintId)
{
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    TMO_SLL_Scan (&(gOsTeAppConsLst), pOsTeCspfCons, tOsTeCspfCons *)
    {
        if (pOsTeCspfCons->u4ConstraintId == u4ConstraintId)
        {
            return pOsTeCspfCons;
        }
    }
    return NULL;
}

/****************************************************************************
 Function    :  OspfTeIsValidIpAddress
 Input       :  The IP Address
 Description :  This is used validate the IP Adress in the SNMP test routines
 Returns     :  TRUE or FALSE
****************************************************************************/
PUBLIC UINT1
OspfTeIsValidIpAddress (UINT4 u4IpAddress)
{
    if (u4IpAddress == LTD_B_CAST_ADDR)
    {
        return (OSPF_TE_APP_FALSE);
    }

    if (!((IS_CLASS_A_ADDR (u4IpAddress)) ||
          (IS_CLASS_B_ADDR (u4IpAddress)) || (IS_CLASS_C_ADDR (u4IpAddress))))
    {
        return OSPF_TE_APP_FALSE;
    }

    if ((u4IpAddress & LOOP_BACK_ADDR_MASK) == LOOP_BACK_ADDRESES)
    {
        /* Address is Loop back adress of 127.x.x.x */
        return OSPF_TE_APP_FALSE;
    }

    /* Valid IP address can be assigned to router */
    return OSPF_TE_APP_TRUE;
}
