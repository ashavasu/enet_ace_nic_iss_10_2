/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostesub.c,v 1.2 2007/11/27 09:20:21 iss Exp $
 *
 * Description:  This file contains the routines for the construction  
 *               of Link TLV  and SUB TLVs.
 * *********************************************************************/

#include "osteinc.h"

PUBLIC UINT1       *OspfTeLinkTypeSubTLV (tOsTeInterface * pOsTeInt,
                                          UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeLinkIDSubTLV (tOsTeInterface * pOsTeInt,
                                        UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeLocalIPAdrSubTLV (tOsTeInterface * pOsTeInt,
                                            UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeRemoteIPAdrSubTLV (tOsTeInterface * pOsTeInt,
                                             UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeTEMetricSubTLV (tOsTeInterface * pOsTeInt,
                                          UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeMaxBwSubTLV (tOsTeInterface * pOsTeInt,
                                       UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeMaxResBwSubTLV (tOsTeInterface * pOsTeInt,
                                          UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeUnResBwSubTLV (tOsTeInterface * pOsTeInt,
                                         UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeAdmnGroupSubTLV (tOsTeInterface * pOsTeInt,
                                           UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeIdentifiersSubTLV (tOsTeInterface * pOsTeInt,
                                             UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeLinkProtectionSubTLV (tOsTeInterface * pOsTeInt,
                                                UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeIntSwCaDeSubTLV (tOsTeInterface * pOsTeInt,
                                           UINT1 *pCurrent);
PUBLIC UINT1       *OspfTeSRLGSubTLV (tOsTeInterface * pOsTeInt,
                                      UINT1 *pCurrent);

UINT1              *(*gasubTLV[]) (tOsTeInterface *, UINT1 *) =
{
NULL,
        OspfTeLinkTypeSubTLV,
        OspfTeLinkIDSubTLV,
        OspfTeLocalIPAdrSubTLV,
        OspfTeRemoteIPAdrSubTLV,
        OspfTeTEMetricSubTLV,
        OspfTeMaxBwSubTLV,
        OspfTeMaxResBwSubTLV,
        OspfTeUnResBwSubTLV,
        OspfTeAdmnGroupSubTLV,
        OspfTeIdentifiersSubTLV,
        OspfTeLinkProtectionSubTLV, OspfTeIntSwCaDeSubTLV, OspfTeSRLGSubTLV};

/*****************************************************************************/
/*  Function    :  OspfTeConstructLinkTlv                                    */
/*                                                                           */
/*  Description :  This function constructs Link TLV                         */
/*                                                                           */
/*  Input       :  pTeIfNode - Holds the interface information               */
/*                 u2TlvLen  - TLV length                                    */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Length of the Link TLV constructed.                       */
/*****************************************************************************/
PUBLIC UINT2
OspfTeConstructLinkTlv (tOsTeInterface * pTeIfNode, UINT1 *pTLVnode)
{
    UINT1               u1Counter;
    UINT1              *pCurrent = NULL;
    UINT1              *pLengthOffSet = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeConstructLinkTlv\n");

    pCurrent = pTLVnode;

    /* Filling Type value */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_LINK_TLV);

    pLengthOffSet = pCurrent;

    /* Filling with Zero intially */
    pCurrent = pCurrent + OSPF_TE_TLV_LEN_LEN;

    for (u1Counter = OSPF_TE_ONE; u1Counter <= OSPF_TE_MAX_SUB_TLVS;
         u1Counter++)
    {
        pCurrent = gasubTLV[u1Counter] (pTeIfNode, pCurrent);
    }

    /* Fill Length of the Link TLV */
    LADD2BYTE (pLengthOffSet,
               (UINT2) (pCurrent - pLengthOffSet - OSPF_TE_TLV_LEN_LEN));

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeConstructLinkTlv\n");

    /* Length of the Link TLV constructed */
    return ((UINT2) (pCurrent - pTLVnode));
}

/*****************************************************************************/
/*  Function    :  OspfTeLinkTypeSubTLV                                      */
/*                                                                           */
/*  Description :  This module constructs Link Type Sub TLV.                 */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeLinkTypeSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLinkTypeSubTLV\n");

    /* Filling SUB TLV Type */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_LINKTYPE);
    /* Filling SUB TLV length field */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_LINKTYPE_LEN);
    /* Filling LINK Type filed */
    LADD1BYTE (pCurrent, pOsTeInt->u1LinkType);
    /* Padding with zeros remaining three bytes */
    LADD3BYTE (pCurrent, (UINT4) OSPF_TE_ZERO);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLinkTypeSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeLinkIDSubTLV                                        */
/*                                                                           */
/*  Description :  This module constructs Link ID Sub TLV.                   */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeLinkIDSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLinkIDSubTLV\n");

    /* Filling SUB TLV Type */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_LINKID);
    /* Filling SUB TLV Length value */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_LINKID_LEN);
    /* Filling Link Id */
    LADDSTR (pCurrent, pOsTeInt->linkId, MAX_IP_ADDR_LEN);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLinkIDSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeLocalIPAdrSubTLV                                    */
/*                                                                           */
/*  Description :  This module constructs Local IP Address Sub TLV.          */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeLocalIPAdrSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLocalIPAdrSubTLV\n");

    if (OspfTeUtilIpAddrComp (pOsTeInt->localIpAddr, gOsTeNullIpAddr)
        == OSPF_TE_EQUAL)
    {
        return pCurrent;
    }

    /* Filling SUB TLV Type */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_LOCALIF);
    /* Filling SUB TLV Length */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_LOCALIF_LEN);
    /* Filling SUB TLV Value */
    LADDSTR (pCurrent, pOsTeInt->localIpAddr, MAX_IP_ADDR_LEN);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLocalIPAdrSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeRemoteIPAdrSubTLV                                   */
/*                                                                           */
/*  Description :  This module constructs Remote IP address Sub TLV.         */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeRemoteIPAdrSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeRemoteIPAdrSubTLV\n");

    if ((pOsTeInt->u1LinkType == OSPF_TE_MULTIACCESS) ||
        (OspfTeUtilIpAddrComp (pOsTeInt->remoteIpAddr, gOsTeNullIpAddr)
         == OSPF_TE_EQUAL))
    {
        return pCurrent;
    }

    /* Filling SUB TLV Type */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_REMOTEIF);
    /* Filling SUB TLV Length */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_REMOTEIF_LEN);
    /* Filling SUB TLV Value */
    LADDSTR (pCurrent, pOsTeInt->remoteIpAddr, MAX_IP_ADDR_LEN);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeRemoteIPAdrSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeTEMetricSubTLV                                      */
/*                                                                           */
/*  Description :  This module constructs TE Metric Sub TLV.                 */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeTEMetricSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeTEMetricSubTLV\n");

    /* Filling SUB TLV Type */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_METRIC);
    /* Filling SUB TLV Length */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_METRIC_LEN);
    /* Filling SUB TLV Value */
    LADD4BYTE (pCurrent, pOsTeInt->u4TeMetric);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeTEMetricSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeMaxBwSubTLV                                         */
/*                                                                           */
/*  Description :  This module constructs Maximum Bandwidth Sub TLV.         */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeMaxBwSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeMaxBwSubTLV\n");

    /* If the MAx Bandwidth is ZERO this sub TLV will not be generated */
    if (pOsTeInt->maxBw == (FLT4) OSPF_TE_ZERO)
    {
        return pCurrent;
    }

    /* Filling SUB TLV Type */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_MAXBW);
    /* Filling SUB TLV Length */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_MAXBW_LEN);
    /* Filling SUB TLV Value */
    LADDFLOAT (pCurrent, pOsTeInt->maxBw);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeMaxBwSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeMaxResBwSubTLV                                      */
/*                                                                           */
/*  Description :  This module constructs Link Type Sub TLV.                 */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeMaxResBwSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeMaxResBwSubTLV\n");

    /* If the Max Res Bandwidth is ZERO this sub TLV will not be generated */
    if (pOsTeInt->maxResBw == (FLT4) OSPF_TE_ZERO)
    {
        return pCurrent;
    }

    /* Filling SUB TLV Type */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_MAXRESBW);
    /* Filling SUB TLV Length */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_MAXRESBW_LEN);
    /* Filling SUB TLV Value */
    LADDFLOAT (pCurrent, pOsTeInt->maxResBw);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeMaxResBwSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeUnResBwSubTLV                                       */
/*                                                                           */
/*  Description :  This module constructs Max Reservable Bandwidth Sub TLV.  */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeUnResBwSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    UINT1               u1Priority;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeUnResBwSubTLV\n");

    /* If Bandwidth at all priority Levels is ZERO, then this sub 
     * TLV is not going to be generated. */
    for (u1Priority = OSPF_TE_ZERO; u1Priority < OSPF_TE_MAX_PRIORITY_LVL;
         u1Priority++)
    {
        if (pOsTeInt->aUnResBw[u1Priority] != OSPF_TE_ZERO)
        {
            break;
        }
    }

    if (u1Priority != OSPF_TE_MAX_PRIORITY_LVL)
    {
        /* Filling SUB TLV Type */
        LADD2BYTE (pCurrent, (UINT2) OSPF_TE_UNRESBW);
        /* Filling SUB TLV Length */
        LADD2BYTE (pCurrent, (UINT2) OSPF_TE_UNRESBW_LEN);
        /* Filling SUB TLV Value */
        for (u1Priority = OSPF_TE_ZERO; u1Priority < OSPF_TE_MAX_PRIORITY_LVL;
             u1Priority++)
        {
            LADDFLOAT (pCurrent, pOsTeInt->aUnResBw[u1Priority]);
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeUnResBwSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeAdmnGroupSubTLV                                     */
/*                                                                           */
/*  Description :  This module constructs Administrative Group Sub TLV.      */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeAdmnGroupSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeAdmnGroupSubTLV\n");

    if (pOsTeInt->u4RsrcClassColor != OSPF_TE_ZERO)
    {
        /* Filling SUB TLV Type */
        LADD2BYTE (pCurrent, (UINT2) OSPF_TE_RSRCLR);
        /* Filling SUB TLV Type */
        LADD2BYTE (pCurrent, (UINT2) OSPF_TE_RSRCLR_LEN);
        /* Filling SUB TLV Type */
        LADD4BYTE (pCurrent, pOsTeInt->u4RsrcClassColor);
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeAdmnGroupSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeIdentifiersSubTLV                                   */
/*                                                                           */
/*  Description :  This module constructs Identifiers Sub TLV.               */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeIdentifiersSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeIdentifiersSubTLV\n");

    /* If the local and remote identifier values are zero 
     * no need to generate the Sub TLV */
    if ((pOsTeInt->u4LocalIdentifier == OSPF_TE_ZERO) &&
        (pOsTeInt->u4RemoteIdentifier == OSPF_TE_ZERO))
    {
        return pCurrent;
    }

    /* Filling SUB TLV Type */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_IDENTIFIERS);
    /* Filling SUB TLV Length  */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_IDENTIFIERS_LEN);
    /* Filling SUB TLV value  -- Link local Identifier value */
    LADD4BYTE (pCurrent, pOsTeInt->u4LocalIdentifier);
    /* Filling SUB TLV value  -- Link Remote Identifier value */
    LADD4BYTE (pCurrent, pOsTeInt->u4RemoteIdentifier);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeIdentifiersSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeLinkProtectionSubTLV                                */
/*                                                                           */
/*  Description :  This module constructs Link protection Type Sub TLV.      */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeLinkProtectionSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLinkProtectionSubTLV\n");

    if (pOsTeInt->u1ProtectionType == OSPF_TE_ZERO)
    {
        return pCurrent;
    }

    /* Filling SUB TLV Type */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_PROTECTION_TYPE);
    /* Filling SUB TLV Type */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_PROTEC_TYPE_LEN);
    /* Filling SUB TLV Type */
    LADD1BYTE (pCurrent, pOsTeInt->u1ProtectionType);
    /* Padding remaining fields with zero */
    LADD3BYTE (pCurrent, (UINT4) OSPF_TE_ZERO);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLinkProtectionSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeIntSwCaDeSubTLV                                     */
/*                                                                           */
/*  Description :  This module constructs Switching capability Sub TLV.      */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeIntSwCaDeSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    UINT4               u4NoOfDesc;
    UINT1               u1Count;
    tOsTeIfDesc        *pOsTeIfDesc = NULL;
    UINT1              *pLengthOffset = NULL;
    tOsTeIfDescNode    *pOsTeIfDescNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeIntSwCaDeSubTLV\n");

    u4NoOfDesc = TMO_SLL_Count (&(pOsTeInt->ifDescrList));

    if (u4NoOfDesc == OSPF_TE_ZERO)
    {
        return pCurrent;
    }

    /* Generating SUB TLV for each interface descriptor */
    TMO_SLL_Scan (&(pOsTeInt->ifDescrList), pOsTeIfDescNode, tOsTeIfDescNode *)
    {
        pOsTeIfDesc = &(pOsTeIfDescNode->ifDesc);

        /* Filling SUB TLV Type */
        LADD2BYTE (pCurrent, (UINT2) OSPF_TE_INSWCA_DESC);
        /* Storing the location of SUB TLV Length field */
        pLengthOffset = pCurrent;
        pCurrent = pCurrent + SUB_TLV_LEN_LEN;
        /* Filling SUB TLV Value */
        LADD1BYTE (pCurrent, pOsTeIfDesc->u1SwitchingCap);
        LADD1BYTE (pCurrent, pOsTeIfDesc->u1EncodingType);
        LADD2BYTE (pCurrent, (UINT2) OSPF_TE_ZERO);

        for (u1Count = OSPF_TE_ZERO; u1Count < OSPF_TE_MAX_PRIORITY_LVL;
             u1Count++)
        {
            LADDFLOAT (pCurrent, pOsTeIfDesc->aDescMaxLSPBw[u1Count]);
        }

        switch (pOsTeIfDesc->u1SwitchingCap)
        {
            case OSPF_TE_PSC_1:
            case OSPF_TE_PSC_2:
            case OSPF_TE_PSC_3:
            case OSPF_TE_PSC_4:
                /* Filling Switching capability information */
                LADDFLOAT (pCurrent, pOsTeIfDesc->minLSPBw);
                LADD2BYTE (pCurrent, pOsTeIfDesc->u2MTU);
                LADD2BYTE (pCurrent, (UINT2) OSPF_TE_ZERO);
                break;

            case OSPF_TE_TDM:
                /* Filling Switching capability information */
                LADDFLOAT (pCurrent, pOsTeIfDesc->minLSPBw);
                LADD1BYTE (pCurrent, pOsTeIfDesc->u1Indication);
                LADD3BYTE (pCurrent, (UINT4) OSPF_TE_ZERO);
                break;

            default:
                /* For other switching types, there is no switching
                 * capability information */
                break;
        }
        /* Filling The legnth field of the Sub TLV */
        /* pCurrent      -- Pointing to the next location to the end 
         *                  SUB TLV. */
        /* pLengthOffset -- Pointing to Length filed of the SUB TLV */
        LADD2BYTE (pLengthOffset,
                   (UINT2) (pCurrent - pLengthOffset - SUB_TLV_LEN_LEN));
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeIntSwCaDeSubTLV\n");

    return pCurrent;
}

/*****************************************************************************/
/*  Function    :  OspfTeSRLGSubTLV                                          */
/*                                                                           */
/*  Description :  This module constructs SRLG Sub TLV.                      */
/*                                                                           */
/*  Input       :  pOsTeInt - Holds the interface information                */
/*                 pCurrent - Pointing to the buffer into which the created  */
/*                            Sub TLV is going to be filled.                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the location next to the end of the            */
/*                 constructed sub TLV.                                      */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeSRLGSubTLV (tOsTeInterface * pOsTeInt, UINT1 *pCurrent)
{
    UINT4               u4Count;
    UINT2               u2SrlgLen;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeSRLGSubTLV\n");

    /* If no SRLG numbers are configured no need to generate 
     * this SUB TLV */
    if (pOsTeInt->osTeSrlg.u4NoOfSrlg == OSPF_TE_ZERO)
    {
        return pCurrent;
    }

    /* Filling SUB TLV Type */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_SRLG);
    u2SrlgLen = (UINT2) (pOsTeInt->osTeSrlg.u4NoOfSrlg * MAX_IP_ADDR_LEN);
    /* Filling SUB TLV Length */
    LADD2BYTE (pCurrent, u2SrlgLen);
    /* Filling SUB TLV Value */
    for (u4Count = OSPF_TE_ZERO; u4Count < pOsTeInt->osTeSrlg.u4NoOfSrlg;
         u4Count++)
    {
        LADD4BYTE (pCurrent, pOsTeInt->osTeSrlg.aSrlgNumber[u4Count]);
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeSRLGSubTLV\n");

    return pCurrent;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostesub.c                      */
/*-----------------------------------------------------------------------*/
