/******************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostetlm.c,v 1.3 2012/07/24 09:43:58 siva Exp $
 *
 * Description:  This file contains the routines for the interaction 
 *               with TLM.
 * ***************************************************************************/

#include "osteinc.h"
/*****************************************************************************/
/*  Function    :  OspfTeProcessTlmMsg                                       */
/*                                                                           */
/*  Description :  This function processes the message received from TLM     */
/*                 It calls the OspfTeProcessTlmLinkInfo to Process the      */
/*                 Link information                                          */
/*                                                                           */
/*  Input       :  pTlmMsg - Holds the message from TLM                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessTlmMsg (tOsTeLinkMsg * pTlmMsg)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessTlmMsg\n");
    switch (pTlmMsg->u1MsgSubType)
    {
        case OSPF_TE_TLM_MODULE_UP:
            /* Register OSPF-TE with TLM */
            if (OspfTeSndMsgToTlm (OSPF_TE_TLM_REG) != OSPF_TE_SUCCESS)
            {
                gOsTeContext.u1TlmRegister = OSPF_TE_FALSE;
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                             "Registration with TLM failed\n");
                return;
            }
            gOsTeContext.u1TlmRegister = OSPF_TE_TRUE;
            break;
        case OSPF_TE_TLM_LINK_INFO:
            if (gOsTeContext.u1TlmRegister != OSPF_TE_TRUE)
            {
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Receiving Msg from "
                             "Module that is not Registered\n");
                break;
            }
            OspfTeProcessTlmLinkInfo (pTlmMsg);
            break;
        case OSPF_TE_TLM_MODULE_DOWN:
            OspfTeProcessDeReg ();
            gOsTeContext.u1TlmRegister = OSPF_TE_FALSE;
            break;
        default:
            OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Unknown Message is received "
                         "from TLM\n");
            break;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessTlmMsg\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessTlmLinkInfo                                   */
/*                                                                           */
/*  Description :  This function processes the TE link information received  */
/*                 from TLM                                                  */
/*                                                                           */
/*  Input       :  pTlmOsTeLinkMsg - Points to the Resource Manager Message   */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessTlmLinkInfo (tOsTeLinkMsg * pTlmOsTeLinkMsg)
{
    tIPADDR             linkIpAddr;
    tOsTeInterface     *pOsTeInt = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessTlmLinkInfo\n");

    OSPF_TE_CRU_BMC_DWTOPDU (linkIpAddr, pTlmOsTeLinkMsg->u4LocalIpAddr);

    pOsTeInt = OspfTeFindTeInterfaceWithIndex (pTlmOsTeLinkMsg->u4IfIndex);

    if (pOsTeInt == NULL)
    {
        switch (pTlmOsTeLinkMsg->u1LinkStatus)
        {
            case OSPF_TE_LINK_UPDATE:
                /* Intentional Fall through */
            case OSPF_TE_LINK_CREATE:
                OspfTeCreateTeLink (pTlmOsTeLinkMsg);
                break;
                break;
            case OSPF_TE_LINK_DELETE:
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Received Delete Link "
                             "Info message for non existing Link\n");
                break;
            default:
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Received Unknown Type Link "
                             "Info message for non existing Link\n");
                break;
        }
    }
    else
    {
        switch (pTlmOsTeLinkMsg->u1LinkStatus)
        {
            case OSPF_TE_LINK_CREATE:
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Received Create Link "
                             "Info message for existing Link\n");
                break;
            case OSPF_TE_LINK_UPDATE:
                OspfTeUpdateTeLink (pTlmOsTeLinkMsg, pOsTeInt);
                break;
            case OSPF_TE_LINK_DELETE:
                OspfTeFlushTeLink (pOsTeInt);
                break;
            default:
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Received Unknown Type Link "
                             "Info message for existing Link\n");
                break;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessTlmLinkInfo\n");

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostetlm.c                      */
/*-----------------------------------------------------------------------*/
