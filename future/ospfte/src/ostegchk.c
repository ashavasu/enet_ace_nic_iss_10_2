/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: ostegchk.c,v 1.2 2016/04/21 10:36:43 siva Exp $
*
* Description:  This file has the routines to check the constraints.
*********************************************************************/

#include "osteinc.h"

/*****************************************************************************/
/* Function     : OspfTeCspfChkProtectionType                                */
/*                                                                           */
/* Description  : This function return OSPF_TE_SUCCESS if link protection    */
/*                is greater or equal to the protection type specified       */
/*                by CSPF request.                                           */
/*                                                                           */
/* Input        : pCspfReq - pointer to CSPF request.                        */
/*                pLinkTlvNode - pointer to Link TLV node.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/
PUBLIC INT4
OspfTeCspfChkProtectionType (tOsTeCspfReq * pCspfReq,
                             tOsTeLinkTlvNode * pLinkTlvNode)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkProtectionType\n");

    if (pLinkTlvNode->u1ProtectionType >= pCspfReq->u1ProtectionType)
    {
        return OSPF_TE_SUCCESS;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkProtectionType\n");

    return OSPF_TE_FAILURE;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkIfDescrConstrain                              */
/*                                                                           */
/* Description  : This function return OSPF_TE_SUCCESS if interface          */
/*                descriptor parameter satisfies the constrain whatever      */
/*                specified by CSPF request                                  */
/*                                                                           */
/* Input        : pCspfReq - pointer to CSPF request.                        */
/*                pLinkTlvNode - Holds link TLV information                  */
/*                u4RouterId - Router ID of the link.                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfChkIfDescrConstrain (tOsTeCspfReq * pCspfReq,
                               tOsTeLinkTlvNode * pLinkTlvNode,
                               UINT4 u4RouterId)
{
    tOsTeIfDescNode    *pIfDescrNode = NULL;
    tOsTeIfDesc        *pIfDescr = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkIfDescrConstrain\n");

    /* Scan Interface descriptor List */
    TMO_SLL_Scan (&pLinkTlvNode->ifDescrLst, pIfDescrNode, tOsTeIfDescNode *)
    {
        pIfDescr = &(pIfDescrNode->ifDesc);

        if (pCspfReq->u1SwitchingCap != OSPF_TE_NULL)
        {
            /* if switching capability does not match consider next
               descriptor */

            if (((gOsTeContext.u4SrcIpAddr == u4RouterId) &&
                 (pLinkTlvNode->u4LinkId == gOsTeContext.u4DestIpAddr)) ||
                ((gOsTeContext.u4DestIpAddr == u4RouterId) &&
                 (pLinkTlvNode->u4LinkId == gOsTeContext.u4SrcIpAddr)))
            {
                if (pIfDescr->u1SwitchingCap != pCspfReq->u1SwitchingCap)
                {
                    continue;
                }
            }

            if (IS_BI_DIRECTION_PATH (pCspfReq->u1Flag))
            {
                if (((IS_SEGMENT_PROTECTION_PATH (pCspfReq->u1Flag)) &&
                     ((pCspfReq->u4WPSrcIpAddr != u4RouterId) &&
                      (pCspfReq->u4WPDestIpAddr != u4RouterId))) ||
                    ((!IS_SEGMENT_PROTECTION_PATH (pCspfReq->u1Flag)) &&
                     ((gOsTeContext.u4SrcIpAddr != u4RouterId) &&
                      (gOsTeContext.u4DestIpAddr != u4RouterId))))
                {
                    if (pIfDescr->u1SwitchingCap != pCspfReq->u1SwitchingCap)
                    {
                        continue;
                    }
                }
            }
        }
        if (pCspfReq->u1EncodingType != OSPF_TE_NULL)
        {
            if (((IS_SEGMENT_PROTECTION_PATH (pCspfReq->u1Flag)) &&
                 ((pCspfReq->u4WPSrcIpAddr == u4RouterId) ||
                  (pCspfReq->u4WPDestIpAddr == u4RouterId))) ||
                ((!IS_SEGMENT_PROTECTION_PATH (pCspfReq->u1Flag)) &&
                 ((gOsTeContext.u4SrcIpAddr == u4RouterId) ||
                  (gOsTeContext.u4DestIpAddr == u4RouterId))))
            {
                /* if Encoding type does not match consider next descriptor */
                if (pIfDescr->u1EncodingType != pCspfReq->u1EncodingType)
                {
                    continue;
                }
            }
        }
        if ((pCspfReq->u1SwitchingCap == OSPF_TE_TDM) &&
            (pCspfReq->u1Indication != OSPF_TE_INVALID_INDICATION))
        {
            /* if Indication type does not match consider next descriptor */
            if (pIfDescr->u1Indication != pCspfReq->u1Indication)
            {
                continue;
            }
        }
        if (pCspfReq->bandwidth != OSPF_TE_NULL)
        {

            if (IS_OSPF_TE_PSC (pIfDescr->u1SwitchingCap))
            {
                /* Bandwidth requested by application should be less than
                   Maximum  Lsp Bandwidth  and greater than Minimum Lsp
                   Bandwidth */
                if ((IS_MAX_LSP_BW_GREATER_THAN_CSPF_BW (pIfDescr, pCspfReq)) &&
                    (IS_MIN_LSP_BW_LESS_THAN_CSPF_BW (pIfDescr, pCspfReq)))
                {
                    return OSPF_TE_SUCCESS;
                }
            }

            else if (pIfDescr->u1SwitchingCap == OSPF_TE_TDM)
            {
                if (pIfDescr->u1Indication == OSPF_TE_STANDARD_SDH)
                {
                    if ((IS_MAX_LSP_BW_GREATER_THAN_CSPF_BW
                         (pIfDescr, pCspfReq)) &&
                        (IS_MIN_LSP_BW_LESS_THAN_CSPF_BW (pIfDescr, pCspfReq)))
                    {
                        return OSPF_TE_SUCCESS;
                    }
                }
                else
                {
                    if ((IS_MAX_LSP_BW_GREATER_THAN_CSPF_BW
                         (pIfDescr, pCspfReq)) &&
                        (IS_MIN_LSP_BW_LESS_THAN_CSPF_BW (pIfDescr, pCspfReq))
                        &&
                        (IS_MIN_LSP_BW_MULTILE_OF_CSPF_BW (pIfDescr, pCspfReq)))
                    {
                        return OSPF_TE_SUCCESS;
                    }
                }
            }
            else
            {
                if (IS_MAX_LSP_BW_GREATER_THAN_CSPF_BW (pIfDescr, pCspfReq))
                {
                    return OSPF_TE_SUCCESS;
                }
            }
            continue;
        }
        return OSPF_TE_SUCCESS;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkIfDescrConstrain\n");

    return OSPF_TE_FAILURE;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkSrlgDisjoint                                  */
/*                                                                           */
/* Description  : This function checks whether backup path is SRLG Disjoint  */
/*                or not.                                                    */
/*                                                                           */
/* Input        : pPrimaryPath  - pointer to Primary Path.                   */
/*                pBackupPath   - pointer to Backup Path.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfChkSrlgDisjoint (tOsTePath * pPrimaryPath, tOsTePath * pBackupPath)
{
    UINT2               u2PrimaryCount;
    UINT4               u4PrimarySrlgCount;
    UINT2               u2BackupCount;
    UINT4               u4BackupSrlgCount;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkSrlgDisjoint\n");

    for (u2PrimaryCount = OSPF_TE_NULL;
         u2PrimaryCount < pPrimaryPath->u2HopCount; u2PrimaryCount++)
    {
        for (u2BackupCount = OSPF_TE_NULL;
             u2BackupCount < pBackupPath->u2HopCount; u2BackupCount++)
        {
            for (u4PrimarySrlgCount = OSPF_TE_NULL;
                 u4PrimarySrlgCount <
                 pPrimaryPath->aSrlg[u2PrimaryCount].u4NoOfSrlg;
                 u4PrimarySrlgCount++)
            {
                for (u4BackupSrlgCount = OSPF_TE_NULL;
                     u4BackupSrlgCount <
                     pBackupPath->aSrlg[u2BackupCount].u4NoOfSrlg;
                     u4BackupSrlgCount++)
                {
                    if (pPrimaryPath->aSrlg[u2PrimaryCount].
                        aSrlgNumber[u4PrimarySrlgCount] ==
                        pBackupPath->aSrlg[u2BackupCount].
                        aSrlgNumber[u4BackupSrlgCount])
                    {
                        return OSPF_TE_FAILURE;
                    }
                }
            }
        }
    }

    if (pPrimaryPath->u2HopCount == pBackupPath->u2HopCount)
    {
        if (IS_OSPF_TE_SAME_PATH (pPrimaryPath, pBackupPath))
        {
            return OSPF_TE_FAILURE;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkSrlgDisjoint\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkEncodingType                                  */
/*                                                                           */
/* Description  : This function Checks whether ingress router or egress      */
/*                router encoding type is same or not.                       */
/*                                                                           */
/* Input        : pVertexPath  - pointer to Vertex path.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfChkEncodingType (tOsTePath * pVertexPath, tOsTeCspfReq * pCspfReq)
{
    tOsTeIfDescNode    *pIfDescrNode = NULL;
    tOsTeIfDesc        *pIfDescr = NULL;
    tOsTeIfDescNode    *pIfNextDescrNode = NULL;
    tOsTeIfDesc        *pNextIfDescr = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkEncodingType\n");

    if (IS_SEGMENT_PROTECTION_PATH (pCspfReq->u1Flag))
    {
        if ((pCspfReq->u4DestIpAddr == pCspfReq->u4WPDestIpAddr)
            && (pCspfReq->u1EncodingType != OSPF_TE_NULL))
        {
            if (IS_BI_DIRECTION_PATH (pCspfReq->u1Flag))
            {
                if (pVertexPath->pOsTeDestLinkTlv == NULL)
                {
                    return OSPF_TE_FAILURE;
                }
            }
            else
            {
                if (pVertexPath->pOsTeDestLinkTlv == NULL)
                {
                    return OSPF_TE_SUCCESS;
                }
            }

            TMO_SLL_Scan (&pVertexPath->pOsTeDestLinkTlv->ifDescrLst,
                          pIfNextDescrNode, tOsTeIfDescNode *)
            {
                pNextIfDescr = &(pIfNextDescrNode->ifDesc);
                if (pCspfReq->u1EncodingType == pNextIfDescr->u1EncodingType)
                {
                    return OSPF_TE_SUCCESS;
                }
            }
            return OSPF_TE_FAILURE;
        }
        else
        {
            return OSPF_TE_SUCCESS;
        }
    }

    if (IS_BI_DIRECTION_PATH (pCspfReq->u1Flag))
    {
        if ((pVertexPath->pOsTeSrcLinkTlv == NULL) ||
            (pVertexPath->pOsTeDestLinkTlv == NULL))
        {
            return OSPF_TE_FAILURE;
        }
    }
    else
    {
        if (pVertexPath->pOsTeDestLinkTlv == NULL)
        {
            return OSPF_TE_SUCCESS;
        }
    }

    if ((TMO_SLL_Count (&pVertexPath->pOsTeSrcLinkTlv->ifDescrLst)
         == OSPF_TE_NULL) &&
        (TMO_SLL_Count (&pVertexPath->pOsTeDestLinkTlv->ifDescrLst)
         == OSPF_TE_NULL))
    {
        return OSPF_TE_SUCCESS;
    }
    if ((TMO_SLL_Count (&pVertexPath->pOsTeSrcLinkTlv->ifDescrLst)
         == OSPF_TE_NULL) ||
        (TMO_SLL_Count (&pVertexPath->pOsTeDestLinkTlv->ifDescrLst)
         == OSPF_TE_NULL))
    {
        return OSPF_TE_FAILURE;
    }
    TMO_SLL_Scan (&pVertexPath->pOsTeSrcLinkTlv->ifDescrLst, pIfDescrNode,
                  tOsTeIfDescNode *)
    {
        pIfDescr = &(pIfDescrNode->ifDesc);
        TMO_SLL_Scan (&pVertexPath->pOsTeDestLinkTlv->ifDescrLst,
                      pIfNextDescrNode, tOsTeIfDescNode *)
        {
            pNextIfDescr = &(pIfNextDescrNode->ifDesc);
            if (pCspfReq->u1EncodingType == OSPF_TE_NULL)
            {
                if (pIfDescr->u1EncodingType == pNextIfDescr->u1EncodingType)
                {
                    pVertexPath->u1EncodingType = pIfDescr->u1EncodingType;
                    return OSPF_TE_SUCCESS;
                }
            }
            else
            {
                if ((pIfDescr->u1EncodingType == pNextIfDescr->u1EncodingType)
                    && (pCspfReq->u1EncodingType == pIfDescr->u1EncodingType))
                {
                    pVertexPath->u1EncodingType = pIfDescr->u1EncodingType;
                    return OSPF_TE_SUCCESS;
                }
            }

        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkEncodingType\n");

    return OSPF_TE_FAILURE;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkSwithingCap                                   */
/*                                                                           */
/* Description  : This function checks whether switching capability is same  */
/*                or not.                                                    */
/*                                                                           */
/* Input        : pOsTeBackLinkTlv - pointer to Link Tlv Node.               */
/*              : pOsTeNextLinkTlv - pointer to Link Tlv Node.               */
/*              : pCspfReq  - pointer to CSPF request.                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/
PUBLIC INT4
OspfTeCspfChkSwithingCap (tOsTeLinkTlvNode * pOsTeBackLinkTlv,
                          tOsTeLinkTlvNode * pOsTeNextLinkTlv,
                          tOsTeCspfReq * pCspfReq, tRouterId rtrId)
{
    tOsTeIfDescNode    *pIfNextDescrNode = NULL;
    tOsTeIfDesc        *pNextIfDescr = NULL;
    tOsTeIfDescNode    *pIfBackDescrNode = NULL;
    tOsTeIfDesc        *pBackIfDescr = NULL;
    UINT4               u4RouterId = OSPF_TE_NULL;
    UINT4               u4Flag = OSPF_TE_NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkSwithingCap\n");

    u4RouterId = OSPF_TE_CRU_BMC_DWFROMPDU (rtrId);

    if (pOsTeNextLinkTlv == NULL)
    {
        if (pCspfReq->u1SwitchingCap != OSPF_TE_NULL)
        {
            TMO_SLL_Scan (&pOsTeBackLinkTlv->ifDescrLst, pIfBackDescrNode,
                          tOsTeIfDescNode *)
            {
                pBackIfDescr = &(pIfBackDescrNode->ifDesc);
                if (pBackIfDescr->u1SwitchingCap == pCspfReq->u1SwitchingCap)
                {
                    return OSPF_TE_SUCCESS;
                }
            }
            return OSPF_TE_FAILURE;
        }
        return OSPF_TE_SUCCESS;
    }

    else
    {
        if (pCspfReq->u1SwitchingCap == OSPF_TE_NULL)
        {
            /* If both links does not have Interface descriptor */
            if ((TMO_SLL_Count (&pOsTeBackLinkTlv->ifDescrLst) == OSPF_TE_NULL)
                && (TMO_SLL_Count (&pOsTeNextLinkTlv->ifDescrLst) ==
                    OSPF_TE_NULL))
            {
                return OSPF_TE_SUCCESS;
            }
            /* If one link does not have Interface descriptor */
            else if ((TMO_SLL_Count (&pOsTeBackLinkTlv->ifDescrLst)
                      == OSPF_TE_NULL) ||
                     (TMO_SLL_Count (&pOsTeNextLinkTlv->ifDescrLst)
                      == OSPF_TE_NULL))
            {
                return OSPF_TE_FAILURE;
            }
            return OSPF_TE_SUCCESS;
        }
        else
        {
            TMO_SLL_Scan (&pOsTeNextLinkTlv->ifDescrLst, pIfNextDescrNode,
                          tOsTeIfDescNode *)
            {
                pNextIfDescr = &(pIfNextDescrNode->ifDesc);
                if (pNextIfDescr->u1SwitchingCap == pCspfReq->u1SwitchingCap)
                {
                    return OSPF_TE_SUCCESS;
                }
                else
                {
                     u4Flag = 1;
                     continue;
                }
            }
            if (u4Flag == 1)
            {
                return OSPF_TE_FAILURE;
            }

            if (((IS_SEGMENT_PROTECTION_PATH (pCspfReq->u1Flag))
                 && (u4RouterId == pCspfReq->u4WPDestIpAddr)) ||
                ((!IS_SEGMENT_PROTECTION_PATH (pCspfReq->u1Flag)) &&
                 (u4RouterId == gOsTeContext.u4DestIpAddr)))
            {
                return OSPF_TE_SUCCESS;
            }

        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkSwithingCap\n");

    return OSPF_TE_FAILURE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostegchk.c                     */
/*-----------------------------------------------------------------------*/
