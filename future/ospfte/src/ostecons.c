/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: ostecons.c,v 1.14 2016/02/21 09:48:28 siva Exp $
*
* Description:  This file has the routines check for the constraints
*               for each link.
*********************************************************************/

#include "osteinc.h"

/*****************************************************************************/
/* Function     : OspfTeCspfChkBwConstrain                                   */
/*                                                                           */
/* Description  : This function return OSPF_TE_SUCCESS if link unreserved    */
/*                bandwidth at particular priority is greater or equal to   */
/*                the bandwidth specified by CSPF request.                   */
/*                                                                           */
/* Input        : pCspfReq - pointer to CSPF request.                        */
/*                pLinkTlvNode - pointer to Link TLV node.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/
PUBLIC INT4
OspfTeCspfChkBwConstrain (tOsTeCspfReq * pCspfReq,
                          tOsTeLinkTlvNode * pLinkTlvNode)
{
    tBandWidth          tempBandWidth = 0.0;
    UINT4               u4NextHopOrRemoteId = OSPF_TE_ZERO;
    UINT4               u4AvlRemoteIpOrId = OSPF_TE_ZERO;
    UINT4               u4AvlLocalIpOrId = OSPF_TE_ZERO;
    UINT2               u2Count = OSPF_TE_ZERO;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkBwConstrain\n");

    if (IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag))
    {
        for (u2Count = OSPF_TE_ZERO;
             u2Count < pCspfReq->osTeExpRoute.u2HopCount; u2Count++)
        {
            if (pLinkTlvNode->u4LocalIpAddr != OSPF_TE_ZERO)
            {
                u4NextHopOrRemoteId =
                    pCspfReq->osTeExpRoute.aNextHops[u2Count].u4NextHopIpAddr;
	        u4AvlLocalIpOrId = pLinkTlvNode->u4LocalIpAddr;
                if (pLinkTlvNode->u1LinkType == OSPF_TE_MULTIACCESS)
                {
                    u4AvlRemoteIpOrId = pLinkTlvNode->u4LinkId;
                }
	        else
		{
		    u4AvlRemoteIpOrId = pLinkTlvNode->u4RemoteIpAddr;
                }
            }
            else if (pLinkTlvNode->u4LocalIdentifier != OSPF_TE_ZERO)
            {
                u4NextHopOrRemoteId =
                    pCspfReq->osTeExpRoute.aNextHops[u2Count].
                    u4RemoteIdentifier;
                u4AvlRemoteIpOrId = pLinkTlvNode->u4RemoteIdentifier;
                u4AvlLocalIpOrId = pLinkTlvNode->u4LocalIdentifier;
            }

            if (IS_BI_DIRECTION_PATH (pCspfReq->u1Flag))
            {
                if ((u4NextHopOrRemoteId == u4AvlRemoteIpOrId) ||
                    (u4NextHopOrRemoteId == u4AvlLocalIpOrId))
                {
                    tempBandWidth
                        = pLinkTlvNode->aUnResrvBw[pCspfReq->u1Priority] +
                        pCspfReq->oldbandwidth;
                    break;
                }
	    /*When Unnumbered interfaces with MBB enable*/
	    else
    	    {	
        	tempBandWidth = pLinkTlvNode->aUnResrvBw[pCspfReq->u1Priority];
		
    	    }

            }
            else if (u4NextHopOrRemoteId == u4AvlRemoteIpOrId)
            {
                tempBandWidth =
                    pLinkTlvNode->aUnResrvBw[pCspfReq->u1Priority] +
                    pCspfReq->oldbandwidth;
                break;
            }
	    else
    	    {	
        	tempBandWidth = pLinkTlvNode->aUnResrvBw[pCspfReq->u1Priority];
		
    	    }

        }
    }
    else
    {
        tempBandWidth = pLinkTlvNode->aUnResrvBw[pCspfReq->u1Priority];
    }

    if (tempBandWidth >= pCspfReq->bandwidth)
    {
        return OSPF_TE_SUCCESS;
    }

    OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "UnReserved bandwidth at requested "
                 "Priority is low than the requested bandwidth\n");
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkBwConstrain\n");
    return OSPF_TE_FAILURE;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkAdminConstrain                                */
/*                                                                           */
/* Description  : This function return OSPF_TE_SUCCESS if link Resource      */
/*                class satisfies the admin constrains  specified            */
/*                by CSPF request.                                           */
/*                                                                           */
/* Input        : pCspfReq - pointer to CSPF request                         */
/*                pLinkTlvNode - pointer to Link Tlv Node                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/
PUBLIC INT4
OspfTeCspfChkAdminConstrain (tOsTeCspfReq * pCspfReq,
                             tOsTeLinkTlvNode * pLinkTlvNode)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkAdminConstrain\n");

    /*
     * RFC: 3209, Section:4.7.4 
     * */


    /* Include-all
     * (include-all == 0) | (((link-attr & include-all) ^ includeall) == 0)*/
    if ((pCspfReq->u4IncludeAllSet != OSPF_TE_NULL) ||
        (pCspfReq->u1RsrcSetType == OSPF_TE_RSRC_TYPE_INCLUDE_ALL))
    {
        if (((pLinkTlvNode->u4RsrcClassColor & pCspfReq->u4IncludeAllSet)^(pCspfReq->u4IncludeAllSet))
            != OSPF_TE_NULL)
        {
            OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
                          "Link does not satisfies includeAllSet admin \n"
                          "constrain link RsrcClassColor: %d \n"
                          "CSPF IncludeAllSet: %d\n",
                          pLinkTlvNode->u4RsrcClassColor,
                          pCspfReq->u4IncludeAllSet);
            return OSPF_TE_FAILURE;
        }
    }

    /* Include-any
     * (include-any == 0) | ((link-attr & include-any) != 0) */
    if ((pCspfReq->u4IncludeAnySet != OSPF_TE_NULL) ||
        (pCspfReq->u1RsrcSetType == OSPF_TE_RSRC_TYPE_INCLUDE_ANY))
    {
        if ((pLinkTlvNode->u4RsrcClassColor & pCspfReq->u4IncludeAnySet)
            == OSPF_TE_NULL)
        {
            OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
                          "Link does not satisfies includeAnySet Admin \n"
                          "Constrain link RsrcClassColor: %d \n"
                          "CSPF IncludeAnySet: %d\n",
                          pLinkTlvNode->u4RsrcClassColor,
                          pCspfReq->u4IncludeAnySet);
            return OSPF_TE_FAILURE;
        }
    }

    /* Exclude-any
     * (link-attr & exclude-any) == 0*/
    if ((pCspfReq->u4ExcludeAnySet != OSPF_TE_NULL) ||
        (pCspfReq->u1RsrcSetType == OSPF_TE_RSRC_TYPE_EXCLUDE_ANY))
    {
        if (((pLinkTlvNode->u4RsrcClassColor & pCspfReq->u4ExcludeAnySet)
             != OSPF_TE_NULL) ||
                ((pLinkTlvNode->u4RsrcClassColor == OSPF_TE_NULL) &&
                (pCspfReq->u1RsrcSetType == OSPF_TE_RSRC_TYPE_EXCLUDE_ANY)))
        {
            OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
                          "Link does not satisfies excludeAnySet Admin \n"
                          "Constrain link RsrcClassColor: %d \n"
                          "CSPF excludeAnySet: %d\n",
                          pLinkTlvNode->u4RsrcClassColor,
                          pCspfReq->u4ExcludeAnySet);
            return OSPF_TE_FAILURE;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkAdminConstrain\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkConstrain                                     */
/*                                                                           */
/* Description  : This function checks whether link's attributes             */
/*                satisfies the constraints specified in CSPF request or not.*/
/*                                                                           */
/* Input        : pCspfReq - pointer to CSPF request.                        */
/*                pLinkTlvNode - Holds the Link TLV information.             */
/*                u4RouterId - advertising router Id of the link.            */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfChkConstrain (tOsTeCspfReq * pCspfReq,
                        tOsTeLinkTlvNode * pLinkTlvNode, UINT4 u4RouterId)
{
    UINT4               u4RtrId = OSPF_TE_ZERO;
    UINT4               u4NextHopIp = OSPF_TE_ZERO;
    UINT4               u4NextHopId = OSPF_TE_ZERO;
    UINT2               u2Count = OSPF_TE_ZERO;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkConstrain\n");

    if (ER_OR_AR_ROUTE_INFO (pCspfReq) == OSPF_TE_FAILED_LINK)
    {
        for (u2Count = OSPF_TE_ZERO;
             u2Count < ER_OR_AR_HOP_COUNT (pCspfReq); u2Count++)
        {
            u4RtrId = ER_OR_AR_ROUTER_ID (pCspfReq, u2Count);
            u4NextHopIp =
                ER_OR_AR (pCspfReq)->aNextHops[u2Count].u4NextHopIpAddr;
            u4NextHopId =
                ER_OR_AR (pCspfReq)->aNextHops[u2Count].u4RemoteIdentifier;

			
            if ((((u4RtrId == pLinkTlvNode->u4LinkId) || (OSPF_TE_NULL == u4RtrId))&&
                ((u4NextHopIp == pLinkTlvNode->u4RemoteIpAddr) ||
                 (u4NextHopIp == OSPF_TE_NULL))) &&
                (((u4RtrId == pLinkTlvNode->u4LinkId) || (OSPF_TE_NULL == u4RtrId))&&
                 ((u4NextHopId == pLinkTlvNode->u4RemoteIdentifier) ||
                  (u4NextHopId == OSPF_TE_NULL))))
            {
                OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                             "Router ID be avoided is obtained\n");
                return OSPF_TE_FAILURE;
            }
        }
    }

    OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
                  "Link Local Identifier: %d Link local Ip Address: %x\n",
                  pLinkTlvNode->u4LocalIdentifier, pLinkTlvNode->u4LocalIpAddr);

    if (pLinkTlvNode->u1LinkType == OSPF_TE_PTOP)
    {
        if ((pLinkTlvNode->u4RemoteIpAddr == OSPF_TE_NULL) &&
            (pLinkTlvNode->u4RemoteIdentifier == OSPF_TE_NULL))
        {
            OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                         "Remote IP address or Remote identifier is  not \n"
                         "specified for the link\n");
            return OSPF_TE_FAILURE;
        }

    }
    if (IS_ONLY_MPLS_LINK_CONSIDERED (pCspfReq->u1Flag))
    {
        if (TMO_SLL_Count (&pLinkTlvNode->ifDescrLst) != OSPF_TE_NULL)
        {
            return OSPF_TE_FAILURE;
        }

    }
    if (IS_ONLY_GMPLS_LINK_CONSIDERED (pCspfReq->u1Flag))
    {
        if (TMO_SLL_Count (&pLinkTlvNode->ifDescrLst) == OSPF_TE_NULL)
        {
            return OSPF_TE_FAILURE;
        }
    }

    if (IS_RESOURCE_AFFINITY (pCspfReq->u1Flag))
    {
        if (OspfTeCspfChkAdminConstrain (pCspfReq, pLinkTlvNode) ==
            OSPF_TE_FAILURE)
        {
            OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                         "Admin Constrain is not satisfied for link.\n");
            return OSPF_TE_FAILURE;
        }
    }
    if (pCspfReq->u1ProtectionType != OSPF_TE_NULL)
    {
        if (OspfTeCspfChkProtectionType (pCspfReq, pLinkTlvNode) ==
            OSPF_TE_FAILURE)
        {
            OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
                          "Link Protection Type constrain is not satisfied \n"
                          "Link Protection Type : %d CSPF request protection \n"
                          "type: %d\n",
                          pLinkTlvNode->u1ProtectionType,
                          pCspfReq->u1ProtectionType);
            return OSPF_TE_FAILURE;
        }
    }
    if ((TMO_SLL_Count (&pLinkTlvNode->ifDescrLst) == OSPF_TE_NULL) &&
        (IS_OSPF_TE_GMPLS_CONSTRAIN (pCspfReq)))
    {
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                     "Interface descriptor is not present but application \n"
                     "specified GMLS constrain\n");
        return OSPF_TE_FAILURE;
    }

    if ((TMO_SLL_Count (&pLinkTlvNode->ifDescrLst) != OSPF_TE_NULL) &&
        (pCspfReq->bandwidth != OSPF_TE_NULL))
    {
        if (OspfTeCspfChkBwConstrain (pCspfReq, pLinkTlvNode) ==
            OSPF_TE_FAILURE)
        {
            OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
                          "Bandwidth constrain is not satisfied \n"
                          "available bandwidth for link : %f \n"
                          "CSPF request bandwidth: %f\n",
                          pLinkTlvNode->aUnResrvBw[pCspfReq->u1Priority],
                          pCspfReq->bandwidth);
            return OSPF_TE_FAILURE;
        }
    }
    if (TMO_SLL_Count (&pLinkTlvNode->ifDescrLst) != OSPF_TE_NULL)
    {
        if (OspfTeCspfChkIfDescrConstrain (pCspfReq, pLinkTlvNode, u4RouterId)
            == OSPF_TE_FAILURE)
        {
            OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                         "Interface descriptor Constrain is not satisfied"
                         "for link.\n");
            return OSPF_TE_FAILURE;
        }

    }

    if (pLinkTlvNode->osTeSrlg.u4NoOfSrlg != OSPF_TE_ZERO)
    {
        if (OspfTeCspfChkSRLGConstraint (pCspfReq, pLinkTlvNode)
            == OSPF_TE_FAILURE)
        {
            OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                         "SRLG Constraint is not satisfied for the Link\n");
            return OSPF_TE_FAILURE;
        }
    }

    if ((IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag)) ||
        (IS_BACKUP_PATH (pCspfReq->u1Flag)))
    {
        /* Assumption :
         * -----------
         * If make before break path is asked by the application, then
         * it is understood that, exiting path is filled in the osteExpRoute
         * which is copied into the gOsTeContext.aPath[ZERO].
         * So check cspf disjoint constraint against the global path */
        if (OspfTeCspfCheckDisjoint (pCspfReq, pLinkTlvNode) == OSPF_TE_FAILURE)
        {
            OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                         "Disjoint Constraint is not satisfied "
                         "for the Link\n");
            return OSPF_TE_FAILURE;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkConstrain\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkStrictExpRoute                                */
/*                                                                           */
/* Description  : This function checks whether path  satisfies the strict    */
/*                explicit route constraints specified in the CSPF requset   */
/*                or not.                                                    */
/*                                                                           */
/* Input        : pPath - poiter to path.                                    */
/*                pExpRoute - pointer to explicit route constrain.           */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfChkStrictExpRoute (tOsTePath * pPath, tOsTeExpRoute * pExpRoute)
{
    UINT2               u2Count;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkStrictExpRoute\n");

    if (pPath->u2HopCount != pExpRoute->u2HopCount)
    {
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                     "Path HopCount is not equal to Explicit route HopCount\n");
        return OSPF_TE_FAILURE;
    }

    for (u2Count = OSPF_TE_NULL; u2Count < pPath->u2HopCount; u2Count++)
    {
        if ((IsOspfTeStrictExpRoute (pPath, pExpRoute, u2Count, u2Count))
            == OSPF_TE_FALSE)
        {
            return OSPF_TE_FAILURE;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkStrictExpRoute\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkLooseExpRoute                                 */
/*                                                                           */
/* Description  : This function checks whether path  satisfies the loose     */
/*                explicit route constraints specified in the CSPF request   */
/*                or not.                                                    */
/*                                                                           */
/* Input        : pPath - poiter to path.                                    */
/*                pExpRoute - pointer to explicit route constrain.           */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/
PUBLIC INT4
OspfTeCspfChkLooseExpRoute (tOsTePath * pPath, tOsTeExpRoute * pExpRoute)
{
    UINT2               u2Count;
    UINT2               u2Count1 = OSPF_TE_NULL;
    UINT2               u2StartCount = OSPF_TE_NULL;
    UINT2               u2LooseCount = OSPF_TE_NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkLooseExpRoute\n");

    for (u2Count = OSPF_TE_NULL; u2Count < pExpRoute->u2HopCount; u2Count++)
    {
        for (u2Count1 = u2StartCount; u2Count1 < pPath->u2HopCount; u2Count1++)
        {
            if ((IsOspfTeStrictExpRoute
                 (pPath, pExpRoute, u2Count1, u2Count)) == OSPF_TE_TRUE)
            {
                u2StartCount = u2Count1;
                u2LooseCount++;
                break;
            }
        }
    }

    if (u2LooseCount == pExpRoute->u2HopCount)
    {
        return OSPF_TE_SUCCESS;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkLooseExpRoute\n");

    return OSPF_TE_FAILURE;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkFailedLink                                    */
/*                                                                           */
/* Description  : This function checks whether path contains failed link     */
/*                or not  specified in the CSPF request                      */
/*                                                                           */
/* Input        : pPath - poiter to path.                                    */
/*                pExpRoute - pointer to explicit route constrain.           */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfChkFailedLink (tOsTePath * pPath, tOsTeExpRoute * pExpRoute)
{
    UINT2               u2Count;
    UINT2               u2Count1;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkFailedLink\n");

    for (u2Count = OSPF_TE_NULL; u2Count < pExpRoute->u2HopCount; u2Count++)
    {
        for (u2Count1 = OSPF_TE_NULL; u2Count1 < pPath->u2HopCount; u2Count1++)
        {
            if ((IsOspfTeStrictExpRoute (pPath, pExpRoute, u2Count1, u2Count))
                == OSPF_TE_TRUE)
            {
                OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                             "Path Contains the failed link\n");
                return OSPF_TE_FAILURE;
            }
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkFailedLink\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkNodeDisjoint                                  */
/*                                                                           */
/* Description  : This function checks whether backup path is Node Disjoint  */
/*                or not.                                                    */
/*                                                                           */
/* Input        : pPrimaryPath  - pointer to Primary Path.                   */
/*                pBackupPath   - pointer to Backup Path.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfChkNodeDisjoint (tOsTePath * pPrimaryPath, tOsTePath * pBackupPath)
{
    UINT2               u2PrimaryCount;
    UINT2               u2BackupCount;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkNodeDisjoint\n");
    for (u2PrimaryCount = OSPF_TE_NULL;
         u2PrimaryCount < (pPrimaryPath->u2HopCount - OSPF_TE_ONE);
         u2PrimaryCount++)
    {
        for (u2BackupCount = OSPF_TE_NULL;
             u2BackupCount < (pBackupPath->u2HopCount - OSPF_TE_ONE);
             u2BackupCount++)
        {
            if (pPrimaryPath->aNextHops[u2PrimaryCount].u4RouterId ==
                pBackupPath->aNextHops[u2BackupCount].u4RouterId)
            {
                return OSPF_TE_FAILURE;
            }
        }
    }

    if ((pPrimaryPath->u2HopCount == OSPF_TE_ONE)
        && (pBackupPath->u2HopCount == OSPF_TE_ONE))
    {
        if (IS_OSPF_TE_SAME_PATH (pPrimaryPath, pBackupPath))
        {
            return OSPF_TE_FAILURE;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkNodeDisjoint\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkLinkDisjoint                                  */
/*                                                                           */
/* Description  : This function checks whether backup path is Link Disjoint  */
/*                or not.                                                    */
/*                                                                           */
/* Input        : pPrimaryPath  - pointer to Primary Path.                   */
/*                pBackupPath   - pointer to Backup Path.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfChkLinkDisjoint (tOsTePath * pPrimaryPath, tOsTePath * pBackupPath)
{
    UINT2               u2PrimaryCount;
    UINT2               u2BackupCount;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkLinkDisjoint\n");
    for (u2PrimaryCount = OSPF_TE_NULL;
         u2PrimaryCount < pPrimaryPath->u2HopCount; u2PrimaryCount++)
    {
        for (u2BackupCount = OSPF_TE_NULL;
             u2BackupCount < pBackupPath->u2HopCount; u2BackupCount++)
        {
            if (IS_OSPF_TE_BACKUP_PATH_LINK_DISJOINT
                (pPrimaryPath, pBackupPath, u2PrimaryCount, u2BackupCount))
            {
                return OSPF_TE_FAILURE;
            }
        }
    }
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkLinkDisjoint\n");
    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfDestIpAddrChk                                    */
/*                                                                           */
/* Description  : This function compares the IP address of the destination   */
/*                with the vertex Id.                                        */
/*                                                                           */
/* Input        : pCspfVertex - pointer to Vertex node.                      */
/*                pCspfReq    - Pointer to CSPF Request.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfDestIpAddrChk (tOsTeCandteNode * pCspfVertex, tOsTeCspfReq * pCspfReq)
{

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfDestIpAddrChk\n");
    if ((pCspfReq->u4DestIpAddr != pCspfVertex->u4VertexId)
        || (pCspfVertex->u1VertexType == OSPF_TE_VERT_NETWORK))
    {
        OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC, "Destination %x not found\n",
                      pCspfReq->u4DestIpAddr);
        return OSPF_TE_FAILURE;
    }

    if (gOsTeContext.aPath[OSPF_TE_NULL] == NULL)
    {
        return OSPF_TE_FAILURE;
    }

    if (IS_BACKUP_PATH (pCspfReq->u1Flag))
    {
        if (gOsTeContext.aPath[OSPF_TE_ONE] == NULL)
        {
            return OSPF_TE_FAILURE;
        }

    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfDestIpAddrChk\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkValidPrimaryPath                              */
/*                                                                           */
/* Description  : This function checks whether path satisfies the constrain  */
/*                or not.                                                    */
/*                                                                           */
/* Input        : pVertexPath - pointer to Primary Path.                     */
/*                pCspfReq    - Pointer to CSPF Request.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/
PUBLIC INT4
OspfTeCspfChkValidPrimaryPath (tOsTePath * pVertexPath, tOsTeCspfReq * pCspfReq)
{
    INT4                i4Status = OSPF_TE_FAILURE;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkValidPrimaryPath\n");

    if (pCspfReq->osTeExpRoute.u2HopCount == OSPF_TE_ZERO)
    {
        return OSPF_TE_SUCCESS;
    }
    else
    {
        switch (pCspfReq->osTeExpRoute.u2Info)
        {
            case OSPF_TE_STRICT_EXP_ROUTE:
                i4Status = OspfTeCspfChkStrictExpRoute
                    (pVertexPath, &(pCspfReq->osTeExpRoute));
                break;

            case OSPF_TE_LOOSE_EXP_ROUTE:
                i4Status = OspfTeCspfChkLooseExpRoute
                    (pVertexPath, &(pCspfReq->osTeExpRoute));
                break;
            case OSPF_TE_FAILED_LINK:
                i4Status = OspfTeCspfChkFailedLink
                    (pVertexPath, &(pCspfReq->osTeExpRoute));
                break;
            default:
                break;
        }

        if (i4Status == OSPF_TE_SUCCESS)
        {
            return OSPF_TE_SUCCESS;
        }

    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkValidPrimaryPath\n");
    return OSPF_TE_FAILURE;

}

/*****************************************************************************/
/* Function     : OspfTeCspfChkValidBackupPath                               */
/*                                                                           */
/* Description  : This function checks whether path satisfies the constrain  */
/*                or not.                                                    */
/*                                                                           */
/* Input        : pVertexPath - pointer to Backup Path.                      */
/*                pCspfReq    - Pointer to CSPF Request.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfChkValidBackupPath (tOsTePath * pVertexPath, tOsTeCspfReq * pCspfReq)
{

    INT4                i4Status;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkValidBackupPath\n");

    if ((!IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag)) &&
        (IS_NODE_DISJOINT (pCspfReq->u1Diversity) ||
         pCspfReq->u1Diversity == OSPF_TE_NULL))
    {
        i4Status =
            OspfTeCspfChkNodeDisjoint (gOsTeContext.
                                       aPath[OSPF_TE_NULL], pVertexPath);
        if (i4Status == OSPF_TE_FAILURE)
        {
            return OSPF_TE_FAILURE;
        }

    }
    if (IS_LINK_DISJOINT (pCspfReq->u1Diversity))
    {
        i4Status =
            OspfTeCspfChkLinkDisjoint (gOsTeContext.
                                       aPath[OSPF_TE_NULL], pVertexPath);

        if (i4Status == OSPF_TE_FAILURE)
        {
            return OSPF_TE_FAILURE;
        }
    }
    if (IS_SRLG_DISJOINT (pCspfReq->u1Diversity))
    {
        i4Status =
            OspfTeCspfChkSrlgDisjoint (gOsTeContext.
                                       aPath[OSPF_TE_NULL], pVertexPath);
        if (i4Status == OSPF_TE_FAILURE)
        {
            return OSPF_TE_FAILURE;
        }

    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkValidBackupPath\n");
    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : IsOspfTeStrictExpRoute                                     */
/*                                                                           */
/* Description  : This function checks if the Hop in the explicit Route      */
/*                matches the hop in the path.                               */
/*                                                                           */
/* Input        : pPath - Pointer to Path.                                   */
/*                pExpRoute - Pointer to Explicit Route                      */
/*                u2Count1 - Index of nexthops of path                       */
/*                u2Count - Index of nexthops of explicitRoute               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_TRUE - If Hop in Explicit Route matches with Path. */
/*                OSPF_TE_FALSE - If Hop in Explicit Route does not match    */
/*                                Path.                                      */
/*****************************************************************************/

PUBLIC BOOLEAN
IsOspfTeStrictExpRoute (tOsTePath * pPath, tOsTeExpRoute * pExpRoute,
                        UINT2 u2Count1, UINT2 u2Count)
{
    if ((pPath->aNextHops[u2Count1].u4RouterId ==
         pExpRoute->aNextHops[u2Count].u4RouterId) &&
        ((pPath->aNextHops[u2Count1].u4NextHopIpAddr ==
          pExpRoute->aNextHops[u2Count].u4NextHopIpAddr) ||
         (pExpRoute->aNextHops[u2Count].u4NextHopIpAddr == OSPF_TE_NULL)) &&
        ((pPath->aNextHops[u2Count1].u4RemoteIdentifier ==
          pExpRoute->aNextHops[u2Count].u4RemoteIdentifier) ||
         pExpRoute->aNextHops[u2Count].u4RemoteIdentifier == OSPF_TE_NULL))
    {
        return OSPF_TE_TRUE;
    }

    return OSPF_TE_FALSE;
}

/*****************************************************************************/
/* Function     : OspfTeCspfChkSRLGConstraint                                */
/*                                                                           */
/* Description  : This function checks whether link's attributes             */
/*                satisfies the SRLG constraints specified in CSPF request   */
/*                                                                           */
/* Input        : pCspfReq - pointer to CSPF request.                        */
/*                pLinkTlvNode - Holds the Link TLV information.             */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfChkSRLGConstraint (tOsTeCspfReq * pCspfReq,
                             tOsTeLinkTlvNode * pLinkTlvNode)
{
    UINT4               u4Count1 = OSPF_TE_ZERO;
    UINT4               u4Count2 = OSPF_TE_ZERO;
    UINT4               u4MatchCount = OSPF_TE_ZERO;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfChkSRLGConstraint\n");

    if (pCspfReq->u1SrlgType == OSPF_TE_ZERO)
    {
        OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkSRLGConstraint\n");
        return OSPF_TE_SUCCESS;
    }

    for (u4Count1 = 0; u4Count1 < pCspfReq->osTeSrlg.u4NoOfSrlg; u4Count1++)
    {
        for (u4Count2 = 0; u4Count2 < pLinkTlvNode->osTeSrlg.u4NoOfSrlg;
             u4Count2++)
        {
            if (pLinkTlvNode->osTeSrlg.aSrlgNumber[u4Count2] ==
                pCspfReq->osTeSrlg.aSrlgNumber[u4Count1])
            {
                if (pCspfReq->u1SrlgType == OSPF_TE_SRLG_TYPE_EXCLUDE_ANY)
                {
                    OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "EXCLUDE ANY SRLG "
                                 "Costraint is not satisfied\n");
                    return OSPF_TE_FAILURE;
                }
                else if (pCspfReq->u1SrlgType == OSPF_TE_SRLG_TYPE_INCLUDE_ANY)
                {
                    OSPF_TE_TRC (OSPF_TE_FN_EXIT,
                                 "EXIT: OspfTeCspfChkSRLGConstraint\n");
                    return OSPF_TE_SUCCESS;
                }
                /* Else case should be OSPF_TE_SRLG_TYPE_INCLUDE_ALL */
                else
                {
                    u4MatchCount++;
                    break;
                }
            }
        }
    }

    if ((pCspfReq->u1SrlgType == OSPF_TE_SRLG_TYPE_INCLUDE_ALL) &&
        (pCspfReq->osTeSrlg.u4NoOfSrlg != u4MatchCount))
    {
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "INCLUDE ALL SRLG "
                     "Constraint is not Satisfied\n");
        return OSPF_TE_FAILURE;
    }
    else if (pCspfReq->u1SrlgType == OSPF_TE_SRLG_TYPE_INCLUDE_ANY)
    {
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "INCLUDE ANY SRLG "
                     "Constraint is not Satisfied\n");
        return OSPF_TE_FAILURE;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfChkSRLGConstraint\n");
    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfCheckDisjoint                                    */
/*                                                                           */
/* Description  : This function checks the Disjoint constraints (Link/Node)  */
/*                against the explicit route mentioned in the CSPF request   */
/*                which is applicable for MBB                                */
/*                                                                           */
/* Input        : pCspfReq - pointer to CSPF request.                        */
/*                pLinkTlvNode - Holds the Link TLV information.             */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/
PUBLIC INT4
OspfTeCspfCheckDisjoint (tOsTeCspfReq * pCspfReq,
                         tOsTeLinkTlvNode * pLinkTlvNode)
{
    UINT2               u2NodeCount = OSPF_TE_ZERO;
    UINT2               u2Count = OSPF_TE_ZERO;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "OspfTeCspfCheckDisjoint");

    /* Assumption :
     * -----------
     * If make before break path is asked by the application, then
     * it is understood that, exiting path is filled in the osteExpRoute
     * which is copied into the gOsTeContext.aPath[ZERO].
     * So check cspf disjoint constraint against the global path 
     * --------------------------------------------------------------- */

    /* If the last hop specified in the path is the destination router id
     * consider the hopcount as hopcount - 1 */
    if ((gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[gOsTeContext.
                                                     aPath[OSPF_TE_NULL]->
                                                     u2HopCount -
                                                     OSPF_TE_ONE].u4RouterId) ==
        pCspfReq->u4DestIpAddr)
    {
        u2NodeCount
            =
            (UINT2) (gOsTeContext.aPath[OSPF_TE_NULL]->u2HopCount -
                     (UINT2) OSPF_TE_ONE);
    }
    else
    {
        u2NodeCount = gOsTeContext.aPath[OSPF_TE_NULL]->u2HopCount;
    }
    /* check for node disjoint */
    if (IS_NODE_DISJOINT (pCspfReq->u1Diversity))
    {
        for (u2Count = OSPF_TE_ZERO; u2Count < u2NodeCount; u2Count++)
        {
			OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
						"Node Disjoint - Compare Router Id(%x), NextHopIpAddr(%x) with LinkId(%x)\n",
						gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].u4RouterId,
						pLinkTlvNode->u4LinkId);
            if (gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].
                u4RouterId == pLinkTlvNode->u4LinkId)
            {
                OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                             "Node Disjoint Constraint is not satisfied "
                             "for the Link\n");
                return OSPF_TE_FAILURE;
            }
        }
    }
    /* check for link disjoint */
    if (IS_LINK_DISJOINT (pCspfReq->u1Diversity))
    {
        for (u2Count = OSPF_TE_ZERO; u2Count < u2NodeCount; u2Count++)
        {
            if (gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].
                u4NextHopIpAddr != OSPF_TE_ZERO)
            {

				OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
						"Link Disjoint -  Compare NextHopIpAddr(%x) with Link Remote Ip Address(%x)\n",
						gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].u4NextHopIpAddr,
						pLinkTlvNode->u4RemoteIpAddr);

                if (gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].
                    u4NextHopIpAddr == pLinkTlvNode->u4RemoteIpAddr)
                {
                    OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                                 "Link Disjoint Constraint is not satisfied "
                                 "for the Link\n");
                    return OSPF_TE_FAILURE;
                }
            }
            else
            {
				OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
						"Link Disjoint - Compare Router Id(%x) with LinkId(%x)\n",
						gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].u4RouterId,
						pLinkTlvNode->u4LinkId);


				OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
						"Link Disjoint - Compare Remote Identifier(%x) with Link Remote Identifier(%x)\n",
						gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].u4RemoteIdentifier,
						pLinkTlvNode->u4RemoteIdentifier);

                if ((gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].
                     u4RouterId == pLinkTlvNode->u4LinkId) &&
                    (gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].
                     u4RemoteIdentifier == pLinkTlvNode->u4RemoteIdentifier))
                {
                    OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                                 "Link Disjoint Constraint is not satisfied "
                                 "for the Link\n");
                    return OSPF_TE_FAILURE;
                }

            }
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfCheckDisjoint\n");
    return OSPF_TE_SUCCESS;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostecons.c                     */
/*-----------------------------------------------------------------------*/
