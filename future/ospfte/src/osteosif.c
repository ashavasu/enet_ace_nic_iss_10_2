/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: osteosif.c,v 1.15 2013/12/18 12:47:59 siva Exp $
*
* Description:  This file has routines for the interaction with 
*               OSPF.
*********************************************************************/

#include "osteinc.h"
PRIVATE VOID        OspfTeProcessNetLsa (tOsTeLsaNode * pOsTeLsa,
                                         tOsLsaInfo * pOsLsaInfo,
                                         tOsTeArea * pTeArea);

PRIVATE VOID        OspfTeProcessNetNbrInfo (UINT1 *pPtr, UINT2 u2Length,
                                             UINT1 u1Flag,
                                             tOsTeArea * pOsTeArea);

PRIVATE UINT2       OspfTeFindChangedNbrs (UINT1 *pPtr, UINT2 u2Length,
                                           UINT1 *pOldPtr, UINT2 u2OldLength,
                                           UINT1 *pNbrs);
PRIVATE VOID
       OspfTeGenerateLsaForFA (tIPADDR advtRtrId, tOsTeArea * pOsTeArea);
/*****************************************************************************/
/*  Function    :  OspfTeProcessOspfMsg                                      */
/*  Description :  This module processes the message received from OSPF. It  */
/*                 calls the respective module to handle the OSPF message.   */
/*  Input       :  Holds the message from OSPF                               */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessOspfMsg (tOsToOpqApp * pOspfMsg)
{
    tOsTeArea          *pTeArea = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessOspfMsg\n");
    pTeArea = OspfTeUtilFindArea (pOspfMsg->u4AreaId);
    if (pTeArea == NULL)
    {
        /* Create an Area node */
        pTeArea = OspfTeAreaCreate (pOspfMsg->u4AreaId);
    }
    if (pTeArea == NULL)
    {
        OSPF_TE_TRC1 (OSPF_TE_CRITICAL_TRC,
                      "Failed to create Area %x\n", pOspfMsg->u4AreaId);
        return;
    }
    switch (pOspfMsg->u4MsgSubType)
    {
        case OSPF_TE_LSA_INFO:

            OSPF_TE_TRC (OSPF_TE_CONTROL_PLANE_TRC,
                         "Received LSA information from OSPF\n");
            OspfTeProcessLsaInfo (pOspfMsg, pTeArea);
            break;

        case OSPF_TE_NBR_IP_ADDR_INFO:

            OSPF_TE_TRC (OSPF_TE_CONTROL_PLANE_TRC,
                         "Received Neighbor IP address information "
                         "from OSPF\n");
            OspfTeProcessNbrInfo (pOspfMsg, pTeArea);
            break;

        case OSPF_REGISTER_WITH_OSPFTE:

            OSPF_TE_TRC (OSPF_TE_CONTROL_PLANE_TRC,
                         "Received Deregistration message information "
                         "from OSPF\n");
            OspfTeAdminDnHdlr ();
            break;

        default:
            OSPF_TE_TRC1 (OSPF_TE_FAILURE_TRC,
                          "Received invalid Message %x from OSPF\n",
                          pOspfMsg->u4MsgSubType);
            break;
    }
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessOspfMsg\n");
    KW_FALSEPOSITIVE_FIX (pTeArea);
    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessLsaInfo                                      */
/*  Description :  This module processes the message received from OSPF. It  */
/*                 calls the respective module to handle the OSPF message.   */
/*  Input       :  Holds the message from OSPF                               */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessLsaInfo (tOsToOpqApp * pOspfMsg, tOsTeArea * pTeArea)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessLsaInfo\n");

    switch (pOspfMsg->lsaInfo.u1LsaType)
    {
        case OSPF_TE_ROUTER_LSA:
            OspfTeProcessRtrLsa (pOspfMsg, pTeArea);
            break;

        case OSPF_TE_NETWORK_LSA:
            /* Fall through */
        case OSPF_TE_TYPE10_LSA:
            OspfTeProcessNetType10LSA (pOspfMsg, pTeArea);
            break;

        case OSPF_TE_TYPE9_LSA:
            OspfTeProcessType9Lsa (pOspfMsg);
            break;

        default:
            OSPF_TE_TRC1 (OSPF_TE_FAILURE_TRC, "Unknown Type %d TE LSA "
                          "is received\n", pOspfMsg->lsaInfo.u1LsaType);
            break;
    }
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessLsaInfo\n");
    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessNbrInfo                                      */
/*  Description :  This module processes the message received from OSPF. It  */
/*                 calls the respective module to handle the OSPF message.   */
/*  Input       :  Holds the message from OSPF                               */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessNbrInfo (tOsToOpqApp * pOspfMsg, tOsTeArea * pTeArea)
{
    tOsTeOsIntInfo     *pOsTeOsIntInfo = NULL;
    tIPADDR             ifIpAddr;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessNbrInfo\n");

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, pOspfMsg->u4IfIpAddr);

    /* Find the interface in OSPF information interface list */
    pOsTeOsIntInfo = OspfTeFindOspfInfoInt (ifIpAddr, pOspfMsg->u4AddrlessIf);
    if (pOsTeOsIntInfo == NULL)
    {
        if (OSPF_TE_OSPF_INFO_IF_ALLOC (pOsTeOsIntInfo, tOsTeOsIntInfo) == NULL)
        {
            OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to allocate "
                         "Memory for OSPF information interface\n");
            return;
        }
        OSPF_TE_MEMSET (pOsTeOsIntInfo, OSPF_TE_ZERO, sizeof (tOsTeOsIntInfo));
        /* Fill the information in the created interface */
        pOsTeOsIntInfo->u1LinkType = OSPF_TE_PTOP;
        pOsTeOsIntInfo->pTeArea = pTeArea;
        pOsTeOsIntInfo->u4AddrlessIf = pOspfMsg->u4AddrlessIf;
        OSPF_TE_CRU_BMC_DWTOPDU (pOsTeOsIntInfo->ifIpAddr,
                                 pOspfMsg->u4IfIpAddr);
        OSPF_TE_CRU_BMC_DWTOPDU (pOsTeOsIntInfo->nbrIpAddr,
                                 pOspfMsg->nbrInfo.u4NbrIpAddr);
        pOsTeOsIntInfo->u1InfoType = OSPF_TE_NEIGHBOR_INFO;
        /* Adding to the OSPF infomration interface list */
        OspfTeIfAddToSortOspfIntIfLst (pOsTeOsIntInfo);
    }
    else
    {
        /* Fill the information in the created interface */
        pOsTeOsIntInfo->pTeArea = pTeArea;
        OSPF_TE_CRU_BMC_DWTOPDU (pOsTeOsIntInfo->nbrIpAddr,
                                 pOspfMsg->nbrInfo.u4NbrIpAddr);
        pOsTeOsIntInfo->u1InfoType = OSPF_TE_NEIGHBOR_INFO;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessNbrInfo\n");
    KW_FALSEPOSITIVE_FIX (pOsTeOsIntInfo);
    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessRtrLsa                                       */
/*  Description :  This module processes the message received from OSPF. It  */
/*                 calls the respective module to handle the OSPF message.   */
/*  Input       :  Holds the message from OSPF                               */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessRtrLsa (tOsToOpqApp * pOspfMsg, tOsTeArea * pTeArea)
{
    tOsLsaInfo         *pOsLsaInfo = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessRtrLsa\n");

    pOsLsaInfo = &(pOspfMsg->lsaInfo);

    if (pTeArea->pRtrLsa == NULL)
    {
        /* NEW Router LSA */
        OspfTeProcessNewRtrLsa (pOsLsaInfo, pTeArea);
    }
    else if ((pTeArea->pRtrLsa != NULL)
             && (pOsLsaInfo->u1LsaStatus != OSPF_TE_FLUSHED_LSA))
    {
        /* NEW INSTANCE Router LSA */
        OspfTeProcessNewInstRtrLsa (pOsLsaInfo, pTeArea);
    }
    else if (pOsLsaInfo->u1LsaStatus == OSPF_TE_FLUSHED_LSA)
    {
        /* FLUSHED Router LSA */
        OspfTeProcessFlushedRtrLsa (pTeArea);
        OSPF_TE_LSA_FREE (pOsLsaInfo->pu1Lsa);
    }
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessRtrLsa\n");
    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessType9Lsa                                     */
/*  Description :  This module processes the message received from OSPF. It  */
/*                 calls the respective module to handle the OSPF message.   */
/*  Input       :  Holds the message received from OSPF                      */
/*  Output      :  None                                                      */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                           */
/*****************************************************************************/
PUBLIC INT4
OspfTeProcessType9Lsa (tOsToOpqApp * pOspfMsg)
{
    tRouterId           rtrId;
    tLINKSTATEID        linkStateId;
    tIPADDR             ifIpAddr;
    UINT4               u4LinkLocalId = OSPF_TE_ZERO;
    tOsTeLsaNode       *pOsTeLsa = NULL;
    tOsLsaInfo         *pOsLsaInfo = NULL;
    tOsTeInterface     *pOsTeInterface = NULL;
    tOsTeOsIntInfo     *pOsTeOsIntInfo = NULL;
    UINT1              *pu1Lsa = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessType9Lsa\n");

    pOsLsaInfo = &(pOspfMsg->lsaInfo);

    /* Verifying whether the LSAs are for TE or not */
    if (*((pOsLsaInfo->pu1Lsa) + OSPF_TE_LSA_OPAQUE_TYPE_OFFSET)
        != OSPF_TE_OPQ_TYPE)
    {
        OSPF_TE_TRC1 (OSPF_TE_FAILURE_TRC, "Type 9 LSA received with Unknown "
                      "Application (%d) type\n",
                      *((pOsLsaInfo->pu1Lsa) + OSPF_TE_LSA_OPAQUE_TYPE_OFFSET));
        return OSPF_TE_FAILURE;
    }

    /* No need to take any action if the Type 9 LSA is received on
     * numbered interface */
    if (pOspfMsg->u4IfIpAddr != OSPF_TE_ZERO)
    {
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Type 9 LSA received on numbered "
                     "interface\n");
        return OSPF_TE_FAILURE;
    }
    /* Processing Type 9 LSA information */
    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, pOspfMsg->u4IfIpAddr);
    OSPF_TE_CRU_BMC_DWTOPDU (rtrId, pOsLsaInfo->u4AdvRtrId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, pOsLsaInfo->u4LinkStateId);

    pOsTeInterface = OspfTeFindTeInterface (ifIpAddr, pOspfMsg->u4AddrlessIf);
    if (pOsTeInterface == NULL)
    {
        pOsTeOsIntInfo = OspfTeFindOspfInfoInt (ifIpAddr,
                                                pOspfMsg->u4AddrlessIf);
        if (pOsTeOsIntInfo != NULL)
        {
            /* If the LSA is not self Originated then only store */
            if (OspfTeUtilIpAddrComp (gOsTeContext.rtrId, rtrId)
                != OSPF_TE_EQUAL)
            {
                if (pOsLsaInfo->u1LsaStatus == OSPF_TE_FLUSHED_LSA)
                {
                    if (pOsTeOsIntInfo->pType9Lsa != NULL)
                    {
                        OSPF_TE_LSA_FREE (pOsTeOsIntInfo->pType9Lsa->pLsa);
                        OSPF_TE_LSA_INFO_FREE (pOsTeOsIntInfo->pType9Lsa);
                        pOsTeOsIntInfo->pType9Lsa = NULL;
                    }
                    OSPF_TE_LSA_FREE (pOsLsaInfo->pu1Lsa);
                }
                else
                {
                    if (pOsTeOsIntInfo->pType9Lsa == NULL)
                    {
                        if (OSPF_TE_LSA_INFO_ALLOC (pOsTeOsIntInfo->pType9Lsa,
                                                    struct _OsTeLsaNode) ==
                            NULL)
                        {
                            OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC,
                                         "LSA Info Allocation Failure\n");
                            return OSPF_TE_FAILURE;
                        }
                        pOsTeOsIntInfo->pType9Lsa->u1LsaType =
                            OSPF_TE_TYPE9_LSA;
                        OSPF_TE_MEMCPY (&
                                        (pOsTeOsIntInfo->pType9Lsa->
                                         linkStateId), &(linkStateId),
                                        OSPF_TE_LSA_ID_LEN);
                        IP_ADDR_COPY (&(pOsTeOsIntInfo->pType9Lsa->advRtrId),
                                      rtrId);
                        pOsTeOsIntInfo->pType9Lsa->pLsa = NULL;
                        pOsTeOsIntInfo->pType9Lsa->pOsTeInt = NULL;
                    }
                    pu1Lsa = pOsLsaInfo->pu1Lsa;

                    if (pOsTeOsIntInfo->pType9Lsa->pLsa != NULL)
                    {
                        /* Freeing old LSA buffer */
                        OSPF_TE_LSA_FREE (pOsTeOsIntInfo->pType9Lsa->pLsa);
                    }
                    pOsTeOsIntInfo->pType9Lsa->pLsa = pu1Lsa;
                    pOsTeOsIntInfo->pType9Lsa->u2LsaLen = pOsLsaInfo->u2LsaLen;
                    pOsTeOsIntInfo->pType9Lsa->u2LsaChksum =
                        OSPF_TE_CRU_BMC_WFROMPDU (pu1Lsa +
                                                  MAX_IP_ADDR_LEN *
                                                  MAX_IP_ADDR_LEN);
                }
            }
        }
        else
        {
            OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Type 9 LSA received does not "
                         "belong to any interface\n");
            return OSPF_TE_FAILURE;
        }
    }
    else
    {
        /* Search in the Database */
        pOsTeLsa = OspfTeLsuSearchDatabase (OSPF_TE_TYPE9_LSA,
                                            &(linkStateId), &(rtrId),
                                            (UINT1 *) pOsTeInterface);

        /* Check if Flushed LSA */
        if (pOsLsaInfo->u1LsaStatus == OSPF_TE_FLUSHED_LSA)
        {
            if (pOsTeLsa != NULL)
            {
                /* Delete the LSA from the Database */
                OspfTeLsuDeleteLsaFromDatabase (pOsTeLsa);
                OSPF_TE_LSA_FREE (pOsLsaInfo->pu1Lsa);
            }
        }
        else
        {
            if (pOsTeLsa == NULL)
            {
                if ((pOsTeLsa = OspfTeLsuAddToLsdb ((UINT1 *) pOsTeInterface,
                                                    pOsLsaInfo->u1LsaType,
                                                    &(linkStateId),
                                                    &(rtrId))) == NULL)
                {
                    OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                                 "Failed to Add LSA to TE database\n");
                    return OSPF_TE_FAILURE;
                }
            }
            OspfTeLsuInstallLsa (pOsLsaInfo->pu1Lsa, pOsTeLsa);
        }

        /* If the LSA is not the self originated LSA */
        if (OspfTeUtilIpAddrComp (gOsTeContext.rtrId, rtrId) != OSPF_TE_EQUAL)
        {
            if (pOsLsaInfo->u1LsaStatus != OSPF_TE_FLUSHED_LSA)
            {
                u4LinkLocalId = OSPF_TE_CRU_BMC_DWFROMPDU (pOsLsaInfo->pu1Lsa +
                                                           OSPF_TE_LSA_HEADER_LENGTH
                                                           +
                                                           OSPF_TE_LSA_ID_LEN);
            }

            if (u4LinkLocalId != pOsTeInterface->u4RemoteIdentifier)
            {
                pOsTeInterface->u4RemoteIdentifier = u4LinkLocalId;
                if (pOsTeInterface->u1LinkStatus == OSPF_TE_ACTIVE)
                {
                    OspfTeGenerateLinkTlv (pOsTeInterface,
                                           OSPF_TE_NEW_INSTANCE_TLV);
                }
            }
        }
    }
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessType9Lsa\n");
    KW_FALSEPOSITIVE_FIX (pOsTeLsa);
    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessNetType10LSA                                 */
/*  Description :  This module processes the message received from OSPF. It  */
/*                 calls the respective module to handle the OSPF message.   */
/*  Input       :  Holds the message from OSPF                               */
/*  Output      :  None                                                      */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                           */
/*****************************************************************************/
PUBLIC INT4
OspfTeProcessNetType10LSA (tOsToOpqApp * pOspfMsg, tOsTeArea * pTeArea)
{
    tRouterId           rtrId;
    tLINKSTATEID        linkStateId;
    tLINKSTATEID        linkStateIdTemp = { 1, 0, 0, 0 };
    tOsTeLsaNode       *pOsTeLsa = NULL;
    tOsLsaInfo         *pOsLsaInfo = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessNetType10LSA\n");
    pOsLsaInfo = &(pOspfMsg->lsaInfo);
    OSPF_TE_CRU_BMC_DWTOPDU (rtrId, pOsLsaInfo->u4AdvRtrId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, pOsLsaInfo->u4LinkStateId);

    /* Verifying whether the LSAs are for TE or not */
    if ((pOsLsaInfo->u1LsaType == OSPF_TE_TYPE10_LSA) &&
        (*((pOsLsaInfo->pu1Lsa) + OSPF_TE_LSA_OPAQUE_TYPE_OFFSET) !=
         OSPF_TE_OPQ_TYPE))
    {
        OSPF_TE_TRC1 (OSPF_TE_FAILURE_TRC, "Type 10 LSA received with Unknown "
                      "Application (%d) type\n",
                      ((pOsLsaInfo->pu1Lsa) + OSPF_TE_LSA_OPAQUE_TYPE_OFFSET));
        return OSPF_TE_FAILURE;
    }

    /* For TLM based TE Links, Area ID will not be provided by TLM.
     * In such cases, We need to associate the Area somehow.
     *
     * Below is the assumption:
     * -----------------------
     *
     * R1---- R2------ R3. be the three routers and the FA TELink is formed 
     * between R1 and R3 with R1's Remote Router ID as R3.
     * 
     * In such cases, where the R1's Remote Router ID is not the adjacent
     * Routers Router-ID, Remote router ID of the TE Link is matched with 
     * the advertising router ID when its sends Router Address TLV initially.
     */

    /* Only Router Address TLV has link state Id as 1.0.0.0 */

    if (OspfTeUtilIpAddrComp (linkStateId, linkStateIdTemp) == OSPF_TE_EQUAL)
    {
        OspfTeGenerateLsaForFA (rtrId, pTeArea);
    }

    /* Search in the Database */
    pOsTeLsa = OspfTeLsuSearchDatabase (pOsLsaInfo->u1LsaType,
                                        &(linkStateId), &(rtrId),
                                        (UINT1 *) pTeArea);

    if (pOsLsaInfo->u1LsaType == OSPF_TE_NETWORK_LSA)
    {
        OspfTeProcessNetLsa (pOsTeLsa, pOsLsaInfo, pTeArea);
    }

    if (pOsLsaInfo->u1LsaStatus == OSPF_TE_FLUSHED_LSA)
    {
        if (pOsTeLsa != NULL)
        {
            /* Delete the LSA from the Database */
            OspfTeLsuDeleteLsaFromDatabase (pOsTeLsa);
            OSPF_TE_LSA_FREE (pOsLsaInfo->pu1Lsa);
        }
    }
    else
    {
        if (pOsTeLsa == NULL)
        {
            if ((pOsTeLsa = OspfTeLsuAddToLsdb ((UINT1 *) pTeArea,
                                                pOsLsaInfo->u1LsaType,
                                                &(linkStateId),
                                                &(rtrId))) == NULL)
            {
                return OSPF_TE_FAILURE;
            }
        }

        OspfTeLsuInstallLsa (pOsLsaInfo->pu1Lsa, pOsTeLsa);
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessNetType10LSA\n");
    KW_FALSEPOSITIVE_FIX (pOsTeLsa);

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessNewRtrLsa                                    */
/*  Description :  This function processes the NEW Router LSA received       */
/*                 from OSPF. Adds all the links to this area. If the links  */
/*                 are already created by the Resource Manager, it generates */
/*                 NEW TLV for those links.                                  */
/*  Input       :  pOsLsaInfo - Poiinter to Router LSA.                      */
/*                 pTeArea - Points to Area node                             */
/*  Output      :  None                                                      */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                           */
/*****************************************************************************/
PUBLIC INT4
OspfTeProcessNewRtrLsa (tOsLsaInfo * pOsLsaInfo, tOsTeArea * pTeArea)
{
    tLinkNode           linkNode;
    tRouterId           rtrId;
    tLINKSTATEID        linkStateId;
    UINT1              *pCurrPtr = NULL;
    tOsTeLsaNode       *pOsTeLsa = NULL;
    UINT1               u1Flag = OSPF_TE_FALSE;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessNewRtrLsa\n");

    OSPF_TE_CRU_BMC_DWTOPDU (gOsTeContext.rtrId, pOsLsaInfo->u4AdvRtrId);
    OSPF_TE_CRU_BMC_DWTOPDU (rtrId, pOsLsaInfo->u4AdvRtrId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, pOsLsaInfo->u4LinkStateId);

    if ((pOsTeLsa = OspfTeLsuAddToLsdb ((UINT1 *) pTeArea,
                                        pOsLsaInfo->u1LsaType,
                                        &(linkStateId), &(rtrId))) == NULL)
    {
        return OSPF_TE_FAILURE;
    }
    OspfTeLsuInstallLsa (pOsLsaInfo->pu1Lsa, pOsTeLsa);

    /* Generate Router Address TLV */
    OspfTeGenerateRtrAddrTlv (pTeArea, OSPF_TE_NEW_TLV);

    /* Processing the Links in the self originated Router LSA */
    pCurrPtr = pOsTeLsa->pLsa + OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN;

    while ((pCurrPtr = OspfTeUtilGetLinksFromLsa (pOsTeLsa->pLsa,
                                                  pOsTeLsa->u2LsaLen,
                                                  pCurrPtr, &linkNode)) != NULL)
    {
        /* No need to process the Stub or Virtual links */
        if ((linkNode.u1LinkType == STUB_LINK) ||
            (linkNode.u1LinkType == VIRTUAL_LINK))
        {
            continue;
        }
        u1Flag = OSPF_TE_TRUE;
        OspfTeProcessRtrLsaNewLink (&linkNode, pTeArea);
    }

    if (u1Flag == OSPF_TE_TRUE)
    {
        OspfTeActivateTeLinks (pTeArea);
    }
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessNewRtrLsa\n");
    KW_FALSEPOSITIVE_FIX (pOsTeLsa);
    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessNewInstRtrLsa                                */
/*  Description :  This function processes the NEW INSTANCE of Router LSA    */
/*                 received from OSPF. Adds all the links to this area.      */
/*                 If the links are already created by the Resource Manager, */
/*                 it generates new TLV for those links.                     */
/*                 If Some links are deleted, then Link TLV will be flushed. */
/*                 If Some links are changed, then Link TLV will be          */
/*                 regenrated.                                               */
/*  Input       :  pOsLsaInfo - Poiinter to Router LSA.                      */
/*                 pArea - Points to Area node                               */
/*  Output      :  None                                                      */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                           */
/*****************************************************************************/
PUBLIC INT4
OspfTeProcessNewInstRtrLsa (tOsLsaInfo * pOsLsaInfo, tOsTeArea * pTeArea)
{
    tLinkNode           linkNode;
    tLinkNode           oldLinkNode;
    tRouterId           rtrId;
    tLINKSTATEID        linkStateId;
    UINT1              *pu1Lsa = NULL;
    UINT1              *pCurrPtr = NULL;
    UINT1              *pCurrPtr1 = NULL;
    UINT1               u1LinkFound = OSPF_TE_FALSE;
    UINT1               u1FlushLinkFound = OSPF_TE_FALSE;
    UINT2               u2OldLinks = OSPF_TE_ZERO;
    UINT2               u2NewLinks = OSPF_TE_ZERO;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessNewInstRtrLsa\n");

    OSPF_TE_CRU_BMC_DWTOPDU (rtrId, pOsLsaInfo->u4AdvRtrId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, pOsLsaInfo->u4LinkStateId);

    pu1Lsa = pOsLsaInfo->pu1Lsa;

    pCurrPtr = pu1Lsa + OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN;
    /* Processing the Links in the self originated Router LSA */
    while ((pCurrPtr = OspfTeUtilGetLinksFromLsa (pu1Lsa,
                                                  pOsLsaInfo->u2LsaLen,
                                                  pCurrPtr, &linkNode)) != NULL)
    {
        /* No need to process the Stub or Virtual links */
        if ((linkNode.u1LinkType == STUB_LINK) ||
            (linkNode.u1LinkType == VIRTUAL_LINK))
        {
            continue;
        }

        u2NewLinks++;

        u1LinkFound = OSPF_TE_FALSE;
        pCurrPtr1 = pTeArea->pRtrLsa->pLsa +
            OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN;
        while ((pCurrPtr1 =
                OspfTeUtilGetLinksFromLsa (pTeArea->pRtrLsa->pLsa,
                                           pTeArea->pRtrLsa->u2LsaLen,
                                           pCurrPtr1, &oldLinkNode)) != NULL)
        {
            if ((oldLinkNode.u1LinkType == STUB_LINK) ||
                (oldLinkNode.u1LinkType == VIRTUAL_LINK))
            {
                continue;
            }

            /* Link is present in Old Router LSA */
            if (OspfTeUtilIpAddrComp (linkNode.linkData,
                                      oldLinkNode.linkData) == OSPF_TE_EQUAL)
            {
                /* There is a change in the already present link */
                if ((OspfTeUtilIpAddrComp (linkNode.linkId,
                                           oldLinkNode.linkId) !=
                     OSPF_TE_EQUAL)
                    || (linkNode.u2LinkCost != oldLinkNode.u2LinkCost))
                {
                    OspfTeProcessRtrLsaModifyLink (&linkNode);
                }
                u1LinkFound = OSPF_TE_TRUE;
                break;
            }
        }
        /* The link is not present in the old Rtr LSA and 
         * present in NEW Rtr LSA */
        if (u1LinkFound != OSPF_TE_TRUE)
        {
            OspfTeProcessRtrLsaNewLink (&linkNode, pTeArea);
        }
    }

    /* To handle the Flushed links in the Router LSA */
    pCurrPtr1 = pTeArea->pRtrLsa->pLsa +
        OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN;
    while ((pCurrPtr1 =
            OspfTeUtilGetLinksFromLsa (pTeArea->pRtrLsa->pLsa,
                                       pTeArea->pRtrLsa->u2LsaLen,
                                       pCurrPtr1, &oldLinkNode)) != NULL)
    {
        if ((oldLinkNode.u1LinkType == STUB_LINK) ||
            (oldLinkNode.u1LinkType == VIRTUAL_LINK))
        {
            continue;
        }

        u2OldLinks++;

        u1FlushLinkFound = OSPF_TE_FALSE;

        pCurrPtr = pu1Lsa + OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN;
        /* Processing the Links in the self originated Router LSA */
        while ((pCurrPtr = OspfTeUtilGetLinksFromLsa (pu1Lsa,
                                                      pOsLsaInfo->u2LsaLen,
                                                      pCurrPtr,
                                                      &linkNode)) != NULL)
        {
            /* No need to process the Stub or Virtual links */
            if ((linkNode.u1LinkType == STUB_LINK) ||
                (linkNode.u1LinkType == VIRTUAL_LINK))
            {
                continue;
            }

            /* Link is present in Old Router LSA */
            if (OspfTeUtilIpAddrComp (linkNode.linkData,
                                      oldLinkNode.linkData) == OSPF_TE_EQUAL)
            {
                u1FlushLinkFound = OSPF_TE_TRUE;
                break;
            }
        }

        /* Link is not present in the NEW Rtr LSA */
        if (u1FlushLinkFound == OSPF_TE_FALSE)
        {
            OspfTeProcessRtrLsaFlushLink (&oldLinkNode, pTeArea);
        }
    }

    if ((u2NewLinks == OSPF_TE_ZERO) && (u2OldLinks != OSPF_TE_ZERO))
    {
        OspfTeInactivateTeLink (pTeArea, OSPF_TE_TRUE);
    }
    else if ((u2NewLinks != OSPF_TE_ZERO) && (u2OldLinks == OSPF_TE_ZERO))
    {
        OspfTeActivateTeLinks (pTeArea);
    }

    OspfTeLsuInstallLsa (pu1Lsa, pTeArea->pRtrLsa);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessNewInstRtrLsa\n");
    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessFlushedRtrLsa                                */
/*  Description :  This function processes the FLUSHED Router LSA            */
/*                 received from OSPF. For Each Link the Link TLV are        */
/*                 flushed, if the TE interface is ACTIVE                    */
/*  Input       :  pArea - Points to Area node                               */
/*  Output      :  None                                                      */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                           */
/*****************************************************************************/
PUBLIC INT4
OspfTeProcessFlushedRtrLsa (tOsTeArea * pArea)
{
    tLinkNode           linkNode;
    UINT1              *pCurrPtr = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessFlushedRtrLsa\n");

    /* Processing the Links in the self originated Router LSA */
    pCurrPtr = pArea->pRtrLsa->pLsa +
        OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN;
    while ((pCurrPtr = OspfTeUtilGetLinksFromLsa (pArea->pRtrLsa->pLsa,
                                                  pArea->pRtrLsa->u2LsaLen,
                                                  pCurrPtr, &linkNode)) != NULL)
    {
        /* No need to process the Stub or Virtual links */
        if ((linkNode.u1LinkType == STUB_LINK) ||
            (linkNode.u1LinkType == VIRTUAL_LINK))
        {
            continue;
        }
        OspfTeProcessRtrLsaFlushLink (&linkNode, pArea);
    }

    OspfTeInactivateTeLink (pArea, OSPF_TE_FALSE);

    /* Freeing the Memory allocated for the Old Router LSA */
    OSPF_TE_LSA_FREE (pArea->pRtrLsa->pLsa);
    OSPF_TE_LSA_INFO_FREE (pArea->pRtrLsa);
    pArea->pRtrLsa = NULL;
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessFlushedRtrLsa\n");
    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessRtrLsaNewLink                                */
/*  Description :  This function processes NEW link in the Router LSA        */
/*                 and make the TE link enabled, if possible                 */
/*  Input       :  pLinkNode - Points to Link node                           */
/*                 pTeArea     - Pointer to TE Area                          */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessRtrLsaNewLink (tLinkNode * pLinkNode, tOsTeArea * pTeArea)
{
    UINT4               u4AddrlessIf = OSPF_TE_ZERO;
    tIPADDR             ifIpAddr = { '0', '0', '0', '0' };
    tOsTeOsIntInfo     *pOsInfoIfNode = NULL;
    tOsTeInterface     *pOsTeInterface = NULL;
    UINT1              *pNbrPtr = NULL;
    tOsTeLsaNode       *pNetworkLsa = NULL;
    UINT2               u2Length = OSPF_TE_ZERO;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessRtrLsaNewLink\n");

    OspfTeGetIfInfoFromRtrLsaLink (pLinkNode->linkData, &ifIpAddr,
                                   &u4AddrlessIf);

    /* For AREA CHANNEL (FA), OspfTeFindTeInterface will return NULL with this
     * input.
     *  -------
     * |Reason |
     *  -------
     * OSPF don't have any information about AREA CHANNEL (FA). So pLinkNode
     * don't have any information regarding AREA CHANNEL (FA).
     */
    pOsTeInterface = OspfTeFindTeInterface (ifIpAddr, u4AddrlessIf);

    /* Find the interface in OSPF information interface list */
    pOsInfoIfNode = OspfTeFindOspfInfoInt (ifIpAddr, u4AddrlessIf);

    /* For AREA CHANNEL (FA) pOsTeInterface will be NULL, 
     * pLinkNode->u1LinkType will be PTOP_LINK */
    if (pLinkNode->u1LinkType == PTOP_LINK)
    {
        if (pOsTeInterface == NULL)
        {
            if (pOsInfoIfNode == NULL)
            {
                /* Create the entry in the OSPF information interface list */
                if (OSPF_TE_OSPF_INFO_IF_ALLOC (pOsInfoIfNode, tOsTeOsIntInfo)
                    == NULL)
                {
                    OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to allocate "
                                 "Memory for OSPF information interface\n");
                    return;
                }
                OSPF_TE_MEMSET (pOsInfoIfNode, OSPF_TE_ZERO,
                                sizeof (tOsTeOsIntInfo));
                KW_FALSEPOSITIVE_FIX (pOsInfoIfNode);
                /* Fill the information in the created interface */
                pOsInfoIfNode->u1LinkType = OSPF_TE_PTOP;
                pOsInfoIfNode->pTeArea = pTeArea;
                pOsInfoIfNode->u4AddrlessIf = u4AddrlessIf;
                IP_ADDR_COPY (pOsInfoIfNode->linkId, pLinkNode->linkId);
                IP_ADDR_COPY (pOsInfoIfNode->ifIpAddr, ifIpAddr);
                pOsInfoIfNode->u4Metric = pLinkNode->u2LinkCost;
                pOsInfoIfNode->u1InfoType = OSPF_TE_ROUTER_LSA_INFO;

                OspfTeProcessNetNbrInfo ((UINT1 *) &pOsInfoIfNode->linkId,
                                         MAX_IP_ADDR_LEN, OSPF_TE_TRUE,
                                         pOsInfoIfNode->pTeArea);

                /* Adding to the OSPF information interface list */
                OspfTeIfAddToSortOspfIntIfLst (pOsInfoIfNode);
            }
            else
            {
                /* Update the link information */
                IP_ADDR_COPY (pOsInfoIfNode->linkId, pLinkNode->linkId);
                pOsInfoIfNode->u4Metric = pLinkNode->u2LinkCost;
                pOsInfoIfNode->u1InfoType = OSPF_TE_ROUTER_LSA_INFO;
                pOsInfoIfNode->u1LinkType = OSPF_TE_PTOP;
                OspfTeProcessNetNbrInfo ((UINT1 *) &pOsInfoIfNode->linkId,
                                         MAX_IP_ADDR_LEN, OSPF_TE_TRUE,
                                         pOsInfoIfNode->pTeArea);
            }
        }
        /* If the interface is present in TE list */
        else
        {
            if (pOsInfoIfNode == NULL)
            {
                return;
            }
            if (pOsTeInterface->u1ChannelType == OSPF_TE_AREA_CHANNEL)
            {
                return;
            }

            /* Updating the link information */
            /* Setting the Link Type */
            pOsTeInterface->u1LinkType = OSPF_TE_PTOP;
            /* Setting the Link Id */
            IP_ADDR_COPY (pOsTeInterface->linkId, pLinkNode->linkId);
            /* Setting the area of the TE link */
            pOsTeInterface->pTeArea = pTeArea;
            /* If no TE metric value is configured, then set the 
             * OSPF metric as the TE link TE metric */
            if (pOsTeInterface->u1MetricType != OSPF_TE_TE_METRIC)
            {
                pOsTeInterface->u4TeMetric = pLinkNode->u2LinkCost;
            }
            if (OspfTeUtilIpAddrComp (pOsTeInterface->localIpAddr,
                                      gOsTeNullIpAddr) != OSPF_TE_EQUAL)
            {
                /* Copy the Remote IP address from Neighbor information */
                IP_ADDR_COPY (pOsTeInterface->remoteIpAddr,
                              pOsInfoIfNode->nbrIpAddr);
            }
            if (pOsInfoIfNode->pType9Lsa != NULL)
            {
                pOsInfoIfNode->pType9Lsa->pOsTeInt = pOsTeInterface;
                OspfTeLsuAddToSortLsaLst (pOsInfoIfNode->pType9Lsa);
            }
            /* Deleting the OSPF information interface from List */
            TMO_SLL_Delete (&(gOsTeContext.ospfInfoIfLst),
                            &(pOsInfoIfNode->NextOsIntInfoNode));
            /* Free the Memory */
            OSPF_TE_OSPF_INFO_IF_FREE (pOsInfoIfNode);

            /* Setting the status of the TE link to ACTIVE */
            pOsTeInterface->u1LinkStatus = OSPF_TE_ACTIVE;

            /* For unnumbered interface, if the link local identifier is
             * not equal to ZERO, then generate Type 9 LSA */
            /* For Data channel There is no adjacency, so generation of Type 9
             * LSA is not useful for Data channel. In this case Remote
             * identifier infomration is expected from Resource Manager */
            if ((OspfTeUtilIpAddrComp (pOsTeInterface->localIpAddr,
                                       gOsTeNullIpAddr) == OSPF_TE_EQUAL) &&
                (pOsTeInterface->u4LocalIdentifier != OSPF_TE_ZERO) &&
                (pOsTeInterface->u1ChannelType ==
                 OSPF_TE_RM_DATACONTROLLINK_INFO))
            {
                OspfTeGenerateType9Lsa (pOsTeInterface, OSPF_TE_NEW_TLV);
            }

            /* Generate the NEW Link TLV for the TE interface */
            OspfTeGenerateLinkTlv (pOsTeInterface, OSPF_TE_NEW_TLV);
            OspfTeProcessNetNbrInfo ((UINT1 *) &pOsTeInterface->linkId,
                                     MAX_IP_ADDR_LEN, OSPF_TE_TRUE, pTeArea);
        }
    }
    else
    {
        /* Interface is not found in TE list */
        if (pOsTeInterface == NULL)
        {
            if (pOsInfoIfNode == NULL)
            {
                /* Create the entry in the OSPF information interface list */
                if (OSPF_TE_OSPF_INFO_IF_ALLOC (pOsInfoIfNode, tOsTeOsIntInfo)
                    == NULL)
                {
                    OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to allocate "
                                 "Memory for OSPF information interface\n");
                    return;
                }
                OSPF_TE_MEMSET (pOsInfoIfNode, OSPF_TE_ZERO,
                                sizeof (tOsTeOsIntInfo));
                /* Fill the information in the created interface */
                pOsInfoIfNode->u1LinkType = OSPF_TE_MULTIACCESS;
                pOsInfoIfNode->pTeArea = pTeArea;
                pOsInfoIfNode->u4AddrlessIf = u4AddrlessIf;
                IP_ADDR_COPY (pOsInfoIfNode->linkId, pLinkNode->linkId);
                IP_ADDR_COPY (pOsInfoIfNode->ifIpAddr, ifIpAddr);
                pOsInfoIfNode->u4Metric = pLinkNode->u2LinkCost;
                pOsInfoIfNode->u1InfoType = OSPF_TE_ROUTER_LSA_INFO;

                pNetworkLsa = OspfTeUtilFindNetworkLsa (pOsInfoIfNode->linkId,
                                                        pTeArea);

                if (pNetworkLsa != NULL)
                {
                    u2Length =
                        (UINT2) (pNetworkLsa->u2LsaLen -
                                 OSPF_TE_NET_NBRS_OFFSET);
                    pNbrPtr = pNetworkLsa->pLsa + OSPF_TE_NET_NBRS_OFFSET;
                    OspfTeProcessNetNbrInfo (pNbrPtr, u2Length, OSPF_TE_TRUE,
                                             pTeArea);
                }
                /* Adding to the OSPF infomration interface list */
                OspfTeIfAddToSortOspfIntIfLst (pOsInfoIfNode);
            }
            else
            {
                /* Update the link information */
                IP_ADDR_COPY (pOsInfoIfNode->linkId, pLinkNode->linkId);
                pOsInfoIfNode->u4Metric = pLinkNode->u2LinkCost;
                pOsInfoIfNode->u1InfoType = OSPF_TE_ROUTER_LSA_INFO;
                pOsInfoIfNode->u1LinkType = OSPF_TE_MULTIACCESS;
            }
        }
        /* If the interface is present in TE list */
        else
        {
            /* Updating the link information */
            /* Setting the Link Type */
            pOsTeInterface->u1LinkType = OSPF_TE_MULTIACCESS;
            /* Setting the Link Id */
            IP_ADDR_COPY (pOsTeInterface->linkId, pLinkNode->linkId);
            /* Setting the area of the TE link */
            pOsTeInterface->pTeArea = pTeArea;
            /* If no TE metric value is configured, then set the 
             * OSPF metric as the TE link TE metric */
            if (pOsTeInterface->u1MetricType != OSPF_TE_TE_METRIC)
            {
                pOsTeInterface->u4TeMetric = pLinkNode->u2LinkCost;
            }
            /* Setting the status of the TE link to ACTIVE */
            pOsTeInterface->u1LinkStatus = OSPF_TE_ACTIVE;
            /* Generate the NEW Link TLV for the TE interface */
            OspfTeGenerateLinkTlv (pOsTeInterface, OSPF_TE_NEW_TLV);

            pNetworkLsa = OspfTeUtilFindNetworkLsa (pLinkNode->linkId, pTeArea);

            if (pNetworkLsa != NULL)
            {
                u2Length =
                    (UINT2) (pNetworkLsa->u2LsaLen - OSPF_TE_NET_NBRS_OFFSET);
                pNbrPtr = pNetworkLsa->pLsa + OSPF_TE_NET_NBRS_OFFSET;
                OspfTeProcessNetNbrInfo (pNbrPtr, u2Length, OSPF_TE_TRUE,
                                         pTeArea);
            }
        }
    }
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessRtrLsaNewLink\n");
    KW_FALSEPOSITIVE_FIX (pOsInfoIfNode);
    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessRtrLsaModifyLink                             */
/*  Description :  This function processes modified link in the Router LSA   */
/*                 and make the Link TLV to be re-generated                  */
/*  Input       :  pLinkNode - Points to Link node                           */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessRtrLsaModifyLink (tLinkNode * pLinkNode)
{
    tIPADDR             ifIpAddr;
    UINT4               u4AddrlessIf;
    tOsTeOsIntInfo     *pOsInfoIfNode = NULL;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessRtrLsaModifyLink\n");

    OspfTeGetIfInfoFromRtrLsaLink (pLinkNode->linkData, &ifIpAddr,
                                   &u4AddrlessIf);

    /* See whether the TE link is present or not */
    pOsTeInterface = OspfTeFindTeInterface (ifIpAddr, u4AddrlessIf);
    if (pOsTeInterface == NULL)
    {
        /* Find the interface in OSPF information interface list */
        pOsInfoIfNode = OspfTeFindOspfInfoInt (ifIpAddr, u4AddrlessIf);
        if (pOsInfoIfNode == NULL)
        {
            return;
        }
        /* Update the Link information */
        IP_ADDR_COPY (pOsInfoIfNode->linkId, pLinkNode->linkId);
        /* Update the metric if the Metric is not learned from 
         * Resource Manager */
        pOsInfoIfNode->u4Metric = pLinkNode->u2LinkCost;
    }
    else
    {
        /* Updating the Link Id field */
        IP_ADDR_COPY (pOsTeInterface->linkId, pLinkNode->linkId);
        /* Update the metric if the Metric is not learned from 
         * Resource Manager */
        if (pOsTeInterface->u1MetricType != OSPF_TE_TE_METRIC)
        {
            pOsTeInterface->u4TeMetric = pLinkNode->u2LinkCost;
        }

        if (pOsTeInterface->u1LinkStatus == OSPF_TE_ACTIVE)
        {
            /* Generate the NEW Link TLV for the TE interface */
            OspfTeGenerateLinkTlv (pOsTeInterface, OSPF_TE_NEW_INSTANCE_TLV);
        }
    }
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessRtrLsaModifyLink\n");
    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessRtrLsaFlushLink                              */
/*  Description :  This function processes Flushed Router LSA link           */
/*                 It generates Flushed TLV for those links that were        */
/*                 deleted.                                                  */
/*  Input       :  pLinkNode - Points to Link Node.                          */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessRtrLsaFlushLink (tLinkNode * pLinkNode, tOsTeArea * pTeArea)
{
    tIPADDR             ifIpAddr;
    UINT4               u4AddrlessIf;
    tOsTeLsaNode       *pType9Lsa = NULL;
    tOsTeOsIntInfo     *pOsInfoIfNode = NULL;
    tOsTeInterface     *pOsTeInterface = NULL;
    tOsTeInterface     *pOsTeDataLink = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeLsaNode       *pNetworkLsa = NULL;
    UINT1              *pNbrPtr = NULL;
    UINT2               u2Length;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessRtrLsaFlushLink\n");

    OspfTeGetIfInfoFromRtrLsaLink (pLinkNode->linkData, &ifIpAddr,
                                   &u4AddrlessIf);

    /* See whether the TE link is present or not */
    pOsTeInterface = OspfTeFindTeInterface (ifIpAddr, u4AddrlessIf);

    if (pOsTeInterface == NULL)
    {
        /* Find the interface in OSPF information interface list */
        pOsInfoIfNode = OspfTeFindOspfInfoInt (ifIpAddr, u4AddrlessIf);

        if (pOsInfoIfNode == NULL)
        {
            return;
        }

        pNetworkLsa = OspfTeUtilFindNetworkLsa (pOsInfoIfNode->linkId,
                                                pOsInfoIfNode->pTeArea);

        if (pNetworkLsa != NULL)
        {
            u2Length =
                (UINT2) (pNetworkLsa->u2LsaLen - OSPF_TE_NET_NBRS_OFFSET);
            pNbrPtr = pNetworkLsa->pLsa + OSPF_TE_NET_NBRS_OFFSET;
            OspfTeProcessNetNbrInfo (pNbrPtr, u2Length, OSPF_TE_FALSE,
                                     pOsInfoIfNode->pTeArea);
        }

        if (pOsInfoIfNode->u1LinkType == OSPF_TE_PTOP)
        {
            TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode,
                          tTMO_SLL_NODE *)
            {
                pOsTeDataLink = OSPF_TE_GET_BASE_PTR (tOsTeInterface,
                                                      NextSortLinkNode,
                                                      pLstNode);
                if (pOsTeDataLink->u1ChannelType != OSPF_TE_DATA_CHANNEL)
                {
                    continue;
                }

                if ((OspfTeUtilIpAddrComp (pOsTeDataLink->linkId,
                                           pOsInfoIfNode->linkId) ==
                     OSPF_TE_EQUAL))
                {
                    pOsTeDataLink->u1LinkStatus = OSPF_TE_INVALID;
                    OspfTeGenerateLinkTlv (pOsTeDataLink, OSPF_TE_FLUSHED_TLV);
                }
            }
        }
        /* Remove interface from List */
        TMO_SLL_Delete (&(gOsTeContext.ospfInfoIfLst),
                        &(pOsInfoIfNode->NextOsIntInfoNode));
        /* Free the Memory */
        OSPF_TE_OSPF_INFO_IF_FREE (pOsInfoIfNode);
    }
    else
    {
        if (pOsTeInterface->u1LinkStatus == OSPF_TE_ACTIVE)
        {
            /* Generate the NEW Link TLV for the TE interface */
            OspfTeGenerateLinkTlv (pOsTeInterface, OSPF_TE_FLUSHED_TLV);

            /* Setting the status of the TE link to INACTIVE */
            pOsTeInterface->u1LinkStatus = OSPF_TE_INVALID;
        }

        if (pLinkNode->u1LinkType == PTOP_LINK)
        {
            OspfTeProcessNetNbrInfo ((UINT1 *) &pLinkNode->linkId,
                                     MAX_IP_ADDR_LEN, OSPF_TE_FALSE, pTeArea);
        }

        if (pOsTeInterface->u1LinkStatus == OSPF_TE_ACTIVE)
        {
            pType9Lsa = OspfTeFlushOpaqueLsa (pOsTeInterface);
        }
        if (pOsTeInterface->u1LinkType != OSPF_TE_INVALID &&
            pOsTeInterface->u1ChannelType == OSPF_TE_DATA_CONTROL_CHANNEL)
        {
            OspfTeCreateOspfInfoInterface (pOsTeInterface, pType9Lsa);
        }

    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessRtrLsaFlushLink\n");
    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessNetLsa                                       */
/*  Description :  This function processes Network LSA to handle data        */
/*                 channels.                                                 */
/*  Input       :  pOsTeLsa  - Points to Lsa Node.                           */
/*                 pOsLsaInfo- Points to Lsa info received.                  */
/*                 pTeArea   - Points to TE Area.                            */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PRIVATE VOID
OspfTeProcessNetLsa (tOsTeLsaNode * pOsTeLsa, tOsLsaInfo * pOsLsaInfo,
                     tOsTeArea * pTeArea)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1              *pOldPtr = NULL;
    UINT1              *pNewNbrs = NULL;
    UINT1              *pDelNbrs = NULL;
    tOsTeInterface     *pOsTeInterface = NULL;
    tOsTeOsIntInfo     *pOsTeOsIntInfo = NULL;
    UINT1              *pPtr = NULL;
    tIPADDR             linkStateId;
    UINT2               u2Length;
    UINT2               u2OldLength;
    UINT2               u2NbrLength;
    UINT1               u1Found = OSPF_TE_FALSE;

    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, pOsLsaInfo->u4LinkStateId);

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR (tOsTeInterface,
                                               NextSortLinkNode, pLstNode);

        if (pOsTeInterface->u1ChannelType != OSPF_TE_DATA_CONTROL_CHANNEL)
        {
            u1Found = OSPF_TE_TRUE;
        }
    }

    /* If no control data channel is present. no need to process the
     * Network LSA. */
    if (u1Found == OSPF_TE_FALSE)
    {
        return;
    }

    u1Found = OSPF_TE_FALSE;

    /* Network LSAs belongs to Router interfaces only need to be processed */
    TMO_SLL_Scan (&(gOsTeContext.ospfInfoIfLst), pOsTeOsIntInfo,
                  tOsTeOsIntInfo *)
    {
        if ((pOsTeOsIntInfo->u1LinkType == OSPF_TE_MULTIACCESS) &&
            (OspfTeUtilIpAddrComp (pOsTeOsIntInfo->linkId, linkStateId)
             == OSPF_TE_EQUAL))
        {
            u1Found = OSPF_TE_TRUE;
        }
    }

    if (u1Found == OSPF_TE_FALSE)
    {
        return;
    }

    if (pOsTeLsa == NULL)
    {
        u2Length = (UINT2) (pOsLsaInfo->u2LsaLen - OSPF_TE_NET_NBRS_OFFSET);
        pPtr = pOsLsaInfo->pu1Lsa + OSPF_TE_NET_NBRS_OFFSET;
        OspfTeProcessNetNbrInfo (pPtr, u2Length, OSPF_TE_TRUE, pTeArea);
    }
    else if (pOsLsaInfo->u1LsaStatus == OSPF_TE_FLUSHED_LSA)
    {
        u2Length = (UINT2) (pOsTeLsa->u2LsaLen - OSPF_TE_NET_NBRS_OFFSET);
        pPtr = pOsTeLsa->pLsa + OSPF_TE_NET_NBRS_OFFSET;
        OspfTeProcessNetNbrInfo (pPtr, u2Length, OSPF_TE_FALSE, pTeArea);
    }
    else
    {
        if (OSPF_TE_MAX_NET_NBRS_ALLOC (pNewNbrs, UINT1) == NULL)
        {
            OSPF_TE_TRC (OSPF_TE_RESOURCE_TRC,
                         "Failed to allocate memory for Neighbors list,"
                         "In processing Network LSA\n");
            return;
        }

        u2Length = (UINT2) (pOsLsaInfo->u2LsaLen - OSPF_TE_NET_NBRS_OFFSET);

        u2OldLength = (UINT2) (pOsTeLsa->u2LsaLen - OSPF_TE_NET_NBRS_OFFSET);

        pPtr = pOsLsaInfo->pu1Lsa + OSPF_TE_NET_NBRS_OFFSET;
        pOldPtr = pOsTeLsa->pLsa + OSPF_TE_NET_NBRS_OFFSET;

        u2NbrLength = OspfTeFindChangedNbrs (pPtr, u2Length, pOldPtr,
                                             u2OldLength, pNewNbrs);

        /* Processing NEW Neighbors */
        OspfTeProcessNetNbrInfo (pNewNbrs, u2NbrLength, OSPF_TE_TRUE, pTeArea);

        pDelNbrs = pNewNbrs;

        u2NbrLength = OspfTeFindChangedNbrs (pOldPtr, u2OldLength,
                                             pPtr, u2Length, pDelNbrs);

        OspfTeProcessNetNbrInfo (pDelNbrs, u2NbrLength, OSPF_TE_FALSE, pTeArea);
        OSPF_TE_MAX_NET_NBRS_FREE (pDelNbrs);
    }
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessNetNbrInfo                                   */
/*  Description :  This function processes Network LSA to handle data        */
/*                 channels.                                                 */
/*  Input       :  pPtr      - Points to Neighors list.                      */
/*                 u2Length  - Neighbor Rtr Ids length.                      */
/*                 u1Flag    - Flag to indicate the add/delete               */
/*                 pTeArea   - Points to TE Area.                            */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PRIVATE VOID
OspfTeProcessNetNbrInfo (UINT1 *pPtr, UINT2 u2Length, UINT1 u1Flag,
                         tOsTeArea * pOsTeArea)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeInterface     *pOsTeInterface = NULL;
    tIPADDR             nbrRtrId;
    UINT2               u2Count;

    for (u2Count = OSPF_TE_ZERO; u2Count < u2Length;
         u2Count = (UINT2) (u2Count + MAX_IP_ADDR_LEN))
    {
        LGETSTR (pPtr, nbrRtrId, MAX_IP_ADDR_LEN);
        TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pOsTeInterface = OSPF_TE_GET_BASE_PTR (tOsTeInterface,
                                                   NextSortLinkNode, pLstNode);
            /* If the Channel is control channel do nothing */
            if (pOsTeInterface->u1ChannelType != OSPF_TE_DATA_CHANNEL)
            {
                continue;
            }

            if (OspfTeUtilIpAddrComp (pOsTeInterface->linkId, nbrRtrId)
                == OSPF_TE_EQUAL)
            {
                if (u1Flag == OSPF_TE_TRUE)
                {
                    pOsTeInterface->u1LinkStatus = OSPF_TE_ACTIVE;
                    pOsTeInterface->pTeArea = pOsTeArea;
                    OspfTeGenerateLinkTlv (pOsTeInterface, OSPF_TE_NEW_TLV);
                }
                else if ((u1Flag == OSPF_TE_FALSE) &&
                         (pOsTeInterface->u1LinkStatus == OSPF_TE_ACTIVE))
                {
                    pOsTeInterface->u1LinkStatus = OSPF_TE_INVALID;
                    OspfTeGenerateLinkTlv (pOsTeInterface, OSPF_TE_FLUSHED_LSA);
                }
            }
        }
    }
}

/*****************************************************************************/
/*  Function    :  OspfTeFindChangedNbrs                                     */
/*  Description :  This function processes Network LSA to handle data        */
/*                 channels.                                                 */
/*  Input       :  pPtr      - Points to Neighors List1.                     */
/*                 u2Length  - Neighbor Rtr Ids length of List1.             */
/*                 pOldPtr   - Points to Neighors List2.                     */
/*                 u2OldLength  - Neighbor Rtr Ids length of List2.          */
/*  Output      :  None                                                      */
/*  Returns     :  Length of the neighbors list.                             */
/*****************************************************************************/
PRIVATE UINT2
OspfTeFindChangedNbrs (UINT1 *pPtr, UINT2 u2Length,
                       UINT1 *pOldPtr, UINT2 u2OldLength, UINT1 *pNbrs)
{
    UINT2               u2Count1;
    UINT2               u2Count2;
    tIPADDR             oldNbrRtrId;
    tIPADDR             newNbrRtrId;
    UINT2               u2NbrLength = OSPF_TE_ZERO;
    UINT1               u1Found = OSPF_TE_FALSE;
    UINT1              *pOrgOldPtr = pOldPtr;

    for (u2Count1 = OSPF_TE_ZERO; u2Count1 < u2Length;
         u2Count1 = (UINT2) (u2Count1 + MAX_IP_ADDR_LEN))
    {
        u1Found = OSPF_TE_FALSE;
        if (u2NbrLength >= OSPF_TE_NET_NBRS_SIZE)
        {
            break;
        }

        LGETSTR (pPtr, newNbrRtrId, MAX_IP_ADDR_LEN);
        pOldPtr = pOrgOldPtr;

        for (u2Count2 = OSPF_TE_ZERO; u2Count2 < u2OldLength;
             u2Count2 = (UINT2) (u2Count2 + MAX_IP_ADDR_LEN))
        {
            LGETSTR (pOldPtr, oldNbrRtrId, MAX_IP_ADDR_LEN);
            if (OspfTeUtilIpAddrComp (newNbrRtrId, oldNbrRtrId)
                == OSPF_TE_EQUAL)
            {
                u1Found = OSPF_TE_TRUE;
                break;
            }
        }
        if (u1Found == OSPF_TE_FALSE)
        {
            IP_ADDR_COPY (pNbrs + u2NbrLength, newNbrRtrId);
            u2NbrLength = (UINT2) (u2NbrLength + MAX_IP_ADDR_LEN);
        }
    }
    return u2NbrLength;
}

/*****************************************************************************/
/*  Function    :  OspfInTeActivateTeLinks                                   */
/*  Description :  This function in-activates all the TE-Links present in the*/
/*                 area and Generates Flush TLV if needed                    */
/*  Input       :  pOsTeArea     - Pointer to Area                           */
/*                 u1Flag        - If OSPF_TE_TRUE, generates Flush TLV      */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeInactivateTeLink (tOsTeArea * pOsTeArea, UINT1 u1Flag)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeInterface     *pOsTeDataLink = NULL;

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeDataLink = OSPF_TE_GET_BASE_PTR (tOsTeInterface,
                                              NextSortLinkNode, pLstNode);

        if ((pOsTeDataLink->u1ChannelType == OSPF_TE_AREA_CHANNEL) &&
            (pOsTeDataLink->u1LinkStatus == OSPF_TE_ACTIVE) &&
            (pOsTeDataLink->pTeArea == pOsTeArea))
        {
            pOsTeDataLink->u1LinkStatus = OSPF_TE_INVALID;
            if (u1Flag == OSPF_TE_TRUE)
            {
                OspfTeGenerateLinkTlv (pOsTeDataLink, OSPF_TE_FLUSHED_TLV);
            }
        }
    }
}

/*****************************************************************************/
/*  Function    :  OspfTeActivateTeLinks                                     */
/*  Description :  This function Activates all the TE-Links present in the   */
/*                 area and Generates Link TLV                               */
/*  Input       :  pOsTeArea     - Pointer to Area                          */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeActivateTeLinks (tOsTeArea * pOsTeArea)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeInterface     *pOsTeDataLink = NULL;

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeDataLink = OSPF_TE_GET_BASE_PTR (tOsTeInterface,
                                              NextSortLinkNode, pLstNode);

        if ((pOsTeDataLink->u1ChannelType == OSPF_TE_AREA_CHANNEL) &&
            (pOsTeDataLink->pTeArea == pOsTeArea) &&
            (OspfTeUtilIpAddrComp (pOsTeDataLink->linkId, gOsTeNullIpAddr)
             != OSPF_TE_EQUAL))
        {
            pOsTeDataLink->u1LinkStatus = OSPF_TE_ACTIVE;
            OspfTeGenerateLinkTlv (pOsTeDataLink, OSPF_TE_NEW_TLV);
        }
    }
}

/*****************************************************************************/
/*  Function    :  OspfTeCreateOspfInfoInterface                             */
/*  Description :  This function Creates the OspfIntInfo from the            */
/*                 OsteInterface provided                                    */
/*  Input       :  pOsTeInterface - Pointer to TE node                       */
/*                 tOsTeLsaNode - Type 9 LSA                                 */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeCreateOspfInfoInterface (tOsTeInterface * pOsTeInterface,
                               tOsTeLsaNode * pType9Lsa)
{
    tOsTeOsIntInfo     *pOsTeOsIntInfo = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCreateOspfInfoInterface\n");
    if (OSPF_TE_OSPF_INFO_IF_ALLOC (pOsTeOsIntInfo, tOsTeOsIntInfo) == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to allocate "
                     "Memory for OSPF information interface\n");
        return;
    }
    OSPF_TE_MEMSET (pOsTeOsIntInfo, OSPF_TE_ZERO, sizeof (tOsTeOsIntInfo));

    /* Fill the information in the created interface */
    pOsTeOsIntInfo->pTeArea = pOsTeInterface->pTeArea;
    pOsTeOsIntInfo->u4AddrlessIf = pOsTeInterface->u4AddrlessIf;

    pOsTeOsIntInfo->u1LinkType = pOsTeInterface->u1LinkType;

    IP_ADDR_COPY (pOsTeOsIntInfo->ifIpAddr, pOsTeInterface->localIpAddr);

    IP_ADDR_COPY (pOsTeOsIntInfo->linkId, pOsTeInterface->linkId);

    if (pOsTeInterface->u1LinkType == OSPF_TE_PTOP)
    {
        if ((OspfTeUtilIpAddrComp (pOsTeInterface->localIpAddr,
                                   gOsTeNullIpAddr) == OSPF_TE_EQUAL))
        {
            IP_ADDR_COPY (pOsTeOsIntInfo->nbrIpAddr, pOsTeInterface->linkId);
        }
        else
        {
            IP_ADDR_COPY (pOsTeOsIntInfo->nbrIpAddr,
                          pOsTeInterface->remoteIpAddr);
        }
        if (pOsTeInterface->u1LinkStatus == OSPF_TE_ACTIVE)
        {
            pOsTeOsIntInfo->u1InfoType = OSPF_TE_ROUTER_LSA_INFO;
        }
        else
        {
            pOsTeOsIntInfo->u1InfoType = OSPF_TE_NEIGHBOR_INFO;
        }
    }
    else
    {
        pOsTeOsIntInfo->u1InfoType = OSPF_TE_ROUTER_LSA_INFO;
    }
    if (pType9Lsa != NULL)
    {
        pOsTeOsIntInfo->pType9Lsa = pType9Lsa;
    }
    /* Adding to the OSPF information interface list */
    OspfTeIfAddToSortOspfIntIfLst (pOsTeOsIntInfo);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCreateOspfInfoInterface\n");
    KW_FALSEPOSITIVE_FIX (pOsTeOsIntInfo);
    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeFlushOpaqueLsa                                      */
/*  Description :  This function Flushes the Self-Originated Opaque LSAs     */
/*  Input       :  pOsTeInterface - Pointer to TE node                       */
/*  Output      :  None                                                      */
/*  Returns     :  tOsTeLsaNode - Pointer to Type 9 LSA                      */
/*****************************************************************************/
PUBLIC tOsTeLsaNode *
OspfTeFlushOpaqueLsa (tOsTeInterface * pOsTeInterface)
{
    tOsTeLsaNode       *pType9Lsa = NULL;
    tOsTeLsaNode       *pTeLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeFlushOpaqueLsa\n");
    /* Scan the Type9 LSA list and flush out the Self originated 
     * Type 9 LSA */
    TMO_SLL_Scan (&(pOsTeInterface->type9TELsaLst), pLstNode, tTMO_SLL_NODE *)
    {
        pTeLsaInfo = OSPF_TE_GET_BASE_PTR (tOsTeLsaNode,
                                           NextSortLsaNode, pLstNode);

        /* Self originated Type 9 LSA */
        if (OspfTeUtilIpAddrComp (pTeLsaInfo->advRtrId,
                                  gOsTeContext.rtrId) == OSPF_TE_EQUAL)
        {
            OspfTeGenerateType9Lsa (pOsTeInterface, OSPF_TE_FLUSHED_TLV);
        }

        else
        {
            pType9Lsa = pTeLsaInfo;
        }
    }

    if (pType9Lsa != NULL)
    {
        TMO_SLL_Delete (&(pOsTeInterface->type9TELsaLst),
                        &(pType9Lsa->NextSortLsaNode));
    }

    OspfTeGenerateLinkTlv (pOsTeInterface, OSPF_TE_FLUSHED_TLV);
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeFlushOpaqueLsa\n");
    return pType9Lsa;
}

/*****************************************************************************/
/*  Function    :  OspfTeGenerateLsaForFA                                    */
/*  Description :  This function generates the Type 10 LSA for FA channel    */
/*  Input       :  advtRtrId - Advertising Router ID                         */
/*                 pOsTeArea - Associated Area                               */
/*                 u1Flag    - Flag to indicate the add/delete               */
/*  Output      :  None                                                      */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PRIVATE VOID
OspfTeGenerateLsaForFA (tIPADDR advtRtrId, tOsTeArea * pOsTeArea)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeInterface     *pOsTeInterface = NULL;

    /* No need to care about the Flushing of FA TELink Information here.
     * It will be done by "OspfTeInactivateTeLink" function for both
     * RMGR and TLM based TE Links
     */

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR (tOsTeInterface,
                                               NextSortLinkNode, pLstNode);

        /* If the channel type is not a AREA CHANNEL (FA) do nothing */
        if ((pOsTeInterface->u1ChannelType != OSPF_TE_AREA_CHANNEL) &&
            (pOsTeInterface->u1ModuleId != OSPF_TE_TLM_MSG))
        {
            continue;
        }

        if (OspfTeUtilIpAddrComp (pOsTeInterface->linkId, advtRtrId)
            == OSPF_TE_EQUAL)
        {
            if (pOsTeInterface->u1LinkStatus != OSPF_TE_ACTIVE)
            {
                pOsTeInterface->u1LinkStatus = OSPF_TE_ACTIVE;
                pOsTeInterface->pTeArea = pOsTeArea;
                pOsTeInterface->u4AreaId = pOsTeArea->u4AreaId;
                OspfTeGenerateLinkTlv (pOsTeInterface, OSPF_TE_NEW_TLV);
            }
        }
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osteosif.c                     */
/*-----------------------------------------------------------------------*/
