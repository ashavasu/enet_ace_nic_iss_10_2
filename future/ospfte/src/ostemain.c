/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostemain.c,v 1.16 2016/02/26 09:48:48 siva Exp $
 *
 * Description:  This file contains the OSPF TE task main loop.
 *               and the initialisation routines.
 * *********************************************************************/

#include "osteinc.h"
#include "osteglob.h"

#include "fsotewr.h"
#ifdef OSPFTEAPPSIM_WANTED
#include "fsoteawr.h"
#endif
#ifdef OSPFTERMSIM_WANTED
#include "fsoterwr.h"
#endif
/***************************************************************************/
/*  Function    :  OspfTeTaskMain                                          */
/*                                                                         */
/*  Description :  Main function of OSPF-TE                                */
/*                                                                         */
/*  Input       :  pTaskId                                                 */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  None                                                    */
/***************************************************************************/
PUBLIC VOID
OspfTeTaskMain (INT1 *pTaskId)
{
    tOspfTeQMsg        *pOspfTeQMsg = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeTaskMain\n");

    UNUSED_PARAM (pTaskId);

    /* OSPF-TE Task initializations */
    if (OspfTeTaskInit () == OSPF_TE_FAILURE)
    {
        /* Failure in Task initializations */
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC,
                     "!!!!! OSPF-TE TASK INIT FAILURE  !!!!!\n");
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    RegisterOSPFTEMibs ();
    lrInitComplete (OSIX_SUCCESS);

    while (OSPF_TE_ONE)
    {
        /* Wait on Queue till message is posted from OSPF or RM */
        while (OsixQueRecv
               (gOspfTeQid, (UINT1 *) &pOspfTeQMsg, OSIX_DEF_MSG_LEN,
                OSIX_WAIT) == OSIX_SUCCESS)
        {
            OspfTeLock ();

            OspfTeMsgHdlr (pOspfTeQMsg);
            /* Free the Q Memory block after processing the message */
            OSPF_TE_QMSG_FREE (pOspfTeQMsg);

            OspfTeUnLock ();
        }
    }
}

/***************************************************************************/
/*  Function    :  OspfTeTaskInit                                          */
/*                                                                         */
/*  Description :  This function creates Q and initialises memory for      */
/*                 TE operation.                                           */
/*                                                                         */
/*  Input       :  None                                                    */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                         */
/***************************************************************************/
PUBLIC INT4
OspfTeTaskInit (VOID)
{
    tOsixQId            u4TeQId;
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeTaskInit\n");

    if (OspfTeInit () == OSPF_TE_FAILURE)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to intialize protocol "
                     "Global structue\n");
        return OSPF_TE_FAILURE;
    }

    /* Create Queue for receiving messages from OSPF and RM */
    if (OsixCreateQ (OSPF_TE_Q_NAME, OSPF_TE_Q_DEPTH, OSIX_GLOBAL,
                     (tOsixQId *) & (u4TeQId)) != OSIX_SUCCESS)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC,
                     "!!!!! OSTE Creation Failed  !!!!!\n");
        return OSPF_TE_FAILURE;
    }
    gOspfTeQid = u4TeQId;

    if (OsixCreateSem (OSPF_TE_MUT_EXCL_SEM_NAME, OSPF_TE_SEM_CREATE_INIT_CNT,
                       OSPF_TE_ZERO, &gOspfTeSemId) != OSIX_SUCCESS)
    {
        OSPF_TE_TRC1 (OSPF_TE_RESOURCE_TRC | OSPF_TE_CRITICAL_TRC,
                      "Semaphore Creation failure for %s \n",
                      OSPF_TE_MUT_EXCL_SEM_NAME);
        return OSPF_TE_FAILURE;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeTaskInit\n");

    return OSPF_TE_SUCCESS;
}

/***************************************************************************/
/*  Function    :  OspfTeInit                                              */
/*                                                                         */
/*  Description :  This module initialises the TE Context structure        */
/*                                                                         */
/*  Input       :  None                                                    */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  OSPF_TE_SUCCESS / OSPF_TE_FAILURE                       */
/***************************************************************************/
PUBLIC INT4
OspfTeInit (VOID)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeInit\n");

    /* Initialise the TE context structure */
    OSPF_TE_MEMSET (&gOsTeContext, OSPF_TE_ZERO, sizeof (tOsTeContext));

    /* Set the default admin status */
    gOsTeContext.u1TeAdminStat = OSPF_TE_DISABLED;
    /* Set the Trace Level to Critical Trace */
    gOsTeContext.u4TeTrcValue = OSPF_TE_CRITICAL_TRC;

    /* Creates a Hash Table which stores all the link information 
     * given by the Resource Manager */
    gOsTeContext.pTeIfHashTbl = TMO_HASH_Create_Table
        (OSPF_TE_IF_HASH_TBL_SIZE, NULL, FALSE);

    if (gOsTeContext.pTeIfHashTbl == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_RESOURCE_TRC,
                     "Interface Hash Table creation FAILED\n");
        return OSPF_TE_FAILURE;
    }

    /* Creates a Hash Table which stores all the candidate nodes which
     * holds the links satisfying the constraints */
    gOsTeContext.pTeCandteLst = TMO_HASH_Create_Table
        (OSPF_TE_CAND_HASH_TBL_SIZE, NULL, FALSE);

    if (gOsTeContext.pTeCandteLst == NULL)
    {
        TMO_HASH_Delete_Table (gOsTeContext.pTeIfHashTbl, NULL);
        OSPF_TE_TRC (OSPF_TE_RESOURCE_TRC,
                     "Candidate Hash Table Creation FAILED\n");
        return OSPF_TE_FAILURE;
    }

    /* Initializing TE interfaces list */
    TMO_SLL_Init (&(gOsTeContext.sortTeIfLst));
    /* Initializing OSPF information interfaces list */
    TMO_SLL_Init (&(gOsTeContext.ospfInfoIfLst));
    /* Initializing areas list */
    TMO_SLL_Init (&(gOsTeContext.areasLst));

#ifdef OSPFTEAPPSIM_WANTED
    TMO_SLL_Init (&gOsTeAppConsLst);
#endif
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeInit\n");

    return OSPF_TE_SUCCESS;
}

/***************************************************************************/
/*  Function    :  OspfTeStart                                             */
/*                                                                         */
/*  Description :  This module starts the TE task                          */
/*                                                                         */
/*  Input       :  None                                                    */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  OSPF_TE_SUCCESS / OSPF_TE_FAILURE                       */
/***************************************************************************/
PUBLIC INT4
OspfTeStart (VOID)
{
    tOsixTaskId         u4OspfTeTaskId;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeStart\n");

    /* Spawn OSPF-TE Task */
    if (OsixCreateTask ((CONST UINT1 *) OSPF_TE_TASK_NAME,
                        OSPF_TE_TASK_PRIORITY, OSIX_DEFAULT_STACK_SIZE,
                        OspfTeTaskMain, NULL, OSIX_DEFAULT_TASK_MODE,
                        (tOsixTaskId *) & (u4OspfTeTaskId)) != OSIX_SUCCESS)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "OSPF Task creation failed\n");
        return OSPF_TE_FAILURE;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeStart\n");

    return OSPF_TE_SUCCESS;
}

/***************************************************************************/
/*  Function    :  OspfTeMsgHdlr                                           */
/*                                                                         */
/*  Description :  This module extracts the message type from the buffer.  */
/*                 It then calls the respective module to process this     */
/*                 message                                                 */
/*                                                                         */
/*  Input       :  pBuffer - Message from OSPF/RM                          */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  None                                                    */
/***************************************************************************/
PUBLIC VOID
OspfTeMsgHdlr (tOspfTeQMsg * pOspfTeQMsg)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeMsgHdlr\n");

    /* If TE admin status is down no need to process the messages */
    if (gOsTeContext.u1TeAdminStat == OSPF_TE_DISABLED)
    {
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                     "OSPF-TE DOWN, DISCARDED RM message\n");
        return;
    }

    /* Calls the respective module for processing the message 
     * received based on the message type */
    switch (pOspfTeQMsg->u4MsgType)
    {
        case OSPF_TE_RMGR_MSG:
            OspfTeProcessRmgrMsg (&(pOspfTeQMsg->rmOspfTeIfParam));
            break;
        case OSPF_TE_CSPF_MSG:
            OspfTeUnLock ();
            OspfTeProcessCspfCompMsg (&(pOspfTeQMsg->cspfCompInfo));
            OspfTeLock ();
            break;
        case OSPF_TE_TLM_MSG:
            OspfTeProcessTlmMsg (&(pOspfTeQMsg->tlmOspfTeIfParam));
            break;
        default:
            OSPF_TE_TRC1 (OSPF_TE_FAILURE_TRC,
                          "Invalid message Type %x received from \
               external entity", pOspfTeQMsg->u4MsgType);
            break;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeMsgHdlr\n");

    return;
}

/***************************************************************************/
/*  Function    :  OspfTeMemInit                                           */
/*                                                                         */
/*  Description :  This module creates Memory Pool needed for the Protocol */
/*                 operation. It also initialises the values to defaults.  */
/*                                                                         */
/*  Input       :  None                                                    */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  OSPF_TE_SUCCESS / OSPF_TE_FAILURE                       */
/***************************************************************************/
PUBLIC INT4
OspfTeMemInit (VOID)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeMemInit\n");

    /* Creating Memory Pool for OSPF_TE data structures */
    if (OspfTeCreateMemPool () != OSPF_TE_SUCCESS)
    {
        /* Free If any mem pools are allocated */
        OspfTeShutDown ();
        OSPF_TE_TRC (OSPF_TE_RESOURCE_TRC,
                     "Failure in Memory Pool creation \n");
        return OSPF_TE_FAILURE;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeMemInit\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeCreateMemPool                                       */
/*                                                                           */
/*  Description :  This module creates memory pool for Protocol operation.   */
/*                                                                           */
/*  Input       :  None                                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS / OSPF_TE_FAILURE                         */
/*****************************************************************************/
PUBLIC INT4
OspfTeCreateMemPool (VOID)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCreateMemPool\n");

    if (OSIX_FAILURE == OspfteSizingMemCreateMemPools ())
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Memory Pool creation FAILED\n");
        return OSPF_TE_FAILURE;
    }

    OSPF_TE_IF_POOL_ID = OSPFTEMemPoolIds[MAX_OSPF_TE_INTF_SIZING_ID];
    OSPF_TE_OSPF_INFO_POOL_ID =
        OSPFTEMemPoolIds[MAX_OSPF_TE_OSINT_INFO_SIZING_ID];
    OSPF_TE_IF_DESC_POOL_ID = OSPFTEMemPoolIds[MAX_OSPF_TE_DESC_SIZING_ID];
    OSPF_TE_AREA_POOL_ID = OSPFTEMemPoolIds[MAX_OSPF_TE_AREAS_SIZING_ID];
    OSPF_TE_TEDB_HASH_POOL_ID = OSPFTEMemPoolIds[MAX_OSPF_TE_DB_NODE_SIZING_ID];
    OSPF_TE_LSA_INFO_POOL_ID =
        OSPFTEMemPoolIds[MAX_OSPF_TE_LSAS_NODE_SIZING_ID];
    OSPF_TE_LINK_TLV_POOL_ID =
        OSPFTEMemPoolIds[MAX_OSPF_TE_LINK_TLV_NODE_SIZING_ID];
    OSPF_TE_CANDTE_POOL_ID =
        OSPFTEMemPoolIds[MAX_OSPF_TE_CANDTE_NODE_SIZING_ID];
    OSPF_TE_QMSG_QID = OSPFTEMemPoolIds[MAX_OSPF_TE_Q_DEPTH_SIZING_ID];
    OSPF_TE_PATH_POOL_ID = OSPFTEMemPoolIds[MAX_OSPF_TE_PATH_NODE_SIZING_ID];
    OSPF_TE_CSPF_REQ_POOL_ID =
        OSPFTEMemPoolIds[MAX_OSPF_TE_CSPF_REQUEST_SIZING_ID];
    OSPF_TE_CSPF_CONS_POOL_ID = OSPFTEMemPoolIds[MAX_OSPF_TE_CONS_SIZING_ID];
    OSPF_TE_LSA_POOL_ID = OSPFTEMemPoolIds[MAX_OSPF_TE_LSAS_SIZING_ID];
    OSPF_TE_MAX_NET_NBRS_POOL_ID =
        OSPFTEMemPoolIds[MAX_OSPF_TE_MAX_NET_NBRS_SIZING_ID];
    OSPF_TE_LINK_TLV_SIZE_POOL_ID =
        OSPFTEMemPoolIds[MAX_OSPF_TE_LINK_TLV_SIZE_SIZING_ID];
    OSPF_TE_RM_LINK_INFO_POOL_ID =
        OSPFTEMemPoolIds[MAX_OSPF_TE_RM_LINK_INFO_SIZING_ID];
    OSPF_TE_RM_LINK_MSG_POOL_ID =
        OSPFTEMemPoolIds[MAX_OSPF_TE_RM_LINK_MSG_BLOCKS_SIZING_ID];
    OSPF_TE_RM_LINK_SRLG_INFO_POOL_ID =
        OSPFTEMemPoolIds[MAX_OSPF_TE_RM_LINK_SRLG_INFO_SIZING_ID];

    OSPF_TE_TRC (OSPF_TE_RESOURCE_TRC,
                 "Successfully created memory pool for OSPF-TE operation\n");

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCreateMemPool\n");

    return OSPF_TE_SUCCESS;
}

/***************************************************************************/
/*  Function    :  OspfTeShutDown                                          */
/*                                                                         */
/*  Description :  This module deletes the Memory Pools created for the    */
/*                 Protocol operation. It also initialises the values to   */
/*                 defaults.                                               */
/*                                                                         */
/*  Input       :  None                                                    */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  None                                                    */
/***************************************************************************/
PUBLIC VOID
OspfTeShutDown (VOID)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeShutDown\n");

    OspfteSizingMemDeleteMemPools ();
    /* Free Memory for LSA nodes */
    OSPF_TE_MEMSET (&(gOsTeContext.teMemPool), OSPF_TE_ZERO,
                    sizeof (tOsTeMemPoolId));

    OSPF_TE_TRC (OSPF_TE_RESOURCE_TRC,
                 "Successfully deleted memory pools allocated for "
                 "OSPF-TE operation\n");

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeShutDown\n");

    return;
}

/***************************************************************************/
/*  Function    :  OspfTeLock                                              */
/*                                                                         */
/*  Description :  This function protects the OSPF data structures         */
/*                                                                         */
/*  Input       :  None                                                    */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  None                                                    */
/***************************************************************************/
PUBLIC INT4
OspfTeLock (VOID)
{
    if (OsixSemTake (gOspfTeSemId) != OSIX_SUCCESS)
    {
        OSPF_TE_TRC1 (OSPF_TE_CRITICAL_TRC,
                      "TakeSem failure for %s \n", OSPF_TE_MUT_EXCL_SEM_NAME);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************/
/*  Function    :  OspfTeUnLock                                            */
/*                                                                         */
/*  Description :  This function protects the OSPF data structures         */
/*                                                                         */
/*  Input       :  None                                                    */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  None                                                    */
/***************************************************************************/
PUBLIC INT4
OspfTeUnLock (VOID)
{
    if (OsixSemGive (gOspfTeSemId) != OSIX_SUCCESS)
    {
        OSPF_TE_TRC1 (OSPF_TE_CRITICAL_TRC,
                      "GiveSem failure for %s \n", OSPF_TE_MUT_EXCL_SEM_NAME);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************/
/*  Function    :  RegisterOSPFTEMibs                                      */
/*                                                                         */
/*  Description :  This function registers OSPFTE mibs                     */
/*                                                                         */
/*  Input       :  None                                                    */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  None                                                    */
/***************************************************************************/
PUBLIC VOID
RegisterOSPFTEMibs (VOID)
{
    RegisterFSOTE ();
#ifdef OSPFTEAPPSIM_WANTED
    RegisterFSOTEA ();
#endif
#ifdef OSPFTERMSIM_WANTED
    RegisterFSOTER ();
#endif
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostemain.c                     */
/*-----------------------------------------------------------------------*/
