/******************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostelink.c,v 1.10 2013/07/06 13:40:26 siva Exp $
 *
 * Description:  This file contains the routines to Manage TE-Links in OSPF-TE
 * ***************************************************************************/

#include "osteinc.h"

PRIVATE tOsTeArea  *OspfTeAssocControlChannel (tOsTeInterface * pTeIfNode);
PRIVATE INT4        OspfTeCheckForTransitOrPTOPLinks (tOsTeArea * pTeArea);
/*****************************************************************************/
/*  Function    :  OspfTeCreateTeLink                                        */
/*                                                                           */
/*  Description :  This function processes the TE-Link Message information   */
/*                 received from RM or TLM and Generates Link TLV and Type 9 */
/*                 Lsas, if the created TE link is active.                   */
/*                                                                           */
/*  Input       :  pTeLinkMsg - Pointer to TE Link message                   */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeCreateTeLink (tOsTeLinkMsg * pOsTeLinkMsg)
{
    tOsTeInterface     *pTeIfNode = NULL;
    tOsTeOsIntInfo     *pOsTeOsIntInfo = NULL;
    UINT1               u1Found = OSPF_TE_FALSE;
    UINT4               u4RemoteId;
    tOsTeArea          *pTeArea = NULL;
    UINT4               u4AddrlessIf;
    tIPADDR             linkIpAddr;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCreateTeLink\n");

    OSPF_TE_CRU_BMC_DWTOPDU (linkIpAddr, pOsTeLinkMsg->u4LocalIpAddr);

    if (pOsTeLinkMsg->u4LocalIpAddr != OSPF_TE_ZERO)
    {
        u4AddrlessIf = OSPF_TE_ZERO;
    }
    else
    {
        u4AddrlessIf = pOsTeLinkMsg->u4IfIndex;
#ifdef CFA_WANTED
#ifdef TLM_WANTED
        if (pOsTeLinkMsg->u1InfoType == OSPF_TE_RM_DATACONTROLLINK_INFO &&
            pOsTeLinkMsg->u4ModuleId == OSPF_TE_TLM_MSG)
        {
            if ((CfaApiGetL3IfFromTeLinkIf (u4AddrlessIf, &u4AddrlessIf, TRUE))
                != CFA_SUCCESS)
            {
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                             "Failed to Get L3Interface for Given TE-Link\n");
                return;
            }
        }
#endif
#endif
    }

    /* Create the TE link and add it to Hash table and sorted 
     * TE interfaces list */
    pTeIfNode = OspfTeIfCreate (linkIpAddr, u4AddrlessIf);

    if (pTeIfNode == NULL)
    {
        return;
    }
    pTeIfNode->u4IfIndex = pOsTeLinkMsg->u4IfIndex;

    /* Store the information passed in RM/TLM message in created TE link */
    OspfTeIfLinkInstall (pTeIfNode, pOsTeLinkMsg);

    if (pOsTeLinkMsg->u1InfoType == OSPF_TE_RM_AREAID_INFO)
    {
        pTeIfNode->u1ChannelType = OSPF_TE_AREA_CHANNEL;

        pTeIfNode->u1LinkType = OSPF_TE_PTOP;
        OSPF_TE_CRU_BMC_DWTOPDU (pTeIfNode->remoteIpAddr,
                                 pOsTeLinkMsg->u4RemoteIpAddr);

        OSPF_TE_CRU_BMC_DWTOPDU (pTeIfNode->linkId,
                                 pOsTeLinkMsg->u4RemoteRtrId);

        if (pTeIfNode->u1ModuleId == OSPF_TE_TLM_MSG)
        {
            pTeIfNode->pTeArea =
                OspfTeMapAreaForFA (pOsTeLinkMsg->u4CfaPortIndex);
        }
        else
        {
            pTeIfNode->pTeArea = OspfTeUtilFindArea (pOsTeLinkMsg->u4AreaId);
        }

        if ((pTeIfNode->pTeArea != NULL) &&
            (OspfTeUtilIpAddrComp (pTeIfNode->linkId, gOsTeNullIpAddr)
             != OSPF_TE_EQUAL))
        {
            pTeIfNode->u4AreaId = pTeIfNode->pTeArea->u4AreaId;
            if (OspfTeCheckForTransitOrPTOPLinks (pTeIfNode->pTeArea)
                == OSPF_TE_TRUE)
            {
                pTeIfNode->u1LinkStatus = OSPF_TE_ACTIVE;
            }
        }
        else
        {
            pTeIfNode->u4AreaId = pOsTeLinkMsg->u4AreaId;
        }

        if (pTeIfNode->u1MetricType != OSPF_TE_TE_METRIC)
        {
            pTeIfNode->u4TeMetric = OSPF_TE_DEFAULT_METRIC;
        }
    }
    else if (pOsTeLinkMsg->u1InfoType == OSPF_TE_RM_DATALINK_INFO)
    {
        if (pTeIfNode->u1ModuleId == OSPF_TE_TLM_MSG)
        {
            pTeIfNode->u1LinkType = (UINT1) pOsTeLinkMsg->u4TeLinkIfType;
        }
        else
        {
            pTeIfNode->u1LinkType = OSPF_TE_PTOP;
        }
        pTeIfNode->u1ChannelType = OSPF_TE_DATA_CHANNEL;

        /* If the link information is of data link information,
         * then, neighbor IP address and neighbor router ID are
         * expected from Resource Manager. */
        OSPF_TE_CRU_BMC_DWTOPDU (pTeIfNode->remoteIpAddr,
                                 pOsTeLinkMsg->u4RemoteIpAddr);

        OSPF_TE_CRU_BMC_DWTOPDU (pTeIfNode->linkId,
                                 pOsTeLinkMsg->u4RemoteRtrId);

        pTeArea = OspfTeAssocControlChannel (pTeIfNode);

        if (pTeArea != NULL)
        {
            pTeIfNode->pTeArea = pTeArea;
            pTeIfNode->u1LinkStatus = OSPF_TE_ACTIVE;
        }
        /* If the TE metric value is zero then, fill the
         * metric value with default value */
        if (pTeIfNode->u4TeMetric == OSPF_TE_ZERO)
        {
            pTeIfNode->u4TeMetric = OSPF_TE_DEFAULT_METRIC;
        }
    }
    else if (pOsTeLinkMsg->u1InfoType == OSPF_TE_RM_DATACONTROLLINK_INFO)
    {
        pTeIfNode->u1ChannelType = OSPF_TE_DATA_CONTROL_CHANNEL;

        TMO_SLL_Scan (&(gOsTeContext.ospfInfoIfLst), pOsTeOsIntInfo,
                      tOsTeOsIntInfo *)
        {
            if ((OspfTeUtilIpAddrComp (pOsTeOsIntInfo->ifIpAddr,
                                       pTeIfNode->localIpAddr) == OSPF_TE_EQUAL)
                && (pOsTeOsIntInfo->u4AddrlessIf == pTeIfNode->u4AddrlessIf))
            {
                pTeIfNode->u1LinkType = pOsTeOsIntInfo->u1LinkType;

                IP_ADDR_COPY (pTeIfNode->linkId, pOsTeOsIntInfo->linkId);

                pTeIfNode->pTeArea = pOsTeOsIntInfo->pTeArea;

                /* For multiaccess interfaces, remote IP address is 
                 * 0.0.0.0 */
                if (pTeIfNode->u1LinkType == OSPF_TE_PTOP)
                {
                    /* For numbered Point-to-Point interfaces fill the
                     * Neighbor IP address */
                    if (OspfTeUtilIpAddrComp (pTeIfNode->localIpAddr,
                                              gOsTeNullIpAddr) != OSPF_TE_EQUAL)
                    {
                        IP_ADDR_COPY (pTeIfNode->remoteIpAddr,
                                      pOsTeOsIntInfo->nbrIpAddr);
                    }
                    else
                    {
                        if (pOsTeOsIntInfo->pType9Lsa != NULL)
                        {
                            pOsTeOsIntInfo->pType9Lsa->pOsTeInt = pTeIfNode;
                            OspfTeLsuAddToSortLsaLst (pOsTeOsIntInfo->
                                                      pType9Lsa);

                            u4RemoteId =
                                OSPF_TE_CRU_BMC_DWFROMPDU (pOsTeOsIntInfo->
                                                           pType9Lsa->pLsa +
                                                           OSPF_TE_LSA_HEADER_LENGTH
                                                           +
                                                           OSPF_TE_LSA_ID_LEN);

                            pOsTeOsIntInfo->pType9Lsa = NULL;
                            pTeIfNode->u4RemoteIdentifier = u4RemoteId;
                        }
                    }
                }

                if (pOsTeOsIntInfo->u1InfoType == OSPF_TE_ROUTER_LSA_INFO)
                {
                    /* Fill the metric with the control channel information */
                    if (pTeIfNode->u4TeMetric == OSPF_TE_ZERO)
                    {
                        pTeIfNode->u4TeMetric = pOsTeOsIntInfo->u4Metric;
                    }
                    pTeIfNode->u1LinkStatus = OSPF_TE_ACTIVE;
                    u1Found = OSPF_TE_TRUE;
                }
                break;
            }
        }
        if (u1Found == OSPF_TE_TRUE)
        {
            /* Deleting the OSPF information interface from
             * OSPF information interface list */
            TMO_SLL_Delete (&(gOsTeContext.ospfInfoIfLst),
                            &(pOsTeOsIntInfo->NextOsIntInfoNode));
            /* Free the Memory */
            OSPF_TE_OSPF_INFO_IF_FREE (pOsTeOsIntInfo);
        }
    }

    /* If the TE interface state is ACTIVE after creating it
     * then generate a LINK TLV for the TE interface */
    if (pTeIfNode->u1LinkStatus == OSPF_TE_ACTIVE)
    {
        OspfTeGenerateLinkTlv (pTeIfNode, OSPF_TE_NEW_TLV);

        /* For unnumbered interface, if the link local identifier is 
         * not equal to ZERO, then generate Type 9 LSA */
        /* For Data channel There is no adjacency, so generation of Type 9
         * LSA is not useful for Data channel. In this case Remote identifier 
         * information is expected TLM */
        if ((OspfTeUtilIpAddrComp (pTeIfNode->localIpAddr,
                                   gOsTeNullIpAddr) == OSPF_TE_EQUAL) &&
            (pTeIfNode->u4LocalIdentifier != OSPF_TE_ZERO) &&
            (pTeIfNode->u1ChannelType == OSPF_TE_RM_DATACONTROLLINK_INFO))
        {
            OspfTeGenerateType9Lsa (pTeIfNode, OSPF_TE_NEW_TLV);
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCreateTeLink\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeUpdateTeLink                                        */
/*                                                                           */
/*  Description :  This function updates the TE link infomration with the    */
/*                 information received from RM or TLM                       */
/*                                                                           */
/*  Input       :  pOsTeLinkMsg - Pointer to TE Link message                 */
/*                 pTeIfNode    - Pointer to TE interface                    */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeUpdateTeLink (tOsTeLinkMsg * pOsTeLinkMsg, tOsTeInterface * pTeIfNode)
{
    UINT4               u4PrevLocalId;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    tOsTeIfDescNode    *pIfDescNode = NULL;
    tIPADDR             ipAddr;
    tIPADDR             localIpAddr;
    UINT4               u4HashIndex;
    UINT4               u4AddrlessIf;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeUpdateTeLink\n");

    OSPF_TE_CRU_BMC_DWTOPDU (localIpAddr, pOsTeLinkMsg->u4LocalIpAddr);

    if (OspfTeUtilIpAddrComp (pTeIfNode->localIpAddr,
                              localIpAddr) != OSPF_TE_EQUAL)
    {
        u4HashIndex = OspfTeIfHashFunc (&(pTeIfNode->localIpAddr),
                                        pTeIfNode->u4AddrlessIf);

        /* Deleting Node from Global Hash Table */
        TMO_HASH_Delete_Node (gOsTeContext.pTeIfHashTbl,
                              &(pTeIfNode->NextLinkNode), u4HashIndex);

        /* Deleting from Sorted TE interface list */
        TMO_SLL_Delete (&(gOsTeContext.sortTeIfLst),
                        (tTMO_SLL_NODE *) & (pTeIfNode->NextSortLinkNode));

        /* If the interface IP address is not 0, then Addressless 
         * interface index is 0, otherwise equals to interface index */
        if (pOsTeLinkMsg->u4LocalIpAddr != OSPF_TE_ZERO)
        {
            u4AddrlessIf = OSPF_TE_ZERO;
        }
        else
        {
            u4AddrlessIf = pOsTeLinkMsg->u4IfIndex;
#ifdef CFA_WANTED
#ifdef TLM_WANTED
            if (pOsTeLinkMsg->u4ModuleId == OSPF_TE_TLM_MSG)
            {
                if ((CfaApiGetL3IfFromTeLinkIf
                     (u4AddrlessIf, &u4AddrlessIf, TRUE)) != CFA_SUCCESS)
                {
                    OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                                 "Failed to Get L3Interface for Given TE-Link\n");
                    return;
                }
            }
#endif
#endif
        }

        /* Update the local IP address field */
        IP_ADDR_COPY (pTeIfNode->localIpAddr, localIpAddr);
        /* Update the addressless interface index field */
        pTeIfNode->u4AddrlessIf = u4AddrlessIf;

        u4HashIndex = OspfTeIfHashFunc (&(pTeIfNode->localIpAddr),
                                        pTeIfNode->u4AddrlessIf);

        /* Add to Hash Table */
        TMO_HASH_Add_Node (gOsTeContext.pTeIfHashTbl,
                           &(pTeIfNode->NextLinkNode), u4HashIndex, NULL);

        /* Add to the sorted TE interface list */
        OspfTeIfAddToSortIfLst (pTeIfNode);
    }

    u4PrevLocalId = pTeIfNode->u4LocalIdentifier;

    OSPF_TE_DYNM_SLL_SCAN (&(pTeIfNode->ifDescrList), pLstNode, pPrevNode,
                           tTMO_SLL_NODE *)
    {
        pIfDescNode = (tOsTeIfDescNode *) pLstNode;
        /* Delete from interface descriptor list */
        TMO_SLL_Delete (&(pTeIfNode->ifDescrList),
                        &(pIfDescNode->NextIfDescrNode));
        /* Free the interface descriptor memory */
        OSPF_TE_IF_DESC_FREE (pLstNode);
    }

    OspfTeIfLinkInstall (pTeIfNode, pOsTeLinkMsg);

    if (pOsTeLinkMsg->u1InfoType == OSPF_TE_RM_AREAID_INFO)
    {
        OSPF_TE_CRU_BMC_DWTOPDU (pTeIfNode->remoteIpAddr,
                                 pOsTeLinkMsg->u4RemoteIpAddr);
        IP_ADDR_COPY (ipAddr, pTeIfNode->linkId);
        OSPF_TE_CRU_BMC_DWTOPDU (pTeIfNode->linkId,
                                 pOsTeLinkMsg->u4RemoteRtrId);
        /* If the TE metric value is zero then, fill the 
         * metric value with default value */
        if (pTeIfNode->u4TeMetric == OSPF_TE_ZERO)
        {
            pTeIfNode->u4TeMetric = OSPF_TE_DEFAULT_METRIC;
        }
        if (pTeIfNode->pTeArea != NULL)
        {
            if ((OspfTeUtilIpAddrComp (ipAddr, gOsTeNullIpAddr)
                 == OSPF_TE_EQUAL) && (pOsTeLinkMsg->u4RemoteRtrId != 0))
            {
                pTeIfNode->u1LinkStatus = OSPF_TE_ACTIVE;
                OspfTeGenerateLinkTlv (pTeIfNode, OSPF_TE_NEW_LSA);
            }
            else if ((OspfTeUtilIpAddrComp (ipAddr, gOsTeNullIpAddr)
                      != OSPF_TE_EQUAL) && (pOsTeLinkMsg->u4RemoteRtrId == 0))
            {
                pTeIfNode->u1LinkStatus = OSPF_TE_INVALID;
                OspfTeGenerateLinkTlv (pTeIfNode, OSPF_TE_FLUSHED_TLV);
            }
        }
        if ((pTeIfNode->pTeArea != NULL) &&
            (pTeIfNode->pTeArea->u4AreaId != pOsTeLinkMsg->u4AreaId))
        {
            if (pTeIfNode->u1LinkStatus == OSPF_TE_ACTIVE)
            {
                OspfTeGenerateLinkTlv (pTeIfNode, OSPF_TE_FLUSHED_TLV);
                pTeIfNode->u1LinkStatus = OSPF_TE_INVALID;
            }
            pTeIfNode->pTeArea = OspfTeUtilFindArea (pOsTeLinkMsg->u4AreaId);

            if (pTeIfNode->pTeArea != NULL)
            {
                pTeIfNode->u1LinkStatus = OSPF_TE_ACTIVE;
                OspfTeGenerateLinkTlv (pTeIfNode, OSPF_TE_NEW_TLV);
            }
            else
            {
                OSPF_TE_TRC1 (OSPF_TE_CRITICAL_TRC,
                              "Area with Area ID %x is not found\n",
                              pOsTeLinkMsg->u4AreaId);
            }
        }
        else if (pTeIfNode->pTeArea == NULL)
        {
            if (pTeIfNode->u1ModuleId == OSPF_TE_TLM_MSG)
            {
                pTeIfNode->pTeArea =
                    OspfTeMapAreaForFA (pOsTeLinkMsg->u4IfIndex);
            }
            else
            {
                pTeIfNode->pTeArea =
                    OspfTeUtilFindArea (pOsTeLinkMsg->u4AreaId);
            }

            if (pTeIfNode->pTeArea != NULL)
            {
                pTeIfNode->u4AreaId = pTeIfNode->pTeArea->u4AreaId;

                if ((OspfTeUtilIpAddrComp (ipAddr, gOsTeNullIpAddr)
                     != OSPF_TE_EQUAL) && (pOsTeLinkMsg->u4RemoteRtrId != 0))
                {
                    pTeIfNode->u1LinkStatus = OSPF_TE_ACTIVE;
                    OspfTeGenerateLinkTlv (pTeIfNode, OSPF_TE_NEW_LSA);
                }
            }
        }
        else if (pTeIfNode->u1LinkStatus == OSPF_TE_ACTIVE)
        {
            OspfTeGenerateLinkTlv (pTeIfNode, OSPF_TE_NEW_INSTANCE_TLV);
        }
    }
    else if (pOsTeLinkMsg->u1InfoType == OSPF_TE_RM_DATALINK_INFO)
    {
        if (pTeIfNode->u1ModuleId == OSPF_TE_TLM_MSG)
        {
            pTeIfNode->u1LinkType = (UINT1) pOsTeLinkMsg->u4TeLinkIfType;
        }
        else
        {
            pTeIfNode->u1LinkType = OSPF_TE_PTOP;
        }

        /* If the link information is of data link information,
         * then, neighbor IP address and neighbor router ID are 
         * expected from Resource Manager. */
        OSPF_TE_CRU_BMC_DWTOPDU (pTeIfNode->remoteIpAddr,
                                 pOsTeLinkMsg->u4RemoteIpAddr);

        OSPF_TE_CRU_BMC_DWTOPDU (pTeIfNode->linkId,
                                 pOsTeLinkMsg->u4RemoteRtrId);

        /* If the TE metric value is zero then, fill the 
         * metric value with default value */
        if (pTeIfNode->u4TeMetric == OSPF_TE_ZERO)
        {
            pTeIfNode->u4TeMetric = OSPF_TE_DEFAULT_METRIC;
        }

        if (pTeIfNode->u1LinkStatus == OSPF_TE_ACTIVE)
        {
            OspfTeGenerateLinkTlv (pTeIfNode, OSPF_TE_NEW_INSTANCE_TLV);
        }
    }
    else if (pOsTeLinkMsg->u1InfoType == OSPF_TE_RM_DATACONTROLLINK_INFO)
    {
        /* If the TE metric value is zero then, fill the 
         * metric value with default value */
        if (pTeIfNode->u4TeMetric == OSPF_TE_ZERO)
        {
            pTeIfNode->u4TeMetric = OSPF_TE_DEFAULT_METRIC;
        }
        /* If the TE interace is in active state then regenerate the 
         * link TLV to reflect the changes in TE link parameters */
        if (pTeIfNode->u1LinkStatus == OSPF_TE_ACTIVE)
        {
            OspfTeGenerateLinkTlv (pTeIfNode, OSPF_TE_NEW_INSTANCE_TLV);

            if ((OspfTeUtilIpAddrComp (pTeIfNode->localIpAddr,
                                       gOsTeNullIpAddr) == OSPF_TE_EQUAL)
                && (pTeIfNode->u1ChannelType ==
                    OSPF_TE_RM_DATACONTROLLINK_INFO))
            {
                if ((u4PrevLocalId == OSPF_TE_ZERO)
                    && (pTeIfNode->u4LocalIdentifier != OSPF_TE_ZERO))
                {
                    OspfTeGenerateType9Lsa (pTeIfNode, OSPF_TE_NEW_TLV);
                }
                else if ((u4PrevLocalId != OSPF_TE_ZERO)
                         && (pTeIfNode->u4LocalIdentifier == OSPF_TE_ZERO))
                {
                    OspfTeGenerateType9Lsa (pTeIfNode, OSPF_TE_FLUSHED_TLV);
                }
                else if ((u4PrevLocalId != pTeIfNode->u4LocalIdentifier))
                {
                    OspfTeGenerateType9Lsa (pTeIfNode,
                                            OSPF_TE_NEW_INSTANCE_TLV);
                }
            }
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeUpdateTeLink\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeFlushTeLink                                         */
/*                                                                           */
/*  Description :  This function deletes the Link information given by the   */
/*                 TLM or RM                                                 */
/*                                                                           */
/*  Input       :  pTeIfNode - Pointer to TE node                            */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeFlushTeLink (tOsTeInterface * pTeIfNode)
{
    tOsTeLsaNode       *pType9Lsa = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeFlushTeLink\n");

    if (pTeIfNode->u1ChannelType != OSPF_TE_DATA_CONTROL_CHANNEL)
    {
        if (pTeIfNode->u1LinkStatus == OSPF_TE_ACTIVE)
        {
            OspfTeGenerateLinkTlv (pTeIfNode, OSPF_TE_FLUSHED_TLV);
        }
    }
    else
    {
        if (pTeIfNode->u1LinkStatus == OSPF_TE_ACTIVE)
        {
            pType9Lsa = OspfTeFlushOpaqueLsa (pTeIfNode);
        }
        if (pTeIfNode->u1LinkType != OSPF_TE_INVALID)
        {
            OspfTeCreateOspfInfoInterface (pTeIfNode, pType9Lsa);
        }

    }

    OspfTeIfDeleteTeLink (pTeIfNode);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeFlushTeLink\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessDeReg                                        */
/*                                                                           */
/*  Description :  This function processes the DeReg information received    */
/*                 from RM or TLM                                            */
/*                                                                           */
/*  Input       :  None                                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessDeReg (VOID)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    tOsTeArea          *pTeArea = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessDeReg\n");

    /* Scan all TE interfaces list */
    OSPF_TE_DYNM_SLL_SCAN (&(gOsTeContext.sortTeIfLst), pLstNode, pPrevNode,
                           tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR (tOsTeInterface,
                                               NextSortLinkNode, pLstNode);

        /* Flushing the TLVs */
        OspfTeFlushTeLink (pOsTeInterface);
    }

    OSPF_TE_DYNM_SLL_SCAN (&(gOsTeContext.areasLst), pLstNode, pPrevNode,
                           tTMO_SLL_NODE *)
    {
        pTeArea = OSPF_TE_GET_BASE_PTR (tOsTeArea, NextAreaNode, pLstNode);

        /* Delete the Type 9 LSAs */
        OspfTeLsuDeleteType9Lsas (pTeArea);

        /* Delete the Type 10 LSAs */
        OspfTeLsuDeleteType10Lsas (pTeArea, OSPF_TE_TRUE);
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessDeReg\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeIfLinkInstall                                       */
/*                                                                           */
/*  Description :  This function copies the RM/TLM message information into  */
/*                 TE link.                                                  */
/*                                                                           */
/*  Input       :  pOsTeIf - Pointer to TE interface                         */
/*                 pOsTeLinkMsg - Pointer to RM/TLM message                  */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  VOID                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeIfLinkInstall (tOsTeInterface * pOsTeIf, tOsTeLinkMsg * pOsTeLinkMsg)
{
    UINT4               u4Count;
    UINT1               u1Priority;
    UINT1              *pCurrent = NULL;
    tOsTeIfDescNode    *pOsTeIfDescNode = NULL;

    if ((pOsTeLinkMsg->u4TeMetric != OSPF_TE_ZERO) ||
        (pOsTeIf->u1MetricType == OSPF_TE_TE_METRIC))
    {
        pOsTeIf->u4TeMetric = pOsTeLinkMsg->u4TeMetric;
        pOsTeIf->u1MetricType = OSPF_TE_TE_METRIC;
    }
    pOsTeIf->u4RsrcClassColor = pOsTeLinkMsg->u4RsrcClassColor;

    /* If the interface is unnumbered only, Identifier information 
     * is set */
    if (OspfTeUtilIpAddrComp (pOsTeIf->localIpAddr, gOsTeNullIpAddr)
        == OSPF_TE_EQUAL)
    {
        pOsTeIf->u4LocalIdentifier = pOsTeLinkMsg->u4LocalIdentifier;
        if (pOsTeLinkMsg->u1InfoType != OSPF_TE_RM_DATACONTROLLINK_INFO)
        {
            pOsTeIf->u4RemoteIdentifier = pOsTeLinkMsg->u4RemoteIdentifier;
        }
    }

    pOsTeIf->maxBw = pOsTeLinkMsg->maxBw;
    pOsTeIf->maxResBw = pOsTeLinkMsg->maxResBw;

    /* Fill the band width information */
    for (u1Priority = OSPF_TE_ZERO; u1Priority < OSPF_TE_MAX_PRIORITY_LVL;
         u1Priority++)
    {
        pOsTeIf->aUnResBw[u1Priority] = pOsTeLinkMsg->aUnResBw[u1Priority];
    }

    /* Filling Link descriptor information */
    pCurrent = pOsTeLinkMsg->pIfDescriptors;

    for (u4Count = OSPF_TE_ZERO;
         ((u4Count < pOsTeLinkMsg->u4IfDescCnt)
          && (u4Count < OSPF_TE_MAX_IF_DESC)); u4Count++)
    {
        if (OSPF_TE_IF_DESC_ALLOC (pOsTeIfDescNode, tOsTeIfDescNode) == NULL)
        {
            OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Buffer Allocation failure "
                         "for If Descriptor\n");
            break;
        }

        MEMCPY (&(pOsTeIfDescNode->ifDesc), pCurrent, sizeof (tOsTeIfDesc));
        /* Add this infomration to the interface descriptor list */
        TMO_SLL_Add (&(pOsTeIf->ifDescrList),
                     &(pOsTeIfDescNode->NextIfDescrNode));

        pCurrent = pCurrent + sizeof (tOsTeIfDesc);
        if ((pOsTeIfDescNode->ifDesc.u1Indication != OSPF_TE_STANDARD_SDH) &&
            (pOsTeIfDescNode->ifDesc.u1Indication != OSPF_TE_ARBITRARY))
        {
            pOsTeIfDescNode->ifDesc.u1Indication = OSPF_TE_STANDARD_SDH;
        }
    }

    if (pOsTeLinkMsg->u4IfDescCnt != OSPF_TE_ZERO)
    {
        /* Freeing the memory allocated for descriptors */
        OSPF_TE_RM_LINK_INFO_FREE (pOsTeLinkMsg->pIfDescriptors);
    }

    /* Filling SRLG information */
    pCurrent = pOsTeLinkMsg->pSrlgs;

    for (u4Count = OSPF_TE_ZERO; u4Count < pOsTeLinkMsg->u4IfSrlgCnt; u4Count++)
    {
        if (u4Count == OSPF_TE_MAX_SRLG_NO)
        {
            break;
        }
        pOsTeIf->osTeSrlg.aSrlgNumber[u4Count] =
            (*(UINT4 *) (VOID *) (pCurrent));
        pCurrent += sizeof (UINT4);
    }

    /* Filling no of SRLG numbers */
    pOsTeIf->osTeSrlg.u4NoOfSrlg = u4Count;

    if (pOsTeLinkMsg->u4IfSrlgCnt != OSPF_TE_ZERO)
    {
        /* Freeing the memory allocated for SRLG numbers */
        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pOsTeLinkMsg->pSrlgs);
    }

    pOsTeIf->u1ProtectionType = pOsTeLinkMsg->u1ProtectionType;

    if (pOsTeLinkMsg->u4ModuleId == OSPF_TE_TLM_MSG)
    {
        pOsTeIf->u1ModuleId = OSPF_TE_TLM_MSG;
    }
    if (pOsTeLinkMsg->u4ModuleId == OSPF_TE_RMGR_MSG)
    {
        pOsTeIf->u1ModuleId = OSPF_TE_RMGR_MSG;
    }

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeAssocControlChannel                                 */
/*                                                                           */
/*  Description :  This function Associates the data channel with the control*/
/*                 Channel and returns the area pointer of the control chn.  */
/*                                                                           */
/*  Input       :  pTeIfNode - Pointer to TE node                            */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the area                                       */
/*****************************************************************************/
PRIVATE tOsTeArea  *
OspfTeAssocControlChannel (tOsTeInterface * pTeIfNode)
{
    tIPADDR             ipAddr;
    tOsTeLsaNode       *pNetworkLsa = NULL;
    tOsTeOsIntInfo     *pOsTeOsIntInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1              *pCurrPtr = NULL;
    tOsTeInterface     *pTeContIfNode = NULL;

    /* Find from The OSPF TE information interface list */
    TMO_SLL_Scan (&(gOsTeContext.ospfInfoIfLst), pOsTeOsIntInfo,
                  tOsTeOsIntInfo *)
    {
        if (pOsTeOsIntInfo->u1LinkType == OSPF_TE_PTOP)
        {
            if (OspfTeUtilIpAddrComp (pOsTeOsIntInfo->linkId,
                                      pTeIfNode->linkId) == OSPF_TE_EQUAL)
            {
                return pOsTeOsIntInfo->pTeArea;
            }
        }
        else
        {
            pNetworkLsa = OspfTeUtilFindNetworkLsa (pOsTeOsIntInfo->linkId,
                                                    pOsTeOsIntInfo->pTeArea);

            if (pNetworkLsa != NULL)
            {
                /* Scan all the Neighbors in Network LSA */
                pCurrPtr = pNetworkLsa->pLsa +
                    OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN;
                while (pCurrPtr < (pNetworkLsa->pLsa + pNetworkLsa->u2LsaLen))
                {
                    IP_ADDR_COPY (ipAddr, pCurrPtr);
                    if (OspfTeUtilIpAddrComp (pTeIfNode->linkId,
                                              ipAddr) == OSPF_TE_EQUAL)
                    {
                        return pOsTeOsIntInfo->pTeArea;
                    }
                    pCurrPtr = pCurrPtr + MAX_IP_ADDR_LEN;
                }
            }
        }
    }

    /* Scan the TE interfaces list to find whether any 
     * Data and control channel is present */
    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pTeContIfNode = OSPF_TE_GET_BASE_PTR (tOsTeInterface,
                                              NextSortLinkNode, pLstNode);

        if (pTeContIfNode->u1ChannelType != OSPF_TE_DATA_CONTROL_CHANNEL)
        {
            continue;
        }

        if (pTeContIfNode->u1LinkType == OSPF_TE_PTOP)
        {
            if (OspfTeUtilIpAddrComp (pTeContIfNode->linkId,
                                      pTeIfNode->linkId) == OSPF_TE_EQUAL)
            {
                return pTeContIfNode->pTeArea;
            }
        }
        else
        {
            if (pTeContIfNode->pTeArea == NULL)
            {
                continue;
            }

            pNetworkLsa = OspfTeUtilFindNetworkLsa (pTeContIfNode->linkId,
                                                    pTeContIfNode->pTeArea);

            if (pNetworkLsa != NULL)
            {
                /* Scan all the Neighbors in Network LSA */
                pCurrPtr = pNetworkLsa->pLsa +
                    OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN;
                while (pCurrPtr < (pNetworkLsa->pLsa + pNetworkLsa->u2LsaLen))
                {
                    IP_ADDR_COPY (ipAddr, pCurrPtr);
                    if (OspfTeUtilIpAddrComp (pTeIfNode->linkId,
                                              ipAddr) == OSPF_TE_EQUAL)
                    {
                        return pTeContIfNode->pTeArea;
                    }
                    pCurrPtr = pCurrPtr + MAX_IP_ADDR_LEN;
                }
            }
        }
    }
    return NULL;
}

/*****************************************************************************/
/*  Function    :  OspfTeCheckForTransitOrPTOPLinks                          */
/*                                                                           */
/*  Description :  This function Associates the data channel with the control*/
/*                 Channel and returns the area pointer of the control chn.  */
/*                                                                           */
/*  Input       :  pTeArea - OSPF-TE Area                                    */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_TRUE when the links are Point to Point or Multi   */
/*                 Access.                                                   */
/*                 OSPF_TE_FALSE when no self originated Router LSA is       */
/*                 present or only Stub or virtual link presents             */
/*****************************************************************************/
PRIVATE INT4
OspfTeCheckForTransitOrPTOPLinks (tOsTeArea * pTeArea)
{
    UINT1              *pCurrPtr = NULL;
    tLinkNode           linkNode;
    if ((pTeArea->pRtrLsa != NULL) && (pTeArea->pRtrLsa->pLsa != NULL))
    {
        pCurrPtr = pTeArea->pRtrLsa->pLsa +
            OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN;
        while ((pCurrPtr =
                OspfTeUtilGetLinksFromLsa (pTeArea->pRtrLsa->pLsa,
                                           pTeArea->pRtrLsa->u2LsaLen,
                                           pCurrPtr, &linkNode)) != NULL)
        {
            if ((linkNode.u1LinkType == STUB_LINK) ||
                (linkNode.u1LinkType == VIRTUAL_LINK))
            {
                continue;
            }
            return OSPF_TE_TRUE;
        }
    }
    return OSPF_TE_FALSE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostelink.c                     */
/*-----------------------------------------------------------------------*/
