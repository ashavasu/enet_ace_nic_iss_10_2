/* $Id: ospftesz.c,v 1.4 2013/11/29 11:04:15 siva Exp $*/

#define _OSPFTESZ_C
#include "osteinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
OspfteSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < OSPFTE_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsOSPFTESizingParams[i4SizingId].
                                     u4StructSize,
                                     FsOSPFTESizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(OSPFTEMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            OspfteSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
OspfteSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsOSPFTESizingParams);
    IssSzRegisterModulePoolId (pu1ModName, OSPFTEMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
OspfteSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < OSPFTE_MAX_SIZING_ID; i4SizingId++)
    {
        if (OSPFTEMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (OSPFTEMemPoolIds[i4SizingId]);
            OSPFTEMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
