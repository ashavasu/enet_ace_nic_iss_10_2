/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: osteutil.c,v 1.7 2013/07/06 13:40:26 siva Exp $
 *
 * Description:  This file contains the utility routines  
 * *********************************************************************/

#include "osteinc.h"

/*****************************************************************************/
/*  Function    :  OspfTeFindTeIfDescr                                       */
/*                                                                           */
/*  Description :  This function finds the TE interface descriptor           */
/*                                                                           */
/*  Input       :  u4IpAddr      - Interface IP Address                      */
/*              :  i4AddrlessIf  -  Addressless interface index              */
/*              :  u4DescrId     - Descriptor Id                             */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to If Descriptor if it is present                 */
/*                 otherwise NULL                                            */
/*****************************************************************************/
PUBLIC tOsTeIfDesc *
OspfTeFindTeIfDescr (UINT4 u4IpAddr, INT4 i4AddrlessIf, UINT4 u4DescrId)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pTeIfNode = NULL;
    tOsTeIfDescNode    *pOsTeIfDescNode = NULL;
    tOsTeIfDesc        *pOsTeIfDesc = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeFindTeIfDescr\n");

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4IpAddr);

    TMO_HASH_Scan_Bucket (gOsTeContext.pTeIfHashTbl,
                          OspfTeIfHashFunc (&(ifIpAddr),
                                            (UINT4) i4AddrlessIf),
                          pTeIfNode, tOsTeInterface *)
    {
        if ((OspfTeUtilIpAddrComp (ifIpAddr, pTeIfNode->localIpAddr)
             == OSPF_TE_EQUAL) && ((UINT4) i4AddrlessIf ==
                                   pTeIfNode->u4AddrlessIf))
        {
            TMO_SLL_Scan (&pTeIfNode->ifDescrList,
                          pOsTeIfDescNode, tOsTeIfDescNode *)
            {
                if (pOsTeIfDescNode->ifDesc.u4DescrId == u4DescrId)
                {
                    pOsTeIfDesc = &(pOsTeIfDescNode->ifDesc);
                }
            }
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeFindTeIfDescr\n");

    return pOsTeIfDesc;
}

/*****************************************************************************/
/*  Function    :  OsTeUtilFindArea                                          */
/*                                                                           */
/*  Description :  This module returns the Area node for the specified       */
/*                 AreaID                                                    */
/*                                                                           */
/*  Input       :  u4AreaId - Holds the Area Id                              */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to area if the area is present                    */
/*                 NULL otherwise.                                           */
/*****************************************************************************/
PUBLIC tOsTeArea   *
OspfTeUtilFindArea (UINT4 u4AreaId)
{
    tOsTeArea          *pTeArea = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeUtilFindArea\n");

    /* Scan areas list to find the area */
    TMO_SLL_Scan (&(gOsTeContext.areasLst), pTeArea, tOsTeArea *)
    {
        if (pTeArea->u4AreaId == u4AreaId)
        {
            OSPF_TE_TRC1 (OSPF_TE_CONTROL_PLANE_TRC,
                          "Area %x node found\n", pTeArea->u4AreaId);
            return pTeArea;
        }
    }

    OSPF_TE_TRC1 (OSPF_TE_CONTROL_PLANE_TRC,
                  "Area %x node not present\n", u4AreaId);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeUtilFindArea\n");

    return NULL;
}

/*****************************************************************************/
/* Function     : OspfTeUtilExtractLsHdr                                     */
/*                                                                           */
/* Description  : Extracts LS Header information from LSA                    */
/*                                                                           */
/* Input        : pLsa - Points to LSA contents                              */
/*                pLsHeader - Holds the filles LS Header                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
OspfTeUtilExtractLsHdr (UINT1 *pLsa, tOsTeLsaHdr * pLsHeader)
{
    UINT1              *pCurrPtr = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeUtilExtractLsHdr\n");

    pCurrPtr = pLsa;

    pLsHeader->u2Age = LGET2BYTE (pCurrPtr);
    pLsHeader->u1Options = LGET1BYTE (pCurrPtr);
    pLsHeader->u1LsaType = LGET1BYTE (pCurrPtr);
    LGETSTR (pCurrPtr, pLsHeader->linkStateId, MAX_IP_ADDR_LEN);
    LGETSTR (pCurrPtr, pLsHeader->advRtrId, MAX_IP_ADDR_LEN);
    pLsHeader->u4SeqNum = LGET4BYTE (pCurrPtr);
    pLsHeader->u2ChkSum = LGET2BYTE (pCurrPtr);
    pLsHeader->u2LsaLen = LGET2BYTE (pCurrPtr);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeUtilExtractLsHdr\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTeUtilHashGetValue                                     */
/*                                                                           */
/* Description  : This procedure gets the hash value for the key             */
/*                                                                           */
/* Input        : u4HashSize - Max. no. of buckets                           */
/*                pKey       - Pointer to information to be hashed           */
/*                u1KeyLen   - Length of the information that is to be hashed*/
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hash Bucket value                                          */
/*****************************************************************************/
PUBLIC UINT4
OspfTeUtilHashGetValue (UINT4 u4HashSize, UINT1 *pKey, UINT1 u1KeyLen)
{
    UINT4               h = OSPF_TE_ZERO;
    UINT4               g;
    UINT1              *p = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeUtilHashGetValue\n");

    for (p = pKey; u1KeyLen; p++, u1KeyLen--)
    {
        h = (h << 4) + (*p);
        g = h & 0xf0000000;
        if (g)
        {
            h = h ^ (g >> 24);
            h = h ^ g;
            g = h & 0xf0000000;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeUtilHashGetValue\n");

    return h % u4HashSize;
}

/*****************************************************************************/
/* Function     : OspfTeUtilLsaIdComp                                        */
/*                                                                           */
/* Description  : This procedure compares two LSA IDs                        */
/*                                                                           */
/* Input        : u1LsaType1    - LSA type 1                                 */
/*                u4AdvRtrId1   - Advertising Router Id 1                    */
/*                pu1LsId1      - Link State Id 1                            */
/*                u1LsaType2    - LSA type 2                                 */
/*                u4AdvRtrId2   - Advertising Router Id 2                    */
/*                pu1LsId2      - Link State Id 2                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      :  OSPF_TE_EQUAL                                             */
/*                 OSPF_TE_GREATER                                           */
/*                 OSPF_TE_LESS                                              */
/*****************************************************************************/
PUBLIC UINT1
OspfTeUtilLsaIdComp (UINT1 u1LsaType1,
                     tLINKSTATEID linkStateId1,
                     tRouterId advRtrId1,
                     UINT1 u1LsaType2,
                     tLINKSTATEID linkStateId2, tRouterId advRtrId2)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeUtilHashGetValue\n");

    if (u1LsaType1 < u1LsaType2)
    {
        return OSPF_TE_LESS;
    }
    if (u1LsaType1 > u1LsaType2)
    {
        return OSPF_TE_GREATER;
    }
    switch (OspfTeUtilIpAddrComp (linkStateId1, linkStateId2))
    {
        case OSPF_TE_LESS:
            return OSPF_TE_LESS;

        case OSPF_TE_GREATER:
            return OSPF_TE_GREATER;

        case OSPF_TE_EQUAL:
            return (OspfTeUtilIpAddrComp (advRtrId1, advRtrId2));

        default:
            return OSPF_TE_THREE;
    }
}

/*****************************************************************************/
/* Function     : OspfTeUtilIpAddrComp                                       */
/*                                                                           */
/* Description  : This procedure compares two IP addresses                   */
/*                                                                           */
/* Input        : addr1 - Pointer to IP address 1                            */
/*                addr2 - Pointer to IP address 2                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_EQUAL                                              */
/*                OSPF_TE_GREATER                                            */
/*                OSPF_TE_LESS                                               */
/*****************************************************************************/
PUBLIC UINT1
OspfTeUtilIpAddrComp (tIPADDR addr1, tIPADDR addr2)
{
    UINT4               u4Addr1;
    UINT4               u4Addr2;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeUtilIpAddrComp\n");

    u4Addr1 = OSPF_TE_CRU_BMC_DWFROMPDU (addr1);
    u4Addr2 = OSPF_TE_CRU_BMC_DWFROMPDU (addr2);

    if (u4Addr1 > u4Addr2)
    {
        return OSPF_TE_GREATER;
    }

    if (u4Addr1 < u4Addr2)
    {
        return OSPF_TE_LESS;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeUtilIpAddrComp\n");

    return OSPF_TE_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTeUtilIpAddrIndComp                                    */
/*                                                                           */
/* Description  : This procedure compares the IP address and the IP          */
/*                interface index.                                           */
/*                                                                           */
/* Input        : ifIpAddr1      : Interface address of the IP address 1     */
/*                u4IfIndex1     : Interface Index 1                         */
/*                ifIpAddr2      : Interface address of the IP address 2     */
/*                u4IfIndex2     : Interface Index 2                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_EQUAL,   if they are equal                         */
/*                OSPF_TE_GREATER, if interface1 is greater                  */
/*                OSPF_TE_LESS,    if interface2 is greater                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
OspfTeUtilIpAddrIndComp (tIPADDR ifIpAddr1,
                         UINT4 u4IfIndex1, tIPADDR ifIpAddr2, UINT4 u4IfIndex2)
{
    UINT1               u1Result;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeUtilIpAddrIndComp\n");

    u1Result = OspfTeUtilIpAddrComp (ifIpAddr1, ifIpAddr2);

    if (u1Result == OSPF_TE_EQUAL)
    {
        if (u4IfIndex1 > u4IfIndex2)
        {
            u1Result = OSPF_TE_GREATER;
        }
        else if (u4IfIndex1 < u4IfIndex2)
        {
            u1Result = OSPF_TE_LESS;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeUtilIpAddrIndComp\n");

    return u1Result;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTeUtilGetLinksFromLsa                                  */
/*                                                                           */
/* Description  : This procedure scans the specified LSA and returns a link  */
/*                details.                                                   */
/*                                                                           */
/* Input        : pLsa        : pointer to LSA                               */
/*                pLinkNode   : pointer to link node structure used          */
/*                              to store links                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pCurrPtr, if link found                                    */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
OspfTeUtilGetLinksFromLsa (UINT1 *pLsa, UINT2 u2LsaLen,
                           UINT1 *pCurrPtr, tLinkNode * pLinkNode)
{
    INT1                i1TosCount = OSPF_TE_NULL;
    UINT1              *pPtr = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeUtilGetLinksFromLsa\n");

    if (pCurrPtr >= pLsa + u2LsaLen)
    {
        pPtr = NULL;
    }
    else
    {
        /* Exctracting Link Id field */
        LGETSTR (pCurrPtr, pLinkNode->linkId, MAX_IP_ADDR_LEN);

        /* Exctracting Link Data field */
        LGETSTR (pCurrPtr, pLinkNode->linkData, MAX_IP_ADDR_LEN);

        /* Exctracting Link Type filed */
        pLinkNode->u1LinkType = LGET1BYTE (pCurrPtr);

        i1TosCount = (INT1) LGET1BYTE (pCurrPtr);

        /* Extracting TOS-0 metric */
        pLinkNode->u2LinkCost = LGET2BYTE (pCurrPtr);

        pCurrPtr = pCurrPtr + (i1TosCount * MAX_IP_ADDR_LEN);

        pPtr = pCurrPtr;
    }
    return pPtr;
}

/*****************************************************************************/
/* Function     : OspfTeUtilExtractLinkTlv                                   */
/*                                                                           */
/* Description  : Extracts Link TE informations from the Type 10 LSA         */
/*                                                                           */
/* Input        : pLsaNode - Holds the Type 10 LSA contents                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tOsTeLinkTlvNode *                                         */
/*                NULL                                                       */
/*****************************************************************************/
PUBLIC tOsTeLinkTlvNode *
OspfTeUtilExtractLinkTlv (tOsTeLsaNode * pTeLsaInfo)
{
    UINT2               u2TlvLen;
    UINT2               u2SrlgCount;
    UINT4               u4SrlgNo;
    UINT2               u2SubTlvType;
    UINT2               u2SubTlvLen;
    UINT2               u2TLVType;
    UINT2               u2Count = OSPF_TE_ZERO;
    UINT1               u1Priority;
    UINT1              *pLinkTlv = NULL;
    UINT1              *pCurrPtr = NULL;
    tOsTeLinkTlvNode   *pLinkTlvNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeUtilExtractLinkTlv\n");

    pLinkTlv = pTeLsaInfo->pLsa + OSPF_TE_LSA_HEADER_LENGTH;

    u2TLVType = LGET2BYTE (pLinkTlv);

    if (u2TLVType != OSPF_TE_LINK_TLV)
    {
        return NULL;
    }

    /* Allocate memory for the Link Node */
    if (OSPF_TE_LINK_TLV_ALLOC (pLinkTlvNode, tOsTeLinkTlvNode) == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Memory Allocation for Link "
                     "TLV failed\n");
        return NULL;
    }

    u2TlvLen = LGET2BYTE (pLinkTlv);

    pCurrPtr = pLinkTlv;

    OSPF_TE_MEMSET (pLinkTlvNode, OSPF_TE_ZERO, sizeof (tOsTeLinkTlvNode));

    TMO_SLL_Init (&(pLinkTlvNode->ifDescrLst));

    /* Continue scanning until processing all sub TLVs */
    while ((pCurrPtr - pLinkTlv) < u2TlvLen)
    {
        /* Extracting the Sub TLVs Type field */
        u2SubTlvType = LGET2BYTE (pCurrPtr);
        /* Extracting the Sub TLVs Length field */
        u2SubTlvLen = LGET2BYTE (pCurrPtr);

        switch (u2SubTlvType)
        {
            case OSPF_TE_LINKTYPE:
                /* Extracting Link Type Value */
                pLinkTlvNode->u1LinkType = LGET1BYTE (pCurrPtr);
                /* Ignoring Padding */
                pCurrPtr = pCurrPtr + OSPF_TE_LINK_TYPE_PAD;
                break;

            case OSPF_TE_LINKID:
                pLinkTlvNode->u4LinkId = LGET4BYTE (pCurrPtr);
                break;

            case OSPF_TE_LOCALIF:
                pLinkTlvNode->u4LocalIpAddr = LGET4BYTE (pCurrPtr);
                break;

            case OSPF_TE_REMOTEIF:
                pLinkTlvNode->u4RemoteIpAddr = LGET4BYTE (pCurrPtr);
                break;

            case OSPF_TE_METRIC:
                pLinkTlvNode->u4TeMetric = LGET4BYTE (pCurrPtr);
                break;

            case OSPF_TE_MAXBW:
                pLinkTlvNode->maxBw = LGETFLOAT (pCurrPtr);
                break;

            case OSPF_TE_MAXRESBW:
                pLinkTlvNode->maxResBw = LGETFLOAT (pCurrPtr);
                break;

            case OSPF_TE_UNRESBW:
                for (u1Priority = OSPF_TE_ZERO;
                     u1Priority < OSPF_TE_MAX_PRIORITY_LVL; u1Priority++)
                {
                    pLinkTlvNode->aUnResrvBw[u1Priority] = LGETFLOAT (pCurrPtr);
                }
                break;

            case OSPF_TE_RSRCLR:
                pLinkTlvNode->u4RsrcClassColor = LGET4BYTE (pCurrPtr);
                break;

            case OSPF_TE_IDENTIFIERS:
                pLinkTlvNode->u4LocalIdentifier = LGET4BYTE (pCurrPtr);
                pLinkTlvNode->u4RemoteIdentifier = LGET4BYTE (pCurrPtr);
                break;

            case OSPF_TE_PROTECTION_TYPE:
                pLinkTlvNode->u1ProtectionType = LGET1BYTE (pCurrPtr);
                /* Omitting Reserved fileds */
                pCurrPtr = pCurrPtr + OSPF_TE_PROTECTION_RES_LEN;
                break;

            case OSPF_TE_INSWCA_DESC:
                OspfTeUtilExtractLinkDesc (pLinkTlvNode, pCurrPtr);
                pCurrPtr = pCurrPtr + u2SubTlvLen;
                break;

            case OSPF_TE_SRLG:
                u2SrlgCount = (UINT2) (u2SubTlvLen / MAX_IP_ADDR_LEN);

                while (u2Count < u2SrlgCount)
                {
                    u4SrlgNo = LGET4BYTE (pCurrPtr);
                    if (u2Count < OSPF_TE_MAX_SRLG_NO)
                    {
                        pLinkTlvNode->osTeSrlg.aSrlgNumber[u2Count] = u4SrlgNo;
                        pLinkTlvNode->osTeSrlg.u4NoOfSrlg++;
                    }
                    u2Count++;
                }
                break;

            default:
                OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Unrecognized Sub "
                             "TLV Type\n");
                pCurrPtr = pCurrPtr + u2SubTlvLen;
                break;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeUtilExtractLinkTlv\n");

    return pLinkTlvNode;
}

/*****************************************************************************/
/* Function     : OspfTeUtilExtractLinkDesc                                  */
/*                                                                           */
/* Description  : Extracts Link Descriptor from Link TLV                     */
/*                                                                           */
/* Input        : pLinkTlvNode - Pointer to Link TLV node                    */
/*                pCurrPtr     - Pointer to Start of the Sub TLV             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the Next location to the End of the Sub TLV     */
/*****************************************************************************/
PUBLIC VOID
OspfTeUtilExtractLinkDesc (tOsTeLinkTlvNode * pLinkTlvNode, UINT1 *pCurrPtr)
{
    UINT1               u1Priority;
    tOsTeIfDesc        *pOsTeIfDesc = NULL;
    tOsTeIfDescNode    *pOsTeIfDescNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeUtilExtractLinkDesc\n");

    if (OSPF_TE_IF_DESC_ALLOC (pOsTeIfDescNode, tOsTeIfDescNode) == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Buffer Allocation failure "
                     "for If Descriptor\n");
        return;
    }

    pOsTeIfDesc = &(pOsTeIfDescNode->ifDesc);
    pOsTeIfDesc->u1Indication = OSPF_TE_INVALID_INDICATION;
    pOsTeIfDesc->minLSPBw = OSPF_TE_ZERO;

    /* Extracting the Link Descriptor information */
    pOsTeIfDesc->u1SwitchingCap = LGET1BYTE (pCurrPtr);
    pOsTeIfDesc->u1EncodingType = LGET1BYTE (pCurrPtr);
    pCurrPtr = pCurrPtr + OSPF_TE_TWO;

    for (u1Priority = OSPF_TE_ZERO; u1Priority < OSPF_TE_MAX_PRIORITY_LVL;
         u1Priority++)
    {
        pOsTeIfDesc->aDescMaxLSPBw[u1Priority] = LGETFLOAT (pCurrPtr);
    }

    switch (pOsTeIfDesc->u1SwitchingCap)
    {
        case OSPF_TE_PSC_1:
        case OSPF_TE_PSC_2:
        case OSPF_TE_PSC_3:
        case OSPF_TE_PSC_4:
            /* Filling Switching capability information */
            pOsTeIfDesc->minLSPBw = LGETFLOAT (pCurrPtr);
            pOsTeIfDesc->u2MTU = LGET2BYTE (pCurrPtr);
            pCurrPtr = pCurrPtr + OSPF_TE_TWO;
            break;

        case OSPF_TE_TDM:
            /* Filling Switching capability information */
            pOsTeIfDesc->minLSPBw = LGETFLOAT (pCurrPtr);
            pOsTeIfDesc->u1Indication = LGET1BYTE (pCurrPtr);
            pCurrPtr = pCurrPtr + OSPF_TE_THREE;
            break;

        default:
            /* For other switching types, there is no switching
             * capability information */
            break;
    }

    /* Add this infomration to the interface descriptor list */
    TMO_SLL_Add (&(pLinkTlvNode->ifDescrLst),
                 &(pOsTeIfDescNode->NextIfDescrNode));

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeUtilExtractLinkDesc\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeGetIfInfoFromRtrLsaLink                             */
/*                                                                           */
/*  Description :  This function determines the interface IP address and     */
/*                 addressless interface index                               */
/*                                                                           */
/*  Input       :  linkData - LINK data value of the link.                   */
/*                 pIfAddr  - Pointer to interface IP address                */
/*                 pu4AddrlessIf - Pointer to addressless if index           */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeGetIfInfoFromRtrLsaLink (tIPADDR linkData, tIPADDR * pIfAddr,
                               UINT4 *pu4AddrlessIf)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessRtrLsaFlushLink\n");

    if (linkData[OSPF_TE_ZERO] == OSPF_TE_ZERO)
    {
        /* Unnumbered Point to Point interface */
        IP_ADDR_COPY (pIfAddr, gOsTeNullIpAddr);
        *pu4AddrlessIf = OSPF_TE_CRU_BMC_DWFROMPDU (linkData);
    }
    else
    {
        /* Numbered Point to Point interface */
        IP_ADDR_COPY (pIfAddr, linkData);
        *pu4AddrlessIf = OSPF_TE_ZERO;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessRtrLsaFlushLink\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeUtilFindNetworkLsa                                  */
/*                                                                           */
/*  Description :  This function finds the network LSA of correspoding to    */
/*                 the given link state Id.                                  */
/*                                                                           */
/*  Input       :  linkStateId - Link State Id of the Network LSA.           */
/*                 pTeArea  - Pointer to TE Area.                            */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to the Network LSA if found, otherwise NULL.      */
/*****************************************************************************/
PUBLIC tOsTeLsaNode *
OspfTeUtilFindNetworkLsa (tLINKSTATEID linkStateId, tOsTeArea * pTeArea)
{
    tTMO_SLL_NODE      *pLsaNode = NULL;
    tOsTeLsaNode       *pNetworkLsa = NULL;

    TMO_SLL_Scan (&(pTeArea->networkLsaLst), pLsaNode, tTMO_SLL_NODE *)
    {
        pNetworkLsa = OSPF_TE_GET_BASE_PTR (tOsTeLsaNode,
                                            NextSortLsaNode, pLsaNode);

        if (OspfTeUtilIpAddrComp (pNetworkLsa->linkStateId,
                                  linkStateId) == OSPF_TE_EQUAL)
        {
            break;
        }
    }
    return pNetworkLsa;
}

/*****************************************************************************/
/*  Function    :  OspfTeMapAreaForFA                                        */
/*                                                                           */
/*  Description :  This module returns the Area node for the specified       */
/*                 FA TE Link                                                */
/*                                                                           */
/*  Input       :  u4CfaPortIndex - Holds the Index of the CFA Port on which */
/*                                  FA TE-Link is created.                   */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to area if the area is present                    */
/*                 NULL otherwise.                                           */
/*****************************************************************************/
PUBLIC tOsTeArea   *
OspfTeMapAreaForFA (UINT4 u4CfaPortIndex)
{
    tOsTeInterface     *pTeInterface = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4AddrlessIf = OSPF_TE_ZERO;
    UINT4               u4IpPortNum = OSPF_TE_ZERO;
    tIPADDR             ifIpAddr;

    MEMSET (ifIpAddr, OSPF_TE_ZERO, sizeof (ifIpAddr));

    /* Assumptions:
     * 1. TLM Always provides FA TE Link Interface Index.
     * 2. Layering Architecture for FA TE Link is as follows:
     * 
     *        -----------------
     *       |   FA TE-Link    |
     *        -----------------
     *       |   Mpls Tunnel   |
     *        -----------------
     *       |  Mpls Interface |
     *        -----------------
     *       |     TE-Link     |
     *        -----------------
     *       |   COMP-Link     |  -----> Which is a IP Interface.
     *       |                 | and this info is provided by TLM
     *        -----------------
     *        
     * 3. CFA Knows this stacking hierarchy.
     * 4. TE-Link Information is added to OSPFTE database well before
     *    FA TELink is created over that TE Link.
     */
    if (NetIpv4GetPortFromIfIndex (u4CfaPortIndex,
                                   &u4IpPortNum) == NETIPV4_FAILURE)
    {
        OSPF_TE_TRC1 (OSPF_TE_CRITICAL_TRC,
                      "No Ip Interface found for FA TELink"
                      " with CfaPort %d \n", u4CfaPortIndex);
        return NULL;

    }

    if (NetIpv4GetIfInfo (u4IpPortNum, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        OSPF_TE_TRC1 (OSPF_TE_CRITICAL_TRC,
                      "No Ip Interface found for FA TELink"
                      " with CfaPort %d \n", u4CfaPortIndex);
        return NULL;
    }

    if (NetIpIfInfo.u4Addr != OSPF_TE_ZERO)
    {
        OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, NetIpIfInfo.u4Addr);
    }
    else
    {
        u4AddrlessIf = u4CfaPortIndex;
    }

    pTeInterface = OspfTeFindTeInterface (ifIpAddr, u4AddrlessIf);

    if (pTeInterface == NULL)
    {
        OSPF_TE_TRC1 (OSPF_TE_CRITICAL_TRC,
                      "No corresponding TELink found for FA TELink"
                      " with CfaPort %d \n", u4CfaPortIndex);
        return NULL;
    }

    return pTeInterface->pTeArea;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTeUtilDwordFromPdu                                     */
/*                                                                           */
/* Description  : This procedure converts Address which is in Network Byte   */
/*                Order to Host Byte Order and returns the same              */
/*                                                                           */
/* Input        : pu1PduAddr   : IP address in Network Byte Order            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP address in Host Byte Order                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
OspfTeUtilDwordFromPdu (UINT1 *pu1PduAddr)
{
    UINT4               u4TempData = 0;
    MEMCPY (&u4TempData, pu1PduAddr, MAX_IPV4_ADDR_LEN);
    return (OSIX_NTOHL (u4TempData));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfTeUtilDwordToPdu                                       */
/*                                                                           */
/* Description  : This procedure converts Address which is in Host Byte      */
/*                Order to Network Byte Order                                */
/*                                                                           */
/* Input        : u4Value      : IP Address in Host Byte Order               */
/*                                                                           */
/* Output       : pu1PduAddr                                                 */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OspfTeUtilDwordToPdu (UINT1 *pu1PduAddr, UINT4 u4Value)
{
    UINT4               u4TempData = 0;
    u4TempData = OSIX_HTONL (u4Value);
    MEMCPY (pu1PduAddr, &u4TempData, MAX_IPV4_ADDR_LEN);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osteutil.c                     */
/*-----------------------------------------------------------------------*/
