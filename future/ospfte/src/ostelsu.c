/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: ostelsu.c,v 1.5 2012/08/07 10:36:39 siva Exp $
*
* Description:  This file has routines for TE database management.
*********************************************************************/

#include "osteinc.h"
/*****************************************************************************/
/* Function     : OspfTeLsuSearchDatabase                                    */
/*                                                                           */
/* Description  : This module searches for the specified advertisement in the*/
/*                specified area's database. If the searched advertisement is*/
/*                not found in the database then this function returns NULL  */
/*                otherwise returns the found LSA.                           */
/*                                                                           */
/* Input        : u1LsaType     - Type of LSA                                */
/*                pLinkStateId  - Points to link state Id.                   */
/*                pAdvRtrId     - Points to Advertising Router Id.           */
/*                pPtr          - Points to the TE interface for Type 9 LSAs */
/*                                or Points to Area for other type LSAs.     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to LSA if LSA is present otherwise NULL            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tOsTeLsaNode *
OspfTeLsuSearchDatabase (UINT1 u1LsaType,
                         tLINKSTATEID * pLinkStateId,
                         tRouterId * pAdvRtrId, UINT1 *pPtr)
{
    UINT4               u4HashIndex;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;
    tOsTeDbNode        *pOsTeDbNode = NULL;
    tOsTeInterface     *pOsTeIf = NULL;
    tTMO_SLL           *pLsaLst = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeArea          *pTeArea = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLsuSearchDatabase\n");

    switch (u1LsaType)
    {
        case OSPF_TE_ROUTER_LSA:
            pTeArea = (tOsTeArea *) (VOID *) pPtr;
            break;
        case OSPF_TE_NETWORK_LSA:
            pTeArea = (tOsTeArea *) (VOID *) pPtr;
            pLsaLst = &(pTeArea->networkLsaLst);
            break;

        case OSPF_TE_TYPE9_LSA:
            pOsTeIf = (tOsTeInterface *) (VOID *) pPtr;
            pLsaLst = &(pOsTeIf->type9TELsaLst);
            break;

        case OSPF_TE_TYPE10_LSA:
            pTeArea = (tOsTeArea *) (VOID *) pPtr;
            u4HashIndex = OspfTeLsuLsaHashFunc (pAdvRtrId);

            TMO_HASH_Scan_Bucket (pTeArea->pTeDbHashTbl, u4HashIndex,
                                  pOsTeDbNode, tOsTeDbNode *)
            {
                if ((OspfTeUtilIpAddrComp (pOsTeDbNode->rtrId, *pAdvRtrId)
                     == OSPF_TE_EQUAL))
                {
                    pLsaLst = &(pOsTeDbNode->TeLsaLst);
                }
            }
            break;

        default:
            break;
    }

    if (pLsaLst != NULL)
    {
        TMO_SLL_Scan (pLsaLst, pLstNode, tTMO_SLL_NODE *)
        {
            if (u1LsaType != OSPF_TE_TYPE10_LSA)
            {
                pOsTeLsaNode =
                    OSPF_TE_GET_BASE_PTR (tOsTeLsaNode, NextSortLsaNode,
                                          pLstNode);
            }
            else
            {
                pOsTeLsaNode = OSPF_TE_GET_BASE_PTR (tOsTeLsaNode,
                                                     NextDbNodeLsa, pLstNode);
            }

            if (OspfTeUtilLsaIdComp (u1LsaType, *pLinkStateId, *pAdvRtrId,
                                     pOsTeLsaNode->u1LsaType,
                                     pOsTeLsaNode->linkStateId,
                                     pOsTeLsaNode->advRtrId) == OSPF_TE_EQUAL)
            {
                return pOsTeLsaNode;
            }
        }
    }
    else if ((pTeArea != NULL) && (pTeArea->pRtrLsa != NULL))
    {
        pOsTeLsaNode = pTeArea->pRtrLsa;
        if (OspfTeUtilLsaIdComp (u1LsaType, *pLinkStateId, *pAdvRtrId,
                                 pOsTeLsaNode->u1LsaType,
                                 pOsTeLsaNode->linkStateId,
                                 pOsTeLsaNode->advRtrId) == OSPF_TE_EQUAL)
        {
            return pOsTeLsaNode;
        }
    }

    OSPF_TE_TRC (OSPF_TE_CONTROL_PLANE_TRC, "LSA Search in Database Failed\n");

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLsuSearchDatabase\n");

    return NULL;
}

/*****************************************************************************/
/* Function     : OspfTeLsuDeleteLsaFromDatabase                             */
/*                                                                           */
/* Description  : This module deletes the specified lsa from the database.   */
/*                The conditions necessary for deleting a lsa from the       */
/*                database should be taken care at the calling place.        */
/*                                                                           */
/* Input        : pLsaInfo - Points to the advertisement                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/

PUBLIC VOID
OspfTeLsuDeleteLsaFromDatabase (tOsTeLsaNode * pLsaInfo)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLsuDeleteLsaFromDatabase\n");

    switch (pLsaInfo->u1LsaType)
    {
        case OSPF_TE_ROUTER_LSA:
            pLsaInfo->pTeArea->pRtrLsa = NULL;
            break;

        case OSPF_TE_NETWORK_LSA:
            pLsaInfo->pTeArea->u4NetLsaChksumSum -= pLsaInfo->u2LsaChksum;
            break;

        case OSPF_TE_TYPE9_LSA:
            pLsaInfo->pOsTeInt->u4Type9LsaChksumSum -= pLsaInfo->u2LsaChksum;
            break;

        case OSPF_TE_TYPE10_LSA:
            pLsaInfo->pTeArea->u4Type10LsaChksumSum -= pLsaInfo->u2LsaChksum;
            break;

        default:
            return;
    }

    OspfTeLsuClearLsaInfo (pLsaInfo);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLsuDeleteLsaFromDatabase\n");

    return;
}

/*****************************************************************************/
/* Function     : OspfTeLsuClearLsaInfo                                      */
/*                                                                           */
/* Description  : Clears the LSA from LSA list.                              */
/*                                                                           */
/* Input        : pLsaInfo - Points to the advertisement                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/

PUBLIC VOID
OspfTeLsuClearLsaInfo (tOsTeLsaNode * pLsaInfo)
{
    UINT4               u4HashIndex;
    tOsTeDbNode        *pOsTeDbNode = NULL;
    tOsTeLsaNode       *pDbLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pLstNode1 = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tTMO_SLL_NODE      *pTempNode1 = NULL;
    tTMO_SLL_NODE      *pLstDbNode = NULL;
    tTMO_SLL_NODE      *pTempDbNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLsuClearLsaInfo\n");

    if (pLsaInfo->u1LsaType == OSPF_TE_TYPE10_LSA)
    {
        /* From the Database find the Hash Node */
        u4HashIndex = OspfTeLsuLsaHashFunc (&(pLsaInfo->advRtrId));

        OSPF_TE_DYNM_SLL_SCAN
            (&(pLsaInfo->pTeArea->pTeDbHashTbl)->HashList[(u4HashIndex)],
             pLstDbNode, pTempDbNode, tTMO_SLL_NODE *)
        {
            pOsTeDbNode = (tOsTeDbNode *) pLstDbNode;
            if (OspfTeUtilIpAddrComp (pOsTeDbNode->rtrId, pLsaInfo->advRtrId)
                == OSPF_TE_EQUAL)
            {
                OSPF_TE_DYNM_SLL_SCAN (&(pOsTeDbNode->TeLsaLst), pLstNode,
                                       pTempNode, tTMO_SLL_NODE *)
                {
                    pDbLsaInfo = OSPF_TE_GET_BASE_PTR (tOsTeLsaNode,
                                                       NextDbNodeLsa, pLstNode);
                    if (OspfTeUtilIpAddrComp (pDbLsaInfo->linkStateId,
                                              pLsaInfo->linkStateId)
                        == OSPF_TE_EQUAL)
                    {
                        TMO_SLL_Delete (&(pOsTeDbNode->TeLsaLst),
                                        (tTMO_SLL_NODE *) & (pLsaInfo->
                                                             NextDbNodeLsa));
                    }
                }

                /* Check if Hash bucket is empty, if so, delete the bucket */
                if (TMO_SLL_Count (&(pOsTeDbNode->TeLsaLst)) == OSPF_TE_ZERO)
                {
                    /* Delete the hash node and free its memory */
                    TMO_HASH_Delete_Node (pLsaInfo->pTeArea->pTeDbHashTbl,
                                          &(pOsTeDbNode->NextTeDbNode),
                                          u4HashIndex);

                    OSPF_TE_TEDB_HASH_NODE_FREE (&(pOsTeDbNode->NextTeDbNode));
                }
                break;
            }                    /* End of check if matching TE DB node got */
        }                        /* End of Scan of Hash Table */
    }

    /* Delete from LSA list */
    switch (pLsaInfo->u1LsaType)
    {
        case OSPF_TE_NETWORK_LSA:
            TMO_SLL_Delete (&(pLsaInfo->pTeArea->networkLsaLst),
                            &(pLsaInfo->NextSortLsaNode));
            break;

        case OSPF_TE_TYPE9_LSA:
            TMO_SLL_Delete (&(pLsaInfo->pOsTeInt->type9TELsaLst),
                            &(pLsaInfo->NextSortLsaNode));
            break;

        case OSPF_TE_TYPE10_LSA:
            TMO_SLL_Delete (&(pLsaInfo->pTeArea->type10OpqLsaLst),
                            &(pLsaInfo->NextSortLsaNode));
            if (pLsaInfo->pLinkTlv != NULL)
            {
                OSPF_TE_DYNM_SLL_SCAN (&(pLsaInfo->pLinkTlv->ifDescrLst),
                                       pLstNode1, pTempNode1, tTMO_SLL_NODE *)
                {
                    TMO_SLL_Delete (&(pLsaInfo->pLinkTlv->ifDescrLst),
                                    pLstNode1);
                    OSPF_TE_IF_DESC_FREE ((tOsTeIfDescNode *) pLstNode1);
                }
                OSPF_TE_LINK_TLV_FREE (pLsaInfo->pLinkTlv);
            }
            break;

        default:
            return;
    }

    OSPF_TE_LSA_FREE (pLsaInfo->pLsa);
    OSPF_TE_LSA_INFO_FREE (pLsaInfo);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLsuClearLsaInfo\n");

    return;
}

/*****************************************************************************/
/* Function     : OspfTeLsuAddToLsdb                                         */
/*                                                                           */
/* Description  : This module allocates a new LSA_INFO structure sets the    */
/*                LsaId fields and adds it to the hash table.                */
/*                                                                           */
/* Input        : pPtr         - Points to the TE interface for Type 9 LSAs  */
/*                               or Points to Area for other type LSAs.      */
/*                u1LsaType    - Indicates Type of LSA                       */
/*                pLinkStateId - Points to link state Id.                    */
/*                pAdvRtrId    - Points to Advertising Router Id.            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to LSA if LSA is added otherwise NULL              */
/*                                                                           */
/*****************************************************************************/

PUBLIC tOsTeLsaNode *
OspfTeLsuAddToLsdb (UINT1 *pPtr,
                    UINT1 u1LsaType,
                    tLINKSTATEID * pLinkStateId, tRouterId * pAdvRtrId)
{
    UINT4               u4HashIndex;
    UINT1               u1DbNodeFound = OSPF_TE_FALSE;
    tOsTeArea          *pTeArea = NULL;
    tOsTeLsaNode       *pLsaNode = NULL;
    tOsTeDbNode        *pOsTeDbNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLsuAddToLsdb\n");

    if (OSPF_TE_LSA_INFO_ALLOC (pLsaNode, tOsTeLsaNode) == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "LSA Info Allocation Failure\n");
        return NULL;
    }

    if (u1LsaType != OSPF_TE_TYPE9_LSA)
    {
        pTeArea = (tOsTeArea *) (VOID *) pPtr;
    }

    if (u1LsaType == OSPF_TE_TYPE10_LSA)
    {
        u4HashIndex = OspfTeLsuLsaHashFunc (pAdvRtrId);

        TMO_HASH_Scan_Bucket (pTeArea->pTeDbHashTbl, u4HashIndex, pOsTeDbNode,
                              tOsTeDbNode *)
        {
            if (OspfTeUtilIpAddrComp (pOsTeDbNode->rtrId, *pAdvRtrId)
                == OSPF_TE_EQUAL)
            {
                u1DbNodeFound = OSPF_TE_TRUE;
                break;
            }                    /* End of check if matching Hash node */
        }                        /* End of scan of Hash Bucket */

        /* If Database node is not present, then Create a hash node */
        if (u1DbNodeFound == OSPF_TE_FALSE)
        {
            if (OSPF_TE_TEDB_HASH_NODE_ALLOC (pOsTeDbNode, tOsTeDbNode) == NULL)
            {
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                             "LSA TEDB Node Allocation Failure\n");
                OSPF_TE_LSA_INFO_FREE (pLsaNode);
                return NULL;
            }

            /* Initialise the Hash node */
            TMO_SLL_Init (&(pOsTeDbNode->TeLsaLst));
            IP_ADDR_COPY (&(pOsTeDbNode->rtrId), pAdvRtrId);

            /* Adding the Created Hash node to Hash Table */
            TMO_HASH_Add_Node (pTeArea->pTeDbHashTbl,
                               &(pOsTeDbNode->NextTeDbNode), u4HashIndex, NULL);
        }
        TMO_SLL_Add (&(pOsTeDbNode->TeLsaLst),
                     (tTMO_SLL_NODE *) & (pLsaNode->NextDbNodeLsa));
    }

    /* Initialise the LSA node added to the LSA list in the Hash Node */
    pLsaNode->u1LsaType = u1LsaType;
    OSPF_TE_MEMCPY (&(pLsaNode->linkStateId), pLinkStateId, OSPF_TE_LSA_ID_LEN);
    IP_ADDR_COPY (&(pLsaNode->advRtrId), pAdvRtrId);
    pLsaNode->pLsa = NULL;

    if (u1LsaType != OSPF_TE_TYPE9_LSA)
    {
        pLsaNode->pTeArea = pTeArea;
    }

    if (u1LsaType == OSPF_TE_TYPE9_LSA)
    {
        pLsaNode->pOsTeInt = (tOsTeInterface *) (VOID *) pPtr;
    }

    if (u1LsaType != OSPF_TE_ROUTER_LSA)
    {
        OspfTeLsuAddToSortLsaLst (pLsaNode);
    }
    else
    {
        pTeArea->pRtrLsa = pLsaNode;
    }

    OSPF_TE_TRC (OSPF_TE_CONTROL_PLANE_TRC, "New LSA Info Created\n");

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLsuAddToLsdb\n");

    return pLsaNode;
}

/*****************************************************************************/
/* Function     : OspfTeLsuAddToSortLsaLst                                   */
/*                                                                           */
/* Description  : Adds the given advertisement to the LSA list.              */
/*                                                                           */
/* Input        : pLsaNode - pointer to the advertisement                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Node                                                       */
/*****************************************************************************/

PUBLIC VOID
OspfTeLsuAddToSortLsaLst (tOsTeLsaNode * pLsaNode)
{
    tTMO_SLL           *pLsaLst = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    tOsTeLsaNode       *pLstLsaNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLsuAddToSortLsaLst\n");

    switch (pLsaNode->u1LsaType)
    {
        case OSPF_TE_NETWORK_LSA:
            pLsaLst = &(pLsaNode->pTeArea->networkLsaLst);
            break;

        case OSPF_TE_TYPE9_LSA:
            pLsaLst = &(pLsaNode->pOsTeInt->type9TELsaLst);
            break;

        case OSPF_TE_TYPE10_LSA:
            pLsaLst = &(pLsaNode->pTeArea->type10OpqLsaLst);
            break;

        default:
            OSPF_TE_TRC1 (OSPF_TE_FAILURE_TRC,
                          "Received invalid LSA Type %d, not adding to "
                          "the sorted list\n", pLsaNode->u1LsaType);
            return;
    }

    TMO_SLL_Scan (pLsaLst, pLstNode, tTMO_SLL_NODE *)
    {
        pLstLsaNode = OSPF_TE_GET_BASE_PTR (tOsTeLsaNode, NextSortLsaNode,
                                            pLstNode);
        if (OspfTeUtilLsaIdComp (pLstLsaNode->u1LsaType,
                                 pLstLsaNode->linkStateId,
                                 pLstLsaNode->advRtrId,
                                 pLsaNode->u1LsaType,
                                 pLsaNode->linkStateId,
                                 pLsaNode->advRtrId) == OSPF_TE_GREATER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }

    TMO_SLL_Insert (pLsaLst, pPrevNode, &(pLsaNode->NextSortLsaNode));

    OSPF_TE_TRC1 (OSPF_TE_CONTROL_PLANE_TRC,
                  "Type %d LSA is added to LSA sorted list\n",
                  pLsaNode->u1LsaType);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLsuAddToSortLsaLst\n");

    return;
}

/*****************************************************************************/
/* Function     : OspfTeLsuInstallLsa                                        */
/*                                                                           */
/* Description  : This procedure installs the LSA in the specified area. If  */
/*                there is any change appropriate routing table entries are  */
/*                re-calculated.                                             */
/*                                                                           */
/* Input        : pu1Lsa    - Points to the LSA                              */
/*                pLsaNode  - Points to the advertisement                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/

PUBLIC VOID
OspfTeLsuInstallLsa (UINT1 *pu1Lsa, tOsTeLsaNode * pLsaInfo)
{
    tOsTeLsaHdr         CurrLsa;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLsuInstallLsa\n");

    /* Extract the LSA message header */
    OspfTeUtilExtractLsHdr (pu1Lsa, &CurrLsa);

    if (pLsaInfo->pLsa != NULL)
    {
        /* Freeing old LSA buffer */
        OSPF_TE_LSA_FREE (pLsaInfo->pLsa);

        /* Updating check sum information */
        switch (pLsaInfo->u1LsaType)
        {
            case OSPF_TE_NETWORK_LSA:
                pLsaInfo->pTeArea->u4NetLsaChksumSum -= pLsaInfo->u2LsaChksum;
                break;

            case OSPF_TE_TYPE9_LSA:
                pLsaInfo->pOsTeInt->u4Type9LsaChksumSum -=
                    pLsaInfo->u2LsaChksum;
                break;

            case OSPF_TE_TYPE10_LSA:
                pLsaInfo->pTeArea->u4Type10LsaChksumSum -=
                    pLsaInfo->u2LsaChksum;
                /* Free the Old TLV information */
                if (pLsaInfo->pLinkTlv != NULL)
                {
                    OSPF_TE_DYNM_SLL_SCAN (&(pLsaInfo->pLinkTlv->ifDescrLst),
                                           pLstNode, pTempNode, tTMO_SLL_NODE *)
                    {
                        TMO_SLL_Delete (&(pLsaInfo->pLinkTlv->ifDescrLst),
                                        pLstNode);
                        OSPF_TE_IF_DESC_FREE ((tOsTeIfDescNode *) pLstNode);
                    }
                    OSPF_TE_LINK_TLV_FREE (pLsaInfo->pLinkTlv);
                }
                break;

            default:
                break;
        }
    }

    OSPF_TE_MEMCPY (pLsaInfo->linkStateId, CurrLsa.linkStateId,
                    OSPF_TE_LSA_ID_LEN);
    IP_ADDR_COPY (pLsaInfo->advRtrId, CurrLsa.advRtrId);
    pLsaInfo->u1LsaType = CurrLsa.u1LsaType;
    pLsaInfo->u2LsaChksum = CurrLsa.u2ChkSum;
    pLsaInfo->u2LsaLen = CurrLsa.u2LsaLen;
    pLsaInfo->pLsa = pu1Lsa;

    switch (pLsaInfo->u1LsaType)
    {
        case OSPF_TE_NETWORK_LSA:
            pLsaInfo->pTeArea->u4NetLsaChksumSum += pLsaInfo->u2LsaChksum;
            break;

        case OSPF_TE_TYPE9_LSA:
            pLsaInfo->pOsTeInt->u4Type9LsaChksumSum += pLsaInfo->u2LsaChksum;
            break;

        case OSPF_TE_TYPE10_LSA:
            pLsaInfo->pTeArea->u4Type10LsaChksumSum += pLsaInfo->u2LsaChksum;
            /* Extracting the Link TLV information */
            pLsaInfo->pLinkTlv = OspfTeUtilExtractLinkTlv (pLsaInfo);
            break;

        default:
            break;
    }

    OSPF_TE_TRC1 (OSPF_TE_CONTROL_PLANE_TRC, "Installed Type %d LSA\n",
                  pLsaInfo->u1LsaType);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLsuInstallLsa\n");

    return;
}

/*****************************************************************************/
/* Function     : OspfTeLsuLsaHashFunc                                       */
/*                                                                           */
/* Description  : This function gives the hash value of the key.             */
/*                                                                           */
/* Input        : pAdvRtrId    - Points to Advertising Router Id.            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hashed value of the Key.                                   */
/*****************************************************************************/

PUBLIC UINT4
OspfTeLsuLsaHashFunc (tRouterId * pAdvRtrId)
{
    UINT4               u4HashIndex;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLsuLsaHashFunc\n");

    u4HashIndex = OspfTeUtilHashGetValue (OSPF_TE_LSA_HASH_TBL_SIZE,
                                          (UINT1 *) pAdvRtrId,
                                          OSPF_TE_LSA_HASH_KEY_SIZE);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLsuLsaHashFunc\n");

    return u4HashIndex;
}

/*****************************************************************************/
/* Function     : OspfTeLsuDeleteAllLsas                                     */
/*                                                                           */
/* Description  : Delete All LSAs from database.                             */
/*                                                                           */
/* Input        : pTeArea - Pointer to TE Area.                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
OspfTeLsuDeleteAllLsas (tOsTeArea * pTeArea)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLsuDeleteAllLsas\n");

    /* Delete the Network LSAs */
    OspfTeLsuDeleteNetLsas (pTeArea);
    /* Delete the Type 9 LSAs */
    OspfTeLsuDeleteType9Lsas (pTeArea);
    /* Delete the Type 10 LSAs */
    OspfTeLsuDeleteType10Lsas (pTeArea, OSPF_TE_FALSE);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLsuDeleteAllLsas\n");

    return;
}

/*****************************************************************************/
/* Function     : OspfTeLsuDeleteNetLsas                                     */
/*                                                                           */
/* Description  : Delete Network LSAs                                        */
/*                                                                           */
/* Input        : pTeArea - Pointer to TE Area.                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
OspfTeLsuDeleteNetLsas (tOsTeArea * pTeArea)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tOsTeLsaNode       *pLsaInfo = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeLsuDeleteNetLsas\n");

    /* Deleting each LSA from Network LSA list */
    OSPF_TE_DYNM_SLL_SCAN (&(pTeArea->networkLsaLst), pLstNode,
                           pTempNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = OSPF_TE_GET_BASE_PTR (tOsTeLsaNode, NextSortLsaNode,
                                         pLstNode);
        TMO_SLL_Delete (&(pTeArea->networkLsaLst),
                        &(pLsaInfo->NextSortLsaNode));
        OSPF_TE_LSA_FREE (pLsaInfo->pLsa);
        OSPF_TE_LSA_INFO_FREE (pLsaInfo);
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeLsuDeleteNetLsas\n");

    return;
}

/*****************************************************************************/
/* Function     : OspfTeLsuDeleteType9Lsas                                   */
/*                                                                           */
/* Description  : Delete Type 9 LSAs.                                        */
/*                                                                           */
/* Input        : pTeArea - Pointer to TE Area.                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
OspfTeLsuDeleteType9Lsas (tOsTeArea * pTeArea)
{
    tOsTeLsaNode       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pLstNode1 = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tOsTeInterface     *pOsTeInterface = NULL;

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR (tOsTeInterface,
                                               NextSortLinkNode, pLstNode);
        if (pOsTeInterface->pTeArea != pTeArea)
        {
            continue;
        }
        /* Deleting each LSA from Network LSA list */
        OSPF_TE_DYNM_SLL_SCAN (&(pOsTeInterface->type9TELsaLst), pLstNode1,
                               pTempNode, tTMO_SLL_NODE *)
        {
            pLsaInfo = OSPF_TE_GET_BASE_PTR (tOsTeLsaNode, NextSortLsaNode,
                                             pLstNode1);
            TMO_SLL_Delete (&(pOsTeInterface->type9TELsaLst),
                            &(pLsaInfo->NextSortLsaNode));
            OSPF_TE_LSA_FREE (pLsaInfo->pLsa);
            OSPF_TE_LSA_INFO_FREE (pLsaInfo);
        }
    }
}

/*****************************************************************************/
/* Function     : OspfTeLsuDeleteType10Lsas                                  */
/*                                                                           */
/* Description  : Delete Type 10 LSAs.                                       */
/*                                                                           */
/* Input        : pTeArea - Pointer to TE Area.                              */
/*                bIsTlmDeReg - If TLM deregistration event is processed     */
/*                              this variable is set to OSPF_TE_TRUE else    */
/*                              this variable is set to OSPF_TE_FALSE.       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
OspfTeLsuDeleteType10Lsas (tOsTeArea * pTeArea, BOOL1 bIsTlmDeReg)
{
    UINT4               u4HashIndex;
    tOsTeLsaNode       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pLstNode1 = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tTMO_SLL_NODE      *pTempNode1 = NULL;
    tTMO_SLL_NODE      *pLstDbNode = NULL;
    tTMO_SLL_NODE      *pTempDbNode = NULL;
    tOsTeDbNode        *pOsTeDbNode = NULL;
    tLINKSTATEID       tempLinkStateId;

    tempLinkStateId[OSPF_TE_ZERO] = OSPF_TE_ONE;
    tempLinkStateId[OSPF_TE_ONE] = OSPF_TE_ZERO;
    tempLinkStateId[OSPF_TE_TWO] = OSPF_TE_ZERO;
    tempLinkStateId[OSPF_TE_THREE] = OSPF_TE_ZERO;

    TMO_HASH_Scan_Table (pTeArea->pTeDbHashTbl, u4HashIndex)
    {
        OSPF_TE_DYNM_SLL_SCAN
            (&(pTeArea->pTeDbHashTbl)->HashList[(u4HashIndex)], pLstDbNode,
             pTempDbNode, tTMO_SLL_NODE *)
        {
            pOsTeDbNode = (tOsTeDbNode *) pLstDbNode;
            /* Deleting all LSAs in the Db Node */
            OSPF_TE_DYNM_SLL_SCAN (&(pOsTeDbNode->TeLsaLst), pLstNode,
                                   pTempNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = OSPF_TE_GET_BASE_PTR (tOsTeLsaNode,
                                                 NextDbNodeLsa, pLstNode);

                /* If TLM Deregistration event is processed, do not delete
                 * LSA's advertised by other routers. */
                if ((bIsTlmDeReg == OSPF_TE_TRUE) &&
                    (OspfTeUtilIpAddrComp (pLsaInfo->advRtrId,
                                           gOsTeContext.rtrId)
                     != OSPF_TE_EQUAL))
                {
                    continue;
                }

                /* If TLM Deregistration event is processed, do not delete
                 * Router Address TLV originated by this router. */
                if ((bIsTlmDeReg == OSPF_TE_TRUE) &&
                    (OspfTeUtilIpAddrComp (pLsaInfo->linkStateId,
                                           tempLinkStateId)
                     == OSPF_TE_EQUAL))
                {
                    continue;
                }

                TMO_SLL_Delete (&(pOsTeDbNode->TeLsaLst),
                                &(pLsaInfo->NextDbNodeLsa));
            }

            /* Check if Hash bucket is empty, if so, delete the bucket */
            if (TMO_SLL_Count (&(pOsTeDbNode->TeLsaLst)) == OSPF_TE_ZERO)
            {
                /* Delete the hash node and free its memory */
                TMO_HASH_Delete_Node (pTeArea->pTeDbHashTbl,
                                      &(pOsTeDbNode->NextTeDbNode),
                                      u4HashIndex);

                OSPF_TE_TEDB_HASH_NODE_FREE (&(pOsTeDbNode->NextTeDbNode));
            }
        }
    }

    pLstNode = NULL;
    pTempNode = NULL;

    OSPF_TE_DYNM_SLL_SCAN (&(pTeArea->type10OpqLsaLst), pLstNode,
                           pTempNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = OSPF_TE_GET_BASE_PTR (tOsTeLsaNode, NextSortLsaNode,
                                         pLstNode);

        /* If TLM Deregistration event is processed, do not delete
         * LSA's advertised by other routers. */
        if ((bIsTlmDeReg == OSPF_TE_TRUE) &&
            (OspfTeUtilIpAddrComp (pLsaInfo->advRtrId,
                                   gOsTeContext.rtrId)
             != OSPF_TE_EQUAL))
        {
            continue;
        }

        /* If TLM Deregistration event is processed, do not delete
         * Router Address TLV originated by this router. */
        if ((bIsTlmDeReg == OSPF_TE_TRUE) &&
            (OspfTeUtilIpAddrComp (pLsaInfo->linkStateId,
                                   tempLinkStateId)
             == OSPF_TE_EQUAL))
        {
            continue;
        }

        TMO_SLL_Delete (&(pTeArea->type10OpqLsaLst),
                        &(pLsaInfo->NextSortLsaNode));
        if (pLsaInfo->pLinkTlv != NULL)
        {
            OSPF_TE_DYNM_SLL_SCAN (&(pLsaInfo->pLinkTlv->ifDescrLst),
                                   pLstNode1, pTempNode1, tTMO_SLL_NODE *)
            {
                TMO_SLL_Delete (&(pLsaInfo->pLinkTlv->ifDescrLst), pLstNode1);
                OSPF_TE_IF_DESC_FREE ((tOsTeIfDescNode *) pLstNode1);
            }
            OSPF_TE_LINK_TLV_FREE (pLsaInfo->pLinkTlv);
        }
        OSPF_TE_LSA_FREE (pLsaInfo->pLsa);
        OSPF_TE_LSA_INFO_FREE (pLsaInfo);
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostelsu.c                      */
/*-----------------------------------------------------------------------*/
