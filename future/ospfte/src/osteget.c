/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: osteget.c,v 1.4 2010/11/24 07:26:37 siva Exp $
*
* Description:  This file has the routines to find TE link information 
*               from TE database. 
*********************************************************************/

#include "osteinc.h"

/*****************************************************************************/
/* Function     : OspfTeCspfSearchDatabase                                   */
/*                                                                           */
/* Description  : This function scan the hash bucket and get the node for    */
/*                the speicified RouterId, if the passed lsa type is type 10 */
/*                lsa. if lsa type is network lsa, this will scan network    */
/*                lsa list and get the lsa node.                             */
/*                                                                           */
/* Input        : u1LsaType  - Type 10 or Network LSA                        */
/*                u4RouterId - Router ID or DR Ip address                    */
/*                pArea      - Points to area node                           */
/*                                                                           */
/* Output       : Node                                                       */
/*                                                                           */
/* Returns      : pPtr - pOsTeDbNode or pOsTeLsaNode                         */
/*                NULL                                                       */
/*****************************************************************************/

PUBLIC UINT1       *
OspfTeCspfSearchDatabase (UINT1 u1LsaType, UINT4 u4RouterId, tOsTeArea * pArea)
{

    tOsTeDbNode        *pOsTeDbNode = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;
    UINT4               u4HashValue;
    tRouterId           routerId;
    tTMO_SLL_NODE      *pLstNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfSearchDatabase\n");

    OSPF_TE_CRU_BMC_DWTOPDU (routerId, u4RouterId);
    /* If lsa Type is type 10 */
    if (u1LsaType == OSPF_TE_TYPE10_LSA)
    {
        u4HashValue = OspfTeLsuLsaHashFunc (&(routerId));

        /* Scan the hash bucket to get all the TYPE 10 LSAs */
        TMO_HASH_Scan_Bucket (pArea->pTeDbHashTbl,
                              u4HashValue, pOsTeDbNode, tOsTeDbNode *)
        {
            if (OspfTeUtilIpAddrComp (pOsTeDbNode->rtrId, routerId) ==
                OSPF_TE_EQUAL)
            {
                return (UINT1 *) pOsTeDbNode;
            }
        }
    }
    /* If lsa Type is network lsa */

    else if (u1LsaType == OSPF_TE_NETWORK_LSA)
    {
        /* Scan Network Lsa List */
        TMO_SLL_Scan ((&pArea->networkLsaLst), pLstNode, tTMO_SLL_NODE *)
        {
            pOsTeLsaNode =
                OSPF_TE_GET_BASE_PTR (tOsTeLsaNode, NextSortLsaNode, pLstNode);
            if (OspfTeUtilIpAddrComp (pOsTeLsaNode->linkStateId, routerId) ==
                OSPF_TE_EQUAL)
            {
                return (UINT1 *) pOsTeLsaNode;
            }
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfSearchDatabase\n");

    return NULL;
}

/*****************************************************************************/
/* Function     : OspfTeCspfGetLinksFromLsa                                  */
/*                                                                           */
/* Description  : This function scans the specified LSA List and get links   */
/*                advertised by the Router.                                  */
/*                                                                           */
/* Input        : pNewVertex                                                 */
/*                                                                           */
/* Output       : pLinkNode                                                  */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfGetLinksFromLsa (tOsTeCandteNode * pNewVertex,
                           tOsTeLinkNode * pLinkNode)
{
    UINT1              *pCurrPtr = NULL;
    tIPADDR             linkId;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfGetLinksFromLsa\n");

    /* If vertex type is router */
    if (pNewVertex->u1VertexType == OSPF_TE_VERT_ROUTER)
    {
        if (pLinkNode->pOsTeLsaNode == NULL)
        {
            /* Extracting the First node */
            pLinkNode->pOsTeLsaNode =
                (tOsTeLsaNode *) TMO_SLL_First (&pNewVertex->pTeDbNode->
                                                TeLsaLst);
        }
        else
        {
            /* Extracting the Next node */
            pLinkNode->pOsTeLsaNode =
                (tOsTeLsaNode *) TMO_SLL_Next (&pNewVertex->pTeDbNode->TeLsaLst,
                                               &(pLinkNode->
                                                 pOsTeLsaNode->NextDbNodeLsa));
        }
        if (pLinkNode->pOsTeLsaNode == NULL)
        {
            return OSPF_TE_FAILURE;
        }
        if (pLinkNode->pOsTeLsaNode->pLinkTlv != NULL)
        {
            pLinkNode->u1LinkType =
                pLinkNode->pOsTeLsaNode->pLinkTlv->u1LinkType;
        }

    }
    /* If vertex type is Network */
    else if (pNewVertex->u1VertexType == OSPF_TE_VERT_NETWORK)
    {
        pCurrPtr = pNewVertex->pTeLsaNode->pLsa +
            OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN + pLinkNode->u1Offset;

        if (pCurrPtr >= pNewVertex->pTeLsaNode->pLsa +
            pNewVertex->pTeLsaNode->u2LsaLen)
        {
            pLinkNode->u1Offset = OSPF_TE_NULL;
            return OSPF_TE_FAILURE;
        }

        LGETSTR (pCurrPtr, &linkId, MAX_IP_ADDR_LEN);
        pLinkNode->u4LinkId = OSPF_TE_CRU_BMC_DWFROMPDU (linkId);
        pLinkNode->u4TeMetric = OSPF_TE_NULL;
        pLinkNode->u1LinkType = OSPF_TE_MULTIACCESS;
        pLinkNode->u1Offset = (UINT1) (pLinkNode->u1Offset + MAX_IP_ADDR_LEN);
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfGetLinksFromLsa\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfGetCandteNode                                    */
/*                                                                           */
/* Description  : This function  allocates memory for a candidate node and   */
/*                initialises the VertexId, Vertex Type fields.              */
/*                                                                           */
/* Input        : u4VertexId   - RouterId                                    */
/*                u1VertexType - VERT_ROUTER or VERT_NETWORK                 */
/*                                                                           */
/* Output       : pTeCandteNode - Holds the candidate node                   */
/*                                                                           */
/* Returns      : tOsTeCandteNode *                                          */
/*                NULL                                                       */
/*****************************************************************************/

PUBLIC tOsTeCandteNode *
OspfTeCspfGetCandteNode (UINT4 u4VertexId, UINT1 u1VertexType)
{
    tOsTeCandteNode    *pCandteNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfGetCandteNode\n");

    if (OSPF_TE_CANDTE_NODE_ALLOC (pCandteNode, tOsTeCandteNode) == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC,
                     "Memory ALLOC for Candidate node failed\n");
        return NULL;
    }

    pCandteNode->u4VertexId = u4VertexId;
    pCandteNode->u4TeMetric = OSPF_TE_NULL;
    pCandteNode->u4ParentId = OSPF_TE_NULL;
    pCandteNode->u1VertexType = u1VertexType;

    /* Initilise Path List */
    TMO_SLL_Init (&(pCandteNode->pathLst));

    if (u1VertexType == OSPF_TE_VERT_ROUTER)
    {
        pCandteNode->pTeDbNode = NULL;
    }
    else
    {
        pCandteNode->pTeLsaNode = NULL;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfGetCandteNode\n");

    return pCandteNode;
}

/*****************************************************************************/
/* Function     : OspfTeCspfSearchCandteLst                                  */
/*                                                                           */
/* Description  : This function searches the Candidate List and returns the  */
/*                particular candidate node.                                 */
/*                                                                           */
/* Input        : u4VertexId - Vertex Id                                     */
/*              : u1VertType - Vertex Type                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tOsTeCandteNode * - pointer to candidate node , if found.  */
/*              : NULL , otherwise.                                          */
/*****************************************************************************/
PUBLIC tOsTeCandteNode *
OspfTeCspfSearchCandteLst (UINT4 u4VertexId, UINT1 u1VertType)
{
    tOsTeCandteNode    *pCandteNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfSearchCandteLst\n");

    TMO_HASH_Scan_Bucket (gOsTeContext.pTeCandteLst,
                          OspfTeCspfHashFunc (u1VertType, u4VertexId),
                          pCandteNode, tOsTeCandteNode *)
    {
        if ((pCandteNode->u4VertexId == u4VertexId) &&
            (pCandteNode->u1VertexType == u1VertType))
        {
            return pCandteNode;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfSearchCandteLst\n");

    return NULL;
}

/*****************************************************************************/
/* Function     : OspfTeCspfHashFunc                                         */
/*                                                                           */
/* Description  : This function gives the hash value of the key.             */
/*                                                                           */
/* Input        : u1LsaType  - LSA Type                                      */
/*                u4AdvRtrId - Advertiser RouterId                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hashed value of the Key.                                   */
/*****************************************************************************/

PUBLIC UINT4
OspfTeCspfHashFunc (UINT1 u1LsaType, UINT4 u4AdvRtrId)
{
    UINT1               au1Buf[OSPF_TE_CSPF_HASH_KEY_SIZE];

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfHashFunc\n");
    au1Buf[OSPF_TE_NULL] = u1LsaType;
    OSPF_TE_MEMCPY (au1Buf + OSPF_TE_ONE, (UINT1 *) &u4AdvRtrId,
                    sizeof (UINT4));

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfHashFunc\n");
    return (OspfTeUtilHashGetValue (OSPF_TE_CAND_HASH_TBL_SIZE, au1Buf,
                                    OSPF_TE_CSPF_HASH_KEY_SIZE));
}

/*****************************************************************************/
/* Function     : OspfTeCspfSearchLsaLinks                                   */
/*                                                                           */
/* Description  : This function checks whether link back to parent is present*/
/*                or not.                                                    */
/*                                                                           */
/* Input        : pPtr   - Pointer to pOsTeDbNode or pOsTeLsaNode.           */
/*                pCandteNode - pointer to Candidate node.                   */
/*                u1VertType - Vertex Type.                                  */
/*                pLinkNode - pointer to link node.                          */
/*                pArea - pointer to area.                                   */
/*                pCspfReq - pointer to cspf request.                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/
PUBLIC INT4
OspfTeCspfSearchLsaLinks (UINT1 *pPtr, tOsTeCandteNode * pCandteNode,
                          UINT1 u1VertType, tOsTeLinkNode * pLinkNode,
                          tOsTeArea * pArea, tOsTeCspfReq * pCspfReq)
{
    tOsTeLsaNode       *pOsTeLsaNode = NULL;
    tOsTeDbNode        *pOsTeDbNode = NULL;
    tOsTeLsaNode       *pOsTeLsaNode1 = NULL;
    tOsTeDbNode        *pOsTeDbNode1 = NULL;
    UINT1              *pPtr1 = NULL;
    UINT1              *pCurrPtr = NULL;
    tIPADDR             linkId;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfSearchLsaLinks\n");

    if ((u1VertType == OSPF_TE_VERT_ROUTER) && (pCandteNode->u1VertexType ==
                                                OSPF_TE_VERT_ROUTER))
    {
        pOsTeDbNode = (tOsTeDbNode *) (VOID *) pPtr;
        /* Scan Te Lsa List  */
        TMO_SLL_Scan (&pOsTeDbNode->TeLsaLst, pOsTeLsaNode, tOsTeLsaNode *)
        {
            if (pOsTeLsaNode->pLinkTlv == NULL)
            {
                continue;
            }
            if ((pOsTeLsaNode->pLinkTlv->u4LinkId ==
                 pCandteNode->u4VertexId)
                && (pOsTeLsaNode->pLinkTlv->u4RemoteIpAddr ==
                    pLinkNode->pOsTeLsaNode->pLinkTlv->u4LocalIpAddr)
                && (pOsTeLsaNode->pLinkTlv->u4RemoteIdentifier ==
                    pLinkNode->pOsTeLsaNode->pLinkTlv->u4LocalIdentifier))
            {
                if (OspfTeCspfChkConstrain
                    (pCspfReq,
                     pOsTeLsaNode->pLinkTlv,
                     pLinkNode->pOsTeLsaNode->pLinkTlv->u4LinkId)
                    == OSPF_TE_FAILURE)
                {
                    return OSPF_TE_FAILURE;
                }
                return OSPF_TE_SUCCESS;
            }
        }
        return OSPF_TE_FAILURE;
    }

    else if ((u1VertType == OSPF_TE_VERT_ROUTER) &&
             (pCandteNode->u1VertexType == OSPF_TE_VERT_NETWORK))
    {
        pOsTeDbNode = (tOsTeDbNode *) (VOID *) pPtr;
        TMO_SLL_Scan (&pOsTeDbNode->TeLsaLst, pOsTeLsaNode, tOsTeLsaNode *)
        {
            if (pOsTeLsaNode->pLinkTlv == NULL)
            {
                continue;
            }
            if (pOsTeLsaNode->pLinkTlv->u4LinkId == pCandteNode->u4VertexId)
            {
                break;
            }
        }
        if (pOsTeLsaNode == NULL)
        {
            return OSPF_TE_FAILURE;
        }

        if ((pPtr1 = OspfTeCspfSearchDatabase (OSPF_TE_TYPE10_LSA,
                                               pCandteNode->u4ParentId,
                                               pArea)) == NULL)
        {
            return OSPF_TE_FAILURE;
        }
        pOsTeDbNode1 = (tOsTeDbNode *) (VOID *) pPtr1;
        TMO_SLL_Scan (&pOsTeDbNode1->TeLsaLst, pOsTeLsaNode1, tOsTeLsaNode *)
        {
            if (pOsTeLsaNode1->pLinkTlv == NULL)
            {
                continue;
            }
            if (pOsTeLsaNode1->pLinkTlv->u4LinkId == pCandteNode->u4VertexId)
            {
                break;
            }
        }
        if (pOsTeLsaNode1 == NULL)
        {
            return OSPF_TE_FAILURE;
        }
        if (OspfTeCspfChkConstrain
            (pCspfReq, pOsTeLsaNode->pLinkTlv,
             pLinkNode->u4LinkId) == OSPF_TE_FAILURE)
        {
            return OSPF_TE_FAILURE;
        }
        return OSPF_TE_SUCCESS;
    }
    else
    {
        pOsTeLsaNode = (tOsTeLsaNode *) (VOID *) pPtr;

        pCurrPtr = pOsTeLsaNode->pLsa +
            OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN;

        while (pCurrPtr < (pOsTeLsaNode->pLsa + pOsTeLsaNode->u2LsaLen))
        {
            LGETSTR (pCurrPtr, &linkId, MAX_IP_ADDR_LEN);
            if (OSPF_TE_CRU_BMC_DWFROMPDU (linkId) == pCandteNode->u4VertexId)
            {
                return OSPF_TE_SUCCESS;
            }
        }

    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfSearchLsaLinks\n");

    return OSPF_TE_FAILURE;
}

/*****************************************************************************/
/* Function     : OspfTeCspfGetNearestCandteNode                             */
/*                                                                           */
/* Description  : This function gets the candidate nearest to the root from  */
/*                the Candidate List.                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tOsTeCandteNode * - Points to the nearest candidate node   */
/*                NULL                                                       */
/*****************************************************************************/

PUBLIC tOsTeCandteNode *
OspfTeCspfGetNearestCandteNode (VOID)
{
    tOsTeCandteNode    *pLstCandteNode = NULL;
    tOsTeCandteNode    *pNearestCandteNode = NULL;
    UINT4               u4HashKey;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfGetNearestCandteNode\n");

    TMO_HASH_Scan_Table (gOsTeContext.pTeCandteLst, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (gOsTeContext.pTeCandteLst,
                              u4HashKey, pLstCandteNode, tOsTeCandteNode *)
        {
            if (pNearestCandteNode == NULL)
            {
                pNearestCandteNode = pLstCandteNode;
            }
            else
            {
                if (pLstCandteNode->u4TeMetric < pNearestCandteNode->u4TeMetric)
                {
                    pNearestCandteNode = pLstCandteNode;
                }
                else if ((pLstCandteNode->u4TeMetric ==
                          pNearestCandteNode->u4TeMetric) &&
                         (pNearestCandteNode->u1VertexType ==
                          OSPF_TE_VERT_ROUTER))
                {
                    pNearestCandteNode = pLstCandteNode;
                }

            }                    /* End of check if Nearest Node present */

        }                        /* End of scan of Candidate bucket  */

    }                            /* End of scan of Candidate Hash Table */

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfGetNearestCandteNode\n");

    return pNearestCandteNode;
}

/*****************************************************************************/
/* Function     : OspfTeCspfSearchUniLinks                                   */
/*                                                                           */
/* Description  : This function checks whether Swithching capability match   */
/*                or not.                                                    */
/*                                                                           */
/* Input        : pPtr   - Pointer to pOsTeDbNode or pOsTeLsaNode.           */
/*                pCandteNode - pointer to Candidate node.                   */
/*                u1VertType - Vertex Type.                                  */
/*                pLinkNode - pointer to link node.                          */
/*                pArea - pointer to area.                                   */
/*                pCspfReq - pointer to cspf request.                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfSearchUniLinks (UINT1 *pPtr, tOsTeCandteNode * pCandteNode,
                          UINT1 u1VertType, tOsTeLinkNode * pLinkNode,
                          tOsTeArea * pArea, tOsTeCspfReq * pCspfReq)
{
    tOsTeLsaNode       *pOsTeLsaNode = NULL;
    tOsTeDbNode        *pOsTeDbNode = NULL;
    tOsTeLsaNode       *pOsTeLsaNode1 = NULL;
    tOsTeDbNode        *pOsTeDbNode1 = NULL;
    UINT1              *pPtr1 = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfSearchUniLinks\n");

    if ((u1VertType == OSPF_TE_VERT_ROUTER) && (pCandteNode->u1VertexType ==
                                                OSPF_TE_VERT_ROUTER))
    {
        pOsTeDbNode = (tOsTeDbNode *) (VOID *) pPtr;
        /* Scan Te Lsa List  */
        TMO_SLL_Scan (&pOsTeDbNode->TeLsaLst, pOsTeLsaNode, tOsTeLsaNode *)
        {
            if (pOsTeLsaNode->pLinkTlv == NULL)
            {
                continue;
            }
            if ((pOsTeLsaNode->pLinkTlv->u4LinkId ==
                 pCandteNode->u4VertexId)
                && (pOsTeLsaNode->pLinkTlv->u4RemoteIpAddr ==
                    pLinkNode->pOsTeLsaNode->pLinkTlv->u4LocalIpAddr)
                && (pOsTeLsaNode->pLinkTlv->u4RemoteIdentifier ==
                    pLinkNode->pOsTeLsaNode->pLinkTlv->u4LocalIdentifier))
            {
                if (OspfTeCspfChkSwithingCap
                    (pLinkNode->pOsTeLsaNode->pLinkTlv,
                     pOsTeLsaNode->pLinkTlv, pCspfReq,
                     pOsTeDbNode->rtrId) == OSPF_TE_FAILURE)
                {
                    return OSPF_TE_FAILURE;
                }

                return OSPF_TE_SUCCESS;
            }
        }
        if (OspfTeCspfChkSwithingCap
            (pLinkNode->pOsTeLsaNode->pLinkTlv,
             NULL, pCspfReq, pOsTeDbNode->rtrId) == OSPF_TE_FAILURE)
        {
            return OSPF_TE_FAILURE;
        }

        return OSPF_TE_SUCCESS;
    }

    else if ((u1VertType == OSPF_TE_VERT_ROUTER) &&
             (pCandteNode->u1VertexType == OSPF_TE_VERT_NETWORK))
    {
        pOsTeDbNode = (tOsTeDbNode *) (VOID *) pPtr;
        TMO_SLL_Scan (&pOsTeDbNode->TeLsaLst, pOsTeLsaNode, tOsTeLsaNode *)
        {
            if (pOsTeLsaNode->pLinkTlv == NULL)
            {
                continue;
            }
            if (pOsTeLsaNode->pLinkTlv->u4LinkId == pCandteNode->u4VertexId)
            {
                break;
            }
        }

        if ((pPtr1 = OspfTeCspfSearchDatabase (OSPF_TE_TYPE10_LSA,
                                               pCandteNode->u4ParentId,
                                               pArea)) == NULL)
        {
            return OSPF_TE_FAILURE;
        }
        pOsTeDbNode1 = (tOsTeDbNode *) (VOID *) pPtr1;
        TMO_SLL_Scan (&pOsTeDbNode1->TeLsaLst, pOsTeLsaNode1, tOsTeLsaNode *)
        {
            if (pOsTeLsaNode1->pLinkTlv == NULL)
            {
                continue;
            }
            if (pOsTeLsaNode1->pLinkTlv->u4LinkId == pCandteNode->u4VertexId)
            {
                break;
            }
        }
        if (pOsTeLsaNode1 == NULL)
        {
            return OSPF_TE_FAILURE;
        }

        if (pOsTeLsaNode != NULL)
        {
            if (OspfTeCspfChkSwithingCap
                (pOsTeLsaNode1->pLinkTlv,
                 pOsTeLsaNode->pLinkTlv, pCspfReq,
                 pOsTeDbNode->rtrId) == OSPF_TE_FAILURE)
            {
                return OSPF_TE_FAILURE;
            }
        }
        else
        {
            if (OspfTeCspfChkSwithingCap
                (pOsTeLsaNode1->pLinkTlv,
                 NULL, pCspfReq, pOsTeDbNode->rtrId) == OSPF_TE_FAILURE)
            {
                return OSPF_TE_FAILURE;
            }
        }

        return OSPF_TE_SUCCESS;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfSearchUniLinks\n");

    return OSPF_TE_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostget.c                       */
/*-----------------------------------------------------------------------*/
