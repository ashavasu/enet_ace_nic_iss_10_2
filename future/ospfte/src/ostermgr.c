/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostermgr.c,v 1.5 2011/11/25 11:06:12 siva Exp $
 *
 * Description:  This file contains the routines for the interaction 
 *               with RM.
 * *********************************************************************/

#include "osteinc.h"
/*****************************************************************************/
/*  Function    :  OspfTeProcessRmgrMsg                                      */
/*                                                                           */
/*  Description :  This function processes the message received from Resource*/
/*                 Manager. It calls the respective function to handle the   */
/*                 Link information/Registration/Deregistration information. */
/*                                                                           */
/*  Input       :  pRmgrMsg - Holds the message from Resource Manager        */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessRmgrMsg (tRmOsTeLinkMsg * pRmgrMsg)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessRmgrMsg\n");

    switch (pRmgrMsg->u1MsgSubType)
    {
        case OSPF_TE_RM_REG:
            gOsTeContext.u1RmRegister = OSPF_TE_TRUE;
            break;
        case OSPF_TE_RM_LINK_INFO:
            /* If the RM is registered, then only process the Link 
             * information message */
            if (gOsTeContext.u1RmRegister == OSPF_TE_TRUE)
            {
                OspfTeProcessRmLinkInfo (pRmgrMsg);
            }
            break;
        case OSPF_TE_RM_DEREG:
            OspfTeProcessDeReg ();
            gOsTeContext.u1RmRegister = OSPF_TE_FALSE;
            break;
        default:
            OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Unknown Message is received "
                         "from Resource Manager\n");
            break;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessRmgrMsg\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessRmLinkInfo                                   */
/*                                                                           */
/*  Description :  This function processes the TE link information received  */
/*                 from Resource Manager.                                    */
/*                                                                           */
/*  Input       :  pRmOsTeLinkMsg - Points to the Resource Manager Message   */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessRmLinkInfo (tRmOsTeLinkMsg * pRmOsTeLinkMsg)
{
    tIPADDR             linkIpAddr;
    tOsTeInterface     *pOsTeInt = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessRmLinkInfo\n");

    OSPF_TE_CRU_BMC_DWTOPDU (linkIpAddr, pRmOsTeLinkMsg->u4LocalIpAddr);

    pOsTeInt = OspfTeFindTeInterfaceWithIndex (pRmOsTeLinkMsg->u4IfIndex);

    if (pOsTeInt == NULL)
    {
        switch (pRmOsTeLinkMsg->u1LinkStatus)
        {
            case OSPF_TE_LINK_CREATE:
                OspfTeCreateTeLink (pRmOsTeLinkMsg);
                break;
            case OSPF_TE_LINK_UPDATE:
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Received Update Link "
                             "Info message for non existing Link\n");
                break;
            case OSPF_TE_LINK_DELETE:
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Received Delete Link "
                             "Info message for non existing Link\n");
                break;
            default:
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Received Unknown Type Link "
                             "Info message for non existing Link\n");
                break;
        }
    }
    else
    {
        switch (pRmOsTeLinkMsg->u1LinkStatus)
        {
            case OSPF_TE_LINK_CREATE:
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Received Create Link "
                             "Info message for existing Link\n");
                break;
            case OSPF_TE_LINK_UPDATE:
                OspfTeUpdateTeLink (pRmOsTeLinkMsg, pOsTeInt);
                break;
            case OSPF_TE_LINK_DELETE:
                OspfTeFlushTeLink (pOsTeInt);
                break;
            default:
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Received Unknown Type Link "
                             "Info message for existing Link\n");
                break;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessRmLinkInfo\n");

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostermgr.c                     */
/*-----------------------------------------------------------------------*/
