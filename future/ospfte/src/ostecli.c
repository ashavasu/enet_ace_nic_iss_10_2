/******************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostecli.c,v 1.6 2013/12/09 10:53:39 siva Exp $
 *
 * Description: Contains Action routines for CLI OSPFTE commands.
 * ***************************************************************************/

#ifndef __OSPFTECLI_C__
#define __OSPFTECLI_C__

#include "osteinc.h"
#include "fsotelw.h"
#include "ostecli.h"
#include "fsotewr.h"
#include "fsotecli.h"

PRIVATE INT4        OspfTeCliSetAdminStatus (tCliHandle, INT4);
PRIVATE INT4        OspfTeCliSetTrace (tCliHandle CliHandle,
                                       UINT4 u4Type, INT4 i4Val);
PRIVATE INT4        OspfTeCliShowOspfTe (tCliHandle CliHandle,
                                         UINT4 u4advRtrId, UINT4 u4LsaType);
PRIVATE INT4        OspfTeCliShowType9Lsa (tCliHandle CliHandle,
                                           UINT4 u4AdvRtrId);

PRIVATE INT4        OspfTeCliShowType10Lsa (tCliHandle CliHandle,
                                            UINT4 u4AdvRtrId);

PRIVATE VOID        OspfTeCliPrintSwiCapType (tCliHandle CliHandle,
                                              UINT1 u1TlvVal1);

/**************************************************************************/
/*  Function Name   : cli_process_ospfte_cmd                              */
/*                                                                        */
/*  Description     : Protocol CLI message handler function               */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u4Command - Command identifier                      */
/*                    ... -Variable command argument list                 */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
cli_process_ospfte_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *apu4args[OSPFTE_CLI_MAX_ARGS];
    UINT4               u4AdvRtrId = OSPF_TE_ZERO;
    INT1                argno = OSPF_TE_ZERO;
    INT4                i4RetVal = CLI_FAILURE;

    va_start (ap, u4Command);
    for (argno = OSPF_TE_ZERO; argno < OSPFTE_CLI_MAX_ARGS; argno++)
    {
        apu4args[argno] = va_arg (ap, UINT4 *);
    }
    va_end (ap);

    CliRegisterLock (CliHandle, OspfTeLock, OspfTeUnLock);

    OspfTeLock ();

    switch (u4Command)
    {
        case OSPFTE_CLI_ADMIN_UP:
            i4RetVal = OspfTeCliSetAdminStatus (CliHandle, OSPF_TE_ENABLED);
            break;

        case OSPFTE_CLI_ADMIN_DOWN:
            i4RetVal = OspfTeCliSetAdminStatus (CliHandle, OSPF_TE_DISABLED);
            break;

            /* Intentional Fall through */
        case OSPFTE_CLI_DBG:
        case OSPFTE_CLI_NO_DBG:
            i4RetVal = OspfTeCliSetTrace (CliHandle, u4Command,
                                          CLI_PTR_TO_I4 (apu4args
                                                         [OSPF_TE_ONE]));
            break;
        case OSPFTE_CLI_SHOW:
            if (apu4args[OSPF_TE_ONE] != NULL)
            {
                u4AdvRtrId = *(apu4args[OSPF_TE_ONE]);
            }
            i4RetVal = OspfTeCliShowOspfTe (CliHandle, u4AdvRtrId,
                                            (UINT4) CLI_PTR_TO_I4 (apu4args
                                                                   [OSPF_TE_TWO]));
        default:
            break;
    }

    OspfTeUnLock ();
    CliUnRegisterLock (CliHandle);
    return i4RetVal;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfTeCliSetTrace                                  */
/*                                                                           */
/*     DESCRIPTION      : This function configures OSPF TE Debug level       */
/*                                                                           */
/*     INPUT            : CliHandle  - CliContext ID                         */
/*                        u4Type - Type of the command                       */
/*                        i4Val - OSPF TE Debug Level                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
OspfTeCliSetTrace (tCliHandle CliHandle, UINT4 u4Type, INT4 i4Val)
{
    UINT4               u4ErrorCode = OSPF_TE_ZERO;
    INT4                i4Level = OSPF_TE_ZERO;

    /*  Getting existing trace level */
    nmhGetFutOspfTeTraceLevel (&i4Level);

    if (u4Type == OSPFTE_CLI_DBG)
    {
        i4Val = i4Val | i4Level;
    }
    else
    {
        i4Val = i4Val & i4Level;
    }

    /* Testing the trace value befor setting it */
    if (nmhTestv2FutOspfTeTraceLevel (&u4ErrorCode, i4Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to Set Debug Level Specified\r\n");
        return CLI_FAILURE;
    }

    nmhSetFutOspfTeTraceLevel (i4Val);

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : OspfTeCliSetAdminStatus                           */
/*                                                                          */
/*     DESCRIPTION      : This function is to enable OSPF-TE                */
/*                                                                          */
/*     INPUT            : CliHandle  - CliContext ID                        */
/*                        UINT4      - Type of the command                  */
/*                                                                          */
/*     OUTPUT           : None                                              */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/****************************************************************************/
INT4
OspfTeCliSetAdminStatus (tCliHandle CliHandle, INT4 i4AdminStatus)
{
    UINT4               u4ErrorCode = OSPF_TE_ZERO;

    /* test for valid value */
    if (nmhTestv2FutOspfTeAdminStatus (&u4ErrorCode, i4AdminStatus) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to Set OSPFTE AdminStatus\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFutOspfTeAdminStatus (i4AdminStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to Set OSPFTE AdminStatus\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : OspeTeShowRunningConfig                             */
/*                                                                        */
/*  Description     : This function  displays the OSPF  Configuration     */
/*                                                                        */
/*  Input(s)        : CliHandle                                           */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
OspfTeShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4OspfTeAdminStatus;

    OspfTeLock ();
    /* Getting admin status of OSPF-TE */
    if (nmhGetFutOspfTeAdminStatus (&i4OspfTeAdminStatus) == SNMP_FAILURE)
    {
        OspfTeUnLock ();
        return CLI_FAILURE;
    }
    /* Checking admin status with its value other than default value  */
    if (i4OspfTeAdminStatus == OSPF_TE_ENABLED)
    {
        CliPrintf (CliHandle, "\r\nmpls traffic-eng\n");
    }
    OspfTeUnLock ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  IssOspfteCliShowDebugging                                 */
/*                                                                           */
/*  Description :  This function Shows the current debug                     */
/*                 configuration of OSPF-TE                                  */
/*                                                                           */
/*  Input       :  tCliHandle CliHandle                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
VOID
IssOspfteCliShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = OSPF_TE_ZERO;

    OspfTeLock ();

    nmhGetFutOspfTeTraceLevel (&i4DbgLevel);

    if (i4DbgLevel == 0)
    {
        OspfTeUnLock ();
        return;
    }
    CliPrintf (CliHandle, "\rOSPF-TE: ");
    if ((i4DbgLevel & OSPF_TE_CRITICAL_TRC))
    {
        CliPrintf (CliHandle, "\r\n OSPF-TE Critical debugging is on");
    }
    if ((i4DbgLevel & OSPF_TE_FN_ENTRY))
    {
        CliPrintf (CliHandle, "\r\n OSPF-TE Function entry debugging is on");
    }
    if ((i4DbgLevel & OSPF_TE_FN_EXIT))
    {
        CliPrintf (CliHandle, "\r\n OSPF-TE Function exit debugging is on");
    }
    if ((i4DbgLevel & OSPF_TE_FAILURE_TRC))
    {
        CliPrintf (CliHandle, "\r\n OSPF_TE Failure debugging is on");
    }
    if ((i4DbgLevel & OSPF_TE_RESOURCE_TRC))
    {
        CliPrintf (CliHandle, "\r\n OSPF-TE Resource failure debugging is on");
    }
    if ((i4DbgLevel & OSPF_TE_CONTROL_PLANE_TRC))
    {
        CliPrintf (CliHandle, "\r\n OSPF-TE Control plane debugging is on");
    }
    if ((i4DbgLevel & OSPF_TE_CSPF_TRC))
    {
        CliPrintf (CliHandle, "\r\n OSPF-TE Cspf debugging is on");
    }
    OspfTeUnLock ();
    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : OspfTeCliShowOspfTe                                */
/*                                                                           */
/*     DESCRIPTION      : This function Displays the OSPFTE Database         */
/*                                                                           */
/*     INPUT            : CliHandle  - CliContext ID                         */
/*                        u4AdvRtrId - Advertising Router ID                 */
/*                        u4LsaType  - LSA Type (Type9/Type10)               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
OspfTeCliShowOspfTe (tCliHandle CliHandle, UINT4 u4AdvRtrId, UINT4 u4LsaType)
{

    if (u4LsaType != 10)
    {
        OspfTeCliShowType9Lsa (CliHandle, u4AdvRtrId);
    }
    if (u4LsaType != 9)
    {
        OspfTeCliShowType10Lsa (CliHandle, u4AdvRtrId);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfTeCliShowType9Lsa                              */
/*                                                                           */
/*     DESCRIPTION      : This function Displays the Opaque-link             */
/*                        LSAs in OSPFTE Database                            */
/*                                                                           */
/*     INPUT            : CliHandle  - CliContext ID                         */
/*                        u4AdvRtrId - Advertising Router ID                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
OspfTeCliShowType9Lsa (tCliHandle CliHandle, UINT4 u4AdvRtrId)
{
    static UINT1        au1LsaLength[28];
    tSNMP_OCTET_STRING_TYPE lsdbAdvt;

    UINT4               u4LsdbIfIp = OSPF_TE_ZERO;
    UINT4               u4LsdpLsId = OSPF_TE_ZERO;
    UINT4               u4LsdbRtrId = OSPF_TE_ZERO;
    UINT4               u4AdvRtr = OSPF_TE_ZERO;
    UINT4               u4SeqNo = OSPF_TE_ZERO;
    UINT4               u4LinkLocalId = OSPF_TE_ZERO;
    INT4                i4LsdbIfIndex = OSPF_TE_ZERO;
    UINT2               u2ChkSum = OSPF_TE_ZERO;
    UINT2               u2Len = OSPF_TE_ZERO;
    UINT1               u1Flag = CLI_FAILURE;
    UINT2               u2LsAge = OSPF_TE_ZERO;
    UINT1              *plsdbpkt = NULL;
    CHR1               *pu1String = NULL;
    UINT1               u1LsType = OSPF_TE_ZERO;

    lsdbAdvt.pu1_OctetList = au1LsaLength;
    lsdbAdvt.i4_Length = 28;
    u1Flag = (UINT1) nmhGetFirstIndexFutOspfTeType9LsdbTable (&u4LsdbIfIp,
                                                              &i4LsdbIfIndex,
                                                              &u4LsdpLsId,
                                                              &u4LsdbRtrId);
    if (u1Flag == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if (u4AdvRtrId != OSPF_TE_ZERO && u4AdvRtrId != u4LsdbRtrId)
        {
            continue;
        }
        u1Flag = (UINT1) nmhGetFutOspfTeType9LsdbAdvertisement (u4LsdbIfIp,
                                                                i4LsdbIfIndex,
                                                                u4LsdpLsId,
                                                                u4LsdbRtrId,
                                                                &lsdbAdvt);
        if (u1Flag == SNMP_FAILURE)
        {
            break;
        }

        CliPrintf (CliHandle, "\t\t Opaque Link Local State\r\n");
        CliPrintf (CliHandle, "\t\t -----------------------\r\n");

        plsdbpkt = lsdbAdvt.pu1_OctetList;
        /* Copy the LS Age which is of two bytes */
        MEMCPY (&u2LsAge, plsdbpkt, sizeof (UINT2));
        plsdbpkt += sizeof (UINT2);
        u2LsAge = OSIX_NTOHS (u2LsAge);
        /* Skip the Options Field */
        plsdbpkt += sizeof (UINT1);
        /* Copy the LS Type which is of one bytes */
        MEMCPY (&u1LsType, plsdbpkt, sizeof (UINT1));
        plsdbpkt += sizeof (UINT1);
        /* Skip the Opaque Type which is of one byte */
        plsdbpkt += sizeof (UINT1);
        /* Skip the Opaque ID which is of three bytes */
        plsdbpkt += 3;
        /* Copy the Advertising Router */
        MEMCPY (&u4AdvRtr, plsdbpkt, sizeof (UINT4));
        plsdbpkt += sizeof (UINT4);
        u4AdvRtr = OSIX_NTOHL (u4AdvRtr);
        /* Copy the LS sequence number */
        MEMCPY (&u4SeqNo, plsdbpkt, sizeof (UINT4));
        plsdbpkt += sizeof (UINT4);
        u4SeqNo = OSIX_NTOHL (u4SeqNo);
        /* Copy the LS LS checksum */
        MEMCPY (&u2ChkSum, plsdbpkt, sizeof (UINT2));
        plsdbpkt += sizeof (UINT2);
        u2ChkSum = OSIX_NTOHS (u2ChkSum);
        /* Copy the length */
        MEMCPY (&u2Len, plsdbpkt, sizeof (UINT2));
        plsdbpkt += sizeof (UINT2);
        u2Len = OSIX_NTOHS (u2Len);
        /* Skip the TLV Type and Length of Type 9 TLV */
        plsdbpkt += sizeof (UINT4);
        /* Copy the Link Local Identifier */
        MEMCPY (&u4LinkLocalId, plsdbpkt, sizeof (UINT4));
        u4LinkLocalId = OSIX_NTOHL (u4LinkLocalId);
        plsdbpkt += sizeof (UINT4);
        CliPrintf (CliHandle, "LS age                : %d\r\n", u2LsAge);
        CliPrintf (CliHandle, "Options               : "
                   "(No ToS Capability, DC)\r\n");
        CliPrintf (CliHandle, "LS Type               : Type - %d\r\n",
                   u1LsType);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AdvRtr);
        CliPrintf (CliHandle, "Advertising Router    : %s\r\n", pu1String);
        CliPrintf (CliHandle, "LS Seq Number         : 0x%x\r\n", u4SeqNo);
        CliPrintf (CliHandle, "Checksum              : 0x%x\r\n", u2ChkSum);
        CliPrintf (CliHandle, "Length                : %d\r\n", u2Len);
        CliPrintf (CliHandle, "Link Local Identifier : %d\r\n", u4LinkLocalId);

    }
    while (nmhGetNextIndexFutOspfTeType9LsdbTable (u4LsdbIfIp, &u4LsdbIfIp,
                                                   i4LsdbIfIndex,
                                                   &i4LsdbIfIndex,
                                                   u4LsdpLsId, &u4LsdpLsId,
                                                   u4LsdbRtrId, &u4LsdbRtrId));
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : OspfTeCliShowType10Lsa                             */
/*                                                                           */
/*     DESCRIPTION      : This function Displays the Opaque-area             */
/*                        LSAs in OSPFTE Database                            */
/*                                                                           */
/*     INPUT            : CliHandle  - CliContext ID                         */
/*                        u4AdvRtrId - Advertising Router ID                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
OspfTeCliShowType10Lsa (tCliHandle CliHandle, UINT4 u4AdvRtrId)
{
    static UINT1        au1LsaLength[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE lsdbAdvt;
    tBandWidth          bandWidth;
    UINT1               au1EncType[11][15] = { {"Packet"}, {"Ethernet"},
    {"ANSI/ETSI PDH"}, {"Reserved"},
    {"SONET"}, {"Reserved"},
    {"Digital Wrapper"},
    {"Lambda"}, {"Fiber"},
    {"Reserved"}, {"Fiber Channel"}
    };
    UINT1               au1ProcType[6][15] = { {"extraTraffic"},
    {"unprotected"},
    {"shared"}, {"dedicated1For1"},
    {"dedicated1Plus1"},
    {"enhanced"}
    };

    UINT4               u4LsdbAreaId = OSPF_TE_ZERO;
    UINT4               u4LsdpLsId = OSPF_TE_ZERO;
    UINT4               u4LsdbRtrId = OSPF_TE_ZERO;
    UINT4               u4AdvRtr = OSPF_TE_ZERO;
    UINT4               u4LinkStateId = OSPF_TE_ZERO;
    UINT4               u2EntLen = OSPF_TE_ZERO;
    UINT4               u4Count = OSPF_TE_ZERO;
    UINT4               u4SeqNo = OSPF_TE_ZERO;
    UINT4               u4TlvVal = OSPF_TE_ZERO;
    INT4                i4LsdbType = OSPF_TE_ZERO;
    UINT2               u2LsAge = OSPF_TE_ZERO;
    UINT2               u2Priority = OSPF_TE_ZERO;
    UINT2               u2LsType = OSPF_TE_ZERO;
    UINT2               u2ChkSum = OSPF_TE_ZERO;
    UINT2               u2Len = OSPF_TE_ZERO;
    UINT2               u2TlvType = OSPF_TE_ZERO;
    UINT2               u2TlvLen = OSPF_TE_ZERO;
    UINT2               u2Count = OSPF_TE_ZERO;
    UINT2               u2TlvVal = OSPF_TE_ZERO;
    UINT1               u1Flag = SNMP_FAILURE;
    UINT1              *plsdbpkt = NULL;
    CHR1               *pu1String = NULL;
    UINT1               u1LsType = OSPF_TE_ZERO;
    UINT1               u1TlvVal1 = OSPF_TE_ZERO;

    lsdbAdvt.pu1_OctetList = au1LsaLength;
    lsdbAdvt.i4_Length = SNMP_MAX_OCTETSTRING_SIZE;
    u1Flag = (UINT1) nmhGetFirstIndexFutOspfTeLsdbTable (&u4LsdbAreaId,
                                                         &i4LsdbType,
                                                         &u4LsdpLsId,
                                                         &u4LsdbRtrId);
    if (u1Flag == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if ((u4AdvRtrId != OSPF_TE_ZERO && u4AdvRtrId != u4LsdbRtrId) ||
            (i4LsdbType != 10))
        {
            continue;
        }
        u1Flag = (UINT1) nmhGetFutOspfTeLsdbAdvertisement (u4LsdbAreaId,
                                                           i4LsdbType,
                                                           u4LsdpLsId,
                                                           u4LsdbRtrId,
                                                           &lsdbAdvt);
        if (u1Flag == SNMP_FAILURE)
        {
            break;
        }

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LsdbAreaId);
        CliPrintf (CliHandle, "\t\t Opaque Area State (%s) \r\n", pu1String);
        CliPrintf (CliHandle, "\t\t ----------------------------\r\n");

        plsdbpkt = lsdbAdvt.pu1_OctetList;
        /* Copy the LS Age which is of two bytes */
        MEMCPY (&u2LsAge, plsdbpkt, sizeof (UINT2));
        plsdbpkt += sizeof (UINT2);
        u2LsAge = OSIX_NTOHS (u2LsAge);
        /* Skip the Options Field */
        plsdbpkt += sizeof (UINT1);
        /* Copy the LS Type which is of one bytes */
        MEMCPY (&u1LsType, plsdbpkt, sizeof (UINT1));
        plsdbpkt += sizeof (UINT1);
        /* Copy the Link State Id which is of four bytes 
         * First byte -> Always holds one, representing TE-LSA 
         * remaining bytes -> Instance */
        MEMCPY (&u4LinkStateId, plsdbpkt, sizeof (UINT4));
        plsdbpkt += sizeof (UINT4);
        u4LinkStateId = OSIX_NTOHL (u4LinkStateId);
        /* Copy the Advertising Router */
        MEMCPY (&u4AdvRtr, plsdbpkt, sizeof (UINT4));
        plsdbpkt += sizeof (UINT4);
        u4AdvRtr = OSIX_NTOHL (u4AdvRtr);
        /* Copy the LS sequence number */
        MEMCPY (&u4SeqNo, plsdbpkt, sizeof (UINT4));
        plsdbpkt += sizeof (UINT4);
        u4SeqNo = OSIX_NTOHL (u4SeqNo);
        /* Copy the LS LS checksum */
        MEMCPY (&u2ChkSum, plsdbpkt, sizeof (UINT2));
        plsdbpkt += sizeof (UINT2);
        u2ChkSum = OSIX_NTOHS (u2ChkSum);
        /* Copy the length */
        MEMCPY (&u2Len, plsdbpkt, sizeof (UINT2));
        plsdbpkt += sizeof (UINT2);
        u2Len = OSIX_NTOHS (u2Len);
        CliPrintf (CliHandle, "LS age                : %d\r\n", u2LsAge);
        CliPrintf (CliHandle, "Options               : "
                   "(No ToS Capability, DC)\r\n");
        CliPrintf (CliHandle, "LS Type               : Type - %d\r\n",
                   u1LsType);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LinkStateId);
        CliPrintf (CliHandle, "Link State ID         : %s\r\n", pu1String);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4AdvRtr);
        CliPrintf (CliHandle, "Advertising Router    : %s\r\n", pu1String);
        CliPrintf (CliHandle, "LS Seq Number         : 0x%x\r\n", u4SeqNo);
        CliPrintf (CliHandle, "Checksum              : 0x%x\r\n", u2ChkSum);
        CliPrintf (CliHandle, "Length                : %d\r\n", u2Len);

        /* Getting the Ls-Type (one -> Router TLV; two -> Link TLV) */
        MEMCPY (&u2LsType, plsdbpkt, sizeof (UINT2));
        plsdbpkt += sizeof (UINT2);
        u2LsType = OSIX_NTOHS (u2LsType);
        if (u2LsType == OSPF_TE_ONE)
        {
            continue;
        }
        /* Copying the entire TLV Length */
        MEMCPY (&u2EntLen, plsdbpkt, sizeof (UINT2));
        u2EntLen = OSIX_NTOHS (u2EntLen);
        plsdbpkt += sizeof (UINT2);

        /* Now we have only opaque sub tlv's. Print it one by one */
        MEMCPY (&u2TlvType, plsdbpkt, sizeof (UINT2));
        u2TlvType = OSIX_NTOHS (u2TlvType);
        plsdbpkt += sizeof (UINT2);
        u4Count = OSPF_TE_ZERO;
        do
        {
            switch (u2TlvType)
            {
                case OSPF_TE_LINKTYPE:    /* Link Type */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    /* Copy the Tlvvalue */
                    MEMCPY (&u1TlvVal1, plsdbpkt, u2TlvLen);
                    if (u1TlvVal1 == OSPF_TE_TYPE10_LINK_P2P)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n   Link connected to "
                                   "Point-to-Point network\r\n");
                    }
                    else if (u1TlvVal1 == OSPF_TE_TYPE10_LINK_MULTI)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n   Link connected to "
                                   "Multi-access network\r\n");
                    }
                    /* Move the pointer at the end of the TLV.
                     * Link Type is only a byte, but for 4 byte alignment 
                     * remaining bytes are filled with zero.
                     * so move upto there */
                    plsdbpkt += sizeof (UINT4);
                    u4Count += sizeof (UINT4);
                    break;
                case OSPF_TE_LINKID:    /* Link Id  */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    /* Copy the Tlvvalue */
                    MEMCPY (&u4TlvVal, plsdbpkt, u2TlvLen);
                    u4TlvVal = OSIX_NTOHL (u4TlvVal);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TlvVal);
                    CliPrintf (CliHandle,
                               "     Link ID            : %s\r\n", pu1String);
                    plsdbpkt += sizeof (UINT4);
                    u4Count += sizeof (UINT4);
                    break;
                case OSPF_TE_LOCALIF:    /* Local Ip */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    /* Copy the Tlvvalue */
                    for (u2Count = OSPF_TE_ZERO; u2Count < u2TlvLen;
                         u2Count = (UINT2) (u2Count + sizeof (UINT4)))
                    {
                        MEMCPY (&u4TlvVal, plsdbpkt, sizeof (UINT4));
                        plsdbpkt += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        u4TlvVal = OSIX_NTOHL (u4TlvVal);
                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TlvVal);
                        CliPrintf (CliHandle,
                                   "     Interface Address  : %s\r\n",
                                   pu1String);
                    }
                    break;
                case OSPF_TE_REMOTEIF:    /* Remote Ip */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    /* Copy the Tlvvalue */
                    for (u2Count = OSPF_TE_ZERO; u2Count < u2TlvLen;
                         u2Count = (UINT2) (u2Count + sizeof (UINT4)))
                    {
                        MEMCPY (&u4TlvVal, plsdbpkt, sizeof (UINT4));
                        plsdbpkt += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        u4TlvVal = OSIX_NTOHL (u4TlvVal);
                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TlvVal);
                        CliPrintf (CliHandle,
                                   "     Neighbor Address   : %s\r\n",
                                   pu1String);
                    }
                    break;
                case OSPF_TE_METRIC:    /* TE Metric */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    /* Copy the Tlvvalue */
                    MEMCPY (&u4TlvVal, plsdbpkt, u2TlvLen);
                    u4TlvVal = OSIX_NTOHL (u4TlvVal);
                    CliPrintf (CliHandle,
                               "     Admin Metric       : %d\r\n", u4TlvVal);
                    plsdbpkt += sizeof (UINT4);
                    u4Count += sizeof (UINT4);
                    break;
                case OSPF_TE_MAXBW:    /* Max Bw */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    /* Copy the Tlvvalue */
                    MEMCPY (&bandWidth, plsdbpkt, u2TlvLen);
                    bandWidth = OSIX_NTOHF (bandWidth);
                    CliPrintf (CliHandle,
                               "     Maximum bandwidth  : %d kbps\r\n",
                               OSPF_TE_CONVERT_BPS_TO_KBPS (bandWidth));
                    plsdbpkt += sizeof (UINT4);
                    u4Count += sizeof (UINT4);
                    break;
                case OSPF_TE_MAXRESBW:    /* Max Res Bw */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    MEMCPY (&bandWidth, plsdbpkt, u2TlvLen);
                    bandWidth = OSIX_NTOHF (bandWidth);
                    CliPrintf (CliHandle,
                               "     Maximum reservable bandwidth  : %d "
                               "kbps\r\n",
                               OSPF_TE_CONVERT_BPS_TO_KBPS (bandWidth));
                    plsdbpkt += sizeof (UINT4);
                    u4Count += sizeof (UINT4);
                    break;
                case OSPF_TE_UNRESBW:    /* Un Res Bw */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    /* Copy the Tlvvalue */
                    CliPrintf (CliHandle,
                               "     Unreserved bandwidth         \r\n");
                    CliPrintf (CliHandle,
                               "       Number of Priority         : %d\r\n",
                               OSPF_TE_MAX_PRIORITY_LVL);
                    for (u2Priority = OSPF_TE_ZERO;
                         u2Priority < OSPF_TE_MAX_PRIORITY_LVL; u2Priority++)
                    {
                        MEMCPY (&bandWidth, plsdbpkt, sizeof (FLT4));
                        bandWidth = OSIX_NTOHF (bandWidth);
                        plsdbpkt += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        CliPrintf (CliHandle,
                                   "\t Priority %d : %d kbps\t", u2Priority,
                                   OSPF_TE_CONVERT_BPS_TO_KBPS (bandWidth));
                        if (u2Priority % 2 == OSPF_TE_ONE)
                        {
                            CliPrintf (CliHandle, "\n\r");
                        }
                    }
                    break;
                case OSPF_TE_RSRCLR:    /* Resource color  */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    /* Copy the Tlvvalue */
                    MEMCPY (&u4TlvVal, plsdbpkt, u2TlvLen);
                    u4TlvVal = OSIX_NTOHL (u4TlvVal);
                    CliPrintf (CliHandle, "     Affinity Bit : 0x%x\r\n",
                               u4TlvVal);
                    plsdbpkt += sizeof (UINT4);
                    u4Count += sizeof (UINT4);
                    break;

                case OSPF_TE_IDENTIFIERS:    /* Local/Remote Identifiers */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    MEMCPY (&u4TlvVal, plsdbpkt, sizeof (UINT4));
                    u4TlvVal = OSIX_NTOHL (u4TlvVal);
                    plsdbpkt += sizeof (UINT4);
                    u4Count += sizeof (UINT4);
                    CliPrintf (CliHandle, "     Local Identifier   : %d\t",
                               u4TlvVal);
                    MEMCPY (&u4TlvVal, plsdbpkt, sizeof (UINT4));
                    u4TlvVal = OSIX_NTOHL (u4TlvVal);
                    CliPrintf (CliHandle, "\tRemote Identifier  : %d \r\n",
                               u4TlvVal);
                    plsdbpkt += sizeof (UINT4);
                    u4Count += sizeof (UINT4);
                    break;
                case OSPF_TE_PROTECTION_TYPE:    /* Link Protection Type */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    /* Copy the Tlvvalue */
                    MEMCPY (&u1TlvVal1, plsdbpkt, u2TlvLen);
                    if (u1TlvVal1 == OSPF_TE_ZERO)
                    {
                        break;
                    }
                    CliPrintf (CliHandle,
                               "     Link Protection    : %s \r\n",
                               au1ProcType[u1TlvVal1 - 1]);
                    /* Move the pointer at the end of the TLV */
                    /* Link Type is only a byte, but remaining three byte 
                     * should be ignored */
                    plsdbpkt += sizeof (UINT4);
                    u4Count += sizeof (UINT4);
                    break;
                case OSPF_TE_INSWCA_DESC:    /* Interface Switching Capability Descriptor */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    /* Copy Switching Capability Information - one byte */
                    MEMCPY (&u1TlvVal1, plsdbpkt, sizeof (UINT1));
                    OspfTeCliPrintSwiCapType (CliHandle, u1TlvVal1);
                    plsdbpkt += sizeof (UINT1);
                    u4Count += sizeof (UINT1);
                    /* Copy Encoding Type Information - one byte */
                    MEMCPY (&u1TlvVal1, plsdbpkt, sizeof (UINT1));
                    if (u1TlvVal1 == OSPF_TE_ZERO)
                    {
                        break;
                    }
                    CliPrintf (CliHandle,
                               "\tEncoding           : %s \r\n",
                               au1EncType[u1TlvVal1 - 1]);
                    plsdbpkt += sizeof (UINT1);
                    u4Count += sizeof (UINT1);
                    /* Next two byte is Reserved */
                    plsdbpkt += sizeof (UINT2);
                    CliPrintf (CliHandle,
                               "     Maximum LSP bandwidth       \r\n");
                    CliPrintf (CliHandle,
                               "       Number of Priority        : %d\r\n",
                               OSPF_TE_MAX_PRIORITY_LVL);
                    for (u2Priority = OSPF_TE_ZERO;
                         u2Priority < OSPF_TE_MAX_PRIORITY_LVL; u2Priority++)
                    {
                        MEMCPY (&bandWidth, plsdbpkt, sizeof (FLT4));
                        bandWidth = OSIX_NTOHF (bandWidth);
                        plsdbpkt += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        CliPrintf (CliHandle,
                                   "\t Priority %d : %d kbps\t", u2Priority,
                                   OSPF_TE_CONVERT_BPS_TO_KBPS (bandWidth));
                        if (u2Priority % 2 == OSPF_TE_ONE)
                        {
                            CliPrintf (CliHandle, "\n\r");
                        }
                    }

                    if (u1TlvVal1 == OSPF_TE_PSC_1 ||
                        u1TlvVal1 == OSPF_TE_PSC_2 ||
                        u1TlvVal1 == OSPF_TE_PSC_3 ||
                        u1TlvVal1 == OSPF_TE_PSC_4)
                    {
                        MEMCPY (&bandWidth, plsdbpkt, sizeof (FLT4));
                        bandWidth = OSIX_NTOHF (bandWidth);
                        plsdbpkt += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        CliPrintf (CliHandle,
                                   "     Min LSP Bandwidth  : %d kbps\r\n",
                                   OSPF_TE_CONVERT_BPS_TO_KBPS (bandWidth));
                        MEMCPY (&u2TlvVal, plsdbpkt, sizeof (UINT2));
                        u2TlvVal = OSIX_NTOHS (u2TlvVal);
                        /* Move to 4 bytes for padding */
                        plsdbpkt += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        CliPrintf (CliHandle,
                                   "     Interface MTU      : %d\r\n",
                                   u2TlvVal);
                    }
                    else if (u1TlvVal1 == OSPF_TE_TDM)
                    {
                        MEMCPY (&bandWidth, plsdbpkt, sizeof (FLT4));
                        bandWidth = OSIX_NTOHF (bandWidth);
                        plsdbpkt += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        CliPrintf (CliHandle,
                                   "     Min LSP Bandwidth  : %d kbps\r\n",
                                   OSPF_TE_CONVERT_BPS_TO_KBPS (bandWidth));
                        MEMCPY (&u1TlvVal1, plsdbpkt, sizeof (UINT1));
                        /* Move to 4 bytes for padding */
                        plsdbpkt += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        CliPrintf (CliHandle,
                                   "     Indication         : %d\r\n",
                                   u1TlvVal1);
                    }
                    break;
                case OSPF_TE_SRLG:    /* Shared Risk Link Group */
                    /* copy the TLV Length to u2Tlv Length */
                    MEMCPY (&u2TlvLen, plsdbpkt, sizeof (UINT2));
                    u2TlvLen = OSIX_NTOHS (u2TlvLen);
                    plsdbpkt += sizeof (UINT2);
                    u4Count += sizeof (UINT2);
                    /* Copy the Tlvvalue */
                    for (u2Count = OSPF_TE_ZERO; u2Count < u2TlvLen;
                         u2Count = (UINT2) (u2Count + sizeof (UINT4)))
                    {
                        MEMCPY (&u4TlvVal, plsdbpkt, sizeof (UINT4));
                        u4TlvVal = OSIX_NTOHL (u4TlvVal);
                        plsdbpkt += sizeof (UINT4);
                        u4Count += sizeof (UINT4);
                        CliPrintf (CliHandle,
                                   "     SRLG               : %d\r\n",
                                   u4TlvVal);
                    }
                    break;
                default:
                    break;
            }
            if (u4Count == u2EntLen)
                break;
            MEMCPY (&u2TlvType, plsdbpkt, sizeof (UINT2));
            u2TlvType = OSIX_NTOHS (u2TlvType);
            plsdbpkt += sizeof (UINT2);
            u4Count += sizeof (UINT2);
        }
        while (u4Count <= u2EntLen);

    }
    while (nmhGetNextIndexFutOspfTeLsdbTable (u4LsdbAreaId, &u4LsdbAreaId,
                                              i4LsdbType, &i4LsdbType,
                                              u4LsdpLsId, &u4LsdpLsId,
                                              u4LsdbRtrId, &u4LsdbRtrId));
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : OspfTeCliPrintSwiCapType                            */
/*                                                                        */
/*  Description     : Maps and Prints the Switching capability with the   */
/*                    given input                                         */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u1TlvVal1 - value corresponds to switching -        */
/*                    capability                                          */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : NONE                                                */
/**************************************************************************/
PRIVATE VOID
OspfTeCliPrintSwiCapType (tCliHandle CliHandle, UINT1 u1TlvVal1)
{
    switch (u1TlvVal1)
    {
        case OSPF_TE_PSC_1:
            CliPrintf (CliHandle, "     Switching Cap      : PSC-1 ");
            break;
        case OSPF_TE_PSC_2:
            CliPrintf (CliHandle, "     Switching Cap      : PSC-2 ");
            break;
        case OSPF_TE_PSC_3:
            CliPrintf (CliHandle, "     Switching Cap      : PSC-3 ");
            break;
        case OSPF_TE_PSC_4:
            CliPrintf (CliHandle, "     Switching Cap      : PSC-4 ");
            break;
        case OSPF_TE_L2SC:
            CliPrintf (CliHandle, "     Switching Cap      : L2SC ");
            break;
        case OSPF_TE_TDM:
            CliPrintf (CliHandle, "     Switching Cap      : TDM ");
            break;
        case OSPF_TE_LSC:
            CliPrintf (CliHandle, "     Switching Cap      : LSC ");
            break;
        case OSPF_TE_FSC:
            CliPrintf (CliHandle, "     Switching Cap      : FSC ");
            break;
        default:
            break;
    }

}
#endif /* __OSPFTECLI_C__ */
