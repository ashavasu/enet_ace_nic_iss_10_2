/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: ostearea.c,v 1.5 2013/12/18 12:47:59 siva Exp $
*
* Description: OSPF-TE area related functions. 
*********************************************************************/

#include "osteinc.h"

/*****************************************************************************/
/*  Function    :  OspfTeAreaCreate                                          */
/*                                                                           */
/*  Description :  This module creates an area node based on router LSA      */
/*                 received from OSPF                                        */
/*                                                                           */
/*  Input       :  u4AreaId - Holds the area identifier                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to area if the area is present                    */
/*                 NULL otherwise.                                           */
/*****************************************************************************/

PUBLIC tOsTeArea   *
OspfTeAreaCreate (UINT4 u4AreaId)
{
    tOsTeArea          *pTeArea = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeAreaCreate\n");

    /* Allocates area memory from the Memory Pool */
    if (OSPF_TE_AREA_ALLOC (pTeArea, tOsTeArea) == NULL)
    {
        OSPF_TE_TRC1 (OSPF_TE_CRITICAL_TRC,
                      "Area %x Alloc Failure\n", u4AreaId);
        return NULL;
    }

    pTeArea->u4AreaId = u4AreaId;

    OSPF_TE_TRC1 (OSPF_TE_RESOURCE_TRC, "Area %x created \n", u4AreaId);

    /* Initialise the Area node to default values */
    if (OspfTeAreaSetDefaultValues (pTeArea) == OSPF_TE_FAILURE)
    {
        OSPF_TE_TRC1 (OSPF_TE_FAILURE_TRC,
                      "Area %x Node initialization failure\n", u4AreaId);

        /* Free the Area block created */
        OSPF_TE_AREA_FREE (pTeArea);

        return NULL;
    }

    /* Add the Area node in the sorted order */
    OspfTeAreaAddToAreaLst (pTeArea);

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR (tOsTeInterface, NextSortLinkNode,
                                               pLstNode);

        if (pOsTeInterface->u1ModuleId != OSPF_TE_RMGR_MSG)
        {
            continue;
        }

        if ((pOsTeInterface->u1ChannelType == OSPF_TE_AREA_CHANNEL) &&
            (pOsTeInterface->u1LinkStatus != OSPF_TE_ACTIVE))
        {
            if (pOsTeInterface->u4AreaId == pTeArea->u4AreaId)
            {
                pOsTeInterface->pTeArea = pTeArea;
                pOsTeInterface->u1LinkStatus = OSPF_TE_ACTIVE;
                OspfTeGenerateLinkTlv (pOsTeInterface, OSPF_TE_NEW_TLV);
            }
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeAreaCreate\n");

    return pTeArea;
}

/*****************************************************************************/
/*  Function    :  OspfTeAreaSetDefaultValues                                */
/*                                                                           */
/*  Description :  This module initialises Area node with default values     */
/*                                                                           */
/*  Input       :  pTeArea - Holds Area node which has to be initialised     */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS                                           */
/*                 OSPF_TE_FAILURE                                           */
/*****************************************************************************/

PUBLIC INT4
OspfTeAreaSetDefaultValues (tOsTeArea * pTeArea)
{
    INT4                i4Status = OSPF_TE_FAILURE;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeAreaSetDefaultValues\n");

    /* Create a Hash Table to store the TE LSAs */
    pTeArea->pTeDbHashTbl = TMO_HASH_Create_Table (OSPF_TE_LSA_HASH_TBL_SIZE,
                                                   NULL, FALSE);

    if (pTeArea->pTeDbHashTbl != NULL)
    {
        TMO_SLL_Init (&(pTeArea->networkLsaLst));
        TMO_SLL_Init (&(pTeArea->type10OpqLsaLst));
        pTeArea->u4NetLsaChksumSum = OSPF_TE_ZERO;
        pTeArea->u4Type10LsaChksumSum = OSPF_TE_ZERO;
        pTeArea->pRtrLsa = NULL;

        i4Status = OSPF_TE_SUCCESS;

        OSPF_TE_TRC1 (OSPF_TE_CONTROL_PLANE_TRC, "Area %x Node initialised\n",
                      pTeArea->u4AreaId);
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeAreaSetDefaultValues\n");

    return i4Status;
}

/*****************************************************************************/
/*  Function    : OspfTeAreaAddToAreaLst                                     */
/*                                                                           */
/*  Description : This module adds the specified area to the list of areas   */
/*                maintained in the OSPF-TE Context structure. The specified */
/*                area is inserted in such a way as to maintain the sorted   */
/*                order                                                      */
/*                                                                           */
/*  Input       : pTeArea - Points to the AREA to be added to the Area List  */
/*                                                                           */
/*  Output      : None                                                       */
/*                                                                           */
/*  Returns     : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OspfTeAreaAddToAreaLst (tOsTeArea * pTeArea)
{
    tOsTeArea          *pPrevArea = NULL;
    tOsTeArea          *pLstArea = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeAreaAddToAreaLst\n");

    TMO_SLL_Scan (&(gOsTeContext.areasLst), pLstArea, tOsTeArea *)
    {
        /* Area list is maintained in ascending order */
        if (pTeArea->u4AreaId < pLstArea->u4AreaId)
        {
            break;
        }
        pPrevArea = pLstArea;
    }

    /* Insert in the area list */
    TMO_SLL_Insert (&(gOsTeContext.areasLst), &pPrevArea->NextAreaNode,
                    &pTeArea->NextAreaNode);

    OSPF_TE_TRC1 (OSPF_TE_CONTROL_PLANE_TRC,
                  "Area %x Node added to Area List in OSPF-TE context \n",
                  pTeArea->u4AreaId);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeAreaAddToAreaLst\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeAreaDelete                                          */
/*                                                                           */
/*  Description :  This module deletes an area node based on Router LSA      */
/*                 received from OSPF. If no interfaces are associated with  */
/*                 this area, Area node is deleted                           */
/*                                                                           */
/*  Input       :  pTeArea - Holds pointer to the Area block to be deleted   */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeAreaDelete (tOsTeArea * pTeArea)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeAreaDelete\n");

    if (pTeArea->pRtrLsa != NULL)
    {
        /* Free the memory for the Router LSA received from OSPF */
        OSPF_TE_LSA_FREE (pTeArea->pRtrLsa->pLsa);
        /* Free the Router LSA */
        OSPF_TE_LSA_INFO_FREE (pTeArea->pRtrLsa);
    }

    /* Delete All LSAs */
    OspfTeLsuDeleteAllLsas (pTeArea);

    /* Delete TE LSA Hash Table */
    TMO_HASH_Delete_Table (pTeArea->pTeDbHashTbl, NULL);

    /* Delink Area node from Area List in OSPF-TE context */
    TMO_SLL_Delete (&(gOsTeContext.areasLst), &(pTeArea->NextAreaNode));

    OSPF_TE_TRC1 (OSPF_TE_CONTROL_PLANE_TRC, "Area %x Deleted\n",
                  pTeArea->u4AreaId);

    /* Frees the memory into Area memory pool */
    OSPF_TE_AREA_FREE (pTeArea);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeAreaDelete\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeAreaDeleteAll                                       */
/*                                                                           */
/*  Description :  This module deletes all the areas, called during OSPF-TE  */
/*                 NOT-IN-SERVICE or when OSPF-TE is brought DOWN            */
/*                                                                           */
/*  Input       :  None                                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeAreaDeleteAll (VOID)
{
    tOsTeArea          *pTeArea = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeAreaDeleteAll\n");

    /* Deletes all the areas in the Area List */
    while ((pTeArea =
            (tOsTeArea *) TMO_SLL_First (&(gOsTeContext.areasLst))) != NULL)
    {
        OspfTeAreaDelete (pTeArea);
    }

    OSPF_TE_TRC (OSPF_TE_CONTROL_PLANE_TRC,
                 "Deleted entire area nodes in the Area List\n");

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeAreaDeleteAll\n");

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostearea.c                     */
/*-----------------------------------------------------------------------*/
