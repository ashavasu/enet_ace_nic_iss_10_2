/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: ostegen.c,v 1.3 2015/02/11 10:40:28 siva Exp $
*
* Description:  This file has the routines to generate Type9LSAs, 
*               Router Address TLV and Link TLVs.
*********************************************************************/

#include "osteinc.h"

/*****************************************************************************/
/*  Function    :  OspfTeGenerateRtrAddrTlv                                  */
/*                                                                           */
/*  Description :  This function generates Router Address TLV & posts message*/
/*                 to OSPF                                                   */
/*                                                                           */
/*  Input       :  pTeArea - Pointer to Area into which the TLV to be        */
/*                           generated                                       */
/*                 u1RtrTlvStatus - Indicates TLV status New or              */
/*                                  New Instance or Flushed                  */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS / OSPF_TE_FAILURE                         */
/*****************************************************************************/
PUBLIC INT4
OspfTeGenerateRtrAddrTlv (tOsTeArea * pTeArea, UINT1 u1RtrTlvStatus)
{
    UINT1              *pRtrAdrTlv = NULL;
    UINT1              *pCurrent = NULL;
    UINT1               au1RtrAddrTlv[OSPF_TE_RTR_ADDR_TLV_LEN];
    tOsTeOspfMsg        osTeOspfMsg;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeGenerateRtrAddrTlv\n");

    MEMSET (au1RtrAddrTlv, 0, OSPF_TE_RTR_ADDR_TLV_LEN);

    pRtrAdrTlv = au1RtrAddrTlv;

    pCurrent = pRtrAdrTlv;

    /* Filling Type value */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_RTR_ADDR_TLV);

    /* Filling Length Value */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_RTR_ADDR_LEN);

    /* Filling Value filed -- Router Id */
    LADDSTR (pCurrent, &(gOsTeContext.rtrId), MAX_IP_ADDR_LEN);

    OSPF_TE_MEMSET (osTeOspfMsg.LsId, OSPF_TE_ZERO, MAX_IP_ADDR_LEN);
    osTeOspfMsg.LsId[OSPF_TE_ZERO] = OSPF_TE_ID;

    IP_ADDR_COPY (osTeOspfMsg.AdvRtrID, gOsTeContext.rtrId);
    osTeOspfMsg.u4AreaID = pTeArea->u4AreaId;
    osTeOspfMsg.u2LSALen = (UINT2) (pCurrent - pRtrAdrTlv);
    osTeOspfMsg.u1OpqLSAType = OSPF_TE_TYPE10_LSA;
    osTeOspfMsg.u1LSAStatus = u1RtrTlvStatus;
    osTeOspfMsg.pu1LSA = pRtrAdrTlv;

    /* Sending TLV infomration to OSPF to propagate in 
     * the form Type 10 Opaque LSA */
    OspfTeSndMsgToOspf (OSPF_TE_OSPF_SEND_OPQ_LSA, &(osTeOspfMsg));

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeGenerateRtrAddrTlv\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeGenerateLinkTlv                                     */
/*                                                                           */
/*  Description :  This function generates Link TLV & posts the message      */
/*                 to OSPF                                                   */
/*                                                                           */
/*  Input       :  pTeIfNode - Holds the interface information               */
/*                 u1TlvStatus - Holds the Link Status                       */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS / OSPF_TE_FAILURE                         */
/*****************************************************************************/
PUBLIC INT4
OspfTeGenerateLinkTlv (tOsTeInterface * pTeIfNode, UINT1 u1TlvStatus)
{
    UINT1              *pLinkTlv = NULL;
    UINT1              *pCurrent = NULL;
    UINT2               u2TLVLength;
    UINT2               u2IfIndex = 0;
    tOsTeOspfMsg        osTeOspfMsg;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeGenerateLinkTlv\n");

    if (OSPF_TE_LINK_TLV_SIZE_ALLOC (pLinkTlv, UINT1) == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Failed to allocate Memory "
                     "for LINK TLV\n");
        return OSPF_TE_FAILURE;
    }

    pCurrent = pLinkTlv;

    u2TLVLength = OspfTeConstructLinkTlv (pTeIfNode, pCurrent);

    osTeOspfMsg.LsId[OSPF_TE_ZERO] = OSPF_TE_ID;
    osTeOspfMsg.LsId[OSPF_TE_ONE] = OSPF_TE_ZERO;
    u2IfIndex = OSIX_HTONS((UINT2) pTeIfNode->u4IfIndex);
    MEMCPY(&osTeOspfMsg.LsId[OSPF_TE_TWO],
           &u2IfIndex, sizeof(UINT2));

    IP_ADDR_COPY (osTeOspfMsg.AdvRtrID, gOsTeContext.rtrId);
    osTeOspfMsg.u4AreaID = pTeIfNode->pTeArea->u4AreaId;
    osTeOspfMsg.u2LSALen = u2TLVLength;
    osTeOspfMsg.u1OpqLSAType = OSPF_TE_TYPE10_LSA;
    osTeOspfMsg.u1LSAStatus = u1TlvStatus;
    osTeOspfMsg.pu1LSA = pLinkTlv;

    /* Sending TLV infomration to OSPF to propagate in 
     * the form Type 10 Opaque LSA */
    OspfTeSndMsgToOspf (OSPF_TE_OSPF_SEND_OPQ_LSA, &(osTeOspfMsg));

    OSPF_TE_LINK_TLV_SIZE_FREE (pLinkTlv);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeGenerateLinkTlv\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeGenerateType9Lsa                                    */
/*                                                                           */
/*  Description :  This function gives the indication to OSPF to generate    */
/*                 the Type 9 LSA, with TE information                       */
/*                                                                           */
/*  Input       :  pTeIfNode - Points to the TE interface                    */
/*                 u1Status  - Status of the Type 9 LSA                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                           */
/*****************************************************************************/
PUBLIC INT4
OspfTeGenerateType9Lsa (tOsTeInterface * pOsTeInt, UINT1 u1Status)
{
    UINT1              *pType9Tlv = NULL;
    UINT1              *pCurrent = NULL;
    UINT1               au1Type9Tlv[OSPF_TE_TYPE9_LSA_TLV_SIZE];
    tOsTeOspfMsg        osTeOspfMsg;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeGenerateType9Lsa\n");

    MEMSET (au1Type9Tlv, 0, OSPF_TE_TYPE9_LSA_TLV_SIZE);

    pType9Tlv = au1Type9Tlv;

    pCurrent = pType9Tlv;

    /* Filling Type value */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_TYPE9_LSA_TLV_TYPE);

    /* Filling Length Value */
    LADD2BYTE (pCurrent, (UINT2) OSPF_TE_TYPE9_LSA_TLV_LENGTH);

    /* Filling Value filed --  Link Local Identifier value */
    LADD4BYTE (pCurrent, pOsTeInt->u4LocalIdentifier);

    OSPF_TE_MEMSET (osTeOspfMsg.LsId, OSPF_TE_ZERO, MAX_IP_ADDR_LEN);
    osTeOspfMsg.LsId[OSPF_TE_ZERO] = OSPF_TE_ID;
    IP_ADDR_COPY (osTeOspfMsg.AdvRtrID, gOsTeContext.rtrId);
    osTeOspfMsg.u4IfaceID = OSPF_TE_ZERO;
    osTeOspfMsg.u4AddrlessIf = pOsTeInt->u4AddrlessIf;
    osTeOspfMsg.u4AreaID = pOsTeInt->pTeArea->u4AreaId;
    osTeOspfMsg.u2LSALen = (UINT2) (pCurrent - pType9Tlv);
    osTeOspfMsg.u1OpqLSAType = OSPF_TE_TYPE9_LSA;
    osTeOspfMsg.u1LSAStatus = u1Status;
    osTeOspfMsg.pu1LSA = pType9Tlv;

    /* Sending TLV infomration to OSPF to propagate in 
     * the form Type 9 Opaque LSA */
    OspfTeSndMsgToOspf (OSPF_TE_OSPF_SEND_OPQ_LSA, &(osTeOspfMsg));

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeGenerateType9Lsa\n");

    return OSPF_TE_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostegen.c                      */
/*-----------------------------------------------------------------------*/
