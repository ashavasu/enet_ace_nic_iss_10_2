/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: fsotelw.c,v 1.7 2014/02/14 14:07:37 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

# include  "osteinc.h"
# include  "fsotelw.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfTeAdminStatus
 Input       :  The Indices

                The Object 
                retValFutOspfTeAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeAdminStatus (INT4 *pi4RetValFutOspfTeAdminStatus)
{
    *pi4RetValFutOspfTeAdminStatus = (INT4) gOsTeContext.u1TeAdminStat;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeTraceLevel
 Input       :  The Indices

                The Object 
                retValFutOspfTeTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeTraceLevel (INT4 *pi4RetValFutOspfTeTraceLevel)
{
    *pi4RetValFutOspfTeTraceLevel = (INT4) gOsTeContext.u4TeTrcValue;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeCspfRunCnt
 Input       :  The Indices

                The Object 
                retValFutOspfTeCspfRunCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeCspfRunCnt (UINT4 *pu4RetValFutOspfTeCspfRunCnt)
{
    *pu4RetValFutOspfTeCspfRunCnt = gOsTeContext.u4CspfRunCnt;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOspfTeAdminStatus
 Input       :  The Indices

                The Object 
                setValFutOspfTeAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfTeAdminStatus (INT4 i4SetValFutOspfTeAdminStatus)
{
    INT4                i4Status = OSPF_TE_SUCCESS;

    if (gOsTeContext.u1TeAdminStat != (UINT1) i4SetValFutOspfTeAdminStatus)
    {
        if (i4SetValFutOspfTeAdminStatus == OSPF_TE_ENABLED)
        {
            i4Status = OspfTeAdminUpHdlr ();
        }
        else
        {
            i4Status = OspfTeAdminDnHdlr ();
        }
    }
    if (i4Status == OSPF_TE_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfTeTraceLevel
 Input       :  The Indices

                The Object 
                setValFutOspfTeTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfTeTraceLevel (INT4 i4SetValFutOspfTeTraceLevel)
{
    gOsTeContext.u4TeTrcValue = (UINT4) i4SetValFutOspfTeTraceLevel;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutOspfTeAdminStatus
 Input       :  The Indices

                The Object 
                testValFutOspfTeAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfTeAdminStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFutOspfTeAdminStatus)
{
    if (IS_VALID_TE_STATUS_VALUE (i4TestValFutOspfTeAdminStatus))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfTeTraceLevel
 Input       :  The Indices

                The Object 
                testValFutOspfTeTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfTeTraceLevel (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFutOspfTeTraceLevel)
{
    UNUSED_PARAM (i4TestValFutOspfTeTraceLevel);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FutOspfTeAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfTeAdminStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfTeTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfTeTraceLevel (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfTeLsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfTeLsdbTable
 Input       :  The Indices
                FutOspfTeLsdbAreaId
                FutOspfTeLsdbType
                FutOspfTeLsdbLsid
                FutOspfTeLsdbRouterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfTeLsdbTable (UINT4 u4FutOspfTeLsdbAreaId,
                                            INT4 i4FutOspfTeLsdbType,
                                            UINT4 u4FutOspfTeLsdbLsid,
                                            UINT4 u4FutOspfTeLsdbRouterId)
{
    tIPADDR             routerId;
    tLINKSTATEID        linkStateId;
    tOsTeArea          *pOsTeArea = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (routerId, u4FutOspfTeLsdbRouterId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, u4FutOspfTeLsdbLsid);

    if ((pOsTeArea = OspfTeUtilFindArea (u4FutOspfTeLsdbAreaId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (OspfTeLsuSearchDatabase ((UINT1) i4FutOspfTeLsdbType,
                                 &linkStateId, &routerId,
                                 (UINT1 *) pOsTeArea) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfTeLsdbTable
 Input       :  The Indices
                FutOspfTeLsdbAreaId
                FutOspfTeLsdbType
                FutOspfTeLsdbLsid
                FutOspfTeLsdbRouterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfTeLsdbTable (UINT4 *pu4FutOspfTeLsdbAreaId,
                                    INT4 *pi4FutOspfTeLsdbType,
                                    UINT4 *pu4FutOspfTeLsdbLsid,
                                    UINT4 *pu4FutOspfTeLsdbRouterId)
{
    tOsTeArea          *pOsTeArea = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;
    tTMO_SLL           *pLsaLst = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    INT1                i1LsaType;
    tIPADDR             routerId;
    tIPADDR             linkStateId;

    TMO_SLL_Scan (&(gOsTeContext.areasLst), pOsTeArea, tOsTeArea *)
    {
        for (i1LsaType = OSPF_TE_ROUTER_LSA; i1LsaType <= OSPF_TE_TYPE10_LSA;
             i1LsaType++)
        {
            switch (i1LsaType)
            {
                case OSPF_TE_ROUTER_LSA:
                    pOsTeLsaNode = pOsTeArea->pRtrLsa;
                    break;
                case OSPF_TE_NETWORK_LSA:
                    pLsaLst = &(pOsTeArea->networkLsaLst);
                    break;
                case OSPF_TE_TYPE10_LSA:
                    pLsaLst = &(pOsTeArea->type10OpqLsaLst);
                    break;
                default:
                    pLsaLst = NULL;
                    pOsTeLsaNode = NULL;
                    break;
            }

            if (pOsTeLsaNode == NULL)
            {
                if (pLsaLst != NULL)
                {
                    if ((pLstNode = TMO_SLL_First (pLsaLst)) != NULL)
                    {
                        pOsTeLsaNode = OSPF_TE_GET_BASE_PTR
                            (tOsTeLsaNode, NextSortLsaNode, pLstNode);
                    }
                }
            }
            if (pOsTeLsaNode != NULL)
            {
                IP_ADDR_COPY (routerId, pOsTeLsaNode->advRtrId);
                IP_ADDR_COPY (linkStateId, pOsTeLsaNode->linkStateId);
                *pu4FutOspfTeLsdbRouterId =
                    OSPF_TE_CRU_BMC_DWFROMPDU (routerId);
                *pu4FutOspfTeLsdbLsid = OSPF_TE_CRU_BMC_DWFROMPDU (linkStateId);
                *pu4FutOspfTeLsdbAreaId = pOsTeArea->u4AreaId;
                *pi4FutOspfTeLsdbType = (INT4) pOsTeLsaNode->u1LsaType;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfTeLsdbTable
 Input       :  The Indices
                FutOspfTeLsdbAreaId
                nextFutOspfTeLsdbAreaId
                FutOspfTeLsdbType
                nextFutOspfTeLsdbType
                FutOspfTeLsdbLsid
                nextFutOspfTeLsdbLsid
                FutOspfTeLsdbRouterId
                nextFutOspfTeLsdbRouterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfTeLsdbTable (UINT4 u4FutOspfTeLsdbAreaId,
                                   UINT4 *pu4NextFutOspfTeLsdbAreaId,
                                   INT4 i4FutOspfTeLsdbType,
                                   INT4 *pi4NextFutOspfTeLsdbType,
                                   UINT4 u4FutOspfTeLsdbLsid,
                                   UINT4 *pu4NextFutOspfTeLsdbLsid,
                                   UINT4 u4FutOspfTeLsdbRouterId,
                                   UINT4 *pu4NextFutOspfTeLsdbRouterId)
{
    tOsTeArea          *pOsTeArea = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;
    tTMO_SLL           *pLsaLst = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    INT1                i1LsaType;
    tIPADDR             routerId;
    tIPADDR             linkStateId;

    OSPF_TE_CRU_BMC_DWTOPDU (routerId, u4FutOspfTeLsdbRouterId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, u4FutOspfTeLsdbLsid);

    TMO_SLL_Scan (&(gOsTeContext.areasLst), pOsTeArea, tOsTeArea *)
    {
        if (pOsTeArea->u4AreaId < u4FutOspfTeLsdbAreaId)
        {
            continue;
        }
        for (i1LsaType = OSPF_TE_ROUTER_LSA; i1LsaType <= OSPF_TE_TYPE10_LSA;
             i1LsaType++)
        {
            if ((i1LsaType < i4FutOspfTeLsdbType) &&
                (pOsTeArea->u4AreaId == u4FutOspfTeLsdbAreaId))
            {
                continue;
            }
            switch (i1LsaType)
            {
                case OSPF_TE_ROUTER_LSA:
                    pOsTeLsaNode = pOsTeArea->pRtrLsa;
                    break;
                case OSPF_TE_NETWORK_LSA:
                    pOsTeLsaNode = NULL;
                    pLsaLst = &(pOsTeArea->networkLsaLst);
                    break;
                case OSPF_TE_TYPE10_LSA:
                    pLsaLst = &(pOsTeArea->type10OpqLsaLst);
                    break;
                default:
                    pLsaLst = NULL;
                    pOsTeLsaNode = NULL;
                    break;
            }

            if (pOsTeLsaNode == NULL)
            {
                if (pLsaLst != NULL)
                {
                    TMO_SLL_Scan (pLsaLst, pLstNode, tTMO_SLL_NODE *)
                    {
                        pOsTeLsaNode = OSPF_TE_GET_BASE_PTR
                            (tOsTeLsaNode, NextSortLsaNode, pLstNode);
                        if ((OspfTeUtilLsaIdComp ((UINT1) i1LsaType,
                                                  pOsTeLsaNode->linkStateId,
                                                  pOsTeLsaNode->advRtrId,
                                                  (UINT1) i4FutOspfTeLsdbType,
                                                  linkStateId, routerId)
                             == OSPF_TE_GREATER))
                        {
                            IP_ADDR_COPY (routerId, pOsTeLsaNode->advRtrId);
                            IP_ADDR_COPY (linkStateId,
                                          pOsTeLsaNode->linkStateId);
                            *pu4NextFutOspfTeLsdbRouterId =
                                OSPF_TE_CRU_BMC_DWFROMPDU (routerId);
                            *pu4NextFutOspfTeLsdbLsid =
                                OSPF_TE_CRU_BMC_DWFROMPDU (linkStateId);
                            *pu4NextFutOspfTeLsdbAreaId = pOsTeArea->u4AreaId;
                            *pi4NextFutOspfTeLsdbType =
                                (INT4) pOsTeLsaNode->u1LsaType;
                            return SNMP_SUCCESS;
                        }
                    }
                }
            }
            else if ((pOsTeLsaNode != NULL) &&
                     (pOsTeArea->u4AreaId > u4FutOspfTeLsdbAreaId))
            {
                IP_ADDR_COPY (routerId, pOsTeLsaNode->advRtrId);
                IP_ADDR_COPY (linkStateId, pOsTeLsaNode->linkStateId);
                *pu4NextFutOspfTeLsdbRouterId =
                    OSPF_TE_CRU_BMC_DWFROMPDU (routerId);
                *pu4NextFutOspfTeLsdbLsid =
                    OSPF_TE_CRU_BMC_DWFROMPDU (linkStateId);
                *pu4NextFutOspfTeLsdbAreaId = pOsTeArea->u4AreaId;
                *pi4NextFutOspfTeLsdbType = (INT4) pOsTeLsaNode->u1LsaType;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfTeLsdbChecksum
 Input       :  The Indices
                FutOspfTeLsdbAreaId
                FutOspfTeLsdbType
                FutOspfTeLsdbLsid
                FutOspfTeLsdbRouterId

                The Object 
                retValFutOspfTeLsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeLsdbChecksum (UINT4 u4FutOspfTeLsdbAreaId,
                             INT4 i4FutOspfTeLsdbType,
                             UINT4 u4FutOspfTeLsdbLsid,
                             UINT4 u4FutOspfTeLsdbRouterId,
                             INT4 *pi4RetValFutOspfTeLsdbChecksum)
{
    tIPADDR             routerId;
    tLINKSTATEID        linkStateId;
    tOsTeArea          *pOsTeArea = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (routerId, u4FutOspfTeLsdbRouterId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, u4FutOspfTeLsdbLsid);

    if ((pOsTeArea = OspfTeUtilFindArea (u4FutOspfTeLsdbAreaId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pOsTeLsaNode = OspfTeLsuSearchDatabase ((UINT1) i4FutOspfTeLsdbType,
                                                 &linkStateId, &routerId,
                                                 (UINT1 *) pOsTeArea)) == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFutOspfTeLsdbChecksum = pOsTeLsaNode->u2LsaChksum;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeLsdbAdvertisement
 Input       :  The Indices
                FutOspfTeLsdbAreaId
                FutOspfTeLsdbType
                FutOspfTeLsdbLsid
                FutOspfTeLsdbRouterId

                The Object 
                retValFutOspfTeLsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeLsdbAdvertisement (UINT4 u4FutOspfTeLsdbAreaId,
                                  INT4 i4FutOspfTeLsdbType,
                                  UINT4 u4FutOspfTeLsdbLsid,
                                  UINT4 u4FutOspfTeLsdbRouterId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFutOspfTeLsdbAdvertisement)
{
    tIPADDR             routerId;
    tLINKSTATEID        linkStateId;
    tOsTeArea          *pOsTeArea = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (routerId, u4FutOspfTeLsdbRouterId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, u4FutOspfTeLsdbLsid);

    if ((pOsTeArea = OspfTeUtilFindArea (u4FutOspfTeLsdbAreaId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pOsTeLsaNode = OspfTeLsuSearchDatabase ((UINT1) i4FutOspfTeLsdbType,
                                                 &linkStateId, &routerId,
                                                 (UINT1 *) pOsTeArea)) == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPF_TE_MEMCPY (pRetValFutOspfTeLsdbAdvertisement->
                    pu1_OctetList, pOsTeLsaNode->pLsa, pOsTeLsaNode->u2LsaLen);
    pRetValFutOspfTeLsdbAdvertisement->i4_Length =
        (INT4) pOsTeLsaNode->u2LsaLen;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfTeType9LsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfTeType9LsdbTable
 Input       :  The Indices
                FutOspfTeType9LsdbIfIpAddress
                FutOspfTeType9LsdbIfIndex
                FutOspfTeType9LsdbLsid
                FutOspfTeType9LsdbRouterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfTeType9LsdbTable (UINT4
                                                 u4FutOspfTeType9LsdbIfIpAddress,
                                                 INT4
                                                 i4FutOspfTeType9LsdbIfIndex,
                                                 UINT4 u4FutOspfTeType9LsdbLsid,
                                                 UINT4
                                                 u4FutOspfTeType9LsdbRouterId)
{
    tIPADDR             ifIpAddr;
    tIPADDR             routerId;
    tLINKSTATEID        linkStateId;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeType9LsdbIfIpAddress);
    OSPF_TE_CRU_BMC_DWTOPDU (routerId, u4FutOspfTeType9LsdbRouterId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, u4FutOspfTeType9LsdbLsid);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeType9LsdbIfIndex)) != NULL)
    {
        TMO_SLL_Scan (&(pOsTeInterface->type9TELsaLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pOsTeLsaNode =
                OSPF_TE_GET_BASE_PTR (tOsTeLsaNode, NextSortLsaNode, pLstNode);

            if ((OspfTeUtilIpAddrComp (pOsTeLsaNode->linkStateId, linkStateId)
                 == OSPF_TE_EQUAL)
                && (OspfTeUtilIpAddrComp (pOsTeLsaNode->advRtrId, routerId) ==
                    OSPF_TE_EQUAL))
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfTeType9LsdbTable
 Input       :  The Indices
                FutOspfTeType9LsdbIfIpAddress
                FutOspfTeType9LsdbIfIndex
                FutOspfTeType9LsdbLsid
                FutOspfTeType9LsdbRouterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfTeType9LsdbTable (UINT4
                                         *pu4FutOspfTeType9LsdbIfIpAddress,
                                         INT4 *pi4FutOspfTeType9LsdbIfIndex,
                                         UINT4 *pu4FutOspfTeType9LsdbLsid,
                                         UINT4 *pu4FutOspfTeType9LsdbRouterId)
{
    tIPADDR             ifIpAddr;
    tLINKSTATEID        linkStateId;
    tIPADDR             routerId;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pLstNode1 = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR
            (tOsTeInterface, NextSortLinkNode, pLstNode);

        if ((pLstNode1 =
             TMO_SLL_First (&(pOsTeInterface->type9TELsaLst))) != NULL)
        {
            pOsTeLsaNode = OSPF_TE_GET_BASE_PTR
                (tOsTeLsaNode, NextSortLsaNode, pLstNode1);

            IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
            IP_ADDR_COPY (routerId, pOsTeLsaNode->advRtrId);
            IP_ADDR_COPY (linkStateId, pOsTeLsaNode->linkStateId);
            *pi4FutOspfTeType9LsdbIfIndex = (INT4) pOsTeInterface->u4AddrlessIf;
            *pu4FutOspfTeType9LsdbIfIpAddress =
                OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
            *pu4FutOspfTeType9LsdbRouterId =
                OSPF_TE_CRU_BMC_DWFROMPDU (routerId);
            *pu4FutOspfTeType9LsdbLsid =
                OSPF_TE_CRU_BMC_DWFROMPDU (linkStateId);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfTeType9LsdbTable
 Input       :  The Indices
                FutOspfTeType9LsdbIfIpAddress
                nextFutOspfTeType9LsdbIfIpAddress
                FutOspfTeType9LsdbIfIndex
                nextFutOspfTeType9LsdbIfIndex
                FutOspfTeType9LsdbLsid
                nextFutOspfTeType9LsdbLsid
                FutOspfTeType9LsdbRouterId
                nextFutOspfTeType9LsdbRouterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfTeType9LsdbTable (UINT4 u4FutOspfTeType9LsdbIfIpAddress,
                                        UINT4
                                        *pu4NextFutOspfTeType9LsdbIfIpAddress,
                                        INT4 i4FutOspfTeType9LsdbIfIndex,
                                        INT4 *pi4NextFutOspfTeType9LsdbIfIndex,
                                        UINT4 u4FutOspfTeType9LsdbLsid,
                                        UINT4 *pu4NextFutOspfTeType9LsdbLsid,
                                        UINT4 u4FutOspfTeType9LsdbRouterId,
                                        UINT4
                                        *pu4NextFutOspfTeType9LsdbRouterId)
{
    tIPADDR             currIfIpAddr;
    tIPADDR             routerId;
    tIPADDR             linkStateId;
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pLstNode1 = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;
    UINT1               u1Flag = OSPF_TE_FALSE;
    UINT1               u1NextTeList = OSPF_TE_FALSE;
    UINT4               u4Temp = OSPF_TE_ZERO;

    OSPF_TE_CRU_BMC_DWTOPDU (currIfIpAddr, u4FutOspfTeType9LsdbIfIpAddress);
    OSPF_TE_CRU_BMC_DWTOPDU (routerId, u4FutOspfTeType9LsdbRouterId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, u4FutOspfTeType9LsdbLsid);

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR
            (tOsTeInterface, NextSortLinkNode, pLstNode);

        if ((u1NextTeList == OSPF_TE_TRUE) && (u1Flag == OSPF_TE_TRUE))
        {
            OSPF_TE_CRU_BMC_DWTOPDU (routerId, u4Temp);
            OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, u4Temp);
        }

        switch (OspfTeUtilIpAddrComp
                ((currIfIpAddr), (pOsTeInterface->localIpAddr)))
        {
            case OSPF_TE_GREATER:
                continue;

            case OSPF_TE_EQUAL:
                if (((UINT4) i4FutOspfTeType9LsdbIfIndex >
                     pOsTeInterface->u4AddrlessIf))
                {
                    continue;
                }
                TMO_SLL_Scan (&(pOsTeInterface->type9TELsaLst),
                              pLstNode1, tTMO_SLL_NODE *)
                {
                    pOsTeLsaNode = OSPF_TE_GET_BASE_PTR
                        (tOsTeLsaNode, NextSortLsaNode, pLstNode1);
                    if ((OspfTeUtilIpAddrComp
                         (pOsTeInterface->localIpAddr,
                          currIfIpAddr) == OSPF_TE_EQUAL) &&
                        (pOsTeInterface->u4AddrlessIf ==
                         (UINT4) i4FutOspfTeType9LsdbIfIndex) &&
                        (OspfTeUtilIpAddrComp
                         (pOsTeLsaNode->linkStateId,
                          linkStateId) == OSPF_TE_EQUAL)
                        &&
                        (OspfTeUtilIpAddrComp (pOsTeLsaNode->advRtrId, routerId)
                         == OSPF_TE_EQUAL))
                    {
                        u1Flag = OSPF_TE_TRUE;
                    }
                    else if (u1Flag == OSPF_TE_TRUE)
                    {
                        IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
                        IP_ADDR_COPY (routerId, pOsTeLsaNode->advRtrId);
                        IP_ADDR_COPY (linkStateId, pOsTeLsaNode->linkStateId);
                        *pi4NextFutOspfTeType9LsdbIfIndex =
                            (INT4) pOsTeInterface->u4AddrlessIf;
                        *pu4NextFutOspfTeType9LsdbIfIpAddress =
                            OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
                        *pu4NextFutOspfTeType9LsdbRouterId =
                            OSPF_TE_CRU_BMC_DWFROMPDU (routerId);
                        *pu4NextFutOspfTeType9LsdbLsid =
                            OSPF_TE_CRU_BMC_DWFROMPDU (linkStateId);
                        return SNMP_SUCCESS;
                    }
                }

                u1NextTeList = OSPF_TE_TRUE;
                continue;

            case OSPF_TE_LESS:
                if ((pLstNode1 =
                     TMO_SLL_First (&(pOsTeInterface->type9TELsaLst))) != NULL)
                {
                    pOsTeLsaNode = OSPF_TE_GET_BASE_PTR
                        (tOsTeLsaNode, NextSortLsaNode, pLstNode1);
                    IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
                    IP_ADDR_COPY (routerId, pOsTeLsaNode->advRtrId);
                    IP_ADDR_COPY (linkStateId, pOsTeLsaNode->linkStateId);
                    *pi4NextFutOspfTeType9LsdbIfIndex =
                        (INT4) pOsTeInterface->u4AddrlessIf;
                    *pu4NextFutOspfTeType9LsdbIfIpAddress =
                        OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
                    *pu4NextFutOspfTeType9LsdbRouterId =
                        OSPF_TE_CRU_BMC_DWFROMPDU (routerId);
                    *pu4NextFutOspfTeType9LsdbLsid =
                        OSPF_TE_CRU_BMC_DWFROMPDU (linkStateId);
                    return SNMP_SUCCESS;
                }
                continue;
            default:
                return SNMP_FAILURE;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfTeType9LsdbChecksum
 Input       :  The Indices
                FutOspfTeType9LsdbIfIpAddress
                FutOspfTeType9LsdbIfIndex
                FutOspfTeType9LsdbLsid
                FutOspfTeType9LsdbRouterId

                The Object 
                retValFutOspfTeType9LsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeType9LsdbChecksum (UINT4 u4FutOspfTeType9LsdbIfIpAddress,
                                  INT4 i4FutOspfTeType9LsdbIfIndex,
                                  UINT4 u4FutOspfTeType9LsdbLsid,
                                  UINT4 u4FutOspfTeType9LsdbRouterId,
                                  INT4 *pi4RetValFutOspfTeType9LsdbChecksum)
{
    tIPADDR             ifIpAddr;
    tIPADDR             routerId;
    tLINKSTATEID        linkStateId;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeType9LsdbIfIpAddress);
    OSPF_TE_CRU_BMC_DWTOPDU (routerId, u4FutOspfTeType9LsdbRouterId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, u4FutOspfTeType9LsdbLsid);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeType9LsdbIfIndex)) != NULL)
    {
        TMO_SLL_Scan (&(pOsTeInterface->type9TELsaLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pOsTeLsaNode =
                OSPF_TE_GET_BASE_PTR (tOsTeLsaNode, NextSortLsaNode, pLstNode);

            if ((OspfTeUtilIpAddrComp (pOsTeLsaNode->linkStateId, linkStateId)
                 == OSPF_TE_EQUAL)
                && (OspfTeUtilIpAddrComp (pOsTeLsaNode->advRtrId, routerId) ==
                    OSPF_TE_EQUAL))
            {
                *pi4RetValFutOspfTeType9LsdbChecksum =
                    (INT4) pOsTeLsaNode->u2LsaChksum;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeType9LsdbAdvertisement
 Input       :  The Indices
                FutOspfTeType9LsdbIfIpAddress
                FutOspfTeType9LsdbIfIndex
                FutOspfTeType9LsdbLsid
                FutOspfTeType9LsdbRouterId

                The Object 
                retValFutOspfTeType9LsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeType9LsdbAdvertisement (UINT4
                                       u4FutOspfTeType9LsdbIfIpAddress,
                                       INT4 i4FutOspfTeType9LsdbIfIndex,
                                       UINT4 u4FutOspfTeType9LsdbLsid,
                                       UINT4
                                       u4FutOspfTeType9LsdbRouterId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFutOspfTeType9LsdbAdvertisement)
{
    tIPADDR             ifIpAddr;
    tIPADDR             routerId;
    tLINKSTATEID        linkStateId;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeType9LsdbIfIpAddress);
    OSPF_TE_CRU_BMC_DWTOPDU (routerId, u4FutOspfTeType9LsdbRouterId);
    OSPF_TE_CRU_BMC_DWTOPDU (linkStateId, u4FutOspfTeType9LsdbLsid);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeType9LsdbIfIndex)) != NULL)
    {
        TMO_SLL_Scan (&(pOsTeInterface->type9TELsaLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pOsTeLsaNode =
                OSPF_TE_GET_BASE_PTR (tOsTeLsaNode, NextSortLsaNode, pLstNode);

            if ((OspfTeUtilIpAddrComp (pOsTeLsaNode->linkStateId, linkStateId)
                 == OSPF_TE_EQUAL)
                && (OspfTeUtilIpAddrComp (pOsTeLsaNode->advRtrId, routerId) ==
                    OSPF_TE_EQUAL))
            {
                OSPF_TE_MEMCPY (pRetValFutOspfTeType9LsdbAdvertisement->
                                pu1_OctetList, pOsTeLsaNode->pLsa,
                                pOsTeLsaNode->u2LsaLen);
                pRetValFutOspfTeType9LsdbAdvertisement->i4_Length =
                    (INT4) pOsTeLsaNode->u2LsaLen;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutOspfTeAreaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfTeAreaTable
 Input       :  The Indices
                FutOspfTeAreaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfTeAreaTable (UINT4 u4FutOspfTeAreaId)
{
    if (OspfTeUtilFindArea (u4FutOspfTeAreaId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfTeAreaTable
 Input       :  The Indices
                FutOspfTeAreaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfTeAreaTable (UINT4 *pu4FutOspfTeAreaId)
{
    tOsTeArea          *pOsTeArea = NULL;
    if ((pOsTeArea = (tOsTeArea *) TMO_SLL_First
         (&(gOsTeContext.areasLst))) != NULL)
    {
        *pu4FutOspfTeAreaId = pOsTeArea->u4AreaId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfTeAreaTable
 Input       :  The Indices
                FutOspfTeAreaId
                nextFutOspfTeAreaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfTeAreaTable (UINT4 u4FutOspfTeAreaId,
                                   UINT4 *pu4NextFutOspfTeAreaId)
{
    tOsTeArea          *pOsTeArea = NULL;
    UINT1               u1Flag = OSPF_TE_FALSE;

    TMO_SLL_Scan (&(gOsTeContext.areasLst), pOsTeArea, tOsTeArea *)
    {
        if (pOsTeArea->u4AreaId == u4FutOspfTeAreaId)
        {
            u1Flag = OSPF_TE_TRUE;
        }
        else if (u1Flag == OSPF_TE_TRUE)
        {
            *pu4NextFutOspfTeAreaId = pOsTeArea->u4AreaId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfTeAreaLsaCount
 Input       :  The Indices
                FutOspfTeAreaId

                The Object 
                retValFutOspfTeAreaLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeAreaLsaCount (UINT4 u4FutOspfTeAreaId,
                             INT4 *pi4RetValFutOspfTeAreaLsaCount)
{
    tOsTeArea          *pOsTeArea = NULL;

    if ((pOsTeArea = OspfTeUtilFindArea (u4FutOspfTeAreaId)) != NULL)
    {
        *pi4RetValFutOspfTeAreaLsaCount =
            (INT4) (TMO_SLL_Count (&pOsTeArea->type10OpqLsaLst) +
                    TMO_SLL_Count (&pOsTeArea->networkLsaLst) +
                    ((pOsTeArea->pRtrLsa !=
                      NULL) ? OSPF_TE_ONE : OSPF_TE_NULL));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeType10AreaCksumSum
 Input       :  The Indices
                FutOspfTeAreaId

                The Object 
                retValFutOspfTeType10AreaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeType10AreaCksumSum (UINT4 u4FutOspfTeAreaId,
                                   INT4 *pi4RetValFutOspfTeType10AreaCksumSum)
{
    tOsTeArea          *pOsTeArea = NULL;

    if ((pOsTeArea = OspfTeUtilFindArea (u4FutOspfTeAreaId)) != NULL)
    {
        *pi4RetValFutOspfTeType10AreaCksumSum =
            (INT4) pOsTeArea->u4Type10LsaChksumSum;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeType2AreaCksumSum
 Input       :  The Indices
                FutOspfTeAreaId

                The Object 
                retValFutOspfTeType2AreaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeType2AreaCksumSum (UINT4 u4FutOspfTeAreaId,
                                  INT4 *pi4RetValFutOspfTeType2AreaCksumSum)
{
    tOsTeArea          *pOsTeArea = NULL;

    if ((pOsTeArea = OspfTeUtilFindArea (u4FutOspfTeAreaId)) != NULL)
    {
        *pi4RetValFutOspfTeType2AreaCksumSum =
            (INT4) pOsTeArea->u4NetLsaChksumSum;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutOspfTeIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfTeIfTable
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfTeIfTable (UINT4 u4FutOspfTeIfIpAddress,
                                          INT4 i4FutOspfTeAddressLessIf)
{
    tIPADDR             ifIpAddr;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if (OspfTeFindTeInterface
        (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfTeIfTable
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfTeIfTable (UINT4 *pu4FutOspfTeIfIpAddress,
                                  INT4 *pi4FutOspfTeAddressLessIf)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pLstNode = TMO_SLL_First (&(gOsTeContext.sortTeIfLst))) != NULL)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR
            (tOsTeInterface, NextSortLinkNode, pLstNode);
        IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
        *pi4FutOspfTeAddressLessIf = (INT4) pOsTeInterface->u4AddrlessIf;
        *pu4FutOspfTeIfIpAddress = OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfTeIfTable
 Input       :  The Indices
                FutOspfTeIfIpAddress
                nextFutOspfTeIfIpAddress
                FutOspfTeAddressLessIf
                nextFutOspfTeAddressLessIf
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfTeIfTable (UINT4 u4FutOspfTeIfIpAddress,
                                 UINT4 *pu4NextFutOspfTeIfIpAddress,
                                 INT4 i4FutOspfTeAddressLessIf,
                                 INT4 *pi4NextFutOspfTeAddressLessIf)
{
    tIPADDR             currIfIpAddr;
    tIPADDR             nextIfIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (currIfIpAddr, u4FutOspfTeIfIpAddress);

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR
            (tOsTeInterface, NextSortLinkNode, pLstNode);

        switch (OspfTeUtilIpAddrComp
                (currIfIpAddr, (pOsTeInterface->localIpAddr)))
        {
            case OSPF_TE_GREATER:
                continue;

            case OSPF_TE_EQUAL:
                if (((UINT4) i4FutOspfTeAddressLessIf >=
                     pOsTeInterface->u4AddrlessIf))
                {
                    continue;
                }
                /* fall through */

            case OSPF_TE_LESS:
                IP_ADDR_COPY (nextIfIpAddr, pOsTeInterface->localIpAddr);
                *pi4NextFutOspfTeAddressLessIf =
                    (INT4) (pOsTeInterface->u4AddrlessIf);
                *pu4NextFutOspfTeIfIpAddress = OSPF_TE_CRU_BMC_DWFROMPDU
                    (nextIfIpAddr);
                return SNMP_SUCCESS;

            default:
                return SNMP_FAILURE;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfAreaId
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf

                The Object 
                retValFutOspfTeIfAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfAreaId (UINT4 u4FutOspfTeIfIpAddress,
                         INT4 i4FutOspfTeAddressLessIf,
                         UINT4 *pu4RetValFutOspfTeIfAreaId)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        if (pOsTeInterface->pTeArea != NULL)
        {
            *pu4RetValFutOspfTeIfAreaId = pOsTeInterface->pTeArea->u4AreaId;
        }
        else
        {
            *pu4RetValFutOspfTeIfAreaId = OSPF_TE_INVALID_INFO;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfType
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf

                The Object 
                retValFutOspfTeIfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfType (UINT4 u4FutOspfTeIfIpAddress,
                       INT4 i4FutOspfTeAddressLessIf,
                       INT4 *pi4RetValFutOspfTeIfType)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        *pi4RetValFutOspfTeIfType = (INT4) pOsTeInterface->u1LinkType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfMetric
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf

                The Object 
                retValFutOspfTeIfMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfMetric (UINT4 u4FutOspfTeIfIpAddress,
                         INT4 i4FutOspfTeAddressLessIf,
                         INT4 *pi4RetValFutOspfTeIfMetric)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        *pi4RetValFutOspfTeIfMetric = (INT4) pOsTeInterface->u4TeMetric;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfMaxBw
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf

                The Object 
                retValFutOspfTeIfMaxBw
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfMaxBw (UINT4 u4FutOspfTeIfIpAddress,
                        INT4 i4FutOspfTeAddressLessIf,
                        tSNMP_COUNTER64_TYPE * pu8RetValFutOspfTeIfMaxBw)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        pu8RetValFutOspfTeIfMaxBw->msn = OSPF_TE_NULL;
        pu8RetValFutOspfTeIfMaxBw->lsn = (UINT4) pOsTeInterface->maxBw;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfMaxReservBw
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf

                The Object 
                retValFutOspfTeIfMaxReservBw
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfMaxReservBw (UINT4 u4FutOspfTeIfIpAddress,
                              INT4 i4FutOspfTeAddressLessIf,
                              tSNMP_COUNTER64_TYPE *
                              pu8RetValFutOspfTeIfMaxReservBw)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        pu8RetValFutOspfTeIfMaxReservBw->msn = OSPF_TE_NULL;
        pu8RetValFutOspfTeIfMaxReservBw->lsn = (UINT4) pOsTeInterface->maxResBw;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfRsrcClassColor
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf

                The Object 
                retValFutOspfTeIfRsrcClassColor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfRsrcClassColor (UINT4 u4FutOspfTeIfIpAddress,
                                 INT4 i4FutOspfTeAddressLessIf,
                                 INT4 *pi4RetValFutOspfTeIfRsrcClassColor)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        *pi4RetValFutOspfTeIfRsrcClassColor =
            (INT4) pOsTeInterface->u4RsrcClassColor;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfOperStat
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf

                The Object 
                retValFutOspfTeIfOperStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfOperStat (UINT4 u4FutOspfTeIfIpAddress,
                           INT4 i4FutOspfTeAddressLessIf,
                           INT4 *pi4RetValFutOspfTeIfOperStat)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        *pi4RetValFutOspfTeIfOperStat = (INT4) pOsTeInterface->u1LinkStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfLinkId
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf

                The Object 
                retValFutOspfTeIfLinkId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfLinkId (UINT4 u4FutOspfTeIfIpAddress,
                         INT4 i4FutOspfTeAddressLessIf,
                         UINT4 *pu4RetValFutOspfTeIfLinkId)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfTeIfLinkId =
            OSPF_TE_CRU_BMC_DWFROMPDU (pOsTeInterface->linkId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfRemoteIpAddr
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf

                The Object 
                retValFutOspfTeIfRemoteIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfRemoteIpAddr (UINT4 u4FutOspfTeIfIpAddress,
                               INT4 i4FutOspfTeAddressLessIf,
                               UINT4 *pu4RetValFutOspfTeIfRemoteIpAddr)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        *pu4RetValFutOspfTeIfRemoteIpAddr =
            OSPF_TE_CRU_BMC_DWFROMPDU (pOsTeInterface->remoteIpAddr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfProtectionType
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf

                The Object 
                retValFutOspfTeIfProtectionType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfProtectionType (UINT4 u4FutOspfTeIfIpAddress,
                                 INT4 i4FutOspfTeAddressLessIf,
                                 INT4 *pi4RetValFutOspfTeIfProtectionType)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        *pi4RetValFutOspfTeIfProtectionType =
            (INT4) pOsTeInterface->u1ProtectionType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfSrlg
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf

                The Object 
                retValFutOspfTeIfSrlg
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfSrlg (UINT4 u4FutOspfTeIfIpAddress,
                       INT4 i4FutOspfTeAddressLessIf,
                       tSNMP_OCTET_STRING_TYPE * pRetValFutOspfTeIfSrlg)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        OSPF_TE_MEMCPY (pRetValFutOspfTeIfSrlg->pu1_OctetList,
                        pOsTeInterface->osTeSrlg.aSrlgNumber,
                        (pOsTeInterface->osTeSrlg.u4NoOfSrlg *
                         MAX_IP_ADDR_LEN));

        /* This Parameter is used for making Octet String. */
        pRetValFutOspfTeIfSrlg->i4_Length =
            (INT4) (pOsTeInterface->osTeSrlg.u4NoOfSrlg * MAX_IP_ADDR_LEN);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutOspfTeIfDescriptorTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfTeIfDescriptorTable
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfTeIfDescriptorTable (UINT4
                                                    u4FutOspfTeIfDescrIpAddress,
                                                    INT4
                                                    i4FutOspfTeIfDescrAddressLessIf,
                                                    UINT4 u4FutOspfTeIfDescrId)
{

    if (OspfTeFindTeIfDescr (u4FutOspfTeIfDescrIpAddress,
                             i4FutOspfTeIfDescrAddressLessIf,
                             u4FutOspfTeIfDescrId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfTeIfDescriptorTable
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfTeIfDescriptorTable (UINT4
                                            *pu4FutOspfTeIfDescrIpAddress,
                                            INT4
                                            *pi4FutOspfTeIfDescrAddressLessIf,
                                            UINT4 *pu4FutOspfTeIfDescrId)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeIfDescNode    *pOsTeIfDescNode = NULL;

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR
            (tOsTeInterface, NextSortLinkNode, pLstNode);
        if ((pOsTeIfDescNode = (tOsTeIfDescNode *)
             TMO_SLL_First (&(pOsTeInterface->ifDescrList))) != NULL)
        {
            *pu4FutOspfTeIfDescrId = pOsTeIfDescNode->ifDesc.u4DescrId;
            IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
            *pi4FutOspfTeIfDescrAddressLessIf =
                (INT4) pOsTeInterface->u4AddrlessIf;
            *pu4FutOspfTeIfDescrIpAddress =
                OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfTeIfDescriptorTable
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                nextFutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                nextFutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId
                nextFutOspfTeIfDescrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfTeIfDescriptorTable (UINT4 u4FutOspfTeIfDescrIpAddress,
                                           UINT4
                                           *pu4NextFutOspfTeIfDescrIpAddress,
                                           INT4 i4FutOspfTeIfDescrAddressLessIf,
                                           INT4
                                           *pi4NextFutOspfTeIfDescrAddressLessIf,
                                           UINT4 u4FutOspfTeIfDescrId,
                                           UINT4 *pu4NextFutOspfTeIfDescrId)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeIfDescNode    *pOsTeIfDescNode = NULL;
    tIPADDR             currIfIpAddr;
    UINT1               u1Flag = OSPF_TE_FALSE;

    OSPF_TE_CRU_BMC_DWTOPDU (currIfIpAddr, u4FutOspfTeIfDescrIpAddress);

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR
            (tOsTeInterface, NextSortLinkNode, pLstNode);
        switch (OspfTeUtilIpAddrComp
                (currIfIpAddr, (pOsTeInterface->localIpAddr)))
        {
            case OSPF_TE_GREATER:
                continue;

            case OSPF_TE_EQUAL:
                if (((UINT4) i4FutOspfTeIfDescrAddressLessIf >
                     pOsTeInterface->u4AddrlessIf))
                {
                    continue;
                }
                TMO_SLL_Scan (&(pOsTeInterface->ifDescrList),
                              pOsTeIfDescNode, tOsTeIfDescNode *)
                {
                    if (pOsTeIfDescNode->ifDesc.u4DescrId
                        == u4FutOspfTeIfDescrId)
                    {
                        u1Flag = OSPF_TE_TRUE;
                    }
                    else if (u1Flag == OSPF_TE_TRUE)
                    {
                        *pu4NextFutOspfTeIfDescrId =
                            pOsTeIfDescNode->ifDesc.u4DescrId;
                        IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
                        *pi4NextFutOspfTeIfDescrAddressLessIf =
                            (INT4) pOsTeInterface->u4AddrlessIf;
                        *pu4NextFutOspfTeIfDescrIpAddress =
                            OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
                        return SNMP_SUCCESS;
                    }
                }
                continue;

            case OSPF_TE_LESS:
                if ((pOsTeIfDescNode = (tOsTeIfDescNode *)
                     TMO_SLL_First (&(pOsTeInterface->ifDescrList))) != NULL)
                {
                    *pu4NextFutOspfTeIfDescrId =
                        pOsTeIfDescNode->ifDesc.u4DescrId;
                    IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
                    *pi4NextFutOspfTeIfDescrAddressLessIf =
                        (INT4) pOsTeInterface->u4AddrlessIf;
                    *pu4NextFutOspfTeIfDescrIpAddress =
                        OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
                    return SNMP_SUCCESS;
                }
                continue;

            default:
                return SNMP_FAILURE;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfDescrSwithingCap
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId

                The Object 
                retValFutOspfTeIfDescrSwithingCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfDescrSwithingCap (UINT4 u4FutOspfTeIfDescrIpAddress,
                                   INT4 i4FutOspfTeIfDescrAddressLessIf,
                                   UINT4 u4FutOspfTeIfDescrId,
                                   INT4 *pi4RetValFutOspfTeIfDescrSwithingCap)
{
    tOsTeIfDesc        *pOsTeIfDesc = NULL;

    if ((pOsTeIfDesc = OspfTeFindTeIfDescr (u4FutOspfTeIfDescrIpAddress,
                                            i4FutOspfTeIfDescrAddressLessIf,
                                            u4FutOspfTeIfDescrId)) != NULL)
    {
        *pi4RetValFutOspfTeIfDescrSwithingCap =
            (INT4) pOsTeIfDesc->u1SwitchingCap;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfDescrEncodingType
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId

                The Object 
                retValFutOspfTeIfDescrEncodingType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfDescrEncodingType (UINT4 u4FutOspfTeIfDescrIpAddress,
                                    INT4
                                    i4FutOspfTeIfDescrAddressLessIf,
                                    UINT4 u4FutOspfTeIfDescrId,
                                    INT4 *pi4RetValFutOspfTeIfDescrEncodingType)
{
    tOsTeIfDesc        *pOsTeIfDesc = NULL;

    if ((pOsTeIfDesc = OspfTeFindTeIfDescr (u4FutOspfTeIfDescrIpAddress,
                                            i4FutOspfTeIfDescrAddressLessIf,
                                            u4FutOspfTeIfDescrId)) != NULL)
    {
        *pi4RetValFutOspfTeIfDescrEncodingType =
            (INT4) pOsTeIfDesc->u1EncodingType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfDescrMinLSPBandwidth
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId

                The Object 
                retValFutOspfTeIfDescrMinLSPBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfDescrMinLSPBandwidth (UINT4
                                       u4FutOspfTeIfDescrIpAddress,
                                       INT4
                                       i4FutOspfTeIfDescrAddressLessIf,
                                       UINT4 u4FutOspfTeIfDescrId,
                                       tSNMP_COUNTER64_TYPE *
                                       pu8RetValFutOspfTeIfDescrMinLSPBandwidth)
{
    tOsTeIfDesc        *pOsTeIfDesc = NULL;

    if ((pOsTeIfDesc = OspfTeFindTeIfDescr (u4FutOspfTeIfDescrIpAddress,
                                            i4FutOspfTeIfDescrAddressLessIf,
                                            u4FutOspfTeIfDescrId)) != NULL)
    {
        pu8RetValFutOspfTeIfDescrMinLSPBandwidth->msn = OSPF_TE_NULL;
        pu8RetValFutOspfTeIfDescrMinLSPBandwidth->lsn =
            (UINT4) pOsTeIfDesc->minLSPBw;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfDescrMTU
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId

                The Object 
                retValFutOspfTeIfDescrMTU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfDescrMTU (UINT4 u4FutOspfTeIfDescrIpAddress,
                           INT4 i4FutOspfTeIfDescrAddressLessIf,
                           UINT4 u4FutOspfTeIfDescrId,
                           INT4 *pi4RetValFutOspfTeIfDescrMTU)
{
    tOsTeIfDesc        *pOsTeIfDesc = NULL;

    if ((pOsTeIfDesc = OspfTeFindTeIfDescr (u4FutOspfTeIfDescrIpAddress,
                                            i4FutOspfTeIfDescrAddressLessIf,
                                            u4FutOspfTeIfDescrId)) != NULL)
    {
        *pi4RetValFutOspfTeIfDescrMTU = (INT4) pOsTeIfDesc->u2MTU;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfDescrIndication
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId

                The Object 
                retValFutOspfTeIfDescrIndication
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfDescrIndication (UINT4 u4FutOspfTeIfDescrIpAddress,
                                  INT4 i4FutOspfTeIfDescrAddressLessIf,
                                  UINT4 u4FutOspfTeIfDescrId,
                                  INT4 *pi4RetValFutOspfTeIfDescrIndication)
{
    tOsTeIfDesc        *pOsTeIfDesc = NULL;

    if ((pOsTeIfDesc = OspfTeFindTeIfDescr (u4FutOspfTeIfDescrIpAddress,
                                            i4FutOspfTeIfDescrAddressLessIf,
                                            u4FutOspfTeIfDescrId)) != NULL)
    {
        *pi4RetValFutOspfTeIfDescrIndication = (INT4) pOsTeIfDesc->u1Indication;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutOspfTeIfSwDescrMaxBwTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfTeIfSwDescrMaxBwTable
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId
                FutOspfTeIfSwDescrMaxBwPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfTeIfSwDescrMaxBwTable (UINT4
                                                      u4FutOspfTeIfDescrIpAddress,
                                                      INT4
                                                      i4FutOspfTeIfDescrAddressLessIf,
                                                      UINT4
                                                      u4FutOspfTeIfDescrId,
                                                      INT4
                                                      i4FutOspfTeIfSwDescrMaxBwPriority)
{

    if (OspfTeFindTeIfDescr (u4FutOspfTeIfDescrIpAddress,
                             i4FutOspfTeIfDescrAddressLessIf,
                             u4FutOspfTeIfDescrId) != NULL)
    {
        if (IS_VALID_PRIORITY_VALUE (i4FutOspfTeIfSwDescrMaxBwPriority))
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfTeIfSwDescrMaxBwTable
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId
                FutOspfTeIfSwDescrMaxBwPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfTeIfSwDescrMaxBwTable (UINT4
                                              *pu4FutOspfTeIfDescrIpAddress,
                                              INT4
                                              *pi4FutOspfTeIfDescrAddressLessIf,
                                              UINT4 *pu4FutOspfTeIfDescrId,
                                              INT4
                                              *pi4FutOspfTeIfSwDescrMaxBwPriority)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeIfDescNode    *pOsTeIfDescNode = NULL;

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR
            (tOsTeInterface, NextSortLinkNode, pLstNode);
        if ((pOsTeIfDescNode = (tOsTeIfDescNode *)
             TMO_SLL_First (&(pOsTeInterface->ifDescrList))) != NULL)
        {
            *pu4FutOspfTeIfDescrId = pOsTeIfDescNode->ifDesc.u4DescrId;
            IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
            *pi4FutOspfTeIfDescrAddressLessIf =
                (INT4) pOsTeInterface->u4AddrlessIf;
            *pu4FutOspfTeIfDescrIpAddress =
                OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
            *pi4FutOspfTeIfSwDescrMaxBwPriority = OSPF_TE_NULL;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfTeIfSwDescrMaxBwTable
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                nextFutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                nextFutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId
                nextFutOspfTeIfDescrId
                FutOspfTeIfSwDescrMaxBwPriority
                nextFutOspfTeIfSwDescrMaxBwPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfTeIfSwDescrMaxBwTable (UINT4
                                             u4FutOspfTeIfDescrIpAddress,
                                             UINT4
                                             *pu4NextFutOspfTeIfDescrIpAddress,
                                             INT4
                                             i4FutOspfTeIfDescrAddressLessIf,
                                             INT4
                                             *pi4NextFutOspfTeIfDescrAddressLessIf,
                                             UINT4 u4FutOspfTeIfDescrId,
                                             UINT4
                                             *pu4NextFutOspfTeIfDescrId,
                                             INT4
                                             i4FutOspfTeIfSwDescrMaxBwPriority,
                                             INT4
                                             *pi4NextFutOspfTeIfSwDescrMaxBwPriority)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeIfDescNode    *pOsTeIfDescNode = NULL;
    tIPADDR             currIfIpAddr;
    UINT1               u1Flag = OSPF_TE_FALSE;

    OSPF_TE_CRU_BMC_DWTOPDU (currIfIpAddr, u4FutOspfTeIfDescrIpAddress);

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR
            (tOsTeInterface, NextSortLinkNode, pLstNode);
        switch (OspfTeUtilIpAddrIndComp (currIfIpAddr,
                                         (UINT4)
                                         i4FutOspfTeIfDescrAddressLessIf,
                                         pOsTeInterface->localIpAddr,
                                         pOsTeInterface->u4AddrlessIf))
        {
            case OSPF_TE_GREATER:
                continue;

            case OSPF_TE_EQUAL:
                TMO_SLL_Scan (&(pOsTeInterface->ifDescrList),
                              pOsTeIfDescNode, tOsTeIfDescNode *)
            {
                if (pOsTeIfDescNode->ifDesc.u4DescrId == u4FutOspfTeIfDescrId)
                {
                    if (i4FutOspfTeIfSwDescrMaxBwPriority
                        < OSPF_TE_MAX_PRIORITY_LVL - OSPF_TE_ONE)
                    {
                        *pu4NextFutOspfTeIfDescrId =
                            pOsTeIfDescNode->ifDesc.u4DescrId;
                        IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
                        *pi4NextFutOspfTeIfDescrAddressLessIf =
                            (INT4) pOsTeInterface->u4AddrlessIf;
                        *pu4NextFutOspfTeIfDescrIpAddress =
                            OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
                        *pi4NextFutOspfTeIfSwDescrMaxBwPriority =
                            i4FutOspfTeIfSwDescrMaxBwPriority + OSPF_TE_ONE;
                        return SNMP_SUCCESS;
                    }
                    u1Flag = OSPF_TE_TRUE;
                }
                else if (u1Flag == OSPF_TE_TRUE)
                {
                    *pu4NextFutOspfTeIfDescrId =
                        pOsTeIfDescNode->ifDesc.u4DescrId;
                    IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
                    *pi4NextFutOspfTeIfDescrAddressLessIf =
                        (INT4) pOsTeInterface->u4AddrlessIf;
                    *pu4NextFutOspfTeIfDescrIpAddress =
                        OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
                    *pi4NextFutOspfTeIfSwDescrMaxBwPriority = OSPF_TE_NULL;
                    return SNMP_SUCCESS;

                }
            }
                continue;

            case OSPF_TE_LESS:
                if ((pOsTeIfDescNode = (tOsTeIfDescNode *)
                     TMO_SLL_First (&(pOsTeInterface->ifDescrList))) != NULL)
                {
                    *pu4NextFutOspfTeIfDescrId =
                        pOsTeIfDescNode->ifDesc.u4DescrId;
                    IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
                    *pi4NextFutOspfTeIfDescrAddressLessIf =
                        (INT4) pOsTeInterface->u4AddrlessIf;
                    *pu4NextFutOspfTeIfDescrIpAddress =
                        OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
                    *pi4NextFutOspfTeIfSwDescrMaxBwPriority = OSPF_TE_NULL;
                    return SNMP_SUCCESS;
                }
                continue;
            default:
                return SNMP_FAILURE;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfSwDescrMaxLSPBandwidth
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfDescrId
                FutOspfTeIfSwDescrMaxBwPriority

                The Object 
                retValFutOspfTeIfSwDescrMaxLSPBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfSwDescrMaxLSPBandwidth (UINT4 u4FutOspfTeIfDescrIpAddress,
                                         INT4
                                         i4FutOspfTeIfDescrAddressLessIf,
                                         UINT4 u4FutOspfTeIfDescrId,
                                         INT4 i4FutOspfTeIfSwDescrMaxBwPriority,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValFutOspfTeIfSwDescrMaxLSPBandwidth)
{
    tOsTeIfDesc        *pOsTeIfDesc = NULL;

    if ((pOsTeIfDesc = OspfTeFindTeIfDescr (u4FutOspfTeIfDescrIpAddress,
                                            i4FutOspfTeIfDescrAddressLessIf,
                                            u4FutOspfTeIfDescrId)) != NULL)
    {
        pu8RetValFutOspfTeIfSwDescrMaxLSPBandwidth->msn = OSPF_TE_NULL;
        pu8RetValFutOspfTeIfSwDescrMaxLSPBandwidth->lsn =
            (UINT4) pOsTeIfDesc->
            aDescMaxLSPBw[i4FutOspfTeIfSwDescrMaxBwPriority];
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutOspfTeIfBandwidthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfTeIfBandwidthTable
 Input       :  The Indices
                FutOspfTeIfIpAddress
                FutOspfTeAddressLessIf
                FutOspfTeIfBandwidthPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfTeIfBandwidthTable (UINT4
                                                   u4FutOspfTeIfIpAddress,
                                                   INT4
                                                   i4FutOspfTeAddressLessIf,
                                                   INT4
                                                   i4FutOspfTeIfBandwidthPriority)
{
    tIPADDR             ifIpAddr;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if (OspfTeFindTeInterface
        (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf) != NULL)
    {
        if (IS_VALID_PRIORITY_VALUE (i4FutOspfTeIfBandwidthPriority))
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfTeIfBandwidthTable
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfBandwidthPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfTeIfBandwidthTable (UINT4
                                           *pu4FutOspfTeIfIpAddress,
                                           INT4
                                           *pi4FutOspfTeAddressLessIf,
                                           INT4
                                           *pi4FutOspfTeIfBandwidthPriority)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pLstNode = TMO_SLL_First (&(gOsTeContext.sortTeIfLst))) != NULL)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR
            (tOsTeInterface, NextSortLinkNode, pLstNode);
        IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
        *pi4FutOspfTeAddressLessIf = (INT4) pOsTeInterface->u4AddrlessIf;
        *pu4FutOspfTeIfIpAddress = OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
        *pi4FutOspfTeIfBandwidthPriority = OSPF_TE_NULL;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfTeIfBandwidthTable
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                nextFutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                nextFutOspfTeIfDescrAddressLessIf
                FutOspfTeIfBandwidthPriority
                nextFutOspfTeIfBandwidthPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfTeIfBandwidthTable (UINT4 u4FutOspfTeIfIpAddress,
                                          UINT4
                                          *pu4NextFutOspfTeIfIpAddress,
                                          INT4
                                          i4FutOspfTeAddressLessIf,
                                          INT4
                                          *pi4NextFutOspfTeAddressLessIf,
                                          INT4 i4FutOspfTeIfBandwidthPriority,
                                          INT4
                                          *pi4NextFutOspfTeIfBandwidthPriority)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tIPADDR             currIfIpAddr;

    OSPF_TE_CRU_BMC_DWTOPDU (currIfIpAddr, u4FutOspfTeIfIpAddress);

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR
            (tOsTeInterface, NextSortLinkNode, pLstNode);
        switch (OspfTeUtilIpAddrIndComp (currIfIpAddr,
                                         (UINT4) i4FutOspfTeAddressLessIf,
                                         pOsTeInterface->localIpAddr,
                                         pOsTeInterface->u4AddrlessIf))
        {
            case OSPF_TE_GREATER:
                continue;

            case OSPF_TE_EQUAL:
                if (i4FutOspfTeIfBandwidthPriority <
                    (OSPF_TE_MAX_PRIORITY_LVL - OSPF_TE_ONE))
                {
                    IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
                    *pi4NextFutOspfTeAddressLessIf =
                        (INT4) pOsTeInterface->u4AddrlessIf;
                    *pu4NextFutOspfTeIfIpAddress =
                        OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
                    *pi4NextFutOspfTeIfBandwidthPriority =
                        i4FutOspfTeIfBandwidthPriority + OSPF_TE_ONE;
                    return SNMP_SUCCESS;
                }
                continue;

            case OSPF_TE_LESS:
                IP_ADDR_COPY (ifIpAddr, pOsTeInterface->localIpAddr);
                *pi4NextFutOspfTeAddressLessIf =
                    (INT4) pOsTeInterface->u4AddrlessIf;
                *pu4NextFutOspfTeIfIpAddress =
                    OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr);
                *pi4NextFutOspfTeIfBandwidthPriority = OSPF_TE_NULL;
                return SNMP_SUCCESS;
            default:
                return SNMP_FAILURE;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfTeIfUnreservedBandwidth
 Input       :  The Indices
                FutOspfTeIfDescrIpAddress
                FutOspfTeIfDescrAddressLessIf
                FutOspfTeIfBandwidthPriority

                The Object 
                retValFutOspfTeIfUnreservedBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfTeIfUnreservedBandwidth (UINT4 u4FutOspfTeIfIpAddress,
                                      INT4 i4FutOspfTeAddressLessIf,
                                      INT4 i4FutOspfTeIfBandwidthPriority,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFutOspfTeIfUnreservedBandwidth)
{
    tIPADDR             ifIpAddr;
    tOsTeInterface     *pOsTeInterface = NULL;

    OSPF_TE_CRU_BMC_DWTOPDU (ifIpAddr, u4FutOspfTeIfIpAddress);

    if ((pOsTeInterface = OspfTeFindTeInterface
         (ifIpAddr, (UINT4) i4FutOspfTeAddressLessIf)) != NULL)
    {
        pu8RetValFutOspfTeIfUnreservedBandwidth->msn = OSPF_TE_NULL;
        pu8RetValFutOspfTeIfUnreservedBandwidth->lsn =
            (UINT4) pOsTeInterface->aUnResBw[i4FutOspfTeIfBandwidthPriority];
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  fsotelw.c                      */
/*-----------------------------------------------------------------------*/
