/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostesnmp.c,v 1.4 2011/12/14 13:27:35 siva Exp $
 *
 * Description:  This file contains the routines for the interaction 
 *               with SNMP.
 * *********************************************************************/

#include "osteinc.h"

/***************************************************************************/
/*  Function    :  OspfTeAdminUpHdlr                                       */
/*                                                                         */
/*  Description :  This function makes the TE Admin Status enabled         */
/*                                                                         */
/*  Input       :  None                                                    */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  OSPF_TE_SUCCESS / OSPF_TE_FAILURE                       */
/***************************************************************************/
PUBLIC INT4
OspfTeAdminUpHdlr (VOID)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeAdminUpHdlr\n");

    /* Register OSPF-TE with OSPF */
    if (OspfTeSndMsgToOspf (OSPF_TE_OSPF_REG, NULL) != OSPF_TE_SUCCESS)
    {
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Registration with OSPF failed\n");
        return OSPF_TE_FAILURE;
    }

    /* Intializing data structures */
    if (OspfTeMemInit () != OSPF_TE_SUCCESS)
    {
        OspfTeSndMsgToOspf (OSPF_TE_OSPF_DEREG, NULL);
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Mem Pools Creation failed\n");
        return OSPF_TE_FAILURE;
    }

    gOsTeContext.u1TeAdminStat = OSPF_TE_ENABLED;

    /* Register OSPF-TE with TLM */
    gOsTeContext.u1TlmRegister = OSPF_TE_TRUE;
    if (OspfTeSndMsgToTlm (OSPF_TE_TLM_REG) != OSPF_TE_SUCCESS)
    {
        gOsTeContext.u1TlmRegister = OSPF_TE_FALSE;
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Registration with TLM failed\n");
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeAdminUpHdlr\n");

    return OSPF_TE_SUCCESS;
}

/***************************************************************************/
/*  Function    :  OspfTeAdminDnHdlr                                       */
/*                                                                         */
/*  Description :  This module deregisters with OSPF. It also releases all */
/*                 the memory blocks into Memory Pool. It shuts down       */
/*                 the OSPF-TE activity                                    */
/*                                                                         */
/*  Input       :  None                                                    */
/*                                                                         */
/*  Output      :  None                                                    */
/*                                                                         */
/*  Returns     :  OSPF_TE_SUCCESS / OSPF_TE_FAILURE                       */
/***************************************************************************/
PUBLIC INT4
OspfTeAdminDnHdlr (VOID)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeAdminDnHdlr\n");

    /* If the RM is registered, then OSPF admin status disabled is 
     * not allowed */
    if (gOsTeContext.u1RmRegister == OSPF_TE_TRUE)
    {
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "RM is registered, so Disable "
                     "is not allowed\n");
        return OSPF_TE_FAILURE;
    }

    /* Deregister OSPF-TE with OSPF */
    if (OspfTeSndMsgToOspf (OSPF_TE_OSPF_DEREG, NULL) != OSPF_TE_SUCCESS)
    {
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "De-Registration with OSPF Failed\n");
        return OSPF_TE_FAILURE;
    }

    /* Deregister OSPF-TE with TLM */
    if (gOsTeContext.u1TlmRegister == OSPF_TE_TRUE)
    {
        if (OspfTeSndMsgToTlm (OSPF_TE_TLM_DEREG) != OSPF_TE_SUCCESS)
        {
            OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                         "De-Registration with TLM failed\n");
            return OSPF_TE_FAILURE;
        }
    }
    /* Delete All Areas -- While deleting it will delete all LSAs */
    OspfTeAreaDeleteAll ();

    /* Delete all OspfTe Interface Information */
    OspfTeIfDeleteAllOsTeInterface ();

    /* Need to delete all Ospf information interfaces */
    OspfTeIfDeleteAllOspfInfoInt ();

    /* Free the blocks into memory pool */
    OspfTeShutDown ();

    gOsTeContext.u1TeAdminStat = OSPF_TE_DISABLED;
    gOsTeContext.u4CspfRunCnt = OSPF_TE_ZERO;
    gOsTeContext.u1TlmRegister = OSPF_TE_FALSE;
    gOsTeContext.u1RmRegister = OSPF_TE_ZERO;

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeAdminDnHdlr\n");

    return OSPF_TE_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostesnmp.c                     */
/*-----------------------------------------------------------------------*/
