/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: osteport.c,v 1.12 2014/03/03 12:22:30 siva Exp $
 *
 * Description:  This file contains the portable routines OSPFTE.
 * *********************************************************************/

#include "osteinc.h"
#include "cfa.h"
#include "tlm.h"

PRIVATE INT4        OspfTeEnqueueMsgFromRmgr (UINT1 u1MsgType,
                                              tRmOsTeLinkMsg * pRmOsTeLinkMsg);

PRIVATE INT4        OspfTeTlmSendLinkInfo (tTlmEventNotification * pTlmLinkMsg);
PRIVATE INT4        OspfTeEnqueueMsgFromTlm (tTlmEventNotification *
                                             pTlmLinkMsg);

/*****************************************************************************/
/*  Function    :  OspfTeSndMsgToOspf                                        */
/*                                                                           */
/*  Description :  This function is used to send TLV information to OSPF to  */
/*                 be flooded as Opaque LSAs.                                */
/*                                                                           */
/*  Input       :  u1MsgType - Indicates type of interaction with OSPF       */
/*                 pOsTeOspfMsg - Pointer to Message to OSPF                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS / OSPF_TE_FAILURE                         */
/*****************************************************************************/
PUBLIC INT4
OspfTeSndMsgToOspf (UINT1 u1MsgType, tOsTeOspfMsg * pOsTeOspfMsg)
{
    INT4                i4Status = OSPF_TE_FAILURE;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeSndMsgToOspf\n");

    switch (u1MsgType)
    {
        case OSPF_TE_OSPF_REG:
            i4Status = OpqAppRegisterInCxt (OSPF_DEFAULT_CXT_ID,
                                            OSPF_TE_OPQ_TYPE,
                                            OSPF_TE_OPQ_LSA_SUPPORTED,
                                            OSPF_TE_OSPF_INFO,
                                            (VOID *) OspfTeEnqueueMsgFromOspf);
            break;

        case OSPF_TE_OSPF_SEND_OPQ_LSA:
            i4Status = OpqAppToOpqModuleSendInCxt (OSPF_DEFAULT_CXT_ID,
                                                   pOsTeOspfMsg);
            break;

        case OSPF_TE_OSPF_DEREG:
            i4Status = OpqAppDeRegisterInCxt (OSPF_DEFAULT_CXT_ID,
                                              OSPF_TE_OPQ_TYPE);
            break;

        default:
            break;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeSndMsgToOspf\n");

    return i4Status;
}

/*****************************************************************************/
/*  Function    :  OspfTeEnqueueMsgFromOspf                                  */
/*                                                                           */
/*  Description :  This function is a call back function called by OSPF to   */
/*                 post the message from OSPF to OSPF-TE                     */
/*                                                                           */
/*  Input       :  None                                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS or                                        */
/*                 OSPF_TE_FAILURE                                           */
/*****************************************************************************/
PUBLIC INT4
OspfTeEnqueueMsgFromOspf (tOsToOpqApp * pOspfMsg)
{
    UINT1              *pLsa = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeEnqueueMsgFromOspf\n");

    OspfTeLock ();

    if (gOsTeContext.u1TeAdminStat != OSPF_TE_ENABLED)
    {
        OspfTeUnLock ();
        return OSPF_TE_FAILURE;
    }

    if (pOspfMsg->u4MsgSubType == OSPF_TE_LSA_INFO)
    {

        if (pOspfMsg->lsaInfo.u2LsaLen > OSPF_TE_MAX_LSA_SIZE)
        {
            OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to allocate memory "
                         "for LSA\n");
            OspfTeUnLock ();
            return OSPF_TE_FAILURE;
        }

        if (OSPF_TE_LSA_ALLOC (pLsa, UINT1) == NULL)
        {
            OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to allocate memory "
                         "for LSA\n");
            OspfTeUnLock ();
            return OSPF_TE_FAILURE;
        }

        OSPF_TE_MEMCPY (pLsa, pOspfMsg->lsaInfo.pu1Lsa,
                        pOspfMsg->lsaInfo.u2LsaLen);

        pOspfMsg->lsaInfo.pu1Lsa = pLsa;
    }

    OspfTeProcessOspfMsg (pOspfMsg);

    OspfTeUnLock ();

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeEnqueueMsgFromOspf\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeRmgrRegister                                        */
/*                                                                           */
/*  Description :  This function is provided to RM for registering with      */
/*                 OSPF-TE                                                   */
/*                                                                           */
/*  Input       :  None                                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                           */
/*****************************************************************************/
PUBLIC INT4
OspfTeRmgrRegister (VOID)
{
    INT4                i4Status;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeRmgrRegister\n");

    i4Status = OspfTeEnqueueMsgFromRmgr (OSPF_TE_RM_REG, NULL);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeRmgrRegister\n");

    return i4Status;
}

/*****************************************************************************/
/*  Function    :  OspfTeRmgrDeRegister                                      */
/*                                                                           */
/*  Description :  This function is provided to RM for de-registering with   */
/*                 OSPF-TE                                                   */
/*                                                                           */
/*  Input       :  None                                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                           */
/*****************************************************************************/
PUBLIC INT4
OspfTeRmgrDeRegister (VOID)
{
    INT4                i4Status;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeRmgrDeRegister\n");

    i4Status = OspfTeEnqueueMsgFromRmgr (OSPF_TE_RM_DEREG, NULL);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeRmgrDeRegister\n");

    return i4Status;
}

/*****************************************************************************/
/*  Function    :  OspfTeRmgrSendLinkInfo                                    */
/*                                                                           */
/*  Description :  This function is provided to RM to send TE link           */
/*                 information to OSPF-TE                                    */
/*                                                                           */
/*  Input       :  None                                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                           */
/*****************************************************************************/
PUBLIC INT4
OspfTeRmgrSendLinkInfo (tRmOsTeLinkMsg * pRmOsTeLinkMsg)
{
    INT4                i4Status;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeRmgrSendLinkInfo\n");

    i4Status = OspfTeEnqueueMsgFromRmgr (OSPF_TE_RM_LINK_INFO, pRmOsTeLinkMsg);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeRmgrSendLinkInfo\n");

    return i4Status;
}

/*****************************************************************************/
/*  Function    :  OspfTeEnqueueMsgFromRmgr                                  */
/*                                                                           */
/*  Description :  This function is used to post the message to OSPF-TE      */
/*                 Queue with the information from RM                        */
/*                                                                           */
/*  Input       :  u1MsgType - Message type from RM                          */
/*                 pRmOsTeLinkMsg - Pointer to RM message                    */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                           */
/*****************************************************************************/
PRIVATE INT4
OspfTeEnqueueMsgFromRmgr (UINT1 u1MsgType, tRmOsTeLinkMsg * pRmOsTeLinkMsg)
{
    UINT4               u4DescSize = OSPF_TE_ZERO;
    UINT4               u4SrlgSize = OSPF_TE_ZERO;
    tOspfTeQMsg        *pOspfQMsg = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeEnqueueMsgFromRmgr\n");

    if (gOsTeContext.u1TeAdminStat != OSPF_TE_ENABLED)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "OSPF-TE is DOWN\n");
        return OSPF_TE_FAILURE;
    }

    if (OSPF_TE_QMSG_ALLOC (pOspfQMsg, tOspfTeQMsg) == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to allocate the Q "
                     "Message\n");
        return OSPF_TE_FAILURE;
    }

    pOspfQMsg->u4MsgType = OSPF_TE_RMGR_MSG;
    pOspfQMsg->rmOspfTeIfParam.u4ModuleId = OSPF_TE_RMGR_MSG;

    switch (u1MsgType)
    {
        case OSPF_TE_RM_REG:
            pOspfQMsg->rmOspfTeIfParam.u1MsgSubType = OSPF_TE_RM_REG;
            break;

        case OSPF_TE_RM_LINK_INFO:
            OSPF_TE_MEMCPY (&(pOspfQMsg->rmOspfTeIfParam), pRmOsTeLinkMsg,
                            sizeof (tRmOsTeLinkMsg));

            pOspfQMsg->rmOspfTeIfParam.u1MsgSubType = OSPF_TE_RM_LINK_INFO;

            u4DescSize = pRmOsTeLinkMsg->u4IfDescCnt * sizeof (tOsTeIfDesc);

            /* Allocating memory for interface descriptors information */
            if ((u4DescSize != OSPF_TE_ZERO) &&
                (OSPF_TE_RM_LINK_INFO_ALLOC
                 (pOspfQMsg->rmOspfTeIfParam.pIfDescriptors, UINT1) == NULL))
            {
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                             "Failed to allocate Memory for "
                             "interface descriptors\n");
                OSPF_TE_QMSG_FREE (pOspfQMsg);
                return OSPF_TE_FAILURE;
            }

            /* Copying the interface descriptors information into Q memory */
            OSPF_TE_MEMCPY (pOspfQMsg->rmOspfTeIfParam.pIfDescriptors,
                            pRmOsTeLinkMsg->pIfDescriptors, u4DescSize);

            u4SrlgSize = pRmOsTeLinkMsg->u4IfSrlgCnt * sizeof (UINT4);

            /* Allocating memory for SRLG numbers */
            if ((u4SrlgSize != OSPF_TE_ZERO) &&
                (OSPF_TE_RM_LINK_SRLG_INFO_ALLOC
                 (pOspfQMsg->rmOspfTeIfParam.pSrlgs, UINT1)) == NULL)
            {
                if (u4DescSize != OSPF_TE_ZERO)
                {
                    OSPF_TE_RM_LINK_INFO_FREE (pOspfQMsg->rmOspfTeIfParam.
                                               pIfDescriptors);
                }
                OSPF_TE_QMSG_FREE (pOspfQMsg);
                OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                             "Failed to allocate Memory for " "SRLG Numbers\n");
                return OSPF_TE_FAILURE;
            }
            /* Copying the SRLG numbers information into Q memory */
            OSPF_TE_MEMCPY (pOspfQMsg->rmOspfTeIfParam.pSrlgs,
                            pRmOsTeLinkMsg->pSrlgs, u4SrlgSize);
            break;

        case OSPF_TE_RM_DEREG:
            pOspfQMsg->rmOspfTeIfParam.u1MsgSubType = OSPF_TE_RM_DEREG;
            break;

        default:
            OSPF_TE_QMSG_FREE (pOspfQMsg);
            OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Unrecognized Msg Type\n");
            return OSPF_TE_FAILURE;
    }

    /* Send the message to OSPF-TE Queue */
    if (OSPF_TE_MSG_SEND (pOspfQMsg) != OSIX_SUCCESS)
    {
        if (u1MsgType == OSPF_TE_RM_LINK_INFO)
        {
            if (u4DescSize != OSPF_TE_ZERO)
            {
                OSPF_TE_RM_LINK_INFO_FREE (pOspfQMsg->rmOspfTeIfParam.
                                           pIfDescriptors);
            }
            if (u4SrlgSize != OSPF_TE_ZERO)
            {
                OSPF_TE_RM_LINK_SRLG_INFO_FREE (pOspfQMsg->rmOspfTeIfParam.
                                                pSrlgs);
            }
        }
        OSPF_TE_QMSG_FREE (pOspfQMsg);
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Sending RM message to OSPF-TE "
                     "Q Failed\n");
        return OSPF_TE_FAILURE;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeEnqueueMsgFromRmgr\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeEnqueueMsgFromRsvpTe                                */
/*                                                                           */
/*  Description :  This function is called from RSVPTE for backup path       */
/*                 construction.                                             */
/*                                                                           */
/*  Input       :  tCspfCompInfo - CSPF computation info.                    */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS/OSPF_TE_FAILURE                           */
/*****************************************************************************/
INT4
OspfTeEnqueueMsgFromRsvpTe (tCspfCompInfo * pCspfCompInfo)
{
    tOspfTeQMsg        *pOspfQMsg = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeEnqueueMsgFromRsvpTe\n");

    if (gOsTeContext.u1TeAdminStat != OSPF_TE_ENABLED)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "OSPF-TE is DOWN\n");
        return OSPF_TE_FAILURE;
    }

    if (OSPF_TE_QMSG_ALLOC (pOspfQMsg, tOspfTeQMsg) == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to allocate the Q "
                     "Message\n");
        return OSPF_TE_FAILURE;
    }

    OSPF_TE_MEMCPY (&(pOspfQMsg->cspfCompInfo), pCspfCompInfo,
                    sizeof (tCspfCompInfo));
    pOspfQMsg->u4MsgType = OSPF_TE_CSPF_MSG;

    /* Send the message to OSPF-TE Queue */
    if (OSPF_TE_MSG_SEND (pOspfQMsg) != OSIX_SUCCESS)
    {
        OSPF_TE_QMSG_FREE (pOspfQMsg);
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Sending CSPF message to OSPF-TE "
                     "Q Failed\n");
        return OSPF_TE_FAILURE;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeEnqueueMsgFromRsvpTe\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeCspfFindPaths                                       */
/*                                                                           */
/*  Description :  This function receives the request for paths from         */
/*                 Application. It does CSPF computataion and returns        */
/*                 constraint based  paths.                                  */
/*                                                                           */
/*  Input       :  pCspfReq - pointer to cspf request.                       */
/*                                                                           */
/*  Output      :  aPath - Holds the paths.                                  */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS                                           */
/*                 OSPF_TE_FAILURE                                           */
/*                 OSPF_TE_MEM_FAILURE                                       */
/*****************************************************************************/
PUBLIC INT4
OspfTeCspfFindPaths (tOsTeCspfReq * pCspfReq, tOsTeAppPath * aPath)
{
    tOsTeArea          *pArea = NULL;
    tOsTePath          *pVertexPath = NULL;
    INT4                i4Status = OSPF_TE_FAILURE;
    UINT2               u2Count;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfFindPaths\n");
    OspfTeLock ();

    /* Validate the Application */
    if ((gOsTeContext.u1TeAdminStat != OSPF_TE_ENABLED) ||
        (pCspfReq->u4DestIpAddr == OSPF_TE_NULL) ||
        (pCspfReq->u1Priority >= OSPF_TE_MAX_PRIORITY_LVL))

    {
        OSPF_TE_TRC3 (OSPF_TE_CSPF_TRC,
                      "Bad CSPF Request. \n"
                      "Dest IpAddr NULL or TE not"
                      " enabled or CSPF priority >= \n"
                      "MAX priority level,  Dest IpAddr %X \n"
                      "TE Admin Status %d Priority in CSPF Request %d\n ",
                      pCspfReq->u4DestIpAddr, gOsTeContext.u1TeAdminStat,
                      pCspfReq->u1Priority);
        OspfTeUnLock ();
        return OSPF_TE_FAILURE;
    }

    if ((IS_ONLY_MPLS_LINK_CONSIDERED (pCspfReq->u1Flag))
        && (IS_ONLY_GMPLS_LINK_CONSIDERED (pCspfReq->u1Flag)))
    {
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "Bad CSPF Request. \n"
                     "BOTH MPLS and GMPLS link flag is set \n");
        OspfTeUnLock ();
        return OSPF_TE_FAILURE;
    }

    if ((IS_SEGMENT_PROTECTION_PATH (pCspfReq->u1Flag))
        && (pCspfReq->u4DestIpAddr == pCspfReq->u4WPDestIpAddr)
        && (pCspfReq->u4SrcIpAddr == pCspfReq->u4WPSrcIpAddr))
    {
        pCspfReq->u1Flag = (UINT1) (pCspfReq->u1Flag -
                                    (UINT1)
                                    OSPF_TE_SEGMENT_PROTECTION_PATH_BIT_MASK);
    }

    if ((IS_SEGMENT_PROTECTION_PATH (pCspfReq->u1Flag))
        && ((pCspfReq->u4WPDestIpAddr == OSPF_TE_NULL)
            || (pCspfReq->u4WPSrcIpAddr == OSPF_TE_NULL)))
    {
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "Bad CSPF Request for \n"
                     "Segment protection path. \n");
        OspfTeUnLock ();
        return OSPF_TE_FAILURE;
    }

    if (IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag) &&
        pCspfReq->osTeExpRoute.u2HopCount == OSPF_TE_ZERO)
    {
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "Bad CSPF Request. \n"
                     "For MBB, Primary Path should be provided\n");
        OspfTeUnLock ();
        return OSPF_TE_FAILURE;
    }

    if (pCspfReq->u1SrlgType != OSPF_TE_ZERO &&
        pCspfReq->osTeSrlg.u4NoOfSrlg == OSPF_TE_ZERO)
    {
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "Bad CSPF Request. \n"
                     "SRLG constraint is specifed without "
                     "providing SRLG Information\n");
        OspfTeUnLock ();
        return OSPF_TE_FAILURE;

    }

    if (pCspfReq->u4SrcIpAddr == OSPF_TE_NULL)
    {
        gOsTeContext.u4SrcIpAddr =
            OSPF_TE_CRU_BMC_DWFROMPDU (gOsTeContext.rtrId);
    }
    else
    {
        gOsTeContext.u4SrcIpAddr = pCspfReq->u4SrcIpAddr;
    }

    gOsTeContext.u4DestIpAddr = pCspfReq->u4DestIpAddr;

    if (pCspfReq->u2HopCount == OSPF_TE_ZERO)
    {
        pCspfReq->u2HopCount = OSPF_TE_MAX_NEXT_HOPS;
    }
    if (pCspfReq->u4MaxPathMetric == OSPF_TE_ZERO)
    {
        pCspfReq->u4MaxPathMetric = OSPF_TE_MAX_METRIC;
    }
    /* If Backup Path is specified, always copy the information in 
     * osTeExplicitPath to the global path info.
     * ASSUMPTION:
     * ----------
     * While Asking MAKE BEFORE BREAK PATH, BACKUP_PATH Bit Mask is 
     * always set and osTeExplicitPath is always filled by working path
     * information */

    if (IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag) ||
        pCspfReq->osTeExpRoute.u2Info == OSPF_TE_PRIMARY_PATH)
    {
        if (OSPF_TE_PATH_NODE_ALLOC (pVertexPath, tOsTePath) == NULL)
        {
            OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC,
                         "Memory ALLOC for Path  node failed\n");
            OspfTeUnLock ();
            return OSPF_TE_MEM_FAILURE;
        }
        gOsTeContext.aPath[OSPF_TE_NULL] = pVertexPath;
        gOsTeContext.aPath[OSPF_TE_NULL]->u2PathType = OSPF_TE_PRIMARY_PATH;
        gOsTeContext.aPath[OSPF_TE_NULL]->u2HopCount =
            pCspfReq->osTeExpRoute.u2HopCount;
        for (u2Count = OSPF_TE_NULL;
             u2Count < pCspfReq->osTeExpRoute.u2HopCount; u2Count++)
        {
            gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].
                u4RouterId =
                pCspfReq->osTeExpRoute.aNextHops[u2Count].u4RouterId;
            gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].
                u4NextHopIpAddr =
                pCspfReq->osTeExpRoute.aNextHops[u2Count].u4NextHopIpAddr;
            gOsTeContext.aPath[OSPF_TE_NULL]->aNextHops[u2Count].
                u4RemoteIdentifier =
                pCspfReq->osTeExpRoute.aNextHops[u2Count].u4RemoteIdentifier;

        }

    }

    /* Scan through each area to compute intra area routes */
    TMO_SLL_Scan (&(gOsTeContext.areasLst), pArea, tOsTeArea *)
    {
        /* Process the Cspf Request */
        if (ER_OR_AR_HOP_COUNT (pCspfReq) != OSPF_TE_ZERO)
        {
            switch (ER_OR_AR_ROUTE_INFO (pCspfReq))
            {
                case OSPF_TE_PRIMARY_PATH:
                    i4Status =
                        OspfTeCspfIntraAreaRoute (pArea, pCspfReq, aPath);
                    break;

                case OSPF_TE_STRICT_EXP_ROUTE:
                    i4Status =
                        OspfTeCspfStrictIntraAreaRoute (pArea, pCspfReq, aPath);
                    break;

                case OSPF_TE_LOOSE_EXP_ROUTE:
                    i4Status =
                        OspfTeCspfLooseIntraAreaRoute (pArea, pCspfReq, aPath);
                    break;

                case OSPF_TE_FAILED_LINK:
                    i4Status =
                        OspfTeCspfIntraAreaRoute (pArea, pCspfReq, aPath);
                    break;

                case OSPF_TE_STRICT_LOOSE_EXP_ROUTE:
                    i4Status =
                        OspfTeCspfStrictLooseIntraAreaRoute (pArea, pCspfReq,
                                                             aPath);
                    break;

                case OSPF_TE_INCLUDE_ANY:
                    i4Status =
                        OspfTeCspfIncludeAnyIntraAreaRoute (pArea, pCspfReq,
                                                            aPath);
                    break;

                default:
                    OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC,
                                 "pOsTeExpRoute Info is not set properly \n");
                    break;
            }
        }
        else
        {
            i4Status = OspfTeCspfCalculatePath (pArea, pCspfReq, aPath);
        }

        if (i4Status != OSPF_TE_FAILURE)
        {
            break;
        }
    }
    OspfTeUnLock ();

    for (u2Count = OSPF_TE_NULL; u2Count < OSPF_TE_TWO; u2Count++)
    {
        if (gOsTeContext.aPath[u2Count] != NULL)
        {
            OSPF_TE_PATH_NODE_FREE (gOsTeContext.aPath[u2Count]);
            gOsTeContext.aPath[u2Count] = NULL;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfFindPaths\n");

    return i4Status;
}

/* Following function is used in CLI callback functions. */
/*****************************************************************************/
/* Function     : OspfTeGetInterfaceNameFromIndex                            */
/*                                                                           */
/* Description  : Function to Get Name From Index                            */
/*                                                                           */
/* Input        : u4IfIndex                                                  */
/*                                                                           */
/* Output       : Name of the interface.                                     */
/*                                                                           */
/* Returns      :                                                            */
/*****************************************************************************/
PUBLIC UINT4
OspfTeGetInterfaceNameFromIndex (UINT4 u4Index, UINT1 *pu1Alias)
{
    return OspfGetInterfaceNameFromIndex (u4Index, pu1Alias);
}

/*****************************************************************************/
/* Function     : OspfTeSndMsgToTlm                                          */
/*                                                                           */
/* Description  : This function is to register with TLM to receive           */
/*                notification from TLM about TE-Links created/modified      */
/*                                                                           */
/* Input        : u1MsgType - Indicates type of interaction with TLM         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/
PUBLIC UINT4
OspfTeSndMsgToTlm (UINT1 u1MsgType)
{
    UINT4               u4Status = TLM_FAILURE;
    tTlmRegParams       TlmReg;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeSndMsgToTlm\n");

    switch (u1MsgType)
    {
        case OSPF_TE_TLM_REG:
            TlmReg.u4ModId = TLM_OSPF_TE_APP_ID;
            TlmReg.u4EventsId = TE_LINK_CREATED |
                TE_LINK_DELETED | TE_LINK_UPDATED;
            TlmReg.pFnRcvPkt = (VOID *) OspfTeTlmSendLinkInfo;
#ifdef TLM_WANTED
            u4Status = TlmApiRegisterProtocols (&TlmReg);
#endif
            break;

        case OSPF_TE_TLM_DEREG:
#ifdef TLM_WANTED
            u4Status = TlmApiDeRegisterProtocols (TLM_OSPF_TE_APP_ID);
#endif
            break;

        default:
            break;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeSndMsgToTlm\n");
#ifdef TLM_WANTED
    if (u4Status == TLM_SUCCESS)
    {
        return OSPF_TE_SUCCESS;
    }
#endif
    UNUSED_PARAM (TlmReg);
    UNUSED_PARAM (u4Status);
    return ((UINT4) OSPF_TE_FAILURE);

}

/*****************************************************************************/
/* Function     : OspfTeTlmSendLinkInfo                                      */
/*                                                                           */
/* Description  : This function is to send the TLM link information          */
/*                to the OSPF-TE                                             */
/*                                                                           */
/* Input        : pTlmLinkMsg - indicates TE-Link information from TLM       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_FAILURE                                               */
/*                OSIX_SUCCESS                                               */
/*****************************************************************************/
PRIVATE INT4
OspfTeTlmSendLinkInfo (tTlmEventNotification * pTlmLinkMsg)
{
    INT4                i4Status = OSIX_FAILURE;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeTlmSendLinkInfo\n");

    i4Status = OspfTeEnqueueMsgFromTlm (pTlmLinkMsg);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeTlmSendLinkInfo\n");
    return i4Status;
}

/*****************************************************************************/
/* Function     : OspfTeEnqueueMsgFromTlm                                    */
/*                                                                           */
/* Description  : This function is to post the message to OSPF-TE            */
/*                Queue with Te-Link Information                             */
/*                                                                           */
/* Input        : pTlmLinkMsg - indicates TE-Link information from TLM       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*                OSIX_FAILURE                                               */
/*****************************************************************************/
PRIVATE INT4
OspfTeEnqueueMsgFromTlm (tTlmEventNotification * pTlmLinkMsg)
{
    UINT4               u4DescSize = OSPF_TE_ZERO;
    UINT4               u4SrlgSize = OSPF_TE_ZERO;
    tOspfTeQMsg        *pOspfQMsg = NULL;
    UINT2               u2Count = OSPF_TE_ZERO;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeEnqueueMsgFromTlm\n");

    if (gOsTeContext.u1TeAdminStat != OSPF_TE_ENABLED)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "OSPF-TE is DOWN\n");
        return OSIX_FAILURE;
    }

    if (OSPF_TE_QMSG_ALLOC (pOspfQMsg, tOspfTeQMsg) == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to allocate the Q "
                     "Message\n");
        return OSIX_FAILURE;
    }

    /* Copy the contents from osTeLinkMsg to the Queue */
    pOspfQMsg->u4MsgType = OSPF_TE_TLM_MSG;
    pOspfQMsg->tlmOspfTeIfParam.u4ModuleId = OSPF_TE_TLM_MSG;
    pOspfQMsg->tlmOspfTeIfParam.u4LocalIpAddr =
        pTlmLinkMsg->LocalIpAddr.u4_addr[0];
    pOspfQMsg->tlmOspfTeIfParam.u4IfIndex = pTlmLinkMsg->u4IfIndex;
    pOspfQMsg->tlmOspfTeIfParam.u4RemoteIpAddr =
        pTlmLinkMsg->RemoteIpAddr.u4_addr[0];
    pOspfQMsg->tlmOspfTeIfParam.u4RemoteRtrId = pTlmLinkMsg->u4RemoteRtrId;
    pOspfQMsg->tlmOspfTeIfParam.u4TeMetric = pTlmLinkMsg->u4TeMetric;
    pOspfQMsg->tlmOspfTeIfParam.u4RsrcClassColor =
        pTlmLinkMsg->u4RsrcClassColor;
    pOspfQMsg->tlmOspfTeIfParam.u4LocalIdentifier =
        pTlmLinkMsg->u4LocalIdentifier;
    pOspfQMsg->tlmOspfTeIfParam.u4RemoteIdentifier =
        pTlmLinkMsg->u4RemoteIdentifier;
    pOspfQMsg->tlmOspfTeIfParam.maxBw = pTlmLinkMsg->maxBw;
    pOspfQMsg->tlmOspfTeIfParam.maxResBw = pTlmLinkMsg->maxResBw;
    pOspfQMsg->tlmOspfTeIfParam.u1ProtectionType =
        (UINT1) pTlmLinkMsg->i4ProtectionType;
    pOspfQMsg->tlmOspfTeIfParam.u1InfoType =
        (UINT1) (pTlmLinkMsg->u1InfoType + (UINT1) OSPF_TE_ONE);
    pOspfQMsg->tlmOspfTeIfParam.u1LinkStatus = pTlmLinkMsg->u1LinkStatus;
    pOspfQMsg->tlmOspfTeIfParam.u4TeLinkIfType = pTlmLinkMsg->u4TeLinkIfType;
    pOspfQMsg->tlmOspfTeIfParam.u4CfaPortIndex =
        pTlmLinkMsg->u4LowerCompIfCfaPortIndex;

    for (u2Count = OSPF_TE_ZERO; u2Count < OSPF_TE_MAX_PRIORITY_LVL; u2Count++)
    {
        pOspfQMsg->tlmOspfTeIfParam.aUnResBw[u2Count] =
            pTlmLinkMsg->aUnResBw[u2Count];
    }
    pOspfQMsg->tlmOspfTeIfParam.u1MsgSubType = pTlmLinkMsg->u1MsgSubType;

    if (pOspfQMsg->tlmOspfTeIfParam.u1MsgSubType != OSPF_TE_TLM_LINK_INFO)
    {
        if (OSPF_TE_MSG_SEND (pOspfQMsg) != OSIX_SUCCESS)
        {
            OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Sending TLM Msg to OSPF-TE Q "
                         "Failed\n");
            OSPF_TE_QMSG_FREE (pOspfQMsg);
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    /* copy the descriptor information from osTeLinkMsg to Queue Memory */
    u4DescSize = pTlmLinkMsg->u4IfDescCnt * sizeof (tOsTeIfDesc);
    pOspfQMsg->tlmOspfTeIfParam.u4IfDescCnt = pTlmLinkMsg->u4IfDescCnt;
    /* Allocating memory for interface descriptors information */
    if ((u4DescSize != OSPF_TE_ZERO) &&
        (OSPF_TE_RM_LINK_INFO_ALLOC
         (pOspfQMsg->tlmOspfTeIfParam.pIfDescriptors, UINT1) == NULL))
    {
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Failed to allocate Memory for "
                     "interface descriptors\n");
        OSPF_TE_QMSG_FREE (pOspfQMsg);
        return OSIX_FAILURE;
    }
    /* Copying the interface descriptors information into Queue memory */
    OSPF_TE_MEMCPY (pOspfQMsg->tlmOspfTeIfParam.pIfDescriptors,
                    pTlmLinkMsg->aIfDescriptors, u4DescSize);

    /* copy the SRLG information from osTeLinkMsg to Queue Memory */
    u4SrlgSize = pTlmLinkMsg->Srlg.u4NoOfSrlg * sizeof (UINT4);
    pOspfQMsg->tlmOspfTeIfParam.u4IfSrlgCnt = pTlmLinkMsg->Srlg.u4NoOfSrlg;
    /* Allocating memory for SRLG numbers */
    if ((u4SrlgSize != OSPF_TE_ZERO) &&
        (OSPF_TE_RM_LINK_SRLG_INFO_ALLOC
         (pOspfQMsg->tlmOspfTeIfParam.pSrlgs, UINT1)) == NULL)
    {
        if (u4DescSize != OSPF_TE_ZERO)
        {
            OSPF_TE_RM_LINK_INFO_FREE (pOspfQMsg->tlmOspfTeIfParam.
                                       pIfDescriptors);
        }
        OSPF_TE_QMSG_FREE (pOspfQMsg);
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                     "Failed to allocate Memory for SRLG Numbers\n");
        return OSIX_FAILURE;
    }
    /* Copying the SRLG numbers information into Q memory */
    OSPF_TE_MEMCPY (pOspfQMsg->tlmOspfTeIfParam.pSrlgs,
                    pTlmLinkMsg->Srlg.aSrlgNumber, u4SrlgSize);

    if (OSPF_TE_MSG_SEND (pOspfQMsg) != OSIX_SUCCESS)
    {
        if (u4DescSize != OSPF_TE_ZERO)
        {
            OSPF_TE_RM_LINK_INFO_FREE (pOspfQMsg->tlmOspfTeIfParam.
                                       pIfDescriptors);
        }
        if (u4SrlgSize != OSPF_TE_ZERO)
        {
            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pOspfQMsg->tlmOspfTeIfParam.pSrlgs);
        }
        OSPF_TE_QMSG_FREE (pOspfQMsg);
        OSPF_TE_TRC (OSPF_TE_FAILURE_TRC, "Sending TLM message to OSPF-TE "
                     "Q Failed\n");
        return OSIX_FAILURE;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeEnqueueMsgFromTlm\n");
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*  Function    :  OspfTeIsEnabled                                           */
/*                                                                           */
/*  Description :  This function is an API provided to other modules to get  */
/*                 the OSPF-TE Admin Status.                                 */
/*                                                                           */
/*  Input       :  None                                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  TRUE   - If OSPFTE is Enabled                             */
/*                 FALSE  - If OSPFTE is Disabled                            */
/*****************************************************************************/
PUBLIC BOOLEAN
OspfTeIsEnabled ()
{
    BOOLEAN             b1Flag = FALSE;
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeIsEnabled\n");
    OspfTeLock ();
    if (gOsTeContext.u1TeAdminStat == OSPF_TE_ENABLED)
    {
        b1Flag = TRUE;
    }
    OspfTeUnLock ();
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeIsEnabled\n");
    return b1Flag;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osteport.c                     */
/*-----------------------------------------------------------------------*/
