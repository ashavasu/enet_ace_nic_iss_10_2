/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: osteif.c,v 1.5 2011/12/14 13:27:35 siva Exp $
*
* Description:  This file has the routines to for managing 
*               OSPFTE interfaces.
*********************************************************************/

#include "osteinc.h"

/*****************************************************************************/
/*  Function    :  OspfTeIfCreate                                            */
/*                                                                           */
/*  Description :  This function creates a TE interface and fills with       */
/*                 default values. It then adds this node into Interface     */
/*                 Hash Table. It also adds this node into sortTeIfLst       */
/*                 in OSPF-TE context.                                       */
/*                                                                           */
/*  Input       :  teIfIpAddr    - Holds the interface IP Address            */
/*                 u4AddrlessIf  - Holds the interface index                 */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  pOsTeIfNode if creation is successful, Otherwise          */
/*                 NULL                                                      */
/*****************************************************************************/
PUBLIC tOsTeInterface *
OspfTeIfCreate (tIPADDR teIfIpAddr, UINT4 u4AddrlessIf)
{
    tOsTeInterface     *pOsTeIf = NULL;

    /* Allocate block for TE Interface data structure */
    if (OSPF_TE_IF_ALLOC (pOsTeIf, tOsTeInterface) == NULL)
    {
        OSPF_TE_TRC2 (OSPF_TE_CRITICAL_TRC,
                      "Interface (Ip: 0x%x Index: %d) Alloc Failure\n",
                      OSPF_TE_CRU_BMC_DWFROMPDU (teIfIpAddr), u4AddrlessIf);
        return NULL;
    }

    /* Set the default values for the TE interface members */
    OsTeIfSetDefaultValues (pOsTeIf);
    IP_ADDR_COPY (pOsTeIf->localIpAddr, teIfIpAddr);
    pOsTeIf->u4AddrlessIf = u4AddrlessIf;

    OSPF_TE_TRC2 (OSPF_TE_CONTROL_PLANE_TRC,
                  "Interface (Ip: 0x%x Index: %d) Creation is successful\n",
                  OSPF_TE_CRU_BMC_DWFROMPDU (teIfIpAddr), u4AddrlessIf);

    /* Add this TE Interface to TE Interface Hash Table */
    TMO_HASH_Add_Node (gOsTeContext.pTeIfHashTbl, &(pOsTeIf->NextLinkNode),
                       OspfTeIfHashFunc (&(pOsTeIf->localIpAddr),
                                         pOsTeIf->u4AddrlessIf), NULL);

    /* Add this TE Interface to Sort Interface List */
    OspfTeIfAddToSortIfLst (pOsTeIf);

    return pOsTeIf;
}

/*****************************************************************************/
/*  Function    :  OsTeIfSetDefaultValues                                    */
/*                                                                           */
/*  Description :  This function set the default values to the TE interafce  */
/*                 members.                                                  */
/*                                                                           */
/*  Input       :  pOsTeIf - Points to the TE Interface                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OsTeIfSetDefaultValues (tOsTeInterface * pOsTeIf)
{
    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "OsTeIfSetDefaultValues\n");

    MEMSET (pOsTeIf, OSPF_TE_ZERO, sizeof (tOsTeInterface));

    /* Initializing interface descriptors list */
    TMO_SLL_Init (&(pOsTeIf->ifDescrList));
    /* Initializing Type 9 LSA list */
    TMO_SLL_Init (&(pOsTeIf->type9TELsaLst));

    pOsTeIf->u1LinkType = OSPF_TE_INVALID;

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "OsTeIfSetDefaultValues\n");
    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeIfAddToSortIfLst                                    */
/*                                                                           */
/*  Description :  This module adds the Interface node to the SortIfLst      */
/*                                                                           */
/*  Input       :  pTeInterface - Points to the TE Interface Node to be added*/
/*                                to the SortIfLst                           */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeIfAddToSortIfLst (tOsTeInterface * pTeInterface)
{
    tOsTeInterface     *pLstTeIf = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "OspfTeIfAddToSortIfLst\n");

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pLstTeIf = OSPF_TE_GET_BASE_PTR (tOsTeInterface, NextSortLinkNode,
                                         pLstNode);

        if (OspfTeUtilIpAddrIndComp (pTeInterface->localIpAddr,
                                     pTeInterface->u4AddrlessIf,
                                     pLstTeIf->localIpAddr,
                                     pLstTeIf->u4AddrlessIf) == OSPF_TE_LESS)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }

    TMO_SLL_Insert (&(gOsTeContext.sortTeIfLst), pPrevLstNode,
                    &(pTeInterface->NextSortLinkNode));

    OSPF_TE_TRC2 (OSPF_TE_CONTROL_PLANE_TRC,
                  "Interface (Ip: 0x%x Index: %d) Node added to sortTeIfLst "
                  "in OSPF-TE context\n",
                  OSPF_TE_CRU_BMC_DWFROMPDU (pTeInterface->localIpAddr),
                  pTeInterface->u4AddrlessIf);
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "OspfTeIfAddToSortIfLst\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeIfAddToSortOspfIntIfLst                             */
/*                                                                           */
/*  Description :  This module adds the OSPF information interface node      */
/*                 to the ospfInfoIfLst                                      */
/*                                                                           */
/*  Input       :  pOsTeOsIntInfo - Points to the Interface Node             */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeIfAddToSortOspfIntIfLst (tOsTeOsIntInfo * pOsTeOsIntInfo)
{
    tOsTeOsIntInfo     *pLstIf = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "OspfTeIfAddToSortOspfIntIfLst\n");

    TMO_SLL_Scan (&(gOsTeContext.ospfInfoIfLst), pLstIf, tOsTeOsIntInfo *)
    {
        if (OspfTeUtilIpAddrIndComp (pOsTeOsIntInfo->ifIpAddr,
                                     pOsTeOsIntInfo->u4AddrlessIf,
                                     pLstIf->ifIpAddr,
                                     pLstIf->u4AddrlessIf) == OSPF_TE_LESS)
        {
            break;
        }
        pPrevLstNode = &(pLstIf->NextOsIntInfoNode);
    }

    TMO_SLL_Insert (&(gOsTeContext.ospfInfoIfLst), pPrevLstNode,
                    &(pOsTeOsIntInfo->NextOsIntInfoNode));

    OSPF_TE_TRC2 (OSPF_TE_CONTROL_PLANE_TRC,
                  "Interface (Ip: 0x%x Index: %d) Node added to sortTeIfLst "
                  "in OSPF-TE context\n",
                  OSPF_TE_CRU_BMC_DWFROMPDU (pOsTeOsIntInfo->ifIpAddr),
                  pOsTeOsIntInfo->u4AddrlessIf);
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "OspfTeIfAddToSortOspfIntIfLst\n");

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeFindTeInterface                                     */
/*                                                                           */
/*  Description :  This function finds the TE interface in the by searching  */
/*                 in TE interface Hash Table.                               */
/*                                                                           */
/*  Input       :  ifIpAddr     - Interface IP Address                       */
/*              :  u4AddrlessIf - Addressless interface index                */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to interface if the interface is present          */
/*                 otherwise NULL                                            */
/*****************************************************************************/
PUBLIC tOsTeInterface *
OspfTeFindTeInterface (tIPADDR ifIpAddr, UINT4 u4AddrlessIf)
{
    tOsTeInterface     *pTeIfNode = NULL;
    UINT4               u4HashIndex;

    u4HashIndex = OspfTeIfHashFunc ((tIPADDR *) ifIpAddr, u4AddrlessIf);
    TMO_HASH_Scan_Bucket (gOsTeContext.pTeIfHashTbl, u4HashIndex,
                          pTeIfNode, tOsTeInterface *)
    {
        if ((OspfTeUtilIpAddrComp (ifIpAddr, pTeIfNode->localIpAddr)
             == OSPF_TE_EQUAL) && (u4AddrlessIf == pTeIfNode->u4AddrlessIf))
        {
            OSPF_TE_TRC2 (OSPF_TE_CONTROL_PLANE_TRC,
                          "Interface (Ip: 0x%x Index: %d) Node found\n",
                          OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr), u4AddrlessIf);
            return pTeIfNode;
        }
    }

    OSPF_TE_TRC2 (OSPF_TE_CONTROL_PLANE_TRC,
                  "Interface (Ip: 0x%x Index: %d) Node not found\n",
                  OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr), u4AddrlessIf);
    return NULL;
}

/*****************************************************************************/
/*  Function    :  OspfTeFindTeInterfaceWithIndex                            */
/*                                                                           */
/*  Description :  This function finds the TE interface in the by searching  */
/*                 in TE interface Hash Table.                               */
/*                                                                           */
/*  Input       :  u4IfIndex    - Interface Index of the interface           */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to interface if the interface is present          */
/*                 otherwise NULL                                            */
/*****************************************************************************/
PUBLIC tOsTeInterface *
OspfTeFindTeInterfaceWithIndex (UINT4 u4IfIndex)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tOsTeInterface     *pTeInterface = NULL;

    TMO_SLL_Scan (&(gOsTeContext.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pTeInterface = OSPF_TE_GET_BASE_PTR (tOsTeInterface,
                                             NextSortLinkNode, pLstNode);
        if (pTeInterface->u4IfIndex == u4IfIndex)
        {
            return pTeInterface;
        }
    }

    OSPF_TE_TRC1 (OSPF_TE_CONTROL_PLANE_TRC,
                  "Interface (Index: %d) Node not found\n", u4IfIndex);
    return NULL;
}

/*****************************************************************************/
/*  Function    :  OspfTeFindOspfInfoInt                                     */
/*                                                                           */
/*  Description :  This function finds the interface in OSPF information     */
/*                 interface list                                            */
/*                                                                           */
/*  Input       :  ifIpAddr     - Interface IP Address                       */
/*              :  u4AddrlessIf - Addressless interface index                */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to interface if the interface is present          */
/*                 otherwise NULL                                            */
/*****************************************************************************/
PUBLIC tOsTeOsIntInfo *
OspfTeFindOspfInfoInt (tIPADDR ifIpAddr, UINT4 u4AddrlessIf)
{
    tOsTeOsIntInfo     *pOsInfoIfNode = NULL;

    /* Scan the interface list to find the interface */
    TMO_SLL_Scan (&(gOsTeContext.ospfInfoIfLst), pOsInfoIfNode,
                  tOsTeOsIntInfo *)
    {
        if ((OspfTeUtilIpAddrComp (ifIpAddr, pOsInfoIfNode->ifIpAddr)
             == OSPF_TE_EQUAL) && (u4AddrlessIf == pOsInfoIfNode->u4AddrlessIf))
        {
            OSPF_TE_TRC2 (OSPF_TE_CONTROL_PLANE_TRC,
                          "Interface (Ip: 0x%x Index: %d) Node found\n",
                          OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr), u4AddrlessIf);

            return pOsInfoIfNode;
        }
    }

    OSPF_TE_TRC2 (OSPF_TE_CONTROL_PLANE_TRC,
                  "Interface (Ip: 0x%x Index: %d) Node not found\n",
                  OSPF_TE_CRU_BMC_DWFROMPDU (ifIpAddr), u4AddrlessIf);
    return NULL;
}

/*****************************************************************************/
/* Function     : OspfTeIfHashFunc                                           */
/*                                                                           */
/* Description  : This function gives the hash value of the key.             */
/*                                                                           */
/* Input        : pIfIpAddr    - pointer to Interface IP Address             */
/*                u4AddrlessIf - Addressless Interface index value           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hashed value of the Key.                                   */
/*****************************************************************************/
PUBLIC UINT4
OspfTeIfHashFunc (tIPADDR * pIfIpAddr, UINT4 u4AddrlessIf)
{
    UINT1               au1Buf[OSPF_TE_IF_HASH_TBL_SIZE];

    IP_ADDR_COPY (au1Buf, pIfIpAddr);
    OSPF_TE_MEMCPY ((au1Buf + MAX_IP_ADDR_LEN),
                    (UINT1 *) &u4AddrlessIf, MAX_IP_ADDR_LEN);

    return (OspfTeUtilHashGetValue (OSPF_TE_IF_HASH_TBL_SIZE, au1Buf,
                                    OSPF_TE_IF_HASH_KEY_SIZE));
}

/*****************************************************************************/
/*  Function    :  OspfTeIfDeleteTeLink                                      */
/*                                                                           */
/*  Description :  This function deletes the TE link from TE links list      */
/*                                                                           */
/*  Input       :  pTeLink - Pointer to TE link which is to be deleted       */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeIfDeleteTeLink (tOsTeInterface * pOsTeLink)
{
    UINT4               u4HashIndex;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;

    /* Delete all the interface descriptors */
    OSPF_TE_DYNM_SLL_SCAN (&(pOsTeLink->ifDescrList), pLstNode,
                           pTempNode, tTMO_SLL_NODE *)
    {
        TMO_SLL_Delete (&(pOsTeLink->ifDescrList), pLstNode);
        OSPF_TE_IF_DESC_FREE ((tOsTeIfDescNode *) pLstNode);
    }

    /* Delete any Type 9 Lsas, if present  */
    OSPF_TE_DYNM_SLL_SCAN (&(pOsTeLink->type9TELsaLst), pLstNode,
                           pTempNode, tTMO_SLL_NODE *)
    {
        pOsTeLsaNode = OSPF_TE_GET_BASE_PTR (tOsTeLsaNode, NextSortLsaNode,
                                             pLstNode);
        TMO_SLL_Delete (&(pOsTeLink->type9TELsaLst), pLstNode);
        OSPF_TE_LSA_INFO_FREE (pOsTeLsaNode);
    }

    u4HashIndex = OspfTeIfHashFunc (&(pOsTeLink->localIpAddr),
                                    pOsTeLink->u4AddrlessIf);

    /* Deleting Node from Global Hash Table */
    TMO_HASH_Delete_Node (gOsTeContext.pTeIfHashTbl,
                          &(pOsTeLink->NextLinkNode), u4HashIndex);

    /* Deleting from Sorted TE interface list */
    TMO_SLL_Delete (&(gOsTeContext.sortTeIfLst),
                    (tTMO_SLL_NODE *) & (pOsTeLink->NextSortLinkNode));

    /* Freeing the memory of the TE interface */
    OSPF_TE_IF_FREE (pOsTeLink);

    return;
}

/*****************************************************************************/
/*  Function    :  OspfTeIfDeleteAllOspfInfoInt                              */
/*                                                                           */
/*  Description :  This function deletes the Ospf information interfaces     */
/*                                                                           */
/*  Input       :  None                                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeIfDeleteAllOspfInfoInt (VOID)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tOsTeOsIntInfo     *pOsTeOsIntInfo = NULL;
    /* Delete all the interface descriptors */
    OSPF_TE_DYNM_SLL_SCAN (&(gOsTeContext.ospfInfoIfLst), pLstNode,
                           pTempNode, tTMO_SLL_NODE *)
    {
        pOsTeOsIntInfo = OSPF_TE_GET_BASE_PTR (tOsTeOsIntInfo,
                                               NextOsIntInfoNode, pLstNode);

        if (pOsTeOsIntInfo->pType9Lsa != NULL)
        {
            OSPF_TE_LSA_FREE (pOsTeOsIntInfo->pType9Lsa->pLsa);
            OSPF_TE_LSA_INFO_FREE (pOsTeOsIntInfo->pType9Lsa);
        }

        TMO_SLL_Delete (&(gOsTeContext.ospfInfoIfLst), pLstNode);
        OSPF_TE_OSPF_INFO_IF_FREE (pOsTeOsIntInfo);
    }
}

/*****************************************************************************/
/*  Function    :  OspfTeIfDeleteAllOsTeInterface                            */
/*                                                                           */
/*  Description :  This function deletes the Te-Link Information             */
/*                                                                           */
/*  Input       :  None                                                      */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  None                                                      */
/*****************************************************************************/
PUBLIC VOID
OspfTeIfDeleteAllOsTeInterface (VOID)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tOsTeInterface     *pOsTeInterface = NULL;

    /* Delete all OsTeInterface -> TE Link Information Received from TLM/RM */
    OSPF_TE_DYNM_SLL_SCAN (&(gOsTeContext.sortTeIfLst), pLstNode,
                           pTempNode, tTMO_SLL_NODE *)
    {
        pOsTeInterface = OSPF_TE_GET_BASE_PTR
            (tOsTeInterface, NextSortLinkNode, pLstNode);
        OspfTeIfDeleteTeLink (pOsTeInterface);
    }

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  osteif.c                       */
/*-----------------------------------------------------------------------*/
