/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: ostecspf.c,v 1.19 2016/04/21 10:36:43 siva Exp $
*
* Description:  This file has the CSPF Algorithm routines.
*********************************************************************/

#include "osteinc.h"

/*****************************************************************************/
/* Function     : OspfTeCspfDetectLoop                                       */
/*                                                                           */
/* Description  : This function scan the candtenode path list and compare    */
/*                u4VertexId with all next hop router Id present in path.    */
/*                                                                           */
/* Input        : u4VertexId   - RouterId                                    */
/*                tOsTeCandteNode - pParent                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                           */
/*                OSPF_TE_FAILURE                                          */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfDetectLoop (tOsTeCandteNode * pParent, UINT4 u4VertexId,
                      UINT1 u1VertexType)
{
    tOsTePath          *pPath = NULL;
    UINT2               u2Count;
    UINT2               u2PathCount = OSPF_TE_NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfDetectLoop\n");

    if ((gOsTeContext.u4SrcIpAddr == u4VertexId) &&
        (u1VertexType == OSPF_TE_VERT_ROUTER))
    {
        return OSPF_TE_FAILURE;
    }

    TMO_SLL_Scan (&pParent->pathLst, pPath, tOsTePath *)
    {
        for (u2Count = OSPF_TE_NULL; u2Count < pPath->u2HopCount; u2Count++)
        {
            if ((pPath->aNextHops[u2Count].u4RouterId == u4VertexId)
                && (u1VertexType == OSPF_TE_VERT_ROUTER))
            {
                u2PathCount++;
            }
        }
    }
    if (TMO_SLL_Count (&pParent->pathLst) != OSPF_TE_NULL)
    {
        if (TMO_SLL_Count (&pParent->pathLst) == u2PathCount)
        {
            return OSPF_TE_FAILURE;
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfDetectLoop\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfUpdateCandteLst                                  */
/*                                                                           */
/* Description  : This function  updates the candidate to the candidate list */
/*                If the candidate is already present in the candidate list, */
/*                this function updates the information of the candidate.    */
/*                                                                           */
/* Input        : pLinkNode - Link node of the parent vertex                 */
/*                pNewVertex - Vertex whose links are processed              */
/*                pArea - Points to the Area node                            */
/*                pCspfReq - Vertex whose links are processed                */
/*                                                                           */
/* Output       : pTeOutputNode                                              */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*                OSPF_TE_MEM_FAILURE                                        */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfUpdateCandteLst (tOsTeCandteNode * pNewVertex,
                           tOsTeLinkNode * pLinkNode,
                           tOsTeArea * pArea, tOsTeCspfReq * pCspfReq)
{
    tOsTeCandteNode    *pCandteNode = NULL;
    UINT4               u4TeMetric;
    UINT1               u1LsaType;
    UINT1               u1VertType;
    UINT4               u4VertexId;
    INT4                i4Status = OSPF_TE_SUCCESS;
    UINT1              *pPtr = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfUpdateCandteLst\n");

    if ((pNewVertex->u1VertexType == OSPF_TE_VERT_ROUTER) &&
        (pLinkNode->u1LinkType == OSPF_TE_MULTIACCESS))
    {
        u1VertType = OSPF_TE_VERT_NETWORK;
        u1LsaType = OSPF_TE_NETWORK_LSA;
    }
    else
    {
        u1VertType = OSPF_TE_VERT_ROUTER;
        u1LsaType = OSPF_TE_TYPE10_LSA;
    }

    u4VertexId = ((pNewVertex->u1VertexType == OSPF_TE_VERT_ROUTER) ?
                  pLinkNode->pOsTeLsaNode->pLinkTlv->u4LinkId :
                  pLinkNode->u4LinkId);

    if ((pPtr =
         OspfTeCspfSearchDatabase (u1LsaType, u4VertexId, pArea)) == NULL)
    {
        OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
                      "LSA node not present Lsa Type  %d Vertex Id %d\n",
                      u1LsaType, u4VertexId);

        return OSPF_TE_FAILURE;
    }

    if (IS_BI_DIRECTION_PATH (pCspfReq->u1Flag))
    {
        /* Check Link back to parent is present or not */
        if (OspfTeCspfSearchLsaLinks
            (pPtr, pNewVertex, u1VertType, pLinkNode, pArea, pCspfReq)
            == OSPF_TE_FAILURE)
        {
            OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC,
                          "Link Back To Parent Not Present Parent %x Dstn %x\n",
                          pNewVertex->u4VertexId, u4VertexId);

            return OSPF_TE_FAILURE;
        }
    }
    else
    {
        if (OspfTeCspfSearchUniLinks
            (pPtr, pNewVertex, u1VertType, pLinkNode, pArea, pCspfReq)
            == OSPF_TE_FAILURE)
        {
            return OSPF_TE_FAILURE;
        }

    }

    /* Check for Loop */
    if (OspfTeCspfDetectLoop (pNewVertex, u4VertexId, u1VertType) ==
        OSPF_TE_FAILURE)
    {
        return OSPF_TE_FAILURE;
    }

    /* Get the cost */

    u4TeMetric = ((pNewVertex->u1VertexType == OSPF_TE_VERT_ROUTER) ?
                  pLinkNode->pOsTeLsaNode->pLinkTlv->u4TeMetric :
                  pLinkNode->u4TeMetric);

    /* Check if the node is present in the Candidate List */

    pCandteNode = OspfTeCspfSearchCandteLst (u4VertexId, u1VertType);

    if (pCandteNode != NULL)
    {
        if ((pCandteNode->u4TeMetric) > (pNewVertex->u4TeMetric + u4TeMetric))
        {
            pCandteNode->u4TeMetric = pNewVertex->u4TeMetric + u4TeMetric;
        }

        pCandteNode->u4ParentId = pNewVertex->u4VertexId;

        /* This adds all the hops to reach the destination */
        i4Status = OspfTeCspfSetNextHops (pNewVertex, pCandteNode,
                                          pCspfReq, pLinkNode, pArea);

        if (i4Status != OSPF_TE_SUCCESS)
        {
            OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                         "UNABLE to set NEXT HOPS to Candidate Node\n");
            return OSPF_TE_FAILURE;
        }

    }
    else
    {
        pCandteNode = OspfTeCspfGetCandteNode (u4VertexId, u1VertType);

        if (pCandteNode == NULL)
        {
            OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC,
                         "Candidate node ALLOC failed \n");
            return OSPF_TE_MEM_FAILURE;
        }

        if (pCandteNode->u1VertexType == OSPF_TE_VERT_ROUTER)
        {
            pCandteNode->pTeDbNode = (tOsTeDbNode *) (VOID *) pPtr;
        }
        else
        {
            pCandteNode->pTeLsaNode = (tOsTeLsaNode *) (VOID *) pPtr;
        }

        i4Status = OspfTeCspfSetNextHops (pNewVertex, pCandteNode,
                                          pCspfReq, pLinkNode, pArea);

        if (i4Status != OSPF_TE_SUCCESS)
        {
            OSPF_TE_TRC (OSPF_TE_FAILURE_TRC,
                         "UNABLE to set NEXT HOPS to Candidate Node\n");
            OspfTeCspfFreeCandteNode (pCandteNode);
            return OSPF_TE_FAILURE;
        }

        pCandteNode->u4TeMetric = pNewVertex->u4TeMetric + u4TeMetric;
        pCandteNode->u4ParentId = pNewVertex->u4VertexId;

        TMO_HASH_Add_Node (gOsTeContext.pTeCandteLst,
                           &(pCandteNode->NextCandteNode),
                           OspfTeCspfHashFunc (pCandteNode->u1VertexType,
                                               pCandteNode->u4VertexId), NULL);
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfUpdateCandteLst\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfSetNextHops                                      */
/*                                                                           */
/* Description  : This Function adds next hops to the vertex node to reach   */
/*                the destination.                                           */
/*                                                                           */
/* Input        : pVertex     - pointer to Vertex Candidate Node.            */
/*                pParent     - pointer to Parent Candidate Node.            */
/*                pCspfReq    - pointer to CPSF request.                     */
/*                pLinkNode   - pointer to Link Node.                        */
/*                pArea       - pointer to Area.                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfSetNextHops (tOsTeCandteNode * pParent,
                       tOsTeCandteNode * pVertex, tOsTeCspfReq * pCspfReq,
                       tOsTeLinkNode * pLinkNode, tOsTeArea * pArea)
{
    tOsTePath          *pParentPath = NULL;
    tOsTePath          *pTmpParentPath = NULL;
    tOsTePath          *pVertexPath = NULL;
    UINT4               u4TeMetric;
    UINT2               u2Count;
    UINT1               u1Flag = OSPF_TE_FALSE;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfSetNextHops\n");

    u4TeMetric = ((pParent->u1VertexType == OSPF_TE_VERT_ROUTER) ?
                  pLinkNode->pOsTeLsaNode->pLinkTlv->u4TeMetric :
                  pLinkNode->u4TeMetric);

    /* If Parent is root or Parent's parent id is root */
    if ((pParent->u4VertexId == gOsTeContext.u4SrcIpAddr) ||
        ((pParent->u4ParentId == gOsTeContext.u4SrcIpAddr)
         && (pParent->u1VertexType == OSPF_TE_VERT_NETWORK)))
    {

        if (pVertex->u1VertexType == OSPF_TE_VERT_ROUTER)
        {

            if (u4TeMetric > pCspfReq->u4MaxPathMetric)
            {
                return OSPF_TE_FAILURE;
            }
            if (OSPF_TE_PATH_NODE_ALLOC (pVertexPath, tOsTePath) == NULL)
            {
                OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC,
                             "Memory ALLOC for Path  node failed\n");
                return OSPF_TE_MEM_FAILURE;
            }

            OSPF_TE_MEMSET (pVertexPath, OSPF_TE_ZERO, sizeof (tOsTePath));
            if (OspfTeCspfUpdateNextHop
                (pVertexPath, pVertex, pParent, pLinkNode, pArea,
                 pCspfReq) == OSPF_TE_FAILURE)
            {
                return OSPF_TE_FAILURE;
            }

            if (pParent->u1VertexType == OSPF_TE_VERT_NETWORK)
            {
                pVertexPath->u4TeMetric = pParent->u4TeMetric;
            }
        }

        return OSPF_TE_SUCCESS;
    }

    /* Scan the Parent's Path List */
    OSPF_TE_DYNM_SLL_SCAN (&(pParent->pathLst), pParentPath, pTmpParentPath,
                           tOsTePath *)
    {
        if (pParentPath->u2HopCount >= pCspfReq->u2HopCount)
        {
            TMO_SLL_Delete (&pParent->pathLst, &(pParentPath->NextPathNode));
            OSPF_TE_PATH_NODE_FREE (pParentPath);
            continue;
        }
        if ((pParentPath->u4TeMetric + u4TeMetric) > pCspfReq->u4MaxPathMetric)
        {
            continue;
        }

        for (u2Count = OSPF_TE_NULL; u2Count < pParentPath->u2HopCount;
             u2Count++)
        {
            if (pParentPath->aNextHops[u2Count].u4RouterId ==
                pVertex->u4VertexId)
            {
                u1Flag = OSPF_TE_TRUE;
                break;
            }
        }

        if (u1Flag == OSPF_TE_TRUE)
        {
            u1Flag = OSPF_TE_FALSE;
            continue;
        }

        if (OSPF_TE_PATH_NODE_ALLOC (pVertexPath, tOsTePath) == NULL)
        {
            OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC,
                         "Memory ALLOC for Path  node failed\n");
            return OSPF_TE_MEM_FAILURE;
        }

        OSPF_TE_MEMSET (pVertexPath, OSPF_TE_ZERO, sizeof (tOsTePath));

        /* Copy the Path Info from Parent to Vertex */
        pVertexPath->u2HopCount = pParentPath->u2HopCount;
        pVertexPath->u4TeMetric = pParentPath->u4TeMetric;
        pVertexPath->pOsTeSrcLinkTlv = pParentPath->pOsTeSrcLinkTlv;

        for (u2Count = OSPF_TE_NULL;
             u2Count < pParentPath->u2HopCount; u2Count++)
        {
            OSPF_TE_MEMCPY (&pVertexPath->aNextHops[u2Count],
                            &pParentPath->aNextHops[u2Count],
                            sizeof (tOsTeNextHop));
            OSPF_TE_MEMCPY (&pVertexPath->aSrlg[u2Count].aSrlgNumber,
                            &pParentPath->aSrlg[u2Count].aSrlgNumber,
                            (pParentPath->aSrlg[u2Count].u4NoOfSrlg *
                             MAX_IP_ADDR_LEN));
            pVertexPath->aSrlg[u2Count].u4NoOfSrlg =
                pParentPath->aSrlg[u2Count].u4NoOfSrlg;
        }

        if (pVertex->u1VertexType == OSPF_TE_VERT_ROUTER)
        {
            if (OspfTeCspfUpdateNextHop
                (pVertexPath, pVertex, pParent, pLinkNode, pArea,
                 pCspfReq) == OSPF_TE_FAILURE)
            {
                continue;
            }
        }
        else
        {
            pVertexPath->u4TeMetric = pParentPath->u4TeMetric + u4TeMetric;
            TMO_SLL_Add (&pVertex->pathLst, &(pVertexPath->NextPathNode));
        }

    }

    if (TMO_SLL_Count (&pParent->pathLst) == OSPF_TE_NULL)
    {
        return OSPF_TE_FAILURE;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfSetNextHops\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfUpdateNextHop                                    */
/*                                                                           */
/* Description  : This function Update the next hop of the vertex node to    */
/*                reach the destination.                                     */
/*                                                                           */
/* Input        : pVertexPath - pointer to Vertex Path.                      */
/*              : pVertex     - pointer to Vertex Candidate Node.            */
/*                pParent     - pointer to Parent Candidate Node.            */
/*                pCspfReq    - pointer to CPSF request.                     */
/*                pLinkNode   - pointer to Link Node.                        */
/*                pArea       - pointer to Area.                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfUpdateNextHop (tOsTePath * pVertexPath, tOsTeCandteNode * pVertex,
                         tOsTeCandteNode * pParent, tOsTeLinkNode * pLinkNode,
                         tOsTeArea * pArea, tOsTeCspfReq * pCspfReq)
{
    UINT4               u4TeMetric;
    UINT1               u1LinkType;
    UINT1               u1Flag = OSPF_TE_FALSE;
    UINT1              *pPtr = NULL;
    UINT1              *pPtr1 = NULL;
    tOsTeDbNode        *pOsTeDbNode = NULL;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;
    tOsTeDbNode        *pOsTeDbNode1 = NULL;
    tOsTeLsaNode       *pOsTeLsaNode1 = NULL;
    UINT4               u4CopySrlg = OSPF_TE_NULL;
    UINT4               u4CopySrlg1 = OSPF_TE_NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfUpdateNextHop\n");

    u4TeMetric = ((pParent->u1VertexType == OSPF_TE_VERT_ROUTER) ?
                  pLinkNode->pOsTeLsaNode->pLinkTlv->u4TeMetric :
                  pLinkNode->u4TeMetric);

    u1LinkType = (UINT1) ((pParent->u1VertexType == OSPF_TE_VERT_ROUTER) ?
                          pLinkNode->pOsTeLsaNode->pLinkTlv->u1LinkType :
                          pLinkNode->u1LinkType);

    if (u1LinkType == OSPF_TE_PTOP)
    {
        pVertexPath->u4TeMetric = pVertexPath->u4TeMetric + u4TeMetric;
        pVertexPath->aNextHops[pVertexPath->u2HopCount].u4RouterId =
            pVertex->u4VertexId;
        pVertexPath->aNextHops[pVertexPath->u2HopCount].u4NextHopIpAddr =
            pLinkNode->pOsTeLsaNode->pLinkTlv->u4RemoteIpAddr;
        pVertexPath->aNextHops[pVertexPath->u2HopCount].u4RemoteIdentifier =
            pLinkNode->pOsTeLsaNode->pLinkTlv->u4RemoteIdentifier;

        u4CopySrlg = pLinkNode->pOsTeLsaNode->pLinkTlv->osTeSrlg.u4NoOfSrlg;
        OSPF_TE_MEMCPY (&pVertexPath->aSrlg[pVertexPath->u2HopCount],
                        &pLinkNode->pOsTeLsaNode->pLinkTlv->osTeSrlg,
                        sizeof (tOsTeSrlg));

        if (pParent->u4VertexId == gOsTeContext.u4SrcIpAddr)
        {
            pVertexPath->pOsTeSrcLinkTlv = pLinkNode->pOsTeLsaNode->pLinkTlv;
        }
        if ((pPtr = OspfTeCspfSearchDatabase
             (OSPF_TE_TYPE10_LSA, pVertex->u4VertexId, pArea)) == NULL)
        {
            OSPF_TE_PATH_NODE_FREE (pVertexPath);
            return OSPF_TE_FAILURE;
        }
        pOsTeDbNode = (tOsTeDbNode *) (VOID *) pPtr;

        TMO_SLL_Scan (&pOsTeDbNode->TeLsaLst, pOsTeLsaNode, tOsTeLsaNode *)
        {
            if (pOsTeLsaNode->pLinkTlv == NULL)
            {
                continue;
            }
            if ((pOsTeLsaNode->pLinkTlv->u4LocalIpAddr ==
                 pLinkNode->pOsTeLsaNode->pLinkTlv->u4RemoteIpAddr)
                && (pOsTeLsaNode->pLinkTlv->u4LocalIdentifier ==
                    pLinkNode->pOsTeLsaNode->pLinkTlv->u4RemoteIdentifier))
            {
                u4CopySrlg1 = pOsTeLsaNode->pLinkTlv->osTeSrlg.u4NoOfSrlg;

                if (u4CopySrlg + u4CopySrlg1 < OSPF_TE_MAX_SRLG_NO)
                {
                    OSPF_TE_MEMCPY (&pVertexPath->
                                    aSrlg[pVertexPath->u2HopCount].
                                    aSrlgNumber[u4CopySrlg],
                                    &pOsTeLsaNode->pLinkTlv->osTeSrlg.
                                    aSrlgNumber, u4CopySrlg1 * MAX_IP_ADDR_LEN);
                    pVertexPath->aSrlg[pVertexPath->u2HopCount].u4NoOfSrlg =
                        u4CopySrlg + u4CopySrlg1;
                }

                if (pVertex->u4VertexId == gOsTeContext.u4DestIpAddr)
                {
                    pVertexPath->pOsTeDestLinkTlv = pOsTeLsaNode->pLinkTlv;
                }
                break;
            }

        }

        pVertexPath->u2HopCount++;
    }
    else
    {
        if ((pPtr = OspfTeCspfSearchDatabase
             (OSPF_TE_TYPE10_LSA, pVertex->u4VertexId, pArea)) == NULL)
        {
            OSPF_TE_PATH_NODE_FREE (pVertexPath);
            return OSPF_TE_FAILURE;
        }
        pOsTeDbNode = (tOsTeDbNode *) (VOID *) pPtr;

        TMO_SLL_Scan (&pOsTeDbNode->TeLsaLst, pOsTeLsaNode, tOsTeLsaNode *)
        {
            if (pOsTeLsaNode->pLinkTlv == NULL)
            {
                continue;
            }
            if (pOsTeLsaNode->pLinkTlv->u4LinkId == pParent->u4VertexId)
            {
                pVertexPath->u4TeMetric = pVertexPath->u4TeMetric + u4TeMetric;
                pVertexPath->aNextHops[pVertexPath->u2HopCount].u4RouterId =
                    pVertex->u4VertexId;
                pVertexPath->aNextHops[pVertexPath->u2HopCount].
                    u4NextHopIpAddr = pOsTeLsaNode->pLinkTlv->u4LocalIpAddr;
                pVertexPath->aNextHops[pVertexPath->u2HopCount].
                    u4RemoteIdentifier =
                    pOsTeLsaNode->pLinkTlv->u4LocalIdentifier;

                u4CopySrlg = pOsTeLsaNode->pLinkTlv->osTeSrlg.u4NoOfSrlg;
                OSPF_TE_MEMCPY (&pVertexPath->aSrlg[pVertexPath->u2HopCount],
                                &pOsTeLsaNode->pLinkTlv->osTeSrlg,
                                sizeof (tOsTeSrlg));
                if (pVertex->u4VertexId == gOsTeContext.u4DestIpAddr)
                {
                    pVertexPath->pOsTeDestLinkTlv = pOsTeLsaNode->pLinkTlv;
                }
                if ((pPtr1 = OspfTeCspfSearchDatabase
                     (OSPF_TE_TYPE10_LSA, pParent->u4ParentId, pArea)) == NULL)
                {
                    OSPF_TE_PATH_NODE_FREE (pVertexPath);
                    return OSPF_TE_FAILURE;
                }
                pOsTeDbNode1 = (tOsTeDbNode *) (VOID *) pPtr1;

                TMO_SLL_Scan (&pOsTeDbNode1->TeLsaLst, pOsTeLsaNode1,
                              tOsTeLsaNode *)
                {
                    if (pOsTeLsaNode1->pLinkTlv == NULL)
                    {
                        continue;
                    }
                    if (pOsTeLsaNode1->pLinkTlv->u4LinkId ==
                        pParent->u4VertexId)
                    {

                        u4CopySrlg1 =
                            pOsTeLsaNode1->pLinkTlv->osTeSrlg.u4NoOfSrlg;

                        if (u4CopySrlg + u4CopySrlg1 < OSPF_TE_MAX_SRLG_NO)
                        {
                            OSPF_TE_MEMCPY (&pVertexPath->
                                            aSrlg[pVertexPath->u2HopCount].
                                            aSrlgNumber[u4CopySrlg],
                                            &pOsTeLsaNode1->pLinkTlv->osTeSrlg.
                                            aSrlgNumber,
                                            u4CopySrlg1 * MAX_IP_ADDR_LEN);
                            pVertexPath->aSrlg[pVertexPath->u2HopCount].
                                u4NoOfSrlg = u4CopySrlg + u4CopySrlg1;
                        }
                        if ((pParent->u4ParentId == gOsTeContext.u4SrcIpAddr)
                            && (pParent->u1VertexType == OSPF_TE_VERT_NETWORK))
                        {
                            pVertexPath->pOsTeSrcLinkTlv =
                                pOsTeLsaNode1->pLinkTlv;
                        }
                        break;
                    }

                }
                pVertexPath->u2HopCount++;
                u1Flag = OSPF_TE_TRUE;
                break;
            }
        }
        if (u1Flag != OSPF_TE_TRUE)
        {
            OSPF_TE_PATH_NODE_FREE (pVertexPath);
            return OSPF_TE_FAILURE;
        }

    }

    if (pVertex->u4VertexId == gOsTeContext.u4DestIpAddr)
    {
        if (OspfTeCspfChkEncodingType (pVertexPath, pCspfReq) !=
            OSPF_TE_SUCCESS)
        {
            OSPF_TE_PATH_NODE_FREE (pVertexPath);
            return OSPF_TE_FAILURE;
        }
        if (OspfTeCspfUpdateGlobalPathLst (pVertexPath, pCspfReq)
            == OSPF_TE_FAILURE)
        {
            OSPF_TE_PATH_NODE_FREE (pVertexPath);
        }
        return OSPF_TE_SUCCESS;
    }

    TMO_SLL_Add (&pVertex->pathLst, &(pVertexPath->NextPathNode));
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfUpdateNextHop\n");
    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/*  Function    :  OspfTeCspfIntraAreaRoute                                  */
/*                                                                           */
/*  Description :  This function does the INTRA AREA CSPF calculation.       */
/*                 It does the CSPF computation based on the constraints till*/
/*                 the destination specified by Application is reached.      */
/*                                                                           */
/*  Input       :  pArea - Points to Area node                               */
/*                 pCspfReq - pointer to cspf request.                       */
/*                                                                           */
/*  Output      :  *aPath - Holds the path                                   */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS                                           */
/*                 OSPF_TE_FAILURE                                           */
/*                 OSPF_TE_MEM_FAILURE                                       */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfIntraAreaRoute (tOsTeArea * pArea,
                          tOsTeCspfReq * pCspfReq, tOsTeAppPath * aPath)
{
    tOsTeCandteNode    *pCspfRoot = NULL;
    UINT1              *pPtr = NULL;
    tOsTeLinkNode       linkNode;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfIntraAreaRoute\n");

    MEMSET (&linkNode, OSPF_TE_NULL, sizeof (tOsTeLinkNode));

    pCspfRoot = OspfTeCspfGetCandteNode
        (gOsTeContext.u4SrcIpAddr, OSPF_TE_VERT_ROUTER);

    if (pCspfRoot == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                     "Failed to get CANDIDATE NODE\n");
        return OSPF_TE_MEM_FAILURE;
    }

    /* Search and get all the TLVs associated with Root */
    if ((pPtr = OspfTeCspfSearchDatabase (OSPF_TE_TYPE10_LSA,
                                          pCspfRoot->u4VertexId,
                                          pArea)) == NULL)
    {
        OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                      "TLVs not present in for RouterId %x\n",
                      pCspfRoot->u4VertexId);
        OspfTeCspfFreeCandteNode (pCspfRoot);
        return OSPF_TE_FAILURE;
    }

    pCspfRoot->pTeDbNode = (tOsTeDbNode *) (VOID *) pPtr;

    while (OSPF_TE_ONE)
    {
        while ((OspfTeCspfGetLinksFromLsa (pCspfRoot, &linkNode)) ==
               OSPF_TE_SUCCESS)
        {
            if (pCspfRoot->u1VertexType == OSPF_TE_VERT_ROUTER)
            {
                if (linkNode.pOsTeLsaNode->pLinkTlv == NULL)
                {
                    continue;
                }
                if (OspfTeCspfChkConstrain (pCspfReq,
                                            linkNode.pOsTeLsaNode->pLinkTlv,
                                            pCspfRoot->u4VertexId)
                    == OSPF_TE_FAILURE)
                {
                    continue;
                }
            }

            /* Add to Candidate List if necessary */
            OspfTeCspfUpdateCandteLst (pCspfRoot, &linkNode, pArea, pCspfReq);

        }
        /* End of WHILE - Search for getting all links from an router */

        OspfTeCspfFreeCandteNode (pCspfRoot);

        if ((pCspfRoot = OspfTeCspfGetNearestCandteNode ()) == NULL)
        {
            OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "Candidate List Empty\n");
            break;
        }

        TMO_HASH_Delete_Node (gOsTeContext.pTeCandteLst,
                              &(pCspfRoot->NextCandteNode),
                              OspfTeCspfHashFunc (pCspfRoot->u1VertexType,
                                                  pCspfRoot->u4VertexId));

        if (OspfTeCspfDestIpAddrChk (pCspfRoot, pCspfReq) == OSPF_TE_SUCCESS)
        {
            OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC,
                          "Found Constraint based path to reach the destination"
                          " %x\n", pCspfReq->u4DestIpAddr);
            OspfTeCspfFreeCandteNode (pCspfRoot);
            break;
        }

        if ((pCspfRoot->u4VertexId == pCspfReq->u4DestIpAddr)
            && (pCspfRoot->u1VertexType == OSPF_TE_VERT_ROUTER))
        {

            OspfTeCspfFreeCandteNode (pCspfRoot);

            if ((pCspfRoot = OspfTeCspfGetNearestCandteNode ()) == NULL)
            {
                OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "Candidate List Empty\n");
                break;
            }

            TMO_HASH_Delete_Node (gOsTeContext.pTeCandteLst,
                                  &(pCspfRoot->NextCandteNode),
                                  OspfTeCspfHashFunc (pCspfRoot->u1VertexType,
                                                      pCspfRoot->u4VertexId));

        }

    }                            /* End of WHILE (1)  */

    if ((pCspfRoot != NULL)
        && (pCspfRoot->u4VertexId == gOsTeContext.u4SrcIpAddr))
    {
        OspfTeCspfFreeCandteNode (pCspfRoot);
    }

    OspfTeCspfClearCandteLst ();

    /* Increment the counter for CSPF run */
    gOsTeContext.u4CspfRunCnt++;

    if (gOsTeContext.aPath[OSPF_TE_NULL] == NULL)
    {
        OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                      "Unable to find Constraint based path to reach the \n"
                      "destination %x\n", pCspfReq->u4DestIpAddr);
        return OSPF_TE_FAILURE;
    }

    /* In case of MBB or Back up path, aPath[one] needs to be filled.
     * If it is empty, then it is understood that, path computation
     * has been failed */
    if (IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag) ||
        IS_BACKUP_PATH (pCspfReq->u1Flag))
    {
        if (gOsTeContext.aPath[OSPF_TE_ONE] == NULL)
        {
            OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                          "Unable to find Backup path to reach the \n"
                          "destination %x\n", pCspfReq->u4DestIpAddr);
            return OSPF_TE_FAILURE;
        }
    }

    OspfTeCspfCopyPaths (aPath, pArea);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfIntraAreaRoute\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfCopyPaths.                                       */
/*                                                                           */
/* Description  : This function copy the paths from global path array to     */
/*                application path array.                                    */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : *aPath - Holds the path.                                   */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
OspfTeCspfCopyPaths (tOsTeAppPath * aPath, tOsTeArea * pArea)
{
    UINT1               u1Count;
    UINT1               u1PathCount = 0;
    UINT2               u2Count;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfCopyPaths\n");

    for (u1Count = OSPF_TE_NULL; u1Count < OSPF_TE_TWO; u1Count++)
    {
        if (gOsTeContext.aPath[u1Count] != NULL)
        {
            if ((gOsTeContext.aPath[u1Count]->u2PathType ==
                 OSPF_TE_PRIMARY_PATH)
                || (gOsTeContext.aPath[u1Count]->u2PathType ==
                    OSPF_TE_BACKUP_PATH))
            {
                aPath[u1PathCount].u2HopCount =
                    gOsTeContext.aPath[u1Count]->u2HopCount;
                aPath[u1PathCount].u2PathType =
                    gOsTeContext.aPath[u1Count]->u2PathType;
                aPath[u1PathCount].u4AreaId = pArea->u4AreaId;
                aPath[u1PathCount].u1EncodingType =
                    gOsTeContext.aPath[u1Count]->u1EncodingType;
                aPath[u1PathCount].u4TeMetric =
                    gOsTeContext.aPath[u1Count]->u4TeMetric;
                for (u2Count = OSPF_TE_NULL;
                     u2Count < gOsTeContext.aPath[u1Count]->u2HopCount;
                     u2Count++)
                {
                    OSPF_TE_MEMCPY (&aPath[u1PathCount].aNextHops[u2Count],
                                    &gOsTeContext.aPath[u1Count]->
                                    aNextHops[u2Count], sizeof (tOsTeNextHop));

                }
                u1PathCount++;
            }
        }

    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfCopyPaths\n");
}

/*****************************************************************************/
/* Function     : OspfTeCspfFreeCandteNode                                   */
/*                                                                           */
/* Description  : This function free the candidate node memory.              */
/*                                                                           */
/* Input        : pCspfRoot   - pointer to candidate node.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
OspfTeCspfFreeCandteNode (tOsTeCandteNode * pCspfRoot)
{

    tOsTePath          *pPath = NULL;
    tOsTePath          *pTmpPath = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfFreeCandteNode\n");

    OSPF_TE_DYNM_SLL_SCAN (&(pCspfRoot->pathLst), pPath, pTmpPath, tOsTePath *)
    {
        TMO_SLL_Delete (&pCspfRoot->pathLst, &(pPath->NextPathNode));
        if ((pCspfRoot->u4VertexId != gOsTeContext.u4DestIpAddr)
            || ((pCspfRoot->u4VertexId == gOsTeContext.u4DestIpAddr) &&
                (pCspfRoot->u1VertexType == OSPF_TE_VERT_NETWORK)))
        {
            OSPF_TE_PATH_NODE_FREE (pPath);
        }
    }

    OSPF_TE_CANDTE_NODE_FREE (pCspfRoot);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfFreeCandteNode\n");
}

/*****************************************************************************/
/* Function     : OspfTeCspfClearCandteLst                                   */
/*                                                                           */
/* Description  : This function clear the candidate list.                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
OspfTeCspfClearCandteLst (VOID)
{
    UINT4               u4HashIndex;
    tOsTeCandteNode    *pCandteNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfClearCandteLst\n");
    TMO_HASH_Scan_Table (gOsTeContext.pTeCandteLst, u4HashIndex)
    {

        while ((pCandteNode
                = (tOsTeCandteNode *) TMO_HASH_Get_First_Bucket_Node
                (gOsTeContext.pTeCandteLst, u4HashIndex)) != NULL)
        {

            TMO_HASH_Delete_Node (gOsTeContext.pTeCandteLst,
                                  &(pCandteNode->NextCandteNode), u4HashIndex);
            OspfTeCspfFreeCandteNode (pCandteNode);
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfClearCandteLst\n");
}

/*****************************************************************************/
/* Function     : OspfTeCspfStrictIntraAreaRoute                             */
/*                                                                           */
/* Description  : This function process the strict explicit route request    */
/*                                                                           */
/* Input        : pArea - Pointer to TE Area.                                */
/*                pCspfReq - Pointer CSPF request                            */
/*                aPath    - Array of paths                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_MEM_FAILURE/OSPF_TE_FAILURE/OSPF_TE_SUCCESS        */
/*****************************************************************************/
PUBLIC INT4
OspfTeCspfStrictIntraAreaRoute (tOsTeArea * pArea,
                                tOsTeCspfReq * pCspfReq, tOsTeAppPath * aPath)
{
    tOsTeCandteNode    *pCspfRoot = NULL;
    tOsTeLinkNode       linkNode;
    tOsTeLsaNode       *pOsTeLsaNode = NULL;
    tOsTeDbNode        *pOsTeDbNode = NULL;
    UINT1              *pPtr = NULL;
    UINT4               u4RouterId = OSPF_TE_ZERO;
    UINT2               u2Count = OSPF_TE_ZERO;
    UINT2               u2Index = OSPF_TE_ZERO;
    UINT1               u1Flag = OSPF_TE_FALSE;
    UINT1               u1TempFlag = OSPF_TE_FALSE;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfStrictIntraAreaRoute\n");

    OSPF_TE_MEMSET (&linkNode, OSPF_TE_ZERO, sizeof (tOsTeLinkNode));
    pCspfRoot = OspfTeCspfGetCandteNode
        (gOsTeContext.u4SrcIpAddr, OSPF_TE_VERT_ROUTER);

    if (pCspfRoot == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                     "Failed to get CANDIDATE NODE\n");
        return OSPF_TE_MEM_FAILURE;
    }

    /* Search and get all the TLVs associated with Root */
    if ((pPtr = OspfTeCspfSearchDatabase (OSPF_TE_TYPE10_LSA,
                                          pCspfRoot->u4VertexId,
                                          pArea)) == NULL)
    {
        OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                      "TLVs not present in for RouterId %x\n",
                      pCspfRoot->u4VertexId);
        OspfTeCspfFreeCandteNode (pCspfRoot);
        return OSPF_TE_FAILURE;
    }

    pCspfRoot->pTeDbNode = (tOsTeDbNode *) (VOID *) pPtr;

    for (u2Count = OSPF_TE_ZERO; u2Count < ER_OR_AR_HOP_COUNT (pCspfReq) &&
         pCspfRoot != NULL; u2Count++)
    {

        	u4RouterId = ER_OR_AR_ROUTER_ID (pCspfReq, u2Count);
        while (OSPF_TE_ONE && (pCspfRoot != NULL))
        {
            while ((OspfTeCspfGetLinksFromLsa (pCspfRoot, &linkNode)) ==
                   OSPF_TE_SUCCESS)
            {

                if (pCspfRoot->u1VertexType == OSPF_TE_VERT_ROUTER)
                {
                    if (linkNode.pOsTeLsaNode->pLinkTlv == NULL)
                    {
                        continue;
                    }
                    if ((linkNode.u1LinkType == PTOP_LINK) &&
                        linkNode.pOsTeLsaNode->pLinkTlv->u4LinkId != u4RouterId)
                    {
                        continue;
                    }
                    if (linkNode.u1LinkType == OSPF_TE_MULTIACCESS)
                    {

                        if ((pPtr =
                             OspfTeCspfSearchDatabase (OSPF_TE_TYPE10_LSA,
                                                       u4RouterId,
                                                       pArea)) == NULL)
                        {
                            continue;
                        }
                        pOsTeDbNode = (tOsTeDbNode *) (VOID *) pPtr;
                        TMO_SLL_Scan (&pOsTeDbNode->TeLsaLst, pOsTeLsaNode,
                                      tOsTeLsaNode *)
                        {
                            if (pOsTeLsaNode->pLinkTlv == NULL)
                            {
                                continue;
                            }
                            if (pOsTeLsaNode->pLinkTlv->u4LinkId ==
                                linkNode.pOsTeLsaNode->pLinkTlv->u4LinkId)
                            {
                                u1Flag = OSPF_TE_TRUE;
                                break;
                            }
                        }
                        if (u1Flag != OSPF_TE_TRUE)
                        {
                            continue;
                        }
                        u1Flag = OSPF_TE_FALSE;
                    }
                    if (OspfTeCspfChkConstrain (pCspfReq,
                                                linkNode.pOsTeLsaNode->pLinkTlv,
                                                pCspfRoot->u4VertexId)
                        == OSPF_TE_FAILURE)
                    {
                        continue;
                    }
                }

                else if (pCspfRoot->u1VertexType == OSPF_TE_VERT_NETWORK)
                {
                    if (linkNode.u4LinkId != u4RouterId)
                    {
                        continue;
                    }
                }
		if(OspfTeCspfUpdateCandteLst (pCspfRoot, &linkNode, pArea,
                                           pCspfReq) == OSPF_TE_FAILURE)
                {
                    OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "Failed to UPDATE the Candidate List\n");
                    continue;
				}
	
            /* End of WHILE - Search for getting all links from an router */
	        }
            OspfTeCspfFreeCandteNode (pCspfRoot);
            u1TempFlag = OSPF_TE_TRUE;    /* to detect for loop execution and this free */

            if ((pCspfRoot = OspfTeCspfGetStrictNearestCandteNode ()) == NULL)
            {
                OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "Candidate List Empty\n");
                u2Count = (UINT2) ER_OR_AR_HOP_COUNT (pCspfReq);
                break;
            }

            TMO_HASH_Delete_Node (gOsTeContext.pTeCandteLst,
                                  &(pCspfRoot->NextCandteNode),
                                  OspfTeCspfHashFunc (pCspfRoot->u1VertexType,
                                                      pCspfRoot->u4VertexId));
            if ((pCspfRoot->u4VertexId == u4RouterId)
                && (pCspfRoot->u1VertexType == OSPF_TE_VERT_ROUTER))
            {
                if (OspfTeCspfDestIpAddrChk (pCspfRoot, pCspfReq) ==
                    OSPF_TE_SUCCESS)
                {
                    OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC,
                                  "Found Constraint based path to reach the \n"
                                  "destination %x\n", pCspfReq->u4DestIpAddr);
                    u2Count = (UINT2) ER_OR_AR_HOP_COUNT (pCspfReq);
                    OspfTeCspfFreeCandteNode (pCspfRoot);
                }
                else if (pCspfRoot->u4VertexId == pCspfReq->u4DestIpAddr)
                {
                    OspfTeCspfFreeCandteNode (pCspfRoot);
                }
                break;
            }
        }
	}
    /* End of WHILE (1)  */
    if (OSPF_TE_TRUE != u1TempFlag && pCspfRoot != NULL)
    {
        OspfTeCspfFreeCandteNode (pCspfRoot);
    }

    if ((pCspfRoot != NULL)
        && (pCspfRoot->u4VertexId == gOsTeContext.u4SrcIpAddr))
    {
        OspfTeCspfFreeCandteNode (pCspfRoot);
    }
    OspfTeCspfClearCandteLst ();

    if (IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag))
    {
        u2Index = OSPF_TE_ONE;
    }
    if (gOsTeContext.aPath[u2Index] == NULL)
    {
        if (pCspfRoot != NULL)
        {
            OspfTeCspfFreeCandteNode (pCspfRoot);
        }
        OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                      "Unable to find Constraint based path to reach the \n"
                      "destination %x\n", pCspfReq->u4DestIpAddr);
        return OSPF_TE_FAILURE;
    }

    OspfTeCspfCopyPaths (aPath, pArea);
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfStrictIntraAreaRoute\n");
    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfGetStrictNearestCandteNode                       */
/*                                                                           */
/* Description  : This function gets the candidate nearest to the root from  */
/*                the Candidate List.                                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tOsTeCandteNode * - Points to the nearest candidate node   */
/*                NULL                                                       */
/*****************************************************************************/
PUBLIC tOsTeCandteNode *
OspfTeCspfGetStrictNearestCandteNode (VOID)
{
    tOsTeCandteNode    *pLstCandteNode = NULL;
    tOsTeCandteNode    *pNearestCandteNode = NULL;
    UINT4               u4HashKey;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY,
                 "ENTRY: OspfTeCspfGetStrictNearestCandteNode\n");

    TMO_HASH_Scan_Table (gOsTeContext.pTeCandteLst, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (gOsTeContext.pTeCandteLst,
                              u4HashKey, pLstCandteNode, tOsTeCandteNode *)
        {
            if (pNearestCandteNode == NULL)
            {
                pNearestCandteNode = pLstCandteNode;
            }
            else
            {
                if ((pLstCandteNode->u1VertexType == OSPF_TE_VERT_NETWORK)
                    && (pNearestCandteNode->u1VertexType ==
                        OSPF_TE_VERT_ROUTER))
                {
                    pNearestCandteNode = pLstCandteNode;
                }
                else if (pLstCandteNode->u4TeMetric <
                         pNearestCandteNode->u4TeMetric)
                {
                    pNearestCandteNode = pLstCandteNode;
                }
            }                    /* End of check if Nearest Node present */
        }                        /* End of scan of Candidate bucket  */
    }                            /* End of scan of Candidate Hash Table */

    OSPF_TE_TRC (OSPF_TE_FN_EXIT,
                 "EXIT: OspfTeCspfGetStrictNearestCandteNode\n");

    return pNearestCandteNode;
}

/*****************************************************************************/
/* Function     : OspfTeCspfLooseIntraAreaRoute                             */
/*                                                                           */
/* Description  : This function process the loose explicit route request    */
/*                                                                           */
/* Input        : pArea - Pointer to TE Area.                                */
/*                pCspfReq - Pointer CSPF request                            */
/*                aPath    - Array of paths                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_MEM_FAILURE/OSPF_TE_FAILURE/OSPF_TE_SUCCESS        */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfLooseIntraAreaRoute (tOsTeArea * pArea,
                               tOsTeCspfReq * pCspfReq, tOsTeAppPath * aPath)
{
    tOsTeCandteNode    *pCspfRoot = NULL;
    INT4                i4Status = OSPF_TE_FAILURE;
    UINT1              *pPtr = NULL;
    tOsTeLinkNode       linkNode;
    UINT2               u2Count;
    UINT4               u4DestRouterId;
    tOsTePath          *pPath = NULL;
    tOsTePath          *pTmpPath = NULL;
    UINT1               u1LooseFlag = OSPF_TE_FALSE;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfLooseIntraAreaRoute\n");

    OSPF_TE_MEMSET (&linkNode, OSPF_TE_ZERO, sizeof (tOsTeLinkNode));
    pCspfRoot = OspfTeCspfGetCandteNode
        (gOsTeContext.u4SrcIpAddr, OSPF_TE_VERT_ROUTER);

    if (pCspfRoot == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                     "Failed to get CANDIDATE NODE\n");
        return OSPF_TE_MEM_FAILURE;
    }

    /* Search and get all the TLVs associated with Root */
    if ((pPtr = OspfTeCspfSearchDatabase (OSPF_TE_TYPE10_LSA,
                                          pCspfRoot->u4VertexId,
                                          pArea)) == NULL)
    {
        OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                      "TLVs not present in for RouterId %x\n",
                      pCspfRoot->u4VertexId);
        OspfTeCspfFreeCandteNode (pCspfRoot);
        return OSPF_TE_FAILURE;
    }

    pCspfRoot->pTeDbNode = (tOsTeDbNode *) (VOID *) pPtr;

    for (u2Count = 0; u2Count <= ER_OR_AR_HOP_COUNT (pCspfReq) &&
         pCspfRoot != NULL; u2Count++)
    {
        if (u2Count < ER_OR_AR_HOP_COUNT (pCspfReq))
        {
            u4DestRouterId = ER_OR_AR_ROUTER_ID (pCspfReq, u2Count);
        }
        else
        {
            u4DestRouterId = gOsTeContext.u4DestIpAddr;
        }

        while (OSPF_TE_ONE)
        {
            while ((OspfTeCspfGetLinksFromLsa (pCspfRoot, &linkNode)) ==
                   OSPF_TE_SUCCESS)
            {

                if (pCspfRoot->u1VertexType == OSPF_TE_VERT_ROUTER)
                {
                    if (linkNode.pOsTeLsaNode->pLinkTlv == NULL)
                    {
                        continue;
                    }
                    if (OspfTeCspfChkConstrain (pCspfReq,
                                                linkNode.pOsTeLsaNode->pLinkTlv,
                                                pCspfRoot->u4VertexId)
                        == OSPF_TE_FAILURE)
                    {
                        continue;
                    }
                }

                OspfTeCspfUpdateCandteLst (pCspfRoot, &linkNode, pArea,
                                           pCspfReq);

            }
            /* End of WHILE - Search for getting all links from an router */

            OspfTeCspfFreeCandteNode (pCspfRoot);

            if ((pCspfRoot = OspfTeCspfGetNearestCandteNode ()) == NULL)
            {
                OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "Candidate List Empty\n");
                u2Count = (UINT2) (ER_OR_AR_HOP_COUNT (pCspfReq) + OSPF_TE_ONE);
                break;
            }

            TMO_HASH_Delete_Node (gOsTeContext.pTeCandteLst,
                                  &(pCspfRoot->NextCandteNode),
                                  OspfTeCspfHashFunc (pCspfRoot->u1VertexType,
                                                      pCspfRoot->u4VertexId));
            if ((pCspfRoot->u4VertexId == u4DestRouterId)
                && (pCspfRoot->u1VertexType == OSPF_TE_VERT_ROUTER)
                && (pCspfRoot->u4VertexId != gOsTeContext.u4DestIpAddr))
            {
                OSPF_TE_DYNM_SLL_SCAN (&(pCspfRoot->pathLst), pPath, pTmpPath,
                                       tOsTePath *)
                {
                    if ((IsOspfTeStrictExpRoute
                         (pPath, ER_OR_AR (pCspfReq),
                          (UINT2) (pPath->u2HopCount - (UINT2) OSPF_TE_ONE),
                          (UINT2) u2Count)) == OSPF_TE_TRUE)
                    {
                        u1LooseFlag = OSPF_TE_TRUE;
                        continue;
                    }
                    else
                    {
                        TMO_SLL_Delete (&pCspfRoot->pathLst,
                                        &(pPath->NextPathNode));
                        OSPF_TE_PATH_NODE_FREE (pPath);
                    }
                }
                if (u1LooseFlag == OSPF_TE_TRUE)
                {
                    OspfTeCspfClearLooseCandteLst (u4DestRouterId);
                    u1LooseFlag = OSPF_TE_FALSE;
                    break;
                }
                else
                {
                    OspfTeCspfFreeCandteNode (pCspfRoot);

                    if ((pCspfRoot = OspfTeCspfGetNearestCandteNode ()) == NULL)
                    {
                        OSPF_TE_TRC (OSPF_TE_CSPF_TRC,
                                     "Candidate List Empty\n");
                        u2Count =
                            (UINT2) (ER_OR_AR_HOP_COUNT (pCspfReq) +
                                     OSPF_TE_ONE);
                        break;
                    }

                    TMO_HASH_Delete_Node (gOsTeContext.pTeCandteLst,
                                          &(pCspfRoot->NextCandteNode),
                                          OspfTeCspfHashFunc (pCspfRoot->
                                                              u1VertexType,
                                                              pCspfRoot->
                                                              u4VertexId));

                }

            }
            if ((pCspfRoot->u4VertexId == gOsTeContext.u4DestIpAddr)
                && (pCspfRoot->u1VertexType == OSPF_TE_VERT_ROUTER))
            {
                if (OspfTeCspfDestIpAddrChk (pCspfRoot, pCspfReq) ==
                    OSPF_TE_SUCCESS)
                {
                    OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC,
                                  "Found Constraint based path to reach the \n"
                                  "destination %x\n", pCspfReq->u4DestIpAddr);
                    i4Status = OSPF_TE_SUCCESS;
                    u2Count =
                        (UINT2) (ER_OR_AR_HOP_COUNT (pCspfReq) + OSPF_TE_ONE);
                    OspfTeCspfFreeCandteNode (pCspfRoot);
                    break;
                }
            }

        }

    }
    /* End of WHILE (1)  */

    if ((pCspfRoot != NULL)
        && (pCspfRoot->u4VertexId == gOsTeContext.u4SrcIpAddr))
    {
        OspfTeCspfFreeCandteNode (pCspfRoot);
    }
    OspfTeCspfClearCandteLst ();

    if (i4Status == OSPF_TE_FAILURE)
    {
        if (pCspfRoot != NULL)
        {
            OspfTeCspfFreeCandteNode (pCspfRoot);
        }
        OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                      "Unable to find Constraint based path to reach the \n"
                      "destination %x\n", pCspfReq->u4DestIpAddr);
        return OSPF_TE_FAILURE;
    }

    OspfTeCspfCopyPaths (aPath, pArea);

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfLooseIntraAreaRoute\n");

    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfClearLooseCandteLst                              */
/*                                                                           */
/* Description  : This function clear the candidate list.                    */
/*                                                                           */
/* Input        : u4RouterId                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/

PUBLIC VOID
OspfTeCspfClearLooseCandteLst (UINT4 u4RouterId)
{
    UINT4               u4HashIndex;
    tOsTeCandteNode    *pCandteNode = NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfClearLooseCandteLst\n");
    TMO_HASH_Scan_Table (gOsTeContext.pTeCandteLst, u4HashIndex)
    {

        while ((pCandteNode
                = (tOsTeCandteNode *) TMO_HASH_Get_First_Bucket_Node
                (gOsTeContext.pTeCandteLst, u4HashIndex)) != NULL)
        {

            TMO_HASH_Delete_Node (gOsTeContext.pTeCandteLst,
                                  &(pCandteNode->NextCandteNode), u4HashIndex);
            if (u4RouterId != pCandteNode->u4VertexId)
            {
                OspfTeCspfFreeCandteNode (pCandteNode);
            }
        }
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfClearLooseCandteLst\n");
}

/*****************************************************************************/
/* Function     : OspfTeCspfUpdateGlobalPathLst                              */
/*                                                                           */
/* Description  : This function Update the Global path List                  */
/*                                                                           */
/* Input        : pVertexPath : pointer to path structure.                   */
/*                pCspfReq    : pointer to CSPF request                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS or OSPF_TE_FAILURE                         */
/*****************************************************************************/
PUBLIC INT4
OspfTeCspfUpdateGlobalPathLst (tOsTePath * pVertexPath, tOsTeCspfReq * pCspfReq)
{

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfUpdateGlobalPathLst\n");

    if (!IS_BACKUP_PATH (pCspfReq->u1Flag))
    {
        if (gOsTeContext.aPath[OSPF_TE_NULL] == NULL)
        {
            if (OspfTeCspfChkValidPrimaryPath (pVertexPath, pCspfReq)
                == OSPF_TE_SUCCESS)
            {
                gOsTeContext.aPath[OSPF_TE_NULL] = pVertexPath;
                gOsTeContext.aPath[OSPF_TE_NULL]->u2PathType =
                    OSPF_TE_PRIMARY_PATH;
                return OSPF_TE_SUCCESS;
            }
            else
            {
                return OSPF_TE_FAILURE;
            }
        }
        else
        {
            if (OspfTeCspfChkValidPrimaryPath (pVertexPath, pCspfReq)
                == OSPF_TE_SUCCESS)
            {
                if (pVertexPath->u4TeMetric <
                    gOsTeContext.aPath[OSPF_TE_NULL]->u4TeMetric)
                {
                    OSPF_TE_PATH_NODE_FREE (gOsTeContext.aPath[OSPF_TE_NULL]);
                    gOsTeContext.aPath[OSPF_TE_NULL] = pVertexPath;
                    gOsTeContext.aPath[OSPF_TE_NULL]->u2PathType =
                        OSPF_TE_PRIMARY_PATH;
                    return OSPF_TE_SUCCESS;

                }
                else
                {
                    return OSPF_TE_FAILURE;
                }
            }
            else
            {
                return OSPF_TE_FAILURE;
            }
        }
    }
    else
    {
        if (gOsTeContext.aPath[OSPF_TE_ONE] == NULL)
        {
            if (OspfTeCspfChkValidBackupPath (pVertexPath, pCspfReq)
                == OSPF_TE_SUCCESS)
            {
                gOsTeContext.aPath[OSPF_TE_ONE] = pVertexPath;
                gOsTeContext.aPath[OSPF_TE_ONE]->u2PathType =
                    OSPF_TE_BACKUP_PATH;
                return OSPF_TE_SUCCESS;
            }
            else
            {
                return OSPF_TE_FAILURE;
            }
        }
        else
        {
            if (OspfTeCspfChkValidBackupPath (pVertexPath, pCspfReq)
                == OSPF_TE_SUCCESS)
            {
                if (pVertexPath->u4TeMetric <
                    gOsTeContext.aPath[OSPF_TE_ONE]->u4TeMetric)
                {
                    OSPF_TE_PATH_NODE_FREE (gOsTeContext.aPath[OSPF_TE_ONE]);
                    gOsTeContext.aPath[OSPF_TE_ONE] = pVertexPath;
                    gOsTeContext.aPath[OSPF_TE_ONE]->u2PathType =
                        OSPF_TE_BACKUP_PATH;
                    return OSPF_TE_SUCCESS;

                }
                else
                {
                    return OSPF_TE_FAILURE;
                }
            }
            else
            {
                return OSPF_TE_FAILURE;
            }
        }
    }
}

/*****************************************************************************/
/*  Function    :  OspfTeCspfCalculatePath                                   */
/*                                                                           */
/*  Description :  This function calculates the constraint based path.       */
/*                                                                           */
/*  Input       :  pArea - Points to Area node                               */
/*                 pCspfReq - pointer to cspf request.                       */
/*                                                                           */
/*  Output      :  *aPath - Holds the path                                   */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS                                           */
/*                 OSPF_TE_FAILURE                                           */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfCalculatePath (tOsTeArea * pArea, tOsTeCspfReq * pCspfReq,
                         tOsTeAppPath * aPath)
{

    INT4                i4Status = OSPF_TE_SUCCESS;
    UINT1               u1BackupPath = OSPF_TE_FALSE;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfCalculatePath\n");
    /* Only if Make Befor Break flag is not set, and explict route
     * is NULL, then first primary path is calculated and the backup
     * path is calculated. 
     * Else if Make Befor Break flag is set, simply backup path can be
     * calculated */
    if ((!IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag)) &&
        (IS_BACKUP_PATH (pCspfReq->u1Flag)))
    {
        pCspfReq->u1Flag =
            (UINT1) (pCspfReq->u1Flag - OSPF_TE_BACKUP_PATH_BIT_MASK);
        u1BackupPath = OSPF_TE_TRUE;
        i4Status = OspfTeCspfIntraAreaRoute (pArea, pCspfReq, aPath);
    }
    /* In case, if the call is not the requirement of backup path
     * ie. only the primary path is reqd. this below condition is
     * hitted */
    if (!IS_BACKUP_PATH (pCspfReq->u1Flag))
    {
        i4Status = OspfTeCspfIntraAreaRoute (pArea, pCspfReq, aPath);
    }
    if (u1BackupPath == OSPF_TE_TRUE)
    {
        pCspfReq->u1Flag =
            (UINT1) (pCspfReq->u1Flag + OSPF_TE_BACKUP_PATH_BIT_MASK);
    }
    if (IS_BACKUP_PATH (pCspfReq->u1Flag) && i4Status == OSPF_TE_SUCCESS)
    {
        i4Status = OspfTeCspfIntraAreaRoute (pArea, pCspfReq, aPath);
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfCalculatePath\n");
    return i4Status;
}

/*****************************************************************************/
/*  Function    :  OspfTeProcessCspfCompMsg                                  */
/*                                                                           */
/*  Description :  This function calculates the constraint based path.       */
/*                                                                           */
/*  Input       :  pCspfCompInfo - CSPF computation info.                    */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  OSPF_TE_SUCCESS                                           */
/*                 OSPF_TE_FAILURE                                           */
/*****************************************************************************/
PUBLIC VOID
OspfTeProcessCspfCompMsg (tCspfCompInfo * pCspfCompInfo)
{
#ifdef MPLS_SIG_WANTED
    tOsTeCspfCons      *pOsTeCspfCons = NULL;
    tOsTeCspfReq       *pOsTeCspfReq = NULL;
    tOsTeAppPath       *pPath = NULL;
    INT4                i4RetVal = OSPF_TE_FAILURE;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeProcessCspfCompMsg\n");

    if (OSPF_TE_CSPF_REQ_ALLOC (pOsTeCspfReq, tOsTeCspfReq) == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to allocate "
                     "Memory for CSPF Request\n");
        return;
    }
    if (OSPF_TE_CSPF_CONS_ALLOC (pOsTeCspfCons, tOsTeCspfCons) == NULL)
    {
        OSPF_TE_TRC (OSPF_TE_CRITICAL_TRC, "Failed to allocate "
                     "Memory for CSPF Constraint\n");
        OSPF_TE_CSPF_REQ_FREE (pOsTeCspfReq);
        return;
    }
    MEMSET (pOsTeCspfReq, 0, sizeof (tOsTeCspfReq));
    MEMSET (pOsTeCspfCons, 0, sizeof (tOsTeCspfCons));

    pOsTeCspfReq->u1Indication = 255;
    pOsTeCspfReq->u4SrcIpAddr = pCspfCompInfo->u4SrcIpAddr;
    pOsTeCspfReq->u4DestIpAddr = pCspfCompInfo->u4DestIpAddr;
    pOsTeCspfReq->u4IncludeAllSet = pCspfCompInfo->u4IncludeAllSet;
    pOsTeCspfReq->u4IncludeAnySet = pCspfCompInfo->u4IncludeAnySet;
    pOsTeCspfReq->u4ExcludeAnySet = pCspfCompInfo->u4ExcludeAnySet;
    pOsTeCspfReq->u1ProtectionType = pCspfCompInfo->u1ProtectionType;
    pOsTeCspfReq->u1SwitchingCap = pCspfCompInfo->u1SwitchingCap;
    pOsTeCspfReq->u1EncodingType = pCspfCompInfo->u1EncodingType;
    pOsTeCspfReq->u1Priority = pCspfCompInfo->u1Priority;
    pOsTeCspfReq->u1Diversity = pCspfCompInfo->u1Diversity;
    /*Copying SRLG values from cspfcomp to cspfreq */
    OSPF_TE_MEMCPY (&pOsTeCspfReq->osTeSrlg, &pCspfCompInfo->osTeSrlg,
                    sizeof (tOsTeSrlg));
    /*Copying Explicit route from cspfcomp to cspfreq */
    OSPF_TE_MEMCPY (&pOsTeCspfReq->osTeExpRoute, &pCspfCompInfo->osTeExpRoute,
                    sizeof (tOsTeExpRoute));
    /*Copying Alternate Explicit route from cspfcomp to cspfreq */
    OSPF_TE_MEMCPY (&pOsTeCspfReq->osTeAlternateRoute,
                    &pCspfCompInfo->osTeAlternateRoute, sizeof (tOsTeExpRoute));
    pOsTeCspfReq->u1Flag = pCspfCompInfo->u1PathInfo;
    pOsTeCspfReq->bandwidth = pCspfCompInfo->bandwidth;
    pOsTeCspfReq->oldbandwidth = pCspfCompInfo->oldbandwidth;
    pOsTeCspfReq->u1RsrcSetType = pCspfCompInfo->u1RsrcSetType;
    pOsTeCspfReq->u1SrlgType = pCspfCompInfo->u1SrlgType;
    pOsTeCspfCons->pCspfReq = pOsTeCspfReq;

    i4RetVal = OspfTeCspfFindPaths (pOsTeCspfCons->pCspfReq,
                                    pOsTeCspfCons->aPath);

    if (pCspfCompInfo->u1PathInfo & OSPF_TE_BACKUP_PATH_BIT_MASK)
    {
        /* Backup path */
        pPath = &(pOsTeCspfCons->aPath[OSPF_TE_ONE]);
    }
    else
    {
        /* Primary path */
        pPath = &(pOsTeCspfCons->aPath[OSPF_TE_ZERO]);
    }

    if (i4RetVal == OSPF_TE_SUCCESS)
    {
        pPath->u1PathReturnCode = CSPF_PATH_FOUND;
    }
    else
    {
        pPath->u1PathReturnCode = CSPF_PATH_NOT_FOUND;
        OSPF_TE_TRC (OSPF_TE_CSPF_TRC, "Path Not found for the "
                     "constraints specified\n");
    }

    RpteEnqueueMsgToRsvpQFromCspf (pCspfCompInfo, pPath);

    OSPF_TE_CSPF_REQ_FREE (pOsTeCspfReq);
    OSPF_TE_CSPF_CONS_FREE (pOsTeCspfCons);

#else
    UNUSED_PARAM (pCspfCompInfo);
#endif
    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeProcessCspfCompMsg \n");
    return;
}

/*****************************************************************************/
/* Function     : OspfTeCspfStrictLooseIntraAreaRoute                        */
/*                                                                           */
/* Description  : This function process the Strict loose explicit route      */
/*                request                                                    */
/*                                                                           */
/* Input        : pArea - Pointer to TE Area.                                */
/*                pCspfReq - Pointer CSPF request                            */
/*                aPath    - Array of paths                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfStrictLooseIntraAreaRoute (tOsTeArea * pArea,
                                     tOsTeCspfReq * pCspfReq,
                                     tOsTeAppPath * aPath)
{
    UINT4               u4OrigSrcIpAddr = OSPF_TE_ZERO;
    UINT4               u4OrigDstIpAddr = OSPF_TE_ZERO;

    /* Size of tOsTeAppPath is greater than 1024 which increases stack
       Memory. So it is made Private */
    /* This function is called only by OSPFTE Module. No other modules calls
     * the function in its thread. So it can be made Private */
    PRIVATE tOsTeAppPath aOsTeSubPath[OSPF_TE_TWO];

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY,
                 "ENTRY: OspfTeCspfStrictLooseIntraAreaRoute \n");

    u4OrigDstIpAddr = pCspfReq->u4DestIpAddr;
    u4OrigSrcIpAddr = pCspfReq->u4SrcIpAddr;

    /* Make the last next hop ip address as Dest Ip Addr and then 
     * call OspfTeCspfStrictIntraAreaRoute with the explicit route
     * mentioned */
    if ((pCspfReq->osTeAlternateRoute.u2HopCount != OSPF_TE_ZERO) &&
        (pCspfReq->osTeAlternateRoute.u2HopCount < OSPF_TE_MAX_NEXT_HOPS))
    {
        pCspfReq->u4DestIpAddr = gOsTeContext.u4DestIpAddr =
            pCspfReq->osTeAlternateRoute.
            aNextHops[pCspfReq->osTeAlternateRoute.u2HopCount - 1].u4RouterId;
        pCspfReq->osTeAlternateRoute.u2Info = OSPF_TE_STRICT_EXP_ROUTE;
    }
    else if ((pCspfReq->osTeExpRoute.u2HopCount != OSPF_TE_ZERO) &&
             (pCspfReq->osTeExpRoute.u2HopCount < OSPF_TE_MAX_NEXT_HOPS))
    {
        pCspfReq->u4DestIpAddr = gOsTeContext.u4DestIpAddr =
            pCspfReq->osTeExpRoute.
            aNextHops[pCspfReq->osTeExpRoute.u2HopCount - 1].u4RouterId;
        pCspfReq->osTeExpRoute.u2Info = OSPF_TE_STRICT_EXP_ROUTE;
    }
    OSPF_TE_MEMSET (aOsTeSubPath, OSPF_TE_ZERO, sizeof (tOsTeAppPath));
    if (OspfTeCspfStrictIntraAreaRoute (pArea, pCspfReq, aOsTeSubPath)
        == OSPF_TE_FAILURE)
    {
        return OSPF_TE_FAILURE;
    }

    if (OspfTeCspfAppendPaths (pCspfReq, aOsTeSubPath, u4OrigSrcIpAddr,
                               aPath) == OSPF_TE_FAILURE)
    {
        return OSPF_TE_FAILURE;
    }

    /* Now make the last next hop ip address as the source ip address
     * and original destination ip address as destination ip and 
     * call OspfTeCspfCalculatePath */

    if ((pCspfReq->osTeAlternateRoute.u2HopCount != OSPF_TE_ZERO) &&
        (pCspfReq->osTeAlternateRoute.u2HopCount < OSPF_TE_MAX_NEXT_HOPS))
    {
        pCspfReq->u4SrcIpAddr = gOsTeContext.u4SrcIpAddr =
            pCspfReq->osTeAlternateRoute.
            aNextHops[pCspfReq->osTeAlternateRoute.u2HopCount - 1].u4RouterId;
    }
    else if ((pCspfReq->osTeExpRoute.u2HopCount != OSPF_TE_ZERO) &&
             (pCspfReq->osTeExpRoute.u2HopCount < OSPF_TE_MAX_NEXT_HOPS))
    {
        pCspfReq->u4SrcIpAddr = gOsTeContext.u4SrcIpAddr =
            pCspfReq->osTeExpRoute.
            aNextHops[pCspfReq->osTeExpRoute.u2HopCount - 1].u4RouterId;
    }

    /* Memsetting the explicit route or actual route received as it is 
     * not required any more and we will compute the dynamic path. */
    OSPF_TE_MEMSET (ER_OR_AR (pCspfReq), OSPF_TE_ZERO, sizeof (tOsTeExpRoute));
    OSPF_TE_MEMSET (aOsTeSubPath, OSPF_TE_ZERO, sizeof (tOsTeAppPath));

    /* Now Performing Path Computation from Last Strict hop to the Loose hop
     * Assumption: Loose hop is set to pCspfReq->u4DestIpAddr from 
     * Applications */
    pCspfReq->u4DestIpAddr = gOsTeContext.u4DestIpAddr = u4OrigDstIpAddr;

    if (IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag))
    {
        OSPF_TE_PATH_NODE_FREE (gOsTeContext.aPath[OSPF_TE_ONE]);
        gOsTeContext.aPath[OSPF_TE_ONE] = NULL;
    }
    else
    {
        OSPF_TE_PATH_NODE_FREE (gOsTeContext.aPath[OSPF_TE_ZERO]);
        gOsTeContext.aPath[OSPF_TE_ZERO] = NULL;
    }

    if (pCspfReq->u4DestIpAddr != pCspfReq->u4SrcIpAddr)
    {
        if (OspfTeCspfCalculatePath (pArea, pCspfReq, aOsTeSubPath) ==
            OSPF_TE_FAILURE)
        {
            /* Since Calculate Path has failed, reset the already computed 
             * path and return failure. */
            OSPF_TE_MEMSET (aPath, OSPF_TE_ZERO,
                            (sizeof (tOsTeAppPath) * OSPF_TE_TWO));
            gOsTeContext.u4SrcIpAddr = u4OrigSrcIpAddr;
            return OSPF_TE_FAILURE;
        }
    }
    /* Reverting Back the Original Source Ip Address */
    gOsTeContext.u4SrcIpAddr = u4OrigSrcIpAddr;

    if (OspfTeCspfAppendPaths (pCspfReq, aOsTeSubPath, u4OrigSrcIpAddr,
                               aPath) == OSPF_TE_FAILURE)
    {
        /* Since Append Paths has failed, reset the already computed
         * path and return failure. */
        OSPF_TE_MEMSET (aPath, OSPF_TE_ZERO,
                        (sizeof (tOsTeAppPath) * OSPF_TE_TWO));
        return OSPF_TE_FAILURE;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT,
                 "EXIT: OspfTeCspfStrictLooseIntraAreaRoute \n");
    return OSPF_TE_SUCCESS;

}

/*****************************************************************************/
/* Function     : OspfTeCspfAppendPaths                                      */
/*                                                                           */
/* Description  : This function appends an intermediate computed path to     */
/*                application path variable. Also it returns failue, if      */
/*                node duplication is detected.                              */
/*                                                                           */
/* Input        : pCspfReq - Pointer to CSPF Request                         */
/*                pOsTeSubPath - Pointer to intermediate computed path       */
/*                u4OrigSrcIPAddr - Original Source Router Id                */
/*                aPath    - Array of paths                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_SUCCESS                                            */
/*                OSPF_TE_FAILURE                                            */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfAppendPaths (tOsTeCspfReq * pCspfReq, tOsTeAppPath * pOsTeSubPath,
                       UINT4 u4OrigSrcIpAddr, tOsTeAppPath * aPath)
{
    UINT1               u2Count1 = OSPF_TE_ZERO;
    UINT1               u2Count2 = OSPF_TE_ZERO;
    UINT2               u2Index = OSPF_TE_NULL;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY, "ENTRY: OspfTeCspfAppendPaths \n");

    /* For Make Before Break. BACKUP PATH bit mask should be set 
     * so, checking the flag is enough */
    if (IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag))
    {
        u2Index = OSPF_TE_ONE;
    }

    /*If aPath is empty, simply copy the intermediate computed path into
     * aPath */
    if ((aPath[OSPF_TE_NULL].u2HopCount == OSPF_TE_NULL) &&
        (aPath[OSPF_TE_ONE].u2HopCount == OSPF_TE_NULL))
    {
        OSPF_TE_MEMCPY (aPath, pOsTeSubPath,
                        (sizeof (tOsTeAppPath) * OSPF_TE_TWO));
        return OSPF_TE_SUCCESS;
    }

    /* To check whether the path already contains the node which are now
     * given by the intermediate computed path */
    for (u2Count1 = OSPF_TE_ZERO;
         u2Count1 < pOsTeSubPath[u2Index].u2HopCount; u2Count1++)
    {
        for (u2Count2 = OSPF_TE_ZERO;
             u2Count2 < aPath[u2Index].u2HopCount; u2Count2++)
        {
            if (aPath[u2Index].aNextHops[u2Count2].u4RouterId ==
                pOsTeSubPath[u2Index].aNextHops[u2Count1].u4RouterId)
            {
                OSPF_TE_TRC (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                             "Path Overlap - Append Paths failed\n");
                return OSPF_TE_FAILURE;
            }
        }

        if (pOsTeSubPath[u2Index].aNextHops[u2Count1].u4RouterId ==
            u4OrigSrcIpAddr)
        {
            OSPF_TE_TRC (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                         "Path Overlap - Append Paths failed\n");
            return OSPF_TE_FAILURE;
        }

    }

    /* Copy the path info from intermediate computed path to aPath */
    for (u2Count1 = OSPF_TE_ZERO;
         u2Count1 < pOsTeSubPath[u2Index].u2HopCount; u2Count1++)
    {
        OSPF_TE_MEMCPY (&aPath[u2Index].aNextHops[(aPath[u2Index].
                                                   u2HopCount + u2Count1)],
                        &pOsTeSubPath[u2Index].aNextHops[u2Count1],
                        sizeof (tOsTeNextHop));
    }

    aPath[u2Index].u2HopCount = (UINT2) (aPath[u2Index].u2HopCount +
                                         pOsTeSubPath[u2Index].u2HopCount);
    aPath[u2Index].u4TeMetric = aPath[u2Index].u4TeMetric +
        pOsTeSubPath[u2Index].u4TeMetric;

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfAppendPaths \n");
    return OSPF_TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : OspfTeCspfIncludeAnyIntraAreaRoute                         */
/*                                                                           */
/* Description  : This function process the include any explicit route       */
/*                request                                                    */
/*                                                                           */
/* Input        : pArea - Pointer to TE Area.                                */
/*                pCspfReq - Pointer CSPF request                            */
/*                aPath    - Array of paths                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_TE_MEM_FAILURE/OSPF_TE_FAILURE/OSPF_TE_SUCCESS        */
/*****************************************************************************/

PUBLIC INT4
OspfTeCspfIncludeAnyIntraAreaRoute (tOsTeArea * pArea,
                                    tOsTeCspfReq * pCspfReq,
                                    tOsTeAppPath * aPath)
{
    /* Size of tOsTeAppPath is greater than 1024 which increases stack
       Memory. So it is made Private */
    /* This function is called only by OSPFTE Module. No other modules calls
     * the function in its thread. So it can be made Private */
    PRIVATE tOsTeAppPath aOsTeIncludePath[OSPF_TE_TWO];
    INT4                i4Status = OSPF_TE_FAILURE;
    UINT2               u2Count = OSPF_TE_ZERO;
    UINT2               u2Index = OSPF_TE_ZERO;
    UINT2               u2HopCount = OSPF_TE_ZERO;

    OSPF_TE_TRC (OSPF_TE_FN_ENTRY,
                 "ENTRY: OspfTeCspfIncludeAnyIntraAreaRoute\n");

    /* For Make Before Break. BACKUP PATH bit mask should be set 
     * so, checking the flag is enough */
    if (IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag))
    {
        u2Index = OSPF_TE_ONE;
    }

    u2HopCount = (UINT2) ER_OR_AR_HOP_COUNT (pCspfReq);
    /* Pass the explicit routes in cspfreq one by one to 
     * OspfTeCspfLooseIntraAreaRoute and get the path info.
     * Select path which has the better path metric */
    for (u2Count = OSPF_TE_ZERO; u2Count < u2HopCount; u2Count++)
    {
        OSPF_TE_MEMSET (aOsTeIncludePath, OSPF_TE_ZERO,
                        (sizeof (tOsTeAppPath) * OSPF_TE_TWO));

        if (!IS_MAKE_BEFORE_BREAK_PATH (pCspfReq->u1Flag))
        {
            pCspfReq->osTeExpRoute.aNextHops[OSPF_TE_ZERO].u4RouterId =
                pCspfReq->osTeExpRoute.aNextHops[u2Count].u4RouterId;
            pCspfReq->osTeExpRoute.aNextHops[OSPF_TE_ZERO].u4NextHopIpAddr =
                pCspfReq->osTeExpRoute.aNextHops[u2Count].u4NextHopIpAddr;
            pCspfReq->osTeExpRoute.aNextHops[OSPF_TE_ZERO].u4RemoteIdentifier =
                pCspfReq->osTeExpRoute.aNextHops[u2Count].u4RemoteIdentifier;
            pCspfReq->osTeExpRoute.u2Info = OSPF_TE_LOOSE_EXP_ROUTE;
            pCspfReq->osTeExpRoute.u2HopCount = OSPF_TE_ONE;
        }
        else
        {
            OSPF_TE_MEMSET (&(pCspfReq->osTeAlternateRoute), OSPF_TE_ZERO,
                            sizeof (tOsTeExpRoute));
            pCspfReq->osTeAlternateRoute.aNextHops[OSPF_TE_ZERO].u4RouterId =
                pCspfReq->osTeAlternateRoute.aNextHops[u2Count].u4RouterId;
            pCspfReq->osTeAlternateRoute.aNextHops[OSPF_TE_ZERO].
                u4NextHopIpAddr = pCspfReq->osTeAlternateRoute.
                aNextHops[u2Count].u4NextHopIpAddr;
            pCspfReq->osTeAlternateRoute.aNextHops[OSPF_TE_ZERO].
                u4RemoteIdentifier = pCspfReq->osTeAlternateRoute.
                aNextHops[u2Count].u4RemoteIdentifier;
            pCspfReq->osTeAlternateRoute.u2Info = OSPF_TE_LOOSE_EXP_ROUTE;
            pCspfReq->osTeAlternateRoute.u2HopCount = OSPF_TE_ONE;

        }
        i4Status = OspfTeCspfLooseIntraAreaRoute (pArea, pCspfReq,
                                                  aOsTeIncludePath);

        if (i4Status == OSPF_TE_SUCCESS)
        {
            if ((u2Count == OSPF_TE_ZERO) ||
                (aOsTeIncludePath[u2Index].u4TeMetric <
                 aPath[u2Index].u4TeMetric))
            {
                OSPF_TE_MEMCPY (aPath, aOsTeIncludePath,
                                (sizeof (tOsTeAppPath) * OSPF_TE_TWO));

                OSPF_TE_TRC2 (OSPF_TE_CSPF_TRC, "Found the Better path by "
                              "considering the hop %x for the destination %x\n",
                              ER_OR_AR_ROUTER_ID (pCspfReq, u2Count),
                              pCspfReq->u4DestIpAddr);
            }
        }
    }

    if (aPath[u2Index].u2HopCount == OSPF_TE_ZERO)
    {
        OSPF_TE_TRC1 (OSPF_TE_CSPF_TRC | OSPF_TE_FAILURE_TRC,
                      "Unable to compute Path for Destination %x with "
                      "INCLUDE ANY constraint\n", pCspfReq->u4DestIpAddr);
        return OSPF_TE_FAILURE;
    }

    OSPF_TE_TRC (OSPF_TE_FN_EXIT, "EXIT: OspfTeCspfIncludeAnyIntraAreaRoute\n");
    return OSPF_TE_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ostecspf.c                     */
/*-----------------------------------------------------------------------*/
