
/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: fsotertest.c,v 1.6 2014/03/03 12:22:29 siva Exp $
*
* Description: Snmp Low Level Routines for set functions.
*********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include "snmccons.h"
#include "osteinc.h"
#include  "fsoterlw.h"
#include  "osterm.h"
#include  "ostermext.h"

UINT1
ValidateFutRMTeLinkSetIndex (UINT4 u4IfIndex)
{
    if (TeFindIf ((INT4) u4IfIndex) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

UINT1
OspfTeValidIpAddress (UINT4 u4IpAddr)
{
    if (u4IpAddr == 0)
    {
        return (TRUE);
    }
    if (u4IpAddr == LTD_B_CAST_ADDR)
    {
        return (FALSE);
    }
    if ((u4IpAddr & LOOP_BACK_ADDR_MASK) == LOOP_BACK_ADDRESSES)
    {
        return (FALSE);
    }
    if (!((IS_CLASS_A_ADDR (u4IpAddr)) ||
          (IS_CLASS_B_ADDR (u4IpAddr)) || (IS_CLASS_C_ADDR (u4IpAddr))))
    {
        return (FALSE);
    }
    return (TRUE);                /*Valid Ip Address */
}

UINT1
ValidateFutRMTeLinkSwDescrSetIndex (UINT4 u4IfIndex, UINT4 u4ifdescrid)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf ((INT4) u4IfIndex)) != NULL)
    {
        if (TeFindIscD (u4ifdescrid, pteiface) != NULL)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

UINT1
ValidateFutRMTeLinkSrlgSetIndex (UINT4 u4IfIndex, UINT4 u4SrlgNo)
{
    tRmTeLink          *pteiface = NULL;

    UNUSED_PARAM (u4SrlgNo);

    if ((pteiface = TeFindIf ((INT4) u4IfIndex)) != NULL)
    {
        if (TMO_SLL_Count (&(pteiface->srlgList)) > OSPF_TE_MAX_SRLG_NO)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutRmTeLinkRegDeregistration
 Input       :  The Indices

                The Object 
                testValFutRmTeLinkRegDeregistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRmTeLinkRegDeregistration (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFutRmTeLinkRegDeregistration)
{
    if ((i4TestValFutRmTeLinkRegDeregistration == RM_REGISTER) ||
        (i4TestValFutRmTeLinkRegDeregistration == RM_DEREGISTER))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkLocalIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkLocalIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkLocalIpAddr (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 UINT4 u4TestValFutRMTeLinkLocalIpAddr)
{
    if ((ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS) &&
        (OspfTeValidIpAddress (u4TestValFutRMTeLinkLocalIpAddr) == TRUE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkRemoteIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkRemoteIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkRemoteIpAddr (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  UINT4 u4TestValFutRMTeLinkRemoteIpAddr)
{
    if ((ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS) &&
        (OspfTeValidIpAddress (u4TestValFutRMTeLinkRemoteIpAddr) == TRUE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkRemoteRtrId
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkRemoteRtrId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkRemoteRtrId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 UINT4 u4TestValFutRMTeLinkRemoteRtrId)
{
    tRouterId           rtrId;

    OSPF_TE_CRU_BMC_DWTOPDU (rtrId, u4TestValFutRMTeLinkRemoteRtrId);
    if ((ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS) &&
        (IS_VALID_ROUTER_ID (&rtrId)))
    {
        return (SNMP_SUCCESS);
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkMetric
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkMetric (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            UINT4 u4TestValFutRMTeLinkMetric)
{
    UNUSED_PARAM (u4TestValFutRMTeLinkMetric);
    if (ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkProtectionType
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkProtectionType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkProtectionType (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 i4TestValFutRMTeLinkProtectionType)
{
    if ((ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS) &&
        (IS_VALID_TE_PROTECTION_TYPE (i4TestValFutRMTeLinkProtectionType)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkResourceClass
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkResourceClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkResourceClass (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   UINT4 u4TestValFutRMTeLinkResourceClass)
{
    UNUSED_PARAM (u4TestValFutRMTeLinkResourceClass);
    if (ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkIncomingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkIncomingIfId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkIncomingIfId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4TestValFutRMTeLinkIncomingIfId)
{
    if ((IS_VALID_TE_INCOMING_ID (i4TestValFutRMTeLinkIncomingIfId)) &&
        (ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkOutgoingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkOutgoingIfId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkOutgoingIfId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4TestValFutRMTeLinkOutgoingIfId)
{
    if ((IS_VALID_TE_OUTGOING_ID (i4TestValFutRMTeLinkOutgoingIfId)) &&
        (ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkMaxBw
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkMaxBw
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkMaxBw (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                           UINT4 u4TestValFutRMTeLinkMaxBw)
{
    UNUSED_PARAM (u4TestValFutRMTeLinkMaxBw);
    if (ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkMaxResBw
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkMaxResBw
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkMaxResBw (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              UINT4 u4TestValFutRMTeLinkMaxResBw)
{
    UNUSED_PARAM (u4TestValFutRMTeLinkMaxResBw);
    if (ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkAreaId
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkAreaId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkAreaId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            UINT4 u4TestValFutRMTeLinkAreaId)
{
    UINT4               u4AreaId = u4TestValFutRMTeLinkAreaId;
    u4AreaId = u4AreaId;
    if ((ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS) &&
        (IS_VALID_TE_AREAID (u4TestValFutRMTeLinkAreaId)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkInfoType
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkInfoType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkInfoType (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              INT4 i4TestValFutRMTeLinkInfoType)
{
    INT4                i4infotype = i4TestValFutRMTeLinkInfoType;
    i4infotype = i4infotype;
    if ((ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS) &&
        (IS_VALID_TE_INFOTYPE (i4TestValFutRMTeLinkInfoType)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValFutRMTeLinkRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4TestValFutRMTeLinkRowStatus)
{
    tRmTeLink          *pteiface = NULL;

    if (gteRM.RMregister != RM_REGISTER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    switch ((UINT1) i4TestValFutRMTeLinkRowStatus)
    {
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:
            if (ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) == SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case ACTIVE:
            if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            else if ((pteiface->u1LinkStatus == NOT_IN_SERVICE) ||
                     (pteiface->u1LinkStatus == NOT_READY) ||
                     (pteiface->u1LinkStatus == ACTIVE))
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        case DESTROY:
            if (TeFindIf (i4IfIndex) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case NOT_IN_SERVICE:
            /*  Not Supported
             *  case falls through
             */
        default:
            /*
             * End Switch
             */
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkSwDescrSwitchingCap
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                testValFutRMTeLinkSwDescrSwitchingCap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkSwDescrSwitchingCap (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         UINT4 u4FutRMTeLinkSwDescriptorId,
                                         INT4
                                         i4TestValFutRMTeLinkSwDescrSwitchingCap)
{
    UINT4               u4swDid = u4FutRMTeLinkSwDescriptorId;

    if ((ValidateFutRMTeLinkSwDescrSetIndex ((UINT4) i4IfIndex, u4swDid) ==
         SNMP_SUCCESS)
        && (IS_VALID_TE_SWITCH_CAP (i4TestValFutRMTeLinkSwDescrSwitchingCap)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkSwDescrEncodingType
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                testValFutRMTeLinkSwDescrEncodingType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkSwDescrEncodingType (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         UINT4 u4FutRMTeLinkSwDescriptorId,
                                         INT4
                                         i4TestValFutRMTeLinkSwDescrEncodingType)
{
    UINT4               u4swDid = u4FutRMTeLinkSwDescriptorId;

    if ((ValidateFutRMTeLinkSwDescrSetIndex ((UINT4) i4IfIndex, u4swDid) ==
         SNMP_SUCCESS)
        &&
        (IS_VALID_TE_ENCODING_TYPE (i4TestValFutRMTeLinkSwDescrEncodingType)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkSwDescrMinLSPBandwidth
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                testValFutRMTeLinkSwDescrMinLSPBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkSwDescrMinLSPBandwidth (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                            UINT4 u4FutRMTeLinkSwDescriptorId,
                                            UINT4
                                            u4TestValFutRMTeLinkSwDescrMinLSPBandwidth)
{
    UINT4               u4swDid = u4FutRMTeLinkSwDescriptorId;

    if ((ValidateFutRMTeLinkSwDescrSetIndex ((UINT4) i4IfIndex, u4swDid) ==
         SNMP_SUCCESS)
        && (IS_VALID_TE_MINLSPBW (u4TestValFutRMTeLinkSwDescrMinLSPBandwidth)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkSwDescrMTU
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                testValFutRMTeLinkSwDescrMTU
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkSwDescrMTU (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                UINT4 u4FutRMTeLinkSwDescriptorId,
                                UINT4 u4TestValFutRMTeLinkSwDescrMTU)
{
    UINT4               u4swDid = u4FutRMTeLinkSwDescriptorId;

    if ((ValidateFutRMTeLinkSwDescrSetIndex ((UINT4) i4IfIndex, u4swDid) ==
         SNMP_SUCCESS) && (IS_VALID_TE_MTU (u4TestValFutRMTeLinkSwDescrMTU)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkSwDescrIndication
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                testValFutRMTeLinkSwDescrIndication
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkSwDescrIndication (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       UINT4 u4FutRMTeLinkSwDescriptorId,
                                       UINT4
                                       u4TestValFutRMTeLinkSwDescrIndication)
{
    UINT4               u4swDid = u4FutRMTeLinkSwDescriptorId;

    if ((ValidateFutRMTeLinkSwDescrSetIndex ((UINT4) i4IfIndex, u4swDid) ==
         SNMP_SUCCESS)
        && (IS_VALID_TE_INDICATION (u4TestValFutRMTeLinkSwDescrIndication)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkSwDescrRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                testValFutRMTeLinkSwDescrRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkSwDescrRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                      UINT4 u4FutRMTeLinkSwDescriptorId,
                                      INT4 i4TestValFutRMTeLinkSwDescrRowStatus)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeIfDescr       *pteIscD = NULL;
    UINT4               u4swDid = u4FutRMTeLinkSwDescriptorId;

    switch ((UINT1) i4TestValFutRMTeLinkSwDescrRowStatus)
    {
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:
            if (ValidateFutRMTeLinkSwDescrSetIndex ((UINT4) i4IfIndex, u4swDid)
                == SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case ACTIVE:
            if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            else if ((pteIscD = TeFindIscD (u4swDid, pteiface)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            else if ((pteIscD->u4Status == NOT_IN_SERVICE) ||
                     (pteIscD->u4Status == NOT_READY) ||
                     (pteIscD->u4Status == ACTIVE))
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        case DESTROY:
            if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            else if (TeFindIscD (u4swDid, pteiface) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case NOT_IN_SERVICE:
            /*  Not Supported
             *  case falls through */
        default:
            /* End Switch */
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkSwDescrMaxLSPBandwidth
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
                FutRMTeLinkSwDescrSwitchingCap
                FutRMTeLinkSwDescrMaxBwPriority

                The Object 
                testValFutRMTeLinkSwDescrMaxLSPBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkSwDescrMaxLSPBandwidth (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                            UINT4 u4FutRMTeLinkSwDescriptorId,
                                            UINT4
                                            u4FutRMTeLinkSwDescrMaxBwPriority,
                                            UINT4
                                            u4TestValFutRMTeLinkSwDescrMaxLSPBandwidth)
{
    UINT4               u4swDid = u4FutRMTeLinkSwDescriptorId;
    tRmTeLink          *pteiface = NULL;

    UNUSED_PARAM (u4FutRMTeLinkSwDescrMaxBwPriority);
    UNUSED_PARAM (u4TestValFutRMTeLinkSwDescrMaxLSPBandwidth);

    if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if (TeFindIscD (u4swDid, pteiface) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkSwDescrMaxBwRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
                FutRMTeLinkSwDescrSwitchingCap
                FutRMTeLinkSwDescrMaxBwPriority

                The Object 
                testValFutRMTeLinkSwDescrMaxBwRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkSwDescrMaxBwRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                           UINT4 u4FutRMTeLinkSwDescriptorId,
                                           UINT4
                                           u4FutRMTeLinkSwDescrMaxBwPriority,
                                           INT4
                                           i4TestValFutRMTeLinkSwDescrMaxBwRowStatus)
{
    tRmTeLink          *pteiface = NULL;
    UINT4               u4swDid = u4FutRMTeLinkSwDescriptorId;
    UNUSED_PARAM (u4FutRMTeLinkSwDescrMaxBwPriority);

    switch ((UINT1) i4TestValFutRMTeLinkSwDescrMaxBwRowStatus)
    {
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:
            if (ValidateFutRMTeLinkSwDescrSetIndex ((UINT4) i4IfIndex, u4swDid)
                != SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case ACTIVE:
            if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            else if (TeFindIscD (u4swDid, pteiface) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case DESTROY:
            if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            else if (TeFindIscD (u4swDid, pteiface) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case NOT_IN_SERVICE:
            /*  Not Supported
             *  case falls through */
        default:
            /* * End Switch */
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkSrlgRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSrlg

                The Object 
                testValFutRMTeLinkSrlgRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkSrlgRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   UINT4 u4FutRMTeLinkSrlg,
                                   INT4 i4TestValFutRMTeLinkSrlgRowStatus)
{
    tRmTeLink          *pteiface = NULL;
    UINT4               u4SrlgNo = u4FutRMTeLinkSrlg;

    switch ((UINT1) i4TestValFutRMTeLinkSrlgRowStatus)
    {
        case CREATE_AND_GO:
            if (ValidateFutRMTeLinkSrlgSetIndex ((UINT4) i4IfIndex, u4SrlgNo) ==
                SNMP_FAILURE)
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:
        case ACTIVE:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        case DESTROY:
            if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            else if (TeFindSrlg (u4SrlgNo, pteiface) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case NOT_IN_SERVICE:
            /*  Not Supported
             *  case falls through */
        default:
            /* End Switch */
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkUnreservedBandwidth
 Input       :  The Indices
                IfIndex
                FutRMTeLinkBandwidthPriority

                The Object 
                testValFutRMTeLinkUnreservedBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkUnreservedBandwidth (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         UINT4 u4FutRMTeLinkBandwidthPriority,
                                         UINT4
                                         u4TestValFutRMTeLinkUnreservedBandwidth)
{

    UNUSED_PARAM (u4FutRMTeLinkBandwidthPriority);
    UNUSED_PARAM (u4TestValFutRMTeLinkUnreservedBandwidth);

    if (TeFindIf (i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutRMTeLinkBandwidthRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkBandwidthPriority

                The Object 
                testValFutRMTeLinkBandwidthRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutRMTeLinkBandwidthRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        UINT4 u4FutRMTeLinkBandwidthPriority,
                                        INT4
                                        i4TestValFutRMTeLinkBandwidthRowStatus)
{
    UNUSED_PARAM (u4FutRMTeLinkBandwidthPriority);

    switch ((UINT1) i4TestValFutRMTeLinkBandwidthRowStatus)
    {
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:
            if (ValidateFutRMTeLinkSetIndex ((UINT4) i4IfIndex) != SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case ACTIVE:
            if (TeFindIf (i4IfIndex) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case DESTROY:
            if (TeFindIf (i4IfIndex) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case NOT_IN_SERVICE:
            /*  Not Supported
             *  case falls through */
        default:
            /* End Switch */
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for all objects in MIB fsoterm.mib  */

/****************************************************************************
 Function    :  nmhDepv2FutRmTeLinkRegDeregistration
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutRmTeLinkRegDeregistration (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutRMTeLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutRMTeLinkTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutRMTeLinkSwDescriptorTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutRMTeLinkSwDescriptorTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutRMTeLinkSwDescrMaxBwTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
                FutRMTeLinkSwDescrMaxBwPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutRMTeLinkSwDescrMaxBwTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutRMTeLinkSrlgTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSrlg
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutRMTeLinkSrlgTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutRMTeLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkBandwidthPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutRMTeLinkBandwidthTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  fsotertest.c                   */
/*-----------------------------------------------------------------------*/
