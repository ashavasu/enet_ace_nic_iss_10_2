/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: fsrmteif.c,v 1.6 2013/07/06 13:40:25 siva Exp $
*
* Description: Snmp Low Level Routines for set functions.
*********************************************************************/

#include "osteinc.h"
#include "osterm.h"
#include "ostermext.h"

/*****************************************************************************/
/* Function     : utilHashGetValue                                           */
/*                                                                           */
/* Description  : This procedure gets the hash value for the key             */
/*                                                                           */
/* Input        : u4HashSize    :  Max. no. of buckets                       */
/*                pKey           :  Pointer to information to be hashed      */
/*                u1KeyLen      :  Length of the information that is to      */
/*                                   be hashed.                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hash Bucket value                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
utilHashGetValue (UINT4 u4HashSize, UINT1 *pKey, UINT1 u1KeyLen)
{

    UINT4               h = 0;
    UINT4               g;
    UINT1              *p;

    for (p = pKey; u1KeyLen; p++, u1KeyLen--)
    {

        h = (h << 4) + (*p);
        g = h & 0xf0000000;
        if (g)
        {

            h = h ^ (g >> 24);
            h = h ^ g;
            g = h & 0xf0000000;
        }
    }
    return h % u4HashSize;
}

void
TeAddTosortIfLst (tRmTeLink * pTeiface)
{
    tRmTeLink          *pteif = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    TMO_SLL_Scan (&(gteRM.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pteif = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);
        if (pTeiface->u4IfIndex < pteif->u4IfIndex)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&(gteRM.sortTeIfLst), pPrevLstNode,
                    &(pTeiface->NextSortLinkNode));
}

void
TeAddTosortIfDescrLst (tRmTeIfDescr * pTeIscD, tRmTeLink * pTeIface)
{
    tRmTeIfDescr       *pteiscd = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    TMO_SLL_Scan (&(pTeIface->ifDescrList), pLstNode, tTMO_SLL_NODE *)
    {
        pteiscd = GET_TE_ISCD_PTR_FROM_SORT_LST (pLstNode);
        if (pTeIscD->u4DescrId < pteiscd->u4DescrId)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&(pTeIface->ifDescrList), pPrevLstNode,
                    &(pTeIscD->NextIfDescrNode));
}

void
TeAddTosortIfSrlgLst (tRmTeLinkSrlg * pTeSrlg, tRmTeLink * pTeIface)
{
    tRmTeLinkSrlg      *ptesrlg = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    TMO_SLL_Scan (&(pTeIface->srlgList), pLstNode, tTMO_SLL_NODE *)
    {
        ptesrlg = GET_TE_SRLG_PTR_FROM_SORT_LST (pLstNode);
        if (pTeSrlg->u4SrlgNo < ptesrlg->u4SrlgNo)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&(pTeIface->srlgList), pPrevLstNode,
                    &(pTeSrlg->NextLinkSrlgNode));
}

tRmTeLink          *
TeIfCreate (INT4 i4IfIndex)
{
    tRmTeLink          *pteif = NULL;
    UINT4               u4Index;

    /*Allocate Buffer for tRmTeLink data structure */
    if (TELINK_ALLOC (pteif) == NULL)
    {
        return NULL;
    }
    MEMSET (pteif, 0, sizeof (tRmTeLink));

    pteif->u1LinkStatus = INVALID;
    pteif->u4IfIndex = (UINT4) i4IfIndex;
    pteif->u4LocalIpAddr = 0;
    pteif->u4RemoteIpAddr = 0;
    pteif->u4RemoteRtrId = 0;
    pteif->u4TeMetric = 0;
    pteif->u1ProtectionType = 0;
    pteif->u4RsrcClassColor = 0;
    pteif->u4LocalIdentifier = 0;
    pteif->u4RemoteIdentifier = 0;
    pteif->maxBw = 0;
    pteif->maxResBw = 0;
    pteif->u4AreaId = 0;
    pteif->u1InfoType = 0;

    TMO_SLL_Init (&(pteif->ifDescrList));
    TMO_SLL_Init (&(pteif->srlgList));

    for (u4Index = 0; u4Index < OSPF_TE_MAX_PRIORITY_LVL; ++u4Index)
    {
        pteif->aUnResBw[u4Index].u4Status = INVALID;
        pteif->aUnResBw[u4Index].unResBw = 0;
    }
    /* Add this TeInterface to HASH TABLE */
    TMO_HASH_Add_Node (gteRM.pTeIfHashTable, &(pteif->NextLinkNode),
                       IF_HASH_FN (pteif->u4IfIndex), NULL);
    /* Add this TeInterface to sortIfLst */
    TeAddTosortIfLst (pteif);

    return (pteif);
}

tRmTeLink          *
TeFindIf (INT4 i4IfIndex)
{
    tRmTeLink          *pteiface = NULL;
    tTMO_HASH_TABLE    *pTeIfHashTable = gteRM.pTeIfHashTable;

    if (pTeIfHashTable == NULL)
    {
        return NULL;
    }

    TMO_HASH_Scan_Bucket (pTeIfHashTable, IF_HASH_FN (i4IfIndex), pteiface,
                          tRmTeLink *)
    {
        if (pteiface->u4IfIndex == (UINT4) i4IfIndex)
        {
            return (pteiface);
        }
    }
    return (NULL);
}

INT4
TeIfDelete (tRmTeLink * pTeNode)
{
    tTMO_HASH_TABLE    *pTeIfHashTable = gteRM.pTeIfHashTable;
    tRmTeIfDescr       *pifDescr = NULL;
    tRmTeLinkSrlg      *pTeSrlg = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;

    /* Delete all the interface descriptors */
    OSPF_TE_RM_DYNM_SLL_SCAN (&(pTeNode->ifDescrList), pLstNode,
                              pTempNode, tTMO_SLL_NODE *)
    {
        pifDescr = (tRmTeIfDescr *) pLstNode;
        TeIscDesDelete (pifDescr, pTeNode);
    }
    OSPF_TE_RM_DYNM_SLL_SCAN (&(pTeNode->srlgList), pLstNode,
                              pTempNode, tTMO_SLL_NODE *)
    {
        pTeSrlg = (tRmTeLinkSrlg *) pLstNode;
        TeLinkSrlgDelete (pTeSrlg, pTeNode);
    }

    TMO_HASH_Delete_Node (pTeIfHashTable,
                          &(pTeNode->NextLinkNode),
                          IF_HASH_FN (pTeNode->u4IfIndex));
    TMO_SLL_Delete (&(gteRM.sortTeIfLst), &(pTeNode->NextSortLinkNode));
    TELINK_FREE (pTeNode);
    return SNMP_SUCCESS;
}

tRmTeIfDescr       *
TeFindIscD (UINT4 u4IscDesr, tRmTeLink * pteiface)
{
    tRmTeIfDescr       *pifDescr = NULL;

    TMO_SLL_Scan (&(pteiface->ifDescrList), pifDescr, tRmTeIfDescr *)
    {
        if (pifDescr->u4DescrId == u4IscDesr)
        {
            break;
        }
    }
    return (pifDescr);
}

tRmTeIfDescr       *
TeIscDesCreate (UINT4 u4IscDesr, tRmTeLink * pteiface)
{
    tRmTeIfDescr       *pTeIfDescr = NULL;
    UINT4               u4Index;

    /*Allocate Buffer for tRmTeLink data structure */
    if (TEIFDESCR_ALLOC (pTeIfDescr) == NULL)
    {
        return (NULL);
    }
    MEMSET (pTeIfDescr, 0, sizeof (tRmTeIfDescr));

    pTeIfDescr->u4Status = INVALID;
    pTeIfDescr->u4DescrId = u4IscDesr;
    pTeIfDescr->u1SwitchingCap = 0;
    pTeIfDescr->u1EncodingType = 0;
    pTeIfDescr->minLSPBw = 0;
    pTeIfDescr->u2MTU = 0;

    pTeIfDescr->u1Indication = OSPF_TE_INVALID_INDICATION;

    for (u4Index = 0; u4Index < OSPF_TE_MAX_PRIORITY_LVL; ++u4Index)
    {
        pTeIfDescr->aDescrBw[u4Index].u4Status = INVALID;
        pTeIfDescr->aDescrBw[u4Index].maxLSPBw = 0;
    }

    /* Add this Interface Descriptor to TMO_SLL in TELINK */
    TeAddTosortIfDescrLst (pTeIfDescr, pteiface);
    KW_FALSEPOSITIVE_FIX (pTeIfDescr);

    return (pTeIfDescr);
}

UINT4
TeIscDesDelete (tRmTeIfDescr * pIfDesr, tRmTeLink * pteiface)
{
    TMO_SLL_Delete (&(pteiface->ifDescrList), &(pIfDesr->NextIfDescrNode));
    TEIFDESCR_FREE (pIfDesr);
    return SNMP_SUCCESS;
}

tRmTeLinkSrlg      *
TeFindSrlg (UINT4 u4LinkSrlg, tRmTeLink * pteiface)
{
    tRmTeLinkSrlg      *pLinkSrlg = NULL;

    TMO_SLL_Scan (&(pteiface->srlgList), pLinkSrlg, tRmTeLinkSrlg *)
    {
        if (pLinkSrlg->u4SrlgNo == u4LinkSrlg)
        {
            break;
        }
    }
    return (pLinkSrlg);
}

tRmTeLinkSrlg      *
TeLinkSrlgCreate (UINT4 u4LinkSrlg, tRmTeLink * pteiface)
{
    tRmTeLinkSrlg      *pLinkSrlg = NULL;

    /*Allocate Buffer for tRmTeLink data structure */
    if (TELINKSRLG_ALLOC (pLinkSrlg) == NULL)
    {
        return (NULL);
    }

    MEMSET (pLinkSrlg, 0, sizeof (tRmTeLinkSrlg));

    pLinkSrlg->u4Status = INVALID;
    pLinkSrlg->u4SrlgNo = u4LinkSrlg;

    /* Add this Interface Descriptor to TMO_SLL in TELINK */
    TeAddTosortIfSrlgLst (pLinkSrlg, pteiface);
    KW_FALSEPOSITIVE_FIX (pLinkSrlg);
    return (pLinkSrlg);
}

void
TeLinkSrlgDelete (tRmTeLinkSrlg * pTeSrlg, tRmTeLink * pteiface)
{
    TMO_SLL_Delete (&(pteiface->srlgList), &(pTeSrlg->NextLinkSrlgNode));
    TESRLG_FREE (pTeSrlg);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  fsrmteif.c                     */
/*-----------------------------------------------------------------------*/
