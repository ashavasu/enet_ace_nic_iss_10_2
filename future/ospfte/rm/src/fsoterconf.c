
/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: fsoterconf.c,v 1.8 2012/02/01 12:41:44 siva Exp $
*
* Description: Snmp Low Level Routines for set functions.
*********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "osteinc.h"
#include  "fsoterlw.h"
#include  "ostermext.h"
#include  "ostermglob.h"
#include  "ospfte.h"

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFutRmTeLinkRegDeregistration
 Input       :  The Indices

                The Object 
                setValFutRmTeLinkRegDeregistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRmTeLinkRegDeregistration (INT4 i4SetValFutRmTeLinkRegDeregistration)
{
    tRmTeLink          *pTeNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    UINT4               u4HashIndex;

    if ((gteRM.RMregister) != (UINT1) i4SetValFutRmTeLinkRegDeregistration)
    {
        if ((UINT1) i4SetValFutRmTeLinkRegDeregistration == RM_REGISTER)
        {
            if (OspfTeRmgrRegister () != OSPF_TE_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            if (OSIX_FAILURE == OstermSizingMemCreateMemPools ())
            {
                return SNMP_FAILURE;
            }
            LINK_QID = OSTERMMemPoolIds[MAX_OSPF_TE_RM_LINKS_SIZING_ID];
            DESCR_QID = OSTERMMemPoolIds[MAX_OSPF_TE_RM_IF_DESCRS_SIZING_ID];
            SRLG_QID = OSTERMMemPoolIds[MAX_OSPF_TE_SRLG_NO_SIZING_ID];
            /* create  HASH TABLE */
            if (gteRM.pTeIfHashTable == NULL)
            {
                gteRM.pTeIfHashTable =
                    TMO_HASH_Create_Table (IF_HASH_TABLE_SIZE, NULL, FALSE);
            }
            TMO_SLL_Init (&(gteRM.sortTeIfLst));
        }
        else if ((UINT1) i4SetValFutRmTeLinkRegDeregistration == RM_DEREGISTER)
        {
            if (OspfTeRmgrDeRegister () != OSPF_TE_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            TMO_HASH_Scan_Table (gteRM.pTeIfHashTable, u4HashIndex)
            {

                while ((pTeNode
                        = (tRmTeLink *) TMO_HASH_Get_First_Bucket_Node
                        (gteRM.pTeIfHashTable, u4HashIndex)) != NULL)
                {

                    TMO_HASH_Delete_Node (gteRM.pTeIfHashTable,
                                          &(pTeNode->NextLinkNode),
                                          u4HashIndex);
                }
            }
            /* Delete all the interface descriptors */
            OSPF_TE_RM_DYNM_SLL_SCAN (&(gteRM.sortTeIfLst), pLstNode,
                                      pTempNode, tTMO_SLL_NODE *)
            {
                pTeNode = OSPF_TE_RM_GET_BASE_PTR (tRmTeLink, NextSortLinkNode,
                                                   pLstNode);
                TMO_SLL_Delete (&gteRM.sortTeIfLst,
                                &(pTeNode->NextSortLinkNode));
                TeIfDelete (pTeNode);
            }

            /* Delete Hash Table */
            TMO_HASH_Delete_Table (gteRM.pTeIfHashTable, NULL);
            gteRM.pTeIfHashTable = NULL;

            /* Deleting the Memory Pools */
            OstermSizingMemDeleteMemPools ();

            /* Setting the Pool Id value to zero */
            gbufqid.u4LinkId = 0;
            gbufqid.u4DescId = 0;
            gbufqid.u4SrlgId = 0;
        }
        else
        {
            return SNMP_FAILURE;
        }
        gteRM.RMregister = (INT1) i4SetValFutRmTeLinkRegDeregistration;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkLocalIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkLocalIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkLocalIpAddr (INT4 i4IfIndex,
                              UINT4 u4SetValFutRMTeLinkLocalIpAddr)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->u4LocalIpAddr != u4SetValFutRMTeLinkLocalIpAddr)
        {
            pTeIface->u4LocalIpAddr = u4SetValFutRMTeLinkLocalIpAddr;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkRemoteIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkRemoteIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkRemoteIpAddr (INT4 i4IfIndex,
                               UINT4 u4SetValFutRMTeLinkRemoteIpAddr)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->u4RemoteIpAddr != u4SetValFutRMTeLinkRemoteIpAddr)
        {
            pTeIface->u4RemoteIpAddr = u4SetValFutRMTeLinkRemoteIpAddr;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) != OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_FAILURE;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkRemoteRtrId
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkRemoteRtrId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkRemoteRtrId (INT4 i4IfIndex,
                              UINT4 u4SetValFutRMTeLinkRemoteRtrId)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->u4RemoteRtrId != u4SetValFutRMTeLinkRemoteRtrId)
        {
            pTeIface->u4RemoteRtrId = u4SetValFutRMTeLinkRemoteRtrId;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkMetric
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkMetric (INT4 i4IfIndex, UINT4 u4SetValFutRMTeLinkMetric)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->u4TeMetric != u4SetValFutRMTeLinkMetric)
        {
            pTeIface->u4TeMetric = u4SetValFutRMTeLinkMetric;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkProtectionType
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkProtectionType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkProtectionType (INT4 i4IfIndex,
                                 INT4 i4SetValFutRMTeLinkProtectionType)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->u1ProtectionType !=
            (UINT1) i4SetValFutRMTeLinkProtectionType)
        {
            pTeIface->u1ProtectionType =
                (UINT1) i4SetValFutRMTeLinkProtectionType;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkResourceClass
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkResourceClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkResourceClass (INT4 i4IfIndex,
                                UINT4 u4SetValFutRMTeLinkResourceClass)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->u4RsrcClassColor != u4SetValFutRMTeLinkResourceClass)
        {
            pTeIface->u4RsrcClassColor = u4SetValFutRMTeLinkResourceClass;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkIncomingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkIncomingIfId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkIncomingIfId (INT4 i4IfIndex,
                               INT4 i4SetValFutRMTeLinkIncomingIfId)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->u4RemoteIdentifier !=
            (UINT4) i4SetValFutRMTeLinkIncomingIfId)
        {
            pTeIface->u4RemoteIdentifier =
                (UINT4) i4SetValFutRMTeLinkIncomingIfId;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkOutgoingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkOutgoingIfId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkOutgoingIfId (INT4 i4IfIndex,
                               INT4 i4SetValFutRMTeLinkOutgoingIfId)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->u4LocalIdentifier !=
            (UINT4) i4SetValFutRMTeLinkOutgoingIfId)
        {
            pTeIface->u4LocalIdentifier =
                (UINT4) i4SetValFutRMTeLinkOutgoingIfId;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkMaxBw
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkMaxBw
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkMaxBw (INT4 i4IfIndex, UINT4 u4SetValFutRMTeLinkMaxBw)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->maxBw != (UINT4) u4SetValFutRMTeLinkMaxBw)
        {
            pTeIface->maxBw = (float) u4SetValFutRMTeLinkMaxBw;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkMaxResBw
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkMaxResBw
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkMaxResBw (INT4 i4IfIndex, UINT4 u4SetValFutRMTeLinkMaxResBw)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->maxResBw != (UINT4) u4SetValFutRMTeLinkMaxResBw)
        {
            pTeIface->maxResBw = (float) u4SetValFutRMTeLinkMaxResBw;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkAreaId
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkAreaId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkAreaId (INT4 i4IfIndex, UINT4 u4SetValFutRMTeLinkAreaId)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->u4AreaId != (UINT4) u4SetValFutRMTeLinkAreaId)
        {
            pTeIface->u4AreaId = (UINT4) u4SetValFutRMTeLinkAreaId;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkInfoType
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkInfoType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkInfoType (INT4 i4IfIndex, INT4 i4SetValFutRMTeLinkInfoType)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    if ((pTeIface = TeFindIf (i4IfIndex)) != NULL)
    {
        if (pTeIface->u1InfoType != (UINT4) i4SetValFutRMTeLinkInfoType)
        {
            pTeIface->u1InfoType = (UINT4) i4SetValFutRMTeLinkInfoType;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValFutRMTeLinkRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkRowStatus (INT4 i4IfIndex, INT4 i4SetValFutRMTeLinkRowStatus)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;

    switch ((UINT1) i4SetValFutRMTeLinkRowStatus)
    {
        case CREATE_AND_GO:
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:

            pTeIface = TeFindIf (i4IfIndex);
            if (pTeIface != NULL)
            {
                return SNMP_FAILURE;
            }
            if ((pTeIface = TeIfCreate (i4IfIndex)) != NULL)
            {
                pTeIface->u1LinkStatus = NOT_READY;
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;
        case ACTIVE:
            pTeIface = TeFindIf (i4IfIndex);
            if (pTeIface == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            pTeIface->u1LinkStatus = ACTIVE;
            if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
            {
                pMeg->u1LinkStatus = OSPF_TE_LINK_CREATE;
                if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                {
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    return SNMP_SUCCESS;
                }
                if (NULL != pMeg->pIfDescriptors)
                {
                    OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                }
                if (NULL != pMeg->pSrlgs)
                {
                    OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                }
                OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            }
            return SNMP_FAILURE;
        case DESTROY:
            pTeIface = TeFindIf (i4IfIndex);
            if (pTeIface == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if (OSPF_TE_RM_LINK_MSG_ALLOC (pMeg, tRmOsTeLinkMsg) == NULL)
                {
                    return SNMP_FAILURE;
                }
                pMeg->u4LocalIpAddr = pTeIface->u4LocalIpAddr;
                pMeg->u4IfIndex = pTeIface->u4IfIndex;
                pMeg->u4IfDescCnt = 0;
                pMeg->u4IfSrlgCnt = 0;
                pMeg->u1LinkStatus = OSPF_TE_LINK_DELETE;

                if (OspfTeRmgrSendLinkInfo (pMeg) != OSPF_TE_SUCCESS)
                {
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    return SNMP_FAILURE;
                }
                if (NULL != pMeg->pIfDescriptors)
                {
                    OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                }
                if (NULL != pMeg->pSrlgs)
                {
                    OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                }
                OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            }
            TeIfDelete (pTeIface);    /* Deleting the TE IFACE NODE */
            return SNMP_SUCCESS;

            /* FREE DESCRIPTOR MEMORY */
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE presently not used.
             * CASE falls through
             */
        default:
            return SNMP_FAILURE;
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkSwDescrSwitchingCap
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                setValFutRMTeLinkSwDescrSwitchingCap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkSwDescrSwitchingCap (INT4 i4IfIndex,
                                      UINT4 u4FutRMTeLinkSwDescriptorId,
                                      INT4
                                      i4SetValFutRMTeLinkSwDescrSwitchingCap)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeLink          *pTeIface = NULL;
    tRmTeIfDescr       *pIfDescr = NULL;
    UINT4               u4IscDesr = u4FutRMTeLinkSwDescriptorId;

    pTeIface = TeFindIf (i4IfIndex);
    if (pTeIface == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfDescr = TeFindIscD (u4IscDesr, pTeIface);
    if (pIfDescr == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIfDescr->u1SwitchingCap !=
        (UINT1) i4SetValFutRMTeLinkSwDescrSwitchingCap)
    {
        pIfDescr->u1SwitchingCap =
            (UINT1) i4SetValFutRMTeLinkSwDescrSwitchingCap;

        if ((pTeIface->u1LinkStatus == ACTIVE)
            && (pIfDescr->u4Status == ACTIVE))
        {
            if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
            {
                if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                {
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    return SNMP_SUCCESS;
                }
                if (NULL != pMeg->pIfDescriptors)
                {
                    OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                }
                if (NULL != pMeg->pSrlgs)
                {
                    OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                }
                OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            }
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkSwDescrEncodingType
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                setValFutRMTeLinkSwDescrEncodingType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkSwDescrEncodingType (INT4 i4IfIndex,
                                      UINT4 u4FutRMTeLinkSwDescriptorId,
                                      INT4
                                      i4SetValFutRMTeLinkSwDescrEncodingType)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeIfDescr       *pIfDescr = NULL;
    tRmOsTeLinkMsg     *pMeg = NULL;
    UINT4               u4IscDesr = u4FutRMTeLinkSwDescriptorId;

    pTeIface = TeFindIf (i4IfIndex);
    if (pTeIface == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfDescr = TeFindIscD (u4IscDesr, pTeIface);
    if (pIfDescr == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIfDescr->u1EncodingType !=
        (UINT1) i4SetValFutRMTeLinkSwDescrEncodingType)
    {
        pIfDescr->u1EncodingType =
            (UINT1) i4SetValFutRMTeLinkSwDescrEncodingType;

        if ((pTeIface->u1LinkStatus == ACTIVE)
            && (pIfDescr->u4Status == ACTIVE))
        {
            if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
            {
                if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                {
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    return SNMP_SUCCESS;
                }
                if (NULL != pMeg->pIfDescriptors)
                {
                    OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                }
                if (NULL != pMeg->pSrlgs)
                {
                    OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                }
                OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            }
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkSwDescrMinLSPBandwidth
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                setValFutRMTeLinkSwDescrMinLSPBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkSwDescrMinLSPBandwidth (INT4 i4IfIndex,
                                         UINT4 u4FutRMTeLinkSwDescriptorId,
                                         UINT4
                                         u4SetValFutRMTeLinkSwDescrMinLSPBandwidth)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeIfDescr       *pIfDescr = NULL;
    tRmOsTeLinkMsg     *pMeg = NULL;
    UINT4               u4IscDesr = u4FutRMTeLinkSwDescriptorId;

    pTeIface = TeFindIf (i4IfIndex);
    if (pTeIface == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfDescr = TeFindIscD (u4IscDesr, pTeIface);
    if (pIfDescr == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIfDescr->minLSPBw != u4SetValFutRMTeLinkSwDescrMinLSPBandwidth)
    {
        pIfDescr->minLSPBw = (float) u4SetValFutRMTeLinkSwDescrMinLSPBandwidth;

        if ((pTeIface->u1LinkStatus == ACTIVE)
            && (pIfDescr->u4Status == ACTIVE))
        {
            if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
            {
                if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                {
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    return SNMP_SUCCESS;
                }
                if (NULL != pMeg->pIfDescriptors)
                {
                    OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                }
                if (NULL != pMeg->pSrlgs)
                {
                    OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                }
                OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            }
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkSwDescrMTU
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                setValFutRMTeLinkSwDescrMTU
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkSwDescrMTU (INT4 i4IfIndex,
                             UINT4 u4FutRMTeLinkSwDescriptorId,
                             UINT4 u4SetValFutRMTeLinkSwDescrMTU)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeIfDescr       *pIfDescr = NULL;
    tRmOsTeLinkMsg     *pMeg = NULL;
    UINT4               u4IscDesr = u4FutRMTeLinkSwDescriptorId;

    pTeIface = TeFindIf (i4IfIndex);
    if (pTeIface == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfDescr = TeFindIscD (u4IscDesr, pTeIface);
    if (pIfDescr == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIfDescr->u2MTU != (UINT2) u4SetValFutRMTeLinkSwDescrMTU)
    {
        pIfDescr->u2MTU = (UINT2) u4SetValFutRMTeLinkSwDescrMTU;

        if ((pTeIface->u1LinkStatus == ACTIVE)
            && (pIfDescr->u4Status == ACTIVE))
        {
            if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
            {
                if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                {
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    return SNMP_SUCCESS;
                }
                if (NULL != pMeg->pIfDescriptors)
                {
                    OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                }
                if (NULL != pMeg->pSrlgs)
                {
                    OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                }
                OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            }
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkSwDescrIndication
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                setValFutRMTeLinkSwDescrIndication
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkSwDescrIndication (INT4 i4IfIndex,
                                    UINT4 u4FutRMTeLinkSwDescriptorId,
                                    UINT4 u4SetValFutRMTeLinkSwDescrIndication)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeIfDescr       *pIfDescr = NULL;
    tRmOsTeLinkMsg     *pMeg = NULL;
    UINT4               u4IscDesr = u4FutRMTeLinkSwDescriptorId;

    pTeIface = TeFindIf (i4IfIndex);
    if (pTeIface == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfDescr = TeFindIscD (u4IscDesr, pTeIface);
    if (pIfDescr == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIfDescr->u1Indication != u4SetValFutRMTeLinkSwDescrIndication)
    {
        pIfDescr->u1Indication = (UINT1) u4SetValFutRMTeLinkSwDescrIndication;

        if ((pTeIface->u1LinkStatus == ACTIVE)
            && (pIfDescr->u4Status == ACTIVE))
        {
            if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
            {
                if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                {
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    return SNMP_SUCCESS;
                }
                if (NULL != pMeg->pIfDescriptors)
                {
                    OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                }
                if (NULL != pMeg->pSrlgs)
                {
                    OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                }
                OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            }
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkSwDescrRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                setValFutRMTeLinkSwDescrRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkSwDescrRowStatus (INT4 i4IfIndex,
                                   UINT4 u4FutRMTeLinkSwDescriptorId,
                                   INT4 i4SetValFutRMTeLinkSwDescrRowStatus)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeIfDescr       *pIfDescr = NULL;
    tRmOsTeLinkMsg     *pMeg = NULL;
    UINT4               u4IscDesr = u4FutRMTeLinkSwDescriptorId;

    u4IscDesr = u4FutRMTeLinkSwDescriptorId;
    switch ((UINT1) i4SetValFutRMTeLinkSwDescrRowStatus)
    {
        case CREATE_AND_GO:
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:
            pteiface = TeFindIf (i4IfIndex);
            if (pteiface == NULL)
            {
                return SNMP_FAILURE;
            }
            pIfDescr = TeFindIscD (u4IscDesr, pteiface);
            if (pIfDescr != NULL)
            {
                return SNMP_FAILURE;
            }
            if ((pIfDescr = TeIscDesCreate (u4IscDesr, pteiface)) != NULL)
            {
                pIfDescr->u4Status = NOT_READY;
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;
        case ACTIVE:
            pteiface = TeFindIf (i4IfIndex);
            if (pteiface == NULL)
            {
                return SNMP_FAILURE;
            }
            pIfDescr = TeFindIscD (u4IscDesr, pteiface);
            if (pIfDescr == NULL)
            {
                return SNMP_FAILURE;
            }
            pIfDescr->u4Status = ACTIVE;
            if (pteiface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pteiface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    return SNMP_SUCCESS;
                }
            }
            return SNMP_FAILURE;
        case DESTROY:
            pteiface = TeFindIf (i4IfIndex);
            if ((pteiface == NULL) || (pteiface->u1LinkStatus == INVALID))
            {
                return SNMP_FAILURE;
            }
            pIfDescr = TeFindIscD (u4IscDesr, pteiface);
            if (pIfDescr == NULL)
            {
                return SNMP_FAILURE;
            }
            TeIscDesDelete (pIfDescr, pteiface);
            if ((pMeg = OspfTeRmFillMsgInfo (pteiface)) != NULL)
            {
                if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                {
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    return SNMP_SUCCESS;
                }
                if (NULL != pMeg->pIfDescriptors)
                {
                    OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                }
                if (NULL != pMeg->pSrlgs)
                {
                    OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                }
                OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            }
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE presently not used.
             * CASE falls through
             */
        default:
            return SNMP_FAILURE;
    }

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkSwDescrMaxLSPBandwidth
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
                FutRMTeLinkSwDescrSwitchingCap
                FutRMTeLinkSwDescrMaxBwPriority

                The Object 
                setValFutRMTeLinkSwDescrMaxLSPBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkSwDescrMaxLSPBandwidth (INT4 i4IfIndex,
                                         UINT4
                                         u4FutRMTeLinkSwDescriptorId,
                                         UINT4
                                         u4FutRMTeLinkSwDescrMaxBwPriority,
                                         UINT4
                                         u4SetValFutRMTeLinkSwDescrMaxLSPBandwidth)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeIfDescr       *pIfDescr = NULL;
    tRmOsTeLinkMsg     *pMeg = NULL;
    UINT4               u4MaxLSPBw;
    UINT4               u4IscDesr = u4FutRMTeLinkSwDescriptorId;

    u4MaxLSPBw = u4SetValFutRMTeLinkSwDescrMaxLSPBandwidth;
    pTeIface = TeFindIf (i4IfIndex);
    if (pTeIface == NULL)
    {
        return SNMP_FAILURE;
    }
    pIfDescr = TeFindIscD (u4IscDesr, pTeIface);
    if (pIfDescr == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIfDescr->aDescrBw[u4FutRMTeLinkSwDescrMaxBwPriority].maxLSPBw !=
        u4MaxLSPBw)
    {
        pIfDescr->aDescrBw[u4FutRMTeLinkSwDescrMaxBwPriority].maxLSPBw =
            (float) u4MaxLSPBw;
        if ((pTeIface->u1LinkStatus == ACTIVE) && (pIfDescr->u4Status == ACTIVE)
            && (pIfDescr->aDescrBw[u4FutRMTeLinkSwDescrMaxBwPriority].
                u4Status == ACTIVE))
        {
            if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
            {
                if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                {
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    return SNMP_SUCCESS;
                }
                if (NULL != pMeg->pIfDescriptors)
                {
                    OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                }
                if (NULL != pMeg->pSrlgs)
                {
                    OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                }
                OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            }
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkSwDescrMaxBwRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
                FutRMTeLinkSwDescrSwitchingCap
                FutRMTeLinkSwDescrMaxBwPriority

                The Object 
                setValFutRMTeLinkSwDescrMaxBwRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkSwDescrMaxBwRowStatus (INT4 i4IfIndex,
                                        UINT4
                                        u4FutRMTeLinkSwDescriptorId,
                                        UINT4
                                        u4FutRMTeLinkSwDescrMaxBwPriority,
                                        INT4
                                        i4SetValFutRMTeLinkSwDescrMaxBwRowStatus)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeIfDescr       *pIfDescr = NULL;
    tRmOsTeLinkMsg     *pMeg = NULL;
    UINT4               u4Prindex;
    UINT4               u4IscDesr = u4FutRMTeLinkSwDescriptorId;

    u4Prindex = u4FutRMTeLinkSwDescrMaxBwPriority;
    u4IscDesr = u4FutRMTeLinkSwDescriptorId;
    switch ((UINT1) i4SetValFutRMTeLinkSwDescrMaxBwRowStatus)
    {
        case CREATE_AND_GO:
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:
            pteiface = TeFindIf (i4IfIndex);
            if (pteiface == NULL)
            {
                return SNMP_FAILURE;
            }
            pIfDescr = TeFindIscD (u4IscDesr, pteiface);
            if (pIfDescr == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pIfDescr->aDescrBw[u4Prindex].u4Status == INVALID)
            {
                pIfDescr->aDescrBw[u4Prindex].u4Status = NOT_READY;
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;
        case ACTIVE:
            pteiface = TeFindIf (i4IfIndex);
            if (pteiface == NULL)
            {
                return SNMP_FAILURE;
            }
            pIfDescr = TeFindIscD (u4IscDesr, pteiface);
            if (pIfDescr == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pIfDescr->aDescrBw[u4Prindex].u4Status == NOT_READY)
            {
                pIfDescr->aDescrBw[u4Prindex].u4Status = ACTIVE;
                if ((pteiface->u1LinkStatus == ACTIVE)
                    && (pIfDescr->u4Status == ACTIVE))
                {
                    if ((pMeg = OspfTeRmFillMsgInfo (pteiface)) != NULL)
                    {
                        if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                        {
                            if (NULL != pMeg->pIfDescriptors)
                            {
                                OSPF_TE_RM_LINK_INFO_FREE (pMeg->
                                                           pIfDescriptors);
                            }
                            if (NULL != pMeg->pSrlgs)
                            {
                                OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                            }
                            OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                            return SNMP_SUCCESS;
                        }
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    }
                }
            }
            return SNMP_FAILURE;
        case DESTROY:
            pteiface = TeFindIf (i4IfIndex);
            if (pteiface == NULL)
            {
                return SNMP_FAILURE;
            }
            pIfDescr = TeFindIscD (u4IscDesr, pteiface);
            if (pIfDescr == NULL)
            {
                return SNMP_FAILURE;
            }
            pIfDescr->aDescrBw[u4Prindex].u4Status = INVALID;
            pIfDescr->aDescrBw[u4Prindex].maxLSPBw = 0;

            if ((pteiface->u1LinkStatus == ACTIVE)
                && (pIfDescr->u4Status == ACTIVE))
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pteiface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
            }
            return SNMP_SUCCESS;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE presently not used.
             * CASE falls through
             */
        default:
            return SNMP_FAILURE;
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkUnreservedBandwidth
 Input       :  The Indices
                IfIndex
                FutRMTeLinkBandwidthPriority

                The Object 
                setValFutRMTeLinkUnreservedBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkUnreservedBandwidth (INT4 i4IfIndex,
                                      UINT4
                                      u4FutRMTeLinkBandwidthPriority,
                                      UINT4
                                      u4SetValFutRMTeLinkUnreservedBandwidth)
{
    tRmTeLink          *pTeIface = NULL;
    tRmOsTeLinkMsg     *pMeg = NULL;
    UINT4               u4UnResBw;
    UINT4               u4Prindex;

    u4Prindex = u4FutRMTeLinkBandwidthPriority;
    u4UnResBw = u4SetValFutRMTeLinkUnreservedBandwidth;

    pTeIface = TeFindIf (i4IfIndex);
    if (pTeIface == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pTeIface->aUnResBw[u4Prindex].unResBw != u4UnResBw)
    {
        pTeIface->aUnResBw[u4Prindex].unResBw = (float) u4UnResBw;
        if ((pTeIface->u1LinkStatus == ACTIVE) &&
            (pTeIface->aUnResBw[u4Prindex].u4Status == ACTIVE))
        {
            if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
            {
                if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                {
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    return SNMP_SUCCESS;
                }
                if (NULL != pMeg->pIfDescriptors)
                {
                    OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                }
                if (NULL != pMeg->pSrlgs)
                {
                    OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                }
                OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            }
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkBandwidthRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkBandwidthPriority

                The Object 
                setValFutRMTeLinkBandwidthRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkBandwidthRowStatus (INT4 i4IfIndex,
                                     UINT4
                                     u4FutRMTeLinkBandwidthPriority,
                                     INT4 i4SetValFutRMTeLinkBandwidthRowStatus)
{
    tRmTeLink          *pTeIface = NULL;
    tRmOsTeLinkMsg     *pMeg = NULL;
    UINT4               u4Prindex;

    u4Prindex = u4FutRMTeLinkBandwidthPriority;
    switch ((UINT1) i4SetValFutRMTeLinkBandwidthRowStatus)
    {
        case CREATE_AND_GO:
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:
            pTeIface = TeFindIf (i4IfIndex);
            if (pTeIface == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pTeIface->aUnResBw[u4Prindex].u4Status == INVALID)
            {
                pTeIface->aUnResBw[u4Prindex].u4Status = NOT_READY;
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;
        case ACTIVE:
            pTeIface = TeFindIf (i4IfIndex);
            if (pTeIface == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pTeIface->aUnResBw[u4Prindex].u4Status == NOT_READY)
            {
                pTeIface->aUnResBw[u4Prindex].u4Status = ACTIVE;
                if ((pTeIface->u1LinkStatus == ACTIVE))
                {
                    if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                    {
                        if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                        {
                            if (NULL != pMeg->pIfDescriptors)
                            {
                                OSPF_TE_RM_LINK_INFO_FREE (pMeg->
                                                           pIfDescriptors);
                            }
                            if (NULL != pMeg->pSrlgs)
                            {
                                OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                            }
                            OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                            return SNMP_SUCCESS;
                        }
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                    }
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;
        case DESTROY:
            pTeIface = TeFindIf (i4IfIndex);
            if (pTeIface == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pTeIface->aUnResBw[u4Prindex].u4Status == INVALID)
            {
                return SNMP_FAILURE;
            }

            pTeIface->aUnResBw[u4Prindex].u4Status = INVALID;
            pTeIface->aUnResBw[u4Prindex].unResBw = 0;
            if ((pTeIface->u1LinkStatus == ACTIVE))
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE presently not used.
             * CASE falls through
             */
        default:
            return SNMP_FAILURE;
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutRMTeLinkSrlgRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSrlg

                The Object 
                setValFutRMTeLinkSrlgRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutRMTeLinkSrlgRowStatus (INT4 i4IfIndex, UINT4 u4FutRMTeLinkSrlg,
                                INT4 i4SetValFutRMTeLinkSrlgRowStatus)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeLinkSrlg      *pLinkSrlg = NULL;
    tRmOsTeLinkMsg     *pMeg = NULL;
    UINT4               u4LinkSrlg;

    u4LinkSrlg = u4FutRMTeLinkSrlg;
    switch ((UINT1) i4SetValFutRMTeLinkSrlgRowStatus)
    {
        case CREATE_AND_GO:
            pTeIface = TeFindIf (i4IfIndex);
            if (pTeIface == NULL)
            {
                return SNMP_FAILURE;
            }
            pLinkSrlg = TeFindSrlg (u4LinkSrlg, pTeIface);
            if (pLinkSrlg != NULL)
            {
                return SNMP_FAILURE;
            }
            if ((pLinkSrlg = TeLinkSrlgCreate (u4LinkSrlg, pTeIface)) == NULL)
            {
                return SNMP_FAILURE;
            }

            pLinkSrlg->u4Status = ACTIVE;
            if (pTeIface->u1LinkStatus == ACTIVE)
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case DESTROY:
            pTeIface = TeFindIf (i4IfIndex);
            if (pTeIface == NULL)
            {
                return SNMP_FAILURE;
            }
            pLinkSrlg = TeFindSrlg (u4LinkSrlg, pTeIface);
            if (pLinkSrlg == NULL)
            {
                return SNMP_FAILURE;
            }
            TeLinkSrlgDelete (pLinkSrlg, pTeIface);

            if ((pTeIface->u1LinkStatus == ACTIVE))
            {
                if ((pMeg = OspfTeRmFillMsgInfo (pTeIface)) != NULL)
                {
                    if (OspfTeRmgrSendLinkInfo (pMeg) == OSPF_TE_SUCCESS)
                    {
                        if (NULL != pMeg->pIfDescriptors)
                        {
                            OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                        }
                        if (NULL != pMeg->pSrlgs)
                        {
                            OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                        }
                        OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                        return SNMP_SUCCESS;
                    }
                    if (NULL != pMeg->pIfDescriptors)
                    {
                        OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
                    }
                    if (NULL != pMeg->pSrlgs)
                    {
                        OSPF_TE_RM_LINK_SRLG_INFO_FREE (pMeg->pSrlgs);
                    }
                    OSPF_TE_RM_LINK_MSG_FREE (pMeg);
                }
                return SNMP_FAILURE;

            }
            return SNMP_SUCCESS;
        case CREATE_AND_WAIT:
            /* No need to support Create and wait */
        case ACTIVE:
            /* Because CreateAndWait need not support ACTIVE is not
             * having any meaning */
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE presently not used.
             * CASE falls through
             */
        default:
            return SNMP_FAILURE;
    }
}

/*****************************************************************************/
/*  Function    :  OspfTeRmFillMsgInfo                                       */
/*                                                                           */
/*  Description :  This function creates the message and fill the message    */
/*                 with link information and returns pointer to the message  */
/*                                                                           */
/*  Input       :  pTeIface - Holds pointer to TE interface                  */
/*                                                                           */
/*  Output      :  None                                                      */
/*                                                                           */
/*  Returns     :  Pointer to message with TE information                    */
/*                 NULL otherwise.                                           */
/*****************************************************************************/
PUBLIC tRmOsTeLinkMsg *
OspfTeRmFillMsgInfo (tRmTeLink * pTeIface)
{
    tRmOsTeLinkMsg     *pMeg = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    tRmTeLinkSrlg      *pTeSrlg = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4Temp = 0;
    UINT1              *pifDesLst = NULL;
    UINT1              *pTempLst = NULL;
    UINT1              *pifSrlgLst = NULL;
    tOsTeIfDesc         ifDesc;
    UINT1               u1Priority;

    MEMSET (&ifDesc, 0, sizeof (tOsTeIfDesc));
    if (OSPF_TE_RM_LINK_MSG_ALLOC (pMeg, tRmOsTeLinkMsg) == NULL)
    {
        return NULL;
    }

    pMeg->u4LocalIpAddr = pTeIface->u4LocalIpAddr;
    pMeg->u4IfIndex = pTeIface->u4IfIndex;
    pMeg->u4RemoteIpAddr = pTeIface->u4RemoteIpAddr;
    pMeg->u4RemoteRtrId = pTeIface->u4RemoteRtrId;
    pMeg->u4TeMetric = pTeIface->u4TeMetric;
    pMeg->u1ProtectionType = pTeIface->u1ProtectionType;
    pMeg->u4RsrcClassColor = pTeIface->u4RsrcClassColor;
    pMeg->u4LocalIdentifier = pTeIface->u4LocalIdentifier;
    pMeg->u4RemoteIdentifier = pTeIface->u4RemoteIdentifier;
    pMeg->maxBw = pTeIface->maxBw * OSPFTE_RM_THOUSAND_BYTES;
    pMeg->maxResBw = pTeIface->maxResBw * OSPFTE_RM_THOUSAND_BYTES;
    pMeg->u4AreaId = pTeIface->u4AreaId;
    pMeg->u1LinkStatus = OSPF_TE_LINK_UPDATE;
    pMeg->u1InfoType = (UINT1) pTeIface->u1InfoType;
    for (u1Priority = 0; u1Priority < OSPF_TE_MAX_PRIORITY_LVL; u1Priority++)
    {
        pMeg->aUnResBw[u1Priority] =
            pTeIface->aUnResBw[u1Priority].unResBw * OSPFTE_RM_THOUSAND_BYTES;
    }

    pMeg->u4IfDescCnt = 0;

    if (pTeIface->ifDescrList.u4_Count != 0)
    {
        OSPF_TE_RM_LINK_INFO_ALLOC (pifDesLst, UINT1);
        if (pifDesLst == NULL)
        {
            OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            return NULL;
        }
        MEMSET (pifDesLst, 0,
                (sizeof (tOsTeIfDesc) * pTeIface->ifDescrList.u4_Count));
    }

    pTempLst = pifDesLst;
    TMO_SLL_Scan (&pTeIface->ifDescrList, pLstNode, tTMO_SLL_NODE *)
    {
        pTeIscD = GET_TE_ISCD_PTR_FROM_SORT_LST (pLstNode);

        if (pTeIscD->u4Status != ACTIVE)
        {
            continue;
        }

        ifDesc.u4DescrId = pTeIscD->u4DescrId;
        ifDesc.u1SwitchingCap = pTeIscD->u1SwitchingCap;
        ifDesc.u1EncodingType = pTeIscD->u1EncodingType;
        ifDesc.u1Indication = pTeIscD->u1Indication;
        ifDesc.u2MTU = pTeIscD->u2MTU;
        ifDesc.minLSPBw = pTeIscD->minLSPBw * OSPFTE_RM_THOUSAND_BYTES;
        for (u1Priority = 0; u1Priority < OSPF_TE_MAX_PRIORITY_LVL;
             u1Priority++)
        {
            ifDesc.aDescMaxLSPBw[u1Priority] =
                pTeIscD->aDescrBw[u1Priority].maxLSPBw *
                OSPFTE_RM_THOUSAND_BYTES;
        }
        OSPF_TE_RM_MEMCPY (pTempLst, &ifDesc, sizeof (tOsTeIfDesc));
        pTempLst = pTempLst + sizeof (tOsTeIfDesc);
        pMeg->u4IfDescCnt += 1;
    }
    pMeg->pIfDescriptors = (UINT1 *) pifDesLst;

    pMeg->u4IfSrlgCnt = pTeIface->srlgList.u4_Count;
    if (pMeg->u4IfSrlgCnt != 0)
    {
        if (OSPF_TE_RM_LINK_SRLG_INFO_ALLOC (pifSrlgLst, UINT1) == NULL)
        {
            if (NULL != pMeg->pIfDescriptors)
            {
                OSPF_TE_RM_LINK_INFO_FREE (pMeg->pIfDescriptors);
            }
            OSPF_TE_RM_LINK_MSG_FREE (pMeg);
            return NULL;
        }

        MEMSET (pifSrlgLst, 0, 4 * pMeg->u4IfSrlgCnt);
        pTempLst = pifSrlgLst;
        TMO_SLL_Scan (&pTeIface->srlgList, pLstNode, tTMO_SLL_NODE *)
        {
            pTeSrlg = GET_TE_SRLG_PTR_FROM_SORT_LST (pLstNode);
            u4Temp = pTeSrlg->u4SrlgNo;
            MEMCPY (pTempLst, (UINT1 *) &u4Temp, 4);
            pTempLst += 4;
        }
    }
    pMeg->pSrlgs = (UINT1 *) pifSrlgLst;
    return (pMeg);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  fsoterconf.c                   */
/*-----------------------------------------------------------------------*/
