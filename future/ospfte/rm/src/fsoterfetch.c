
/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: fsoterfetch.c,v 1.4 2014/02/14 14:07:36 siva Exp $
*
* Description: Snmp Low Level Routines for set functions.
*********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "fsoterlw.h"
#include  "osterm.h"
#include  "ostermext.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutRmTeLinkRegDeregistration
 Input       :  The Indices

                The Object 
                retValFutRmTeLinkRegDeregistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRmTeLinkRegDeregistration (INT4 *pi4RetValFutRmTeLinkRegDeregistration)
{
    *pi4RetValFutRmTeLinkRegDeregistration = (INT4) (gteRM.RMregister);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutRMTeLinkTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutRMTeLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutRMTeLinkTable (INT4 i4IfIndex)
{
    if (TeFindIf (i4IfIndex) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutRMTeLinkTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutRMTeLinkTable (INT4 *pi4IfIndex)
{
    tRmTeLink          *pTeIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pLstNode = TMO_SLL_First (&(gteRM.sortTeIfLst))) != NULL)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);

        *pi4IfIndex = (INT4) (pTeIface->u4IfIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutRMTeLinkTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutRMTeLinkTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRmTeLink          *pTeIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    TMO_SLL_Scan (&(gteRM.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);
        if (pTeIface->u4IfIndex > (UINT4) i4IfIndex)
        {
            *pi4NextIfIndex = (INT4) pTeIface->u4IfIndex;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkLocalIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkLocalIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkLocalIpAddr (INT4 i4IfIndex,
                              UINT4 *pu4RetValFutRMTeLinkLocalIpAddr)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pu4RetValFutRMTeLinkLocalIpAddr = pteiface->u4LocalIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkRemoteIpAddr
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkRemoteIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkRemoteIpAddr (INT4 i4IfIndex,
                               UINT4 *pu4RetValFutRMTeLinkRemoteIpAddr)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pu4RetValFutRMTeLinkRemoteIpAddr = pteiface->u4RemoteIpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkRemoteRtrId
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkRemoteRtrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkRemoteRtrId (INT4 i4IfIndex,
                              UINT4 *pu4RetValFutRMTeLinkRemoteRtrId)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pu4RetValFutRMTeLinkRemoteRtrId = pteiface->u4RemoteRtrId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkMetric
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkMetric (INT4 i4IfIndex, UINT4 *pu4RetValFutRMTeLinkMetric)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pu4RetValFutRMTeLinkMetric = (UINT4) pteiface->u4TeMetric;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkProtectionType
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkProtectionType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkProtectionType (INT4 i4IfIndex,
                                 INT4 *pi4RetValFutRMTeLinkProtectionType)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pi4RetValFutRMTeLinkProtectionType = (INT4) pteiface->u1ProtectionType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkResourceClass
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkResourceClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkResourceClass (INT4 i4IfIndex,
                                UINT4 *pu4RetValFutRMTeLinkResourceClass)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pu4RetValFutRMTeLinkResourceClass = pteiface->u4RsrcClassColor;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkIncomingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkIncomingIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkIncomingIfId (INT4 i4IfIndex,
                               INT4 *pi4RetValFutRMTeLinkIncomingIfId)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pi4RetValFutRMTeLinkIncomingIfId = (INT4) pteiface->u4RemoteIdentifier;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkOutgoingIfId
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkOutgoingIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkOutgoingIfId (INT4 i4IfIndex,
                               INT4 *pi4RetValFutRMTeLinkOutgoingIfId)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pi4RetValFutRMTeLinkOutgoingIfId = (INT4) pteiface->u4LocalIdentifier;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkMaxBw
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkMaxBw
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkMaxBw (INT4 i4IfIndex, UINT4 *pu4RetValFutRMTeLinkMaxBw)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pu4RetValFutRMTeLinkMaxBw = (UINT4) pteiface->maxBw;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkMaxResBw
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkMaxResBw
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkMaxResBw (INT4 i4IfIndex, UINT4 *pu4RetValFutRMTeLinkMaxResBw)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pu4RetValFutRMTeLinkMaxResBw = (UINT4) pteiface->maxResBw;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkAreaId
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkAreaId (INT4 i4IfIndex, UINT4 *pu4RetValFutRMTeLinkAreaId)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pu4RetValFutRMTeLinkAreaId = pteiface->u4AreaId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkInfoType
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkInfoType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkInfoType (INT4 i4IfIndex, INT4 *pi4RetValFutRMTeLinkInfoType)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pi4RetValFutRMTeLinkInfoType = (INT4) pteiface->u1InfoType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValFutRMTeLinkRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkRowStatus (INT4 i4IfIndex, INT4 *pi4RetValFutRMTeLinkRowStatus)
{
    tRmTeLink          *pteiface = NULL;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pi4RetValFutRMTeLinkRowStatus = (INT4) pteiface->u1LinkStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutRMTeLinkSwDescriptorTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutRMTeLinkSwDescriptorTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutRMTeLinkSwDescriptorTable (INT4 i4IfIndex,
                                                      UINT4
                                                      u4FutRMTeLinkSwDescriptorId)
{
    tRmTeLink          *pTeIface = NULL;
    UINT4               u4teIscD = u4FutRMTeLinkSwDescriptorId;

    if ((pTeIface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if (TeFindIscD (u4teIscD, pTeIface) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutRMTeLinkSwDescriptorTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutRMTeLinkSwDescriptorTable (INT4 *pi4IfIndex,
                                              UINT4
                                              *pu4FutRMTeLinkSwDescriptorId)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pLstNode = TMO_SLL_First (&(gteRM.sortTeIfLst))) != NULL)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);

        if ((pLstNode = TMO_SLL_First (&(pTeIface->ifDescrList))) != NULL)
        {
            pTeIscD = GET_TE_ISCD_PTR_FROM_SORT_LST (pLstNode);

            *pi4IfIndex = (INT4) (pTeIface->u4IfIndex);
            *pu4FutRMTeLinkSwDescriptorId = (UINT4) pTeIscD->u4DescrId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutRMTeLinkSwDescriptorTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FutRMTeLinkSwDescriptorId
                nextFutRMTeLinkSwDescriptorId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutRMTeLinkSwDescriptorTable (INT4 i4IfIndex,
                                             INT4 *pi4NextIfIndex,
                                             UINT4 u4FutRMTeLinkSwDescriptorId,
                                             UINT4
                                             *pu4NextFutRMTeLinkSwDescriptorId)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;

    TMO_SLL_Scan (&(gteRM.sortTeIfLst), pIfNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pIfNode);
        if (pTeIface->u4IfIndex == (UINT4) i4IfIndex)
        {
            TMO_SLL_Scan (&(pTeIface->ifDescrList), pLstNode, tTMO_SLL_NODE *)
            {
                pTeIscD = GET_TE_ISCD_PTR_FROM_SORT_LST (pLstNode);

                if (pTeIscD->u4DescrId > u4FutRMTeLinkSwDescriptorId)
                {
                    *pi4NextIfIndex = (INT4) pTeIface->u4IfIndex;
                    *pu4NextFutRMTeLinkSwDescriptorId =
                        (UINT4) pTeIscD->u4DescrId;
                    return SNMP_SUCCESS;
                }
            }
        }
        else if (pTeIface->u4IfIndex > (UINT4) i4IfIndex)
        {
            if ((pLstNode = TMO_SLL_First (&(pTeIface->ifDescrList))) != NULL)
            {
                pTeIscD = GET_TE_ISCD_PTR_FROM_SORT_LST (pLstNode);
                *pi4NextIfIndex = (INT4) pTeIface->u4IfIndex;
                *pu4NextFutRMTeLinkSwDescriptorId = (UINT4) pTeIscD->u4DescrId;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkSwDescrSwitchingCap
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                retValFutRMTeLinkSwDescrSwitchingCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkSwDescrSwitchingCap (INT4 i4IfIndex,
                                      UINT4 u4FutRMTeLinkSwDescriptorId,
                                      INT4
                                      *pi4RetValFutRMTeLinkSwDescrSwitchingCap)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    UINT4               u4IscD = u4FutRMTeLinkSwDescriptorId;

    if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if ((pTeIscD = TeFindIscD (u4IscD, pteiface)) != NULL)
    {
        *pi4RetValFutRMTeLinkSwDescrSwitchingCap =
            (INT4) pTeIscD->u1SwitchingCap;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkSwDescrEncodingType
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                retValFutRMTeLinkSwDescrEncodingType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkSwDescrEncodingType (INT4 i4IfIndex,
                                      UINT4 u4FutRMTeLinkSwDescriptorId,
                                      INT4
                                      *pi4RetValFutRMTeLinkSwDescrEncodingType)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    UINT4               u4IscD = u4FutRMTeLinkSwDescriptorId;

    if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if ((pTeIscD = TeFindIscD (u4IscD, pteiface)) != NULL)
    {
        *pi4RetValFutRMTeLinkSwDescrEncodingType =
            (INT4) pTeIscD->u1EncodingType;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkSwDescrMinLSPBandwidth
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                retValFutRMTeLinkSwDescrMinLSPBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkSwDescrMinLSPBandwidth (INT4 i4IfIndex,
                                         UINT4 u4FutRMTeLinkSwDescriptorId,
                                         UINT4
                                         *pu4RetValFutRMTeLinkSwDescrMinLSPBandwidth)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    UINT4               u4IscD = u4FutRMTeLinkSwDescriptorId;

    if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if ((pTeIscD = TeFindIscD (u4IscD, pteiface)) != NULL)
    {
        *pu4RetValFutRMTeLinkSwDescrMinLSPBandwidth = (UINT4) pTeIscD->minLSPBw;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkSwDescrMTU
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                retValFutRMTeLinkSwDescrMTU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkSwDescrMTU (INT4 i4IfIndex, UINT4 u4FutRMTeLinkSwDescriptorId,
                             UINT4 *pu4RetValFutRMTeLinkSwDescrMTU)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    UINT4               u4IscD = u4FutRMTeLinkSwDescriptorId;

    if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if ((pTeIscD = TeFindIscD (u4IscD, pteiface)) != NULL)
    {
        *pu4RetValFutRMTeLinkSwDescrMTU = (UINT2) pTeIscD->u2MTU;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkSwDescrIndication
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                retValFutRMTeLinkSwDescrIndication
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkSwDescrIndication (INT4 i4IfIndex,
                                    UINT4 u4FutRMTeLinkSwDescriptorId,
                                    UINT4
                                    *pu4RetValFutRMTeLinkSwDescrIndication)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    UINT4               u4IscD = u4FutRMTeLinkSwDescriptorId;

    if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if ((pTeIscD = TeFindIscD (u4IscD, pteiface)) != NULL)
    {
        *pu4RetValFutRMTeLinkSwDescrIndication = (UINT4) pTeIscD->u1Indication;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkSwDescrRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId

                The Object 
                retValFutRMTeLinkSwDescrRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkSwDescrRowStatus (INT4 i4IfIndex,
                                   UINT4 u4FutRMTeLinkSwDescriptorId,
                                   INT4 *pi4RetValFutRMTeLinkSwDescrRowStatus)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    UINT4               u4IscD = u4FutRMTeLinkSwDescriptorId;

    if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if ((pTeIscD = TeFindIscD (u4IscD, pteiface)) != NULL)
    {
        *pi4RetValFutRMTeLinkSwDescrRowStatus = (INT4) pTeIscD->u4Status;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutRMTeLinkSwDescrMaxBwTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutRMTeLinkSwDescrMaxBwTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
                FutRMTeLinkSwDescrSwitchingCap
                FutRMTeLinkSwDescrMaxBwPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutRMTeLinkSwDescrMaxBwTable (INT4 i4IfIndex,
                                                      UINT4
                                                      u4FutRMTeLinkSwDescriptorId,
                                                      UINT4
                                                      u4FutRMTeLinkSwDescrMaxBwPriority)
{
    tRmTeLink          *pTeIface = NULL;
    UINT4               u4teIscD = u4FutRMTeLinkSwDescriptorId;

    if ((pTeIface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if (TeFindIscD (u4teIscD, pTeIface) == NULL)
    {
        return SNMP_FAILURE;
    }
    if (IS_VALID_TE_BW_PRIORITY (u4FutRMTeLinkSwDescrMaxBwPriority))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutRMTeLinkSwDescrMaxBwTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
                FutRMTeLinkSwDescrSwitchingCap
                FutRMTeLinkSwDescrMaxBwPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutRMTeLinkSwDescrMaxBwTable (INT4 *pi4IfIndex,
                                              UINT4
                                              *pu4FutRMTeLinkSwDescriptorId,
                                              UINT4
                                              *pu4FutRMTeLinkSwDescrMaxBwPriority)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pDescLstNode = NULL;
    UINT1               u1Priority;

    TMO_SLL_Scan (&(gteRM.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);
        TMO_SLL_Scan (&(pTeIface->ifDescrList), pDescLstNode, tTMO_SLL_NODE *)
        {
            pTeIscD = GET_TE_ISCD_PTR_FROM_SORT_LST (pDescLstNode);
            for (u1Priority = 0; u1Priority < OSPF_TE_MAX_PRIORITY_LVL;
                 u1Priority++)
            {
                if (pTeIscD->aDescrBw[u1Priority].u4Status != 0)
                {
                    *pi4IfIndex = (INT4) (pTeIface->u4IfIndex);
                    *pu4FutRMTeLinkSwDescriptorId = pTeIscD->u4DescrId;
                    *pu4FutRMTeLinkSwDescrMaxBwPriority = u1Priority;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutRMTeLinkSwDescrMaxBwTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FutRMTeLinkSwDescriptorId
                nextFutRMTeLinkSwDescriptorId
                FutRMTeLinkSwDescrSwitchingCap
                nextFutRMTeLinkSwDescrSwitchingCap
                FutRMTeLinkSwDescrMaxBwPriority
                nextFutRMTeLinkSwDescrMaxBwPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutRMTeLinkSwDescrMaxBwTable (INT4 i4IfIndex,
                                             INT4 *pi4NextIfIndex,
                                             UINT4 u4FutRMTeLinkSwDescriptorId,
                                             UINT4
                                             *pu4NextFutRMTeLinkSwDescriptorId,
                                             UINT4
                                             u4FutRMTeLinkSwDescrMaxBwPriority,
                                             UINT4
                                             *pu4NextFutRMTeLinkSwDescrMaxBwPriority)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;
    UINT1               u1Priority;

    TMO_SLL_Scan (&(gteRM.sortTeIfLst), pIfNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pIfNode);
        if (pTeIface->u4IfIndex == (UINT4) i4IfIndex)
        {
            TMO_SLL_Scan (&(pTeIface->ifDescrList), pLstNode, tTMO_SLL_NODE *)
            {
                pTeIscD = GET_TE_ISCD_PTR_FROM_SORT_LST (pLstNode);
                if (pTeIscD->u4DescrId == u4FutRMTeLinkSwDescriptorId)
                {
                    for (u1Priority =
                         (UINT1) (u4FutRMTeLinkSwDescrMaxBwPriority + 1);
                         u1Priority < OSPF_TE_MAX_PRIORITY_LVL; u1Priority++)
                    {
                        if (pTeIscD->aDescrBw[u1Priority].u4Status != 0)
                        {
                            *pi4NextIfIndex = (INT4) pTeIface->u4IfIndex;
                            *pu4NextFutRMTeLinkSwDescriptorId =
                                pTeIscD->u4DescrId;
                            *pu4NextFutRMTeLinkSwDescrMaxBwPriority =
                                u1Priority;
                            return SNMP_SUCCESS;
                        }
                    }
                }
                else if (pTeIscD->u4DescrId > u4FutRMTeLinkSwDescriptorId)
                {
                    for (u1Priority = 0;
                         u1Priority < OSPF_TE_MAX_PRIORITY_LVL; u1Priority++)
                    {
                        if (pTeIscD->aDescrBw[u1Priority].u4Status != 0)
                        {
                            *pi4NextIfIndex = (INT4) pTeIface->u4IfIndex;
                            *pu4NextFutRMTeLinkSwDescriptorId =
                                pTeIscD->u4DescrId;
                            *pu4NextFutRMTeLinkSwDescrMaxBwPriority =
                                u1Priority;
                            return SNMP_SUCCESS;
                        }
                    }
                }
            }
        }
        else if (pTeIface->u4IfIndex > (UINT4) i4IfIndex)
        {
            TMO_SLL_Scan (&(pTeIface->ifDescrList), pLstNode, tTMO_SLL_NODE *)
            {
                pTeIscD = GET_TE_ISCD_PTR_FROM_SORT_LST (pLstNode);
                for (u1Priority = 0; u1Priority < OSPF_TE_MAX_PRIORITY_LVL;
                     u1Priority++)
                {
                    if (pTeIscD->aDescrBw[u1Priority].u4Status != 0)
                    {
                        *pi4NextIfIndex = (INT4) pTeIface->u4IfIndex;
                        *pu4NextFutRMTeLinkSwDescriptorId = pTeIscD->u4DescrId;
                        *pu4NextFutRMTeLinkSwDescrMaxBwPriority = u1Priority;
                        return SNMP_SUCCESS;
                    }
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkSwDescrMaxLSPBandwidth
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
                FutRMTeLinkSwDescrSwitchingCap
                FutRMTeLinkSwDescrMaxBwPriority

                The Object 
                retValFutRMTeLinkSwDescrMaxLSPBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkSwDescrMaxLSPBandwidth (INT4 i4IfIndex,
                                         UINT4 u4FutRMTeLinkSwDescriptorId,
                                         UINT4
                                         u4FutRMTeLinkSwDescrMaxBwPriority,
                                         UINT4
                                         *pu4RetValFutRMTeLinkSwDescrMaxLSPBandwidth)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    UINT4               u4IscD = u4FutRMTeLinkSwDescriptorId;
    UINT4               u4MaxBwPrio = u4FutRMTeLinkSwDescrMaxBwPriority;

    if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if ((pTeIscD = TeFindIscD (u4IscD, pteiface)) != NULL)
    {
        *pu4RetValFutRMTeLinkSwDescrMaxLSPBandwidth =
            (UINT4) pTeIscD->aDescrBw[u4MaxBwPrio].maxLSPBw;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkSwDescrMaxBwRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSwDescriptorId
                FutRMTeLinkSwDescrSwitchingCap
                FutRMTeLinkSwDescrMaxBwPriority

                The Object 
                retValFutRMTeLinkSwDescrMaxBwRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkSwDescrMaxBwRowStatus (INT4 i4IfIndex,
                                        UINT4 u4FutRMTeLinkSwDescriptorId,
                                        UINT4 u4FutRMTeLinkSwDescrMaxBwPriority,
                                        INT4
                                        *pi4RetValFutRMTeLinkSwDescrMaxBwRowStatus)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeIfDescr       *pTeIscD = NULL;
    UINT4               u4IscD = u4FutRMTeLinkSwDescriptorId;
    UINT4               u4MaxBwPrio = u4FutRMTeLinkSwDescrMaxBwPriority;

    if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if ((pTeIscD = TeFindIscD (u4IscD, pteiface)) != NULL)
    {
        *pi4RetValFutRMTeLinkSwDescrMaxBwRowStatus =
            (INT4) pTeIscD->aDescrBw[u4MaxBwPrio].u4Status;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutRMTeLinkSrlgTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutRMTeLinkSrlgTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSrlg
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutRMTeLinkSrlgTable (INT4 i4IfIndex,
                                              UINT4 u4FutRMTeLinkSrlg)
{
    tRmTeLink          *pTeIface = NULL;
    UINT4               u4teSrlg = u4FutRMTeLinkSrlg;

    if ((pTeIface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if (TeFindSrlg (u4teSrlg, pTeIface) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutRMTeLinkSrlgTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSrlg
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutRMTeLinkSrlgTable (INT4 *pi4IfIndex,
                                      UINT4 *pu4FutRMTeLinkSrlg)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeLinkSrlg      *pTeSrlg = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pLstNode = TMO_SLL_First (&(gteRM.sortTeIfLst))) != NULL)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);

        if ((pLstNode = TMO_SLL_First (&(pTeIface->srlgList))) != NULL)
        {
            pTeSrlg = GET_TE_SRLG_PTR_FROM_SORT_LST (pLstNode);

            *pi4IfIndex = (INT4) (pTeIface->u4IfIndex);
            *pu4FutRMTeLinkSrlg = (UINT4) pTeSrlg->u4SrlgNo;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutRMTeLinkSrlgTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FutRMTeLinkSrlg
                nextFutRMTeLinkSrlg
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutRMTeLinkSrlgTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                     UINT4 u4FutRMTeLinkSrlg,
                                     UINT4 *pu4NextFutRMTeLinkSrlg)
{
    tRmTeLink          *pTeIface = NULL;
    tRmTeLinkSrlg      *pTeSrlg = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;

    TMO_SLL_Scan (&(gteRM.sortTeIfLst), pIfNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pIfNode);
        if (pTeIface->u4IfIndex == (UINT4) i4IfIndex)
        {
            TMO_SLL_Scan (&(pTeIface->srlgList), pLstNode, tTMO_SLL_NODE *)
            {
                pTeSrlg = GET_TE_SRLG_PTR_FROM_SORT_LST (pLstNode);

                if (pTeSrlg->u4SrlgNo > u4FutRMTeLinkSrlg)
                {
                    *pi4NextIfIndex = (INT4) pTeIface->u4IfIndex;
                    *pu4NextFutRMTeLinkSrlg = (UINT4) pTeSrlg->u4SrlgNo;
                    return SNMP_SUCCESS;
                }
            }
        }
        else if (pTeIface->u4IfIndex > (UINT4) i4IfIndex)
        {
            if ((pLstNode = TMO_SLL_First (&(pTeIface->srlgList))) != NULL)
            {
                pTeSrlg = GET_TE_SRLG_PTR_FROM_SORT_LST (pLstNode);
                *pi4NextIfIndex = (INT4) pTeIface->u4IfIndex;
                *pu4NextFutRMTeLinkSrlg = (UINT4) pTeSrlg->u4SrlgNo;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkSrlgRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkSrlg

                The Object 
                retValFutRMTeLinkSrlgRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkSrlgRowStatus (INT4 i4IfIndex, UINT4 u4FutRMTeLinkSrlg,
                                INT4 *pi4RetValFutRMTeLinkSrlgRowStatus)
{
    tRmTeLink          *pteiface = NULL;
    tRmTeLinkSrlg      *pTeSrlg = NULL;
    UINT4               u4Srlg = u4FutRMTeLinkSrlg;

    if ((pteiface = TeFindIf (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    else if ((pTeSrlg = TeFindSrlg (u4Srlg, pteiface)) != NULL)
    {
        *pi4RetValFutRMTeLinkSrlgRowStatus = (INT4) pTeSrlg->u4Status;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutRMTeLinkBandwidthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutRMTeLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkBandwidthPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutRMTeLinkBandwidthTable (INT4 i4IfIndex,
                                                   UINT4
                                                   u4FutRMTeLinkBandwidthPriority)
{
    if (TeFindIf (i4IfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    if (IS_VALID_TE_BW_PRIORITY (u4FutRMTeLinkBandwidthPriority))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutRMTeLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                FutRMTeLinkBandwidthPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutRMTeLinkBandwidthTable (INT4 *pi4IfIndex,
                                           UINT4
                                           *pu4FutRMTeLinkBandwidthPriority)
{
    tRmTeLink          *pTeIface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1Priority;

    TMO_SLL_Scan (&(gteRM.sortTeIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pLstNode);
        for (u1Priority = 0; u1Priority < OSPF_TE_MAX_PRIORITY_LVL;
             u1Priority++)
        {
            if (pTeIface->aUnResBw[u1Priority].u4Status != 0)
            {
                *pi4IfIndex = (INT4) (pTeIface->u4IfIndex);
                *pu4FutRMTeLinkBandwidthPriority = u1Priority;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutRMTeLinkBandwidthTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FutRMTeLinkBandwidthPriority
                nextFutRMTeLinkBandwidthPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutRMTeLinkBandwidthTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                          UINT4 u4FutRMTeLinkBandwidthPriority,
                                          UINT4
                                          *pu4NextFutRMTeLinkBandwidthPriority)
{
    tRmTeLink          *pTeIface = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;
    UINT1               u1Priority;

    TMO_SLL_Scan (&(gteRM.sortTeIfLst), pIfNode, tTMO_SLL_NODE *)
    {
        pTeIface = GET_TE_IF_PTR_FROM_SORT_LST (pIfNode);
        if (pTeIface->u4IfIndex == (UINT4) i4IfIndex)
        {
            for (u1Priority = (UINT1) (u4FutRMTeLinkBandwidthPriority + 1);
                 u1Priority < OSPF_TE_MAX_PRIORITY_LVL; u1Priority++)
            {
                if (pTeIface->aUnResBw[u1Priority].u4Status != 0)
                {
                    *pi4NextIfIndex = (INT4) pTeIface->u4IfIndex;
                    *pu4NextFutRMTeLinkBandwidthPriority = u1Priority;
                    return SNMP_SUCCESS;
                }
            }
        }
        else if (pTeIface->u4IfIndex > (UINT4) i4IfIndex)
        {
            for (u1Priority = 0; u1Priority < OSPF_TE_MAX_PRIORITY_LVL;
                 u1Priority++)
            {
                if (pTeIface->aUnResBw[u1Priority].u4Status != 0)
                {
                    *pi4NextIfIndex = (INT4) pTeIface->u4IfIndex;
                    *pu4NextFutRMTeLinkBandwidthPriority = u1Priority;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkUnreservedBandwidth
 Input       :  The Indices
                IfIndex
                FutRMTeLinkBandwidthPriority

                The Object 
                retValFutRMTeLinkUnreservedBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkUnreservedBandwidth (INT4 i4IfIndex,
                                      UINT4 u4FutRMTeLinkBandwidthPriority,
                                      UINT4
                                      *pu4RetValFutRMTeLinkUnreservedBandwidth)
{
    tRmTeLink          *pteiface = NULL;
    UINT4               u4UnResBwPrio = u4FutRMTeLinkBandwidthPriority;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pu4RetValFutRMTeLinkUnreservedBandwidth =
            (UINT4) pteiface->aUnResBw[u4UnResBwPrio].unResBw;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutRMTeLinkBandwidthRowStatus
 Input       :  The Indices
                IfIndex
                FutRMTeLinkBandwidthPriority

                The Object 
                retValFutRMTeLinkBandwidthRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutRMTeLinkBandwidthRowStatus (INT4 i4IfIndex,
                                     UINT4 u4FutRMTeLinkBandwidthPriority,
                                     INT4
                                     *pi4RetValFutRMTeLinkBandwidthRowStatus)
{
    tRmTeLink          *pteiface = NULL;
    UINT4               u4UnResBwPrio = u4FutRMTeLinkBandwidthPriority;

    if ((pteiface = TeFindIf (i4IfIndex)) != NULL)
    {
        *pi4RetValFutRMTeLinkBandwidthRowStatus =
            (INT4) pteiface->aUnResBw[u4UnResBwPrio].u4Status;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  fsoterfetch.c                  */
/*-----------------------------------------------------------------------*/
