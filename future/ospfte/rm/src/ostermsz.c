/* $Id: ostermsz.c,v 1.4 2013/11/29 11:04:15 siva Exp $*/
#define _OSTERMSZ_C
#include "osteinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
OstermSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < OSTERM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsOSTERMSizingParams[i4SizingId].
                                     u4StructSize,
                                     FsOSTERMSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(OSTERMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            OstermSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
OstermSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsOSTERMSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, OSTERMMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
OstermSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < OSTERM_MAX_SIZING_ID; i4SizingId++)
    {
        if (OSTERMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (OSTERMMemPoolIds[i4SizingId]);
            OSTERMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
