#ifndef _FSOTERWR_H
#define _FSOTERWR_H

VOID RegisterFSOTER(VOID);
INT4 FutRmTeLinkRegDeregistrationGet(tSnmpIndex *, tRetVal *);
INT4 FutRmTeLinkRegDeregistrationSet(tSnmpIndex *, tRetVal *);
INT4 FutRmTeLinkRegDeregistrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRmTeLinkRegDeregistrationDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFutRMTeLinkTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutRMTeLinkLocalIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkRemoteIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkRemoteRtrIdGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkMetricGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkProtectionTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkResourceClassGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkIncomingIfIdGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkOutgoingIfIdGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkMaxBwGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkMaxResBwGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkInfoTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkLocalIpAddrSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkRemoteIpAddrSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkRemoteRtrIdSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkMetricSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkProtectionTypeSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkResourceClassSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkIncomingIfIdSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkOutgoingIfIdSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkMaxBwSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkMaxResBwSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkAreaIdSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkInfoTypeSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkLocalIpAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkRemoteIpAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkRemoteRtrIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkProtectionTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkResourceClassTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkIncomingIfIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkOutgoingIfIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkMaxBwTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkMaxResBwTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkAreaIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkInfoTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);













INT4 GetNextIndexFutRMTeLinkSwDescriptorTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutRMTeLinkSwDescriptorIdGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrSwitchingCapGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrEncodingTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMinLSPBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMTUGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrIndicationGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrSwitchingCapSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrEncodingTypeSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMinLSPBandwidthSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMTUSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrIndicationSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrSwitchingCapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrEncodingTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMinLSPBandwidthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMTUTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrIndicationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescriptorTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFutRMTeLinkSwDescrMaxBwTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutRMTeLinkSwDescrMaxBwPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMaxLSPBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMaxBwRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMaxLSPBandwidthSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMaxBwRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMaxLSPBandwidthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMaxBwRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSwDescrMaxBwTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFutRMTeLinkSrlgTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutRMTeLinkSrlgGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSrlgRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSrlgRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSrlgRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkSrlgTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFutRMTeLinkBandwidthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutRMTeLinkBandwidthPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkUnreservedBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkBandwidthRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkUnreservedBandwidthSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkBandwidthRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkUnreservedBandwidthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkBandwidthRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutRMTeLinkBandwidthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


#endif /* _FSOTERWR_H */
