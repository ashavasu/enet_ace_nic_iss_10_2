/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: fsoterlw.h,v 1.3 2008/08/20 15:22:26 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutRmTeLinkRegDeregistration ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutRmTeLinkRegDeregistration ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutRmTeLinkRegDeregistration ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutRmTeLinkRegDeregistration ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutRMTeLinkTable. */
INT1
nmhValidateIndexInstanceFutRMTeLinkTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutRMTeLinkTable  */

INT1
nmhGetFirstIndexFutRMTeLinkTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutRMTeLinkTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutRMTeLinkLocalIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkRemoteIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkRemoteRtrId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkMetric ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkProtectionType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutRMTeLinkResourceClass ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkIncomingIfId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutRMTeLinkOutgoingIfId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutRMTeLinkMaxBw ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkMaxResBw ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkAreaId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkInfoType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutRMTeLinkRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutRMTeLinkLocalIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkRemoteIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkRemoteRtrId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkMetric ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkProtectionType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFutRMTeLinkResourceClass ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkIncomingIfId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFutRMTeLinkOutgoingIfId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFutRMTeLinkMaxBw ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkMaxResBw ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkAreaId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkInfoType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFutRMTeLinkRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutRMTeLinkLocalIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkRemoteIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkRemoteRtrId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkMetric ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkProtectionType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FutRMTeLinkResourceClass ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkIncomingIfId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FutRMTeLinkOutgoingIfId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FutRMTeLinkMaxBw ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkMaxResBw ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkAreaId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkInfoType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FutRMTeLinkRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutRMTeLinkTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutRMTeLinkSwDescriptorTable. */
INT1
nmhValidateIndexInstanceFutRMTeLinkSwDescriptorTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutRMTeLinkSwDescriptorTable  */

INT1
nmhGetFirstIndexFutRMTeLinkSwDescriptorTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutRMTeLinkSwDescriptorTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutRMTeLinkSwDescrSwitchingCap ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFutRMTeLinkSwDescrEncodingType ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFutRMTeLinkSwDescrMinLSPBandwidth ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkSwDescrMTU ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkSwDescrIndication ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkSwDescrRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutRMTeLinkSwDescrSwitchingCap ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFutRMTeLinkSwDescrEncodingType ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFutRMTeLinkSwDescrMinLSPBandwidth ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkSwDescrMTU ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkSwDescrIndication ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkSwDescrRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutRMTeLinkSwDescrSwitchingCap ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FutRMTeLinkSwDescrEncodingType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FutRMTeLinkSwDescrMinLSPBandwidth ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkSwDescrMTU ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkSwDescrIndication ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkSwDescrRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutRMTeLinkSwDescriptorTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutRMTeLinkSwDescrMaxBwTable. */
INT1
nmhValidateIndexInstanceFutRMTeLinkSwDescrMaxBwTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutRMTeLinkSwDescrMaxBwTable  */

INT1
nmhGetFirstIndexFutRMTeLinkSwDescrMaxBwTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutRMTeLinkSwDescrMaxBwTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutRMTeLinkSwDescrMaxLSPBandwidth ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkSwDescrMaxBwRowStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutRMTeLinkSwDescrMaxLSPBandwidth ARG_LIST((INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkSwDescrMaxBwRowStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutRMTeLinkSwDescrMaxLSPBandwidth ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkSwDescrMaxBwRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutRMTeLinkSwDescrMaxBwTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutRMTeLinkSrlgTable. */
INT1
nmhValidateIndexInstanceFutRMTeLinkSrlgTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutRMTeLinkSrlgTable  */

INT1
nmhGetFirstIndexFutRMTeLinkSrlgTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutRMTeLinkSrlgTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutRMTeLinkSrlgRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutRMTeLinkSrlgRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutRMTeLinkSrlgRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutRMTeLinkSrlgTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutRMTeLinkBandwidthTable. */
INT1
nmhValidateIndexInstanceFutRMTeLinkBandwidthTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutRMTeLinkBandwidthTable  */

INT1
nmhGetFirstIndexFutRMTeLinkBandwidthTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutRMTeLinkBandwidthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutRMTeLinkUnreservedBandwidth ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFutRMTeLinkBandwidthRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutRMTeLinkUnreservedBandwidth ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFutRMTeLinkBandwidthRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutRMTeLinkUnreservedBandwidth ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FutRMTeLinkBandwidthRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutRMTeLinkBandwidthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
