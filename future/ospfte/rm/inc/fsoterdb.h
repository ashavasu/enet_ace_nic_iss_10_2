/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsoterdb.h,v 1.3 2008/08/20 15:22:26 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSOTERDB_H
#define _FSOTERDB_H

UINT1 FutRMTeLinkTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FutRMTeLinkSwDescriptorTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FutRMTeLinkSwDescrMaxBwTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FutRMTeLinkSrlgTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FutRMTeLinkBandwidthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsoter [] ={1,3,6,1,4,1,2076,72,10};
tSNMP_OID_TYPE fsoterOID = {9, fsoter};


UINT4 FutRmTeLinkRegDeregistration [ ] ={1,3,6,1,4,1,2076,72,10,1,1,1};
UINT4 FutRMTeLinkLocalIpAddr [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,1};
UINT4 FutRMTeLinkRemoteIpAddr [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,2};
UINT4 FutRMTeLinkRemoteRtrId [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,3};
UINT4 FutRMTeLinkMetric [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,4};
UINT4 FutRMTeLinkProtectionType [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,5};
UINT4 FutRMTeLinkResourceClass [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,6};
UINT4 FutRMTeLinkIncomingIfId [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,7};
UINT4 FutRMTeLinkOutgoingIfId [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,8};
UINT4 FutRMTeLinkMaxBw [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,9};
UINT4 FutRMTeLinkMaxResBw [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,10};
UINT4 FutRMTeLinkAreaId [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,11};
UINT4 FutRMTeLinkInfoType [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,12};
UINT4 FutRMTeLinkRowStatus [ ] ={1,3,6,1,4,1,2076,72,10,1,2,1,13};
UINT4 FutRMTeLinkSwDescriptorId [ ] ={1,3,6,1,4,1,2076,72,10,1,3,1,1};
UINT4 FutRMTeLinkSwDescrSwitchingCap [ ] ={1,3,6,1,4,1,2076,72,10,1,3,1,2};
UINT4 FutRMTeLinkSwDescrEncodingType [ ] ={1,3,6,1,4,1,2076,72,10,1,3,1,3};
UINT4 FutRMTeLinkSwDescrMinLSPBandwidth [ ] ={1,3,6,1,4,1,2076,72,10,1,3,1,4};
UINT4 FutRMTeLinkSwDescrMTU [ ] ={1,3,6,1,4,1,2076,72,10,1,3,1,5};
UINT4 FutRMTeLinkSwDescrIndication [ ] ={1,3,6,1,4,1,2076,72,10,1,3,1,6};
UINT4 FutRMTeLinkSwDescrRowStatus [ ] ={1,3,6,1,4,1,2076,72,10,1,3,1,7};
UINT4 FutRMTeLinkSwDescrMaxBwPriority [ ] ={1,3,6,1,4,1,2076,72,10,1,4,1,1};
UINT4 FutRMTeLinkSwDescrMaxLSPBandwidth [ ] ={1,3,6,1,4,1,2076,72,10,1,4,1,2};
UINT4 FutRMTeLinkSwDescrMaxBwRowStatus [ ] ={1,3,6,1,4,1,2076,72,10,1,4,1,3};
UINT4 FutRMTeLinkSrlg [ ] ={1,3,6,1,4,1,2076,72,10,1,5,1,1};
UINT4 FutRMTeLinkSrlgRowStatus [ ] ={1,3,6,1,4,1,2076,72,10,1,5,1,2};
UINT4 FutRMTeLinkBandwidthPriority [ ] ={1,3,6,1,4,1,2076,72,10,1,6,1,1};
UINT4 FutRMTeLinkUnreservedBandwidth [ ] ={1,3,6,1,4,1,2076,72,10,1,6,1,2};
UINT4 FutRMTeLinkBandwidthRowStatus [ ] ={1,3,6,1,4,1,2076,72,10,1,6,1,3};


tMbDbEntry fsoterMibEntry[]= {

{{12,FutRmTeLinkRegDeregistration}, NULL, FutRmTeLinkRegDeregistrationGet, FutRmTeLinkRegDeregistrationSet, FutRmTeLinkRegDeregistrationTest, FutRmTeLinkRegDeregistrationDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FutRMTeLinkLocalIpAddr}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkLocalIpAddrGet, FutRMTeLinkLocalIpAddrSet, FutRMTeLinkLocalIpAddrTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkRemoteIpAddr}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkRemoteIpAddrGet, FutRMTeLinkRemoteIpAddrSet, FutRMTeLinkRemoteIpAddrTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkRemoteRtrId}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkRemoteRtrIdGet, FutRMTeLinkRemoteRtrIdSet, FutRMTeLinkRemoteRtrIdTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkMetric}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkMetricGet, FutRMTeLinkMetricSet, FutRMTeLinkMetricTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkProtectionType}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkProtectionTypeGet, FutRMTeLinkProtectionTypeSet, FutRMTeLinkProtectionTypeTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkResourceClass}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkResourceClassGet, FutRMTeLinkResourceClassSet, FutRMTeLinkResourceClassTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkIncomingIfId}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkIncomingIfIdGet, FutRMTeLinkIncomingIfIdSet, FutRMTeLinkIncomingIfIdTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkOutgoingIfId}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkOutgoingIfIdGet, FutRMTeLinkOutgoingIfIdSet, FutRMTeLinkOutgoingIfIdTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkMaxBw}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkMaxBwGet, FutRMTeLinkMaxBwSet, FutRMTeLinkMaxBwTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkMaxResBw}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkMaxResBwGet, FutRMTeLinkMaxResBwSet, FutRMTeLinkMaxResBwTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkAreaId}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkAreaIdGet, FutRMTeLinkAreaIdSet, FutRMTeLinkAreaIdTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkInfoType}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkInfoTypeGet, FutRMTeLinkInfoTypeSet, FutRMTeLinkInfoTypeTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 0, NULL},

{{13,FutRMTeLinkRowStatus}, GetNextIndexFutRMTeLinkTable, FutRMTeLinkRowStatusGet, FutRMTeLinkRowStatusSet, FutRMTeLinkRowStatusTest, FutRMTeLinkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutRMTeLinkTableINDEX, 1, 0, 1, NULL},

{{13,FutRMTeLinkSwDescriptorId}, GetNextIndexFutRMTeLinkSwDescriptorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FutRMTeLinkSwDescriptorTableINDEX, 2, 0, 0, NULL},

{{13,FutRMTeLinkSwDescrSwitchingCap}, GetNextIndexFutRMTeLinkSwDescriptorTable, FutRMTeLinkSwDescrSwitchingCapGet, FutRMTeLinkSwDescrSwitchingCapSet, FutRMTeLinkSwDescrSwitchingCapTest, FutRMTeLinkSwDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutRMTeLinkSwDescriptorTableINDEX, 2, 0, 0, NULL},

{{13,FutRMTeLinkSwDescrEncodingType}, GetNextIndexFutRMTeLinkSwDescriptorTable, FutRMTeLinkSwDescrEncodingTypeGet, FutRMTeLinkSwDescrEncodingTypeSet, FutRMTeLinkSwDescrEncodingTypeTest, FutRMTeLinkSwDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutRMTeLinkSwDescriptorTableINDEX, 2, 0, 0, NULL},

{{13,FutRMTeLinkSwDescrMinLSPBandwidth}, GetNextIndexFutRMTeLinkSwDescriptorTable, FutRMTeLinkSwDescrMinLSPBandwidthGet, FutRMTeLinkSwDescrMinLSPBandwidthSet, FutRMTeLinkSwDescrMinLSPBandwidthTest, FutRMTeLinkSwDescriptorTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FutRMTeLinkSwDescriptorTableINDEX, 2, 0, 0, NULL},

{{13,FutRMTeLinkSwDescrMTU}, GetNextIndexFutRMTeLinkSwDescriptorTable, FutRMTeLinkSwDescrMTUGet, FutRMTeLinkSwDescrMTUSet, FutRMTeLinkSwDescrMTUTest, FutRMTeLinkSwDescriptorTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FutRMTeLinkSwDescriptorTableINDEX, 2, 0, 0, NULL},

{{13,FutRMTeLinkSwDescrIndication}, GetNextIndexFutRMTeLinkSwDescriptorTable, FutRMTeLinkSwDescrIndicationGet, FutRMTeLinkSwDescrIndicationSet, FutRMTeLinkSwDescrIndicationTest, FutRMTeLinkSwDescriptorTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FutRMTeLinkSwDescriptorTableINDEX, 2, 0, 0, NULL},

{{13,FutRMTeLinkSwDescrRowStatus}, GetNextIndexFutRMTeLinkSwDescriptorTable, FutRMTeLinkSwDescrRowStatusGet, FutRMTeLinkSwDescrRowStatusSet, FutRMTeLinkSwDescrRowStatusTest, FutRMTeLinkSwDescriptorTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutRMTeLinkSwDescriptorTableINDEX, 2, 0, 1, NULL},

{{13,FutRMTeLinkSwDescrMaxBwPriority}, GetNextIndexFutRMTeLinkSwDescrMaxBwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FutRMTeLinkSwDescrMaxBwTableINDEX, 3, 0, 0, NULL},

{{13,FutRMTeLinkSwDescrMaxLSPBandwidth}, GetNextIndexFutRMTeLinkSwDescrMaxBwTable, FutRMTeLinkSwDescrMaxLSPBandwidthGet, FutRMTeLinkSwDescrMaxLSPBandwidthSet, FutRMTeLinkSwDescrMaxLSPBandwidthTest, FutRMTeLinkSwDescrMaxBwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FutRMTeLinkSwDescrMaxBwTableINDEX, 3, 0, 0, NULL},

{{13,FutRMTeLinkSwDescrMaxBwRowStatus}, GetNextIndexFutRMTeLinkSwDescrMaxBwTable, FutRMTeLinkSwDescrMaxBwRowStatusGet, FutRMTeLinkSwDescrMaxBwRowStatusSet, FutRMTeLinkSwDescrMaxBwRowStatusTest, FutRMTeLinkSwDescrMaxBwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutRMTeLinkSwDescrMaxBwTableINDEX, 3, 0, 1, NULL},

{{13,FutRMTeLinkSrlg}, GetNextIndexFutRMTeLinkSrlgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FutRMTeLinkSrlgTableINDEX, 2, 0, 0, NULL},

{{13,FutRMTeLinkSrlgRowStatus}, GetNextIndexFutRMTeLinkSrlgTable, FutRMTeLinkSrlgRowStatusGet, FutRMTeLinkSrlgRowStatusSet, FutRMTeLinkSrlgRowStatusTest, FutRMTeLinkSrlgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutRMTeLinkSrlgTableINDEX, 2, 0, 1, NULL},

{{13,FutRMTeLinkBandwidthPriority}, GetNextIndexFutRMTeLinkBandwidthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FutRMTeLinkBandwidthTableINDEX, 2, 0, 0, NULL},

{{13,FutRMTeLinkUnreservedBandwidth}, GetNextIndexFutRMTeLinkBandwidthTable, FutRMTeLinkUnreservedBandwidthGet, FutRMTeLinkUnreservedBandwidthSet, FutRMTeLinkUnreservedBandwidthTest, FutRMTeLinkBandwidthTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FutRMTeLinkBandwidthTableINDEX, 2, 0, 0, NULL},

{{13,FutRMTeLinkBandwidthRowStatus}, GetNextIndexFutRMTeLinkBandwidthTable, FutRMTeLinkBandwidthRowStatusGet, FutRMTeLinkBandwidthRowStatusSet, FutRMTeLinkBandwidthRowStatusTest, FutRMTeLinkBandwidthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutRMTeLinkBandwidthTableINDEX, 2, 0, 1, NULL},
};
tMibData fsoterEntry = { 29, fsoterMibEntry };
#endif /* _FSOTERDB_H */

