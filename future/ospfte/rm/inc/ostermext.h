/*****************************************************************
 * File containing global parameters and data structures.
 *****************************************************************/
#ifndef _OS_TE_RM_EXT_H_
#define _OS_TE_RM_EXT_H_

extern tbufQID      gbufqid; 
extern tTeRM        gteRM; 
#endif  /* _OS_TE_RM_EXT_H_ */
