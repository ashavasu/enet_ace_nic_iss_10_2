/*****************************************************************
 * File containing global parameters and data structures.
 *****************************************************************/
#ifndef _OS_TE_RM_GLOB_H_
#define _OS_TE_RM_GLOB_H_

/* Declaration of Global variables */
tbufQID gbufqid; 
tTeRM   gteRM;  
#endif  /* _OSPF_TE_GLOB_H_ */
