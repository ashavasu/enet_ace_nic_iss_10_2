/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostermsz.h,v 1.1 2012/02/01 12:40:28 siva Exp $
 *
 * Description: This file contains definitions of global variables
 *              used.
 *********************************************************************/
enum {
    MAX_OSPF_TE_RM_IF_DESCRS_SIZING_ID,
    MAX_OSPF_TE_RM_LINKS_SIZING_ID,
    MAX_OSPF_TE_SRLG_NO_SIZING_ID,
    OSTERM_MAX_SIZING_ID
};


#ifdef  _OSTERMSZ_C
tMemPoolId OSTERMMemPoolIds[ OSTERM_MAX_SIZING_ID];
INT4  OstermSizingMemCreateMemPools(VOID);
VOID  OstermSizingMemDeleteMemPools(VOID);
INT4  OstermSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _OSTERMSZ_C  */
extern tMemPoolId OSTERMMemPoolIds[ ];
extern INT4  OstermSizingMemCreateMemPools(VOID);
extern VOID  OstermSizingMemDeleteMemPools(VOID);
extern INT4  OstermSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _OSTERMSZ_C  */


#ifdef  _OSTERMSZ_C
tFsModSizingParams FsOSTERMSizingParams [] = {
{ "tRmTeIfDescr", "MAX_OSPF_TE_RM_IF_DESCRS", sizeof(tRmTeIfDescr),MAX_OSPF_TE_RM_IF_DESCRS, MAX_OSPF_TE_RM_IF_DESCRS,0 },
{ "tRmTeLink", "MAX_OSPF_TE_RM_LINKS", sizeof(tRmTeLink),MAX_OSPF_TE_RM_LINKS, MAX_OSPF_TE_RM_LINKS,0 },
{ "tRmTeLinkSrlg", "MAX_OSPF_TE_SRLG_NO", sizeof(tRmTeLinkSrlg),MAX_OSPF_TE_SRLG_NO, MAX_OSPF_TE_SRLG_NO,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _OSTERMSZ_C  */
extern tFsModSizingParams FsOSTERMSizingParams [];
#endif /*  _OSTERMSZ_C  */


