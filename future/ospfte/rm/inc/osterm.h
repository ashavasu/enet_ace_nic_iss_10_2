/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: osterm.h,v 1.5 2014/03/03 12:22:28 siva Exp $
 *
 * Description: This file contains definitions of global variables
 *              used.
 *********************************************************************/

/*****************************************************************
 * File containing global parameters and data structures.
 *****************************************************************/
#ifndef _OS_TE_RM_H_
#define _OS_TE_RM_H_

/* Include files */
#include "lr.h"
#include "ospfte.h"

/* MIB related */
#define INVALID    0

#define IS_VALID_ROUTER_ID(x) TRUE
#define LTD_B_CAST_ADDR 0xffffffff   /* Limited Broadcast address */
#define LOOP_BACK_ADDR_MASK 0xff000000 /* Loop back Address range 127.x.x.x*/
#define LOOP_BACK_ADDRESSES 0x7f000000 /* Loop back Address range 127.x.x.x*/
#define IS_CLASS_A_ADDR(u4Addr) \
                              ((u4Addr & 0x80000000) == 0)

#define IS_CLASS_B_ADDR(u4Addr) \
                              ((u4Addr & 0xc0000000) == 0x80000000)

#define IS_CLASS_C_ADDR(u4Addr) \
                              ((u4Addr & 0xe0000000) == 0xc0000000)

#define IS_MCAST_D_ADDR(u4Addr) \
                              (u4Addr >= 0xe0000000 && u4Addr <= 0xe00000ff)

typedef struct _tTeRM{
        tTMO_HASH_TABLE *pTeIfHashTable;  /* Hash Table for storing TE Link attributes */
 tTMO_SLL        sortTeIfLst;      /* TE Link interfaces in sorted order */
 INT1 RMregister;
 UINT1 aResv[3];
}tTeRM;

#define RM_REGISTER     1        /* RM Module to register itself with OSPFTE */
#define RM_DEREGISTER   2        /* RM Module to deregister itself with OSPFTE */

#define OSPFTE_RM_THOUSAND_BYTES    1000
typedef struct _bufQID {
 UINT4 u4LinkId;
 UINT4 u4DescId;
 UINT4 u4SrlgId;
}tbufQID;

typedef struct _RmTeDescrMaxLSPBw {
    UINT4              u4Status;
    tBandWidth         maxLSPBw;
} tRmTeDescrMaxLSPBw;


typedef struct _RmTeDescrUnResBw {
    UINT4              u4Status;
    tBandWidth         unResBw;
} tRmTeDescrUnResBw;

typedef struct _RmSrlgNode {
    tTMO_SLL_NODE     NextLinkSrlgNode;
    UINT4             u4Status;
    UINT4             u4SrlgNo;
} tRmTeLinkSrlg;

typedef  struct _RmTeIfDescr {
    tTMO_SLL_NODE      NextIfDescrNode;  /* Points to next Interface node   */
    UINT4              u4Status;
    UINT4              u4DescrId;        /* Descriptor identifier */
    tBandWidth         minLSPBw;
    UINT1              u1SwitchingCap;   /* Descriptor Switching capability */
    UINT1              u1EncodingType;   /* Descriptor Encoding Type */
    UINT2              u2MTU;
    tRmTeDescrMaxLSPBw aDescrBw [OSPF_TE_MAX_PRIORITY_LVL];
    UINT1              u1Indication;
    UINT1              au1Rsvd[3];      /* For padding */
} tRmTeIfDescr;

typedef struct _RmTeLink {
    tTMO_HASH_NODE     NextLinkNode;     /* Points to next Interface node   */
    tTMO_SLL_NODE      NextSortLinkNode; /* Points to next sorted interface
                                          * node */
    UINT4              u4IfIndex;        /* Interface Index */
    UINT4              u4LocalIpAddr;         /* Local IP address of the interface */
    UINT4              u4RemoteIpAddr;   /* Remote IP address of the interface */
    UINT4              u4RemoteRtrId;    /* Remote Router's Router Id -- This 
                                              information is to be configured for
                                              FA link */
    UINT4              u4TeMetric;       /* TE Metric value */
    UINT4              u4RsrcClassColor; /* Administrative Group */
    UINT4              u4LocalIdentifier;/* Local Identifier value */
    UINT4              u4RemoteIdentifier;  /* Remote Identifier value */
    tBandWidth         maxBw;          /* newly added variable: Maxmimum Bandwidth */
    tBandWidth        maxResBw;       /* newly added variable: Max Res Bandwidth */
    UINT4              u4AreaId;       /* newly added variable: AreaId */
    UINT4              u1InfoType;    /* newly added variable: InfoType */
    tRmTeDescrUnResBw  aUnResBw[OSPF_TE_MAX_PRIORITY_LVL]; /* Unreserved BW */
    tTMO_SLL           ifDescrList; /* SLL of interface descriptors */
    tTMO_SLL           srlgList;   /* SLL of SRLG list */
    UINT1              u1ProtectionType;  /* Protection Type */
    UINT1              u1LinkStatus;     /* Indicates IF Link is ACTIVE or
                                          * NOT ACTIVE */
    UINT1              au1Rsvd[2];      /* For padding */
} tRmTeLink;

typedef struct _RmLinkSrlgInfo {
    UINT4           au4SrlgInfo[MAX_OSPF_TE_SRLG_NO];
}tRmLinkSrlgInfo;

/* Macros for error and informational */
#define SNMP_SUCCESS 1

/*Macros for defining respective QIDs for Link,Descr,Srlg */
#define LINK_QID gbufqid.u4LinkId
#define DESCR_QID gbufqid.u4DescId
#define SRLG_QID gbufqid.u4SrlgId

/*Macros for sizes of the buffer */
#define MAX_RM_LINKS 40
#define MAX_IF_DESCRS 10

UINT4 utilHashGetValue (UINT4 , UINT1 *, UINT1 );
/* 
 * no. of hash buckets = 
 * total no. of objects stored / no. of objects per hash bucket;
 * no. of objects per hash bucket is chosen to be 16
 * it is subject to change
 */
#define  BUCKET_SIZE  16 

#define  IF_HASH_TABLE_SIZE       ((MAX_RM_LINKS / BUCKET_SIZE) + 1)   

/* Macros for test routines */
#define IF_HASH_FN(u4IfIndex) utilHashGetValue(IF_HASH_TABLE_SIZE, \
                                    (UINT1 *)(&(u4IfIndex)), sizeof(UINT4))
#define IS_VALID_TE_PROTECTION_TYPE(x)     ((x == 0x01) || \
                              (x == 0x02) || \
         (x == 0x04) || \
         (x == 0x08) || \
         (x == 0x10) || \
         (x == 0x20) || \
         (x == 0x40) || \
         (x == 0x80))
#define IS_VALID_TE_INCOMING_ID(x)     (x >= 0)
#define IS_VALID_TE_OUTGOING_ID(x)     (x >= 0)
#define IS_VALID_TE_MAXRESBW(x)           ((x >= 0) && (x <= 0xffffffff))
#define IS_VALID_TE_AREAID(x)           (TRUE)
#define IS_VALID_TE_INFOTYPE(x)        ((x == OSPF_TE_RM_AREAID_INFO) \
                                     || (x == OSPF_TE_RM_DATALINK_INFO) \
                                     || (x == OSPF_TE_RM_DATACONTROLLINK_INFO))

#define IS_VALID_TE_SWITCH_CAP(x)     (((x >= OSPF_TE_PSC_1) &&  \
                                (x <= OSPF_TE_PSC_4)) || (x == OSPF_TE_L2SC)  \
                          || (x == OSPF_TE_TDM) || (x == OSPF_TE_LSC) \
           || (x == OSPF_TE_FSC))

#define IS_VALID_TE_ENCODING_TYPE(x)     (((x >= 1) && (x <= 11)) && \
                                          ((x != 4) || (x != 6) || (x != 10))) 

#define IS_VALID_TE_MINLSPBW(x)     (x <= 0xffffffff)
#define IS_VALID_TE_MTU(x)     (x <= 0xffff)

#define IS_VALID_TE_INDICATION(x)     ((x == 0) || (x == 1))

/* #define IS_VALID_TE_INDICATION(x)     (x <= 0xffffffff) */
#define IS_VALID_TE_BW_PRIORITY(x)     (x <= 7)

/**********************************************
 * Getting the appropriate pointer from SLL.
 **********************************************/
#define OSPFTE_OFFSET(x,y)  ((FS_ULONG)(&(((x *)0)->y))) 

#define GET_TE_IF_PTR_FROM_SORT_LST(x)  ((tRmTeLink *)(((FS_ULONG)x) \
                    - OSPFTE_OFFSET(tRmTeLink, NextSortLinkNode)))
#define GET_TE_ISCD_PTR_FROM_SORT_LST(x) ((tRmTeIfDescr *)(((FS_ULONG)x) \
                    - OSPFTE_OFFSET(tRmTeIfDescr, NextIfDescrNode)))
#define GET_TE_SRLG_PTR_FROM_SORT_LST(x) ((tRmTeLinkSrlg *)(((FS_ULONG)x) \
                    - OSPFTE_OFFSET(tRmTeLinkSrlg, NextLinkSrlgNode)))

/* Macros for memory allocation using cru buffers */
#define TELINK_ALLOC(pu1Block) \
        (pu1Block = (tRmTeLink *) MemAllocMemBlk ((tMemPoolId)LINK_QID))
#define TEIFDESCR_ALLOC(pu1Block) \
        (pu1Block = (tRmTeIfDescr *) MemAllocMemBlk ((tMemPoolId)DESCR_QID))
#define TELINKSRLG_ALLOC(pu1Block) \
        (pu1Block = (tRmTeLinkSrlg *) MemAllocMemBlk ((tMemPoolId)SRLG_QID))
 
/* Macros for memory deallocation using cru buffers */
#define  TELINK_FREE(pu1Block) MemReleaseMemBlock((tMemPoolId)LINK_QID,(UINT1 *)pu1Block)
#define  TEIFDESCR_FREE(pu1Block) MemReleaseMemBlock((tMemPoolId)DESCR_QID,(UINT1 *)pu1Block)
#define  TESRLG_FREE(pu1Block) MemReleaseMemBlock((tMemPoolId)SRLG_QID,(UINT1 *)pu1Block)

#define OSPF_TE_RM_MEMCPY     MEMCPY

/* Function prototypes used in this module */

void TeAddTosortIfLst(tRmTeLink *pTeiface);
void TeAddTosortIfDescrLst(tRmTeIfDescr *pTeIscD, tRmTeLink *pTeIface);
void TeAddTosortIfSrlgLst(tRmTeLinkSrlg *pTeSrlg, tRmTeLink *pTeIface);
tRmTeLink * TeIfCreate (INT4 i4IfIndex);
tRmTeLink * TeFindIf (INT4 i4IfIndex);
INT4 TeIfDelete (tRmTeLink * pTeNode);
tRmTeIfDescr * TeFindIscD (UINT4 u4IscDesr, tRmTeLink *pteiface);
tRmTeIfDescr * TeIscDesCreate(UINT4 u4IscDesr, tRmTeLink *pteiface);
UINT4 TeIscDesDelete(tRmTeIfDescr *pIfDesr, tRmTeLink *pteiface);
tRmTeLinkSrlg * TeFindSrlg (UINT4 u4LinkSrlg, tRmTeLink *pteiface);
tRmTeLinkSrlg * TeLinkSrlgCreate (UINT4 u4LinkSrlg, tRmTeLink *pteiface);
void TeLinkSrlgDelete(tRmTeLinkSrlg *pTeSrlg, tRmTeLink *pteiface);

UINT1 ValidateFutRMTeLinkSetIndex (UINT4 u4IfIndex);
UINT1 OspfTeValidIpAddress (UINT4 u4IpAddr);
UINT1 ValidateFutRMTeLinkSwDescrSetIndex (UINT4 u4IfIndex, UINT4 u4ifdescrid);
UINT1 ValidateFutRMTeLinkSrlgSetIndex (UINT4 u4IfIndex, UINT4 u4SrlgNo);
PUBLIC tRmOsTeLinkMsg *
OspfTeRmFillMsgInfo (tRmTeLink * pTeIface);

#define OSPF_TE_RM_DYNM_SLL_SCAN(pList, pNode, pTempNode, TypeCast) \
        for(((pNode) = (TypeCast)(TMO_SLL_First((pList)))), \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
            ((TypeCast)    \
            (TMO_SLL_Next((pList),  \
            ((tTMO_SLL_NODE *)(pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
            ((TypeCast)  \
            (TMO_SLL_Next((pList),  \
            ((tTMO_SLL_NODE *)(pNode))))))))

/*  Getting the base pointers */
#define OSPF_TE_RM_GET_BASE_PTR(type, memberName, pMember) \
       (type *) ((FS_ULONG)(pMember) - ((FS_ULONG) &((type *)0)->memberName))

#endif  /* _OS_TE_RM_H_ */
