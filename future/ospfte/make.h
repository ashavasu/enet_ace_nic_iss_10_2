################################################################
# Copyright (C) 2007 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.3 2011/07/26 10:48:09 siva Exp $*
#
# Description: Specifies the options and modules to be
#              including for building the FutureSoftOSPF-TE product
################################################################

GLOBAL_OPNS = ${GENERAL_COMPILATION_SWITCHES} \
              ${SYSTEM_COMPILATION_SWITCHES} 

############################################################################
#                         Directories                                      #
############################################################################

#Fill the base directory
OSPF_BASE_DIR       = ${BASE_DIR}/ospf
OSPF_BUDDY_BASE_DIR = ${OSPF_BASE_DIR}/buddy
OSPFTE_BASE_DIR = ${BASE_DIR}/ospfte
OSPFTE_SRCD     = ${OSPFTE_BASE_DIR}/src
OSPFTE_INCD     = ${OSPFTE_BASE_DIR}/inc
OSPFTE_OBJD     = ${OSPFTE_BASE_DIR}/obj

OSPF_BUDDYSRCD = ${OSPF_BUDDY_BASE_DIR}/src
OSPF_BUDDYINCD = ${OSPF_BUDDY_BASE_DIR}/inc
OSPF_BUDDYLIBD = ${OSPF_BUDDY_BASE_DIR}/lib
OSPF_BUDDYOBJD = ${OSPF_BUDDY_BASE_DIR}/obj

APP_SRCD     = ${OSPFTE_BASE_DIR}/app/src
APP_INCD     = ${OSPFTE_BASE_DIR}/app/inc
APP_OBJD     = ${OSPFTE_BASE_DIR}/app/obj

RM_SRCD     = ${OSPFTE_BASE_DIR}/rm/src
RM_INCD     = ${OSPFTE_BASE_DIR}/rm/inc
RM_OBJD     = ${OSPFTE_BASE_DIR}/rm/obj

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

OSPFTE_GLOBAL_INCLUDES  = -I${OSPFTE_INCD} -I${OSPF_BUDDYINCD} -I${APP_INCD} -I${RM_INCD}

GLOBAL_INCLUDES  = ${OSPFTE_GLOBAL_INCLUDES} \
                   ${COMMON_INCLUDE_DIRS}

DEPENDENCIES_FOR_OSPFTE = ${OSPFTE_BASE_DIR}/make.h \
                          ${OSPFTE_BASE_DIR}/Makefile \
                          ${BASE_DIR}/LR/make.h
     
ifeq (${CLI}, YES)
OSPFTE_CLI_INCLUDE_FILES =    \
    $(CLI_INCL_DIR)/ostecli.h

endif

#############################################################################

##########################################################################
# Each line below indicates whether that module is to be included in the #
# OSPFTE build. A YES indicates inclusion.                               #
##########################################################################
OSPFTEAPPSIM                     = YES
OSPFTERMSIM                      = YES
