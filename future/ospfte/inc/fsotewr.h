#ifndef _FSOTEWR_H
#define _FSOTEWR_H

VOID RegisterFSOTE(VOID);
INT4 FutOspfTeAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeCspfRunCntGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfTeTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfTeAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FutOspfTeTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 GetNextIndexFutOspfTeLsdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfTeLsdbAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeLsdbTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeLsdbLsidGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeLsdbRouterIdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeLsdbChecksumGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeLsdbAdvertisementGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFutOspfTeType9LsdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfTeType9LsdbIfIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeType9LsdbIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeType9LsdbLsidGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeType9LsdbRouterIdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeType9LsdbChecksumGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeType9LsdbAdvertisementGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFutOspfTeAreaTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfTeAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeAreaLsaCountGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeType10AreaCksumSumGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeType2AreaCksumSumGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFutOspfTeIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfTeIfIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeAddressLessIfGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfAreaIdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfMetricGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfMaxBwGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfMaxReservBwGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfRsrcClassColorGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfOperStatGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfLinkIdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfRemoteIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfProtectionTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfSrlgGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFutOspfTeIfDescriptorTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfTeIfDescrIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfDescrAddressLessIfGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfDescrIdGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfDescrSwithingCapGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfDescrEncodingTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfDescrMinLSPBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfDescrMTUGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfDescrIndicationGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFutOspfTeIfSwDescrMaxBwTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfTeIfSwDescrMaxBwPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfSwDescrMaxLSPBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFutOspfTeIfBandwidthTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfTeIfBandwidthPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfTeIfUnreservedBandwidthGet(tSnmpIndex *, tRetVal *);
#endif /* _FSOTEWR_H */
