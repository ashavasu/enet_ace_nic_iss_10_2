/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostedbg.h,v 1.2 2011/10/28 10:58:54 siva Exp $
 *
 * Description: This file contains procedures and definitions
 *              used for debugging.
 *********************************************************************/

#ifndef _OSPF_TE_DBG_H_
#define _OSPF_TE_DBG_H

#define OSPF_TE_TRC_FLAG     gOsTeContext.u4TeTrcValue
#define OSPF_TE_NAME         "OSPF_TE"

#ifdef TRACE_WANTED
#define OSPF_TE_TRC(Value, Fmt)           UtlTrcLog(OSPF_TE_TRC_FLAG, \
                                                Value,        \
                                                OSPF_TE_NAME,        \
                                                Fmt)

#define OSPF_TE_TRC1(Value, Fmt, Arg)     UtlTrcLog(OSPF_TE_TRC_FLAG, \
                                                Value,        \
                                                OSPF_TE_NAME,        \
                                                Fmt,          \
                                                Arg)
#define OSPF_TE_TRC2(Value, Fmt, Arg1, Arg2)                      \
                                      UtlTrcLog(OSPF_TE_TRC_FLAG, \
                                               Value,        \
                                               OSPF_TE_NAME,        \
                                               Fmt,          \
                                               Arg1, Arg2)

#define OSPF_TE_TRC3(Value, Fmt, Arg1, Arg2, Arg3)                \
                                      UtlTrcLog(OSPF_TE_TRC_FLAG, \
                                               Value,        \
                                               OSPF_TE_NAME,        \
                                               Fmt,          \
                                               Arg1, Arg2, Arg3)

#define OSPF_TE_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)          \
                                      UtlTrcLog(OSPF_TE_TRC_FLAG, \
                                               Value,        \
                                               OSPF_TE_NAME,        \
                                               Fmt,          \
                                               Arg1, Arg2,   \
                                               Arg3, Arg4)

#define OSPF_TE_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
                                      UtlTrcLog(OSPF_TE_TRC_FLAG, \
                                               Value,        \
                                               OSPF_TE_NAME,        \
                                               Fmt,          \
                                               Arg1, Arg2, Arg3, \
                                               Arg4, Arg5)

#define OSPF_TE_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
                                      UtlTrcLog(OSPF_TE_TRC_FLAG, \
                                               Value,        \
                                               OSPF_TE_NAME,        \
                                               Fmt,          \
                                               Arg1, Arg2, Arg3, \
                                               Arg4, Arg5, Arg6)
#else
#define  OSPF_TE_TRC(Value, Fmt)
#define  OSPF_TE_TRC1(Value, Fmt, Arg)
#define  OSPF_TE_TRC2(Value, Fmt, Arg1, Arg2)
#define  OSPF_TE_TRC3(Value, Fmt, Arg1, Arg2, Arg3)
#define  OSPF_TE_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)
#define  OSPF_TE_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define  OSPF_TE_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#endif
          
#endif   /* _OSPF_TE_DBG_H_ */


