/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: ostetdfs.h,v 1.5 2012/05/28 13:41:34 siva Exp $
*
* Description: This file contains definition of the structures used.
*********************************************************************/

#ifndef _OSPF_TE_TDFS_H_
#define _OSPF_TE_TDFS_H_

#define  MAX_IP_ADDR_LEN 4

/* --------------------- */
/* Hash Table structures */
/* --------------------- */
typedef tTMO_HASH_TABLE    tOsTeDbHashTbl;     /* Holds the TE LSAs          */ 
typedef tTMO_HASH_TABLE    tOsTeCspfTree;      /* Holds the Candidate nodes  */
typedef tTMO_HASH_TABLE    tOsTeIfTbl;         /* Holds the Link information */ 
                                               /* for the given interface    */ 

/* --------------------------- */
/* Memory Pool data structures */
/* --------------------------- */
/* Holds all the memory pool for OSPF-TE operation */
typedef struct _OsTeMemPool {
    tMemPoolId   TeIfPoolId;           /* TE Interface Memory Pool           */
    tMemPoolId   OspfInfIfPoolId;      /* Interface Memory Pool              */
    tMemPoolId   IfDescPoolId;         /* Interface descriptors pool Id      */
    tMemPoolId   AreaPoolId;           /* Area Memory Pool                   */
    tMemPoolId   TeDbNodePoolId;       /* TE Node Memory Pool                */
    tMemPoolId   TeLsaInfoPoolId;      /* TE LSA Info Memory Pool            */
    tMemPoolId   LinkTlvPoolId;        /* Link TLV Memory Pool               */
    tMemPoolId   CandtePoolId;         /* Candidate Node Memory Pool         */
    tMemPoolId   QMsgPoolId;           /* OSPF-TE Message Memory Pool        */
    tMemPoolId   PathPoolId;           /* Path Pool                          */
    tMemPoolId   CspfReqPoolId;        /* Cspf Memory Pool                   */
    tMemPoolId   CspfConsPoolId;       /* Cspf Constraint Pool               */
    tMemPoolId   LinkTlvSizePoolId;    /* Link TLV size Pool Id              */
    tMemPoolId   MaxNetNbrsPoolId;     /* Max net nbrs pool Id               */
    UINT4        TeLsaPoolId;          /* TE LSA Memory Pool                 */
    tMemPoolId   RmLinkInfoPoolId;     /* Rm Link Info pool Id               */
    tMemPoolId   RmLinkSrlgPoolId;     /* Rm Link SRLG pool ID               */
    tMemPoolId   RmLinkMsgPoolId;      /* Rm Link Msg pool ID                */
} tOsTeMemPoolId;

/* ---------------------- */
/* OSPF-TE Area structure */
/* ---------------------- */
/* Holds all the TE Area specific information */
typedef struct _OsTeArea {
    tTMO_SLL_NODE         NextAreaNode;     /* Points to next Area node   */
    tOsTeDbHashTbl       *pTeDbHashTbl;     /* Holds the TE LSAs          */
    struct _OsTeLsaNode  *pRtrLsa;       /* Holds Self originated 
                                                * Router LSA OSPF            */
    tTMO_SLL            networkLsaLst;         /* Holds the list of network 
                                                * links associated with this 
                                                * area                       */
    tTMO_SLL            type10OpqLsaLst;       /* This list is used to store
      * Type 10 LSAs */ 
    UINT4               u4AreaId;              /* Identifies the Area        */
    UINT4               u4NetLsaChksumSum;    /* Includes Network and Type 10 
                                                * LSAs                       */
    UINT4               u4Type10LsaChksumSum;  /* Holds Type 10 LSA checksum */
} tOsTeArea;





/* --------------------------- */
/* OSPF-TE Interface structure */
/* --------------------------- */
typedef struct _OsTeInterface {
    tTMO_HASH_NODE     NextLinkNode;
    tTMO_SLL_NODE      NextSortLinkNode;
    tOsTeArea         *pTeArea; 
    tTMO_SLL           ifDescrList;
    tTMO_SLL           type9TELsaLst;
    tOsTeSrlg          osTeSrlg;
    tBandWidth         maxBw;
    tBandWidth         maxResBw;
    tBandWidth         aUnResBw [OSPF_TE_MAX_PRIORITY_LVL];
    tIPADDR            linkId;
    tIPADDR            localIpAddr;
    tIPADDR            remoteIpAddr;
    UINT4              u4TeMetric;
    UINT4              u4RsrcClassColor;
    UINT4              u4IfIndex;
    UINT4              u4AddrlessIf;
    UINT4              u4LocalIdentifier;
    UINT4              u4RemoteIdentifier;
    UINT4              u4Type9LsaChksumSum;
    UINT4              u4AreaId;
    UINT1              u1ProtectionType;
    UINT1              u1LinkStatus;
    UINT1              u1LinkType;
    UINT1              u1ChannelType; 
    UINT1              u1MetricType;
    UINT1              u1ModuleId;
    UINT1              au1Rsvd[2]; 
} tOsTeInterface;


/* ------------------------------------------- */
/* OSPF-TE Interface descriptor node structure */
/* ------------------------------------------- */
typedef struct _OsTeIfDescNode {
    tTMO_SLL_NODE      NextIfDescrNode;
    tOsTeIfDesc        ifDesc;         
} tOsTeIfDescNode;


/* ------------------------------------ */
/* OSPF information interface structure */
/* ------------------------------------ */
/* OSPF information interface data structure. */
typedef struct _OsTeOsIntInfo {
   tTMO_SLL_NODE      NextOsIntInfoNode;
   tOsTeArea         *pTeArea;        
   struct _OsTeLsaNode *pType9Lsa;
   tIPADDR            linkId;
   tIPADDR            ifIpAddr;
   tIPADDR            nbrIpAddr;
   UINT4              u4AddrlessIf;
   UINT4              u4Metric;
   UINT1              u1InfoType;
                      /* 1 -- Information learned from Router LSA */
                      /* 2 -- Information learned from Passive interface 
                              information */ 
   UINT1             u1LinkType;
                      /* Point-to-Point or Multiaccess */
   UINT1             au1Rsvd[2];
} tOsTeOsIntInfo;


/* ------------------------------- */
/* OSPF-TE Link TLV node structure */   
/* ------------------------------- */
/* Holds the Link TLV information associated with an interface */
typedef struct _OsTeLinkTlvNode {
    tTMO_SLL_NODE      NextLinkTlvNode;
    tOsTeSrlg          osTeSrlg;
    tTMO_SLL           ifDescrLst;
    tBandWidth         maxBw; 
    tBandWidth         maxResBw;
    tBandWidth         aUnResrvBw [OSPF_TE_MAX_PRIORITY_LVL];
    UINT4              u4LinkId;
    UINT4              u4TeMetric;
    UINT4              u4LocalIpAddr;
    UINT4              u4RemoteIpAddr;
    UINT4              u4RsrcClassColor;
    UINT4              u4LocalIdentifier;
    UINT4              u4RemoteIdentifier;
    UINT1              u1ProtectionType;
    UINT1              u1LinkType;
    UINT1              au1Unused[2];
} tOsTeLinkTlvNode;


/* ------------------------------------ */
/* OSPF-TE Link TLV Hash node structure */
/* ------------------------------------ */
/* Holds the Link TLV Hash node information */
typedef struct _OsTeDbNode {
    tTMO_HASH_NODE    NextTeDbNode;       /* Points to next TEDB hash node   */
    tTMO_SLL          TeLsaLst;           /* List holding all TE LSAs        */
    tRouterId         rtrId;              /* Holds the Router ID             */
} tOsTeDbNode;


/* -------------------------- */
/* OSPF-TE LSA Node structure */
/* -------------------------- */
/* Holds the LSA node information */
typedef struct _OsTeLsaNode {
   tTMO_SLL_NODE     NextDbNodeLsa;   /* Points to next sorted LSA node     */
   tTMO_SLL_NODE     NextSortLsaNode;
   tOsTeArea        *pTeArea;         /* Holds area associated with this LSA */
   tOsTeInterface   *pOsTeInt;        /* Pointer to TE interface             */
   tOsTeLinkTlvNode *pLinkTlv;
   tRouterId         advRtrId;        /* RouterId of router that originated  *
                           * LSA                                 */
   tLINKSTATEID      linkStateId;     /* Link State Id of the LSA node       */
   UINT1            *pLsa;            /* Holds the LSA contents              */
   UINT2             u2LsaChksum;     /* Checksum of complete contents of LSA*/
   UINT2             u2LsaLen;        /* Length in bytes of LSA including    *
                                       * 20-byte header                      */
   UINT1             u1LsaType;       /* Type of the LSA                     */
   UINT1             au1Rsvd[3];      /* Added for padding                   */
} tOsTeLsaNode;

/* ---------------------------- */
/* OSPF-TE LSA Header structure */
/* ---------------------------- */
/* Holds the LSA Header information */
typedef struct _OsTeLsaHdr {
   UINT2           u2Age;           /* LSA Age                     */
   UINT1           u1Options;       /* LSA options                 */
   UINT1           u1LsaType;       /* Type of the LSA             */
   tLINKSTATEID    linkStateId;     /* Link state id of the router */
   tRouterId       advRtrId;        /* Advertising Router Id.      */
   UINT4           u4SeqNum;        /* Sequence Number             */
   UINT2           u2ChkSum;        /* LSA Check sum               */
   UINT2           u2LsaLen;        /* LSA Length                  */
} tOsTeLsaHdr;


typedef struct _OsTePath {
    tTMO_SLL_NODE      NextPathNode;
    tOsTeLinkTlvNode  *pOsTeSrcLinkTlv;
    tOsTeLinkTlvNode  *pOsTeDestLinkTlv;
    tOsTeNextHop       aNextHops[OSPF_TE_MAX_NEXT_HOPS];
    tOsTeSrlg          aSrlg[OSPF_TE_MAX_NEXT_HOPS];
    UINT4              u4TeMetric;
    UINT4              u4AreaId;
    UINT2              u2PathType;
    UINT2              u2HopCount;
    UINT1              u1EncodingType;
    UINT1              au1Unused [3];
} tOsTePath;


typedef struct t_OsTeCandteNode {
    tTMO_HASH_NODE     NextCandteNode;
    union{
          tOsTeDbNode    *pOsTeDbNode;
          tOsTeLsaNode   *pOsTeLsaNode;
    } unNodeInfo;
    tTMO_SLL           pathLst;
    UINT4              u4VertexId;
    UINT4              u4ParentId;
    UINT4              u4TeMetric;
    UINT1              u1VertexType;
    UINT1              au1Unused[3];
} tOsTeCandteNode;

#define pTeDbNode  unNodeInfo.pOsTeDbNode
#define pTeLsaNode unNodeInfo.pOsTeLsaNode


typedef struct _OsTeLinkNode {
    tOsTeLsaNode      *pOsTeLsaNode;
    UINT4              u4LinkId;
    UINT4              u4TeMetric;
    UINT1              u1LinkType;
    UINT1              u1Offset;
    UINT1              au1Unused[2];
} tOsTeLinkNode;


/* ------------------------- */
/* OSPF-TE context structure */
/* ------------------------- */
/* Holds all the information needed for the OSPF-TE operation */
typedef struct _OsTeContext {
    tOsTeIfTbl           *pTeIfHashTbl;     /* Holds the TE Interface Hash   *
          * Table pointer.                */
    tTMO_SLL              sortTeIfLst; 
    tTMO_SLL              ospfInfoIfLst;    /* Holds the OSPF information    */
    tTMO_SLL              areasLst;         /* TE Areas List */
    tOsTeCspfTree        *pTeCandteLst;     /* Holds Candidate Table pointer */
    tOsTePath            *aPath[OSPF_TE_TWO];
    tOsTeMemPoolId        teMemPool;        /* Memory Pool needed by TE      */
    tRouterId             rtrId;            /* Router Id                     */
    UINT4                 u4TeTrcValue;     /* Trace value                   */
    UINT4                 u4CspfRunCnt;     /* Holds the count of CSPF       *
                                             * calculation done.             */
    UINT4                 u4SrcIpAddr;
    UINT4                 u4DestIpAddr;
    UINT1                 u1TeAdminStat;    /* OSPF-TE Adminstrative Status  */
    UINT1                 u1RmRegister;     /* Indicates whether RM is       
                                               Registered or not             */ 
    UINT1                 u1TlmRegister;    /* Indicates whether TLM is 
                                               Registered or not              */
    UINT1                 au1Unused [1];    /* Added for Padding             */
} tOsTeContext;


/* structure used to store links obtained from router or network lsa
 * during calculation of intra area routes
 */
typedef struct _LinkNode {
    tIPADDR             linkId;
                            /* Identifies the object that the router connects
                             * to another router/network.  Its value depends
                             * upon u1LinkType */
    tIPADDR             linkData;
                            /* Provides the IP address of the next hop router */
    UINT2               u2LinkCost;
                            /* Indicates the cost of using this outbound
                             * router link for traffic of the specified TOS */
    UINT1               u1LinkType;
                            /* Indicates the type of link by which the router
                             * is connected to another router/network */
    UINT1               au1Rsvd;
                            /* Included for 4-byte Alignment */
} tLinkNode;

typedef struct _RmLinkInfo  {
    tOsTeIfDesc     atOsTeIfDesc[OSPF_TE_MAX_IF_DESC];
} tRmLinkInfo;

typedef struct _OspfTeLsa {
    UINT1         au1Lsa[OSPF_TE_MAX_LSA_SIZE];
}tOspfTeLsa;

#endif  /* _OSPF_TE_TDFS_H_ */
