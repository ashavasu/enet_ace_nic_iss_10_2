/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: osteprot.h,v 1.11 2013/04/17 12:15:12 siva Exp $
*
* Description: This file contains function prototypes definitions.
*********************************************************************/

#ifndef _OSPF_TE_PROT_H_
#define _OSPF_TE_PROT_H_
#define VOID void
/************************** ostemain.c ***************************************/
/* Prototypes of MAIN module */
PUBLIC INT4 OspfTeTaskInit      (VOID);
PUBLIC INT4 OspfTeInit          (VOID);
PUBLIC VOID OspfTeMsgHdlr       (tOspfTeQMsg *pOspfTeQMsg);
PUBLIC INT4 OspfTeMemInit       (VOID);
PUBLIC INT4 OspfTeCreateMemPool (VOID);


/************************** ostearea.c ***************************************/
/* Prototypes of AREA module */
PUBLIC tOsTeArea * OspfTeAreaCreate (UINT4 u4AreaId);
PUBLIC INT4 OspfTeAreaSetDefaultValues (tOsTeArea *pTeArea);
PUBLIC VOID OspfTeAreaAddToAreaLst (tOsTeArea *pTeArea);
PUBLIC VOID OspfTeAreaDelete (tOsTeArea *pTeArea);
PUBLIC VOID OspfTeAreaDeleteAll (VOID);


/************************** osteosif.c ***************************************/
/* Prototypes of OSPF interface module */
PUBLIC INT4 OspfTeSndMsgToOspf (UINT1 u1Type, tOsTeOspfMsg * pOsTeOspfMsg);
PUBLIC INT4 OspfTeEnqueueMsgFromOspf (tOsToOpqApp *pOspfMsg);
PUBLIC VOID OspfTeProcessOspfMsg (tOsToOpqApp * pOspfMsg);
PUBLIC VOID OspfTeProcessLsaInfo (tOsToOpqApp * pOspfMsg, 
                    tOsTeArea * pTeArea);
PUBLIC VOID OspfTeProcessNbrInfo (tOsToOpqApp * pOspfMsg, 
                    tOsTeArea * pTeArea);
PUBLIC VOID OspfTeProcessRtrLsa (tOsToOpqApp * pOspfMsg, 
                        tOsTeArea * pTeArea);
PUBLIC INT4 OspfTeProcessType9Lsa (tOsToOpqApp *pOspfMsg); 

PUBLIC INT4 OspfTeProcessNetType10LSA (tOsToOpqApp * pOspfMsg, 
                         tOsTeArea * pTeArea);
PUBLIC INT4 OspfTeProcessNewRtrLsa (tOsLsaInfo *pOsLsaInfo, 
                      tOsTeArea * pTeArea);
PUBLIC INT4 OspfTeProcessNewInstRtrLsa (tOsLsaInfo *pOsLsaInfo, 
                          tOsTeArea * pTeArea);
PUBLIC INT4 OspfTeProcessFlushedRtrLsa (tOsTeArea *pTeArea); 
PUBLIC VOID OspfTeProcessRtrLsaNewLink (tLinkNode * pLinkNode, 
                          tOsTeArea * pTeArea);
PUBLIC VOID OspfTeProcessRtrLsaModifyLink (tLinkNode * pLinkNode); 
PUBLIC VOID OspfTeProcessRtrLsaFlushLink (tLinkNode * pLinkNode,
                            tOsTeArea * pTeArea); 
PUBLIC VOID
OspfTeGetIfInfoFromRtrLsaLink (tIPADDR linkData, tIPADDR * pIfAddr,
                                UINT4 *pu4AddrlessIf);
PUBLIC
VOID OspfTeInactivateTeLink (tOsTeArea *pOsTeArea, UINT1);
PUBLIC
VOID OspfTeActivateTeLinks (tOsTeArea *pOsTeArea);
PUBLIC VOID
OspfTeCreateOspfInfoInterface (tOsTeInterface *pOsTeInterface,
                               tOsTeLsaNode *pType9Lsa);
PUBLIC tOsTeLsaNode * 
OspfTeFlushOpaqueLsa (tOsTeInterface *pOsTeInterface);
/************************** osteif.c ***************************************/
/* Prototypes of TE interface module */
PUBLIC tOsTeInterface * OspfTeIfCreate (tIPADDR teIfIpAddr, 
                         UINT4 u4AddrlessIf);
PUBLIC VOID OspfTeIfLinkInstall (tOsTeInterface * pOsTeIf, 
         tRmOsTeLinkMsg * pRmOsTeLinkMsg);
PUBLIC VOID OsTeIfSetDefaultValues (tOsTeInterface * pOsTeIf);
PUBLIC VOID OspfTeIfAddToSortIfLst (tOsTeInterface * pTeInterface);
PUBLIC VOID OspfTeIfAddToSortOspfIntIfLst (tOsTeOsIntInfo *pOsTeOsIntInfo);
PUBLIC tOsTeInterface * OspfTeFindTeInterface (tIPADDR ifIpAddr, 
                    UINT4 u4AddrlessIf);
PUBLIC tOsTeInterface * OspfTeFindTeInterfaceWithIndex (UINT4 u4IfIndex);
PUBLIC tOsTeOsIntInfo *
OspfTeFindOspfInfoInt (tIPADDR ifIpAddr, UINT4 u4AddrlessIf);
PUBLIC UINT4 OspfTeIfHashFunc (tIPADDR *pIfIpAddr, UINT4 u4AddrlessIf);
PUBLIC VOID OspfTeIfDeleteTeLink (tOsTeInterface * pOsTeLink);
PUBLIC VOID OspfTeIfDeleteAllOspfInfoInt (VOID);
PUBLIC VOID OspfTeIfDeleteAllOsTeInterface (VOID);
PUBLIC UINT4 OspfTeGetInterfaceNameFromIndex (UINT4 u4Index, UINT1 *pu1Alias);

/************************** ostesnmp.c ***************************************/
/* Prototypes of TE configuration module */
PUBLIC INT4 OspfTeAdminUpHdlr (VOID);
PUBLIC INT4 OspfTeAdminDnHdlr (VOID);


/************************** ostesnmp.c ***************************************/
/* Prototypes of RM interaction module */
PUBLIC VOID OspfTeProcessRmgrMsg (tRmOsTeLinkMsg * pRmgrMsg);
PUBLIC VOID OspfTeProcessRmLinkInfo (tRmOsTeLinkMsg * pRmOsTeLinkMsg);
PUBLIC VOID OspfTeProcessCspfCompMsg (tCspfCompInfo *pCspfCompInfo);

/************************** osteutil.c ***************************************/
/* Prototypes of utility functions */
PUBLIC tOsTeArea * OspfTeUtilFindArea (UINT4 u4AreaId);
PUBLIC tOsTeArea * OspfTeMapAreaForFA (UINT4 u4FAIfIndex);
PUBLIC VOID OspfTeUtilExtractLsHdr (UINT1 *pLsa, tOsTeLsaHdr * pLsHeader);
PUBLIC UINT4 OspfTeUtilHashGetValue (UINT4 u4HashSize, UINT1 *pKey, 
                       UINT1 u1KeyLen);
PUBLIC UINT1 OspfTeUtilLsaIdComp (UINT1 u1LsaType1,
                                  tLINKSTATEID linkStateId1,
                                  tRouterId advRtrId1,
                                  UINT1 u1LsaType2,
                                  tLINKSTATEID linkStateId2, 
                                  tRouterId advRtrId2);
PUBLIC UINT1
OspfTeUtilIpAddrComp (tIPADDR addr1, tIPADDR addr2);
PUBLIC UINT1 OspfTeUtilIpAddrIndComp (tIPADDR ifIpAddr1,
                                      UINT4 u4IfIndex1, tIPADDR ifIpAddr2, 
                                      UINT4 u4IfIndex2);
PUBLIC UINT1 * 
OspfTeUtilGetLinksFromLsa (UINT1 *pLsa, UINT2 u2LsaLen,
                           UINT1 *pCurrPtr, tLinkNode * pLinkNode);
PUBLIC tOsTeLinkTlvNode * OspfTeUtilExtractLinkTlv (tOsTeLsaNode * pTeLsaInfo);
PUBLIC VOID
OspfTeUtilExtractLinkDesc (tOsTeLinkTlvNode * pLinkTlvNode, UINT1 *pCurrPtr);
PUBLIC tOsTeIfDesc *
OspfTeFindTeIfDescr (UINT4 u4IpAddr, INT4 i4AddrlessIf, UINT4 u4DescrId);
PUBLIC tOsTeLsaNode *
OspfTeUtilFindNetworkLsa (tLINKSTATEID linkStateId, tOsTeArea * pTeArea);
PUBLIC UINT4 OspfTeUtilDwordFromPdu PROTO ((UINT1 *pu1PduAddr));
PUBLIC VOID OspfTeUtilDwordToPdu PROTO ((UINT1 *pu1PduAddr, UINT4 u4Value));

/************************** ostesub.c ***************************************/
/* Prototypes of TLV construction function */
PUBLIC UINT2 OspfTeConstructLinkTlv (tOsTeInterface *pTeIfNode,
                                       UINT1 *pTLVnode);


/************************** ostelsu.c ***************************************/
/* Prototypes of LSA functions  */
PUBLIC tOsTeLsaNode *
OspfTeLsuSearchDatabase (UINT1 u1LsaType,
                         tLINKSTATEID * pLinkStateId,
                         tRouterId * pAdvRtrId, UINT1 *pPtr);
PUBLIC VOID
OspfTeLsuDeleteLsaFromDatabase (tOsTeLsaNode * pLsaInfo);
PUBLIC VOID
OspfTeLsuClearLsaInfo (tOsTeLsaNode * pLsaInfo);
PUBLIC tOsTeLsaNode *
OspfTeLsuAddToLsdb (UINT1 *pPtr,
                    UINT1 u1LsaType,
                    tLINKSTATEID * pLinkStateId, tRouterId * pAdvRtrId);
PUBLIC VOID  OspfTeLsuAddToSortLsaLst (tOsTeLsaNode * pLsaNode);
PUBLIC VOID  OspfTeLsuInstallLsa (UINT1 *pu1Lsa, tOsTeLsaNode * pLsaInfo);
PUBLIC UINT4 OspfTeLsuLsaHashFunc (tRouterId * pAdvRtrId);
PUBLIC VOID  OspfTeLsuDeleteAllLsas (tOsTeArea *pTeArea);
PUBLIC VOID  OspfTeLsuDeleteNetLsas (tOsTeArea *pTeArea);
PUBLIC VOID  OspfTeLsuDeleteType9Lsas (tOsTeArea * pTeArea);
PUBLIC VOID  OspfTeLsuDeleteType10Lsas (tOsTeArea *pTeArea, BOOL1 bIsTlmDeReg);


/************************** ostegen.c ***************************************/
/* Prototypes of TLV generation function */
PUBLIC INT4
OspfTeGenerateRtrAddrTlv (tOsTeArea *pTeArea, UINT1 u1RtrTlvStatus);
PUBLIC INT4
OspfTeGenerateLinkTlv (tOsTeInterface *pTeIfNode, UINT1  u1TlvStatus);
PUBLIC INT4
OspfTeGenerateType9Lsa (tOsTeInterface *pOsTeInt, UINT1 u1Status);

/************************** ostegchk.c ***************************************/

PUBLIC INT4 OspfTeCspfChkProtectionType (tOsTeCspfReq * pCspfReq,
                                         tOsTeLinkTlvNode * pLinkTlvNode);
PUBLIC INT4 OspfTeCspfChkBwConstrain (tOsTeCspfReq * pCspfReq,
                        tOsTeLinkTlvNode * pLinkTlvNode);

PUBLIC INT4 OspfTeCspfChkAdminConstrain (tOsTeCspfReq * pCspfReq,
                                         tOsTeLinkTlvNode * pLinkTlvNode);

PUBLIC INT4 OspfTeCspfChkConstrain (tOsTeCspfReq * pCspfReq,
          tOsTeLinkTlvNode * pLinkTlvNode, UINT4 u4RouterId);

PUBLIC INT4 OspfTeCspfChkIfDescrConstrain (tOsTeCspfReq * pCspfReq,
     tOsTeLinkTlvNode * pLinkTlvNode, UINT4 u4RouterId);

PUBLIC INT4 OspfTeCspfChkSwithingCap (tOsTeLinkTlvNode * pOsTeBackLinkTlv,
                                     tOsTeLinkTlvNode * pOsTeNextLinkTlv,
                                     tOsTeCspfReq *pCspfReq,
         tRouterId  rtrId);

PUBLIC INT4 OspfTeCspfChkEncodingType (tOsTePath * pVertexPath, 
                         tOsTeCspfReq *pCspfReq);


/************************** ostecspf.c ***************************************/
PUBLIC INT4 OspfTeCspfDetectLoop (tOsTeCandteNode * pParent, UINT4 u4VertexId, UINT1 u1VertexType);
PUBLIC INT4 OspfTeCspfUpdateCandteLst (tOsTeCandteNode * pNewVertex,
                               tOsTeLinkNode * pLinkNode,
                               tOsTeArea * pArea, tOsTeCspfReq * pCspfReq);
PUBLIC INT4 OspfTeCspfSetNextHops (tOsTeCandteNode *pParent ,
                       tOsTeCandteNode *pVertex , tOsTeCspfReq * pCspfReq, 
                       tOsTeLinkNode * pLinkNode, tOsTeArea * pArea);
PUBLIC INT4 OspfTeCspfUpdateNextHop
                         (tOsTePath  * pVertexPath, tOsTeCandteNode * pVertex,
                  tOsTeCandteNode  * pParent, tOsTeLinkNode * pLinkNode,
                         tOsTeArea * pArea, tOsTeCspfReq * pCspfReq);
PUBLIC tOsTeCandteNode * OspfTeCspfGetNearestCandteNode (VOID);
PUBLIC INT4 OspfTeCspfIntraAreaRoute (tOsTeArea * pArea,
                          tOsTeCspfReq * pCspfReq, tOsTeAppPath * aPath);
PUBLIC VOID OspfTeCspfFreeCandteNode (tOsTeCandteNode  *pCspfRoot);
PUBLIC VOID OspfTeCspfClearCandteLst (VOID);
PUBLIC VOID OspfTeCspfCopyPaths (tOsTeAppPath * aPath, tOsTeArea *pArea);
PUBLIC INT4 OspfTeCspfStrictIntraAreaRoute (tOsTeArea * pArea,
                                tOsTeCspfReq * pCspfReq, tOsTeAppPath * aPath);
PUBLIC tOsTeCandteNode * OspfTeCspfGetStrictNearestCandteNode (VOID);
PUBLIC INT4 OspfTeCspfLooseIntraAreaRoute (tOsTeArea * pArea,
                                tOsTeCspfReq * pCspfReq, tOsTeAppPath * aPath);
PUBLIC INT4 OspfTeCspfStrictLooseIntraAreaRoute (tOsTeArea * pArea,
                                tOsTeCspfReq * pCspfReq, tOsTeAppPath * aPath);
PUBLIC INT4 OspfTeCspfIncludeAnyIntraAreaRoute (tOsTeArea * pArea,
                                tOsTeCspfReq * pCspfReq, tOsTeAppPath * aPath);

PUBLIC INT4 OspfTeCspfAppendPaths (tOsTeCspfReq * pCspfReq, 
                                   tOsTeAppPath * pOsTeSubPath,
                                   UINT4 u4OrigSrcIpAddr, tOsTeAppPath * aPath);

PUBLIC VOID OspfTeCspfClearLooseCandteLst (UINT4   u4RouterId);

/************************** ostecons.c ***************************************/

PUBLIC INT4 OspfTeCspfChkStrictExpRoute (tOsTePath * pPath,
                                         tOsTeExpRoute * pExpRoute);

PUBLIC INT4 OspfTeCspfChkLooseExpRoute (tOsTePath * pPath,
                                        tOsTeExpRoute * pExpRoute);

PUBLIC INT4 OspfTeCspfChkFailedLink (tOsTePath * pPath,
                                     tOsTeExpRoute * pExpRoute);

PUBLIC INT4 OspfTeCspfDestIpAddrChk (tOsTeCandteNode * pCspfVertex,
                       tOsTeCspfReq * pCspfReq);

PUBLIC INT4 OspfTeCspfChkNodeDisjoint (tOsTePath * pPrimaryPath,
                         tOsTePath * pBackupPath);

PUBLIC INT4 OspfTeCspfChkLinkDisjoint (tOsTePath * pPrimaryPath,
                         tOsTePath * pBackupPath);

PUBLIC INT4 OspfTeCspfChkSrlgDisjoint (tOsTePath * pPrimaryPath,
                         tOsTePath * pBackupPath);



PUBLIC INT4 
OspfTeCspfUpdateGlobalPathLst   (tOsTePath * pVertexPath,
                  tOsTeCspfReq * pCspfReq);

PUBLIC INT4
OspfTeCspfChkValidPrimaryPath (tOsTePath   *pVertexPath,
                              tOsTeCspfReq   *pCspfReq);


PUBLIC INT4
OspfTeCspfChkValidBackupPath (tOsTePath *pVertexPath, 
               tOsTeCspfReq *pCspfReq);

PUBLIC INT4
OspfTeCspfCalculatePath (tOsTeArea  *pArea, tOsTeCspfReq *pCspfReq, 
           tOsTeAppPath  *aPath);

PUBLIC INT4
OspfTeCspfChkSRLGConstraint (tOsTeCspfReq * pCspfReq, 
                             tOsTeLinkTlvNode * pLinkTlvNode);

PUBLIC INT4
OspfTeCspfCheckDisjoint (tOsTeCspfReq * pCspfReq,
                         tOsTeLinkTlvNode * pLinkTlvNode);

/************************** ostermgr.c ***************************************/
PUBLIC VOID OspfTeProcessRmDeReg (VOID);

/************************** osteget.c ***************************************/

PUBLIC UINT4 OspfTeCspfHashFunc (UINT1 u1LsaType, UINT4 u4AdvRtrId);

PUBLIC UINT1 *  OspfTeCspfSearchDatabase (UINT1 u1LsaType, 
   UINT4 u4RouterId, tOsTeArea * pArea);

PUBLIC INT4 OspfTeCspfGetLinksFromLsa (tOsTeCandteNode * pNewVertex,
                                        tOsTeLinkNode * pLinkNode);

PUBLIC tOsTeCandteNode *  OspfTeCspfGetCandteNode 
                           (UINT4 u4VertexId, UINT1 u1VertexType);

PUBLIC tOsTeCandteNode * OspfTeCspfSearchCandteLst
                          (UINT4 u4VertexId, UINT1 u1VertType);

PUBLIC INT4 OspfTeCspfSearchLsaLinks 
                    (UINT1 *pPtr,tOsTeCandteNode * pCandteNode, 
       UINT1 u1VertType, tOsTeLinkNode  *pLinkNode, 
       tOsTeArea * pArea, tOsTeCspfReq * pCspfReq); 

PUBLIC INT4 OspfTeCspfSearchUniLinks 
                    (UINT1 *pPtr,tOsTeCandteNode * pCandteNode, 
       UINT1 u1VertType, tOsTeLinkNode  *pLinkNode, 
       tOsTeArea * pArea, tOsTeCspfReq * pCspfReq); 

PUBLIC BOOLEAN IsOspfTeStrictExpRoute
                    (tOsTePath *pPath, tOsTeExpRoute *pExpRoute,
                     UINT2 u2Count1, UINT2 u2Count);        

INT4 OspfTeCspfFindPaths (tOsTeCspfReq  *pCspfReq, tOsTeAppPath  *aPath);
/* Prototypes for creating, deleting and flushing the TE-Links */
PUBLIC VOID OspfTeCreateTeLink (tOsTeLinkMsg * pOsTeLinkMsg); 
PUBLIC VOID OspfTeUpdateTeLink (tOsTeLinkMsg * pOsTeLinkMsg, 
                  tOsTeInterface * pTeIfNode);
PUBLIC VOID OspfTeFlushTeLink (tOsTeInterface * pTeIfNode);

/* Registering for TLM */
PUBLIC UINT4 OspfTeSndMsgToTlm (UINT1 u1MsgType);
PUBLIC VOID OspfTeProcessTlmMsg (tOsTeLinkMsg * pTlmMsg);
PUBLIC VOID OspfTeProcessTlmLinkInfo (tOsTeLinkMsg * pTlmOsTeLinkMsg);
/************************** TLM and RM Deregistration ************************/
PUBLIC VOID OspfTeProcessDeReg (VOID);
#endif  /* _OSPF_TE_PROT_H_ */
