/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: osteinc.h,v 1.7 2012/03/21 13:05:43 siva Exp $
*
* Description: This file contains the list of include files.
*********************************************************************/

#ifndef _OSPF_TE_INC_H_
#define _OSPF_TE_INC_H_

#include "lr.h"

#include "ospf.h"
#include "ip.h"

#include "ospfte.h"

#include "ostedefn.h"
#include "osteport.h"
#include "ostemacs.h"
#include "ostemsgs.h"
#include "ostetdfs.h"
#include "osteextn.h"
#include "ostedbg.h"
#include "ostebufif.h"
#include "ostecmn.h"
#include "osteprot.h"

#ifdef SNMP_WANTED
#include "snmcport.h"
#include "snmccons.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#endif
#include "cfa.h"
#include "fssnmp.h"
#include "snmccons.h"
#include "osterm.h"
#include "ospftesz.h"
#include "ostermsz.h"


#endif  /* _OSPF_TE_INC_H_ */
