/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostebufif.h,v 1.4 2012/02/01 12:41:40 siva Exp $
 *
 * Description: This file contains constants , typedefs & macros
 *              related to Buffer interface.
 *********************************************************************/

#ifndef _OSBUFIF_H
#define _OSBUFIF_H

#define  OSPF_TE_IF_POOL_ID         gOsTeContext.teMemPool.TeIfPoolId
#define  OSPF_TE_OSPF_INFO_POOL_ID  gOsTeContext.teMemPool.OspfInfIfPoolId
#define  OSPF_TE_IF_DESC_POOL_ID    gOsTeContext.teMemPool.IfDescPoolId
#define  OSPF_TE_AREA_POOL_ID       gOsTeContext.teMemPool.AreaPoolId
#define  OSPF_TE_TEDB_HASH_POOL_ID  gOsTeContext.teMemPool.TeDbNodePoolId
#define  OSPF_TE_LSA_INFO_POOL_ID   gOsTeContext.teMemPool.TeLsaInfoPoolId
#define  OSPF_TE_LSA_POOL_ID        gOsTeContext.teMemPool.TeLsaPoolId
#define  OSPF_TE_LINK_TLV_POOL_ID   gOsTeContext.teMemPool.LinkTlvPoolId
#define  OSPF_TE_CANDTE_POOL_ID     gOsTeContext.teMemPool.CandtePoolId
#define  OSPF_TE_PATH_POOL_ID       gOsTeContext.teMemPool.PathPoolId
#define  OSPF_TE_QMSG_QID           gOsTeContext.teMemPool.QMsgPoolId
#define  OSPF_TE_CSPF_REQ_POOL_ID   gOsTeContext.teMemPool.CspfReqPoolId
#define  OSPF_TE_CSPF_CONS_POOL_ID  gOsTeContext.teMemPool.CspfConsPoolId
#define OSPF_TE_LINK_TLV_SIZE_POOL_ID gOsTeContext.teMemPool.LinkTlvSizePoolId
#define OSPF_TE_RM_LINK_INFO_POOL_ID  gOsTeContext.teMemPool.RmLinkInfoPoolId
#define OSPF_TE_RM_LINK_SRLG_INFO_POOL_ID \
                         gOsTeContext.teMemPool.RmLinkSrlgPoolId
#define OSPF_TE_MAX_NET_NBRS_POOL_ID gOsTeContext.teMemPool.MaxNetNbrsPoolId
#define OSPF_TE_RM_LINK_MSG_POOL_ID gOsTeContext.teMemPool.RmLinkMsgPoolId

#define OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(PoolId,pu1Block,type) \
         (pu1Block = (type *) MemAllocMemBlk ((tMemPoolId)PoolId))

#define OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(PoolId,pu1Block) \
         MemReleaseMemBlock((tMemPoolId)PoolId, (UINT1 *)pu1Block)


/* Macros for Buffer allocation */
#define OSPF_TE_IF_ALLOC(pu1Block,type) \
     OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_IF_POOL_ID,pu1Block,type)

#define OSPF_TE_OSPF_INFO_IF_ALLOC(pu1Block,type) \
     OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_OSPF_INFO_POOL_ID,pu1Block,type)

#define OSPF_TE_IF_DESC_ALLOC(pu1Block,type) \
     OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_IF_DESC_POOL_ID,pu1Block,type)

#define OSPF_TE_AREA_ALLOC(pu1Block,type) \
     OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_AREA_POOL_ID,pu1Block,type)

#define OSPF_TE_TEDB_HASH_NODE_ALLOC(pu1Block,type) \
     OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_TEDB_HASH_POOL_ID,pu1Block,type)
 
#define OSPF_TE_LSA_INFO_ALLOC(pu1Block,type) \
     OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_LSA_INFO_POOL_ID,pu1Block,type)

#define OSPF_TE_LSA_ALLOC(pu1Block,type)\
          OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ( OSPF_TE_LSA_POOL_ID,pu1Block,type)
 
#define OSPF_TE_LINK_TLV_ALLOC(pu1Block,type) \
     OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_LINK_TLV_POOL_ID,pu1Block,type)

#define OSPF_TE_CANDTE_NODE_ALLOC(pu1Block,type) \
     OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_CANDTE_POOL_ID,pu1Block,type)

#define OSPF_TE_PATH_NODE_ALLOC(pu1Block,type) \
     OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_PATH_POOL_ID,pu1Block,type)

#define OSPF_TE_QMSG_ALLOC(pu1Block,type) \
            OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_QMSG_QID,pu1Block,type)

#define OSPF_TE_CSPF_REQ_ALLOC(pu1Block,type) \
            OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_CSPF_REQ_POOL_ID, \
                                              pu1Block,type)

#define OSPF_TE_CSPF_CONS_ALLOC(pu1Block,type) \
            OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_CSPF_CONS_POOL_ID, \
                                              pu1Block,type)

#define OSPF_TE_RM_LINK_INFO_ALLOC(pu1Block,type) \
             OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_RM_LINK_INFO_POOL_ID, \
                                 pu1Block,type)

#define OSPF_TE_RM_LINK_SRLG_INFO_ALLOC(pu1Block,type) \
             OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_RM_LINK_SRLG_INFO_POOL_ID, \
                                 pu1Block,type)

#define OSPF_TE_RM_LINK_MSG_ALLOC(pu1Block,type) \
             OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_RM_LINK_MSG_POOL_ID, \
                                 pu1Block,type)

#define OSPF_TE_LINK_TLV_SIZE_ALLOC(pu1Block,type) \
             OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_LINK_TLV_SIZE_POOL_ID, \
                  pu1Block,type)

#define OSPF_TE_MAX_NET_NBRS_ALLOC(pu1Block,type) \
              OSPF_TE_CRU_BUF_ALLOCATE_FREE_OBJ(OSPF_TE_MAX_NET_NBRS_POOL_ID, \
               pu1Block,type)
/* Macros for Buffer Freeing */ 
#define  OSPF_TE_IF_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_IF_POOL_ID, (UINT1 *)ptr)
 
#define  OSPF_TE_OSPF_INFO_IF_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_OSPF_INFO_POOL_ID, \
                       (UINT1 *)ptr)
 
#define  OSPF_TE_IF_DESC_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_IF_DESC_POOL_ID, \
                       (UINT1 *)ptr)
 
#define  OSPF_TE_AREA_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_AREA_POOL_ID, (UINT1 *)ptr)

#define  OSPF_TE_TEDB_HASH_NODE_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_TEDB_HASH_POOL_ID,\
                     (UINT1 *)ptr)

#define  OSPF_TE_LSA_INFO_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_LSA_INFO_POOL_ID,\
                                          (UINT1 *)ptr)

#define  OSPF_TE_LSA_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_LSA_POOL_ID,\
                                           (UINT1 *)ptr)

#define  OSPF_TE_LINK_TLV_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_LINK_TLV_POOL_ID,\
                                          (UINT1 *)ptr)

#define  OSPF_TE_CANDTE_NODE_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_CANDTE_POOL_ID,\
                                          (UINT1 *)ptr)

#define  OSPF_TE_PATH_NODE_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_PATH_POOL_ID,\
                                          (UINT1 *)ptr)

#define  OSPF_TE_QMSG_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_QMSG_QID,\
                                          (UINT1 *)ptr)

#define OSPF_TE_CSPF_REQ_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_CSPF_REQ_POOL_ID, \
                                          (UINT1 *)ptr)

#define OSPF_TE_CSPF_CONS_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_CSPF_CONS_POOL_ID, \
                                          (UINT1 *)ptr)

#define OSPF_TE_RM_LINK_INFO_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_RM_LINK_INFO_POOL_ID, \
                                          (UINT1 *)ptr)

#define OSPF_TE_RM_LINK_SRLG_INFO_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_RM_LINK_SRLG_INFO_POOL_ID, \
                                        (UINT1 *)ptr)
#define OSPF_TE_RM_LINK_MSG_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_RM_LINK_MSG_POOL_ID, \
                                          (UINT1 *)ptr)

#define OSPF_TE_MAX_NET_NBRS_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_MAX_NET_NBRS_POOL_ID, \
                                        (UINT1 *)ptr)

#define OSPF_TE_LINK_TLV_SIZE_FREE(ptr) \
            OSPF_TE_CRU_BUF_RELEASE_FREE_OBJ(OSPF_TE_LINK_TLV_SIZE_POOL_ID, \
                                        (UINT1 *)ptr)

#endif  /* _OSBUFIF_H */
