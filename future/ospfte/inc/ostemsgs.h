/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: ostemsgs.h,v 1.5 2011/10/28 10:58:54 siva Exp $
*
* Description: This file contains definitions of message structures.
*********************************************************************/

#ifndef _OSPF_TE_MSGS_H_
#define _OSPF_TE_MSGS_H_

#include "ostetdfs.h"
#include "rsvp.h"
/*  Macro used to send message from OSPF to OSPF-TE */
#define OSPF_TE_MSG_SEND(pMsg)  OsixSendToQ ((UINT4) 0, \
                               (const UINT1 *) OSPF_TE_Q_NAME, \
                               (tOsixMsg *)(VOID *)  pMsg, \
                                OSIX_MSG_NORMAL) 

/* ----------------------------------------- */
/* OSPF-TE Message Header structure          */
/* ----------------------------------------- */
/* Holds the message header information given by OSPF or RM */
typedef struct _OspfTeQMsg {
    union {
        tOsTeLinkMsg  ospfTeIfParam;
        tOsToOpqApp   osToOsTeIfParam;
        tCspfCompInfo cspfCompInfo;
    } unOspfTeMsgIfParam;
    UINT4    u4MsgType;
} tOspfTeQMsg;

#define  rmOspfTeIfParam   unOspfTeMsgIfParam.ospfTeIfParam
#define  tlmOspfTeIfParam  unOspfTeMsgIfParam.ospfTeIfParam
#define  osToOsTeIfParam   unOspfTeMsgIfParam.osToOsTeIfParam
#define  cspfCompInfo      unOspfTeMsgIfParam.cspfCompInfo

typedef tOpqLSAInfo tOsTeOspfMsg;

#endif  /* _OSPF_TE_MSGS_ */
