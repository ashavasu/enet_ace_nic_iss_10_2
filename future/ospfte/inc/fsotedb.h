/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsotedb.h,v 1.3 2008/08/20 15:22:22 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSOTEDB_H
#define _FSOTEDB_H

UINT1 FutOspfTeLsdbTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfTeType9LsdbTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfTeAreaTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfTeIfTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FutOspfTeIfDescriptorTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FutOspfTeIfSwDescrMaxBwTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfTeIfBandwidthTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsote [] ={1,3,6,1,4,1,2076,72};
tSNMP_OID_TYPE fsoteOID = {8, fsote};


UINT4 FutOspfTeAdminStatus [ ] ={1,3,6,1,4,1,2076,72,1,1};
UINT4 FutOspfTeTraceLevel [ ] ={1,3,6,1,4,1,2076,72,1,2};
UINT4 FutOspfTeCspfRunCnt [ ] ={1,3,6,1,4,1,2076,72,1,3};
UINT4 FutOspfTeLsdbAreaId [ ] ={1,3,6,1,4,1,2076,72,2,1,1};
UINT4 FutOspfTeLsdbType [ ] ={1,3,6,1,4,1,2076,72,2,1,2};
UINT4 FutOspfTeLsdbLsid [ ] ={1,3,6,1,4,1,2076,72,2,1,3};
UINT4 FutOspfTeLsdbRouterId [ ] ={1,3,6,1,4,1,2076,72,2,1,4};
UINT4 FutOspfTeLsdbChecksum [ ] ={1,3,6,1,4,1,2076,72,2,1,5};
UINT4 FutOspfTeLsdbAdvertisement [ ] ={1,3,6,1,4,1,2076,72,2,1,6};
UINT4 FutOspfTeType9LsdbIfIpAddress [ ] ={1,3,6,1,4,1,2076,72,3,1,1};
UINT4 FutOspfTeType9LsdbIfIndex [ ] ={1,3,6,1,4,1,2076,72,3,1,2};
UINT4 FutOspfTeType9LsdbLsid [ ] ={1,3,6,1,4,1,2076,72,3,1,3};
UINT4 FutOspfTeType9LsdbRouterId [ ] ={1,3,6,1,4,1,2076,72,3,1,4};
UINT4 FutOspfTeType9LsdbChecksum [ ] ={1,3,6,1,4,1,2076,72,3,1,5};
UINT4 FutOspfTeType9LsdbAdvertisement [ ] ={1,3,6,1,4,1,2076,72,3,1,6};
UINT4 FutOspfTeAreaId [ ] ={1,3,6,1,4,1,2076,72,4,1,1};
UINT4 FutOspfTeAreaLsaCount [ ] ={1,3,6,1,4,1,2076,72,4,1,2};
UINT4 FutOspfTeType10AreaCksumSum [ ] ={1,3,6,1,4,1,2076,72,4,1,3};
UINT4 FutOspfTeType2AreaCksumSum [ ] ={1,3,6,1,4,1,2076,72,4,1,4};
UINT4 FutOspfTeIfIpAddress [ ] ={1,3,6,1,4,1,2076,72,5,1,1};
UINT4 FutOspfTeAddressLessIf [ ] ={1,3,6,1,4,1,2076,72,5,1,2};
UINT4 FutOspfTeIfAreaId [ ] ={1,3,6,1,4,1,2076,72,5,1,3};
UINT4 FutOspfTeIfType [ ] ={1,3,6,1,4,1,2076,72,5,1,4};
UINT4 FutOspfTeIfMetric [ ] ={1,3,6,1,4,1,2076,72,5,1,5};
UINT4 FutOspfTeIfMaxBw [ ] ={1,3,6,1,4,1,2076,72,5,1,6};
UINT4 FutOspfTeIfMaxReservBw [ ] ={1,3,6,1,4,1,2076,72,5,1,7};
UINT4 FutOspfTeIfRsrcClassColor [ ] ={1,3,6,1,4,1,2076,72,5,1,8};
UINT4 FutOspfTeIfOperStat [ ] ={1,3,6,1,4,1,2076,72,5,1,9};
UINT4 FutOspfTeIfLinkId [ ] ={1,3,6,1,4,1,2076,72,5,1,10};
UINT4 FutOspfTeIfRemoteIpAddr [ ] ={1,3,6,1,4,1,2076,72,5,1,11};
UINT4 FutOspfTeIfProtectionType [ ] ={1,3,6,1,4,1,2076,72,5,1,12};
UINT4 FutOspfTeIfSrlg [ ] ={1,3,6,1,4,1,2076,72,5,1,13};
UINT4 FutOspfTeIfDescrIpAddress [ ] ={1,3,6,1,4,1,2076,72,6,1,1};
UINT4 FutOspfTeIfDescrAddressLessIf [ ] ={1,3,6,1,4,1,2076,72,6,1,2};
UINT4 FutOspfTeIfDescrId [ ] ={1,3,6,1,4,1,2076,72,6,1,3};
UINT4 FutOspfTeIfDescrSwithingCap [ ] ={1,3,6,1,4,1,2076,72,6,1,4};
UINT4 FutOspfTeIfDescrEncodingType [ ] ={1,3,6,1,4,1,2076,72,6,1,5};
UINT4 FutOspfTeIfDescrMinLSPBandwidth [ ] ={1,3,6,1,4,1,2076,72,6,1,6};
UINT4 FutOspfTeIfDescrMTU [ ] ={1,3,6,1,4,1,2076,72,6,1,7};
UINT4 FutOspfTeIfDescrIndication [ ] ={1,3,6,1,4,1,2076,72,6,1,8};
UINT4 FutOspfTeIfSwDescrMaxBwPriority [ ] ={1,3,6,1,4,1,2076,72,7,1,1};
UINT4 FutOspfTeIfSwDescrMaxLSPBandwidth [ ] ={1,3,6,1,4,1,2076,72,7,1,2};
UINT4 FutOspfTeIfBandwidthPriority [ ] ={1,3,6,1,4,1,2076,72,8,1,1};
UINT4 FutOspfTeIfUnreservedBandwidth [ ] ={1,3,6,1,4,1,2076,72,8,1,2};


tMbDbEntry fsoteMibEntry[]= {

{{10,FutOspfTeAdminStatus}, NULL, FutOspfTeAdminStatusGet, FutOspfTeAdminStatusSet, FutOspfTeAdminStatusTest, FutOspfTeAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfTeTraceLevel}, NULL, FutOspfTeTraceLevelGet, FutOspfTeTraceLevelSet, FutOspfTeTraceLevelTest, FutOspfTeTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfTeCspfRunCnt}, NULL, FutOspfTeCspfRunCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FutOspfTeLsdbAreaId}, GetNextIndexFutOspfTeLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfTeLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeLsdbType}, GetNextIndexFutOspfTeLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfTeLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeLsdbLsid}, GetNextIndexFutOspfTeLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfTeLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeLsdbRouterId}, GetNextIndexFutOspfTeLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfTeLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeLsdbChecksum}, GetNextIndexFutOspfTeLsdbTable, FutOspfTeLsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfTeLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeLsdbAdvertisement}, GetNextIndexFutOspfTeLsdbTable, FutOspfTeLsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FutOspfTeLsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeType9LsdbIfIpAddress}, GetNextIndexFutOspfTeType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfTeType9LsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeType9LsdbIfIndex}, GetNextIndexFutOspfTeType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfTeType9LsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeType9LsdbLsid}, GetNextIndexFutOspfTeType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfTeType9LsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeType9LsdbRouterId}, GetNextIndexFutOspfTeType9LsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfTeType9LsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeType9LsdbChecksum}, GetNextIndexFutOspfTeType9LsdbTable, FutOspfTeType9LsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfTeType9LsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeType9LsdbAdvertisement}, GetNextIndexFutOspfTeType9LsdbTable, FutOspfTeType9LsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FutOspfTeType9LsdbTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeAreaId}, GetNextIndexFutOspfTeAreaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfTeAreaTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfTeAreaLsaCount}, GetNextIndexFutOspfTeAreaTable, FutOspfTeAreaLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfTeAreaTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfTeType10AreaCksumSum}, GetNextIndexFutOspfTeAreaTable, FutOspfTeType10AreaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfTeAreaTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfTeType2AreaCksumSum}, GetNextIndexFutOspfTeAreaTable, FutOspfTeType2AreaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfTeAreaTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfTeIfIpAddress}, GetNextIndexFutOspfTeIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeAddressLessIf}, GetNextIndexFutOspfTeIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfAreaId}, GetNextIndexFutOspfTeIfTable, FutOspfTeIfAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfType}, GetNextIndexFutOspfTeIfTable, FutOspfTeIfTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfMetric}, GetNextIndexFutOspfTeIfTable, FutOspfTeIfMetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfMaxBw}, GetNextIndexFutOspfTeIfTable, FutOspfTeIfMaxBwGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfMaxReservBw}, GetNextIndexFutOspfTeIfTable, FutOspfTeIfMaxReservBwGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfRsrcClassColor}, GetNextIndexFutOspfTeIfTable, FutOspfTeIfRsrcClassColorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfOperStat}, GetNextIndexFutOspfTeIfTable, FutOspfTeIfOperStatGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfLinkId}, GetNextIndexFutOspfTeIfTable, FutOspfTeIfLinkIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfRemoteIpAddr}, GetNextIndexFutOspfTeIfTable, FutOspfTeIfRemoteIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfProtectionType}, GetNextIndexFutOspfTeIfTable, FutOspfTeIfProtectionTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfSrlg}, GetNextIndexFutOspfTeIfTable, FutOspfTeIfSrlgGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FutOspfTeIfTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfTeIfDescrIpAddress}, GetNextIndexFutOspfTeIfDescriptorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfTeIfDescriptorTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfTeIfDescrAddressLessIf}, GetNextIndexFutOspfTeIfDescriptorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfTeIfDescriptorTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfTeIfDescrId}, GetNextIndexFutOspfTeIfDescriptorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FutOspfTeIfDescriptorTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfTeIfDescrSwithingCap}, GetNextIndexFutOspfTeIfDescriptorTable, FutOspfTeIfDescrSwithingCapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfTeIfDescriptorTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfTeIfDescrEncodingType}, GetNextIndexFutOspfTeIfDescriptorTable, FutOspfTeIfDescrEncodingTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfTeIfDescriptorTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfTeIfDescrMinLSPBandwidth}, GetNextIndexFutOspfTeIfDescriptorTable, FutOspfTeIfDescrMinLSPBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FutOspfTeIfDescriptorTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfTeIfDescrMTU}, GetNextIndexFutOspfTeIfDescriptorTable, FutOspfTeIfDescrMTUGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfTeIfDescriptorTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfTeIfDescrIndication}, GetNextIndexFutOspfTeIfDescriptorTable, FutOspfTeIfDescrIndicationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfTeIfDescriptorTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfTeIfSwDescrMaxBwPriority}, GetNextIndexFutOspfTeIfSwDescrMaxBwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfTeIfSwDescrMaxBwTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeIfSwDescrMaxLSPBandwidth}, GetNextIndexFutOspfTeIfSwDescrMaxBwTable, FutOspfTeIfSwDescrMaxLSPBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FutOspfTeIfSwDescrMaxBwTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfTeIfBandwidthPriority}, GetNextIndexFutOspfTeIfBandwidthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfTeIfBandwidthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfTeIfUnreservedBandwidth}, GetNextIndexFutOspfTeIfBandwidthTable, FutOspfTeIfUnreservedBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FutOspfTeIfBandwidthTableINDEX, 3, 0, 0, NULL},
};
tMibData fsoteEntry = { 44, fsoteMibEntry };
#endif /* _FSOTEDB_H */

