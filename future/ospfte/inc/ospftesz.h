/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ospftesz.h,v 1.1 2012/02/01 12:40:27 siva Exp $
 *
 * Description: This file contains definitions of global variables
 *              used.
 *********************************************************************/

enum {
    MAX_OSPF_TE_AREAS_SIZING_ID,
    MAX_OSPF_TE_CANDTE_NODE_SIZING_ID,
    MAX_OSPF_TE_CONS_SIZING_ID,
    MAX_OSPF_TE_CSPF_REQUEST_SIZING_ID,
    MAX_OSPF_TE_DB_NODE_SIZING_ID,
    MAX_OSPF_TE_DESC_SIZING_ID,
    MAX_OSPF_TE_INTF_SIZING_ID,
    MAX_OSPF_TE_LINK_TLV_NODE_SIZING_ID,
    MAX_OSPF_TE_LINK_TLV_SIZE_SIZING_ID,
    MAX_OSPF_TE_LSAS_NODE_SIZING_ID,
    MAX_OSPF_TE_LSAS_SIZING_ID,
    MAX_OSPF_TE_MAX_NET_NBRS_SIZING_ID,
    MAX_OSPF_TE_OSINT_INFO_SIZING_ID,
    MAX_OSPF_TE_PATH_NODE_SIZING_ID,
    MAX_OSPF_TE_Q_DEPTH_SIZING_ID,
    MAX_OSPF_TE_RM_LINK_INFO_SIZING_ID,
    MAX_OSPF_TE_RM_LINK_MSG_BLOCKS_SIZING_ID,
    MAX_OSPF_TE_RM_LINK_SRLG_INFO_SIZING_ID,
    OSPFTE_MAX_SIZING_ID
};


#ifdef  _OSPFTESZ_C
tMemPoolId OSPFTEMemPoolIds[ OSPFTE_MAX_SIZING_ID];
INT4  OspfteSizingMemCreateMemPools(VOID);
VOID  OspfteSizingMemDeleteMemPools(VOID);
INT4  OspfteSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _OSPFTESZ_C  */
extern tMemPoolId OSPFTEMemPoolIds[ ];
extern INT4  OspfteSizingMemCreateMemPools(VOID);
extern VOID  OspfteSizingMemDeleteMemPools(VOID);
extern INT4  OspfteSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _OSPFTESZ_C  */


#ifdef  _OSPFTESZ_C
tFsModSizingParams FsOSPFTESizingParams [] = {
{ "tOsTeArea", "MAX_OSPF_TE_AREAS", sizeof(tOsTeArea),MAX_OSPF_TE_AREAS, MAX_OSPF_TE_AREAS,0 },
{ "tOsTeCandteNode", "MAX_OSPF_TE_CANDTE_NODE", sizeof(tOsTeCandteNode),MAX_OSPF_TE_CANDTE_NODE, MAX_OSPF_TE_CANDTE_NODE,0 },
{ "tOsTeCspfReq", "MAX_OSPF_TE_CONS", sizeof(tOsTeCspfReq),MAX_OSPF_TE_CONS, MAX_OSPF_TE_CONS,0 },
{ "tOsTeCspfReq", "MAX_OSPF_TE_CSPF_REQUEST", sizeof(tOsTeCspfReq),MAX_OSPF_TE_CSPF_REQUEST, MAX_OSPF_TE_CSPF_REQUEST,0 },
{ "tOsTeDbNode", "MAX_OSPF_TE_DB_NODE", sizeof(tOsTeDbNode),MAX_OSPF_TE_DB_NODE, MAX_OSPF_TE_DB_NODE,0 },
{ "tOsTeIfDescNode", "MAX_OSPF_TE_DESC", sizeof(tOsTeIfDescNode),MAX_OSPF_TE_DESC, MAX_OSPF_TE_DESC,0 },
{ "tOsTeInterface", "MAX_OSPF_TE_INTF", sizeof(tOsTeInterface),MAX_OSPF_TE_INTF, MAX_OSPF_TE_INTF,0 },
{ "tOsTeLinkTlvNode", "MAX_OSPF_TE_LINK_TLV_NODE", sizeof(tOsTeLinkTlvNode),MAX_OSPF_TE_LINK_TLV_NODE, MAX_OSPF_TE_LINK_TLV_NODE,0 },
{ "UINT1[OSPF_TE_LINK_TLV_MAX_SIZE]", "MAX_OSPF_TE_LINK_TLV_SIZE", sizeof(UINT1[OSPF_TE_LINK_TLV_MAX_SIZE]),MAX_OSPF_TE_LINK_TLV_SIZE, MAX_OSPF_TE_LINK_TLV_SIZE,0 },
{ "tOsTeLsaNode", "MAX_OSPF_TE_LSAS_NODE", sizeof(tOsTeLsaNode),MAX_OSPF_TE_LSAS_NODE, MAX_OSPF_TE_LSAS_NODE,0 },
{ "tOspfTeLsa", "MAX_OSPF_TE_LSAS", sizeof(tOspfTeLsa),MAX_OSPF_TE_LSAS, MAX_OSPF_TE_LSAS,0 },
{ "UINT1[OSPF_TE_NET_NBRS_SIZE]", "MAX_OSPF_TE_MAX_NET_NBRS", sizeof(UINT1[OSPF_TE_NET_NBRS_SIZE]),MAX_OSPF_TE_MAX_NET_NBRS, MAX_OSPF_TE_MAX_NET_NBRS,0 },
{ "tOsTeOsIntInfo", "MAX_OSPF_TE_OSINT_INFO", sizeof(tOsTeOsIntInfo),MAX_OSPF_TE_OSINT_INFO, MAX_OSPF_TE_OSINT_INFO,0 },
{ "tOsTePath", "MAX_OSPF_TE_PATH_NODE", sizeof(tOsTePath),MAX_OSPF_TE_PATH_NODE, MAX_OSPF_TE_PATH_NODE,0 },
{ "tOspfTeQMsg", "MAX_OSPF_TE_Q_DEPTH", sizeof(tOspfTeQMsg),MAX_OSPF_TE_Q_DEPTH, MAX_OSPF_TE_Q_DEPTH,0 },
{ "tRmLinkInfo", "MAX_OSPF_TE_RM_LINK_INFO", sizeof(tRmLinkInfo),MAX_OSPF_TE_RM_LINK_INFO, MAX_OSPF_TE_RM_LINK_INFO,0 },
{ "tRmOsTeLinkMsg", "MAX_OSPF_TE_RM_LINK_MSG_BLOCKS", sizeof(tRmOsTeLinkMsg),MAX_OSPF_TE_RM_LINK_MSG_BLOCKS, MAX_OSPF_TE_RM_LINK_MSG_BLOCKS,0 },
{ "tRmLinkSrlgInfo", "MAX_OSPF_TE_RM_LINK_SRLG_INFO", sizeof(tRmLinkSrlgInfo),MAX_OSPF_TE_RM_LINK_SRLG_INFO, MAX_OSPF_TE_RM_LINK_SRLG_INFO,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _OSPFTESZ_C  */
extern tFsModSizingParams FsOSPFTESizingParams [];
#endif /*  _OSPFTESZ_C  */


