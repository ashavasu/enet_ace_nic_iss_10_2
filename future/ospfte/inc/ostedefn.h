/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostedefn.h,v 1.3 2011/11/25 11:06:09 siva Exp $
 *
 * Description: This file contains definitions macros for some 
 *              Hard coded values.
 *********************************************************************/

#ifndef _OSPF_TE_DEFN_H_
#define _OSPF_TE_DEFN_H_

#define OSPF_TE_LSA_ID_LEN        4

/* Opaque application ID for TE */
#define OSPF_TE_OPQ_TYPE            1
/* Type 9 and Type 10 LSAs support is needed by OSPF-TE */
#define OSPF_TE_OPQ_LSA_SUPPORTED   3  /* 0 1 1 */ 
#define OSPF_TE_OSPF_INFO           31 

#define OSPF_TE_TRUE              1 
#define OSPF_TE_FALSE             2

#define OSPF_TE_INVALID           0

#define OSPF_TE_DOWN              1
#define OSPF_TE_ACTIVE            3

#define OSPF_TE_ENABLED           1
#define OSPF_TE_DISABLED          2

#define OSPF_TE_MEM_FAILURE       2 
#define ALLOC_FAILURE            -1

#define OSPF_TE_OSPF_MSG          1
#define OSPF_TE_RMGR_MSG          2
#define OSPF_TE_CSPF_MSG          3
#define OSPF_TE_TLM_MSG           4

#define OSPF_TE_RM_REG            1
#define OSPF_TE_RM_LINK_INFO      2
#define OSPF_TE_RM_DEREG          3

#define OSPF_TE_TLM_REG           1
#define OSPF_TE_TLM_DEREG         2

#define OSPF_TE_OSPF_REG            1
#define OSPF_TE_OSPF_SEND_OPQ_LSA   2
#define OSPF_TE_OSPF_DEREG          3

#define OSPF_TE_AREA_CHANNEL           1
#define OSPF_TE_DATA_CHANNEL           2
#define OSPF_TE_DATA_CONTROL_CHANNEL   3

#define OSPF_TE_DEFAULT_METRIC  10

/* link types to be used in router lsa */
#define  PTOP_LINK                1
#define  TRANSIT_LINK             2
#define  STUB_LINK                3
#define  VIRTUAL_LINK             4

#define OSPF_TE_PTOP              1
#define OSPF_TE_MULTIACCESS       2

#define OSPF_TE_NEW_LSA           1
#define OSPF_TE_NEW_INSTANCE_LSA  2 
#define OSPF_TE_FLUSHED_LSA       4

#define OSPF_TE_EQUAL             0 
#define OSPF_TE_GREATER           1 
#define OSPF_TE_LESS              2 

#define OSPF_TE_ROUTER_LSA        1 
#define OSPF_TE_NETWORK_LSA       2
#define OSPF_TE_TYPE9_LSA         9 
#define OSPF_TE_TYPE10_LSA        10 

#define OSPF_TE_NEW_TLV           1
#define OSPF_TE_NEW_INSTANCE_TLV  2
#define OSPF_TE_FLUSHED_TLV       4 

/* Defintiions needed for messages */
#define OSPF_TE_ID                1
#define OSPF_TE_LINK_TLV          2
#define OSPF_TE_TLV_TYPE_LEN      2
#define OSPF_TE_TLV_VALUE_LEN     2
#define OSPF_TE_TLV_LEN_LEN       2

#define SUB_TLV_LEN_LEN           2

/* SUB TLV Types */
#define OSPF_TE_LINKTYPE          1
#define OSPF_TE_LINKID            2
#define OSPF_TE_LOCALIF           3
#define OSPF_TE_REMOTEIF          4
#define OSPF_TE_METRIC            5
#define OSPF_TE_MAXBW             6
#define OSPF_TE_MAXRESBW          7
#define OSPF_TE_UNRESBW           8
#define OSPF_TE_RSRCLR            9
#define OSPF_TE_IDENTIFIERS       11
#define OSPF_TE_PROTECTION_TYPE   14
#define OSPF_TE_INSWCA_DESC       15 
#define OSPF_TE_SRLG              16

#define  OSPF_TE_MAX_SUB_TLVS      13

/* SUB TLV Lengths */
#define OSPF_TE_LINKTYPE_LEN      1
#define OSPF_TE_LINKID_LEN        4
#define OSPF_TE_LOCALIF_LEN       4
#define OSPF_TE_REMOTEIF_LEN      4
#define OSPF_TE_METRIC_LEN        4
#define OSPF_TE_MAXBW_LEN         4
#define OSPF_TE_MAXRESBW_LEN      4
#define OSPF_TE_UNRESBW_LEN       32
#define OSPF_TE_RSRCLR_LEN        4
#define OSPF_TE_IDENTIFIERS_LEN   8 
#define OSPF_TE_PROTEC_TYPE_LEN   4

/* Router Address TLV */
#define OSPF_TE_RTR_ADDR_TLV      1
#define OSPF_TE_LINK_TLV          2

#define OSPF_TE_RTR_ADDR_TLV_LEN  8  /* Type   - 2 bytes
                                      * Length - 2 bytes
                                      * Value  - 4 bytes  
          */
#define OSPF_TE_RTR_ADDR_LEN      4  /* Value - 4 bytes */

#define OSPF_TE_TYPE9_LSA_TLV_TYPE       1
#define OSPF_TE_TYPE9_LSA_TLV_LENGTH     4
#define OSPF_TE_TYPE9_LSA_TLV_SIZE       8

#define OSPF_TE_LSA_HEADER_LENGTH       20 
#define OSPF_TE_LSA_OPAQUE_TYPE_OFFSET  4

#define OSPF_TE_MAX_NBRS                32 /* This MACRO value should 
           be changed to be same as 
           MAX_NBRS defined 
           in ospf/inc/oscfgvar.h*/ 
#define OSPF_TE_NET_NBRS_SIZE           (OSPF_TE_MAX_NBRS * MAX_IP_ADDR_LEN) 
#define OSPF_TE_NET_NBRS_OFFSET   (OSPF_TE_LSA_HEADER_LENGTH + MAX_IP_ADDR_LEN)

#define OSPF_TE_ZERO                     0
#define OSPF_TE_LINK_TYPE_SUB_SIZE       8
#define OSPF_TE_LINK_ID_SUB_SIZE         8
#define OSPF_TE_LOCAL_IP_ADDR_SUB_SIZE   8
#define OSPF_TE_REMOTE_IP_ADDR_SUB_SIZE  8
#define OSPF_TE_METRIC_SUB_SIZE          8

#define OSPF_TE_ADMIN_GROUP_SUB_SIZE     8
#define OSPF_TE_IDENTIFIERS_SUB_SIZE     12 
#define OSPF_TE_PROTEC_TYPE_SUB_SIZE     8 

#define OSPF_TE_STANDARD_SDH      0 
#define OSPF_TE_ARBITRARY         1 


#define OSPF_TE_TE_METRIC        1
#define OSPF_TE_OSPF_METRIC      2

#define OSPF_TE_LINK_TYPE_PAD       3
#define OSPF_TE_PROTECTION_RES_LEN  3

#define OSPF_TE_PASS_ADD   1
#define OSPF_TE_PASS_DEL   2

#define  OSPF_TE_ONE               1
#define  OSPF_TE_TWO               2
#define  OSPF_TE_THREE             3
#define  OSPF_TE_NULL              0
#define  OSTE_ONE_BYTE             8

#define OSPF_TE_VERT_NETWORK       0 
#define OSPF_TE_VERT_ROUTER        1

#define OSPF_TE_MAX_METRIC         0xffffffff
#define OSPF_TE_INVALID_INFO       0xffffffff

#endif  /* _OSPF_TE_DEFN_H_ */
