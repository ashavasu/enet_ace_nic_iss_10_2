/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: osteglob.h,v 1.3 2011/12/12 12:55:29 siva Exp $
 *
 * Description: This file contains definitions of global variables
 *              used. 
 *********************************************************************/

#ifndef _OSPF_TE_GLOB_H_
#define _OSPF_TE_GLOB_H_

tIPADDR          gOsTeNullIpAddr       = {0,0,0,0};
tOsTeContext     gOsTeContext;
tOsixSemId       gOspfTeSemId;
tOsixQId gOspfTeQid = 0;

#endif  /* _OSPF_TE_GLOB_H_ */
