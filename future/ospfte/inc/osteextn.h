/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: osteextn.h,v 1.4 2011/10/28 10:58:54 siva Exp $
 *
 * Description: This file contains declarations of global variables
 *              used. 
 *********************************************************************/

#ifndef _OSPF_TE_EXTN_H_
#define _OSPF_TE_EXTN_H_
extern tOsTeContext   gOsTeContext;
extern tIPADDR        gOsTeNullIpAddr;
extern double UtilCeil(double);
extern INT4   BuddyCreate       (UINT4 u4Size, UINT4 u4NumUnits, UINT4 u4TypeOfMem);
extern INT4   BuddyDestroy      (UINT4 u4Id);
extern UINT4  BuddyAllocate     (UINT4 u4Size, UINT4 u4Id, UINT1 **ptr);
extern INT4   BuddyRelease      (UINT1* pu1Ptr, UINT4 u4Id);
extern tTMO_SLL   gOsTeAppConsLst;


#endif  /* _OSPF_TE_EXTN_H_ */
