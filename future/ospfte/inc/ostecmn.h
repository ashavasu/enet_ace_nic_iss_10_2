/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ostecmn.h,v 1.2 2012/06/15 05:58:50 siva Exp $
 *
 * Description: This file contains the Structures that are Common to
 *              both OSPFTE and APP
 *********************************************************************/

#ifndef _OSPF_TE_CMN_H_
#define _OSPF_TE_CMN_H_

/* -------------------- */
/* CSPF Request message */
/* -------------------- */
typedef struct _OsTeCspfReq {
    /* Based on the u1SrlgType include any srlg or include all srlg 
     * or exclude any srlg is considered */
    tOsTeSrlg          osTeSrlg; 
    /* Explicit route path. In Case of MBB, will hold the primary path */
    tOsTeExpRoute        osTeExpRoute;
    /* In Case of MBB, MAY hold the Alternate Path if Specified 
     * The Alternate path specified may be include or exclude */
    tOsTeExpRoute        osTeAlternateRoute;
    /* Bandwidth Desired */
    tBandWidth           bandwidth;
    /* In Case of MBB, holds the bandwidth of the tunnel mentioned
     * in Primary Path */
    tBandWidth            oldbandwidth;
    /* Source Address to be considered by CSPF */
    UINT4                u4SrcIpAddr;   
    /* Destination Address to be considered by CSPF */
    UINT4                u4DestIpAddr;
    UINT4                u4WPSrcIpAddr;
    UINT4                u4WPDestIpAddr;
    UINT4                u4MaxPathMetric;
    /* Include all, Include Any, Exclude Any Sets corresponds to Resource color */
    UINT4                u4IncludeAllSet;
    UINT4                u4IncludeAnySet;
    UINT4                u4ExcludeAnySet;
    UINT2                u2HopCount;
    UINT1                u1ProtectionType;
    UINT1                u1Indication;
    UINT1                u1SwitchingCap;
    UINT1                u1EncodingType;
    /* Priority desired - In case of Diff-serv, should be considered as
     * TE-CLASS Priority */
    UINT1                u1Priority;
    /* May hold the flags: OSPF_TE_NODE_DISJOINT_BIT_MASK or
     * OSPF_TE_LINK_DISJOINT_BIT_MASK or OSPF_TE_SRLG_DISJOINT_BIT_MASK or 
     * All */
    UINT1                u1Diversity; 
    /* backup path , RsrcAffinity, direction */
    UINT1                u1Flag;
    /* May hold OSPF_TE_SRLG_TYPE_INCLUDE_ALL or OSPF_TE_SRLG_TYPE_INCLUDE_ANY
     * or OSPF_TE_SRLG_TYPE_EXCLUDE_ANY  */
    UINT1                u1SrlgType;
    /* May hold OSPF_TE_RSRC_TYPE_INCLUDE_ALL or OSPF_TE_RSRC_TYPE_INCLUDE_ANY
     * or OSPF_TE_RSRC_TYPE_EXCLUDE_ANY  */
    UINT1                u1RsrcSetType;
    UINT1                au1Unused [1];
} tOsTeCspfReq;

typedef struct _OsTeCspfCons {
 tTMO_SLL_NODE      NextConsNode;
 UINT4              u4ConstraintId;
 UINT4              u4ConsStatus;
 tOsTeCspfReq       *pCspfReq;
 tOsTeAppPath       aPath[2];
}tOsTeCspfCons;

#endif  /* _OSPF_TE_CMN_H_ */
