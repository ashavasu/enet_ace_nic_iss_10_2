/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: ostemacs.h,v 1.9 2014/02/14 14:07:36 siva Exp $
*
* Description: This file contains definitions of macros used.
*********************************************************************/

#ifndef _OSPF_TE_MACS_H_
#define _OSPF_TE_MACS_H_

#define OSPF_TE_DYNM_SLL_SCAN(pList, pNode, pTempNode, TypeCast) \
        for(((pNode) = (TypeCast)(TMO_SLL_First((pList)))), \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
            ((TypeCast)    \
            (TMO_SLL_Next((pList),  \
            ((tTMO_SLL_NODE *)(VOID *) (pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
            ((TypeCast)  \
            (TMO_SLL_Next((pList),  \
            ((tTMO_SLL_NODE *)(VOID *) (pNode))))))))

/*  Getting the base pointers */
#define OSPF_TE_GET_BASE_PTR(type, memberName, pMember) \
       (type *) ((FS_ULONG)(pMember) - ((FS_ULONG) &((type *)0)->memberName))

#define IS_MAX_AGE(age) ((age >= MAX_AGE && age < DO_NOT_AGE) || \
                       (age >= MAX_AGE + DO_NOT_AGE))


#define GET_LSA_INFO_PTR_FROM_LST(x) (tLsaInfo *)(((UINT1*)x) \
                                   - OSPF_OFFSET(tLsaInfo, nextLsaInfo))


#define  LADD1BYTE(addr, val)     *((UINT1 *)addr) = val; addr++;
#define  LADD2BYTE(addr, val)     OSPF_TE_CRU_BMC_WTOPDU(addr, val); addr += 2;
#define  LADD3BYTE(addr, val)     THREE_BYTE_TO_PDU(addr, (val)); addr += 3;
#define  LADD4BYTE(addr, val)     OSPF_TE_CRU_BMC_DWTOPDU(addr, val); addr += 4;
#define  LADDSTR(addr, src, len)  OSPF_TE_MEMCPY (addr, src, len); addr += len;

/* Convert Float to data in packet (Network byte order) */
#define LADDFLOAT(addr, val)      OSPF_TE_CRU_BMC_FLTOPDU(addr, val); \
                           addr += 4;
#define OSPF_TE_CRU_BMC_FLTOPDU(pu1PduAddr,F4Value) \
        *((FLT4 *) (VOID *) (pu1PduAddr)) = OSIX_HTONF(F4Value);

/* macros extracting info from linear buffer */
#define LGETSTR(addr, dest, len) \
                           OSPF_TE_MEMCPY ((UINT1 *)dest, (UINT1 *)addr, len);\
                               addr += len;

/* the following macros have certain constraints
 * 1. used only in the rhs of assignment operator
 * 2. addr should be a single variable(not an expression)
 * 3. the macro itself should not be a part of any expression
 */

#define  LGET1BYTE(addr)  *((UINT1 *)addr); addr += 1;
#define  LGET2BYTE(addr)  OSPF_TE_CRU_BMC_WFROMPDU(addr); addr += 2;
#define  LGET3BYTE(addr)  THREE_BYTE_FROM_PDU(addr); addr += 3;
#define  LGET4BYTE(addr)  OSPF_TE_CRU_BMC_DWFROMPDU(addr); addr += 4;

/* This macro extracts 4 bytes from packet and stores in float */
#define LGETFLOAT(addr)  (FLT4) OSPF_TE_CRU_BMC_FLFROMPDU(addr); addr += 4;

#define OSPF_TE_CRU_BMC_FLFROMPDU(pu1PduAddr) \
        OSIX_NTOHF(*((FLT4 *) (VOID *) (pu1PduAddr)))

#define THREE_BYTE_TO_PDU(addr, val) { \
                 *((UINT1 *)(addr)   ) =(UINT1) ((val >> 16)&0xff); \
                 *((UINT1 *)(addr) +1) =(UINT1) ((val >> 8)&0xff); \
                 *((UINT1 *)(addr) +2) =(UINT1) (val & 0xff);}

#define THREE_BYTE_FROM_PDU(addr) \
       ( ((*((UINT1 *)(addr)   ) << 16) & 0x00ff0000) |\
         ((*((UINT1 *)(addr) +1) << 8 ) & 0x0000ff00) |\
         ((*((UINT1 *)(addr) +2)      ) & 0x000000ff) )

#define  IP_ADDR_COPY(dst,src)   OSPF_TE_MEMCPY(dst, src, MAX_IP_ADDR_LEN)

#define IS_SELF_ORIGINATED_LSA(pLsaInfo) \
                        (UtilIpAddrComp(pLsaInfo->advRtrId,\
                        gOsTeContext.rtrId) == OSPF_EQUAL)


#define OSPF_TE_IF_HASH_FUNC(x)\
        OspfTeUtilHashGetValue (OSPF_TE_IF_HASH_TBL_SIZE, (UINT1 *)(&(x)),\
                                sizeof(UINT4))


#define IS_ONLY_MPLS_LINK_CONSIDERED(u1Flag) \
 (u1Flag & OSPF_TE_MPLS_LINK_BIT_MASK)
#define IS_ONLY_GMPLS_LINK_CONSIDERED(u1Flag) \
 (u1Flag & OSPF_TE_GMPLS_LINK_BIT_MASK)
#define IS_RESOURCE_AFFINITY(u1Flag) \
 (u1Flag & OSPF_TE_RESOURCE_AFFINITY_BIT_MASK)
#define IS_BI_DIRECTION_PATH(u1Flag) \
 (u1Flag & OSPF_TE_BI_DIRECTIONAL_PATH_BIT_MASK)
#define IS_BACKUP_PATH(u1Flag)      \
        (u1Flag & OSPF_TE_BACKUP_PATH_BIT_MASK)
#define IS_SEGMENT_PROTECTION_PATH(u1Flag)      \
        (u1Flag & OSPF_TE_SEGMENT_PROTECTION_PATH_BIT_MASK)
#define IS_MAKE_BEFORE_BREAK_PATH(u1Flag)      \
        (u1Flag & OSPF_TE_MAKE_BEFORE_BREAK_PATH_BIT_MASK)

#define IS_NODE_DISJOINT(u1Flag)     (u1Flag & OSPF_TE_NODE_DISJOINT_BIT_MASK)
#define IS_LINK_DISJOINT(u1Flag)     (u1Flag & OSPF_TE_LINK_DISJOINT_BIT_MASK)
#define IS_SRLG_DISJOINT(u1Flag)     (u1Flag & OSPF_TE_SRLG_DISJOINT_BIT_MASK)

#define IS_VALID_PRIORITY_VALUE(i4Value) ((i4Value >= OSPF_TE_NULL) &&\
                             (i4Value < OSPF_TE_MAX_PRIORITY_LVL))
#define IS_VALID_TE_STATUS_VALUE(i4Value) ((i4Value == OSPF_TE_ENABLED) ||\
                              (i4Value == OSPF_TE_DISABLED))
 
#define IS_OSPF_TE_PSC(u1SwitchingCap) ((u1SwitchingCap == OSPF_TE_PSC_1) ||\
                         (u1SwitchingCap == OSPF_TE_PSC_2) ||\
                         (u1SwitchingCap == OSPF_TE_PSC_3) ||\
                         (u1SwitchingCap == OSPF_TE_PSC_4))


#define ER_OR_AR_HOP_COUNT(pCspfReq) \
    (pCspfReq->osTeAlternateRoute.u2HopCount != OSPF_TE_ZERO ? \
     pCspfReq->osTeAlternateRoute.u2HopCount :  \
     pCspfReq->osTeExpRoute.u2HopCount)

#define ER_OR_AR_ROUTER_ID(pCspfReq, u2Count) \
    (pCspfReq->osTeAlternateRoute.u2HopCount != OSPF_TE_ZERO ? \
     pCspfReq->osTeAlternateRoute.aNextHops[u2Count].u4RouterId : \
     pCspfReq->osTeExpRoute.aNextHops[u2Count].u4RouterId)

#define ER_OR_AR_ROUTE_INFO(pCspfReq) \
    (pCspfReq->osTeAlternateRoute.u2Info != OSPF_TE_ZERO ? \
     pCspfReq->osTeAlternateRoute.u2Info :  \
     pCspfReq->osTeExpRoute.u2Info)

#define ER_OR_AR(pCspfReq) \
    (pCspfReq->osTeAlternateRoute.u2HopCount != OSPF_TE_ZERO ? \
     &(pCspfReq->osTeAlternateRoute) : &(pCspfReq->osTeExpRoute))

#define IS_OSPF_TE_SAME_PATH(pPrimaryPath, pBackupPath)\
                ((pPrimaryPath->aNextHops[OSPF_TE_NULL].u4RouterId ==\
                 pBackupPath->aNextHops[OSPF_TE_NULL].u4RouterId) &&\
                (pPrimaryPath->aNextHops[OSPF_TE_NULL].u4NextHopIpAddr ==\
                 pBackupPath->aNextHops[OSPF_TE_NULL].u4NextHopIpAddr)&&\
                (pPrimaryPath->aNextHops[OSPF_TE_NULL].u4RemoteIdentifier ==\
                 pBackupPath->aNextHops[OSPF_TE_NULL].u4RemoteIdentifier)) 


#define IS_OSPF_TE_BACKUP_PATH_LINK_DISJOINT(pPrimaryPath, pBackupPath, u2PrimaryCount, u2BackupCount)\
        ((pPrimaryPath->aNextHops[u2PrimaryCount].u4RouterId ==\
                pBackupPath->aNextHops[u2BackupCount].u4RouterId) &&\
          (((pPrimaryPath->aNextHops[u2PrimaryCount].u4NextHopIpAddr ==\
                  pBackupPath->aNextHops[u2BackupCount].u4NextHopIpAddr)\
                 && (pBackupPath->aNextHops[u2BackupCount].u4NextHopIpAddr !=\
                     OSPF_TE_NULL)) ||\
                ((pPrimaryPath->aNextHops[u2PrimaryCount].u4RemoteIdentifier ==\
                  pBackupPath->aNextHops[u2BackupCount].u4RemoteIdentifier)\
                 && (pBackupPath->aNextHops[u2BackupCount].u4RemoteIdentifier !=\
                     OSPF_TE_NULL))))

#define IS_OSPF_TE_GMPLS_CONSTRAIN(pCspfReq)\
          ((pCspfReq->u1SwitchingCap != OSPF_TE_NULL) ||\
           (pCspfReq->u1EncodingType != OSPF_TE_NULL) ||\
           (pCspfReq->u1Indication != OSPF_TE_INVALID_INDICATION))


#define IS_MAX_LSP_BW_GREATER_THAN_CSPF_BW(pIfDescr, pCspfReq)\
    ((pIfDescr->aDescMaxLSPBw[pCspfReq->u1Priority] +\
            pCspfReq->oldbandwidth) >= pCspfReq->bandwidth)
  
#define IS_MIN_LSP_BW_LESS_THAN_CSPF_BW(pIfDescr, pCspfReq)\
     (pIfDescr->minLSPBw <= pCspfReq->bandwidth)

#if (MATH_SUPPORT == FSAP_OFF)
#define OSPTE_CEIL(x)  (ceil ((double)x))
#else
#define OSPTE_CEIL(x)  (UtilCeil ((double)x))
#endif

#define IS_MIN_LSP_BW_MULTILE_OF_CSPF_BW(pIfDescr, pCspfReq)\
                    (((tBandWidth)(pCspfReq->bandwidth / pIfDescr->minLSPBw)\
       - ((tBandWidth)OSPTE_CEIL((pCspfReq->bandwidth / pIfDescr->minLSPBw))))\
       == 0)

#define OSPF_TE_CONVERT_BPS_TO_KBPS(f4Val) \
    (UINT4) ((f4Val * 8) / 1000)
#define OSPF_TE_TYPE10_LINK_P2P    1
#define OSPF_TE_TYPE10_LINK_MULTI  2

#endif  /* _OSPF_TE_MACS_H_ */
