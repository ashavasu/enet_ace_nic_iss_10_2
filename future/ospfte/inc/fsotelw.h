/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: fsotelw.h,v 1.3 2008/08/20 15:22:22 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

# include  "osteinc.h"

INT1
nmhGetFutOspfTeAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFutOspfTeTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFutOspfTeCspfRunCnt ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfTeAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFutOspfTeTraceLevel ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfTeAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfTeTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfTeAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfTeTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfTeLsdbTable. */
INT1
nmhValidateIndexInstanceFutOspfTeLsdbTable ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfTeLsdbTable  */

INT1
nmhGetFirstIndexFutOspfTeLsdbTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfTeLsdbTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfTeLsdbChecksum ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfTeLsdbAdvertisement ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FutOspfTeType9LsdbTable. */
INT1
nmhValidateIndexInstanceFutOspfTeType9LsdbTable ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfTeType9LsdbTable  */

INT1
nmhGetFirstIndexFutOspfTeType9LsdbTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfTeType9LsdbTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfTeType9LsdbChecksum ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfTeType9LsdbAdvertisement ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FutOspfTeAreaTable. */
INT1
nmhValidateIndexInstanceFutOspfTeAreaTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfTeAreaTable  */

INT1
nmhGetFirstIndexFutOspfTeAreaTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfTeAreaTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfTeAreaLsaCount ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFutOspfTeType10AreaCksumSum ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFutOspfTeType2AreaCksumSum ARG_LIST((UINT4 ,INT4 *));

/* Proto Validate Index Instance for FutOspfTeIfTable. */
INT1
nmhValidateIndexInstanceFutOspfTeIfTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfTeIfTable  */

INT1
nmhGetFirstIndexFutOspfTeIfTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfTeIfTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfTeIfAreaId ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfTeIfType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfTeIfMetric ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfTeIfMaxBw ARG_LIST((UINT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFutOspfTeIfMaxReservBw ARG_LIST((UINT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFutOspfTeIfRsrcClassColor ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfTeIfOperStat ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfTeIfLinkId ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfTeIfRemoteIpAddr ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFutOspfTeIfProtectionType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFutOspfTeIfSrlg ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FutOspfTeIfDescriptorTable. */
INT1
nmhValidateIndexInstanceFutOspfTeIfDescriptorTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfTeIfDescriptorTable  */

INT1
nmhGetFirstIndexFutOspfTeIfDescriptorTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfTeIfDescriptorTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfTeIfDescrSwithingCap ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfTeIfDescrEncodingType ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfTeIfDescrMinLSPBandwidth ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFutOspfTeIfDescrMTU ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfTeIfDescrIndication ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FutOspfTeIfSwDescrMaxBwTable. */
INT1
nmhValidateIndexInstanceFutOspfTeIfSwDescrMaxBwTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfTeIfSwDescrMaxBwTable  */

INT1
nmhGetFirstIndexFutOspfTeIfSwDescrMaxBwTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfTeIfSwDescrMaxBwTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfTeIfSwDescrMaxLSPBandwidth ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for FutOspfTeIfBandwidthTable. */
INT1
nmhValidateIndexInstanceFutOspfTeIfBandwidthTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfTeIfBandwidthTable  */

INT1
nmhGetFirstIndexFutOspfTeIfBandwidthTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfTeIfBandwidthTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfTeIfUnreservedBandwidth ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
