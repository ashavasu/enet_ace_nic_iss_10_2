/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: osteport.h,v 1.5 2013/04/17 12:15:12 siva Exp $
*
* Description: This file contains definitions of portable constants.
*********************************************************************/

#ifndef _OSPF_TE_PORT_H_
#define _OSPF_TE_PORT_H_

#define OSPF_TE_TASK_NAME              "OSTE"
#define OSPF_TE_TASK_PRIORITY          50

#define OSPF_TE_Q_NAME                (const  UINT1 *) "OSTE"
#define OSPF_TE_Q_DEPTH                20

#define OSPF_TE_OSIX_WAIT              OSIX_WAIT
#define OSPF_TE_OSIX_WAIT_TIME         0

#define OSPF_TE_MUT_EXCL_SEM_NAME (const UINT1 *) "TMES"
#define OSPF_TE_SEM_CREATE_INIT_CNT        1

#define OSPF_TE_IF_HASH_TBL_SIZE       9
#define OSPF_TE_IF_HASH_KEY_SIZE       8

#define OSPF_TE_CSPF_HASH_KEY_SIZE     5
#define OSPF_TE_LSA_HASH_TBL_SIZE      5
#define OSPF_TE_LSA_HASH_KEY_SIZE      4
#define OSPF_TE_CAND_HASH_TBL_SIZE     5


#define OSPF_TE_MAX_AREAS              5
#define OSPF_TE_MAX_IF                 10
#define OSPF_TE_MAX_IF_DESC            3

#define OSPF_TE_MAX_CSPF_REQUEST       1
#define OSPF_TE_MAX_CSPF_CONS          1
#define OSPF_TE_MAX_LSAS               800
#define OSPF_TE_LSA_BLOCK_SIZE         512


#define OSPF_TE_LINK_TLV_MAX_SIZE      (4 + 120 + 4 + OSPF_TE_MAX_SRLG_NO * 4 \
                          + (4 + 44) * OSPF_TE_MAX_IF_DESC)

#define OSPF_TE_MEMSET                 MEMSET
#define OSPF_TE_MEMCPY                 MEMCPY
#define OSPF_TE_MEMCMP                 MEMCMP

#define OSPF_TE_CRU_BMC_WFROMPDU(pu1PduAddr) \
        OSIX_NTOHS(*((UINT2 *)(VOID *) (pu1PduAddr)))

#define OSPF_TE_CRU_BMC_DWFROMPDU(pu1PduAddr) \
        OspfTeUtilDwordFromPdu (pu1PduAddr)


#define OSPF_TE_CRU_BMC_WTOPDU(pu1PduAddr,u2Value) \
         *((UINT2 *)(VOID *) (pu1PduAddr)) = OSIX_HTONS(u2Value);

#define OSPF_TE_CRU_BMC_DWTOPDU(pu1PduAddr,u4Value) \
        OspfTeUtilDwordToPdu (pu1PduAddr,u4Value)        

#endif  /* _OSPF_TE_PORT_H_ */
