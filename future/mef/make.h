#
# Copyright (C) 2010 Aricent Inc . All Rights Reserved
# $Id: make.h,v 1.8 2013/11/29 11:10:11 siva Exp $            
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 20 MARCH  2012                                |
# |                                                                          |
# |   DESCRIPTION            : This file contains all the warning options    |
# |                            that are used for building this module        |
# +--------------------------------------------------------------------------+


#Project base directories
PROJECT_NAME            = FutureMEF
PROJECT_BASE_DIR        = ${BASE_DIR}/mef
PROJECT_INC_DIR         = $(PROJECT_BASE_DIR)/inc
MEF_MAKE_H              = make.h
MEF_MAKEFILE            = Makefile

PROJECT_OBJECT_DIR     = $(PROJECT_BASE_DIR)/obj
PROJECT_SOURCE_DIR     = $(PROJECT_BASE_DIR)/src

PROJECT_FINAL_OBJ  = $(PROJECT_NAME).o
MEF_INCLUDE_DIRS = -I$(PROJECT_INC_DIR)


# List of include files of MEF

MEF_INC_FILES = $(PROJECT_INC_DIR)/mefextn.h \
                $(PROJECT_INC_DIR)/fsmefwr.h \
                $(PROJECT_INC_DIR)/fsmeflw.h \
                $(PROJECT_INC_DIR)/fsmefdef.h\
                $(PROJECT_INC_DIR)/fsmefglb.h\
                $(PROJECT_INC_DIR)/fsmefinc.h\
                $(PROJECT_INC_DIR)/fsmefpro.h\
                $(PROJECT_INC_DIR)/fsmeftdf.h\
                $(PROJECT_INC_DIR)/mefextn.h \
                $(PROJECT_INC_DIR)/mefsz.h   \
                $(PROJECT_INC_DIR)/fsmefdb.h \
                $(PROJECT_INC_DIR)/fsmeftrc.h 


MEF_SRC_FILES = $(PROJECT_SOURCE_DIR)/fsmefwr.c  \
                $(PROJECT_SOURCE_DIR)/fsmeflw.c  \
                $(PROJECT_SOURCE_DIR)/fsmefini.c \
                $(PROJECT_SOURCE_DIR)/fsmefutl.c \
                $(PROJECT_SOURCE_DIR)/mefsz.c    \
                $(PROJECT_SOURCE_DIR)/fsmefapi.c \
                $(PROJECT_SOURCE_DIR)/fsmefque.c \
                $(PROJECT_SOURCE_DIR)/fsmefcxt.c \
                $(PROJECT_SOURCE_DIR)/fsmeftrc.c \
                $(PROJECT_SOURCE_DIR)/fsmefcli.c \
                $(PROJECT_SOURCE_DIR)/fsmefsrc.c 

MEF_OBJ_LIST = $(PROJECT_OBJECT_DIR)/fsmefini.o \
               $(PROJECT_OBJECT_DIR)/fsmefutl.o \
               $(PROJECT_OBJECT_DIR)/fsmeflw.o  \
               $(PROJECT_OBJECT_DIR)/fsmefapi.o \
               $(PROJECT_OBJECT_DIR)/mefsz.o    \
               $(PROJECT_OBJECT_DIR)/fsmefque.o \
               $(PROJECT_OBJECT_DIR)/fsmefcxt.o \
               $(PROJECT_OBJECT_DIR)/fsmeftrc.o \
               $(PROJECT_OBJECT_DIR)/fsmefsrc.o

ifeq (${SNMP_2}, YES)
MEF_OBJ_LIST +=   ${PROJECT_OBJECT_DIR}/fsmefwr.o
endif

ifeq (${CLI}, YES)
MEF_OBJ_LIST +=   $(PROJECT_OBJECT_DIR)/fsmefcli.o
endif

CLI_INC_DIR = $(BASE_DIR)/inc/cli
MEF_CLI_INC_FILES = $(CLI_INC_DIR)/fsmefcli.h

PROJECT_FINAL_INCLUDES_DIRS   = $(MEF_INCLUDE_DIRS) \
                                $(COMMON_INCLUDE_DIRS)

PROJECT_OBJECT_LIST = $(MEF_OBJ_LIST)

#Dependencies
PROJECT_COMMON_DEPENDENCIES =   $(MEF_INC_FILES)\
                                $(MEF_CLI_INC_FILES)

PROJECT_DEPENDENCIES        =   $(PROJECT_COMMON_DEPENDENCIES) \
                                $(COMMON_DEPENDENCIES)


PROJECT_COMPILATION_SWITCHES =-DDEBUG_WANTED 
PROJECT_COMPILATION_SWITCHES += $(PROJECT_DEF_COMPILATION_SWITCHES)

ifeq (DMEF_TEST_WANTED, $(findstring DMEF_TEST_WANTED,$(PROJECT_COMPILATION_SWITCHES)))
MEF_TEST_BASE_DIR  = ${BASE_DIR}/mef/test
MEF_TEST_OBJ_DIR  = ${BASE_DIR}/mef/test/obj
MEF_OBJ_LIST += ${MEF_TEST_OBJ_DIR}/FutureMefTest.o
endif
