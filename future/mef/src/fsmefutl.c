/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmefutl.c,v 1.45 2017/09/22 12:25:59 siva Exp $
 *
 * Description:
 *********************************************************************/
#ifndef __FSMEF_UTL_C__
#define __FSMEF_UTL_C__

#include  "fsmefinc.h"
#include  "vlnpbcli.h"

extern UINT4        FsUniCVlanEvcRowStatus[13];

PUBLIC tIssBool     MsrisRetoreinProgress;

/*****************************************************************************/
/*    Function Name       : FsMefUtilGetTransportMode                        */
/*                                                                           */
/*    Description         : This function retuns the transport mode of the   */
/*                          given context                                    */
/*                                                                           */
/*    Input(s)            : u4FsMefContextId - Context Id.                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : eMefTransportMode - MEF_TRANS_MODE_PB,           */
/*                                              MEF_TRANS_MODE_MPLS          */
/*****************************************************************************/
eMefTransportMode
FsMefUtilGetTransportMode (UINT4 u4FsMefContextId)
{
    return gMefGlobalInfo.apContextInfo[u4FsMefContextId]->eMefTransportMode;
}

/*****************************************************************************/
/*    Function Name       : MefInitMefUniInfo                                */
/*                                                                           */
/*    Description         : This function set the default value of UNI.      */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : i4IfIndex - Interfcae index                      */
/*                          pMefUniInfo - pointer of UNI node.               */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
MefInitDefaultUniInfo (INT4 i4IfIndex, tMefUniInfo * pMefUniInfo)
{

    UINT4               u4ContextId = 0;
    UINT4               u4BridgeMode = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1TransModeFlag = 0;

    VcmGetContextInfoFromIfIndex ((UINT4) i4IfIndex, &u4ContextId,
                                  &u2LocalPort);
    L2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);
    u1TransModeFlag = FsMefUtilGetTransportMode (u4ContextId);

    MEMSET (pMefUniInfo, 0, sizeof (tMefUniInfo));
    pMefUniInfo->i4IfIndex = i4IfIndex;
    pMefUniInfo->bMultiplexing = MEF_ENABLED;
    pMefUniInfo->u2DefaultCeVlanId = 1;
    pMefUniInfo->bBundling = MEF_ENABLED;
    pMefUniInfo->bAllToOneBundling = MEF_DISABLED;
    pMefUniInfo->u1Dot1xTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
    pMefUniInfo->u1LacpTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
    pMefUniInfo->u1StpTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;

    if ((u1TransModeFlag == MEF_TRANS_MODE_MPLS) &&
        (u4BridgeMode == MEF_CUSTOMER_BRIDGE_MODE))
    {
        pMefUniInfo->u1GvrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
        pMefUniInfo->u1MvrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
        pMefUniInfo->u1GmrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
        pMefUniInfo->u1MmrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
    }
    else
    {
        pMefUniInfo->u1GvrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
        pMefUniInfo->u1MvrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
        pMefUniInfo->u1GmrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
        pMefUniInfo->u1MmrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    pMefUniInfo->u1ElmiTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
    pMefUniInfo->u1LldpTunnelingStatus = MEF_TUNNEL_PROTOCOL_DISCARD;
    pMefUniInfo->u1EcfmTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
    pMefUniInfo->u4UniOverride = MEF_UNI_OVERRIDE_DISABLE;
    pMefUniInfo->u1EoamTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
}

/*****************************************************************************/
/*    Function Name       : MefCheckAndVerifyUntaggedCepPep                  */
/*                                                                           */
/*    Description         : This function set untagged-cep to TRUE to handle */
/*                          untagged and priority tagged packets are         */
/*                          mapped to a C-Vlan when Preservation = No        */
/*                                                                           */
/*    Input(s)            : u4FsMefContextId - Context Id                    */
/*                          i4IfIndex - Interfcae index                      */
/*                          u2CVlanId - Customer Vlan Id                     */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS/SNMP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT1
MefCheckAndVerifyUntaggedCepPep (UINT4 u4FsMefContextId, INT4 i4IfIndex,
                                 UINT2 u2CVlanId)
{
    INT4                i4UntaggedPep = 0;
    INT4                i4UntaggedCep = 0;

    if (VlanSelectContext (u4FsMefContextId) == VLAN_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (nmhGetDot1adMICVidRegistrationUntaggedPep (i4IfIndex, u2CVlanId,
                                                   &i4UntaggedPep) ==
        SNMP_FAILURE)
    {
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    if (nmhGetDot1adMICVidRegistrationUntaggedCep (i4IfIndex, u2CVlanId,
                                                   &i4UntaggedCep) ==
        SNMP_FAILURE)
    {
        VlanReleaseContext ();
        return SNMP_SUCCESS;
    }

    if ((i4UntaggedPep == VLAN_SNMP_TRUE) && (i4UntaggedCep != VLAN_SNMP_TRUE))
    {
        /* Preservation = No */
        /* Since the untagged and priority tagged packets are
         * mapped to a C-Vlan so untagged-cep should be set to TRUE.
         * if it is not set already.
         */
        if (nmhSetDot1adMICVidRegistrationUntaggedCep (i4IfIndex, u2CVlanId,
                                                       VLAN_SNMP_TRUE) ==
            SNMP_SUCCESS)
        {
            VlanReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    VlanReleaseContext ();
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : MefSetUniInfoEntry                               */
/*                                                                           */
/*    Description         : It will set the Value of UNI                     */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pMefUniInfo - pointer to UNI Node                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS/OSIX_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
MefSetUniInfoEntry (tMefUniInfo * pMefUniInfo)
{
    UINT2               u2FsMefLocalPort = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4IfIndex = 0;
    INT4                i4Bundle = 0;
    INT4                i4Multiplex = 0;
    INT4                i4AllToOneBundle = 0;
#ifdef MPLS_WANTED
    INT4                i4RowStatus = 0;
#endif
    UINT4               u4FsMefContextId = 0;
    tSNMP_OCTET_STRING_TYPE MefUniId;
    MEMSET (&MefUniId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MefUniId.pu1_OctetList = pMefUniInfo->au1UniId;
    MefUniId.i4_Length = STRLEN (pMefUniInfo->au1UniId);

    i4IfIndex = pMefUniInfo->i4IfIndex;

    if (MefGetContexId (i4IfIndex, &u4FsMefContextId,
                        &u2FsMefLocalPort) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /*this is used to setting service type in UNI on PB and MPLS both ENABLE 
     * should be 1 and DISABLE should be 2*/
    i4Bundle = (pMefUniInfo->bBundling == MEF_ENABLED) ?
        VLAN_ENABLED : VLAN_DISABLED;

    i4Multiplex = (pMefUniInfo->bMultiplexing == MEF_ENABLED) ?
        VLAN_ENABLED : VLAN_DISABLED;

    i4AllToOneBundle = (pMefUniInfo->bAllToOneBundling == MEF_ENABLED) ?
        VLAN_ENABLED : VLAN_DISABLED;

    if (FsMefUtilGetTransportMode (u4FsMefContextId) == MEF_TRANS_MODE_PB)
    {

        if (pMefUniInfo->bAllToOneBundling == MEF_ENABLED)
        {
            if (nmhTestv2IfMainBrgPortType (&u4ErrorCode, i4IfIndex,
                                            MEF_CNP_PORTBASED_PORT) ==
                SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            if (CfaSetIfMainBrgPortType (i4IfIndex, MEF_CNP_PORTBASED_PORT)
                == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

        }
        else
        {
            if (nmhTestv2IfMainBrgPortType (&u4ErrorCode, i4IfIndex,
                                            MEF_CUSTOMER_EDGE_PORT) ==
                SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            if (CfaSetIfMainBrgPortType (i4IfIndex, MEF_CUSTOMER_EDGE_PORT)
                == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            VLAN_LOCK ();
            if (nmhTestv2FsMIPbPortBundleStatus (&u4ErrorCode, i4IfIndex,
                                                 i4Bundle) == SNMP_FAILURE)
            {
                VLAN_UNLOCK ();
                return OSIX_FAILURE;
            }

            if (nmhTestv2FsMIPbPortMultiplexStatus (&u4ErrorCode, i4IfIndex,
                                                    i4Multiplex) ==
                SNMP_FAILURE)
            {
                VLAN_UNLOCK ();
                return OSIX_FAILURE;
            }

            if (nmhTestv2FsMIPbPortCVlan (&u4ErrorCode, i4IfIndex,
                                          pMefUniInfo->u2DefaultCeVlanId)
                == SNMP_FAILURE)
            {
                VLAN_UNLOCK ();
                return OSIX_FAILURE;
            }

            if (nmhSetFsMIPbPortCVlan
                (i4IfIndex, pMefUniInfo->u2DefaultCeVlanId) == SNMP_FAILURE)
            {
                VLAN_UNLOCK ();
                return OSIX_FAILURE;
            }

            VLAN_UNLOCK ();
        }
    }
    else
    {
#ifdef MPLS_WANTED
        VLAN_LOCK ();
        if (nmhTestv2FsDot1qPvid (&u4ErrorCode, i4IfIndex,
                                  pMefUniInfo->u2DefaultCeVlanId) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }
        VLAN_UNLOCK ();

        MPLS_L2VPN_LOCK ();

        if (nmhGetFsMplsPortRowStatus (i4IfIndex, &i4RowStatus) != SNMP_SUCCESS)
        {
            if (nmhTestv2FsMplsPortRowStatus (&u4ErrorCode, i4IfIndex,
                                              CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return OSIX_FAILURE;
            }

            if (nmhSetFsMplsPortRowStatus (i4IfIndex, CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return OSIX_FAILURE;
            }

            if (nmhTestv2FsMplsPortBundleStatus (&u4ErrorCode, i4IfIndex,
                                                 i4Bundle) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return OSIX_FAILURE;
            }

            if (nmhTestv2FsMplsPortMultiplexStatus (&u4ErrorCode, i4IfIndex,
                                                    i4Multiplex) ==
                SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return OSIX_FAILURE;
            }

            if (nmhTestv2FsMplsPortAllToOneBundleStatus
                (&u4ErrorCode, i4IfIndex, i4AllToOneBundle) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return OSIX_FAILURE;
            }
        }
        MPLS_L2VPN_UNLOCK ();
#endif
    }

    if (nmhTestv2IfMainDesc (&u4ErrorCode,
                             i4IfIndex, &MefUniId) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    VLAN_LOCK ();
    if (nmhTestv2FsMIVlanTunnelProtocolDot1x
        (&u4ErrorCode, i4IfIndex,
         pMefUniInfo->u1Dot1xTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIVlanTunnelProtocolLacp
        (&u4ErrorCode, i4IfIndex,
         pMefUniInfo->u1LacpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIVlanTunnelProtocolStp
        (&u4ErrorCode, i4IfIndex,
         pMefUniInfo->u1StpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIVlanTunnelProtocolGvrp
        (&u4ErrorCode, i4IfIndex,
         pMefUniInfo->u1GvrpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIVlanTunnelProtocolMvrp
        (&u4ErrorCode, i4IfIndex,
         pMefUniInfo->u1MvrpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIVlanTunnelProtocolGmrp
        (&u4ErrorCode, i4IfIndex,
         pMefUniInfo->u1GmrpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIVlanTunnelProtocolMmrp
        (&u4ErrorCode, i4IfIndex,
         pMefUniInfo->u1MmrpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIVlanTunnelProtocolElmi
        (&u4ErrorCode, i4IfIndex,
         pMefUniInfo->u1ElmiTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIVlanTunnelProtocolLldp
        (&u4ErrorCode, i4IfIndex,
         pMefUniInfo->u1LldpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIVlanTunnelProtocolEcfm
        (&u4ErrorCode, i4IfIndex,
         pMefUniInfo->u1EcfmTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIVlanTunnelOverrideOption
        (&u4ErrorCode, i4IfIndex, pMefUniInfo->u4UniOverride) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIVlanTunnelProtocolEoam
        (&u4ErrorCode, i4IfIndex,
         pMefUniInfo->u1EoamTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    VLAN_UNLOCK ();

    if (FsMefUtilGetTransportMode (u4FsMefContextId) == MEF_TRANS_MODE_PB)
    {
        VLAN_LOCK ();
        if (nmhSetFsMIPbPortBundleStatus (i4IfIndex, i4Bundle) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (nmhSetFsMIPbPortMultiplexStatus (i4IfIndex, i4Multiplex)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }
        VLAN_UNLOCK ();
    }
    else
    {
#ifdef MPLS_WANTED
        VLAN_LOCK ();
        if (nmhSetFsDot1qPvid (i4IfIndex,
                               pMefUniInfo->u2DefaultCeVlanId) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }
        VLAN_UNLOCK ();

        MPLS_L2VPN_LOCK ();
        if (i4RowStatus != ACTIVE)
        {
            if (nmhSetFsMplsPortBundleStatus (i4IfIndex, i4Bundle) ==
                SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return OSIX_FAILURE;
            }

            if (nmhSetFsMplsPortMultiplexStatus (i4IfIndex,
                                                 i4Multiplex) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return OSIX_FAILURE;
            }

            if (nmhSetFsMplsPortAllToOneBundleStatus (i4IfIndex,
                                                      i4AllToOneBundle) ==
                SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return OSIX_FAILURE;
            }

            if (nmhTestv2FsMplsPortRowStatus (&u4ErrorCode, i4IfIndex,
                                              ACTIVE) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return OSIX_FAILURE;
            }

            if (nmhSetFsMplsPortRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return OSIX_FAILURE;
            }
        }
        MPLS_L2VPN_UNLOCK ();
#endif
    }
    if (nmhSetIfMainDesc (i4IfIndex, &MefUniId) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    VLAN_LOCK ();
    if (nmhSetFsMIVlanTunnelProtocolDot1x
        (i4IfIndex, pMefUniInfo->u1Dot1xTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIVlanTunnelProtocolLacp
        (i4IfIndex, pMefUniInfo->u1LacpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIVlanTunnelProtocolStp
        (i4IfIndex, pMefUniInfo->u1StpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIVlanTunnelProtocolGvrp
        (i4IfIndex, pMefUniInfo->u1GvrpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIVlanTunnelProtocolGmrp
        (i4IfIndex, pMefUniInfo->u1GmrpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIVlanTunnelProtocolMvrp
        (i4IfIndex, pMefUniInfo->u1MvrpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIVlanTunnelProtocolMmrp
        (i4IfIndex, pMefUniInfo->u1MmrpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIVlanTunnelProtocolLldp
        (i4IfIndex, pMefUniInfo->u1LldpTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIVlanTunnelProtocolElmi
        (i4IfIndex, pMefUniInfo->u1ElmiTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIVlanTunnelProtocolEcfm
        (i4IfIndex, pMefUniInfo->u1EcfmTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIVlanTunnelOverrideOption
        (i4IfIndex, pMefUniInfo->u4UniOverride) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIVlanTunnelProtocolEoam
        (i4IfIndex, pMefUniInfo->u1EoamTunnelingStatus) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }
    /* Untagged CEP and Untagged PEP as FALSE at a time is not supported in certain Target */
    if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_UNTAG_CEP_PEP))
    {
        if (MefCheckAndVerifyUntaggedCepPep (u4FsMefContextId, i4IfIndex,
                                             pMefUniInfo->u2DefaultCeVlanId) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }
    }
    VLAN_UNLOCK ();

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefResetUniInfoEntry
 *
 * DESCRIPTION      : This function Reset the Uni table.
 *
 * INPUT            : i4IfIndex - Interface Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefResetUniInfoEntry (INT4 i4IfIndex)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4FsMefContextId = 0;
    UINT2               u2FsMefLocalPort = 0;
    UINT1               au1Desc[UNI_ID_MAX_LENGTH];
    tSNMP_OCTET_STRING_TYPE MefUniId;

    MEMSET (au1Desc, 0, UNI_ID_MAX_LENGTH);
    MEMSET (&MefUniId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MefUniId.pu1_OctetList = (UINT1 *) &au1Desc;
    MefUniId.i4_Length = STRLEN (&au1Desc);
    if (MefGetContexId (i4IfIndex, &u4FsMefContextId, &u2FsMefLocalPort)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2IfMainDesc (&u4ErrorCode,
                             i4IfIndex, &MefUniId) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetIfMainDesc (i4IfIndex, &MefUniId) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (FsMefUtilGetTransportMode (u4FsMefContextId) == MEF_TRANS_MODE_PB)
    {
        if (MefDeleteCvlanEvcMapEntries (i4IfIndex) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
#ifdef MPLS_WANTED
        VLAN_LOCK ();
        if (nmhTestv2FsDot1qPvid (&u4ErrorCode,
                                  i4IfIndex, MEF_DEFAULT_VLAN) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }
        VLAN_UNLOCK ();

        MPLS_L2VPN_LOCK ();
        if (nmhTestv2FsMplsPortRowStatus (&u4ErrorCode, i4IfIndex,
                                          DESTROY) != SNMP_FAILURE)
        {
            nmhSetFsMplsPortRowStatus (i4IfIndex, DESTROY);
        }
        MPLS_L2VPN_UNLOCK ();

        VLAN_LOCK ();
        if (nmhSetFsDot1qPvid (i4IfIndex, MEF_DEFAULT_VLAN) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }
        VLAN_UNLOCK ();
#endif
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefUniCreateTable
 *
 * DESCRIPTION      : This function creates the Uni table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefUniCreateTable (VOID)
{
    MEF_UNI_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMefUniInfo, MefUniRbNode),
                              MefUniEnrtyRBTreeCmp);
    if (MEF_UNI_TABLE == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefUniDeleteTable
 *
 * DESCRIPTION      : This function deletes the MefUni table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
VOID
MefUniDeleteTable (VOID)
{
    tMefUniInfo        *pMefUniInfo = NULL;
    pMefUniInfo = RBTreeGetFirst (MEF_UNI_TABLE);

    while (pMefUniInfo != NULL)
    {
        RBTreeRemove (MEF_UNI_TABLE, pMefUniInfo);

        MemReleaseMemBlock (MEF_UNI_POOL_ID, (UINT1 *) pMefUniInfo);

        pMefUniInfo = RBTreeGetFirst (MEF_UNI_TABLE);
    }

    RBTreeDestroy (MEF_UNI_TABLE, NULL, 0);
    MEF_UNI_TABLE = NULL;

    return;
}

/************************************************************************
 *  Function Name   : MefUniEnrtyRBTreeCmp
 *  Description     : RBTree Compare function for CFA Index entry.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 *************************************************************************/
INT4
MefUniEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMefUniInfo        *pMefUniInfoA = NULL;
    tMefUniInfo        *pMefUniInfoB = NULL;

    pMefUniInfoA = (tMefUniInfo *) pRBElem1;
    pMefUniInfoB = (tMefUniInfo *) pRBElem2;

    if ((pMefUniInfoA->i4IfIndex) < (pMefUniInfoB->i4IfIndex))
    {
        return -1;
    }
    else if ((pMefUniInfoA->i4IfIndex) > (pMefUniInfoB->i4IfIndex))
    {
        return 1;
    }

    return 0;

}

/***************************************************************************
 * FUNCTION NAME    : MefAddUniEntry
 *
 * DESCRIPTION      : This function add a MefUniAddNode node to the MEF
 *                    UNI table.
 *
 * INPUT            : pMefUniInfo - pointer to UNI node information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefAddUniEntry (tMefUniInfo * pMefUniInfo)
{

    if (RBTreeAdd (MEF_UNI_TABLE, pMefUniInfo) != RB_SUCCESS)
    {
        MemReleaseMemBlock (MEF_UNI_POOL_ID, (UINT1 *) pMefUniInfo);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeletUniEntry
 *
 * DESCRIPTION      : This function delete a tMefUniInfo  node from the
 *                    MEF UNI table.
 *
 * INPUT            : i4IfIndex - Interface Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeleteUniEntry (INT4 i4IfIndex)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (RBTreeRemove (MEF_UNI_TABLE, pMefUniInfo) != RB_FAILURE)
    {
        MemReleaseMemBlock (MEF_UNI_POOL_ID, (UINT1 *) pMefUniInfo);
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefGetUniEntry
 *
 * DESCRIPTION      : This function Gets a tMefUniInfo node from the
 *                    MEF UNI table.
 *
 * INPUT            : i4IfIndex - Interface Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tMefUniInfo - pointer to pseudowire IfIndex Map  entry
 *                    ELSE NULL
 **************************************************************************/
tMefUniInfo        *
MefGetUniEntry (INT4 i4IfIndex)
{
    tMefUniInfo         MefUniInfo;
    tMefUniInfo        *pMefUniInfo = NULL;

    MEMSET (&MefUniInfo, 0, sizeof (tMefUniInfo));
    MefUniInfo.i4IfIndex = i4IfIndex;
    pMefUniInfo = RBTreeGet (MEF_UNI_TABLE, &MefUniInfo);

    return (pMefUniInfo);
}

/***************************************************************************
 * FUNCTION NAME    : MefGetContexId
 *
 * DESCRIPTION      : This function Gets Context Id and local port.
 *                    
 *
 * INPUT            : i4IfIndex - Interface Index 
 *
 * OUTPUT           : pu4ContextId - context Id
                      pu2LocalPortId - Local Port ID  
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                    
 **************************************************************************/
INT4
MefGetContexId (INT4 i4IfIndex, UINT4 *u4FsMefContextId,
                UINT2 *u2FsMefLocalPort)
{

    if (VcmGetContextInfoFromIfIndex ((UINT4) i4IfIndex, u4FsMefContextId,
                                      u2FsMefLocalPort) == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefEvcCreateTable
 *
 * DESCRIPTION      : This function creates the Evc table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefEvcCreateTable (VOID)
{
    MEF_EVC_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMefEvcInfo, MefEvcRbNode),
                              MefEvcEnrtyRBTreeCmp);
    if (MEF_EVC_TABLE == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefGetEvcEntry
 *
 * DESCRIPTION      : This function returns tMefUniInfo node from the
 *                    MEF EVC table for the given EvcContextId and EvcVlanId.
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Vlan Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : tMefEvcInfo - pointer to pseudowire IfIndex Map  entry
 *                    ELSE NULL
 **************************************************************************/
tMefEvcInfo        *
MefGetEvcEntry (INT4 i4FsEvcContextId, INT4 i4FsEvcVlanId)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    tMefEvcInfo         MefEvcInfo;

    MEMSET (&MefEvcInfo, 0, sizeof (tMefEvcInfo));
    MefEvcInfo.u4EvcContextId = i4FsEvcContextId;
    MefEvcInfo.i4EvcVlanId = i4FsEvcVlanId;

    pMefEvcInfo = (tMefEvcInfo *) RBTreeGet (MEF_EVC_TABLE, &MefEvcInfo);
    if (pMefEvcInfo == NULL)
    {
        return (pMefEvcInfo);
    }

    return (pMefEvcInfo);
}

/*****************************************************************************/
/*    Function Name       : MefInitMefEvcInfo                                */
/*                                                                           */
/*    Description         : This function set the default value of Evc.      */
/*                                                                           */
/*    Input(s)            : i4FsEvcContextId - Context Id                    */
/*                          i4FsEvcVlanId - Vlan Id                          */
/*    Output(s)           : pMefEvcInfo - Pointde to the given entry         */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
MefInitDefaultEvcInfo (INT4 i4FsEvcContextId,
                       INT4 i4FsEvcVlanId, tMefEvcInfo * pMefEvcInfo)
{
    MEMSET (pMefEvcInfo, 0, sizeof (tMefEvcInfo));

    pMefEvcInfo->u4EvcContextId = i4FsEvcContextId;
    pMefEvcInfo->i4EvcVlanId = i4FsEvcVlanId;
    pMefEvcInfo->EvcType = MEF_MULTIPOINT_TO_MULTIPOINT;
    pMefEvcInfo->i4EvcMaxUni = 2;
    pMefEvcInfo->bCeVlanIdPreservation = MEF_ENABLED;
    pMefEvcInfo->bCeVlanCoSPreservation = MEF_ENABLED;
    pMefEvcInfo->i4EvcMtu = MEF_DEFAULT_EVC_MTU;
    MEF_EVC_DOT1X_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    MEF_EVC_LACP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    MEF_EVC_STP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    MEF_EVC_GVRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    MEF_EVC_MVRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    MEF_EVC_GMRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    MEF_EVC_MMRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    MEF_EVC_IGMP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    MEF_EVC_ELMI_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    MEF_EVC_LLDP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    MEF_EVC_PTP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    MEF_EVC_EOAM_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    pMefEvcInfo->u1LoopbackStatus = MEF_DISABLED;
}

/***************************************************************************
 * FUNCTION NAME    : MefAddEvcEntry
 *
 * DESCRIPTION      : This function add a MefEvcAddNode node to the MEF
 *                    Evc table.
 *
 * INPUT            : pMefEvcInfo - pointer to Evc node information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefAddEvcEntry (tMefEvcInfo * pMefEvcInfo)
{

    if (RBTreeAdd (MEF_EVC_TABLE, pMefEvcInfo) != RB_SUCCESS)
    {
        MemReleaseMemBlock (MEF_EVC_POOL_ID, (UINT1 *) pMefEvcInfo);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeletEvcEntry
 *
 * DESCRIPTION      : This function delete a tMefEvcInfo  node from the
 *                    MEF EVC table.
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Valn Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeleteEvcEntry (tMefEvcInfo * pMefEvcInfo)
{
    tMefEvcFilterInfo   MefEvcFilterInfo;
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;
    UINT4               u4Error = 0;

    MefEvcFilterInfo.u4EvcFilterContextId = pMefEvcInfo->u4EvcContextId;
    MefEvcFilterInfo.i4EvcFilterVlanId = pMefEvcInfo->i4EvcVlanId;
    MefEvcFilterInfo.i4EvcFilterInstance = 0;

    if (pMefEvcInfo->EvcType == MEF_POINT_TO_POINT)
    {
        VLAN_LOCK ();
        if (nmhTestv2FsMIPbSVlanConfigServiceType
            (&u4Error, pMefEvcInfo->u4EvcContextId,
             (UINT4) pMefEvcInfo->i4EvcVlanId,
             MEF_MULTIPOINT_TO_MULTIPOINT) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (nmhSetFsMIPbSVlanConfigServiceType (pMefEvcInfo->u4EvcContextId,
                                                (UINT4)
                                                pMefEvcInfo->i4EvcVlanId,
                                                MEF_MULTIPOINT_TO_MULTIPOINT) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        VLAN_UNLOCK ();
    }

    /* Walk through the EVC filter table entries delete all the
     * Configuration when the EVC table row status is set to DESTROY */

    pMefEvcFilterInfo =
        (tMefEvcFilterInfo *) RBTreeGetNext (MEF_EVC_FILTER_TABLE,
                                             &MefEvcFilterInfo, NULL);
    while ((pMefEvcFilterInfo != NULL) &&
           (pMefEvcFilterInfo->u4EvcFilterContextId ==
            pMefEvcInfo->u4EvcContextId) &&
           (pMefEvcFilterInfo->i4EvcFilterVlanId == pMefEvcInfo->i4EvcVlanId))
    {
        if (nmhTestv2IssExtL2FilterStatus (&u4Error,
                                           pMefEvcFilterInfo->i4EvcFilterId,
                                           DESTROY) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetIssExtL2FilterStatus (pMefEvcFilterInfo->i4EvcFilterId,
                                        DESTROY) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        /* Delete the EVC Filter entry */
        RBTreeRemove (MEF_EVC_FILTER_TABLE, pMefEvcFilterInfo);
        MemReleaseMemBlock (MEF_EVC_FILTER_POOL_ID,
                            (UINT1 *) pMefEvcFilterInfo);

        pMefEvcFilterInfo =
            RBTreeGetNext (MEF_EVC_FILTER_TABLE, &MefEvcFilterInfo, NULL);
    }

    if (RBTreeRemove (MEF_EVC_TABLE, pMefEvcInfo) != RB_FAILURE)
    {
        MemReleaseMemBlock (MEF_EVC_POOL_ID, (UINT1 *) pMefEvcInfo);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : MefDeleteAllEvcEntries                           */
/*                                                                           */
/*    Description         : This function will delete all the EVC entries of */
/*                          the given context.                               */
/*                                                                           */
/*    Input(s)            : u4ContextId                                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS                                     */
/*                          OSIX_FAILURE                                     */
/*****************************************************************************/
INT4
MefDeleteAllEvcEntries (UINT4 u4ContextId)
{
    tMefEvcInfo         MefEvcInfo;
    tMefEvcInfo        *pMefEvcInfo = NULL;

    MefEvcInfo.u4EvcContextId = u4ContextId;
    MefEvcInfo.i4EvcVlanId = 0;

    pMefEvcInfo =
        (tMefEvcInfo *) RBTreeGetNext (MEF_EVC_TABLE, &MefEvcInfo, NULL);

    while ((pMefEvcInfo != NULL) &&
           (pMefEvcInfo->u4EvcContextId == u4ContextId))
    {
        if (MefDeleteEvcEntry (pMefEvcInfo) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }

        pMefEvcInfo =
            (tMefEvcInfo *) RBTreeGetNext (MEF_EVC_TABLE, &MefEvcInfo, NULL);
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * * FUNCTION NAME    : MefEvcInfoCreateTable
 * *
 * * DESCRIPTION      : This function creates the Evc Info table.
 * *
 * * INPUT            : None
 * *
 * * OUTPUT           : None
 * *
 * * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * *
 * **************************************************************************/

INT4
MefEvcInfoCreateTable (VOID)
{
    ISS_EVC_INFO_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tEvcInfo, IssEvcInfoRbNode),
                              MefIssEvcInfoEntryRBTreeCmp);
    if (ISS_EVC_INFO_TABLE == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefGetEvcInfoEntry
 *
 * DESCRIPTION      : This function returns tEvcInfo node from the
 *                    MEF EVC table for the given EvcVlanId.
 *
 * INPUT            : SVlanId - Vlan Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : tEvcInfo - pointer to Evc entry
 *                    ELSE NULL
**************************************************************************/
tEvcInfo           *
MefGetEvcInfoEntry (INT4 SVlanId)
{
    tEvcInfo           *pIssEvcInfo = NULL;
    tEvcInfo            IssEvcInfo;

    MEMSET (&IssEvcInfo, 0, sizeof (tEvcInfo));
    IssEvcInfo.SVlanId = (UINT2) SVlanId;

    pIssEvcInfo = (tEvcInfo *) RBTreeGet (ISS_EVC_INFO_TABLE, &IssEvcInfo);
    if (pIssEvcInfo == NULL)
    {
        return (pIssEvcInfo);
    }

    return (pIssEvcInfo);
}

/*****************************************************************************/
/*    Function Name       : MefEvcInitDefaultEvcInfo                         */
/*                                                                           */
/*    Description         : This function set the default value of Evc Info. */
/*                                                                           */
/*    Input(s)            : u4EvcId - Evc Index                              */
/*                          i4FsEvcVlanId - Vlan Id                          */
/*    Output(s)           : pIssEvcInfo - Pointer to the given entry         */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
MefEvcInitDefaultEvcInfo (UINT4 u4EvcId, INT4 i4FsEvcVlanId,
                          tEvcInfo * pIssEvcInfo)
{
    MEMSET (pIssEvcInfo, 0, sizeof (tEvcInfo));

    pIssEvcInfo->u4EvcId = u4EvcId;
    pIssEvcInfo->SVlanId = (UINT2) i4FsEvcVlanId;
    pIssEvcInfo->bCeVlanIdPreservation = MEF_DISABLED;
    pIssEvcInfo->bCeVlanCoSPreservation = MEF_DISABLED;
}

/***************************************************************************
 * FUNCTION NAME    : MefAddEvcInfoEntry
 *
 * DESCRIPTION      : This function add a EvcInfoAddNode node to the MEF
 *                    Evc info table.
 *
 * INPUT            : pIssEvcInfo - pointer to Evc node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefAddEvcInfoEntry (tEvcInfo * pIssEvcInfo)
{

    if (RBTreeAdd (ISS_EVC_INFO_TABLE, pIssEvcInfo) != RB_SUCCESS)
    {
        MemReleaseMemBlock (MEF_ISS_EVC_INFO_POOL_ID, (UINT1 *) pIssEvcInfo);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeleteEvcInfoEntry
 *
 * DESCRIPTION      : This function delete a tEvcInfo  node from the
 *                    MEF EVC table.
 *
 * INPUT            : tEvcInfo - EvcInfo structure entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
**************************************************************************/
INT4
MefDeleteEvcInfoEntry (tEvcInfo * pIssEvcInfo)
{
    if (RBTreeRemove (ISS_EVC_INFO_TABLE, pIssEvcInfo) != RB_FAILURE)
    {
        MemReleaseMemBlock (MEF_ISS_EVC_INFO_POOL_ID, (UINT1 *) pIssEvcInfo);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : MefGetEvcFilterEntry
 *
 * DESCRIPTION      : This function returns tMefEvcFilterInfo node from the
 *                    MEF EVC table for the given EvcContextId and EvcVlanId.
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Vlan Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : tMefEvcInfo - pointer to pseudowire IfIndex Map  entry
 *                    ELSE NULL
 **************************************************************************/
tMefEvcFilterInfo  *
MefGetEvcFilterEntry (INT4 i4FsEvcContextId, INT4 i4FsEvcVlanId,
                      INT4 i4FsEvcInstance)
{
    tMefEvcFilterInfo   MefEvcFilterInfo;
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;

    MEMSET (&MefEvcFilterInfo, 0, sizeof (tMefEvcFilterInfo));
    MefEvcFilterInfo.u4EvcFilterContextId = i4FsEvcContextId;
    MefEvcFilterInfo.i4EvcFilterVlanId = i4FsEvcVlanId;
    MefEvcFilterInfo.i4EvcFilterInstance = i4FsEvcInstance;

    pMefEvcFilterInfo = (tMefEvcFilterInfo *) RBTreeGet
        (MEF_EVC_FILTER_TABLE, &MefEvcFilterInfo);

    return (pMefEvcFilterInfo);
}

/***************************************************************************
 * FUNCTION NAME    : MefSetEvcEntries 
 *
 * DESCRIPTION      : 
 *                    
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Valn Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefSetEvcEntries (UINT4 u4FsEvcContextId, INT4 i4FsEvcVlanId,
                  INT4 i4FsEvcFilterInstance)
{
    tMefEvcFilterInfo   MefEvcFilterInfo;
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;
    UINT4               u4Error = 0;

    MEMSET (&MefEvcFilterInfo, 0, sizeof (tMefEvcFilterInfo));

    MefEvcFilterInfo.u4EvcFilterContextId = u4FsEvcContextId;
    MefEvcFilterInfo.i4EvcFilterVlanId = i4FsEvcVlanId;
    MefEvcFilterInfo.i4EvcFilterInstance = i4FsEvcFilterInstance;

    /* Walk through the EVC filter table entries apply all the 
     * Configuration when the EVC table row status is set */

    pMefEvcFilterInfo =
        (tMefEvcFilterInfo *) RBTreeGet (MEF_EVC_FILTER_TABLE,
                                         &MefEvcFilterInfo);
    if (pMefEvcFilterInfo != NULL)
    {
        if (nmhTestv2IssExtL2FilterStatus (&u4Error,
                                           pMefEvcFilterInfo->i4EvcFilterId,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetIssExtL2FilterStatus (pMefEvcFilterInfo->i4EvcFilterId,
                                        CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhTestv2IssExtL2FilterDstMacAddr (&u4Error,
                                               pMefEvcFilterInfo->i4EvcFilterId,
                                               pMefEvcFilterInfo->
                                               EvcFilterMacAddr) ==
            SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetIssExtL2FilterDstMacAddr (pMefEvcFilterInfo->i4EvcFilterId,
                                            pMefEvcFilterInfo->EvcFilterMacAddr)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhTestv2IssExtL2FilterAction (&u4Error,
                                           pMefEvcFilterInfo->i4EvcFilterId,
                                           pMefEvcFilterInfo->i4EvcFilterAction)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetIssExtL2FilterAction (pMefEvcFilterInfo->i4EvcFilterId,
                                        pMefEvcFilterInfo->i4EvcFilterAction)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;

        }

        if (nmhSetIssExtL2FilterStatus (pMefEvcFilterInfo->i4EvcFilterId,
                                        ACTIVE) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetIssExtL2FilterStatus (pMefEvcFilterInfo->i4EvcFilterId,
                                        ACTIVE) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

                /* HDC 59957 */
/***************************************************************************
 * FUNCTION NAME    : MefSetServiceType
 *
 * DESCRIPTION      : This function sets service type in VLAN PB module. 
 *                    
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Vlan Id
 *                    i4ServiceType - Service Type
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefSetServiceType (UINT4 u4FsEvcContextId, INT4 i4FsEvcVlanId,
                   INT4 i4ServiceType)
{
    UINT4               u4Error = 0;

    if (i4ServiceType == MEF_ROOTED_MULTIPOINT)
    {
        return OSIX_SUCCESS;
    }
    VLAN_LOCK ();
    if (nmhTestv2FsMIPbSVlanConfigServiceType (&u4Error, u4FsEvcContextId,
                                               (UINT4) i4FsEvcVlanId,
                                               i4ServiceType) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIPbSVlanConfigServiceType (u4FsEvcContextId,
                                            (UINT4) i4FsEvcVlanId,
                                            i4ServiceType) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return OSIX_FAILURE;
    }

    VLAN_UNLOCK ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefEvcTunnelInfoToVlan
 *
 * DESCRIPTION      : This function sets the tunnel status in VLAN PB module.
 *
 *
 * INPUT            : pMefEvcInfo - Pointer to Evc Table info
 *                    i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Vlan Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefEvcTunnelInfoToVlan (tMefEvcInfo * pMefEvcInfo, INT4 i4FsEvcContextId,
                        INT4 i4FsEvcIndex)
{
    UINT4               pu4ErrorCode = 0;
    VLAN_LOCK ();

    if (nmhTestv2FsMIServiceVlanTunnelProtocolStatus
        (&pu4ErrorCode, i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_ECFM,
         MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIServiceVlanTunnelProtocolStatus
        (i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_ECFM,
         MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (VlanSetTunnelStatusPerVLAN (i4FsEvcContextId, i4FsEvcIndex,
                                    MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo)) !=
        VLAN_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIServiceVlanTunnelProtocolStatus
        (&pu4ErrorCode, i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_GVRP,
         MEF_EVC_GVRP_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIServiceVlanTunnelProtocolStatus
        (i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_GVRP,
         MEF_EVC_GVRP_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (VlanSetTunnelStatusPerVLAN (i4FsEvcContextId, i4FsEvcIndex,
                                    MEF_EVC_GVRP_TUNNEL_STATUS (pMefEvcInfo)) !=
        VLAN_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIServiceVlanTunnelProtocolStatus
        (&pu4ErrorCode, i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_GMRP,
         MEF_EVC_GMRP_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIServiceVlanTunnelProtocolStatus
        (i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_GMRP,
         MEF_EVC_GMRP_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (VlanSetTunnelStatusPerVLAN (i4FsEvcContextId, i4FsEvcIndex,
                                    MEF_EVC_GMRP_TUNNEL_STATUS (pMefEvcInfo)) !=
        VLAN_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIServiceVlanTunnelProtocolStatus
        (&pu4ErrorCode, i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_MMRP,
         MEF_EVC_MMRP_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIServiceVlanTunnelProtocolStatus
        (i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_MMRP,
         MEF_EVC_MMRP_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (VlanSetTunnelStatusPerVLAN (i4FsEvcContextId, i4FsEvcIndex,
                                    MEF_EVC_MMRP_TUNNEL_STATUS (pMefEvcInfo)) !=
        VLAN_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIServiceVlanTunnelProtocolStatus
        (&pu4ErrorCode, i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_MVRP,
         MEF_EVC_MVRP_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIServiceVlanTunnelProtocolStatus
        (i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_MVRP,
         MEF_EVC_MVRP_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (VlanSetTunnelStatusPerVLAN (i4FsEvcContextId, i4FsEvcIndex,
                                    MEF_EVC_MVRP_TUNNEL_STATUS (pMefEvcInfo)) !=
        VLAN_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIServiceVlanTunnelProtocolStatus
        (&pu4ErrorCode, i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_IGMP,
         MEF_EVC_IGMP_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIServiceVlanTunnelProtocolStatus
        (i4FsEvcContextId, i4FsEvcIndex, L2_PROTO_IGMP,
         MEF_EVC_IGMP_TUNNEL_STATUS (pMefEvcInfo)) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (VlanSetTunnelStatusPerVLAN (i4FsEvcContextId, i4FsEvcIndex,
                                    MEF_EVC_IGMP_TUNNEL_STATUS (pMefEvcInfo)) !=
        VLAN_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    VLAN_UNLOCK ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefEvcFilterCreateTable
 *
 * DESCRIPTION      : This function creates the EvcFilter table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefEvcFilterCreateTable (VOID)
{
    MEF_EVC_FILTER_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tMefEvcFilterInfo, MefEvcFilterRbNode),
                              MefEvcFilterEnrtyRBTreeCmp);
    if (MEF_EVC_FILTER_TABLE == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : MefEvcEnrtyRBTreeCmp
 *  Description     : RBTree Compare function for context and Vlan Id entries.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 *************************************************************************/
INT4
MefEvcEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMefEvcInfo        *pMefEvcInfoA = NULL;
    tMefEvcInfo        *pMefEvcInfoB = NULL;

    pMefEvcInfoA = (tMefEvcInfo *) pRBElem1;
    pMefEvcInfoB = (tMefEvcInfo *) pRBElem2;

    if ((pMefEvcInfoA->u4EvcContextId) < (pMefEvcInfoB->u4EvcContextId))
    {
        return -1;
    }
    else if ((pMefEvcInfoA->u4EvcContextId) > (pMefEvcInfoB->u4EvcContextId))
    {
        return 1;
    }

    if ((pMefEvcInfoA->i4EvcVlanId) < (pMefEvcInfoB->i4EvcVlanId))
    {
        return -1;
    }
    else if ((pMefEvcInfoA->i4EvcVlanId) > (pMefEvcInfoB->i4EvcVlanId))
    {
        return 1;
    }

    return 0;

}

/************************************************************************
 *  Function Name   : MefIssEvcInfoEntryRBTreeCmp
 *  Description     : RBTree Compare function for Vlan Id entries.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
*************************************************************************/
INT4
MefIssEvcInfoEntryRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tEvcInfo           *pIssEvcInfoA = NULL;
    tEvcInfo           *pIssEvcInfoB = NULL;

    pIssEvcInfoA = (tEvcInfo *) pRBElem1;
    pIssEvcInfoB = (tEvcInfo *) pRBElem2;

    if ((pIssEvcInfoA->SVlanId) < (pIssEvcInfoB->SVlanId))
    {
        return -1;
    }
    else if ((pIssEvcInfoA->SVlanId) > (pIssEvcInfoB->SVlanId))
    {
        return 1;
    }

    return 0;
}

/************************************************************************
 *  Function Name   : MefEvcFilterEnrtyRBTreeCmp
 *  Description     : RBTree Compare function for context, Vlan Id and 
 *                    FilterInstance entries.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 *************************************************************************/
INT4
MefEvcFilterEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMefEvcFilterInfo  *pMefEvcFilterInfoA = NULL;
    tMefEvcFilterInfo  *pMefEvcFilterInfoB = NULL;

    pMefEvcFilterInfoA = (tMefEvcFilterInfo *) pRBElem1;
    pMefEvcFilterInfoB = (tMefEvcFilterInfo *) pRBElem2;

    if ((pMefEvcFilterInfoA->u4EvcFilterContextId) <
        (pMefEvcFilterInfoB->u4EvcFilterContextId))
    {
        return -1;
    }
    else if ((pMefEvcFilterInfoA->u4EvcFilterContextId) >
             (pMefEvcFilterInfoB->u4EvcFilterContextId))
    {
        return 1;
    }

    if ((pMefEvcFilterInfoA->i4EvcFilterVlanId) <
        (pMefEvcFilterInfoB->i4EvcFilterVlanId))
    {
        return -1;
    }
    else if ((pMefEvcFilterInfoA->i4EvcFilterVlanId) >
             (pMefEvcFilterInfoB->i4EvcFilterVlanId))
    {
        return 1;
    }

    if ((pMefEvcFilterInfoA->i4EvcFilterInstance) <
        (pMefEvcFilterInfoB->i4EvcFilterInstance))
    {
        return -1;
    }
    else if ((pMefEvcFilterInfoA->i4EvcFilterInstance) >
             (pMefEvcFilterInfoB->i4EvcFilterInstance))
    {
        return 1;
    }

    return 0;

}

/***************************************************************************
 * FUNCTION NAME    : MefFilterCreateTable
 *
 * DESCRIPTION      : This function creates the MefFilter table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefFilterCreateTable (VOID)
{
    MEF_FILTER_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMefFilterInfo, MefFilterRbNode),
                              MefFilterEnrtyRBTreeCmp);
    if (MEF_FILTER_TABLE == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefClassMapCreateTable
 *
 * DESCRIPTION      : This function creates the MefClassMap table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefClassMapCreateTable (VOID)
{
    MEF_CLASS_MAP_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tMefClassMapInfo, MefClassMapRbNode),
                              MefClassMapEnrtyRBTreeCmp);
    if (MEF_CLASS_MAP_TABLE == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefClassCreateTable
 *
 * DESCRIPTION      : This function creates the MefClass table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefClassCreateTable (VOID)
{
    MEF_CLASS_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMefClassInfo, MefClassRbNode),
                              MefClassEnrtyRBTreeCmp);
    if (MEF_CLASS_TABLE == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefMeterCreateTable
 *
 * DESCRIPTION      : This function creates the MefMeter table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefMeterCreateTable (VOID)
{
    MEF_METER_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMefMeterInfo, MefMeterRbNode),
                              MefMeterEnrtyRBTreeCmp);
    if (MEF_METER_TABLE == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefPolicyMapCreateTable
 *
 * DESCRIPTION      : This function creates the MefPolicyMap table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefPolicyMapCreateTable (VOID)
{
    MEF_POLICY_MAP_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tMefPolicyMapInfo, MefPolicyMapRbNode),
                              MefPolicyMapEnrtyRBTreeCmp);
    if (MEF_POLICY_MAP_TABLE == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : MefFilterEnrtyRBTreeCmp
 *  Description     : RBTree Compare function for FilterId entries.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 *************************************************************************/
INT4
MefFilterEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMefFilterInfo     *pMefFilterInfoA = NULL;
    tMefFilterInfo     *pMefFilterInfoB = NULL;

    pMefFilterInfoA = (tMefFilterInfo *) pRBElem1;
    pMefFilterInfoB = (tMefFilterInfo *) pRBElem2;

    if ((pMefFilterInfoA->u4FilterNo) < (pMefFilterInfoB->u4FilterNo))
    {
        return -1;
    }
    else if ((pMefFilterInfoA->u4FilterNo) > (pMefFilterInfoB->u4FilterNo))
    {
        return 1;
    }

    return 0;

}

/************************************************************************
 *  Function Name   : MefClassMapEnrtyRBTreeCmp
 *  Description     : RBTree Compare function for ClassMapId entries.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 *************************************************************************/
INT4
MefClassMapEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMefClassMapInfo   *pMefClassMapInfoA = NULL;
    tMefClassMapInfo   *pMefClassMapInfoB = NULL;

    pMefClassMapInfoA = (tMefClassMapInfo *) pRBElem1;
    pMefClassMapInfoB = (tMefClassMapInfo *) pRBElem2;

    if ((pMefClassMapInfoA->u4ClassMapClfrId) <
        (pMefClassMapInfoB->u4ClassMapClfrId))
    {
        return -1;
    }
    else if ((pMefClassMapInfoA->u4ClassMapClfrId) >
             (pMefClassMapInfoB->u4ClassMapClfrId))
    {
        return 1;
    }

    return 0;

}

/************************************************************************
 *  Function Name   : MefClassEnrtyRBTreeCmp
 *  Description     : RBTree Compare function for ClassId entries.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 *************************************************************************/
INT4
MefClassEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMefClassInfo      *pMefClassInfoA = NULL;
    tMefClassInfo      *pMefClassInfoB = NULL;

    pMefClassInfoA = (tMefClassInfo *) pRBElem1;
    pMefClassInfoB = (tMefClassInfo *) pRBElem2;

    if ((pMefClassInfoA->u4ClassId) < (pMefClassInfoB->u4ClassId))
    {
        return -1;
    }
    else if ((pMefClassInfoA->u4ClassId) > (pMefClassInfoB->u4ClassId))
    {
        return 1;
    }

    return 0;

}

/************************************************************************
 *  Function Name   : MefMeterEnrtyRBTreeCmp
 *  Description     : RBTree Compare function for MeterId entries.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 *************************************************************************/
INT4
MefMeterEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMefMeterInfo      *pMefMeterInfoA = NULL;
    tMefMeterInfo      *pMefMeterInfoB = NULL;

    pMefMeterInfoA = (tMefMeterInfo *) pRBElem1;
    pMefMeterInfoB = (tMefMeterInfo *) pRBElem2;

    if ((pMefMeterInfoA->u4MeterId) < (pMefMeterInfoB->u4MeterId))
    {
        return -1;
    }
    else if ((pMefMeterInfoA->u4MeterId) > (pMefMeterInfoB->u4MeterId))
    {
        return 1;
    }

    return 0;

}

/************************************************************************
 *  Function Name   : MefPolicyMapEnrtyRBTreeCmp
 *  Description     : RBTree Compare function for u4PolicyMapId entries.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 *************************************************************************/
INT4
MefPolicyMapEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMefPolicyMapInfo  *pMefPolicyMapInfoA = NULL;
    tMefPolicyMapInfo  *pMefPolicyMapInfoB = NULL;

    pMefPolicyMapInfoA = (tMefPolicyMapInfo *) pRBElem1;
    pMefPolicyMapInfoB = (tMefPolicyMapInfo *) pRBElem2;

    if ((pMefPolicyMapInfoA->u4PolicyMapId) <
        (pMefPolicyMapInfoB->u4PolicyMapId))
    {
        return -1;
    }
    else if ((pMefPolicyMapInfoA->u4PolicyMapId) >
             (pMefPolicyMapInfoB->u4PolicyMapId))
    {
        return 1;
    }

    return 0;
}

/***************************************************************************
 * FUNCTION NAME    : MefGetFilterEntry
 *
 * DESCRIPTION      : This function returns tMefFilterInfo node from the
 *                    MEF MefFilter table for the given EvcContextId and
 *                    EvcVlanId.
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Vlan Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : tMefEvcInfo - pointer to pseudowire IfIndex Map  entry
 *                    ELSE NULL
 **************************************************************************/
tMefFilterInfo     *
MefGetFilterEntry (INT4 i4FsMefFilterNo)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;
    tMefFilterInfo      MefFilterInfo;

    MEMSET (&MefFilterInfo, 0, sizeof (tMefFilterInfo));
    MefFilterInfo.u4FilterNo = i4FsMefFilterNo;

    pMefFilterInfo = (tMefFilterInfo *) RBTreeGet
        (MEF_FILTER_TABLE, &MefFilterInfo);

    return (pMefFilterInfo);
}

/***************************************************************************
 * FUNCTION NAME    : MefGetClassMapEntry
 *
 * DESCRIPTION      : This function returns tMefClassMapInfo node from the
 *                    MEF MefClassMap table for the given MefClassMapId.
 *
 * INPUT            : u4FsMefClassMapId - MefClassMap Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : tMefClassMapInfo - pointer to tMefClassMapInfo entry
 *                    ELSE NULL
 **************************************************************************/
tMefClassMapInfo   *
MefGetClassMapEntry (UINT4 u4FsMefClassMapId)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;
    tMefClassMapInfo    MefClassMapInfo;

    MEMSET (&MefClassMapInfo, 0, sizeof (tMefClassMapInfo));
    MefClassMapInfo.u4ClassMapClfrId = u4FsMefClassMapId;

    pMefClassMapInfo = (tMefClassMapInfo *) RBTreeGet
        (MEF_CLASS_MAP_TABLE, &MefClassMapInfo);
    if (pMefClassMapInfo == NULL)
    {
        return (pMefClassMapInfo);
    }

    return (pMefClassMapInfo);
}

/***************************************************************************
 * FUNCTION NAME    : MefGetClassEntry
 *
 * DESCRIPTION      : This function returns tMefClassInfo node from the
 *                    MEF MefClass table for the given MefClassId.
 *
 * INPUT            : u4FsMefClassId - MefClass Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : tMefClassInfo - pointer to tMefClassInfo entry
 *                    ELSE NULL
 **************************************************************************/
tMefClassInfo      *
MefGetClassEntry (UINT4 u4FsMefClassId)
{
    tMefClassInfo      *pMefClassInfo = NULL;
    tMefClassInfo       MefClassInfo;

    MEMSET (&MefClassInfo, 0, sizeof (tMefClassInfo));
    MefClassInfo.u4ClassId = u4FsMefClassId;

    pMefClassInfo = (tMefClassInfo *) RBTreeGet
        (MEF_CLASS_TABLE, &MefClassInfo);
    if (pMefClassInfo == NULL)
    {
        return (pMefClassInfo);
    }

    return (pMefClassInfo);
}

/***************************************************************************
 * FUNCTION NAME    : MefGetMeterEntry
 *
 * DESCRIPTION      : This function returns tMefMeterInfo node from the
 *                    MEF MefMeter table for the given MefMeterId.
 *
 * INPUT            : u4FsMeterId - MefClass Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : tMefClassInfo - pointer to tMefClassInfo entry
 *                    ELSE NULL
 **************************************************************************/
tMefMeterInfo      *
MefGetMeterEntry (UINT4 u4FsMeterId)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;
    tMefMeterInfo       MefMeterInfo;

    MEMSET (&MefMeterInfo, 0, sizeof (tMefMeterInfo));
    MefMeterInfo.u4MeterId = u4FsMeterId;

    pMefMeterInfo = (tMefMeterInfo *) RBTreeGet
        (MEF_METER_TABLE, &MefMeterInfo);
    if (pMefMeterInfo == NULL)
    {
        return (pMefMeterInfo);
    }

    return (pMefMeterInfo);
}

/***************************************************************************
 * FUNCTION NAME    : MefGetPolicyMapEntry
 *
 * DESCRIPTION      : This function returns tMefPolicyMapInfo node from the
 *                    MEF MefPolicyMap table for the given MefPolicyMapId.
 *
 * INPUT            : u4FsMeterId - MefClass Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : tMefClassInfo - pointer to tMefClassInfo entry
 *                    ELSE NULL
 **************************************************************************/
tMefPolicyMapInfo  *
MefGetPolicyMapEntry (UINT4 u4FsPolicyMapId)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;
    tMefPolicyMapInfo   MefPolicyMapInfo;

    MEMSET (&MefPolicyMapInfo, 0, sizeof (tMefPolicyMapInfo));
    MefPolicyMapInfo.u4PolicyMapId = u4FsPolicyMapId;

    pMefPolicyMapInfo = (tMefPolicyMapInfo *) RBTreeGet
        (MEF_POLICY_MAP_TABLE, &MefPolicyMapInfo);
    if (pMefPolicyMapInfo == NULL)
    {
        return (pMefPolicyMapInfo);
    }

    return (pMefPolicyMapInfo);
}

/*****************************************************************************/
/*    Function Name       : MefInitDefaultFilterInfo                         */
/*                                                                           */
/*    Description         : This function set the default value of Evc.      */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : i4FsEvcContextId - Context Id                    */
/*                          i4FsEvcVlanId - Vlan Id                          */
/*    Output(s)           : pMefEvcInfo - Pointde to the given entry         */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
MefInitDefaultFilterInfo (INT4 i4FsMefFilterNo, tMefFilterInfo * pMefFilterInfo)
{
    MEMSET (pMefFilterInfo, 0, sizeof (tMefFilterInfo));

    pMefFilterInfo->u4FilterNo = i4FsMefFilterNo;
    pMefFilterInfo->i1CVlanPriority = -1;
    pMefFilterInfo->u2Evc = 1;
    pMefFilterInfo->i1Dscp = ISS_DSCP_INVALID;
    pMefFilterInfo->u1Direction = ISS_DIRECTION_IN;
}

/***************************************************************************
 * FUNCTION NAME    : MefAddFilterEntry
 *
 * DESCRIPTION      : This function add a MefFilterAddNode node to the MEF
 *                    Filter table.
 *
 * INPUT            : pMefFilterInfo - pointer to Filter node information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefAddFilterEntry (tMefFilterInfo * pMefFilterInfo)
{
    if (RBTreeAdd (MEF_FILTER_TABLE, pMefFilterInfo) != RB_SUCCESS)
    {
        MemReleaseMemBlock (MEF_FILTER_POOL_ID, (UINT1 *) pMefFilterInfo);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefSetFilterEntries 
 *
 * DESCRIPTION      : 
 *                    
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Valn Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefSetFilterEntries (INT4 i4FsMefFilterNo)
{
    UINT1               au1PortList[BRG_PHY_PORT_LIST_SIZE];
    tSNMP_OCTET_STRING_TYPE PortList;
    tMefFilterInfo     *pMefFilterInfo = NULL;
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4Dscp = 0;

    MEMSET (au1PortList, 0, BRG_PHY_PORT_LIST_SIZE);
    PortList.i4_Length = BRG_PHY_PORT_LIST_SIZE;
    PortList.pu1_OctetList = au1PortList;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    i4Dscp = (INT4) pMefFilterInfo->i1Dscp;

    if (nmhGetIssExtL3FilterStatus (i4FsMefFilterNo, &i4RowStatus) ==
        SNMP_FAILURE)
    {
        if (nmhTestv2IssExtL3FilterStatus (&u4ErrorCode, i4FsMefFilterNo,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetIssExtL3FilterStatus (i4FsMefFilterNo, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2IssExtL3FilterStatus (&u4ErrorCode, i4FsMefFilterNo,
                                           NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetIssExtL3FilterStatus (i4FsMefFilterNo, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    if (nmhTestv2IssExtL3FilterDirection (&u4ErrorCode, i4FsMefFilterNo,
                                          pMefFilterInfo->u1Direction)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetIssExtL3FilterDirection (i4FsMefFilterNo,
                                       pMefFilterInfo->u1Direction)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2IssExtL3FilterDscp (&u4ErrorCode, i4FsMefFilterNo,
                                     i4Dscp) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetIssExtL3FilterDscp (i4FsMefFilterNo, i4Dscp) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2IssExtL3FilterAction (&u4ErrorCode, i4FsMefFilterNo,
                                       ISS_ALLOW) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetIssExtL3FilterAction (i4FsMefFilterNo, ISS_ALLOW) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliSetMemberPort (PortList.pu1_OctetList, PortList.i4_Length,
                      pMefFilterInfo->u4IfIndex);

    if (pMefFilterInfo->u1Direction == ISS_DIRECTION_IN)
    {
        nmhSetIssExtL3FilterInPortList (i4FsMefFilterNo, &PortList);
    }
    else if (pMefFilterInfo->u1Direction == ISS_DIRECTION_OUT)
    {
        nmhSetIssExtL3FilterOutPortList (i4FsMefFilterNo, &PortList);
    }

    if (nmhTestv2IssMetroL3FilterSVlanId (&u4ErrorCode, i4FsMefFilterNo,
                                          pMefFilterInfo->u2Evc) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetIssMetroL3FilterSVlanId (i4FsMefFilterNo, pMefFilterInfo->u2Evc)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2IssMetroL3FilterCVlanPriority (&u4ErrorCode, i4FsMefFilterNo,
                                                pMefFilterInfo->i1CVlanPriority)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetIssMetroL3FilterCVlanPriority (i4FsMefFilterNo,
                                             pMefFilterInfo->i1CVlanPriority)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2IssExtL3FilterStatus (&u4ErrorCode, i4FsMefFilterNo,
                                       ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetIssExtL3FilterStatus (i4FsMefFilterNo, ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeletFilterEntry
 *
 * DESCRIPTION      : This function delete a tMefFilterInfo  node from the
 *                    MEF MefFilter table.
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Valn Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeleteFilterEntry (tMefFilterInfo * pMefFilterInfo)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetIssExtL3FilterStatus (pMefFilterInfo->u4FilterNo, &i4RowStatus)
        == SNMP_SUCCESS)
    {
        if (nmhTestv2IssExtL3FilterStatus
            (&u4ErrorCode, pMefFilterInfo->u4FilterNo, DESTROY) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetIssExtL3FilterStatus (pMefFilterInfo->u4FilterNo, DESTROY)
            == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    if (RBTreeRemove (MEF_FILTER_TABLE, pMefFilterInfo) != RB_FAILURE)
    {
        MemReleaseMemBlock (MEF_FILTER_POOL_ID, (UINT1 *) pMefFilterInfo);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : MefInitDefaultClassMapInfo                       */
/*                                                                           */
/*    Description         : This function set the default value of ClassMap. */
/*                                                                           */
/*    Input(s)            : u4FsMefClassMapId - Class Map Id                 */
/*                                                                           */
/*    Output(s)           : pMefClassMapInfo - Pointde to the given          */
/*                          u4FsMefClassMapId entry                          */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
MefInitDefaultClassMapInfo (UINT4 u4FsMefClassMapId,
                            tMefClassMapInfo * pMefClassMapInfo)
{
    UINT1               u1ClassMapName[MEF_MAX_NAME_LENGTH];

    MEMSET (pMefClassMapInfo, 0, sizeof (tMefClassMapInfo));

    pMefClassMapInfo->u4ClassMapClfrId = u4FsMefClassMapId;
    pMefClassMapInfo->u4ClassId = 0;

    MEMSET (u1ClassMapName, 0, MEF_MAX_NAME_LENGTH);
    SPRINTF ((CHR1 *) u1ClassMapName, "ClassMap_%u", u4FsMefClassMapId);
    STRCPY (&pMefClassMapInfo->au1ClassMapName, u1ClassMapName);

}

/***************************************************************************
 * FUNCTION NAME    : MefAddClassMapEntry
 *
 * DESCRIPTION      : This function add a MefClassMapAddNode node to the MEF
 *                    ClassMap table.
 *
 * INPUT            : pMefClassMapInfo - pointer to ClassMap information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefAddClassMapEntry (tMefClassMapInfo * pMefClassMapInfo)
{

    if (RBTreeAdd (MEF_CLASS_MAP_TABLE, pMefClassMapInfo) != RB_SUCCESS)
    {
        MemReleaseMemBlock (MEF_CLASS_MAP_POOL_ID, (UINT1 *) pMefClassMapInfo);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefSetClassMapEntries 
 *
 * DESCRIPTION      : 
 *                    
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Valn Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefSetClassMapEntries (INT4 u4FsMefClassMapId)
{
    tSNMP_OCTET_STRING_TYPE MefOctStrClassMapName;
    tMefClassMapInfo   *pMefClassMapInfo = NULL;
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4FsMefPriorityMapId = 0;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);
    if (pMefClassMapInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (nmhGetFsQoSClassMapStatus (u4FsMefClassMapId, &i4RowStatus)
        != SNMP_SUCCESS)
    {
        if (nmhTestv2FsQoSClassMapStatus (&u4ErrorCode, u4FsMefClassMapId,
                                          CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsQoSClassMapStatus (u4FsMefClassMapId,
                                       CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsQoSClassMapStatus (&u4ErrorCode, u4FsMefClassMapId,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsQoSClassMapStatus (u4FsMefClassMapId,
                                       NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    MEMSET (&MefOctStrClassMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MefOctStrClassMapName.i4_Length =
        STRLEN (pMefClassMapInfo->au1ClassMapName);
    MefOctStrClassMapName.pu1_OctetList = pMefClassMapInfo->au1ClassMapName;

    if (nmhTestv2FsQoSClassMapName (&u4ErrorCode,
                                    u4FsMefClassMapId,
                                    &MefOctStrClassMapName) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSClassMapName (u4FsMefClassMapId,
                                 &MefOctStrClassMapName) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSClassMapPriorityMapId (u4FsMefClassMapId,
                                          u4FsMefPriorityMapId) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSClassMapL3FilterId (&u4ErrorCode,
                                          u4FsMefClassMapId,
                                          pMefClassMapInfo->u4FilterId)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSClassMapL3FilterId (u4FsMefClassMapId,
                                       pMefClassMapInfo->u4FilterId)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSClassMapCLASS (&u4ErrorCode,
                                     u4FsMefClassMapId,
                                     pMefClassMapInfo->u4ClassId)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSClassMapCLASS (u4FsMefClassMapId,
                                  pMefClassMapInfo->u4ClassId) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSClassMapStatus (&u4ErrorCode, u4FsMefClassMapId,
                                      ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSClassMapStatus (u4FsMefClassMapId, ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeletClassMapEntry
 *
 * DESCRIPTION      : This function delete a tMefClassMapInfo  node from the
 *                    MEF MefClassMap table.
 *
 * INPUT            : pMefClassMapInfo - Pointer to the MefClassMap entry to 
 *                    be deleted
 *                    
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeleteClassMapEntry (tMefClassMapInfo * pMefClassMapInfo)
{
    tMefClassInfo      *pMefClassInfo = NULL;
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4TempFilterId = 0;

    if (nmhGetFsQoSClassMapStatus (pMefClassMapInfo->u4ClassMapClfrId,
                                   &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsQoSClassMapL3FilterId (pMefClassMapInfo->u4ClassMapClfrId,
                                           MEF_ZERO) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if ((pMefClassMapInfo->u4ClassMapClfrId) > QOS_CM_TBL_DEF_ENTRY_MAX)
        {
            if (nmhTestv2FsQoSClassMapStatus (&u4ErrorCode,
                                              pMefClassMapInfo->
                                              u4ClassMapClfrId,
                                              DESTROY) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            if (nmhSetFsQoSClassMapStatus (pMefClassMapInfo->u4ClassMapClfrId,
                                           DESTROY) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
    }
    /* Reset corresponding Class ref count if the id not '0' */
    if (pMefClassMapInfo->u4ClassId != 0)
    {
        if (nmhSetFsQoSClassMapL3FilterId (pMefClassMapInfo->u4ClassId,
                                           u4TempFilterId) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        pMefClassInfo = MefGetClassEntry (pMefClassMapInfo->u4ClassId);
        if (pMefClassInfo == NULL)
        {
            return OSIX_FAILURE;
        }
        /* Incremnet the RefCount of Meter Id  Entry in the Meter Table */
        pMefClassInfo->u4RefCount = (pMefClassInfo->u4RefCount) - 1;
    }

    if (RBTreeRemove (MEF_CLASS_MAP_TABLE, pMefClassMapInfo) != RB_FAILURE)
    {
        MemReleaseMemBlock (MEF_CLASS_MAP_POOL_ID, (UINT1 *) pMefClassMapInfo);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;

}

/*****************************************************************************/
/*    Function Name       : MefInitDefaultClassInfo                          */
/*                                                                           */
/*    Description         : This function set the default value of Class.    */
/*                                                                           */
/*    Input(s)            : u4FsMefClassMapId - Class Map Id                 */
/*                                                                           */
/*    Output(s)           : pMefClassMapInfo - Pointde to the given          */
/*                          u4FsMefClassMapId entry                          */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
MefInitDefaultClassInfo (UINT4 u4FsMefClassCLASS, tMefClassInfo * pMefClassInfo)
{
    MEMSET (pMefClassInfo, 0, sizeof (tMefClassInfo));

    pMefClassInfo->u4ClassId = u4FsMefClassCLASS;
}

/***************************************************************************
 * FUNCTION NAME    : MefAddClassEntry
 *
 * DESCRIPTION      : This function add a MefClassAddNode node to the MEF
 *                    Class table.
 *
 * INPUT            : pMefClassInfo - pointer to Class information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefAddClassEntry (tMefClassInfo * pMefClassInfo)
{

    if (RBTreeAdd (MEF_CLASS_TABLE, pMefClassInfo) != RB_SUCCESS)
    {
        MemReleaseMemBlock (MEF_CLASS_POOL_ID, (UINT1 *) pMefClassInfo);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefSetClassEntries 
 *
 * DESCRIPTION      : 
 *                    
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Valn Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefSetClassEntries (UINT4 u4FsMefClassCLASS)
{
    tSNMP_OCTET_STRING_TYPE MefOctStrGroupName;
    tMefClassInfo      *pMefClassInfo = NULL;
    UINT4               u4ErrorCode = 0;
    UINT1               u1GroupName[MEF_MAX_NAME_LENGTH];

    pMefClassInfo = MefGetClassEntry (u4FsMefClassCLASS);
    if (pMefClassInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSClassToPriorityStatus (&u4ErrorCode,
                                             u4FsMefClassCLASS,
                                             CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSClassToPriorityStatus (u4FsMefClassCLASS,
                                          CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (u1GroupName, 0, MEF_MAX_NAME_LENGTH);
    MEMSET (&MefOctStrGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    SPRINTF ((CHR1 *) u1GroupName, "CLASS-%u", u4FsMefClassCLASS);

    MefOctStrGroupName.i4_Length = STRLEN (u1GroupName);
    MefOctStrGroupName.pu1_OctetList = u1GroupName;

    if (nmhTestv2FsQoSClassToPriorityGroupName (&u4ErrorCode,
                                                u4FsMefClassCLASS,
                                                &MefOctStrGroupName)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSClassToPriorityGroupName (u4FsMefClassCLASS,
                                             &MefOctStrGroupName)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSClassToPriorityStatus (&u4ErrorCode,
                                             u4FsMefClassCLASS,
                                             ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSClassToPriorityStatus (u4FsMefClassCLASS,
                                          ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeleteClassEntries
 *
 * DESCRIPTION      : This function deletes the tMefClassInfo node from the
 *                    MEF MefClass table and removes the PriorityMaps.
 *
 * INPUT            : pMefClassInfo - Pointer to the MefClass entry to
 *                    be deleted
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeleteClassEntries (tMefClassInfo * pMefClassInfo)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsQoSClassToPriorityStatus (&u4ErrorCode,
                                             pMefClassInfo->u4ClassId,
                                             DESTROY) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSClassToPriorityStatus (pMefClassInfo->u4ClassId,
                                          DESTROY) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeletClassEntry
 *
 * DESCRIPTION      : This function delete a tMefClassInfo  node from the
 *                    MEF MefClass table.
 *
 * INPUT            : pMefClassInfo - Pointer to the MefClass entry to 
 *                    be deleted
 *                    
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeleteClassEntry (tMefClassInfo * pMefClassInfo)
{

    if (RBTreeRemove (MEF_CLASS_TABLE, pMefClassInfo) != RB_FAILURE)
    {
        MEMSET (pMefClassInfo, 0, sizeof (tMefClassInfo));
        MemReleaseMemBlock (MEF_CLASS_POOL_ID, (UINT1 *) pMefClassInfo);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;

}

/*****************************************************************************/
/*    Function Name       : MefInitDefaultMeterInfo                          */
/*                                                                           */
/*    Description         : This function set the default value of Meter.    */
/*                                                                           */
/*    Input(s)            : u4FsMefClassMapId - Class Map Id                 */
/*                                                                           */
/*    Output(s)           : pMefMeterInfo - Pointde to the given             */
/*                          u4FsMefClassMapId entry                          */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
MefInitDefaultMeterInfo (UINT4 u4FsMefMeterId, tMefMeterInfo * pMefMeterInfo)
{
    UINT1               u1MeterName[MEF_MAX_NAME_LENGTH];

    MEMSET (pMefMeterInfo, 0, sizeof (tMefMeterInfo));

    pMefMeterInfo->u4MeterId = u4FsMefMeterId;
    pMefMeterInfo->u1ColorMode = QOS_METER_COLOR_BLIND;
    pMefMeterInfo->u1MeterType = QOS_METER_TYPE_MEF_DECOUPLED;

    MEMSET (u1MeterName, 0, MEF_MAX_NAME_LENGTH);
    SPRINTF ((CHR1 *) u1MeterName, "Meter-%u", u4FsMefMeterId);
    STRCPY (&pMefMeterInfo->au1MeterName, u1MeterName);
}

/***************************************************************************
 * FUNCTION NAME    : MefAddMeterEntry
 *
 * DESCRIPTION      : This function add a MefMeterAddNode node to the MEF
 *                    Meter table.
 *
 * INPUT            : pMefMeterInfo - pointer to Meter information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefAddMeterEntry (tMefMeterInfo * pMefMeterInfo)
{

    if (RBTreeAdd (MEF_METER_TABLE, pMefMeterInfo) != RB_SUCCESS)
    {
        MemReleaseMemBlock (MEF_METER_POOL_ID, (UINT1 *) pMefMeterInfo);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefSetMeterEntries 
 *
 * DESCRIPTION      : 
 *                    
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Valn Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefSetMeterEntries (UINT4 u4FsMefMeterId)
{
    tSNMP_OCTET_STRING_TYPE FsQoSMeterName;
    tMefMeterInfo      *pMefMeterInfo = NULL;
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = OSIX_SUCCESS;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (nmhGetFsQoSMeterStatus (u4FsMefMeterId, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsQoSMeterStatus (&u4ErrorCode, pMefMeterInfo->u4MeterId,
                                       CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsQoSMeterStatus (pMefMeterInfo->u4MeterId,
                                    CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsQoSMeterStatus (&u4ErrorCode, u4FsMefMeterId,
                                       NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsQoSMeterStatus (u4FsMefMeterId, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    FsQoSMeterName.pu1_OctetList = pMefMeterInfo->au1MeterName;
    FsQoSMeterName.i4_Length = STRLEN (pMefMeterInfo->au1MeterName);

    if (nmhTestv2FsQoSMeterName (&u4ErrorCode, u4FsMefMeterId,
                                 &FsQoSMeterName) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhSetFsQoSMeterName (u4FsMefMeterId, &FsQoSMeterName) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSMeterType
        (&u4ErrorCode, u4FsMefMeterId, pMefMeterInfo->u1MeterType) ==
        SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhSetFsQoSMeterType
        (u4FsMefMeterId, pMefMeterInfo->u1MeterType) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSMeterColorMode
        (&u4ErrorCode, u4FsMefMeterId, pMefMeterInfo->u1ColorMode) ==
        SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhSetFsQoSMeterColorMode
        (u4FsMefMeterId, pMefMeterInfo->u1ColorMode) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSMeterCIR
        (&u4ErrorCode, u4FsMefMeterId, pMefMeterInfo->u4CIR) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhSetFsQoSMeterCIR
        (u4FsMefMeterId, pMefMeterInfo->u4CIR) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSMeterCBS
        (&u4ErrorCode, u4FsMefMeterId, pMefMeterInfo->u4CBS) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhSetFsQoSMeterCBS
        (u4FsMefMeterId, pMefMeterInfo->u4CBS) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSMeterEIR
        (&u4ErrorCode, u4FsMefMeterId, pMefMeterInfo->u4EIR) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhSetFsQoSMeterEIR
        (u4FsMefMeterId, pMefMeterInfo->u4EIR) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSMeterEBS
        (&u4ErrorCode, u4FsMefMeterId, pMefMeterInfo->u4EBS) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhSetFsQoSMeterEBS
        (u4FsMefMeterId, pMefMeterInfo->u4EBS) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSMeterStatus (&u4ErrorCode, u4FsMefMeterId,
                                   ACTIVE) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    if (nmhSetFsQoSMeterStatus (u4FsMefMeterId, ACTIVE) == SNMP_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeletMeterEntry
 *
 * DESCRIPTION      : This function delete a tMefMeterInfo  node from the
 *                    MEF MefMeter table.
 *
 * INPUT            : pMefClassInfo - Pointer to the MefClass entry to 
 *                    be deleted
 *                    
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeleteMeterEntry (tMefMeterInfo * pMefMeterInfo)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsQoSMeterStatus (&u4ErrorCode,
                                   pMefMeterInfo->u4MeterId,
                                   DESTROY) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSMeterStatus (pMefMeterInfo->u4MeterId, DESTROY)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (RBTreeRemove (MEF_METER_TABLE, pMefMeterInfo) != RB_FAILURE)
    {
        MemReleaseMemBlock (MEF_METER_POOL_ID, (UINT1 *) pMefMeterInfo);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;

}

/*****************************************************************************/
/*    Function Name       : MefInitDefaultPolicyMapInfo                      */
/*                                                                           */
/*    Description         : This function set the default value of Meter.    */
/*                                                                           */
/*    Input(s)            : u4FsMefClassMapId - Class Map Id                 */
/*                                                                           */
/*    Output(s)           : pMefPolicyMapInfo - Pointde to the given         */
/*                          u4FsMefClassMapId entry                          */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
MefInitDefaultPolicyMapInfo (UINT4 u4FsPolicyMapId,
                             tMefPolicyMapInfo * pMefPolicyMapInfo)
{
    UINT1               u1PolicyMapName[MEF_MAX_NAME_LENGTH];

    MEMSET (pMefPolicyMapInfo, 0, sizeof (tMefPolicyMapInfo));

    pMefPolicyMapInfo->u4PolicyMapId = u4FsPolicyMapId;
    pMefPolicyMapInfo->u4MeterId = 0;

    MEMSET (u1PolicyMapName, 0, MEF_MAX_NAME_LENGTH);
    SPRINTF ((CHR1 *) u1PolicyMapName, "PolicyMap-%u", u4FsPolicyMapId);
    STRCPY (&pMefPolicyMapInfo->au1PolicyMapName, u1PolicyMapName);
}

/***************************************************************************
 * FUNCTION NAME    : MefAddPolicyMapEntry
 *
 * DESCRIPTION      : This function add a MefPolicyMapAddNode node to the MEF
 *                    PolicyMap table.
 *
 * INPUT            : pMefPolicyMapInfo - pointer to PolicyMap information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefAddPolicyMapEntry (tMefPolicyMapInfo * pMefPolicyMapInfo)
{

    if (RBTreeAdd (MEF_POLICY_MAP_TABLE, pMefPolicyMapInfo) != RB_SUCCESS)
    {
        MemReleaseMemBlock (MEF_POLICY_MAP_POOL_ID,
                            (UINT1 *) pMefPolicyMapInfo);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefSetPolicyMapEntries 
 *
 * DESCRIPTION      : 
 *                    
 *
 * INPUT            : i4FsEvcContextId - Context Id
 *                    i4FsEvcVlanId - Valn Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefSetPolicyMapEntries (UINT4 u4FsMefPolicyMapId)
{
    tSNMP_OCTET_STRING_TYPE FsQoSPolicyMapName;
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    tSNMP_OCTET_STRING_TYPE *pOctStrActFlag = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);
    if (pMefPolicyMapInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (nmhGetFsQoSPolicyMapStatus (u4FsMefPolicyMapId, &i4RowStatus)
        != SNMP_SUCCESS)
    {
        if (nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode,
                                           u4FsMefPolicyMapId,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsQoSPolicyMapStatus (u4FsMefPolicyMapId, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode,
                                           u4FsMefPolicyMapId,
                                           NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (nmhSetFsQoSPolicyMapStatus (u4FsMefPolicyMapId, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    FsQoSPolicyMapName.pu1_OctetList = pMefPolicyMapInfo->au1PolicyMapName;
    FsQoSPolicyMapName.i4_Length = STRLEN (pMefPolicyMapInfo->au1PolicyMapName);

    if (nmhTestv2FsQoSPolicyMapName
        (&u4ErrorCode, u4FsMefPolicyMapId, &FsQoSPolicyMapName) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSPolicyMapName
        (u4FsMefPolicyMapId, &FsQoSPolicyMapName) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSPolicyMapCLASS
        (&u4ErrorCode, u4FsMefPolicyMapId, pMefPolicyMapInfo->u4ClassId) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSPolicyMapCLASS
        (u4FsMefPolicyMapId, pMefPolicyMapInfo->u4ClassId) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsQoSPolicyMapMeterTableId
        (&u4ErrorCode, u4FsMefPolicyMapId, pMefPolicyMapInfo->u4MeterId)
        == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSPolicyMapMeterTableId
        (u4FsMefPolicyMapId, pMefPolicyMapInfo->u4MeterId) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* By Default, RED Packets should be marked as dropped in MEF */
    pMefPolicyMapInfo->u1OutProActionFlag = MEF_CLI_OUT_ACT_DROP;
    /* Allocate Memory For OutActFlag */
    pOctStrActFlag = allocmem_octetstring (MEF_POLICY_ACTION_FLAG_LENGTH);
    if (pOctStrActFlag == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);
    MEMCPY (pOctStrActFlag->pu1_OctetList,
            &pMefPolicyMapInfo->u1OutProActionFlag,
            MEF_POLICY_ACTION_FLAG_LENGTH);

    if (nmhTestv2FsQoSPolicyMapOutProfileActionFlag
        (&u4ErrorCode, u4FsMefPolicyMapId, pOctStrActFlag) == SNMP_FAILURE)
    {
        free_octetstring (pOctStrActFlag);
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSPolicyMapOutProfileActionFlag (u4FsMefPolicyMapId,
                                                  pOctStrActFlag) ==
        SNMP_FAILURE)
    {
        free_octetstring (pOctStrActFlag);
        return OSIX_FAILURE;
    }
    free_octetstring (pOctStrActFlag);

    if (nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode,
                                       u4FsMefPolicyMapId,
                                       ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsQoSPolicyMapStatus (u4FsMefPolicyMapId, ACTIVE) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeletPolicyMapEntry
 *
 * DESCRIPTION      : This function delete a tMefMeterInfo  node from the
 *                    MEF MefPolicyMap table.
 *
 * INPUT            : pMefPolicyMapInfo - Pointer to the MefPolicyMap entry to 
 *                    be deleted
 *                    
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeletePolicyMapEntry (tMefPolicyMapInfo * pMefPolicyMapInfo)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;
    tMefClassInfo      *pMefClassInfo = NULL;
    UINT4               u4ErrorCode = 0;
    INT4                i4QoSPolicyMapStatus = 0;

    if (nmhGetFsQoSPolicyMapStatus (pMefPolicyMapInfo->u4PolicyMapId,
                                    &i4QoSPolicyMapStatus) != SNMP_FAILURE)
    {
        if (pMefPolicyMapInfo->u4PolicyMapId > QOS_POL_TBL_DEF_ENTRY_MAX)
        {
            if (nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode,
                                               pMefPolicyMapInfo->u4PolicyMapId,
                                               DESTROY) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            if (nmhSetFsQoSPolicyMapStatus (pMefPolicyMapInfo->u4PolicyMapId,
                                            DESTROY) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
    }

    /* Reset corresponding  Meter ref count if the id not '0' */
    if (pMefPolicyMapInfo->u4MeterId != 0)
    {
        pMefMeterInfo = MefGetMeterEntry (pMefPolicyMapInfo->u4MeterId);
        if (pMefMeterInfo == NULL)
        {
            return OSIX_FAILURE;
        }
        /* Incremnet the RefCount of Meter Id  Entry in the Meter Table */
        pMefMeterInfo->u4RefCount = (pMefMeterInfo->u4RefCount) - 1;
    }

    /* Reset corresponding Class ref count if the id not '0' */
    if (pMefPolicyMapInfo->u4ClassId != 0)
    {
        pMefClassInfo = MefGetClassEntry (pMefPolicyMapInfo->u4ClassId);
        if (pMefClassInfo == NULL)
        {
            return OSIX_FAILURE;
        }
        /* Incremnet the RefCount of Meter Id  Entry in the Meter Table */
        pMefClassInfo->u4RefCount = (pMefClassInfo->u4RefCount) - 1;
    }

    if (RBTreeRemove (MEF_POLICY_MAP_TABLE, pMefPolicyMapInfo) != RB_FAILURE)
    {
        MemReleaseMemBlock (MEF_POLICY_MAP_POOL_ID,
                            (UINT1 *) pMefPolicyMapInfo);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;

}

/***************************************************************************
 * FUNCTION NAME    : MefCVlanEvcTableTable
 *
 * DESCRIPTION      : This function creates the UniCVlanEvc table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefCVlanEvcTableTable (VOID)
{
    MEF_CVLAN_EVC_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF
                              (tMefCVlanEvcInfo, MefCVlanEvcTableRbNode),
                              MefCVlanEvcEnrtyRBTreeCmp);
    if (MEF_CVLAN_EVC_TABLE == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : MefCVlanEvcEnrtyRBTreeCmp
 *  Description     : RBTree Compare function for u4PolicyMapId entries.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 *************************************************************************/
INT4
MefCVlanEvcEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMefCVlanEvcInfo   *pMefCVlanEvcInfoA = NULL;
    tMefCVlanEvcInfo   *pMefCVlanEvcInfoB = NULL;

    pMefCVlanEvcInfoA = (tMefCVlanEvcInfo *) pRBElem1;
    pMefCVlanEvcInfoB = (tMefCVlanEvcInfo *) pRBElem2;

    if ((pMefCVlanEvcInfoA->u4IfIndex) < (pMefCVlanEvcInfoB->u4IfIndex))
    {
        return -1;
    }
    else if ((pMefCVlanEvcInfoA->u4IfIndex) > (pMefCVlanEvcInfoB->u4IfIndex))
    {
        return 1;
    }

    if ((pMefCVlanEvcInfoA->i4CVlanId) < (pMefCVlanEvcInfoB->i4CVlanId))
    {
        return -1;
    }
    else if ((pMefCVlanEvcInfoA->i4CVlanId) > (pMefCVlanEvcInfoB->i4CVlanId))
    {
        return 1;
    }

    return 0;
}

/***************************************************************************
 * FUNCTION NAME    : MefGetCVlanEvcEntry
 *
 * DESCRIPTION      : This function returns tUniCVlanEvcInfo node from the
 *                    MEF CVlanEvc table for the given IfIndex and EvcVlanId.
 *
 * INPUT            : IfIndex - if Index
 *                    EvcVlanId - Vlan Id
 *
 * OUTPUT           : None
 *
 * RETURNS          : tUniCVlanEvcInfo - pointer to pseudowire IfIndex Map  entry
 *                    ELSE NULL
 **************************************************************************/
tMefCVlanEvcInfo   *
MefGetCVlanEvcEntry (INT4 i4IfIndex, INT4 i4FsUniCVlanEvcCVlanId)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    tMefCVlanEvcInfo    CVlanEvcInfo;

    MEMSET (&CVlanEvcInfo, 0, sizeof (tMefCVlanEvcInfo));
    CVlanEvcInfo.u4IfIndex = i4IfIndex;
    CVlanEvcInfo.i4CVlanId = i4FsUniCVlanEvcCVlanId;

    pCVlanEvcInfo = (tMefCVlanEvcInfo *) RBTreeGet
        (MEF_CVLAN_EVC_TABLE, &CVlanEvcInfo);
    if (pCVlanEvcInfo == NULL)
    {
        return (pCVlanEvcInfo);
    }

    return (pCVlanEvcInfo);
}

/***************************************************************************
 * FUNCTION NAME    : MefAddCVlanEvcEntry
 *
 * DESCRIPTION      : This function add a UniCVlanEvcEntry node to the MEF
 *                    CVlanEvc table.
 *
 * INPUT            : pCVlanEvcInfo - pointer to Evc node information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefAddCVlanEvcEntry (tMefCVlanEvcInfo * pCVlanEvcInfo)
{
    if (RBTreeAdd (MEF_CVLAN_EVC_TABLE, pCVlanEvcInfo) != RB_SUCCESS)
    {
        MemReleaseMemBlock (MEF_CVLAN_EVC_POOL_ID, (UINT1 *) pCVlanEvcInfo);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeleteCVlanEvcEntry
 *
 * DESCRIPTION      : This function delete a tUniCVlanEvcInfo  node from the
 *                    MEF CVlanEvc table.
 *
 * INPUT            : pCVlanEvcInfo - Pointer to be deleted
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeleteCVlanEvcEntry (tMefCVlanEvcInfo * pCVlanEvcInfo)
{

    if (RBTreeRemove (MEF_CVLAN_EVC_TABLE, pCVlanEvcInfo) != RB_FAILURE)
    {
        MemReleaseMemBlock (MEF_CVLAN_EVC_POOL_ID, (UINT1 *) pCVlanEvcInfo);
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

#ifdef MPLS_WANTED
/***************************************************************************
 * FUNCTION NAME    : MefSetMPLSCVlanEvcEntry
 *
 * DESCRIPTION      : This function map the UNI to VPLS.
 *
 * INPUT            : pCVlanEvcInfo - Pointer to be deleted
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefSetMPLSCVlanEvcEntry (tMefCVlanEvcInfo * pCVlanEvcInfo)
{

    UINT4               u4VplsConfigIndex = 0;
    UINT4               pu4NextVplsConfigIndex = 0;
    UINT4               pu4NextPwIndex = 0;
    UINT4               u4PwIndex = 0;
    UINT4               u4EnetInstance = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4MplsVplsL2MapFdbId = 0;

    MPLS_L2VPN_LOCK ();
    for (u4VplsConfigIndex = 1; u4VplsConfigIndex <= MAX_L2VPN_PW_VC_ENTRIES;
         u4VplsConfigIndex++)
    {
        nmhGetFsMplsVplsL2MapFdbId (u4VplsConfigIndex, &i4MplsVplsL2MapFdbId);
        /*Comparing the EVC entry and Fdb Id */
        if (i4MplsVplsL2MapFdbId == pCVlanEvcInfo->i4EvcIndex)
        {
            break;
        }

    }
    pu4NextVplsConfigIndex = u4VplsConfigIndex;
    /*loop for Maximum VPLS Entry */
    for (; u4VplsConfigIndex <= MAX_L2VPN_PW_VC_ENTRIES;)
    {

        if (nmhGetNextIndexVplsPwBindTable
            (u4VplsConfigIndex, &pu4NextVplsConfigIndex, u4PwIndex,
             &pu4NextPwIndex) == SNMP_FAILURE)
        {
            break;
        }

        if (u4VplsConfigIndex != pu4NextVplsConfigIndex)
        {
            break;
        }
        u4PwIndex = pu4NextPwIndex;

        if (nmhGetFsMplsL2VpnNextFreePwEnetPwInstance (u4PwIndex,
                                                       &u4EnetInstance)
            == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhTestv2PwEnetRowStatus (&u4ErrorCode, u4PwIndex, u4EnetInstance,
                                      CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhSetPwEnetRowStatus (u4PwIndex, u4EnetInstance, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhTestv2PwEnetPortVlan (&u4ErrorCode, u4PwIndex, u4EnetInstance,
                                     pCVlanEvcInfo->i4CVlanId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhSetPwEnetPortVlan (u4PwIndex, u4EnetInstance,
                                  pCVlanEvcInfo->i4CVlanId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhTestv2PwEnetPwVlan (&u4ErrorCode, u4PwIndex, u4EnetInstance,
                                   pCVlanEvcInfo->i4CVlanId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhSetPwEnetPwVlan (u4PwIndex, u4EnetInstance,
                                pCVlanEvcInfo->i4CVlanId) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhTestv2PwEnetPortIfIndex (&u4ErrorCode, u4PwIndex, u4EnetInstance,
                                        pCVlanEvcInfo->u4IfIndex) ==
            SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhSetPwEnetPortIfIndex (u4PwIndex, u4EnetInstance,
                                     pCVlanEvcInfo->u4IfIndex) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhTestv2PwEnetVlanMode (&u4ErrorCode, u4PwIndex,
                                     u4EnetInstance, MEF_VLAN_MODE_NO_CHANG)
            == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhSetPwEnetVlanMode (u4PwIndex, u4EnetInstance,
                                  MEF_VLAN_MODE_NO_CHANG) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhTestv2PwEnetRowStatus (&u4ErrorCode, u4PwIndex,
                                      u4EnetInstance, ACTIVE) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhSetPwEnetRowStatus (u4PwIndex, u4EnetInstance,
                                   ACTIVE) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
    }
    MPLS_L2VPN_UNLOCK ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeleteMPLSCVlanEvcEntry
 *
 * DESCRIPTION      : This function unmap the UNI to VPLS.
 *
 * INPUT            : pCVlanEvcInfo - Pointer to be deleted
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeleteMPLSCVlanEvcEntry (tMefCVlanEvcInfo * pCVlanEvcInfo)
{
    UINT1               u1Flag = TRUE;
    UINT4               u4VplsConfigIndex = 0;
    UINT4               pu4NextVplsConfigIndex = 0;
    UINT4               pu4NextPwIndex = 0;
    UINT4               u4PwIndex = 0;
    UINT4               u4EnetInstance = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4PwEnetPwVlan = 0;
    UINT4               u4PwEnetPortIfIndex = 0;
    INT4                i4MplsVplsL2MapFdbId = 0;

    MPLS_L2VPN_LOCK ();
    for (u4VplsConfigIndex = 1; u4VplsConfigIndex <= MAX_L2VPN_PW_VC_ENTRIES;
         u4VplsConfigIndex++)
    {
        nmhGetFsMplsVplsL2MapFdbId (u4VplsConfigIndex, &i4MplsVplsL2MapFdbId);

        /*Comparing the EVC entry and Fdb Id */
        if (i4MplsVplsL2MapFdbId == pCVlanEvcInfo->i4EvcIndex)
        {
            break;
        }

    }
    pu4NextVplsConfigIndex = u4VplsConfigIndex;
    /*loop for max VPLS ENTRY,this will search the (port,vlan) as AC and delete the mappping */
    for (; u4VplsConfigIndex <= MAX_L2VPN_PW_VC_ENTRIES;)
    {

        if (nmhGetNextIndexVplsPwBindTable
            (u4VplsConfigIndex, &pu4NextVplsConfigIndex, u4PwIndex,
             &pu4NextPwIndex) == SNMP_FAILURE)
        {
            break;
        }

        if (u4VplsConfigIndex != pu4NextVplsConfigIndex)
        {
            break;
        }
        u4PwIndex = pu4NextPwIndex;

        if (u1Flag == TRUE)
        {
            /*this loop is for findig the Enet instance associated with pseudowire */
            for (u4EnetInstance = 1;
                 u4EnetInstance < MAX_L2VPN_ENET_ENTRIES; u4EnetInstance++)
            {
                nmhGetPwEnetPortVlan (u4PwIndex, u4EnetInstance,
                                      &u4PwEnetPwVlan);
                if (u4PwEnetPwVlan == (UINT4) (pCVlanEvcInfo->i4CVlanId))
                {
                    nmhGetPwEnetPortIfIndex (u4PwIndex, u4EnetInstance,
                                             (INT4 *) &u4PwEnetPortIfIndex);
                    if (u4PwEnetPortIfIndex == pCVlanEvcInfo->u4IfIndex)
                    {
                        u1Flag = FALSE;
                        break;
                    }
                }
            }
        }

        if (nmhTestv2PwEnetRowStatus (&u4ErrorCode, u4PwIndex,
                                      u4EnetInstance, DESTROY) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
        if (nmhSetPwEnetRowStatus (u4PwIndex, u4EnetInstance,
                                   DESTROY) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return OSIX_FAILURE;
        }
    }
    MPLS_L2VPN_UNLOCK ();
    return OSIX_SUCCESS;
}
#endif
/*****************************************************************************/
/*    Function Name       : MefUpdateUniL2cpInfo                             */
/*                                                                           */
/*    Description         : This function set the default value of UNI L2cp. */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pMefUniInfo - pointer of UNI node.               */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
MefUpdateUniL2cpInfo (tMefUniInfo * pMefUniInfo)
{
    if (pMefUniInfo->bAllToOneBundling == MEF_ENABLED)
    {
        pMefUniInfo->u1StpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
        pMefUniInfo->u1LacpTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
        pMefUniInfo->u1Dot1xTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
        pMefUniInfo->u1ElmiTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
        pMefUniInfo->u1LldpTunnelingStatus = MEF_TUNNEL_PROTOCOL_DISCARD;
        pMefUniInfo->u1GvrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
        pMefUniInfo->u1MvrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
        pMefUniInfo->u1GmrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
        pMefUniInfo->u1MmrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    else
    {
        pMefUniInfo->u1StpTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
        pMefUniInfo->u1LacpTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
        pMefUniInfo->u1Dot1xTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
        pMefUniInfo->u1ElmiTunnelingStatus = MEF_TUNNEL_PROTOCOL_PEER;
        pMefUniInfo->u1LldpTunnelingStatus = MEF_TUNNEL_PROTOCOL_DISCARD;
        pMefUniInfo->u1GvrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
        pMefUniInfo->u1MvrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
        pMefUniInfo->u1GmrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
        pMefUniInfo->u1MmrpTunnelingStatus = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
}

/*****************************************************************************/
/*    Function Name       : MefActOnServiceMultiplexingChange                */
/*                                                                           */
/*    Description         : This function will remove all the entries for    */
/*                          the provided UNI from pMefCVlanEvcInfo           */
/*                                                                           */
/*    Input(s)            : pMefUniInfo - pointer of UNI node.               */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
INT4
MefActOnServiceMultiplexingChange (tMefUniInfo * pMefUniInfo)
{
    tMefCVlanEvcInfo    CVlanEvcInfo;
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    UINT4               u4ErrorCode = 0;

    MEMSET (&CVlanEvcInfo, 0, sizeof (tMefCVlanEvcInfo));
    CVlanEvcInfo.u4IfIndex = pMefUniInfo->i4IfIndex;
    CVlanEvcInfo.i4CVlanId = 0;

    pCVlanEvcInfo =
        (tMefCVlanEvcInfo *) RBTreeGetNext (MEF_CVLAN_EVC_TABLE, &CVlanEvcInfo,
                                            NULL);

    while (pCVlanEvcInfo != NULL)
    {
        if (pCVlanEvcInfo->u4IfIndex != (UINT4) pMefUniInfo->i4IfIndex)
        {
            break;
        }

        VLAN_LOCK ();
        if (nmhTestv2Dot1adMICVidRegistrationRowStatus (&u4ErrorCode,
                                                        pCVlanEvcInfo->
                                                        u4IfIndex,
                                                        pCVlanEvcInfo->
                                                        i4CVlanId,
                                                        DESTROY) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (nmhSetDot1adMICVidRegistrationRowStatus (pCVlanEvcInfo->u4IfIndex,
                                                     pCVlanEvcInfo->i4CVlanId,
                                                     DESTROY) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }
        VLAN_UNLOCK ();

        CVlanEvcInfo.i4CVlanId = pCVlanEvcInfo->i4CVlanId;

        if (RBTreeRemove (MEF_CVLAN_EVC_TABLE, pCVlanEvcInfo) != RB_FAILURE)
        {
            MemReleaseMemBlock (MEF_CVLAN_EVC_POOL_ID, (UINT1 *) pCVlanEvcInfo);
        }

        pCVlanEvcInfo =
            (tMefCVlanEvcInfo *) RBTreeGetNext (MEF_CVLAN_EVC_TABLE,
                                                &CVlanEvcInfo, NULL);
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefIsMsrInProgress
 *
 * DESCRIPTION      : This function returns TRUE if mib save status is 
 *                    in progress 
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_TRUE/OSIX_FALSE
 *
 **************************************************************************/
INT4
MefIsMsrInProgress (VOID)
{
    if (MsrisRetoreinProgress == ISS_TRUE)
    {
        return OSIX_TRUE;
    }
    return OSIX_FALSE;
}

/***************************************************************************
 * FUNCTION NAME    : MefSetUniCVlanEvcEntries
 *
 * DESCRIPTION      : This function sets fsDot1qPvid value if the bridge port
 *                    type is CNP, else configures dot1adMICVidRegistrationTable
 *                    table.
 *
 * INPUT            : pCVlanEvcInfo - Pointer to the tMefCVlanEvcInfo entry.
 *                    
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefSetUniCVlanEvcEntries (tMefCVlanEvcInfo * pCVlanEvcInfo)
{
    tMefEvcInfo        *pEvcInfo = NULL;
    tMefUniInfo        *pMefUniInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4ErrorCode = 0;
    UINT2               u2LocalPort = 0;
    INT4                i4BrgPortType = 0;
    INT4                i4RowStatus = 0;
    INT4                i4UntaggedCep = VLAN_SNMP_FALSE;
    INT4                i4UntaggedPep = VLAN_SNMP_FALSE;
    INT1                i1RetVal = SNMP_FAILURE;

    CfaGetInterfaceBrgPortType (pCVlanEvcInfo->u4IfIndex, &i4BrgPortType);

    if (MefGetContexId (pCVlanEvcInfo->u4IfIndex, &u4ContextId, &u2LocalPort)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pEvcInfo = MefGetEvcEntry (u4ContextId, pCVlanEvcInfo->i4EvcIndex);
    if (pEvcInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    VLAN_LOCK ();

    if (i4BrgPortType == MEF_CNP_PORTBASED_PORT)
    {
        if (nmhTestv2FsDot1qPvid (&u4ErrorCode, pCVlanEvcInfo->u4IfIndex,
                                  pCVlanEvcInfo->i4EvcIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (nmhSetFsDot1qPvid
            (pCVlanEvcInfo->u4IfIndex,
             pCVlanEvcInfo->i4EvcIndex) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        VLAN_UNLOCK ();
        pCVlanEvcInfo->u1RowStatus = ACTIVE;
        return OSIX_SUCCESS;
    }
    if (nmhGetDot1adMICVidRegistrationRowStatus (pCVlanEvcInfo->u4IfIndex,
                                                 pCVlanEvcInfo->i4CVlanId,
                                                 &i4RowStatus) != SNMP_SUCCESS)
    {
        if (nmhTestv2Dot1adMICVidRegistrationRowStatus (&u4ErrorCode,
                                                        pCVlanEvcInfo->
                                                        u4IfIndex,
                                                        pCVlanEvcInfo->
                                                        i4CVlanId,
                                                        CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (nmhSetDot1adMICVidRegistrationRowStatus (pCVlanEvcInfo->u4IfIndex,
                                                     pCVlanEvcInfo->i4CVlanId,
                                                     CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (nmhTestv2Dot1adMICVidRegistrationSVid (&u4ErrorCode,
                                                   pCVlanEvcInfo->u4IfIndex,
                                                   pCVlanEvcInfo->i4CVlanId,
                                                   pCVlanEvcInfo->i4EvcIndex)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (nmhSetDot1adMICVidRegistrationSVid (pCVlanEvcInfo->u4IfIndex,
                                                pCVlanEvcInfo->i4CVlanId,
                                                pCVlanEvcInfo->i4EvcIndex)
            == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        pMefUniInfo = MefGetUniEntry (pCVlanEvcInfo->u4IfIndex);
        if (pMefUniInfo == NULL)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if ((pEvcInfo->bCeVlanIdPreservation == MEF_DISABLED) ||
            (pEvcInfo->bCeVlanCoSPreservation == MEF_DISABLED))
        {
            i4UntaggedPep = VLAN_SNMP_TRUE;
            i1RetVal =
                nmhTestv2Dot1adMICVidRegistrationUntaggedPep (&u4ErrorCode,
                                                              (INT4)
                                                              pCVlanEvcInfo->
                                                              u4IfIndex,
                                                              pCVlanEvcInfo->
                                                              i4CVlanId,
                                                              i4UntaggedPep);
            if ((CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS)
                && (u4ErrorCode == CLI_PB_UNTAGPEP_ERR))
            {
                i4UntaggedPep = VLAN_SNMP_FALSE;
            }

        }

        if (nmhSetDot1adMICVidRegistrationUntaggedPep (pCVlanEvcInfo->u4IfIndex,
                                                       pCVlanEvcInfo->i4CVlanId,
                                                       i4UntaggedPep) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (pCVlanEvcInfo->i4CVlanId == (INT4) pMefUniInfo->u2DefaultCeVlanId)
        {
            i4UntaggedCep = VLAN_SNMP_TRUE;
        }

        if (nmhSetDot1adMICVidRegistrationUntaggedCep (pCVlanEvcInfo->u4IfIndex,
                                                       pCVlanEvcInfo->i4CVlanId,
                                                       i4UntaggedCep) ==
            SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (nmhTestv2Dot1adMICVidRegistrationRowStatus (&u4ErrorCode,
                                                        pCVlanEvcInfo->
                                                        u4IfIndex,
                                                        pCVlanEvcInfo->
                                                        i4CVlanId,
                                                        ACTIVE) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (nmhSetDot1adMICVidRegistrationRowStatus (pCVlanEvcInfo->u4IfIndex,
                                                     pCVlanEvcInfo->i4CVlanId,
                                                     ACTIVE) == SNMP_FAILURE)
        {
            VLAN_UNLOCK ();
            return OSIX_FAILURE;
        }
    }
    UNUSED_PARAM (i1RetVal);
    VLAN_UNLOCK ();

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    :  MefValidateEvcIndexForTransMode
 *
 * DESCRIPTION      : This function Validate the Evc Index is valid for transport 
 *                    Mode or not.
 *
 * INPUT            : u4EvcIndex -  EVC index.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefValidateEvcIndexForTransMode (UINT4 u4ContextId, INT4 i4EvcIndex)
{

    if (i4EvcIndex > MEF_VPLS_VLAN_MAX)
    {
        return OSIX_FAILURE;
    }

    if (FsMefUtilGetTransportMode (u4ContextId) == MEF_TRANS_MODE_PB)
    {
        if (i4EvcIndex > MEF_VPLS_VLAN_MIN)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (i4EvcIndex < MEF_VPLS_VLAN_MIN)
        {
            return OSIX_FAILURE;
        }

    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    :  MefDeleteCvlanEvcMapEntries
 *
 * DESCRIPTION      : This function deletes all the Cvlan to EVC map entries 
 *                    from the CvlanEvcMapTable for the given IfIndex.
 *
 * INPUT            : i4IfIndex -  Interface index.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeleteCvlanEvcMapEntries (INT4 i4IfIndex)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    tMefCVlanEvcInfo    CVlanEvcInfo;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    INT4                i4SetValFsUniCVlanEvcRowStatus = DESTROY;

    MEMSET (&CVlanEvcInfo, 0, sizeof (tMefCVlanEvcInfo));
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    CVlanEvcInfo.u4IfIndex = (UINT4) i4IfIndex;
    CVlanEvcInfo.i4CVlanId = 0;

    pCVlanEvcInfo = (tMefCVlanEvcInfo *) RBTreeGetNext
        (MEF_CVLAN_EVC_TABLE, &CVlanEvcInfo, NULL);

    while ((pCVlanEvcInfo != NULL) &&
           (pCVlanEvcInfo->u4IfIndex == (UINT4) i4IfIndex))
    {
        CVlanEvcInfo.u4IfIndex = pCVlanEvcInfo->u4IfIndex;
        CVlanEvcInfo.i4CVlanId = pCVlanEvcInfo->i4CVlanId;
        MefDeleteCVlanEvcEntry (pCVlanEvcInfo);

        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                              FsUniCVlanEvcRowStatus, u4SeqNum,
                              TRUE, MefApiLock, MefApiUnLock, 2, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                          i4IfIndex, CVlanEvcInfo.i4CVlanId,
                          i4SetValFsUniCVlanEvcRowStatus));

        pCVlanEvcInfo = (tMefCVlanEvcInfo *)
            RBTreeGetNext (MEF_CVLAN_EVC_TABLE, &CVlanEvcInfo, NULL);
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefUniListCreateTable
 *
 * DESCRIPTION      : This function creates the Uni List table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefUniListCreateTable (VOID)
{
    MEF_UNI_LIST_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMefUniList, MefUniListRbNode),
                              MefUniListEntryRBTreeCmp);
    if (MEF_UNI_LIST_TABLE == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : MefUniListEbtryRBTreeCmp
 *  Description     : RBTree Compare function for two Uni List entries.
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 *************************************************************************/
INT4
MefUniListEntryRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tMefUniList        *pMefUniListInfoA = NULL;
    tMefUniList        *pMefUniListInfoB = NULL;

    pMefUniListInfoA = (tMefUniList *) pRBElem1;
    pMefUniListInfoB = (tMefUniList *) pRBElem2;

    if ((pMefUniListInfoA->u4ContextId) < (pMefUniListInfoB->u4ContextId))
    {
        return -1;
    }
    else if ((pMefUniListInfoA->u4ContextId) > (pMefUniListInfoB->u4ContextId))
    {
        return 1;
    }

    if ((pMefUniListInfoA->i4EvcIndex) < (pMefUniListInfoB->i4EvcIndex))
    {
        return -1;
    }
    else if ((pMefUniListInfoA->i4EvcIndex) > (pMefUniListInfoB->i4EvcIndex))
    {
        return 1;
    }

    if ((pMefUniListInfoA->i4IfIndex) < (pMefUniListInfoB->i4IfIndex))
    {
        return -1;
    }
    else if ((pMefUniListInfoA->i4IfIndex) > (pMefUniListInfoB->i4IfIndex))
    {
        return 1;
    }
    return 0;

}

/***************************************************************************
 * FUNCTION NAME    : MefGetUniListEntry
 *
 * DESCRIPTION      : This function Gets a tMefUniList node from the
 *                    MEF UNI List table.
 *
 * INPUT            : i4IfIndex - Interface Index
 *                    U4ContextId - context id
 *                    i4EvcIndex  -Evc Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tMefUniList - pointer to UNI List  entry
 *                    ELSE NULL
 **************************************************************************/
tMefUniList        *
MefGetUniListEntry (UINT4 u4ContextId, INT4 i4EvcIndex, INT4 i4IfIndex)
{
    tMefUniList         MefUniListEntry;
    tMefUniList        *pMefUniListEntry = NULL;

    MEMSET (&MefUniListEntry, 0, sizeof (tMefUniList));
    MefUniListEntry.u4ContextId = u4ContextId;
    MefUniListEntry.i4EvcIndex = i4EvcIndex;
    MefUniListEntry.i4IfIndex = i4IfIndex;
    pMefUniListEntry = RBTreeGet (MEF_UNI_LIST_TABLE, &MefUniListEntry);

    return (pMefUniListEntry);
}

/***************************************************************************
 * FUNCTION NAME    : MefAddUniListEntry
 *
 * DESCRIPTION      : This function add a tMefUniList node to the MEF
 *                    Uni List table.
 *
 * INPUT            : pMefUniListInfo - pointer to UniList information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
MefAddUniListEntry (tMefUniList * pMefUniListInfo)
{

    if (RBTreeAdd (MEF_UNI_LIST_TABLE, pMefUniListInfo) != RB_SUCCESS)
    {
        MemReleaseMemBlock (MEF_UNI_LIST_POOL_ID, (UINT1 *) pMefUniListInfo);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefDeleteUniListEntry
 *
 * DESCRIPTION      : This function delete a tMefUniList  node from the
 *                    Uni List table.
 *
 * INPUT            : pMefUniListInfo - pointer to UniList information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefDeleteUniListEntry (tMefUniList * pMefUniListInfo)
{
    if (RBTreeRemove (MEF_UNI_LIST_TABLE, pMefUniListInfo) != RB_FAILURE)
    {
        MemReleaseMemBlock (MEF_UNI_LIST_POOL_ID, (UINT1 *) pMefUniListInfo);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : MefUniListDeleteTable
 *
 * DESCRIPTION      : This function deletes the MefUniList table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
VOID
MefUniListDeleteTable (VOID)
{
    tMefUniList        *pMefUniListInfo = NULL;
    pMefUniListInfo = RBTreeGetFirst (MEF_UNI_LIST_TABLE);

    while (pMefUniListInfo != NULL)
    {
        RBTreeRemove (MEF_UNI_LIST_TABLE, pMefUniListInfo);

        MemReleaseMemBlock (MEF_UNI_LIST_POOL_ID, (UINT1 *) pMefUniListInfo);

        pMefUniListInfo = RBTreeGetFirst (MEF_UNI_LIST_TABLE);
    }

    RBTreeDestroy (MEF_UNI_LIST_TABLE, NULL, 0);
    MEF_UNI_LIST_TABLE = NULL;

    return;
}

/***************************************************************************
 * FUNCTION NAME    :  MefGetFreeEvcId
 *
 * DESCRIPTION      : This function used to generate the EVC ID when EVC
 *                    get created.
 *
 * INPUT            : u4EvcIndex -  EVC index.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
UINT4
MefGetFreeEvcId (INT4 i4FsEvcIndex)
{

    UINT4               u4Index = 0;
    tEvcInfo           *pIssEvcInfo = NULL;

    pIssEvcInfo = MefGetEvcInfoEntry (i4FsEvcIndex);

    if (pIssEvcInfo == NULL)
    {
        for (u4Index = 1; u4Index <= VLAN_MAX_VLAN_ID + 1; u4Index++)
        {
            if (gau4EvcIndex[u4Index - 1] == 0)
            {
                gau4EvcIndex[u4Index - 1] = u4Index;
                return (u4Index);
            }
        }
    }

    return OSIX_FAILURE;
}

#ifdef Y1564_WANTED
/***************************************************************************
 * FUNCTION NAME    :  MefUtilCheckEvcEntryPresence
 *
 * DESCRIPTION      : This function used to check whether the given EVC ID
 *                    is present in MEF or not.
 *
 * INPUT            :  u4ContextId - Context Identifier
 *                     u4EvcIndex -  EVC index.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefUtilCheckEvcEntryPresence (UINT4 u4ContextId, UINT4 u4EvcId)
{
    tMefEvcInfo        *pEvcInfo = NULL;
    INT4                i4FsEvcFilterNo = MEF_ZERO;
    INT4                i4NextFsEvcFilterNo = MEF_ZERO;
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4FilterEvcIndex = MEF_ZERO;

    MefApiLock ();

    pEvcInfo = MefGetEvcEntry (u4ContextId, u4EvcId);

    if (pEvcInfo == NULL)
    {
        MefApiUnLock ();
        return OSIX_FAILURE;
    }

    while (i4RetVal == SNMP_SUCCESS)
    {
        if (nmhGetNextIndexFsMefFilterTable
            (i4FsEvcFilterNo, &i4NextFsEvcFilterNo) == SNMP_FAILURE)
        {
            MefApiUnLock ();
            return OSIX_FAILURE;
        }

        nmhGetFsMefFilterEvc (i4NextFsEvcFilterNo, &i4FilterEvcIndex);

        if (u4EvcId != (UINT4) i4FilterEvcIndex)
        {
            i4FsEvcFilterNo = i4NextFsEvcFilterNo;
            continue;
        }
        MefApiUnLock ();
        return OSIX_SUCCESS;
    }
    MefApiUnLock ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    :  MefUtilGetBanwidthProfile
 *
 * DESCRIPTION      :  This function fetches the bandwidth profile parameters.
 *
 * INPUT            :  u4ContextId - Context Identifier
 *                     u4EvcIndex -  EVC index.
 *
 * OUTPUT           :  pu4MefMeterCIR - CIR
 *                     pu4MefMeterCBS - CBS
 *                     pu4MefMeterEIR - EIR
 *                     pu4MefMeterEBS - EBS
 *                     pu1MefMeterColorMode - Color Mode
 *                     pu1MefMeterType - Coupling Flag
 *
 * RETURNS          : OSIX_FAILURE/OSIX_SUCCESS
 *
 **************************************************************************/
INT4
MefUtilGetBanwidthProfile (UINT4 u4ContextId, UINT4 u4EvcId,
                           UINT4 *pu4MefMeterCIR, UINT4 *pu4MefMeterCBS,
                           UINT4 *pu4MefMeterEIR, UINT4 *pu4MefMeterEBS,
                           UINT1 *pu1MefMeterColorMode, UINT1 *pu1MefMeterType)
{
    INT4                i4FilterIfIndex = MEF_ZERO;
    INT4                i4FsEvcIndex = MEF_ZERO;
    INT4                i4FilterEvcIndex = MEF_ZERO;
    INT4                i4FsEvcFilterNo = MEF_ZERO;
    INT4                i4NextFsEvcFilterNo = MEF_ZERO;
    INT4                i4ColorMode = MEF_ZERO;
    INT4                i4MeterType = MEF_ZERO;
    UINT4               u4ClassMapId = MEF_ZERO;
    UINT4               u4NextClassMapId = MEF_ZERO;
    UINT4               u4ClassMapFilterId = MEF_ZERO;
    UINT4               u4ClassMapCLASS = MEF_ZERO;
    UINT4               u4PolicyMapId = MEF_ZERO;
    UINT4               u4NextPolicyMapId = MEF_ZERO;
    UINT4               u4PolicyMapCLASS = MEF_ZERO;
    UINT4               u4PolicyMapMeterTableId = MEF_ZERO;
    UINT4               u4CIR = MEF_ZERO;
    UINT4               u4CBS = MEF_ZERO;
    UINT4               u4EIR = MEF_ZERO;
    UINT4               u4EBS = MEF_ZERO;

    INT1                i1RetVal = SNMP_SUCCESS;

    /* According to MEF standard there must be one bandwidth profile mapping
     * to a EVC, so fetching all the filter information witj  IfIndex and EVC Id*/
    i4FsEvcIndex = (INT4) u4EvcId;
    i4FsEvcFilterNo = MEF_ZERO;

    MefApiLock ();
    if (nmhValidateIndexInstanceFsEvcTable ((INT4) u4ContextId,
                                            (INT4) u4EvcId) == SNMP_FAILURE)
    {
        MefApiUnLock ();
        return OSIX_FAILURE;
    }

    while (i1RetVal == SNMP_SUCCESS)
    {
        i1RetVal = nmhGetNextIndexFsMefFilterTable (i4FsEvcFilterNo,
                                                    &i4NextFsEvcFilterNo);

        if (i1RetVal == SNMP_FAILURE)
        {
            MefApiUnLock ();
            return OSIX_FAILURE;
        }

        nmhGetFsMefFilterIfIndex (i4NextFsEvcFilterNo, &i4FilterIfIndex);
        nmhGetFsMefFilterEvc (i4NextFsEvcFilterNo, &i4FilterEvcIndex);

        if (i4FsEvcIndex != i4FilterEvcIndex)
        {
            i4FsEvcFilterNo = i4NextFsEvcFilterNo;
            continue;
        }

        /* With the Filter information fetch the Class Id
         * Scan the class map table and check if any class is mapped
         * is mapped for this filter number */

        u4ClassMapId = MEF_ZERO;

        while (nmhGetNextIndexFsMefClassMapTable (u4ClassMapId,
                                                  &u4NextClassMapId) !=
               SNMP_FAILURE)
        {
            nmhGetFsMefClassMapFilterId (u4NextClassMapId, &u4ClassMapFilterId);

            if (u4ClassMapFilterId != (UINT4) i4NextFsEvcFilterNo)
            {
                u4ClassMapId = u4NextClassMapId;
                continue;
            }

            /* Filter and the matching class Id are retrieved.
             * Scan the Policymap table to find the meter Information
             * for the corresponding class Id */
            nmhGetFsMefClassMapCLASS (u4NextClassMapId, &u4ClassMapCLASS);

            u4PolicyMapId = MEF_ZERO;

            while (nmhGetNextIndexFsMefPolicyMapTable (u4PolicyMapId,
                                                       &u4NextPolicyMapId) !=
                   SNMP_FAILURE)
            {

                nmhGetFsMefPolicyMapCLASS (u4NextPolicyMapId,
                                           &u4PolicyMapCLASS);

                if (u4PolicyMapCLASS != u4ClassMapCLASS)
                {
                    u4PolicyMapId = u4NextPolicyMapId;
                    continue;
                }

                /* Meter ID is found for the EVC and context ID
                 * Fill the corresponding information to the SLA Entry */
                nmhGetFsMefPolicyMapMeterTableId (u4NextPolicyMapId,
                                                  &u4PolicyMapMeterTableId);

                nmhGetFsMefMeterCIR (u4PolicyMapMeterTableId, &u4CIR);
                *pu4MefMeterCIR = u4CIR;

                nmhGetFsMefMeterCBS (u4PolicyMapMeterTableId, &u4CBS);
                *pu4MefMeterCBS = u4CBS;

                nmhGetFsMefMeterEIR (u4PolicyMapMeterTableId, &u4EIR);
                *pu4MefMeterEIR = u4EIR;

                nmhGetFsMefMeterEBS (u4PolicyMapMeterTableId, &u4EBS);
                *pu4MefMeterEBS = u4EBS;

                nmhGetFsMefMeterColorMode (u4PolicyMapMeterTableId,
                                           &i4ColorMode);
                *pu1MefMeterColorMode = (UINT1) i4ColorMode;

                nmhGetFsMefMeterType (u4PolicyMapMeterTableId, &i4MeterType);

                if (i4MeterType == QOS_METER_TYPE_MEF_DECOUPLED)
                {
                    *pu1MefMeterType = MEF_ZERO;
                }
                else if (i4MeterType == QOS_METER_TYPE_MEF_COUPLED)
                {
                    *pu1MefMeterType = QOS_METER_TYPE_MEF_COUPLED -
                        QOS_METER_TYPE_MEF_DECOUPLED;
                }

                MefApiUnLock ();
                return OSIX_SUCCESS;
            }

            /* Get the Next class Entry for the filter */
            u4ClassMapId = u4NextClassMapId;
        }

        /*Get the next filter Information */
        i4FsEvcFilterNo = i4NextFsEvcFilterNo;
    }

    MefApiUnLock ();
    return OSIX_FAILURE;
}
#endif /* Y1564_WANTED */

#endif
