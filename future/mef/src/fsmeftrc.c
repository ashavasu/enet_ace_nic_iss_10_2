/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmeftrc.c,v 1.1 2012/04/30 10:49:54 siva Exp $
 *
 * Description:
 *********************************************************************/
#ifndef __FSMEFTRC_C__
#define __FSMEFTRC_C__

#include "fsmefinc.h"

/****************************************************************************
 * Function     : MefTrace
 *
 * Description  : Converts variable argument in to string send to Trace function
 *                function
 *
 * Input        : c1FunName - Function name
 *                u4LineNum - Line number
 *                u4TraceType  - Trace flag
 *                fmt  - format strong, variable argument
 *
 * Output       : None
 *
 * Returns      : NONE
 *
 *****************************************************************************/
VOID
MefTrace (CONST CHR1 *c1FunName, UINT4 u4LineNum, UINT4 TraceType,
          CONST CHR1 *fmt, ...)
{
    PRIVATE CHR1        ac1ModuleString[MEF_MAX_TRC_LEN] = {0};
    va_list             ap;
    UINT4               u4OffSet = 0;

    UNUSED_PARAM (c1FunName);
    UNUSED_PARAM (u4LineNum);
    UNUSED_PARAM (TraceType);

    SNPRINTF (ac1ModuleString, sizeof (ac1ModuleString), "MEF: ");
    u4OffSet = STRLEN (ac1ModuleString);
    va_start (ap, fmt);
    vsprintf (&ac1ModuleString[u4OffSet], fmt, ap);
    va_end (ap);

    UtlTrcPrint ((CONST CHR1 *) ac1ModuleString);
}

#endif
