/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmefque.c,v 1.1 2012/04/30 10:49:54 siva Exp $
 *
 * Description:
 * *********************************************************************/
#ifndef __FSMEFQ_C__
#define __FSMEFQ_C__

#include "fsmefinc.h"
/******************************************************************************
 * FUNCTION NAME    : MefPostMessageToQ                                       *
 *                                                                            *
 * DESCRIPTION      : Function to post configuration message and events to    *
 *                    MEF task                                                *
 *                                                                            *
 * INPUT            : pMefMsg - Pointer to msg to be posted                   *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                               *
 *                                                                            *
 ******************************************************************************/
INT4
MefPostMessageToQ (tMefMsg *pMefMsg)
{
    if (OsixSendToQ (SELF, MEF_QUEUE_NAME, (tOsixMsg *) pMefMsg,
                     OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        MEF_TRC (ALL_FAILURE_TRC, "Failed to Post Message to Queue \r\n");
        return OSIX_FAILURE;
    }

    if (OsixSendEvent (SELF, MEF_TASK_NAME, MEF_MSG_IN_Q_EVENT) != OSIX_SUCCESS)
    {
        MEF_TRC (ALL_FAILURE_TRC, "Failed to Send MEF_MSG_IN_Q_EVENT \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*   Function Name       : MefQueueMsgHandler                               */
/*                                                                          */
/*   Description         : This function processes messages received by     */
/*                         MEF task to indicate occurance of event.         */
/*                                                                          */
/*   Input(s)            : None.                                            */
/*                                                                          */
/*   Output(s)           : None                                             */
/*                                                                          */
/*   Returns             : VOID.                                            */
/*****************************************************************************/
VOID
MefQueueMsgHandler ()
{
    tMefMsg            *pMefMsg = NULL;
    tOsixMsg           *pOsixMsg = NULL;

    while (OsixReceiveFromQ (SELF, MEF_QUEUE_NAME, OSIX_NO_WAIT, 0,
                             &(pOsixMsg)) == OSIX_SUCCESS)
    {
        pMefMsg = (tMefMsg *) pOsixMsg;

        switch (pMefMsg->u4MsgType)
        {
            case MEF_CREATE_CONTEXT:
                 MefContextCreateAndInit (pMefMsg->u4ContextId); 
                 L2MI_SYNC_GIVE_SEM ();
                 break;

            case MEF_DELETE_CONTEXT:
                 MefContextDelete (pMefMsg->u4ContextId);
                 L2MI_SYNC_GIVE_SEM ();
                 break;

            default:
                 break;
        }

        MemReleaseMemBlock (MEF_QUEUE_MSG_POOL_ID, (UINT1 *)pMefMsg);
    }
}
#endif
