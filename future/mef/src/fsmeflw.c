/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmeflw.c,v 1.37 2015/09/09 11:55:06 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "fsmefinc.h"
#include  "vlnmpbnp.h"

/* LOW LEVEL Routines for Table : FsMefContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefContextTable
 Input       :  The Indices
                FsMefContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefContextTable (UINT4 u4FsMefContextId)
{
    if (VcmIsL2VcExist (u4FsMefContextId) != VCM_TRUE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefContextTable
 Input       :  The Indices
                FsMefContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefContextTable (UINT4 *pu4FsMefContextId)
{

    *pu4FsMefContextId = VCM_DEFAULT_CONTEXT;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefContextTable
 Input       :  The Indices
                FsMefContextId
                nextFsMefContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefContextTable (UINT4 u4FsMefContextId,
                                  UINT4 *pu4NextFsMefContextId)
{
    if (VcmGetNextActiveL2Context (u4FsMefContextId, pu4NextFsMefContextId)
        != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefContextName
 Input       :  The Indices
                FsMefContextId

                The Object 
                retValFsMefContextName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefContextName (UINT4 u4FsMefContextId,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsMefContextName)
{
    INT4                i4RetVal = SNMP_FAILURE;

    i4RetVal = nmhGetFsVcAlias (u4FsMefContextId, pRetValFsMefContextName);

    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMefTransmode
 Input       :  The Indices
                FsMefContextId

                The Object
                retValFsMefTransmode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefTransmode (UINT4 u4FsMefContextId, INT4 *pi4RetValFsMefTransmode)
{

    *pi4RetValFsMefTransmode =
        gMefGlobalInfo.apContextInfo[u4FsMefContextId]->eMefTransportMode;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMefFrameLossBufferClear
 Input       :  The Indices
                FsMefContextId

                The Object 
                retValFsMefFrameLossBufferClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFrameLossBufferClear (UINT4 u4FsMefContextId,
                                 INT4 *pi4RetValFsMefFrameLossBufferClear)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_CC_LOCK ();
    i4RetVal = nmhGetFsMIY1731FrameLossBufferClear (u4FsMefContextId,
                                                    pi4RetValFsMefFrameLossBufferClear);

    ECFM_CC_UNLOCK ();
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMefFrameDelayBufferClear
 Input       :  The Indices
                FsMefContextId

                The Object 
                retValFsMefFrameDelayBufferClear
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFrameDelayBufferClear (UINT4 u4FsMefContextId,
                                  INT4 *pi4RetValFsMefFrameDelayBufferClear)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_LBLT_LOCK ();
    i4RetVal = nmhGetFsMIY1731FrameDelayBufferClear (u4FsMefContextId,
                                                     pi4RetValFsMefFrameDelayBufferClear);
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMefFrameLossBufferSize
 Input       :  The Indices
                FsMefContextId

                The Object 
                retValFsMefFrameLossBufferSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFrameLossBufferSize (UINT4 u4FsMefContextId,
                                INT4 *pi4RetValFsMefFrameLossBufferSize)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_CC_LOCK ();
    i4RetVal = nmhGetFsMIY1731FrameLossBufferSize (u4FsMefContextId,
                                                   pi4RetValFsMefFrameLossBufferSize);
    ECFM_CC_UNLOCK ();
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMefFrameDelayBufferSize
 Input       :  The Indices
                FsMefContextId

                The Object 
                retValFsMefFrameDelayBufferSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFrameDelayBufferSize (UINT4 u4FsMefContextId,
                                 INT4 *pi4RetValFsMefFrameDelayBufferSize)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_LBLT_LOCK ();
    i4RetVal = nmhGetFsMIY1731FrameDelayBufferSize (u4FsMefContextId,
                                                    pi4RetValFsMefFrameDelayBufferSize);
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMefTransmode
 Input       :  The Indices
                FsMefContextId

                The Object
                setValFsMefTransmode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefTransmode (UINT4 u4FsMefContextId, INT4 i4SetValFsMefTransmode)
{
    gMefGlobalInfo.apContextInfo[u4FsMefContextId]->eMefTransportMode
        = i4SetValFsMefTransmode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefFrameLossBufferClear
 Input       :  The Indices
                FsMefContextId

                The Object 
                setValFsMefFrameLossBufferClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefFrameLossBufferClear (UINT4 u4FsMefContextId,
                                 INT4 i4SetValFsMefFrameLossBufferClear)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_CC_LOCK ();
    i4RetVal = nmhSetFsMIY1731FrameLossBufferClear (u4FsMefContextId,
                                                    i4SetValFsMefFrameLossBufferClear);
    ECFM_CC_UNLOCK ();
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMefFrameDelayBufferClear
 Input       :  The Indices
                FsMefContextId

                The Object 
                setValFsMefFrameDelayBufferClear
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefFrameDelayBufferClear (UINT4 u4FsMefContextId,
                                  INT4 i4SetValFsMefFrameDelayBufferClear)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_LBLT_LOCK ();
    i4RetVal = nmhSetFsMIY1731FrameDelayBufferClear (u4FsMefContextId,
                                                     i4SetValFsMefFrameDelayBufferClear);
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMefFrameLossBufferSize
 Input       :  The Indices
                FsMefContextId

                The Object 
                setValFsMefFrameLossBufferSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefFrameLossBufferSize (UINT4 u4FsMefContextId,
                                INT4 i4SetValFsMefFrameLossBufferSize)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_CC_LOCK ();
    i4RetVal = nmhSetFsMIY1731FrameLossBufferSize (u4FsMefContextId,
                                                   i4SetValFsMefFrameLossBufferSize);
    ECFM_CC_UNLOCK ();
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMefFrameDelayBufferSize
 Input       :  The Indices
                FsMefContextId

                The Object 
                setValFsMefFrameDelayBufferSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefFrameDelayBufferSize (UINT4 u4FsMefContextId,
                                 INT4 i4SetValFsMefFrameDelayBufferSize)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_LBLT_LOCK ();
    i4RetVal = nmhSetFsMIY1731FrameDelayBufferSize (u4FsMefContextId,
                                                    i4SetValFsMefFrameDelayBufferSize);
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMefTransmode
 Input       :  The Indices
                FsMefContextId

                The Object
                testValFsMefTransmode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)                            
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefTransmode (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                         INT4 i4TestValFsMefTransmode)
{
    tMefEvcInfo         MefEvcInfo;
    tMefEvcInfo        *pMefEvcInfo = NULL;

    if (VcmIsL2VcExist (u4FsMefContextId) != VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsMefTransmode != MEF_TRANS_MODE_PB) &&
        (i4TestValFsMefTransmode != MEF_TRANS_MODE_MPLS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&MefEvcInfo, 0, sizeof (tMefEvcInfo));

    MefEvcInfo.u4EvcContextId = u4FsMefContextId;
    pMefEvcInfo = (tMefEvcInfo *) RBTreeGetNext
        (MEF_EVC_TABLE, &MefEvcInfo, NULL);

    if (pMefEvcInfo != NULL)
    {
        if (pMefEvcInfo->u4EvcContextId != u4FsMefContextId)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefFrameLossBufferClear
 Input       :  The Indices
                FsMefContextId

                The Object 
                testValFsMefFrameLossBufferClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefFrameLossBufferClear (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                                    INT4 i4TestValFsMefFrameLossBufferClear)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_CC_LOCK ();
    i4RetVal = nmhTestv2FsMIY1731FrameLossBufferClear
        (pu4ErrorCode, u4FsMefContextId, i4TestValFsMefFrameLossBufferClear);
    ECFM_CC_UNLOCK ();
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefFrameDelayBufferClear
 Input       :  The Indices
                FsMefContextId

                The Object 
                testValFsMefFrameDelayBufferClear
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefFrameDelayBufferClear (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMefContextId,
                                     INT4 i4TestValFsMefFrameDelayBufferClear)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_LBLT_LOCK ();
    i4RetVal = nmhTestv2FsMIY1731FrameDelayBufferClear
        (pu4ErrorCode, u4FsMefContextId, i4TestValFsMefFrameDelayBufferClear);
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMefFrameLossBufferSize
 Input       :  The Indices
                FsMefContextId

                The Object 
                testValFsMefFrameLossBufferSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefFrameLossBufferSize (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                                   INT4 i4TestValFsMefFrameLossBufferSize)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_CC_LOCK ();
    i4RetVal = nmhTestv2FsMIY1731FrameLossBufferSize
        (pu4ErrorCode, u4FsMefContextId, i4TestValFsMefFrameLossBufferSize);
    ECFM_CC_UNLOCK ();
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefFrameDelayBufferSize
 Input       :  The Indices
                FsMefContextId

                The Object 
                testValFsMefFrameDelayBufferSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefFrameDelayBufferSize (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                                    INT4 i4TestValFsMefFrameDelayBufferSize)
{
    INT4                i4RetVal = SNMP_FAILURE;

    ECFM_LBLT_LOCK ();
    i4RetVal = nmhTestv2FsMIY1731FrameDelayBufferSize
        (pu4ErrorCode, u4FsMefContextId, i4TestValFsMefFrameDelayBufferSize);
    ECFM_LBLT_UNLOCK ();
    return i4RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMefContextTable
 Input       :  The Indices
                FsMefContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMefContextTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsUniTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsUniTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsUniTable (INT4 i4IfIndex)
{
    UINT1               u1IfType = 0;

    if (CfaValidateIfIndex (i4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    CfaGetIfaceType (i4IfIndex, &u1IfType);

    if (u1IfType != CFA_ENET)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsUniTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsUniTable (INT4 *pi4IfIndex)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = RBTreeGetFirst (MEF_UNI_TABLE);

    if (pMefUniInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = pMefUniInfo->i4IfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsUniTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsUniTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tMefUniInfo        *pMefUniInfo = NULL;
    tMefUniInfo         MefUniInfo;

    MEMSET (&MefUniInfo, 0, sizeof (tMefUniInfo));
    MefUniInfo.i4IfIndex = i4IfIndex;

    pMefUniInfo = (tMefUniInfo *) RBTreeGetNext
        (MEF_UNI_TABLE, &MefUniInfo, NULL);

    if (pMefUniInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextIfIndex = pMefUniInfo->i4IfIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsUniId
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniId (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pRetValFsUniId)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }

    MEF_CPY_TO_SNMP (pRetValFsUniId,
                     &pMefUniInfo->au1UniId, STRLEN (pMefUniInfo->au1UniId));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniPhysicalMedium
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniPhysicalMedium
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniPhysicalMedium (INT4 i4IfIndex, INT4 *pi4RetValFsUniPhysicalMedium)
{
    return (nmhGetIfType (i4IfIndex, pi4RetValFsUniPhysicalMedium));
}

/****************************************************************************
 Function    :  nmhGetFsUniSpeed
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniSpeed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniSpeed (INT4 i4IfIndex, UINT4 *pu4RetValFsUniSpeed)
{
    return (nmhGetIssPortCtrlSpeed (i4IfIndex, (INT4 *) pu4RetValFsUniSpeed));
}

/****************************************************************************
 Function    :  nmhGetFsUniMode
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniMode (INT4 i4IfIndex, INT4 *pi4RetValFsUniMode)
{
    return (nmhGetIssPortCtrlDuplex (i4IfIndex, pi4RetValFsUniMode));
}

/****************************************************************************
 Function    :  nmhGetFsUniMacLayer
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniMacLayer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniMacLayer (INT4 i4IfIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValFsUniMacLayer)
{

    CHR1 CONST         *pau1UniMacLayer = { "IEEE 802.3 - 2005" };

    UNUSED_PARAM (i4IfIndex);

    pRetValFsUniMacLayer->i4_Length = STRLEN (pau1UniMacLayer);
    MEMCPY (pRetValFsUniMacLayer->pu1_OctetList, pau1UniMacLayer,
            pRetValFsUniMacLayer->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniMtu
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniMtu (INT4 i4IfIndex, INT4 *pi4RetValFsUniMtu)
{
    return (nmhGetIfMtu (i4IfIndex, pi4RetValFsUniMtu));
}

/****************************************************************************
 Function    :  nmhGetFsUniServiceMultiplexingBundling
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniServiceMultiplexingBundling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniServiceMultiplexingBundling (INT4 i4IfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsUniServiceMultiplexingBundling)
{
    UINT1               u1PortServiceStatus = 0;
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if (pMefUniInfo->bAllToOneBundling == MEF_ENABLED)
    {
        u1PortServiceStatus =
            (u1PortServiceStatus | MEF_ALL_TO_ONE_BUNDLE_BITMASK);
    }

    if (pMefUniInfo->bMultiplexing == MEF_ENABLED)
    {
        u1PortServiceStatus = (u1PortServiceStatus | MEF_MULTIPLEX_BITMASK);
    }

    if (pMefUniInfo->bBundling == MEF_ENABLED)
    {
        u1PortServiceStatus = (u1PortServiceStatus | MEF_BUNDLE_BITMASK);
    }

    MEF_CPY_TO_SNMP (pRetValFsUniServiceMultiplexingBundling,
                     &u1PortServiceStatus, sizeof (UINT1));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsUniCVlanId
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniCVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniCVlanId (INT4 i4IfIndex, INT4 *pi4RetValFsUniCVlanId)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }

    *pi4RetValFsUniCVlanId = pMefUniInfo->u2DefaultCeVlanId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniMaxEvcs
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniMaxEvcs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniMaxEvcs (INT4 i4IfIndex, INT4 *pi4RetValFsUniMaxEvcs)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->bMultiplexing == OSIX_TRUE)
    {
        *pi4RetValFsUniMaxEvcs = MEF_MAX_VLAN_CURR_ENTRIES;
    }
    else
    {
        *pi4RetValFsUniMaxEvcs = MEF_MIN_EVC_PER_UNI;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniL2CPDot1x
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniL2CPDot1x
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPDot1x (INT4 i4IfIndex, INT4 *pi4RetValFsUniL2CPDot1x)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPDot1x = pMefUniInfo->u1Dot1xTunnelingStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsUniL2CPLacp
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniL2CPLacp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPLacp (INT4 i4IfIndex, INT4 *pi4RetValFsUniL2CPLacp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPLacp = pMefUniInfo->u1LacpTunnelingStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsUniL2CPStp
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniL2CPStp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPStp (INT4 i4IfIndex, INT4 *pi4RetValFsUniL2CPStp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPStp = pMefUniInfo->u1StpTunnelingStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniL2CPGvrp
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniL2CPGvrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPGvrp (INT4 i4IfIndex, INT4 *pi4RetValFsUniL2CPGvrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPGvrp = pMefUniInfo->u1GvrpTunnelingStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFGmrp
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniL2CPGmrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPGmrp (INT4 i4IfIndex, INT4 *pi4RetValFsUniL2CPGmrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPGmrp = pMefUniInfo->u1GmrpTunnelingStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsUniL2CPMvrp
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniL2CPMvrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPMvrp (INT4 i4IfIndex, INT4 *pi4RetValFsUniL2CPMvrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPMvrp = pMefUniInfo->u1MvrpTunnelingStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsUniL2CPMmrp
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniL2CPMmrp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPMmrp (INT4 i4IfIndex, INT4 *pi4RetValFsUniL2CPMmrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPMmrp = pMefUniInfo->u1MmrpTunnelingStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniL2CPLldp
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniL2CPLldp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPLldp (INT4 i4IfIndex, INT4 *pi4RetValFsUniL2CPLldp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPLldp = pMefUniInfo->u1LldpTunnelingStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniL2CPElmi
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniL2CPElmi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPElmi (INT4 i4IfIndex, INT4 *pi4RetValFsUniL2CPElmi)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPElmi = pMefUniInfo->u1ElmiTunnelingStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniRowStatus (INT4 i4IfIndex, INT4 *pi4RetValFsUniRowStatus)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);

    if (pMefUniInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsUniRowStatus = pMefUniInfo->i4RowStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniL2CPEcfm
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsUniL2CPEcfm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPEcfm (INT4 i4IfIndex, INT4 *pi4RetValFsUniL2CPEcfm)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPEcfm = pMefUniInfo->u1EcfmTunnelingStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniL2CPOverrideOption
 Input       :  The Indices
                IfIndex

                The Object
                retValFsUniL2CPOverrideOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPOverrideOption (INT4 i4IfIndex,
                               INT4 *pi4RetValFsUniL2CPOverrideOption)
{
    tMefUniInfo        *pMefUniInfo = NULL;
    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPOverrideOption = pMefUniInfo->u4UniOverride;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniL2CPEoam
 Input       :  The Indices
                IfIndex

                The Object
                retValFsUniL2CPEoam
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniL2CPEoam (INT4 i4IfIndex, INT4 *pi4RetValFsUniL2CPEoam)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsUniL2CPEoam = pMefUniInfo->u1EoamTunnelingStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsUniId
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniId (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pSetValFsUniId)
{

    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }

    MEMSET (&pMefUniInfo->au1UniId, 0, UNI_ID_MAX_LENGTH);
    MEF_CPY_FROM_SNMP (&pMefUniInfo->au1UniId, pSetValFsUniId,
                       pSetValFsUniId->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsUniServiceMultiplexingBundling
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniServiceMultiplexingBundling
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniServiceMultiplexingBundling (INT4 i4IfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pSetValFsUniServiceMultiplexingBundling)
{
    UINT1               u1PortServiceStatus = 0;
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }

    MEF_CPY_FROM_SNMP (&u1PortServiceStatus,
                       pSetValFsUniServiceMultiplexingBundling, sizeof (UINT1));

    pMefUniInfo->bMultiplexing = DISABLED;
    pMefUniInfo->bBundling = MEF_DISABLED;
    pMefUniInfo->bAllToOneBundling = MEF_DISABLED;

    if (u1PortServiceStatus & MEF_MULTIPLEX_BITMASK)
    {
        pMefUniInfo->bMultiplexing = MEF_ENABLED;
        pMefUniInfo->bBundling = MEF_DISABLED;
        pMefUniInfo->bAllToOneBundling = MEF_DISABLED;
    }

    if (u1PortServiceStatus & MEF_BUNDLE_BITMASK)
    {
        pMefUniInfo->bBundling = MEF_ENABLED;
        pMefUniInfo->bAllToOneBundling = MEF_DISABLED;
    }
    else if (u1PortServiceStatus & MEF_ALL_TO_ONE_BUNDLE_BITMASK)
    {
        pMefUniInfo->bAllToOneBundling = MEF_ENABLED;
        pMefUniInfo->bMultiplexing = MEF_DISABLED;
        pMefUniInfo->bBundling = MEF_DISABLED;
    }

    if (pMefUniInfo->bMultiplexing == MEF_DISABLED)
    {
        if (MefActOnServiceMultiplexingChange (pMefUniInfo) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    MefUpdateUniL2cpInfo (pMefUniInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsUniCVlanId
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniCVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniCVlanId (INT4 i4IfIndex, INT4 i4SetValFsUniCVlanId)
{

    tMefUniInfo        *pMefUniInfo = NULL;
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    UINT2              u2PrevDefCeVlanId = 0;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    u2PrevDefCeVlanId = pMefUniInfo->u2DefaultCeVlanId;

    pCVlanEvcInfo = MefGetCVlanEvcEntry (i4IfIndex, (INT4)u2PrevDefCeVlanId);
    if((pCVlanEvcInfo != NULL) && ( u2PrevDefCeVlanId != (UINT2) i4SetValFsUniCVlanId))
    {
	    if (nmhSetDot1adMICVidRegistrationUntaggedCep (i4IfIndex,
				    (INT4)u2PrevDefCeVlanId,
				    VLAN_SNMP_FALSE) ==
			    SNMP_FAILURE)
	    {
		    return SNMP_FAILURE;
	    }
    }

    pMefUniInfo->u2DefaultCeVlanId = (UINT2)i4SetValFsUniCVlanId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPDot1x
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniL2CPDot1x
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPDot1x (INT4 i4IfIndex, INT4 i4SetValFsUniL2CPDot1x)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u1Dot1xTunnelingStatus = i4SetValFsUniL2CPDot1x;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPLacp
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniL2CPLacp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPLacp (INT4 i4IfIndex, INT4 i4SetValFsUniL2CPLacp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u1LacpTunnelingStatus = i4SetValFsUniL2CPLacp;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPStp
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniL2CPStp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPStp (INT4 i4IfIndex, INT4 i4SetValFsUniL2CPStp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u1StpTunnelingStatus = i4SetValFsUniL2CPStp;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPGvrp
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniL2CPGvrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPGvrp (INT4 i4IfIndex, INT4 i4SetValFsUniL2CPGvrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u1GvrpTunnelingStatus = i4SetValFsUniL2CPGvrp;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPGmrp
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniL2CPGmrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPGmrp (INT4 i4IfIndex, INT4 i4SetValFsUniL2CPGmrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u1GmrpTunnelingStatus = i4SetValFsUniL2CPGmrp;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPMvrp
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniL2CPMvrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPMvrp (INT4 i4IfIndex, INT4 i4SetValFsUniL2CPMvrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u1MvrpTunnelingStatus = i4SetValFsUniL2CPMvrp;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPMmrp
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniL2CPMmrp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPMmrp (INT4 i4IfIndex, INT4 i4SetValFsUniL2CPMmrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u1MmrpTunnelingStatus = i4SetValFsUniL2CPMmrp;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPLldp
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniL2CPLldp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPLldp (INT4 i4IfIndex, INT4 i4SetValFsUniL2CPLldp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u1LldpTunnelingStatus = i4SetValFsUniL2CPLldp;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPElmi
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniL2CPElmi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPElmi (INT4 i4IfIndex, INT4 i4SetValFsUniL2CPElmi)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u1ElmiTunnelingStatus = i4SetValFsUniL2CPElmi;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsUniRowStatus
 Input       :  The Indices
                IfIndex

                The Object
                setValFsUniRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniRowStatus (INT4 i4IfIndex, INT4 i4SetValFsUniRowStatus)
{

#ifdef MPLS_WANTED
    UINT4               u4ErrorCode = 0;
    UINT4               u4FsMefContextId = 0;
    UINT2               u2FsMefLocalPort = 0;
#endif
    tMefUniInfo        *pMefUniInfo = NULL;
    pMefUniInfo = MefGetUniEntry (i4IfIndex);

    switch (i4SetValFsUniRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pMefUniInfo != NULL)
            {
                return SNMP_FAILURE;
            }
            pMefUniInfo = (tMefUniInfo *) MemAllocMemBlk (MEF_UNI_POOL_ID);
            if (pMefUniInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            MefInitDefaultUniInfo (i4IfIndex, pMefUniInfo);

            if (MefAddUniEntry (pMefUniInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            pMefUniInfo->i4RowStatus = NOT_READY;
            break;

        case ACTIVE:
            if (pMefUniInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            if (MefIsMsrInProgress () != OSIX_TRUE)
            {
                if (MefSetUniInfoEntry (pMefUniInfo) == OSIX_FAILURE)
                {
#ifdef MPLS_WANTED
                    if (nmhTestv2FsMplsPortRowStatus (&u4ErrorCode,
                                                      i4IfIndex,
                                                      DESTROY) == SNMP_SUCCESS)
                    {
                        nmhSetFsMplsPortRowStatus (i4IfIndex, DESTROY);
                    }
#endif
                    return SNMP_FAILURE;
                }
            }
            pMefUniInfo->i4RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            if (pMefUniInfo == NULL)
            {
                return SNMP_FAILURE;
            }
#ifdef MPLS_WANTED
            if (MefGetContexId (i4IfIndex, &u4FsMefContextId, &u2FsMefLocalPort)
                == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }

            if (FsMefUtilGetTransportMode (u4FsMefContextId)
                == MEF_TRANS_MODE_MPLS)
            {
                if (MefResetUniInfoEntry (i4IfIndex) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
#endif
            pMefUniInfo->i4RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            if (pMefUniInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            if (MefResetUniInfoEntry (i4IfIndex) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (MefDeleteUniEntry (i4IfIndex) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPEcfm
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsUniL2CPEcfm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPEcfm (INT4 i4IfIndex, INT4 i4SetValFsUniL2CPEcfm)
{
    tMefUniInfo        *pMefUniInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2Protocol = L2_PROTO_ECFM;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u1EcfmTunnelingStatus = i4SetValFsUniL2CPEcfm;

    if ((L2IwfSetProtocolTunnelStatusOnPort (u4ContextId, (UINT2) i4IfIndex,
                                             u2Protocol,
                                             i4SetValFsUniL2CPEcfm)) ==
        L2IWF_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPOverrideOption
 Input       :  The Indices
                IfIndex

                The Object
                setValFsUniL2CPOverrideOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPOverrideOption (INT4 i4IfIndex,
                               INT4 i4SetValFsUniL2CPOverrideOption)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u4UniOverride = i4SetValFsUniL2CPOverrideOption;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsUniL2CPEoam
 Input       :  The Indices
                IfIndex

                The Object
                setValFsUniL2CPEoam
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniL2CPEoam (INT4 i4IfIndex, INT4 i4SetValFsUniL2CPEoam)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    pMefUniInfo->u1EoamTunnelingStatus = i4SetValFsUniL2CPEoam;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsUniId
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                  tSNMP_OCTET_STRING_TYPE * pTestValFsUniId)
{
    tMefUniInfo        *pMefUniInfo = NULL;
    tMefUniInfo         MefUniInfo;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if (pTestValFsUniId->i4_Length > UNI_ID_MAX_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&MefUniInfo, 0, sizeof (tMefUniInfo));
    MefUniInfo.i4IfIndex = 0;

    pMefUniInfo =
        (tMefUniInfo *) RBTreeGetNext (MEF_UNI_TABLE, &MefUniInfo, NULL);

    while (pMefUniInfo != NULL)
    {
        if ((pMefUniInfo->i4IfIndex != i4IfIndex) &&
            ((STRLEN (pMefUniInfo->au1UniId) ==
              (UINT4) pTestValFsUniId->i4_Length) &&
             (STRNCMP (pMefUniInfo->au1UniId, pTestValFsUniId->pu1_OctetList,
                       pTestValFsUniId->i4_Length) == 0)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (MEF_CLI_UNI_ID_EXIST);
            return SNMP_FAILURE;
        }

        MefUniInfo.i4IfIndex = pMefUniInfo->i4IfIndex;

        pMefUniInfo =
            (tMefUniInfo *) RBTreeGetNext (MEF_UNI_TABLE, &MefUniInfo, NULL);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsUniServiceMultiplexingBundling
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniServiceMultiplexingBundling
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniServiceMultiplexingBundling (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pTestValFsUniServiceMultiplexingBundling)
{
    UINT1               u1PortServiceStatus = 0;
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }

    MEF_CPY_FROM_SNMP (&u1PortServiceStatus,
                       pTestValFsUniServiceMultiplexingBundling,
                       sizeof (UINT1));

    if ((u1PortServiceStatus > MEF_ALL_TO_ONE_BUNDLE_BITMASK)
        || (u1PortServiceStatus < MEF_MULTIPLEX_BITMASK))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsUniCVlanId
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniCVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniCVlanId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                       INT4 i4TestValFsUniCVlanId)
{

    tMefUniInfo        *pMefUniInfo = NULL;

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsUniCVlanId > MEF_MAX_VLAN_CURR_ENTRIES) ||
        (i4TestValFsUniCVlanId == 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPDot1x
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniL2CPDot1x
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPDot1x (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                         INT4 i4TestValFsUniL2CPDot1x)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    if ((i4TestValFsUniL2CPDot1x != MEF_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsUniL2CPDot1x != MEF_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsUniL2CPDot1x != MEF_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsUniL2CPDot1x == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        if (PnacGetPnacEnableStatus () == PNAC_SUCCESS)
        {
            CLI_SET_ERR (MEF_CLI_TUNNEL_PROTOCOL_ENA_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    VLAN_LOCK ();
    if (nmhTestv2FsMIVlanTunnelProtocolDot1x
        (pu4ErrorCode, i4IfIndex, i4TestValFsUniL2CPDot1x) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPLacp
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniL2CPLacp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPLacp (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        INT4 i4TestValFsUniL2CPLacp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    if ((i4TestValFsUniL2CPLacp != MEF_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsUniL2CPLacp != MEF_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsUniL2CPLacp != MEF_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    VLAN_LOCK ();
    if (nmhTestv2FsMIVlanTunnelProtocolLacp
        (pu4ErrorCode, i4IfIndex, i4TestValFsUniL2CPLacp) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPStp
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniL2CPStp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPStp (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                       INT4 i4TestValFsUniL2CPStp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    if ((i4TestValFsUniL2CPStp != MEF_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsUniL2CPStp != MEF_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsUniL2CPStp != MEF_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (((pMefUniInfo->bMultiplexing == MEF_ENABLED) ||
         (pMefUniInfo->bBundling == MEF_ENABLED)) &&
        (i4TestValFsUniL2CPStp == MEF_TUNNEL_PROTOCOL_TUNNEL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    if ((pMefUniInfo->bAllToOneBundling == MEF_ENABLED) &&
        ((i4TestValFsUniL2CPStp == MEF_TUNNEL_PROTOCOL_PEER) ||
         (i4TestValFsUniL2CPStp == MEF_TUNNEL_PROTOCOL_DISCARD)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    VLAN_LOCK ();
    if (nmhTestv2FsMIVlanTunnelProtocolStp
        (pu4ErrorCode, i4IfIndex, i4TestValFsUniL2CPStp) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPGvrp
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniL2CPGvrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPGvrp (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        INT4 i4TestValFsUniL2CPGvrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;
    UINT4               u4BridgeMode = 0;
    UINT4               u4MefContextId = 0;
    UINT2               u2MefLocalPort = 0;

    if ((i4TestValFsUniL2CPGvrp != MEF_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsUniL2CPGvrp != MEF_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsUniL2CPGvrp != MEF_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MefGetContexId (i4IfIndex, &u4MefContextId,
                        &u2MefLocalPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Customer Gvrp peering is not supported. */
    L2IwfGetBridgeMode (u4MefContextId, &u4BridgeMode);
    if (u4BridgeMode == VLAN_PROVIDER_CORE_BRIDGE_MODE ||
        u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        if (i4TestValFsUniL2CPGvrp == MEF_TUNNEL_PROTOCOL_PEER)
        {
            CLI_SET_ERR (MEF_CLI_GVRP_PEER_ERROR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    VLAN_LOCK ();
    if (nmhTestv2FsMIVlanTunnelProtocolGvrp
        (pu4ErrorCode, i4IfIndex, i4TestValFsUniL2CPGvrp) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPGmrp
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniL2CPGmrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPGmrp (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        INT4 i4TestValFsUniL2CPGmrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    if ((i4TestValFsUniL2CPGmrp != MEF_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsUniL2CPGmrp != MEF_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsUniL2CPGmrp != MEF_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    VLAN_LOCK ();
    if (nmhTestv2FsMIVlanTunnelProtocolGmrp
        (pu4ErrorCode, i4IfIndex, i4TestValFsUniL2CPGmrp) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPMvrp
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniL2CPMvrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPMvrp (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        INT4 i4TestValFsUniL2CPMvrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    if ((i4TestValFsUniL2CPMvrp != MEF_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsUniL2CPMvrp != MEF_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsUniL2CPMvrp != MEF_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFsUniL2CPMvrp == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsUniL2CPMvrp != MEF_TUNNEL_PROTOCOL_TUNNEL) &&
        (pMefUniInfo->bMultiplexing != MEF_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    VLAN_LOCK ();
    if (nmhTestv2FsMIVlanTunnelProtocolMvrp
        (pu4ErrorCode, i4IfIndex, i4TestValFsUniL2CPMvrp) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPMmrp
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniL2CPMmrp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPMmrp (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        INT4 i4TestValFsUniL2CPMmrp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    if ((i4TestValFsUniL2CPMmrp != MEF_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsUniL2CPMmrp != MEF_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsUniL2CPMmrp != MEF_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFsUniL2CPMmrp == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsUniL2CPMmrp != MEF_TUNNEL_PROTOCOL_TUNNEL) &&
        (pMefUniInfo->bMultiplexing != MEF_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    VLAN_LOCK ();
    if (nmhTestv2FsMIVlanTunnelProtocolMmrp
        (pu4ErrorCode, i4IfIndex, i4TestValFsUniL2CPMmrp) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPLldp
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniL2CPLldp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPLldp (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        INT4 i4TestValFsUniL2CPLldp)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    if ((i4TestValFsUniL2CPLldp != MEF_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsUniL2CPLldp != MEF_TUNNEL_PROTOCOL_DISCARD) &&
        (i4TestValFsUniL2CPLldp != MEF_TUNNEL_PROTOCOL_TUNNEL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* 
     * Only allow MEF_TUNNEL_PROTOCOL_PEER in case of EVCType == EPL.
     * Other cases it should be MEF_TUNNEL_PROTOCOL_DISCARD
     */

    if (i4TestValFsUniL2CPLldp != MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        if ((i4TestValFsUniL2CPLldp == MEF_TUNNEL_PROTOCOL_PEER) &&
            (pMefUniInfo->bAllToOneBundling != MEF_ENABLED))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
            return SNMP_FAILURE;
        }
    }

    VLAN_LOCK ();
    if (nmhTestv2FsMIVlanTunnelProtocolLldp
        (pu4ErrorCode, i4IfIndex, i4TestValFsUniL2CPLldp) == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPElmi
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniL2CPElmi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPElmi (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        INT4 i4TestValFsUniL2CPElmi)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    if ((i4TestValFsUniL2CPElmi != MEF_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsUniL2CPElmi != MEF_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    VLAN_LOCK ();
    if (nmhTestv2FsMIVlanTunnelProtocolElmi
        (pu4ErrorCode, i4IfIndex, i4TestValFsUniL2CPElmi) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsUniRowStatus
 Input       :  The Indices
                IfIndex

                The Object
                testValFsUniRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                         INT4 i4TestValFsUniRowStatus)
{
    UINT1               u1IfType = 0;
    tMefUniInfo        *pMefUniInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4BridgeMode = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1TransModeFlag = 0;

    /* If the port is not mapped to any context, then don't
     * allow setting Uni Id. */
    if (VcmGetContextInfoFromIfIndex ((UINT4) i4IfIndex, &u4ContextId,
                                      &u2LocalPort) != VCM_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_BRG_PORT_TYPE_MAP_ERR);
        return SNMP_FAILURE;
    }
    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    L2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);
    u1TransModeFlag = FsMefUtilGetTransportMode (u4ContextId);

    switch (i4TestValFsUniRowStatus)
    {
        case CREATE_AND_WAIT:

            if (pMefUniInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (MEF_CLI_ENTRY_ALREADY_EXIT);
                return SNMP_FAILURE;
            }

            if ((u1TransModeFlag == MEF_TRANS_MODE_MPLS) &&
                (u4BridgeMode == MEF_CUSTOMER_BRIDGE_MODE))
            {
                if (VlanValidatePortType (i4IfIndex, MEF_CUSTOMER_BRIDGE_PORT)
                    == VLAN_FALSE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (MEF_CLI_BRG_PORT_TYPE_MODE_ERR);
                    return SNMP_FAILURE;
                }
            }

            if (((u1TransModeFlag == MEF_TRANS_MODE_PB) ||
                 (u1TransModeFlag == MEF_TRANS_MODE_MPLS)) &&
                (u4BridgeMode == MEF_PROVIDER_EDGE_BRIDGE_MODE))
            {
                if (!
                    ((VlanValidatePortType (i4IfIndex, MEF_CUSTOMER_EDGE_PORT))
                     ||
                     (VlanValidatePortType
                      (i4IfIndex, MEF_CNP_PORTBASED_PORT))))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (MEF_CLI_BRG_PORT_TYPE_MODE_ERR);
                    return SNMP_FAILURE;
                }
            }
            break;

        case ACTIVE:
        case DESTROY:
        case NOT_IN_SERVICE:

            if (pMefUniInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
                return SNMP_FAILURE;
            }

            if (pMefUniInfo->bAllToOneBundling == MEF_ENABLED)
            {
                if ((u1TransModeFlag == MEF_TRANS_MODE_MPLS) &&
                    (u4BridgeMode == MEF_CUSTOMER_BRIDGE_MODE))
                {
                    if (VlanValidatePortType
                        (i4IfIndex, MEF_CUSTOMER_BRIDGE_PORT) == VLAN_FALSE)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (MEF_CLI_BRG_PORT_TYPE_MODE_ERR);
                        return SNMP_FAILURE;
                    }
                }

                if ((u1TransModeFlag == MEF_TRANS_MODE_PB) &&
                    (u4BridgeMode == MEF_PROVIDER_EDGE_BRIDGE_MODE))
                {

                    if (VlanValidatePortType (i4IfIndex, MEF_CNP_PORTBASED_PORT)
                        == VLAN_FAILURE)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (MEF_CLI_BRG_PORT_TYPE_MODE_ERR);
                        return SNMP_FAILURE;
                    }
                }

                if ((u1TransModeFlag == MEF_TRANS_MODE_MPLS) &&
                    (u4BridgeMode == MEF_PROVIDER_EDGE_BRIDGE_MODE))
                {
                    if (!((VlanValidatePortType
                           (i4IfIndex, MEF_CUSTOMER_EDGE_PORT)) ||
                          (VlanValidatePortType
                           (i4IfIndex, MEF_CNP_PORTBASED_PORT))))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (MEF_CLI_BRG_PORT_TYPE_MODE_ERR);
                        return SNMP_FAILURE;
                    }

                }
            }
            else
            {
                if ((u1TransModeFlag == MEF_TRANS_MODE_MPLS) &&
                    (u4BridgeMode == MEF_CUSTOMER_BRIDGE_MODE))
                {
                    if (VlanValidatePortType
                        (i4IfIndex, MEF_CUSTOMER_BRIDGE_PORT) == VLAN_FALSE)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (MEF_CLI_BRG_PORT_TYPE_MODE_ERR);
                    }
                }

                if (u1TransModeFlag == MEF_TRANS_MODE_PB)
                {
                    if (VlanValidatePortType (i4IfIndex, MEF_CUSTOMER_EDGE_PORT)
                        == VLAN_FALSE)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (MEF_CLI_BRG_PORT_TYPE_MODE_ERR);
                        return SNMP_FAILURE;
                    }
                }
                else if ((u1TransModeFlag == MEF_TRANS_MODE_MPLS) &&
                         (u4BridgeMode == MEF_PROVIDER_EDGE_BRIDGE_MODE))
                {
                    if (!((VlanValidatePortType
                           (i4IfIndex, MEF_CUSTOMER_EDGE_PORT)) ||
                          (VlanValidatePortType
                           (i4IfIndex, MEF_CNP_PORTBASED_PORT))))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (MEF_CLI_BRG_PORT_TYPE_MODE_ERR);
                        return SNMP_FAILURE;
                    }
                }
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    if (CfaValidateIfIndex (i4IfIndex) != CFA_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    CfaGetIfaceType (i4IfIndex, &u1IfType);

    if (u1IfType != CFA_ENET)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPEcfm
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsUniL2CPEcfm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPEcfm (UINT4 *pu4ErrorCode,
                        INT4 i4IfIndex, INT4 i4TestValFsUniL2CPEcfm)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    if ((i4TestValFsUniL2CPEcfm != MEF_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsUniL2CPEcfm != MEF_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsUniL2CPEcfm != MEF_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPOverrideOption
 Input       :  The Indices
                IfIndex

                The Object
                testValFsUniL2CPOverrideOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPOverrideOption (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4TestValFsUniL2CPOverrideOption)
{
    tMefUniInfo        *pMefUniInfo = NULL;
    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsUniL2CPOverrideOption != MEF_UNI_OVERRIDE_ENABLE)
        && (i4TestValFsUniL2CPOverrideOption != MEF_UNI_OVERRIDE_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsUniL2CPEoam
 Input       :  The Indices
                IfIndex

                The Object
                testValFsUniL2CPEoam
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniL2CPEoam (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        INT4 i4TestValFsUniL2CPEoam)
{
    tMefUniInfo        *pMefUniInfo = NULL;

    if ((i4TestValFsUniL2CPEoam != MEF_TUNNEL_PROTOCOL_PEER) &&
        (i4TestValFsUniL2CPEoam != MEF_TUNNEL_PROTOCOL_TUNNEL) &&
        (i4TestValFsUniL2CPEoam != MEF_TUNNEL_PROTOCOL_DISCARD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_INVALID_UNI_L2CP_STATUS);
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (pMefUniInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsUniTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsUniTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsEvcTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEvcTable
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEvcTable (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex)
{
    tMefEvcInfo         MefEvcInfo;
    tMefEvcInfo        *pMefEvcInfo = NULL;

    MEMSET (&MefEvcInfo, 0, sizeof (tMefEvcInfo));
    MefEvcInfo.u4EvcContextId = i4FsEvcContextId;
    MefEvcInfo.i4EvcVlanId = i4FsEvcIndex;

    pMefEvcInfo = (tMefEvcInfo *) RBTreeGet (MEF_EVC_TABLE, &MefEvcInfo);

    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEvcTable
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEvcTable (INT4 *pi4FsEvcContextId, INT4 *pi4FsEvcIndex)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = (tMefEvcInfo *) RBTreeGetFirst (MEF_EVC_TABLE);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4FsEvcContextId = pMefEvcInfo->u4EvcContextId;
    *pi4FsEvcIndex = pMefEvcInfo->i4EvcVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEvcTable
 Input       :  The Indices
                FsEvcContextId
                nextFsEvcContextId
                FsEvcIndex
                nextFsEvcIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEvcTable (INT4 i4FsEvcContextId, INT4 *pi4NextFsEvcContextId,
                           INT4 i4FsEvcIndex, INT4 *pi4NextFsEvcIndex)
{
    tMefEvcInfo         MefEvcInfo;
    tMefEvcInfo        *pMefEvcInfo = NULL;

    MEMSET (&MefEvcInfo, 0, sizeof (tMefEvcInfo));

    MefEvcInfo.u4EvcContextId = i4FsEvcContextId;
    MefEvcInfo.i4EvcVlanId = i4FsEvcIndex;

    pMefEvcInfo = (tMefEvcInfo *) RBTreeGetNext
        (MEF_EVC_TABLE, &MefEvcInfo, NULL);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsEvcContextId = pMefEvcInfo->u4EvcContextId;
    *pi4NextFsEvcIndex = pMefEvcInfo->i4EvcVlanId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvcId
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                retValFsEvcId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcId (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
               tSNMP_OCTET_STRING_TYPE * pRetValFsEvcId)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEF_CPY_TO_SNMP (pRetValFsEvcId,
                     &pMefEvcInfo->au1EvcId, STRLEN (pMefEvcInfo->au1EvcId));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEvcType
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                retValFsEvcType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcType (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                 INT4 *pi4RetValFsEvcType)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsEvcType = pMefEvcInfo->EvcType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEvcMaxUni
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                retValFsEvcMaxUni
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcMaxUni (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                   INT4 *pi4RetValFsEvcMaxUni)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pMefEvcInfo->EvcType == MEF_POINT_TO_POINT)
    {
        *pi4RetValFsEvcMaxUni = 2;
    }
    else
    {
        *pi4RetValFsEvcMaxUni = SYS_DEF_MAX_PORTS_PER_CONTEXT;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEvcCVlanIdPreservation
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                retValFsEvcCVlanIdPreservation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcCVlanIdPreservation (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                                INT4 *pi4RetValFsEvcCVlanIdPreservation)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsEvcCVlanIdPreservation = pMefEvcInfo->bCeVlanIdPreservation;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEvcCVlanCoSPreservation
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                retValFsEvcCVlanCoSPreservation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcCVlanCoSPreservation (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                                 INT4 *pi4RetValFsEvcCVlanCoSPreservation)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsEvcCVlanCoSPreservation = pMefEvcInfo->bCeVlanCoSPreservation;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEvcRowStatus
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                retValFsEvcRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcRowStatus (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                      INT4 *pi4RetValFsEvcRowStatus)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsEvcRowStatus = pMefEvcInfo->i4RowStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEvcMtu
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                retValFsEvcMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcMtu (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                INT4 *pi4RetValFsEvcMtu)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsEvcMtu = pMefEvcInfo->i4EvcMtu;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEvcL2CPTunnel
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object
                retValFsEvcL2CPTunnel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcL2CPTunnel (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsEvcL2CPTunnel)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    UINT4               u4EvcTunnelProtocolList = 0;
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (MEF_EVC_DOT1X_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_DOT1X_BITMASK);
    }
    if (MEF_EVC_LACP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_LACP_BITMASK);
    }
    if (MEF_EVC_STP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_STP_BITMASK);
    }
    if (MEF_EVC_GVRP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_GVRP_BITMASK);
    }
    if (MEF_EVC_GMRP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_GMRP_BITMASK);
    }
    if (MEF_EVC_IGMP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_IGMP_BITMASK);
    }
    if (MEF_EVC_MVRP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_MVRP_BITMASK);
    }
    if (MEF_EVC_MMRP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_MMRP_BITMASK);
    }
    if (MEF_EVC_ELMI_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_ELMI_BITMASK);
    }
    if (MEF_EVC_LLDP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_LLDP_BITMASK);
    }
    if (MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_ECFM_BITMASK);
    }
    if (MEF_EVC_EOAM_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_TUNNEL)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_EOAM_BITMASK);
    }

    MEF_CPY_TO_SNMP (pRetValFsEvcL2CPTunnel, &u4EvcTunnelProtocolList,
                     sizeof (UINT4));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEvcL2CPPeer
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object
                retValFsEvcL2CPPeer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcL2CPPeer (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValFsEvcL2CPPeer)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    UINT4               u4EvcTunnelProtocolList = 0;
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (MEF_EVC_DOT1X_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_PEER)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_DOT1X_BITMASK);
    }
    if (MEF_EVC_LACP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_PEER)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_LACP_BITMASK);
    }
    if (MEF_EVC_STP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_PEER)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_STP_BITMASK);
    }
    if (MEF_EVC_ELMI_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_PEER)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_ELMI_BITMASK);
    }
    if (MEF_EVC_LLDP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_PEER)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_LLDP_BITMASK);
    }
    if (MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_PEER)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_ECFM_BITMASK);
    }
    if (MEF_EVC_EOAM_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_PEER)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_EOAM_BITMASK);
    }
    if (MEF_EVC_PTP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_PEER)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_PTP_BITMASK);
    }

    MEF_CPY_TO_SNMP (pRetValFsEvcL2CPPeer, &u4EvcTunnelProtocolList,
                     sizeof (UINT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEvcL2CPDiscard
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object
                retValFsEvcL2CPDiscard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcL2CPDiscard (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsEvcL2CPDiscard)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    UINT4               u4EvcTunnelProtocolList = 0;
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (MEF_EVC_DOT1X_TUNNEL_STATUS (pMefEvcInfo) ==
        MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_DOT1X_BITMASK);
    }
    if (MEF_EVC_LACP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_LACP_BITMASK);
    }
    if (MEF_EVC_STP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_STP_BITMASK);
    }
    if (MEF_EVC_GVRP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_GVRP_BITMASK);
    }
    if (MEF_EVC_GMRP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_GMRP_BITMASK);
    }
    if (MEF_EVC_IGMP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_IGMP_BITMASK);
    }
    if (MEF_EVC_MVRP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_MVRP_BITMASK);
    }
    if (MEF_EVC_MMRP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_MMRP_BITMASK);
    }
    if (MEF_EVC_ELMI_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_ELMI_BITMASK);
    }
    if (MEF_EVC_LLDP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_LLDP_BITMASK);
    }
    if (MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_ECFM_BITMASK);
    }
    if (MEF_EVC_EOAM_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_EOAM_BITMASK);
    }
    if (MEF_EVC_PTP_TUNNEL_STATUS (pMefEvcInfo) == MEF_TUNNEL_PROTOCOL_DISCARD)
    {
        u4EvcTunnelProtocolList =
            (u4EvcTunnelProtocolList | MEF_EVC_PTP_BITMASK);
    }

    MEF_CPY_TO_SNMP (pRetValFsEvcL2CPDiscard, &u4EvcTunnelProtocolList,
                     sizeof (UINT4));

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvcId
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                setValFsEvcId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcId (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
               tSNMP_OCTET_STRING_TYPE * pSetValFsEvcId)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pMefEvcInfo->au1EvcId, 0, EVC_ID_MAX_LENGTH);

    MEF_CPY_FROM_SNMP (&pMefEvcInfo->au1EvcId, pSetValFsEvcId,
                       pSetValFsEvcId->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEvcType
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                setValFsEvcType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcType (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                 INT4 i4SetValFsEvcType)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefEvcInfo->EvcType = i4SetValFsEvcType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEvcCVlanIdPreservation
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                setValFsEvcCVlanIdPreservation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcCVlanIdPreservation (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                                INT4 i4SetValFsEvcCVlanIdPreservation)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    tEvcInfo           *pIssEvcInfo = NULL;
    UINT4               u4Action = MEF_EVC_ATTR_CREATE;
    tMefCVlanEvcInfo    CVlanEvcInfo;
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    INT4                i4VlanPreStatus = 0;
    UINT4               u4PreIfIndex = 0;
   
    MEMSET (&CVlanEvcInfo, 0, sizeof (tMefCVlanEvcInfo));

    pIssEvcInfo = MefGetEvcInfoEntry (i4FsEvcIndex);
    if (pIssEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* EVC Attribute changes update to Vlan and PB modules */
    if (i4SetValFsEvcCVlanIdPreservation != pIssEvcInfo->bCeVlanIdPreservation)
    {
        u4Action = MEF_EVC_ATTR_MODIFY;
        pIssEvcInfo->bCeVlanIdPreservation = i4SetValFsEvcCVlanIdPreservation;
    }

    if (u4Action == MEF_EVC_ATTR_MODIFY)
    {
        VlanHwSetEvcAttribute (i4FsEvcContextId, u4Action, pIssEvcInfo);
    }

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefEvcInfo->bCeVlanIdPreservation = i4SetValFsEvcCVlanIdPreservation;

    if (i4SetValFsEvcCVlanIdPreservation == MEF_ENABLED)
    {
	    i4VlanPreStatus = VLAN_SNMP_FALSE; 
    }	
    else 	
    {
	    i4VlanPreStatus = VLAN_SNMP_TRUE; 
    }

    CVlanEvcInfo.u4IfIndex = 0;
    CVlanEvcInfo.i4CVlanId = 0;

    pCVlanEvcInfo =
	    (tMefCVlanEvcInfo *) RBTreeGetNext (MEF_CVLAN_EVC_TABLE,
			    &CVlanEvcInfo, NULL);

    while (pCVlanEvcInfo != NULL)
    {
	if ((pCVlanEvcInfo->i4EvcIndex == i4FsEvcIndex))
	{
		if (i4VlanPreStatus == VLAN_SNMP_TRUE)
		{
			if (u4PreIfIndex != pCVlanEvcInfo->u4IfIndex)
			{
				if ((nmhSetDot1adMICVidRegistrationUntaggedPep ((INT4)pCVlanEvcInfo->u4IfIndex,
								pCVlanEvcInfo->i4CVlanId,i4VlanPreStatus)) == SNMP_FAILURE)
				{
					return SNMP_FAILURE;
				}
			}
			u4PreIfIndex = pCVlanEvcInfo->u4IfIndex;
		}
		else
		{

			if ((nmhSetDot1adMICVidRegistrationUntaggedPep ((INT4)pCVlanEvcInfo->u4IfIndex,
							pCVlanEvcInfo->i4CVlanId,i4VlanPreStatus)) == SNMP_FAILURE)
			{
				return SNMP_FAILURE;
			}
		}

	}
	CVlanEvcInfo.u4IfIndex = pCVlanEvcInfo->u4IfIndex ;
	CVlanEvcInfo.i4CVlanId = pCVlanEvcInfo->i4CVlanId ;
	pCVlanEvcInfo =
	        (tMefCVlanEvcInfo *) RBTreeGetNext (MEF_CVLAN_EVC_TABLE,
	    		    &CVlanEvcInfo, NULL);


    }   

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEvcCVlanCoSPreservation
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                setValFsEvcCVlanCoSPreservation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcCVlanCoSPreservation (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                                 INT4 i4SetValFsEvcCVlanCoSPreservation)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    tEvcInfo           *pIssEvcInfo = NULL;
    UINT4               u4Action = MEF_EVC_ATTR_CREATE;
    BOOLEAN             bFsEvcCVlanCoSPres;
    if (i4SetValFsEvcCVlanCoSPreservation == MEF_ENABLED)
    {
        bFsEvcCVlanCoSPres = 1;
    }
    if (i4SetValFsEvcCVlanCoSPreservation == MEF_DISABLED)
    {
        bFsEvcCVlanCoSPres = 0;
    }

    pIssEvcInfo = MefGetEvcInfoEntry (i4FsEvcIndex);
    if (pIssEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* EVC Attribute changes update to Vlan and PB modules */
    if (bFsEvcCVlanCoSPres != pIssEvcInfo->bCeVlanCoSPreservation)
    {
        u4Action = MEF_EVC_ATTR_MODIFY;
        pIssEvcInfo->bCeVlanCoSPreservation = bFsEvcCVlanCoSPres;
    }

    if (u4Action == MEF_EVC_ATTR_MODIFY)
    {
        VlanHwSetEvcAttribute (i4FsEvcContextId, u4Action, pIssEvcInfo);
    }

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefEvcInfo->bCeVlanCoSPreservation = i4SetValFsEvcCVlanCoSPreservation;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEvcRowStatus
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                setValFsEvcRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcRowStatus (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                      INT4 i4SetValFsEvcRowStatus)
{
    tMefEvcFilterInfo   MefEvcFilterInfo;
    tMefEvcInfo        *pMefEvcInfo = NULL;
    tEvcInfo           *pIssEvcInfo = NULL;
    UINT4               u4Index = 0;
    UINT4               u4EvcId = 0;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);

    switch (i4SetValFsEvcRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pMefEvcInfo != NULL)
            {
                return SNMP_FAILURE;
            }
            pMefEvcInfo = (tMefEvcInfo *) MemAllocMemBlk (MEF_EVC_POOL_ID);
            if (pMefEvcInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            MefInitDefaultEvcInfo (i4FsEvcContextId, i4FsEvcIndex, pMefEvcInfo);

            if (MefAddEvcEntry (pMefEvcInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            u4EvcId = MefGetFreeEvcId (i4FsEvcIndex);

            pIssEvcInfo = (tEvcInfo *) MemAllocMemBlk (MEF_ISS_EVC_INFO_POOL_ID);
            if (pIssEvcInfo == NULL) 
            {
                return SNMP_FAILURE;
            }

            MefEvcInitDefaultEvcInfo ((INT4)u4EvcId, i4FsEvcIndex, pIssEvcInfo);

            if (MefAddEvcInfoEntry (pIssEvcInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            pMefEvcInfo->i4RowStatus = NOT_READY;
            break;

        case ACTIVE:
            if (pMefEvcInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            if (MefIsMsrInProgress () != OSIX_TRUE)
            {
                /* HDC 59957 */
                if (MefSetServiceType (i4FsEvcContextId, i4FsEvcIndex,
                                       pMefEvcInfo->EvcType) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
                if (MefEvcTunnelInfoToVlan
                    (pMefEvcInfo, i4FsEvcContextId,
                     i4FsEvcIndex) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            pMefEvcInfo->i4RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            if (pMefEvcInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (&MefEvcFilterInfo, 0, sizeof (tMefEvcFilterInfo));
            pMefEvcInfo->i4RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            if (pMefEvcInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            if (MefDeleteEvcEntry (pMefEvcInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            /* Delete Evc Info entry */
            pIssEvcInfo = MefGetEvcInfoEntry (i4FsEvcIndex);
            if (pIssEvcInfo != NULL)
            {
                for (u4Index = 1; u4Index <= VLAN_MAX_VLAN_ID + 1; u4Index++)
                {
                    if (gau4EvcIndex[u4Index - 1] == (pIssEvcInfo->u4EvcId))
                    {
                        gau4EvcIndex[u4Index - 1] = 0;
                        break;
                    }
                }
                if (MefDeleteEvcInfoEntry (pIssEvcInfo) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEvcL2CPTunnel
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object
                setValFsEvcL2CPTunnel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcL2CPTunnel (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsEvcL2CPTunnel)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    UINT4               u4EvcTunnelProtocolList = 0;
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    MEF_CPY_FROM_SNMP (&u4EvcTunnelProtocolList, pSetValFsEvcL2CPTunnel,
                       sizeof (UINT4));

    if (u4EvcTunnelProtocolList & MEF_EVC_DOT1X_BITMASK)
    {
        MEF_EVC_DOT1X_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_LACP_BITMASK)
    {
        MEF_EVC_LACP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_STP_BITMASK)
    {
        MEF_EVC_STP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_GVRP_BITMASK)
    {
        MEF_EVC_GVRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_GMRP_BITMASK)
    {
        MEF_EVC_GMRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_IGMP_BITMASK)
    {
        MEF_EVC_IGMP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_MVRP_BITMASK)
    {
        MEF_EVC_MVRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_MMRP_BITMASK)
    {
        MEF_EVC_MMRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_ELMI_BITMASK)
    {
        MEF_EVC_ELMI_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_LLDP_BITMASK)
    {
        MEF_EVC_LLDP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_ECFM_BITMASK)
    {
        MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_EOAM_BITMASK)
    {
        MEF_EVC_EOAM_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_TUNNEL;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEvcL2CPPeer
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object
                setValFsEvcL2CPPeer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcL2CPPeer (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                     tSNMP_OCTET_STRING_TYPE * pSetValFsEvcL2CPPeer)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    UINT4               u4EvcTunnelProtocolList = 0;
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    MEF_CPY_FROM_SNMP (&u4EvcTunnelProtocolList, pSetValFsEvcL2CPPeer,
                       sizeof (UINT4));

    if (u4EvcTunnelProtocolList & MEF_EVC_DOT1X_BITMASK)
    {
        MEF_EVC_DOT1X_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_LACP_BITMASK)
    {
        MEF_EVC_LACP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_STP_BITMASK)
    {
        MEF_EVC_STP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_ELMI_BITMASK)
    {
        MEF_EVC_ELMI_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_LLDP_BITMASK)
    {
        MEF_EVC_LLDP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_ECFM_BITMASK)
    {
        MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_EOAM_BITMASK)
    {
        MEF_EVC_EOAM_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_PTP_BITMASK)
    {
        MEF_EVC_PTP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_PEER;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEvcL2CPDiscard
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object
                setValFsEvcL2CPDiscard
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcL2CPDiscard (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsEvcL2CPDiscard)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    UINT4               u4EvcTunnelProtocolList = 0;
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    MEF_CPY_FROM_SNMP (&u4EvcTunnelProtocolList, pSetValFsEvcL2CPDiscard,
                       sizeof (UINT4));

    if (u4EvcTunnelProtocolList & MEF_EVC_DOT1X_BITMASK)
    {
        MEF_EVC_DOT1X_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_LACP_BITMASK)
    {
        MEF_EVC_LACP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_STP_BITMASK)
    {
        MEF_EVC_STP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_GVRP_BITMASK)
    {
        MEF_EVC_GVRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_GMRP_BITMASK)
    {
        MEF_EVC_GMRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_IGMP_BITMASK)
    {
        MEF_EVC_IGMP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_MVRP_BITMASK)
    {
        MEF_EVC_MVRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_MMRP_BITMASK)
    {
        MEF_EVC_MMRP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_ELMI_BITMASK)
    {
        MEF_EVC_ELMI_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_LLDP_BITMASK)
    {
        MEF_EVC_LLDP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_ECFM_BITMASK)
    {
        MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_EOAM_BITMASK)
    {
        MEF_EVC_EOAM_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_PTP_BITMASK)
    {
        MEF_EVC_PTP_TUNNEL_STATUS (pMefEvcInfo) = MEF_TUNNEL_PROTOCOL_DISCARD;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvcId
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                testValFsEvcId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcId (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                  tSNMP_OCTET_STRING_TYPE * pTestValFsEvcId)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    INT4                i4NextEvcContextId = 0;
    INT4                i4EvcContextId = 0;
    INT4                i4NextEvc = 0;
    INT4                i4Evc = 0;
    UINT1               au1EvcId[EVC_ID_MAX_LENGTH];
    tSNMP_OCTET_STRING_TYPE EvcId;    
                                                        
    MEMSET (&au1EvcId, 0, EVC_ID_MAX_LENGTH);
    EvcId.pu1_OctetList = au1EvcId;
    EvcId.i4_Length = EVC_ID_MAX_LENGTH;
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pMefEvcInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pTestValFsEvcId->i4_Length > EVC_ID_MAX_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (MEMCMP (pMefEvcInfo->au1EvcId, pTestValFsEvcId->pu1_OctetList, EVC_ID_MAX_LENGTH) == 0)
    {
       return SNMP_SUCCESS;
    }


    if (nmhGetNextIndexFsEvcTable (i4FsEvcContextId, &i4NextEvcContextId,
                                       0, &i4NextEvc) != SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    do 
    {
	if (i4FsEvcContextId != i4NextEvcContextId)
	{
            break;			
	}
	else
	{
	    nmhGetFsEvcId (i4NextEvcContextId, i4NextEvc, &EvcId);
   	    if (MEMCMP (EvcId.pu1_OctetList, pTestValFsEvcId->pu1_OctetList, EVC_ID_MAX_LENGTH) == 0)
	    {			
       		 *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            	 CLI_SET_ERR (MEF_CLI_EVC_ID_EXIST);
       		 return SNMP_FAILURE;
	    } 
	}
		
	i4EvcContextId = i4NextEvcContextId;
        i4Evc = i4NextEvc;
        if (nmhGetNextIndexFsEvcTable (i4EvcContextId, &i4NextEvcContextId,
                                       i4Evc, &i4NextEvc) != SNMP_SUCCESS)
        {
            break;
        }
        continue;
    }
    while (i4EvcContextId == i4NextEvcContextId);
                                     
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEvcType
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                testValFsEvcType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcType (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                    INT4 i4FsEvcIndex, INT4 i4TestValFsEvcType)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pMefEvcInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsEvcType != MEF_POINT_TO_POINT) &&
        (i4TestValFsEvcType != MEF_MULTIPOINT_TO_MULTIPOINT) &&
        (i4TestValFsEvcType != MEF_ROOTED_MULTIPOINT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEvcCVlanIdPreservation
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                testValFsEvcCVlanIdPreservation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcCVlanIdPreservation (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                                   INT4 i4FsEvcIndex,
                                   INT4 i4TestValFsEvcCVlanIdPreservation)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pMefEvcInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsEvcCVlanIdPreservation != MEF_DISABLED) &&
        (i4TestValFsEvcCVlanIdPreservation != MEF_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEvcCVlanCoSPreservation
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                testValFsEvcCVlanCoSPreservation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcCVlanCoSPreservation (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                                    INT4 i4FsEvcIndex,
                                    INT4 i4TestValFsEvcCVlanCoSPreservation)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pMefEvcInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsEvcCVlanCoSPreservation != MEF_DISABLED) &&
        (i4TestValFsEvcCVlanCoSPreservation != MEF_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEvcRowStatus
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                testValFsEvcRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                         INT4 i4FsEvcIndex, INT4 i4TestValFsEvcRowStatus)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);

    if (VcmIsL2VcExist ((UINT4) i4FsEvcContextId) != VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (MefValidateEvcIndexForTransMode ((UINT4) i4FsEvcContextId, i4FsEvcIndex)
        == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    switch (i4TestValFsEvcRowStatus)
    {
        case CREATE_AND_WAIT:

            if (pMefEvcInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case DESTROY:
        case NOT_IN_SERVICE:

            if (pMefEvcInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    if ((i4TestValFsEvcRowStatus == DESTROY) &&
        (EcfmApiValidateEvcToMepMapping ((UINT4) i4FsEvcIndex) ==
                                                   ECFM_FAILURE))
    {
        CLI_SET_ERR (MEF_CLI_MEP_EVC_MAP_EXISTS);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEvcL2CPTunnel
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object
                testValFsEvcL2CPTunnel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcL2CPTunnel (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                          INT4 i4FsEvcIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsEvcL2CPTunnel)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    UINT4               u4EvcTunnelProtocolList = 0;
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return SNMP_FAILURE;
    }
    MEF_CPY_FROM_SNMP (&u4EvcTunnelProtocolList, pTestValFsEvcL2CPTunnel,
                       sizeof (UINT4));
    if (u4EvcTunnelProtocolList > MEF_EVC_MAX_BITMASK)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (u4EvcTunnelProtocolList & MEF_EVC_PTP_BITMASK)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_EVC_TUNNEL_NO_SUPPORT);
        return SNMP_FAILURE;
    }
    if ((u4EvcTunnelProtocolList & MEF_EVC_DOT1X_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_LACP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_STP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_LLDP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_ELMI_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_EOAM_BITMASK))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pMefEvcInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEvcL2CPPeer
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object
                testValFsEvcL2CPPeer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcL2CPPeer (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                        INT4 i4FsEvcIndex,
                        tSNMP_OCTET_STRING_TYPE * pTestValFsEvcL2CPPeer)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    UINT4               u4EvcTunnelProtocolList = 0;
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return SNMP_FAILURE;
    }
    MEF_CPY_FROM_SNMP (&u4EvcTunnelProtocolList, pTestValFsEvcL2CPPeer,
                       sizeof (UINT4));
    if (u4EvcTunnelProtocolList > MEF_EVC_MAX_BITMASK)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4EvcTunnelProtocolList & MEF_EVC_GVRP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_GMRP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_IGMP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_MVRP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_MMRP_BITMASK))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_EVC_PEER_NO_SUPPORT);
        return SNMP_FAILURE;
    }

    if ((u4EvcTunnelProtocolList & MEF_EVC_DOT1X_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_LACP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_STP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_LLDP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_ELMI_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_EOAM_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_PTP_BITMASK))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pMefEvcInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsEvcL2CPDiscard
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object
                testValFsEvcL2CPDiscard
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcL2CPDiscard (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                           INT4 i4FsEvcIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsEvcL2CPDiscard)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;
    UINT4               u4EvcTunnelProtocolList = 0;
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return SNMP_FAILURE;
    }
    MEF_CPY_FROM_SNMP (&u4EvcTunnelProtocolList, pTestValFsEvcL2CPDiscard,
                       sizeof (UINT4));
    if (u4EvcTunnelProtocolList > MEF_EVC_MAX_BITMASK)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4EvcTunnelProtocolList & MEF_EVC_DOT1X_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_LACP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_STP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_LLDP_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_ELMI_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_EOAM_BITMASK)
        || (u4EvcTunnelProtocolList & MEF_EVC_PTP_BITMASK))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pMefEvcInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEvcLoopbackStatus
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                retValFsEvcLoopbackStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcLoopbackStatus (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                           INT4 *pi4RetValFsEvcLoopbackStatus)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsEvcLoopbackStatus = (INT4) pMefEvcInfo->u1LoopbackStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvcLoopbackStatus
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                setValFsEvcLoopbackStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcLoopbackStatus (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                           INT4 i4SetValFsEvcLoopbackStatus)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefEvcInfo->u1LoopbackStatus = (UINT1) i4SetValFsEvcLoopbackStatus;

    /* Configure the EVC loopback Status in VLAN module */
    VLAN_LOCK ();
    if (nmhSetFsMIDot1qFutureVlanLoopbackStatus (i4FsEvcContextId,
                                                 (UINT4) i4FsEvcIndex,
                                                 i4SetValFsEvcLoopbackStatus)
        == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/********************************************************************************************
 Function    :  nmhTestv2FsEvcLoopbackStatus
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex

                The Object 
                testValFsEvcLoopbackStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcLoopbackStatus (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                              INT4 i4FsEvcIndex,
                              INT4 i4TestValFsEvcLoopbackStatus)
{
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsEvcLoopbackStatus != MEF_DISABLED) &&
        (i4TestValFsEvcLoopbackStatus != MEF_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    VLAN_LOCK ();
    if (nmhTestv2FsMIDot1qFutureVlanLoopbackStatus (pu4ErrorCode,
                                                    i4FsEvcContextId,
                                                    (UINT4) i4FsEvcIndex,
                                                    i4TestValFsEvcLoopbackStatus)
        == SNMP_FAILURE)
    {
        VLAN_UNLOCK ();
        return SNMP_FAILURE;
    }
    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEvcTable
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEvcTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsEvcFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEvcFilterTable
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsEvcFilterTable (INT4 i4FsEvcContextId,
                                          INT4 i4FsEvcIndex,
                                          INT4 i4FsEvcFilterInstance)
{
    tMefEvcFilterInfo   MefEvcFilterInfo;
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;

    MEMSET (&MefEvcFilterInfo, 0, sizeof (tMefEvcFilterInfo));

    MefEvcFilterInfo.u4EvcFilterContextId = i4FsEvcContextId;
    MefEvcFilterInfo.i4EvcFilterVlanId = i4FsEvcIndex;
    MefEvcFilterInfo.i4EvcFilterInstance = i4FsEvcFilterInstance;

    pMefEvcFilterInfo = (tMefEvcFilterInfo *)
        RBTreeGet (MEF_EVC_FILTER_TABLE, &MefEvcFilterInfo);
    if (pMefEvcFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsEvcFilterTable
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsEvcFilterTable (INT4 *pi4FsEvcContextId, INT4 *pi4FsEvcIndex,
                                  INT4 *pi4FsEvcFilterInstance)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;

    pMefEvcFilterInfo = (tMefEvcFilterInfo *) RBTreeGetFirst
        (MEF_EVC_FILTER_TABLE);
    if (pMefEvcFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4FsEvcContextId = pMefEvcFilterInfo->u4EvcFilterContextId;
    *pi4FsEvcIndex = pMefEvcFilterInfo->i4EvcFilterVlanId;
    *pi4FsEvcFilterInstance = pMefEvcFilterInfo->i4EvcFilterInstance;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsEvcFilterTable
 Input       :  The Indices
                FsEvcContextId
                nextFsEvcContextId
                FsEvcIndex
                nextFsEvcIndex
                FsEvcFilterInstance
                nextFsEvcFilterInstance
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsEvcFilterTable (INT4 i4FsEvcContextId,
                                 INT4 *pi4NextFsEvcContextId, INT4 i4FsEvcIndex,
                                 INT4 *pi4NextFsEvcIndex,
                                 INT4 i4FsEvcFilterInstance,
                                 INT4 *pi4NextFsEvcFilterInstance)
{
    tMefEvcFilterInfo   MefEvcFilterInfo;
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;

    MEMSET (&MefEvcFilterInfo, 0, sizeof (tMefEvcFilterInfo));
    MefEvcFilterInfo.u4EvcFilterContextId = i4FsEvcContextId;
    MefEvcFilterInfo.i4EvcFilterVlanId = i4FsEvcIndex;
    MefEvcFilterInfo.i4EvcFilterInstance = i4FsEvcFilterInstance;

    pMefEvcFilterInfo = (tMefEvcFilterInfo *) RBTreeGetNext
        (MEF_EVC_FILTER_TABLE, &MefEvcFilterInfo, NULL);
    if (pMefEvcFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsEvcContextId = pMefEvcFilterInfo->u4EvcFilterContextId;
    *pi4NextFsEvcIndex = pMefEvcFilterInfo->i4EvcFilterVlanId;
    *pi4NextFsEvcFilterInstance = pMefEvcFilterInfo->i4EvcFilterInstance;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvcFilterDestMacAddress
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                retValFsEvcFilterDestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcFilterDestMacAddress (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                                 INT4 i4FsEvcFilterInstance,
                                 tMacAddr * pRetValFsEvcFilterDestMacAddress)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;

    pMefEvcFilterInfo = MefGetEvcFilterEntry
        (i4FsEvcContextId, i4FsEvcIndex, i4FsEvcFilterInstance);
    if (pMefEvcFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValFsEvcFilterDestMacAddress,
            &pMefEvcFilterInfo->EvcFilterMacAddr, sizeof (tMacAddr));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsEvcFilterAction
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                retValFsEvcFilterAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcFilterAction (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                         INT4 i4FsEvcFilterInstance,
                         INT4 *pi4RetValFsEvcFilterAction)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;

    pMefEvcFilterInfo = MefGetEvcFilterEntry (i4FsEvcContextId, i4FsEvcIndex,
                                              i4FsEvcFilterInstance);
    if (pMefEvcFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsEvcFilterAction = pMefEvcFilterInfo->i4EvcFilterAction;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEvcFilterId
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                retValFsEvcFilterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcFilterId (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                     INT4 i4FsEvcFilterInstance, INT4 *pi4RetValFsEvcFilterId)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;

    pMefEvcFilterInfo = MefGetEvcFilterEntry (i4FsEvcContextId, i4FsEvcIndex,
                                              i4FsEvcFilterInstance);
    if (pMefEvcFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsEvcFilterId = pMefEvcFilterInfo->i4EvcFilterId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEvcFilterRowStatus
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                retValFsEvcFilterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsEvcFilterRowStatus (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                            INT4 i4FsEvcFilterInstance,
                            INT4 *pi4RetValFsEvcFilterRowStatus)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;

    pMefEvcFilterInfo = MefGetEvcFilterEntry (i4FsEvcContextId, i4FsEvcIndex,
                                              i4FsEvcFilterInstance);
    if (pMefEvcFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsEvcFilterRowStatus = pMefEvcFilterInfo->i4RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsEvcFilterDestMacAddress
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                setValFsEvcFilterDestMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcFilterDestMacAddress (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                                 INT4 i4FsEvcFilterInstance,
                                 tMacAddr SetValFsEvcFilterDestMacAddress)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;

    pMefEvcFilterInfo = MefGetEvcFilterEntry (i4FsEvcContextId, i4FsEvcIndex,
                                              i4FsEvcFilterInstance);
    if (pMefEvcFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pMefEvcFilterInfo->EvcFilterMacAddr,
            SetValFsEvcFilterDestMacAddress, sizeof (tMacAddr));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsEvcFilterAction
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                setValFsEvcFilterAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcFilterAction (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                         INT4 i4FsEvcFilterInstance,
                         INT4 i4SetValFsEvcFilterAction)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;

    pMefEvcFilterInfo = MefGetEvcFilterEntry (i4FsEvcContextId, i4FsEvcIndex,
                                              i4FsEvcFilterInstance);
    if (pMefEvcFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefEvcFilterInfo->i4EvcFilterAction = i4SetValFsEvcFilterAction;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEvcFilterId
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                setValFsEvcFilterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcFilterId (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                     INT4 i4FsEvcFilterInstance, INT4 i4SetValFsEvcFilterId)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;

    pMefEvcFilterInfo = MefGetEvcFilterEntry (i4FsEvcContextId, i4FsEvcIndex,
                                              i4FsEvcFilterInstance);
    if (pMefEvcFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pMefEvcFilterInfo->i4EvcFilterId = i4SetValFsEvcFilterId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEvcFilterRowStatus
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                setValFsEvcFilterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsEvcFilterRowStatus (INT4 i4FsEvcContextId, INT4 i4FsEvcIndex,
                            INT4 i4FsEvcFilterInstance,
                            INT4 i4SetValFsEvcFilterRowStatus)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;
    tMefEvcInfo        *pMefEvcInfo = NULL;
    UINT4               u4Error = 0;

    /* To Create EVC Filter, Entry EVC Entry should be created
     * and should not be in ACTVIE state */
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if (pMefEvcInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return SNMP_FAILURE;
    }

    pMefEvcFilterInfo = MefGetEvcFilterEntry (i4FsEvcContextId, i4FsEvcIndex,
                                              i4FsEvcFilterInstance);

    switch (i4SetValFsEvcFilterRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pMefEvcFilterInfo != NULL)
            {
                CLI_SET_ERR (MEF_CLI_FILTER_EXIST);
                return SNMP_FAILURE;
            }
            pMefEvcFilterInfo =
                (tMefEvcFilterInfo *) MemAllocMemBlk (MEF_EVC_FILTER_POOL_ID);
            if (pMefEvcFilterInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            MEMSET (pMefEvcFilterInfo, 0, sizeof (tMefEvcFilterInfo));
            /*Updating the other structure elements with default values */
            pMefEvcFilterInfo->i4EvcFilterAction = ISS_DROP;
            pMefEvcFilterInfo->u4EvcFilterContextId = i4FsEvcContextId;
            pMefEvcFilterInfo->i4EvcFilterVlanId = i4FsEvcIndex;
            pMefEvcFilterInfo->i4EvcFilterInstance = i4FsEvcFilterInstance;
            pMefEvcFilterInfo->i4RowStatus = CREATE_AND_WAIT;

            if (RBTreeAdd (MEF_EVC_FILTER_TABLE, pMefEvcFilterInfo) !=
                RB_SUCCESS)
            {
                MemReleaseMemBlock (MEF_EVC_FILTER_POOL_ID,
                                    (UINT1 *) pMefEvcFilterInfo);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
            if (pMefEvcFilterInfo == NULL)
            {
                CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
                return SNMP_FAILURE;
            }
            if (MefIsMsrInProgress () != OSIX_TRUE)
            {
                if (MefSetEvcEntries (i4FsEvcContextId, i4FsEvcIndex,
                                      i4FsEvcFilterInstance) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            pMefEvcFilterInfo->i4RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            if (pMefEvcFilterInfo == NULL)
            {
                CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
                return SNMP_FAILURE;
            }

            if (nmhTestv2IssExtL2FilterStatus (&u4Error,
                                               pMefEvcFilterInfo->i4EvcFilterId,
                                               DESTROY) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (nmhSetIssExtL2FilterStatus
                (pMefEvcFilterInfo->i4EvcFilterId, DESTROY) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            pMefEvcFilterInfo->i4RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            if (pMefEvcFilterInfo == NULL)
            {
                CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
                return SNMP_FAILURE;
            }
            nmhTestv2IssExtL2FilterStatus (&u4Error,
                                           pMefEvcFilterInfo->i4EvcFilterId,
                                           DESTROY);
            nmhSetIssExtL2FilterStatus (pMefEvcFilterInfo->i4EvcFilterId,
                                        DESTROY);

            /* Delete the EVC Filter entry */
            RBTreeRemove (MEF_EVC_FILTER_TABLE, pMefEvcFilterInfo);
            MemReleaseMemBlock (MEF_EVC_FILTER_POOL_ID,
                                (UINT1 *) pMefEvcFilterInfo);
            break;

        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsEvcFilterDestMacAddress
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                testValFsEvcFilterDestMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcFilterDestMacAddress (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                                    INT4 i4FsEvcIndex,
                                    INT4 i4FsEvcFilterInstance,
                                    tMacAddr TestValFsEvcFilterDestMacAddress)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;
    tMefEvcInfo        *pMefEvcInfo = NULL;

    UNUSED_PARAM (TestValFsEvcFilterDestMacAddress);

    pMefEvcFilterInfo = MefGetEvcFilterEntry (i4FsEvcContextId, i4FsEvcIndex,
                                              i4FsEvcFilterInstance);
    if (pMefEvcFilterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    else if (pMefEvcFilterInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* To Create EVC Filter Entry EVC Entry should be created
     * and should not be in ACTVIE state */
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if ((pMefEvcInfo == NULL) || (pMefEvcInfo->i4RowStatus == ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEvcFilterAction
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                testValFsEvcFilterAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcFilterAction (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                            INT4 i4FsEvcIndex, INT4 i4FsEvcFilterInstance,
                            INT4 i4TestValFsEvcFilterAction)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;
    tMefEvcInfo        *pMefEvcInfo = NULL;

    pMefEvcFilterInfo = MefGetEvcFilterEntry (i4FsEvcContextId, i4FsEvcIndex,
                                              i4FsEvcFilterInstance);
    if (pMefEvcFilterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    else if (pMefEvcFilterInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* To Create EVC Filter Entry EVC Entry should be created
     * and should not be in ACTVIE state */
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if ((pMefEvcInfo == NULL) || (pMefEvcInfo->i4RowStatus == ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsEvcFilterAction != ISS_ALLOW) &&
        (i4TestValFsEvcFilterAction != ISS_DROP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEvcFilterId
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                testValFsEvcFilterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcFilterId (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                        INT4 i4FsEvcIndex, INT4 i4FsEvcFilterInstance,
                        INT4 i4TestValFsEvcFilterId)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;
    tMefEvcInfo        *pMefEvcInfo = NULL;

    /* To Create EVC Filter Entry EVC Entry should be created
     * and should not be in ACTVIE state */
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if ((pMefEvcInfo == NULL) || (pMefEvcInfo->i4RowStatus == ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceIssAclL2FilterTable (i4TestValFsEvcFilterId)
        == SNMP_SUCCESS)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_EXIST);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pMefEvcFilterInfo = MefGetEvcFilterEntry (i4FsEvcContextId, i4FsEvcIndex,
                                              i4FsEvcFilterInstance);

    if (pMefEvcFilterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    else if (pMefEvcFilterInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEvcFilterRowStatus
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance

                The Object 
                testValFsEvcFilterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEvcFilterRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsEvcContextId,
                               INT4 i4FsEvcIndex, INT4 i4FsEvcFilterInstance,
                               INT4 i4TestValFsEvcFilterRowStatus)
{
    tMefEvcFilterInfo  *pMefEvcFilterInfo = NULL;
    tMefEvcInfo        *pMefEvcInfo = NULL;

    if (VcmIsL2VcExist ((UINT4) i4FsEvcContextId) != VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* To Create EVC Filter, Entry EVC Entry should be created
     * and should not be in ACTVIE state */
    pMefEvcInfo = MefGetEvcEntry (i4FsEvcContextId, i4FsEvcIndex);
    if ((pMefEvcInfo == NULL) || (pMefEvcInfo->i4RowStatus == ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return SNMP_FAILURE;
    }

    pMefEvcFilterInfo = MefGetEvcFilterEntry (i4FsEvcContextId, i4FsEvcIndex,
                                              i4FsEvcFilterInstance);

    switch (i4TestValFsEvcFilterRowStatus)
    {
        case CREATE_AND_WAIT:

            if (pMefEvcFilterInfo != NULL)
            {
                CLI_SET_ERR (MEF_CLI_FILTER_EXIST);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case DESTROY:
        case NOT_IN_SERVICE:

            if (pMefEvcInfo == NULL)
            {
                CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsEvcFilterTable
 Input       :  The Indices
                FsEvcContextId
                FsEvcIndex
                FsEvcFilterInstance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsEvcFilterTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefETreeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefETreeTable
 Input       :  The Indices
                FsMefETreeIngresPort
                FsMefETreeEvcIndex
                FsMefETreeEgressPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefETreeTable (INT4 i4FsMefETreeIngresPort,
                                         INT4 i4FsMefETreeEvcIndex,
                                         INT4 i4FsMefETreeEgressPort)
{
    return (nmhValidateIndexInstanceIssPortIsolationTable
            (i4FsMefETreeIngresPort, i4FsMefETreeEvcIndex,
             i4FsMefETreeEgressPort));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefETreeTable
 Input       :  The Indices
                FsMefETreeIngresPort
                FsMefETreeEvcIndex
                FsMefETreeEgressPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefETreeTable (INT4 *pi4FsMefETreeIngresPort,
                                 INT4 *pi4FsMefETreeEvcIndex,
                                 INT4 *pi4FsMefETreeEgressPort)
{
    return (nmhGetFirstIndexIssPortIsolationTable
            (pi4FsMefETreeIngresPort, pi4FsMefETreeEvcIndex,
             pi4FsMefETreeEgressPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefETreeTable
 Input       :  The Indices
                FsMefETreeIngresPort
                nextFsMefETreeIngresPort
                FsMefETreeEvcIndex
                nextFsMefETreeEvcIndex
                FsMefETreeEgressPort
                nextFsMefETreeEgressPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefETreeTable (INT4 i4FsMefETreeIngresPort,
                                INT4 *pi4NextFsMefETreeIngresPort,
                                INT4 i4FsMefETreeEvcIndex,
                                INT4 *pi4NextFsMefETreeEvcIndex,
                                INT4 i4FsMefETreeEgressPort,
                                INT4 *pi4NextFsMefETreeEgressPort)
{
    return (nmhGetNextIndexIssPortIsolationTable
            (i4FsMefETreeIngresPort, pi4NextFsMefETreeIngresPort,
             i4FsMefETreeEvcIndex, pi4NextFsMefETreeEvcIndex,
             i4FsMefETreeEgressPort, pi4NextFsMefETreeEgressPort));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefETreeRowStatus
 Input       :  The Indices
                FsMefETreeIngresPort
                FsMefETreeEvcIndex
                FsMefETreeEgressPort

                The Object 
                retValFsMefETreeRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefETreeRowStatus (INT4 i4FsMefETreeIngresPort,
                           INT4 i4FsMefETreeEvcIndex,
                           INT4 i4FsMefETreeEgressPort,
                           INT4 *pi4RetValFsMefETreeRowStatus)
{
    return (nmhGetIssPortIsolationRowStatus
            (i4FsMefETreeIngresPort, i4FsMefETreeEvcIndex,
             i4FsMefETreeEgressPort, pi4RetValFsMefETreeRowStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMefETreeRowStatus
 Input       :  The Indices
                FsMefETreeIngresPort
                FsMefETreeEvcIndex
                FsMefETreeEgressPort

                The Object 
                setValFsMefETreeRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefETreeRowStatus (INT4 i4FsMefETreeIngresPort,
                           INT4 i4FsMefETreeEvcIndex,
                           INT4 i4FsMefETreeEgressPort,
                           INT4 i4SetValFsMefETreeRowStatus)
{
    return (nmhSetIssPortIsolationRowStatus
            (i4FsMefETreeIngresPort, i4FsMefETreeEvcIndex,
             i4FsMefETreeEgressPort, i4SetValFsMefETreeRowStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMefETreeRowStatus
 Input       :  The Indices
                FsMefETreeIngresPort
                FsMefETreeEvcIndex
                FsMefETreeEgressPort

                The Object 
                testValFsMefETreeRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefETreeRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMefETreeIngresPort,
                              INT4 i4FsMefETreeEvcIndex,
                              INT4 i4FsMefETreeEgressPort,
                              INT4 i4TestValFsMefETreeRowStatus)
{
    return (nmhTestv2IssPortIsolationRowStatus
            (pu4ErrorCode, i4FsMefETreeIngresPort, i4FsMefETreeEvcIndex,
             i4FsMefETreeEgressPort, i4TestValFsMefETreeRowStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMefETreeTable
 Input       :  The Indices
                FsMefETreeIngresPort
                FsMefETreeEvcIndex
                FsMefETreeEgressPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMefETreeTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsUniCVlanEvcTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsUniCVlanEvcTable
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsUniCVlanEvcTable (INT4 i4IfIndex,
                                            INT4 i4FsUniCVlanEvcCVlanId)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    tMefCVlanEvcInfo    CVlanEvcInfo;

    MEMSET (&CVlanEvcInfo, 0, sizeof (tMefCVlanEvcInfo));
    CVlanEvcInfo.u4IfIndex = i4IfIndex;
    CVlanEvcInfo.i4CVlanId = i4FsUniCVlanEvcCVlanId;

    pCVlanEvcInfo = (tMefCVlanEvcInfo *) RBTreeGet
        (MEF_CVLAN_EVC_TABLE, &CVlanEvcInfo);

    if (pCVlanEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsUniCVlanEvcTable
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsUniCVlanEvcTable (INT4 *pi4IfIndex,
                                    INT4 *pi4FsUniCVlanEvcCVlanId)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;

    pCVlanEvcInfo = (tMefCVlanEvcInfo *) RBTreeGetFirst (MEF_CVLAN_EVC_TABLE);
    if (pCVlanEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = pCVlanEvcInfo->u4IfIndex;
    *pi4FsUniCVlanEvcCVlanId = pCVlanEvcInfo->i4CVlanId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsUniCVlanEvcTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsUniCVlanEvcCVlanId
                nextFsUniCVlanEvcCVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsUniCVlanEvcTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                   INT4 i4FsUniCVlanEvcCVlanId,
                                   INT4 *pi4NextFsUniCVlanEvcCVlanId)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    tMefCVlanEvcInfo    CVlanEvcInfo;

    MEMSET (&CVlanEvcInfo, 0, sizeof (tMefCVlanEvcInfo));
    CVlanEvcInfo.u4IfIndex = i4IfIndex;
    CVlanEvcInfo.i4CVlanId = i4FsUniCVlanEvcCVlanId;

    pCVlanEvcInfo = (tMefCVlanEvcInfo *) RBTreeGetNext
        (MEF_CVLAN_EVC_TABLE, &CVlanEvcInfo, NULL);

    if (pCVlanEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = pCVlanEvcInfo->u4IfIndex;
    *pi4NextFsUniCVlanEvcCVlanId = pCVlanEvcInfo->i4CVlanId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsUniCVlanEvcEvcIndex
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId

                The Object 
                retValFsUniCVlanEvcEvcIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniCVlanEvcEvcIndex (INT4 i4IfIndex, INT4 i4FsUniCVlanEvcCVlanId,
                             INT4 *pi4RetValFsUniCVlanEvcEvcIndex)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;

    pCVlanEvcInfo = MefGetCVlanEvcEntry (i4IfIndex, i4FsUniCVlanEvcCVlanId);
    if (pCVlanEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsUniCVlanEvcEvcIndex = pCVlanEvcInfo->i4EvcIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniCVlanEvcRowStatus
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId

                The Object 
                retValFsUniCVlanEvcRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniCVlanEvcRowStatus (INT4 i4IfIndex, INT4 i4FsUniCVlanEvcCVlanId,
                              INT4 *pi4RetValFsUniCVlanEvcRowStatus)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;

    pCVlanEvcInfo = MefGetCVlanEvcEntry (i4IfIndex, i4FsUniCVlanEvcCVlanId);
    if (pCVlanEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsUniCVlanEvcRowStatus = pCVlanEvcInfo->u1RowStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUniEvcId
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId

                The Object 
                retValFsUniEvcId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUniEvcId (INT4 i4IfIndex, INT4 i4FsUniCVlanEvcCVlanId,
                  tSNMP_OCTET_STRING_TYPE * pRetValFsUniEvcId)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;

    pCVlanEvcInfo = MefGetCVlanEvcEntry (i4IfIndex, i4FsUniCVlanEvcCVlanId);
    if (pCVlanEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEF_CPY_TO_SNMP (pRetValFsUniEvcId,
                     &pCVlanEvcInfo->au1UniEvcId,
                     STRLEN (pCVlanEvcInfo->au1UniEvcId));
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsUniCVlanEvcEvcIndex
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId

                The Object 
                setValFsUniCVlanEvcEvcIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniCVlanEvcEvcIndex (INT4 i4IfIndex, INT4 i4FsUniCVlanEvcCVlanId,
                             INT4 i4SetValFsUniCVlanEvcEvcIndex)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;

    pCVlanEvcInfo = MefGetCVlanEvcEntry (i4IfIndex, i4FsUniCVlanEvcCVlanId);
    if (pCVlanEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pCVlanEvcInfo->i4EvcIndex = i4SetValFsUniCVlanEvcEvcIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsUniCVlanEvcRowStatus
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId

                The Object 
                setValFsUniCVlanEvcRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniCVlanEvcRowStatus (INT4 i4IfIndex, INT4 i4FsUniCVlanEvcCVlanId,
                              INT4 i4SetValFsUniCVlanEvcRowStatus)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    UINT4               u4ErrorCode = 0;
    UINT4               u4ContextId = 0;
    INT4                i4BrgPortType = 0;
    INT4                i4CVidRegRowStatus = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1Flag = MEF_TRANS_MODE_PB;

    pCVlanEvcInfo = MefGetCVlanEvcEntry (i4IfIndex, i4FsUniCVlanEvcCVlanId);
    CfaGetInterfaceBrgPortType (i4IfIndex, &i4BrgPortType);

    if (MefGetContexId (i4IfIndex, &u4ContextId, &u2LocalPort) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (FsMefUtilGetTransportMode (u4ContextId) != MEF_TRANS_MODE_PB)
    {
        u1Flag = MEF_TRANS_MODE_MPLS;
    }

    switch (i4SetValFsUniCVlanEvcRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pCVlanEvcInfo != NULL)
            {
                return SNMP_FAILURE;
            }
            pCVlanEvcInfo =
                (tMefCVlanEvcInfo *) MemAllocMemBlk (MEF_CVLAN_EVC_POOL_ID);
            if (pCVlanEvcInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            MEMSET (pCVlanEvcInfo, 0, sizeof (tMefCVlanEvcInfo));
            pCVlanEvcInfo->u4IfIndex = i4IfIndex;
            pCVlanEvcInfo->i4CVlanId = i4FsUniCVlanEvcCVlanId;

            if (MefAddCVlanEvcEntry (pCVlanEvcInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            pCVlanEvcInfo->u1RowStatus = NOT_READY;
            break;

        case ACTIVE:
            if (pCVlanEvcInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            if (MefIsMsrInProgress () != OSIX_TRUE)
            {
                if (u1Flag == MEF_TRANS_MODE_PB)
                {
                    if (MefSetUniCVlanEvcEntries (pCVlanEvcInfo) ==
                        OSIX_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                }
                else
                {
#ifdef MPLS_WANTED
                    if (MefSetMPLSCVlanEvcEntry (pCVlanEvcInfo) == OSIX_FAILURE)
                    {
                        MefDeleteMPLSCVlanEvcEntry (pCVlanEvcInfo);
                        return SNMP_FAILURE;
                    }
#endif
                }
            }
            pCVlanEvcInfo->u1RowStatus = ACTIVE;

            break;

        case NOT_IN_SERVICE:

            if (pCVlanEvcInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            if (u1Flag == MEF_TRANS_MODE_PB)
            {

                if (i4BrgPortType == MEF_CNP_PORTBASED_PORT)
                {
                    pCVlanEvcInfo->u1RowStatus = NOT_IN_SERVICE;
                    return SNMP_SUCCESS;
                }
                VLAN_LOCK ();

                if (nmhTestv2Dot1adMICVidRegistrationRowStatus (&u4ErrorCode,
                                                                i4IfIndex,
                                                                i4FsUniCVlanEvcCVlanId,
                                                                NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    VLAN_UNLOCK ();
                    return SNMP_FAILURE;
                }

                if (nmhSetDot1adMICVidRegistrationRowStatus (i4IfIndex,
                                                             i4FsUniCVlanEvcCVlanId,
                                                             NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    VLAN_UNLOCK ();
                    return SNMP_FAILURE;
                }

                VLAN_UNLOCK ();
            }
            else
            {

#ifdef MPLS_WANTED
                if (MefSetMPLSCVlanEvcEntry (pCVlanEvcInfo) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
#endif
            }
            pCVlanEvcInfo->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            if (pCVlanEvcInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            if (u1Flag == MEF_TRANS_MODE_PB)
            {
                if (i4BrgPortType == MEF_CNP_PORTBASED_PORT)
                {
                    if (MefDeleteCVlanEvcEntry (pCVlanEvcInfo) == OSIX_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                    return SNMP_SUCCESS;
                }

                VLAN_LOCK ();

                if (nmhGetDot1adMICVidRegistrationRowStatus (i4IfIndex,
                                                             i4FsUniCVlanEvcCVlanId,
                                                             &i4CVidRegRowStatus)
                    != SNMP_FAILURE)
                {
                    if (nmhTestv2Dot1adMICVidRegistrationRowStatus
                        (&u4ErrorCode, i4IfIndex, i4FsUniCVlanEvcCVlanId,
                         DESTROY) == SNMP_FAILURE)
                    {
                        VLAN_UNLOCK ();
                        return SNMP_FAILURE;
                    }

                    if (nmhSetDot1adMICVidRegistrationRowStatus (i4IfIndex,
                                                                 i4FsUniCVlanEvcCVlanId,
                                                                 DESTROY) ==
                        SNMP_FAILURE)
                    {
                        VLAN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }

                VLAN_UNLOCK ();
            }
            else
            {
#ifdef MPLS_WANTED
                if (MefDeleteMPLSCVlanEvcEntry (pCVlanEvcInfo) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
#endif
            }
            if (MefDeleteCVlanEvcEntry (pCVlanEvcInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsUniEvcId
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId

                The Object 
                setValFsUniEvcId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsUniEvcId (INT4 i4IfIndex, INT4 i4FsUniCVlanEvcCVlanId,
                  tSNMP_OCTET_STRING_TYPE * pSetValFsUniEvcId)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;

    pCVlanEvcInfo = MefGetCVlanEvcEntry (i4IfIndex, i4FsUniCVlanEvcCVlanId);
    if (pCVlanEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&pCVlanEvcInfo->au1UniEvcId, 0, UNI_ID_MAX_LENGTH);
    MEF_CPY_FROM_SNMP (&pCVlanEvcInfo->au1UniEvcId, pSetValFsUniEvcId,
                       pSetValFsUniEvcId->i4_Length);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsUniCVlanEvcEvcIndex
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId

                The Object 
                testValFsUniCVlanEvcEvcIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniCVlanEvcEvcIndex (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4FsUniCVlanEvcCVlanId,
                                INT4 i4TestValFsUniCVlanEvcEvcIndex)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    tMefEvcInfo        *pEvcInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    pCVlanEvcInfo = MefGetCVlanEvcEntry (i4IfIndex, i4FsUniCVlanEvcCVlanId);
    if (pCVlanEvcInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_CVLAN_EVC_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if (MefGetContexId (i4IfIndex, &u4ContextId, &u2LocalPort) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_PORT_MAP_ERR);
        return SNMP_FAILURE;
    }

    pEvcInfo = MefGetEvcEntry (u4ContextId, i4TestValFsUniCVlanEvcEvcIndex);
    if (pEvcInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return SNMP_FAILURE;
    }
    if (MefValidateEvcIndexForTransMode
        (u4ContextId, i4TestValFsUniCVlanEvcEvcIndex) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pCVlanEvcInfo->u1RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsUniCVlanEvcRowStatus
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId

                The Object 
                testValFsUniCVlanEvcRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniCVlanEvcRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 INT4 i4FsUniCVlanEvcCVlanId,
                                 INT4 i4TestValFsUniCVlanEvcRowStatus)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4BridgeMode = 0;
    UINT2               u2LocalPort = 0;
    INT4                i4BrgPortType = 0;
    UINT1               u1TransModeFlag = 0;

    pCVlanEvcInfo = MefGetCVlanEvcEntry (i4IfIndex, i4FsUniCVlanEvcCVlanId);

    if (MefGetContexId (i4IfIndex, &u4ContextId, &u2LocalPort) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_PORT_MAP_ERR);
        return SNMP_FAILURE;
    }


    CfaGetInterfaceBrgPortType (i4IfIndex, &i4BrgPortType);
    L2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);
    u1TransModeFlag = FsMefUtilGetTransportMode (u4ContextId);

    switch (i4TestValFsUniCVlanEvcRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pCVlanEvcInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (MEF_CLI_CVLAN_EVC_EXIST);
                return SNMP_FAILURE;
            }

            if ((u1TransModeFlag == MEF_TRANS_MODE_MPLS) &&
                (u4BridgeMode == MEF_CUSTOMER_BRIDGE_MODE))
            {
                if (VlanValidatePortType (i4IfIndex, MEF_CUSTOMER_BRIDGE_PORT)
                    == VLAN_FALSE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (MEF_CLI_BRG_PORT_TYPE_MODE_ERR);
                }
            }

            if ((u1TransModeFlag == MEF_TRANS_MODE_PB) &&
                (u4BridgeMode == MEF_PROVIDER_EDGE_BRIDGE_MODE))
            {

                if (i4BrgPortType != MEF_CUSTOMER_EDGE_PORT)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (MEF_CLI_CVID_ENTRY_ERR);
                    return SNMP_FAILURE;
                }
            }
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:
            if (pCVlanEvcInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (MEF_CLI_CVLAN_EVC_NOT_EXIST);
                return SNMP_FAILURE;
            }
            if ((pCVlanEvcInfo->i4EvcIndex == 0) &&
                i4TestValFsUniCVlanEvcRowStatus == ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    if ((i4BrgPortType == MEF_CNP_PORTBASED_PORT) &&
        (i4FsUniCVlanEvcCVlanId != 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_BRG_PORT_TYPE_MODE_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsUniEvcId
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId

                The Object 
                testValFsUniEvcId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsUniEvcId (UINT4 *pu4ErrorCode,
                     INT4 i4IfIndex,
                     INT4 i4FsUniCVlanEvcCVlanId,
                     tSNMP_OCTET_STRING_TYPE * pTestValFsUniEvcId)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    tMefCVlanEvcInfo    CVlanEvcInfo;

    pCVlanEvcInfo = MefGetCVlanEvcEntry (i4IfIndex, i4FsUniCVlanEvcCVlanId);
    if (pCVlanEvcInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_CVLAN_EVC_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if (pTestValFsUniEvcId->i4_Length > UNI_ID_MAX_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pCVlanEvcInfo->u1RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    MEMSET (&CVlanEvcInfo, 0, sizeof (tMefCVlanEvcInfo));
    CVlanEvcInfo.u4IfIndex = 0;
    CVlanEvcInfo.i4CVlanId = 0;

    pCVlanEvcInfo =
        (tMefCVlanEvcInfo *) RBTreeGetNext (MEF_CVLAN_EVC_TABLE, &CVlanEvcInfo,
                                            NULL);

    while (pCVlanEvcInfo != NULL)
    {
        if ((pCVlanEvcInfo->u4IfIndex != (UINT4) i4IfIndex) &&
            (pCVlanEvcInfo->i4CVlanId != i4FsUniCVlanEvcCVlanId) &&
            ((STRLEN (pCVlanEvcInfo->au1UniEvcId) ==
              (UINT4) pTestValFsUniEvcId->i4_Length) &&
             (STRNCMP
              (pCVlanEvcInfo->au1UniEvcId, pTestValFsUniEvcId->pu1_OctetList,
               pTestValFsUniEvcId->i4_Length) == 0)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (MEF_CLI_UNI_EVC_ID_EXIST);
            return SNMP_FAILURE;
        }

        CVlanEvcInfo.u4IfIndex = pCVlanEvcInfo->u4IfIndex;
        CVlanEvcInfo.i4CVlanId = pCVlanEvcInfo->i4CVlanId;

        pCVlanEvcInfo =
            (tMefCVlanEvcInfo *) RBTreeGetNext (MEF_CVLAN_EVC_TABLE,
                                                &CVlanEvcInfo, NULL);
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsUniCVlanEvcTable
 Input       :  The Indices
                IfIndex
                FsUniCVlanEvcCVlanId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsUniCVlanEvcTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefFilterTable
 Input       :  The Indices
                FsMefFilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefFilterTable (INT4 i4FsMefFilterNo)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;
    tMefFilterInfo      MefFilterInfo;

    MEMSET (&MefFilterInfo, 0, sizeof (tMefFilterInfo));
    MefFilterInfo.u4FilterNo = i4FsMefFilterNo;

    pMefFilterInfo =
        (tMefFilterInfo *) RBTreeGet (MEF_FILTER_TABLE, &MefFilterInfo);

    if (pMefFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefFilterTable
 Input       :  The Indices
                FsMefFilterNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefFilterTable (INT4 *pi4FsMefFilterNo)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = (tMefFilterInfo *) RBTreeGetFirst (MEF_FILTER_TABLE);
    if (pMefFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMefFilterNo = pMefFilterInfo->u4FilterNo;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefFilterTable
 Input       :  The Indices
                FsMefFilterNo
                nextFsMefFilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefFilterTable (INT4 i4FsMefFilterNo,
                                 INT4 *pi4NextFsMefFilterNo)
{
    tMefFilterInfo      MefFilterInfo;
    tMefFilterInfo     *pMefFilterInfo = NULL;

    MEMSET (&MefFilterInfo, 0, sizeof (tMefFilterInfo));
    MefFilterInfo.u4FilterNo = i4FsMefFilterNo;

    pMefFilterInfo = (tMefFilterInfo *)
        RBTreeGetNext (MEF_FILTER_TABLE, &MefFilterInfo, NULL);
    if (pMefFilterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMefFilterNo = pMefFilterInfo->u4FilterNo;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefFilterDscp
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                retValFsMefFilterDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFilterDscp (INT4 i4FsMefFilterNo, INT4 *pi4RetValFsMefFilterDscp)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefFilterDscp = pMefFilterInfo->i1Dscp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFilterDirection
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                retValFsMefFilterDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFilterDirection (INT4 i4FsMefFilterNo,
                            INT4 *pi4RetValFsMefFilterDirection)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefFilterDirection = pMefFilterInfo->u1Direction;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFilterEvc
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                retValFsMefFilterEvc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFilterEvc (INT4 i4FsMefFilterNo, INT4 *pi4RetValFsMefFilterEvc)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefFilterEvc = pMefFilterInfo->u2Evc;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFilterCVlanPriority
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                retValFsMefFilterCVlanPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFilterCVlanPriority (INT4 i4FsMefFilterNo,
                                INT4 *pi4RetValFsMefFilterCVlanPriority)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefFilterCVlanPriority = pMefFilterInfo->i1CVlanPriority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFilterIfIndex
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                retValFsMefFilterIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFilterIfIndex (INT4 i4FsMefFilterNo,
                          INT4 *pi4RetValFsMefFilterIfIndex)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefFilterIfIndex = pMefFilterInfo->u4IfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFilterStatus
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                retValFsMefFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFilterStatus (INT4 i4FsMefFilterNo, INT4 *pi4RetValFsMefFilterStatus)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefFilterStatus = pMefFilterInfo->u1Status;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMefFilterDscp
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                setValFsMefFilterDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefFilterDscp (INT4 i4FsMefFilterNo, INT4 i4SetValFsMefFilterDscp)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    pMefFilterInfo->i1Dscp = i4SetValFsMefFilterDscp;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefFilterDirection
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                setValFsMefFilterDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefFilterDirection (INT4 i4FsMefFilterNo,
                            INT4 i4SetValFsMefFilterDirection)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    pMefFilterInfo->u1Direction = i4SetValFsMefFilterDirection;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefFilterEvc
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                setValFsMefFilterEvc
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefFilterEvc (INT4 i4FsMefFilterNo, INT4 i4SetValFsMefFilterEvc)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    pMefFilterInfo->u2Evc = i4SetValFsMefFilterEvc;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefFilterCVlanPriority
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                setValFsMefFilterCVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefFilterCVlanPriority (INT4 i4FsMefFilterNo,
                                INT4 i4SetValFsMefFilterCVlanPriority)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    pMefFilterInfo->i1CVlanPriority = i4SetValFsMefFilterCVlanPriority;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefFilterIfIndex
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                setValFsMefFilterIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefFilterIfIndex (INT4 i4FsMefFilterNo, INT4 i4SetValFsMefFilterIfIndex)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    pMefFilterInfo->u4IfIndex = i4SetValFsMefFilterIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefFilterStatus
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                setValFsMefFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefFilterStatus (INT4 i4FsMefFilterNo, INT4 i4SetValFsMefFilterStatus)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);

    if ((i4SetValFsMefFilterStatus == CREATE_AND_WAIT) &&
        (pMefFilterInfo != NULL))
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsMefFilterStatus)
    {
        case CREATE_AND_WAIT:

            pMefFilterInfo =
                (tMefFilterInfo *) MemAllocMemBlk (MEF_FILTER_POOL_ID);
            if (pMefFilterInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            MefInitDefaultFilterInfo (i4FsMefFilterNo, pMefFilterInfo);

            if (MefAddFilterEntry (pMefFilterInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            pMefFilterInfo->u1Status = NOT_READY;

            break;

        case ACTIVE:

            if (MefIsMsrInProgress () != OSIX_TRUE)
            {
                if (MefSetFilterEntries (i4FsMefFilterNo) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            pMefFilterInfo->u1Status = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            pMefFilterInfo->u1Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            if (MefDeleteFilterEntry (pMefFilterInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMefFilterDscp
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                testValFsMefFilterDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefFilterDscp (UINT4 *pu4ErrorCode, INT4 i4FsMefFilterNo,
                          INT4 i4TestValFsMefFilterDscp)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    UNUSED_PARAM (i4TestValFsMefFilterDscp);

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMefFilterDscp < MEF_MIN_DSCP_VALUE) ||
        (i4TestValFsMefFilterDscp > MEF_MAX_DSCP_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pMefFilterInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefFilterDirection
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                testValFsMefFilterDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefFilterDirection (UINT4 *pu4ErrorCode, INT4 i4FsMefFilterNo,
                               INT4 i4TestValFsMefFilterDirection)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if (pMefFilterInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMefFilterDirection != MEF_FILTER_DIRECTION_IN) &&
        (i4TestValFsMefFilterDirection != MEF_FILTER_DIRECTION_OUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefFilterEvc
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                testValFsMefFilterEvc
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefFilterEvc (UINT4 *pu4ErrorCode, INT4 i4FsMefFilterNo,
                         INT4 i4TestValFsMefFilterEvc)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    UNUSED_PARAM (i4TestValFsMefFilterEvc);

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if (pMefFilterInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefFilterCVlanPriority
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                testValFsMefFilterCVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefFilterCVlanPriority (UINT4 *pu4ErrorCode, INT4 i4FsMefFilterNo,
                                   INT4 i4TestValFsMefFilterCVlanPriority)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if (pMefFilterInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMefFilterCVlanPriority < MEF_MIN_CVLAN_PRIORITY) ||
        (i4TestValFsMefFilterCVlanPriority > MEF_MAX_CVLAN_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefFilterIfIndex
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                testValFsMefFilterIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefFilterIfIndex (UINT4 *pu4ErrorCode, INT4 i4FsMefFilterNo,
                             INT4 i4TestValFsMefFilterIfIndex)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;
    tMefUniInfo        *pMefUniInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);
    if (pMefFilterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    pMefUniInfo = MefGetUniEntry (i4TestValFsMefFilterIfIndex);
    if (pMefUniInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if (pMefFilterInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefFilterStatus
 Input       :  The Indices
                FsMefFilterNo

                The Object 
                testValFsMefFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefFilterStatus (UINT4 *pu4ErrorCode, INT4 i4FsMefFilterNo,
                            INT4 i4TestValFsMefFilterStatus)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefFilterInfo = MefGetFilterEntry (i4FsMefFilterNo);

    switch (i4TestValFsMefFilterStatus)
    {
        case CREATE_AND_WAIT:
            if (pMefFilterInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (MEF_CLI_FILTER_EXIST);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case DESTROY:
        case NOT_IN_SERVICE:

            if (pMefFilterInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
                return SNMP_FAILURE;
            }
            break;

        default:

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMefFilterTable
 Input       :  The Indices
                FsMefFilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMefFilterTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefClassMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefClassMapTable
 Input       :  The Indices
                FsMefClassMapId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefClassMapTable (UINT4 u4FsMefClassMapId)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;
    tMefClassMapInfo    MefClassMapInfo;

    MEMSET (&MefClassMapInfo, 0, sizeof (tMefClassMapInfo));
    MefClassMapInfo.u4ClassMapClfrId = u4FsMefClassMapId;

    pMefClassMapInfo = (tMefClassMapInfo *) RBTreeGet
        (MEF_CLASS_MAP_TABLE, &MefClassMapInfo);

    if (pMefClassMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefClassMapTable
 Input       :  The Indices
                FsMefClassMapId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefClassMapTable (UINT4 *pu4FsMefClassMapId)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;

    pMefClassMapInfo = (tMefClassMapInfo *) RBTreeGetFirst
        (MEF_CLASS_MAP_TABLE);
    if (pMefClassMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4FsMefClassMapId = pMefClassMapInfo->u4ClassMapClfrId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefClassMapTable
 Input       :  The Indices
                FsMefClassMapId
                nextFsMefClassMapId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefClassMapTable (UINT4 u4FsMefClassMapId,
                                   UINT4 *pu4NextFsMefClassMapId)
{
    tMefClassMapInfo    MefClassMapInfo;
    tMefClassMapInfo   *pMefClassMapInfo = NULL;

    MEMSET (&MefClassMapInfo, 0, sizeof (tMefClassMapInfo));
    MefClassMapInfo.u4ClassMapClfrId = u4FsMefClassMapId;

    pMefClassMapInfo = (tMefClassMapInfo *) RBTreeGetNext
        (MEF_CLASS_MAP_TABLE, &MefClassMapInfo, NULL);
    if (pMefClassMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsMefClassMapId = pMefClassMapInfo->u4ClassMapClfrId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefClassMapName
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                retValFsMefClassMapName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefClassMapName (UINT4 u4FsMefClassMapId,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsMefClassMapName)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);
    if (pMefClassMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEF_CPY_TO_SNMP (pRetValFsMefClassMapName,
                     &pMefClassMapInfo->au1ClassMapName,
                     STRLEN (pMefClassMapInfo->au1ClassMapName));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMefClassMapFilterId
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                retValFsMefClassMapFilterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefClassMapFilterId (UINT4 u4FsMefClassMapId,
                             UINT4 *pu4RetValFsMefClassMapFilterId)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);
    if (pMefClassMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMefClassMapFilterId = pMefClassMapInfo->u4FilterId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefClassMapCLASS
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                retValFsMefClassMapCLASS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefClassMapCLASS (UINT4 u4FsMefClassMapId,
                          UINT4 *pu4RetValFsMefClassMapCLASS)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);
    if (pMefClassMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMefClassMapCLASS = pMefClassMapInfo->u4ClassId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefClassMapStatus
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                retValFsMefClassMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefClassMapStatus (UINT4 u4FsMefClassMapId,
                           INT4 *pi4RetValFsMefClassMapStatus)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);
    if (pMefClassMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefClassMapStatus = pMefClassMapInfo->u1Status;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMefClassMapName
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                setValFsMefClassMapName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefClassMapName (UINT4 u4FsMefClassMapId,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsMefClassMapName)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);
    if (pMefClassMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEF_CPY_FROM_SNMP (&pMefClassMapInfo->au1ClassMapName,
                       pSetValFsMefClassMapName,
                       pSetValFsMefClassMapName->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefClassMapFilterId
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                setValFsMefClassMapFilterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefClassMapFilterId (UINT4 u4FsMefClassMapId,
                             UINT4 u4SetValFsMefClassMapFilterId)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);
    if (pMefClassMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefClassMapInfo->u4FilterId = u4SetValFsMefClassMapFilterId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefClassMapCLASS
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                setValFsMefClassMapCLASS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefClassMapCLASS (UINT4 u4FsMefClassMapId,
                          UINT4 u4SetValFsMefClassMapCLASS)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;
    tMefClassInfo      *pMefClassInfo = NULL;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);
    if (pMefClassMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If the Same value is configured again no need to incrment the
     * RefCount and Set */
    if (pMefClassMapInfo->u4ClassId == u4SetValFsMefClassMapCLASS)
    {
        return SNMP_SUCCESS;
    }

    pMefClassMapInfo->u4ClassId = u4SetValFsMefClassMapCLASS;

    if (u4SetValFsMefClassMapCLASS != 0)
    {
        pMefClassInfo = MefGetClassEntry (u4SetValFsMefClassMapCLASS);
        if (pMefClassInfo == NULL)
        {
            return SNMP_FAILURE;
        }

        pMefClassInfo->u4RefCount = (pMefClassInfo->u4RefCount) + 1;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefClassMapStatus
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                setValFsMefClassMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefClassMapStatus (UINT4 u4FsMefClassMapId,
                           INT4 i4SetValFsMefClassMapStatus)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);

    if ((i4SetValFsMefClassMapStatus == CREATE_AND_WAIT) &&
        (pMefClassMapInfo != NULL))
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsMefClassMapStatus)
    {
        case CREATE_AND_WAIT:

            pMefClassMapInfo = (tMefClassMapInfo *) MemAllocMemBlk
                (MEF_CLASS_MAP_POOL_ID);
            if (pMefClassMapInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            MefInitDefaultClassMapInfo (u4FsMefClassMapId, pMefClassMapInfo);

            if (MefAddClassMapEntry (pMefClassMapInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            pMefClassMapInfo->u1Status = NOT_READY;

            break;

        case ACTIVE:

            if (MefIsMsrInProgress () != OSIX_TRUE)
            {
                if (MefSetClassMapEntries (u4FsMefClassMapId) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            pMefClassMapInfo->u1Status = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            pMefClassMapInfo->u1Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            if (MefDeleteClassMapEntry (pMefClassMapInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMefClassMapName
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                testValFsMefClassMapName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefClassMapName (UINT4 *pu4ErrorCode, UINT4 u4FsMefClassMapId,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsMefClassMapName)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);
    if (pMefClassMapInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_CLASSMAP_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValFsMefClassMapName->i4_Length > MEF_MAX_NAME_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefClassMapFilterId
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                testValFsMefClassMapFilterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefClassMapFilterId (UINT4 *pu4ErrorCode, UINT4 u4FsMefClassMapId,
                                UINT4 u4TestValFsMefClassMapFilterId)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;
    tMefFilterInfo     *pMefFilterInfo = NULL;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);
    if (pMefClassMapInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_CLASSMAP_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pMefFilterInfo = MefGetFilterEntry (u4TestValFsMefClassMapFilterId);
    if (pMefFilterInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefClassMapCLASS
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                testValFsMefClassMapCLASS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefClassMapCLASS (UINT4 *pu4ErrorCode, UINT4 u4FsMefClassMapId,
                             UINT4 u4TestValFsMefClassMapCLASS)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;
    tMefClassInfo      *pMefClassInfo = NULL;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);
    if (pMefClassMapInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_CLASSMAP_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pMefClassInfo = MefGetClassEntry (u4TestValFsMefClassMapCLASS);
    if (pMefClassInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_CLASS_NOT_EXIST);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefClassMapStatus
 Input       :  The Indices
                FsMefClassMapId

                The Object 
                testValFsMefClassMapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefClassMapStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMefClassMapId,
                              INT4 i4TestValFsMefClassMapStatus)
{
    tMefClassMapInfo   *pMefClassMapInfo = NULL;
    tMefClassMapInfo    MefClassMapInfo;
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;
    tMefPolicyMapInfo   MefPolicyMapInfo;
    UINT4               u4ClassMapClassCount = 0;
    UINT4               u4ClassId = 0;
    BOOLEAN             bClassFlag = OSIX_FALSE;

    pMefClassMapInfo = MefGetClassMapEntry (u4FsMefClassMapId);

    switch (i4TestValFsMefClassMapStatus)
    {
        case CREATE_AND_WAIT:
            if (pMefClassMapInfo != NULL)
            {
                CLI_SET_ERR (MEF_CLI_CLASSMAP_EXIST);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:
            if (pMefClassMapInfo == NULL)
            {
                CLI_SET_ERR (MEF_CLI_CLASSMAP_NOT_EXIST);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            u4ClassId = pMefClassMapInfo->u4ClassId;

            MEMSET (&MefClassMapInfo, 0, sizeof (tMefClassMapInfo));
            MEMSET (&MefPolicyMapInfo, 0, sizeof (tMefPolicyMapInfo));

            /*If the Class having Entry In the Policy Map Table and  No of 
             * ClassMap Entry is 1 then return Failure otherwise Return Success */
            MefClassMapInfo.u4ClassMapClfrId = 0;
            pMefClassMapInfo = (tMefClassMapInfo *)
                RBTreeGetNext (MEF_CLASS_MAP_TABLE, &MefClassMapInfo, NULL);

            while (pMefClassMapInfo != NULL)
            {
                if (u4ClassId == pMefClassMapInfo->u4ClassId)
                {
                    u4ClassMapClassCount++;
                }

                MefClassMapInfo.u4ClassMapClfrId =
                    pMefClassMapInfo->u4ClassMapClfrId;

                pMefClassMapInfo = (tMefClassMapInfo *)
                    RBTreeGetNext (MEF_CLASS_MAP_TABLE, &MefClassMapInfo, NULL);
            }

            MefPolicyMapInfo.u4PolicyMapId = 0;
            pMefPolicyMapInfo = (tMefPolicyMapInfo *)
                RBTreeGetNext (MEF_POLICY_MAP_TABLE, &MefPolicyMapInfo, NULL);
            while (pMefPolicyMapInfo != NULL)
            {
                if (u4ClassId == pMefPolicyMapInfo->u4ClassId)
                {
                    bClassFlag = OSIX_TRUE;
                    break;
                }

                MefPolicyMapInfo.u4PolicyMapId =
                    pMefPolicyMapInfo->u4PolicyMapId;
                pMefPolicyMapInfo =
                    (tMefPolicyMapInfo *) RBTreeGetNext (MEF_POLICY_MAP_TABLE,
                                                         &MefPolicyMapInfo,
                                                         NULL);
            }
            if ((bClassFlag == OSIX_TRUE) && (u4ClassMapClassCount < 2))
            {
                CLI_SET_ERR (MEF_CLI_CLS_MAP_RFE_ERR);
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMefClassMapTable
 Input       :  The Indices
                FsMefClassMapId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMefClassMapTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefClassTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefClassTable
 Input       :  The Indices
                FsMefClassCLASS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefClassTable (UINT4 u4FsMefClassCLASS)
{
    tMefClassInfo      *pMefClassInfo = NULL;
    tMefClassInfo       MefClassInfo;

    MEMSET (&MefClassInfo, 0, sizeof (tMefClassInfo));
    MefClassInfo.u4ClassId = u4FsMefClassCLASS;

    pMefClassInfo = (tMefClassInfo *) RBTreeGet
        (MEF_CLASS_TABLE, &MefClassInfo);

    if (pMefClassInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefClassTable
 Input       :  The Indices
                FsMefClassCLASS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefClassTable (UINT4 *pu4FsMefClassCLASS)
{
    tMefClassInfo      *pMefClassInfo = NULL;

    pMefClassInfo = (tMefClassInfo *) RBTreeGetFirst (MEF_CLASS_TABLE);
    if (pMefClassInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4FsMefClassCLASS = pMefClassInfo->u4ClassId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefClassTable
 Input       :  The Indices
                FsMefClassCLASS
                nextFsMefClassCLASS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefClassTable (UINT4 u4FsMefClassCLASS,
                                UINT4 *pu4NextFsMefClassCLASS)
{
    tMefClassInfo       MefClassInfo;
    tMefClassInfo      *pMefClassInfo = NULL;

    MEMSET (&MefClassInfo, 0, sizeof (tMefClassInfo));
    MefClassInfo.u4ClassId = u4FsMefClassCLASS;

    pMefClassInfo = (tMefClassInfo *) RBTreeGetNext
        (MEF_CLASS_TABLE, &MefClassInfo, NULL);

    if (pMefClassInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsMefClassCLASS = pMefClassInfo->u4ClassId;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefClassStatus
 Input       :  The Indices
                FsMefClassCLASS

                The Object 
                retValFsMefClassStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefClassStatus (UINT4 u4FsMefClassCLASS,
                        INT4 *pi4RetValFsMefClassStatus)
{
    tMefClassInfo      *pMefClassInfo = NULL;

    pMefClassInfo = MefGetClassEntry (u4FsMefClassCLASS);
    if (pMefClassInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefClassStatus = pMefClassInfo->u1Status;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMefClassStatus
 Input       :  The Indices
                FsMefClassCLASS

                The Object 
                setValFsMefClassStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefClassStatus (UINT4 u4FsMefClassCLASS, INT4 i4SetValFsMefClassStatus)
{
    tMefClassInfo      *pMefClassInfo = NULL;

    pMefClassInfo = MefGetClassEntry (u4FsMefClassCLASS);

    if ((i4SetValFsMefClassStatus == CREATE_AND_WAIT) &&
        (pMefClassInfo != NULL))
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsMefClassStatus)
    {
        case CREATE_AND_WAIT:

            pMefClassInfo =
                (tMefClassInfo *) MemAllocMemBlk (MEF_CLASS_POOL_ID);
            if (pMefClassInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            MefInitDefaultClassInfo (u4FsMefClassCLASS, pMefClassInfo);

            if (MefAddClassEntry (pMefClassInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            pMefClassInfo->u1Status = NOT_READY;

            break;

        case ACTIVE:

            pMefClassInfo->u1Status = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            pMefClassInfo->u1Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            if (MefDeleteClassEntry (pMefClassInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMefClassStatus
 Input       :  The Indices
                FsMefClassCLASS

                The Object 
                testValFsMefClassStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefClassStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMefClassCLASS,
                           INT4 i4TestValFsMefClassStatus)
{
    tMefClassInfo      *pMefClassInfo = NULL;

    pMefClassInfo = MefGetClassEntry (u4FsMefClassCLASS);

    switch (i4TestValFsMefClassStatus)
    {
        case CREATE_AND_WAIT:
            if (pMefClassInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (MEF_CLI_CLASS_EXIST);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:
            if (pMefClassInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (MEF_CLI_CLASS_NOT_EXIST);
                return SNMP_FAILURE;
            }

            if (pMefClassInfo->u4RefCount != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (MEF_CLI_CLASS_RFE_ERR);
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMefClassTable
 Input       :  The Indices
                FsMefClassCLASS
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMefClassTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefMeterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefMeterTable
 Input       :  The Indices
                FsMefMeterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefMeterTable (UINT4 u4FsMefMeterId)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;
    tMefMeterInfo       MefMeterInfo;

    MEMSET (&MefMeterInfo, 0, sizeof (tMefMeterInfo));
    MefMeterInfo.u4MeterId = u4FsMefMeterId;

    pMefMeterInfo = (tMefMeterInfo *) RBTreeGet
        (MEF_METER_TABLE, &MefMeterInfo);

    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefMeterTable
 Input       :  The Indices
                FsMefMeterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefMeterTable (UINT4 *pu4FsMefMeterId)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = (tMefMeterInfo *) RBTreeGetFirst (MEF_METER_TABLE);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4FsMefMeterId = pMefMeterInfo->u4MeterId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefMeterTable
 Input       :  The Indices
                FsMefMeterId
                nextFsMefMeterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefMeterTable (UINT4 u4FsMefMeterId,
                                UINT4 *pu4NextFsMefMeterId)
{
    tMefMeterInfo       MefMeterInfo;
    tMefMeterInfo      *pMefMeterInfo = NULL;

    MEMSET (&MefMeterInfo, 0, sizeof (tMefMeterInfo));
    MefMeterInfo.u4MeterId = u4FsMefMeterId;

    pMefMeterInfo = (tMefMeterInfo *) RBTreeGetNext
        (MEF_METER_TABLE, &MefMeterInfo, NULL);

    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsMefMeterId = pMefMeterInfo->u4MeterId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefMeterName
 Input       :  The Indices
                FsMefMeterId

                The Object 
                retValFsMefMeterName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMeterName (UINT4 u4FsMefMeterId,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsMefMeterName)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEF_CPY_TO_SNMP (pRetValFsMefMeterName,
                     &pMefMeterInfo->au1MeterName,
                     STRLEN (pMefMeterInfo->au1MeterName));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMeterType
 Input       :  The Indices
                FsMefMeterId

                The Object 
                retValFsMefMeterType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMeterType (UINT4 u4FsMefMeterId, INT4 *pi4RetValFsMefMeterType)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefMeterType = pMefMeterInfo->u1MeterType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMeterColorMode
 Input       :  The Indices
                FsMefMeterId

                The Object 
                retValFsMefMeterColorMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMeterColorMode (UINT4 u4FsMefMeterId,
                           INT4 *pi4RetValFsMefMeterColorMode)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefMeterColorMode = pMefMeterInfo->u1ColorMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMeterCIR
 Input       :  The Indices
                FsMefMeterId

                The Object 
                retValFsMefMeterCIR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMeterCIR (UINT4 u4FsMefMeterId, UINT4 *pu4RetValFsMefMeterCIR)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMefMeterCIR = pMefMeterInfo->u4CIR;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMeterCBS
 Input       :  The Indices
                FsMefMeterId

                The Object 
                retValFsMefMeterCBS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMeterCBS (UINT4 u4FsMefMeterId, UINT4 *pu4RetValFsMefMeterCBS)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMefMeterCBS = pMefMeterInfo->u4CBS;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMeterEIR
 Input       :  The Indices
                FsMefMeterId

                The Object 
                retValFsMefMeterEIR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMeterEIR (UINT4 u4FsMefMeterId, UINT4 *pu4RetValFsMefMeterEIR)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMefMeterEIR = pMefMeterInfo->u4EIR;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMeterEBS
 Input       :  The Indices
                FsMefMeterId

                The Object 
                retValFsMefMeterEBS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMeterEBS (UINT4 u4FsMefMeterId, UINT4 *pu4RetValFsMefMeterEBS)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMefMeterEBS = pMefMeterInfo->u4EBS;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMeterStatus
 Input       :  The Indices
                FsMefMeterId

                The Object 
                retValFsMefMeterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMeterStatus (UINT4 u4FsMefMeterId, INT4 *pi4RetValFsMefMeterStatus)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefMeterStatus = pMefMeterInfo->u1Status;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMefMeterName
 Input       :  The Indices
                FsMefMeterId

                The Object 
                setValFsMefMeterName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMeterName (UINT4 u4FsMefMeterId,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsMefMeterName)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEF_CPY_FROM_SNMP (&pMefMeterInfo->au1MeterName,
                       pSetValFsMefMeterName, MEF_TABLE_NAME_MAX_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMeterType
 Input       :  The Indices
                FsMefMeterId

                The Object 
                setValFsMefMeterType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMeterType (UINT4 u4FsMefMeterId, INT4 i4SetValFsMefMeterType)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefMeterInfo->u1MeterType = i4SetValFsMefMeterType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMeterColorMode
 Input       :  The Indices
                FsMefMeterId

                The Object 
                setValFsMefMeterColorMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMeterColorMode (UINT4 u4FsMefMeterId,
                           INT4 i4SetValFsMefMeterColorMode)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefMeterInfo->u1ColorMode = i4SetValFsMefMeterColorMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMeterCIR
 Input       :  The Indices
                FsMefMeterId

                The Object 
                setValFsMefMeterCIR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMeterCIR (UINT4 u4FsMefMeterId, UINT4 u4SetValFsMefMeterCIR)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefMeterInfo->u4CIR = u4SetValFsMefMeterCIR;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMeterCBS
 Input       :  The Indices
                FsMefMeterId

                The Object 
                setValFsMefMeterCBS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMeterCBS (UINT4 u4FsMefMeterId, UINT4 u4SetValFsMefMeterCBS)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefMeterInfo->u4CBS = u4SetValFsMefMeterCBS;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMeterEIR
 Input       :  The Indices
                FsMefMeterId

                The Object 
                setValFsMefMeterEIR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMeterEIR (UINT4 u4FsMefMeterId, UINT4 u4SetValFsMefMeterEIR)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefMeterInfo->u4EIR = u4SetValFsMefMeterEIR;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMeterEBS
 Input       :  The Indices
                FsMefMeterId

                The Object 
                setValFsMefMeterEBS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMeterEBS (UINT4 u4FsMefMeterId, UINT4 u4SetValFsMefMeterEBS)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefMeterInfo->u4EBS = u4SetValFsMefMeterEBS;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMeterStatus
 Input       :  The Indices
                FsMefMeterId

                The Object 
                setValFsMefMeterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMeterStatus (UINT4 u4FsMefMeterId, INT4 i4SetValFsMefMeterStatus)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);

    if ((i4SetValFsMefMeterStatus == CREATE_AND_WAIT)
        && (pMefMeterInfo != NULL))
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsMefMeterStatus)
    {
        case CREATE_AND_WAIT:

            pMefMeterInfo =
                (tMefMeterInfo *) MemAllocMemBlk (MEF_METER_POOL_ID);
            if (pMefMeterInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            MefInitDefaultMeterInfo (u4FsMefMeterId, pMefMeterInfo);

            if (MefAddMeterEntry (pMefMeterInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            pMefMeterInfo->u1Status = NOT_READY;

            break;

        case ACTIVE:

            if (MefIsMsrInProgress () != OSIX_TRUE)
            {
                if (MefSetMeterEntries (u4FsMefMeterId) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            pMefMeterInfo->u1Status = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            pMefMeterInfo->u1Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            if (MefDeleteMeterEntry (pMefMeterInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMefMeterName
 Input       :  The Indices
                FsMefMeterId

                The Object 
                testValFsMefMeterName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMeterName (UINT4 *pu4ErrorCode, UINT4 u4FsMefMeterId,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsMefMeterName)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValFsMefMeterName->i4_Length > MEF_MAX_NAME_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMeterType
 Input       :  The Indices
                FsMefMeterId

                The Object 
                testValFsMefMeterType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMeterType (UINT4 *pu4ErrorCode, UINT4 u4FsMefMeterId,
                         INT4 i4TestValFsMefMeterType)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMefMeterType != QOS_METER_TYPE_MEF_DECOUPLED) &&
        (i4TestValFsMefMeterType != QOS_METER_TYPE_MEF_COUPLED))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pMefMeterInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMeterColorMode
 Input       :  The Indices
                FsMefMeterId

                The Object 
                testValFsMefMeterColorMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMeterColorMode (UINT4 *pu4ErrorCode, UINT4 u4FsMefMeterId,
                              INT4 i4TestValFsMefMeterColorMode)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMefMeterColorMode != QOS_METER_COLOR_AWARE) &&
        (i4TestValFsMefMeterColorMode != QOS_METER_COLOR_BLIND))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pMefMeterInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMeterCIR
 Input       :  The Indices
                FsMefMeterId

                The Object 
                testValFsMefMeterCIR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMeterCIR (UINT4 *pu4ErrorCode, UINT4 u4FsMefMeterId,
                        UINT4 u4TestValFsMefMeterCIR)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pMefMeterInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValFsMefMeterCIR > MEF_METER_CIR_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMeterCBS
 Input       :  The Indices
                FsMefMeterId

                The Object 
                testValFsMefMeterCBS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMeterCBS (UINT4 *pu4ErrorCode, UINT4 u4FsMefMeterId,
                        UINT4 u4TestValFsMefMeterCBS)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pMefMeterInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValFsMefMeterCBS > MEF_METER_CBS_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMeterEIR
 Input       :  The Indices
                FsMefMeterId

                The Object 
                testValFsMefMeterEIR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMeterEIR (UINT4 *pu4ErrorCode, UINT4 u4FsMefMeterId,
                        UINT4 u4TestValFsMefMeterEIR)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    UNUSED_PARAM (u4TestValFsMefMeterEIR);

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u4TestValFsMefMeterEIR > MEF_METER_EIR_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pMefMeterInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMeterEBS
 Input       :  The Indices
                FsMefMeterId

                The Object 
                testValFsMefMeterEBS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMeterEBS (UINT4 *pu4ErrorCode, UINT4 u4FsMefMeterId,
                        UINT4 u4TestValFsMefMeterEBS)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    UNUSED_PARAM (u4TestValFsMefMeterEBS);

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);
    if (pMefMeterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u4TestValFsMefMeterEBS > MEF_METER_EBS_MAX_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pMefMeterInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMeterStatus
 Input       :  The Indices
                FsMefMeterId

                The Object 
                testValFsMefMeterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMeterStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMefMeterId,
                           INT4 i4TestValFsMefMeterStatus)
{
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefMeterInfo = MefGetMeterEntry (u4FsMefMeterId);

    switch (i4TestValFsMefMeterStatus)
    {
        case CREATE_AND_WAIT:
            if (pMefMeterInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (MEF_CLI_METER_EXIST);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:
            if (pMefMeterInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (MEF_CLI_METER_NOT_EXIST);
                return SNMP_FAILURE;
            }

            if (pMefMeterInfo->u4RefCount != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (MEF_CLI_METER_RFE_ERR);
                return SNMP_FAILURE;
            }

            if (i4TestValFsMefMeterStatus == ACTIVE)
            {
                if (ISS_HW_SUPPORTED != IssGetHwCapabilities (ISS_HW_METER_COLOR_BLIND))
                {
                    if (pMefMeterInfo->u1ColorMode == QOS_METER_COLOR_BLIND)
                    {
                        if (MefDeleteMeterEntry (pMefMeterInfo) == OSIX_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        CLI_SET_ERR (MEF_CLI_METER_COLOR_BLIND_MODE_NOT_SUPPORTED);
                        return SNMP_FAILURE;
                    }
                }
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMefMeterTable
 Input       :  The Indices
                FsMefMeterId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMefMeterTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefPolicyMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefPolicyMapTable
 Input       :  The Indices
                FsMefPolicyMapId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefPolicyMapTable (UINT4 u4FsMefPolicyMapId)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;
    tMefPolicyMapInfo   MefPolicyMapInfo;

    MEMSET (&MefPolicyMapInfo, 0, sizeof (tMefPolicyMapInfo));
    MefPolicyMapInfo.u4PolicyMapId = u4FsMefPolicyMapId;

    pMefPolicyMapInfo = (tMefPolicyMapInfo *) RBTreeGet
        (MEF_POLICY_MAP_TABLE, &MefPolicyMapInfo);

    if (pMefPolicyMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefPolicyMapTable
 Input       :  The Indices
                FsMefPolicyMapId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefPolicyMapTable (UINT4 *pu4FsMefPolicyMapId)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;

    pMefPolicyMapInfo = (tMefPolicyMapInfo *) RBTreeGetFirst
        (MEF_POLICY_MAP_TABLE);
    if (pMefPolicyMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4FsMefPolicyMapId = pMefPolicyMapInfo->u4PolicyMapId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefPolicyMapTable
 Input       :  The Indices
                FsMefPolicyMapId
                nextFsMefPolicyMapId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefPolicyMapTable (UINT4 u4FsMefPolicyMapId,
                                    UINT4 *pu4NextFsMefPolicyMapId)
{
    tMefPolicyMapInfo   MefPolicyMapInfo;
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;

    MEMSET (&MefPolicyMapInfo, 0, sizeof (tMefPolicyMapInfo));
    MefPolicyMapInfo.u4PolicyMapId = u4FsMefPolicyMapId;

    pMefPolicyMapInfo = (tMefPolicyMapInfo *) RBTreeGetNext
        (MEF_POLICY_MAP_TABLE, &MefPolicyMapInfo, NULL);

    if (pMefPolicyMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsMefPolicyMapId = pMefPolicyMapInfo->u4PolicyMapId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefPolicyMapName
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                retValFsMefPolicyMapName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefPolicyMapName (UINT4 u4FsMefPolicyMapId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsMefPolicyMapName)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);
    if (pMefPolicyMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEF_CPY_TO_SNMP (pRetValFsMefPolicyMapName,
                     &pMefPolicyMapInfo->au1PolicyMapName,
                     STRLEN (pMefPolicyMapInfo->au1PolicyMapName));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefPolicyMapCLASS
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                retValFsMefPolicyMapCLASS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefPolicyMapCLASS (UINT4 u4FsMefPolicyMapId,
                           UINT4 *pu4RetValFsMefPolicyMapCLASS)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);
    if (pMefPolicyMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMefPolicyMapCLASS = pMefPolicyMapInfo->u4ClassId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefPolicyMapMeterTableId
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                retValFsMefPolicyMapMeterTableId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefPolicyMapMeterTableId (UINT4 u4FsMefPolicyMapId,
                                  UINT4 *pu4RetValFsMefPolicyMapMeterTableId)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);
    if (pMefPolicyMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMefPolicyMapMeterTableId = pMefPolicyMapInfo->u4MeterId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefPolicyMapStatus
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                retValFsMefPolicyMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefPolicyMapStatus (UINT4 u4FsMefPolicyMapId,
                            INT4 *pi4RetValFsMefPolicyMapStatus)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);
    if (pMefPolicyMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefPolicyMapStatus = pMefPolicyMapInfo->u1Status;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMefPolicyMapName
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                setValFsMefPolicyMapName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefPolicyMapName (UINT4 u4FsMefPolicyMapId,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsMefPolicyMapName)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);
    if (pMefPolicyMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEF_CPY_FROM_SNMP (&pMefPolicyMapInfo->au1PolicyMapName,
                       pSetValFsMefPolicyMapName,
                       pSetValFsMefPolicyMapName->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefPolicyMapCLASS
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                setValFsMefPolicyMapCLASS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefPolicyMapCLASS (UINT4 u4FsMefPolicyMapId,
                           UINT4 u4SetValFsMefPolicyMapCLASS)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;
    tMefClassInfo      *pMefClassInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);
    if (pMefPolicyMapInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_POLICYMAP_NOT_EXIST);
        return SNMP_FAILURE;
    }

    /* If the Same value is configured again no need to incrment the
     * RefCount and Set */
    if (pMefPolicyMapInfo->u4ClassId == u4SetValFsMefPolicyMapCLASS)
    {
        return SNMP_SUCCESS;
    }

    pMefPolicyMapInfo->u4ClassId = u4SetValFsMefPolicyMapCLASS;

    if (u4SetValFsMefPolicyMapCLASS != 0)
    {
        pMefClassInfo = MefGetClassEntry (u4SetValFsMefPolicyMapCLASS);
        if (pMefClassInfo == NULL)
        {
            return SNMP_FAILURE;
        }
        /* Incremnet the RefCount of Meter Id  Entry in the Meter Table */
        pMefClassInfo->u4RefCount = (pMefClassInfo->u4RefCount) + 1;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefPolicyMapMeterTableId
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                setValFsMefPolicyMapMeterTableId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefPolicyMapMeterTableId (UINT4 u4FsMefPolicyMapId,
                                  UINT4 u4SetValFsMefPolicyMapMeterTableId)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);
    if (pMefPolicyMapInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_POLICYMAP_NOT_EXIST);
        return SNMP_FAILURE;
    }

    /* If the Same value is configured again no need to incrment the
     * RefCount and Set */
    if (pMefPolicyMapInfo->u4MeterId == u4SetValFsMefPolicyMapMeterTableId)
    {
        return (SNMP_SUCCESS);
    }

    pMefPolicyMapInfo->u4MeterId = u4SetValFsMefPolicyMapMeterTableId;

    if (u4SetValFsMefPolicyMapMeterTableId != 0)
    {
        pMefMeterInfo = MefGetMeterEntry (u4SetValFsMefPolicyMapMeterTableId);
        if (pMefMeterInfo == NULL)
        {
            return SNMP_FAILURE;
        }
        /* Incremnet the RefCount of Meter Id  Entry in the Meter Table */
        pMefMeterInfo->u4RefCount = (pMefMeterInfo->u4RefCount) + 1;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefPolicyMapStatus
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                setValFsMefPolicyMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefPolicyMapStatus (UINT4 u4FsMefPolicyMapId,
                            INT4 i4SetValFsMefPolicyMapStatus)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);

    if ((i4SetValFsMefPolicyMapStatus == CREATE_AND_WAIT)
        && (pMefPolicyMapInfo != NULL))
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsMefPolicyMapStatus)
    {
        case CREATE_AND_WAIT:

            pMefPolicyMapInfo =
                (tMefPolicyMapInfo *) MemAllocMemBlk (MEF_POLICY_MAP_POOL_ID);
            if (pMefPolicyMapInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            MefInitDefaultPolicyMapInfo (u4FsMefPolicyMapId, pMefPolicyMapInfo);

            if (MefAddPolicyMapEntry (pMefPolicyMapInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            pMefPolicyMapInfo->u1Status = NOT_READY;

            break;

        case ACTIVE:

            if (MefIsMsrInProgress () != OSIX_TRUE)
            {
                if (MefSetPolicyMapEntries (u4FsMefPolicyMapId) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            pMefPolicyMapInfo->u1Status = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            pMefPolicyMapInfo->u1Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            if (MefDeletePolicyMapEntry (pMefPolicyMapInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMefPolicyMapName
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                testValFsMefPolicyMapName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefPolicyMapName (UINT4 *pu4ErrorCode, UINT4 u4FsMefPolicyMapId,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsMefPolicyMapName)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);
    if (pMefPolicyMapInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pTestValFsMefPolicyMapName->i4_Length > MEF_MAX_NAME_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pMefPolicyMapInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefPolicyMapCLASS
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                testValFsMefPolicyMapCLASS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefPolicyMapCLASS (UINT4 *pu4ErrorCode, UINT4 u4FsMefPolicyMapId,
                              UINT4 u4TestValFsMefPolicyMapCLASS)
{
    tMefPolicyMapInfo  *PolicyMapInfo = NULL;
    tMefPolicyMapInfo   MefPolicyMapInfo;
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;
    tMefClassInfo      *pMefClassInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);
    if (pMefPolicyMapInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (MEF_CLI_POLICYMAP_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if (pMefPolicyMapInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pMefClassInfo = MefGetClassEntry (u4TestValFsMefPolicyMapCLASS);
    if (pMefClassInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_CLASS_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if ((pMefClassInfo->u4RefCount) < 1)
    {
        CLI_SET_ERR (MEF_CLI_ERR_NO_FILTER);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    MEMSET (&MefPolicyMapInfo, 0, sizeof (tMefPolicyMapInfo));
    MefPolicyMapInfo.u4PolicyMapId = 0;
    do
    {
        PolicyMapInfo = (tMefPolicyMapInfo *) RBTreeGetNext
            (MEF_POLICY_MAP_TABLE, &MefPolicyMapInfo, NULL);
        if (PolicyMapInfo != NULL)
        {
            if (PolicyMapInfo->u4ClassId == u4TestValFsMefPolicyMapCLASS)

            {
                CLI_SET_ERR (MEF_CLI_ERR_PLY_MULTIMAP);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            MEMSET (&MefPolicyMapInfo, 0, sizeof (tMefPolicyMapInfo));
            MefPolicyMapInfo.u4PolicyMapId = PolicyMapInfo->u4PolicyMapId;
        }
    }
    while (PolicyMapInfo != NULL);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefPolicyMapMeterTableId
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                testValFsMefPolicyMapMeterTableId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefPolicyMapMeterTableId (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMefPolicyMapId,
                                     UINT4 u4TestValFsMefPolicyMapMeterTableId)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;
    tMefMeterInfo      *pMefMeterInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);
    if (pMefPolicyMapInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_POLICYMAP_NOT_EXIST);
        return SNMP_FAILURE;
    }

    if (pMefPolicyMapInfo->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pMefMeterInfo = MefGetMeterEntry (u4TestValFsMefPolicyMapMeterTableId);
    if (pMefMeterInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MEF_CLI_METER_NOT_EXIST);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefPolicyMapStatus
 Input       :  The Indices
                FsMefPolicyMapId

                The Object 
                testValFsMefPolicyMapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefPolicyMapStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMefPolicyMapId,
                               INT4 i4TestValFsMefPolicyMapStatus)
{
    tMefPolicyMapInfo  *pMefPolicyMapInfo = NULL;

    pMefPolicyMapInfo = MefGetPolicyMapEntry (u4FsMefPolicyMapId);

    switch (i4TestValFsMefPolicyMapStatus)
    {
        case CREATE_AND_WAIT:
            if (pMefPolicyMapInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (MEF_CLI_POLICYMAP_EXIST);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:
            if (pMefPolicyMapInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (MEF_CLI_POLICYMAP_NOT_EXIST);
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMefPolicyMapTable
 Input       :  The Indices
                FsMefPolicyMapId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMefPolicyMapTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefMepTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefMepTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefMepTable (UINT4 u4FsMefContextId,
                                       UINT4 u4FsMefMegIndex,
                                       UINT4 u4FsMefMeIndex,
                                       UINT4 u4FsMefMepIdentifier)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepTable (u4FsMefContextId,
                                                   u4FsMefMegIndex,
                                                   u4FsMefMeIndex,
                                                   u4FsMefMepIdentifier)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefMepTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefMepTable (UINT4 *pu4FsMefContextId,
                               UINT4 *pu4FsMefMegIndex, UINT4 *pu4FsMefMeIndex,
                               UINT4 *pu4FsMefMepIdentifier)
{
    ECFM_CC_LOCK ();
    if (nmhGetFirstIndexFsMIY1731MepTable (pu4FsMefContextId,
                                           pu4FsMefMegIndex, pu4FsMefMeIndex,
                                           pu4FsMefMepIdentifier)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefMepTable
 Input       :  The Indices
                FsMefContextId
                nextFsMefContextId
                FsMefMegIndex
                nextFsMefMegIndex
                FsMefMeIndex
                nextFsMefMeIndex
                FsMefMepIdentifier
                nextFsMefMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefMepTable (UINT4 u4FsMefContextId,
                              UINT4 *pu4NextFsMefContextId,
                              UINT4 u4FsMefMegIndex,
                              UINT4 *pu4NextFsMefMegIndex, UINT4 u4FsMefMeIndex,
                              UINT4 *pu4NextFsMefMeIndex,
                              UINT4 u4FsMefMepIdentifier,
                              UINT4 *pu4NextFsMefMepIdentifier)
{
    ECFM_CC_LOCK ();
    if (nmhGetNextIndexFsMIY1731MepTable (u4FsMefContextId,
                                          pu4NextFsMefContextId,
                                          u4FsMefMegIndex,
                                          pu4NextFsMefMegIndex,
                                          u4FsMefMeIndex,
                                          pu4NextFsMefMeIndex,
                                          u4FsMefMepIdentifier,
                                          pu4NextFsMefMepIdentifier)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitLmmStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitLmmStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitLmmStatus (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                 UINT4 u4FsMefMeIndex,
                                 UINT4 u4FsMefMepIdentifier,
                                 INT4 *pi4RetValFsMefMepTransmitLmmStatus)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLmmStatus (u4FsMefContextId,
                                             u4FsMefMegIndex,
                                             u4FsMefMeIndex,
                                             u4FsMefMepIdentifier,
                                             pi4RetValFsMefMepTransmitLmmStatus)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitLmmResultOK
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitLmmResultOK
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitLmmResultOK (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 *pi4RetValFsMefMepTransmitLmmResultOK)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLmmResultOK (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               pi4RetValFsMefMepTransmitLmmResultOK)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitLmmInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitLmmInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitLmmInterval (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 *pi4RetValFsMefMepTransmitLmmInterval)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLmmInterval (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               pi4RetValFsMefMepTransmitLmmInterval)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitLmmDeadline
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitLmmDeadline
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitLmmDeadline (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   UINT4 *pu4RetValFsMefMepTransmitLmmDeadline)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLmmDeadline (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               pu4RetValFsMefMepTransmitLmmDeadline)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitLmmDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitLmmDropEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitLmmDropEnable (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4
                                     *pi4RetValFsMefMepTransmitLmmDropEnable)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLmmDropEnable (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 pi4RetValFsMefMepTransmitLmmDropEnable)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitLmmPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitLmmPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitLmmPriority (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 *pi4RetValFsMefMepTransmitLmmPriority)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLmmPriority (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               pi4RetValFsMefMepTransmitLmmPriority)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitLmmDestMacAddress
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitLmmDestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitLmmDestMacAddress (UINT4 u4FsMefContextId,
                                         UINT4 u4FsMefMegIndex,
                                         UINT4 u4FsMefMeIndex,
                                         UINT4 u4FsMefMepIdentifier,
                                         tMacAddr *
                                         pRetValFsMefMepTransmitLmmDestMacAddress)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLmmDestMacAddress (u4FsMefContextId,
                                                     u4FsMefMegIndex,
                                                     u4FsMefMeIndex,
                                                     u4FsMefMepIdentifier,
                                                     pRetValFsMefMepTransmitLmmDestMacAddress)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitLmmDestMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitLmmDestMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitLmmDestMepId (UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    UINT4
                                    *pu4RetValFsMefMepTransmitLmmDestMepId)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLmmDestMepId (u4FsMefContextId,
                                                u4FsMefMegIndex,
                                                u4FsMefMeIndex,
                                                u4FsMefMepIdentifier,
                                                pu4RetValFsMefMepTransmitLmmDestMepId)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitLmmDestIsMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitLmmDestIsMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitLmmDestIsMepId (UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      INT4
                                      *pi4RetValFsMefMepTransmitLmmDestIsMepId)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLmmDestIsMepId (u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  pi4RetValFsMefMepTransmitLmmDestIsMepId)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitLmmMessages
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitLmmMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitLmmMessages (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 *pi4RetValFsMefMepTransmitLmmMessages)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTransmitLmmMessages (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               pi4RetValFsMefMepTransmitLmmMessages)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTxFCf
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTxFCf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTxFCf (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                     UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                     UINT4 *pu4RetValFsMefMepTxFCf)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTxFCf (u4FsMefContextId,
                                 u4FsMefMegIndex,
                                 u4FsMefMeIndex,
                                 u4FsMefMepIdentifier,
                                 pu4RetValFsMefMepTxFCf) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepRxFCb
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepRxFCb
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepRxFCb (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                     UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                     UINT4 *pu4RetValFsMefMepRxFCb)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepRxFCb (u4FsMefContextId,
                                 u4FsMefMegIndex,
                                 u4FsMefMeIndex,
                                 u4FsMefMepIdentifier,
                                 pu4RetValFsMefMepRxFCb) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTxFCb
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTxFCb
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTxFCb (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                     UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                     UINT4 *pu4RetValFsMefMepTxFCb)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepTxFCb (u4FsMefContextId,
                                 u4FsMefMegIndex,
                                 u4FsMefMeIndex,
                                 u4FsMefMepIdentifier,
                                 pu4RetValFsMefMepTxFCb) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepNearEndFrameLossThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepNearEndFrameLossThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepNearEndFrameLossThreshold (UINT4 u4FsMefContextId,
                                         UINT4 u4FsMefMegIndex,
                                         UINT4 u4FsMefMeIndex,
                                         UINT4 u4FsMefMepIdentifier,
                                         UINT4
                                         *pu4RetValFsMefMepNearEndFrameLossThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepNearEndFrameLossThreshold (u4FsMefContextId,
                                                     u4FsMefMegIndex,
                                                     u4FsMefMeIndex,
                                                     u4FsMefMepIdentifier,
                                                     pu4RetValFsMefMepNearEndFrameLossThreshold)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepFarEndFrameLossThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepFarEndFrameLossThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepFarEndFrameLossThreshold (UINT4 u4FsMefContextId,
                                        UINT4 u4FsMefMegIndex,
                                        UINT4 u4FsMefMeIndex,
                                        UINT4 u4FsMefMepIdentifier,
                                        UINT4
                                        *pu4RetValFsMefMepFarEndFrameLossThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepFarEndFrameLossThreshold (u4FsMefContextId,
                                                    u4FsMefMegIndex,
                                                    u4FsMefMeIndex,
                                                    u4FsMefMepIdentifier,
                                                    pu4RetValFsMefMepFarEndFrameLossThreshold)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitDmStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitDmStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitDmStatus (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                UINT4 u4FsMefMeIndex,
                                UINT4 u4FsMefMepIdentifier,
                                INT4 *pi4RetValFsMefMepTransmitDmStatus)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmStatus (u4FsMefContextId,
                                            u4FsMefMegIndex,
                                            u4FsMefMeIndex,
                                            u4FsMefMepIdentifier,
                                            pi4RetValFsMefMepTransmitDmStatus)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitDmResultOK
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitDmResultOK
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitDmResultOK (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  INT4 *pi4RetValFsMefMepTransmitDmResultOK)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmResultOK (u4FsMefContextId,
                                              u4FsMefMegIndex,
                                              u4FsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              pi4RetValFsMefMepTransmitDmResultOK)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitDmType
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitDmType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitDmType (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                              UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                              INT4 *pi4RetValFsMefMepTransmitDmType)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmType (u4FsMefContextId,
                                          u4FsMefMegIndex,
                                          u4FsMefMeIndex,
                                          u4FsMefMepIdentifier,
                                          pi4RetValFsMefMepTransmitDmType)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitDmInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitDmInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitDmInterval (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  INT4 *pi4RetValFsMefMepTransmitDmInterval)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmInterval (u4FsMefContextId,
                                              u4FsMefMegIndex,
                                              u4FsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              pi4RetValFsMefMepTransmitDmInterval)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitDmMessages
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitDmMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitDmMessages (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  INT4 *pi4RetValFsMefMepTransmitDmMessages)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmMessages (u4FsMefContextId,
                                              u4FsMefMegIndex,
                                              u4FsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              pi4RetValFsMefMepTransmitDmMessages)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitDmmDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitDmmDropEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitDmmDropEnable (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4
                                     *pi4RetValFsMefMepTransmitDmmDropEnable)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmmDropEnable (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 pi4RetValFsMefMepTransmitDmmDropEnable)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmit1DmDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmit1DmDropEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmit1DmDropEnable (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4
                                     *pi4RetValFsMefMepTransmit1DmDropEnable)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmit1DmDropEnable (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 pi4RetValFsMefMepTransmit1DmDropEnable)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitDmmPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitDmmPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitDmmPriority (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 *pi4RetValFsMefMepTransmitDmmPriority)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmmPriority (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               pi4RetValFsMefMepTransmitDmmPriority)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmit1DmPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmit1DmPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmit1DmPriority (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 *pi4RetValFsMefMepTransmit1DmPriority)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmit1DmPriority (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               pi4RetValFsMefMepTransmit1DmPriority)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitDmDestMacAddress
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitDmDestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitDmDestMacAddress (UINT4 u4FsMefContextId,
                                        UINT4 u4FsMefMegIndex,
                                        UINT4 u4FsMefMeIndex,
                                        UINT4 u4FsMefMepIdentifier,
                                        tMacAddr *
                                        pRetValFsMefMepTransmitDmDestMacAddress)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmDestMacAddress (u4FsMefContextId,
                                                    u4FsMefMegIndex,
                                                    u4FsMefMeIndex,
                                                    u4FsMefMepIdentifier,
                                                    pRetValFsMefMepTransmitDmDestMacAddress)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitDmDestMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitDmDestMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitDmDestMepId (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   UINT4 *pu4RetValFsMefMepTransmitDmDestMepId)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmDestMepId (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               pu4RetValFsMefMepTransmitDmDestMepId)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitDmDestIsMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitDmDestIsMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitDmDestIsMepId (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4
                                     *pi4RetValFsMefMepTransmitDmDestIsMepId)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmDestIsMepId (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 pi4RetValFsMefMepTransmitDmDestIsMepId)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepTransmitDmDeadline
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepTransmitDmDeadline
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepTransmitDmDeadline (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  UINT4 *pu4RetValFsMefMepTransmitDmDeadline)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepTransmitDmDeadline (u4FsMefContextId,
                                              u4FsMefMegIndex,
                                              u4FsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              pu4RetValFsMefMepTransmitDmDeadline)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepDmrOptionalFields
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepDmrOptionalFields
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepDmrOptionalFields (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                 UINT4 u4FsMefMeIndex,
                                 UINT4 u4FsMefMepIdentifier,
                                 INT4 *pi4RetValFsMefMepDmrOptionalFields)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepDmrOptionalFields (u4FsMefContextId,
                                             u4FsMefMegIndex,
                                             u4FsMefMeIndex,
                                             u4FsMefMepIdentifier,
                                             pi4RetValFsMefMepDmrOptionalFields)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMep1DmRecvCapability
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMep1DmRecvCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMep1DmRecvCapability (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                 UINT4 u4FsMefMeIndex,
                                 UINT4 u4FsMefMepIdentifier,
                                 INT4 *pi4RetValFsMefMep1DmRecvCapability)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731Mep1DmRecvCapability (u4FsMefContextId,
                                             u4FsMefMegIndex,
                                             u4FsMefMeIndex,
                                             u4FsMefMepIdentifier,
                                             pi4RetValFsMefMep1DmRecvCapability)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepFrameDelayThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepFrameDelayThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepFrameDelayThreshold (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   UINT4 *pu4RetValFsMefMepFrameDelayThreshold)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731MepFrameDelayThreshold (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               pu4RetValFsMefMepFrameDelayThreshold)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepRowStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepRowStatus (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                         UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                         INT4 *pi4RetValFsMefMepRowStatus)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepRowStatus (u4FsMefContextId,
                                     u4FsMefMegIndex,
                                     u4FsMefMeIndex,
                                     u4FsMefMepIdentifier,
                                     pi4RetValFsMefMepRowStatus)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepRxFCf
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepRxFCf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepRxFCf (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                     UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                     UINT4 *pu4RetValFsMefMepRxFCf)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepRxFCf (u4FsMefContextId,
                                 u4FsMefMegIndex,
                                 u4FsMefMeIndex,
                                 u4FsMefMepIdentifier,
                                 pu4RetValFsMefMepRxFCf) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMep1DmTransInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMep1DmTransInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMep1DmTransInterval (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                UINT4 u4FsMefMeIndex,
                                UINT4 u4FsMefMepIdentifier,
                                INT4 *pi4RetValFsMefMep1DmTransInterval)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731Mep1DmTransInterval (u4FsMefContextId,
                                            u4FsMefMegIndex,
                                            u4FsMefMeIndex,
                                            u4FsMefMepIdentifier,
                                            pi4RetValFsMefMep1DmTransInterval)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitLmmStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitLmmStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitLmmStatus (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                 UINT4 u4FsMefMeIndex,
                                 UINT4 u4FsMefMepIdentifier,
                                 INT4 i4SetValFsMefMepTransmitLmmStatus)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmStatus (u4FsMefContextId,
                                             u4FsMefMegIndex,
                                             u4FsMefMeIndex,
                                             u4FsMefMepIdentifier,
                                             i4SetValFsMefMepTransmitLmmStatus)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitLmmInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitLmmInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitLmmInterval (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 i4SetValFsMefMepTransmitLmmInterval)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmInterval
        (u4FsMefContextId,
         u4FsMefMegIndex,
         u4FsMefMeIndex,
         u4FsMefMepIdentifier,
         i4SetValFsMefMepTransmitLmmInterval) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitLmmDeadline
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitLmmDeadline
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitLmmDeadline (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   UINT4 u4SetValFsMefMepTransmitLmmDeadline)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmDeadline (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               u4SetValFsMefMepTransmitLmmDeadline)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitLmmDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitLmmDropEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitLmmDropEnable (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4 i4SetValFsMefMepTransmitLmmDropEnable)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmDropEnable (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 i4SetValFsMefMepTransmitLmmDropEnable)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitLmmPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitLmmPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitLmmPriority (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 i4SetValFsMefMepTransmitLmmPriority)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmPriority (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               i4SetValFsMefMepTransmitLmmPriority)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitLmmDestMacAddress
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitLmmDestMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitLmmDestMacAddress (UINT4 u4FsMefContextId,
                                         UINT4 u4FsMefMegIndex,
                                         UINT4 u4FsMefMeIndex,
                                         UINT4 u4FsMefMepIdentifier,
                                         tMacAddr
                                         SetValFsMefMepTransmitLmmDestMacAddress)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmDestMacAddress (u4FsMefContextId,
                                                     u4FsMefMegIndex,
                                                     u4FsMefMeIndex,
                                                     u4FsMefMepIdentifier,
                                                     SetValFsMefMepTransmitLmmDestMacAddress)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitLmmDestMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitLmmDestMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitLmmDestMepId (UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    UINT4 u4SetValFsMefMepTransmitLmmDestMepId)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmDestMepId (u4FsMefContextId,
                                                u4FsMefMegIndex,
                                                u4FsMefMeIndex,
                                                u4FsMefMepIdentifier,
                                                u4SetValFsMefMepTransmitLmmDestMepId)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitLmmDestIsMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitLmmDestIsMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitLmmDestIsMepId (UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      INT4
                                      i4SetValFsMefMepTransmitLmmDestIsMepId)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmDestIsMepId (u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  i4SetValFsMefMepTransmitLmmDestIsMepId)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitLmmMessages
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitLmmMessages
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitLmmMessages (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 i4SetValFsMefMepTransmitLmmMessages)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepTransmitLmmMessages
        (u4FsMefContextId,
         u4FsMefMegIndex,
         u4FsMefMeIndex,
         u4FsMefMepIdentifier,
         i4SetValFsMefMepTransmitLmmMessages) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepNearEndFrameLossThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepNearEndFrameLossThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepNearEndFrameLossThreshold (UINT4 u4FsMefContextId,
                                         UINT4 u4FsMefMegIndex,
                                         UINT4 u4FsMefMeIndex,
                                         UINT4 u4FsMefMepIdentifier,
                                         UINT4
                                         u4SetValFsMefMepNearEndFrameLossThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepNearEndFrameLossThreshold (u4FsMefContextId,
                                                     u4FsMefMegIndex,
                                                     u4FsMefMeIndex,
                                                     u4FsMefMepIdentifier,
                                                     u4SetValFsMefMepNearEndFrameLossThreshold)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepFarEndFrameLossThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepFarEndFrameLossThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepFarEndFrameLossThreshold (UINT4 u4FsMefContextId,
                                        UINT4 u4FsMefMegIndex,
                                        UINT4 u4FsMefMeIndex,
                                        UINT4 u4FsMefMepIdentifier,
                                        UINT4
                                        u4SetValFsMefMepFarEndFrameLossThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepFarEndFrameLossThreshold (u4FsMefContextId,
                                                    u4FsMefMegIndex,
                                                    u4FsMefMeIndex,
                                                    u4FsMefMepIdentifier,
                                                    u4SetValFsMefMepFarEndFrameLossThreshold)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitDmStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitDmStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitDmStatus (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                UINT4 u4FsMefMeIndex,
                                UINT4 u4FsMefMepIdentifier,
                                INT4 i4SetValFsMefMepTransmitDmStatus)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmStatus (u4FsMefContextId,
                                            u4FsMefMegIndex,
                                            u4FsMefMeIndex,
                                            u4FsMefMepIdentifier,
                                            i4SetValFsMefMepTransmitDmStatus)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitDmType
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitDmType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitDmType (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                              UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                              INT4 i4SetValFsMefMepTransmitDmType)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmType (u4FsMefContextId,
                                          u4FsMefMegIndex,
                                          u4FsMefMeIndex,
                                          u4FsMefMepIdentifier,
                                          i4SetValFsMefMepTransmitDmType)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitDmInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitDmInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitDmInterval (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  INT4 i4SetValFsMefMepTransmitDmInterval)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmInterval (u4FsMefContextId,
                                              u4FsMefMegIndex,
                                              u4FsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              i4SetValFsMefMepTransmitDmInterval)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitDmMessages
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitDmMessages
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitDmMessages (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  INT4 i4SetValFsMefMepTransmitDmMessages)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmMessages (u4FsMefContextId,
                                              u4FsMefMegIndex,
                                              u4FsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              i4SetValFsMefMepTransmitDmMessages)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitDmmDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitDmmDropEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitDmmDropEnable (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4 i4SetValFsMefMepTransmitDmmDropEnable)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmmDropEnable (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 i4SetValFsMefMepTransmitDmmDropEnable)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmit1DmDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmit1DmDropEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmit1DmDropEnable (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4 i4SetValFsMefMepTransmit1DmDropEnable)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmit1DmDropEnable (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 i4SetValFsMefMepTransmit1DmDropEnable)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitDmmPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitDmmPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitDmmPriority (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 i4SetValFsMefMepTransmitDmmPriority)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmmPriority (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               i4SetValFsMefMepTransmitDmmPriority)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmit1DmPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmit1DmPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmit1DmPriority (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 i4SetValFsMefMepTransmit1DmPriority)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmit1DmPriority (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               i4SetValFsMefMepTransmit1DmPriority)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitDmDestMacAddress
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitDmDestMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitDmDestMacAddress (UINT4 u4FsMefContextId,
                                        UINT4 u4FsMefMegIndex,
                                        UINT4 u4FsMefMeIndex,
                                        UINT4 u4FsMefMepIdentifier,
                                        tMacAddr
                                        SetValFsMefMepTransmitDmDestMacAddress)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmDestMacAddress (u4FsMefContextId,
                                                    u4FsMefMegIndex,
                                                    u4FsMefMeIndex,
                                                    u4FsMefMepIdentifier,
                                                    SetValFsMefMepTransmitDmDestMacAddress)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitDmDestMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitDmDestMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitDmDestMepId (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   UINT4 u4SetValFsMefMepTransmitDmDestMepId)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmDestMepId (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               u4SetValFsMefMepTransmitDmDestMepId)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitDmDestIsMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitDmDestIsMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitDmDestIsMepId (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4 i4SetValFsMefMepTransmitDmDestIsMepId)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmDestIsMepId (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 i4SetValFsMefMepTransmitDmDestIsMepId)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepTransmitDmDeadline
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepTransmitDmDeadline
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepTransmitDmDeadline (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  UINT4 u4SetValFsMefMepTransmitDmDeadline)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepTransmitDmDeadline (u4FsMefContextId,
                                              u4FsMefMegIndex,
                                              u4FsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              u4SetValFsMefMepTransmitDmDeadline)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepDmrOptionalFields
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepDmrOptionalFields
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepDmrOptionalFields (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                 UINT4 u4FsMefMeIndex,
                                 UINT4 u4FsMefMepIdentifier,
                                 INT4 i4SetValFsMefMepDmrOptionalFields)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepDmrOptionalFields (u4FsMefContextId,
                                             u4FsMefMegIndex,
                                             u4FsMefMeIndex,
                                             u4FsMefMepIdentifier,
                                             i4SetValFsMefMepDmrOptionalFields)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMep1DmRecvCapability
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMep1DmRecvCapability
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMep1DmRecvCapability (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                 UINT4 u4FsMefMeIndex,
                                 UINT4 u4FsMefMepIdentifier,
                                 INT4 i4SetValFsMefMep1DmRecvCapability)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731Mep1DmRecvCapability (u4FsMefContextId,
                                             u4FsMefMegIndex,
                                             u4FsMefMeIndex,
                                             u4FsMefMepIdentifier,
                                             i4SetValFsMefMep1DmRecvCapability)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepFrameDelayThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepFrameDelayThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepFrameDelayThreshold (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   UINT4 u4SetValFsMefMepFrameDelayThreshold)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731MepFrameDelayThreshold (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               u4SetValFsMefMepFrameDelayThreshold)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepRowStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepRowStatus (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                         UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                         INT4 i4SetValFsMefMepRowStatus)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepRowStatus (u4FsMefContextId,
                                     u4FsMefMegIndex,
                                     u4FsMefMeIndex,
                                     u4FsMefMepIdentifier,
                                     i4SetValFsMefMepRowStatus) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMep1DmTransInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMep1DmTransInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMep1DmTransInterval (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                UINT4 u4FsMefMeIndex,
                                UINT4 u4FsMefMepIdentifier,
                                INT4 i4SetValFsMefMep1DmTransInterval)
{
    ECFM_LBLT_LOCK ();
    if (nmhSetFsMIY1731Mep1DmTransInterval (u4FsMefContextId,
                                            u4FsMefMegIndex,
                                            u4FsMefMeIndex,
                                            u4FsMefMepIdentifier,
                                            i4SetValFsMefMep1DmTransInterval)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitLmmStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitLmmStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitLmmStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    INT4 i4TestValFsMefMepTransmitLmmStatus)
{
    ECFM_CC_LOCK ();

    if (nmhTestv2FsMIY1731MepTransmitLmmStatus
        (pu4ErrorCode,
         u4FsMefContextId,
         u4FsMefMegIndex,
         u4FsMefMeIndex,
         u4FsMefMepIdentifier,
         i4TestValFsMefMepTransmitLmmStatus) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitLmmInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitLmmInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitLmmInterval (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      INT4 i4TestValFsMefMepTransmitLmmInterval)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmInterval
        (pu4ErrorCode,
         u4FsMefContextId,
         u4FsMefMegIndex,
         u4FsMefMeIndex,
         u4FsMefMepIdentifier,
         i4TestValFsMefMepTransmitLmmInterval) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitLmmDeadline
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitLmmDeadline
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitLmmDeadline (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      UINT4
                                      u4TestValFsMefMepTransmitLmmDeadline)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmDeadline (pu4ErrorCode,
                                                  u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  u4TestValFsMefMepTransmitLmmDeadline)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitLmmDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitLmmDropEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitLmmDropEnable (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMefContextId,
                                        UINT4 u4FsMefMegIndex,
                                        UINT4 u4FsMefMeIndex,
                                        UINT4 u4FsMefMepIdentifier,
                                        INT4
                                        i4TestValFsMefMepTransmitLmmDropEnable)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmDropEnable (pu4ErrorCode,
                                                    u4FsMefContextId,
                                                    u4FsMefMegIndex,
                                                    u4FsMefMeIndex,
                                                    u4FsMefMepIdentifier,
                                                    i4TestValFsMefMepTransmitLmmDropEnable)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitLmmPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitLmmPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitLmmPriority (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      INT4 i4TestValFsMefMepTransmitLmmPriority)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmPriority (pu4ErrorCode,
                                                  u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  i4TestValFsMefMepTransmitLmmPriority)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitLmmDestMacAddress
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitLmmDestMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitLmmDestMacAddress (UINT4 *pu4ErrorCode,
                                            UINT4 u4FsMefContextId,
                                            UINT4 u4FsMefMegIndex,
                                            UINT4 u4FsMefMeIndex,
                                            UINT4 u4FsMefMepIdentifier,
                                            tMacAddr
                                            TestValFsMefMepTransmitLmmDestMacAddress)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmDestMacAddress (pu4ErrorCode,
                                                        u4FsMefContextId,
                                                        u4FsMefMegIndex,
                                                        u4FsMefMeIndex,
                                                        u4FsMefMepIdentifier,
                                                        TestValFsMefMepTransmitLmmDestMacAddress)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitLmmDestMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitLmmDestMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitLmmDestMepId (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMefContextId,
                                       UINT4 u4FsMefMegIndex,
                                       UINT4 u4FsMefMeIndex,
                                       UINT4 u4FsMefMepIdentifier,
                                       UINT4
                                       u4TestValFsMefMepTransmitLmmDestMepId)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmDestMepId (pu4ErrorCode,
                                                   u4FsMefContextId,
                                                   u4FsMefMegIndex,
                                                   u4FsMefMeIndex,
                                                   u4FsMefMepIdentifier,
                                                   u4TestValFsMefMepTransmitLmmDestMepId)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitLmmDestIsMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitLmmDestIsMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitLmmDestIsMepId (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsMefContextId,
                                         UINT4 u4FsMefMegIndex,
                                         UINT4 u4FsMefMeIndex,
                                         UINT4 u4FsMefMepIdentifier,
                                         INT4
                                         i4TestValFsMefMepTransmitLmmDestIsMepId)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmDestIsMepId (pu4ErrorCode,
                                                     u4FsMefContextId,
                                                     u4FsMefMegIndex,
                                                     u4FsMefMeIndex,
                                                     u4FsMefMepIdentifier,
                                                     i4TestValFsMefMepTransmitLmmDestIsMepId)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitLmmMessages
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitLmmMessages
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitLmmMessages (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      INT4 i4TestValFsMefMepTransmitLmmMessages)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitLmmMessages (pu4ErrorCode,
                                                  u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  i4TestValFsMefMepTransmitLmmMessages)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepNearEndFrameLossThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepNearEndFrameLossThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepNearEndFrameLossThreshold (UINT4 *pu4ErrorCode,
                                            UINT4 u4FsMefContextId,
                                            UINT4 u4FsMefMegIndex,
                                            UINT4 u4FsMefMeIndex,
                                            UINT4 u4FsMefMepIdentifier,
                                            UINT4
                                            u4TestValFsMefMepNearEndFrameLossThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepNearEndFrameLossThreshold (pu4ErrorCode,
                                                        u4FsMefContextId,
                                                        u4FsMefMegIndex,
                                                        u4FsMefMeIndex,
                                                        u4FsMefMepIdentifier,
                                                        u4TestValFsMefMepNearEndFrameLossThreshold)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepFarEndFrameLossThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepFarEndFrameLossThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepFarEndFrameLossThreshold (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsMefContextId,
                                           UINT4 u4FsMefMegIndex,
                                           UINT4 u4FsMefMeIndex,
                                           UINT4 u4FsMefMepIdentifier,
                                           UINT4
                                           u4TestValFsMefMepFarEndFrameLossThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepFarEndFrameLossThreshold (pu4ErrorCode,
                                                       u4FsMefContextId,
                                                       u4FsMefMegIndex,
                                                       u4FsMefMeIndex,
                                                       u4FsMefMepIdentifier,
                                                       u4TestValFsMefMepFarEndFrameLossThreshold)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitDmStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitDmStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitDmStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 i4TestValFsMefMepTransmitDmStatus)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmStatus (pu4ErrorCode,
                                               u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               i4TestValFsMefMepTransmitDmStatus)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitDmType
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitDmType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitDmType (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                                 UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                 UINT4 u4FsMefMepIdentifier,
                                 INT4 i4TestValFsMefMepTransmitDmType)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmType (pu4ErrorCode,
                                             u4FsMefContextId,
                                             u4FsMefMegIndex,
                                             u4FsMefMeIndex,
                                             u4FsMefMepIdentifier,
                                             i4TestValFsMefMepTransmitDmType)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitDmInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitDmInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitDmInterval (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4 i4TestValFsMefMepTransmitDmInterval)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmInterval (pu4ErrorCode,
                                                 u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 i4TestValFsMefMepTransmitDmInterval)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitDmMessages
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitDmMessages
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitDmMessages (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4 i4TestValFsMefMepTransmitDmMessages)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmMessages (pu4ErrorCode,
                                                 u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 i4TestValFsMefMepTransmitDmMessages)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitDmmDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitDmmDropEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitDmmDropEnable (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMefContextId,
                                        UINT4 u4FsMefMegIndex,
                                        UINT4 u4FsMefMeIndex,
                                        UINT4 u4FsMefMepIdentifier,
                                        INT4
                                        i4TestValFsMefMepTransmitDmmDropEnable)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmmDropEnable (pu4ErrorCode,
                                                    u4FsMefContextId,
                                                    u4FsMefMegIndex,
                                                    u4FsMefMeIndex,
                                                    u4FsMefMepIdentifier,
                                                    i4TestValFsMefMepTransmitDmmDropEnable)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmit1DmDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmit1DmDropEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmit1DmDropEnable (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMefContextId,
                                        UINT4 u4FsMefMegIndex,
                                        UINT4 u4FsMefMeIndex,
                                        UINT4 u4FsMefMepIdentifier,
                                        INT4
                                        i4TestValFsMefMepTransmit1DmDropEnable)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmit1DmDropEnable (pu4ErrorCode,
                                                    u4FsMefContextId,
                                                    u4FsMefMegIndex,
                                                    u4FsMefMeIndex,
                                                    u4FsMefMepIdentifier,
                                                    i4TestValFsMefMepTransmit1DmDropEnable)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitDmmPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitDmmPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitDmmPriority (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      INT4 i4TestValFsMefMepTransmitDmmPriority)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmmPriority (pu4ErrorCode,
                                                  u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  i4TestValFsMefMepTransmitDmmPriority)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmit1DmPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmit1DmPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmit1DmPriority (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      INT4 i4TestValFsMefMepTransmit1DmPriority)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmit1DmPriority (pu4ErrorCode,
                                                  u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  i4TestValFsMefMepTransmit1DmPriority)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitDmDestMacAddress
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitDmDestMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitDmDestMacAddress (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsMefContextId,
                                           UINT4 u4FsMefMegIndex,
                                           UINT4 u4FsMefMeIndex,
                                           UINT4 u4FsMefMepIdentifier,
                                           tMacAddr
                                           TestValFsMefMepTransmitDmDestMacAddress)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmDestMacAddress (pu4ErrorCode,
                                                       u4FsMefContextId,
                                                       u4FsMefMegIndex,
                                                       u4FsMefMeIndex,
                                                       u4FsMefMepIdentifier,
                                                       TestValFsMefMepTransmitDmDestMacAddress)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitDmDestMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitDmDestMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitDmDestMepId (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      UINT4
                                      u4TestValFsMefMepTransmitDmDestMepId)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmDestMepId (pu4ErrorCode,
                                                  u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  u4TestValFsMefMepTransmitDmDestMepId)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitDmDestIsMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitDmDestIsMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitDmDestIsMepId (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMefContextId,
                                        UINT4 u4FsMefMegIndex,
                                        UINT4 u4FsMefMeIndex,
                                        UINT4 u4FsMefMepIdentifier,
                                        INT4
                                        i4TestValFsMefMepTransmitDmDestIsMepId)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmDestIsMepId (pu4ErrorCode,
                                                    u4FsMefContextId,
                                                    u4FsMefMegIndex,
                                                    u4FsMefMeIndex,
                                                    u4FsMefMepIdentifier,
                                                    i4TestValFsMefMepTransmitDmDestIsMepId)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepTransmitDmDeadline
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepTransmitDmDeadline
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepTransmitDmDeadline (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     UINT4 u4TestValFsMefMepTransmitDmDeadline)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepTransmitDmDeadline (pu4ErrorCode,
                                                 u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 u4TestValFsMefMepTransmitDmDeadline)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepDmrOptionalFields
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepDmrOptionalFields
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepDmrOptionalFields (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    INT4 i4TestValFsMefMepDmrOptionalFields)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepDmrOptionalFields (pu4ErrorCode,
                                                u4FsMefContextId,
                                                u4FsMefMegIndex,
                                                u4FsMefMeIndex,
                                                u4FsMefMepIdentifier,
                                                i4TestValFsMefMepDmrOptionalFields)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMep1DmRecvCapability
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMep1DmRecvCapability
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMep1DmRecvCapability (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    INT4 i4TestValFsMefMep1DmRecvCapability)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731Mep1DmRecvCapability (pu4ErrorCode,
                                                u4FsMefContextId,
                                                u4FsMefMegIndex,
                                                u4FsMefMeIndex,
                                                u4FsMefMepIdentifier,
                                                i4TestValFsMefMep1DmRecvCapability)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepFrameDelayThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepFrameDelayThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepFrameDelayThreshold (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      UINT4
                                      u4TestValFsMefMepFrameDelayThreshold)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731MepFrameDelayThreshold (pu4ErrorCode,
                                                  u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  u4TestValFsMefMepFrameDelayThreshold)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepRowStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                            UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                            UINT4 u4FsMefMepIdentifier,
                            INT4 i4TestValFsMefMepRowStatus)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepRowStatus (pu4ErrorCode,
                                        u4FsMefContextId,
                                        u4FsMefMegIndex,
                                        u4FsMefMeIndex,
                                        u4FsMefMepIdentifier,
                                        i4TestValFsMefMepRowStatus)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMep1DmTransInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMep1DmTransInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMep1DmTransInterval (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 i4TestValFsMefMep1DmTransInterval)
{
    ECFM_LBLT_LOCK ();
    if (nmhTestv2FsMIY1731Mep1DmTransInterval (pu4ErrorCode,
                                               u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               i4TestValFsMefMep1DmTransInterval)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMefMepTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMefMepTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefFdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefFdTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
                FsMefFdSeqNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefFdTable (UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      UINT4 u4FsMefFdTransId,
                                      UINT4 u4FsMefFdSeqNumber)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdTable (u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  u4FsMefFdTransId,
                                                  u4FsMefFdSeqNumber)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefFdTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
                FsMefFdSeqNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefFdTable (UINT4 *pu4FsMefContextId, UINT4 *pu4FsMefMegIndex,
                              UINT4 *pu4FsMefMeIndex,
                              UINT4 *pu4FsMefMepIdentifier,
                              UINT4 *pu4FsMefFdTransId,
                              UINT4 *pu4FsMefFdSeqNumber)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFirstIndexFsMIY1731FdTable (pu4FsMefContextId,
                                          pu4FsMefMegIndex,
                                          pu4FsMefMeIndex,
                                          pu4FsMefMepIdentifier,
                                          pu4FsMefFdTransId,
                                          pu4FsMefFdSeqNumber) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefFdTable
 Input       :  The Indices
                FsMefContextId
                nextFsMefContextId
                FsMefMegIndex
                nextFsMefMegIndex
                FsMefMeIndex
                nextFsMefMeIndex
                FsMefMepIdentifier
                nextFsMefMepIdentifier
                FsMefFdTransId
                nextFsMefFdTransId
                FsMefFdSeqNumber
                nextFsMefFdSeqNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefFdTable (UINT4 u4FsMefContextId,
                             UINT4 *pu4NextFsMefContextId,
                             UINT4 u4FsMefMegIndex, UINT4 *pu4NextFsMefMegIndex,
                             UINT4 u4FsMefMeIndex, UINT4 *pu4NextFsMefMeIndex,
                             UINT4 u4FsMefMepIdentifier,
                             UINT4 *pu4NextFsMefMepIdentifier,
                             UINT4 u4FsMefFdTransId,
                             UINT4 *pu4NextFsMefFdTransId,
                             UINT4 u4FsMefFdSeqNumber,
                             UINT4 *pu4NextFsMefFdSeqNumber)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetNextIndexFsMIY1731FdTable (u4FsMefContextId,
                                         pu4NextFsMefContextId,
                                         u4FsMefMegIndex,
                                         pu4NextFsMefMegIndex,
                                         u4FsMefMeIndex, pu4NextFsMefMeIndex,
                                         u4FsMefMepIdentifier,
                                         pu4NextFsMefMepIdentifier,
                                         u4FsMefFdTransId,
                                         pu4NextFsMefFdTransId,
                                         u4FsMefFdSeqNumber,
                                         pu4NextFsMefFdSeqNumber)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefFdTxTimeStampf
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
                FsMefFdSeqNumber

                The Object 
                retValFsMefFdTxTimeStampf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdTxTimeStampf (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                           UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                           UINT4 u4FsMefFdTransId, UINT4 u4FsMefFdSeqNumber,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsMefFdTxTimeStampf)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdTxTimeStampf (u4FsMefContextId,
                                       u4FsMefMegIndex,
                                       u4FsMefMeIndex,
                                       u4FsMefMepIdentifier,
                                       u4FsMefFdTransId,
                                       u4FsMefFdSeqNumber,
                                       pRetValFsMefFdTxTimeStampf)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdMeasurementTimeStamp
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
                FsMefFdSeqNumber

                The Object 
                retValFsMefFdMeasurementTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdMeasurementTimeStamp (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   UINT4 u4FsMefFdTransId,
                                   UINT4 u4FsMefFdSeqNumber,
                                   UINT4 *pu4RetValFsMefFdMeasurementTimeStamp)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdMeasurementTimeStamp (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               u4FsMefFdTransId,
                                               u4FsMefFdSeqNumber,
                                               pu4RetValFsMefFdMeasurementTimeStamp)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdPeerMepMacAddress
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
                FsMefFdSeqNumber

                The Object 
                retValFsMefFdPeerMepMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdPeerMepMacAddress (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                UINT4 u4FsMefMeIndex,
                                UINT4 u4FsMefMepIdentifier,
                                UINT4 u4FsMefFdTransId,
                                UINT4 u4FsMefFdSeqNumber,
                                tMacAddr * pRetValFsMefFdPeerMepMacAddress)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdPeerMepMacAddress (u4FsMefContextId,
                                            u4FsMefMegIndex,
                                            u4FsMefMeIndex,
                                            u4FsMefMepIdentifier,
                                            u4FsMefFdTransId,
                                            u4FsMefFdSeqNumber,
                                            pRetValFsMefFdPeerMepMacAddress)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdIfIndex
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
                FsMefFdSeqNumber

                The Object 
                retValFsMefFdIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdIfIndex (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                      UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                      UINT4 u4FsMefFdTransId, UINT4 u4FsMefFdSeqNumber,
                      INT4 *pi4RetValFsMefFdIfIndex)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdIfIndex (u4FsMefContextId,
                                  u4FsMefMegIndex,
                                  u4FsMefMeIndex,
                                  u4FsMefMepIdentifier,
                                  u4FsMefFdTransId,
                                  u4FsMefFdSeqNumber,
                                  pi4RetValFsMefFdIfIndex) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdDelayValue
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
                FsMefFdSeqNumber

                The Object 
                retValFsMefFdDelayValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdDelayValue (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                         UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                         UINT4 u4FsMefFdTransId, UINT4 u4FsMefFdSeqNumber,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsMefFdDelayValue)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdDelayValue (u4FsMefContextId,
                                     u4FsMefMegIndex,
                                     u4FsMefMeIndex,
                                     u4FsMefMepIdentifier,
                                     u4FsMefFdTransId,
                                     u4FsMefFdSeqNumber,
                                     pRetValFsMefFdDelayValue) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdIFDV
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
                FsMefFdSeqNumber

                The Object 
                retValFsMefFdIFDV
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdIFDV (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                   UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                   UINT4 u4FsMefFdTransId, UINT4 u4FsMefFdSeqNumber,
                   tSNMP_OCTET_STRING_TYPE * pRetValFsMefFdIFDV)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdIFDV (u4FsMefContextId,
                               u4FsMefMegIndex,
                               u4FsMefMeIndex,
                               u4FsMefMepIdentifier,
                               u4FsMefFdTransId,
                               u4FsMefFdSeqNumber, pRetValFsMefFdIFDV)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdFDV
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
                FsMefFdSeqNumber

                The Object 
                retValFsMefFdFDV
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdFDV (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                  UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                  UINT4 u4FsMefFdTransId, UINT4 u4FsMefFdSeqNumber,
                  tSNMP_OCTET_STRING_TYPE * pRetValFsMefFdFDV)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdFDV (u4FsMefContextId, u4FsMefMegIndex,
                              u4FsMefMeIndex, u4FsMefMepIdentifier,
                              u4FsMefFdTransId, u4FsMefFdSeqNumber,
                              pRetValFsMefFdFDV) == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdMeasurementType
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
                FsMefFdSeqNumber

                The Object 
                retValFsMefFdMeasurementType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdMeasurementType (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                              UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                              UINT4 u4FsMefFdTransId, UINT4 u4FsMefFdSeqNumber,
                              INT4 *pi4RetValFsMefFdMeasurementType)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdMeasurementType (u4FsMefContextId, u4FsMefMegIndex,
                                          u4FsMefMeIndex,
                                          u4FsMefMepIdentifier,
                                          u4FsMefFdTransId,
                                          u4FsMefFdSeqNumber,
                                          pi4RetValFsMefFdMeasurementType)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefFdStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefFdStatsTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefFdStatsTable (UINT4 u4FsMefContextId,
                                           UINT4 u4FsMefMegIndex,
                                           UINT4 u4FsMefMeIndex,
                                           UINT4 u4FsMefMepIdentifier,
                                           UINT4 u4FsMefFdTransId)
{
    ECFM_LBLT_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FdStatsTable (u4FsMefContextId,
                                                       u4FsMefMegIndex,
                                                       u4FsMefMeIndex,
                                                       u4FsMefMepIdentifier,
                                                       u4FsMefFdTransId)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefFdStatsTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefFdStatsTable (UINT4 *pu4FsMefContextId,
                                   UINT4 *pu4FsMefMegIndex,
                                   UINT4 *pu4FsMefMeIndex,
                                   UINT4 *pu4FsMefMepIdentifier,
                                   UINT4 *pu4FsMefFdTransId)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFirstIndexFsMIY1731FdStatsTable (pu4FsMefContextId,
                                               pu4FsMefMegIndex,
                                               pu4FsMefMeIndex,
                                               pu4FsMefMepIdentifier,
                                               pu4FsMefFdTransId)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefFdStatsTable
 Input       :  The Indices
                FsMefContextId
                nextFsMefContextId
                FsMefMegIndex
                nextFsMefMegIndex
                FsMefMeIndex
                nextFsMefMeIndex
                FsMefMepIdentifier
                nextFsMefMepIdentifier
                FsMefFdTransId
                nextFsMefFdTransId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefFdStatsTable (UINT4 u4FsMefContextId,
                                  UINT4 *pu4NextFsMefContextId,
                                  UINT4 u4FsMefMegIndex,
                                  UINT4 *pu4NextFsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 *pu4NextFsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  UINT4 *pu4NextFsMefMepIdentifier,
                                  UINT4 u4FsMefFdTransId,
                                  UINT4 *pu4NextFsMefFdTransId)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetNextIndexFsMIY1731FdStatsTable (u4FsMefContextId,
                                              pu4NextFsMefContextId,
                                              u4FsMefMegIndex,
                                              pu4NextFsMefMegIndex,
                                              u4FsMefMeIndex,
                                              pu4NextFsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              pu4NextFsMefMepIdentifier,
                                              u4FsMefFdTransId,
                                              pu4NextFsMefFdTransId)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefFdStatsTimeStamp
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId

                The Object 
                retValFsMefFdStatsTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdStatsTimeStamp (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                             UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                             UINT4 u4FsMefFdTransId,
                             UINT4 *pu4RetValFsMefFdStatsTimeStamp)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdStatsTimeStamp (u4FsMefContextId,
                                         u4FsMefMegIndex,
                                         u4FsMefMeIndex,
                                         u4FsMefMepIdentifier,
                                         u4FsMefFdTransId,
                                         pu4RetValFsMefFdStatsTimeStamp)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdStatsDmmOut
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId

                The Object 
                retValFsMefFdStatsDmmOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdStatsDmmOut (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                          UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                          UINT4 u4FsMefFdTransId,
                          UINT4 *pu4RetValFsMefFdStatsDmmOut)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdStatsDmmOut (u4FsMefContextId,
                                      u4FsMefMegIndex,
                                      u4FsMefMeIndex,
                                      u4FsMefMepIdentifier,
                                      u4FsMefFdTransId,
                                      pu4RetValFsMefFdStatsDmmOut)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdStatsDmrIn
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId

                The Object 
                retValFsMefFdStatsDmrIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdStatsDmrIn (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                         UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                         UINT4 u4FsMefFdTransId,
                         UINT4 *pu4RetValFsMefFdStatsDmrIn)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdStatsDmrIn (u4FsMefContextId,
                                     u4FsMefMegIndex,
                                     u4FsMefMeIndex,
                                     u4FsMefMepIdentifier,
                                     u4FsMefFdTransId,
                                     pu4RetValFsMefFdStatsDmrIn)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdStatsDelayAverage
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId

                The Object 
                retValFsMefFdStatsDelayAverage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdStatsDelayAverage (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                UINT4 u4FsMefMeIndex,
                                UINT4 u4FsMefMepIdentifier,
                                UINT4 u4FsMefFdTransId,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsMefFdStatsDelayAverage)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdStatsDelayAverage (u4FsMefContextId,
                                            u4FsMefMegIndex,
                                            u4FsMefMeIndex,
                                            u4FsMefMepIdentifier,
                                            u4FsMefFdTransId,
                                            pRetValFsMefFdStatsDelayAverage)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdStatsFDVAverage
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId

                The Object 
                retValFsMefFdStatsFDVAverage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdStatsFDVAverage (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                              UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                              UINT4 u4FsMefFdTransId,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsMefFdStatsFDVAverage)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdStatsFDVAverage (u4FsMefContextId,
                                          u4FsMefMegIndex,
                                          u4FsMefMeIndex,
                                          u4FsMefMepIdentifier,
                                          u4FsMefFdTransId,
                                          pRetValFsMefFdStatsFDVAverage)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdStatsIFDVAverage
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId

                The Object 
                retValFsMefFdStatsIFDVAverage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdStatsIFDVAverage (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                               UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                               UINT4 u4FsMefFdTransId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMefFdStatsIFDVAverage)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdStatsIFDVAverage (u4FsMefContextId,
                                           u4FsMefMegIndex,
                                           u4FsMefMeIndex,
                                           u4FsMefMepIdentifier,
                                           u4FsMefFdTransId,
                                           pRetValFsMefFdStatsIFDVAverage)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdStatsDelayMin
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId

                The Object 
                retValFsMefFdStatsDelayMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdStatsDelayMin (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                            UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                            UINT4 u4FsMefFdTransId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsMefFdStatsDelayMin)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdStatsDelayMin (u4FsMefContextId,
                                        u4FsMefMegIndex,
                                        u4FsMefMeIndex,
                                        u4FsMefMepIdentifier,
                                        u4FsMefFdTransId,
                                        pRetValFsMefFdStatsDelayMin)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFdStatsDelayMax
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFdTransId

                The Object 
                retValFsMefFdStatsDelayMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFdStatsDelayMax (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                            UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                            UINT4 u4FsMefFdTransId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsMefFdStatsDelayMax)
{
    ECFM_LBLT_LOCK ();
    if (nmhGetFsMIY1731FdStatsDelayMax (u4FsMefContextId,
                                        u4FsMefMegIndex,
                                        u4FsMefMeIndex,
                                        u4FsMefMepIdentifier,
                                        u4FsMefFdTransId,
                                        pRetValFsMefFdStatsDelayMax)
        == SNMP_FAILURE)
    {
        ECFM_LBLT_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_LBLT_UNLOCK ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefFlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefFlTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId
                FsMefFlSeqNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefFlTable (UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      UINT4 u4FsMefFlTransId,
                                      UINT4 u4FsMefFlSeqNumber)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlTable (u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  u4FsMefFlTransId,
                                                  u4FsMefFlSeqNumber)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefFlTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId
                FsMefFlSeqNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefFlTable (UINT4 *pu4FsMefContextId, UINT4 *pu4FsMefMegIndex,
                              UINT4 *pu4FsMefMeIndex,
                              UINT4 *pu4FsMefMepIdentifier,
                              UINT4 *pu4FsMefFlTransId,
                              UINT4 *pu4FsMefFlSeqNumber)
{
    ECFM_CC_LOCK ();
    if (nmhGetFirstIndexFsMIY1731FlTable (pu4FsMefContextId,
                                          pu4FsMefMegIndex, pu4FsMefMeIndex,
                                          pu4FsMefMepIdentifier,
                                          pu4FsMefFlTransId,
                                          pu4FsMefFlSeqNumber) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefFlTable
 Input       :  The Indices
                FsMefContextId
                nextFsMefContextId
                FsMefMegIndex
                nextFsMefMegIndex
                FsMefMeIndex
                nextFsMefMeIndex
                FsMefMepIdentifier
                nextFsMefMepIdentifier
                FsMefFlTransId
                nextFsMefFlTransId
                FsMefFlSeqNumber
                nextFsMefFlSeqNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefFlTable (UINT4 u4FsMefContextId,
                             UINT4 *pu4NextFsMefContextId,
                             UINT4 u4FsMefMegIndex, UINT4 *pu4NextFsMefMegIndex,
                             UINT4 u4FsMefMeIndex, UINT4 *pu4NextFsMefMeIndex,
                             UINT4 u4FsMefMepIdentifier,
                             UINT4 *pu4NextFsMefMepIdentifier,
                             UINT4 u4FsMefFlTransId,
                             UINT4 *pu4NextFsMefFlTransId,
                             UINT4 u4FsMefFlSeqNumber,
                             UINT4 *pu4NextFsMefFlSeqNumber)
{
    ECFM_CC_LOCK ();
    if (nmhGetNextIndexFsMIY1731FlTable (u4FsMefContextId,
                                         pu4NextFsMefContextId,
                                         u4FsMefMegIndex,
                                         pu4NextFsMefMegIndex,
                                         u4FsMefMeIndex, pu4NextFsMefMeIndex,
                                         u4FsMefMepIdentifier,
                                         pu4NextFsMefMepIdentifier,
                                         u4FsMefFlTransId,
                                         pu4NextFsMefFlTransId,
                                         u4FsMefFlSeqNumber,
                                         pu4NextFsMefFlSeqNumber)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefFlMeasurementTimeStamp
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId
                FsMefFlSeqNumber

                The Object 
                retValFsMefFlMeasurementTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlMeasurementTimeStamp (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   UINT4 u4FsMefFlTransId,
                                   UINT4 u4FsMefFlSeqNumber,
                                   UINT4 *pu4RetValFsMefFlMeasurementTimeStamp)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlMeasurementTimeStamp (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               u4FsMefFlTransId,
                                               u4FsMefFlSeqNumber,
                                               pu4RetValFsMefFlMeasurementTimeStamp)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlPeerMepMacAddress
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId
                FsMefFlSeqNumber

                The Object 
                retValFsMefFlPeerMepMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlPeerMepMacAddress (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                UINT4 u4FsMefMeIndex,
                                UINT4 u4FsMefMepIdentifier,
                                UINT4 u4FsMefFlTransId,
                                UINT4 u4FsMefFlSeqNumber,
                                tMacAddr * pRetValFsMefFlPeerMepMacAddress)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlPeerMepMacAddress (u4FsMefContextId,
                                            u4FsMefMegIndex,
                                            u4FsMefMeIndex,
                                            u4FsMefMepIdentifier,
                                            u4FsMefFlTransId,
                                            u4FsMefFlSeqNumber,
                                            pRetValFsMefFlPeerMepMacAddress)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlIfIndex
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId
                FsMefFlSeqNumber

                The Object 
                retValFsMefFlIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlIfIndex (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                      UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                      UINT4 u4FsMefFlTransId, UINT4 u4FsMefFlSeqNumber,
                      INT4 *pi4RetValFsMefFlIfIndex)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlIfIndex (u4FsMefContextId,
                                  u4FsMefMegIndex,
                                  u4FsMefMeIndex,
                                  u4FsMefMepIdentifier,
                                  u4FsMefFlTransId,
                                  u4FsMefFlSeqNumber,
                                  pi4RetValFsMefFlIfIndex) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlFarEndLoss
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId
                FsMefFlSeqNumber

                The Object 
                retValFsMefFlFarEndLoss
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlFarEndLoss (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                         UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                         UINT4 u4FsMefFlTransId, UINT4 u4FsMefFlSeqNumber,
                         UINT4 *pu4RetValFsMefFlFarEndLoss)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlFarEndLoss (u4FsMefContextId,
                                     u4FsMefMegIndex,
                                     u4FsMefMeIndex,
                                     u4FsMefMepIdentifier,
                                     u4FsMefFlTransId,
                                     u4FsMefFlSeqNumber,
                                     pu4RetValFsMefFlFarEndLoss)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlNearEndLoss
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId
                FsMefFlSeqNumber

                The Object 
                retValFsMefFlNearEndLoss
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlNearEndLoss (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                          UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                          UINT4 u4FsMefFlTransId, UINT4 u4FsMefFlSeqNumber,
                          UINT4 *pu4RetValFsMefFlNearEndLoss)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlNearEndLoss (u4FsMefContextId,
                                      u4FsMefMegIndex,
                                      u4FsMefMeIndex,
                                      u4FsMefMepIdentifier,
                                      u4FsMefFlTransId,
                                      u4FsMefFlSeqNumber,
                                      pu4RetValFsMefFlNearEndLoss)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlMeasurementTime
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId
                FsMefFlSeqNumber

                The Object 
                retValFsMefFlMeasurementTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlMeasurementTime (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                              UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                              UINT4 u4FsMefFlTransId, UINT4 u4FsMefFlSeqNumber,
                              INT4 *pi4RetValFsMefFlMeasurementTime)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlMeasurementTime (u4FsMefContextId,
                                          u4FsMefMegIndex,
                                          u4FsMefMeIndex,
                                          u4FsMefMepIdentifier,
                                          u4FsMefFlTransId,
                                          u4FsMefFlSeqNumber,
                                          pi4RetValFsMefFlMeasurementTime)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefFlStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefFlStatsTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefFlStatsTable (UINT4 u4FsMefContextId,
                                           UINT4 u4FsMefMegIndex,
                                           UINT4 u4FsMefMeIndex,
                                           UINT4 u4FsMefMepIdentifier,
                                           UINT4 u4FsMefFlTransId)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731FlStatsTable (u4FsMefContextId,
                                                       u4FsMefMegIndex,
                                                       u4FsMefMeIndex,
                                                       u4FsMefMepIdentifier,
                                                       u4FsMefFlTransId)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefFlStatsTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefFlStatsTable (UINT4 *pu4FsMefContextId,
                                   UINT4 *pu4FsMefMegIndex,
                                   UINT4 *pu4FsMefMeIndex,
                                   UINT4 *pu4FsMefMepIdentifier,
                                   UINT4 *pu4FsMefFlTransId)
{
    ECFM_CC_LOCK ();
    if (nmhGetFirstIndexFsMIY1731FlStatsTable (pu4FsMefContextId,
                                               pu4FsMefMegIndex,
                                               pu4FsMefMeIndex,
                                               pu4FsMefMepIdentifier,
                                               pu4FsMefFlTransId)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefFlStatsTable
 Input       :  The Indices
                FsMefContextId
                nextFsMefContextId
                FsMefMegIndex
                nextFsMefMegIndex
                FsMefMeIndex
                nextFsMefMeIndex
                FsMefMepIdentifier
                nextFsMefMepIdentifier
                FsMefFlTransId
                nextFsMefFlTransId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefFlStatsTable (UINT4 u4FsMefContextId,
                                  UINT4 *pu4NextFsMefContextId,
                                  UINT4 u4FsMefMegIndex,
                                  UINT4 *pu4NextFsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 *pu4NextFsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  UINT4 *pu4NextFsMefMepIdentifier,
                                  UINT4 u4FsMefFlTransId,
                                  UINT4 *pu4NextFsMefFlTransId)
{
    ECFM_CC_LOCK ();
    if (nmhGetNextIndexFsMIY1731FlStatsTable (u4FsMefContextId,
                                              pu4NextFsMefContextId,
                                              u4FsMefMegIndex,
                                              pu4NextFsMefMegIndex,
                                              u4FsMefMeIndex,
                                              pu4NextFsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              pu4NextFsMefMepIdentifier,
                                              u4FsMefFlTransId,
                                              pu4NextFsMefFlTransId)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefFlStatsTimeStamp
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId

                The Object 
                retValFsMefFlStatsTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlStatsTimeStamp (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                             UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                             UINT4 u4FsMefFlTransId,
                             UINT4 *pu4RetValFsMefFlStatsTimeStamp)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlStatsTimeStamp (u4FsMefContextId,
                                         u4FsMefMegIndex,
                                         u4FsMefMeIndex,
                                         u4FsMefMepIdentifier,
                                         u4FsMefFlTransId,
                                         pu4RetValFsMefFlStatsTimeStamp)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlStatsMessagesOut
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId

                The Object 
                retValFsMefFlStatsMessagesOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlStatsMessagesOut (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                               UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                               UINT4 u4FsMefFlTransId,
                               UINT4 *pu4RetValFsMefFlStatsMessagesOut)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlStatsMessagesOut (u4FsMefContextId,
                                           u4FsMefMegIndex,
                                           u4FsMefMeIndex,
                                           u4FsMefMepIdentifier,
                                           u4FsMefFlTransId,
                                           pu4RetValFsMefFlStatsMessagesOut)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlStatsMessagesIn
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId

                The Object 
                retValFsMefFlStatsMessagesIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlStatsMessagesIn (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                              UINT4 u4FsMefMeIndex, UINT4 u4FsMefMepIdentifier,
                              UINT4 u4FsMefFlTransId,
                              UINT4 *pu4RetValFsMefFlStatsMessagesIn)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlStatsMessagesIn (u4FsMefContextId,
                                          u4FsMefMegIndex,
                                          u4FsMefMeIndex,
                                          u4FsMefMepIdentifier,
                                          u4FsMefFlTransId,
                                          pu4RetValFsMefFlStatsMessagesIn)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlStatsFarEndLossAverage
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId

                The Object 
                retValFsMefFlStatsFarEndLossAverage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlStatsFarEndLossAverage (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     UINT4 u4FsMefFlTransId,
                                     UINT4
                                     *pu4RetValFsMefFlStatsFarEndLossAverage)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlStatsFarEndLossAverage (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 u4FsMefFlTransId,
                                                 pu4RetValFsMefFlStatsFarEndLossAverage)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlStatsNearEndLossAverage
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId

                The Object 
                retValFsMefFlStatsNearEndLossAverage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlStatsNearEndLossAverage (UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      UINT4 u4FsMefFlTransId,
                                      UINT4
                                      *pu4RetValFsMefFlStatsNearEndLossAverage)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlStatsNearEndLossAverage (u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  u4FsMefFlTransId,
                                                  pu4RetValFsMefFlStatsNearEndLossAverage)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlStatsMeasurementType
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId

                The Object 
                retValFsMefFlStatsMeasurementType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlStatsMeasurementType (UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   UINT4 u4FsMefFlTransId,
                                   INT4 *pi4RetValFsMefFlStatsMeasurementType)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlStatsMeasurementType (u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               u4FsMefFlTransId,
                                               pi4RetValFsMefFlStatsMeasurementType)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlStatsFarEndLossMin
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId

                The Object 
                retValFsMefFlStatsFarEndLossMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlStatsFarEndLossMin (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                 UINT4 u4FsMefMeIndex,
                                 UINT4 u4FsMefMepIdentifier,
                                 UINT4 u4FsMefFlTransId,
                                 UINT4 *pu4RetValFsMefFlStatsFarEndLossMin)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlStatsFarEndLossMin (u4FsMefContextId,
                                             u4FsMefMegIndex,
                                             u4FsMefMeIndex,
                                             u4FsMefMepIdentifier,
                                             u4FsMefFlTransId,
                                             pu4RetValFsMefFlStatsFarEndLossMin)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlStatsFarEndLossMax
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId

                The Object 
                retValFsMefFlStatsFarEndLossMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlStatsFarEndLossMax (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                 UINT4 u4FsMefMeIndex,
                                 UINT4 u4FsMefMepIdentifier,
                                 UINT4 u4FsMefFlTransId,
                                 UINT4 *pu4RetValFsMefFlStatsFarEndLossMax)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlStatsFarEndLossMax (u4FsMefContextId,
                                             u4FsMefMegIndex,
                                             u4FsMefMeIndex,
                                             u4FsMefMepIdentifier,
                                             u4FsMefFlTransId,
                                             pu4RetValFsMefFlStatsFarEndLossMax)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlStatsNearEndLossMin
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId

                The Object 
                retValFsMefFlStatsNearEndLossMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlStatsNearEndLossMin (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  UINT4 u4FsMefFlTransId,
                                  UINT4 *pu4RetValFsMefFlStatsNearEndLossMin)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlStatsNearEndLossMin (u4FsMefContextId,
                                              u4FsMefMegIndex,
                                              u4FsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              u4FsMefFlTransId,
                                              pu4RetValFsMefFlStatsNearEndLossMin)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefFlStatsNearEndLossMax
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
                FsMefFlTransId

                The Object 
                retValFsMefFlStatsNearEndLossMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefFlStatsNearEndLossMax (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  UINT4 u4FsMefFlTransId,
                                  UINT4 *pu4RetValFsMefFlStatsNearEndLossMax)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731FlStatsNearEndLossMax (u4FsMefContextId,
                                              u4FsMefMegIndex,
                                              u4FsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              u4FsMefFlTransId,
                                              pu4RetValFsMefFlStatsNearEndLossMax)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefMepAvailabilityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefMepAvailabilityTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefMepAvailabilityTable (UINT4 u4FsMefContextId,
                                                   UINT4 u4FsMefMegIndex,
                                                   UINT4 u4FsMefMeIndex,
                                                   UINT4 u4FsMefMepIdentifier)
{
    ECFM_CC_LOCK ();
    if (nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable
        (u4FsMefContextId,
         u4FsMefMegIndex, u4FsMefMeIndex, u4FsMefMepIdentifier) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefMepAvailabilityTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefMepAvailabilityTable (UINT4 *pu4FsMefContextId,
                                           UINT4 *pu4FsMefMegIndex,
                                           UINT4 *pu4FsMefMeIndex,
                                           UINT4 *pu4FsMefMepIdentifier)
{
    ECFM_CC_LOCK ();
    if (nmhGetFirstIndexFsMIY1731MepAvailabilityTable (pu4FsMefContextId,
                                                       pu4FsMefMegIndex,
                                                       pu4FsMefMeIndex,
                                                       pu4FsMefMepIdentifier)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefMepAvailabilityTable
 Input       :  The Indices
                FsMefContextId
                nextFsMefContextId
                FsMefMegIndex
                nextFsMefMegIndex
                FsMefMeIndex
                nextFsMefMeIndex
                FsMefMepIdentifier
                nextFsMefMepIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefMepAvailabilityTable (UINT4 u4FsMefContextId,
                                          UINT4 *pu4NextFsMefContextId,
                                          UINT4 u4FsMefMegIndex,
                                          UINT4 *pu4NextFsMefMegIndex,
                                          UINT4 u4FsMefMeIndex,
                                          UINT4 *pu4NextFsMefMeIndex,
                                          UINT4 u4FsMefMepIdentifier,
                                          UINT4 *pu4NextFsMefMepIdentifier)
{
    ECFM_CC_LOCK ();
    if (nmhGetNextIndexFsMIY1731MepAvailabilityTable (u4FsMefContextId,
                                                      pu4NextFsMefContextId,
                                                      u4FsMefMegIndex,
                                                      pu4NextFsMefMegIndex,
                                                      u4FsMefMeIndex,
                                                      pu4NextFsMefMeIndex,
                                                      u4FsMefMepIdentifier,
                                                      pu4NextFsMefMepIdentifier)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityStatus (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  INT4 *pi4RetValFsMefMepAvailabilityStatus)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityStatus (u4FsMefContextId,
                                              u4FsMefMegIndex,
                                              u4FsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              pi4RetValFsMefMepAvailabilityStatus)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityResultOK
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityResultOK
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityResultOK (UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    INT4 *pi4RetValFsMefMepAvailabilityResultOK)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityResultOK (u4FsMefContextId,
                                                u4FsMefMegIndex,
                                                u4FsMefMeIndex,
                                                u4FsMefMepIdentifier,
                                                pi4RetValFsMefMepAvailabilityResultOK)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityInterval (UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    INT4 *pi4RetValFsMefMepAvailabilityInterval)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityInterval (u4FsMefContextId,
                                                u4FsMefMegIndex,
                                                u4FsMefMeIndex,
                                                u4FsMefMepIdentifier,
                                                pi4RetValFsMefMepAvailabilityInterval)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityDeadline
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityDeadline
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityDeadline (UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    UINT4
                                    *pu4RetValFsMefMepAvailabilityDeadline)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityDeadline (u4FsMefContextId,
                                                u4FsMefMegIndex,
                                                u4FsMefMeIndex,
                                                u4FsMefMepIdentifier,
                                                pu4RetValFsMefMepAvailabilityDeadline)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityLowerThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityLowerThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityLowerThreshold (UINT4 u4FsMefContextId,
                                          UINT4 u4FsMefMegIndex,
                                          UINT4 u4FsMefMeIndex,
                                          UINT4 u4FsMefMepIdentifier,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsMefMepAvailabilityLowerThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityLowerThreshold (u4FsMefContextId,
                                                      u4FsMefMegIndex,
                                                      u4FsMefMeIndex,
                                                      u4FsMefMepIdentifier,
                                                      pRetValFsMefMepAvailabilityLowerThreshold)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityUpperThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityUpperThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityUpperThreshold (UINT4 u4FsMefContextId,
                                          UINT4 u4FsMefMegIndex,
                                          UINT4 u4FsMefMeIndex,
                                          UINT4 u4FsMefMepIdentifier,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsMefMepAvailabilityUpperThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityUpperThreshold (u4FsMefContextId,
                                                      u4FsMefMegIndex,
                                                      u4FsMefMeIndex,
                                                      u4FsMefMepIdentifier,
                                                      pRetValFsMefMepAvailabilityUpperThreshold)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityModestAreaIsAvailable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityModestAreaIsAvailable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityModestAreaIsAvailable (UINT4 u4FsMefContextId,
                                                 UINT4 u4FsMefMegIndex,
                                                 UINT4 u4FsMefMeIndex,
                                                 UINT4 u4FsMefMepIdentifier,
                                                 INT4
                                                 *pi4RetValFsMefMepAvailabilityModestAreaIsAvailable)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityModestAreaIsAvailable (u4FsMefContextId,
                                                             u4FsMefMegIndex,
                                                             u4FsMefMeIndex,
                                                             u4FsMefMepIdentifier,
                                                             pi4RetValFsMefMepAvailabilityModestAreaIsAvailable)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityWindowSize
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityWindowSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityWindowSize (UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      UINT4
                                      *pu4RetValFsMefMepAvailabilityWindowSize)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityWindowSize (u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  pu4RetValFsMefMepAvailabilityWindowSize)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityDestMacAddress
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityDestMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityDestMacAddress (UINT4 u4FsMefContextId,
                                          UINT4 u4FsMefMegIndex,
                                          UINT4 u4FsMefMeIndex,
                                          UINT4 u4FsMefMepIdentifier,
                                          tMacAddr *
                                          pRetValFsMefMepAvailabilityDestMacAddress)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityDestMacAddress (u4FsMefContextId,
                                                      u4FsMefMegIndex,
                                                      u4FsMefMeIndex,
                                                      u4FsMefMepIdentifier,
                                                      pRetValFsMefMepAvailabilityDestMacAddress)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityDestMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityDestMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityDestMepId (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     UINT4
                                     *pu4RetValFsMefMepAvailabilityDestMepId)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityDestMepId (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 pu4RetValFsMefMepAvailabilityDestMepId)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityDestIsMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityDestIsMepId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityDestIsMepId (UINT4 u4FsMefContextId,
                                       UINT4 u4FsMefMegIndex,
                                       UINT4 u4FsMefMeIndex,
                                       UINT4 u4FsMefMepIdentifier,
                                       INT4
                                       *pi4RetValFsMefMepAvailabilityDestIsMepId)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityDestIsMepId (u4FsMefContextId,
                                                   u4FsMefMegIndex,
                                                   u4FsMefMeIndex,
                                                   u4FsMefMepIdentifier,
                                                   pi4RetValFsMefMepAvailabilityDestIsMepId)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityType
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityType (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                UINT4 u4FsMefMeIndex,
                                UINT4 u4FsMefMepIdentifier,
                                INT4 *pi4RetValFsMefMepAvailabilityType)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityType (u4FsMefContextId,
                                            u4FsMefMegIndex,
                                            u4FsMefMeIndex,
                                            u4FsMefMepIdentifier,
                                            pi4RetValFsMefMepAvailabilityType)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilitySchldDownInitTime
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilitySchldDownInitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilitySchldDownInitTime (UINT4 u4FsMefContextId,
                                             UINT4 u4FsMefMegIndex,
                                             UINT4 u4FsMefMeIndex,
                                             UINT4 u4FsMefMepIdentifier,
                                             UINT4
                                             *pu4RetValFsMefMepAvailabilitySchldDownInitTime)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilitySchldDownInitTime (u4FsMefContextId,
                                                         u4FsMefMegIndex,
                                                         u4FsMefMeIndex,
                                                         u4FsMefMepIdentifier,
                                                         pu4RetValFsMefMepAvailabilitySchldDownInitTime)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilitySchldDownEndTime
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilitySchldDownEndTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilitySchldDownEndTime (UINT4 u4FsMefContextId,
                                            UINT4 u4FsMefMegIndex,
                                            UINT4 u4FsMefMeIndex,
                                            UINT4 u4FsMefMepIdentifier,
                                            UINT4
                                            *pu4RetValFsMefMepAvailabilitySchldDownEndTime)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilitySchldDownEndTime (u4FsMefContextId,
                                                        u4FsMefMegIndex,
                                                        u4FsMefMeIndex,
                                                        u4FsMefMepIdentifier,
                                                        pu4RetValFsMefMepAvailabilitySchldDownEndTime)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityPriority (UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    UINT4
                                    *pu4RetValFsMefMepAvailabilityPriority)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityPriority (u4FsMefContextId,
                                                u4FsMefMegIndex,
                                                u4FsMefMeIndex,
                                                u4FsMefMepIdentifier,
                                                pu4RetValFsMefMepAvailabilityPriority)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityDropEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityDropEnable (UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      INT4
                                      *pi4RetValFsMefMepAvailabilityDropEnable)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityDropEnable (u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  pi4RetValFsMefMepAvailabilityDropEnable)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityPercentage
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityPercentage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityPercentage (UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsMefMepAvailabilityPercentage)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityPercentage (u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  pRetValFsMefMepAvailabilityPercentage)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefMepAvailabilityRowStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                retValFsMefMepAvailabilityRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefMepAvailabilityRowStatus (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4
                                     *pi4RetValFsMefMepAvailabilityRowStatus)
{
    ECFM_CC_LOCK ();
    if (nmhGetFsMIY1731MepAvailabilityRowStatus (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 pi4RetValFsMefMepAvailabilityRowStatus)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityStatus (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                  UINT4 u4FsMefMeIndex,
                                  UINT4 u4FsMefMepIdentifier,
                                  INT4 i4SetValFsMefMepAvailabilityStatus)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityStatus (u4FsMefContextId,
                                              u4FsMefMegIndex,
                                              u4FsMefMeIndex,
                                              u4FsMefMepIdentifier,
                                              i4SetValFsMefMepAvailabilityStatus)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityInterval (UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    INT4 i4SetValFsMefMepAvailabilityInterval)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityInterval (u4FsMefContextId,
                                                u4FsMefMegIndex,
                                                u4FsMefMeIndex,
                                                u4FsMefMepIdentifier,
                                                i4SetValFsMefMepAvailabilityInterval)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityDeadline
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityDeadline
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityDeadline (UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    UINT4 u4SetValFsMefMepAvailabilityDeadline)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityDeadline (u4FsMefContextId,
                                                u4FsMefMegIndex,
                                                u4FsMefMeIndex,
                                                u4FsMefMepIdentifier,
                                                u4SetValFsMefMepAvailabilityDeadline)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityLowerThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityLowerThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityLowerThreshold (UINT4 u4FsMefContextId,
                                          UINT4 u4FsMefMegIndex,
                                          UINT4 u4FsMefMeIndex,
                                          UINT4 u4FsMefMepIdentifier,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSetValFsMefMepAvailabilityLowerThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityLowerThreshold (u4FsMefContextId,
                                                      u4FsMefMegIndex,
                                                      u4FsMefMeIndex,
                                                      u4FsMefMepIdentifier,
                                                      pSetValFsMefMepAvailabilityLowerThreshold)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityUpperThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityUpperThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityUpperThreshold (UINT4 u4FsMefContextId,
                                          UINT4 u4FsMefMegIndex,
                                          UINT4 u4FsMefMeIndex,
                                          UINT4 u4FsMefMepIdentifier,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSetValFsMefMepAvailabilityUpperThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityUpperThreshold (u4FsMefContextId,
                                                      u4FsMefMegIndex,
                                                      u4FsMefMeIndex,
                                                      u4FsMefMepIdentifier,
                                                      pSetValFsMefMepAvailabilityUpperThreshold)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityModestAreaIsAvailable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityModestAreaIsAvailable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityModestAreaIsAvailable (UINT4 u4FsMefContextId,
                                                 UINT4 u4FsMefMegIndex,
                                                 UINT4 u4FsMefMeIndex,
                                                 UINT4 u4FsMefMepIdentifier,
                                                 INT4
                                                 i4SetValFsMefMepAvailabilityModestAreaIsAvailable)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityModestAreaIsAvailable
        (u4FsMefContextId, u4FsMefMegIndex, u4FsMefMeIndex,
         u4FsMefMepIdentifier,
         i4SetValFsMefMepAvailabilityModestAreaIsAvailable) == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityWindowSize
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityWindowSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityWindowSize (UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      UINT4
                                      u4SetValFsMefMepAvailabilityWindowSize)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityWindowSize (u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  u4SetValFsMefMepAvailabilityWindowSize)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityDestMacAddress
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityDestMacAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityDestMacAddress (UINT4 u4FsMefContextId,
                                          UINT4 u4FsMefMegIndex,
                                          UINT4 u4FsMefMeIndex,
                                          UINT4 u4FsMefMepIdentifier,
                                          tMacAddr
                                          SetValFsMefMepAvailabilityDestMacAddress)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityDestMacAddress (u4FsMefContextId,
                                                      u4FsMefMegIndex,
                                                      u4FsMefMeIndex,
                                                      u4FsMefMepIdentifier,
                                                      SetValFsMefMepAvailabilityDestMacAddress)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityDestMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityDestMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityDestMepId (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     UINT4
                                     u4SetValFsMefMepAvailabilityDestMepId)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityDestMepId (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 u4SetValFsMefMepAvailabilityDestMepId)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityDestIsMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityDestIsMepId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityDestIsMepId (UINT4 u4FsMefContextId,
                                       UINT4 u4FsMefMegIndex,
                                       UINT4 u4FsMefMeIndex,
                                       UINT4 u4FsMefMepIdentifier,
                                       INT4
                                       i4SetValFsMefMepAvailabilityDestIsMepId)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityDestIsMepId (u4FsMefContextId,
                                                   u4FsMefMegIndex,
                                                   u4FsMefMeIndex,
                                                   u4FsMefMepIdentifier,
                                                   i4SetValFsMefMepAvailabilityDestIsMepId)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityType
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityType (UINT4 u4FsMefContextId, UINT4 u4FsMefMegIndex,
                                UINT4 u4FsMefMeIndex,
                                UINT4 u4FsMefMepIdentifier,
                                INT4 i4SetValFsMefMepAvailabilityType)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityType (u4FsMefContextId,
                                            u4FsMefMegIndex,
                                            u4FsMefMeIndex,
                                            u4FsMefMepIdentifier,
                                            i4SetValFsMefMepAvailabilityType)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilitySchldDownInitTime
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilitySchldDownInitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilitySchldDownInitTime (UINT4 u4FsMefContextId,
                                             UINT4 u4FsMefMegIndex,
                                             UINT4 u4FsMefMeIndex,
                                             UINT4 u4FsMefMepIdentifier,
                                             UINT4
                                             u4SetValFsMefMepAvailabilitySchldDownInitTime)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilitySchldDownInitTime (u4FsMefContextId,
                                                         u4FsMefMegIndex,
                                                         u4FsMefMeIndex,
                                                         u4FsMefMepIdentifier,
                                                         u4SetValFsMefMepAvailabilitySchldDownInitTime)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilitySchldDownEndTime
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilitySchldDownEndTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilitySchldDownEndTime (UINT4 u4FsMefContextId,
                                            UINT4 u4FsMefMegIndex,
                                            UINT4 u4FsMefMeIndex,
                                            UINT4 u4FsMefMepIdentifier,
                                            UINT4
                                            u4SetValFsMefMepAvailabilitySchldDownEndTime)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilitySchldDownEndTime (u4FsMefContextId,
                                                        u4FsMefMegIndex,
                                                        u4FsMefMeIndex,
                                                        u4FsMefMepIdentifier,
                                                        u4SetValFsMefMepAvailabilitySchldDownEndTime)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityPriority (UINT4 u4FsMefContextId,
                                    UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                    UINT4 u4FsMefMepIdentifier,
                                    UINT4 u4SetValFsMefMepAvailabilityPriority)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityPriority (u4FsMefContextId,
                                                u4FsMefMegIndex,
                                                u4FsMefMeIndex,
                                                u4FsMefMepIdentifier,
                                                u4SetValFsMefMepAvailabilityPriority)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityDropEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityDropEnable (UINT4 u4FsMefContextId,
                                      UINT4 u4FsMefMegIndex,
                                      UINT4 u4FsMefMeIndex,
                                      UINT4 u4FsMefMepIdentifier,
                                      INT4
                                      i4SetValFsMefMepAvailabilityDropEnable)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityDropEnable (u4FsMefContextId,
                                                  u4FsMefMegIndex,
                                                  u4FsMefMeIndex,
                                                  u4FsMefMepIdentifier,
                                                  i4SetValFsMefMepAvailabilityDropEnable)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefMepAvailabilityRowStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                setValFsMefMepAvailabilityRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefMepAvailabilityRowStatus (UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4 i4SetValFsMefMepAvailabilityRowStatus)
{
    ECFM_CC_LOCK ();
    if (nmhSetFsMIY1731MepAvailabilityRowStatus (u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 i4SetValFsMefMepAvailabilityRowStatus)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMefContextId,
                                     UINT4 u4FsMefMegIndex,
                                     UINT4 u4FsMefMeIndex,
                                     UINT4 u4FsMefMepIdentifier,
                                     INT4 i4TestValFsMefMepAvailabilityStatus)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityStatus (pu4ErrorCode,
                                                 u4FsMefContextId,
                                                 u4FsMefMegIndex,
                                                 u4FsMefMeIndex,
                                                 u4FsMefMepIdentifier,
                                                 i4TestValFsMefMepAvailabilityStatus)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityInterval
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityInterval (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMefContextId,
                                       UINT4 u4FsMefMegIndex,
                                       UINT4 u4FsMefMeIndex,
                                       UINT4 u4FsMefMepIdentifier,
                                       INT4
                                       i4TestValFsMefMepAvailabilityInterval)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityInterval (pu4ErrorCode,
                                                   u4FsMefContextId,
                                                   u4FsMefMegIndex,
                                                   u4FsMefMeIndex,
                                                   u4FsMefMepIdentifier,
                                                   i4TestValFsMefMepAvailabilityInterval)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityDeadline
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityDeadline
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityDeadline (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMefContextId,
                                       UINT4 u4FsMefMegIndex,
                                       UINT4 u4FsMefMeIndex,
                                       UINT4 u4FsMefMepIdentifier,
                                       UINT4
                                       u4TestValFsMefMepAvailabilityDeadline)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityDeadline (pu4ErrorCode,
                                                   u4FsMefContextId,
                                                   u4FsMefMegIndex,
                                                   u4FsMefMeIndex,
                                                   u4FsMefMepIdentifier,
                                                   u4TestValFsMefMepAvailabilityDeadline)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityLowerThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityLowerThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityLowerThreshold (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsMefContextId,
                                             UINT4 u4FsMefMegIndex,
                                             UINT4 u4FsMefMeIndex,
                                             UINT4 u4FsMefMepIdentifier,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pTestValFsMefMepAvailabilityLowerThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityLowerThreshold (pu4ErrorCode,
                                                         u4FsMefContextId,
                                                         u4FsMefMegIndex,
                                                         u4FsMefMeIndex,
                                                         u4FsMefMepIdentifier,
                                                         pTestValFsMefMepAvailabilityLowerThreshold)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityUpperThreshold
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityUpperThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityUpperThreshold (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsMefContextId,
                                             UINT4 u4FsMefMegIndex,
                                             UINT4 u4FsMefMeIndex,
                                             UINT4 u4FsMefMepIdentifier,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pTestValFsMefMepAvailabilityUpperThreshold)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityUpperThreshold (pu4ErrorCode,
                                                         u4FsMefContextId,
                                                         u4FsMefMegIndex,
                                                         u4FsMefMeIndex,
                                                         u4FsMefMepIdentifier,
                                                         pTestValFsMefMepAvailabilityUpperThreshold)
        == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityModestAreaIsAvailable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityModestAreaIsAvailable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityModestAreaIsAvailable (UINT4 *pu4ErrorCode,
                                                    UINT4 u4FsMefContextId,
                                                    UINT4 u4FsMefMegIndex,
                                                    UINT4 u4FsMefMeIndex,
                                                    UINT4 u4FsMefMepIdentifier,
                                                    INT4
                                                    i4TestValFsMefMepAvailabilityModestAreaIsAvailable)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityModestAreaIsAvailable
        (pu4ErrorCode, u4FsMefContextId, u4FsMefMegIndex, u4FsMefMeIndex,
         u4FsMefMepIdentifier,
         i4TestValFsMefMepAvailabilityModestAreaIsAvailable) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityWindowSize
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityWindowSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityWindowSize (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsMefContextId,
                                         UINT4 u4FsMefMegIndex,
                                         UINT4 u4FsMefMeIndex,
                                         UINT4 u4FsMefMepIdentifier,
                                         UINT4
                                         u4TestValFsMefMepAvailabilityWindowSize)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityWindowSize (pu4ErrorCode,
                                                     u4FsMefContextId,
                                                     u4FsMefMegIndex,
                                                     u4FsMefMeIndex,
                                                     u4FsMefMepIdentifier,
                                                     u4TestValFsMefMepAvailabilityWindowSize)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityDestMacAddress
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityDestMacAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityDestMacAddress (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsMefContextId,
                                             UINT4 u4FsMefMegIndex,
                                             UINT4 u4FsMefMeIndex,
                                             UINT4 u4FsMefMepIdentifier,
                                             tMacAddr
                                             TestValFsMefMepAvailabilityDestMacAddress)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityDestMacAddress (pu4ErrorCode,
                                                         u4FsMefContextId,
                                                         u4FsMefMegIndex,
                                                         u4FsMefMeIndex,
                                                         u4FsMefMepIdentifier,
                                                         TestValFsMefMepAvailabilityDestMacAddress)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityDestMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityDestMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityDestMepId (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMefContextId,
                                        UINT4 u4FsMefMegIndex,
                                        UINT4 u4FsMefMeIndex,
                                        UINT4 u4FsMefMepIdentifier,
                                        UINT4
                                        u4TestValFsMefMepAvailabilityDestMepId)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityDestMepId (pu4ErrorCode,
                                                    u4FsMefContextId,
                                                    u4FsMefMegIndex,
                                                    u4FsMefMeIndex,
                                                    u4FsMefMepIdentifier,
                                                    u4TestValFsMefMepAvailabilityDestMepId)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityDestIsMepId
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityDestIsMepId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityDestIsMepId (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsMefContextId,
                                          UINT4 u4FsMefMegIndex,
                                          UINT4 u4FsMefMeIndex,
                                          UINT4 u4FsMefMepIdentifier,
                                          INT4
                                          i4TestValFsMefMepAvailabilityDestIsMepId)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityDestIsMepId (pu4ErrorCode,
                                                      u4FsMefContextId,
                                                      u4FsMefMegIndex,
                                                      u4FsMefMeIndex,
                                                      u4FsMefMepIdentifier,
                                                      i4TestValFsMefMepAvailabilityDestIsMepId)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityType
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityType (UINT4 *pu4ErrorCode, UINT4 u4FsMefContextId,
                                   UINT4 u4FsMefMegIndex, UINT4 u4FsMefMeIndex,
                                   UINT4 u4FsMefMepIdentifier,
                                   INT4 i4TestValFsMefMepAvailabilityType)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityType (pu4ErrorCode,
                                               u4FsMefContextId,
                                               u4FsMefMegIndex,
                                               u4FsMefMeIndex,
                                               u4FsMefMepIdentifier,
                                               i4TestValFsMefMepAvailabilityType)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilitySchldDownInitTime
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilitySchldDownInitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilitySchldDownInitTime (UINT4 *pu4ErrorCode,
                                                UINT4 u4FsMefContextId,
                                                UINT4 u4FsMefMegIndex,
                                                UINT4 u4FsMefMeIndex,
                                                UINT4 u4FsMefMepIdentifier,
                                                UINT4
                                                u4TestValFsMefMepAvailabilitySchldDownInitTime)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilitySchldDownInitTime (pu4ErrorCode,
                                                            u4FsMefContextId,
                                                            u4FsMefMegIndex,
                                                            u4FsMefMeIndex,
                                                            u4FsMefMepIdentifier,
                                                            u4TestValFsMefMepAvailabilitySchldDownInitTime)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilitySchldDownEndTime
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilitySchldDownEndTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilitySchldDownEndTime (UINT4 *pu4ErrorCode,
                                               UINT4 u4FsMefContextId,
                                               UINT4 u4FsMefMegIndex,
                                               UINT4 u4FsMefMeIndex,
                                               UINT4 u4FsMefMepIdentifier,
                                               UINT4
                                               u4TestValFsMefMepAvailabilitySchldDownEndTime)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilitySchldDownEndTime (pu4ErrorCode,
                                                           u4FsMefContextId,
                                                           u4FsMefMegIndex,
                                                           u4FsMefMeIndex,
                                                           u4FsMefMepIdentifier,
                                                           u4TestValFsMefMepAvailabilitySchldDownEndTime)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityPriority
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityPriority (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMefContextId,
                                       UINT4 u4FsMefMegIndex,
                                       UINT4 u4FsMefMeIndex,
                                       UINT4 u4FsMefMepIdentifier,
                                       UINT4
                                       u4TestValFsMefMepAvailabilityPriority)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityPriority (pu4ErrorCode,
                                                   u4FsMefContextId,
                                                   u4FsMefMegIndex,
                                                   u4FsMefMeIndex,
                                                   u4FsMefMepIdentifier,
                                                   u4TestValFsMefMepAvailabilityPriority)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityDropEnable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityDropEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityDropEnable (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsMefContextId,
                                         UINT4 u4FsMefMegIndex,
                                         UINT4 u4FsMefMeIndex,
                                         UINT4 u4FsMefMepIdentifier,
                                         INT4
                                         i4TestValFsMefMepAvailabilityDropEnable)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityDropEnable (pu4ErrorCode,
                                                     u4FsMefContextId,
                                                     u4FsMefMegIndex,
                                                     u4FsMefMeIndex,
                                                     u4FsMefMepIdentifier,
                                                     i4TestValFsMefMepAvailabilityDropEnable)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefMepAvailabilityRowStatus
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier

                The Object 
                testValFsMefMepAvailabilityRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefMepAvailabilityRowStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsMefContextId,
                                        UINT4 u4FsMefMegIndex,
                                        UINT4 u4FsMefMeIndex,
                                        UINT4 u4FsMefMepIdentifier,
                                        INT4
                                        i4TestValFsMefMepAvailabilityRowStatus)
{
    ECFM_CC_LOCK ();
    if (nmhTestv2FsMIY1731MepAvailabilityRowStatus (pu4ErrorCode,
                                                    u4FsMefContextId,
                                                    u4FsMefMegIndex,
                                                    u4FsMefMeIndex,
                                                    u4FsMefMepIdentifier,
                                                    i4TestValFsMefMepAvailabilityRowStatus)
        == SNMP_FAILURE)

    {
        ECFM_CC_UNLOCK ();
        return SNMP_FAILURE;
    }
    ECFM_CC_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMefMepAvailabilityTable
 Input       :  The Indices
                FsMefContextId
                FsMefMegIndex
                FsMefMeIndex
                FsMefMepIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMefMepAvailabilityTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMefUniListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMefUniListTable
 Input       :  The Indices
                FsMefContextId
                FsEvcIndex
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMefUniListTable (UINT4 u4FsMefContextId,
                                           INT4 i4FsEvcIndex, INT4 i4IfIndex)
{
    tMefEvcInfo         MefEvcInfo;
    tMefEvcInfo        *pMefEvcInfo = NULL;
    UINT1               u1IfType = 0;

    if (VcmIsL2VcExist (u4FsMefContextId) != VCM_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (CfaValidateIfIndex (i4IfIndex) != CFA_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    CfaGetIfaceType (i4IfIndex, &u1IfType);

    if (u1IfType != CFA_ENET)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&MefEvcInfo, 0, sizeof (tMefEvcInfo));
    MefEvcInfo.u4EvcContextId = u4FsMefContextId;
    MefEvcInfo.i4EvcVlanId = i4FsEvcIndex;

    pMefEvcInfo = (tMefEvcInfo *) RBTreeGet (MEF_EVC_TABLE, &MefEvcInfo);

    if (pMefEvcInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMefUniListTable
 Input       :  The Indices
                FsMefContextId
                FsEvcIndex
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMefUniListTable (UINT4 *pu4FsMefContextId,
                                   INT4 *pi4FsEvcIndex, INT4 *pi4IfIndex)
{
    tMefUniList        *pMefUniListInfo = NULL;

    pMefUniListInfo = (tMefUniList *) RBTreeGetFirst (MEF_UNI_LIST_TABLE);
    if (pMefUniListInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4FsMefContextId = pMefUniListInfo->u4ContextId;
    *pi4FsEvcIndex = pMefUniListInfo->i4EvcIndex;
    *pi4IfIndex = pMefUniListInfo->i4IfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMefUniListTable
 Input       :  The Indices
                FsMefContextId
                nextFsMefContextId
                FsEvcIndex
                nextFsEvcIndex
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMefUniListTable (UINT4 u4FsMefContextId,
                                  UINT4 *pu4NextFsMefContextId,
                                  INT4 i4FsEvcIndex,
                                  INT4 *pi4NextFsEvcIndex,
                                  INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tMefUniList         MefUniListInfo;
    tMefUniList        *pMefUniListInfo = NULL;

    MEMSET (&MefUniListInfo, 0, sizeof (tMefUniList));

    MefUniListInfo.u4ContextId = u4FsMefContextId;
    MefUniListInfo.i4EvcIndex = i4FsEvcIndex;
    MefUniListInfo.i4IfIndex = i4IfIndex;

    pMefUniListInfo = (tMefUniList *) RBTreeGetNext
        (MEF_UNI_LIST_TABLE, &MefUniListInfo, NULL);
    if (pMefUniListInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsMefContextId = pMefUniListInfo->u4ContextId;
    *pi4NextFsEvcIndex = pMefUniListInfo->i4EvcIndex;
    *pi4NextIfIndex = pMefUniListInfo->i4IfIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMefUniId
 Input       :  The Indices
                FsMefContextId
                FsEvcIndex
                IfIndex

                The Object 
                retValFsMefUniId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefUniId (UINT4 u4FsMefContextId,
                  INT4 i4FsEvcIndex,
                  INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pRetValFsMefUniId)
{
    tMefUniInfo        *pMefUniInfo = NULL;
    UNUSED_PARAM (u4FsMefContextId);
    UNUSED_PARAM (i4FsEvcIndex);
    UNUSED_PARAM (i4IfIndex);

    pMefUniInfo = MefGetUniEntry (i4IfIndex);
    if (pMefUniInfo == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return SNMP_FAILURE;
    }

    MEF_CPY_TO_SNMP (pRetValFsMefUniId,
                     &pMefUniInfo->au1UniId, STRLEN (pMefUniInfo->au1UniId));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefUniType
 Input       :  The Indices
                FsMefContextId
                FsEvcIndex
                IfIndex

                The Object 
                retValFsMefUniType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefUniType (UINT4 u4FsMefContextId,
                    INT4 i4FsEvcIndex,
                    INT4 i4IfIndex, INT4 *pi4RetValFsMefUniType)
{
    tMefUniList        *pMefUniListEntry = NULL;

    pMefUniListEntry =
        MefGetUniListEntry (u4FsMefContextId, i4FsEvcIndex, i4IfIndex);
    if (pMefUniListEntry == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_LIST_NOT_EXIST);
        return SNMP_FAILURE;
    }

    *pi4RetValFsMefUniType = pMefUniListEntry->UniType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMefUniListRowStatus
 Input       :  The Indices
                FsMefContextId
                FsEvcIndex
                IfIndex

                The Object 
                retValFsMefUniListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMefUniListRowStatus (UINT4 u4FsMefContextId,
                             INT4 i4FsEvcIndex,
                             INT4 i4IfIndex,
                             INT4 *pi4RetValFsMefUniListRowStatus)
{
    tMefUniList        *pMefUniListEntry = NULL;

    pMefUniListEntry =
        MefGetUniListEntry (u4FsMefContextId, i4FsEvcIndex, i4IfIndex);
    if (pMefUniListEntry == NULL)
    {
        CLI_SET_ERR (MEF_CLI_UNI_LIST_NOT_EXIST);
        return SNMP_FAILURE;
    }
    *pi4RetValFsMefUniListRowStatus = pMefUniListEntry->i4RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMefUniType
 Input       :  The Indices
                FsMefContextId
                FsEvcIndex
                IfIndex

                The Object 
                setValFsMefUniType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefUniType (UINT4 u4FsMefContextId,
                    INT4 i4FsEvcIndex,
                    INT4 i4IfIndex, INT4 i4SetValFsMefUniType)
{
    tMefUniList        *pMefUniListInfo = NULL;

    pMefUniListInfo =
        MefGetUniListEntry (u4FsMefContextId, i4FsEvcIndex, i4IfIndex);
    if (pMefUniListInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMefUniListInfo->UniType = i4SetValFsMefUniType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMefUniListRowStatus
 Input       :  The Indices
                FsMefContextId
                FsEvcIndex
                IfIndex

                The Object 
                setValFsMefUniListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMefUniListRowStatus (UINT4 u4FsMefContextId,
                             INT4 i4FsEvcIndex,
                             INT4 i4IfIndex, INT4 i4SetValFsMefUniListRowStatus)
{
    tMefUniList        *pMefUniListInfo = NULL;

    pMefUniListInfo =
        MefGetUniListEntry (u4FsMefContextId, i4FsEvcIndex, i4IfIndex);

    switch (i4SetValFsMefUniListRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pMefUniListInfo != NULL)
            {
                return SNMP_FAILURE;
            }
            pMefUniListInfo =
                (tMefUniList *) MemAllocMemBlk (MEF_UNI_LIST_POOL_ID);
            if (pMefUniListInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (pMefUniListInfo, 0, sizeof (tMefUniList));

            pMefUniListInfo->u4ContextId = u4FsMefContextId;
            pMefUniListInfo->i4EvcIndex = i4FsEvcIndex;
            pMefUniListInfo->i4IfIndex = i4IfIndex;
            pMefUniListInfo->UniType = MEF_UNI_ROOT;

            if (MefAddUniListEntry (pMefUniListInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            pMefUniListInfo->i4RowStatus = NOT_READY;
            break;

        case ACTIVE:
            if (pMefUniListInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            pMefUniListInfo->i4RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            if (pMefUniListInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            pMefUniListInfo->i4RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            if (pMefUniListInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            if (MefDeleteUniListEntry (pMefUniListInfo) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMefUniType
 Input       :  The Indices
                FsMefContextId
                FsEvcIndex
                IfIndex

                The Object 
                testValFsMefUniType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefUniType (UINT4 *pu4ErrorCode,
                       UINT4 u4FsMefContextId,
                       INT4 i4FsEvcIndex,
                       INT4 i4IfIndex, INT4 i4TestValFsMefUniType)
{
    tMefUniList        *pMefUniListInfo = NULL;
    INT4                i4EvcType = 0;

    pMefUniListInfo =
        MefGetUniListEntry (u4FsMefContextId, i4FsEvcIndex, i4IfIndex);
    if (pMefUniListInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pMefUniListInfo->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMefUniType != MEF_UNI_ROOT) &&
        (i4TestValFsMefUniType != MEF_UNI_LEAF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    nmhGetFsEvcType (u4FsMefContextId, i4FsEvcIndex, &i4EvcType);
    if (i4TestValFsMefUniType == MEF_UNI_LEAF &&
        (i4EvcType == MEF_POINT_TO_POINT
         || i4EvcType == MEF_MULTIPOINT_TO_MULTIPOINT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#if 0
    /* for E-Tree scenario, atleast one Root is must */
    else if (i4TestValFsMefUniType == MEF_UNI_LEAF
             && i4EvcType == MEF_ROOTED_MULTIPOINT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMefUniListRowStatus
 Input       :  The Indices
                FsMefContextId
                FsEvcIndex
                IfIndex

                The Object 
                testValFsMefUniListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMefUniListRowStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMefContextId,
                                INT4 i4FsEvcIndex,
                                INT4 i4IfIndex,
                                INT4 i4TestValFsMefUniListRowStatus)
{
    tMefUniList        *pMefUniListEntry = NULL;

    pMefUniListEntry =
        MefGetUniListEntry (u4FsMefContextId, i4FsEvcIndex, i4IfIndex);

    if (VcmIsL2VcExist (u4FsMefContextId) != VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    switch (i4TestValFsMefUniListRowStatus)
    {
        case CREATE_AND_WAIT:

            if (pMefUniListEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case DESTROY:
        case NOT_IN_SERVICE:

            if (pMefUniListEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMefUniListTable
 Input       :  The Indices
                FsMefContextId
                FsEvcIndex
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMefUniListTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsEvcStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsEvcStatsTable
 Input       :  The Indices
                FsEvcStatsContextId
                FsEvcStatsId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsEvcStatsTable(INT4 i4FsEvcStatsContextId , INT4 i4FsEvcStatsId)
{
    if ( nmhValidateIndexInstanceFsEvcTable(i4FsEvcStatsContextId, 
                                            i4FsEvcStatsId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsEvcStatsTable
 Input       :  The Indices
                FsEvcStatsContextId
                FsEvcStatsId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsEvcStatsTable(INT4 *pi4FsEvcStatsContextId , INT4 *pi4FsEvcStatsId)
{
    if (nmhGetFirstIndexFsEvcTable (pi4FsEvcStatsContextId, 
                                    pi4FsEvcStatsId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsEvcStatsTable
 Input       :  The Indices
                FsEvcStatsContextId
                nextFsEvcStatsContextId
                FsEvcStatsId
                nextFsEvcStatsId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsEvcStatsTable(INT4 i4FsEvcStatsContextId ,INT4 *pi4NextFsEvcStatsContextId  , INT4 i4FsEvcStatsId ,INT4 *pi4NextFsEvcStatsId )
{
    if(nmhGetNextIndexFsEvcTable (i4FsEvcStatsContextId, pi4NextFsEvcStatsContextId, 
                                  i4FsEvcStatsId, pi4NextFsEvcStatsId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsEvcStatsRxFrames
 Input       :  The Indices
                FsEvcStatsContextId
                FsEvcStatsId

                The Object 
                retValFsEvcStatsRxFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsEvcStatsRxFrames(INT4 i4FsEvcStatsContextId , INT4 i4FsEvcStatsId , UINT4 *pu4RetValFsEvcStatsRxFrames)
{
    VLAN_LOCK ();
    VlanSelectContext (i4FsEvcStatsContextId);
    VlanHwGetVlanStats (i4FsEvcStatsContextId,
            			i4FsEvcStatsId,
			            EVC_STAT_EVC_IN_FRAMES,
	   	                pu4RetValFsEvcStatsRxFrames);
    VlanReleaseContext();
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsEvcStatsRxBytes
 Input       :  The Indices
                FsEvcStatsContextId
                FsEvcStatsId

                The Object 
                retValFsEvcStatsRxBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsEvcStatsRxBytes(INT4 i4FsEvcStatsContextId , INT4 i4FsEvcStatsId , UINT4 *pu4RetValFsEvcStatsRxBytes)
{
    VLAN_LOCK ();
    VlanSelectContext (i4FsEvcStatsContextId);
    VlanHwGetVlanStats (i4FsEvcStatsContextId,
                        i4FsEvcStatsId,
                        EVC_STAT_EVC_OUT_FRAMES,
                        pu4RetValFsEvcStatsRxBytes);
    VlanReleaseContext();
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsEvcStatsTxFrames
 Input       :  The Indices
                FsEvcStatsContextId
                FsEvcStatsId

                The Object 
                retValFsEvcStatsTxFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsEvcStatsTxFrames(INT4 i4FsEvcStatsContextId , INT4 i4FsEvcStatsId , UINT4 *pu4RetValFsEvcStatsTxFrames)
{
    VLAN_LOCK ();
    VlanSelectContext (i4FsEvcStatsContextId);
    VlanHwGetVlanStats (i4FsEvcStatsContextId,
                        i4FsEvcStatsId,
                        EVC_STAT_EVC_OUT_FRAMES,
                        pu4RetValFsEvcStatsTxFrames);
    VlanReleaseContext();
    VLAN_UNLOCK ();

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsEvcStatsTxBytes
 Input       :  The Indices
                FsEvcStatsContextId
                FsEvcStatsId

                The Object 
                retValFsEvcStatsTxBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsEvcStatsTxBytes(INT4 i4FsEvcStatsContextId , INT4 i4FsEvcStatsId , UINT4 *pu4RetValFsEvcStatsTxBytes)
{
    VLAN_LOCK ();
    VlanSelectContext (i4FsEvcStatsContextId);
    VlanHwGetVlanStats (i4FsEvcStatsContextId,
                        i4FsEvcStatsId,
                        EVC_STAT_EVC_OUT_BYTES,
                        pu4RetValFsEvcStatsTxBytes);
    VlanReleaseContext();
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsEvcStatsDiscardFrames
 Input       :  The Indices
                FsEvcStatsContextId
                FsEvcStatsId

                The Object 
                retValFsEvcStatsDiscardFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsEvcStatsDiscardFrames(INT4 i4FsEvcStatsContextId , INT4 i4FsEvcStatsId , UINT4 *pu4RetValFsEvcStatsDiscardFrames)
{
    VLAN_LOCK ();
    VlanSelectContext (i4FsEvcStatsContextId);
    VlanHwGetVlanStats (i4FsEvcStatsContextId,
                        i4FsEvcStatsId,
                        EVC_STAT_EVC_DISCARD_FRAMES,
                        pu4RetValFsEvcStatsDiscardFrames);
    VlanReleaseContext();
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsEvcStatsDiscardBytes
 Input       :  The Indices
                FsEvcStatsContextId
                FsEvcStatsId

                The Object 
                retValFsEvcStatsDiscardBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsEvcStatsDiscardBytes(INT4 i4FsEvcStatsContextId , INT4 i4FsEvcStatsId , UINT4 *pu4RetValFsEvcStatsDiscardBytes)
{
    VLAN_LOCK ();
    VlanSelectContext (i4FsEvcStatsContextId);
    VlanHwGetVlanStats (i4FsEvcStatsContextId,
                        i4FsEvcStatsId,
                        EVC_STAT_EVC_DISCARD_BYTES,
                        pu4RetValFsEvcStatsDiscardBytes);
    VlanReleaseContext();
    VLAN_UNLOCK ();
    return SNMP_SUCCESS;
}
