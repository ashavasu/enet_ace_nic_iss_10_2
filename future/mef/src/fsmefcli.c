/********************************************************************
* Copyright (C) 2012 Aricent Inc . All Rights Reserved
*
* $Id: fsmefcli.c,v 1.35 2015/07/18 10:04:45 siva Exp $
*
* Description:
* *********************************************************************/

#ifndef __FSMEFCLI_C__
#define __FSMEFCLI_C__

#include "fsmefinc.h"
#include "fsmefmcli.h"
#include "cfacli.h"
#include "vlancli.h"
#include "vlnpbcli.h"
#include "ecfmcli.h"
#include "mplscli.h"
#include "qosxcli.h"
#include "isscli.h"
#include "vcmcli.h"

/***************************************************************************
 * FUNCTION NAME    : cli_process_mef_cmd
 *
 * DESCRIPTION      : This function is exported to CLI module to handle the
 *                    MEF CLI commands to take the corresponding action.
 *                    Only SNMP Low level routines are called from CLI.
 *
 * INPUT            : Variable arguments
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
cli_process_mef_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *pau4Args[MEF_CLI_MAX_ARGS];
    UINT4               u4ContextId = CLI_GET_CXT_ID ();
    UINT4               u4ErrCode = 0;
    INT4                i4EvcType = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4TransMode = 0;
    UINT1               u1UniType = 0;
    INT1                i1ArgNo = 0;
    INT4                i4Priority = 0;
    INT4                i4Dscp = 0;
    INT4                i4UniListType = 0;

    CliRegisterLock (CliHandle, MefApiLock, MefApiUnLock);

    MefApiLock ();

    if (u4ContextId == VCM_INVALID_VC)
    {
        u4ContextId = VCM_DEFAULT_CONTEXT;
    }

    va_start (ap, u4Command);

    i4IfIndex = va_arg (ap, INT4);

    if (i4IfIndex == 0)
    {
        i4IfIndex = CLI_GET_IFINDEX ();
    }

    while (1)
    {
        pau4Args[i1ArgNo++] = va_arg (ap, UINT4 *);

        if (i1ArgNo == MEF_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);
    switch (u4Command)
    {
        case MEF_CLI_TRANSMODE:

            if (pau4Args[0] != NULL)
            {
                i4TransMode = MEF_TRANS_MODE_PB;
            }
            else if (pau4Args[1] != NULL)
            {
                i4TransMode = MEF_TRANS_MODE_MPLS;
            }
            i4RetVal = MefCliSetTranportMode (CliHandle, u4ContextId,
                                              i4TransMode);
            break;

        case MEF_CLI_UNI_ID:

            i4RetVal = MefCliSetUniId (CliHandle, i4IfIndex,
                                       (UINT1 *) pau4Args[0]);
            break;

        case MEF_CLI_NO_UNI_ID:

            i4RetVal = MefCliSetUniId (CliHandle, i4IfIndex, NULL);
            break;

        case MEF_CLI_UNI_TYPE:

            if (pau4Args[0] != NULL)
            {
                u1UniType = u1UniType | MEF_MULTIPLEX_BITMASK;
            }

            if (pau4Args[1] != NULL)
            {
                u1UniType = u1UniType | MEF_BUNDLE_BITMASK;
            }

            if (pau4Args[2] != NULL)
            {
                u1UniType = u1UniType | MEF_ALL_TO_ONE_BUNDLE_BITMASK;
            }

            i4RetVal = MefCliSetUniType (CliHandle, i4IfIndex, u1UniType);
            break;

        case MEF_CLI_NO_UNI_TYPE:

            u1UniType = MEF_DEFAULT_UNI_TYPE;
            i4RetVal = MefCliSetUniType (CliHandle, i4IfIndex, u1UniType);
            break;

        case MEF_CLI_DEF_UNI_CEVLAN:

            i4RetVal = MefCliSetUniDefaultCeVlan (CliHandle, i4IfIndex,
                                                  (UINT1 *) (pau4Args[0]));
            break;

        case MEF_CLI_UNI_L2CP:

            i4RetVal = MefCliSetUniL2cpDisposition (CliHandle, i4IfIndex,
                                                    CLI_PTR_TO_I4 (pau4Args[0]),
                                                    CLI_PTR_TO_I4 (pau4Args
                                                                   [1]));
            break;

        case MEF_CLI_UNI_L2CP_OVERRIDE:

            i4RetVal =
                MefCliSetUniL2cpOverride (CliHandle, i4IfIndex,
                                          CLI_PTR_TO_U4 (pau4Args[0]));

            break;

        case MEF_CLI_EVC_L2CP:

            i4RetVal = MefCliSetEvcL2cpStatus (CliHandle, u4ContextId,
                                               CLI_PTR_TO_I4 (pau4Args[0]),
                                               CLI_PTR_TO_I4 (pau4Args[1]),
                                               CLI_PTR_TO_I4 (pau4Args[2]));
            break;
        case MEF_CLI_EVC_ID:

            i4RetVal = MefCliSetEvcId (CliHandle, u4ContextId,
                                       CLI_PTR_TO_I4 (pau4Args[0]),
                                       (UINT1 *) pau4Args[1]);
            break;

        case MEF_CLI_NO_EVC_ID:

            i4RetVal = MefCliSetEvcId (CliHandle, u4ContextId,
                                       CLI_PTR_TO_I4 (pau4Args[0]), NULL);
            break;

        case MEF_CLI_EVC_TYPE:
            if (pau4Args[1] != NULL)
            {
                i4EvcType = MEF_POINT_TO_POINT;
            }
            if (pau4Args[2] != NULL)
            {
                i4EvcType = MEF_MULTIPOINT_TO_MULTIPOINT;
            }
            if (pau4Args[3] != NULL)
            {
                i4EvcType = MEF_ROOTED_MULTIPOINT;
            }

            i4RetVal = MefCliSetEvcType (CliHandle, u4ContextId,
                                         CLI_PTR_TO_I4 (pau4Args[0]),
                                         i4EvcType);
            break;

        case MEF_CLI_EVC_PRESERVE:

            if (pau4Args[1] != NULL)
            {
                i4RetVal = MefCliSetEvcVlanIdPreserve (CliHandle, u4ContextId,
                                                       CLI_PTR_TO_I4 (pau4Args
                                                                      [0]),
                                                       MEF_ENABLED);
            }

            if (pau4Args[2] != NULL)
            {
                i4RetVal = MefCliSetEvcVlanCosPreserve (CliHandle, u4ContextId,
                                                        CLI_PTR_TO_I4 (pau4Args
                                                                       [0]),
                                                        MEF_ENABLED);
            }
            break;

        case MEF_CLI_EVC_NO_PRESERVE:

            if (pau4Args[1] != NULL)
            {
                i4RetVal = MefCliSetEvcVlanIdPreserve (CliHandle, u4ContextId,
                                                       CLI_PTR_TO_I4 (pau4Args
                                                                      [0]),
                                                       MEF_DISABLED);
            }

            if (pau4Args[2] != NULL)
            {
                i4RetVal = MefCliSetEvcVlanCosPreserve (CliHandle, u4ContextId,
                                                        CLI_PTR_TO_I4 (pau4Args
                                                                       [0]),
                                                        MEF_DISABLED);
            }
            break;

        case MEF_CLI_EVC_LOOPBACK:

            if (pau4Args[1] != NULL)
            {
                i4RetVal = MefCliSetEvcLoopbackStatus (CliHandle, u4ContextId,
                                                       CLI_PTR_TO_I4 (pau4Args
                                                                      [0]),
                                                       MEF_ENABLED);
            }

            if (pau4Args[2] != NULL)
            {
                i4RetVal = MefCliSetEvcLoopbackStatus (CliHandle, u4ContextId,
                                                       CLI_PTR_TO_I4 (pau4Args
                                                                      [0]),
                                                       MEF_DISABLED);
            }
            break;

        case MEF_CLI_CEVLAN_EVC_MAP:

            i4RetVal = MefCliMapCeVlanEvcInfo (CliHandle, i4IfIndex,
                                               pau4Args[0], pau4Args[1],
                                               (UINT1 *) pau4Args[2]);
            break;

        case MEF_CLI_NO_CEVLAN_EVC_MAP:

            i4RetVal = MefCliMapCeVlanEvcInfo (CliHandle, i4IfIndex,
                                               pau4Args[0], NULL, NULL);
            break;

        case MEF_CLI_FILTER_PRIORITY:
            i4Dscp = -1;
            i4RetVal = MefCliConfFilterInfo (CliHandle, pau4Args[0], i4IfIndex,
                                             pau4Args[1], (INT4 *) pau4Args[2],
                                             &i4Dscp, pau4Args[3], pau4Args[4]);
            break;
        case MEF_CLI_FILTER_DSCP:
            i4Priority = -1;
            i4RetVal = MefCliConfFilterInfo (CliHandle, pau4Args[0], i4IfIndex,
                                             pau4Args[1], &i4Priority,
                                             (INT4 *) pau4Args[2], pau4Args[3],
                                             pau4Args[4]);
            break;

        case MEF_CLI_NO_FILTER:
            i4RetVal = MefCliConfFilterInfo (CliHandle, pau4Args[0], i4IfIndex,
                                             pau4Args[1], (INT4 *) pau4Args[2],
                                             (INT4 *) pau4Args[3], pau4Args[4],
                                             pau4Args[5]);
            break;

        case MEF_CLI_CLASSMAP:
            i4RetVal = MefCliConfClassMapInfo (CliHandle, pau4Args[0],
                                               pau4Args[1], pau4Args[2]);
            break;

        case MEF_CLI_NO_CLASSMAP:
            i4RetVal = MefCliConfClassMapInfo (CliHandle, pau4Args[0],
                                               NULL, NULL);
            break;

        case MEF_CLI_CLASS:
        case MEF_CLI_NO_CLASS:
            i4RetVal = MefCliConfClassInfo (CliHandle, pau4Args[0], u4Command);
            break;

        case MEF_CLI_METER:
            i4RetVal = MefCliConfMeterInfo (CliHandle, pau4Args[0],
                                            pau4Args[1], pau4Args[2],
                                            pau4Args[3], pau4Args[4],
                                            pau4Args[5], pau4Args[6]);
            break;

        case MEF_CLI_NO_METER:
            i4RetVal = MefCliConfMeterInfo (CliHandle, pau4Args[0],
                                            NULL, NULL, NULL, NULL, NULL, NULL);
            break;

        case MEF_CLI_POLICY_MAP:
            i4RetVal = MefCliConfPolicyMapInfo (CliHandle, pau4Args[0],
                                                pau4Args[1], pau4Args[2]);
            break;

        case MEF_CLI_NO_POLICY_MAP:
            i4RetVal = MefCliConfPolicyMapInfo (CliHandle, pau4Args[0],
                                                NULL, NULL);
            break;

        case MEF_CLI_ETREE:
            i4RetVal = MefCliConfETreeInfo (CliHandle, pau4Args[0],
                                            pau4Args[1], pau4Args[2],
                                            pau4Args[3], pau4Args[4],
                                            pau4Args[5], pau4Args[6]);
            break;

        case MEF_CLI_NO_ETREE:
            i4RetVal = MefCliRemoveETreeInfo (CliHandle, pau4Args[0],
                                              pau4Args[1], pau4Args[2]);
            break;
        case MEF_CLI_EVC_FILTER:
            i4RetVal = MefCliEvcFilterConfig (CliHandle, u4ContextId,
                                              CLI_PTR_TO_I4 (pau4Args[0]),
                                              pau4Args[1], pau4Args[2],
                                              (UINT1 *) pau4Args[3],
                                              pau4Args[4], pau4Args[5]);
            break;

        case MEF_CLI_NO_EVC_FILTER:
            i4RetVal = MefCliEvcFilterConfig (CliHandle, u4ContextId,
                                              CLI_PTR_TO_I4 (pau4Args[0]),
                                              pau4Args[1], NULL,
                                              NULL, NULL, NULL);
            break;

        case MEF_CLI_UNILIST:
            /* apu4Args[0] = Evc Id    */
            /* apu4Args[1] = IfIndex   */
            /* apu4Args[2] = Root      */
            /* apu4Args[3] = Leaf      */
            if (pau4Args[2] != NULL)
            {
                i4UniListType = MEF_UNI_ROOT;
            }
            if (pau4Args[3] != NULL)
            {
                i4UniListType = MEF_UNI_LEAF;
            }
            i4RetVal = MefCliUniListConfig (CliHandle, u4ContextId,
                                            CLI_PTR_TO_I4 (pau4Args[0]),
                                            CLI_PTR_TO_U4 (pau4Args[1]),
                                            i4UniListType);
            break;

        case MEF_CLI_NO_UNILIST:
            i4UniListType = -1;
            i4RetVal = MefCliUniListConfig (CliHandle, u4ContextId,
                                            CLI_PTR_TO_I4 (pau4Args[0]),
                                            CLI_PTR_TO_U4 (pau4Args[1]),
                                            i4UniListType);
            break;

        default:
            CliPrintf (CliHandle, "%% Invalid MEF Command\r\n");
            i4RetVal = CLI_FAILURE;
    }

    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < MEF_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s", gapc1MefCliErrString[u4ErrCode]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_CFA) &&
                 (u4ErrCode < CLI_CFA_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       CfaCliErrString[CLI_ERR_OFFSET_CFA (u4ErrCode)]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_VLAN) &&
                 (u4ErrCode < CLI_VLAN_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       VlanCliErrString[CLI_ERR_OFFSET_VLAN (u4ErrCode)]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_VLAN_PB) &&
                 (u4ErrCode < CLI_PB_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       PbCliErrString[CLI_ERR_OFFSET_VLAN_PB (u4ErrCode)]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_ECFM) &&
                 (u4ErrCode < CLI_ECFM_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       gaEcfmCliErrString[CLI_ERR_OFFSET_ECFM (u4ErrCode)]);
        }
#ifdef MPLS_WANTED
        else if ((u4ErrCode >= CLI_ERR_START_ID_MPLS) &&
                 (u4ErrCode < MPLS_CLI_MAX_ERROR_MSGS))
        {
            CliPrintf (CliHandle, "%% %s",
                       MPLS_CLI_ERROR_MSGS[CLI_ERR_OFFSET_MPLS (u4ErrCode)]);
        }
#endif
        else if ((u4ErrCode >= CLI_ERR_START_ID_QOS) &&
                 (u4ErrCode < QOS_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       gapi1QoSCliErrString[CLI_ERR_OFFSET_QOS (u4ErrCode)]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_ISS) &&
                 (u4ErrCode < CLI_ISS_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       IssCliErrString[CLI_ERR_OFFSET_ISS (u4ErrCode)]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_VCM) &&
                 (u4ErrCode < CLI_VCM_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       VcmCliErrString[CLI_ERR_OFFSET_VCM (u4ErrCode)]);
        }
        else
        {
            CliPrintf (CliHandle, "%% Fatal Error. Command failed \r\n");
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetVal);

    MefApiUnLock ();
    CliUnRegisterLock (CliHandle);

    return i4RetVal;
}

INT4
MefCliSetTranportMode (tCliHandle CliHandle, UINT4 u4ContextId,
                       INT4 i4TransMode)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsMefTransmode (&u4ErrorCode, u4ContextId, i4TransMode)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefTransmode (u4ContextId, i4TransMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliSetUniId (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pau1UniId)
{
    tSNMP_OCTET_STRING_TYPE UniId;
    UINT4               u4ErrorCode = 0;
    UINT4               u4FsMefContextId = 0;
    UINT4 		u4CliError =0;
    UINT1               au1UniId[UNI_ID_MAX_LENGTH];
    INT4                i4RowStatus = 0;
    INT1                i1RetVal = CLI_FAILURE;
    INT4                i4BrgMode = 0;

    MEMSET (au1UniId, 0, UNI_ID_MAX_LENGTH);

    UniId.pu1_OctetList = au1UniId;
    UniId.i4_Length = UNI_ID_MAX_LENGTH;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsUniRowStatus (i4IfIndex, &i4RowStatus) == SNMP_FAILURE)
    {
        if (pau1UniId == NULL)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsUniRowStatus (&u4ErrorCode, i4IfIndex, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsUniRowStatus (i4IfIndex, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (pau1UniId == NULL)
        {
            if (nmhSetFsUniRowStatus (i4IfIndex, DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            
            VLAN_LOCK ();
            if (nmhGetFsMIVlanBridgeMode ((INT4) u4FsMefContextId, &i4BrgMode)
                                           == SNMP_FAILURE)
            {
                VLAN_UNLOCK ();
                return OSIX_FAILURE;
            }
            VLAN_UNLOCK ();

            if (i4BrgMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
            {
                if (nmhTestv2IfMainBrgPortType (&u4ErrorCode, i4IfIndex,
                                                CFA_PROVIDER_NETWORK_PORT) ==
                                                SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }

                if (CfaSetIfMainBrgPortType (i4IfIndex, CFA_PROVIDER_NETWORK_PORT)
                                             == SNMP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
            }
            return CLI_SUCCESS;
        }

        if (nmhTestv2FsUniRowStatus (&u4ErrorCode, i4IfIndex, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsUniRowStatus (i4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (pau1UniId == NULL)
    {
        UniId.pu1_OctetList = au1UniId;
        UniId.i4_Length = 0;
    }
    else
    {
        UniId.pu1_OctetList = pau1UniId;
        UniId.i4_Length = STRLEN (pau1UniId);
    }

    if (nmhTestv2FsUniId (&u4ErrorCode, i4IfIndex, &UniId) != SNMP_FAILURE)
    {
        if (nmhSetFsUniId (i4IfIndex, &UniId) == SNMP_SUCCESS)
        {
            i1RetVal = CLI_SUCCESS;
        }
    } 
    else 
    {
	/*If nmhTestv2FsUniId failed, check if it is due to MEF_CLI_UNI_ID_EXIST error. 
	 *If so, no need to destroy the UNI, if there is a UNI already exists on this port.*/
	if ((CLI_GET_ERR (&u4CliError) == CLI_SUCCESS) && (u4CliError == MEF_CLI_UNI_ID_EXIST))
	{
	   MEMSET (au1UniId, 0, UNI_ID_MAX_LENGTH);
	   UniId.pu1_OctetList = au1UniId;
           UniId.i4_Length = 0;
	   if (nmhGetFsUniId (i4IfIndex, &UniId) == SNMP_SUCCESS)
	   {
              if (nmhTestv2FsUniRowStatus (&u4ErrorCode, i4IfIndex, ACTIVE)
                  == SNMP_FAILURE)
              {
                  nmhSetFsUniRowStatus (i4IfIndex, DESTROY);
                  return CLI_FAILURE;
              }

              if (nmhSetFsUniRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
              {
                  nmhSetFsUniRowStatus (i4IfIndex, DESTROY);
                  return CLI_FAILURE;
              }
    	      return CLI_FAILURE;
	   }
	}
    }

    if (i1RetVal == CLI_SUCCESS)
    {
        if (nmhTestv2FsUniRowStatus (&u4ErrorCode, i4IfIndex, ACTIVE)
            == SNMP_FAILURE)
        {
            nmhSetFsUniRowStatus (i4IfIndex, DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetFsUniRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFsUniRowStatus (i4IfIndex, DESTROY);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsUniRowStatus (&u4ErrorCode, i4IfIndex, DESTROY)
            != SNMP_FAILURE)
        {
            nmhSetFsUniRowStatus (i4IfIndex, DESTROY);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

INT4
MefCliSetUniType (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1UniType)
{

    tSNMP_OCTET_STRING_TYPE UniType;
    INT4                i4RowStatus = 0;
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    UniType.pu1_OctetList = &u1UniType;
    UniType.i4_Length = sizeof (UINT1);

    if (nmhGetFsUniRowStatus (i4IfIndex, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return CLI_FAILURE;
    }
    else
    {
        if (nmhSetFsUniRowStatus (i4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsUniServiceMultiplexingBundling (&u4ErrorCode, i4IfIndex,
                                                   &UniType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsUniServiceMultiplexingBundling (i4IfIndex, &UniType)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsUniRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliSetUniDefaultCeVlan (tCliHandle CliHandle, INT4 i4IfIndex,
                           UINT1 *pi1DefCeVlan)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4DefCeVlan = 0;

    UNUSED_PARAM (CliHandle);

    MEMCPY (&i4DefCeVlan, pi1DefCeVlan, sizeof (INT4));

    if (nmhGetFsUniRowStatus (i4IfIndex, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return CLI_FAILURE;
    }
    else
    {
        if (nmhSetFsUniRowStatus (i4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsUniCVlanId (&u4ErrorCode, i4IfIndex, i4DefCeVlan)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsUniCVlanId (i4IfIndex, i4DefCeVlan) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsUniRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliSetUniL2cpDisposition (tCliHandle CliHandle, INT4 i4IfIndex,
                             INT4 i4Protocol, INT4 i4Action)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Dispositon = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = CLI_FAILURE;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsUniRowStatus (i4IfIndex, &i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    else
    {
        if (nmhSetFsUniRowStatus (i4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    switch (i4Action)
    {
        case MEF_L2CP_PROTO_ACTION_DISCARD:
            i4Dispositon = MEF_TUNNEL_PROTOCOL_DISCARD;
            break;

        case MEF_L2CP_PROTO_ACTION_PEER:
            i4Dispositon = MEF_TUNNEL_PROTOCOL_PEER;
            break;

        case MEF_L2CP_PROTO_ACTION_TUNNEL:
            i4Dispositon = MEF_TUNNEL_PROTOCOL_TUNNEL;
            break;
    }

    switch (i4Protocol)
    {
        case MEF_L2CP_PROTOCOL_DOT1X:

            if (nmhTestv2FsUniL2CPDot1x (&u4ErrorCode, i4IfIndex, i4Dispositon)
                != SNMP_FAILURE)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_LACP:

            if (nmhTestv2FsUniL2CPLacp (&u4ErrorCode, i4IfIndex, i4Dispositon)
                != SNMP_FAILURE)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_STP:

            if (nmhTestv2FsUniL2CPStp (&u4ErrorCode, i4IfIndex, i4Dispositon)
                != SNMP_FAILURE)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_GVRP:

            if (nmhTestv2FsUniL2CPGvrp (&u4ErrorCode, i4IfIndex, i4Dispositon)
                != SNMP_FAILURE)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_GMRP:

            if (nmhTestv2FsUniL2CPGmrp (&u4ErrorCode, i4IfIndex, i4Dispositon)
                != SNMP_FAILURE)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_MVRP:

            if (nmhTestv2FsUniL2CPMvrp (&u4ErrorCode, i4IfIndex, i4Dispositon)
                != SNMP_FAILURE)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_MMRP:

            if (nmhTestv2FsUniL2CPMmrp (&u4ErrorCode, i4IfIndex, i4Dispositon)
                != SNMP_FAILURE)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_ELMI:

            if (nmhTestv2FsUniL2CPElmi (&u4ErrorCode, i4IfIndex, i4Dispositon)
                != SNMP_FAILURE)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_LLDP:

            if (nmhTestv2FsUniL2CPLldp (&u4ErrorCode, i4IfIndex, i4Dispositon)
                != SNMP_FAILURE)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_ECFM:

            if (nmhTestv2FsUniL2CPEcfm (&u4ErrorCode, i4IfIndex, i4Dispositon)
                != SNMP_FAILURE)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;
        case MEF_L2CP_PROTOCOL_EOAM:

            if (nmhTestv2FsUniL2CPEoam (&u4ErrorCode, i4IfIndex, i4Dispositon)
                != SNMP_FAILURE)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;
    }

    if (i4RetVal == CLI_FAILURE)
    {
        nmhSetFsUniRowStatus (i4IfIndex, ACTIVE);
        return CLI_FAILURE;
    }

    switch (i4Protocol)
    {
        case MEF_L2CP_PROTOCOL_DOT1X:

            if (nmhSetFsUniL2CPDot1x (i4IfIndex, i4Dispositon) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_LACP:

            if (nmhSetFsUniL2CPLacp (i4IfIndex, i4Dispositon) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_STP:

            if (nmhSetFsUniL2CPStp (i4IfIndex, i4Dispositon) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_GVRP:

            if (nmhSetFsUniL2CPGvrp (i4IfIndex, i4Dispositon) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_GMRP:

            if (nmhSetFsUniL2CPGmrp (i4IfIndex, i4Dispositon) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_MVRP:

            if (nmhSetFsUniL2CPMvrp (i4IfIndex, i4Dispositon) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_MMRP:

            if (nmhSetFsUniL2CPMmrp (i4IfIndex, i4Dispositon) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_ELMI:

            if (nmhSetFsUniL2CPElmi (i4IfIndex, i4Dispositon) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_LLDP:

            if (nmhSetFsUniL2CPLldp (i4IfIndex, i4Dispositon) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;

        case MEF_L2CP_PROTOCOL_ECFM:

            if (nmhSetFsUniL2CPEcfm (i4IfIndex, i4Dispositon) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;
        case MEF_L2CP_PROTOCOL_EOAM:

            if (nmhSetFsUniL2CPEoam (i4IfIndex, i4Dispositon) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
            break;
    }

    if ((nmhSetFsUniRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE) ||
        i4RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliSetUniL2cpOverride (tCliHandle CliHandle, INT4 i4IfIndex,
                          UINT4 u4OverrideOption)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = CLI_SUCCESS;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFsUniRowStatus (i4IfIndex, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_UNI_NOT_EXIST);
        return CLI_FAILURE;
    }

    else
    {
        if (nmhTestv2FsUniRowStatus (&u4ErrorCode, i4IfIndex, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsUniRowStatus (i4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsUniL2CPOverrideOption
        (&u4ErrorCode, i4IfIndex, u4OverrideOption) == SNMP_SUCCESS)
    {
        if (nmhSetFsUniL2CPOverrideOption (i4IfIndex, u4OverrideOption) ==
            SNMP_FAILURE)
        {
            i4RetVal = CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsUniRowStatus (&u4ErrorCode, i4IfIndex, ACTIVE) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsUniRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_FAILURE;
    }
    if (nmhTestv2FsUniRowStatus (&u4ErrorCode, i4IfIndex, ACTIVE) ==
        SNMP_FAILURE || i4RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsUniRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliSetEvcL2cpStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                        INT4 i4EvcId, UINT4 u4L2PType, UINT4 u4TunnelStatus)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4EvcBitmask = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = CLI_FAILURE;
    tSNMP_OCTET_STRING_TYPE EvcTunnelProtocolList;
    UNUSED_PARAM (CliHandle);

    if (nmhGetFsEvcRowStatus (u4ContextId, i4EvcId, &i4RowStatus) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return CLI_FAILURE;
    }
    else
    {
        if (nmhTestv2FsEvcRowStatus
            (&u4ErrorCode, u4ContextId, i4EvcId,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsEvcRowStatus (u4ContextId, i4EvcId, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (u4L2PType == MEF_CLI_L2CP_PROTOCOL_GVRP)
    {
        u4EvcBitmask = MEF_EVC_GVRP_BITMASK;
    }
    else if (u4L2PType == MEF_CLI_L2CP_PROTOCOL_GMRP)
    {
        u4EvcBitmask = MEF_EVC_GMRP_BITMASK;
    }
    else if (u4L2PType == MEF_CLI_L2CP_PROTOCOL_IGMP)
    {
        u4EvcBitmask = MEF_EVC_IGMP_BITMASK;
    }
    else if (u4L2PType == MEF_CLI_L2CP_PROTOCOL_MVRP)
    {
        u4EvcBitmask = MEF_EVC_MVRP_BITMASK;
    }
    else if (u4L2PType == MEF_CLI_L2CP_PROTOCOL_MMRP)
    {
        u4EvcBitmask = MEF_EVC_MMRP_BITMASK;
    }
    else
    {
        u4EvcBitmask = MEF_EVC_ECFM_BITMASK;
    }

    EvcTunnelProtocolList.pu1_OctetList = (UINT1 *) &u4EvcBitmask;
    EvcTunnelProtocolList.i4_Length = sizeof (UINT4);

    if (u4TunnelStatus == MEF_L2CP_PROTO_ACTION_TUNNEL)
    {
        if (nmhTestv2FsEvcL2CPTunnel
            (&u4ErrorCode, u4ContextId, i4EvcId,
             &EvcTunnelProtocolList) == SNMP_SUCCESS)
        {
            i4RetVal = CLI_SUCCESS;
        }
    }
    else if (u4TunnelStatus == MEF_L2CP_PROTO_ACTION_PEER)
    {
        if (nmhTestv2FsEvcL2CPPeer
            (&u4ErrorCode, u4ContextId, i4EvcId,
             &EvcTunnelProtocolList) == SNMP_SUCCESS)
        {
            i4RetVal = CLI_SUCCESS;
        }
    }
    else
    {
        if (nmhTestv2FsEvcL2CPDiscard
            (&u4ErrorCode, u4ContextId, i4EvcId,
             &EvcTunnelProtocolList) == SNMP_SUCCESS)
        {
            i4RetVal = CLI_SUCCESS;
        }
    }

    if (i4RetVal == CLI_FAILURE)
    {
        nmhSetFsEvcRowStatus (u4ContextId, i4EvcId, ACTIVE);
        return CLI_FAILURE;
    }

    if (u4TunnelStatus == MEF_L2CP_PROTO_ACTION_TUNNEL)
    {
        if (nmhSetFsEvcL2CPTunnel (u4ContextId, i4EvcId, &EvcTunnelProtocolList)
            == SNMP_SUCCESS)
        {
            i4RetVal = CLI_SUCCESS;
        }
    }
    else if (u4TunnelStatus == MEF_L2CP_PROTO_ACTION_PEER)
    {
        if (nmhSetFsEvcL2CPPeer (u4ContextId, i4EvcId, &EvcTunnelProtocolList)
            == SNMP_SUCCESS)
        {
            i4RetVal = CLI_SUCCESS;
        }
    }
    else
    {
        if (nmhSetFsEvcL2CPDiscard
            (u4ContextId, i4EvcId, &EvcTunnelProtocolList) == SNMP_SUCCESS)
        {
            i4RetVal = CLI_SUCCESS;
        }
    }

    if ((nmhSetFsEvcRowStatus (u4ContextId, i4EvcId, ACTIVE) == SNMP_FAILURE) ||
        i4RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
MefCliSetEvcId (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Evc,
                UINT1 *pau1EvcId)
{
    tSNMP_OCTET_STRING_TYPE EvcId;
    UINT4               u4ErrorCode = 0;
    UINT1               au1EvcId[EVC_ID_MAX_LENGTH];
    INT4                i4RowStatus = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsEvcRowStatus (u4ContextId, i4Evc, &i4RowStatus) == SNMP_FAILURE)
    {
        if (pau1EvcId == NULL)
        {
            CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
            return CLI_FAILURE;
        }
        else
        {
            if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                         CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        if (pau1EvcId == NULL)
        {
            if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                         DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, DESTROY)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            return CLI_SUCCESS;
        }
        else
        {
            if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                         NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }

    if (pau1EvcId == NULL)
    {
        MEMSET (au1EvcId, 0, EVC_ID_MAX_LENGTH);
        EvcId.pu1_OctetList = au1EvcId;
        EvcId.i4_Length = 0;
    }
    else
    {
        EvcId.pu1_OctetList = pau1EvcId;
        EvcId.i4_Length = STRLEN (pau1EvcId);
    }

    if (nmhTestv2FsEvcId (&u4ErrorCode, u4ContextId, i4Evc, &EvcId)
        == SNMP_FAILURE)
    {
        nmhSetFsEvcRowStatus (u4ContextId, i4Evc, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcId (u4ContextId, i4Evc, &EvcId) == SNMP_FAILURE)
    {
        nmhSetFsEvcRowStatus (u4ContextId, i4Evc, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                 ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsEvcRowStatus (u4ContextId, i4Evc, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsEvcRowStatus (u4ContextId, i4Evc, DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliSetEvcType (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Evc,
                  UINT4 i4EvcType)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4PrevEvcType = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsEvcRowStatus (u4ContextId, i4Evc, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                 NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetFsEvcType (u4ContextId, i4Evc, &i4PrevEvcType);
    if (nmhTestv2FsEvcType (&u4ErrorCode, u4ContextId, i4Evc,
                            i4EvcType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcType (u4ContextId, i4Evc, i4EvcType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                 ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsEvcType (u4ContextId, i4Evc, i4PrevEvcType);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliSetEvcVlanIdPreserve (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Evc,
                            INT4 i4IdPreserve)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsEvcRowStatus (u4ContextId, i4Evc, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                 NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcCVlanIdPreservation (&u4ErrorCode, u4ContextId, i4Evc,
                                           i4IdPreserve) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcCVlanIdPreservation (u4ContextId, i4Evc, i4IdPreserve)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                 ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliSetEvcVlanCosPreserve (tCliHandle CliHandle, UINT4 u4ContextId,
                             INT4 i4Evc, INT4 i4CosPreserve)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsEvcRowStatus (u4ContextId, i4Evc, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                 NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcCVlanCoSPreservation (&u4ErrorCode, u4ContextId, i4Evc,
                                            i4CosPreserve) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcCVlanCoSPreservation (u4ContextId, i4Evc, i4CosPreserve)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                 ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliSetEvcLoopbackStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                            INT4 i4Evc, INT4 i4LoopBackStatus)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhGetFsEvcRowStatus ((INT4) u4ContextId, i4Evc, &i4RowStatus) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, (INT4) u4ContextId, i4Evc,
                                 NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcLoopbackStatus (&u4ErrorCode, (INT4) u4ContextId, i4Evc,
                                      i4LoopBackStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcLoopbackStatus ((INT4) u4ContextId, i4Evc, i4LoopBackStatus)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, (INT4) u4ContextId, i4Evc,
                                 ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcRowStatus ((INT4) u4ContextId, i4Evc, ACTIVE) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliMapCeVlanEvcInfo (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 *pu4CeVlan,
                        UINT4 *pu4Evc, UINT1 *pau1UniEvcId)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CeVlan = 0;
    INT4                i4Evc = 0;
    INT4                i4RetVal = CLI_FAILURE;
    tSNMP_OCTET_STRING_TYPE UniEvcId;
    UINT1               au1UniEvcId[UNI_ID_MAX_LENGTH];

    UNUSED_PARAM (CliHandle);

    MEMCPY (&i4CeVlan, pu4CeVlan, sizeof (INT4));
    if (pu4Evc == NULL)
    {
        if (nmhTestv2FsUniCVlanEvcRowStatus (&u4ErrorCode, i4IfIndex, i4CeVlan,
                                             DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsUniCVlanEvcRowStatus (i4IfIndex, i4CeVlan, DESTROY)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
    MEMCPY (&i4Evc, pu4Evc, sizeof (INT4));

    if (nmhTestv2FsUniCVlanEvcRowStatus (&u4ErrorCode, i4IfIndex, i4CeVlan,
                                         CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsUniCVlanEvcRowStatus (i4IfIndex, i4CeVlan, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsUniCVlanEvcEvcIndex (&u4ErrorCode, i4IfIndex, i4CeVlan,
                                        i4Evc) != SNMP_FAILURE)
    {
        if (nmhSetFsUniCVlanEvcEvcIndex (i4IfIndex, i4CeVlan, i4Evc)
            == SNMP_SUCCESS)
        {
            i4RetVal = CLI_SUCCESS;
        }
    }

    if (pau1UniEvcId == NULL)
    {
        MEMSET (au1UniEvcId, 0, UNI_ID_MAX_LENGTH);
        UniEvcId.pu1_OctetList = au1UniEvcId;
        UniEvcId.i4_Length = 0;
    }
    else
    {
        MEMSET (au1UniEvcId, 0, UNI_ID_MAX_LENGTH);
        UniEvcId.pu1_OctetList = pau1UniEvcId;
        UniEvcId.i4_Length = STRLEN (pau1UniEvcId);
    }

    if (nmhTestv2FsUniEvcId (&u4ErrorCode, i4IfIndex, i4CeVlan, &UniEvcId) !=
        SNMP_FAILURE)
    {
        if (nmhSetFsUniEvcId (i4IfIndex, i4CeVlan, &UniEvcId) == SNMP_SUCCESS)
        {
            i4RetVal = CLI_SUCCESS;
        }
    }
    if (i4RetVal == CLI_SUCCESS)
    {
        if (nmhTestv2FsUniCVlanEvcRowStatus (&u4ErrorCode, i4IfIndex, i4CeVlan,
                                             ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsUniCVlanEvcRowStatus (i4IfIndex, i4CeVlan, ACTIVE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsUniCVlanEvcRowStatus (&u4ErrorCode, i4IfIndex, i4CeVlan,
                                             DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsUniCVlanEvcRowStatus (i4IfIndex, i4CeVlan, DESTROY)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
MefCliConfFilterInfo (tCliHandle CliHandle, UINT4 *pu4Filter, INT4 i4IfIndex,
                      UINT4 *pu4Evc, INT4 *pi4Priority, INT4 *pi4Dscp,
                      UINT4 *pu4InDirection, UINT4 *pu4OutDirection)
{

    UINT4               u4ErrorCode = 0;
    INT4                i4FilterNo = 0;
    INT4                i4Evc = 0;
    INT4                i4Priority = 0;
    INT4                i4Dscp = 0;
    INT4                i4FilterIfIndex = 0;

    MEMCPY (&i4FilterNo, pu4Filter, sizeof (INT4));

    UNUSED_PARAM (CliHandle);

    if ((pu4Evc == NULL) && (pu4InDirection == NULL)
        && (pu4OutDirection == NULL))
    {
        if (nmhGetFsMefFilterIfIndex (i4FilterNo, &i4FilterIfIndex)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (i4IfIndex != i4FilterIfIndex)
        {
            CLI_SET_ERR (MEF_CLI_FILTER_UNI_ERR);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMefFilterStatus (&u4ErrorCode, i4FilterNo, DESTROY)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefFilterStatus (i4FilterNo, DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    if (nmhTestv2FsMefFilterStatus (&u4ErrorCode, i4FilterNo, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefFilterStatus (i4FilterNo, CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefFilterIfIndex (&u4ErrorCode, i4FilterNo, i4IfIndex)
        == SNMP_FAILURE)
    {
        nmhSetFsMefFilterStatus (i4FilterNo, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsMefFilterIfIndex (i4FilterNo, i4IfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (pu4Evc != NULL)
    {
        MEMCPY (&i4Evc, pu4Evc, sizeof (INT4));

        if (nmhTestv2FsMefFilterEvc (&u4ErrorCode, i4FilterNo, i4Evc)
            == SNMP_FAILURE)
        {
            nmhSetFsMefFilterStatus (i4FilterNo, DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetFsMefFilterEvc (i4FilterNo, i4Evc) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if ((pi4Priority != NULL) && (*pi4Priority != -1))
    {
        MEMCPY (&i4Priority, pi4Priority, sizeof (INT4));

        if (nmhTestv2FsMefFilterCVlanPriority (&u4ErrorCode, i4FilterNo,
                                               i4Priority) == SNMP_FAILURE)
        {
            nmhSetFsMefFilterStatus (i4FilterNo, DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetFsMefFilterCVlanPriority (i4FilterNo, i4Priority)
            == SNMP_FAILURE)
        {
            nmhSetFsMefFilterStatus (i4FilterNo, DESTROY);
            return CLI_FAILURE;
        }
    }

    if ((pi4Dscp != NULL) && (*pi4Dscp != -1))
    {
        MEMCPY (&i4Dscp, pi4Dscp, sizeof (INT4));

        if (nmhTestv2FsMefFilterDscp (&u4ErrorCode, i4FilterNo, i4Dscp)
            == SNMP_FAILURE)
        {
            nmhSetFsMefFilterStatus (i4FilterNo, DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetFsMefFilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (pu4OutDirection != NULL)
    {

        if (nmhTestv2FsMefFilterDirection (&u4ErrorCode, i4FilterNo,
                                           MEF_FILTER_DIRECTION_OUT) ==
            SNMP_FAILURE)
        {
            nmhSetFsMefFilterStatus (i4FilterNo, DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetFsMefFilterDirection (i4FilterNo, MEF_FILTER_DIRECTION_OUT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (pu4InDirection != NULL)
    {
        if (nmhTestv2FsMefFilterDirection (&u4ErrorCode, i4FilterNo,
                                           MEF_FILTER_DIRECTION_IN) ==
            SNMP_FAILURE)
        {
            nmhSetFsMefFilterStatus (i4FilterNo, DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetFsMefFilterDirection (i4FilterNo, MEF_FILTER_DIRECTION_IN)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMefFilterStatus (&u4ErrorCode, i4FilterNo, ACTIVE)
        == SNMP_FAILURE)
    {
        nmhSetFsMefFilterStatus (i4FilterNo, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsMefFilterStatus (i4FilterNo, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsMefFilterStatus (i4FilterNo, DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliConfClassMapInfo (tCliHandle CliHandle, UINT4 *pu4ClassMapId,
                        UINT4 *pu4FilterId, UINT4 *pu4Class)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4ClassMapId = 0;
    INT4                i4FilterId = 0;
    INT4                i4Class = 0;

    UNUSED_PARAM (CliHandle);

    MEMCPY (&i4ClassMapId, pu4ClassMapId, sizeof (INT4));
    if (pu4FilterId == NULL)
    {
        if (nmhTestv2FsMefClassMapStatus (&u4ErrorCode, i4ClassMapId,
                                          DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefClassMapStatus (i4ClassMapId, DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    MEMCPY (&i4FilterId, pu4FilterId, sizeof (INT4));
    MEMCPY (&i4Class, pu4Class, sizeof (INT4));

    if (nmhTestv2FsMefClassMapStatus (&u4ErrorCode, i4ClassMapId,
                                      CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefClassMapStatus (i4ClassMapId, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefClassMapFilterId (&u4ErrorCode, i4ClassMapId,
                                        i4FilterId) == SNMP_FAILURE)
    {
        nmhSetFsMefClassMapStatus (i4ClassMapId, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsMefClassMapFilterId (i4ClassMapId, i4FilterId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefClassMapCLASS (&u4ErrorCode, i4ClassMapId, i4Class)
        == SNMP_FAILURE)
    {
        nmhSetFsMefClassMapStatus (i4ClassMapId, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsMefClassMapCLASS (i4ClassMapId, i4Class) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefClassMapStatus (&u4ErrorCode, i4ClassMapId,
                                      ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefClassMapStatus (i4ClassMapId, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliConfClassInfo (tCliHandle CliHandle, UINT4 *pu4Class, UINT4 u4Command)
{

    UINT4               u4ErrorCode = 0;
    INT4                i4Class = 0;

    UNUSED_PARAM (CliHandle);

    MEMCPY (&i4Class, pu4Class, sizeof (INT4));

    if (u4Command == MEF_CLI_NO_CLASS)
    {
        if (nmhTestv2FsMefClassStatus (&u4ErrorCode, i4Class, DESTROY)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefClassStatus (i4Class, DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    if (nmhTestv2FsMefClassStatus (&u4ErrorCode, i4Class, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefClassStatus (i4Class, CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefClassStatus (&u4ErrorCode, i4Class, ACTIVE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefClassStatus (i4Class, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliConfMeterInfo (tCliHandle CliHandle, UINT4 *pu4MeterId, UINT4 *pu4Cir,
                     UINT4 *pu4Cbs, UINT4 *pu4Eir, UINT4 *pu4Ebs,
                     UINT4 *pu4ColorAware, UINT4 *pu4CouplingFlag)
{

    UINT4               u4ErrorCode = 0;
    UINT4               u4MeterId = 0;
    UINT4               u4Cir = 0;
    UINT4               u4Cbs = 0;
    UINT4               u4Eir = 0;
    UINT4               u4Ebs = 0;
    INT4                i4RowStatus = 0;

    MEMCPY (&u4MeterId, pu4MeterId, sizeof (UINT4));
    if (pu4Cir == NULL)
    {
        if (nmhTestv2FsMefMeterStatus (&u4ErrorCode, u4MeterId, DESTROY)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMeterStatus (u4MeterId, DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    MEMCPY (&u4Cir, pu4Cir, sizeof (UINT4));
    MEMCPY (&u4Cbs, pu4Cbs, sizeof (UINT4));
    MEMCPY (&u4Eir, pu4Eir, sizeof (UINT4));
    MEMCPY (&u4Ebs, pu4Ebs, sizeof (UINT4));

    UNUSED_PARAM (CliHandle);

    if ((nmhGetFsMefMeterStatus (u4MeterId, &i4RowStatus)) == SNMP_FAILURE)
    {
        if (nmhTestv2FsMefMeterStatus (&u4ErrorCode, u4MeterId, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMeterStatus (u4MeterId, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsMefMeterStatus (&u4ErrorCode, u4MeterId, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMeterStatus (u4MeterId, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMefMeterCIR (&u4ErrorCode, u4MeterId, u4Cir) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMeterCIR (u4MeterId, u4Cir) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMeterCBS (&u4ErrorCode, u4MeterId, u4Cbs) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMeterCBS (u4MeterId, u4Cbs) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMeterEIR (&u4ErrorCode, u4MeterId, u4Eir) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMeterEIR (u4MeterId, u4Eir) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMeterEBS (&u4ErrorCode, u4MeterId, u4Ebs) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMeterEBS (u4MeterId, u4Ebs) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (pu4ColorAware != NULL)
    {
        if (nmhTestv2FsMefMeterColorMode (&u4ErrorCode, u4MeterId,
                                          MEF_METER_COLOR_AWARE) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMeterColorMode (u4MeterId, MEF_METER_COLOR_AWARE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (pu4CouplingFlag != NULL)
    {
        if (nmhTestv2FsMefMeterType (&u4ErrorCode, u4MeterId, MEF_METER_COUPLED)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMeterType (u4MeterId, MEF_METER_COUPLED) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMefMeterStatus (&u4ErrorCode, u4MeterId, ACTIVE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMeterStatus (u4MeterId, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliConfPolicyMapInfo (tCliHandle CliHandle, UINT4 *pu4PolicyMapId,
                         UINT4 *pu4Class, UINT4 *pu4Meter)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4PolicyMapId = 0;
    UINT4               u4Class = 0;
    UINT4               u4Meter = 0;
    INT4                i4RetVal = CLI_FAILURE;

    UNUSED_PARAM (CliHandle);

    MEMCPY (&u4PolicyMapId, pu4PolicyMapId, sizeof (UINT4));
    if (pu4Class == NULL)
    {
        if (nmhTestv2FsMefPolicyMapStatus (&u4ErrorCode, u4PolicyMapId,
                                           DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefPolicyMapStatus (u4PolicyMapId, DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    MEMCPY (&u4Class, pu4Class, sizeof (UINT4));

    if (nmhTestv2FsMefPolicyMapStatus (&u4ErrorCode, u4PolicyMapId,
                                       CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefPolicyMapStatus (u4PolicyMapId, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefPolicyMapCLASS (&u4ErrorCode, u4PolicyMapId, u4Class)
        != SNMP_FAILURE)
    {
        if (nmhSetFsMefPolicyMapCLASS (u4PolicyMapId, u4Class) == SNMP_SUCCESS)
        {
            i4RetVal = CLI_SUCCESS;
        }

        if (pu4Meter != NULL)
        {
            MEMCPY (&u4Meter, pu4Meter, sizeof (UINT4));
            if (nmhTestv2FsMefPolicyMapMeterTableId
                (&u4ErrorCode, u4PolicyMapId, u4Meter) != SNMP_FAILURE)
            {
                if (nmhSetFsMefPolicyMapMeterTableId (u4PolicyMapId, u4Meter)
                    == SNMP_SUCCESS)
                {
                    i4RetVal = CLI_SUCCESS;
                }
            }
            else
            {
                i4RetVal = CLI_FAILURE;
            }
        }
    }

    if (i4RetVal == CLI_SUCCESS)
    {
        if (nmhTestv2FsMefPolicyMapStatus (&u4ErrorCode, u4PolicyMapId,
                                           ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefPolicyMapStatus (u4PolicyMapId, ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsMefPolicyMapStatus (&u4ErrorCode, u4PolicyMapId,
                                           DESTROY) != SNMP_FAILURE)
        {
            nmhSetFsMefPolicyMapStatus (u4PolicyMapId, DESTROY);
        }
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliGetContextInfoForShowCmd (tCliHandle CliHandle, UINT1 *pu1ContextName,
                                UINT4 u4PreContextId, UINT4 *pu4ContextId)
{
    UNUSED_PARAM (CliHandle);
    if (pu1ContextName != NULL)
    {
        if (VcmIsSwitchExist (pu1ContextName, pu4ContextId) != VCM_TRUE)
        {
            CLI_SET_ERR (MEF_CLI_INVALID_CONTEXT);
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (u4PreContextId == (MEF_SIZING_CONTEXT_COUNT + 1))
        {
            if (nmhGetFirstIndexFsMefContextTable (pu4ContextId) !=
                SNMP_SUCCESS)
            {
                return OSIX_FAILURE;
            }
        }
        else if (nmhGetNextIndexFsMefContextTable (u4PreContextId, pu4ContextId)
                 != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

INT4
MefCliConfETreeInfo (tCliHandle CliHandle, UINT4 *pu4Evc,
                     UINT4 *pu4RootUniPortType, UINT4 *pu4RootUniPort,
                     UINT4 *pu4LeafUniList1Type, UINT4 *pu4LeafUniList1,
                     UINT4 *pu4LeafUniList2Type, UINT4 *pu4LeafUniList2)
{
    UINT4               au4LeafPortList[MEF_ETREE_MAX_LEAF_UNIS];
    UINT4               u4IfType = 0;
    UINT4               u4RootPort = 0;
    UINT4               u4Scan = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4Count = 0;
    UINT4               u4MaxCount = 0;
    INT4                i4Evc = 0;

    MEMSET (au4LeafPortList, 0, sizeof (au4LeafPortList));
    MEMCPY (&i4Evc, pu4Evc, sizeof (INT4));

    if (pu4RootUniPortType != NULL)
    {
        if (CfaCliValidateXInterfaceName ((INT1 *) pu4RootUniPortType, NULL,
                                          &u4IfType) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid ingress port\r\n");
            return CLI_FAILURE;
        }
    }

    if (pu4RootUniPort != NULL)
    {
        if (MefCliUtilConvertStrToPortArray
            (CliHandle, (UINT1 *) pu4RootUniPort, &u4RootPort, &u4Count, 1,
             u4IfType) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        u4Count = 0;
    }

    if (pu4LeafUniList1Type != NULL)
    {
        if (CfaCliValidateXInterfaceName ((INT1 *) pu4LeafUniList1Type, NULL,
                                          &u4IfType) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid Interface type\r\n");
            return CLI_FAILURE;
        }
    }

    if (pu4LeafUniList1 != NULL)
    {
        if (MefCliUtilConvertStrToPortArray
            (CliHandle, (UINT1 *) pu4LeafUniList1,
             (au4LeafPortList + u4MaxCount), &u4Count, MEF_ETREE_MAX_LEAF_UNIS,
             u4IfType) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        u4MaxCount += u4Count;
        u4Count = 0;
    }

    if (pu4LeafUniList2Type != NULL)
    {
        if (CfaCliValidateXInterfaceName ((INT1 *) pu4LeafUniList2Type, NULL,
                                          &u4IfType) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid Interface type\r\n");
            return CLI_FAILURE;
        }
    }

    if (pu4LeafUniList2 != NULL)
    {
        if (MefCliUtilConvertStrToPortArray
            (CliHandle, (UINT1 *) pu4LeafUniList2,
             (au4LeafPortList + u4MaxCount), &u4Count, MEF_ETREE_MAX_LEAF_UNIS,
             u4IfType) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        u4MaxCount += u4Count;
        u4Count = 0;
    }

    for (u4Scan = 0; u4Scan < MEF_ETREE_MAX_LEAF_UNIS; u4Scan++)
    {
        if (0 == au4LeafPortList[u4Scan])
        {
            continue;
        }

        if (nmhTestv2FsMefETreeRowStatus (&u4ErrorCode, u4RootPort,
                                          i4Evc, au4LeafPortList[u4Scan],
                                          CREATE_AND_GO) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MEF_CLI_PORT_LIST_ERR);
            return CLI_FAILURE;
        }

        if (nmhSetFsMefETreeRowStatus (u4RootPort, i4Evc,
                                       au4LeafPortList[u4Scan],
                                       CREATE_AND_GO) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MEF_CLI_PORT_LIST_ERR);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

INT4
MefCliRemoveETreeInfo (tCliHandle CliHandle, UINT4 *pu4Evc,
                       UINT4 *pu4RootUniPortType, UINT4 *pu4RootUniPort)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfType = 0;
    UINT4               u4Count = 0;
    INT4                i4RootPort = 0;
    INT4                i4NextEvc = 0;
    INT4                i4NextRootPort = 0;
    INT4                i4NextLeafPort = 0;
    INT4                i4LeafPort = 0;
    INT4                i4Evc = 0;

    MEMCPY (&i4Evc, pu4Evc, sizeof (INT4));

    if (pu4RootUniPortType != NULL)
    {
        if (CfaCliValidateXInterfaceName ((INT1 *) pu4RootUniPortType, NULL,
                                          &u4IfType) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid Ingress port\r\n");
            return CLI_FAILURE;
        }
    }

    if (pu4RootUniPort != NULL)
    {
        if (MefCliUtilConvertStrToPortArray
            (CliHandle, (UINT1 *) pu4RootUniPort, (UINT4 *) &i4RootPort,
             &u4Count, 1, u4IfType) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    while (nmhGetNextIndexFsMefETreeTable (i4RootPort, &i4NextRootPort, i4Evc,
                                           &i4NextEvc, i4LeafPort,
                                           &i4NextLeafPort) == SNMP_SUCCESS)
    {
        if ((i4RootPort != i4NextRootPort) || (i4Evc != i4NextEvc))
        {
            break;
        }

        if (nmhTestv2FsMefETreeRowStatus (&u4ErrorCode, i4NextRootPort,
                                          i4Evc, i4NextLeafPort,
                                          DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefETreeRowStatus (i4NextRootPort, i4Evc, i4NextLeafPort,
                                       DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        i4RootPort = i4NextRootPort;
        i4NextRootPort = 0;
        i4NextLeafPort = 0;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : cli_process_mef_sh_cmd
 *
 * DESCRIPTION      : This function is exported to CLI module to handle the
 *                    MEF CLI show commands to display the information.
 *                    Only SNMP Low level routines are called from CLI.
 *
 * INPUT            : Variable arguments
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
cli_process_mef_sh_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *pau4Args[MEF_CLI_MAX_ARGS];
    tMefFrameLoss       MefFrameLoss;
    tMefFrameDelay      MefFrameDelay;
    tMefAvailable       MefAvailable;
    UINT4               u4ContextId = 0;
    UINT4               u4PreContextId = (MEF_SIZING_CONTEXT_COUNT + 1);
    UINT4               u4ErrCode = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;
    INT1                i1ArgNo = 0;
    UINT1              *pu1ContextName = NULL;
    tVlanId             VlanId = 0;

    CliRegisterLock (CliHandle, MefApiLock, MefApiUnLock);

    MefApiLock ();

    va_start (ap, u4Command);

    i4IfIndex = va_arg (ap, INT4);

    if (i4IfIndex == 0)
    {
        i4IfIndex = CLI_GET_IFINDEX ();
    }

    pu1ContextName = va_arg (ap, UINT1 *);

    while (1)
    {
        pau4Args[i1ArgNo++] = va_arg (ap, UINT4 *);

        if (i1ArgNo == MEF_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    while (MefCliGetContextInfoForShowCmd (CliHandle, pu1ContextName,
                                           u4PreContextId, &u4ContextId)
           == OSIX_SUCCESS)
    {

        switch (u4Command)
        {
            case MEF_CLI_SHOW_UNI:

                i4RetVal = MefCliShowUniInfo (CliHandle, u4ContextId);
                break;

            case MEF_CLI_SHOW_EVC:

                i4RetVal = MefCliShowEvcInfo (CliHandle, u4ContextId);
                break;

            case MEF_CLI_SHOW_CEVLAN_EVC_MAP:

                i4RetVal = MefCliShowCeVlanEvcMapInfo (CliHandle, u4ContextId);
                break;

            case MEF_CLI_CLEAR_FRAME_LOSS:

                i4RetVal = MefCliClearFrameLossBuffer (CliHandle, u4ContextId);
                break;

            case MEF_CLI_CLEAR_FRAME_DELAY:
                i4RetVal = MefCliClearFrameDelayBuffer (CliHandle, u4ContextId);
                break;

            case MEF_CLI_CLEAR_L2_TUNNEL_COUNTERS:
                if (pau4Args[0] != NULL)
                {
                    VlanId = CLI_PTR_TO_I4 (pau4Args[0]);
                }

                i4RetVal =
                    MefCliClearTunnelCounter (CliHandle, u4ContextId, VlanId);
                break;

            case MEF_CLI_FRAME_LOSS:

                MEMSET (&MefFrameLoss, 0, sizeof (tMefFrameLoss));

                MefFrameLoss.u4ContextId = u4ContextId;

                if (pau4Args[0] != NULL)
                {
                    MefFrameLoss.u4LmState = MEF_TX_STATUS_START;
                }
                else
                {
                    MefFrameLoss.u4LmState = MEF_TX_STATUS_STOP;
                }

                MEMCPY (&(MefFrameLoss.u4MdIndex), pau4Args[2], sizeof (UINT4));
                MEMCPY (&(MefFrameLoss.u4MaIndex), pau4Args[3], sizeof (UINT4));
                MEMCPY (&(MefFrameLoss.u4MepIndex), pau4Args[4],
                        sizeof (UINT4));

                if (pau4Args[5] != NULL)
                {

                    MEMCPY (&(MefFrameLoss.u4RmepId), pau4Args[5],
                            sizeof (INT4));
                    MefFrameLoss.bIsMepId = OSIX_TRUE;
                }
                else
                {
                    CliStrToMac ((UINT1 *) pau4Args[6],
                                 (MefFrameLoss.RmepMacAddr));
                    MefFrameLoss.bIsMepId = OSIX_FALSE;
                }

                if (pau4Args[7] != NULL)
                {
                    MEMCPY (&(MefFrameLoss.i4LmInterval), pau4Args[7],
                            sizeof (INT4));
                }
                else
                {
                    MefFrameLoss.i4LmInterval = MEF_DEFAULT_LMM_INTERVAL;
                }

                if (pau4Args[8] != NULL)
                {
                    MEMCPY (&(MefFrameLoss.i4LmCount), pau4Args[8],
                            sizeof (INT4));
                }

                if (pau4Args[9] != NULL)
                {
                    MEMCPY (&(MefFrameLoss.i4LmDeadLine), pau4Args[9],
                            sizeof (INT4));
                }

                i4RetVal = MefCliConfigFrameLoss (CliHandle, &MefFrameLoss);
                break;

            case MEF_CLI_FRAME_DELAY:

                MEMSET (&MefFrameDelay, 0, sizeof (tMefFrameDelay));

                MefFrameDelay.u4ContextId = u4ContextId;

                if (pau4Args[0] != NULL)
                {
                    MefFrameDelay.u4DmState = MEF_TX_STATUS_START;
                }
                else
                {
                    MefFrameDelay.u4DmState = MEF_TX_STATUS_STOP;
                }

                if (pau4Args[2] != NULL)
                {
                    MefFrameDelay.i4DmType = MEF_DM_TYPE_1DM;
                }
                else
                {
                    MefFrameDelay.i4DmType = MEF_DM_TYPE_DMM;
                }

                MEMCPY (&(MefFrameDelay.u4MdIndex), pau4Args[4],
                        sizeof (UINT4));
                MEMCPY (&(MefFrameDelay.u4MaIndex), pau4Args[5],
                        sizeof (UINT4));
                MEMCPY (&(MefFrameDelay.u4MepIndex), pau4Args[6],
                        sizeof (UINT4));

                if (pau4Args[7] != NULL)
                {

                    MEMCPY (&(MefFrameDelay.u4RmepId), pau4Args[7],
                            sizeof (INT4));
                    MefFrameDelay.bIsMepId = OSIX_TRUE;
                }
                else
                {
                    CliStrToMac ((UINT1 *) pau4Args[8],
                                 (MefFrameDelay.RmepMacAddr));
                    MefFrameDelay.bIsMepId = OSIX_FALSE;
                }

                if (pau4Args[9] != NULL)
                {
                    *pau4Args[9] = ((*pau4Args[9]) / MEF_VAL_10);
                    MEMCPY (&(MefFrameDelay.i4DmInterval), pau4Args[9],
                            sizeof (INT4));
                }
                else
                {
                    MefFrameDelay.i4DmInterval = MEF_DM_INTERVAL_DEF_VAL;
                }

                if (pau4Args[10] != NULL)
                {
                    MEMCPY (&(MefFrameDelay.i4DmCount), pau4Args[10],
                            sizeof (INT4));
                }

                if (pau4Args[11] != NULL)
                {
                    MEMCPY (&(MefFrameDelay.i4DmDeadLine), pau4Args[11],
                            sizeof (INT4));
                }

                i4RetVal = MefCliConfigFrameDelay (CliHandle, &MefFrameDelay);
                break;

            case MEF_CLI_AVAILABILITY:

                MEMSET (&MefAvailable, 0, sizeof (tMefAvailable));

                MefAvailable.u4ContextId = u4ContextId;

                if (pau4Args[0] != NULL)
                {
                    MefAvailable.u4AvailabileState = MEF_TX_STATUS_START;
                }
                else
                {
                    MefAvailable.u4AvailabileState = MEF_TX_STATUS_STOP;
                }

                MEMCPY (&(MefAvailable.u4MdIndex), pau4Args[2], sizeof (UINT4));
                MEMCPY (&(MefAvailable.u4MaIndex), pau4Args[3], sizeof (UINT4));
                MEMCPY (&(MefAvailable.u4MepIndex), pau4Args[4],
                        sizeof (UINT4));

                if (pau4Args[5] != NULL)
                {

                    MEMCPY (&(MefAvailable.u4RmepId), pau4Args[5],
                            sizeof (INT4));
                    MefAvailable.bIsMepId = OSIX_TRUE;
                }
                else
                {
                    CliStrToMac ((UINT1 *) pau4Args[6],
                                 (MefAvailable.RmepMacAddr));
                    MefAvailable.bIsMepId = OSIX_FALSE;
                }

                if (pau4Args[7] != NULL)
                {
                    MEMCPY (&(MefAvailable.i4AvailInterval), pau4Args[7],
                            sizeof (INT4));
                }
                else
                {
                    MefAvailable.i4AvailInterval = MEF_AVLBLTY_INTERVAL_DEFAULT;
                }

                if (pau4Args[8] != NULL)
                {
                    MEMCPY (&(MefAvailable.u4AvailWindowSize), pau4Args[8],
                            sizeof (UINT4));
                }
                else
                {
                    MefAvailable.u4AvailWindowSize = MEF_AVLBLTY_WINDOW_DEFAULT;
                }

                if (pau4Args[9] != NULL)
                {
                    MEMCPY (&(MefAvailable.i4AvailDeadLine), pau4Args[9],
                            sizeof (INT4));
                }
                else
                {
                    MefAvailable.i4AvailDeadLine = MEF_AVLBLTY_DEADLINE_DEFAULT;
                }
                if (pau4Args[10] != NULL)
                {
                    MefAvailable.i4AvailAlgoType = MEF_AVLBLTY_STATIC_METHOD;
                }
                else
                {
                    MefAvailable.i4AvailAlgoType = MEF_AVLBLTY_SLIDING_METHOD;
                }

                if (pau4Args[12] != NULL)
                {
                    MefAvailable.LowThreshold.pu1_OctetList =
                        (UINT1 *) pau4Args[12];
                    MefAvailable.LowThreshold.i4_Length = STRLEN (pau4Args[12]);
                }

                if (pau4Args[13] != NULL)
                {
                    MefAvailable.UpperThreshold.pu1_OctetList =
                        (UINT1 *) pau4Args[13];
                    MefAvailable.UpperThreshold.i4_Length =
                        STRLEN (pau4Args[13]);
                }

                if (pau4Args[14] != NULL)
                {
                    MefAvailable.bIsModestAreaAvail = MEF_SNMP_TRUE;
                }
                else
                {
                    MefAvailable.bIsModestAreaAvail = MEF_SNMP_FALSE;
                }

                if (pau4Args[16] != NULL)
                {
                    MEMCPY (&(MefAvailable.u4DownTimeStart), pau4Args[16],
                            sizeof (INT4));
                }

                if (pau4Args[17] != NULL)
                {
                    MEMCPY (&(MefAvailable.u4DownTimeEnd), pau4Args[17],
                            sizeof (INT4));
                }
                i4RetVal = MefCliConfigAvailability (CliHandle, &MefAvailable);

                break;

            case MEF_CLI_SHOW_FRAME_LOSS:
                i4RetVal = MefCliShowFrameLoss (CliHandle, u4ContextId,
                                                pau4Args[0], pau4Args[1],
                                                pau4Args[2]);
                break;

            case MEF_CLI_SHOW_FRAME_DELAY:
                i4RetVal = MefCliShowFrameDelay (CliHandle, u4ContextId,
                                                 pau4Args[0], pau4Args[1],
                                                 pau4Args[2]);
                break;

            case MEF_CLI_SHOW_AVAILABILITY:
                i4RetVal = MefCliShowAvailability (CliHandle, u4ContextId,
                                                   pau4Args[0], pau4Args[1],
                                                   pau4Args[2]);
                break;
            case MEF_CLI_SHOW_FILTER:
                i4RetVal = MefCliShowFilterInfo (CliHandle, pau4Args[0]);
                break;

            case MEF_CLI_SHOW_CLASSMAP:
                i4RetVal = MefCliShowClassMapInfo (CliHandle, pau4Args[0]);
                break;

            case MEF_CLI_SHOW_CLASS:
                i4RetVal = MefCliShowClassInfo (CliHandle, pau4Args[0]);
                break;

            case MEF_CLI_SHOW_METER:
                i4RetVal = MefCliShowMeterInfo (CliHandle, pau4Args[0]);
                break;

            case MEF_CLI_SHOW_POLICYMAP:
                i4RetVal = MefCliShowPolicyMapInfo (CliHandle, pau4Args[0]);
                break;

            case MEF_CLI_SHOW_MEF_INFO:
                i4RetVal = MefCliShowMefInfo (CliHandle, u4ContextId);
                break;

            case MEF_CLI_SHOW_MEF_ETREE:
                i4RetVal = MefCliShowMefEtree (CliHandle, pau4Args[0],
                                               pau4Args[1]);
                break;
            case MEF_CLI_SHOW_UNILIST:
                if (pau4Args[0] == NULL && pau4Args[1] == NULL)
                {
                    i4RetVal = MefCliShowUniList (CliHandle, u4ContextId, 0, 0);
                }
                else if (pau4Args[1] == NULL && pau4Args[0] != NULL)
                {
                    i4RetVal =
                        MefCliShowUniList (CliHandle, u4ContextId,
                                           CLI_PTR_TO_U4 (pau4Args[0]), 0);
                }
                else
                {
                    i4RetVal =
                        MefCliShowUniList (CliHandle, u4ContextId,
                                           CLI_PTR_TO_U4 (pau4Args[0]),
                                           CLI_PTR_TO_U4 (pau4Args[1]));
                }
                break;

             case MEF_CLI_SHOW_EVC_STATS:
                if(pau4Args[0] == NULL)
                {
                    i4RetVal = 
                        MefCliShowEvcStats(CliHandle, u4ContextId, 
                                           0);
                }
                else
                { 
                    i4RetVal = 
                        MefCliShowEvcStats(CliHandle, u4ContextId, 
                                           *pau4Args[0]);
                }
                break;

            default:
                break;
        }

        if ((pu1ContextName != NULL) || (u4Command == MEF_CLI_SHOW_MEF_ETREE) ||
            (u4Command == MEF_CLI_SHOW_POLICYMAP)
            || (u4Command == MEF_CLI_SHOW_METER)
            || (u4Command == MEF_CLI_SHOW_CLASS)
            || (u4Command == MEF_CLI_SHOW_CLASSMAP)
            || (u4Command == MEF_CLI_SHOW_FILTER)
            || (u4Command == MEF_CLI_FRAME_LOSS)
            || (u4Command == MEF_CLI_FRAME_DELAY)
            || (u4Command == MEF_CLI_AVAILABILITY))
        {
            break;
        }

        u4PreContextId = u4ContextId;
    }

    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < MEF_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s", gapc1MefCliErrString[u4ErrCode]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_CFA) &&
                 (u4ErrCode < CLI_CFA_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       CfaCliErrString[CLI_ERR_OFFSET_CFA (u4ErrCode)]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_VLAN) &&
                 (u4ErrCode < CLI_VLAN_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       VlanCliErrString[CLI_ERR_OFFSET_VLAN (u4ErrCode)]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_VLAN_PB) &&
                 (u4ErrCode < CLI_PB_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       PbCliErrString[CLI_ERR_OFFSET_VLAN_PB (u4ErrCode)]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_ECFM) &&
                 (u4ErrCode < CLI_ECFM_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       gaEcfmCliErrString[CLI_ERR_OFFSET_ECFM (u4ErrCode)]);
        }
#ifdef MPLS_WANTED
        else if ((u4ErrCode >= CLI_ERR_START_ID_MPLS) &&
                 (u4ErrCode < MPLS_CLI_MAX_ERROR_MSGS))
        {
            CliPrintf (CliHandle, "%% %s",
                       MPLS_CLI_ERROR_MSGS[CLI_ERR_OFFSET_MPLS (u4ErrCode)]);
        }
#endif
        else if ((u4ErrCode >= CLI_ERR_START_ID_QOS) &&
                 (u4ErrCode < QOS_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       gapi1QoSCliErrString[CLI_ERR_OFFSET_QOS (u4ErrCode)]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_ISS) &&
                 (u4ErrCode < CLI_ISS_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       IssCliErrString[CLI_ERR_OFFSET_ISS (u4ErrCode)]);
        }
        else if ((u4ErrCode >= CLI_ERR_START_ID_VCM) &&
                 (u4ErrCode < CLI_VCM_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s",
                       VcmCliErrString[CLI_ERR_OFFSET_VCM (u4ErrCode)]);
        }
        else
        {
            CliPrintf (CliHandle, "%% Fatal Error. Command failed \r\n");
        }

        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    MefApiUnLock ();

    CliUnRegisterLock (CliHandle);

    return i4RetVal;
}

INT4
MefCliShowUniInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT1               au1L2cpState[][8] = { {"Peer"},
    {"Tunnel"},
    {"Discard"}
    };
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    UINT1               au1UniId[UNI_ID_MAX_LENGTH];
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE UniId;
    tSNMP_OCTET_STRING_TYPE UniType;
    UINT4               u4IfContextId = 0;
    UINT4               u4UniSpeed = 0;
    UINT2               u2LocalPortId = 0;
    INT4                i4UniMtu = 0;
    INT4                i4PreIfIndex = 0;
    INT4                i4IfIndex = 0;
    INT4                i4UniMode = 0;
    INT4                i4UniDefCeVlanId = 0;
    INT4                i4UniMaxEvcs = 0;
    INT4                i4UniL2cp = 0;
    INT4                i4Status = 0;
    UINT1               u1UniType = 0;

    VcmGetAliasName (u4ContextId, au1ContextName);
    CliPrintf (CliHandle, "\r\nSwitch %s \r\n", au1ContextName);

    while (nmhGetNextIndexFsUniTable (i4PreIfIndex, &i4IfIndex) == SNMP_SUCCESS)
    {
        VcmGetContextInfoFromIfIndex (i4IfIndex, &u4IfContextId,
                                      &u2LocalPortId);
        if (u4IfContextId != u4ContextId)
        {
            i4PreIfIndex = i4IfIndex;
            continue;
        }

        MEMSET (ai1IfName, 0, sizeof (ai1IfName));
        CfaCliGetIfName (i4IfIndex, ai1IfName);
        CliPrintf (CliHandle, "\r\n%s, ", ai1IfName);

        nmhGetFsUniRowStatus (i4IfIndex, &i4Status);
        if (i4Status == ACTIVE)
        {
            CliPrintf (CliHandle, "Status    : Active\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Status    : Not Active\r\n");
        }

        MEMSET (au1UniId, 0, UNI_ID_MAX_LENGTH);
        UniId.pu1_OctetList = au1UniId;
        UniId.i4_Length = UNI_ID_MAX_LENGTH;

        nmhGetFsUniId (i4IfIndex, &UniId);
        if (UniId.i4_Length != 0)
        {
            CliPrintf (CliHandle, "UNI Identifier   : %s \r\n",
                       UniId.pu1_OctetList);
        }

        nmhGetFsUniSpeed (i4IfIndex, &u4UniSpeed);
        CliPrintf (CliHandle, "Speed ");
        if (u4UniSpeed == ISS_10MBPS)
        {
            CliPrintf (CliHandle, "10 Mbps, ");
        }
        else if (u4UniSpeed == ISS_100MBPS)
        {
            CliPrintf (CliHandle, "100 Mbps, ");
        }
        else if (u4UniSpeed == ISS_1GB)
        {
            CliPrintf (CliHandle, "1 Gbps, ");
        }
        else if (u4UniSpeed == ISS_2500MBPS)
        {
            CliPrintf (CliHandle, "%-12s", "2.5 Gbps");
        }
        else if (u4UniSpeed == ISS_10GB)
        {
            CliPrintf (CliHandle, "10 Gbps, ");
        }
        else
        {
            CliPrintf (CliHandle, "Auto-speed, ");
        }

        nmhGetFsUniMode (i4IfIndex, &i4UniMode);
        if (i4UniMode == ISS_FULLDUP)
        {
            CliPrintf (CliHandle, "Full duplex, ");
        }
        else if (i4UniMode == ISS_HALFDUP)
        {
            CliPrintf (CliHandle, "Half duplex, ");
        }

        nmhGetFsUniMtu (i4IfIndex, &i4UniMtu);
        CliPrintf (CliHandle, "MTU %d bytes \r\n", i4UniMtu);

        UniType.pu1_OctetList = &u1UniType;
        UniType.i4_Length = sizeof (UINT1);
        nmhGetFsUniServiceMultiplexingBundling (i4IfIndex, &UniType);
        if (u1UniType & MEF_MULTIPLEX_BITMASK)
        {
            CliPrintf (CliHandle, "Multplexing         : Enabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Multplexing         : Disabled \r\n");
        }

        if (u1UniType & MEF_BUNDLE_BITMASK)
        {
            CliPrintf (CliHandle, "Bundling            : Enabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Bundling            : Disabled \r\n");
        }

        if (u1UniType & MEF_ALL_TO_ONE_BUNDLE_BITMASK)
        {
            CliPrintf (CliHandle, "All-to-One Bundling : Enabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "All-to-One Bundling : Disabled \r\n");
        }

        nmhGetFsUniCVlanId (i4IfIndex, &i4UniDefCeVlanId);
        CliPrintf (CliHandle, "Default CE-VLAN %d, ", i4UniDefCeVlanId);

        nmhGetFsUniMaxEvcs (i4IfIndex, &i4UniMaxEvcs);
        CliPrintf (CliHandle, "Max number of EVCs %d \r\n", i4UniMaxEvcs);

        CliPrintf (CliHandle, "\r\nUNI L2CP Processing \r\n");
        nmhGetFsUniL2CPDot1x (i4IfIndex, &i4UniL2cp);
        CliPrintf (CliHandle, "  Dot1x  : %s\r\n", au1L2cpState[i4UniL2cp - 1]);
        nmhGetFsUniL2CPLacp (i4IfIndex, &i4UniL2cp);
        CliPrintf (CliHandle, "  LA     : %s\r\n", au1L2cpState[i4UniL2cp - 1]);
        nmhGetFsUniL2CPStp (i4IfIndex, &i4UniL2cp);
        CliPrintf (CliHandle, "  STP    : %s\r\n", au1L2cpState[i4UniL2cp - 1]);
        nmhGetFsUniL2CPGvrp (i4IfIndex, &i4UniL2cp);
        CliPrintf (CliHandle, "  GVRP   : %s\r\n", au1L2cpState[i4UniL2cp - 1]);
        nmhGetFsUniL2CPGmrp (i4IfIndex, &i4UniL2cp);
        CliPrintf (CliHandle, "  GMRP   : %s\r\n", au1L2cpState[i4UniL2cp - 1]);
        nmhGetFsUniL2CPMvrp (i4IfIndex, &i4UniL2cp);
        CliPrintf (CliHandle, "  MVRP   : %s\r\n", au1L2cpState[i4UniL2cp - 1]);
        nmhGetFsUniL2CPMmrp (i4IfIndex, &i4UniL2cp);
        CliPrintf (CliHandle, "  MMRP   : %s\r\n", au1L2cpState[i4UniL2cp - 1]);
        nmhGetFsUniL2CPLldp (i4IfIndex, &i4UniL2cp);
        CliPrintf (CliHandle, "  LLDP   : %s\r\n", au1L2cpState[i4UniL2cp - 1]);
        nmhGetFsUniL2CPElmi (i4IfIndex, &i4UniL2cp);
        CliPrintf (CliHandle, "  ELMI   : %s\r\n", au1L2cpState[i4UniL2cp - 1]);
        nmhGetFsUniL2CPEcfm (i4IfIndex, &i4UniL2cp);
        CliPrintf (CliHandle, "  ECFM   : %s\r\n", au1L2cpState[i4UniL2cp - 1]);
        nmhGetFsUniL2CPEoam (i4IfIndex, &i4UniL2cp);
        CliPrintf (CliHandle, "  EOAM   : %s\r\n", au1L2cpState[i4UniL2cp - 1]);

        i4PreIfIndex = i4IfIndex;
    }

    return CLI_SUCCESS;
}

INT4
MefCliShowEvcInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT1               au1State[][9] = { {"Enabled"},
    {"Disabled"}
    };
    UINT1               au1L2cpState[][8] = { {"Peer"},
    {"Tunnel"}, {"Discard"}
    };
    UINT1               au4EvcProtocolAction[MEF_MAX_EVC_ONLY_L2CP_PROTOCOL][8];
    UINT1               au1EvcId[EVC_ID_MAX_LENGTH];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    tSNMP_OCTET_STRING_TYPE EvcId;
    tSNMP_OCTET_STRING_TYPE EvcTunnelProtocolList;
    UINT4               u4EvcProtocolList = 0;
    INT4                i4EvcContextId = 0;
    INT4                i4PreEvcContextId = (INT4) u4ContextId;
    INT4                i4Evc = 0;
    INT4                i4PreEvc = 0;
    INT4                i4EvcType = 0;
    INT4                i4EvcMaxUni = 0;
    INT4                i4EvcVlanIdPre = 0;
    INT4                i4EvcCosPre = 0;
    INT4                i4EvcLoopbackStatus = 0;
    INT4                i4Status = 0;
    INT4                i4EvcMtu = 0;

    VcmGetAliasName (u4ContextId, au1ContextName);
    CliPrintf (CliHandle, "\r\n\r Switch %s \r\n", au1ContextName);

    while (nmhGetNextIndexFsEvcTable (i4PreEvcContextId, &i4EvcContextId,
                                      i4PreEvc, &i4Evc) == SNMP_SUCCESS)
    {
        if (i4EvcContextId != (INT4) u4ContextId)
        {
            break;
        }

        CliPrintf (CliHandle, "EVC                  : %d\r\n", i4Evc);

        EvcId.pu1_OctetList = au1EvcId;
        EvcId.i4_Length = EVC_ID_MAX_LENGTH;
        MEMSET (au1EvcId, 0, EVC_ID_MAX_LENGTH);
        nmhGetFsEvcId (i4EvcContextId, i4Evc, &EvcId);
        if (EvcId.i4_Length != 0)
        {
            CliPrintf (CliHandle, "EVC Identifier       : %s\r\n",
                       EvcId.pu1_OctetList);
        }

        nmhGetFsEvcType (i4EvcContextId, i4Evc, &i4EvcType);
        CliPrintf (CliHandle, "EVC Type             : ");
        if (i4EvcType == MEF_POINT_TO_POINT)
        {
            CliPrintf (CliHandle, "Point-to-Point (E-Line) \r\n");
        }
        else if (i4EvcType == MEF_MULTIPOINT_TO_MULTIPOINT)
        {
            CliPrintf (CliHandle, "Multipoint-to-Multipoint (E-LAN) \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Rooted-MultiPoint (E-Tree) \r\n");
        }

        nmhGetFsEvcMaxUni (i4EvcContextId, i4Evc, &i4EvcMaxUni);
        CliPrintf (CliHandle, "Max UNIs             : %d \r\n", i4EvcMaxUni);

        nmhGetFsEvcCVlanIdPreservation (i4EvcContextId, i4Evc, &i4EvcVlanIdPre);
        CliPrintf (CliHandle, "VLAN ID preservation : %s \r\n",
                   au1State[i4EvcVlanIdPre - 1]);

        nmhGetFsEvcCVlanCoSPreservation (i4EvcContextId, i4Evc, &i4EvcCosPre);
        CliPrintf (CliHandle, "CoS preservation     : %s \r\n",
                   au1State[i4EvcCosPre - 1]);

        nmhGetFsEvcLoopbackStatus (i4EvcContextId, i4Evc, &i4EvcLoopbackStatus);
        CliPrintf (CliHandle, "Loopback Status      : %s \r\n",
                   au1State[i4EvcLoopbackStatus - 1]);

        nmhGetFsEvcMtu (i4EvcContextId, i4Evc, &i4EvcMtu);
        CliPrintf (CliHandle, "EVC Mtu              : %d\r\n", i4EvcMtu);

        nmhGetFsEvcRowStatus (i4EvcContextId, i4Evc, &i4Status);
        if (i4Status == ACTIVE)
        {
            CliPrintf (CliHandle, "Status               : Active\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Status               : Not Active\r\n");
        }

        MefCliShowEvcFilterInfo (CliHandle, i4EvcContextId, i4Evc);

        CliPrintf (CliHandle, "\r\nEVC L2CP Processing \r\n");
        EvcTunnelProtocolList.pu1_OctetList = (UINT1 *) &u4EvcProtocolList;
        EvcTunnelProtocolList.i4_Length = sizeof (UINT4);
        nmhGetFsEvcL2CPTunnel (i4EvcContextId, i4Evc, &EvcTunnelProtocolList);
        if (u4EvcProtocolList & MEF_EVC_GVRP_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_GVRP],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_TUNNEL - 1]);
        }
        if (u4EvcProtocolList & MEF_EVC_GMRP_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_GMRP],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_TUNNEL - 1]);
        }
        if (u4EvcProtocolList & MEF_EVC_IGMP_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_IGMP],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_TUNNEL - 1]);
        }
        if (u4EvcProtocolList & MEF_EVC_MVRP_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_MVRP],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_TUNNEL - 1]);
        }
        if (u4EvcProtocolList & MEF_EVC_MMRP_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_MMRP],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_TUNNEL - 1]);
        }
        if (u4EvcProtocolList & MEF_EVC_ECFM_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_ECFM],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_TUNNEL - 1]);
        }
        u4EvcProtocolList = 0;
        nmhGetFsEvcL2CPPeer (i4EvcContextId, i4Evc, &EvcTunnelProtocolList);
        if (u4EvcProtocolList & MEF_EVC_ECFM_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_ECFM],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_PEER - 1]);
        }
        u4EvcProtocolList = 0;
        nmhGetFsEvcL2CPDiscard (i4EvcContextId, i4Evc, &EvcTunnelProtocolList);

        if (u4EvcProtocolList & MEF_EVC_GVRP_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_GVRP],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_DISCARD - 1]);
        }
        if (u4EvcProtocolList & MEF_EVC_GMRP_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_GMRP],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_DISCARD - 1]);
        }
        if (u4EvcProtocolList & MEF_EVC_IGMP_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_IGMP],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_DISCARD - 1]);
        }
        if (u4EvcProtocolList & MEF_EVC_MVRP_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_MVRP],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_DISCARD - 1]);
        }
        if (u4EvcProtocolList & MEF_EVC_MMRP_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_MMRP],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_DISCARD - 1]);
        }
        if (u4EvcProtocolList & MEF_EVC_ECFM_BITMASK)
        {
            STRCPY (au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_ECFM],
                    au1L2cpState[MEF_TUNNEL_PROTOCOL_DISCARD - 1]);
        }
        CliPrintf (CliHandle, "  GVRP   : %s\r\n",
                   au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_GVRP]);
        CliPrintf (CliHandle, "  GMRP   : %s\r\n",
                   au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_GMRP]);
        CliPrintf (CliHandle, "  IGMP   : %s\r\n",
                   au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_IGMP]);
        CliPrintf (CliHandle, "  MVRP   : %s\r\n",
                   au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_MVRP]);
        CliPrintf (CliHandle, "  MMRP   : %s\r\n",
                   au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_MMRP]);
        CliPrintf (CliHandle, "  ECFM   : %s\r\n",
                   au4EvcProtocolAction[MEF_EVC_L2CP_PROTOCOL_ECFM]);

        i4PreEvcContextId = i4EvcContextId;
        i4PreEvc = i4Evc;
    }

    return CLI_SUCCESS;
}

INT4
MefCliShowEvcFilterInfo (tCliHandle CliHandle, UINT4 u4CxtId, INT4 i4Evc)
{
    UINT1               au1MacAddr[MEF_CLI_MAX_MAC_STRING_SIZE];
    INT4                i4NextContextId = 0;
    INT4                i4NextEvc = 0;
    INT4                i4Instance = 0;
    INT4                i4NextInstance = 0;
    INT4                i4FilterId = 0;
    INT4                i4Action = 0;
    tMacAddr            MacAddr;
    BOOLEAN             bPrint = OSIX_TRUE;

    while (nmhGetNextIndexFsEvcFilterTable (u4CxtId, &i4NextContextId,
                                            i4Evc, &i4NextEvc, i4Instance,
                                            &i4NextInstance) == SNMP_SUCCESS)
    {
        if ((u4CxtId != (UINT4) i4NextContextId) || (i4Evc != i4NextEvc))
        {
            break;
        }

        if (bPrint == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "\r\n Instance  FilterId  MAC Address  "
                       "       Action\r\n");
            CliPrintf (CliHandle, " ========  ========  =============== "
                       "    =========\r\n");
            bPrint = OSIX_FALSE;
        }
        nmhGetFsEvcFilterId (i4NextContextId, i4NextEvc, i4NextInstance,
                             &i4FilterId);
        nmhGetFsEvcFilterDestMacAddress (i4NextContextId, i4NextEvc,
                                         i4NextInstance, &MacAddr);
        PrintMacAddress (MacAddr, au1MacAddr);
        nmhGetFsEvcFilterAction (i4NextContextId, i4NextEvc, i4NextInstance,
                                 &i4Action);

        CliPrintf (CliHandle, "   %d   ", i4NextInstance);
        CliPrintf (CliHandle, "       %d   ", i4FilterId);
        CliPrintf (CliHandle, "   %s", au1MacAddr);
        if (i4Action == ISS_DROP)
        {
            CliPrintf (CliHandle, "Drop \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Allow \r\n");
        }
        i4Instance = i4NextInstance;
    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

INT4
MefCliShowCeVlanEvcMapInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4IfContextId = 0;
    INT4                i4PreIfIndex = 0;
    INT4                i4IfIndex = 0;
    INT4                i4PreCeVlan = 0;
    INT4                i4CeVlan = 0;
    INT4                i4Evc = 0;
    INT4                i4Status = 0;
    UINT2               u2LocalPortId = 0;
    UINT1               au1UniEvcId[UNI_ID_MAX_LENGTH];
    tSNMP_OCTET_STRING_TYPE UniEvcId;

    VcmGetAliasName (u4ContextId, au1ContextName);
    CliPrintf (CliHandle, "\r\n\r Switch %s \r\n", au1ContextName);
    CliPrintf (CliHandle, "\r\r --------------------- \r\n\r");
    CliPrintf (CliHandle, " CE-VLAN and EVC Mapping Information \r\n\r\n");
    CliPrintf (CliHandle, " CE-VLAN  UNI      EVC      Status  UniEvcId\r\n");

    while (nmhGetNextIndexFsUniCVlanEvcTable (i4PreIfIndex, &i4IfIndex,
                                              i4PreCeVlan,
                                              &i4CeVlan) == SNMP_SUCCESS)
    {
        VcmGetContextInfoFromIfIndex (i4IfIndex, &u4IfContextId,
                                      &u2LocalPortId);

        if (u4ContextId != u4IfContextId)
        {
            i4PreIfIndex = i4IfIndex;
            i4PreCeVlan = i4CeVlan;
            continue;
        }

        MEMSET (ai1IfName, 0, sizeof (ai1IfName));
        CfaCliGetIfName (i4IfIndex, ai1IfName);

        nmhGetFsUniCVlanEvcEvcIndex (i4IfIndex, i4CeVlan, &i4Evc);
        nmhGetFsUniCVlanEvcRowStatus (i4IfIndex, i4CeVlan, &i4Status);

        UniEvcId.pu1_OctetList = au1UniEvcId;
        UniEvcId.i4_Length = UNI_ID_MAX_LENGTH;
        MEMSET (au1UniEvcId, 0, UNI_ID_MAX_LENGTH);
        nmhGetFsUniEvcId (i4IfIndex, i4CeVlan, &UniEvcId);

        if (i4Status == ACTIVE)
        {
            CliPrintf (CliHandle, " %d       %s    %d      Active  ",
                       i4CeVlan, ai1IfName, i4Evc);
            if (UniEvcId.i4_Length != 0)
            {
                CliPrintf (CliHandle, "%s \n\r", UniEvcId.pu1_OctetList);
            }
            else
            {
                CliPrintf (CliHandle, "\n\r");
            }
        }
        else
        {
            CliPrintf (CliHandle, " %d  %s    %d    Not Active  ",
                       i4CeVlan, ai1IfName, i4Evc);
            if (UniEvcId.i4_Length != 0)
            {
                CliPrintf (CliHandle, "%s \n\r", UniEvcId.pu1_OctetList);
            }
            else
            {
                CliPrintf (CliHandle, "\n\r");
            }

        }
        i4PreIfIndex = i4IfIndex;
        i4PreCeVlan = i4CeVlan;

    }

    return CLI_SUCCESS;
}

/***************************************************************************/
/*  Function Name   : MefCliUtilStrToPortArray                             */
/*                                                                         */
/*  Description     : This function is used to convert the pu1str          */
/*                    to interface index formats.                          */
/*  Input(s)        :                                                      */
/*                    CliHandle   - CLI Handle                             */
/*                    pu1Str      - string containing port list formats    */
/*                    u4PortArrayLen - Port Array length                   */
/*                    u4IfType     - Interface type                        */
/*                                                                         */
/*  Output(s)       : *pu4PortArray - Array of pointers.                   */
/*                    *pu4Count     - Number of interface indices in array */
/*                                                                         */
/*  Returns         : None                                                 */
/***************************************************************************/
INT4
MefCliUtilConvertStrToPortArray (tCliHandle CliHandle, UINT1 *pu1Str,
                                 UINT4 *pu4PortArray, UINT4 *pu4Count,
                                 UINT4 u4PortArrayLen, UINT4 u4IfType)
{
    tCliIfaceList       CliIfaceList;
    tCliPortList        CliPortList;
    INT1               *pi1Temp = (INT1 *) pu1Str;
    INT1               *pi1Pos = NULL;
    UINT4               u4Val1 = 0;
    UINT4               u4Val2 = 0;
    UINT4               u4RetPortNum = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4RetType = 0;
    INT1                ai1TempArr[MEF_ETREE_MAX_LEAF_UNIS + 1];
    INT1                i1EndFlag = OSIX_FALSE;
    INT1                i1TempChar = 0;

    if (!(pi1Temp))
    {
        CliPrintf (CliHandle, "\r\n %% Invalid interface port list \r\n");
        i4RetStatus = CLI_FAILURE;
    }

    while ((i4RetStatus != CLI_FAILURE) && (i1EndFlag != OSIX_TRUE))
    {
        pi1Pos = pi1Temp;

        /* Check for port list seperater delimiters */
        pi1Pos = CliGetToken (pi1Pos, CLI_LIST_DELIMIT, CLI_VALIDIFACE_LIST);

        if (!pi1Pos)
        {
            i4RetStatus = CLI_FAILURE;
            CliPrintf (CliHandle, "\r\n %% Invalid interface port list \r\n");
            break;
        }

        if (!(*pi1Pos))
        {
            i1EndFlag = OSIX_TRUE;
        }

        if (CFA_PSEUDO_WIRE != u4IfType)
        {
            i1TempChar = *pi1Pos;
        }

        *pi1Pos = '\0';

        if (CFA_PSEUDO_WIRE != u4IfType)
        {
            /* Get values between the list seperaters based on the type */
            i4RetType = CliGetValFromIfaceType (&CliIfaceList, pi1Temp,
                                                u4IfType);
            *pi1Pos = i1TempChar;
        }
        else
        {
            /* Get values between the list seperaters based on the type */
            i4RetType = CliGetVal (&CliPortList, pi1Temp);
        }

        switch (i4RetType)
        {
            case CLI_LIST_TYPE_VAL:
                if (CFA_PSEUDO_WIRE != u4IfType)
                {
                    u4Val1 = CliIfaceList.uPortList.u4PortNum;
                    u4Val2 = CliIfaceList.uPortList.u4PortNum;
                }
                else
                {
                    u4Val1 = CliPortList.uPortList.u4PortNum;
                    u4Val2 = CliPortList.uPortList.u4PortNum;
                }
                break;

            case CLI_LIST_TYPE_RANGE:
                if (CFA_PSEUDO_WIRE != u4IfType)
                {
                    u4Val1 = CliIfaceList.uPortList.PortListRange.u4PortFrom;
                    u4Val2 = CliIfaceList.uPortList.PortListRange.u4PortTo;
                }
                else
                {
                    u4Val1 = CliPortList.uPortList.PortListRange.u4PortFrom;
                    u4Val2 = CliPortList.uPortList.PortListRange.u4PortTo;
                }
                break;

            default:
                i4RetStatus = CLI_FAILURE;
                CliPrintf (CliHandle, "\r\n%% Invalid Port List");
                continue;
        }                        /* End of switch */

        if (pu4PortArray != NULL)
        {
            for (; u4Val1 <= u4Val2; u4Val1++)
            {
                if (u4PortArrayLen > *pu4Count)
                {
                    if (CFA_PSEUDO_WIRE == u4IfType)
                    {
                        SPRINTF ((CHR1 *) ai1TempArr, "%u", u4Val1);
                        if (CliGetIfIndexFromTypeAndPortNum (u4IfType,
                                                             ai1TempArr,
                                                             &u4RetPortNum) ==
                            CLI_SUCCESS)
                        {
                            pu4PortArray[*pu4Count] = u4RetPortNum;
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "%% Interface does not exist\r\n");
                            return CLI_FAILURE;
                        }
                    }
                    else
                    {
                        pu4PortArray[*pu4Count] = u4Val1;
                    }
                    (*pu4Count)++;
                }
                else
                {
                    CliPrintf (CliHandle, "%% Only %d ports can be configured"
                               " as Uplink ports\r\n", MEF_ETREE_MAX_LEAF_UNIS);
                    return CLI_FAILURE;
                }
            }
        }
        pi1Temp = pi1Pos + 1;
    }

    return i4RetStatus;
}

INT4
MefCliClearFrameLossBuffer (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsMefFrameLossBufferClear (&u4ErrorCode, u4ContextId,
                                            MEF_SNMP_TRUE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefFrameLossBufferClear (u4ContextId, MEF_SNMP_TRUE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliClearTunnelCounter (tCliHandle CliHandle, UINT4 u4ContextId,
                          tVlanId VlanId)
{
    UNUSED_PARAM (CliHandle);
    if (VlanPbClearEvcL2TunnelCounters (u4ContextId, VlanId) != VLAN_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
MefCliClearFrameDelayBuffer (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsMefFrameDelayBufferClear (&u4ErrorCode, u4ContextId,
                                             MEF_SNMP_TRUE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefFrameDelayBufferClear (u4ContextId, MEF_SNMP_TRUE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliConfigFrameLoss (tCliHandle CliHandle, tMefFrameLoss * pMefFrameLoss)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4ContextId = pMefFrameLoss->u4ContextId;
    UINT4               u4MdIndex = pMefFrameLoss->u4MdIndex;
    UINT4               u4MaIndex = pMefFrameLoss->u4MaIndex;
    UINT4               u4MepIndex = pMefFrameLoss->u4MepIndex;

    UNUSED_PARAM (CliHandle);

    if (nmhValidateIndexInstanceFsMefMepTable (u4ContextId, u4MdIndex,
                                               u4MaIndex,
                                               u4MepIndex) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_MEP_NOT_EXIST);
        return CLI_FAILURE;
    }

    if (pMefFrameLoss->u4LmState == MEF_TX_STATUS_STOP)
    {
        if (nmhTestv2FsMefMepTransmitLmmStatus (&u4ErrorCode, u4ContextId,
                                                u4MdIndex, u4MaIndex,
                                                u4MepIndex,
                                                pMefFrameLoss->u4LmState) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepTransmitLmmStatus (u4ContextId, u4MdIndex, u4MaIndex,
                                             u4MepIndex,
                                             pMefFrameLoss->u4LmState) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }

    if (pMefFrameLoss->bIsMepId == OSIX_TRUE)
    {
        if (nmhTestv2FsMefMepTransmitLmmDestIsMepId (&u4ErrorCode, u4ContextId,
                                                     u4MdIndex, u4MaIndex,
                                                     u4MepIndex,
                                                     MEF_SNMP_TRUE) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepTransmitLmmDestIsMepId (u4ContextId, u4MdIndex,
                                                  u4MaIndex, u4MepIndex,
                                                  MEF_SNMP_TRUE) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMefMepTransmitLmmDestMepId (&u4ErrorCode,
                                                   u4ContextId, u4MdIndex,
                                                   u4MaIndex, u4MepIndex,
                                                   pMefFrameLoss->u4RmepId) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepTransmitLmmDestMepId (u4ContextId, u4MdIndex,
                                                u4MaIndex, u4MepIndex,
                                                pMefFrameLoss->u4RmepId)
            == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsMefMepTransmitLmmDestIsMepId (&u4ErrorCode, u4ContextId,
                                                     u4MdIndex, u4MaIndex,
                                                     u4MepIndex,
                                                     MEF_SNMP_FALSE) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepTransmitLmmDestIsMepId (u4ContextId, u4MdIndex,
                                                  u4MaIndex, u4MepIndex,
                                                  MEF_SNMP_FALSE) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMefMepTransmitLmmDestMacAddress (&u4ErrorCode,
                                                        u4ContextId, u4MdIndex,
                                                        u4MaIndex, u4MepIndex,
                                                        pMefFrameLoss->
                                                        RmepMacAddr) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepTransmitLmmDestMacAddress (u4ContextId, u4MdIndex,
                                                     u4MaIndex, u4MepIndex,
                                                     pMefFrameLoss->RmepMacAddr)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMefMepTransmitLmmMessages (&u4ErrorCode, u4ContextId,
                                              u4MdIndex, u4MaIndex, u4MepIndex,
                                              pMefFrameLoss->i4LmCount) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    GET_LMM_INTERVAL (pMefFrameLoss->i4LmInterval);
    if (pMefFrameLoss->i4LmInterval == MEF_MAX_LMM_INTERVALS)
    {
        /* Not a valid value */
        CLI_SET_ERR (MEF_CLI_INVALID_LMM_INTERVAL_ERR);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepTransmitLmmInterval (&u4ErrorCode, u4ContextId,
                                              u4MdIndex, u4MaIndex, u4MepIndex,
                                              pMefFrameLoss->i4LmInterval) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepTransmitLmmDeadline (&u4ErrorCode, u4ContextId,
                                              u4MdIndex, u4MaIndex, u4MepIndex,
                                              pMefFrameLoss->i4LmDeadLine) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepTransmitLmmStatus (&u4ErrorCode, u4ContextId,
                                            u4MdIndex, u4MaIndex, u4MepIndex,
                                            pMefFrameLoss->u4LmState) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepTransmitLmmMessages (u4ContextId, u4MdIndex, u4MaIndex,
                                           u4MepIndex, pMefFrameLoss->i4LmCount)
        == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepTransmitLmmInterval (u4ContextId, u4MdIndex, u4MaIndex,
                                           u4MepIndex,
                                           pMefFrameLoss->i4LmInterval) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepTransmitLmmDeadline (u4ContextId, u4MdIndex, u4MaIndex,
                                           u4MepIndex,
                                           pMefFrameLoss->i4LmDeadLine) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepTransmitLmmStatus (u4ContextId, u4MdIndex, u4MaIndex,
                                         u4MepIndex, pMefFrameLoss->u4LmState)
        == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliConfigFrameDelay (tCliHandle CliHandle, tMefFrameDelay * pMefFrameDelay)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4ContextId = pMefFrameDelay->u4ContextId;
    UINT4               u4MdIndex = pMefFrameDelay->u4MdIndex;
    UINT4               u4MaIndex = pMefFrameDelay->u4MaIndex;
    UINT4               u4MepIndex = pMefFrameDelay->u4MepIndex;

    UNUSED_PARAM (CliHandle);

    if (nmhValidateIndexInstanceFsMefMepTable (u4ContextId, u4MdIndex,
                                               u4MaIndex,
                                               u4MepIndex) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_MEP_NOT_EXIST);
        return CLI_FAILURE;
    }

    if (pMefFrameDelay->u4DmState == MEF_TX_STATUS_STOP)
    {
        if (nmhTestv2FsMefMepTransmitDmStatus (&u4ErrorCode, u4ContextId,
                                               u4MdIndex, u4MaIndex, u4MepIndex,
                                               pMefFrameDelay->u4DmState)
            == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepTransmitDmStatus (u4ContextId, u4MdIndex, u4MaIndex,
                                            u4MepIndex,
                                            pMefFrameDelay->u4DmState) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }

    if (pMefFrameDelay->bIsMepId == OSIX_TRUE)
    {
        if (nmhTestv2FsMefMepTransmitDmDestIsMepId (&u4ErrorCode, u4ContextId,
                                                    u4MdIndex, u4MaIndex,
                                                    u4MepIndex,
                                                    MEF_SNMP_TRUE) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepTransmitDmDestIsMepId (u4ContextId, u4MdIndex,
                                                 u4MaIndex, u4MepIndex,
                                                 MEF_SNMP_TRUE) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMefMepTransmitDmDestMepId (&u4ErrorCode,
                                                  u4ContextId, u4MdIndex,
                                                  u4MaIndex, u4MepIndex,
                                                  pMefFrameDelay->u4RmepId) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepTransmitDmDestMepId (u4ContextId, u4MdIndex,
                                               u4MaIndex, u4MepIndex,
                                               pMefFrameDelay->u4RmepId)
            == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsMefMepTransmitDmDestIsMepId (&u4ErrorCode, u4ContextId,
                                                    u4MdIndex, u4MaIndex,
                                                    u4MepIndex,
                                                    MEF_SNMP_FALSE) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepTransmitDmDestIsMepId (u4ContextId, u4MdIndex,
                                                 u4MaIndex, u4MepIndex,
                                                 MEF_SNMP_FALSE) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMefMepTransmitDmDestMacAddress (&u4ErrorCode,
                                                       u4ContextId, u4MdIndex,
                                                       u4MaIndex, u4MepIndex,
                                                       pMefFrameDelay->
                                                       RmepMacAddr) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepTransmitDmDestMacAddress (u4ContextId, u4MdIndex,
                                                    u4MaIndex, u4MepIndex,
                                                    pMefFrameDelay->RmepMacAddr)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMefMepTransmitDmType (&u4ErrorCode, u4ContextId, u4MdIndex,
                                         u4MaIndex, u4MepIndex,
                                         pMefFrameDelay->i4DmType) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepTransmitDmMessages (&u4ErrorCode, u4ContextId,
                                             u4MdIndex, u4MaIndex, u4MepIndex,
                                             pMefFrameDelay->i4DmCount) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepTransmitDmInterval (&u4ErrorCode, u4ContextId,
                                             u4MdIndex, u4MaIndex, u4MepIndex,
                                             pMefFrameDelay->i4DmInterval) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepTransmitDmDeadline (&u4ErrorCode, u4ContextId,
                                             u4MdIndex, u4MaIndex, u4MepIndex,
                                             pMefFrameDelay->i4DmDeadLine) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepTransmitDmStatus (&u4ErrorCode, u4ContextId,
                                           u4MdIndex, u4MaIndex, u4MepIndex,
                                           pMefFrameDelay->u4DmState) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepTransmitDmType (u4ContextId, u4MdIndex, u4MaIndex,
                                      u4MepIndex, pMefFrameDelay->i4DmType)
        == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepTransmitDmMessages (u4ContextId, u4MdIndex, u4MaIndex,
                                          u4MepIndex, pMefFrameDelay->i4DmCount)
        == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepTransmitDmInterval (u4ContextId, u4MdIndex, u4MaIndex,
                                          u4MepIndex,
                                          pMefFrameDelay->i4DmInterval) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepTransmitDmDeadline (u4ContextId, u4MdIndex, u4MaIndex,
                                          u4MepIndex,
                                          pMefFrameDelay->i4DmDeadLine) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepTransmitDmStatus (u4ContextId, u4MdIndex, u4MaIndex,
                                        u4MepIndex, pMefFrameDelay->u4DmState)
        == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliConfigAvailability (tCliHandle CliHandle, tMefAvailable * pMefAvailable)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4ContextId = pMefAvailable->u4ContextId;
    UINT4               u4MdIndex = pMefAvailable->u4MdIndex;
    UINT4               u4MaIndex = pMefAvailable->u4MaIndex;
    UINT4               u4MepIndex = pMefAvailable->u4MepIndex;
    INT4                i4RowStatus = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhValidateIndexInstanceFsMefMepTable (u4ContextId, u4MdIndex,
                                               u4MaIndex,
                                               u4MepIndex) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_MEP_NOT_EXIST);
        return CLI_FAILURE;
    }

    if (nmhGetFsMefMepAvailabilityRowStatus (u4ContextId, u4MdIndex,
                                             u4MaIndex, u4MepIndex,
                                             &i4RowStatus) != SNMP_SUCCESS)
    {
        if (nmhTestv2FsMefMepAvailabilityRowStatus (&u4ErrorCode, u4ContextId,
                                                    u4MdIndex, u4MaIndex,
                                                    u4MepIndex,
                                                    CREATE_AND_WAIT) ==
            SNMP_SUCCESS)
        {
            if (nmhSetFsMefMepAvailabilityRowStatus (u4ContextId, u4MdIndex,
                                                     u4MaIndex, u4MepIndex,
                                                     CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }
    if (pMefAvailable->u4AvailabileState == MEF_TX_STATUS_STOP)
    {
        if (nmhTestv2FsMefMepAvailabilityStatus (&u4ErrorCode, u4ContextId,
                                                 u4MdIndex, u4MaIndex,
                                                 u4MepIndex, MEF_TX_STATUS_STOP)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepAvailabilityStatus (u4ContextId, u4MdIndex,
                                              u4MaIndex, u4MepIndex,
                                              MEF_TX_STATUS_STOP)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
    if (nmhTestv2FsMefMepAvailabilityRowStatus (&u4ErrorCode, u4ContextId,
                                                u4MdIndex, u4MaIndex,
                                                u4MepIndex,
                                                NOT_IN_SERVICE) == SNMP_SUCCESS)
    {
        if (nmhSetFsMefMepAvailabilityRowStatus (u4ContextId, u4MdIndex,
                                                 u4MaIndex, u4MepIndex,
                                                 NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        return CLI_FAILURE;
    }

    if (pMefAvailable->bIsMepId == OSIX_TRUE)
    {
        if (nmhTestv2FsMefMepAvailabilityDestIsMepId (&u4ErrorCode, u4ContextId,
                                                      u4MdIndex, u4MaIndex,
                                                      u4MepIndex,
                                                      MEF_SNMP_TRUE) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepAvailabilityDestIsMepId (u4ContextId, u4MdIndex,
                                                   u4MaIndex, u4MepIndex,
                                                   MEF_SNMP_TRUE)
            == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMefMepAvailabilityDestMepId (&u4ErrorCode, u4ContextId,
                                                    u4MdIndex, u4MaIndex,
                                                    u4MepIndex,
                                                    pMefAvailable->u4RmepId) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepAvailabilityDestMepId (u4ContextId, u4MdIndex,
                                                 u4MaIndex, u4MepIndex,
                                                 pMefAvailable->u4RmepId)
            == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsMefMepAvailabilityDestIsMepId (&u4ErrorCode, u4ContextId,
                                                      u4MdIndex, u4MaIndex,
                                                      u4MepIndex,
                                                      MEF_SNMP_FALSE) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepAvailabilityDestIsMepId (u4ContextId, u4MdIndex,
                                                   u4MaIndex, u4MepIndex,
                                                   MEF_SNMP_FALSE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMefMepAvailabilityDestMacAddress (&u4ErrorCode,
                                                         u4ContextId, u4MdIndex,
                                                         u4MaIndex, u4MepIndex,
                                                         pMefAvailable->
                                                         RmepMacAddr) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepAvailabilityDestMacAddress (u4ContextId, u4MdIndex,
                                                      u4MaIndex, u4MepIndex,
                                                      pMefAvailable->
                                                      RmepMacAddr) ==
            SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMefMepAvailabilityWindowSize (&u4ErrorCode, u4ContextId,
                                                 u4MdIndex, u4MaIndex,
                                                 u4MepIndex,
                                                 pMefAvailable->
                                                 u4AvailWindowSize) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepAvailabilityWindowSize (u4ContextId, u4MdIndex, u4MaIndex,
                                              u4MepIndex,
                                              pMefAvailable->u4AvailWindowSize)
        == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    GET_AVAILABILITY_INTERVAL (pMefAvailable->i4AvailInterval);
    if (pMefAvailable->i4AvailInterval == MEF_AVLBLTY_MAX_INTERVALS)
    {
        /* Not a valid value */
        CLI_SET_ERR (MEF_CLI_INVALID_AVAILABILITY_INTERVAL_ERR);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepAvailabilityInterval (&u4ErrorCode, u4ContextId,
                                               u4MdIndex, u4MaIndex, u4MepIndex,
                                               pMefAvailable->i4AvailInterval)
        == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepAvailabilityInterval (u4ContextId, u4MdIndex, u4MaIndex,
                                            u4MepIndex,
                                            pMefAvailable->i4AvailInterval) ==
        SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepAvailabilityDeadline
        (&u4ErrorCode, u4ContextId, u4MdIndex, u4MaIndex,
         u4MepIndex, pMefAvailable->i4AvailDeadLine) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepAvailabilityDeadline
        (u4ContextId, u4MdIndex, u4MaIndex,
         u4MepIndex, pMefAvailable->i4AvailDeadLine) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (pMefAvailable->LowThreshold.pu1_OctetList != NULL)
    {
        if (nmhTestv2FsMefMepAvailabilityLowerThreshold
            (&u4ErrorCode, u4ContextId, u4MdIndex, u4MaIndex,
             u4MepIndex, &(pMefAvailable->LowThreshold)) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepAvailabilityLowerThreshold
            (u4ContextId, u4MdIndex, u4MaIndex,
             u4MepIndex, &(pMefAvailable->LowThreshold)) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
    }

    if (pMefAvailable->UpperThreshold.pu1_OctetList != NULL)
    {
        if (nmhTestv2FsMefMepAvailabilityUpperThreshold
            (&u4ErrorCode, u4ContextId, u4MdIndex, u4MaIndex,
             u4MepIndex, &(pMefAvailable->UpperThreshold)) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepAvailabilityUpperThreshold
            (u4ContextId, u4MdIndex, u4MaIndex,
             u4MepIndex, &(pMefAvailable->UpperThreshold)) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMefMepAvailabilitySchldDownInitTime
        (&u4ErrorCode, u4ContextId, u4MdIndex, u4MaIndex,
         u4MepIndex, pMefAvailable->u4DownTimeStart) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepAvailabilitySchldDownInitTime
        (u4ContextId, u4MdIndex, u4MaIndex,
         u4MepIndex, pMefAvailable->u4DownTimeStart) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepAvailabilitySchldDownEndTime
        (&u4ErrorCode, u4ContextId, u4MdIndex, u4MaIndex,
         u4MepIndex, pMefAvailable->u4DownTimeEnd) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepAvailabilitySchldDownEndTime
        (u4ContextId, u4MdIndex, u4MaIndex,
         u4MepIndex, pMefAvailable->u4DownTimeEnd) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (pMefAvailable->bIsModestAreaAvail == CLI_TRUE)
    {
        if (nmhTestv2FsMefMepAvailabilityModestAreaIsAvailable
            (&u4ErrorCode, u4ContextId, u4MdIndex, u4MaIndex,
             u4MepIndex, MEF_SNMP_TRUE) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepAvailabilityModestAreaIsAvailable
            (u4ContextId, u4MdIndex, u4MaIndex,
             u4MepIndex, MEF_SNMP_TRUE) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2FsMefMepAvailabilityModestAreaIsAvailable
            (&u4ErrorCode, u4ContextId, u4MdIndex, u4MaIndex,
             u4MepIndex, MEF_SNMP_FALSE) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMefMepAvailabilityModestAreaIsAvailable
            (u4ContextId, u4MdIndex, u4MaIndex,
             u4MepIndex, MEF_SNMP_FALSE) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMefMepAvailabilityType
        (&u4ErrorCode, u4ContextId, u4MdIndex, u4MaIndex,
         u4MepIndex, pMefAvailable->i4AvailAlgoType) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepAvailabilityType
        (u4ContextId, u4MdIndex, u4MaIndex,
         u4MepIndex, pMefAvailable->i4AvailAlgoType) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepAvailabilityRowStatus
        (&u4ErrorCode, u4ContextId, u4MdIndex, u4MaIndex,
         u4MepIndex, ACTIVE) == SNMP_SUCCESS)
    {
        if (nmhSetFsMefMepAvailabilityRowStatus
            (u4ContextId, u4MdIndex, u4MaIndex,
             u4MepIndex, ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMefMepAvailabilityStatus
        (&u4ErrorCode, u4ContextId, u4MdIndex, u4MaIndex,
         u4MepIndex, MEF_TX_STATUS_START) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMefMepAvailabilityStatus
        (u4ContextId, u4MdIndex, u4MaIndex, u4MepIndex,
         MEF_TX_STATUS_START) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliShowFrameLoss (tCliHandle CliHandle, UINT4 u4ContextId,
                     UINT4 *pu4MdIndex, UINT4 *pu4MaIndex, UINT4 *pu4MepIndex)
{
    CHR1                ac1TimeStr[TIME_STR_BUF_SIZE];
    UINT1               au1MacAddr[MEF_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    tMacAddr            PeerMac;
    UINT4               u4MdIndex = *pu4MdIndex;
    UINT4               u4MaIndex = *pu4MaIndex;
    UINT4               u4MepIndex = *pu4MepIndex;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextMdIndex = 0;
    UINT4               u4NextMaIndex = 0;
    UINT4               u4NextMepIndex = 0;
    UINT4               u4TransId = 0;
    UINT4               u4NextTransId = 0;
    UINT4               u4FlAvg = 0;
    UINT4               u4FlMax = 0;
    UINT4               u4FlMin = 0;
    UINT4               u4Time = 0;

    MEMSET (PeerMac, 0, sizeof (tMacAddr));
    VcmGetAliasName (u4ContextId, au1ContextName);
    CliPrintf (CliHandle, "\r\nSwitch %s \r\n", au1ContextName);

    while (nmhGetNextIndexFsMefFlStatsTable (u4ContextId, &u4NextContextId,
                                             u4MdIndex, &u4NextMdIndex,
                                             u4MaIndex, &u4NextMaIndex,
                                             u4MepIndex, &u4NextMepIndex,
                                             u4TransId, &u4NextTransId)
           == SNMP_SUCCESS)
    {

        if ((u4MdIndex != u4NextMdIndex) || (u4MaIndex != u4NextMaIndex) ||
            (u4MepIndex != u4NextMepIndex) || (u4ContextId != u4NextContextId))
        {
            return CLI_SUCCESS;
        }

        /* Assuming that in a transaction at least Sequence number 1 
           of Frame loss frames are sent. To print the MAC address
           of Peer MEP pass the Sequesnce umber as 1 */
        nmhGetFsMefFlPeerMepMacAddress (u4NextContextId, u4NextMdIndex,
                                        u4NextMaIndex, u4NextMepIndex,
                                        u4NextTransId, 1, &PeerMac);
        PrintMacAddress (PeerMac, au1MacAddr);

        nmhGetFsMefFlStatsTimeStamp (u4NextContextId, u4NextMdIndex,
                                     u4NextMaIndex, u4NextMepIndex,
                                     u4NextTransId, &u4Time);
        UtlGetTimeStrForTicks ((u4Time * SYS_NUM_OF_TIME_UNITS_IN_A_SEC),
                               ac1TimeStr);

        CliPrintf (CliHandle, "\r\nFrame Loss Statistic of MAC %s "
                   "at Time = %s \r\n", au1MacAddr, ac1TimeStr);

        nmhGetFsMefFlStatsNearEndLossMax (u4NextContextId, u4NextMdIndex,
                                          u4NextMaIndex, u4NextMepIndex,
                                          u4NextTransId, &u4FlMax);

        nmhGetFsMefFlStatsNearEndLossAverage (u4NextContextId, u4NextMdIndex,
                                              u4NextMaIndex, u4NextMepIndex,
                                              u4NextTransId, &u4FlAvg);

        nmhGetFsMefFlStatsNearEndLossMin (u4NextContextId, u4NextMdIndex,
                                          u4NextMaIndex, u4NextMepIndex,
                                          u4NextTransId, &u4FlMin);

        CliPrintf (CliHandle, "Near-end loss max/avg/min = %u/%u/%u \r\n",
                   u4FlMax, u4FlAvg, u4FlMin);

        nmhGetFsMefFlStatsFarEndLossMax (u4NextContextId, u4NextMdIndex,
                                         u4NextMaIndex, u4NextMepIndex,
                                         u4NextTransId, &u4FlMax);

        nmhGetFsMefFlStatsFarEndLossAverage (u4NextContextId, u4NextMdIndex,
                                             u4NextMaIndex, u4NextMepIndex,
                                             u4NextTransId, &u4FlAvg);

        nmhGetFsMefFlStatsFarEndLossMin (u4NextContextId, u4NextMdIndex,
                                         u4NextMaIndex, u4NextMepIndex,
                                         u4NextTransId, &u4FlMin);

        CliPrintf (CliHandle, "Far-end loss max/avg/min = %u/%u/%u \r\n\r\n",
                   u4FlMax, u4FlAvg, u4FlMin);

        u4ContextId = u4NextContextId;
        u4MdIndex = u4NextMdIndex;
        u4MaIndex = u4NextMaIndex;
        u4MepIndex = u4NextMepIndex;
        u4TransId = u4NextTransId;
    }

    return CLI_SUCCESS;
}

INT4
MefCliShowFrameDelay (tCliHandle CliHandle, UINT4 u4ContextId,
                      UINT4 *pu4MdIndex, UINT4 *pu4MaIndex, UINT4 *pu4MepIndex)
{
    CHR1                ac1TimeStr[TIME_STR_BUF_SIZE];
    UINT1               au1MacAddr[MEF_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    /* The following variables are declared to store float values using sprintf.
     * Float Size = <10 digits>.<6 digits> => Array size = 10 + 6 + 1 + 1.
     * where 10 digits for before dot notation.
     *        6 digits for after dot notation.
     *        1 digit for dot.
     *        1 digit for ending the array with NULL. */
    UINT1               au1DelayMin[MEF_CLI_FLOAT_STORAGE_SIZE] = { 0 };
    UINT1               au1DelayMax[MEF_CLI_FLOAT_STORAGE_SIZE] = { 0 };
    UINT1               au1DelayAvg[MEF_CLI_FLOAT_STORAGE_SIZE] = { 0 };
    UINT1               au1IFDVAvg[MEF_CLI_FLOAT_STORAGE_SIZE] = { 0 };
    UINT1               au1FDVAvg[MEF_CLI_FLOAT_STORAGE_SIZE] = { 0 };
    tMacAddr            PeerMac;
    UINT4               u4MdIndex = *pu4MdIndex;
    UINT4               u4MaIndex = *pu4MaIndex;
    UINT4               u4MepIndex = *pu4MepIndex;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextMdIndex = 0;
    UINT4               u4NextMaIndex = 0;
    UINT4               u4NextMepIndex = 0;
    UINT4               u4TransId = 0;
    UINT4               u4NextTransId = 0;
    UINT4               u4Time = 0;
    UINT4               u4Sec = 0;
    UINT4               u4NanoSec = 0;
    FLT4                f4MSec = 0.0;
    tSNMP_OCTET_STRING_TYPE FdAvg;
    tSNMP_OCTET_STRING_TYPE FdMax;
    tSNMP_OCTET_STRING_TYPE FdMin;
    tSNMP_OCTET_STRING_TYPE Ifdv;
    tSNMP_OCTET_STRING_TYPE Fdv;

    MEMSET (PeerMac, 0, sizeof (tMacAddr));
    FdMax.pu1_OctetList = au1DelayMax;
    FdMax.i4_Length = sizeof (au1DelayMax);
    FdMin.pu1_OctetList = au1DelayMin;
    FdMin.i4_Length = sizeof (au1DelayMin);
    FdAvg.pu1_OctetList = au1DelayAvg;
    FdMin.i4_Length = sizeof (au1DelayAvg);
    Ifdv.pu1_OctetList = au1IFDVAvg;
    FdMin.i4_Length = sizeof (au1IFDVAvg);
    Fdv.pu1_OctetList = au1FDVAvg;
    FdMin.i4_Length = sizeof (au1FDVAvg);

    VcmGetAliasName (u4ContextId, au1ContextName);
    CliPrintf (CliHandle, "\r\nSwitch %s \r\n", au1ContextName);

    while (nmhGetNextIndexFsMefFdStatsTable (u4ContextId, &u4NextContextId,
                                             u4MdIndex, &u4NextMdIndex,
                                             u4MaIndex, &u4NextMaIndex,
                                             u4MepIndex, &u4NextMepIndex,
                                             u4TransId, &u4NextTransId)
           == SNMP_SUCCESS)
    {
        if ((u4MdIndex != u4NextMdIndex) || (u4MaIndex != u4NextMaIndex) ||
            (u4MepIndex != u4NextMepIndex) || (u4ContextId != u4NextContextId))
        {
            return CLI_SUCCESS;
        }

        /* Assuming that in a transaction at least Sequence number 1 
           of Frame Delay frames are sent. To print the MAC address
           of Peer MEP pass the Sequesnce number as 1 */
        nmhGetFsMefFdPeerMepMacAddress (u4NextContextId, u4NextMdIndex,
                                        u4NextMaIndex, u4NextMepIndex,
                                        u4NextTransId, 1, &PeerMac);
        PrintMacAddress (PeerMac, au1MacAddr);

        nmhGetFsMefFdStatsTimeStamp (u4NextContextId, u4NextMdIndex,
                                     u4NextMaIndex, u4NextMepIndex,
                                     u4NextTransId, &u4Time);
        UtlGetTimeStrForTicks ((u4Time * SYS_NUM_OF_TIME_UNITS_IN_A_SEC),
                               ac1TimeStr);

        CliPrintf (CliHandle, "\r\n Frame Dely Statistic of MAC %s "
                   "at Time = %s \r\n", au1MacAddr, ac1TimeStr);

        nmhGetFsMefFdStatsDelayMax (u4NextContextId, u4NextMdIndex,
                                    u4NextMaIndex, u4NextMepIndex,
                                    u4NextTransId, &FdMax);

        MEF_GET_4BYTE (u4Sec, FdMax.pu1_OctetList);
        MEF_GET_4BYTE (u4NanoSec, FdMax.pu1_OctetList);
        f4MSec = (((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000);
        MEMSET (au1DelayMax, 0, sizeof (au1DelayMax));
        SPRINTF ((CHR1 *) au1DelayMax, "%f", f4MSec);

        nmhGetFsMefFdStatsDelayAverage (u4NextContextId, u4NextMdIndex,
                                        u4NextMaIndex, u4NextMepIndex,
                                        u4NextTransId, &FdAvg);
        MEF_GET_4BYTE (u4Sec, FdAvg.pu1_OctetList);
        MEF_GET_4BYTE (u4NanoSec, FdAvg.pu1_OctetList);
        f4MSec = (((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000);
        MEMSET (au1DelayAvg, 0, sizeof (au1DelayAvg));
        SPRINTF ((CHR1 *) au1DelayAvg, "%f", f4MSec);

        nmhGetFsMefFdStatsDelayMin (u4NextContextId, u4NextMdIndex,
                                    u4NextMaIndex, u4NextMepIndex,
                                    u4NextTransId, &FdMin);
        MEF_GET_4BYTE (u4Sec, FdMin.pu1_OctetList);
        MEF_GET_4BYTE (u4NanoSec, FdMin.pu1_OctetList);
        f4MSec = (((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000);
        MEMSET (au1DelayMin, 0, sizeof (au1DelayMin));
        SPRINTF ((CHR1 *) au1DelayMin, "%f", f4MSec);

        CliPrintf (CliHandle, "Delay min/avg/max = %s ms/%s ms/%s ms,\r\n",
                   au1DelayMin, au1DelayAvg, au1DelayMax);

        nmhGetFsMefFdStatsIFDVAverage (u4NextContextId, u4NextMdIndex,
                                       u4NextMaIndex, u4NextMepIndex,
                                       u4NextTransId, &Ifdv);
        MEF_GET_4BYTE (u4Sec, Ifdv.pu1_OctetList);
        MEF_GET_4BYTE (u4NanoSec, Ifdv.pu1_OctetList);
        f4MSec = (((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000);
        MEMSET (au1IFDVAvg, 0, sizeof (au1IFDVAvg));
        SPRINTF ((CHR1 *) au1IFDVAvg, "%f", f4MSec);

        nmhGetFsMefFdStatsFDVAverage (u4NextContextId, u4NextMdIndex,
                                      u4NextMaIndex, u4NextMepIndex,
                                      u4NextTransId, &Fdv);
        MEF_GET_4BYTE (u4Sec, Fdv.pu1_OctetList);
        MEF_GET_4BYTE (u4NanoSec, Fdv.pu1_OctetList);
        f4MSec = (((FLT4) u4Sec) * 1000) + (((FLT4) u4NanoSec) / 1000000);
        MEMSET (au1FDVAvg, 0, sizeof (au1FDVAvg));
        SPRINTF ((CHR1 *) au1FDVAvg, "%f", f4MSec);

        CliPrintf (CliHandle,
                   "Delay variation avg IFDV/FDV  = %s ms/%s ms \r\n\r\n",
                   au1IFDVAvg, au1FDVAvg);

        u4ContextId = u4NextContextId;
        u4MdIndex = u4NextMdIndex;
        u4MaIndex = u4NextMaIndex;
        u4MepIndex = u4NextMepIndex;
        u4TransId = u4NextTransId;
    }

    return CLI_SUCCESS;
}

INT4
MefCliShowAvailability (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 *pu4MdIndex, UINT4 *pu4MaIndex,
                        UINT4 *pu4MepIndex)
{
    tSNMP_OCTET_STRING_TYPE Percentage;
    /* The following variables are declared to store float values using sprintf.
     * Float Size = <10 digits>.<6 digits> => Array size = 10 + 6 + 1 + 1.
     * where 10 digits for before dot notation.
     *        6 digits for after dot notation.
     *        1 digit for dot.
     *        1 digit for ending the array with NULL. */

    UINT1               au1Percentage[MEF_CLI_FLOAT_STORAGE_SIZE] = { 0 };
    UINT1               au1MacAddr[MEF_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    tMacAddr            PeerMac;
    UINT4               u4RmepId = 0;
    UINT4               u4MdIndex = *pu4MdIndex;
    UINT4               u4MaIndex = *pu4MaIndex;
    UINT4               u4MepIndex = *pu4MepIndex;
    INT4                i4IsMepId = 0;

    MEMSET (au1Percentage, 0, sizeof (au1Percentage));
    Percentage.pu1_OctetList = au1Percentage;
    Percentage.i4_Length = sizeof (au1Percentage);

    VcmGetAliasName (u4ContextId, au1ContextName);
    CliPrintf (CliHandle, "\r\nSwitch %s \r\n", au1ContextName);

    if (nmhGetFsMefMepAvailabilityPercentage (u4ContextId, u4MdIndex,
                                              u4MaIndex, u4MepIndex,
                                              &Percentage) == SNMP_SUCCESS)
    {

        nmhGetFsMefMepAvailabilityDestIsMepId (u4ContextId, u4MdIndex,
                                               u4MaIndex, u4MepIndex,
                                               &i4IsMepId);
        if (i4IsMepId == MEF_SNMP_TRUE)
        {
            nmhGetFsMefMepAvailabilityDestMepId (u4ContextId, u4MdIndex,
                                                 u4MaIndex, u4MepIndex,
                                                 &u4RmepId);

            CliPrintf (CliHandle, "\r\nAvailability Percentage of "
                       "RMEP-ID %u : %s \r\n", u4RmepId, au1Percentage);

        }
        else
        {
            nmhGetFsMefMepAvailabilityDestMacAddress (u4ContextId, u4MdIndex,
                                                      u4MaIndex, u4MepIndex,
                                                      &PeerMac);
            PrintMacAddress (PeerMac, au1MacAddr);

            CliPrintf (CliHandle, "\r\nAvailability Percentage of MAC %s : "
                       "%s \r\n", au1MacAddr, Percentage.pu1_OctetList);
        }
    }

    return CLI_SUCCESS;
}

INT4
MefCliShowFilterInfo (tCliHandle CliHandle, UINT4 *pu4FilterNo)
{
    INT4                i4FilterNo = 0;
    INT4                i4NextFilterNo = 0;

    if (pu4FilterNo != NULL)
    {
        MEMCPY (&i4FilterNo, pu4FilterNo, sizeof (INT4));
        return MefCliUtilPrintFilterInfo (CliHandle, i4FilterNo);
    }

    while (nmhGetNextIndexFsMefFilterTable (i4FilterNo, &i4NextFilterNo)
           == SNMP_SUCCESS)
    {
        if (MefCliUtilPrintFilterInfo (CliHandle, i4NextFilterNo)
            == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        i4FilterNo = i4NextFilterNo;
    }
    return CLI_SUCCESS;
}

INT4
MefCliUtilPrintFilterInfo (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4Direction = 0;
    INT4                i4IfIndex = 0;
    INT4                i4Priority = 0;
    INT4                i4Dscp = 0;
    INT4                i4Evc = 0;
    INT4                i4Status = 0;

    if (nmhValidateIndexInstanceFsMefFilterTable (i4FilterNo) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
        return CLI_FAILURE;
    }

    nmhGetFsMefFilterDirection (i4FilterNo, &i4Direction);
    nmhGetFsMefFilterIfIndex (i4FilterNo, &i4IfIndex);
    nmhGetFsMefFilterCVlanPriority (i4FilterNo, &i4Priority);
    nmhGetFsMefFilterDscp (i4FilterNo, &i4Dscp);
    nmhGetFsMefFilterEvc (i4FilterNo, &i4Evc);
    nmhGetFsMefFilterStatus (i4FilterNo, &i4Status);
    MEMSET (ai1IfName, 0, sizeof (ai1IfName));
    CfaCliGetIfName (i4IfIndex, ai1IfName);

    CliPrintf (CliHandle, "\r\n %-25s : %d\r\n", "Filter No", i4FilterNo);
    CliPrintf (CliHandle, " %-25s : %s\r\n", "Interface", ai1IfName);

    if (i4Direction == ISS_DIRECTION_IN)
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Direction", "In");
    }
    else
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Direction", "Out");
    }

    CliPrintf (CliHandle, " %-25s : %d\r\n", "C-VLAN priority", i4Priority);

    CliPrintf (CliHandle, " %-25s : %d\r\n", "EVC", i4Evc);

    if (i4Dscp != -1)
    {
        CliPrintf (CliHandle, " %-25s : %d\r\n", "Dscp", i4Dscp);
    }
    else
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Dscp", "NONE");
    }

    if (i4Status == ACTIVE)
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Status", "Active");
    }
    else
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Status", "Not Active");
    }

    return CLI_SUCCESS;
}

INT4
MefCliShowClassMapInfo (tCliHandle CliHandle, UINT4 *pu4ClassMapId)
{
    UINT4               u4ClassMapId = 0;
    UINT4               u4NextClassMapId = 0;

    if (pu4ClassMapId != NULL)
    {
        return MefCliUtilPrintClassMapInfo (CliHandle, *pu4ClassMapId);
    }

    while (nmhGetNextIndexFsMefClassMapTable (u4ClassMapId, &u4NextClassMapId)
           == SNMP_SUCCESS)
    {
        if (MefCliUtilPrintClassMapInfo (CliHandle, u4NextClassMapId)
            == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        u4ClassMapId = u4NextClassMapId;
    }
    return CLI_SUCCESS;
}

INT4
MefCliUtilPrintClassMapInfo (tCliHandle CliHandle, UINT4 u4ClassMapId)
{
    tSNMP_OCTET_STRING_TYPE OctStrName;
    UINT1               au1ClassMapName[MEF_MAX_NAME_LENGTH + 1];
    UINT4               u4FilterId = 0;
    UINT4               u4Class = 0;
    INT4                i4Status = 0;

    MEMSET (au1ClassMapName, 0, MEF_MAX_NAME_LENGTH + 1);

    OctStrName.pu1_OctetList = au1ClassMapName;
    OctStrName.i4_Length = MEF_MAX_NAME_LENGTH;

    if (nmhValidateIndexInstanceFsMefClassMapTable (u4ClassMapId)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    nmhGetFsMefClassMapName (u4ClassMapId, &OctStrName);
    nmhGetFsMefClassMapFilterId (u4ClassMapId, &u4FilterId);
    nmhGetFsMefClassMapCLASS (u4ClassMapId, &u4Class);
    nmhGetFsMefClassMapStatus (u4ClassMapId, &i4Status);

    CliPrintf (CliHandle, "\r\n %-25s : %u\r\n", "ClassMap Id", u4ClassMapId);
    CliPrintf (CliHandle, " %-25s : %s\r\n", "ClassMap Name",
               OctStrName.pu1_OctetList);
    CliPrintf (CliHandle, " %-25s : %u\r\n", "Filter Id", u4FilterId);
    CliPrintf (CliHandle, " %-25s : %u\r\n", "Class", u4Class);
    if (i4Status == ACTIVE)
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Status", "Active");
    }
    else
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Status", "Not Active");
    }

    return CLI_SUCCESS;
}

INT4
MefCliShowClassInfo (tCliHandle CliHandle, UINT4 *pu4Class)
{
    UINT4               u4Class = 0;
    UINT4               u4NextClass = 0;

    if (pu4Class != NULL)
    {
        return MefCliUtilPrintClassInfo (CliHandle, *pu4Class);
    }

    while (nmhGetNextIndexFsMefClassTable (u4Class, &u4NextClass)
           == SNMP_SUCCESS)
    {
        if (MefCliUtilPrintClassInfo (CliHandle, u4NextClass) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        u4Class = u4NextClass;
    }
    return CLI_SUCCESS;
}

INT4
MefCliUtilPrintClassInfo (tCliHandle CliHandle, UINT4 u4Class)
{
    INT4                i4Status = 0;

    if (nmhValidateIndexInstanceFsMefClassTable (u4Class) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    nmhGetFsMefClassStatus (u4Class, &i4Status);

    CliPrintf (CliHandle, "\r\n %-25s : %u\r\n", "Class", u4Class);

    if (i4Status == ACTIVE)
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Status", "Active");
    }
    else
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Status", "Not Active");
    }

    return CLI_SUCCESS;
}

INT4
MefCliShowMeterInfo (tCliHandle CliHandle, UINT4 *pu4MeterId)
{
    UINT4               u4MeterId = 0;
    UINT4               u4NextMeterId = 0;

    if (pu4MeterId != NULL)
    {
        return MefCliUtilPrintMeterInfo (CliHandle, *pu4MeterId);
    }

    while (nmhGetNextIndexFsMefMeterTable (u4MeterId, &u4NextMeterId)
           == SNMP_SUCCESS)
    {
        if (MefCliUtilPrintMeterInfo (CliHandle, u4NextMeterId) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        u4MeterId = u4NextMeterId;
    }
    return CLI_SUCCESS;
}

INT4
MefCliUtilPrintMeterInfo (tCliHandle CliHandle, UINT4 u4MeterId)
{
    tSNMP_OCTET_STRING_TYPE OctStrName;
    UINT1               au1MeterName[MEF_MAX_NAME_LENGTH + 1];
    INT4                i4CouplingFlag = 0;
    INT4                i4Status = 0;
    INT4                i4ClorMode = 0;
    UINT4               u4CIR = 0;
    UINT4               u4CBS = 0;
    UINT4               u4EIR = 0;
    UINT4               u4EBS = 0;

    MEMSET (au1MeterName, 0, MEF_MAX_NAME_LENGTH + 1);
    OctStrName.pu1_OctetList = au1MeterName;
    OctStrName.i4_Length = MEF_MAX_NAME_LENGTH;

    if (nmhValidateIndexInstanceFsMefMeterTable (u4MeterId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    nmhGetFsMefMeterName (u4MeterId, &OctStrName);
    nmhGetFsMefMeterType (u4MeterId, &i4CouplingFlag);
    nmhGetFsMefMeterColorMode (u4MeterId, &i4ClorMode);
    nmhGetFsMefMeterCIR (u4MeterId, &u4CIR);
    nmhGetFsMefMeterCBS (u4MeterId, &u4CBS);
    nmhGetFsMefMeterEIR (u4MeterId, &u4EIR);
    nmhGetFsMefMeterEBS (u4MeterId, &u4EBS);
    nmhGetFsMefMeterStatus (u4MeterId, &i4Status);

    CliPrintf (CliHandle, "\r\n %-25s : %u\r\n", "Meter Id", u4MeterId);
    CliPrintf (CliHandle, " %-25s : %s\r\n", "Meter Name",
               OctStrName.pu1_OctetList);

    if (i4CouplingFlag == QOS_METER_TYPE_MEF_COUPLED)
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Coupling flag", "Coupled");
    }
    else if (i4CouplingFlag == QOS_METER_TYPE_MEF_DECOUPLED)
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Coupling flag", "DeCoupled");
    }

    CliPrintf (CliHandle, " %-25s : %u\r\n", "CIR", u4CIR);
    CliPrintf (CliHandle, " %-25s : %u\r\n", "CBS", u4CBS);
    CliPrintf (CliHandle, " %-25s : %u\r\n", "EIR", u4EIR);
    CliPrintf (CliHandle, " %-25s : %u\r\n", "EBS", u4EBS);

    if (i4Status == ACTIVE)
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Status", "Active");
    }
    else
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Status", "Not Active");
    }
    return CLI_SUCCESS;
}

INT4
MefCliShowPolicyMapInfo (tCliHandle CliHandle, UINT4 *pu4PolicyMapId)
{
    UINT4               u4PolicyMapId = 0;
    UINT4               u4NextPolicyMapId = 0;

    if (pu4PolicyMapId != NULL)
    {
        return MefCliUtilPrintPolicyMapInfo (CliHandle, *pu4PolicyMapId);
    }

    while (nmhGetNextIndexFsMefPolicyMapTable
           (u4PolicyMapId, &u4NextPolicyMapId) == SNMP_SUCCESS)
    {
        if (MefCliUtilPrintPolicyMapInfo (CliHandle, u4NextPolicyMapId)
            == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        u4PolicyMapId = u4NextPolicyMapId;
    }

    return CLI_SUCCESS;
}

INT4
MefCliUtilPrintPolicyMapInfo (tCliHandle CliHandle, UINT4 u4PolicyMapId)
{
    tSNMP_OCTET_STRING_TYPE OctStrName;
    UINT1               au1PolicyName[MEF_MAX_NAME_LENGTH + 1];
    UINT4               u4Class = 0;
    UINT4               u4MeterTableId = 0;
    INT4                i4Status = 0;

    MEMSET (au1PolicyName, 0, MEF_MAX_NAME_LENGTH + 1);
    OctStrName.pu1_OctetList = au1PolicyName;
    OctStrName.i4_Length = MEF_MAX_NAME_LENGTH;

    nmhGetFsMefPolicyMapName (u4PolicyMapId, &OctStrName);
    nmhGetFsMefPolicyMapCLASS (u4PolicyMapId, &u4Class);
    nmhGetFsMefPolicyMapMeterTableId (u4PolicyMapId, &u4MeterTableId);
    nmhGetFsMefPolicyMapStatus (u4PolicyMapId, &i4Status);

    CliPrintf (CliHandle, "\r\n %-25s : %u\r\n", "PolicyMap Id", u4PolicyMapId);
    CliPrintf (CliHandle, " %-25s : %s\r\n", "Policy Name",
               OctStrName.pu1_OctetList);
    CliPrintf (CliHandle, " %-25s : %u\r\n", "Class", u4Class);
    CliPrintf (CliHandle, " %-25s : %u\r\n", "Meter Table Id", u4MeterTableId);

    if (i4Status == ACTIVE)
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Status", "Active");
    }
    else
    {
        CliPrintf (CliHandle, " %-25s : %s\r\n", "Status", "Not Active");
    }

    return CLI_SUCCESS;
}

INT4
MefCliShowMefInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{

    tSNMP_OCTET_STRING_TYPE MefContextName;
    UINT1               u1TempName[MEF_MAX_NAME_LENGTH];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    INT4                i4MefTransmode = 0;
    INT4                i4FrameLossBufferSize = 0;
    INT4                i4FrameDelayBufferSize = 0;

    MEMSET (&MefContextName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (u1TempName, 0, MEF_MAX_NAME_LENGTH);
    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);

    VcmGetAliasName (u4ContextId, au1ContextName);
    CliPrintf (CliHandle, "\r\n\r Switch %s \r\n", au1ContextName);

    CliPrintf (CliHandle, "\nMef Info \r\n");
    CliPrintf (CliHandle, "--------\r\n");

    MefContextName.pu1_OctetList = u1TempName;
    MefContextName.i4_Length = MEF_MAX_NAME_LENGTH;

    nmhGetFsMefContextName (u4ContextId, &MefContextName);

    if (MefContextName.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Mef Context Name  : %s\r\n",
                   MefContextName.pu1_OctetList);
    }

    CliPrintf (CliHandle, "Bridge Mode       : ");
    nmhGetFsMefTransmode (u4ContextId, &i4MefTransmode);
    if (i4MefTransmode == MEF_TRANS_MODE_PB)
    {
        CliPrintf (CliHandle, "Provider Bridge \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "MPLS \r\n");
    }

    nmhGetFsMefFrameLossBufferSize (u4ContextId, &i4FrameLossBufferSize);
    CliPrintf (CliHandle, "Mef Frame Loss Buffer Size   : %d \r\n",
               i4FrameLossBufferSize);

    nmhGetFsMefFrameDelayBufferSize (u4ContextId, &i4FrameDelayBufferSize);
    CliPrintf (CliHandle, "Mef Frame Delay Buffer Size  : %d \r\n",
               i4FrameDelayBufferSize);

    return CLI_SUCCESS;
}

INT4
MefCliShowMefEtree (tCliHandle CliHandle, UINT4 *pu4PortType, UINT4 *pu4Port)
{
    UINT4               u4IfType = 0;
    UINT4               u4Port = 0;
    UINT4               u4Count = 0;

    if ((pu4PortType != NULL) && (pu4Port != NULL))
    {
        if (CfaCliValidateXInterfaceName ((INT1 *) pu4PortType, NULL,
                                          &u4IfType) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid port\r\n");
            return CLI_FAILURE;
        }

        if (MefCliUtilConvertStrToPortArray (CliHandle, (UINT1 *) pu4Port,
                                             &u4Port, &u4Count, 1, u4IfType)
            == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    return MefCliShowEtreeInfo (CliHandle, u4Port);
}

INT4
MefCliShowEtreeInfo (tCliHandle CliHandle, INT4 i4IngressPort)
{
    UINT4               au4EgressPorts[MEF_ETREE_MAX_LEAF_UNIS];
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Scan = 0;
    UINT4               u4Temp = 0;
    INT4                i4IngressIndex = 0;
    INT4                i4NextIngressIndex = 0;
    INT4                i4EgressIndex = 0;
    INT4                i4NextEgressIndex = 0;
    INT4                i4Evc = 0;
    INT4                i4NextEvc = 0;
    INT1                i1MatchFound = OSIX_FALSE;
    INT1                i1RetVal = SNMP_FAILURE;
    INT1                i1Print = OSIX_TRUE;

    MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au4EgressPorts, 0, (sizeof (UINT4) * MEF_ETREE_MAX_LEAF_UNIS));

    if (0 == i4IngressPort)
    {
        i1MatchFound = OSIX_TRUE;
    }

    if (nmhGetFirstIndexFsMefETreeTable (&i4IngressIndex, &i4Evc,
                                         &i4EgressIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    while (1)
    {
        i1RetVal = nmhGetNextIndexFsMefETreeTable (i4IngressIndex,
                                                   &i4NextIngressIndex,
                                                   i4Evc,
                                                   &i4NextEvc,
                                                   i4EgressIndex,
                                                   &i4NextEgressIndex);
        if (i4IngressIndex == i4IngressPort)
        {
            i1MatchFound = OSIX_TRUE;
        }

        if (i1MatchFound == OSIX_TRUE)
        {
            au4EgressPorts[u4Scan] = i4EgressIndex;
            u4Scan++;
            if (i1Print == OSIX_TRUE)
            {
                CliPrintf (CliHandle,
                           "\r\nIngress Port     VlanId     Egress List\r\n");
                CliPrintf (CliHandle,
                           "============     ======     ===========\r\n");
                i1Print = OSIX_FALSE;
            }
        }
        if (((i4IngressIndex != i4NextIngressIndex) &&
             (OSIX_TRUE == i1MatchFound)) ||
            ((i4Evc != i4NextEvc) && (OSIX_TRUE == i1MatchFound)))
        {
            CfaCliGetIfName (i4IngressIndex, ai1IfName);
            CliPrintf (CliHandle, "%-18s", ai1IfName);

            CliPrintf (CliHandle, "%-10d", i4Evc);

            for (u4Temp = 0; u4Temp < (u4Scan - 1); u4Temp++)
            {
                CfaCliGetIfName (au4EgressPorts[u4Temp], ai1IfName);
                CliPrintf (CliHandle, "%s,", ai1IfName);
                if ((u4Temp != 0) && ((u4Temp % 5) == 0))
                {
                    CliPrintf (CliHandle, "\r\n%44s", " ");
                }
            }

            CfaCliGetIfName (au4EgressPorts[u4Temp], ai1IfName);
            CliPrintf (CliHandle, "%s", ai1IfName);
            CliPrintf (CliHandle, "\r\n");

            if ((i4IngressPort != 0) && (i4IngressIndex != i4NextIngressIndex))
            {
                break;
            }
            u4Scan = 0;
            MEMSET (au4EgressPorts, 0, MEF_ETREE_MAX_LEAF_UNIS);
        }

        if (i1RetVal == SNMP_FAILURE)
        {
            break;
        }
        else
        {
            i4IngressIndex = i4NextIngressIndex;
            i4Evc = i4NextEvc;
            i4EgressIndex = i4NextEgressIndex;
            i4NextIngressIndex = i4NextEvc = 0;
        }
    }

    return CLI_SUCCESS;
}

INT4
MefCliEvcFilterConfig (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Evc,
                       UINT4 *pu4Instance, UINT4 *pu4FilterId,
                       UINT1 *pu1MacAddr, UINT4 *pu4Allow, UINT4 *pu4Drop)
{
    tMacAddr            MacAddr;
    INT4                i4Instace = 0;
    INT4                i4FilterId = 0;
    INT4                i4Action = 0;
    INT4                i4RowStatus = 0;
    INT4                i4FilterStatus = 0;
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    MEMCPY (&i4Instace, pu4Instance, sizeof (INT4));
    if (pu4FilterId != NULL)
    {
        MEMCPY (&i4FilterId, pu4FilterId, sizeof (INT4));
    }
    CliStrToMac (pu1MacAddr, MacAddr);

    if (pu4Allow != NULL)
    {
        i4Action = ISS_ALLOW;
    }
    else if (pu4Drop != NULL)
    {
        i4Action = ISS_DROP;
    }

    if (nmhGetFsEvcRowStatus (u4ContextId, i4Evc, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MEF_CLI_EVC_NOT_EXIST);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                 NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* HDC 60138 */
    if (nmhGetFsEvcFilterRowStatus ((INT4) u4ContextId, i4Evc, i4Instace,
                                    &i4FilterStatus) == SNMP_FAILURE)
    {
        if ((pu4FilterId == NULL) && (pu1MacAddr == NULL) &&
            (pu4Allow == NULL) && (pu4Drop == NULL))
        {
            CLI_SET_ERR (MEF_CLI_FILTER_NOT_EXIST);
            return CLI_FAILURE;
        }
        else
        {
            if (nmhTestv2FsEvcFilterRowStatus (&u4ErrorCode, (INT4) u4ContextId,
                                               i4Evc, i4Instace,
                                               CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsEvcFilterRowStatus ((INT4) u4ContextId, i4Evc,
                                            i4Instace,
                                            CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        if ((pu4FilterId == NULL) && (pu1MacAddr == NULL) &&
            (pu4Allow == NULL) && (pu4Drop == NULL))
        {
            if (nmhTestv2FsEvcFilterRowStatus (&u4ErrorCode, (INT4) u4ContextId,
                                               i4Evc, i4Instace,
                                               DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsEvcFilterRowStatus ((INT4) u4ContextId, i4Evc,
                                            i4Instace, DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                         ACTIVE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, ACTIVE) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            return CLI_SUCCESS;
        }
        else
        {
            if (nmhTestv2FsEvcFilterRowStatus (&u4ErrorCode, (INT4) u4ContextId,
                                               i4Evc, i4Instace,
                                               NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsEvcFilterRowStatus ((INT4) u4ContextId, i4Evc,
                                            i4Instace,
                                            NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                nmhSetFsEvcFilterRowStatus ((INT4) u4ContextId, i4Evc,
                                            i4Instace, DESTROY);
                return CLI_FAILURE;
            }
        }
    }
    if (nmhTestv2FsEvcFilterId (&u4ErrorCode, u4ContextId, i4Evc, i4Instace,
                                i4FilterId) == SNMP_FAILURE)
    {
        /* IMPORTANT:Since Filter Id is the global id, failing its configuration
         * should result in deleting the entry. */
        nmhSetFsEvcFilterRowStatus ((INT4) u4ContextId, i4Evc, i4Instace,
                                    DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcFilterId (u4ContextId, i4Evc, i4Instace, i4FilterId)
        == SNMP_FAILURE)
    {
        /* IMPORTANT:Since Filter Id is the global id, failing its configuration
         * should result in deleting the entry. */
        nmhSetFsEvcFilterRowStatus ((INT4) u4ContextId, i4Evc, i4Instace,
                                    DESTROY);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcFilterDestMacAddress (&u4ErrorCode, u4ContextId, i4Evc,
                                            i4Instace, MacAddr) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcFilterDestMacAddress (u4ContextId, i4Evc, i4Instace, MacAddr)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcFilterAction (&u4ErrorCode, u4ContextId, i4Evc,
                                    i4Instace, i4Action) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcFilterAction (u4ContextId, i4Evc, i4Instace, i4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcFilterRowStatus (&u4ErrorCode, (INT4) u4ContextId,
                                       i4Evc, i4Instace,
                                       ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsEvcFilterRowStatus ((INT4) u4ContextId, i4Evc,
                                    i4Instace, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsEvcRowStatus (&u4ErrorCode, u4ContextId, i4Evc,
                                 ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsEvcRowStatus (u4ContextId, i4Evc, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
MefCliUniListConfig (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Evc,
                     UINT4 u4IfIndex, INT4 i4UniListType)
{
    INT4                i4RowStatus = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = 0;
    UINT1               u1MemPortType = 0;
    INT1                i1RetVal = VLAN_FAILURE;


    UNUSED_PARAM (CliHandle);

    i1RetVal = VlanMemberPortType (i4Evc, u4IfIndex, &u1MemPortType);
 
    if ( i1RetVal == VLAN_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (u1MemPortType == FORBIDDEN_PORT)
    {
        CliPrintf (CliHandle, "%% Gigabitethernet 0/%d is a Forbiddenport\r\n",u4IfIndex);
        return CLI_FAILURE;
    }
    else if (u1MemPortType == NON_MEMBER_PORT)
    {
        CliPrintf (CliHandle, "WARNING: Gigabitethernet 0/%d is not a Member Port\r\n",u4IfIndex);
    }

    i4RetVal =
        nmhGetFsMefUniListRowStatus (u4ContextId, i4Evc, u4IfIndex,
                                     &i4RowStatus);
    if (i4RetVal == SNMP_FAILURE && i4UniListType != -1)
    {
        /* New entry in Uni List Table */
        if (nmhTestv2FsMefUniListRowStatus
            (&u4ErrorCode, u4ContextId, i4Evc, u4IfIndex,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMefUniListRowStatus
            (u4ContextId, i4Evc, u4IfIndex, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Entry already exist in Uni List Table */
        if (nmhTestv2FsMefUniListRowStatus
            (&u4ErrorCode, u4ContextId, i4Evc, u4IfIndex,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMefUniListRowStatus
            (u4ContextId, i4Evc, u4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (i4UniListType != -1)
    {
        if (nmhTestv2FsMefUniType
            (&u4ErrorCode, u4ContextId, i4Evc, u4IfIndex,
             i4UniListType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMefUniType (u4ContextId, i4Evc, u4IfIndex, i4UniListType)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /*Make the row status as active */
        if (nmhTestv2FsMefUniListRowStatus
            (&u4ErrorCode, u4ContextId, i4Evc, u4IfIndex,
             ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMefUniListRowStatus (u4ContextId, i4Evc, u4IfIndex, ACTIVE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    if (i4UniListType == -1)
    {
        /* destroy the Uni List Table */
        if (nmhTestv2FsMefUniListRowStatus
            (&u4ErrorCode, u4ContextId, i4Evc, u4IfIndex,
             DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMefUniListRowStatus (u4ContextId, i4Evc, u4IfIndex, DESTROY)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

INT4
MefCliShowUniList (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4EvcIndex,
                   UINT4 u4IfIndex)
{
    UINT1               au1UniId[UNI_ID_MAX_LENGTH];
    tSNMP_OCTET_STRING_TYPE UniId;
    UINT4               u4PrevContextId = 0;
    INT4                i4EvcIndex = 0, i4PrevEvcIndex = 0;
    INT4                i4IfIndex = 0, i4PrevIfIndex = 0;
    INT4                i4UniType = 0;
    tMefUniList        *pMefUniListEntry = NULL;
    UINT1               au1Name[CFA_MAX_PORT_NAME_LENGTH];

    /* Display the UNI List for the given Evc and UNI Combination */
    if (u4EvcIndex != 0 && u4IfIndex != 0)
    {
        pMefUniListEntry =
            MefGetUniListEntry (u4ContextId, (INT4) u4EvcIndex,
                                (INT4) u4IfIndex);
        if (pMefUniListEntry == NULL)
        {
            return CLI_FAILURE;
        }

        CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1Name);
        MEMSET (&au1UniId, 0, UNI_ID_MAX_LENGTH);
        UniId.pu1_OctetList = au1UniId;
        UniId.i4_Length = UNI_ID_MAX_LENGTH;
        nmhGetFsMefUniId (u4ContextId, (INT4) u4EvcIndex, (INT4) u4IfIndex,
                          &UniId);
        MefCliDisplayUniList (CliHandle, (INT4) u4EvcIndex, au1Name, au1UniId,
                              pMefUniListEntry->UniType);
        return CLI_SUCCESS;
    }
    /*Display all the Uni List Table entries of an evc */
    if (u4EvcIndex != 0 && u4IfIndex == 0)
    {
        u4PrevContextId = u4ContextId;
        i4PrevEvcIndex = i4EvcIndex;
        if (nmhGetFirstIndexFsMefUniListTable
            (&u4ContextId, &i4EvcIndex, &i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        do
        {
            if (u4ContextId != u4PrevContextId && i4EvcIndex != i4PrevEvcIndex)
            {
                continue;
            }
            pMefUniListEntry =
                MefGetUniListEntry (u4ContextId, i4EvcIndex, i4IfIndex);
            if (pMefUniListEntry == NULL)
            {
                return CLI_FAILURE;
            }
            CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1Name);
            MEMSET (&au1UniId, 0, UNI_ID_MAX_LENGTH);
            UniId.pu1_OctetList = au1UniId;
            UniId.i4_Length = UNI_ID_MAX_LENGTH;
            nmhGetFsMefUniId (u4ContextId, i4EvcIndex, i4IfIndex, &UniId);
            MefCliDisplayUniList (CliHandle, i4EvcIndex, au1Name, au1UniId,
                                  pMefUniListEntry->UniType);
            u4PrevContextId = u4ContextId;
            i4PrevEvcIndex = i4EvcIndex;
            i4PrevIfIndex = i4IfIndex;
        }
        while (nmhGetNextIndexFsMefUniListTable (u4PrevContextId, &u4ContextId,
                                                 i4PrevEvcIndex, &i4EvcIndex,
                                                 i4PrevIfIndex,
                                                 &i4IfIndex) == SNMP_SUCCESS);
        return CLI_SUCCESS;
    }

    /* Display all the Uni List Table entries */
    if (u4EvcIndex == 0 && u4IfIndex == 0)
    {
        u4ContextId = 0;
        if (nmhGetFirstIndexFsMefUniListTable
            (&u4ContextId, &i4EvcIndex, &i4IfIndex) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        do
        {
            MEMSET (&au1UniId, 0, UNI_ID_MAX_LENGTH);
            UniId.pu1_OctetList = au1UniId;
            UniId.i4_Length = UNI_ID_MAX_LENGTH;

            nmhGetFsMefUniId (u4ContextId, i4EvcIndex, i4IfIndex, &UniId);
            nmhGetFsMefUniType (u4ContextId, i4EvcIndex, i4IfIndex, &i4UniType);

            CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1Name);
            MefCliDisplayUniList (CliHandle, i4EvcIndex, au1Name, au1UniId,
                                  i4UniType);
            u4PrevContextId = u4ContextId;
            i4PrevEvcIndex = i4EvcIndex;
            i4PrevIfIndex = i4IfIndex;
        }
        while (nmhGetNextIndexFsMefUniListTable (u4PrevContextId, &u4ContextId,
                                                 i4PrevEvcIndex, &i4EvcIndex,
                                                 i4PrevIfIndex,
                                                 &i4IfIndex) == SNMP_SUCCESS);
    }
    return CLI_SUCCESS;
}

INT4
MefCliDisplayUniList (tCliHandle CliHandle, INT4 i4Evc, UINT1 *au1Uni,
                      UINT1 *au1UniId, INT4 i4UniType)
{

    CliPrintf (CliHandle, "EVC\t UNI\n");
    CliPrintf (CliHandle, "%d\t %s\n", i4Evc, au1Uni);
    CliPrintf (CliHandle, "ethernet Uni List:\t");
    if (i4UniType == MEF_UNI_ROOT)
    {
        CliPrintf (CliHandle, "< %s, Root >\r\n", au1UniId);
    }
    else if (i4UniType == MEF_UNI_LEAF)
    {
        CliPrintf (CliHandle, "< %s, Leaf >\r\n", au1UniId);
    }
    return CLI_SUCCESS;
}
/***************************************************************************
 * FUNCTION NAME    : MefCliDisplayEvcStats
 *
 * DESCRIPTION      : This function display the number of unicast, broadcast
 *                    and unknown unicast packets/bytes transmitted/received/
 *                    dropped by the EVC
 *
 *
 * INPUT            : CliHandle
 *                    u4ContextId
 *                    u4EvcVlanId
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4 MefCliDisplayEvcStats(tCliHandle CliHandle, UINT4 u4EvcVlanId, INT4 i4EvcContextId) 
{
    UINT4 u4Frames = 0;
    UINT4 u4Bytes  = 0;

    nmhGetFsEvcStatsRxFrames(i4EvcContextId, u4EvcVlanId, &u4Frames);
    CliPrintf (CliHandle, " Total Number of frames received         : %u\r\n", 
               u4Frames);

    nmhGetFsEvcStatsRxBytes(i4EvcContextId, u4EvcVlanId, &u4Bytes);
    CliPrintf (CliHandle, " Total Number of Bytes received          : %u\r\n",
               u4Bytes);     
 
    nmhGetFsEvcStatsTxFrames(i4EvcContextId, u4EvcVlanId, &u4Frames);
    CliPrintf (CliHandle, " Total Number of frames Transmitted      : %u\r\n",
               u4Frames);

    nmhGetFsEvcStatsTxBytes(i4EvcContextId, u4EvcVlanId, &u4Bytes);
    CliPrintf (CliHandle, " Total Number of Bytes Transmitted       : %u\r\n",
               u4Bytes);     

    nmhGetFsEvcStatsDiscardFrames(i4EvcContextId, u4EvcVlanId, &u4Frames);
    CliPrintf (CliHandle, " Total Number of frames Discarded        : %u\r\n",
               u4Frames);

    nmhGetFsEvcStatsDiscardBytes(i4EvcContextId, u4EvcVlanId, &u4Bytes);
    CliPrintf (CliHandle, " Total Number of Bytes Discarded         : %u\r\n",
               u4Bytes);     
    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MefCliShowEvcStats
 *
 * DESCRIPTION      : This function display the number of unicast, broadcast 
 *                    and unknown unicast packets/bytes transmitted/received/
 *                    dropped by the EVC
 *                   
 *
 * INPUT            : CliHandle
 *                    u4ContextId
 *                    u4EvcVlanId
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/


INT4 MefCliShowEvcStats(tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4EvcVlanId)
{
	INT4                i4EvcContextId    = 0;
	INT4                i4PreEvcContextId = (INT4) u4ContextId;
	INT4                i4Evc             = 0;
	INT4                i4PreEvc          = 0;
	INT4                i4RowStatus = 0;
	UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
	if (u4EvcVlanId != 0)
	{
		VcmGetAliasName (u4ContextId, au1ContextName);
		CliPrintf (CliHandle, "\r\n\r Switch %s \r\n", au1ContextName);
		if (nmhGetFsEvcRowStatus (u4ContextId, u4EvcVlanId, 
					&i4RowStatus) == SNMP_FAILURE)
		{
                        CliPrintf(CliHandle, " EVC-%d does not exists\r\n", u4EvcVlanId);
			return CLI_SUCCESS;
		}
		CliPrintf (CliHandle, " EVC-%d Statistics \r\n", u4EvcVlanId);
		MefCliDisplayEvcStats(CliHandle, u4EvcVlanId, u4ContextId);
	}
	else
	{
		VcmGetAliasName (u4ContextId, au1ContextName);
		CliPrintf (CliHandle, "\r\n\r Switch %s \r\n", au1ContextName);
		while (nmhGetNextIndexFsEvcTable (i4PreEvcContextId, &i4EvcContextId,
					i4PreEvc, &i4Evc) == SNMP_SUCCESS)
		{
			if (i4EvcContextId != (INT4) u4ContextId)
			{
				break;
			}
			CliPrintf (CliHandle, " EVC-%d Statistics \r\n", i4Evc);
			MefCliDisplayEvcStats(CliHandle, i4Evc, i4EvcContextId);
			i4PreEvc = i4Evc;
			i4PreEvcContextId = i4EvcContextId;
		}
	}
	return CLI_SUCCESS;
}
#endif
