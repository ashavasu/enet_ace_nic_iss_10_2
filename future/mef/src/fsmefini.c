/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmefini.c,v 1.7 2014/10/10 12:05:49 siva Exp $
 *
 * Description:
 *********************************************************************/
#ifndef __FSMEF_INI_C__
#define __FSMEF_INI_C__

#include  "fsmefinc.h"

/****************************************************************************
* Function Name      : MefTask                                              *
*                                                                           *
* Description        : This function is creates the MEF module semaphore    *
*                      and create the memory pools and tables required by   *
*                      MEF module and Registers the MIB with the SNMP.      *
*                                                                           *
* Input(s)           : None.                                                *
*                                                                           *
* Output(s)          : None.                                                *
*                                                                           *
* Return Value(s)    : VOID                                                 *
*****************************************************************************/
VOID
MefTask (INT1 *i1Params)
{
    INT4                i4RetStatus = OSIX_FAILURE;
    UINT4               u4Events = 0;

    UNUSED_PARAM (i1Params);

    MEMSET (&(gMefGlobalInfo), 0, (sizeof (tMefGlobalInfo)));

    gMefGlobalInfo.bIsMefInitialised = OSIX_FALSE;

    i4RetStatus =
        OsixCreateSem (MEF_SEM_NAME, 1, 0, &(gMefGlobalInfo.MefSemId));
    if (i4RetStatus != OSIX_SUCCESS)
    {
        MEF_TRC (OS_RESOURCE_TRC, "Semaphore Creation Failed \r\n");
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    i4RetStatus = OsixCreateQ (MEF_QUEUE_NAME, MEF_MAX_Q_DEPTH, 0,
                               &(gMefGlobalInfo.MefQueueId));
    if (i4RetStatus != OSIX_SUCCESS)
    {
        MEF_TRC (OS_RESOURCE_TRC, "Queue Creation Failed \r\n");
        return;
    }

    if (MefSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "Memory Pools Creation Failed \r\n");
        OsixDeleteSem (0, MEF_SEM_NAME);
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    if (MefCreateTables () != OSIX_SUCCESS)
    {
        MEF_TRC (OS_RESOURCE_TRC, "Tables Creation Failed \r\n");
        OsixDeleteSem (0, MEF_SEM_NAME);
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    if (MefContextCreateAndInit (L2IWF_DEFAULT_CONTEXT) != OSIX_SUCCESS)
    {
        MEF_TRC (OS_RESOURCE_TRC, "Default Context Creation Failed \r\n");
        OsixDeleteSem (0, MEF_SEM_NAME);
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    gMefGlobalInfo.bIsMefInitialised = OSIX_TRUE;

#ifdef SNMP_2_WANTED
    RegisterFSMEF ();
#endif

    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixReceiveEvent (MEF_MSG_IN_Q_EVENT, OSIX_WAIT, 0, &u4Events)
            == OSIX_SUCCESS)
        {
            MefApiLock ();

            if (u4Events & MEF_MSG_IN_Q_EVENT)
            {
                MefQueueMsgHandler ();
            }

            MefApiUnLock ();
        }
    }
}

/*****************************************************************************
* Function Name       :   MefCreateTables                                    *
*                                                                            *
* Description         :   This function creates tables maintained in the     *
*                         MEF module                                         *
*                                                                            *
* Input(s)            :   None                                               *
*                                                                            *
* Output(s)           :   None                                               *
*                                                                            *
* Returns             :   OSIX_SUCCESS/OSIX_FAILURE                          *
*****************************************************************************/
INT4
MefCreateTables ()
{
    if (MefUniCreateTable () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "UNI Table Creation Failed \r\n");
        MefSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (MefEvcCreateTable () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "EVC Table Creation Failed \r\n");
        MefSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (MefEvcFilterCreateTable () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "EVC Filter Table Creation Failed \r\n");
        MefSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (MefEvcInfoCreateTable () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "EVC Info Table Creation Failed \r\n");
        MefSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (MefFilterCreateTable () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "Filter Table Creation Failed \r\n");
        MefSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (MefClassMapCreateTable () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "ClassMap Table Creation Failed \r\n");
        MefSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (MefClassCreateTable () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "Class Table Creation Failed \r\n");
        MefSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (MefMeterCreateTable () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "Meter Table Creation Failed \r\n");
        MefSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (MefPolicyMapCreateTable () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "PolicyMap Table Creation Failed \r\n");
        MefSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (MefCVlanEvcTableTable () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "CE-VLAN and EVC Map Table Creation"
                 " Failed \r\n");
        MefSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (MefUniListCreateTable () == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC, "Uni List Table Creation" " Failed \r\n");
        MefSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif
