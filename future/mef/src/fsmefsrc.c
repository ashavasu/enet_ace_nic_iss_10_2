/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmefsrc.c,v 1.18 2016/06/18 11:45:01 siva Exp $
 *
 * Description:
 * *********************************************************************/
#ifndef __FSMEFSRC_C__
#define __FSMEFSRC_C__

#include "fsmefinc.h"

INT4
MefShowRunningConfig (tCliHandle CliHandle)
{
    MefApiLock ();
    MefShowRunningScalarConfig (CliHandle);
    MefShowRunningEvcConfig (CliHandle);
    MefShowRunningETreeConfig (CliHandle);
    MefShowRunningClassConfig (CliHandle);
    MefShowRunningClassMapConfig (CliHandle);
    MefShowRunningMeterConfig (CliHandle);
    MefShowRunningPolicyMapConfig (CliHandle);
    MefShowRunningUniConfig (CliHandle); 
    MefApiUnLock ();
    return CLI_SUCCESS;
}

INT4
MefShowRunningScalarConfig (tCliHandle CliHandle)
{
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    UINT4               u4ContextId = 0;
    UINT4               u4PrevContextId = 0;
    INT4                i4MefTransmode = 0;

    if (nmhGetFirstIndexFsMefContextTable (&u4ContextId) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {

        nmhGetFsMefTransmode (u4ContextId, &i4MefTransmode);
        if (i4MefTransmode == MEF_TRANS_MODE_MPLS)
        {
            
        VcmGetAliasName (u4ContextId, au1ContextName);
        CliPrintf (CliHandle, "!\r\n\r\n");    
        CliPrintf (CliHandle, "Switch %s \r\n", au1ContextName);

        CliPrintf (CliHandle, "mef transmode mpls\r\n\r\n");
        }

        u4PrevContextId = u4ContextId;
    }
    while (nmhGetNextIndexFsMefContextTable (u4PrevContextId, &u4ContextId)
           == SNMP_SUCCESS);
    return CLI_SUCCESS;
}

INT4
MefShowRunningUniListConfig (tCliHandle CliHandle,INT4 i4CurrEvcId)
{
    UINT1               au1UniId[UNI_ID_MAX_LENGTH];
    tSNMP_OCTET_STRING_TYPE UniId;
    tMefUniList        *pMefUniListInfo = NULL;
    UINT1               au1Name[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4ContextId = 0, u4PrevContextId = 0;
    INT4                i4EvcIndex = 0, i4PrevEvcIndex = 0;
    INT4                i4IfIndex = 0, i4PrevIfIndex = 0;
    INT4                i4Status = 0;
    INT4                i4UniType = 0;
    
    pMefUniListInfo = (tMefUniList *) RBTreeGetFirst (MEF_UNI_LIST_TABLE);
    while (pMefUniListInfo != NULL)
    {
        if(i4CurrEvcId == pMefUniListInfo->i4EvcIndex)
        {
            break;
        }
        pMefUniListInfo = (tMefUniList *) RBTreeGetNext
            (MEF_UNI_LIST_TABLE, pMefUniListInfo, NULL);

    }

    if (pMefUniListInfo == NULL)
    {
        return CLI_SUCCESS;
    }

    i4EvcIndex = i4CurrEvcId;

    do
    {
        nmhGetFsMefUniListRowStatus (u4ContextId, i4EvcIndex, i4IfIndex,
                                     &i4Status);
        MEMSET (&au1UniId, 0, UNI_ID_MAX_LENGTH);
        UniId.pu1_OctetList = au1UniId;
        UniId.i4_Length = UNI_ID_MAX_LENGTH;

        nmhGetFsMefUniId (u4ContextId, i4EvcIndex, i4IfIndex, &UniId);
        nmhGetFsMefUniType (u4ContextId, i4EvcIndex, i4IfIndex, &i4UniType);

        if (i4CurrEvcId == i4EvcIndex)
        {
            CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1Name);

            if (i4UniType == MEF_UNI_ROOT)
            {
                CliPrintf (CliHandle,
                           "ethernet uni-list uni gigabitethernet %s uni-type root\n",
                           au1Name + 2);
            }
            else if (i4UniType == MEF_UNI_LEAF)
            {
                CliPrintf (CliHandle,
                           "ethernet uni-list uni gigabitethernet %s uni-type leaf\n",
                           au1Name + 2);
            }
        }
        u4PrevContextId = u4ContextId;
        i4PrevEvcIndex = i4EvcIndex;
        i4PrevIfIndex = i4IfIndex;
    }
    while (nmhGetNextIndexFsMefUniListTable (u4PrevContextId, &u4ContextId,
                                             i4PrevEvcIndex, &i4EvcIndex,
                                             i4PrevIfIndex,
                                             &i4IfIndex) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
MefShowRunningUniConfig (tCliHandle CliHandle)
{
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1UniId[UNI_ID_MAX_LENGTH];
    UINT1               au1L2cpState[][8] = { {"peer"},
    {"tunnel"},
    {"discard"}
    };
    tSNMP_OCTET_STRING_TYPE UniId;
    tSNMP_OCTET_STRING_TYPE UniType;
    UINT4               u4BridgeMode = 0;
    UINT4               u4ContextId = 0;
    INT4                i4PreIfIndex = 0;
    INT4                i4IfIndex = 0;
    INT4                i4UniDefCeVlanId = 0;
    INT4                i4UniL2cp = 0;
    INT4                i4Status = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1UniType = 0;
    UINT1               u1TransModeFlag = 0;


    MEMSET (au1UniId, 0, UNI_ID_MAX_LENGTH);
    UniId.pu1_OctetList = au1UniId;
    UniId.i4_Length = UNI_ID_MAX_LENGTH;

    UniType.pu1_OctetList = &u1UniType;
    UniType.i4_Length = sizeof (UINT1);

    VcmGetContextInfoFromIfIndex ((UINT4) i4IfIndex, &u4ContextId,
                                  &u2LocalPort);
    L2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);
    u1TransModeFlag = FsMefUtilGetTransportMode (u4ContextId);

    while (nmhGetNextIndexFsUniTable (i4PreIfIndex, &i4IfIndex) == SNMP_SUCCESS)
    {
        nmhGetFsUniRowStatus (i4IfIndex, &i4Status);
        if (i4Status != ACTIVE)
        {
            i4PreIfIndex = i4IfIndex;
            continue;
        }

        CliPrintf (CliHandle, "! \r\n\r\n");

        MEMSET (ai1IfName, 0, sizeof (ai1IfName));

        CfaCliGetIfName (i4IfIndex, ai1IfName);
        CliPrintf (CliHandle, "interface gigabitethernet %s\r\n",
                   (ai1IfName + 2));

        nmhGetFsUniId (i4IfIndex, &UniId);

        if (UniId.i4_Length != 0)
        {
            CliPrintf (CliHandle, "ethernet uni id %s \r\n\r\n",
                       UniId.pu1_OctetList);
        }

        nmhGetFsUniServiceMultiplexingBundling (i4IfIndex, &UniType);
        if (u1UniType != MEF_DEFAULT_UNI_TYPE)
        {
            CliPrintf (CliHandle, "ethernet type uni ");
            if (u1UniType & MEF_MULTIPLEX_BITMASK)
            {
                CliPrintf (CliHandle, "multiplex");
            }

            if (u1UniType & MEF_BUNDLE_BITMASK)
            {
                CliPrintf (CliHandle, "bundle");
            }

            if (u1UniType & MEF_ALL_TO_ONE_BUNDLE_BITMASK)
            {
                CliPrintf (CliHandle, "all-to-one-bundle");
            }
            CliPrintf (CliHandle, "\r\n\r\n");
        }

        nmhGetFsUniCVlanId (i4IfIndex, &i4UniDefCeVlanId);
        if (i4UniDefCeVlanId != MEF_DEFAULT_VLAN)
        {
            CliPrintf (CliHandle, "ethernet uni cvlan-id %d \r\n\r\n",
                       i4UniDefCeVlanId);
        }

        nmhGetFsUniL2CPDot1x (i4IfIndex, &i4UniL2cp);
        if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_PEER)
        {
            CliPrintf (CliHandle, "ethernet uni l2cp dot1x  %s\r\n\r\n",
                       au1L2cpState[i4UniL2cp - 1]);
        }

        nmhGetFsUniL2CPLacp (i4IfIndex, &i4UniL2cp);
        if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_PEER)
        {
            CliPrintf (CliHandle, "ethernet uni l2cp lacp %s\r\n\r\n",
                       au1L2cpState[i4UniL2cp - 1]);
        }

        nmhGetFsUniL2CPStp (i4IfIndex, &i4UniL2cp);
        if (u1UniType & MEF_ALL_TO_ONE_BUNDLE_BITMASK)
        {
            if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_TUNNEL)
            {
                CliPrintf (CliHandle, "ethernet uni l2cp stp %s\r\n\r\n",
                           au1L2cpState[i4UniL2cp - 1]);
            }
        }
        else
        {
            if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_PEER)
            {
                CliPrintf (CliHandle, "ethernet uni l2cp stp %s\r\n\r\n",
                           au1L2cpState[i4UniL2cp - 1]);
            }
        }


        if ((u1TransModeFlag == MEF_TRANS_MODE_MPLS) &&
            (u4BridgeMode == MEF_CUSTOMER_BRIDGE_MODE))
        {
            nmhGetFsUniL2CPGvrp (i4IfIndex, &i4UniL2cp);

            if(i4UniL2cp != MEF_TUNNEL_PROTOCOL_PEER)
            {
                CliPrintf (CliHandle, "ethernet uni l2cp gvrp %s\r\n\r\n",
                           au1L2cpState[i4UniL2cp - 1]);
            }

            nmhGetFsUniL2CPGmrp (i4IfIndex, &i4UniL2cp);
            if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_PEER)
            {
                CliPrintf (CliHandle, "ethernet uni l2cp gmrp %s\r\n\r\n",
                           au1L2cpState[i4UniL2cp - 1]);
            }
            nmhGetFsUniL2CPMvrp (i4IfIndex, &i4UniL2cp);
            if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_PEER)
            {
                CliPrintf (CliHandle, "ethernet uni l2cp mvrp %s\r\n\r\n",
                           au1L2cpState[i4UniL2cp - 1]);
            }

            nmhGetFsUniL2CPMmrp (i4IfIndex, &i4UniL2cp);
            if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_PEER)
            {
                CliPrintf (CliHandle, "ethernet uni l2cp mmrp %s\r\n\r\n",
                           au1L2cpState[i4UniL2cp - 1]);
            }

        }

        else
        {
            nmhGetFsUniL2CPGvrp (i4IfIndex, &i4UniL2cp);

            if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_TUNNEL)
          
            {
                CliPrintf (CliHandle, "ethernet uni l2cp gvrp %s\r\n\r\n",
                           au1L2cpState[i4UniL2cp - 1]);
            }

            nmhGetFsUniL2CPGmrp (i4IfIndex, &i4UniL2cp);
            if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_TUNNEL)
            {
                CliPrintf (CliHandle, "ethernet uni l2cp gmrp %s\r\n\r\n",
                           au1L2cpState[i4UniL2cp - 1]);
            }

            nmhGetFsUniL2CPMvrp (i4IfIndex, &i4UniL2cp);
            if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_TUNNEL)
            {
                CliPrintf (CliHandle, "ethernet uni l2cp mvrp %s\r\n\r\n",
                           au1L2cpState[i4UniL2cp - 1]);
            }

            nmhGetFsUniL2CPMmrp (i4IfIndex, &i4UniL2cp);
            if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_TUNNEL)
            {
                CliPrintf (CliHandle, "ethernet uni l2cp mmrp %s\r\n\r\n",
                           au1L2cpState[i4UniL2cp - 1]);
            }

        }
        nmhGetFsUniL2CPLldp (i4IfIndex, &i4UniL2cp);
        if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_DISCARD)
        {
            CliPrintf (CliHandle, "ethernet uni l2cp lldp %s\r\n\r\n",
                       au1L2cpState[i4UniL2cp - 1]);
        }

        nmhGetFsUniL2CPElmi (i4IfIndex, &i4UniL2cp);
        if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_PEER)
        {
            CliPrintf (CliHandle, "ethernet uni l2cp elmi %s\r\n\r\n",
                       au1L2cpState[i4UniL2cp - 1]);
        }

        nmhGetFsUniL2CPEcfm (i4IfIndex, &i4UniL2cp);
        if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_TUNNEL)
        {
            CliPrintf (CliHandle, "ethernet uni l2cp ecfm %s\r\n\r\n",
                       au1L2cpState[i4UniL2cp - 1]);
        }

        nmhGetFsUniL2CPOverrideOption (i4IfIndex, &i4UniL2cp);
        if (i4UniL2cp != MEF_UNI_OVERRIDE_DISABLE)
        {
            CliPrintf (CliHandle, "ethernet uni override enable\r\n\r\n");
        }

        nmhGetFsUniL2CPEoam (i4IfIndex, &i4UniL2cp);
        if (i4UniL2cp != MEF_TUNNEL_PROTOCOL_PEER)
        {
            CliPrintf (CliHandle, "ethernet uni l2cp eoam %s\r\n\r\n",
                       au1L2cpState[i4UniL2cp - 1]);
        }

        i4PreIfIndex = i4IfIndex;
        MefShowRunningCeVlanEvcMapConfig (CliHandle,i4IfIndex);
        MefShowRunningFilterConfig (CliHandle,i4IfIndex);

    }
    UNUSED_PARAM (u2LocalPort);
    return CLI_SUCCESS;
}

INT4
MefShowRunningEvcConfig (tCliHandle CliHandle)
{
    UINT1               au1EvcId[EVC_ID_MAX_LENGTH];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    tSNMP_OCTET_STRING_TYPE EvcId;
    INT4                i4EvcContextId = 0;
    INT4                i4PreEvcContextId = 0;
    INT4                i4Evc = 0;
    INT4                i4PreEvc = 0;
    INT4                i4EvcType = 0;
    INT4                i4EvcVlanIdPre = 0;
    INT4                i4EvcLoopbackStatus = 0;
    INT4                i4EvcCosPre = 0;
    INT4                i4Status = 0;
    tMefEvcInfo        *pMefEvcInfo = NULL;
    if (nmhGetFirstIndexFsEvcTable (&i4EvcContextId, &i4Evc) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {

        nmhGetFsEvcRowStatus (i4EvcContextId, i4Evc, &i4Status);
        if (i4Status != ACTIVE)
        {
            i4PreEvcContextId = i4EvcContextId;
            i4PreEvc = i4Evc;
            if (nmhGetNextIndexFsEvcTable (i4PreEvcContextId, &i4EvcContextId,
                                           i4PreEvc, &i4Evc) != SNMP_SUCCESS)
            {
                break;
            }
            continue;
        }

        if ((i4PreEvcContextId != i4EvcContextId) || (i4EvcContextId == 0))
        {
            VcmGetAliasName (i4EvcContextId, au1ContextName);
            
           
            CliPrintf (CliHandle, "!\r\n\r\n");
            CliPrintf (CliHandle, "Switch %s \r\n", au1ContextName);
        }

        CliPrintf (CliHandle, "vlan %d\r\n\r\n", i4Evc);
        /* TODO Take care for VPLS */

        MEMSET (&au1EvcId, 0, EVC_ID_MAX_LENGTH);
        EvcId.pu1_OctetList = au1EvcId;
        EvcId.i4_Length = EVC_ID_MAX_LENGTH;
        nmhGetFsEvcId (i4EvcContextId, i4Evc, &EvcId);
        if (EvcId.i4_Length != 0)
        {
            CliPrintf (CliHandle, "ethernet evc id %s \r\n\r\n",
                       EvcId.pu1_OctetList);
        }

        nmhGetFsEvcType (i4EvcContextId, i4Evc, &i4EvcType);
        if (i4EvcType != MEF_MULTIPOINT_TO_MULTIPOINT)
        {
            if (i4EvcType == MEF_POINT_TO_POINT)
            {
                CliPrintf (CliHandle,
                           "ethernet  evc type point-to-point\r\n\r\n");
            }
            if (i4EvcType == MEF_ROOTED_MULTIPOINT)
            {
                CliPrintf (CliHandle, "ethernet  evc type rooted-multipoint"
                           "\r\n\r\n");
            }
        }

        nmhGetFsEvcCVlanIdPreservation (i4EvcContextId, i4Evc, &i4EvcVlanIdPre);
        if (i4EvcVlanIdPre == MEF_DISABLED)
        {
            CliPrintf (CliHandle, "no ethernet evc preservation vlan-id\r\n\r\n");
        }

        nmhGetFsEvcCVlanCoSPreservation (i4EvcContextId, i4Evc, &i4EvcCosPre);
        if (i4EvcCosPre == MEF_DISABLED)
        {
            CliPrintf (CliHandle, "no ethernet evc preservation cos\r\n\r\n");
        }

        nmhGetFsEvcLoopbackStatus (i4EvcContextId, i4Evc, &i4EvcLoopbackStatus);
        if (i4EvcLoopbackStatus == MEF_ENABLED)
        {
            CliPrintf (CliHandle, "ethernet evc loopback enabled\r\n\r\n");
        }

        MefShowRunningEvcFilterConfig (CliHandle, i4EvcContextId, i4Evc);

        /* GVRP, GMRP, MVRP, MMRP and IGMP protocols do not support PEER */

        pMefEvcInfo = MefGetEvcEntry (i4EvcContextId, i4Evc);
        if (pMefEvcInfo != NULL)
        {
            if (MEF_EVC_GVRP_TUNNEL_STATUS (pMefEvcInfo) !=
                MEF_TUNNEL_PROTOCOL_TUNNEL)
            {
                CliPrintf (CliHandle,
                           "ethernet evc l2cp gvrp discard \r\n\r\n");
            }
            if (MEF_EVC_GMRP_TUNNEL_STATUS (pMefEvcInfo) !=
                MEF_TUNNEL_PROTOCOL_TUNNEL)
            {
                CliPrintf (CliHandle,
                           "ethernet evc l2cp gmrp discard \r\n\r\n");
            }
            if (MEF_EVC_MVRP_TUNNEL_STATUS (pMefEvcInfo) !=
                MEF_TUNNEL_PROTOCOL_TUNNEL)
            {
                CliPrintf (CliHandle,
                           "ethernet evc l2cp mvrp discard \r\n\r\n");
            }
            if (MEF_EVC_MMRP_TUNNEL_STATUS (pMefEvcInfo) !=
                MEF_TUNNEL_PROTOCOL_TUNNEL)
            {
                CliPrintf (CliHandle,
                           "ethernet evc l2cp mmrp discard \r\n\r\n");
            }
            if (MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo) !=
                MEF_TUNNEL_PROTOCOL_PEER)
            {
                if (MEF_EVC_ECFM_TUNNEL_STATUS (pMefEvcInfo) !=
                    MEF_TUNNEL_PROTOCOL_DISCARD)
                {
                    CliPrintf (CliHandle,
                               "ethernet evc l2cp ecfm tunnel \r\n\r\n");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "ethernet evc l2cp ecfm discard \r\n\r\n");
                }
            }
        }

        i4PreEvcContextId = i4EvcContextId;
        i4PreEvc = i4Evc;
        MefShowRunningUniListConfig (CliHandle,i4Evc);
    }
    while (nmhGetNextIndexFsEvcTable (i4PreEvcContextId, &i4EvcContextId,
                                      i4PreEvc, &i4Evc) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\n!\r\n");

    return CLI_SUCCESS;
}

INT4
MefShowRunningEvcFilterConfig (tCliHandle CliHandle, INT4 u4CxtId, INT4 i4Evc)
{
    UINT1               au1MacAddr[MEF_CLI_MAX_MAC_STRING_SIZE];
    tMacAddr            MacAddr;
    INT4                i4EvcFilterInstance = 0;
    INT4                i4PreEvcFilterInstance = 0;
    INT4                i4EvcContextId = 0;
    INT4                i4PreEvcContextId = 0;
    INT4                i4EvcIndex = 0;
    INT4                i4PreEvcIndex = 0;
    INT4                i4EvcFilterAction = 0;
    INT4                i4FsEvcFilterId = 0;
    INT4                i4RowStatus = 0;
    UINT1               u1FilterAction[][6] = { {"allow"},
    {"drop"}
    };

    while (nmhGetNextIndexFsEvcFilterTable (i4PreEvcContextId, &i4EvcContextId,
                                            i4PreEvcIndex, &i4EvcIndex,
                                            i4PreEvcFilterInstance,
                                            &i4EvcFilterInstance) ==
           SNMP_SUCCESS)
    {

        i4PreEvcFilterInstance = i4EvcFilterInstance;
        i4PreEvcContextId = i4EvcContextId;
        i4PreEvcIndex = i4EvcIndex;

        if ((i4EvcContextId != u4CxtId) || (i4EvcIndex != i4Evc))
        {
            continue;
        }

        nmhGetFsEvcFilterRowStatus (i4EvcContextId, i4EvcIndex,
                                    i4EvcFilterInstance, &i4RowStatus);

        if (i4RowStatus != ACTIVE)
        {
            continue;
        }

        MEMSET (&MacAddr, 0, sizeof (tMacAddr));

        nmhGetFsEvcFilterDestMacAddress (i4EvcContextId, i4EvcIndex,
                                         i4EvcFilterInstance, &MacAddr);

        nmhGetFsEvcFilterAction (i4EvcContextId, i4EvcIndex,
                                 i4EvcFilterInstance, &i4EvcFilterAction);

        nmhGetFsEvcFilterId (i4EvcContextId, i4EvcIndex,
                             i4EvcFilterInstance, &i4FsEvcFilterId);

        PrintMacAddress (MacAddr, au1MacAddr);

        CliPrintf (CliHandle, "ethernet evc filter-instance %d  filter-id %d"
                   " dest-mac %s %s\r\n", i4EvcFilterInstance,
                   i4FsEvcFilterId, au1MacAddr,
                   u1FilterAction[i4EvcFilterAction - 1]);

    }

    return CLI_SUCCESS;
}

INT4
MefShowRunningETreeConfig (tCliHandle CliHandle)
{
    INT4                i4MefETreeIngresPort = 0;
    INT4                i4MefETreeEvcIndex = 0;
    INT4                i4MefETreeEgressPort = 0;
    INT4                i4PreMefETreeIngresPort = 0;
    INT4                i4PreMefETreeEvcIndex = 0;
    INT4                i4PreMefETreeEgressPort = 0;
    INT4                i4MefETreeRowStatus = 0;

    if (nmhGetFirstIndexFsMefETreeTable (&i4MefETreeIngresPort,
                                         &i4MefETreeEvcIndex,
                                         &i4MefETreeEgressPort) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "! \r\n\r\n");

    do
    {

        nmhGetFsMefETreeRowStatus (i4MefETreeIngresPort, i4MefETreeEvcIndex,
                                   i4MefETreeEgressPort, &i4MefETreeRowStatus);

        if (i4MefETreeRowStatus != ACTIVE)
        {

            if (nmhGetNextIndexFsMefETreeTable (i4PreMefETreeIngresPort,
                                                &i4MefETreeIngresPort,
                                                i4PreMefETreeEvcIndex,
                                                &i4MefETreeEvcIndex,
                                                i4PreMefETreeEgressPort,
                                                &i4MefETreeEgressPort) !=
                SNMP_SUCCESS)
            {
                break;
            }

            continue;
        }

        CliPrintf (CliHandle, "mef e-tree evc %d ingress-port gigabitethernet"
                   " 0/%d egress-port-list gigabitethernet 0/%d"
                   "\r\n\r\n", i4MefETreeEvcIndex,
                   i4MefETreeIngresPort, i4MefETreeEgressPort);

        i4PreMefETreeIngresPort = i4MefETreeIngresPort;
        i4PreMefETreeEvcIndex = i4MefETreeEvcIndex;
        i4PreMefETreeEgressPort = i4MefETreeEgressPort;
    }
    while (nmhGetNextIndexFsMefETreeTable
           (i4PreMefETreeIngresPort, &i4MefETreeIngresPort,
            i4PreMefETreeEvcIndex, &i4MefETreeEvcIndex, i4PreMefETreeEgressPort,
            &i4MefETreeEgressPort) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

INT4
MefShowRunningCeVlanEvcMapConfig (tCliHandle CliHandle,INT4 i4CurrIfIndex)
{
    tMefCVlanEvcInfo   *pCVlanEvcInfo = NULL;
    INT4                i4IfIndex = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4CeVlan = 0;
    INT4                i4PrevCeVlan = 0;
    INT4                i4Evc = 0;
    INT4                i4Status = 0;
    UINT1               au1UniEvcId[UNI_ID_MAX_LENGTH];
    tSNMP_OCTET_STRING_TYPE UniEvcId;

    pCVlanEvcInfo = (tMefCVlanEvcInfo *) RBTreeGetFirst (MEF_CVLAN_EVC_TABLE);

    while (pCVlanEvcInfo != NULL)
    {
        if(i4CurrIfIndex == (INT4) pCVlanEvcInfo->u4IfIndex)
        {
            break;
        }
        pCVlanEvcInfo = (tMefCVlanEvcInfo *) RBTreeGetNext
            (MEF_CVLAN_EVC_TABLE, pCVlanEvcInfo, NULL);

    }

    if (pCVlanEvcInfo == NULL)
    {
        return CLI_SUCCESS;
    }

    i4IfIndex = (INT4) pCVlanEvcInfo->u4IfIndex;
    do
    {
        nmhGetFsUniCVlanEvcRowStatus (i4IfIndex, i4CeVlan, &i4Status);
        if (i4Status != ACTIVE)
        {
            i4PrevIfIndex = i4IfIndex;
            i4PrevCeVlan = i4CeVlan;

            if (nmhGetNextIndexFsUniCVlanEvcTable (i4PrevIfIndex, &i4IfIndex,
                                                   i4PrevCeVlan, &i4CeVlan)
                != SNMP_SUCCESS)
            {
                break;
            }
            continue;
        }
        if (i4CurrIfIndex == i4IfIndex)
        {
            nmhGetFsUniCVlanEvcEvcIndex (i4IfIndex, i4CeVlan, &i4Evc);

            MEMSET (&au1UniEvcId, 0, UNI_ID_MAX_LENGTH);
            UniEvcId.pu1_OctetList = au1UniEvcId;
            UniEvcId.i4_Length = UNI_ID_MAX_LENGTH;
            nmhGetFsUniEvcId (i4IfIndex, i4CeVlan, &UniEvcId);
            if (UniEvcId.i4_Length != 0)
            {
                CliPrintf (CliHandle, "ethernet map ce-vlan %d", i4CeVlan);
                CliPrintf (CliHandle, " evc %d ", i4Evc);
                CliPrintf (CliHandle, "uni-evc-id %s \r\n\r\n",
                           UniEvcId.pu1_OctetList);
            }
            else
            {
                CliPrintf (CliHandle, "ethernet map ce-vlan %d", i4CeVlan);
                CliPrintf (CliHandle, " evc %d \r\n", i4Evc);
            }
        }
        i4PrevIfIndex = i4IfIndex;
        i4PrevCeVlan = i4CeVlan;

    }
    while (nmhGetNextIndexFsUniCVlanEvcTable (i4PrevIfIndex, &i4IfIndex,
                                              i4PrevCeVlan, &i4CeVlan)
           == SNMP_SUCCESS);
    return CLI_SUCCESS;
}

INT4
MefShowRunningFilterConfig (tCliHandle CliHandle,INT4 i4CurrIfIndex)
{
    tMefFilterInfo     *pMefFilterInfo = NULL;
    INT4                i4FilterNo = 0;
    INT4                i4PrevFilterNo = 0;
    INT4                i4Dscp = 0;
    INT4                i4CVlanPriority = 0;
    INT4                i4Direction = 0;
    INT4                i4IfIndex = 0;
    INT4                i4Evc = 0;
    INT4                i4Status = 0;

    pMefFilterInfo = (tMefFilterInfo *) RBTreeGetFirst (MEF_FILTER_TABLE);

    while (pMefFilterInfo != NULL)
    {
        if(i4CurrIfIndex == (INT4) pMefFilterInfo->u4IfIndex)
        {
            break;
        }
        pMefFilterInfo = (tMefFilterInfo *)
            RBTreeGetNext (MEF_FILTER_TABLE, pMefFilterInfo, NULL);
    }

    if (pMefFilterInfo == NULL)
    {
        return CLI_SUCCESS;
    }

    i4FilterNo = (INT4) pMefFilterInfo->u4FilterNo;

    do
    {
        nmhGetFsMefFilterStatus (i4FilterNo, &i4Status);
        if (i4Status != ACTIVE)
        {
            i4PrevFilterNo = i4FilterNo;

            if (nmhGetNextIndexFsMefFilterTable (i4PrevFilterNo, &i4FilterNo)
                != SNMP_SUCCESS)
            {
                break;
            }
            continue;
        }

        nmhGetFsMefFilterIfIndex (i4FilterNo, &i4IfIndex);

        if (i4CurrIfIndex == i4IfIndex)
        {

            CliPrintf (CliHandle, "mef filter %d ", i4FilterNo);

            nmhGetFsMefFilterEvc (i4FilterNo, &i4Evc);
            CliPrintf (CliHandle, "evc %d ", i4Evc);


            nmhGetFsMefFilterCVlanPriority (i4FilterNo, &i4CVlanPriority);
            if (i4CVlanPriority != -1)
            {
                CliPrintf (CliHandle, "priority %d ", i4CVlanPriority);
            }

            nmhGetFsMefFilterDscp (i4FilterNo, &i4Dscp);
            if (i4Dscp != ISS_DSCP_INVALID)
            {
                CliPrintf (CliHandle, "dscp %d ", i4Dscp);
            }

            nmhGetFsMefFilterDirection (i4FilterNo, &i4Direction);
            if (i4Direction == ISS_DIRECTION_IN)
            {
                CliPrintf (CliHandle, "direction in \r\n\r\n");
            }

            if (i4Direction == ISS_DIRECTION_OUT)
            {
                CliPrintf (CliHandle, "direction out \r\n\r\n");
            }
        }
        i4PrevFilterNo = i4FilterNo;

    }
    while (nmhGetNextIndexFsMefFilterTable (i4PrevFilterNo, &i4FilterNo)
           == SNMP_SUCCESS);
    return CLI_SUCCESS;
}

INT4
MefShowRunningClassConfig (tCliHandle CliHandle)
{
    UINT4               u4PrevClass = 0;
    UINT4               u4Class = 0;
    INT4                i4Status = 0;

    CliPrintf (CliHandle, "\r\n\r\n");
    if (nmhGetFirstIndexFsMefClassTable (&u4Class) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        nmhGetFsMefClassStatus (u4Class, &i4Status);
        if (i4Status == ACTIVE)
        {
            CliPrintf (CliHandle, "mef class %d \r\n", u4Class);
        }
        u4PrevClass = u4Class;

    }
    while (nmhGetNextIndexFsMefClassTable (u4PrevClass, &u4Class)
           == SNMP_SUCCESS);
    return CLI_SUCCESS;
}

INT4
MefShowRunningClassMapConfig (tCliHandle CliHandle)
{
    UINT4               u4ClassMapId = 0;
    UINT4               u4PrevClassMapId = 0;
    UINT4               u4FilterId = 0;
    UINT4               u4Class = 0;
    INT4                i4Status = 0;

    if (nmhGetFirstIndexFsMefClassMapTable (&u4ClassMapId) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        nmhGetFsMefClassMapStatus (u4ClassMapId, &i4Status);
        if (i4Status == ACTIVE)
        {
            CliPrintf (CliHandle, "mef classmap %u ", u4ClassMapId);

            nmhGetFsMefClassMapFilterId (u4ClassMapId, &u4FilterId);
            CliPrintf (CliHandle, "filter %u ", u4FilterId);

            nmhGetFsMefClassMapCLASS (u4ClassMapId, &u4Class);
            CliPrintf (CliHandle, "class %u \r\n\r\n", u4Class);
        }

        u4PrevClassMapId = u4ClassMapId;

    }
    while (nmhGetNextIndexFsMefClassMapTable (u4PrevClassMapId, &u4ClassMapId)
           == SNMP_SUCCESS);
    return CLI_SUCCESS;
}

INT4
MefShowRunningMeterConfig (tCliHandle CliHandle)
{
    UINT4               u4MeterId = 0;
    UINT4               u4PrevMeterId = 0;
    UINT4               u4Cir = 0;
    UINT4               u4Cbs = 0;
    UINT4               u4Eir = 0;
    UINT4               u4Ebs = 0;
    INT4                i4ColorMode = 0;
    INT4                i4MeterType = 0;
    INT4                i4Status = 0;

    if (nmhGetFirstIndexFsMefMeterTable (&u4MeterId) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        nmhGetFsMefMeterStatus (u4MeterId, &i4Status);
        if (i4Status != ACTIVE)
        {
            u4PrevMeterId = u4MeterId;

            if (nmhGetNextIndexFsMefMeterTable (u4PrevMeterId, &u4MeterId)
                != SNMP_SUCCESS)
            {
                break;
            }
            continue;
        }

        CliPrintf (CliHandle, "mef meter %d ", u4MeterId);

        nmhGetFsMefMeterCIR (u4MeterId, &u4Cir);
        CliPrintf (CliHandle, "cir %u ", u4Cir);

        nmhGetFsMefMeterCBS (u4MeterId, &u4Cbs);
        CliPrintf (CliHandle, "cbs %u ", u4Cbs);

        nmhGetFsMefMeterEIR (u4MeterId, &u4Eir);
        CliPrintf (CliHandle, "eir %u ", u4Eir);

        nmhGetFsMefMeterEBS (u4MeterId, &u4Ebs);
        CliPrintf (CliHandle, "ebs %u ", u4Ebs);

        nmhGetFsMefMeterColorMode (u4MeterId, &i4ColorMode);
        if (i4ColorMode != QOS_METER_COLOR_BLIND)
        {
            CliPrintf (CliHandle, "color-aware");
        }

        nmhGetFsMefMeterType (u4MeterId, &i4MeterType);
        if (i4MeterType != QOS_METER_TYPE_MEF_DECOUPLED)
        {
            CliPrintf (CliHandle, "coupling-flag");
        }

        CliPrintf (CliHandle, "\r\n");
        u4PrevMeterId = u4MeterId;

    }
    while (nmhGetNextIndexFsMefMeterTable (u4PrevMeterId, &u4MeterId)
           == SNMP_SUCCESS);
    return CLI_SUCCESS;
}

INT4
MefShowRunningPolicyMapConfig (tCliHandle CliHandle)
{
    UINT4               u4PolicyMapId = 0;
    UINT4               u4PrevPolicyMapId = 0;
    UINT4               u4Class = 0;
    UINT4               u4FilterId = 0;
    INT4                i4Status = 0;

    if (nmhGetFirstIndexFsMefPolicyMapTable (&u4PolicyMapId) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        nmhGetFsMefPolicyMapStatus (u4PolicyMapId, &i4Status);
        if (i4Status == ACTIVE)
        {
            CliPrintf (CliHandle, "mef policy-map %d ", u4PolicyMapId);

            nmhGetFsMefPolicyMapCLASS (u4PolicyMapId, &u4Class);
            CliPrintf (CliHandle, "class %d ", u4Class);

            nmhGetFsMefPolicyMapMeterTableId (u4PolicyMapId, &u4FilterId);
            CliPrintf (CliHandle, "meter %d \n", u4FilterId);
        }

        u4PrevPolicyMapId = u4PolicyMapId;

    }
    while (nmhGetNextIndexFsMefPolicyMapTable (u4PrevPolicyMapId,
                                               &u4PolicyMapId) == SNMP_SUCCESS);
    return CLI_SUCCESS;
}
#endif
