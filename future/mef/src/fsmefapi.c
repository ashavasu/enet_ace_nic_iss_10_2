/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmefapi.c,v 1.3 2014/10/10 12:04:42 siva Exp $
 *
 * Description:
 *********************************************************************/
#ifndef __FSMEFAPI_C__
#define __FSMEFAPI_C__

#include "fsmefinc.h"

/***************************************************************************
* FUNCTION NAME    : MefApiLock
*
* DESCRIPTION      : Function to take the mutual exclusion protocol
*                    semaphore
*
* INPUT            : NONE
*
* OUTPUT           : NONE
*
* RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
*                                                                          
**************************************************************************/
INT4
MefApiLock (VOID)
{
    if (OsixTakeSem (SELF, MEF_SEM_NAME, OSIX_WAIT, 0) == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Failed to Take Mutual Exclusion Semaphore\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
* FUNCTION NAME    : MefApiUnLock
*
* DESCRIPTION      : Function gives the mutual exclusion protocol
*                    semaphore
*
* INPUT            : NONE
*
* OUTPUT           : NONE
*
* RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
*                                                                          
**************************************************************************/
INT4
MefApiUnLock (VOID)
{
    if (OsixGiveSem (SELF, MEF_SEM_NAME) == OSIX_FAILURE)
    {
        MEF_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Failed to Give Mutual Exclusion Semaphore\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/******************************************************************************
 *    Function Name       : MefApiHandleContextIndication                     *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever a context created, this *
 *                          function post a message to MEF for creating a     *
 *                          context                                           *
 *                                                                            *
 *    Input(s)            : u4ContextId  - Virtual Context Id                 *
 *                          u4Indication - MEF_CREATE_CONTEXT                 *
 *                                         MEF_DELETE_CONTEXT                 *
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns            : OSIX_SUCCESS                                       *
 *                         OSIX_FAILURE                                       *
 *****************************************************************************/
PUBLIC INT4
MefApiHandleContextIndication (UINT4 u4ContextId, UINT4 u4Indication)
{
    tMefMsg            *pMefMsg = NULL;

    if (gMefGlobalInfo.bIsMefInitialised != OSIX_TRUE)
    {
        return OSIX_FAILURE;
    }

    pMefMsg = (tMefMsg *) MemAllocMemBlk (MEF_QUEUE_MSG_POOL_ID);
    if (pMefMsg == NULL)
    {
        MEF_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "Failed to Allocate Memory to Post Create/Delete"
                      " Message for Context %u \r\n", u4ContextId);
        return OSIX_FAILURE;
    }

    MEMSET (pMefMsg, 0, sizeof (tMefMsg));

    pMefMsg->u4ContextId = u4ContextId;

    switch (u4Indication)
    {
        case MEF_CREATE_CONTEXT:
            pMefMsg->u4MsgType = MEF_CREATE_CONTEXT;
            break;

        case MEF_DELETE_CONTEXT:
            pMefMsg->u4MsgType = MEF_DELETE_CONTEXT;
            break;
    }

    if (MefPostMessageToQ (pMefMsg) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (MEF_QUEUE_MSG_POOL_ID, (UINT1 *) pMefMsg);
        return OSIX_FAILURE;
    }

    L2MI_SYNC_TAKE_SEM ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 *    Function Name       : MefApiHandlePortIndication                        *
 *                                                                            *
 *    Description         : Invoked by L2IWF whenever some port is mapped to  *
 *                          any  context                                      *
 *                                                                            *
 *    Input(s)            : u4ContextId  - context Id                         *
 *                          u2IfIndex    - mapped Port number                 *
 *                          u2LocalPortId - local Port number                 *
 *                          u4Inication - MEF_CREATE_PORT                     *
 *                                        MEF_DELETE_PORT                     *
 *                                        MEF_MAP_PORT                        *
 *                                        MEF_UNMAP_PORT                      * 
 *                                                                            *
 *    Output(s)           : None                                              *
 *                                                                            *
 *    Returns             : OSIX_SUCCESS                                      *
 *                          OSIX_FAILURE                                      *
 *****************************************************************************/
PUBLIC INT4
MefApiHandlePortIndication (UINT4 u4ContextId, UINT4 u4IfIndex,
                            UINT2 u2LocalPortId, UINT4 u4Inication)
{
    tMefMsg            *pMefMsg = NULL;

    if (gMefGlobalInfo.bIsMefInitialised != OSIX_TRUE)
    {
        return OSIX_FAILURE;
    }

    pMefMsg = (tMefMsg *) MemAllocMemBlk (MEF_QUEUE_MSG_POOL_ID);
    if (pMefMsg == NULL)
    {
        MEF_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "Failed to Allocate Memory to Post Create/Delete/Map/Unmap"
                      " Message for Port %d \r\n", u4IfIndex);
        return OSIX_FAILURE;
    }

    MEMSET (pMefMsg, 0, sizeof (tMefMsg));

    pMefMsg->u4ContextId = u4ContextId;
    pMefMsg->u4IfIndex = u4IfIndex;
    pMefMsg->u2PortNum = 0;

    switch (u4Inication)
    {
        case MEF_CREATE_PORT:
            pMefMsg->u4MsgType = MEF_CREATE_PORT;
            pMefMsg->u2PortNum = u2LocalPortId;
            break;

        case MEF_DELETE_PORT:
            pMefMsg->u4MsgType = MEF_DELETE_PORT;
            break;

        case MEF_MAP_PORT:
            pMefMsg->u4MsgType = MEF_MAP_PORT;
            pMefMsg->u2PortNum = u2LocalPortId;
            break;

        case MEF_UNMAP_PORT:
            pMefMsg->u4MsgType = MEF_UNMAP_PORT;
            break;
    }

    if (MefPostMessageToQ (pMefMsg) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (MEF_QUEUE_MSG_POOL_ID, (UINT1 *) pMefMsg);
        return OSIX_FAILURE;
    }

    L2MI_SYNC_TAKE_SEM ();
    return OSIX_SUCCESS;
}

/******************************************************************************
*    Function Name       : MefApiGetEvcInfo                                  *
*                                                                            *
*    Description         : Invoked by PB,QoS, EOAM modules whenever EVC is   *
*                          created. This function gets the tEvcInfo details  *
*                          for the given SVlanId                             *
*                                                                            *
*    Input(s)            : SVlanId  - Service Vlan                           *
*                          tEvcInfo - EvcInfo structure                      *
*                                                                            *
*    Output(s)           : None                                              *
*                                                                            *
*    Returns             : OSIX_SUCCESS                                      *
*                          OSIX_FAILURE                                      *
*****************************************************************************/
PUBLIC VOID
MefApiGetEvcInfo  (UINT4 SVlanId, tEvcInfo *pIssEvcInfoLocal)
{
     
     tEvcInfo   *pIssEvcInfo;

     pIssEvcInfo = MefGetEvcInfoEntry ((INT4)SVlanId);
     if (pIssEvcInfo != NULL)
     {
          pIssEvcInfoLocal->u4EvcId = pIssEvcInfo->u4EvcId;
          pIssEvcInfoLocal->SVlanId = pIssEvcInfo->SVlanId;
          pIssEvcInfoLocal->bCeVlanIdPreservation = pIssEvcInfo->bCeVlanIdPreservation;
          pIssEvcInfoLocal->bCeVlanCoSPreservation = pIssEvcInfo->bCeVlanCoSPreservation;
     }
}
                                                    
#endif
