/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmefcxt.c,v 1.1 2012/04/30 10:49:54 siva Exp $
 *
 * Description:
 * *********************************************************************/
#ifndef __FSMEFCXT_C__
#define __FSMEFCXT_C__
                                                            
#include "fsmefinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : MefContextCreateAndInit                          */
/*                                                                           */
/*    Description         : This function creates and do the proper          */
/*                          initialization for the context entry.            */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
MefContextCreateAndInit (UINT4 u4ContextId)
{
    tMefContextInfo         *pMefContextInfo = NULL;

    pMefContextInfo = MemAllocMemBlk (MEF_CONTEXT_POOL_ID);
    if (pMefContextInfo == NULL)
    {
        MEF_TRC_ARG1 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "Failed to Allocate Memory to Create Context %u \r\n",
                      u4ContextId);
        return OSIX_FAILURE;
    }

    MEMSET (pMefContextInfo, 0, sizeof (tMefContextInfo));

    pMefContextInfo->eMefTransportMode = MEF_TRANS_MODE_PB;

    gMefGlobalInfo.apContextInfo[u4ContextId] = pMefContextInfo;

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : MefContextDelete                                 */
/*                                                                           */
/*    Description         : This function clears the entries present in the  */
/*                          context and deletes context                      */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context index                      */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
MefContextDelete (UINT4 u4ContextId)
{
    tMefContextInfo         *pMefContextInfo = NULL;

    pMefContextInfo = gMefGlobalInfo.apContextInfo[u4ContextId];

    if (pMefContextInfo == NULL)
    {
        MEF_TRC_ARG1 (ALL_FAILURE_TRC, "Failed to Delete Context %u. "
                      "No Such Context Exists \r\n", u4ContextId);
        return OSIX_FAILURE;
    }
    
    if (MefDeleteAllEvcEntries (u4ContextId) != OSIX_SUCCESS)
    {
        MEF_TRC_ARG1 (ALL_FAILURE_TRC, "Failed to Delete All EVC Entries "
                      "in Context %u \r\n", u4ContextId);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (MEF_CONTEXT_POOL_ID, (UINT1 *) pMefContextInfo);

    gMefGlobalInfo.apContextInfo[u4ContextId] = NULL;

    return OSIX_SUCCESS;
}
#endif
