/* $Id: mefsz.c,v 1.2 2014/02/27 12:38:19 siva Exp $ */
#define _MEFSZ_C
#include "fsmefinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
INT4
MefSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MEF_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsMEFSizingParams[i4SizingId].u4StructSize,
                                     FsMEFSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(MEFMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            MefSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
MefSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsMEFSizingParams);
    return OSIX_SUCCESS;
}

VOID
MefSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MEF_MAX_SIZING_ID; i4SizingId++)
    {
        if (MEFMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (MEFMemPoolIds[i4SizingId]);
            MEFMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
