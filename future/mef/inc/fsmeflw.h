/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* 
* $Id: fsmeflw.h,v 1.10 2015/06/24 12:46:16 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMefContextTable. */
INT1
nmhValidateIndexInstanceFsMefContextTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefContextTable  */

INT1
nmhGetFirstIndexFsMefContextTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefContextTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefContextName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefTransmode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMefFrameLossBufferClear ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMefFrameDelayBufferClear ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMefFrameLossBufferSize ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMefFrameDelayBufferSize ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMefTransmode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMefFrameLossBufferClear ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMefFrameDelayBufferClear ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMefFrameLossBufferSize ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMefFrameDelayBufferSize ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMefTransmode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMefFrameLossBufferClear ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMefFrameDelayBufferClear ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMefFrameLossBufferSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMefFrameDelayBufferSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMefContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsUniTable. */
INT1
nmhValidateIndexInstanceFsUniTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsUniTable  */

INT1
nmhGetFirstIndexFsUniTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsUniTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsUniId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsUniPhysicalMedium ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniSpeed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsUniMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniMacLayer ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsUniMtu ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniServiceMultiplexingBundling ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsUniCVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniMaxEvcs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPDot1x ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPLacp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPStp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPGvrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPGmrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPMvrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPMmrp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPLldp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPElmi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPEcfm ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPOverrideOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsUniL2CPEoam ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsUniId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsUniServiceMultiplexingBundling ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsUniCVlanId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPDot1x ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPLacp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPStp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPGvrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPGmrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPMvrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPMmrp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPLldp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPElmi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPEcfm ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPOverrideOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsUniL2CPEoam ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsUniId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsUniServiceMultiplexingBundling ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsUniCVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPDot1x ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPLacp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPStp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPGvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPGmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPMvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPMmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPLldp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPElmi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPEcfm ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPOverrideOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsUniL2CPEoam ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsUniTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsEvcTable. */
INT1
nmhValidateIndexInstanceFsEvcTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsEvcTable  */

INT1
nmhGetFirstIndexFsEvcTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEvcTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvcId ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsEvcType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsEvcMaxUni ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsEvcCVlanIdPreservation ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsEvcCVlanCoSPreservation ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsEvcRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsEvcMtu ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsEvcL2CPTunnel ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsEvcL2CPPeer ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsEvcL2CPDiscard ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsEvcLoopbackStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvcId ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsEvcType ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsEvcCVlanIdPreservation ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsEvcCVlanCoSPreservation ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsEvcRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsEvcL2CPTunnel ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsEvcL2CPPeer ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsEvcL2CPDiscard ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsEvcLoopbackStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvcId ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsEvcType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsEvcCVlanIdPreservation ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsEvcCVlanCoSPreservation ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsEvcRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsEvcL2CPTunnel ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsEvcL2CPPeer ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsEvcL2CPDiscard ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsEvcLoopbackStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEvcTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsEvcFilterTable. */
INT1
nmhValidateIndexInstanceFsEvcFilterTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsEvcFilterTable  */

INT1
nmhGetFirstIndexFsEvcFilterTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEvcFilterTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvcFilterDestMacAddress ARG_LIST((INT4  , INT4  , INT4 ,tMacAddr * ));

INT1
nmhGetFsEvcFilterAction ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsEvcFilterId ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsEvcFilterRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsEvcFilterDestMacAddress ARG_LIST((INT4  , INT4  , INT4  ,tMacAddr ));

INT1
nmhSetFsEvcFilterAction ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsEvcFilterId ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsEvcFilterRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsEvcFilterDestMacAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tMacAddr ));

INT1
nmhTestv2FsEvcFilterAction ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsEvcFilterId ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsEvcFilterRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsEvcFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMefETreeTable. */
INT1
nmhValidateIndexInstanceFsMefETreeTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefETreeTable  */

INT1
nmhGetFirstIndexFsMefETreeTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefETreeTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefETreeRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMefETreeRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMefETreeRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMefETreeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsUniCVlanEvcTable. */
INT1
nmhValidateIndexInstanceFsUniCVlanEvcTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsUniCVlanEvcTable  */

INT1
nmhGetFirstIndexFsUniCVlanEvcTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsUniCVlanEvcTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsUniCVlanEvcEvcIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsUniCVlanEvcRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsUniEvcId ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsUniCVlanEvcEvcIndex ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsUniCVlanEvcRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsUniEvcId ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsUniCVlanEvcEvcIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsUniCVlanEvcRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsUniEvcId ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsUniCVlanEvcTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMefFilterTable. */
INT1
nmhValidateIndexInstanceFsMefFilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefFilterTable  */

INT1
nmhGetFirstIndexFsMefFilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefFilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefFilterDscp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMefFilterDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMefFilterEvc ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMefFilterCVlanPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMefFilterIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMefFilterStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMefFilterDscp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMefFilterDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMefFilterEvc ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMefFilterCVlanPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMefFilterIfIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMefFilterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMefFilterDscp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMefFilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMefFilterEvc ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMefFilterCVlanPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMefFilterIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMefFilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMefFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMefClassMapTable. */
INT1
nmhValidateIndexInstanceFsMefClassMapTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefClassMapTable  */

INT1
nmhGetFirstIndexFsMefClassMapTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefClassMapTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefClassMapName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefClassMapFilterId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMefClassMapCLASS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMefClassMapStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMefClassMapName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMefClassMapFilterId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMefClassMapCLASS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMefClassMapStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMefClassMapName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMefClassMapFilterId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefClassMapCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefClassMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMefClassMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMefClassTable. */
INT1
nmhValidateIndexInstanceFsMefClassTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefClassTable  */

INT1
nmhGetFirstIndexFsMefClassTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefClassTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefClassStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMefClassStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMefClassStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMefClassTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMefMeterTable. */
INT1
nmhValidateIndexInstanceFsMefMeterTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefMeterTable  */

INT1
nmhGetFirstIndexFsMefMeterTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefMeterTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefMeterName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefMeterType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMefMeterColorMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMefMeterCIR ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMefMeterCBS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMefMeterEIR ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMefMeterEBS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMefMeterStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMefMeterName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMefMeterType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMefMeterColorMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMefMeterCIR ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMefMeterCBS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMefMeterEIR ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMefMeterEBS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMefMeterStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMefMeterName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMefMeterType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMeterColorMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMeterCIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMeterCBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMeterEIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMeterEBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMeterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMefMeterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMefPolicyMapTable. */
INT1
nmhValidateIndexInstanceFsMefPolicyMapTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefPolicyMapTable  */

INT1
nmhGetFirstIndexFsMefPolicyMapTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefPolicyMapTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefPolicyMapName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefPolicyMapCLASS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMefPolicyMapMeterTableId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMefPolicyMapStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMefPolicyMapName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMefPolicyMapCLASS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMefPolicyMapMeterTableId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMefPolicyMapStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMefPolicyMapName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMefPolicyMapCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefPolicyMapMeterTableId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefPolicyMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMefPolicyMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMefMepTable. */
INT1
nmhValidateIndexInstanceFsMefMepTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefMepTable  */

INT1
nmhGetFirstIndexFsMefMepTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefMepTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefMepTransmitLmmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitLmmResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitLmmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitLmmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepTransmitLmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitLmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitLmmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMefMepTransmitLmmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepTransmitLmmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitLmmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTxFCf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepRxFCb ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepTxFCb ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepNearEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepFarEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepTransmitDmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitDmResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitDmType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitDmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitDmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitDmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmit1DmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitDmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmit1DmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitDmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMefMepTransmitDmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepTransmitDmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepTransmitDmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepDmrOptionalFields ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMep1DmRecvCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepFrameDelayThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepRxFCf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMep1DmTransInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMefMepTransmitLmmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitLmmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitLmmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepTransmitLmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitLmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitLmmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMefMepTransmitLmmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepTransmitLmmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitLmmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepNearEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepFarEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepTransmitDmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitDmType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitDmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitDmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitDmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmit1DmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitDmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmit1DmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitDmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMefMepTransmitDmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepTransmitDmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepTransmitDmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepDmrOptionalFields ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMep1DmRecvCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepFrameDelayThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMep1DmTransInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMefMepTransmitLmmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitLmmInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitLmmDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepTransmitLmmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitLmmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitLmmDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMefMepTransmitLmmDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepTransmitLmmDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitLmmMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepNearEndFrameLossThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepFarEndFrameLossThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepTransmitDmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitDmType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitDmInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitDmMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitDmmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmit1DmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitDmmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmit1DmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitDmDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMefMepTransmitDmDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepTransmitDmDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepTransmitDmDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepDmrOptionalFields ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMep1DmRecvCapability ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepFrameDelayThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMep1DmTransInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMefMepTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMefFdTable. */
INT1
nmhValidateIndexInstanceFsMefFdTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefFdTable  */

INT1
nmhGetFirstIndexFsMefFdTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefFdTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefFdTxTimeStampf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefFdMeasurementTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFdPeerMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMefFdIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefFdDelayValue ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefFdIFDV ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefFdFDV ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefFdMeasurementType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMefFdStatsTable. */
INT1
nmhValidateIndexInstanceFsMefFdStatsTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefFdStatsTable  */

INT1
nmhGetFirstIndexFsMefFdStatsTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefFdStatsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefFdStatsTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFdStatsDmmOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFdStatsDmrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFdStatsDelayAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefFdStatsFDVAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefFdStatsIFDVAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefFdStatsDelayMin ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefFdStatsDelayMax ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMefFlTable. */
INT1
nmhValidateIndexInstanceFsMefFlTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefFlTable  */

INT1
nmhGetFirstIndexFsMefFlTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefFlTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefFlMeasurementTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFlPeerMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMefFlIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefFlFarEndLoss ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFlNearEndLoss ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFlMeasurementTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMefFlStatsTable. */
INT1
nmhValidateIndexInstanceFsMefFlStatsTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefFlStatsTable  */

INT1
nmhGetFirstIndexFsMefFlStatsTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefFlStatsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefFlStatsTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFlStatsMessagesOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFlStatsMessagesIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFlStatsFarEndLossAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFlStatsNearEndLossAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFlStatsMeasurementType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefFlStatsFarEndLossMin ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFlStatsFarEndLossMax ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFlStatsNearEndLossMin ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefFlStatsNearEndLossMax ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMefMepAvailabilityTable. */
INT1
nmhValidateIndexInstanceFsMefMepAvailabilityTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefMepAvailabilityTable  */

INT1
nmhGetFirstIndexFsMefMepAvailabilityTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefMepAvailabilityTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefMepAvailabilityStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepAvailabilityResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepAvailabilityInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepAvailabilityDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepAvailabilityLowerThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefMepAvailabilityUpperThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefMepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepAvailabilityWindowSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepAvailabilityDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsMefMepAvailabilityDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepAvailabilityDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepAvailabilityType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepAvailabilitySchldDownInitTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepAvailabilitySchldDownEndTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepAvailabilityPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMefMepAvailabilityDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMefMepAvailabilityPercentage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefMepAvailabilityRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMefMepAvailabilityStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepAvailabilityInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepAvailabilityDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepAvailabilityLowerThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMefMepAvailabilityUpperThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMefMepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepAvailabilityWindowSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepAvailabilityDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsMefMepAvailabilityDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepAvailabilityDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepAvailabilityType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepAvailabilitySchldDownInitTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepAvailabilitySchldDownEndTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepAvailabilityPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMefMepAvailabilityDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMefMepAvailabilityRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMefMepAvailabilityStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepAvailabilityInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepAvailabilityDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepAvailabilityLowerThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMefMepAvailabilityUpperThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMefMepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepAvailabilityWindowSize ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepAvailabilityDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsMefMepAvailabilityDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepAvailabilityDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepAvailabilityType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepAvailabilitySchldDownInitTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepAvailabilitySchldDownEndTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepAvailabilityPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMefMepAvailabilityDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMefMepAvailabilityRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMefMepAvailabilityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMefUniListTable. */
INT1
nmhValidateIndexInstanceFsMefUniListTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMefUniListTable  */

INT1
nmhGetFirstIndexFsMefUniListTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMefUniListTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMefUniId ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMefUniType ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMefUniListRowStatus ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMefUniType ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMefUniListRowStatus ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMefUniType ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMefUniListRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMefUniListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsEvcStatsTable. */
INT1
nmhValidateIndexInstanceFsEvcStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsEvcStatsTable  */

INT1
nmhGetFirstIndexFsEvcStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsEvcStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsEvcStatsRxFrames ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsEvcStatsRxBytes ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsEvcStatsTxFrames ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsEvcStatsTxBytes ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsEvcStatsDiscardFrames ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsEvcStatsDiscardBytes ARG_LIST((INT4  , INT4 ,UINT4 *));
