/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mefextn.h,v 1.19 2015/08/21 12:09:52 siva Exp $
 *
 * Description: This file contains the PUBLICs.
 *            
 *******************************************************************/

/* PUBLIC Get routine */
PUBLIC INT1
nmhGetIfType ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetIssPortCtrlSpeed ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetIfMtu ARG_LIST((INT4 ,INT4 *));

PUBLIC INT4 
nmhGetIssPortCtrlDuplex PROTO ((INT4, INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitLmmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIVlanTunnelProtocolDot1x ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIVlanTunnelProtocolLacp ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIVlanTunnelProtocolStp ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIVlanTunnelProtocolGvrp ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIVlanTunnelProtocolGmrp ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIVlanTunnelProtocolIgmp ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIVlanTunnelProtocolMvrp ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIVlanTunnelProtocolMmrp ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIVlanTunnelProtocolLldp ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIVlanTunnelProtocolElmi ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731ContextName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731FrameLossBufferClear ARG_LIST((UINT4 ,INT4 *));

PUBLIC INT1                                                                                                                  nmhGetFsMIY1731FrameDelayBufferClear ARG_LIST((UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731FrameLossBufferSize ARG_LIST((UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731FrameDelayBufferSize ARG_LIST((UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitLmmResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitLmmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitLmmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitLmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitLmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitLmmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitLmmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitLmmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitLmmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTxFCf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepRxFCb ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTxFCb ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepNearEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepFarEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitDmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitDmResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitDmType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitDmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitDmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitDmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmit1DmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitDmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmit1DmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitDmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitDmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitDmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepTransmitDmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepDmrOptionalFields ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731Mep1DmRecvCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepFrameDelayThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepRxFCf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731Mep1DmTransInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731FdTxTimeStampf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731FdMeasurementTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FdPeerMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

PUBLIC INT1
nmhGetFsMIY1731FdIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731FdDelayValue ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731FdIFDV ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731FdFDV ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731FdMeasurementType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731FdStatsTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FdStatsDmmOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FdStatsDmrIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FdStatsDelayAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731FdStatsFDVAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731FdStatsIFDVAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731FdStatsDelayMin ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731FdStatsDelayMax ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731FlMeasurementTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlPeerMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

PUBLIC INT1
nmhGetFsMIY1731FlIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlFarEndLoss ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlNearEndLoss ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlMeasurementTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlStatsTimeStamp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlStatsMessagesOut ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlStatsMessagesIn ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlStatsFarEndLossAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlStatsNearEndLossAverage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlStatsMeasurementType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlStatsFarEndLossMin ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlStatsFarEndLossMax ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlStatsNearEndLossMin ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731FlStatsNearEndLossMax ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));


PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityResultOK ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityLowerThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityUpperThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityWindowSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * ));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilitySchldDownInitTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilitySchldDownEndTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityPercentage ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFsMIY1731MepAvailabilityRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetNextIndexVplsPwBindTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

PUBLIC INT1
nmhGetFsMplsL2VpnNextFreePwEnetPwInstance ARG_LIST((UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetPwEnetPortVlan ARG_LIST((UINT4  , UINT4 ,UINT4 *));

PUBLIC INT1
nmhGetPwEnetPortIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMplsVplsL2MapFdbId ARG_LIST((UINT4 ,INT4 *));

PUBLIC INT1
nmhGetFsMplsPortRowStatus ARG_LIST((INT4  ,INT4 *));

/* PUBLIC Set routine */

PUBLIC INT1
nmhSetFsMIY1731FrameLossBufferClear ARG_LIST((UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731FrameDelayBufferClear ARG_LIST((UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731FrameLossBufferSize ARG_LIST((UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731FrameDelayBufferSize ARG_LIST((UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolDot1x ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolLacp ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolStp ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolGvrp ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolGmrp ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolIgmp ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolMvrp ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolMmrp ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolLldp ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolElmi ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolEcfm ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelProtocolEoam ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIVlanTunnelOverrideOption ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitLmmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitLmmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitLmmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitLmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitLmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitLmmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitLmmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitLmmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitLmmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepNearEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepFarEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitDmStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitDmType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitDmInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitDmMessages ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitDmmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmit1DmDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitDmmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmit1DmPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitDmDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitDmDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitDmDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepTransmitDmDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepDmrOptionalFields ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731Mep1DmRecvCapability ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepFrameDelayThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731Mep1DmTransInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityInterval ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityLowerThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityUpperThreshold ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityWindowSize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityDestMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityDestIsMepId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilitySchldDownInitTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilitySchldDownEndTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityPriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityDropEnable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIY1731MepAvailabilityRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetIfMainBrgPortType ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIPbPortBundleStatus ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIPbPortMultiplexStatus ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIPbSVlanConfigServiceType ARG_LIST((INT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetIfMainDesc ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhSetFsDot1qPvid ARG_LIST((INT4  ,UINT4 ));

PUBLIC INT1
nmhSetFsMplsPortBundleStatus ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMplsPortMultiplexStatus ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMplsPortAllToOneBundleStatus ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMplsPortRowStatus ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetPwEnetPwVlan ARG_LIST((UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetPwEnetVlanMode ARG_LIST((UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetPwEnetPortVlan ARG_LIST((UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhSetPwEnetPortIfIndex ARG_LIST((UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetPwEnetVcIfIndex ARG_LIST((UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetPwEnetRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhSetDot1adMIPepPvid ARG_LIST((INT4  , INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsMIPbPortCVlan ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFsQoSPolicyMapMeterTableId ARG_LIST((UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsQoSPolicyMapOutProfileActionFlag ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhSetFsQoSPolicyMapOutProfileActionFlag ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhSetFsQoSClassToPriorityGroupName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhSetFsQoSClassMapPriorityMapId ARG_LIST((UINT4 ,UINT4 ));

/* PUBLIC Test routine */
PUBLIC INT1
nmhTestv2IssExtL3FilterDscp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2IssExtL3FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsQoSPolicyMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsQoSClassToPriorityGroupName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolDot1x ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolLacp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolStp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolGvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolGmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolIgmp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolMvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolMmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolLldp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolElmi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolEcfm ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelProtocolEoam ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIVlanTunnelOverrideOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731FrameLossBufferClear ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731FrameDelayBufferClear ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731FrameLossBufferSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731FrameDelayBufferSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitLmmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitLmmInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitLmmDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitLmmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitLmmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitLmmDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitLmmDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitLmmDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitLmmMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepNearEndFrameLossThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepFarEndFrameLossThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitDmStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitDmType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitDmInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitDmMessages ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitDmmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmit1DmDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitDmmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmit1DmPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitDmDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitDmDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitDmDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepTransmitDmDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepDmrOptionalFields ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731Mep1DmRecvCapability ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepFrameDelayThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731Mep1DmTransInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityDeadline ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityLowerThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityUpperThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityWindowSize ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityDestMacAddress ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityDestMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityDestIsMepId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilitySchldDownInitTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityPriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityDropEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilityRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIY1731MepAvailabilitySchldDownEndTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2IfMainBrgPortType (UINT4 *  ,INT4  ,INT4 );

PUBLIC INT1
nmhTestv2FsMIPbPortBundleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIPbPortMultiplexStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIPbSVlanConfigServiceType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2IfMainDesc ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhTestv2FsDot1qPvid ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2FsMplsPortBundleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMplsPortMultiplexStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMplsPortAllToOneBundleStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMplsPortRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhValidateIndexInstanceFsMIY1731MepTable ARG_LIST((UINT4  , UINT4  , UINT4  ,
                                                    UINT4 ));                   
PUBLIC INT1
nmhGetFirstIndexFsMIY1731MepTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * ,
                                            UINT4 *));
PUBLIC INT1
nmhGetNextIndexFsMIY1731MepTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * ,
                                          UINT4 , UINT4 * , UINT4 , UINT4 *));
PUBLIC INT1
nmhValidateIndexInstanceFsMIY1731FdTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

PUBLIC INT1
nmhGetFirstIndexFsMIY1731FdTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

PUBLIC INT1
nmhGetNextIndexFsMIY1731FdTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

PUBLIC INT1
nmhValidateIndexInstanceFsMIY1731FdStatsTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

PUBLIC INT1
nmhGetFirstIndexFsMIY1731FdStatsTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

PUBLIC INT1
nmhGetNextIndexFsMIY1731FdStatsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

PUBLIC INT1
nmhValidateIndexInstanceFsMIY1731FlTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

PUBLIC INT1
nmhGetFirstIndexFsMIY1731FlTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

PUBLIC INT1
nmhGetNextIndexFsMIY1731FlTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

PUBLIC INT1
nmhValidateIndexInstanceFsMIY1731FlStatsTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

PUBLIC INT1
nmhGetFirstIndexFsMIY1731FlStatsTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

PUBLIC INT1
nmhGetNextIndexFsMIY1731FlStatsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

PUBLIC INT1
nmhValidateIndexInstanceFsMIY1731MepAvailabilityTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

PUBLIC INT1
nmhGetFirstIndexFsMIY1731MepAvailabilityTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

PUBLIC INT1
nmhGetNextIndexFsMIY1731MepAvailabilityTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

PUBLIC INT1 nmhSetIssExtL2FilterStatus ARG_LIST((INT4  ,INT4 ));
PUBLIC INT1 nmhSetIssExtL2FilterDstMacAddr ARG_LIST((INT4  ,tMacAddr ));
PUBLIC INT1 nmhSetIssExtL2FilterAction ARG_LIST((INT4  ,INT4 ));
PUBLIC INT1 nmhSetIssExtL3FilterDscp ARG_LIST((INT4  ,INT4 ));
PUBLIC INT1 nmhSetIssExtL2FilterDirection ARG_LIST((INT4  ,INT4 ));
PUBLIC INT1 nmhSetIssExtL3FilterStatus ARG_LIST((INT4  ,INT4 ));
PUBLIC INT1 nmhGetIssExtL3FilterStatus ARG_LIST((INT4  ,INT4 *));
PUBLIC INT1 nmhSetIssExtL3FilterDstMacAddr ARG_LIST((INT4  ,tMacAddr ));
PUBLIC INT1 nmhSetIssExtL3FilterAction ARG_LIST((INT4  ,INT4 ));
PUBLIC INT1 nmhSetIssExtL3FilterDirection ARG_LIST((INT4  ,INT4 ));              
PUBLIC INT1 nmhSetFsQoSClassMapStatus ARG_LIST((UINT4  ,INT4 ));
PUBLIC INT1 nmhGetFsQoSClassMapStatus ARG_LIST((UINT4  ,INT4 *));
PUBLIC INT1 nmhSetFsQoSClassMapName ARG_LIST((UINT4  ,
                                              tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 nmhSetFsQoSClassMapL3FilterId ARG_LIST((UINT4  ,UINT4 ));
PUBLIC INT1 nmhSetFsQoSClassMapCLASS ARG_LIST((UINT4  ,UINT4 ));
PUBLIC INT1 nmhSetFsQoSClassToPriorityStatus ARG_LIST((UINT4  ,INT4 ));
PUBLIC INT1 nmhSetFsQoSMeterStatus ARG_LIST((UINT4  ,INT4 ));
PUBLIC INT1 nmhSetFsQoSMeterName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 nmhSetFsQoSMeterType ARG_LIST((UINT4  ,INT4 ));
PUBLIC INT1 nmhSetFsQoSMeterColorMode ARG_LIST((UINT4  ,INT4 ));
PUBLIC INT1 nmhSetFsQoSMeterCIR ARG_LIST((UINT4  ,UINT4 ));
PUBLIC INT1 nmhSetFsQoSMeterCBS ARG_LIST((UINT4  ,UINT4 ));
PUBLIC INT1 nmhSetFsQoSMeterEIR ARG_LIST((UINT4  ,UINT4 ));
PUBLIC INT1 nmhSetFsQoSMeterEBS ARG_LIST((UINT4  ,UINT4 ));                      
PUBLIC INT1 nmhSetFsQoSPolicyMapStatus ARG_LIST((UINT4  ,INT4 ));
PUBLIC INT1 nmhGetFsQoSPolicyMapStatus ARG_LIST((UINT4  ,INT4 *));
PUBLIC INT1 nmhSetFsQoSPolicyMapName ARG_LIST((UINT4  ,
                                              tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 nmhSetFsQoSPolicyMapIfIndex ARG_LIST((UINT4  ,UINT4 ));
PUBLIC INT1 nmhSetFsQoSPolicyMapCLASS ARG_LIST((UINT4  ,UINT4 ));
PUBLIC INT1 nmhSetIssMetroL3FilterSVlanId ARG_LIST((INT4  ,INT4 ));
PUBLIC INT1 nmhSetIssMetroL3FilterCVlanPriority ARG_LIST((INT4  ,INT4 ));
PUBLIC INT1 nmhSetIssExtL3FilterInPortList ARG_LIST((INT4,
                                           tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 nmhSetIssExtL3FilterOutPortList ARG_LIST((INT4,
                                           tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 nmhValidateIndexInstanceIssAclL2FilterTable ARG_LIST((INT4));
PUBLIC INT1 nmhTestv2IssExtL3FilterStatus ARG_LIST ((UINT4 *, INT4, INT4 ));
PUBLIC INT1 nmhTestv2IssExtL2FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
PUBLIC INT1 nmhTestv2IssExtL2FilterDstMacAddr ARG_LIST((UINT4 *  ,INT4  ,                                                               tMacAddr ));
PUBLIC INT1 nmhTestv2IssExtL2FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
PUBLIC INT1 nmhTestv2IssExtL2FilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
PUBLIC INT1 nmhTestv2IssExtL3FilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
PUBLIC INT1 nmhTestv2FsQoSClassMapName ARG_LIST((UINT4 *  ,UINT4  ,                                                              tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 nmhTestv2FsQoSClassMapL3FilterId ARG_LIST((UINT4 *  ,UINT4  ,                                                              UINT4 ));
PUBLIC INT1 nmhTestv2FsQoSClassMapCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
PUBLIC INT1 nmhTestv2FsQoSClassToPriorityStatus ARG_LIST((UINT4 *  ,UINT4  ,                                                               INT4 ));
PUBLIC INT1 nmhTestv2FsQoSMeterName ARG_LIST((UINT4 *  ,UINT4  ,                                                               tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 nmhTestv2FsQoSMeterType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
PUBLIC INT1 nmhTestv2FsQoSMeterColorMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
PUBLIC INT1 nmhTestv2FsQoSMeterCIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));         
PUBLIC INT1 nmhTestv2FsQoSMeterCBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
PUBLIC INT1 nmhTestv2FsQoSMeterEIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
PUBLIC INT1 nmhTestv2FsQoSMeterEBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
PUBLIC INT1 nmhTestv2FsQoSPolicyMapName ARG_LIST((UINT4 *  ,UINT4  ,                                                              tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT1 nmhTestv2FsQoSPolicyMapIfIndex ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
PUBLIC INT1 nmhTestv2FsQoSPolicyMapCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
PUBLIC INT1 nmhTestv2FsQoSPolicyMapMeterTableId ARG_LIST((UINT4 *  ,UINT4  ,                                                               UINT4 ));
PUBLIC INT1 nmhTestv2IssMetroL3FilterSVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
PUBLIC INT1 nmhTestv2IssMetroL3FilterCVlanPriority ARG_LIST((UINT4 *  ,INT4  ,
                                                            INT4 ));

PUBLIC INT1
nmhValidateIndexInstanceIssPortIsolationTable ARG_LIST((INT4  , INT4  , INT4 ));

PUBLIC INT1
nmhGetFirstIndexIssPortIsolationTable ARG_LIST((INT4 * , INT4 * , INT4 *));

PUBLIC INT1
nmhGetNextIndexIssPortIsolationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

PUBLIC INT1
nmhGetIssPortIsolationStorageType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

PUBLIC INT1
nmhGetIssPortIsolationRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

PUBLIC INT1
nmhSetIssPortIsolationRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2IssPortIsolationRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

PUBLIC INT1
nmhSetDot1adMICVidRegistrationUntaggedPep ARG_LIST( (INT4 , INT4 , INT4));

PUBLIC INT1
nmhTestv2Dot1adMICVidRegistrationUntaggedPep ARG_LIST( (UINT4 *, INT4 , INT4 , INT4));

PUBLIC INT1
nmhSetDot1adMICVidRegistrationUntaggedCep ARG_LIST( (INT4 , INT4 , INT4));

PUBLIC INT1
nmhSetDot1adMICVidRegistrationRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

PUBLIC INT1
nmhGetDot1adMICVidRegistrationRowStatus ARG_LIST((INT4  , INT4  ,INT4 *));

PUBLIC INT1
nmhSetDot1adCVidRegistrationSVid ARG_LIST((INT4  , INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2Dot1adMICVidRegistrationRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

PUBLIC INT1
nmhSetDot1adMICVidRegistrationSVid ARG_LIST((INT4  , INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2Dot1adMICVidRegistrationSVid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2Dot1adMIPepPvid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsMIPbPortCVlan ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsQoSClassMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FsQoSMeterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2PwEnetVcIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2PwEnetPwVlan ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2PwEnetVlanMode ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2PwEnetPortVlan ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

PUBLIC INT1
nmhTestv2PwEnetPortIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhTestv2PwEnetRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

PUBLIC INT1
nmhGetFsVcAlias (INT4 i4FsVCId, tSNMP_OCTET_STRING_TYPE * pRetValFsVcAlias);

PUBLIC INT1
nmhGetFsMIVlanBridgeMode ARG_LIST ((INT4, INT4 *));

PUBLIC INT1
nmhTestv2FsMIDot1qFutureVlanLoopbackStatus ARG_LIST ((UINT4 *, INT4, UINT4, INT4));

PUBLIC INT1
nmhSetFsMIDot1qFutureVlanLoopbackStatus ARG_LIST ((INT4, UINT4, INT4));


INT1
nmhTestv2FsMIServiceVlanTunnelProtocolStatus (UINT4 *pu4ErrorCode,
                                              INT4 i4FsMIVlanContextId,
                                              INT4 i4FsMIServiceVlanId,
                                              INT4 i4FsMIServiceProtocolEnum,
                                              INT4
                                              i4TestValFsMIServiceVlanTunnelProtocolStatus);
INT1
nmhSetFsMIServiceVlanTunnelProtocolStatus (INT4 i4FsMIVlanContextId,
                                           INT4 i4FsMIServiceVlanId,
                                           INT4 i4FsMIServiceProtocolEnum,
                                           INT4
                                           i4SetValFsMIServiceVlanTunnelProtocolStatus);
INT4
VlanSetTunnelStatusPerVLAN (UINT4 u4ContextId, tVlanId VlanId,
                                            UINT1 u1ProtocolTunnelStatus);

INT1
nmhGetFsQoSMeterStatus (UINT4 u4FsQoSMeterId, INT4 *pi4RetValFsQoSMeterStatus);

INT1
nmhGetDot1adMICVidRegistrationUntaggedPep (INT4 i4Dot1adMIPortNum,
                                           INT4 i4CVidRegistrationCVid,
                                           INT4 *pi4CVidRegistrationUntaggedPep);

INT1
nmhGetDot1adMICVidRegistrationUntaggedCep (INT4 i4Dot1adMIPortNum,
                                           INT4 i4CVidRegistrationCVid,
                                           INT4 *pi4CVidRegistrationUntaggedCep);
