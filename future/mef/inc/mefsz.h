/********************************************************************
* Copyright (C) 2012 Aricent Inc . All Rights Reserved
*
* $Id: mefsz.h,v 1.6 2014/10/10 12:05:50 siva Exp $
*
* Description:
*********************************************************************/

enum {
    MAX_MEF_CLS_MAP_TBL_MAX_ENTRIES_SIZING_ID,
    MAX_MEF_CVLAN_EVC_MAP_ENTIES_SIZING_ID,
    MAX_MEF_EVC_ENTRIES_SIZING_ID,
    MAX_MEF_ISS_EVC_ENTRIES_SIZING_ID,
    MAX_MEF_EVC_FILTER_ENTRIES_SIZING_ID,
    MAX_MEF_FILTER_ENTRIES_SIZING_ID,
    MAX_MEF_MAX_CONTEXTS_SIZING_ID,
    MAX_MEF_MAX_NUM_OF_CLASSES_SIZING_ID,
    MAX_MEF_METER_TBL_MAX_ENTRIES_SIZING_ID,
    MAX_MEF_PLY_MAP_TBL_MAX_ENTRIES_SIZING_ID,
    MAX_MEF_Q_MSGS_SIZING_ID,
    MAX_MEF_UNI_ENTRIES_SIZING_ID,
    MAX_MEF_UNI_LIST_ENTRIES_SIZING_ID,
    MEF_MAX_SIZING_ID
};


#ifdef  _MEFSZ_C
tMemPoolId MEFMemPoolIds[ MEF_MAX_SIZING_ID];
INT4  MefSizingMemCreateMemPools(VOID);
VOID  MefSizingMemDeleteMemPools(VOID);
INT4  MefSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _MEFSZ_C  */
extern tMemPoolId MEFMemPoolIds[ ];
extern INT4  MefSizingMemCreateMemPools(VOID);
extern VOID  MefSizingMemDeleteMemPools(VOID);
#endif /*  _MEFSZ_C  */


#ifdef  _MEFSZ_C
tFsModSizingParams FsMEFSizingParams [] = {
{ "tMefClassMapInfo", "MAX_MEF_CLS_MAP_TBL_MAX_ENTRIES", sizeof(tMefClassMapInfo),MAX_MEF_CLS_MAP_TBL_MAX_ENTRIES, MAX_MEF_CLS_MAP_TBL_MAX_ENTRIES,0 },
{ "tMefCVlanEvcInfo", "MAX_MEF_CVLAN_EVC_MAP_ENTIES", sizeof(tMefCVlanEvcInfo),MAX_MEF_CVLAN_EVC_MAP_ENTIES, MAX_MEF_CVLAN_EVC_MAP_ENTIES,0 },
{ "tMefEvcInfo", "MAX_MEF_EVC_ENTRIES", sizeof(tMefEvcInfo),MAX_MEF_EVC_ENTRIES, MAX_MEF_EVC_ENTRIES,0 },
{ "tEvcInfo", "MAX_MEF_EVC_ENTRIES", sizeof(tEvcInfo),MAX_MEF_EVC_ENTRIES, MAX_MEF_EVC_ENTRIES,0 },
{ "tMefEvcFilterInfo", "MAX_MEF_EVC_FILTER_ENTRIES", sizeof(tMefEvcFilterInfo),MAX_MEF_EVC_FILTER_ENTRIES, MAX_MEF_EVC_FILTER_ENTRIES,0 },
{ "tMefFilterInfo", "MAX_MEF_FILTER_ENTRIES", sizeof(tMefFilterInfo),MAX_MEF_FILTER_ENTRIES, MAX_MEF_FILTER_ENTRIES,0 },
{ "tMefContextInfo", "MAX_MEF_MAX_CONTEXTS", sizeof(tMefContextInfo),MAX_MEF_MAX_CONTEXTS, MAX_MEF_MAX_CONTEXTS,0 },
{ "tMefClassInfo", "MAX_MEF_MAX_NUM_OF_CLASSES", sizeof(tMefClassInfo),MAX_MEF_MAX_NUM_OF_CLASSES, MAX_MEF_MAX_NUM_OF_CLASSES,0 },
{ "tMefMeterInfo", "MAX_MEF_METER_TBL_MAX_ENTRIES", sizeof(tMefMeterInfo),MAX_MEF_METER_TBL_MAX_ENTRIES, MAX_MEF_METER_TBL_MAX_ENTRIES,0 },
{ "tMefPolicyMapInfo", "MAX_MEF_PLY_MAP_TBL_MAX_ENTRIES", sizeof(tMefPolicyMapInfo),MAX_MEF_PLY_MAP_TBL_MAX_ENTRIES, MAX_MEF_PLY_MAP_TBL_MAX_ENTRIES,0 },
{ "tMefMsg", "MAX_MEF_Q_MSGS", sizeof(tMefMsg),MAX_MEF_Q_MSGS, MAX_MEF_Q_MSGS,0 },
{ "tMefUniInfo", "MAX_MEF_UNI_ENTRIES", sizeof(tMefUniInfo),MAX_MEF_UNI_ENTRIES, MAX_MEF_UNI_ENTRIES,0 },
{ "tMefUniList", "MAX_MEF_UNI_LIST_ENTRIES", sizeof(tMefUniList),MAX_MEF_UNI_LIST_ENTRIES, MAX_MEF_UNI_LIST_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _MEFSZ_C  */
extern tFsModSizingParams FsMEFSizingParams [];
#endif /*  _MEFSZ_C  */


