/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmefdef.h,v 1.24 2015/07/11 06:54:14 siva Exp $
 *
 * Description:
 *********************************************************************/
#ifndef __FSMEF_DEF_H__
#define __FSMEF_DEF_H__

#define MEF_UNI_EVC_ID_MAX_LEN        (UNI_ID_MAX_LENGTH + EVC_ID_MAX_LENGTH)
#define MEF_TABLE_NAME_MAX_LEN        32
#define MEF_MAX_Q_DEPTH               10

#define MEF_MSG_IN_Q_EVENT            0x01

#define MEF_POLICY_ACTION_FLAG_LENGTH 1
#define MEF_CLI_OUT_ACT_DROP                 0x0001
/* TODO Move below macro to sizing */
#define MEF_SEM_NAME                  (UINT1 *)"MEFS"
#define MEF_TASK_NAME                 (UINT1 *)"MEF"
#define MEF_QUEUE_NAME                (UINT1 *)"MEFQ"

#define MEF_MAX_TRC_LEN               256
#define MEF_CLI_MAX_ARGS              18

#define MEF_CLI_MAX_MAC_STRING_SIZE   21 

#define MEF_SIZING_CONTEXT_COUNT   FsMEFSizingParams[MAX_MEF_MAX_CONTEXTS_SIZING_ID].u4PreAllocatedUnits

#define MEF_UNI_POOL_ID            MEFMemPoolIds[MAX_MEF_UNI_ENTRIES_SIZING_ID]
#define MEF_EVC_POOL_ID            MEFMemPoolIds[MAX_MEF_EVC_ENTRIES_SIZING_ID]
#define MEF_ISS_EVC_INFO_POOL_ID   MEFMemPoolIds[MAX_MEF_ISS_EVC_ENTRIES_SIZING_ID]
#define MEF_EVC_FILTER_POOL_ID     MEFMemPoolIds[MAX_MEF_EVC_FILTER_ENTRIES_SIZING_ID]
#define MEF_FILTER_POOL_ID         MEFMemPoolIds[MAX_MEF_FILTER_ENTRIES_SIZING_ID]
#define MEF_CLASS_MAP_POOL_ID      MEFMemPoolIds[MAX_MEF_CLS_MAP_TBL_MAX_ENTRIES_SIZING_ID]
#define MEF_CLASS_POOL_ID          MEFMemPoolIds[MAX_MEF_MAX_NUM_OF_CLASSES_SIZING_ID]
#define MEF_METER_POOL_ID          MEFMemPoolIds[MAX_MEF_METER_TBL_MAX_ENTRIES_SIZING_ID]
#define MEF_POLICY_MAP_POOL_ID     MEFMemPoolIds[MAX_MEF_PLY_MAP_TBL_MAX_ENTRIES_SIZING_ID]
#define MEF_CVLAN_EVC_POOL_ID      MEFMemPoolIds[MAX_MEF_CVLAN_EVC_MAP_ENTIES_SIZING_ID]
#define MEF_QUEUE_MSG_POOL_ID      MEFMemPoolIds[MAX_MEF_Q_MSGS_SIZING_ID]
#define MEF_CONTEXT_POOL_ID        MEFMemPoolIds[MAX_MEF_MAX_CONTEXTS_SIZING_ID]
#define MEF_UNI_LIST_POOL_ID       MEFMemPoolIds[MAX_MEF_UNI_LIST_ENTRIES_SIZING_ID]

#define MEF_UNI_TABLE              gMefGlobalInfo.MefUniTable
#define MEF_EVC_TABLE              gMefGlobalInfo.MefEvcTable
#define ISS_EVC_INFO_TABLE         gMefGlobalInfo.MefIssEvcInfoTable
#define MEF_EVC_FILTER_TABLE       gMefGlobalInfo.MefEvcFilterTable
#define MEF_FILTER_TABLE           gMefGlobalInfo.MefFilterTable
#define MEF_CLASS_MAP_TABLE        gMefGlobalInfo.MefClassMapTable
#define MEF_CLASS_TABLE            gMefGlobalInfo.MefClassTable
#define MEF_METER_TABLE            gMefGlobalInfo.MefMeterTable
#define MEF_POLICY_MAP_TABLE       gMefGlobalInfo.MefPolicyMapTable
#define MEF_CVLAN_EVC_TABLE        gMefGlobalInfo.MefCVlanEvcTable
#define MEF_UNI_LIST_TABLE         gMefGlobalInfo.MefUniListTable
#define MEF_CNP_PORTBASED_PORT     CFA_CNP_PORTBASED_PORT   
#define MEF_CUSTOMER_EDGE_PORT     CFA_CUSTOMER_EDGE_PORT
#define MEF_CUSTOMER_BRIDGE_PORT   CFA_CUSTOMER_BRIDGE_PORT
#define MEF_MAX_VLAN_CURR_ENTRIES  VLAN_MAX_CURR_ENTRIES
#define MEF_CUSTOMER_BRIDGE_MODE   VLAN_CUSTOMER_BRIDGE_MODE
#define MEF_PROVIDER_EDGE_BRIDGE_MODE VLAN_PROVIDER_EDGE_BRIDGE_MODE

#define MEF_ENABLED                1
#define MEF_DISABLED               2


#define MEF_EVC_ATTR_CREATE                  0  
#define MEF_EVC_ATTR_MODIFY                  1 
#define MEF_EVC_ATTR_DELETE                  2 

#define MEF_DEFAULT_VLAN                    1
#define MEF_MIN_EVC_PER_UNI                 1

#define MEF_CPY_TO_SNMP(pSnmpStruct, pu1Strg, u1Len) \
                   MEMCPY((pSnmpStruct)->pu1_OctetList, (pu1Strg), (u1Len)) ; \
                              (pSnmpStruct)->i4_Length = (u1Len);

#define MEF_CPY_FROM_SNMP(pu1Strg, pSnmpStruct, u1Len) \
             MEMCPY((pu1Strg), (pSnmpStruct)->pu1_OctetList, (u1Len))

#define MEF_TUNNEL_PROTOCOL_TUNNEL    VLAN_TUNNEL_PROTOCOL_TUNNEL
#define MEF_TUNNEL_PROTOCOL_PEER      VLAN_TUNNEL_PROTOCOL_PEER
#define MEF_TUNNEL_PROTOCOL_DISCARD   VLAN_TUNNEL_PROTOCOL_DISCARD


#define MEF_DEFAULT_LMM_INTERVAL         MEF_LMM_INTERVAL_100_Ms

#define MEF_MIN_CVLAN_PRIORITY   0
#define MEF_MAX_CVLAN_PRIORITY   7

#define MEF_METER_CIR_MIN_VAL                 0
#define MEF_METER_CIR_MAX_VAL                 10485760
#define MEF_METER_CBS_MIN_VAL                 0
#define MEF_METER_CBS_MAX_VAL                 10485760
#define MEF_METER_EIR_MIN_VAL                 0
#define MEF_METER_EIR_MAX_VAL                 10485760
#define MEF_METER_EBS_MIN_VAL                 0
#define MEF_METER_EBS_MAX_VAL                 10485760

#define MEF_MIN_DSCP_VALUE                    0
#define MEF_MAX_DSCP_VALUE                    63


/* Following macros are replication of  ECFM macros */
#define MEF_LM_MESSAGE_MIN               0
#define MEF_LM_MESSAGE_MAX               8192

#define MEF_LM_DEADLINE_MAX              172800

#define MEF_TX_STATUS_READY              0
#define MEF_TX_STATUS_NOT_READY          1

#define MEF_LMM_INTERVAL_100_Ms          1   /* LMM Interval Values */
#define MEF_LMM_INTERVAL_1_S             2   /* LMM Interval Values */
#define MEF_LMM_INTERVAL_10_S            3   /* LMM Interval Values */
#define MEF_LMM_INTERVAL_1_MIN           4   /* LMM Interval Values */
#define MEF_LMM_INTERVAL_10_MIN          5   /* LMM Interval Values */
#define MEF_MAX_LMM_INTERVALS            (MEF_LMM_INTERVAL_10_MIN + 1)

#define GET_LMM_INTERVAL(i4LmmInterval) \
            if(i4LmmInterval == 100) i4LmmInterval = MEF_LMM_INTERVAL_100_Ms;\
            else if(i4LmmInterval == 1000) i4LmmInterval = MEF_LMM_INTERVAL_1_S; \
            else if(i4LmmInterval == 10000) i4LmmInterval = MEF_LMM_INTERVAL_10_S; \
            else if(i4LmmInterval == 60000) i4LmmInterval = MEF_LMM_INTERVAL_1_MIN; \
            else if(i4LmmInterval == 600000) i4LmmInterval = MEF_LMM_INTERVAL_10_MIN; \
            else i4LmmInterval = MEF_MAX_LMM_INTERVALS;

#define GET_AVAILABILITY_INTERVAL(i4AvlbltyInterval) \
            if(i4AvlbltyInterval == 100) i4AvlbltyInterval = 1;\
            else if(i4AvlbltyInterval == 1000) i4AvlbltyInterval = 2; \
            else if(i4AvlbltyInterval == 10000) i4AvlbltyInterval = 3; \
            else if(i4AvlbltyInterval == 60000) i4AvlbltyInterval = 4; \
            else if(i4AvlbltyInterval == 600000) i4AvlbltyInterval = 5; \
            else if(i4AvlbltyInterval == 1800000) i4AvlbltyInterval = 6; \
            else if(i4AvlbltyInterval == 3600000) i4AvlbltyInterval = 7; \
            else  i4AvlbltyInterval = 8; 
            
#define MEF_AVLBLTY_DEADLINE_DEFAULT       3600 
#define MEF_AVLBLTY_INTERVAL_DEFAULT       100   
#define MEF_AVLBLTY_MAX_INTERVALS          8
#define MEF_AVLBLTY_WINDOW_DEFAULT         5
#define MEF_AVLBLTY_STATIC_METHOD          1
#define MEF_AVLBLTY_SLIDING_METHOD         2

#define MEF_VAL_10                      10
#define MEF_DM_INTERVAL_DEF_VAL          100
#define MEF_VLAN_MODE_NO_CHANG            2
#define MEF_VPLS_VLAN_MIN                 VLAN_VFI_MIN_ID
#define MEF_VPLS_VLAN_MAX                 VLAN_VFI_MAX_ID

#define MEF_DM_TYPE_1DM             1 
#define MEF_DM_TYPE_DMM             2

#define MEF_DEFAULT_EVC_MTU         1522

#define MEF_EVC_DOT1X_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1Dot1xTunnelStatus
#define MEF_EVC_LACP_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1LacpTunnelStatus
#define MEF_EVC_STP_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1StpTunnelStatus
#define MEF_EVC_GVRP_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1GvrpTunnelStatus
#define MEF_EVC_MVRP_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1MvrpTunnelStatus
#define MEF_EVC_GMRP_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1GmrpTunnelStatus
#define MEF_EVC_MMRP_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1MmrpTunnelStatus
#define MEF_EVC_IGMP_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1IgmpTunnelStatus
#define MEF_EVC_ELMI_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1ElmiTunnelStatus
#define MEF_EVC_LLDP_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1LldpTunnelStatus
#define MEF_EVC_ECFM_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1EcfmTunnelStatus
#define MEF_EVC_EOAM_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1EoamTunnelStatus
#define MEF_EVC_PTP_TUNNEL_STATUS(pMefEvcInfo) (pMefEvcInfo)->MefTunnelL2ProtoInfo.u1PtpTunnelStatus

#define MEF_EVC_DOT1X_BITMASK 0x01
#define MEF_EVC_LACP_BITMASK 0x02
#define MEF_EVC_STP_BITMASK 0x04
#define MEF_EVC_GVRP_BITMASK 0x08
#define MEF_EVC_GMRP_BITMASK 0x10
#define MEF_EVC_IGMP_BITMASK 0x20
#define MEF_EVC_MVRP_BITMASK 0x40
#define MEF_EVC_MMRP_BITMASK 0x80
#define MEF_EVC_ELMI_BITMASK 0x0100
#define MEF_EVC_LLDP_BITMASK 0x0200
#define MEF_EVC_ECFM_BITMASK 0x0400
#define MEF_EVC_EOAM_BITMASK 0x0800
#define MEF_EVC_PTP_BITMASK 0x1000

#define MEF_EVC_L2CP_PROTOCOL_GVRP 0
#define MEF_EVC_L2CP_PROTOCOL_GMRP 1
#define MEF_EVC_L2CP_PROTOCOL_IGMP 2
#define MEF_EVC_L2CP_PROTOCOL_MVRP 3
#define MEF_EVC_L2CP_PROTOCOL_MMRP 4
#define MEF_EVC_L2CP_PROTOCOL_ECFM 5

#define MEF_MAX_EVC_ONLY_L2CP_PROTOCOL 6

#define MEF_EVC_MAX_BITMASK (MEF_EVC_DOT1X_BITMASK + MEF_EVC_LACP_BITMASK + MEF_EVC_STP_BITMASK +\
                             MEF_EVC_GVRP_BITMASK + MEF_EVC_GMRP_BITMASK + MEF_EVC_IGMP_BITMASK +\
                             MEF_EVC_MVRP_BITMASK + MEF_EVC_MMRP_BITMASK + MEF_EVC_ELMI_BITMASK +\
                             MEF_EVC_LLDP_BITMASK + MEF_EVC_ECFM_BITMASK + MEF_EVC_EOAM_BITMASK +\
                             MEF_EVC_PTP_BITMASK)


#define MEF_ZERO                0

#define EVC_STAT_EVC_IN_FRAMES 6
#define EVC_STAT_EVC_IN_BYTES 7
#define EVC_STAT_EVC_OUT_FRAMES 8
#define EVC_STAT_EVC_OUT_BYTES 9
#define EVC_STAT_EVC_DISCARD_FRAMES 10
#define EVC_STAT_EVC_DISCARD_BYTES 11

#endif
