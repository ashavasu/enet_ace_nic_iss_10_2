/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmefpro.h,v 1.24 2016/03/30 11:23:57 siva Exp $
 *
 * Description:
 *********************************************************************/

INT4  MefApiLock (VOID);
INT4  MefApiUnLock (VOID);
INT4  MefCreateTables (VOID);

/**************************fsmefque.c*******************************/
VOID MefQueueMsgHandler (VOID);
INT4 MefPostMessageToQ (tMefMsg *pMsg);

/**************************fsmefcxt.c*******************************/
INT4 MefContextCreateAndInit (UINT4 u4ContextId);
INT4 MefContextDelete (UINT4 u4ContextId);

/**************************fsmeftrc.c*******************************/
VOID MefTrace (CONST CHR1 *c1FunName, UINT4 u4LineNum, UINT4 TraceType,
               CONST CHR1 *fmt, ...);

/**************************fsmefutl.c*******************************/
eMefTransportMode FsMefUtilGetTransportMode (UINT4 u4FsMefContextId);
VOID  MefInitDefaultUniInfo (INT4 i4IfIndex, tMefUniInfo * pMefUniInfo);
INT4  MefSetUniInfoEntry (tMefUniInfo *pMefUniInfo);
INT4  MefResetUniInfoEntry (INT4 i4IfIndex);
INT4  MefUniCreateTable(VOID);
VOID  MefUniDeleteTable(VOID);
INT4  MefAddUniEntry (tMefUniInfo *pMefUniInfo);
INT4  MefDeleteUniEntry (INT4 i4IfIndex);
tMefUniInfo * MefGetUniEntry (INT4 i4IfIndex);
INT4  MefGetContexId (INT4 i4IfIndex, UINT4 *u4FsMefContextId,
                      UINT2 *u2FsMefLocalPort);
INT4  MefEvcCreateTable(VOID);
tMefEvcInfo * MefGetEvcEntry (INT4 i4FsEvcContextId, INT4 i4FsEvcVlanId);
VOID  MefInitDefaultEvcInfo (INT4 i4FsEvcContextId, INT4 i4FsEvcVlanId,
                             tMefEvcInfo * pMefEvcInfo);
INT4  MefAddEvcEntry (tMefEvcInfo * pMefEvcInfo);
INT4  MefDeleteEvcEntry (tMefEvcInfo * pMefEvcInfo);
INT4  MefDeleteAllEvcEntries (UINT4 u4ContextId);
INT4  MefEvcInfoCreateTable(VOID);
tEvcInfo * MefGetEvcInfoEntry (INT4 i4SVlan);
VOID  MefEvcInitDefaultEvcInfo (UINT4 u4EvcId, INT4 i4FsEvcVlanId, tEvcInfo * pIssEvcInfo);
INT4  MefAddEvcInfoEntry (tEvcInfo * pIssEvcInfo);
INT4  MefDeleteEvcInfoEntry (tEvcInfo * pIssEvcInfo);

tMefEvcFilterInfo * MefGetEvcFilterEntry (INT4 i4FsEvcContextId,
                                          INT4 i4FsEvcVlanId,
                                           INT4 i4FsEvcInstance);
INT4  MefSetEvcEntries (UINT4 u4FsEvcContextId, INT4 i4FsEvcVlanId, 
                        INT4 i4FsEvcFilterInstance);
INT4  MefEvcFilterCreateTable(VOID);
INT4  MefFilterCreateTable(VOID);
INT4  MefClassMapCreateTable(VOID);
INT4  MefClassCreateTable(VOID);
INT4  MefMeterCreateTable(VOID);
INT4  MefPolicyMapCreateTable(VOID);
INT4  MefCVlanEvcTableTable (VOID);
tMefFilterInfo * MefGetFilterEntry (INT4 i4FsMefFilterNo);
tMefClassMapInfo * MefGetClassMapEntry (UINT4 u4FsMefClassMapId);
tMefClassInfo * MefGetClassEntry (UINT4 u4FsMefClassId);
tMefMeterInfo * MefGetMeterEntry (UINT4 u4FsMeterId);
tMefPolicyMapInfo * MefGetPolicyMapEntry (UINT4 u4FsPolicyMapId);
tMefCVlanEvcInfo * 
MefGetCVlanEvcEntry (INT4 i4IfIndex, INT4 i4FsUniCVlanEvcCVlanId);
VOID  MefInitDefaultFilterInfo (INT4 i4FsMefFilterNo,
                                tMefFilterInfo * pMefFilterInfo);
INT4  MefAddFilterEntry (tMefFilterInfo * pMefFilterInfo);
INT4  MefSetFilterEntries (INT4 i4FsMefFilterNo);
INT4  MefDeleteFilterEntry (tMefFilterInfo * pMefFilterInfo);
VOID  MefInitDefaultClassMapInfo (UINT4 u4FsMefClassMapId,
                                  tMefClassMapInfo * pMefClassMapInfo);
INT4  MefAddClassMapEntry (tMefClassMapInfo * pMefClassMapInfo);
INT4  MefSetClassMapEntries (INT4 u4FsMefClassMapId);
INT4  MefDeleteClassMapEntry (tMefClassMapInfo * pMefClassMapInfo);
VOID  MefInitDefaultClassInfo (UINT4 u4FsMefClassCLASS,
                               tMefClassInfo * pMefClassInfo);
INT4  MefAddClassEntry (tMefClassInfo * pMefClassInfo);
INT4  MefSetClassEntries (UINT4 u4FsMefClassCLASS);
INT4  MefDeleteClassEntries (tMefClassInfo * pMefClassInfo);
INT4  MefDeleteClassEntry (tMefClassInfo * pMefClassInfo);
VOID  MefInitDefaultMeterInfo (UINT4 u4FsMefMeterId,
                               tMefMeterInfo * pMefMeterInfo);
INT4  MefAddMeterEntry (tMefMeterInfo * pMefMeterInfo);
INT4  MefSetMeterEntries (UINT4 u4FsMefMeterId);
INT4  MefDeleteMeterEntry (tMefMeterInfo * pMefMeterInfo);
VOID  MefInitDefaultPolicyMapInfo (UINT4 u4FsMefMeterId,
                                   tMefPolicyMapInfo * pMefPolicyMapInfo);
INT4  MefAddPolicyMapEntry (tMefPolicyMapInfo * pMefPolicyMapInfo);
INT4  MefSetPolicyMapEntries (UINT4 u4FsMefPolicyMapId);
INT4  MefDeletePolicyMapEntry (tMefPolicyMapInfo * pMefPolicyMapInfo);
INT4  MefAddCVlanEvcEntry (tMefCVlanEvcInfo * pMefCVlanEvcInfo);
INT4  MefDeleteCVlanEvcEntry (tMefCVlanEvcInfo * pMefCVlanEvcInfo);
VOID  MefUpdateUniL2cpInfo (tMefUniInfo * pMefUniInfo);
INT4  MefActOnServiceMultiplexingChange (tMefUniInfo * pMefUniInfo);
INT4  MefSetMPLSCVlanEvcEntry (tMefCVlanEvcInfo *pCVlanEvcInfo);
INT4  MefDeleteMPLSCVlanEvcEntry (tMefCVlanEvcInfo *pCVlanEvcInfo);
INT4  MefIsMsrInProgress (VOID);
INT4  MefSetUniCVlanEvcEntries (tMefCVlanEvcInfo * pCVlanEvcInfo);
INT4 MefValidateEvcIndexForTransMode (UINT4 u4ContextId,INT4 i4EvcIndex);
UINT4  MefGetFreeEvcId (INT4 i4FsEvcIndex);
INT4 MefSetServiceType (UINT4 u4FsEvcContextId, INT4 i4FsEvcVlanId,
                        INT4 i4ServiceType);
#ifdef Y1564_WANTED
INT4 MefUtilCheckEvcEntryPresence (UINT4 u4ContextId, UINT4 u4EvcId);
INT4 MefUtilGetBanwidthProfile (UINT4 u4ContextId, UINT4 u4EvcId,
                                UINT4 *pu4MefMeterCIR, UINT4 *pu4MefMeterCBS,
                                UINT4 *pu4MefMeterEIR, UINT4 *pu4MefMeterEBS,
                                UINT1 *pu1MefMeterColorMode,
                                UINT1 *pu1MefMeterType);
#endif
INT4
MefEvcTunnelInfoToVlan (tMefEvcInfo *pMefEvcInfo, INT4 i4FsEvcContextId, INT4 i4FsEvcIndex);
INT4 VlanHwSetEvcAttribute (INT4 i4FsEvcContextId, UINT4 u4Action, tEvcInfo * pIssEvcInfo);
INT4 VlanHwGetVlanStats (INT4 u4ContextId, UINT4 i4VlanId, UINT1 i1StatType,
                    UINT4 *pu4Value);
INT4 VlanSelectContext (UINT4 u4ContextId);
INT4 VlanReleaseContext (VOID);

INT4
MefDeleteCvlanEvcMapEntries (INT4 i4IfIndex);

INT4       MefEvcEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4       MefEvcFilterEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4       MefUniEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4       MefFilterEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4       MefClassMapEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4       MefClassEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4       MefMeterEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4       MefPolicyMapEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4       MefCVlanEvcEnrtyRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4       MefIssEvcInfoEntryRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT1       MefCheckAndVerifyUntaggedCepPep(UINT4 u4FsMefContextId, INT4 i4IfIndex, UINT2 u2CVlanId);

extern INT4 VlanHwAddSVlanMap PROTO ((UINT4 u4ContextId, tVlanSVlanMap VlanSVlanMap));



/**************************fsmefcli.c*******************************/
INT4        MefCliSetTranportMode (tCliHandle CliHandle,
                                           UINT4 u4ContextId, INT4 i4TransMode);
INT4        MefCliSetUniId (tCliHandle CliHandle, INT4 i4IfIndex,
                                    UINT1 *);
INT4        MefCliSetUniType (tCliHandle CliHandle, INT4 i4IfIndex,
                                      UINT1 u1UniType);
INT4        MefCliSetUniDefaultCeVlan (tCliHandle CliHandle,
                                               INT4 i4IfIndex,
                                               UINT1 *pu1DefCeVlan);
INT4        MefCliSetUniL2cpDisposition (tCliHandle CliHandle,
                                                 INT4 i4IfIndex, INT4, INT4);
INT4
            MefCliSetUniL2cpOverride (tCliHandle CliHandle, 
                                                INT4 i4IfIndex, UINT4 u4OverrideOption);

INT4        MefCliSetEvcL2cpStatus (tCliHandle CliHandle, 
                                         UINT4 u4ContextId, INT4 i4EvcId,
                                         UINT4, UINT4);

INT4        MefCliSetEvcId (tCliHandle CliHandle, UINT4 u4ContextId,
                                    INT4 i4Evc, UINT1 *);
INT4        MefCliSetEvcType (tCliHandle CliHandle, UINT4 u4ContextId,
                                      INT4 i4Evc, UINT4 i4EvcType);
INT4        MefCliSetEvcVlanIdPreserve (tCliHandle CliHandle,
                                                UINT4 u4ContextId, INT4 i4Evc,
                                                INT4 i4IdPreserve);
INT4        MefCliSetEvcVlanCosPreserve (tCliHandle CliHandle,
                                                 UINT4 u4ContextId, INT4 i4Evc,
                                                 INT4 i4CosPreserve);
INT4        MefCliSetEvcLoopbackStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                             INT4 i4Evc, INT4 i4LoopBackStatus);
INT4        MefCliMapCeVlanEvcInfo (tCliHandle CliHandle,
                                            INT4 i4IfIndex, UINT4 *pi4CeVlan,
                                            UINT4 *pi4Evc, UINT1 *pau1UniEvcId);
INT4        MefCliConfFilterInfo (tCliHandle CliHandle, UINT4 *, INT4,
                                          UINT4 *, INT4 *, INT4 *, UINT4 *,
                                          UINT4 *);
INT4        MefCliConfClassMapInfo (tCliHandle CliHandle, UINT4 *,
                                            UINT4 *, UINT4 *);
INT4        MefCliConfClassInfo (tCliHandle CliHandle, UINT4 *, UINT4);
INT4        MefCliConfMeterInfo (tCliHandle CliHandle, UINT4 *, UINT4 *,
                                         UINT4 *, UINT4 *, UINT4 *, UINT4 *,
                                         UINT4 *);
INT4        MefCliConfPolicyMapInfo (tCliHandle CliHandle, UINT4 *,
                                             UINT4 *, UINT4 *);
INT4        MefCliShowUniInfo (tCliHandle CliHandle, UINT4);
INT4        MefCliShowEvcInfo (tCliHandle CliHandle, UINT4);
INT4        MefCliShowEvcFilterInfo (tCliHandle CliHandle,
                                             UINT4 u4CxtId, INT4 i4Evc);
INT4        MefCliShowCeVlanEvcMapInfo (tCliHandle CliHandle, UINT4);
INT4        MefCliGetContextInfoForShowCmd (tCliHandle, UINT1 *, UINT4,
                                                    UINT4 *);
INT4        MefCliConfETreeInfo (tCliHandle, UINT4 *, UINT4 *, UINT4 *,
                                         UINT4 *, UINT4 *, UINT4 *, UINT4 *);
INT4        MefCliUtilConvertStrToPortArray (tCliHandle CliHandle, UINT1 *pu1Str,
                                  UINT4 *pu4PortArray, UINT4 *pu4Count,
                                  UINT4 u4PortArrayLen, UINT4 u4IfType);
INT4       MefCliRemoveETreeInfo (tCliHandle CliHandle, UINT4 *, UINT4 *, UINT4 *);
INT4       MefCliClearFrameDelayBuffer (tCliHandle CliHandle, UINT4 u4ContextId);
INT4       MefCliClearTunnelCounter (tCliHandle CliHandle, UINT4 u4ContextId, tVlanId VlanId);
INT4       MefCliClearFrameLossBuffer (tCliHandle CliHandle, UINT4 u4ContextId);
INT4       MefCliConfigFrameLoss (tCliHandle CliHandle, tMefFrameLoss *);
INT4       MefCliConfigFrameDelay (tCliHandle CliHandle, tMefFrameDelay *);
INT4       MefCliConfigAvailability (tCliHandle CliHandle, tMefAvailable *);
INT4       MefCliShowFrameLoss (tCliHandle CliHandle, UINT4, UINT4 *, UINT4 *, UINT4 *);
INT4       MefCliShowFrameDelay (tCliHandle CliHandle, UINT4, UINT4 *, UINT4 *, UINT4 *);
INT4       MefCliShowAvailability (tCliHandle CliHandle, UINT4, UINT4 *, UINT4 *, UINT4 *);
INT4       MefCliShowMefEtree (tCliHandle CliHandle, UINT4 *pu4PortType, UINT4 *pu4Port);
INT4       MefCliShowEtreeInfo (tCliHandle CliHandle, INT4 i4IngressPort);
INT4       MefCliEvcFilterConfig (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Evc,
                        UINT4 *pu4Instance, UINT4 *pu4FilterId,
                        UINT1 *pu1MacAddr, UINT4 *pu4Allow, UINT4 *pu4Drop);
INT4        MefCliShowFilterInfo (tCliHandle CliHandle, UINT4 *);
INT4        MefCliUtilPrintFilterInfo (tCliHandle CliHandle, INT4);
INT4        MefCliShowClassMapInfo (tCliHandle CliHandle, UINT4 *);
INT4        MefCliUtilPrintClassMapInfo (tCliHandle CliHandle, UINT4);
INT4        MefCliShowClassInfo (tCliHandle CliHandle, UINT4 *);
INT4        MefCliUtilPrintClassInfo (tCliHandle CliHandle, UINT4);
INT4        MefCliShowMeterInfo (tCliHandle CliHandle, UINT4 *);
INT4        MefCliUtilPrintMeterInfo (tCliHandle CliHandle, UINT4);
INT4        MefCliShowPolicyMapInfo (tCliHandle CliHandle, UINT4 *);
INT4        MefCliUtilPrintPolicyMapInfo (tCliHandle, UINT4);
INT4        MefCliShowMefInfo (tCliHandle CliHandle, UINT4);
INT4        MefCliShowEvcStats(tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4EvcVlanId);
INT4        MefCliDisplayEvcStats(tCliHandle CliHandle, UINT4 u4EvcVlanId, 
                    INT4 i4EvcContextId);

/**************************fsmefsrc.c*******************************/
INT4        MefShowRunningScalarConfig (tCliHandle CliHandle);
INT4        MefShowRunningUniConfig (tCliHandle CliHandle);
INT4        MefShowRunningEvcConfig (tCliHandle CliHandle);
INT4        MefShowRunningFilterConfig (tCliHandle CliHandle,INT4 i4CurrIfIndex);
INT4        MefShowRunningClassConfig (tCliHandle CliHandle);
INT4        MefShowRunningClassMapConfig (tCliHandle CliHandle);
INT4        MefShowRunningMeterConfig (tCliHandle CliHandle);
INT4        MefShowRunningPolicyMapConfig (tCliHandle CliHandle);
INT4        MefShowRunningCeVlanEvcMapConfig (tCliHandle CliHandle,INT4 i4CurrIfIndex);
INT4        MefShowRunningEvcFilterConfig (tCliHandle CliHandle,
                                                   INT4 u4CxtId, INT4 i4Evc);
INT4        MefShowRunningETreeConfig (tCliHandle CliHandle);
INT4        MefShowRunningUniListConfig (tCliHandle CliHandle,INT4 i4CurrEvcId);
/************************* UNI List Table *********************************/
INT4 MefUniListCreateTable (VOID);
INT4 MefUniListEntryRBTreeCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
tMefUniList * MefGetUniListEntry (UINT4 u4ContextId, INT4 i4EvcIndex, INT4 i4IfIndex);
INT4 MefAddUniListEntry (tMefUniList *pMefUniListInfo);
INT4 MefDeleteUniListEntry (tMefUniList *pMefUniListInfo);
VOID MefUniListDeleteTable (VOID);

INT4 MefCliUniListConfig (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Evc,
                          UINT4 u4IfIndex, INT4 i4UniListType);
INT4 MefCliShowUniList (tCliHandle CliHandle, UINT4 u4ContextId, 
                        UINT4 u4EvcIndex, UINT4 u4IfIndex);
INT4 MefCliDisplayUniList (tCliHandle CliHandle, INT4 i4Evc, UINT1 *au1Uni, 
                           UINT1 *au1UniId, INT4 i4UniType);
extern tSNMP_OCTET_STRING_TYPE * allocmem_octetstring PROTO((INT4));
extern VOID free_octetstring PROTO((tSNMP_OCTET_STRING_TYPE *));                                                                
