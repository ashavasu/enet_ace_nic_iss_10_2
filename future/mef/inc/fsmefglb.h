/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmefglb.h,v 1.2 2014/10/10 12:05:50 siva Exp $
 *
 * Description:
 *********************************************************************/
#ifndef __FSMEF_GLOB_H__
#define __FSMEF_GLOB_H__

#ifdef __FSMEF_INI_C__

tMefGlobalInfo  gMefGlobalInfo;
UINT4     gau4EvcIndex[VLAN_MAX_VLAN_ID + 1];

#else

PUBLIC tMefGlobalInfo  gMefGlobalInfo;
PUBLIC UINT4 gau4EvcIndex[VLAN_MAX_VLAN_ID + 1];

#endif
#endif
