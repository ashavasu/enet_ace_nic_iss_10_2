/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmeftrc.h,v 1.1 2012/04/30 10:49:52 siva Exp $
 *
 * Description:
 *********************************************************************/
#ifndef __FSMEFTRC_H__
#define __FSMEFTRC_H__

#define MEF_TRC(TraceType, Str) \
                MefTrace(__FUNCTION__, __LINE__, TraceType, Str);

#define MEF_TRC_ARG1(TraceType, Str, Arg1) \
                MefTrace(__FUNCTION__, __LINE__, TraceType, Str, Arg1);

#define MEF_TRC_ARG2(TraceType, Str, Arg1, Arg2) \
                MefTrace(__FUNCTION__, __LINE__, TraceType, Str, Arg1, Arg2);

#endif
