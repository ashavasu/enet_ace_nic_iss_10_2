/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmeftdf.h,v 1.15 2014/10/10 12:05:50 siva Exp $
 *
 * Description:
 *********************************************************************/
#ifndef __FSMEF_TDF_H__
#define __FSMEF_TDF_H__

typedef enum
{
    MEF_POINT_TO_POINT = 1,
    MEF_MULTIPOINT_TO_MULTIPOINT,
    MEF_ROOTED_MULTIPOINT
}eMefEvcType;

typedef enum
{
    MEF_TRANS_MODE_PB = 1,
    MEF_TRANS_MODE_MPLS
}eMefTransportMode;

typedef enum
{
    MEF_UNI_ROOT = 1,
    MEF_UNI_LEAF
}eMefUniType;
typedef struct _tMefTunnelL2ProtoInfo {
    UINT1               u1Dot1xTunnelStatus;
    UINT1               u1LacpTunnelStatus;
    UINT1               u1StpTunnelStatus;
    UINT1               u1GvrpTunnelStatus;
    UINT1               u1MvrpTunnelStatus;
    UINT1               u1GmrpTunnelStatus;
    UINT1               u1MmrpTunnelStatus;
    UINT1               u1IgmpTunnelStatus;
    UINT1               u1ElmiTunnelStatus;
    UINT1               u1LldpTunnelStatus;
    UINT1               u1EcfmTunnelStatus;
    UINT1               u1EoamTunnelStatus;
    UINT1               u1PtpTunnelStatus;
    UINT1               au1Pad[3];
}tMefTunnelL2ProtoInfo;
typedef struct MefUniInfo
{
    tRBNodeEmbd           MefUniRbNode;
    UINT1                 au1UniId [UNI_ID_MAX_LENGTH];
    UINT4                 u4UniOverride;
    INT4                  i4IfIndex;
    INT4                  i4RowStatus;
    UINT2                 u2DefaultCeVlanId;
    BOOLEAN               bMultiplexing;
    BOOLEAN               bBundling;
    BOOLEAN               bAllToOneBundling;
    UINT1                 u1Dot1xTunnelingStatus;
    UINT1                 u1LacpTunnelingStatus;
    UINT1                 u1StpTunnelingStatus;
    UINT1                 u1GvrpTunnelingStatus;
    UINT1                 u1MvrpTunnelingStatus;
    UINT1                 u1GmrpTunnelingStatus;
    UINT1                 u1MmrpTunnelingStatus;
    UINT1                 u1ElmiTunnelingStatus;
    UINT1                 u1LldpTunnelingStatus;
    UINT1                 u1EcfmTunnelingStatus;
    UINT1                 u1EoamTunnelingStatus;
}tMefUniInfo;

typedef struct MefEvcInfo{
    tRBNodeEmbd           MefEvcRbNode;
    UINT4                 u4EvcContextId;
    INT4                  i4EvcVlanId;
    UINT1                 au1EvcId [EVC_ID_MAX_LENGTH];
    eMefEvcType           EvcType;
    INT4                  i4EvcMaxUni;
    INT4                  i4RowStatus;
    INT4                  i4EvcMtu;
    BOOLEAN               bCeVlanIdPreservation;
    BOOLEAN               bCeVlanCoSPreservation;
    tMefTunnelL2ProtoInfo MefTunnelL2ProtoInfo;
    UINT1                 u1LoopbackStatus;
    UINT1                 au1Pad[1];
} tMefEvcInfo;

typedef struct MefEvcFilerInfo
{
    tRBNodeEmbd           MefEvcFilterRbNode;
    UINT4                 u4EvcFilterContextId;
    INT4                  i4EvcFilterVlanId;
    INT4                  i4EvcFilterInstance;
    INT4                  i4EvcFilterAction;
    INT4                  i4EvcFilterId;
    INT4                  i4RowStatus;
    tMacAddr              EvcFilterMacAddr;
    UINT1                 au1Pad[2];
} tMefEvcFilterInfo;

typedef struct MefFilterInfo
{
    tRBNodeEmbd           MefFilterRbNode;
    UINT4                 u4FilterNo;
    UINT4                 u4IfIndex;
    UINT2                 u2Evc;
    INT1                  i1CVlanPriority;
    INT1                  i1Dscp;
    UINT1                 u1Direction;
    UINT1                 u1Status;
    UINT1                 au1Pad[2];
} tMefFilterInfo;

typedef struct MefClassMapInfo
{
    tRBNodeEmbd           MefClassMapRbNode;
    UINT1                 au1ClassMapName[MEF_TABLE_NAME_MAX_LEN];
    UINT4                 u4ClassMapClfrId;
    UINT4                 u4FilterId;
    UINT4                 u4ClassId;
    UINT1                 u1Status;
    UINT1                 au1Pad[3];

}tMefClassMapInfo;

typedef struct MefClassInfo
{
    tRBNodeEmbd           MefClassRbNode;
    UINT4                 u4ClassId;
    UINT4                 u4RefCount;  /* No. of usage of this Entry */
    UINT1                 u1Status;
    UINT1                 au1Pad[3];

}tMefClassInfo;

typedef struct MefMeterInfo
{
    tRBNodeEmbd           MefMeterRbNode;
    UINT1                 au1MeterName[MEF_TABLE_NAME_MAX_LEN];
    UINT4                 u4MeterId;
    UINT4                 u4RefCount;  /* No. of usage of this Entry */
    UINT4                 u4CIR;
    UINT4                 u4CBS;
    UINT4                 u4EIR;
    UINT4                 u4EBS;
    UINT1                 u1MeterType;
    UINT1                 u1ColorMode;
    UINT1                 u1Status;
    UINT1                 au1Pad[1];
}tMefMeterInfo;

typedef struct MefPolicyMapInfo
{
    tRBNodeEmbd           MefPolicyMapRbNode;
    UINT1                 au1PolicyMapName[MEF_TABLE_NAME_MAX_LEN];
    UINT4                 u4PolicyMapId;
    UINT4                 u4MeterId;
    UINT4                 u4ClassId;
    UINT1                 u1Status;
    UINT1                 u1OutProActionFlag;
    UINT1                 au1Pad[2];
}tMefPolicyMapInfo;

typedef struct MefCVlanEvcInfo
{
  tRBNodeEmbd           MefCVlanEvcTableRbNode;
  UINT4                 u4IfIndex;
  INT4                  i4CVlanId;
  INT4                  i4EvcIndex;
  UINT1                 u1RowStatus;
  UINT1                 au1UniEvcId [UNI_ID_MAX_LENGTH];
  UINT1                 au1Pad[3];
} tMefCVlanEvcInfo;


typedef struct MefContextInfo
{
    eMefTransportMode        eMefTransportMode;
}tMefContextInfo;

typedef struct MefGlobalInfo
{
   tOsixSemId                MefSemId;
   tOsixQId                  MefQueueId;
   tMemPoolId                MefUniMemPoolId;
   tRBTree                   MefUniTable;
   tRBTree                   MefEvcTable;
   tRBTree                   MefIssEvcInfoTable;
   tRBTree                   MefEvcFilterTable;
   tRBTree                   MefFilterTable;
   tRBTree                   MefClassMapTable;
   tRBTree                   MefClassTable;
   tRBTree                   MefMeterTable;
   tRBTree                   MefPolicyMapTable;
   tRBTree                   MefCVlanEvcTable;
   tRBTree                   MefUniListTable;
   tMefContextInfo          *apContextInfo [SYS_DEF_MAX_NUM_CONTEXTS];
   BOOLEAN                   bIsMefInitialised;
   UINT1                     au1Pad[3];
}tMefGlobalInfo;

typedef struct MefMsg
{
   UINT4    u4MsgType;
   UINT4    u4ContextId;
   UINT4    u4IfIndex;
   UINT2    u2PortNum;
   UINT1    au1Pad[2];
}tMefMsg;

typedef struct MefFrameLoss
{
   UINT4    u4ContextId;
   UINT4    u4LmState;
   UINT4    u4MdIndex;
   UINT4    u4MaIndex;
   UINT4    u4MepIndex;
   UINT4    u4RmepId;
   INT4     i4LmInterval;
   INT4     i4LmCount;
   INT4     i4LmDeadLine;
   tMacAddr RmepMacAddr;
   BOOLEAN  bIsMepId;
   UINT1    u1Pad;
}tMefFrameLoss;

typedef struct MefFrameDelay
{
   UINT4    u4ContextId;
   UINT4    u4DmState;
   UINT4    u4MdIndex;
   UINT4    u4MaIndex;
   UINT4    u4MepIndex;
   UINT4    u4RmepId;
   INT4     i4DmType;
   INT4     i4DmInterval;
   INT4     i4DmCount;
   INT4     i4DmDeadLine;
   tMacAddr RmepMacAddr;
   BOOLEAN  bIsMepId;
   UINT1    u1Pad;
}tMefFrameDelay;

typedef struct MefAvailable
{
   UINT4    u4ContextId;
   UINT4    u4AvailabileState;
   UINT4    u4MdIndex;
   UINT4    u4MaIndex;
   UINT4    u4MepIndex;
   UINT4    u4RmepId;
   INT4     i4AvailInterval;
   UINT4    u4AvailWindowSize;
   INT4     i4AvailDeadLine;
   INT4     i4AvailAlgoType;
   tSNMP_OCTET_STRING_TYPE  LowThreshold;
   tSNMP_OCTET_STRING_TYPE  UpperThreshold;
   UINT4    u4DownTimeStart;
   UINT4    u4DownTimeEnd;
   tMacAddr RmepMacAddr;
   BOOLEAN  bIsModestAreaAvail;
   BOOLEAN  bIsMepId;
}tMefAvailable;

typedef struct MefUniList
{
    tRBNodeEmbd      MefUniListRbNode;
    UINT4            u4ContextId;
    INT4             i4EvcIndex;
    INT4             i4IfIndex;
    eMefUniType      UniType;
    INT4             i4RowStatus;
    UINT1            au1MefUniId [UNI_ID_MAX_LENGTH];
}tMefUniList;
#endif
