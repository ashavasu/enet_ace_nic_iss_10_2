/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmefdb.h,v 1.11 2016/06/18 11:45:01 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMEFDB_H
#define _FSMEFDB_H

UINT1 FsMefContextTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsUniTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsEvcTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsEvcFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMefETreeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsUniCVlanEvcTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMefFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMefClassMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMefClassTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMefMeterTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMefPolicyMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMefMepTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMefFdTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMefFdStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMefFlTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMefFlStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMefMepAvailabilityTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMefUniListTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsEvcStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsmef [] ={1,3,6,1,4,1,29601,2,71};
tSNMP_OID_TYPE fsmefOID = {9, fsmef};


UINT4 FsMefContextId [ ] ={1,3,6,1,4,1,29601,2,71,1,1,1,1};
UINT4 FsMefContextName [ ] ={1,3,6,1,4,1,29601,2,71,1,1,1,2};
UINT4 FsMefTransmode [ ] ={1,3,6,1,4,1,29601,2,71,1,1,1,3};
UINT4 FsMefFrameLossBufferClear [ ] ={1,3,6,1,4,1,29601,2,71,1,1,1,4};
UINT4 FsMefFrameDelayBufferClear [ ] ={1,3,6,1,4,1,29601,2,71,1,1,1,5};
UINT4 FsMefFrameLossBufferSize [ ] ={1,3,6,1,4,1,29601,2,71,1,1,1,6};
UINT4 FsMefFrameDelayBufferSize [ ] ={1,3,6,1,4,1,29601,2,71,1,1,1,7};
UINT4 FsUniId [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,1};
UINT4 FsUniPhysicalMedium [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,2};
UINT4 FsUniSpeed [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,3};
UINT4 FsUniMode [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,4};
UINT4 FsUniMacLayer [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,5};
UINT4 FsUniMtu [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,6};
UINT4 FsUniServiceMultiplexingBundling [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,7};
UINT4 FsUniCVlanId [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,8};
UINT4 FsUniMaxEvcs [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,9};
UINT4 FsUniL2CPDot1x [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,10};
UINT4 FsUniL2CPLacp [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,11};
UINT4 FsUniL2CPStp [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,12};
UINT4 FsUniL2CPGvrp [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,13};
UINT4 FsUniL2CPGmrp [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,14};
UINT4 FsUniL2CPMvrp [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,15};
UINT4 FsUniL2CPMmrp [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,16};
UINT4 FsUniL2CPLldp [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,17};
UINT4 FsUniL2CPElmi [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,18};
UINT4 FsUniRowStatus [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,19};
UINT4 FsUniL2CPEcfm [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,20};
UINT4 FsUniL2CPOverrideOption [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,21};
UINT4 FsUniL2CPEoam [ ] ={1,3,6,1,4,1,29601,2,71,2,1,1,22};
UINT4 FsEvcContextId [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,1};
UINT4 FsEvcIndex [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,2};
UINT4 FsEvcId [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,3};
UINT4 FsEvcType [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,4};
UINT4 FsEvcMaxUni [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,5};
UINT4 FsEvcCVlanIdPreservation [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,6};
UINT4 FsEvcCVlanCoSPreservation [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,7};
UINT4 FsEvcRowStatus [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,8};
UINT4 FsEvcMtu [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,9};
UINT4 FsEvcL2CPTunnel [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,10};
UINT4 FsEvcL2CPPeer [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,11};
UINT4 FsEvcL2CPDiscard [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,12};
UINT4 FsEvcLoopbackStatus [ ] ={1,3,6,1,4,1,29601,2,71,3,1,1,13};
UINT4 FsEvcFilterInstance [ ] ={1,3,6,1,4,1,29601,2,71,3,2,1,1};
UINT4 FsEvcFilterDestMacAddress [ ] ={1,3,6,1,4,1,29601,2,71,3,2,1,2};
UINT4 FsEvcFilterAction [ ] ={1,3,6,1,4,1,29601,2,71,3,2,1,3};
UINT4 FsEvcFilterId [ ] ={1,3,6,1,4,1,29601,2,71,3,2,1,4};
UINT4 FsEvcFilterRowStatus [ ] ={1,3,6,1,4,1,29601,2,71,3,2,1,5};
UINT4 FsMefETreeIngresPort [ ] ={1,3,6,1,4,1,29601,2,71,4,1,1,1};
UINT4 FsMefETreeEvcIndex [ ] ={1,3,6,1,4,1,29601,2,71,4,1,1,2};
UINT4 FsMefETreeEgressPort [ ] ={1,3,6,1,4,1,29601,2,71,4,1,1,3};
UINT4 FsMefETreeRowStatus [ ] ={1,3,6,1,4,1,29601,2,71,4,1,1,4};
UINT4 FsUniCVlanEvcCVlanId [ ] ={1,3,6,1,4,1,29601,2,71,5,1,1,1};
UINT4 FsUniCVlanEvcEvcIndex [ ] ={1,3,6,1,4,1,29601,2,71,5,1,1,2};
UINT4 FsUniCVlanEvcRowStatus [ ] ={1,3,6,1,4,1,29601,2,71,5,1,1,3};
UINT4 FsUniEvcId [ ] ={1,3,6,1,4,1,29601,2,71,5,1,1,4};
UINT4 FsMefFilterNo [ ] ={1,3,6,1,4,1,29601,2,71,6,1,1,1};
UINT4 FsMefFilterDscp [ ] ={1,3,6,1,4,1,29601,2,71,6,1,1,2};
UINT4 FsMefFilterDirection [ ] ={1,3,6,1,4,1,29601,2,71,6,1,1,3};
UINT4 FsMefFilterEvc [ ] ={1,3,6,1,4,1,29601,2,71,6,1,1,4};
UINT4 FsMefFilterCVlanPriority [ ] ={1,3,6,1,4,1,29601,2,71,6,1,1,5};
UINT4 FsMefFilterIfIndex [ ] ={1,3,6,1,4,1,29601,2,71,6,1,1,6};
UINT4 FsMefFilterStatus [ ] ={1,3,6,1,4,1,29601,2,71,6,1,1,7};
UINT4 FsMefClassMapId [ ] ={1,3,6,1,4,1,29601,2,71,6,2,1,1};
UINT4 FsMefClassMapName [ ] ={1,3,6,1,4,1,29601,2,71,6,2,1,2};
UINT4 FsMefClassMapFilterId [ ] ={1,3,6,1,4,1,29601,2,71,6,2,1,3};
UINT4 FsMefClassMapCLASS [ ] ={1,3,6,1,4,1,29601,2,71,6,2,1,4};
UINT4 FsMefClassMapStatus [ ] ={1,3,6,1,4,1,29601,2,71,6,2,1,5};
UINT4 FsMefClassCLASS [ ] ={1,3,6,1,4,1,29601,2,71,6,3,1,1};
UINT4 FsMefClassStatus [ ] ={1,3,6,1,4,1,29601,2,71,6,3,1,2};
UINT4 FsMefMeterId [ ] ={1,3,6,1,4,1,29601,2,71,6,4,1,1};
UINT4 FsMefMeterName [ ] ={1,3,6,1,4,1,29601,2,71,6,4,1,2};
UINT4 FsMefMeterType [ ] ={1,3,6,1,4,1,29601,2,71,6,4,1,3};
UINT4 FsMefMeterColorMode [ ] ={1,3,6,1,4,1,29601,2,71,6,4,1,4};
UINT4 FsMefMeterCIR [ ] ={1,3,6,1,4,1,29601,2,71,6,4,1,5};
UINT4 FsMefMeterCBS [ ] ={1,3,6,1,4,1,29601,2,71,6,4,1,6};
UINT4 FsMefMeterEIR [ ] ={1,3,6,1,4,1,29601,2,71,6,4,1,7};
UINT4 FsMefMeterEBS [ ] ={1,3,6,1,4,1,29601,2,71,6,4,1,8};
UINT4 FsMefMeterStatus [ ] ={1,3,6,1,4,1,29601,2,71,6,4,1,9};
UINT4 FsMefPolicyMapId [ ] ={1,3,6,1,4,1,29601,2,71,6,5,1,1};
UINT4 FsMefPolicyMapName [ ] ={1,3,6,1,4,1,29601,2,71,6,5,1,2};
UINT4 FsMefPolicyMapCLASS [ ] ={1,3,6,1,4,1,29601,2,71,6,5,1,3};
UINT4 FsMefPolicyMapMeterTableId [ ] ={1,3,6,1,4,1,29601,2,71,6,5,1,4};
UINT4 FsMefPolicyMapStatus [ ] ={1,3,6,1,4,1,29601,2,71,6,5,1,5};
UINT4 FsMefMegIndex [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,1};
UINT4 FsMefMeIndex [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,2};
UINT4 FsMefMepIdentifier [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,3};
UINT4 FsMefMepTransmitLmmStatus [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,4};
UINT4 FsMefMepTransmitLmmResultOK [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,5};
UINT4 FsMefMepTransmitLmmInterval [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,6};
UINT4 FsMefMepTransmitLmmDeadline [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,7};
UINT4 FsMefMepTransmitLmmDropEnable [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,8};
UINT4 FsMefMepTransmitLmmPriority [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,9};
UINT4 FsMefMepTransmitLmmDestMacAddress [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,10};
UINT4 FsMefMepTransmitLmmDestMepId [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,11};
UINT4 FsMefMepTransmitLmmDestIsMepId [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,12};
UINT4 FsMefMepTransmitLmmMessages [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,13};
UINT4 FsMefMepTxFCf [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,14};
UINT4 FsMefMepRxFCb [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,15};
UINT4 FsMefMepTxFCb [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,16};
UINT4 FsMefMepNearEndFrameLossThreshold [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,17};
UINT4 FsMefMepFarEndFrameLossThreshold [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,18};
UINT4 FsMefMepTransmitDmStatus [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,19};
UINT4 FsMefMepTransmitDmResultOK [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,20};
UINT4 FsMefMepTransmitDmType [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,21};
UINT4 FsMefMepTransmitDmInterval [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,22};
UINT4 FsMefMepTransmitDmMessages [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,23};
UINT4 FsMefMepTransmitDmmDropEnable [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,24};
UINT4 FsMefMepTransmit1DmDropEnable [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,25};
UINT4 FsMefMepTransmitDmmPriority [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,26};
UINT4 FsMefMepTransmit1DmPriority [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,27};
UINT4 FsMefMepTransmitDmDestMacAddress [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,28};
UINT4 FsMefMepTransmitDmDestMepId [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,29};
UINT4 FsMefMepTransmitDmDestIsMepId [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,30};
UINT4 FsMefMepTransmitDmDeadline [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,31};
UINT4 FsMefMepDmrOptionalFields [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,32};
UINT4 FsMefMep1DmRecvCapability [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,33};
UINT4 FsMefMepFrameDelayThreshold [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,34};
UINT4 FsMefMepRowStatus [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,35};
UINT4 FsMefMepRxFCf [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,36};
UINT4 FsMefMep1DmTransInterval [ ] ={1,3,6,1,4,1,29601,2,71,7,1,1,37};
UINT4 FsMefFdTransId [ ] ={1,3,6,1,4,1,29601,2,71,7,2,1,1};
UINT4 FsMefFdSeqNumber [ ] ={1,3,6,1,4,1,29601,2,71,7,2,1,2};
UINT4 FsMefFdTxTimeStampf [ ] ={1,3,6,1,4,1,29601,2,71,7,2,1,3};
UINT4 FsMefFdMeasurementTimeStamp [ ] ={1,3,6,1,4,1,29601,2,71,7,2,1,4};
UINT4 FsMefFdPeerMepMacAddress [ ] ={1,3,6,1,4,1,29601,2,71,7,2,1,5};
UINT4 FsMefFdIfIndex [ ] ={1,3,6,1,4,1,29601,2,71,7,2,1,6};
UINT4 FsMefFdDelayValue [ ] ={1,3,6,1,4,1,29601,2,71,7,2,1,7};
UINT4 FsMefFdIFDV [ ] ={1,3,6,1,4,1,29601,2,71,7,2,1,8};
UINT4 FsMefFdFDV [ ] ={1,3,6,1,4,1,29601,2,71,7,2,1,9};
UINT4 FsMefFdMeasurementType [ ] ={1,3,6,1,4,1,29601,2,71,7,2,1,10};
UINT4 FsMefFdStatsTimeStamp [ ] ={1,3,6,1,4,1,29601,2,71,7,3,1,1};
UINT4 FsMefFdStatsDmmOut [ ] ={1,3,6,1,4,1,29601,2,71,7,3,1,2};
UINT4 FsMefFdStatsDmrIn [ ] ={1,3,6,1,4,1,29601,2,71,7,3,1,3};
UINT4 FsMefFdStatsDelayAverage [ ] ={1,3,6,1,4,1,29601,2,71,7,3,1,4};
UINT4 FsMefFdStatsFDVAverage [ ] ={1,3,6,1,4,1,29601,2,71,7,3,1,5};
UINT4 FsMefFdStatsIFDVAverage [ ] ={1,3,6,1,4,1,29601,2,71,7,3,1,6};
UINT4 FsMefFdStatsDelayMin [ ] ={1,3,6,1,4,1,29601,2,71,7,3,1,7};
UINT4 FsMefFdStatsDelayMax [ ] ={1,3,6,1,4,1,29601,2,71,7,3,1,8};
UINT4 FsMefFlTransId [ ] ={1,3,6,1,4,1,29601,2,71,7,4,1,1};
UINT4 FsMefFlSeqNumber [ ] ={1,3,6,1,4,1,29601,2,71,7,4,1,2};
UINT4 FsMefFlMeasurementTimeStamp [ ] ={1,3,6,1,4,1,29601,2,71,7,4,1,3};
UINT4 FsMefFlPeerMepMacAddress [ ] ={1,3,6,1,4,1,29601,2,71,7,4,1,4};
UINT4 FsMefFlIfIndex [ ] ={1,3,6,1,4,1,29601,2,71,7,4,1,5};
UINT4 FsMefFlFarEndLoss [ ] ={1,3,6,1,4,1,29601,2,71,7,4,1,6};
UINT4 FsMefFlNearEndLoss [ ] ={1,3,6,1,4,1,29601,2,71,7,4,1,7};
UINT4 FsMefFlMeasurementTime [ ] ={1,3,6,1,4,1,29601,2,71,7,4,1,8};
UINT4 FsMefFlStatsTimeStamp [ ] ={1,3,6,1,4,1,29601,2,71,7,5,1,1};
UINT4 FsMefFlStatsMessagesOut [ ] ={1,3,6,1,4,1,29601,2,71,7,5,1,2};
UINT4 FsMefFlStatsMessagesIn [ ] ={1,3,6,1,4,1,29601,2,71,7,5,1,3};
UINT4 FsMefFlStatsFarEndLossAverage [ ] ={1,3,6,1,4,1,29601,2,71,7,5,1,4};
UINT4 FsMefFlStatsNearEndLossAverage [ ] ={1,3,6,1,4,1,29601,2,71,7,5,1,5};
UINT4 FsMefFlStatsMeasurementType [ ] ={1,3,6,1,4,1,29601,2,71,7,5,1,6};
UINT4 FsMefFlStatsFarEndLossMin [ ] ={1,3,6,1,4,1,29601,2,71,7,5,1,7};
UINT4 FsMefFlStatsFarEndLossMax [ ] ={1,3,6,1,4,1,29601,2,71,7,5,1,8};
UINT4 FsMefFlStatsNearEndLossMin [ ] ={1,3,6,1,4,1,29601,2,71,7,5,1,9};
UINT4 FsMefFlStatsNearEndLossMax [ ] ={1,3,6,1,4,1,29601,2,71,7,5,1,10};
UINT4 FsMefMepAvailabilityStatus [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,1};
UINT4 FsMefMepAvailabilityResultOK [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,2};
UINT4 FsMefMepAvailabilityInterval [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,3};
UINT4 FsMefMepAvailabilityDeadline [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,4};
UINT4 FsMefMepAvailabilityLowerThreshold [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,5};
UINT4 FsMefMepAvailabilityUpperThreshold [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,6};
UINT4 FsMefMepAvailabilityModestAreaIsAvailable [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,7};
UINT4 FsMefMepAvailabilityWindowSize [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,8};
UINT4 FsMefMepAvailabilityDestMacAddress [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,9};
UINT4 FsMefMepAvailabilityDestMepId [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,10};
UINT4 FsMefMepAvailabilityDestIsMepId [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,11};
UINT4 FsMefMepAvailabilityType [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,12};
UINT4 FsMefMepAvailabilitySchldDownInitTime [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,13};
UINT4 FsMefMepAvailabilitySchldDownEndTime [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,14};
UINT4 FsMefMepAvailabilityPriority [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,15};
UINT4 FsMefMepAvailabilityDropEnable [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,16};
UINT4 FsMefMepAvailabilityPercentage [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,17};
UINT4 FsMefMepAvailabilityRowStatus [ ] ={1,3,6,1,4,1,29601,2,71,7,6,1,18};
UINT4 FsMefUniId [ ] ={1,3,6,1,4,1,29601,2,71,2,2,1,1};
UINT4 FsMefUniType [ ] ={1,3,6,1,4,1,29601,2,71,2,2,1,2};
UINT4 FsMefUniListRowStatus [ ] ={1,3,6,1,4,1,29601,2,71,2,2,1,3};
UINT4 FsEvcStatsContextId [ ] ={1,3,6,1,4,1,29601,2,71,8,1,1,1};
UINT4 FsEvcStatsId [ ] ={1,3,6,1,4,1,29601,2,71,8,1,1,2};
UINT4 FsEvcStatsRxFrames [ ] ={1,3,6,1,4,1,29601,2,71,8,1,1,3};
UINT4 FsEvcStatsRxBytes [ ] ={1,3,6,1,4,1,29601,2,71,8,1,1,4};
UINT4 FsEvcStatsTxFrames [ ] ={1,3,6,1,4,1,29601,2,71,8,1,1,5};
UINT4 FsEvcStatsTxBytes [ ] ={1,3,6,1,4,1,29601,2,71,8,1,1,6};
UINT4 FsEvcStatsDiscardFrames [ ] ={1,3,6,1,4,1,29601,2,71,8,1,1,7};
UINT4 FsEvcStatsDiscardBytes [ ] ={1,3,6,1,4,1,29601,2,71,8,1,1,8};




tMbDbEntry fsmefMibEntry[]= {

{{13,FsMefContextId}, GetNextIndexFsMefContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMefContextName}, GetNextIndexFsMefContextTable, FsMefContextNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMefTransmode}, GetNextIndexFsMefContextTable, FsMefTransmodeGet, FsMefTransmodeSet, FsMefTransmodeTest, FsMefContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefContextTableINDEX, 1, 0, 0, "1"},

{{13,FsMefFrameLossBufferClear}, GetNextIndexFsMefContextTable, FsMefFrameLossBufferClearGet, FsMefFrameLossBufferClearSet, FsMefFrameLossBufferClearTest, FsMefContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMefFrameDelayBufferClear}, GetNextIndexFsMefContextTable, FsMefFrameDelayBufferClearGet, FsMefFrameDelayBufferClearSet, FsMefFrameDelayBufferClearTest, FsMefContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefContextTableINDEX, 1, 0, 0, NULL},

{{13,FsMefFrameLossBufferSize}, GetNextIndexFsMefContextTable, FsMefFrameLossBufferSizeGet, FsMefFrameLossBufferSizeSet, FsMefFrameLossBufferSizeTest, FsMefContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMefContextTableINDEX, 1, 0, 0, "1024"},

{{13,FsMefFrameDelayBufferSize}, GetNextIndexFsMefContextTable, FsMefFrameDelayBufferSizeGet, FsMefFrameDelayBufferSizeSet, FsMefFrameDelayBufferSizeTest, FsMefContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMefContextTableINDEX, 1, 0, 0, "1024"},

{{13,FsUniId}, GetNextIndexFsUniTable, FsUniIdGet, FsUniIdSet, FsUniIdTest, FsUniTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniPhysicalMedium}, GetNextIndexFsUniTable, FsUniPhysicalMediumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniSpeed}, GetNextIndexFsUniTable, FsUniSpeedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniMode}, GetNextIndexFsUniTable, FsUniModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsUniTableINDEX, 1, 0, 0, "1"},

{{13,FsUniMacLayer}, GetNextIndexFsUniTable, FsUniMacLayerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniMtu}, GetNextIndexFsUniTable, FsUniMtuGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniServiceMultiplexingBundling}, GetNextIndexFsUniTable, FsUniServiceMultiplexingBundlingGet, FsUniServiceMultiplexingBundlingSet, FsUniServiceMultiplexingBundlingTest, FsUniTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniCVlanId}, GetNextIndexFsUniTable, FsUniCVlanIdGet, FsUniCVlanIdSet, FsUniCVlanIdTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniMaxEvcs}, GetNextIndexFsUniTable, FsUniMaxEvcsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsUniTableINDEX, 1, 0, 0, "1"},

{{13,FsUniL2CPDot1x}, GetNextIndexFsUniTable, FsUniL2CPDot1xGet, FsUniL2CPDot1xSet, FsUniL2CPDot1xTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniL2CPLacp}, GetNextIndexFsUniTable, FsUniL2CPLacpGet, FsUniL2CPLacpSet, FsUniL2CPLacpTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniL2CPStp}, GetNextIndexFsUniTable, FsUniL2CPStpGet, FsUniL2CPStpSet, FsUniL2CPStpTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniL2CPGvrp}, GetNextIndexFsUniTable, FsUniL2CPGvrpGet, FsUniL2CPGvrpSet, FsUniL2CPGvrpTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniL2CPGmrp}, GetNextIndexFsUniTable, FsUniL2CPGmrpGet, FsUniL2CPGmrpSet, FsUniL2CPGmrpTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniL2CPMvrp}, GetNextIndexFsUniTable, FsUniL2CPMvrpGet, FsUniL2CPMvrpSet, FsUniL2CPMvrpTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniL2CPMmrp}, GetNextIndexFsUniTable, FsUniL2CPMmrpGet, FsUniL2CPMmrpSet, FsUniL2CPMmrpTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniL2CPLldp}, GetNextIndexFsUniTable, FsUniL2CPLldpGet, FsUniL2CPLldpSet, FsUniL2CPLldpTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniL2CPElmi}, GetNextIndexFsUniTable, FsUniL2CPElmiGet, FsUniL2CPElmiSet, FsUniL2CPElmiTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniRowStatus}, GetNextIndexFsUniTable, FsUniRowStatusGet, FsUniRowStatusSet, FsUniRowStatusTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 1, NULL},

{{13,FsUniL2CPEcfm}, GetNextIndexFsUniTable, FsUniL2CPEcfmGet, FsUniL2CPEcfmSet, FsUniL2CPEcfmTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniL2CPOverrideOption}, GetNextIndexFsUniTable, FsUniL2CPOverrideOptionGet, FsUniL2CPOverrideOptionSet, FsUniL2CPOverrideOptionTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsUniL2CPEoam}, GetNextIndexFsUniTable, FsUniL2CPEoamGet, FsUniL2CPEoamSet, FsUniL2CPEoamTest, FsUniTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniTableINDEX, 1, 0, 0, NULL},

{{13,FsMefUniId}, GetNextIndexFsMefUniListTable, FsMefUniIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefUniListTableINDEX, 3, 0, 0, NULL},

{{13,FsMefUniType}, GetNextIndexFsMefUniListTable, FsMefUniTypeGet, FsMefUniTypeSet, FsMefUniTypeTest, FsMefUniListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefUniListTableINDEX, 3, 0, 0, "1"},

{{13,FsMefUniListRowStatus}, GetNextIndexFsMefUniListTable, FsMefUniListRowStatusGet, FsMefUniListRowStatusSet, FsMefUniListRowStatusTest, FsMefUniListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefUniListTableINDEX, 3, 0, 1, NULL},

{{13,FsEvcContextId}, GetNextIndexFsEvcTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcIndex}, GetNextIndexFsEvcTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcId}, GetNextIndexFsEvcTable, FsEvcIdGet, FsEvcIdSet, FsEvcIdTest, FsEvcTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcType}, GetNextIndexFsEvcTable, FsEvcTypeGet, FsEvcTypeSet, FsEvcTypeTest, FsEvcTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvcTableINDEX, 2, 0, 0, "2"},

{{13,FsEvcMaxUni}, GetNextIndexFsEvcTable, FsEvcMaxUniGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsEvcTableINDEX, 2, 0, 0, "2"},

{{13,FsEvcCVlanIdPreservation}, GetNextIndexFsEvcTable, FsEvcCVlanIdPreservationGet, FsEvcCVlanIdPreservationSet, FsEvcCVlanIdPreservationTest, FsEvcTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcCVlanCoSPreservation}, GetNextIndexFsEvcTable, FsEvcCVlanCoSPreservationGet, FsEvcCVlanCoSPreservationSet, FsEvcCVlanCoSPreservationTest, FsEvcTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcRowStatus}, GetNextIndexFsEvcTable, FsEvcRowStatusGet, FsEvcRowStatusSet, FsEvcRowStatusTest, FsEvcTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvcTableINDEX, 2, 0, 1, NULL},

{{13,FsEvcMtu}, GetNextIndexFsEvcTable, FsEvcMtuGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcL2CPTunnel}, GetNextIndexFsEvcTable, FsEvcL2CPTunnelGet, FsEvcL2CPTunnelSet, FsEvcL2CPTunnelTest, FsEvcTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcL2CPPeer}, GetNextIndexFsEvcTable, FsEvcL2CPPeerGet, FsEvcL2CPPeerSet, FsEvcL2CPPeerTest, FsEvcTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcL2CPDiscard}, GetNextIndexFsEvcTable, FsEvcL2CPDiscardGet, FsEvcL2CPDiscardSet, FsEvcL2CPDiscardTest, FsEvcTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcLoopbackStatus}, GetNextIndexFsEvcTable, FsEvcLoopbackStatusGet, FsEvcLoopbackStatusSet, FsEvcLoopbackStatusTest, FsEvcTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvcTableINDEX, 2, 0, 0, "2"},

{{13,FsEvcFilterInstance}, GetNextIndexFsEvcFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsEvcFilterTableINDEX, 3, 0, 0, NULL},

{{13,FsEvcFilterDestMacAddress}, GetNextIndexFsEvcFilterTable, FsEvcFilterDestMacAddressGet, FsEvcFilterDestMacAddressSet, FsEvcFilterDestMacAddressTest, FsEvcFilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsEvcFilterTableINDEX, 3, 0, 0, NULL},

{{13,FsEvcFilterAction}, GetNextIndexFsEvcFilterTable, FsEvcFilterActionGet, FsEvcFilterActionSet, FsEvcFilterActionTest, FsEvcFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvcFilterTableINDEX, 3, 0, 0, NULL},

{{13,FsEvcFilterId}, GetNextIndexFsEvcFilterTable, FsEvcFilterIdGet, FsEvcFilterIdSet, FsEvcFilterIdTest, FsEvcFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsEvcFilterTableINDEX, 3, 0, 0, NULL},

{{13,FsEvcFilterRowStatus}, GetNextIndexFsEvcFilterTable, FsEvcFilterRowStatusGet, FsEvcFilterRowStatusSet, FsEvcFilterRowStatusTest, FsEvcFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsEvcFilterTableINDEX, 3, 0, 1, NULL},

{{13,FsMefETreeIngresPort}, GetNextIndexFsMefETreeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMefETreeTableINDEX, 3, 0, 0, NULL},

{{13,FsMefETreeEvcIndex}, GetNextIndexFsMefETreeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMefETreeTableINDEX, 3, 0, 0, NULL},

{{13,FsMefETreeEgressPort}, GetNextIndexFsMefETreeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMefETreeTableINDEX, 3, 0, 0, NULL},

{{13,FsMefETreeRowStatus}, GetNextIndexFsMefETreeTable, FsMefETreeRowStatusGet, FsMefETreeRowStatusSet, FsMefETreeRowStatusTest, FsMefETreeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefETreeTableINDEX, 3, 0, 1, NULL},

{{13,FsUniCVlanEvcCVlanId}, GetNextIndexFsUniCVlanEvcTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsUniCVlanEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsUniCVlanEvcEvcIndex}, GetNextIndexFsUniCVlanEvcTable, FsUniCVlanEvcEvcIndexGet, FsUniCVlanEvcEvcIndexSet, FsUniCVlanEvcEvcIndexTest, FsUniCVlanEvcTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsUniCVlanEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsUniCVlanEvcRowStatus}, GetNextIndexFsUniCVlanEvcTable, FsUniCVlanEvcRowStatusGet, FsUniCVlanEvcRowStatusSet, FsUniCVlanEvcRowStatusTest, FsUniCVlanEvcTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsUniCVlanEvcTableINDEX, 2, 0, 1, NULL},

{{13,FsUniEvcId}, GetNextIndexFsUniCVlanEvcTable, FsUniEvcIdGet, FsUniEvcIdSet, FsUniEvcIdTest, FsUniCVlanEvcTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsUniCVlanEvcTableINDEX, 2, 0, 0, NULL},

{{13,FsMefFilterNo}, GetNextIndexFsMefFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMefFilterTableINDEX, 1, 0, 0, NULL},

{{13,FsMefFilterDscp}, GetNextIndexFsMefFilterTable, FsMefFilterDscpGet, FsMefFilterDscpSet, FsMefFilterDscpTest, FsMefFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMefFilterTableINDEX, 1, 0, 0, "-1"},

{{13,FsMefFilterDirection}, GetNextIndexFsMefFilterTable, FsMefFilterDirectionGet, FsMefFilterDirectionSet, FsMefFilterDirectionTest, FsMefFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefFilterTableINDEX, 1, 0, 0, "1"},

{{13,FsMefFilterEvc}, GetNextIndexFsMefFilterTable, FsMefFilterEvcGet, FsMefFilterEvcSet, FsMefFilterEvcTest, FsMefFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMefFilterTableINDEX, 1, 0, 0, "0"},

{{13,FsMefFilterCVlanPriority}, GetNextIndexFsMefFilterTable, FsMefFilterCVlanPriorityGet, FsMefFilterCVlanPrioritySet, FsMefFilterCVlanPriorityTest, FsMefFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMefFilterTableINDEX, 1, 0, 0, "-1"},

{{13,FsMefFilterIfIndex}, GetNextIndexFsMefFilterTable, FsMefFilterIfIndexGet, FsMefFilterIfIndexSet, FsMefFilterIfIndexTest, FsMefFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMefFilterTableINDEX, 1, 0, 0, NULL},

{{13,FsMefFilterStatus}, GetNextIndexFsMefFilterTable, FsMefFilterStatusGet, FsMefFilterStatusSet, FsMefFilterStatusTest, FsMefFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefFilterTableINDEX, 1, 0, 1, NULL},

{{13,FsMefClassMapId}, GetNextIndexFsMefClassMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefClassMapTableINDEX, 1, 0, 0, NULL},

{{13,FsMefClassMapName}, GetNextIndexFsMefClassMapTable, FsMefClassMapNameGet, FsMefClassMapNameSet, FsMefClassMapNameTest, FsMefClassMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMefClassMapTableINDEX, 1, 0, 0, NULL},

{{13,FsMefClassMapFilterId}, GetNextIndexFsMefClassMapTable, FsMefClassMapFilterIdGet, FsMefClassMapFilterIdSet, FsMefClassMapFilterIdTest, FsMefClassMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefClassMapTableINDEX, 1, 0, 0, NULL},

{{13,FsMefClassMapCLASS}, GetNextIndexFsMefClassMapTable, FsMefClassMapCLASSGet, FsMefClassMapCLASSSet, FsMefClassMapCLASSTest, FsMefClassMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefClassMapTableINDEX, 1, 0, 0, "0"},

{{13,FsMefClassMapStatus}, GetNextIndexFsMefClassMapTable, FsMefClassMapStatusGet, FsMefClassMapStatusSet, FsMefClassMapStatusTest, FsMefClassMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefClassMapTableINDEX, 1, 0, 1, NULL},

{{13,FsMefClassCLASS}, GetNextIndexFsMefClassTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefClassTableINDEX, 1, 0, 0, NULL},

{{13,FsMefClassStatus}, GetNextIndexFsMefClassTable, FsMefClassStatusGet, FsMefClassStatusSet, FsMefClassStatusTest, FsMefClassTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefClassTableINDEX, 1, 0, 1, NULL},

{{13,FsMefMeterId}, GetNextIndexFsMefMeterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefMeterTableINDEX, 1, 0, 0, NULL},

{{13,FsMefMeterName}, GetNextIndexFsMefMeterTable, FsMefMeterNameGet, FsMefMeterNameSet, FsMefMeterNameTest, FsMefMeterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMefMeterTableINDEX, 1, 0, 0, NULL},

{{13,FsMefMeterType}, GetNextIndexFsMefMeterTable, FsMefMeterTypeGet, FsMefMeterTypeSet, FsMefMeterTypeTest, FsMefMeterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMeterTableINDEX, 1, 0, 0, NULL},

{{13,FsMefMeterColorMode}, GetNextIndexFsMefMeterTable, FsMefMeterColorModeGet, FsMefMeterColorModeSet, FsMefMeterColorModeTest, FsMefMeterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMeterTableINDEX, 1, 0, 0, NULL},

{{13,FsMefMeterCIR}, GetNextIndexFsMefMeterTable, FsMefMeterCIRGet, FsMefMeterCIRSet, FsMefMeterCIRTest, FsMefMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMeterTableINDEX, 1, 0, 0, NULL},

{{13,FsMefMeterCBS}, GetNextIndexFsMefMeterTable, FsMefMeterCBSGet, FsMefMeterCBSSet, FsMefMeterCBSTest, FsMefMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMeterTableINDEX, 1, 0, 0, NULL},

{{13,FsMefMeterEIR}, GetNextIndexFsMefMeterTable, FsMefMeterEIRGet, FsMefMeterEIRSet, FsMefMeterEIRTest, FsMefMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMeterTableINDEX, 1, 0, 0, NULL},

{{13,FsMefMeterEBS}, GetNextIndexFsMefMeterTable, FsMefMeterEBSGet, FsMefMeterEBSSet, FsMefMeterEBSTest, FsMefMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMeterTableINDEX, 1, 0, 0, NULL},

{{13,FsMefMeterStatus}, GetNextIndexFsMefMeterTable, FsMefMeterStatusGet, FsMefMeterStatusSet, FsMefMeterStatusTest, FsMefMeterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMeterTableINDEX, 1, 0, 1, NULL},

{{13,FsMefPolicyMapId}, GetNextIndexFsMefPolicyMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefPolicyMapTableINDEX, 1, 0, 0, NULL},

{{13,FsMefPolicyMapName}, GetNextIndexFsMefPolicyMapTable, FsMefPolicyMapNameGet, FsMefPolicyMapNameSet, FsMefPolicyMapNameTest, FsMefPolicyMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMefPolicyMapTableINDEX, 1, 0, 0, NULL},

{{13,FsMefPolicyMapCLASS}, GetNextIndexFsMefPolicyMapTable, FsMefPolicyMapCLASSGet, FsMefPolicyMapCLASSSet, FsMefPolicyMapCLASSTest, FsMefPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefPolicyMapTableINDEX, 1, 0, 0, "0"},

{{13,FsMefPolicyMapMeterTableId}, GetNextIndexFsMefPolicyMapTable, FsMefPolicyMapMeterTableIdGet, FsMefPolicyMapMeterTableIdSet, FsMefPolicyMapMeterTableIdTest, FsMefPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefPolicyMapTableINDEX, 1, 0, 0, "0"},

{{13,FsMefPolicyMapStatus}, GetNextIndexFsMefPolicyMapTable, FsMefPolicyMapStatusGet, FsMefPolicyMapStatusSet, FsMefPolicyMapStatusTest, FsMefPolicyMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefPolicyMapTableINDEX, 1, 0, 1, NULL},

{{13,FsMefMegIndex}, GetNextIndexFsMefMepTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMeIndex}, GetNextIndexFsMefMepTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepIdentifier}, GetNextIndexFsMefMepTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitLmmStatus}, GetNextIndexFsMefMepTable, FsMefMepTransmitLmmStatusGet, FsMefMepTransmitLmmStatusSet, FsMefMepTransmitLmmStatusTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitLmmResultOK}, GetNextIndexFsMefMepTable, FsMefMepTransmitLmmResultOKGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMefMepTableINDEX, 4, 0, 0, "1"},

{{13,FsMefMepTransmitLmmInterval}, GetNextIndexFsMefMepTable, FsMefMepTransmitLmmIntervalGet, FsMefMepTransmitLmmIntervalSet, FsMefMepTransmitLmmIntervalTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitLmmDeadline}, GetNextIndexFsMefMepTable, FsMefMepTransmitLmmDeadlineGet, FsMefMepTransmitLmmDeadlineSet, FsMefMepTransmitLmmDeadlineTest, FsMefMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitLmmDropEnable}, GetNextIndexFsMefMepTable, FsMefMepTransmitLmmDropEnableGet, FsMefMepTransmitLmmDropEnableSet, FsMefMepTransmitLmmDropEnableTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "2"},

{{13,FsMefMepTransmitLmmPriority}, GetNextIndexFsMefMepTable, FsMefMepTransmitLmmPriorityGet, FsMefMepTransmitLmmPrioritySet, FsMefMepTransmitLmmPriorityTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "7"},

{{13,FsMefMepTransmitLmmDestMacAddress}, GetNextIndexFsMefMepTable, FsMefMepTransmitLmmDestMacAddressGet, FsMefMepTransmitLmmDestMacAddressSet, FsMefMepTransmitLmmDestMacAddressTest, FsMefMepTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitLmmDestMepId}, GetNextIndexFsMefMepTable, FsMefMepTransmitLmmDestMepIdGet, FsMefMepTransmitLmmDestMepIdSet, FsMefMepTransmitLmmDestMepIdTest, FsMefMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitLmmDestIsMepId}, GetNextIndexFsMefMepTable, FsMefMepTransmitLmmDestIsMepIdGet, FsMefMepTransmitLmmDestIsMepIdSet, FsMefMepTransmitLmmDestIsMepIdTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitLmmMessages}, GetNextIndexFsMefMepTable, FsMefMepTransmitLmmMessagesGet, FsMefMepTransmitLmmMessagesSet, FsMefMepTransmitLmmMessagesTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTxFCf}, GetNextIndexFsMefMepTable, FsMefMepTxFCfGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepRxFCb}, GetNextIndexFsMefMepTable, FsMefMepRxFCbGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTxFCb}, GetNextIndexFsMefMepTable, FsMefMepTxFCbGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepNearEndFrameLossThreshold}, GetNextIndexFsMefMepTable, FsMefMepNearEndFrameLossThresholdGet, FsMefMepNearEndFrameLossThresholdSet, FsMefMepNearEndFrameLossThresholdTest, FsMefMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepFarEndFrameLossThreshold}, GetNextIndexFsMefMepTable, FsMefMepFarEndFrameLossThresholdGet, FsMefMepFarEndFrameLossThresholdSet, FsMefMepFarEndFrameLossThresholdTest, FsMefMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitDmStatus}, GetNextIndexFsMefMepTable, FsMefMepTransmitDmStatusGet, FsMefMepTransmitDmStatusSet, FsMefMepTransmitDmStatusTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitDmResultOK}, GetNextIndexFsMefMepTable, FsMefMepTransmitDmResultOKGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMefMepTableINDEX, 4, 0, 0, "1"},

{{13,FsMefMepTransmitDmType}, GetNextIndexFsMefMepTable, FsMefMepTransmitDmTypeGet, FsMefMepTransmitDmTypeSet, FsMefMepTransmitDmTypeTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "2"},

{{13,FsMefMepTransmitDmInterval}, GetNextIndexFsMefMepTable, FsMefMepTransmitDmIntervalGet, FsMefMepTransmitDmIntervalSet, FsMefMepTransmitDmIntervalTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "100"},

{{13,FsMefMepTransmitDmMessages}, GetNextIndexFsMefMepTable, FsMefMepTransmitDmMessagesGet, FsMefMepTransmitDmMessagesSet, FsMefMepTransmitDmMessagesTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitDmmDropEnable}, GetNextIndexFsMefMepTable, FsMefMepTransmitDmmDropEnableGet, FsMefMepTransmitDmmDropEnableSet, FsMefMepTransmitDmmDropEnableTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "2"},

{{13,FsMefMepTransmit1DmDropEnable}, GetNextIndexFsMefMepTable, FsMefMepTransmit1DmDropEnableGet, FsMefMepTransmit1DmDropEnableSet, FsMefMepTransmit1DmDropEnableTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "2"},

{{13,FsMefMepTransmitDmmPriority}, GetNextIndexFsMefMepTable, FsMefMepTransmitDmmPriorityGet, FsMefMepTransmitDmmPrioritySet, FsMefMepTransmitDmmPriorityTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "7"},

{{13,FsMefMepTransmit1DmPriority}, GetNextIndexFsMefMepTable, FsMefMepTransmit1DmPriorityGet, FsMefMepTransmit1DmPrioritySet, FsMefMepTransmit1DmPriorityTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "7"},

{{13,FsMefMepTransmitDmDestMacAddress}, GetNextIndexFsMefMepTable, FsMefMepTransmitDmDestMacAddressGet, FsMefMepTransmitDmDestMacAddressSet, FsMefMepTransmitDmDestMacAddressTest, FsMefMepTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitDmDestMepId}, GetNextIndexFsMefMepTable, FsMefMepTransmitDmDestMepIdGet, FsMefMepTransmitDmDestMepIdSet, FsMefMepTransmitDmDestMepIdTest, FsMefMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitDmDestIsMepId}, GetNextIndexFsMefMepTable, FsMefMepTransmitDmDestIsMepIdGet, FsMefMepTransmitDmDestIsMepIdSet, FsMefMepTransmitDmDestIsMepIdTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepTransmitDmDeadline}, GetNextIndexFsMefMepTable, FsMefMepTransmitDmDeadlineGet, FsMefMepTransmitDmDeadlineSet, FsMefMepTransmitDmDeadlineTest, FsMefMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepDmrOptionalFields}, GetNextIndexFsMefMepTable, FsMefMepDmrOptionalFieldsGet, FsMefMepDmrOptionalFieldsSet, FsMefMepDmrOptionalFieldsTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "2"},

{{13,FsMefMep1DmRecvCapability}, GetNextIndexFsMefMepTable, FsMefMep1DmRecvCapabilityGet, FsMefMep1DmRecvCapabilitySet, FsMefMep1DmRecvCapabilityTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "1"},

{{13,FsMefMepFrameDelayThreshold}, GetNextIndexFsMefMepTable, FsMefMepFrameDelayThresholdGet, FsMefMepFrameDelayThresholdSet, FsMefMepFrameDelayThresholdTest, FsMefMepTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "200"},

{{13,FsMefMepRowStatus}, GetNextIndexFsMefMepTable, FsMefMepRowStatusGet, FsMefMepRowStatusSet, FsMefMepRowStatusTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 1, NULL},

{{13,FsMefMepRxFCf}, GetNextIndexFsMefMepTable, FsMefMepRxFCfGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefMepTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMep1DmTransInterval}, GetNextIndexFsMefMepTable, FsMefMep1DmTransIntervalGet, FsMefMep1DmTransIntervalSet, FsMefMep1DmTransIntervalTest, FsMefMepTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepTableINDEX, 4, 0, 0, "0"},

{{13,FsMefFdTransId}, GetNextIndexFsMefFdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefFdTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFdSeqNumber}, GetNextIndexFsMefFdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefFdTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFdTxTimeStampf}, GetNextIndexFsMefFdTable, FsMefFdTxTimeStampfGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefFdTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFdMeasurementTimeStamp}, GetNextIndexFsMefFdTable, FsMefFdMeasurementTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMefFdTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFdPeerMepMacAddress}, GetNextIndexFsMefFdTable, FsMefFdPeerMepMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMefFdTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFdIfIndex}, GetNextIndexFsMefFdTable, FsMefFdIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMefFdTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFdDelayValue}, GetNextIndexFsMefFdTable, FsMefFdDelayValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefFdTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFdIFDV}, GetNextIndexFsMefFdTable, FsMefFdIFDVGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefFdTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFdFDV}, GetNextIndexFsMefFdTable, FsMefFdFDVGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefFdTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFdMeasurementType}, GetNextIndexFsMefFdTable, FsMefFdMeasurementTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMefFdTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFdStatsTimeStamp}, GetNextIndexFsMefFdStatsTable, FsMefFdStatsTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMefFdStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFdStatsDmmOut}, GetNextIndexFsMefFdStatsTable, FsMefFdStatsDmmOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFdStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFdStatsDmrIn}, GetNextIndexFsMefFdStatsTable, FsMefFdStatsDmrInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFdStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFdStatsDelayAverage}, GetNextIndexFsMefFdStatsTable, FsMefFdStatsDelayAverageGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefFdStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFdStatsFDVAverage}, GetNextIndexFsMefFdStatsTable, FsMefFdStatsFDVAverageGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefFdStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFdStatsIFDVAverage}, GetNextIndexFsMefFdStatsTable, FsMefFdStatsIFDVAverageGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefFdStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFdStatsDelayMin}, GetNextIndexFsMefFdStatsTable, FsMefFdStatsDelayMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefFdStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFdStatsDelayMax}, GetNextIndexFsMefFdStatsTable, FsMefFdStatsDelayMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefFdStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFlTransId}, GetNextIndexFsMefFlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefFlTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFlSeqNumber}, GetNextIndexFsMefFlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMefFlTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFlMeasurementTimeStamp}, GetNextIndexFsMefFlTable, FsMefFlMeasurementTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMefFlTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFlPeerMepMacAddress}, GetNextIndexFsMefFlTable, FsMefFlPeerMepMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMefFlTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFlIfIndex}, GetNextIndexFsMefFlTable, FsMefFlIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMefFlTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFlFarEndLoss}, GetNextIndexFsMefFlTable, FsMefFlFarEndLossGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFlTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFlNearEndLoss}, GetNextIndexFsMefFlTable, FsMefFlNearEndLossGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFlTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFlMeasurementTime}, GetNextIndexFsMefFlTable, FsMefFlMeasurementTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMefFlTableINDEX, 6, 0, 0, NULL},

{{13,FsMefFlStatsTimeStamp}, GetNextIndexFsMefFlStatsTable, FsMefFlStatsTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMefFlStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFlStatsMessagesOut}, GetNextIndexFsMefFlStatsTable, FsMefFlStatsMessagesOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFlStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFlStatsMessagesIn}, GetNextIndexFsMefFlStatsTable, FsMefFlStatsMessagesInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFlStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFlStatsFarEndLossAverage}, GetNextIndexFsMefFlStatsTable, FsMefFlStatsFarEndLossAverageGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFlStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFlStatsNearEndLossAverage}, GetNextIndexFsMefFlStatsTable, FsMefFlStatsNearEndLossAverageGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFlStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFlStatsMeasurementType}, GetNextIndexFsMefFlStatsTable, FsMefFlStatsMeasurementTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMefFlStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFlStatsFarEndLossMin}, GetNextIndexFsMefFlStatsTable, FsMefFlStatsFarEndLossMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFlStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFlStatsFarEndLossMax}, GetNextIndexFsMefFlStatsTable, FsMefFlStatsFarEndLossMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFlStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFlStatsNearEndLossMin}, GetNextIndexFsMefFlStatsTable, FsMefFlStatsNearEndLossMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFlStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefFlStatsNearEndLossMax}, GetNextIndexFsMefFlStatsTable, FsMefFlStatsNearEndLossMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMefFlStatsTableINDEX, 5, 0, 0, NULL},

{{13,FsMefMepAvailabilityStatus}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityStatusGet, FsMefMepAvailabilityStatusSet, FsMefMepAvailabilityStatusTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityResultOK}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityResultOKGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMefMepAvailabilityTableINDEX, 4, 0, 0, "1"},

{{13,FsMefMepAvailabilityInterval}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityIntervalGet, FsMefMepAvailabilityIntervalSet, FsMefMepAvailabilityIntervalTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityDeadline}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityDeadlineGet, FsMefMepAvailabilityDeadlineSet, FsMefMepAvailabilityDeadlineTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityLowerThreshold}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityLowerThresholdGet, FsMefMepAvailabilityLowerThresholdSet, FsMefMepAvailabilityLowerThresholdTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityUpperThreshold}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityUpperThresholdGet, FsMefMepAvailabilityUpperThresholdSet, FsMefMepAvailabilityUpperThresholdTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityModestAreaIsAvailable}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityModestAreaIsAvailableGet, FsMefMepAvailabilityModestAreaIsAvailableSet, FsMefMepAvailabilityModestAreaIsAvailableTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityWindowSize}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityWindowSizeGet, FsMefMepAvailabilityWindowSizeSet, FsMefMepAvailabilityWindowSizeTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityDestMacAddress}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityDestMacAddressGet, FsMefMepAvailabilityDestMacAddressSet, FsMefMepAvailabilityDestMacAddressTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityDestMepId}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityDestMepIdGet, FsMefMepAvailabilityDestMepIdSet, FsMefMepAvailabilityDestMepIdTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityDestIsMepId}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityDestIsMepIdGet, FsMefMepAvailabilityDestIsMepIdSet, FsMefMepAvailabilityDestIsMepIdTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityType}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityTypeGet, FsMefMepAvailabilityTypeSet, FsMefMepAvailabilityTypeTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilitySchldDownInitTime}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilitySchldDownInitTimeGet, FsMefMepAvailabilitySchldDownInitTimeSet, FsMefMepAvailabilitySchldDownInitTimeTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilitySchldDownEndTime}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilitySchldDownEndTimeGet, FsMefMepAvailabilitySchldDownEndTimeSet, FsMefMepAvailabilitySchldDownEndTimeTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityPriority}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityPriorityGet, FsMefMepAvailabilityPrioritySet, FsMefMepAvailabilityPriorityTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, "7"},

{{13,FsMefMepAvailabilityDropEnable}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityDropEnableGet, FsMefMepAvailabilityDropEnableSet, FsMefMepAvailabilityDropEnableTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 0, "2"},

{{13,FsMefMepAvailabilityPercentage}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityPercentageGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMefMepAvailabilityTableINDEX, 4, 0, 0, NULL},

{{13,FsMefMepAvailabilityRowStatus}, GetNextIndexFsMefMepAvailabilityTable, FsMefMepAvailabilityRowStatusGet, FsMefMepAvailabilityRowStatusSet, FsMefMepAvailabilityRowStatusTest, FsMefMepAvailabilityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMefMepAvailabilityTableINDEX, 4, 0, 1, NULL},

{{13,FsEvcStatsContextId}, GetNextIndexFsEvcStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsEvcStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcStatsId}, GetNextIndexFsEvcStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsEvcStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcStatsRxFrames}, GetNextIndexFsEvcStatsTable, FsEvcStatsRxFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsEvcStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcStatsRxBytes}, GetNextIndexFsEvcStatsTable, FsEvcStatsRxBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsEvcStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcStatsTxFrames}, GetNextIndexFsEvcStatsTable, FsEvcStatsTxFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsEvcStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcStatsTxBytes}, GetNextIndexFsEvcStatsTable, FsEvcStatsTxBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsEvcStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcStatsDiscardFrames}, GetNextIndexFsEvcStatsTable, FsEvcStatsDiscardFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsEvcStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsEvcStatsDiscardBytes}, GetNextIndexFsEvcStatsTable, FsEvcStatsDiscardBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsEvcStatsTableINDEX, 2, 0, 0, NULL},
};
tMibData fsmefEntry = { 185, fsmefMibEntry };

#endif /* _FSMEFDB_H */

