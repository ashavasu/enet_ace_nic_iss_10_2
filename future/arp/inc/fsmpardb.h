/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpardb.h,v 1.5 2015/09/15 06:46:24 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPARDB_H
#define _FSMPARDB_H

UINT1 FsMIArpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmpar [] ={1,3,6,1,4,1,29601,2,33};
tSNMP_OID_TYPE fsmparOID = {9, fsmpar};


UINT4 FsMIArpCacheTimeout [ ] ={1,3,6,1,4,1,29601,2,33,1,1,1};
UINT4 FsMIArpCachePendTime [ ] ={1,3,6,1,4,1,29601,2,33,1,1,2};
UINT4 FsMIArpMaxRetries [ ] ={1,3,6,1,4,1,29601,2,33,1,1,3};
UINT4 FsMIArpPendingEntryCount [ ] ={1,3,6,1,4,1,29601,2,33,1,1,4};
UINT4 FsMIArpCacheEntryCount [ ] ={1,3,6,1,4,1,29601,2,33,1,1,5};
UINT4 FsMIArpContextDebug [ ] ={1,3,6,1,4,1,29601,2,33,1,1,6};



tMbDbEntry fsmparMibEntry[]= {

{{12,FsMIArpCacheTimeout}, GetNextIndexFsMIArpTable, FsMIArpCacheTimeoutGet, FsMIArpCacheTimeoutSet, FsMIArpCacheTimeoutTest, FsMIArpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIArpTableINDEX, 1, 0, 0, "300"},

{{12,FsMIArpCachePendTime}, GetNextIndexFsMIArpTable, FsMIArpCachePendTimeGet, FsMIArpCachePendTimeSet, FsMIArpCachePendTimeTest, FsMIArpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIArpTableINDEX, 1, 0, 0, "30"},

{{12,FsMIArpMaxRetries}, GetNextIndexFsMIArpTable, FsMIArpMaxRetriesGet, FsMIArpMaxRetriesSet, FsMIArpMaxRetriesTest, FsMIArpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIArpTableINDEX, 1, 0, 0, "3"},

{{12,FsMIArpPendingEntryCount}, GetNextIndexFsMIArpTable, FsMIArpPendingEntryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIArpTableINDEX, 1, 0, 0, NULL},

{{12,FsMIArpCacheEntryCount}, GetNextIndexFsMIArpTable, FsMIArpCacheEntryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIArpTableINDEX, 1, 0, 0, NULL},

{{12,FsMIArpContextDebug}, GetNextIndexFsMIArpTable, FsMIArpContextDebugGet, FsMIArpContextDebugSet, FsMIArpContextDebugTest, FsMIArpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIArpTableINDEX, 1, 0, 0, "0"},
};

tMibData fsmparEntry = { sizeof(fsmparMibEntry) / sizeof(fsmparMibEntry[0]), fsmparMibEntry};

#endif /* _FSMPARDB_H */

