/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arpinc.h,v 1.14 2015/09/29 10:34:54 siva Exp $
 *
 * Description: This file contains the common includes of ARP
 *              module     
 *
 *******************************************************************/
#ifndef _ARP_INC_H
#define _ARP_INC_H

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "rtm.h"
#include "ipcli.h"
#include "iss.h"
#include "arp.h"
#include "vcm.h"
#include "rmgr.h"
#include "arptypes.h"
#include "arpport.h"
#include "trieapif.h"
#include "snmctdfs.h"
#include "arpproto.h"
#include "snmccons.h"
#include "arpextn.h"
#include "arptrace.h"
#include "fsarplw.h"
#include "fssyslog.h"

#ifdef NPAPI_WANTED
#include "ipnp.h"
#include "rportnp.h"
#include "arpnpwr.h"
#endif /*NPAPI_WANTED*/

#ifdef ICCH_WANTED
#include "icch.h"
#include "arpicch.h"
#include "la.h"
#endif /*ICCH_WANTED*/

#ifdef VRRP_WANTED
#include "vrrp.h"
#endif

#ifdef DHCPC_WANTED
#include "dhcp.h"
#endif

#ifdef LNXIP4_WANTED
#include "fssocket.h"

#ifdef LINUX_ARP_NETLINK
#include "arpntlnk.h"
#endif /* LINUX_ARP_NETLINK */
#endif /* LNXIP4_WANTED */

#ifdef SNMP_2_WANTED
#include "fsarpwr.h"
#include "fsmparwr.h"
#endif
#include "arpsz.h"
#endif /* _ARP_INC_H */
