/* $Id: arpsz.h,v 1.6 2017/12/14 10:25:28 siva Exp $ */
enum {
    MAX_ARP_CONTEXTS_SIZING_ID,
    MAX_ARP_ENTRIES_SIZING_ID,
    MAX_ARP_PKT_QUE_SIZE_SIZING_ID,
    MAX_ARP_RM_QUE_SIZE_SIZING_ID,
    MAX_ARP_DYN_MSG_SIZE_SIZING_ID,
    MAX_ARP_ENET_ENTRIES_SIZING_ID,
#ifdef ICCH_WANTED
    MAX_ARP_ICCH_QUE_SIZE_SIZING_ID,
#endif
    MAX_ARP_VLAN_QUE_SIZE_SIZING_ID,
    ARP_MAX_SIZING_ID
};


#ifdef  _ARPSZ_C
tMemPoolId ARPMemPoolIds[ ARP_MAX_SIZING_ID];
INT4  ArpSizingMemCreateMemPools(VOID);
VOID  ArpSizingMemDeleteMemPools(VOID);
INT4  ArpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _ARPSZ_C  */
extern tMemPoolId ARPMemPoolIds[ ];
extern INT4  ArpSizingMemCreateMemPools(VOID);
extern VOID  ArpSizingMemDeleteMemPools(VOID);
extern INT4  ArpSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _ARPSZ_C  */


#ifdef  _ARPSZ_C
tFsModSizingParams FsARPSizingParams [] = {
{ "tArpCxt", "MAX_ARP_CONTEXTS", sizeof(tArpCxt),MAX_ARP_CONTEXTS, MAX_ARP_CONTEXTS,0 },
{ "t_ARP_CACHE", "MAX_ARP_ENTRIES", sizeof(t_ARP_CACHE),MAX_ARP_ENTRIES, MAX_ARP_ENTRIES,0 },
{ "tArpQMsg", "MAX_ARP_PKT_QUE_SIZE", sizeof(tArpQMsg),MAX_ARP_PKT_QUE_SIZE, MAX_ARP_PKT_QUE_SIZE,0 },
{ "tArpRmMsg", "MAX_ARP_RM_QUE_SIZE", sizeof(tArpRmMsg),MAX_ARP_RM_QUE_SIZE, MAX_ARP_RM_QUE_SIZE,0 },
{ "tArpRedTable", "MAX_ARP_DYN_MSG_SIZE", sizeof(tArpRedTable),MAX_ARP_DYN_MSG_SIZE, MAX_ARP_DYN_MSG_SIZE,0 },
{ "tArpEthernetEntry", "MAX_ARP_ENET_ENTRIES", sizeof(tArpEthernetEntry),MAX_ARP_ENET_ENTRIES, MAX_ARP_ENET_ENTRIES,0 },
#ifdef ICCH_WANTED
{ "tArpIcchData", "MAX_ARP_ICCH_QUE_SIZE", sizeof(tArpIcchData),MAX_ARP_ICCH_QUE_SIZE, MAX_ARP_ICCH_QUE_SIZE,0 },
#endif
{ "tArpQFlushMac", "MAX_ARP_PKT_QUE_SIZE", sizeof(tArpQFlushMac),MAX_ARP_VLAN_QUE_SIZE,MAX_ARP_VLAN_QUE_SIZE,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _ARPSZ_C  */
extern tFsModSizingParams FsARPSizingParams [];
#endif /*  _ARPSZ_C  */


