/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsarplw.h,v 1.7 2015/09/15 06:46:24 siva Exp $
 *
 * Description: Proto types for ARP Low Level Routines
 *******************************************************************/
#ifndef _FSARPLW_H_
#define _FSARPLW_H_

/* Proto type for Low Level GET Routine All Objects.  */
INT1
nmhGetFsArpCacheTimeout ARG_LIST((INT4 *));

INT1
nmhGetFsArpCachePendTime ARG_LIST((INT4 *));

INT1
nmhGetFsArpMaxRetries ARG_LIST((INT4 *));

INT1
nmhGetFsArpPendingEntryCount ARG_LIST((INT4 *));

INT1
nmhGetFsArpCacheEntryCount ARG_LIST((INT4 *));

INT1
nmhGetFsArpRedEntryTime ARG_LIST((INT4 *));

INT1
nmhGetFsArpRedExitTime ARG_LIST((INT4 *));

INT1
nmhGetFsArpCacheFlushStatus ARG_LIST((INT4 *));

INT1
nmhGetFsArpGlobalDebug ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsArpCacheTimeout ARG_LIST((INT4 ));

INT1
nmhSetFsArpCachePendTime ARG_LIST((INT4 ));

INT1
nmhSetFsArpMaxRetries ARG_LIST((INT4 ));

INT1
nmhSetFsArpCacheFlushStatus ARG_LIST((INT4 ,UINT4));

INT1
nmhSetFsArpGlobalDebug ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */
INT1
nmhTestv2FsArpCacheTimeout ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsArpCachePendTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsArpMaxRetries ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsArpCacheFlushStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsArpGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsArpCacheTimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsArpCachePendTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsArpMaxRetries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsArpCacheFlushStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsArpGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

#endif
