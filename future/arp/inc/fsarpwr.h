/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsarpwr.h,v 1.7 2015/09/15 06:46:24 siva Exp $
 *
 * Description: Proto types for ARP wrapper Routines
 *******************************************************************/
#ifndef _FSARPWR_H
#define _FSARPWR_H

VOID RegisterFSARP(VOID);
INT4 FsArpCacheTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsArpCachePendTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsArpMaxRetriesGet(tSnmpIndex *, tRetVal *);
INT4 FsArpPendingEntryCountGet(tSnmpIndex *, tRetVal *);
INT4 FsArpRedEntryTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsArpRedExitTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsArpCacheEntryCountGet(tSnmpIndex *, tRetVal *);
INT4 FsArpCacheTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsArpCachePendTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsArpMaxRetriesSet(tSnmpIndex *, tRetVal *);
INT4 FsArpCacheTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsArpCachePendTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsArpMaxRetriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsArpCacheTimeoutDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsArpCachePendTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsArpMaxRetriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsArpCacheFlushStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsArpGlobalDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsArpCacheFlushStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsArpGlobalDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsArpCacheFlushStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsArpGlobalDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsArpCacheFlushStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsArpGlobalDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSARPWR_H */
