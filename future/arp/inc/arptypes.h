/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arptypes.h,v 1.24 2016/10/03 10:34:38 siva Exp $
 *
 * Description: The header file for ARP module
 *
 *******************************************************************/
#ifndef   __ARPTYPES_H__
#define   __ARPTYPES_H__

/* DEFINITIONS */
/* ----------- */
#define   ARP_X25_TYPE               2
#define   ARP_PPP_TYPE               4
#define   ARP_UNDEFINED_TYPE         5

#define   ARP_BCAST                  1
#define   ARP_UCAST                  4
#define   ARP_HASH_TAB_SIZE         16

#define ARP_SIZING_CONTEXT_COUNT    FsARPSizingParams[MAX_ARP_CONTEXTS_SIZING_ID].u4PreAllocatedUnits

/* State of a Cache entry */
#define   ARP_DISABLE                6
#define   IP_ARP_HDR_LEN            28

/* Return values from ArpGetValidEntry to be used by IP iface */
#define   BAD_ENTRY                  2  /* If hardware type is not supported */
#define   ARP_RETRY_LIMIT            3  /* No more requests for this entry   */

/* ARP cache timeout  */
#define   ARP_MIN_CACHE_TIMEOUT       30
#define   ARP_MAX_CACHE_TIMEOUT    86400
#define   ARP_DEF_CACHE_TIMEOUT      300

/* Timeout for pending entries */
#define   ARP_MIN_PEND_TIME      30
#define   ARP_MAX_PEND_TIME    3000
#define   ARP_DEF_PEND_TIME      30

/* Retries for ARP request */
#define   ARP_MIN_REQ_RETRIES     2
#define   ARP_MAX_REQ_RETRIES    10

/*
 * For each of the hardware types (interface types) that support
 * ARP protocol, there will be an entry in this table.
 * This is done by calling arp_add_hardware routine.
 * This makes core ARP software as hardware independent.
 */
#define   ARP_HW_FREE            0
#define   ARP_HW_VALID           1

#define   ARP_ENET_SNAP_ENCAP    6
#define   MAX_ARP_TYPES          11

#define   ARP_PROTOCOL                    0x0806

/* MACROS */
/* ------ */
/*  Default size + Address lengths  */
#define  ARP_HDR_SIZE(i1Hwalen,i1Palen)\
         (8+(2*i1Hwalen)+(2*i1Palen))

#define   ARP_MAX_BYTES(u4Bytes, max)  \
          (((u4Bytes) <= max) ? (u4Bytes) : (max))

#ifdef L2RED_WANTED
#define ARP_GET_NODE_STATUS()  gArpRedGlobalInfo.u1NodeStatus
#else
#define ARP_GET_NODE_STATUS()  RM_ACTIVE
#endif
 
#define   ARP_AUDIT_SHOW_CMD    "end;show ip arp > "
#define   ARP_CLI_MAX_GROUPS_LINE_LEN    200
#define   ARP_CLI_EOF                  2
#define   ARP_CLI_NO_EOF                 1
#define   ARP_CLI_RDONLY               OSIX_FILE_RO
#define   ARP_CLI_WRONLY               OSIX_FILE_WO


#define ARP_AUDIT_FILE_ACTIVE "/tmp/arp_output_file_active"
#define ARP_AUDIT_FILE_STDBY "/tmp/arp_output_file_stdby"

#ifdef L2RED_WANTED
#define ARP_GET_RMNODE_STATUS()  RmGetNodeState ()
#else
#define ARP_GET_RMNODE_STATUS()  RM_ACTIVE
#endif

#define   ARP_RB_LESSER               -1
#define   ARP_RB_GREATER               1
#define   ARP_RB_EQUAL                 0

#define   ARP_ZERO                     0
#define   ARP_ONE                      1

#define ARP_DEF_NET_MASK             0xffffffff
                                             
/* DATA STRUCTURES */
/* --------------- */
typedef struct _StandardArpHdr
{
    UINT2  u2HwType;
    UINT2  u2ProtType;
    UINT1  u1HwaLen;
    UINT1  u1PaLen;
    UINT2  u2Opcode;
} tArpHeader;

/*
 * Mapping between CRU H/w types and ARP H/w types constants.
 */
typedef struct
{
    INT2  i2CruType;
    INT2  i2ArpType;
} t_ARP_CRU_TYPE;

typedef struct _ArpRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tArpRmCtrlMsg;
 
typedef struct _ArpRedTable {
    tRBNodeEmbd  RbNode;  /* RbNode for the cache entry */
    UINT4        u4IpAddr; /* Ip address */
    UINT4        u4IfIndex;  /* IfIndex for this Arp entry */
    UINT2        u2Port;  /* IP port number of the Interface */
    INT2         i2IntfType; /* Type of this interface */
    INT1         i1HwAddr[CFA_ENET_ADDR_LEN]; /* Hardware address for the IP */
    UINT1        u1RowStatus; /* Row status */
    UINT1        u1Action; /* action to be performed on the entry. Whether
                             to add or delete the entry */
    UINT1        u1HwStatus; /* Hardware status - Present in NP or not */
    UINT1        u1DelFlag; /* delete flag - to delete the entry or not */
    INT1         i1State; /* status of this entry 
                             ARP_DYNAMIC
                             ARP_AGEOUT
                             ARP_PENDING_*/
    UINT1        au1Pad[1];
    UINT4        u4LastUpdatedtime;
}tArpRedTable;

typedef struct _ArpRmMsg
{
     tArpRmCtrlMsg RmCtrlMsg; /* Control message from RM */
} tArpRmMsg;

typedef struct _ArpHACacheMarker
{
    UINT4    u4IfIndex;
    UINT4    u4IpAddr;
}tArpHACacheMarker;

typedef struct _sArpRedGlobalInfo{
    tArpHACacheMarker   ArpHACacheMarker;/* Holds the keys for the ArpTable
                                           * entry which is marked for batch
                                           * processing module.*/

    INT4                i4ArpRedEntryTime; /* Time stamp when arp switchover
                                            * from active to standby starts */
    INT4                i4ArpRedExitTime;  /* Time stamp when arp switchover
                                              from active to standby is over */
    UINT1               u1BulkUpdStatus;/* bulk update status
                                         * ARP_HA_UPD_NOT_STARTED 0
                                         * ARP_HA_UPD_COMPLETED 1
                                         * ARP_HA_UPD_IN_PROGRESS 2,
                                         * ARP_HA_UPD_ABORTED 3 */
    UINT1   u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
    UINT1   u1NumPeersUp;     /* Indicates number of standby nodes 
                                 that are up. */
    UINT1   bBulkReqRcvd;     /* To check whether bulk request recieved 
                                 from standby before RM_STANDBY_UP event. */
    UINT1   u1RedStatus;      /* Red status whether redundancy enabled or not */
    UINT1   u1Pad[3];
}tArpRedGlobalInfo;

typedef struct _ArpNpInParams
{
    UINT4       u4ContextId;
    UINT4       u4IpAddr;
}tArpNpInParams;

typedef struct _ArpNpOutParams
{
    UINT4       u4ContextId;
    UINT4       u4IpAddr;
    UINT4       u4CfaIndex;
    INT1        HwAddress[CFA_ENET_ADDR_LEN];
    INT1        i1Pad[2];
}tArpNpOutParams;


#ifndef ARP_ARRAY_TO_RBTREE_WANTED

#define ARP_MAX_ENET_ENTRIES                1

#define ARP_CREATE_ENET_TABLE()           ArpUtilRBTreeCreateStub () 

#define ARP_DELETE_ENET_TABLE()  

#define ARP_CREATE_ENET_ENTRIES()                                               \
        MEMSET (gArpGblInfo.aEnet, ARP_ZERO,                                    \
                    IPIF_MAX_LOGICAL_IFACES * sizeof (tArpEthernetEntry))

#define ARP_GET_ENET_ENTRY(u2Port)  (&(gArpGblInfo.aEnet[u2Port]))

#define ARP_DELETE_ENET_ENTRIES()

#define ARP_MAX_SIZING_ENET_ENTRIES         IPIF_MAX_LOGICAL_IFACES

#else  /* ARP_ARRAY_TO_RBTREE_WANTED */

#define ARP_MAX_ENET_ENTRIES               (IPIF_MAX_LOGICAL_IFACES  + 1)

#define ARP_CREATE_ENET_TABLE()            ArpCreateArpEnetTable () 

#define ARP_DELETE_ENET_TABLE()            ArpDeleteArpEnetTable () 

#define ARP_CREATE_ENET_ENTRIES()          ArpCreateAllArpEnetTableEntries ()

#define ARP_GET_ENET_ENTRY(u2Port)         ArpGetArpEnetTableEntry (u2Port) 

#define ARP_DELETE_ENET_ENTRIES()          ArpDelAllArpEnetTableEntries ()

#define ARP_MAX_SIZING_ENET_ENTRIES       ((FsARPSizingParams[MAX_ARP_ENET_ENTRIES_SIZING_ID].u4PreAllocatedUnits) - 1) 

#endif


#endif          /*__IP_ARP_H__*/
