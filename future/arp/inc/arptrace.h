/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arptrace.h,v 1.7 2015/09/15 06:46:24 siva Exp $
 *
 * Description: ARP Trace macros 
 *
 *******************************************************************/

#ifndef _ARP_TRACE_H
#define _ARP_TRACE_H

extern UINT4 gu4ArpDbg;

#ifdef TRACE_WANTED

#define   ARP_DBG_FLAG(Cxt)   ArpGetDebugFlag (Cxt)
#define   ARP_NAME      "ARP"    
#if MBSM_WANTED
#define   MBSM_IP       "MbsmIp"   
#endif

#define   ARP_MOD_TRC      0x00100000    /* includes ARP,RARP,INARP */
#define   CONTEXT_INFO(Fmt, Cxt)  " Context - %d :" Fmt, Cxt

 /* General Macros */

#define ARP_PKT_DUMP(Cxt, cmod, mask,  mod, pBuf, Length, fmt) \
{\
 UtlTrcLog(ARP_DBG_FLAG (Cxt), mask , mod, CONTEXT_INFO (fmt, Cxt)); \
  if ((ARP_DBG_FLAG (Cxt) & mask) == DUMP_TRC)\
   DumpPkt(pBuf, Length); \
}


#define ARP_TRC(Cxt, cmod, mask,mod,fmt) {\
                     UtlTrcLog(ARP_DBG_FLAG (Cxt), \
                                  mask,               \
                                  mod,         \
                                  CONTEXT_INFO (fmt, Cxt));\
                }

#define ARP_TRC_ARG1(Cxt, cmod,  mask,  mod, fmt, arg1) \
            {\
           UtlTrcLog(ARP_DBG_FLAG (Cxt) ,    \
            mask,                \
            mod,            \
            CONTEXT_INFO (fmt, Cxt), \
            arg1); \
              }

#define ARP_TRC_ARG2(Cxt, cmod, mask, mod, fmt,arg1,arg2) \
            {\
            UtlTrcLog(ARP_DBG_FLAG (Cxt),    \
            mask,                \
            mod,            \
            CONTEXT_INFO (fmt, Cxt), \
            arg1,                \
            arg2);\
             }

#define ARP_TRC_ARG3(Cxt, cmod, mask, mod, fmt,arg1,arg2,arg3) \
            {\
            UtlTrcLog(ARP_DBG_FLAG (Cxt),    \
            mask,                \
            mod,            \
            CONTEXT_INFO (fmt, Cxt), \
            arg1,                \
            arg2,                \
            arg3);\
             }

#define ARP_TRC_ARG4(Cxt, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4) \
            {\
            UtlTrcLog(ARP_DBG_FLAG (Cxt),    \
            mask,                \
            mod,            \
            CONTEXT_INFO (fmt, Cxt), \
            arg1,                \
            arg2,                \
            arg3,                \
            arg4);\
             }

#define ARP_TRC_ARG5(Cxt, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5) \
            {\
            UtlTrcLog(ARP_DBG_FLAG (Cxt),    \
            mask,                \
            mod,            \
            CONTEXT_INFO (fmt, Cxt), \
            arg1,                \
            arg2,                \
            arg3,                \
            arg4,                \
            arg5);\
             }

#define ARP_TRC_ARG6(Cxt, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5,arg6) \
                {\
            UtlTrcLog(ARP_DBG_FLAG (Cxt),    \
            mask,                \
            mod,            \
            CONTEXT_INFO (fmt, Cxt), \
            arg1,                \
            arg2,                \
            arg3,                \
            arg4,                \
            arg5,                \
            arg6);\
             }
#define ARP_GBL_TRC(cmod, mask,mod,fmt) {\
                     UtlTrcLog(ARP_DBG_FLAG (ARP_DEFAULT_CONTEXT), \
                                  mask,               \
                                  mod,         \
                                  fmt);  \
                }

#define ARP_GBL_TRC_ARG1(cmod,  mask,  mod, fmt, arg1) \
            {\
            UtlTrcLog(ARP_DBG_FLAG (ARP_DEFAULT_CONTEXT) ,    \
            mask,                \
            mod,            \
            fmt,  \
            arg1); \
              }

#else
#define ARP_NAME
#define ARP_MOD_TRC     
#define ARP_PKT_DUMP(Cxt, cmod, mask,  mod, pBuf, Length, fmt) 
#define ARP_TRC(Cxt, cmod, mask,mod,fmt) 
#define ARP_TRC_ARG1(Cxt, cmod, mask, mod, fmt,arg1)
#define ARP_TRC_ARG2(Cxt, cmod, mask, mod, fmt,arg1,arg2)
#define ARP_TRC_ARG3(Cxt, cmod, mask, mod, fmt,arg1,arg2,arg3)
#define ARP_TRC_ARG4(Cxt, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4)
#define ARP_TRC_ARG5(Cxt, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5)
#define ARP_TRC_ARG6(Cxt, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5,arg6)
#define ARP_GBL_TRC(cmod, mask,mod,fmt)
#define ARP_GBL_TRC_ARG1(cmod,  mask,  mod, fmt, arg1)

#endif /* TRACE_WANTED */
#endif /* _ARP_TRACE_H */
