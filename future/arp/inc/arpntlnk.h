/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arpntlnk.h,v 1.6 2014/11/20 10:58:18 siva Exp $
 *
 * Description:This file contains porting related to linux
 *             defintions used in ARP NETLINK module.               
 *
 *******************************************************************/
#ifndef _ARPNTLNK_H
#define _ARPNTLNK_H

#include "fssocket.h"


INT4 ArpNetlinkInformationFetch (struct nlmsghdr *);

INT4 ArpNetlinkNeighChange (struct nlmsghdr *);

VOID ArpNetlinkParseRtattr (struct rtattr **tb, INT4 max, struct rtattr *rta, INT4 len);

INT4 ArpNetLinkInit (VOID);

INT4 ArpNetlinkParseInfo (INT4 (*filter) (struct nlmsghdr *),INT4 i4SockId);

#endif /* _ARPNTLNK_H */
