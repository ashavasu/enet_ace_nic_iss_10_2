/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: arpred.h,v 1.5 2018/02/19 09:56:22 siva Exp $
 *
 * Description: This file contains all macro definitions and 
 *              function prototypes for ARP Server module.
 *              
 *******************************************************************/
#ifndef __ARP_RED_H
#define __ARP_RED_H

#include "arpinc.h"

/* Represents the message types encoded in the update messages */
typedef enum {
    ARP_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
    ARP_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG,
    ARP_RED_BULK_CACHE_INFO,
    ARP_RED_DYN_CACHE_INFO,
    ARP_RED_CACHE_INFO,
    ARP_RED_DYN_CACHE_TIME_INFO
}eArpRedRmMsgType;

typedef enum{
    ARP_HA_UPD_NOT_STARTED = 1,/* 1 */
    ARP_HA_UPD_IN_PROGRESS,    /* 2 */
    ARP_HA_UPD_COMPLETED,      /* 3 */
    ARP_HA_UPD_ABORTED,        /* 4 */
    ARP_HA_MAX_BLK_UPD_STATUS
} eArpHaBulkUpdStatus;

VOID ArpRedHandleDynSyncAudit (VOID);
VOID ArpExecuteCmdAndCalculateChkSum  (VOID);


/* Macro Definitions for ARP Server Redundancy */


#define ARP_RM_GET_NUM_STANDBY_NODES_UP() \
          gArpRedGlobalInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define ARP_NUM_STANDBY_NODES() gArpRedGlobalInfo.u1NumPeersUp

#define ARP_RM_BULK_REQ_RCVD() gArpRedGlobalInfo.bBulkReqRcvd

#define ARP_RED_MSG_SIZE(pInfo) \
        ((pInfo->u1Action == ARP_RED_ADD_CACHE) ?  \
        ARP_RED_DYN_INFO_SIZE : 4 + 4)

#define ARP_IS_STANDBY_UP() \
          ((gArpRedGlobalInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)

/* RM wanted */

#define ARP_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define ARP_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define ARP_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define ARP_RM_PUT_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
    RM_COPY_TO_OFFSET (pMsg, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size;\
}while (0)

#define ARP_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define ARP_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define ARP_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define ARP_RM_GET_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
        RM_GET_DATA_N_BYTE (pMsg, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)

#define ARP_RED_MAX_MSG_SIZE        1500
#define ARP_RED_TYPE_FIELD_SIZE     1
#define ARP_RED_LEN_FIELD_SIZE      2
#define ARP_RED_MIM_MSG_SIZE        (1 + 2 + 2)
#define ARP_RED_DYN_INFO_SIZE       (6 + 4 + 2 + 2 + 1 + 1 + 4 /*u4LastUpdatedtime*/)
#define ARP_RED_DYN_INFO_TIME_SIZE  (4 + 2 + 4)

#define ARP_ONE_BYTE                1
#define ARP_TWO_BYTES               2
#define ARP_FOUR_BYTES              4

#define ARP_RED_BULK_UPD_TAIL_MSG_SIZE       3
#define ARP_RED_BULK_REQ_MSG_SIZE            3

#define ARP_RED_BULQ_REQ_SIZE       3

#define ARP_RED_ADD_CACHE            1
#define ARP_RED_DEL_CACHE            2

#define ARP_RED_CACHE_DEL         1
#define ARP_RED_CACHE_DONT_DEL    0

/* Function prototypes for ARP Redundancy */

#endif /* __ARP_RED_H */
