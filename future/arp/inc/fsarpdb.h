/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsarpdb.h,v 1.7 2015/09/15 06:46:24 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSARPDB_H
#define _FSARPDB_H


UINT4 fsarp [] ={1,3,6,1,4,1,2076,109};
tSNMP_OID_TYPE fsarpOID = {8, fsarp};


UINT4 FsArpCacheTimeout [ ] ={1,3,6,1,4,1,2076,109,1,1};
UINT4 FsArpCachePendTime [ ] ={1,3,6,1,4,1,2076,109,1,2};
UINT4 FsArpMaxRetries [ ] ={1,3,6,1,4,1,2076,109,1,3};
UINT4 FsArpPendingEntryCount [ ] ={1,3,6,1,4,1,2076,109,2,1};
UINT4 FsArpCacheEntryCount [ ] ={1,3,6,1,4,1,2076,109,2,2};
UINT4 FsArpRedEntryTime [ ] ={1,3,6,1,4,1,2076,109,2,3};
UINT4 FsArpRedExitTime [ ] ={1,3,6,1,4,1,2076,109,2,4};
UINT4 FsArpCacheFlushStatus [ ] ={1,3,6,1,4,1,2076,109,2,5};
UINT4 FsArpGlobalDebug [ ] ={1,3,6,1,4,1,2076,109,2,6};




tMbDbEntry fsarpMibEntry[]= {

{{10,FsArpCacheTimeout}, NULL, FsArpCacheTimeoutGet, FsArpCacheTimeoutSet, FsArpCacheTimeoutTest, FsArpCacheTimeoutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "7200"},

{{10,FsArpCachePendTime}, NULL, FsArpCachePendTimeGet, FsArpCachePendTimeSet, FsArpCachePendTimeTest, FsArpCachePendTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "30"},

{{10,FsArpMaxRetries}, NULL, FsArpMaxRetriesGet, FsArpMaxRetriesSet, FsArpMaxRetriesTest, FsArpMaxRetriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{10,FsArpPendingEntryCount}, NULL, FsArpPendingEntryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsArpCacheEntryCount}, NULL, FsArpCacheEntryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsArpRedEntryTime}, NULL, FsArpRedEntryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsArpRedExitTime}, NULL, FsArpRedExitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsArpCacheFlushStatus}, NULL, FsArpCacheFlushStatusGet, FsArpCacheFlushStatusSet, FsArpCacheFlushStatusTest, FsArpCacheFlushStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsArpGlobalDebug}, NULL, FsArpGlobalDebugGet, FsArpGlobalDebugSet, FsArpGlobalDebugTest, FsArpGlobalDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
};

tMibData fsarpEntry = { sizeof(fsarpMibEntry) / sizeof(fsarpMibEntry[0]), fsarpMibEntry };

#endif /* _FSARPDB_H */

