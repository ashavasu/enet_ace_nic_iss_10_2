/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arpextn.h,v 1.11 2014/06/30 12:14:12 siva Exp $
 *
 * Description: This file contains the extern definitions of ARP
 *              module
 *
 *******************************************************************/
#ifndef _ARP_EXTN_H
#define _ARP_EXTN_H

extern t_ARP_CRU_TYPE      Arp_Cru_Type[MAX_ARP_TYPES];

extern t_ARP_HW_TABLE     *(*arp_get_hw_entry) PROTO ((INT2 i2Hardware)),
    *(*arp_get_free_hw_entry) PROTO ((INT2 i2Hardware));

#ifdef LINUX_ARP_NETLINK
extern INT4                gi4NetlinkSock;
#endif

extern tArpGblInfo         gArpGblInfo;

extern tArpRedGlobalInfo   gArpRedGlobalInfo;
extern INT1                gi4ArpSysLogId;
extern UINT4               gu4IpIfaces[IPIF_MAX_LOGICAL_IFACES + 1];

extern tArpRegTbl    gaArpRegTbl[ARP_MAX_REG_MODULES];

extern UINT4 IssSzGetSizingParamsForModule (CHR1 * pu1ModName, CHR1 * pu1MacroName);
#endif /* _ARP_EXTN_H */
