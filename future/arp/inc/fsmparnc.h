
#ifndef _H_i_ARICENT_MIARP_MIB
#define _H_i_ARICENT_MIARP_MIB
/* $Id: fsmparnc.h,v 1.1 2015/12/29 12:05:06 siva Exp $
    ISS Wrapper header
    module ARICENT-MIARP-MIB

 */
/********************************************************************
* FUNCTION NcFsMIArpCacheTimeoutSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIArpCacheTimeoutSet (
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpCacheTimeout );

/********************************************************************
* FUNCTION NcFsMIArpCacheTimeoutTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIArpCacheTimeoutTest (UINT4 *pu4Error,
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpCacheTimeout );

/********************************************************************
* FUNCTION NcFsMIArpCachePendTimeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIArpCachePendTimeSet (
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpCachePendTime );

/********************************************************************
* FUNCTION NcFsMIArpCachePendTimeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIArpCachePendTimeTest (UINT4 *pu4Error,
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpCachePendTime );

/********************************************************************
* FUNCTION NcFsMIArpMaxRetriesSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIArpMaxRetriesSet (
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpMaxRetries );

/********************************************************************
* FUNCTION NcFsMIArpMaxRetriesTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIArpMaxRetriesTest (UINT4 *pu4Error,
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpMaxRetries );

/********************************************************************
* FUNCTION NcFsMIArpPendingEntryCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIArpPendingEntryCountGet (
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 *pi4FsMIArpPendingEntryCount );

/********************************************************************
* FUNCTION NcFsMIArpCacheEntryCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMIArpCacheEntryCountGet (
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 *pi4FsMIArpCacheEntryCount );

/* END i_ARICENT_MIARP_MIB.c */


#endif
