/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmparlw.h,v 1.4 2015/09/15 06:46:24 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIArpTable. */
INT1
nmhValidateIndexInstanceFsMIArpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIArpTable  */

INT1
nmhGetFirstIndexFsMIArpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIArpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIArpCacheTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIArpCachePendTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIArpMaxRetries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIArpPendingEntryCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIArpCacheEntryCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIArpRedEntryTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIArpRedExitTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIArpContextDebug ARG_LIST((INT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIArpCacheTimeout ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIArpCachePendTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIArpMaxRetries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIArpContextDebug ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIArpCacheTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIArpCachePendTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIArpMaxRetries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIArpContextDebug ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIArpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
