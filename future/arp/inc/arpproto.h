/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arpproto.h,v 1.34 2016/05/26 10:25:34 siva Exp $
 *
 * Description: Contains Prototypes required for IP/RIP module.
 *
 *******************************************************************/
#ifndef _ARPPROTO_H_
#define _ARPPROTO_H_

INT4 arp_extract_hdr PROTO ((t_ARP_PKT * pArp_pkt,
   tCRU_BUF_CHAIN_HEADER * pBuf));

VOID arp_put_hdr PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, t_ARP_PKT * pArp_pkt));
#ifdef IP_WANTED
INT4 ArpCheckToSendProxyArpReply PROTO ((UINT4 u4ContextId, 
                            UINT2 u2Port, UINT4 u4SrcIpAddr, UINT4 u4TgtIpAddr));

INT4 ArpCheckDestRouteForProxyArpReply (UINT4 u4ContextId, UINT4 u4IpAddr, 
                           UINT2 u2Port, UINT1 u1QueryFlag,tNetIpv4RtInfo * NetIpRtInfo);
INT4
ArpBroadcastCheckforProxyArp (UINT4 u4TgtIpAddr, UINT4 u4DestMask);
#endif

INT4
ArpCliGetShowCmdOutputToFile (UINT1 *);

INT4
ArpCliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
ArpCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);


INT4
ArpGetShowCmdOutputAndCalcChkSum (UINT2 *);

INT4 ArpProcessQMsg PROTO ((tArpQMsg *pArpQMsg));
 
INT4 ArpSendRequest PROTO ((UINT2 u2Port,
                              UINT4 u4Dest,
                              INT2 i2Hardware, UINT1 u1EncapType));

VOID ArpCheckAndStartTimer PROTO ((t_ARP_CACHE *pArpCache, UINT4 u4Duration));

VOID ArpCheckAndSendReq PROTO ((INT1 i1State, UINT2 u2Port, UINT4 u4IpAddr,
                         INT2 i2Hardware, UINT1 u1Encaptype));
#if defined (LNXIP4_WANTED) && defined (LINUX_ARP_NETLINK) 
VOID ArpSendReqAndStartTimer (tARP_DATA tArpData);
#endif
INT4 arp_input PROTO ((tArpQMsg *));

VOID arp_init_hw_table PROTO ((VOID));
INT4 arp_add_hardware PROTO ((UINT1 u1IfaceType,
                              INT4 (*addr_fn)
                              PROTO (
                                     (UINT2 u2Port, INT1 * i1pAddr,
                                      INT1 * pHwalen, INT1 * pPalen)),
                              INT2 i2Protocol, INT4 (*pWrite) PROTO ((VOID))));

t_ARP_HW_TABLE *arp_direct_get_hw_entry PROTO ((INT2 i2Hardware));

VOID arp_drop PROTO ((VOID *pArg));

INT1 ArpGetValidEntryWithIndex PROTO ((UINT4, UINT4, INT1 *, UINT1 *));

VOID arp_timer_expiry_handler PROTO ((VOID));

INT4 ArpEnable PROTO ((VOID));

VOID ArpDisable PROTO ((VOID));

INT4 ArpTestForRouteInCxt PROTO ((UINT4, UINT4, UINT2));

VOID ArpActOnIfOperChg PROTO ((UINT4, UINT2, UINT4));

VOID ArpActOnIfDelete PROTO ((UINT4));

VOID  ArpActOnStaticRtChange PROTO ((UINT2 u2Port, UINT4 u4RtNet,
                                     UINT4 u4RtMask));
    
VOID
ArpHandleUcastRtChg PROTO ((tNetIpv4RtInfo * pRtChg, tNetIpv4RtInfo * pRtChg1, UINT1 u1BitMap));
    
VOID  ArpHandleOperStateChgForStaticArp PROTO ((t_ARP_CACHE  *pArpCache, 
                                    UINT4 u4Status, UINT1 *pu1IfName));

INT4 ArpGetNextArpEntry PROTO ((INT4 *pi4CfaIfIndex, UINT4 *pu4IpAddr ,
                                                    UINT1 *pMacAddr));
INT4 ArpNpDel (UINT4 u4ContextId, UINT4 u4IpAddr, 
               INT1 *i1pHwAddr, INT1 i1Hwalen, 
                                  UINT4 u4CfaIfIndex, INT1 i1State);

INT4 ArpNpAdd (UINT4 u4ContextId, UINT4 u4Tproto_addr,
               INT2 i2Hardware, INT1 *i1pHwAddr,
                INT1 i1Hwalen, UINT4 u4CfaIfIndex, UINT1 u1EncapType, 
                INT1 i1State);

INT4 ArpNpModify (UINT4 u4ContextId, 
                  UINT4 u4Tproto_addr, INT2 i2Hardware, INT1 *i1pHwAddr,
                INT1 i1Hwalen, UINT4 u4CfaIfIndex, UINT1 u1EncapType, 
                INT1 i1State);

INT4 ArpNpGetNext (tArpNpInParams ArpNpInParam, tArpNpOutParams *pArpNpOutParam);

INT4 ArpNpGet (tArpNpInParams ArpNpInParam, tArpNpOutParams *pArpNpOutParam);

VOID  ArpProcessRcvdIpPkt PROTO ((UINT2, tCRU_BUF_CHAIN_HEADER *));

VOID ARPProcessDataPktEvent PROTO ((VOID));

INT4 ArpReplaceArpCacheWithIndex PROTO ((UINT4, UINT4, t_ARP_CACHE *,
                                         tCRU_BUF_CHAIN_HEADER **, INT4));


INT4 ArpAddArpCache PROTO ((t_ARP_CACHE * pArpCacheNode));

INT4 ArpUpdateArpCacheStateWithIndex PROTO ((UINT4, UINT4, INT1));

VOID ArpIncArpCacheRetryWithIndex PROTO ((UINT4, UINT4));


INT4 ArpArpcacheLock PROTO ((VOID));

INT4 ArpArpcacheUnLock PROTO ((VOID));

INT4 ArpGetHwAddr PROTO ((UINT2 u2IfIndex, UINT1 *pHwAddr));

VOID ARP_Process_Pkt_Arrival_Event PROTO ((VOID));

VOID ArpIncMsrNotifyCfg (INT4 i4ContextId, UINT4 *pu4ObjectId, UINT4 u4OidLen, INT4 i4SetVal);

#ifdef LNXIP4_WANTED
INT4 ArpLnxInit PROTO ((VOID));
INT4 ArpSetLnxMaxRetires PROTO ((INT4 i4ArpMaxRetries));
INT4 ArpSetLnxArpCacheTimeout PROTO ((INT4 i4ArpCacheTimeout));
INT4 ArpVerifyIfAddrExists PROTO ((UINT2 u2Port));
#endif /* LNXIP4_WANTED */

INT4 ArpHandleContextCreate PROTO ((UINT4));
INT4 ArpHandleContextDelete PROTO ((UINT4));
INT4 ArpRBTreeCacheEntryCmp PROTO ((tRBElem *, tRBElem *));
INT4 ArpVcmIsVRExist PROTO ((UINT4));
UINT4 ArpGetDebugFlag PROTO ((UINT4 u4ContextId));

INT4  ArpRBTreeRedEntryCmp PROTO ((tRBElem *, tRBElem *));
INT4  ArpRedInitGlobalInfo PROTO ((VOID));
INT4  ArpRedRmRegisterProtocols PROTO ((tRmRegParams *pRmReg));
INT4  ArpRedRmDeRegisterProtocols PROTO ((VOID));
INT4  ArpRedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
VOID  ArpRedHandleRmEvents PROTO ((VOID));
VOID  ArpRedHandleGoActive PROTO ((VOID));
VOID  ArpRedHandleGoStandby PROTO ((tArpRmMsg * pMsg));
VOID  ArpRedHandleStandbyToActive PROTO ((VOID));
VOID  ArpRedHandleActiveToStandby PROTO ((VOID));
VOID  ArpRedHandleIdleToActive PROTO ((VOID));
VOID  ArpRedHandleIdleToStandby PROTO ((VOID));
VOID  ArpRedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
VOID  ArpRedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
UINT4 ArpRedRmHandleProtocolEvent PROTO ((tRmProtoEvt *pEvt));
INT4  ArpRedRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));
INT4  ArpRedSendMsgToRm PROTO ((tRmMsg * pMsg , UINT2 u2Length ));
VOID  ArpRedSendBulkReqMsg PROTO ((VOID));
INT4  ArpRedDeInitGlobalInfo PROTO ((VOID));
VOID  ArpRedSendBulkUpdTailMsg PROTO ((VOID));
VOID  ArpRedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT2 *pu2OffSet));
VOID  ArpRedAddDynamicInfo PROTO ((t_ARP_CACHE *pArpCache, UINT1 u1Operation));
VOID  ArpRedSendDynamicCacheInfo PROTO ((VOID));
VOID  ArpRedSendDynamicCacheTimeInfo PROTO ((t_ARP_CACHE *pArpCache));
VOID  ArpRedSendBulkUpdMsg PROTO ((VOID));
VOID  ArpRedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);
VOID  ArpRedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);
VOID  ArpRedProcessDynamicCacheTimeInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID  ArpRedHwAudit PROTO ((VOID));
INT4  ArpRedSendBulkCacheInfo (UINT1 *pu1BulkUpdPendFlg);
INT4  ArpStartTimers (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                      VOID *pArg, VOID *pOut);
INT4  ArpSendGratArpRequest (tArpQMsg * pArpQMsg);
INT4
ArpUtilRBTreeCreateStub PROTO ((VOID));

/*---------------------------------------------------------------*/
/*-----------------arprbutl.c---------------------------------------*/
/*---------------------------------------------------------------*/
INT4
ArpCreateArpEnetTable PROTO ((VOID));
VOID
ArpDeleteArpEnetTable PROTO ((VOID));
INT4
ArpEnetTableCmp PROTO ((tRBElem * Node,
                        tRBElem * NodeIn));
INT4
ArpAddArpEnetTableEntry PROTO ((UINT2 u2Port,
                               tArpEthernetEntry **ppArpEthernetEntry));
tArpEthernetEntry *
ArpGetArpEnetTableEntry PROTO ((UINT2 u2Port));
tArpEthernetEntry  *
ArpGetFirstArpEnetTableEntry PROTO ((VOID));
tArpEthernetEntry *
ArpGetNextArpEnetTableEntry PROTO ((UINT2 u2Port));
INT4
ArpDelArpEnetTableEntry PROTO ((tArpEthernetEntry *pArpEthernetEntry));
VOID
ArpDelAllArpEnetTableEntries PROTO ((VOID));
VOID
ArpCreateAllArpEnetTableEntries PROTO ((VOID));

VOID
ArpActOnL2IfDelete (UINT4 u4IfIndex, UINT4 u4PhyIfIndex);
/*-----------------arprbutl.c---------------------------------------*/

#endif
