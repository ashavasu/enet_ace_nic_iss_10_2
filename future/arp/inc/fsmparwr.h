/* $Id: fsmparwr.h,v 1.4 2015/09/15 06:46:24 siva Exp $ */
#ifndef _FSMPARWR_H
#define _FSMPARWR_H
INT4 GetNextIndexFsMIArpTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMPAR(VOID);

VOID UnRegisterFSMPAR(VOID);
INT4 FsMIArpCacheTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpCachePendTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpMaxRetriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpPendingEntryCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpCacheEntryCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpContextDebugGet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpRedEntryTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpRedExitTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpCacheTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpCachePendTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpMaxRetriesSet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpContextDebugSet(tSnmpIndex *, tRetVal *);
INT4 FsMIArpCacheTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIArpCachePendTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIArpMaxRetriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIArpContextDebugTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIArpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSMPARWR_H */
