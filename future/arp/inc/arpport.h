/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arpport.h,v 1.18 2017/12/14 10:25:28 siva Exp $
 *
 * Description: The OS Dependent header file for ARP module.
 *
 *******************************************************************/
#ifndef   __ARPPORT_H__
#define   __ARPPORT_H__


/* TYPE DEFINITIONS */
/* ---------------- */
typedef tTmrAppTimer tArpTimer;


/* DEFINITIONS */
/* ----------- */
#define   ARP_TMR_SUCCESS            TMR_SUCCESS
#define   ARP_TMR_FAILURE            TMR_FAILURE
#define   ARP_TIMER_DATA(pTimer)     (pTimer)->u4Data
#define   ARP_EVENT_WAIT_FLAGS       (OSIX_WAIT | OSIX_EV_ANY)

#define   ARP_CFA_MODULE_ID                      2 
#define   ARP_EVENT_WAIT_TIMEOUT                 0
#define   ARP_CACHE_TIMER_ID                     1
#define   ARP_REQUEST_RETRY_TIMER_ID             2

#define   ARP_CACHE_TIMER_EVENT         0x00000001
#define   ARP_PACKET_ARRIVAL_EVENT      0x00000002
#define   ARP_KERNEL_UPD_EVENT          0x00000004  
#define   ARP_RM_PKT_EVENT              0x00000008
#define   ARP_HA_PEND_BLKUPD_EVENT      0x00000010
#define   ARP_RED_TIMER_EVENT           0x00000020
#define   ARP_RED_START_TIMER_EVENT     0x00000040
#define   ARP_ICCH_PKT_EVENT            0x00000080 /*Event from ICCH module*/
#define   ARP_FLUSH_BY_PORT_EVENT       0x00000100

#define   ARP_ENET_PHYS_INTERFACE_TYPE           6
#define   ARP_NETLINK_MAX_NOTIFY                 1

#define   ARP_TASK_MODE         OSIX_DEFAULT_TASK_MODE
#define   ARP_TASK_PRIORITY          30
#define   ARP_TASK_STACK_SIZE    0x8000
#define   ARP_PACKET_ARRIVAL_Q_MODE      OSIX_GLOBAL
#define   ARP_PACKET_ARRIVAL_Q_DEPTH     5000
#define   ARP_IP_PKT_ARRIVAL_Q_DEPTH     5000
#define   ARP_NEW_PACKET_ARRIVAL_Q_DEPTH 5000
#define   ARP_RM_PKT_ARRIVAL_Q_DEPTH     1000
#define   ARP_ICCH_PKT_ARRIVAL_Q_DEPTH   1000
#define   tARP_INTERFACE                  tCRU_INTERFACE
#define   ARP_FRMRL_TYPE            15
#define   ARP_MAX_ENCAPS_ADDRESS_LENGTH   22

#define   ARP_TASK_NAME                  "ARP"
#define   ARP_PACKET_ARRIVAL_Q_NAME      "ARPQ"
#define   ARP_IP_PKT_ARRIVAL_Q_NAME      "ARIQ"
#define   ARP_NEW_PACKET_ARRIVAL_Q_NAME  "ARIPoQ"
#define   ARP_RM_PKT_ARRIVAL_Q_NAME      "ARPRMQ"
#define   ARP_ICCH_PKT_ARRIVAL_Q_NAME    "ARPICQ"

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
#define   ARP_VRF_IP_PKT_ARRIVAL_Q_NAME  "DYNARPVRF"
#endif

  /* ARPCACHE TABLE Semaphore related Macros */
#define ARP_CACHE_SEMAPHORE       ((UINT1 *)"ARDS")
#define ARP_PROTOCOL_SEMAPHORE    ((UINT1 *)"ARPS")

#define MAX_NETLINK_BUF_LEN 4096
#define   ARP_PROTOCOL_ADDR_LEN                 4
#define   HIWORD(u4Dword)   (u4Dword >> 16)
#define   LOWORD(u4Dword)   (u4Dword & 0xffff)


#define  ARP_DS_LOCK     ArpArpcacheLock   
#define  ARP_DS_UNLOCK   ArpArpcacheUnLock 

#define   ARP_ALLOCATE_BUF(u4Size, u4ValidOffset)                 \
          CRU_BUF_Allocate_MsgBufChain((u4Size), (u4ValidOffset))

#define ARP_GET_DATA_PTR_IF_LINEAR(pBuf, u4Offset, u4NumBytes)    \
   CRU_BUF_Get_DataPtr_IfLinear((pBuf), (u4Offset), (u4NumBytes))

 /* APIs for Taking/Releasing ARP Protocol Lock */


/* MACROS */
/* ------ */


#define ARP_INIT_COMPLETE(u4Status)      lrInitComplete(u4Status)

#define ARP_CREATE_TMR_LIST(au1TaskName, u4Event, pTmrListId)         \
   TmrCreateTimerList((au1TaskName), (u4Event), NULL, (pTmrListId))

#define ARP_NEXT_TIMER(TimerListId)             \
   TmrGetNextExpiredTimer((TimerListId))

#define ARP_LOWER_LAYER_ADDRESS(u2Index) \
   IP_LOWER_LAYER_ADDRESS(u2Index)

#define ARP_GREATER      1
#define ARP_LESSER      -1
#define ARP_EQUAL        0

#define ARP_INDEX_COMPARE(pArpCache, u4IpAddr) \
     ((pArpCache->u4Ip_addr > u4IpAddr) ? ARP_GREATER : \
         ((pArpCache->u4Ip_addr < u4IpAddr) ? ARP_LESSER : ARP_EQUAL))

#ifdef RM_WANTED
#define   ARP_IS_NP_PROGRAMMING_ALLOWED() \
           L2RED_IS_NP_PROGRAMMING_ALLOWED () 
#else
#define   ARP_IS_NP_PROGRAMMING_ALLOWED() OSIX_TRUE
#endif

#define   NP_UPDATED            1
#define   NP_NOT_UPDATED        0

#define   tARP_APP_TIMER_LIST   tTMO_APP_TIMER_LIST
#define   tARP_APP_TIMER        tTMO_APP_TIMER
#define   ARP_OK                TMO_OK
#define   ARP_ERROR             TMO_ERROR
#define   ARP_OR                TMO_OR

      /** as defined in tmo_hash.h **/
#define   ARP_HASH_Replace_Node    TMO_HASH_Replace_Node

     /** end if tmo macros **/

/* EXTERNS */
/* ------- */

extern INT4 CfaHandleQoSPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                    UINT2                  u2IfIndex, 
                                    UINT4                  u4NextHopIpAddr,
                                    UINT1                 *au1MediaAddr, 
                                    UINT2                  u2Protocol, 
                                    UINT1                  u1PktType,
                                    UINT1                  u1EncapType));

#endif          /*__ARPPORT_H__*/
