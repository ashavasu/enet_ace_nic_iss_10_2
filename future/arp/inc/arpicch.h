/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: arpicch.h,v 1.3 2015/11/04 11:19:54 siva Exp $
 *
 * Description: This file contains all macro definitions and 
 *              function prototypes for ICCH module.
 *              
 *******************************************************************/
#ifndef __ARP_ICCH_H
#define __ARP_ICCH_H



/* tArpIcchData - Used to enqueue message to Icch */
typedef struct _ArpIcchData
{
    tIcchMsg     *pIcchMsg;  /* Message buffer sent from ICCH to ARP */
    UINT2         u2DataLen; /* Buffer length */
    UINT1         u1Event;   /* Event from ICCH to ARP */
    UINT1         au1Reserved[1];
}tArpIcchData;

enum
{
    ARP_ICCH_BULK_REQUEST = ICCH_BULK_UPDT_REQ_MSG,
    ARP_ICCH_TAIL_MSG = ICCH_BULK_UPDT_TAIL_MSG,
    ARP_ICCH_BULK_UPDATE, 
    ARP_ICCH_SYNC_MSG
};

#define ARP_ICCH_TYPE_FIELD_SIZE 1
#define ARP_ICCH_LEN_FIELD_SIZE 2
#define ARP_ICCH_VLAN_ID_SIZE 2
#define ARP_ICCH_BULK_REQ_MSG_LEN 3
#define ARP_ICCH_TAIL_MSG_LEN 3
#define ARP_ICCH_IP_ADDR_SIZE 4
#define ARP_ICCH_ARP_STATE_SIZE 1
#define ARP_ICCH_HW_ADDR_SIZE CFA_ENET_ADDR_LEN
#define ARP_ICCH_HW_TYPE_SIZE 2
#define ARP_ICCH_ROW_STATUS_SIZE 1
#define ARP_ICCH_HW_STATUS_SIZE 1
#define ARP_ICCH_MAX_MSG_SIZE 1500


/* ICCH wanted */

#define ARP_ICCH_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    ICCH_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define ARP_ICCH_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    ICCH_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define ARP_ICCH_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    ICCH_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define ARP_ICCH_PUT_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
    ICCH_COPY_TO_OFFSET (pMsg, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size;\
}while (0)

#define ARP_ICCH_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        ICCH_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define ARP_ICCH_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        ICCH_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define ARP_ICCH_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        ICCH_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define ARP_ICCH_GET_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
        ICCH_GET_DATA_N_BYTE (pMsg, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)
/* Function prototypes for ARP ICCH */
INT4 ArpIcchRegisterWithICCH (VOID);
INT4 
ArpIcchHandleUpdateEvents (UINT1 u1Event, tIcchMsg * pData, UINT2 u2DataLen);
VOID ArpIcchProcessUpdateMsg (VOID);
VOID ArpIcchHandleUpdates (tIcchMsg * pMsg, UINT2 u2DataLen);
INT4 ArpIcchSendMsgToIcch (tIcchMsg * pMsg, UINT2 u2Len);
VOID
ArpIcchProcessBulkInfo (tIcchMsg *pMsg, UINT4 *pu4Offset, UINT2 u2TotalMsgLen);
VOID
ArpIcchSendBulkUpdates (VOID);
VOID
ArpIcchInitBulkReq (VOID);
INT4
ArpIcchCheckAndSendMsg (tIcchMsg **ppIcchMsg, UINT2 u2MsgLen, UINT2 *pu2OffSet);
INT4
ArpIcchSendBulkUpdTailMsg (VOID);
INT4
ArpIcchSyncArpMsg (tARP_DATA *pArpData, UINT4 u4IfIndex);
INT4
ArpIcchProcessArpSyncMsg (tIcchMsg *pMsg, UINT4 *pu4Offset);
INT4
ArpIcchDeRegisterWithICCH (VOID);
#endif /* __ARP_ICCH_H */
