#* $Id: make.h,v 1.3 2012/08/31 10:48:14 siva Exp $
#!/bin/csh
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       :                                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

ARP_COMPILATION_SWITCHES = ${GENERAL_COMPILATION_SWITCHES} ${SYSTEM_COMPILATION_SWITCHES}
ARP_COMPILATION_SWITCHES += -UDUPLICATE_ARP_RESP_NOT_WANTED

ARP_COMPILATION_SWITCHES += -DOPTIMIZED_HW_AUDIT_WANTED
############################################################################
#                         Directories                                      #
############################################################################

CLI_INC      = $(BASE_DIR)/inc/cli
ARPD         = ${BASE_DIR}/arp
ARP_INCD     = ${ARPD}/inc
ARP_SRCD     = ${ARPD}/src
ARP_OBJD     = ${ARPD}/obj

ifeq (DARP_TEST_WANTED, $(findstring DARP_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
ARP_TSTD = $(ARPD)/test
endif


############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

ARP_INCLUDES = -I${ARP_INCD} \
                $(COMMON_INCLUDE_DIRS)

ifeq (DARP_TEST_WANTED, $(findstring DARP_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
ARP_INCLUDES += -I$(ARP_TSTD)
endif

#############################################################################

