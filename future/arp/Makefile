#####################################################################
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved        ####
####  MAKEFILE HEADER ::                                         ####
#* $Id: Makefile,v 1.16 2015/12/29 11:48:27 siva Exp $		     ####
##|###############################################################|##
##|    FILE NAME               ::  Makefile                       |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.      |##
##|                                                               |##
##|    MODULE NAME             ::  ARP                            |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  UNIX                           |##
##|                                                               |##
##|              DESCRIPTION   ::  Make file for Future ARP       |##
##|                                Router ARP Module              |##
##|                                This file will be invoked by   |##
##|                                the top level makefile.        |##
##|###############################################################|##
.PHONY :  START ECHO DONE clean

include ../LR/make.h
include ../LR/make.rule
include make.h

ISS_C_FLAGS         = ${CC_FLAGS}  ${CC_DEBUG_OPTIONS} ${ARP_COMPILATION_SWITCHES} ${ARP_INCLUDES}

#Arp Dependencies List.

ARP_DPNDS = ${COMMON_DEPENDENCIES} \
            ${COMN_INCL_DIR}/lr.h \
            ${COMN_INCL_DIR}/arp.h \
            ${ARPD}/Makefile \
            ${ARPD}/make.h \
            ${ARP_INCD}/arptrace.h \
            ${ARP_INCD}/arpextn.h \
            ${ARP_INCD}/arpproto.h \
            ${ARP_INCD}/arpinc.h \
            ${ARP_INCD}/arptypes.h \
            ${ARP_INCD}/fsarplw.h \
            ${ARP_INCD}/fsmparlw.h \
            ${ARP_INCD}/arpsz.h \
            ${ARP_INCD}/arpport.h \
            ${ARP_INCD}/arpred.h

ifeq (DARP_TEST_WANTED, $(findstring DARP_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
ARP_DPNDS += $(CLI_INC)/arpcli.h
ARP_DPNDS += $(ARP_TSTD)/arptstcli.h
ARP_DPNDS += $(ARP_TSTD)/arptest.h
endif


#Object Module List 

ARP_OBJS  = ${ARP_OBJD}/arpapi.o \
            ${ARP_OBJD}/arpmain.o \
            ${ARP_OBJD}/arpcache.o \
            ${ARP_OBJD}/arptskmg.o \
            ${ARP_OBJD}/arphwtbl.o \
            ${ARP_OBJD}/fsarplw.o \
            ${ARP_OBJD}/fsmparlw.o \
            ${ARP_OBJD}/arpsz.o \
            ${ARP_OBJD}/arphrif.o

ifeq (${MBSM}, YES)
ARP_OBJS += ${ARP_OBJD}/arpmbsm.o
ARP_DPNDS +=  ${COMN_INCL_DIR}/mbsm.h
endif

ifeq (${LNXIP4}, YES)
ARP_OBJS  += ${ARP_OBJD}/arplnxip.o

ifeq (DLINUX_ARP_NETLINK, $(findstring DLINUX_ARP_NETLINK,$(SYSTEM_COMPILATION_SWITCHES)))
ARP_OBJS  += ${ARP_OBJD}/arpntlnk.o
ARP_DPNDS += ${ARP_INCD}/arpntlnk.h
endif
endif

ifeq (${L2RED}, YES)
ARP_OBJS += ${ARP_OBJD}/arpred.o
else
ARP_OBJS += ${ARP_OBJD}/arpstb.o
endif

ifeq (${ICCH}, YES)
ARP_OBJS += ${ARP_OBJD}/arpicch.o
endif

ifeq (${NPAPI}, YES)
ARP_OBJS += ${ARP_OBJD}/arpnpapi.o
ifneq (DVRF_WANTED, $(findstring DVRF_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
ARP_OBJS  += ${ARP_OBJD}/arpsinp.o
endif
endif

ifeq (${FSIP4}, YES)
ARP_OBJS += ${ARP_OBJD}/arpfsip.o
endif

ifeq (${SNMP_2}, YES)
ARP_OBJS += ${ARP_OBJD}/fsarpwr.o
ARP_OBJS += ${ARP_OBJD}/fsmparwr.o
ARP_DPNDS += ${ARP_INCD}/fsarpwr.h
ARP_DPNDS += ${ARP_INCD}/fsmparwr.h
endif

ifeq (DARP_TEST_WANTED, $(findstring DARP_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
ARP_OBJS += $(ARP_TSTD)/arptstcli.o
ARP_OBJS += $(ARP_TSTD)/arptest.o
endif

ifeq (DARP_ARRAY_TO_RBTREE_WANTED, $(findstring DARP_ARRAY_TO_RBTREE_WANTED, $(ARP_COMPILATION_SWITCHES)))
ARP_OBJS += ${ARP_OBJD}/arprbutl.o
endif

ifeq (${NETCONF}, YES)
ARP_OBJS  += ${ARP_OBJD}/fsarpnc.o
ARP_DPNDS += ${ARP_INCD}/fsarpnc.h
ARP_OBJS  += ${ARP_OBJD}/fsmparnc.o
ARP_DPNDS += ${ARP_INCD}/fsmparnc.h
endif

#Final Object

ARP_OBJ   = ${ARP_OBJD}/FutureARP.o

START : ECHO ${ARP_OBJ} DONE

ECHO  :
	@echo Making ARP Submodule...

DONE  :
	@echo ARP Submodule done.

${ARP_OBJ} : obj ${ARP_OBJS}
	${LD} ${LD_FLAGS} -o ${ARP_OBJ} ${ARP_OBJS} ${CC_COMMON_OPTIONS}

obj:
ifdef MKDIR
	$(MKDIR) $(MKDIR_FLAGS) ${ARP_OBJD}
endif

#Object Module Dependency

${ARP_OBJD}/arpapi.o: ${ARP_SRCD}/arpapi.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpapi.c -o $@

${ARP_OBJD}/arpmain.o: ${ARP_SRCD}/arpmain.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpmain.c -o $@

${ARP_OBJD}/arpfsip.o: ${ARP_SRCD}/arpfsip.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpfsip.c -o $@

#${ARP_OBJD}/arpsnif.o: ${ARP_SRCD}/arpsnif.c \
#    ${ARP_DPNDS}
#	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpsnif.c -o $@

${ARP_OBJD}/arptskmg.o: ${ARP_SRCD}/arptskmg.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arptskmg.c -o $@

${ARP_OBJD}/arplnxip.o: ${ARP_SRCD}/arplnxip.c \
    ${ARP_INCD}/arpinc.h \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arplnxip.c -o $@

${ARP_OBJD}/arpsinp.o: ${ARP_SRCD}/arpsinp.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpsinp.c -o $@

${ARP_OBJD}/arpntlnk.o: ${ARP_SRCD}/arpntlnk.c \
    ${ARP_INCD}/arpinc.h \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpntlnk.c -o $@

${ARP_OBJD}/arphrif.o: ${ARP_SRCD}/arphrif.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arphrif.c -o $@

${ARP_OBJD}/arpcache.o: ${ARP_SRCD}/arpcache.c \
    ${ARP_INCD}/arpinc.h \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpcache.c -o $@

${ARP_OBJD}/arphwtbl.o: ${ARP_SRCD}/arphwtbl.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arphwtbl.c -o $@

${ARP_OBJD}/arpmbsm.o: ${ARP_SRCD}/arpmbsm.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpmbsm.c -o $@

${ARP_OBJD}/arpnpapi.o: ${ARP_SRCD}/arpnpapi.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpnpapi.c -o $@

${ARP_OBJD}/fsarpwr.o: ${ARP_SRCD}/fsarpwr.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/fsarpwr.c -o $@

${ARP_OBJD}/fsmparwr.o: ${ARP_SRCD}/fsmparwr.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/fsmparwr.c -o $@

${ARP_OBJD}/fsarplw.o: ${ARP_SRCD}/fsarplw.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/fsarplw.c -o $@

${ARP_OBJD}/fsmparlw.o: ${ARP_SRCD}/fsmparlw.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/fsmparlw.c -o $@

${ARP_OBJD}/arpsz.o: ${ARP_SRCD}/arpsz.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpsz.c -o $@

${ARP_OBJD}/arpred.o: ${ARP_SRCD}/arpred.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpred.c -o $@

${ARP_OBJD}/arpicch.o: ${ARP_SRCD}/arpicch.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpicch.c -o $@

${ARP_OBJD}/arpstb.o: ${ARP_SRCD}/arpstb.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arpstb.c -o $@

${ARP_OBJD}/arprbutl.o: ${ARP_SRCD}/arprbutl.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/arprbutl.c -o $@

${ARP_TSTD}/arptstcli.o: ${ARP_TSTD}/arptstcli.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_TSTD}/arptstcli.c -o $@

${ARP_TSTD}/arptest.o: ${ARP_TSTD}/arptest.c \
    ${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_TSTD}/arptest.c -o $@


ifeq (${NETCONF}, YES)
${ARP_OBJD}/fsarpnc.o: ${ARP_SRCD}/fsarpnc.c \
	${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/fsarpnc.c -o $@

${ARP_OBJD}/fsmparnc.o: ${ARP_SRCD}/fsmparnc.c \
	${ARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${ARP_SRCD}/fsmparnc.c -o $@
endif

clean :
	@echo "Cleaning the ARP object files"
	${RM} ${RM_FLAGS} ${ARP_OBJ} ${ARP_OBJS}
	$(RM) $(RM_FLAGS) 

pkg:
	CUR_PWD=${PWD};\
        cd ${BASE_DIR}/../..;\
        tar cvzf $(BASE_DIR)/../../arp.tgz  -T ${BASE_DIR}/arp/FILES.NEW;\
        cd ${CUR_PWD};

