/********************************************************************
 * $Id: arpapi.c,v 1.19 2016/02/28 10:56:38 siva Exp $
 *******************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : arpapi.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    MODULE NAME           : ARP                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DESCRIPTION           : This file contains the exported APIs of        */
/*                            the ARP module                                 */
/*---------------------------------------------------------------------------*/

#include "arpinc.h"

/*****************************************************************************
*  Function Name             : ArpAddAllArpEntriesInCxt                     
*  Description               : This function scans the complete ARP cache   
*                              table and adds the ARP entries configured    
*                              or learnt in the given context in the        
*                              in the L3 IP Table                           
*  Input(s)                  : u4ContextId - Virtual Router Id              
*  Output(s)                 : None                                         
*  Returns                   : VOID                                         
*****************************************************************************/

VOID
ArpAddAllArpEntriesInCxt (UINT4 u4ContextId)
{
    UINT1               au1MacAddr[CFA_ENET_ADDR_LEN];
    t_ARP_CACHE        *pArpCache, *pNextArpCache = NULL;
    tArpCxt            *pArpCxt = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;

    ARP_PROT_LOCK ();
    ARP_DS_LOCK ();

    pArpCxt = gArpGblInfo.apArpCxt[u4ContextId];
    if (pArpCxt == NULL)
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                      "ArpAddAllArpEntriesInCxt: Invalid context id - %d\n",
                      u4ContextId);
        ARP_DS_UNLOCK ();
        ARP_PROT_UNLOCK ();
        return;
    }

    pArpCache = RBTreeGetFirst (gArpGblInfo.ArpTable);

    while (pArpCache != NULL)
    {
        pNextArpCache = RBTreeGetNext (gArpGblInfo.ArpTable, pArpCache, NULL);
        if (pArpCache->pArpCxt->u4ContextId != u4ContextId)
        {
            pArpCache = pNextArpCache;
            continue;
        }

        if (pArpCache->i1State == ARP_STATIC_NOT_IN_SERVICE)
        {
            /* Statically configured and Rowstaus as NOT_IN_SERVICE 
             * or NOT_READY
             */
            pArpCache = pNextArpCache;
            continue;
        }

        if (pArpCache->i1State == ARP_STATIC)
        {
            /* Dont add the Entries to NP if OperState of interface
             * is down. Entries will be added to NP when interface 
             * comes up */
            MEMSET (&NetIpIfInfo, ARP_ZERO, sizeof (tNetIpv4IfInfo));
            if (NetIpv4GetIfInfo ((UINT4) pArpCache->u2Port,
                                  &NetIpIfInfo) != NETIPV4_SUCCESS)
            {
                pArpCache = pNextArpCache;
                continue;
            }
            if (NetIpIfInfo.u4Oper == IPIF_OPER_DISABLE)
            {
                pArpCache = pNextArpCache;
                continue;
            }
        }

        MEMCPY (au1MacAddr, pArpCache->i1Hw_addr, CFA_ENET_ADDR_LEN);
        ArpNpAdd (pArpCache->pArpCxt->u4ContextId,
                  pArpCache->u4Ip_addr, pArpCache->i2Hardware,
                  (INT1 *) au1MacAddr, pArpCache->i1Hwalen,
                  pArpCache->u4IfIndex, pArpCache->u1EncapType,
                  pArpCache->i1State);

        pArpCache = pNextArpCache;
    }

    ARP_DS_UNLOCK ();
    ARP_PROT_UNLOCK ();
    return;
}

/*****************************************************************************
*  Function Name             : ArpDelAllArpEntriesInCxt                     
*  Description               : This function scans the complete ARP cache   
*                              table and deletes the ARP cache entries      
*                              learnt / configured in the given context    
*                              from the hardware IP table.                  
*  Input(s)                  : u4ContextId - Virtual Router Id              
*  Output(s)                 : None                                         
*  Returns                   : None                                         
*****************************************************************************/
VOID
ArpDelAllArpEntriesInCxt (UINT4 u4ContextId)
{
    t_ARP_CACHE        *pArpCache, *pNextArpCache = NULL;
    tArpCxt            *pArpCxt = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;
    tCfaIfInfo          CfaIfInfo;

    ARP_PROT_LOCK ();
    ARP_DS_LOCK ();

    pArpCxt = gArpGblInfo.apArpCxt[u4ContextId];
    if (pArpCxt == NULL)
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                      "ArpDelAllArpEntriesInCxt: Invalid context id - %d\n",
                      u4ContextId);
        ARP_DS_UNLOCK ();
        ARP_PROT_UNLOCK ();
        return;
    }

    pArpCache = RBTreeGetFirst (gArpGblInfo.ArpTable);

    while (pArpCache != NULL)
    {
        pNextArpCache = RBTreeGetNext (gArpGblInfo.ArpTable, pArpCache, NULL);

        if (pArpCache->pArpCxt->u4ContextId != u4ContextId)
        {
            pArpCache = pNextArpCache;
            continue;
        }

        if (pArpCache->i1State == ARP_STATIC_NOT_IN_SERVICE)
        {
            /* Statically configured and Rowstaus as NOT_IN_SERVICE 
             * or NOT_READY
             */
            pArpCache = pNextArpCache;
            continue;
        }

        if (pArpCache->i1State == ARP_STATIC)
        {
            /* If interface is down,Static Entry is made not available
             * in NP .So no need to update NP */
            MEMSET (&NetIpIfInfo, ARP_ZERO, sizeof (tNetIpv4IfInfo));
            if (NetIpv4GetIfInfo ((UINT4) pArpCache->u2Port,
                                  &NetIpIfInfo) != NETIPV4_SUCCESS)
            {
                pArpCache = pNextArpCache;
                continue;
            }
            if (NetIpIfInfo.u4Oper == IPIF_OPER_DISABLE)
            {
                pArpCache = pNextArpCache;
                continue;
            }
        }

        /* This function is invoked when ip routing is disabled.
         * Hence dont invoke ArpNpDel since it will check for
         * ip routing status. If ip routing is disabled it will
         * just return success */
        if (CfaGetIfInfo (pArpCache->u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
        {
            ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, CONTROL_PLANE_TRC, ARP_NAME,
                          "CfaGetIfInfo failed for ifIndex - 0x%x \n",
                          pArpCache->u4IfIndex);
            pArpCache = pNextArpCache;
            continue;
        }

#ifdef NPAPI_WANTED
        if ((CfaIsMgmtPort (pArpCache->u4IfIndex) == FALSE) &&
            (CfaIsLinuxVlanIntf (pArpCache->u4IfIndex) == FALSE))
        {
            ArpFsNpIpv4VrfArpDel (pArpCache->pArpCxt->u4ContextId,
                                  pArpCache->u4Ip_addr, CfaIfInfo.au1IfName,
                                  pArpCache->i1State);
        }
#endif
        pArpCache = pNextArpCache;
    }

    ARP_DS_UNLOCK ();
    ARP_PROT_UNLOCK ();

    return;
}

/****************************************************************************
* Function     : ArpResolveWithIndex
*                                            
* Description  : This function resolves the hardware address and the        
*                encapsulation type for the given IP address learnt on the 
*                given interface.
*
* Input        : u4IfIndex   - Interface on which the IP address is learnt
*                u4IpAddr - IpAddress to be resolved.     
*
* Output       : pi1Hw_addr -Hardware address.
*                pu1EncapType - EncapType for that Entry.   
*
* Returns      : ARP_SUCCESS/ARP_FAILURE   
*
***************************************************************************/
INT1
ArpResolveWithIndex (UINT4 u4IfIndex, UINT4 u4IpAddr, INT1 *pi1Hw_addr,
                     UINT1 *pu1EncapType)
{
    INT1                i1RetVal = 0;
    UINT4               u4ContextId = 0;

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    ARP_PROT_LOCK ();
    i1RetVal = ArpGetValidEntryWithIndex (u4IfIndex, u4IpAddr,
                                          pi1Hw_addr, pu1EncapType);
    if (i1RetVal == ARP_FAILURE)
    {
        ARP_TRC (u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                 "ArpGetValidEntry Failed.\n");
        ARP_PROT_UNLOCK ();
        return ARP_FAILURE;
    }
    ARP_PROT_UNLOCK ();
    return i1RetVal;
}

/****************************************************************************
* Function     : ArpNotifyContextStatus 
*                                            
* Description  : This function is invoked by the VCM module to notify the 
*                 context creation / context deletion events to the ARP task.
*
* Input        : u4ContextId - The virtual router Id                      
*                u4Event     - Context create / delete event.
*
* Output       : None 
*
* Returns      : ARP_SUCCESS/ARP_FAILURE   
*
***************************************************************************/

VOID
ArpNotifyContextStatus (UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1Event)
{
    tArpQMsg            ArpQMsg;

    UNUSED_PARAM (u4IfIndex);

    MEMSET (&ArpQMsg, ARP_ZERO, sizeof (tArpQMsg));

    if (u1Event == VCM_CONTEXT_CREATE)
    {
        ARP_PROT_LOCK ();
        ArpHandleContextCreate (u4ContextId);
        ARP_PROT_UNLOCK ();
        return;
    }
    else
    {
        ArpQMsg.u4MsgType = ARP_CONTEXT_DELETE;
    }

    ArpQMsg.u4ContextId = u4ContextId;

    if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
    {
        return;
    }

    return;
}

/*****************************************************************************/
/* Function     : arp_add                                                    */
/*                                                                           */
/*  Description : Add the ArpCache With the Provided Information             */
/*                                                                           */
/* Input        : tArpData - Information for Adding Arp                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : ARP_SUCCESS/ARP_FAILURE                                    */
/*****************************************************************************/
INT1
arp_add (tARP_DATA ArpData)
{
    ARP_PROT_LOCK ();
    if (ArpAddArpEntry (ArpData, NULL, ARP_ZERO) == ARP_SUCCESS)
    {
        ARP_PROT_UNLOCK ();
        return ARP_SUCCESS;
    }
    ARP_PROT_UNLOCK ();
    return ARP_FAILURE;
}

/****************************************************************************
* Function     : ArpResolve     
*                                            
* Description  : To check whether the entry is available in the Arp cache.  
*
* Input        : u4IpAddr - IpAddress to be resolved.     
*
* Output       : pi1Hw_addr -Hardware address.
*                pu1EncapType - EncapType for that Entry.   
*
* Returns      : ARP_SUCCESS/ARP_FAILURE   
*
***************************************************************************/
INT1
ArpResolve (UINT4 u4IpAddr, INT1 *pi1Hw_addr, UINT1 *pu1EncapType)
{
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CFA_FAILURE;

    /* ARP protocol lock is taken within the API ArpResolveWithIndex */
    i4RetVal = CfaIpIfGetIfIndexForNetInCxt (ARP_DEFAULT_CONTEXT,
                                             u4IpAddr, &u4IfIndex);

    if (i4RetVal == CFA_SUCCESS)
    {
        return (ArpResolveWithIndex (u4IfIndex, u4IpAddr,
                                     pi1Hw_addr, pu1EncapType));
    }
    return ARP_FAILURE;
}

/****************************************************************************
* Function     : ArpSetArpDebug     
*                                            
* Description  : This function configures the ARP global debug flag or the
*                 context based ARP debug flag.
*
* Input        : u4ContextId - The context Id where the debug flag is
*                               configured / Invalid context Id - to 
*                               configure global debug flag
*                u4TrcValue  - The configured trace value.
*
* Output       : None                         
*
* Returns      : None                      
*
***************************************************************************/
VOID
ArpSetArpDebug (UINT4 u4ContextId, UINT4 u4TrcValue)
{
    ARP_PROT_LOCK ();
    if (u4ContextId == ARP_DEFAULT_CONTEXT)
    {
        gArpGblInfo.u4ArpDbg = u4TrcValue;
    }
    else
    {
        gArpGblInfo.apArpCxt[u4ContextId]->u4ArpDbg = u4TrcValue;
    }
    ARP_PROT_UNLOCK ();
    return;
}

/****************************************************************************
* Function     : ArpGetIntfForAddr  
*                                            
* Description  : This function retrives the interface index for the given 
*                 address in the given context
*
* Input        : u4ContextId - The context Id
*                u4IpAddress - Ip Address
*
* Output       : pu4IfIndex - Interface index on which the Ip address is added
*
* Returns      : ARP_SUCCESS/ARP_FAILURE   
*
***************************************************************************/
INT4
ArpGetIntfForAddr (UINT4 u4ContextId, UINT4 u4IpAddress, UINT4 *pu4IfIndex)
{
    t_ARP_CACHE         ArpCache;
    t_ARP_CACHE        *pArpCache = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2Port = 0;
    UINT2               u2Cntr = 0;

    if (IpIsLocalAddrInCxt (u4ContextId, u4IpAddress, &u2Port) == IP_SUCCESS)
    {
        if (NetIpv4GetCfaIfIndexFromPort (u2Port, &u4IfIndex)
            == NETIPV4_SUCCESS)
        {
            MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
            ArpCache.u4IfIndex = u4IfIndex;
            ArpCache.u4Ip_addr = u4IpAddress;
            pArpCache =
                RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);
            if (pArpCache != NULL)
            {
                *pu4IfIndex = u4IfIndex;
                return ARP_SUCCESS;
            }
        }
    }

    MEMSET (gu4IpIfaces, ZERO, (IPIF_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
    if (VcmGetCxtIpIfaceList (u4ContextId, gu4IpIfaces) == VCM_FAILURE)
    {
        return ARP_FAILURE;
    }
    for (u2Cntr = 1; ((u2Cntr <= gu4IpIfaces[0]) &&
                      (u2Cntr < ARP_MAX_SIZING_ENET_ENTRIES + 1)); u2Cntr++)
    {
        ArpCache.u4IfIndex = gu4IpIfaces[u2Cntr];
        ArpCache.u4Ip_addr = u4IpAddress;
        pArpCache = RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);
        if (pArpCache != NULL)
        {
            *pu4IfIndex = gu4IpIfaces[u2Cntr];
            return ARP_SUCCESS;
        }
    }
    return ARP_FAILURE;
}

/****************************************************************************
 * * Function     : ArpGetGlblCxt
 * *
 * * Description  : This function retrives the pointer for given context
 * *
 * * Input        : u4ContextId - The context Id
 * *
 * * Output       : pArpCxt - pointer to arp context 
 * *
 * * Returns      :  pArpCxt pointer /NULL
 * *
 * ***************************************************************************/

tArpCxt            *
ArpGetGlblCxt (UINT4 u4ContextId)
{
    tArpCxt            *pArpCxt = NULL;

    ARP_PROT_LOCK ();

    pArpCxt = gArpGblInfo.apArpCxt[u4ContextId];

    ARP_PROT_UNLOCK ();

    return (pArpCxt);
}

/*****************************************************************************/
/* Function Name      : ArpRegDeRegProtocol                                  */
/*                                                                           */
/* Description        : This fucntion is used by other protocols to register */
/*                        with ARP for receving the arp basic information.     */
/*                                                                           */
/* Input(s)           : u4Type - ARP_INDICATION_REGISTER                     */
/*                               ARP_INDICATION_DEREGISTER                   */
/*                    : pArpRegTbl    - Information about the registration   */
/*                                         protocol and call back function.    */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
ArpRegDeRegProtocol (UINT4 u4Type, tArpRegTbl * pArpRegTbl)
{
    UINT4               u4ContextId = 0;

    UNUSED_PARAM (u4ContextId);

    if (u4Type >= ARP_INDICATION_INVALID)
    {
        ARP_TRC (u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRegDeRegProtocol - Failed - Invalid Type\n");
        return OSIX_FAILURE;
    }

    if (pArpRegTbl->u1ProtoId >= ARP_MAX_REG_MODULES)
    {
        if (u4Type == ARP_INDICATION_REGISTER)
        {
            ARP_TRC (u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                     "ArpRegDeRegProtocol - Registration Failed - Max protocols reached\n");
        }
        else
        {
            ARP_TRC (u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                     "ArpRegDeRegProtocol - De-Registration Failed - Max protocols reached\n");
        }
        return OSIX_FAILURE;
    }

    ARP_PROT_LOCK ();
    if (u4Type == ARP_INDICATION_REGISTER)
    {
        if (pArpRegTbl->pArpBasicInfo == NULL)
        {
            ARP_TRC (u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                     "ArpRegDeRegProtocol - Registration Failed - "
                     "Input Callback function is NULL\n");
            ARP_PROT_UNLOCK ();
            return OSIX_FAILURE;
        }

        if (gaArpRegTbl[pArpRegTbl->u1ProtoId].pArpBasicInfo != NULL)
        {
            ARP_TRC (u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                     "ArpRegDeRegProtocol - Registration Failed - Already registered\n");
            ARP_PROT_UNLOCK ();
            return OSIX_FAILURE;
        }

        gaArpRegTbl[pArpRegTbl->u1ProtoId].u1ProtoId = pArpRegTbl->u1ProtoId;

        gaArpRegTbl[pArpRegTbl->u1ProtoId].pArpBasicInfo =
            pArpRegTbl->pArpBasicInfo;
    }
    else
    {
        gaArpRegTbl[pArpRegTbl->u1ProtoId].u1ProtoId = 0;
        gaArpRegTbl[pArpRegTbl->u1ProtoId].pArpBasicInfo = NULL;
    }
    ARP_PROT_UNLOCK ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : ArpGetShowCmdOutputAndCalcChkSum                     */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ARP_SUCESS/ARP_FAILURE                             */
/*****************************************************************************/
INT4
ArpGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED && defined RM_WANTED)

    if (ARP_GET_NODE_STATUS () == RM_ACTIVE)
    {
        if (ArpCliGetShowCmdOutputToFile ((UINT1 *) ARP_AUDIT_FILE_ACTIVE) !=
            CLI_SUCCESS)
        {
            return ARP_FAILURE;
        }
        if (ArpCliCalcSwAudCheckSum
            ((UINT1 *) ARP_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != ARP_SUCCESS)
        {
            return ARP_FAILURE;
        }
    }
    else if (ARP_GET_NODE_STATUS () == RM_STANDBY)
    {
        if (ArpCliGetShowCmdOutputToFile ((UINT1 *) ARP_AUDIT_FILE_STDBY) !=
            CLI_SUCCESS)
        {
            return ARP_FAILURE;
        }
        if (ArpCliCalcSwAudCheckSum
            ((UINT1 *) ARP_AUDIT_FILE_STDBY, pu2SwAudChkSum) != ARP_SUCCESS)
        {
            return ARP_FAILURE;
        }
    }
    else
    {
        return ARP_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return ARP_SUCCESS;
}

/****************************************************************************
* Function     : ArpResolveInCxt
*
* Description  : To check whether the entry is available in the Arp cache.
*
* Input        : u4IpAddr - IpAddress to be resolved.
*
* Output       : pi1Hw_addr -Hardware address.
*                pu1EncapType - EncapType for that Entry.
*
* Returns      : ARP_SUCCESS/ARP_FAILURE
*
***************************************************************************/
INT1
ArpResolveInCxt (UINT4 u4IpAddr, INT1 *pi1Hw_addr, UINT1 *pu1EncapType, UINT4 u4ContextId)
{
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CFA_FAILURE;

    /* ARP protocol lock is taken within the API ArpResolveWithIndex */
    i4RetVal = CfaIpIfGetIfIndexForNetInCxt (u4ContextId,
                                             u4IpAddr, &u4IfIndex);

    if (i4RetVal == CFA_SUCCESS)
    {
        return (ArpResolveWithIndex (u4IfIndex, u4IpAddr,
                                     pi1Hw_addr, pu1EncapType));
    }
    return ARP_FAILURE;
}

#ifdef ICCH_WANTED
/*****************************************************************************/
/* Function Name      : ArpIsMclagEnabled                                    */
/*                                                                           */
/* Description        : This function is used to check whether MC-LAG is     */
/*                      enabled on L3 interface index                        */
/*                                                                           */
/* Input(s)           : u4IfIndex  - L3 interface index                      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ARP_SUCCESS / ARP_FAILURE.                           */
/*****************************************************************************/
INT4
ArpIsMclagEnabled (UINT4 u4IfIndex)
{
    tPortList           *EgressPortList = NULL;
    tPortList           *UntagPortList = NULL;
    UINT4               u4PhyIfIndex = 0;
    tVlanId             VlanId = 0;
    UINT1               u1IsMclagEnabled = 0;
    BOOL1               bResult = FALSE;

    EgressPortList =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (EgressPortList == NULL)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                "Memory Allocation failed for Egressport list\r\n");
        return ARP_FAILURE;
    }

    MEMSET (*EgressPortList, 0, sizeof (tPortList));

    UntagPortList =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    if (UntagPortList == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) EgressPortList);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                "Memory Allocation failed for Ingress portlist\r\n");
        return ARP_FAILURE;
    }

    MEMSET (UntagPortList, 0, sizeof (tPortList));

    if (CfaGetVlanId (u4IfIndex, &VlanId) == CFA_SUCCESS)
    {
        if (VLAN_SUCCESS != VlanGetVlanMemberPorts (VlanId, *EgressPortList,
                *UntagPortList))
        {
            ARP_TRC_ARG2 (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC, ALL_FAILURE_TRC,
                         ARP_NAME, "Failed to get the member of VLAN %u while "
                         "checking if MC-LAG is enabled on L3 interface %u\r\n",
                         VlanId, u4IfIndex);
            FsUtilReleaseBitList ((UINT1 *) EgressPortList);
            FsUtilReleaseBitList ((UINT1 *) UntagPortList);

            return ARP_FAILURE;
        }

        /* Loop for only port-channel interfaces to identify whether MC-LAG
         * is enabled or not */
        for (u4PhyIfIndex = SYS_DEF_MAX_PHYSICAL_INTERFACES; u4PhyIfIndex <= 
                        (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG_INTF); 
                        u4PhyIfIndex++)
        {
            OSIX_BITLIST_IS_BIT_SET ((*EgressPortList),
                    u4PhyIfIndex,
                    BRG_PORT_LIST_SIZE, bResult);

            if (OSIX_TRUE == bResult)
            {
                LaApiIsMclagInterface (u4PhyIfIndex,
                        &u1IsMclagEnabled);
                if (u1IsMclagEnabled == OSIX_TRUE)
                {
                    FsUtilReleaseBitList ((UINT1 *) EgressPortList);
                    FsUtilReleaseBitList ((UINT1 *) UntagPortList);
                    return ARP_SUCCESS;
                }
            }
        }
    }
    FsUtilReleaseBitList ((UINT1 *) EgressPortList);
    FsUtilReleaseBitList ((UINT1 *) UntagPortList);
    return ARP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : ArpIsMclagEnabled                                    */
/*                                                                           */
/* Description        : This function is used to initiage ARP bulk request   */
/*                      from external module                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */ 
/*****************************************************************************/
VOID
ArpApiIcchInitBulkReq ()
{
    ArpIcchInitBulkReq();
    return;
}

/*****************************************************************************/
/* Function Name      : ArpNotifyL2IfStChg                                   */
/*                                                                           */
/* Description        : This function is used to notify ARP module on the    */
/*                      deletion of L2 entries learnt on the specific        */
/*                      L2 physical port (can be a LAG interface as well),   */
/*                      Interface status change ARP_L2_IF_DELETED is         */
/*                      notified to ARP_PKT_Q_ID queue  and an event         */
/*                      ARP_PACKET_ARRIVAL_EVENT is notified to ARP module.  */
/*                                                                           */
/* Input(s)           : u4IfIndex - IVR interface index, corresponding to the*/
/*                      VLAN entry, specified in the recently deleted FDB    */
/*                      entry.                                               */
/*                      u4PhyIndex - Physical port index/LAG index,          */
/*                      corresponding to the destination port, as specified  */
/*                      in the recently deleted FDB entry.                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ARP_FAILURE - if notification to ARP module fails    */
/*                      ARP_SUCCESS - otherwise                              */ 
/*****************************************************************************/
INT4
ArpNotifyL2IfStChg (UINT4 u4IfIndex, UINT4 u4PhyIfIndex)
{
    tArpQMsg            ArpQMsg;

    MEMSET (&ArpQMsg, ARP_ZERO, sizeof (tArpQMsg));


    ArpQMsg.u4MsgType = ARP_L2_IF_DELETED;
    ArpQMsg.u4IfIndex = u4IfIndex;
    ArpQMsg.u4PhyIfIndex = u4PhyIfIndex;

    if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
    {
        ARP_TRC_ARG2 (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                 "Unable to notify the L2 interface deletion to ARP module for \
                 IVR interface %d and physical index%d\r\n", u4IfIndex, u4PhyIfIndex);
        return ARP_FAILURE;
    }

    return ARP_SUCCESS;
}

#endif /* ICCH_WANTED */
