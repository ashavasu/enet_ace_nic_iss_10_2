/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arphrif.c,v 1.41 2015/09/29 10:34:54 siva Exp $
 *
 * Description: This file interface to Lower Layer.
 *
 *******************************************************************/
#include "arpinc.h"
#ifdef NAT_WANTED
#include "nat.h"
#endif

/*-------------------------------------------------------------------+
 * Function           : arp_input
 *
 * Input(s)           : pArpQMsg - pointer to the tArpQMsg structure.
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS or ARP_FAILURE
 *
 * Action :
 *
 * Called from LL when there is a packet with type as ARP.
 * The procedure calls arp_add to add this entry to ARP cache.
 * (this is done irrespective of whether this is a REQUEST or RESPONSE)
 * If the packet is addressed to us, and if the packet carries the ARP
 * REQUEST then the response is sent back.
+-------------------------------------------------------------------*/
INT4
arp_input (tArpQMsg * pArpQMsg)
{
#ifdef IP_WANTED
    UINT4               u4SrcAddress = 0;
    INT4                i4Tmp = 0;
#endif /*IP_WANTED */
    tArpCxt            *pArpCxt = NULL;
    t_ARP_PKT           Arp_pkt;
    t_ARP_HW_TABLE     *pHw_entry = NULL;
    t_ARP_CACHE         ArpCache;
    INT4                i4ArpCacheCheck;
    tARP_DATA           tArpData;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT1               u1IfaceUNNUM = FALSE;
    UINT4               u4IfIndex = 0;
    UINT2               u2Port = pArpQMsg->u2Port;
    tCRU_BUF_CHAIN_HEADER *pBuf = pArpQMsg->pBuf;
#if defined (VRRP_WANTED) && defined (IP_WANTED)
    tIPvXAddr           IpAddr;
    UINT1               au1SrcHwAddr[CFA_ENET_ADDR_LEN];
    INT4                i4OperVrId = 0;
    INT4                i4VrrpRetVal = VRRP_NOT_OK;
    UINT4               u4TempAddr = 0;
#endif /* IP_WANTED */
    INT4                i4RetVal = 0;
    tNetIpv4RtInfo      OutRtInfo;
    tRtInfoQueryMsg     RtQuery;
#ifdef NAT_WANTED
    UINT4               u4IpAddress = 0;
    INT1                i1IsNatIpEntry = FALSE;
    INT1                i1Status = 0;
    INT1                i1NatPolicyStatus = 0;
#endif

#ifdef DUPLICATE_ARP_RESP_NOT_WANTED
    UNUSED_PARAM (i4RetVal);
    UNUSED_PARAM (OutRtInfo);
    UNUSED_PARAM (RtQuery);
#endif

#if defined (VRRP_WANTED) && defined (IP_WANTED)
    IPVX_ADDR_CLEAR (&IpAddr);
#endif
#ifdef NPAPI_WANTED
UINT1 u1NextHopFlag = 0;
#endif
    if (u2Port >= ARP_MAX_SIZING_ENET_ENTRIES)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_FAILURE;
    }

#ifdef IP_WANTED
    if (pArpQMsg->u2Protocol == RARP_PROTOCOL)
    {
        if (RarpInput (pBuf, u2Port) != ARP_SUCCESS)
        {
            return ARP_FAILURE;
        }
        return ARP_SUCCESS;
    }
#endif
    if (NetIpv4GetIfInfo ((UINT4) u2Port, &NetIpIfInfo) != NETIPV4_SUCCESS)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_FAILURE;
    }

    /* no ARP response if the admin status of port is disabled */

    if (NetIpIfInfo.u4Oper == IPIF_OPER_DISABLE)
    {
        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                      ARP_NAME, "Releasing buffer %x.\n", pBuf);
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_SUCCESS;
    }

    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) u2Port, &u4IfIndex)
        == NETIPV4_FAILURE)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_FAILURE;
    }

    pArpCxt = gArpGblInfo.apArpCxt[NetIpIfInfo.u4ContextId];

    /*
     * arp_extract_hdr() returns ARP_FAILURE if either
     * the hardware length or the protocol address length
     * is invalid. For the hardware length, it must be
     * 6 for the ethernet hardware address. For the protocol
     * address length, it must be 4 bytes for the IP protocol
     * 0x800 address length.
     */
    if (arp_extract_hdr (&Arp_pkt, pBuf) == ARP_FAILURE)

    {
        ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, DATA_PATH_TRC, ARP_NAME,
                 "Invalid Header Length (1)(arp_extract_hdr).\n");

        pArpCxt->Arp_stat.u4Bad_length++;
        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                      ARP_NAME, "Releasing buffer %x.\n", pBuf);
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_FAILURE;
    }

    pArpCxt->Arp_stat.u4Total_in_pkts++;

    /* Invalid address ..IP address same as that of our network. Dont learn 
     * the information */

    if (NetIpIfInfo.u4NetMask != IP_ADDR_31BIT_MASK)
    {
        if (NetIpIfInfo.u4Addr != 0)
        {
            if (Arp_pkt.u4Sproto_addr ==
                (NetIpIfInfo.u4Addr & NetIpIfInfo.u4NetMask))
            {
                pArpCxt->Arp_stat.u4Req_discards++;
                ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                              BUFFER_TRC, ARP_NAME,
                              "Releasing buffer %x.\n", pBuf);
                IP_RELEASE_BUF (pBuf, FALSE);
                return ARP_FAILURE;
            }
        }
        else
        {
            u1IfaceUNNUM = TRUE;
        }
    }

    if (IP_GET_BUF_LENGTH (pBuf) < (UINT4) IP_ARP_HDR_LEN)
    {

        ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, DATA_PATH_TRC, ARP_NAME,
                 "Protocol error. - Invalid Header Length.\n");

        pArpCxt->Arp_stat.u4Bad_length++;
        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                      ARP_NAME, "Releasing buffer %x.\n", pBuf);
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_FAILURE;
    }

    if ((Arp_pkt.i2Opcode != ARP_REQUEST) && (Arp_pkt.i2Opcode != ARP_RESPONSE))
    {

        ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, DATA_PATH_TRC, ARP_NAME,
                 "Protocol error. - Invalid Opcode.\n");

        /* Other high level protocols not supported */
        pArpCxt->Arp_stat.u4Bad_type++;
        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                      ARP_NAME, "Releasing buffer %x.\n", pBuf);
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_FAILURE;
    }

    ARP_PKT_DUMP (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, DUMP_TRC,
                  ARP_NAME, pBuf, IP_ARP_HDR_LEN, "Rcvd ARP packet from LL.\n");

    /* find if this is one of the supported hardware type. */
    if ((pHw_entry = arp_get_hw_entry (Arp_pkt.i2Hardware)) == NULL)
    {
        ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, DATA_PATH_TRC, ARP_NAME,
                 "Protocol error. - Invalid Hardware type.\n");

        pArpCxt->Arp_stat.u4Bad_type++;    /* ARP doesn't support this hw */
        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                      ARP_NAME, "Releasing buffer %x.\n", pBuf);
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_FAILURE;
    }

    /* Find if high level protocol is OK */
    if (Arp_pkt.i2Protocol != pHw_entry->i2Protocol)
    {
        /* Other high level protocols not supported */
        ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, DATA_PATH_TRC, ARP_NAME,
                 "Protocol error. - Unsupported high level Protocol.\n");
        pArpCxt->Arp_stat.u4Bad_type++;
        IP_RELEASE_BUF (pBuf, FALSE);
        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                      ARP_NAME, "Releasing buffer %x.\n", pBuf);
        return ARP_FAILURE;
    }

    /* Validate Lengths */
    if ((Arp_pkt.i1Hwalen > CFA_ENET_ADDR_LEN) ||
        (Arp_pkt.i1Palen > ARP_MAX_PROTO_ADDR_LEN))
    {
        ARP_TRC_ARG2 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, DATA_PATH_TRC,
                      ARP_NAME,
                      "Invalid length (2) Hardware Addr                                                                  length  %d.\t Protocol Addr Length %d\n",
                      Arp_pkt.i1Hwalen, Arp_pkt.i1Palen);

        pArpCxt->Arp_stat.u4Bad_length++;
        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                      ARP_NAME, "Releasing buffer %x.\n", pBuf);
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_FAILURE;
    }

    /* validate source MAC address.if source MAC is multicast or broadcast 
     * We should discard packet*/
    if (FS_UTIL_IS_MCAST_MAC (Arp_pkt.i1Shwaddr) == OSIX_TRUE)
    {
        pArpCxt->Arp_stat.u4Bad_address++;
        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                      ARP_NAME, "Releasing buffer %x.\n", pBuf);
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_FAILURE;
    }

#ifdef DUPLICATE_ARP_RESP_NOT_WANTED
    /* If the port receives ARP REQUEST irrespective of its having the Target Addres
       or not it sends ARP RESPONSE. To avoid this ARP RESPONSE from unwanted ports
       are suppressed. */
    if (((Arp_pkt.u4Sproto_addr != 0) &&
         ((Arp_pkt.u4Sproto_addr & NetIpIfInfo.u4NetMask) !=
          (NetIpIfInfo.u4Addr & NetIpIfInfo.u4NetMask))) ||
        ((Arp_pkt.u4Tproto_addr != 0) &&
         ((Arp_pkt.u4Tproto_addr & NetIpIfInfo.u4NetMask) !=
          (NetIpIfInfo.u4Addr & NetIpIfInfo.u4NetMask))))
    {
        pArpCxt->Arp_stat.u4Bad_address++;
        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                      ARP_NAME, "Releasing buffer %x.\n", pBuf);
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_FAILURE;
    }
#else

    if (Arp_pkt.u4Sproto_addr != 0)
    {
        MEMSET (&OutRtInfo, 0, sizeof (tNetIpv4RtInfo));
        MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

        RtQuery.u4DestinationIpAddress = Arp_pkt.u4Sproto_addr;
        RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
        RtQuery.u2AppIds = (UINT2) ALL_ROUTING_PROTOCOL;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
        RtQuery.u4ContextId = pArpCxt->u4ContextId;

        ARP_PROT_UNLOCK ();

        i4RetVal = NetIpv4GetRoute (&RtQuery, &OutRtInfo);

        ARP_PROT_LOCK ();

        if ((i4RetVal == NETIPV4_FAILURE) || (OutRtInfo.u2RtType !=
                                              IPROUTE_LOCAL_TYPE))
        {
            /* When interface address is zero, there is a chance that
             * arp reply may be for the address got through DHCP.DHCP
             * trigger arp for the address offered by DHCP server before
             * configuring the offered address to ensure no neighbour 
             * is having that IP address In this case,We should accept 
             * the Arp Reply and add the entry in cache */

            i4RetVal =
                NetIpv4IpIsLocalNetInCxt (pArpCxt->u4ContextId,
                                          Arp_pkt.u4Sproto_addr);

            if (i4RetVal == NETIPV4_FAILURE)
            {
#ifdef DHCPC_WANTED
                if (DhcpCheckOfferedIpOnInterface ((UINT4) u2Port,
                                                   Arp_pkt.u4Sproto_addr) !=
                    DHCP_SUCCESS)
#endif
                {
                    if (u1IfaceUNNUM != TRUE)
                    {
                        pArpCxt->Arp_stat.u4Bad_address++;
                        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                      BUFFER_TRC, ARP_NAME,
                                      "Releasing buffer %x.\n", pBuf);
                        IP_RELEASE_BUF (pBuf, FALSE);
                        return ARP_FAILURE;
                    }
                }
            }
        }
    }
#endif
    ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, CONTROL_PLANE_TRC,
                  ARP_NAME, "ARP Lookup for %x.\n", Arp_pkt.u4Sproto_addr);

    i4ArpCacheCheck = ArpLookupWithIndex (u4IfIndex, Arp_pkt.u4Sproto_addr,
                                          &ArpCache);

    if ((i4ArpCacheCheck == ARP_SUCCESS)
        && ((ArpCache.i1State != (INT1) ARP_STATIC) &&
            (ArpCache.i1State != (INT1) ARP_STATIC_NOT_IN_SERVICE) &&
            (ArpCache.i1State != (INT1) ARP_INVALID)))
    {
        /* If we have an entry corresponding to this update it with
         * the latest information.  Don't allow dynamic entries to overwrite
         * static entries. When the interfaces is disassociated with the 
         * static arp entry, the state of Static ARP entry is Invalid*/

        /* Validate Lengths */
        if (Arp_pkt.i1Hwalen > CFA_ENET_ADDR_LEN ||
            Arp_pkt.i1Palen > ARP_MAX_PROTO_ADDR_LEN)
        {
            ARP_TRC_ARG2 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, DATA_PATH_TRC,
                          ARP_NAME,
                          "Invalid Length (3) Hardware Addr length  %d.\t                                         Protocol Addr Length %d\n",
                          Arp_pkt.i1Hwalen, Arp_pkt.i1Palen);

            pArpCxt->Arp_stat.u4Bad_length++;
            ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                          ARP_NAME, "Releasing buffer %x.\n", pBuf);
            IP_RELEASE_BUF (pBuf, FALSE);
            return ARP_FAILURE;
        }

        MEMSET (&tArpData, 0, sizeof (tARP_DATA));
        tArpData.i2Hardware = Arp_pkt.i2Hardware;
        tArpData.u4IpAddr = Arp_pkt.u4Sproto_addr;
        tArpData.u2Port = u2Port;
        tArpData.i1Hwalen = CFA_ENET_ADDR_LEN;
        MEMCPY (tArpData.i1Hw_addr, Arp_pkt.i1Shwaddr, CFA_ENET_ADDR_LEN);
        tArpData.u1EncapType = (UINT1) pArpQMsg->u4EncapType;
        tArpData.i1State = (INT1) ARP_DYNAMIC;
        tArpData.u1RowStatus = ACTIVE;
        if (ArpAddArpEntry (tArpData, NULL, 0) == ARP_FAILURE)
        {
            IP_RELEASE_BUF (pBuf, FALSE);
            return ARP_FAILURE;
        }
#ifdef ICCH_WANTED
        if (ArpIsMclagEnabled (u4IfIndex) == ARP_SUCCESS)
        {
            /* Initiate ARP sync-up for MC-LAG */
            ArpIcchSyncArpMsg (&tArpData, u4IfIndex);
        }
#endif

    }
    if ((NetIpIfInfo.u4Addr != 0) || (u1IfaceUNNUM == TRUE))
    {
#if defined (VRRP_WANTED) && defined (IP_WANTED)
        if (VrrpEnabledOnInterface (u4IfIndex) == VRRP_OK)
        {
            u4TempAddr = OSIX_HTONL (Arp_pkt.u4Tproto_addr);
            IPVX_ADDR_INIT_FROMV4 (IpAddr, u4TempAddr);

            i4VrrpRetVal = VrrpGetVridFromAssocIpAddress
                (u4IfIndex, IpAddr, &i4OperVrId);

            if ((NetIpv4IfIsOurAddressInCxt (pArpCxt->u4ContextId,
                                             Arp_pkt.u4Tproto_addr)
                 == NETIPV4_SUCCESS) || (i4VrrpRetVal == VRRP_OK) ||
                (ArpCheckToSendProxyArpReply (pArpCxt->u4ContextId, u2Port,
                                              Arp_pkt.u4Sproto_addr,
                                              Arp_pkt.u4Tproto_addr) ==
                 ARP_SUCCESS))
            {
                if ((i4ArpCacheCheck == ARP_FAILURE) &&
                    (Arp_pkt.u4Sproto_addr != 0))
                {
                    /* Now we know that ARP packet is addressed to us.
                     * Create a new entry with the senders IP address.
                     * This is done irrespective of whether it 
                     * is REQUEST or RESPONSE. */
                    ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                  CONTROL_PLANE_TRC, ARP_NAME,
                                  "Adding an entry in cache for IP address "
                                  "%x.\n", Arp_pkt.u4Sproto_addr);

                    MEMSET (&tArpData, 0, sizeof (tARP_DATA));
                    tArpData.i2Hardware = Arp_pkt.i2Hardware;
                    tArpData.u4IpAddr = Arp_pkt.u4Sproto_addr;
                    tArpData.u2Port = u2Port;
                    tArpData.i1Hwalen = CFA_ENET_ADDR_LEN;
                    MEMCPY (tArpData.i1Hw_addr, Arp_pkt.i1Shwaddr,
                            CFA_ENET_ADDR_LEN);
                    tArpData.u1EncapType = (UINT1) pArpQMsg->u4EncapType;
                    tArpData.i1State = (INT1) ARP_DYNAMIC;
                    tArpData.u1RowStatus = ACTIVE;

                    if (ArpAddArpEntry (tArpData, NULL, 0) == ARP_FAILURE)
                    {
                        IP_RELEASE_BUF (pBuf, FALSE);
                        return ARP_FAILURE;
                    }
                }

                /* Now check if it is a request or response.
                 * We don't have anything to do in case of response 
                 * as the entry is already updated. 
                 * If it is a REQUEST then respond with our address. */

                if (Arp_pkt.i2Opcode == ARP_REQUEST)
                {
                    ARP_TRC_ARG3 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                  DATA_PATH_TRC, ARP_NAME,
                                  "rcvd ARP req on interface %d for %x from %x \n",
                                  NetIpIfInfo.u4Addr,
                                  Arp_pkt.u4Tproto_addr, Arp_pkt.u4Sproto_addr);

                    pArpCxt->Arp_stat.u4In_requests++;

                    /* Swap sender's and target's (us) hardware and protocol
                     * fields, and send the packet back as a reply
                     */
                    MEMCPY (Arp_pkt.i1Thwaddr, Arp_pkt.i1Shwaddr,
                            CFA_ENET_ADDR_LEN);

                    if (i4OperVrId != 0)
                    {
                        VrrpGetMacAddress (u4IfIndex, i4OperVrId,
                                           IPVX_ADDR_FMLY_IPV4,
                                           (UINT1 *) &au1SrcHwAddr);
                        MEMCPY (Arp_pkt.i1Shwaddr, au1SrcHwAddr,
                                CFA_ENET_ADDR_LEN);
                    }
                    else
                    {
                        if ((NetIpv4IfIsOurAddressInCxt (pArpCxt->u4ContextId,
                                                         Arp_pkt.u4Tproto_addr)
                             != NETIPV4_SUCCESS) &&
                            (NetIpIfInfo.u1ProxyArpAdminStatus ==
                             ARP_PROXY_DISABLE))
                        {
                            IP_RELEASE_BUF (pBuf, FALSE);
                            return ARP_FAILURE;
                        }
                        /* lets check in the array first.
                         * if hardware address is valid, we can use it.
                         * and improve little performance.
                         */

                        if (ARP_GET_ENET_ENTRY (u2Port)->u1HwAddrValidFlag
                            == TRUE)
                        {
                            MEMCPY (Arp_pkt.i1Shwaddr,
                                    ARP_GET_ENET_ENTRY (u2Port)->au1HwAddr,
                                    CFA_ENET_ADDR_LEN);
                        }
                        else
                        {
                            /* array is not having entry
                             *  call function to find hardware address 
                             */
                            if (pHw_entry->get_hw_address == NULL)
                            {
                                /* We can't reply for this request */
                                ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                         DATA_PATH_TRC, ARP_NAME,
                                         "Not Our Address\n");
                                ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId,
                                              ARP_MOD_TRC, BUFFER_TRC, ARP_NAME,
                                              "Releasing buffer %x.\n", pBuf);
                                IP_RELEASE_BUF (pBuf, FALSE);
                                pArpCxt->Arp_stat.u4Req_discards++;
                                return ARP_FAILURE;
                            }

                            /* get the hardware address corresponding to 
                               this interface */

                            if (pHw_entry->get_hw_address
                                ((UINT2) u4IfIndex,
                                 Arp_pkt.i1Shwaddr, NULL, NULL) != ARP_SUCCESS)

                            {
                                ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId,
                                              ARP_MOD_TRC, BUFFER_TRC, ARP_NAME,
                                              "Releasing buffer %x.\n", pBuf);

                                IP_RELEASE_BUF (pBuf, FALSE);
                                pArpCxt->Arp_stat.u4Req_discards++;
                                return ARP_FAILURE;
                            }

                            /* at this point, hardware address is captured.
                             * copy it in array for future reference.
                             */

                            MEMCPY (ARP_GET_ENET_ENTRY (u2Port)->au1HwAddr,
                                    Arp_pkt.i1Shwaddr, CFA_ENET_ADDR_LEN);
                        }

                    }

                    Arp_pkt.i2Opcode = ARP_RESPONSE;

                    /* Use the target IP address present in the 
                     * packet itself. */
                    u4SrcAddress = Arp_pkt.u4Tproto_addr;
                    /*That sent REQ */
                    Arp_pkt.u4Tproto_addr = Arp_pkt.u4Sproto_addr;
                    Arp_pkt.u4Sproto_addr = u4SrcAddress;

                    /* Validate Lengths */
                    if (Arp_pkt.i1Hwalen > CFA_ENET_ADDR_LEN ||
                        Arp_pkt.i1Palen > ARP_MAX_PROTO_ADDR_LEN)
                    {
                        ARP_TRC_ARG2 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                      DATA_PATH_TRC, ARP_NAME,
                                      "Invalid Length (4) Hardware Addr length  %d.\t                                                   Protocol Addr Length %d\n",
                                      Arp_pkt.i1Hwalen, Arp_pkt.i1Palen);

                        pArpCxt->Arp_stat.u4Bad_length++;
                        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                      BUFFER_TRC, ARP_NAME,
                                      "Releasing buffer %x.\n", pBuf);
                        IP_RELEASE_BUF (pBuf, FALSE);
                        return ARP_FAILURE;
                    }

                    arp_put_hdr (pBuf, &Arp_pkt);

                    pArpCxt->Arp_stat.u4Out_Replies++;

                    i4Tmp =
                        CfaHandlePktFromIp (pBuf, u4IfIndex,
                                            0, (UINT1 *) Arp_pkt.i1Thwaddr,
                                            ARP_PROTOCOL, ARP_UCAST, 0);

                    if (i4Tmp != CFA_SUCCESS)
                    {
                        ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                 DATA_PATH_TRC, ARP_NAME,
                                 "Queue Over flow - Enqueue Failure\n");
                        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                      BUFFER_TRC, ARP_NAME,
                                      "Releasing buffer %x.\n", pBuf);
                        IP_RELEASE_BUF (pBuf, FALSE);
                        i4Tmp = ARP_FAILURE;
                    }
                    else
                    {
                        ARP_PKT_DUMP (NetIpIfInfo.u4ContextId,
                                      ARP_MOD_TRC, DUMP_TRC, ARP_NAME,
                                      pBuf, IP_ARP_HDR_LEN,
                                      "Sending ARP Reply .\n");
                        i4Tmp = ARP_SUCCESS;
                    }

                    return i4Tmp;
                }
                else
                {
                    ARP_TRC_ARG3 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                  DATA_PATH_TRC, ARP_NAME,
                                  "rcvd ARP reply on interface %d for %x "
                                  "from %x \n", u4IfIndex,
                                  Arp_pkt.u4Tproto_addr, Arp_pkt.u4Sproto_addr);

                    pArpCxt->Arp_stat.u4Replies++;    /* Response packet */
                }
            }
        }
        else
        {
#endif /* VRRP_WANTED && IP_WANTED */
#ifdef NAT_WANTED
            if (CFA_CHECK_IF_NAT_ENABLE (u4IfIndex) == NAT_ENABLE)
            {
                /* Search in the NAT Static Table */
                u4IpAddress = NatSearchStaticTable (Arp_pkt.u4Tproto_addr,
                                                    NAT_INBOUND, u4IfIndex);

                if (u4IpAddress == 0)
                {
                    i1Status =
                        NatSearchGlobalIpAddrTable (Arp_pkt.u4Tproto_addr,
                                                    u4IfIndex);
                    if (NAT_SUCCESS != i1Status)
                    {
                        i1NatPolicyStatus =
                            NatApiSearchPolicyTable (Arp_pkt.u4Tproto_addr);
                    }
                }

                if ((u4IpAddress != 0) || (i1Status == NAT_SUCCESS) ||
                    (NAT_SUCCESS == i1NatPolicyStatus))
                {
                    i1IsNatIpEntry = TRUE;
                }

            }
#endif

            if ((NetIpv4IfIsOurAddressInCxt (pArpCxt->u4ContextId,
                                             Arp_pkt.u4Tproto_addr)
                 == NETIPV4_SUCCESS)
#ifdef NAT_WANTED
                || (i1IsNatIpEntry == TRUE)
#endif
#ifdef IP_WANTED
                || (ArpCheckToSendProxyArpReply (pArpCxt->u4ContextId, u2Port,
                                                 Arp_pkt.u4Sproto_addr,
                                                 Arp_pkt.u4Tproto_addr) ==
                    ARP_SUCCESS)
#endif
                )
            {
                if ((i4ArpCacheCheck == ARP_FAILURE) &&
                    (Arp_pkt.u4Sproto_addr != 0))
                {
                    /* Now we know that ARP packet is addressed to us.
                     * Create a new entry with the senders IP address.
                     * This is done irrespective of whether it is 
                     * REQUEST or RESPONSE. */
                    ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                  CONTROL_PLANE_TRC, ARP_NAME,
                                  "Adding an entry in cache for IP address %x.\n",
                                  Arp_pkt.u4Sproto_addr);

                    MEMSET (&tArpData, 0, sizeof (tARP_DATA));
                    tArpData.i2Hardware = Arp_pkt.i2Hardware;
                    tArpData.u4IpAddr = Arp_pkt.u4Sproto_addr;
                    tArpData.u2Port = u2Port;
                    tArpData.i1Hwalen = CFA_ENET_ADDR_LEN;
                    MEMCPY (tArpData.i1Hw_addr, Arp_pkt.i1Shwaddr,
                            CFA_ENET_ADDR_LEN);
                    tArpData.u1EncapType = (UINT1) pArpQMsg->u4EncapType;
                    tArpData.i1State = (INT1) ARP_DYNAMIC;
                    tArpData.u1RowStatus = ACTIVE;

                    if (ArpAddArpEntry (tArpData, NULL, 0) == ARP_FAILURE)
                    {
                        IP_RELEASE_BUF (pBuf, FALSE);
                        return ARP_FAILURE;
                    }
                }

                /* Now check if it is a request or response.
                 * We don't have anything to do in case of response 
                 * as the entry is already updated. If it is 
                 * a REQUEST then respond with our address. */

                if (Arp_pkt.i2Opcode == ARP_REQUEST)
                {
                    ARP_TRC_ARG3 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                  DATA_PATH_TRC, ARP_NAME,
                                  "rcvd ARP req on interface %d for %x from %x \n",
                                  u4IfIndex, Arp_pkt.u4Tproto_addr,
                                  Arp_pkt.u4Sproto_addr);

                    pArpCxt->Arp_stat.u4In_requests++;

#ifdef LNXIP4_WANTED
                    /* If LINUX_IP is enabled, LIUNX ARP module would have 
                     * received the ARP Request and sent ARP Reply. 
                     * So return at this point without worrying about the 
                     * ARP Reply.  */
                    IP_RELEASE_BUF (pBuf, FALSE);
                    return (ARP_SUCCESS);
#endif /* LNXIP4_WANTED */

#ifdef IP_WANTED
                    /* Swap sender's and target's (us) hardware and protocol
                     * fields, and send the packet back as a reply
                     */
                    MEMCPY (Arp_pkt.i1Thwaddr, Arp_pkt.i1Shwaddr,
                            Arp_pkt.i1Hwalen);

                    /* lets check in the array first.
                     * if hardware address is valid, we can use it.
                     * and improve little performance.
                     */

                    if (ARP_GET_ENET_ENTRY (u2Port)->u1HwAddrValidFlag == TRUE)
                    {
                        MEMCPY (Arp_pkt.i1Shwaddr,
                                ARP_GET_ENET_ENTRY (u2Port)->au1HwAddr,
                                CFA_ENET_ADDR_LEN);
                    }
                    else
                    {
                        /* array is not having entry
                         *  call function to find hardware address 
                         */
                        if (pHw_entry->get_hw_address == NULL)
                        {
                            /* We can't reply for this request */
                            ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                     DATA_PATH_TRC, ARP_NAME,
                                     "Not Our Address\n");
                            ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                          BUFFER_TRC, ARP_NAME,
                                          "Releasing buffer %x.\n", pBuf);
                            IP_RELEASE_BUF (pBuf, FALSE);
                            pArpCxt->Arp_stat.u4Req_discards++;
                            return ARP_FAILURE;
                        }

                        /* get the hardware address corresponding to 
                         * this interface */
                        if (pHw_entry->get_hw_address
                            ((UINT2) u4IfIndex,
                             Arp_pkt.i1Shwaddr, NULL, NULL) != ARP_SUCCESS)
                        {
                            ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                          BUFFER_TRC, ARP_NAME,
                                          "Releasing buffer %x.\n", pBuf);

                            IP_RELEASE_BUF (pBuf, FALSE);
                            pArpCxt->Arp_stat.u4Req_discards++;
                            return ARP_FAILURE;
                        }

                        /* at this point, hardware address is captured.
                         * copy it in array for future reference.
                         */

                        MEMCPY (ARP_GET_ENET_ENTRY (u2Port)->au1HwAddr,
                                Arp_pkt.i1Shwaddr, CFA_ENET_ADDR_LEN);
                    }

#ifdef EVPN_VXLAN_WANTED
                    if (ArpEvpnLookupWithIpAddr (Arp_pkt.u4Tproto_addr,
                        &ArpCache) == ARP_SUCCESS)
                    {
                        MEMCPY (Arp_pkt.i1Shwaddr, ArpCache.i1Hw_addr, 
                                CFA_ENET_ADDR_LEN);      
                    }
#endif
                    Arp_pkt.i2Opcode = ARP_RESPONSE;

                    u4SrcAddress = Arp_pkt.u4Tproto_addr;
                    Arp_pkt.u4Tproto_addr = Arp_pkt.u4Sproto_addr;
                    Arp_pkt.u4Sproto_addr = u4SrcAddress;

                    /* Validate Lengths */
                    if (Arp_pkt.i1Hwalen > CFA_ENET_ADDR_LEN ||
                        Arp_pkt.i1Palen > ARP_MAX_PROTO_ADDR_LEN)
                    {
                        ARP_TRC_ARG2 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                      DATA_PATH_TRC, ARP_NAME,
                                      "Invalid Length (5) Hardware Addr length  %d.\t                                          Protocol Addr Length %d\n",
                                      Arp_pkt.i1Hwalen, Arp_pkt.i1Palen);

                        pArpCxt->Arp_stat.u4Bad_length++;

                        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                      BUFFER_TRC, ARP_NAME,
                                      "Releasing buffer %x.\n", pBuf);
                        IP_RELEASE_BUF (pBuf, FALSE);
                        return ARP_FAILURE;
                    }

                    arp_put_hdr (pBuf, &Arp_pkt);

                    pArpCxt->Arp_stat.u4Out_Replies++;

                    i4Tmp =
                        CfaHandlePktFromIp (pBuf, u4IfIndex, 0,
                                            (UINT1 *) Arp_pkt.i1Thwaddr,
                                            ARP_PROTOCOL, ARP_UCAST, 0);

                    if (i4Tmp != CFA_SUCCESS)
                    {
                        ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                 DATA_PATH_TRC, ARP_NAME,
                                 "Queue Over flow - Enqueue Failure\n");
                        ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                      BUFFER_TRC, ARP_NAME,
                                      "Releasing buffer %x.\n", pBuf);
                        IP_RELEASE_BUF (pBuf, FALSE);
                        i4Tmp = ARP_FAILURE;
                    }
                    else
                    {
                        ARP_PKT_DUMP (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                      DUMP_TRC, ARP_NAME, pBuf,
                                      IP_ARP_HDR_LEN, "Sending ARP Reply.\n");
                        i4Tmp = ARP_SUCCESS;
                    }

                    return i4Tmp;
#endif /* IP_WANTED */

                }
                else
                {
                    ARP_TRC_ARG3 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC,
                                  DATA_PATH_TRC, ARP_NAME,
                                  "rcvd ARP reply on interface %d for %x from %x \n",
                                  u4IfIndex, Arp_pkt.u4Tproto_addr,
                                  Arp_pkt.u4Sproto_addr);

		    pArpCxt->Arp_stat.u4Replies++;    /* Response packet */
#ifdef NPAPI_WANTED
		    u1NextHopFlag =
			    RtmApiIsReachableNextHopInCxt (pArpCxt->u4ContextId,
					    Arp_pkt.u4Sproto_addr);
		    ArpFsNpIpv4VrfCheckHitOnArpEntry (pArpCxt->u4ContextId, Arp_pkt.u4Sproto_addr,                                                                                        u1NextHopFlag);
#endif
		}
            }
            else
            {

                pArpCxt->Arp_stat.u4Req_discards++;
            }
#if defined (VRRP_WANTED) && defined (IP_WANTED)
        }
#endif /*VRRP_WANTED && IP_WANTED */

    }
    ARP_TRC_ARG1 (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, BUFFER_TRC,
                  ARP_NAME, "Releasing buffer %x.\n", pBuf);
    IP_RELEASE_BUF (pBuf, FALSE);

    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : ArpProcessQMsg
 *
 * Input(s)           : pArpQMsg - pointer to a tArpQMsg structure.
 *
 * Output(s)          : None.
 
 * Returns            : ARP_SUCCESS or ARP_FAILURE
 *
 * Action :
 *
 * Calls arp_add which adds a cache entry and sends an ARP request to 
 * obtain the hardware address for the destination.  The cache entry 
 * is updated and kept in PENDING state. The data is enqueued as 
 * pending data so that it can be sent when a response arrives for 
 * this request.
+-------------------------------------------------------------------*/
INT4
ArpProcessQMsg (tArpQMsg * pArpQMsg)
{
    tArpCxt            *pArpCxt = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;
    tARP_DATA           tArpData;
    UINT1               u1EncapType = 0;
    INT2                i2Hardware = 0;

    if (NetIpv4GetIfInfo ((UINT4) pArpQMsg->u2Port, &NetIpIfInfo)
        != NETIPV4_SUCCESS)
    {
        ARP_GBL_TRC_ARG1 (ARP_MOD_TRC, BUFFER_TRC, ARP_NAME,
                          "Releasing buffer %x.\n", pArpQMsg->pBuf);
        IP_RELEASE_BUF (pArpQMsg->pBuf, FALSE);
        return ARP_FAILURE;
    }

    pArpCxt = gArpGblInfo.apArpCxt[NetIpIfInfo.u4ContextId];

    i2Hardware = ARP_IFACE_TYPE ((INT2) NetIpIfInfo.u4IfType);

    u1EncapType = NetIpIfInfo.u1EncapType;

    if (u1EncapType == ARP_ENET_AUTO_ENCAP)
    {
        u1EncapType = ARP_ENET_V2_ENCAP;
    }

    MEMSET (&tArpData, 0, sizeof (tARP_DATA));
    tArpData.i2Hardware = i2Hardware;
    tArpData.u4IpAddr = pArpQMsg->u4IpAddr;
    tArpData.u2Port = pArpQMsg->u2Port;
    tArpData.u1EncapType = u1EncapType;
    tArpData.i1State = (INT1) ARP_PENDING;
    /*tArpData.u1RowStatus = ACTIVE; */

    /* In this fun SrcModId is included to send the packet to cfa 
     * * in case of IP.  */
    if (ArpAddArpEntry (tArpData, pArpQMsg->pBuf, pArpQMsg->u1SrcModId)
        != ARP_SUCCESS)
    {
        pArpCxt->Arp_stat.u4Out_drops++;
        return ARP_FAILURE;
    }
#if defined (LNXIP4_WANTED) && defined (LINUX_ARP_NETLINK)
    /* When LNXIP switch is defined the entry will be added as
     * pending entry using ArpAddArpEntry but since the timer 
     * should not be started for pending entries in LNXIP, 
     * timer will not be started and req will not be sent
     * This is a special scenario where other protocols in user space
     * request for resolving the entry. 
     * Hence this function is used to send req and start the timer
     * when requested from the higher layers in lnxip alone.
     */
    ArpSendReqAndStartTimer (tArpData);
#endif
    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : arp_extract_hdr
 *
 * Input(s)           : pArp_pkt, pBuf
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 * Extracts ARP packet into a local structure.
 *
+-------------------------------------------------------------------*/
INT4
arp_extract_hdr (t_ARP_PKT * pArp_pkt, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               au1ArpHdr[ARP_PACKET_LEN];
    UINT1               u1HwaLen = 0;
    UINT1               u1PaLen = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4TgtIp = 0;
    UINT4               u4Offset = 0;

    tArpHeader         *pArpHdr = NULL;

    if ((pArpHdr =
         (tArpHeader *) (VOID *) ARP_GET_DATA_PTR_IF_LINEAR (pBuf, 0,
                                                             sizeof
                                                             (tArpHeader))) ==
        NULL)
    {
        pArpHdr = (tArpHeader *) (VOID *) au1ArpHdr;
        IP_COPY_FROM_BUF (pBuf, (UINT1 *) pArpHdr, 0, sizeof (tArpHeader));
    }

    u1HwaLen = pArpHdr->u1HwaLen;
    u1PaLen = pArpHdr->u1PaLen;

    if (u1HwaLen > CFA_ENET_ADDR_LEN || u1PaLen > ARP_MAX_PROTO_ADDR_LEN)
    {
        return (ARP_FAILURE);
    }
    pArp_pkt->i2Hardware = IP_NTOHS (pArpHdr->u2HwType);
    pArp_pkt->i2Protocol = IP_NTOHS (pArpHdr->u2ProtType);
    pArp_pkt->i1Hwalen = CFA_ENET_ADDR_LEN;
    pArp_pkt->i1Palen = ARP_MAX_PROTO_ADDR_LEN;
    pArp_pkt->i2Opcode = IP_NTOHS (pArpHdr->u2Opcode);

    /* Copy the remaining of the packet from pBuf */
    u4Offset = sizeof (tArpHeader);

    IP_COPY_FROM_BUF (pBuf, &au1ArpHdr[u4Offset], u4Offset,
                      ((CFA_ENET_ADDR_LEN + ARP_MAX_PROTO_ADDR_LEN) * 2));

    /* Copy the Sender Hardware address */
    MEMCPY (pArp_pkt->i1Shwaddr, &au1ArpHdr[u4Offset], CFA_ENET_ADDR_LEN);
    u4Offset += CFA_ENET_ADDR_LEN;

    /* Copy the Sender Protocol address */

    MEMCPY (&u4SrcIp, &au1ArpHdr[u4Offset], ARP_MAX_PROTO_ADDR_LEN);
    u4Offset += ARP_MAX_PROTO_ADDR_LEN;

    /* Copy the Target Hardware address */

    MEMCPY (pArp_pkt->i1Thwaddr, &au1ArpHdr[u4Offset], CFA_ENET_ADDR_LEN);
    u4Offset += CFA_ENET_ADDR_LEN;

    /* Copy the Target Protocol address */

    MEMCPY (&u4TgtIp, &au1ArpHdr[u4Offset], ARP_MAX_PROTO_ADDR_LEN);
    u4Offset += ARP_MAX_PROTO_ADDR_LEN;

    pArp_pkt->u4Sproto_addr = IP_NTOHL (u4SrcIp);
    pArp_pkt->u4Tproto_addr = IP_NTOHL (u4TgtIp);

    return (ARP_SUCCESS);

}

/*-------------------------------------------------------------------+
 * Function           : ArpTestForRouteInCxt   
 *
 * Input(s)           : u4Addr, u2Port
 *
 * Output(s)          : None
 *
 * Returns            : ARP_SUCCESS, ARP_FAILURE
 *
 * Action :
 * Uses NetIpv4GetFwdTableRouteEntry to check if route exists in 
 * the virtual router.
 *
+-------------------------------------------------------------------*/
INT4
ArpTestForRouteInCxt (UINT4 u4ContextId, UINT4 u4Addr, UINT2 u2Port)
{
    /* The function is used only by arp_input function.
     * Hence, rather using the Netip call to obtain the context information
     * from the  port number, the context id is passed 
     * as an additional argument */

    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    RtQuery.u4DestinationIpAddress = u4Addr;
    RtQuery.u4DestinationSubnetMask = ARP_DEF_NET_MASK;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    RtQuery.u4ContextId = u4ContextId;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        if (NetIpRtInfo.u4RtIfIndx != (UINT4) u2Port)
        {
            return ARP_SUCCESS;
        }
    }

    return ARP_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : arp_put_hdr
 *
 * Input(s)           : pBuf, pArp_pkt
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Puts the constructed ARP packet into message buffer.
 * Only actual hardware addresses are copied. (actual length)
 *
+-------------------------------------------------------------------*/
VOID
arp_put_hdr (tCRU_BUF_CHAIN_HEADER * pBuf, t_ARP_PKT * pArp_pkt)
{
    UINT1               au1ArpHdr[ARP_PACKET_LEN];
    UINT4               u4SrcIp = 0;
    UINT4               u4TgtIp = 0;
    UINT4               u4Offset = 0;

    tArpHeader         *pArpHdr = NULL;

    pArpHdr = (tArpHeader *) (VOID *) au1ArpHdr;

    pArpHdr->u2HwType = IP_HTONS (pArp_pkt->i2Hardware);
    pArpHdr->u2ProtType = IP_HTONS (pArp_pkt->i2Protocol);
    pArpHdr->u1HwaLen = CFA_ENET_ADDR_LEN;
    pArpHdr->u1PaLen = ARP_MAX_PROTO_ADDR_LEN;
    pArpHdr->u2Opcode = IP_HTONS (pArp_pkt->i2Opcode);

    u4SrcIp = IP_HTONL (pArp_pkt->u4Sproto_addr);
    u4TgtIp = IP_HTONL (pArp_pkt->u4Tproto_addr);

    /* Copy the Sender Hardware address */

    u4Offset = sizeof (tArpHeader);
    MEMCPY (&au1ArpHdr[u4Offset], pArp_pkt->i1Shwaddr, CFA_ENET_ADDR_LEN);
    u4Offset += CFA_ENET_ADDR_LEN;

    /* Copy the Sender Protocol address */

    MEMCPY (&au1ArpHdr[u4Offset], &u4SrcIp, ARP_MAX_PROTO_ADDR_LEN);
    u4Offset += ARP_MAX_PROTO_ADDR_LEN;

    /* Copy the Target Hardware address */

    MEMCPY (&au1ArpHdr[u4Offset], pArp_pkt->i1Thwaddr, CFA_ENET_ADDR_LEN);
    u4Offset += CFA_ENET_ADDR_LEN;

    /* Copy the Target Protocol address */

    MEMCPY (&au1ArpHdr[u4Offset], &u4TgtIp, ARP_MAX_PROTO_ADDR_LEN);
    u4Offset += ARP_MAX_PROTO_ADDR_LEN;

    /* Copy the message to the buffer, The size of Msg will be u4Offset */

    IP_COPY_TO_BUF (pBuf, au1ArpHdr, 0, u4Offset);
}

#ifdef IP_WANTED
/*-------------------------------------------------------------------+
 * Function           : ArpCheckToSendProxyArpReply                                    
 *
 * Input(s)           : u2SrcPort - IP Port Number 
 *                      u4SrcIpAddr - Source Ip address in ARP packet
 *                      u4TgtIpAddr - Target Ip address in ARP packet
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS/ARP_FAILURE.
 *
 * Action :
 * Core Module of the Proxy ARP Feature, based on RFC 1027.
 * Checks if ARP Response to be sent to the Requesting Host, based
 * on checks like Sanity(Source and Target interfaces enabled with Proxy ARP feature,
 * Source and Target in same network and different sub-network,
 * Route to reach the target, Route should be a default route.  
 *
+-------------------------------------------------------------------*/
INT4
ArpCheckToSendProxyArpReply (UINT4 u4ContextId, UINT2 u2SrcPort,
                             UINT4 u4SrcIpAddr, UINT4 u4TgtIpAddr)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT1               u1QueryFlag;
    UINT4               u4SrcSubnet;
    UINT4               u4TgtSubnet;
#ifdef EVPN_VXLAN_WANTED
    t_ARP_CACHE         ArpCache;

    MEMSET (&ArpCache, 0, sizeof (ArpCache));
    
    if (ArpEvpnLookupWithIpAddr (u4TgtIpAddr, &ArpCache) == ARP_SUCCESS)
    {
        return ARP_SUCCESS;
    }
#endif

    /* Extract Proxy ARP Feature Status Variable for a given Interface */

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetIfInfo ((UINT4) u2SrcPort, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), ARP_NAME,
                 "ArpCheckToSendProxyArpReply: u1ProxyArpAdminStatus extraction failed\n");

        return ARP_FAILURE;
    }

    /* Check if Proxy ARP Feature ENABLED for a given Interface */
    if (NetIpIfInfo.u1ProxyArpAdminStatus == ARP_PROXY_DISABLE)
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                      (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), ARP_NAME,
                      "ArpCheckToSendProxyArpReply: Proxy ARP Disabled for "
                      "Source Interface - %s.\n", NetIpIfInfo.au1IfName);

        return ARP_FAILURE;
    }

    /************************ SANITY CHECK ENDS ************************/

    /* Check if the Target Host is reachable by giving the Target IP
       Care should be taken here to choose between "RTM_QUERIED_FOR_NEXT_HOP"
       and "RTM_QUERIED_FOR_EXACT_DEST" */

    u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    if ((ArpCheckDestRouteForProxyArpReply (u4ContextId, u4TgtIpAddr, u2SrcPort,
                                            u1QueryFlag,
                                            &NetIpRtInfo)) != ARP_SUCCESS)
    {
        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), ARP_NAME,
                 "ArpCheckToSendProxyArpReply: No Route to Target!!!\n");
        return ARP_FAILURE;
    }

    /* Check Target IP Address is not a Broadcast Address */
    if (ArpBroadcastCheckforProxyArp (u4TgtIpAddr,
                                      NetIpRtInfo.u4DestMask) == ARP_FAILURE)
    {
        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), ARP_NAME,
                 "ArpCheckToSendProxyArpReply: Target Address is a Broadcast!!!\n");
        return ARP_FAILURE;
    }

    /* Local proxy arp is disabled or Subnet option is 
     * enabled means, proxy arp feature will be available
     * only in different subnet */
    if (NetIpIfInfo.u1LocalProxyArpStatus == ARP_PROXY_DISABLE)
    {
        /* Extract the Source SUBNET Number by masking Source IP Address
           with the (Sub)NetMask of the Interface in which the ARP Request
           was received */
        u4SrcSubnet = u4SrcIpAddr & NetIpIfInfo.u4NetMask;

        /* Get the Destination IP Interface details */
        if (NetIpv4GetIfInfo (NetIpRtInfo.u4RtIfIndx, &NetIpIfInfo) ==
            NETIPV4_FAILURE)
        {
            return ARP_FAILURE;
        }

        /* Extract the Target SUBNET Number by masking Target IP Address
           with the (Sub)NetMask of the Interface in which the path to the
           Target exists */
        u4TgtSubnet = u4TgtIpAddr & NetIpIfInfo.u4NetMask;

        /* Check if Source & Target host IP Address belong to different Subnet */
        if (u4SrcSubnet == u4TgtSubnet)
        {
            ARP_TRC_ARG2 (u4ContextId, ARP_MOD_TRC,
                          (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), ARP_NAME,
                          "ArpCheckToSendProxyArpReply: Matching Source Subnet %x & "
                          "Target Subnet %x\n", u4SrcSubnet, u4TgtSubnet);

            return ARP_FAILURE;
        }
    }
    /* Check if Target Interface is enabled for Proxy ARP */
    if (NetIpIfInfo.u1ProxyArpAdminStatus == ARP_PROXY_DISABLE)
    {

        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC,
                      (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), ARP_NAME,
                      "ArpCheckToSendProxyArpReply: Proxy ARP Disabled for Target "
                      "Interface - %s.\n", NetIpIfInfo.au1IfName);
        return ARP_FAILURE;
    }
    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : ArpCheckDestRouteForProxyArpReply
 *
 * Input(s)           : u4DestIpAddr, u2Port, u1QueryFlag
 *
 * Output(s)          : pNetIpRtInfo
 *
 * Returns            : ARP_SUCCESS, ARP_FAILURE
 *
 * Action :
 * Checks if route exists to the Target host. Also checks if
 * the target interface is not that of the source interface
+-------------------------------------------------------------------*/
INT4
ArpCheckDestRouteForProxyArpReply (UINT4 u4ContextId, UINT4 u4DestIpAddr,
                                   UINT2 u2Port, UINT1 u1QueryFlag,
                                   tNetIpv4RtInfo * pNetIpRtInfo)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    tRtInfoQueryMsg     RtQuery;

    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    RtQuery.u4DestinationIpAddress = u4DestIpAddr;
    RtQuery.u4DestinationSubnetMask = ARP_DEF_NET_MASK;
    RtQuery.u1QueryFlag = u1QueryFlag;
    RtQuery.u4ContextId = u4ContextId;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetIfInfo ((UINT4) u2Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        ARP_TRC (u4ContextId, ARP_MOD_TRC,
                 (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), ARP_NAME,
                 "ArpCheckToSendProxyArpReply: interface info "
                 "is not available!!!\n");
        return ARP_FAILURE;
    }

    if (NetIpv4GetRoute (&RtQuery, pNetIpRtInfo) == NETIPV4_SUCCESS)
    {
        /* Subnetoption is disabled or local proxy is enabled means,
         * proxy support is for both diff and same subnets */
        if (NetIpIfInfo.u1LocalProxyArpStatus == ARP_PROXY_DISABLE)
        {
            if (pNetIpRtInfo->u4RtIfIndx == (UINT4) u2Port)
            {
                return ARP_FAILURE;
            }
        }

        /* Check if the Route returned is not the DEFAULT Route */
        if ((pNetIpRtInfo->u4DestNet == 0) && (pNetIpRtInfo->u4DestMask == 0))
        {
            ARP_TRC (u4ContextId, ARP_MOD_TRC,
                     (CONTROL_PLANE_TRC | ALL_FAILURE_TRC), ARP_NAME,
                     "ArpCheckToSendProxyArpReply: Only Default Route "
                     "identified for Target!!!\n");
            return ARP_FAILURE;
        }

        return ARP_SUCCESS;
    }
    return ARP_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : ArpBroadcastCheckforProxyArp
 *
 * Input(s)           : u4DestIpAddr, u4DestMask
 *
 * Output(s)          : None
 *
 * Returns            : ARP_SUCCESS, ARP_FAILURE
 *
 * Action :
 * Checks if the Target IP Address in one of those Four Broadcast
 * Address Types, as mentioned in RFC 1027
+-------------------------------------------------------------------*/
INT4
ArpBroadcastCheckforProxyArp (UINT4 u4DestIpAddr, UINT4 u4DestMask)
{
    UINT4               u4NetMask;
    UINT4               u4SubnetMask;
    UINT4               u4TmpMaskAddr;

    /* Extract the Subnet Mask of the Interface */
    u4SubnetMask = u4DestMask;
    u4SubnetMask = ~u4SubnetMask;
    u4TmpMaskAddr = u4DestIpAddr & u4SubnetMask;

    /* Check if the Target IP is any of those two types of
       Subnet Broadcast Address */
    if ((u4TmpMaskAddr == (IP_GEN_BCAST_ADDR & u4SubnetMask)) ||
        (u4TmpMaskAddr == (IP_ANY_ADDR & u4SubnetMask)))
    {
        return ARP_FAILURE;
    }

    /* Extract the Default NetMask for this Target IP Address */
    u4NetMask = IpGetDefaultNetmask (u4DestIpAddr);
    u4NetMask = ~u4NetMask;
    u4TmpMaskAddr = u4DestIpAddr & u4NetMask;

    /* Check if the Target IP is any of those two types of
       IP Directed Network Broadcast Address */
    if ((u4TmpMaskAddr == (IP_GEN_BCAST_ADDR & u4NetMask)) ||
        (u4TmpMaskAddr == (IP_ANY_ADDR & u4NetMask)))
    {
        return ARP_FAILURE;
    }

    return (ARP_SUCCESS);

}
#endif
