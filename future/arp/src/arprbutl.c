
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arprbutl.c,v 1.1 2013/11/29 11:08:51 siva Exp $
 *
 * Description: This file contains RB tree utilities regarding            
 *              ARP_ARRAY_TO_RBTREE_WANTED flag. 
 *
 *******************************************************************/

#include "arpinc.h"

/*--------  RBTREE UTILITIES FOR gArpGblInfo.ArpEnetTable -----------*/

/*****************************************************************************/
/* Function Name      : ArpCreateArpEnetTable                                */
/*                                                                           */
/* Description        : This routine creates gArpGblInfo.ArpEnetTable.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ARP_SUCCESS/ARP_FAILURE                              */
/*****************************************************************************/
INT4
ArpCreateArpEnetTable ()
{
    /* RBTree creation for ArpEnetTable. */
    gArpGblInfo.ArpEnetTable = RBTreeCreateEmbedded (0, ArpEnetTableCmp);

    if (gArpGblInfo.ArpEnetTable == NULL)
    {
        ARP_GBL_TRC (ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                     "RB Tree creation for ArpEnetTable is failed\r\n");
        return ARP_FAILURE;
    }
    return ARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ArpDeleteArpEnetTable                                */
/*                                                                           */
/* Description        : This routine deletes gArpGblInfo.ArpEnetTable.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
ArpDeleteArpEnetTable ()
{
    /* RBTree deletion for ArpEnetTable. */
    ArpDelAllArpEnetTableEntries ();
    if (gArpGblInfo.ArpEnetTable != NULL)
    {
        RBTreeDestroy (gArpGblInfo.ArpEnetTable, NULL, 0);
        gArpGblInfo.ArpEnetTable = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : ArpEnetTableCmp                                      */
/*                                                                           */
/* Description        : This routine is used in gArpGblInfo.ArpEnetTable for */
/*                      comparing two keys used in RBTree functionality.     */
/*                                                                           */
/* Input(s)           : pNode     - Key 1                                    */
/*                      pNodeIn   - Key 2                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
ArpEnetTableCmp (tRBElem * Node,
                 tRBElem * NodeIn)
{
    tArpEthernetEntry     *pNode = NULL;
    tArpEthernetEntry     *pNodeIn = NULL;
  
    pNode = (tArpEthernetEntry *)Node;
    pNodeIn = (tArpEthernetEntry *) NodeIn;

    /* key 1 --> Port identifier.
     */
    if (pNode->u2Port < pNodeIn->u2Port)
    {
        return (ARP_RB_LESSER);
    }
    else if (pNode->u2Port > pNodeIn->u2Port)
    {
        return (ARP_RB_GREATER);
    }
    return (ARP_RB_EQUAL);
}

/*****************************************************************************/
/* Function Name      : ArpAddArpEnetTableEntry                              */
/*                                                                           */
/* Description        : This routine add a tArpEthernetEntry  in             */
/*                      gArpGblInfo.ArpEnetTable.                            */
/*                                                                           */
/* Input(s)           : u2Port    - Port identifier.                         */
/*                                                                           */
/* Output(s)          : ppArpEthernetEntry  - pointer to the pArpEthernetEntry.*/
/*                                                                           */
/* Return Value(s)    : ARP_SUCCESS / ARP_FAILURE                            */
/*****************************************************************************/
INT4
ArpAddArpEnetTableEntry (UINT2 u2Port, 
                         tArpEthernetEntry **ppArpEthernetEntry)
{
    (*ppArpEthernetEntry) = NULL;

    if (((*ppArpEthernetEntry) = ArpGetArpEnetTableEntry (u2Port)) != NULL)
    {
        return ARP_SUCCESS;
    }

    (*ppArpEthernetEntry) = (tArpEthernetEntry *)(VOID *) 
               MemAllocMemBlk (ARP_ENET_POOL_ID);
    if ((*ppArpEthernetEntry) == NULL)
    {
        ARP_GBL_TRC (ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                   "Memory allocation for tArpEthernetEntry failed\r\n");
        return ARP_FAILURE;
    }

    MEMSET ((*ppArpEthernetEntry), 0, sizeof (tArpEthernetEntry));

    (*ppArpEthernetEntry)->u2Port = u2Port;

    if (RBTreeAdd (gArpGblInfo.ArpEnetTable, (tRBElem *)
                       (*ppArpEthernetEntry)) == RB_FAILURE)
    {
        MemReleaseMemBlock (ARP_ENET_POOL_ID,
                            (UINT1 *) (*ppArpEthernetEntry));
        (*ppArpEthernetEntry) = NULL;
        ARP_GBL_TRC (ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                 "ArpAddArpEnetTableEntry:Global RBTree Addition Failed for "
                 "gArpGblInfo.ArpEnetTable Entry \r\n");
        return ARP_FAILURE;
    }
    return ARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ArpGetArpEnetTableEntry                              */
/*                                                                           */
/* Description        : This routine gets a tArpEthernetEntry  for the       */
/*                      given u2Port.                                        */
/*                                                                           */
/* Input(s)           : u2Port    - Port identifier.                         */
/*                                                                           */
/* Output(s)          : pArpEthernetEntry  - pointer to the pArpEthernetEntry*/
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
tArpEthernetEntry *
ArpGetArpEnetTableEntry (UINT2 u2Port)
{
    tArpEthernetEntry  *pArpEthernetEntry = NULL;
    tArpEthernetEntry   ArpEthernetEntry;

    MEMSET (&ArpEthernetEntry, 0, sizeof (tArpEthernetEntry));

    ArpEthernetEntry.u2Port = u2Port;

    pArpEthernetEntry  = (tArpEthernetEntry *) RBTreeGet 
        ((gArpGblInfo.ArpEnetTable), (tRBElem *) &ArpEthernetEntry);

    return pArpEthernetEntry;
}

/*****************************************************************************/
/* Function Name      : ArpGetFirstArpEnetTableEntry                         */
/*                                                                           */
/* Description        : This routine is used to get first tArpEthernetEntry  */
/*                      from gArpGblInfo.ArpEnetTable.                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : pArpEthernetEntry - pointer to the tArpEthernetEntry*/
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
tArpEthernetEntry  *
ArpGetFirstArpEnetTableEntry ()
{
    tArpEthernetEntry    *pArpEthernetEntry = NULL;

    pArpEthernetEntry = (tArpEthernetEntry *)
                    RBTreeGetFirst (gArpGblInfo.ArpEnetTable);

    return pArpEthernetEntry;
}

/*****************************************************************************/
/* Function Name      : ArpGetNextArpEnetTableEntry                          */
/*                                                                           */
/* Description        : This routine is used to get the next                 */
/*                      tArpEthernetEntry from gArpGblInfo.ArpEnetTable.     */
/*                                                                           */
/* Input(s)           : u2Port    - Port identifier.                         */
/*                                                                           */
/* Output(s)          : pArpEthernetEntry - pointer to the tArpEthernetEntry */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
tArpEthernetEntry *
ArpGetNextArpEnetTableEntry (UINT2 u2Port)
{
    tArpEthernetEntry  *pArpEthernetEntry = NULL;
    tArpEthernetEntry   ArpEthernetEntry;

    MEMSET (&ArpEthernetEntry, 0, sizeof (tArpEthernetEntry));
    
    ArpEthernetEntry.u2Port = u2Port;

    pArpEthernetEntry  = (tArpEthernetEntry *) RBTreeGetNext 
      ((gArpGblInfo.ArpEnetTable), (tRBElem *) &ArpEthernetEntry, NULL);

    return pArpEthernetEntry;
}

/*****************************************************************************/
/* Function Name      : ArpDelArpEnetTableEntry                              */
/*                                                                           */
/* Description        : This routine deletes a tArpEthernetEntry from the    */
/*                      gArpGblInfo.ArpEnetTable.                            */
/*                                                                           */
/* Input(s)           : pArpEthernetEntry - node to be deleted.              */
/*                                                                           */
/* Return Value(s)    : ARP_SUCCESS/ARP_FAILURE                            */
/*****************************************************************************/
INT4
ArpDelArpEnetTableEntry (tArpEthernetEntry *pArpEthernetEntry)
{
    if (pArpEthernetEntry == NULL)
    {
        return ARP_FAILURE;
    }
    if (RBTreeRemove ((gArpGblInfo.ArpEnetTable), 
            (tRBElem *) pArpEthernetEntry) == RB_FAILURE)
    {
        ARP_GBL_TRC (ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                 "ArpDelArpEnetTableEntry: RBTree remove failed \r\n");
        return ARP_FAILURE;
    }
    MemReleaseMemBlock (ARP_ENET_POOL_ID,
                        (UINT1 *) (pArpEthernetEntry));
    pArpEthernetEntry = NULL;
    return ARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ArpDelAllArpEnetTableEntries                         */
/*                                                                           */
/* Description        : This function deletes all the tArpEthernetEntry      */
/*                      entries of gArpGblInfo.ArpEnetTable.                 */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
ArpDelAllArpEnetTableEntries ()
{
    tArpEthernetEntry   *pArpEthernetEntry = NULL;
    tArpEthernetEntry   *pNextArpEthernetEntry = NULL;

    pArpEthernetEntry = ArpGetFirstArpEnetTableEntry ();

    while (pArpEthernetEntry != NULL)
    {
        /* Delete all the nodes */
        pNextArpEthernetEntry = ArpGetNextArpEnetTableEntry 
                        (pArpEthernetEntry->u2Port);

        ArpDelArpEnetTableEntry (pArpEthernetEntry);

        pArpEthernetEntry = pNextArpEthernetEntry;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : ArpCreateArpEnetTableEntries                         */
/*                                                                           */
/* Description        : This function creates all the tArpEthernetEntry      */
/*                      entries of gArpGblInfo.ArpEnetTable.                 */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
ArpCreateAllArpEnetTableEntries ()
{
    tArpEthernetEntry *pEnetEntry = NULL;
    UINT2              u2PortVar = 0;

    for (u2PortVar = 0; u2PortVar <
            FsARPSizingParams[MAX_ARP_ENET_ENTRIES_SIZING_ID].u4PreAllocatedUnits;
            u2PortVar++)
    {
        ArpAddArpEnetTableEntry (u2PortVar, &pEnetEntry);
    }
    return;
}

