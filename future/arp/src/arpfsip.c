/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: arpfsip.c,v 1.20 2017/11/23 14:11:53 siva Exp $
 *
 * Description:Contains functions to process arp packets,   
 *             send packets handle timer expiry and         
 *             initialize ARP.                              
 *                               
 *******************************************************************/
#include "arpinc.h"

/*-------------------------------------------------------------------+
 * Function           : ArpSendRequest
 *
 * Input(s)           : u2Port, u4Dest, i2Hardware
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS or ARP_FAILURE
 *
 * Action :
 * Constructs and sends an ARP request.
 * This is called from arp_nq.
 *
+-------------------------------------------------------------------*/
INT4
ArpSendRequest (UINT2 u2Port, UINT4 u4Dest, INT2 i2Hardware, UINT1 u1EncapType)
{
    t_ARP_PKT           Arp_pkt;
    t_ARP_HW_TABLE     *pHw_entry = NULL;
    tCfaIfInfo          CfaIfInfo;
    tNetIpv4IfInfo      NetIpIfInfo;
    tArpCxt            *pArpCxt = NULL;
    UINT4               u4IfIndex = 0;
#ifdef VRRP_WANTED
    UINT4               au1SrcHwAddr[CFA_ENET_ADDR_LEN];
    INT4                i4OperVrId = 0;
#endif

    MEMSET (&Arp_pkt, 0, sizeof (t_ARP_PKT));
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (u2Port >= ARP_MAX_SIZING_ENET_ENTRIES)
    {
        return ARP_FAILURE;
    }

    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) u2Port,
                                      &u4IfIndex) == NETIPV4_FAILURE)
    {
        return ARP_FAILURE;
    }
    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetIfInfo ((UINT4) u2Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return ARP_FAILURE;
    }
    /* Get hardware table entry. */
    if ((pHw_entry = arp_get_hw_entry (i2Hardware)) == NULL)
    {

        ARP_TRC (NetIpIfInfo.u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC,
                 ARP_NAME, "Invalid Hardware Type .\n");
        return ARP_FAILURE;
    }

    Arp_pkt.i2Opcode = ARP_REQUEST;
    Arp_pkt.u4Tproto_addr = u4Dest;

    if (CfaIpIfGetSrcAddressOnInterface (u4IfIndex, u4Dest,
                                         &Arp_pkt.u4Sproto_addr) == CFA_FAILURE)
    {
        /*find our IP addr */
        return ARP_FAILURE;
    }

    /* The mac address depends on interface type. Hence function is stored
     * in hw_table.
     */

#ifdef VRRP_WANTED
    if ((IsVrrpMasterIpAddress (u4IfIndex,
                                Arp_pkt.u4Sproto_addr, &i4OperVrId)) == VRRP_OK)
    {
        VrrpGetMacAddress (u4IfIndex, i4OperVrId,
                           IPVX_ADDR_FMLY_IPV4, (UINT1 *) &au1SrcHwAddr);
        MEMCPY (Arp_pkt.i1Shwaddr, au1SrcHwAddr, CFA_ENET_ADDR_LEN);
    }
    else if (ARP_GET_ENET_ENTRY (u2Port)->u1HwAddrValidFlag == TRUE)
    {
        MEMCPY (Arp_pkt.i1Shwaddr,
                ARP_GET_ENET_ENTRY (u2Port)->au1HwAddr, CFA_ENET_ADDR_LEN);
    }
#else
    if (ARP_GET_ENET_ENTRY (u2Port)->u1HwAddrValidFlag == TRUE)
    {
        /* lets check in the array first.
         * if hardware address is valid, we can use it.
         * and improve little performance.
         */

        MEMCPY (Arp_pkt.i1Shwaddr, ARP_GET_ENET_ENTRY (u2Port)->au1HwAddr,
                CFA_ENET_ADDR_LEN);
    }
#endif /* #ifdef VRRP_WANTED */
    else
    {
        pArpCxt = gArpGblInfo.apArpCxt[NetIpIfInfo.u4ContextId];

        /* array is not having entry
         *  call function to find hardware address 
         */
        if (pHw_entry->get_hw_address == NULL)
        {
            pArpCxt->Arp_stat.u4Req_discards++;
            return ARP_FAILURE;
        }

        /* get the hardware address corresponding to this interface */
        /*
         * Not using the function pointer coz, new CFA's API is not compliant
         * with the earlier function pointer type
         */
        if (CfaGetIfInfo ((UINT2) u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
        {
            return ARP_FAILURE;
        }

        MEMCPY (Arp_pkt.i1Shwaddr, CfaIfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);
        /* API to get hardware address does not give the hardware address len 
         * so ARP is restricted to run only on ETHERNET
         */

        /* at this point, hardware address is captured.
         * copy it in array for future reference.
         */
        MEMCPY (ARP_GET_ENET_ENTRY (u2Port)->au1HwAddr,
                Arp_pkt.i1Shwaddr, CFA_ENET_ADDR_LEN);
    }

    MEMSET (Arp_pkt.i1Thwaddr, ARP_UINT1_ALL_ONE, CFA_ENET_ADDR_LEN);

    if (ArpSendReqOrResp (u2Port, u1EncapType, &Arp_pkt) == ARP_FAILURE)
    {
        return ARP_FAILURE;
    }

    if (pArpCxt != NULL)
    {
        pArpCxt->Arp_stat.u4Out_requests++;
    }
    return ARP_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpCheckAndStartTimer
 *
 * Input(s)           : pArpCache - pointer to the cache.
 *                      u4Duration - Duration of the timer.
 *
 * Output(s)          : None
 *
 * Returns            : None.
 *
 * Action             : This function starts the timer.this function is 
 *                      common to lnxip and fsip in fsip case no 
 *                      check is required.
 *
+-------------------------------------------------------------------*/
VOID
ArpCheckAndStartTimer (t_ARP_CACHE * pArpCache, UINT4 u4Duration)
{
    tHwPortInfo         HwPortInfo;

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    if (ARP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        /* When RM Stacking Interface is an Inband Ethernet port,ARP packets
           has to be transmitted over the Stacking Interface irrespective of
           RM Node State .Hence below check is Introduced */
        if ((IssGetRMTypeFromNvRam () != ISS_RM_STACK_INTERFACE_TYPE_INBAND) ||
            (pArpCache->u4IfIndex != HwPortInfo.au4ConnectingPortIfIndex[0]))
        {
            return;
        }
    }

    if (TmrStartTimer (ARP_TIMER_LIST_ID, &(pArpCache->Timer.Timer_node),
                       SYS_NUM_OF_TIME_UNITS_IN_A_SEC * (u4Duration))
        == TMR_FAILURE)
    {
        ARP_TRC (pArpCache->pArpCxt->u4ContextId, ARP_MOD_TRC, ALL_FAILURE_TRC,
                 ARP_NAME, "Timer Failure in ArpCheckAndStartTimer fn.\n");
    }

}

/*-------------------------------------------------------------------+
 *
 * Function           : ArpCheckAndSendReq
 *
 * Input(s)           : i1State - State of the entry.
 *                      u2Port  - Port in which req to be sent.
 *                      u4IpAddr - IpAddress of the entry.
 *                      i2Hardware - Hardware type.
 *                      u1Encaptype - Encapsulation type 
                                      (used to fill ethernet header).
 *
 * Output(s)          : None
 *
 * Returns            : None.
 *
 * Action             : This function sends request.this function is 
 *                      common to lnxip and fsip in fsip case no 
 *                      check is required.
 *
+-------------------------------------------------------------------*/
VOID
ArpCheckAndSendReq (INT1 i1State, UINT2 u2Port, UINT4 u4IpAddr, INT2 i2Hardware,
                    UINT1 u1EncapType)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4ConnectingPortIfIndex = 0;

    UNUSED_PARAM (i1State);
    if (NetIpv4GetCfaIfIndexFromPort ((UINT4) u2Port,
                                      &u4IfIndex) == NETIPV4_FAILURE)
    {
        return;
    }
    CfaGetRmConnectingPortIfIndex (&u4ConnectingPortIfIndex);
    if (ARP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        /* When RM Stacking Interface is an Inband Ethernet port,ARP packets
           has to be transmitted over the Stacking Interface irrespective of
           RM Node State .Hence below check is Introduced */
        if ((IssGetRMTypeFromNvRam () != ISS_RM_STACK_INTERFACE_TYPE_INBAND) &&
            (u4IfIndex != u4ConnectingPortIfIndex))
        {
            return;
        }
    }

    if (ArpSendRequest (u2Port, u4IpAddr, i2Hardware, u1EncapType)
        == ARP_FAILURE)
    {
        ARP_GBL_TRC (ARP_MOD_TRC, ALL_FAILURE_TRC, ARP_NAME,
                     "ArpSendRequest failure in ArpCheckAndSendReq fn.\n");
    }
}

/*-------------------------------------------------------------------+
 * Function           : ArpSendReqOrResp
 *
 * Input(s)           : u2Port - Port number
 *                      pArpPkt - Pointer to ARP packet to be sent   
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_SUCCESS or ARP_FAILURE
 *
 * Action             : Constructs and sends an ARP request/response.
 *                      This function is also called by DHCP/VRRP.    
+-------------------------------------------------------------------*/
INT4
ArpSendReqOrResp (UINT2 u2Port, UINT1 u1EncapType, t_ARP_PKT * pArpPkt)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT1               au1BcastHwaddr[CFA_ENET_ADDR_LEN] =
        { ARP_UINT1_ALL_ONE, ARP_UINT1_ALL_ONE, ARP_UINT1_ALL_ONE,
        ARP_UINT1_ALL_ONE, ARP_UINT1_ALL_ONE, ARP_UINT1_ALL_ONE
    };

    if (NetIpv4GetCfaIfIndexFromPort (u2Port, &u4IfIndex) == NETIPV4_FAILURE)
    {
        return (ARP_FAILURE);
    }

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
    pArpPkt->i2Hardware = ARP_ENET_TYPE;
    pArpPkt->i2Protocol = IP_PROTOCOL;
    pArpPkt->i1Hwalen = CFA_ENET_ADDR_LEN;
    pArpPkt->i1Palen = ARP_MAX_PROTO_ADDR_LEN;

    pBuf = ARP_ALLOCATE_BUF (ARP_HDR_SIZE (pArpPkt->i1Hwalen, pArpPkt->i1Palen)
                             + ARP_MAX_ENCAPS_ADDRESS_LENGTH,
                             ARP_MAX_ENCAPS_ADDRESS_LENGTH);
    if (pBuf == NULL)
    {

        ARP_TRC (u4ContextId, ARP_MOD_TRC, BUFFER_TRC, ARP_NAME,
                 "Buffer Allocation Failure.\n");
        return ARP_FAILURE;
    }

    arp_put_hdr (pBuf, pArpPkt);

    i4RetVal = CfaHandlePktFromIp (pBuf, u4IfIndex, 0,
                                   au1BcastHwaddr, ARP_PROTOCOL, ARP_BCAST,
                                   u1EncapType);

    if (i4RetVal != CFA_SUCCESS)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return ARP_FAILURE;
    }
    else
    {
        ARP_TRC_ARG1 (u4ContextId, ARP_MOD_TRC, DATA_PATH_TRC, ARP_NAME,
                      "Sending ARP request/response for %x ",
                      pArpPkt->u4Sproto_addr);
    }

    return ARP_SUCCESS;

}
