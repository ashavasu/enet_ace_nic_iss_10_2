/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmparlw.c,v 1.5 2015/09/15 06:46:24 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmparlw.h"
# include  "fsarplw.h"
# include  "arpinc.h"

extern UINT4 FsMIArpContextDebug[12];

/* LOW LEVEL Routines for Table : FsMIArpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIArpTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIArpTable (INT4 i4FsMIStdIpContextId)
{
    if ((i4FsMIStdIpContextId < ARP_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= ARP_SIZING_CONTEXT_COUNT))
    {
        return SNMP_FAILURE;
    }
    if (ArpVcmIsVRExist ((UINT4) i4FsMIStdIpContextId) != ARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIArpTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIArpTable (INT4 *pi4FsMIStdIpContextId)
{
    if (VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId) !=
        ARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIArpTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIArpTable (INT4 i4FsMIStdIpContextId,
                             INT4 *pi4NextFsMIStdIpContextId)
{
    if (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                   (UINT4 *) pi4NextFsMIStdIpContextId) !=
        ARP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIArpCacheTimeout
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIArpCacheTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIArpCacheTimeout (INT4 i4FsMIStdIpContextId,
                           INT4 *pi4RetValFsMIArpCacheTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsArpCacheTimeout (pi4RetValFsMIArpCacheTimeout);
    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIArpCachePendTime
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIArpCachePendTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIArpCachePendTime (INT4 i4FsMIStdIpContextId,
                            INT4 *pi4RetValFsMIArpCachePendTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsArpCachePendTime (pi4RetValFsMIArpCachePendTime);
    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIArpMaxRetries
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIArpMaxRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIArpMaxRetries (INT4 i4FsMIStdIpContextId,
                         INT4 *pi4RetValFsMIArpMaxRetries)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsArpMaxRetries (pi4RetValFsMIArpMaxRetries);
    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIArpPendingEntryCount
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIArpPendingEntryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIArpPendingEntryCount (INT4 i4FsMIStdIpContextId,
                                INT4 *pi4RetValFsMIArpPendingEntryCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsArpPendingEntryCount (pi4RetValFsMIArpPendingEntryCount);

    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIArpCacheEntryCount
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIArpCacheEntryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIArpCacheEntryCount (INT4 i4FsMIStdIpContextId,
                              INT4 *pi4RetValFsMIArpCacheEntryCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsArpCacheEntryCount (pi4RetValFsMIArpCacheEntryCount);

    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIArpRedEntryTime
 Input       :  The Indices
                FsMIStdIpContextId

                The Object
                retValFsMIArpRedEntryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsMIArpRedEntryTime (INT4 i4FsMIStdIpContextId,
                           INT4 *pi4RetValFsMIArpRedEntryTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsArpRedEntryTime (pi4RetValFsMIArpRedEntryTime);

    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIArpRedExitTime
 Input       :  The Indices
                FsMIStdIpContextId
 
                The Object
                retValFsMIArpRedExitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsMIArpRedExitTime (INT4 i4FsMIStdIpContextId,
                          INT4 *pi4RetValFsMIArpRedExitTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsArpRedExitTime (pi4RetValFsMIArpRedExitTime);

    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIArpContextDebug
 Input       :  The Indices
                FsMIStdIpContextId

                The Object
                retValFsMIArpContextDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIArpContextDebug(INT4 i4FsMIStdIpContextId , INT4 *pi4RetValFsMIArpContextDebug)
{
    INT4 u4DebugLevel = ARP_FAILURE;

    u4DebugLevel = ArpGetDebugFlag((UINT4)i4FsMIStdIpContextId);

    if (u4DebugLevel == ARP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsMIArpContextDebug = u4DebugLevel;
        return SNMP_SUCCESS;
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIArpCacheTimeout
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIArpCacheTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIArpCacheTimeout (INT4 i4FsMIStdIpContextId,
                           INT4 i4SetValFsMIArpCacheTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsArpCacheTimeout (i4SetValFsMIArpCacheTimeout);
    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIArpCachePendTime
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIArpCachePendTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIArpCachePendTime (INT4 i4FsMIStdIpContextId,
                            INT4 i4SetValFsMIArpCachePendTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsArpCachePendTime (i4SetValFsMIArpCachePendTime);
    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIArpMaxRetries
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIArpMaxRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIArpMaxRetries (INT4 i4FsMIStdIpContextId,
                         INT4 i4SetValFsMIArpMaxRetries)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsArpMaxRetries (i4SetValFsMIArpMaxRetries);
    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIArpContextDebug
 Input       :  The Indices
                FsMIStdIpContextId

                The Object
                setValFsMIArpContextDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIArpContextDebug(INT4 i4FsMIStdIpContextId , INT4 i4SetValFsMIArpContextDebug)
{
  ARP_PROT_UNLOCK ();  /* Lock taken in cli and snmp thread
                        so releasing the lock */

  ArpSetArpDebug((UINT4)i4FsMIStdIpContextId, (UINT4)i4SetValFsMIArpContextDebug);
  ARP_PROT_LOCK ();

  ArpIncMsrNotifyCfg (i4FsMIStdIpContextId,
                        FsMIArpContextDebug,
                        (sizeof (FsMIArpContextDebug) / sizeof (UINT4)),
                        i4SetValFsMIArpContextDebug);

  return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIArpCacheTimeout
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIArpCacheTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIArpCacheTimeout (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                              INT4 i4TestValFsMIArpCacheTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsArpCacheTimeout (pu4ErrorCode, i4TestValFsMIArpCacheTimeout);
    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIArpCachePendTime
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIArpCachePendTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIArpCachePendTime (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                               INT4 i4TestValFsMIArpCachePendTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsArpCachePendTime (pu4ErrorCode,
                                     i4TestValFsMIArpCachePendTime);
    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIArpMaxRetries
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIArpMaxRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIArpMaxRetries (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                            INT4 i4TestValFsMIArpMaxRetries)
{
    INT1                i1Return = SNMP_FAILURE;

    if (ArpSetContext (i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsArpMaxRetries (pu4ErrorCode, i4TestValFsMIArpMaxRetries);
    ArpResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIArpContextDebug
 Input       :  The Indices
                FsMIStdIpContextId

                The Object
                testValFsMIArpContextDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIArpContextDebug(UINT4 *pu4ErrorCode , INT4 i4FsMIStdIpContextId , INT4 i4TestValFsMIArpContextDebug)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIStdIpContextId);
    UNUSED_PARAM (i4TestValFsMIArpContextDebug);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIArpTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIArpTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
