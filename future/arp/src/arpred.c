/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: arpred.c,v 1.19 2018/02/19 09:56:23 siva Exp $ 
 *
 * Description: This file contains ARP Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __ARPRED_C
#define __ARPRED_C

#include "arpinc.h"
#include "arpred.h"

/************************************************************************
 *  Function Name   : ArpRedInitGlobalInfo                            
 *                                                                    
 *  Description     : This function is invoked by the ARP module while
 *                    task initialisation. It initialises the red     
 *                    global variables.
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
ArpRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Entering ArpRedInitGlobalInfo\r \n");

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_ARP_APP_ID;
    RmRegParams.pFnRcvPkt = ArpRedRmCallBack;

    /* Registering ARP with RM */
    if (ArpRedRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {

        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedInitGlobalInfo: Registration with RM failed");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedInitGlobalInfo: Registration with RM failed"));
        return OSIX_FAILURE;
    }
    ARP_GET_NODE_STATUS () = RM_INIT;
    ARP_NUM_STANDBY_NODES () = 0;
    gArpRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
    /* BulkReqRcvd is set as FALSE initially. When bulk request is received
     * the flag is set to true. This is checked for sending bulk update
     * message to the standby node */

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Exiting ArpRedInitGlobalInfo\r \n");

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : ArpRedRmRegisterProtocols                         */
/*                                                                      */
/*  Description     : This function is invoked by the ARP module to     */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : pRmReg -- Pointer to structure tRmRegParams       */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
ArpRedRmRegisterProtocols (tRmRegParams * pRmReg)
{

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedRmRegisterProtocols\r \n");

    /* Registering ARP protocol with RM */
    if (RmRegisterProtocols (pRmReg) == RM_FAILURE)
    {

        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedRmRegisterProtocols: Registration with RM failed");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedRmRegisterProtocols: Registration with RM failed"));
        return OSIX_FAILURE;
    }

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedRmRegisterProtocols\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : ArpRedDeInitGlobalInfo                        
 *                                                                    
 * Description        : This function is invoked by the ARP module    
 *                      during module shutdown and this function      
 *                      deinitializes the redundancy global variables 
 *                      and de-register ARP with RM.            
 *                                                                    
 * Input(s)           : None                                          
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                   
 ************************************************************************/

INT4
ArpRedDeInitGlobalInfo (VOID)
{
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedDeInitGlobalInfo\r \n");
    /* Deregister from SYSLOG */
    if (ArpRedRmDeRegisterProtocols () == OSIX_FAILURE)
    {

        SYS_LOG_DEREGISTER (gi4ArpSysLogId);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | INIT_SHUT_TRC, ARP_NAME,
                 "ArpRedDeInitGlobalInfo: De-Registration with RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedDeInitGlobalInfo: De-Registration with RM failed"));
        return OSIX_FAILURE;
    }

    SYS_LOG_DEREGISTER (gi4ArpSysLogId);
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedDeInitGlobalInfo\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : ArpRmDeRegisterProtocols                        
 *                                                                    
 *  Description     : This function is invoked by the ARP module to   
 *                    de-register itself with the RM module.          
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
ArpRedRmDeRegisterProtocols (VOID)
{

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedRmDeRegisterProtocols\r \n");

    /* Deregiserting ARP from RM */
    if (RmDeRegisterProtocols (RM_ARP_APP_ID) == RM_FAILURE)
    {

        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedRmDeRegisterProtocols: De-Registration with RM failed");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedRmDeRegisterProtocols: De-Registration "
                      "with RM failed"));
        return OSIX_FAILURE;
    }

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedRmDeRegisterProtocols\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : ArpRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to ARP        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
ArpRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tArpRmMsg          *pMsg = NULL;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Entering ArpRedRmCallBack\r \n");

    /* Callback function for RM events. The event and the message is sent as 
     * parameters to this function */

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_DYNAMIC_SYNCH_AUDIT))
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedRmCallBack:This event is not associated with RM \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                      "ArpRedRmCallBack:This event is not associated with RM"));
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Queue message associated with the event is not present */
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedRmCallBack: Queue Message associated with the event "
                 "is not sent by RM \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                      "ArpRedRmCallBack: Queue Message associated with the "
                      "event is not sent by RM"));
        return OSIX_FAILURE;
    }

    if ((pMsg =
         (tArpRmMsg *) MemAllocMemBlk ((tMemPoolId) ARP_RM_MSG_POOL_ID)) ==
        NULL)
    {

        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 " ArpRedRmCallBack: Queue message allocation failure\r \n");

        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      " ArpRedRmCallBack: Queue message allocation failure"));

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            ArpRedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }
    MEMSET (pMsg, 0, sizeof (tArpRmMsg));

    pMsg->RmCtrlMsg.pData = pData;
    pMsg->RmCtrlMsg.u1Event = u1Event;
    pMsg->RmCtrlMsg.u2DataLen = u2DataLen;

    /* Send the message associated with the event to ARP module in a queue */
    if (OsixQueSend (ARP_RM_PKT_Q_ID,
                     (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock ((tMemPoolId) ARP_RM_MSG_POOL_ID, (UINT1 *) pMsg);

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "DhCRedRmCallBack: Q send failure\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "DhCRedRmCallBack: Q send failure"));
        return OSIX_FAILURE;
    }

    /* Post a event to ARP to process RM events */
    OsixEvtSend (ARP_TASK_ID, ARP_RM_PKT_EVENT);

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Exiting ArpRedRmCallBack\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : ArpRedHandleRmEvents                          
 *                                                                    
 * Description        : This function is invoked by the ARP module to 
 *                      process all the events and messages posted by 
 *                      the RM module                                 
 *                                                                    
 * Input(s)           : pMsg -- Pointer to the ARP Q Msg       
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : None                                          
 ************************************************************************/

PUBLIC VOID
ArpRedHandleRmEvents ()
{
    tArpRmMsg          *pMsg = NULL;
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Entering ArpRedHandleRmEvents\r \n");
    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    while (OsixQueRecv (ARP_RM_PKT_Q_ID,
                        (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pMsg->RmCtrlMsg.u1Event)
        {
            case GO_ACTIVE:

                ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                         CONTROL_PLANE_TRC, ARP_NAME,
                         "ArpRedHandleRmEvents: Received GO_ACTIVE event\r \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                              "ArpRedHandleRmEvents: Received GO_ACTIVE event"));
                ArpRedHandleGoActive ();
                break;
            case GO_STANDBY:
                ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                         CONTROL_PLANE_TRC, ARP_NAME,
                         "ArpRedHandleRmEvents: Received GO_STANDBY event \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                              "ArpRedHandleRmEvents: Received GO_STANDBY event"));
                ArpRedHandleGoStandby (pMsg);
                /* pMsg is passed as a argument to GoStandby function and
                 * it is released there. If the transformation is from active 
                 * to standby then there is no need for releasing the mempool
                 * as we are calling DeInit function. This function clears
                 * all the mempools, so it is returned here instead of break.*/
                return;
            case RM_STANDBY_UP:
                ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                         CONTROL_PLANE_TRC, ARP_NAME,
                         "ArpRedHandleRmEvents: Received RM_STANDBY_UP"
                         " event \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                              "ArpRedHandleRmEvents: Received RM_STANDBY_UP"
                              " event"));
                /* Standby up event, number of standby node is updated. 
                 * BulkReqRcvd flag is checked, if it is true, then Bulk update
                 * message is sent to the standby node and the flag is reset */

                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                ARP_NUM_STANDBY_NODES () = pData->u1NumStandby;
                ArpRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                if (ARP_RM_BULK_REQ_RCVD () == OSIX_TRUE)
                {
                    ARP_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                    gArpRedGlobalInfo.u1BulkUpdStatus = ARP_HA_UPD_NOT_STARTED;
                    ArpRedSendBulkUpdMsg ();
                }
                break;
            case RM_STANDBY_DOWN:
                ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                         CONTROL_PLANE_TRC, ARP_NAME,
                         "ArpRedHandleRmEvents: Received RM_STANDBY_DOWN "
                         "event \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                              "ArpRedHandleRmEvents: Received RM_STANDBY_DOWN "
                              "event"));
                /* Standby down event, number of standby nodes is updated */
                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                ARP_NUM_STANDBY_NODES () = pData->u1NumStandby;
                ArpRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            case RM_MESSAGE:
                /* Read the sequence number from the RM Header */
                RM_PKT_GET_SEQNUM (pMsg->RmCtrlMsg.pData, &u4SeqNum);
                /* Remove the RM Header */
                RM_STRIP_OFF_RM_HDR (pMsg->RmCtrlMsg.pData,
                                     pMsg->RmCtrlMsg.u2DataLen);
                ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                         CONTROL_PLANE_TRC, ARP_NAME,
                         "ArpRedHandleRmEvents: Received RM_MESSAGE event \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                              "ArpRedHandleRmEvents: Received RM_MESSAGE event"));
                ProtoAck.u4AppId = RM_ARP_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                if (gArpRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
                {
                    /* Process the message at active */
                    ArpRedProcessPeerMsgAtActive (pMsg->RmCtrlMsg.pData,
                                                  pMsg->RmCtrlMsg.u2DataLen);
                }
                else if (gArpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
                {
                    /* Process the message at standby */
                    ArpRedProcessPeerMsgAtStandby (pMsg->RmCtrlMsg.pData,
                                                   pMsg->RmCtrlMsg.u2DataLen);
                }
                else
                {
                    /* Message is received at the idle node so ignore */
                    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                             CONTROL_PLANE_TRC, ARP_NAME,
                             "ArpRedHandleRmEvents: Sync-up message received"
                             " at Idle Node!!!!\r\n");
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                                  "ArpRedHandleRmEvents: Sync-up message "
                                  "received at Idle Node"));
                }

                RM_FREE (pMsg->RmCtrlMsg.pData);
                RmApiSendProtoAckToRM (&ProtoAck);
                break;
            case RM_CONFIG_RESTORE_COMPLETE:
                /* Config restore complete event is sent by the RM on completing
                 * the static configurations. If the node status is init, 
                 * and the rm state is standby, then the ARP status is changed 
                 * from idle to standby */
                if (gArpRedGlobalInfo.u1NodeStatus == RM_INIT)
                {
                    if (ARP_GET_RMNODE_STATUS () == RM_STANDBY)
                    {
                        ArpRedHandleIdleToStandby ();

                        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                        RmApiHandleProtocolEvent (&ProtoEvt);
                    }
                }
                break;
            case L2_INITIATE_BULK_UPDATES:
                /* L2 Initiate bulk update is sent by RM to the standby node 
                 * to send a bulk update request to the active node */
                ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                         CONTROL_PLANE_TRC, ARP_NAME,
                         "ArpRedHandleRmEvents: Received "
                         "L2_INITIATE_BULK_UPDATES \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                              "ArpRedHandleRmEvents: Received "
                              "L2_INITIATE_BULK_UPDATES"));
                ArpRedSendBulkReqMsg ();
                break;
            case RM_DYNAMIC_SYNCH_AUDIT:
                ArpRedHandleDynSyncAudit ();
                break;

            default:
                ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                         CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                         "ArpRedHandleRmEvents: Invalid RM event received\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                              "ArpRedHandleRmEvents: Invalid RM event received"));
                break;

        }
        MemReleaseMemBlock (ARP_RM_MSG_POOL_ID, (UINT1 *) pMsg);
    }
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Exiting ArpRedHandleRmEvents\r \n");
    return;
}

/************************************************************************
 * Function Name      : ArpRedHandleGoActive                          
 *                                                                      
 * Description        : This function is invoked by the ARP upon
 *                      receiving the GO_ACTIVE indication from RM      
 *                      module. And this function responds to RM with an
 *                      acknowledgement.                                
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
ArpRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Entering ArpRedHandleGoActive\r \n");
    ProtoEvt.u4AppId = RM_ARP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to not started. */
    gArpRedGlobalInfo.u1BulkUpdStatus = ARP_HA_UPD_NOT_STARTED;

    if (ARP_GET_NODE_STATUS () == RM_ACTIVE)
    {
        /* Go active received by the active node, so ignore */
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC, ARP_NAME,
                 "ArpRedHandleGoActive: GO_ACTIVE event reached when node "
                 "is already active \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                      "ArpRedHandleGoActive: GO_ACTIVE event reached when "
                      "node is already active"));
        return;
    }
    if (ARP_GET_NODE_STATUS () == RM_INIT)
    {
        /* Go active received by idle node, so state changed to active */
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC, ARP_NAME,
                 "ArpRedHandleGoActive: Idle to Active transition...\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                      "ArpRedHandleGoActive: Idle to Active transition"));

        ArpRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    if (ARP_GET_NODE_STATUS () == RM_STANDBY)
    {
#if defined (NPAPI_WANTED) && defined (MBSM_WANTED) && defined (CFA_WANTED)
        /* This filter will add copytocpu and remove the
         * RedirectPort action */
        if (ArpFsNpCfaModifyFilter (GO_ACTIVE) == FNP_FAILURE)
        {
            ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                     CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                     "ArpRedHandleGoActive: Failed to Modify CFA_NP_ARP_UCAST "
                     "and CFA_NP_ARP_BCAST Filter action\r\n");
        }
#endif

        /* Go active received by standby node, 
         * Do hardware audit, and start the timers for all the arp entries 
         * and change the state to active */
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC, ARP_NAME,
                 "ArpRedHandleGoActive: Standby to Active transition...\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                      "ArpRedHandleGoActive: Standby to Active transition"));
        ArpRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (ARP_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        /* If bulk update req received flag is true, send the bulk updates */
        ARP_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        gArpRedGlobalInfo.u1BulkUpdStatus = ARP_HA_UPD_NOT_STARTED;
        ArpRedSendBulkUpdMsg ();
    }
    RmApiHandleProtocolEvent (&ProtoEvt);

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Exiting ArpRedHandleGoActive\r \n");
    return;
}

/************************************************************************
 * Function Name      : ArpRedHandleGoStandby                         
 *                                                                    
 * Description        : This function is invoked by the ARP upon
 *                      receiving the GO_STANBY indication from RM    
 *                      module. And this function responds to RM module 
 *                      with an acknowledgement.                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
ArpRedHandleGoStandby (tArpRmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedHandleGoStandby\r \n");
    ProtoEvt.u4AppId = RM_ARP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to Update not started */
    MemReleaseMemBlock (ARP_RM_MSG_POOL_ID, (UINT1 *) pMsg);
    gArpRedGlobalInfo.u1BulkUpdStatus = ARP_HA_UPD_NOT_STARTED;

    if (ARP_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go standby received by standby node. So ignore */
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC, ARP_NAME,
                 "ArpRedHandleGoStandby: GO_STANDBY event reached when "
                 "node is already in standby \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                      "ArpRedHandleGoStandby: GO_STANDBY event reached when "
                      "node is already in standby"));
        return;
    }
    if (ARP_GET_NODE_STATUS () == RM_INIT)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC, ARP_NAME,
                 "ArpRedHandleGoStandby: GO_STANDBY event reached when node "
                 "is already idle \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                      "ArpRedHandleGoStandby: GO_STANDBY event reached when "
                      "node is already idle"));

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        return;
    }
    else
    {
#if defined (NPAPI_WANTED) && defined (MBSM_WANTED) && defined (CFA_WANTED)
        /* This filter will remove copytocpu and add the
         * RedirectPort action for Ucast and will disable
         * Bcast filter */
        if (ArpFsNpCfaModifyFilter (GO_STANDBY) == FNP_FAILURE)
        {
            ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                     CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                     "ArpRedHandleGoStandby: Failed to Modify CFA_NP_ARP_UCAST "
                     "and CFA_NP_ARP_BCAST Filter action\r\n");
        }
#endif

        /* Go standby received by active event. Clear all the dynamic data and
         * populate again by bulk updates */
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC, ARP_NAME,
                 "ArpRedHandleGoStandby: Active to Standby transition..\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                      "ArpRedHandleGoStandby: Active to Standby transition"));
        ArpRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Exiting ArpRedHandleGoStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedHandleIdleToActive                        */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedHandleIdleToActive (VOID)
{
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedHandleIdleToActive\r \n");

    /* Node status is set to active and the number of standby nodes 
     * are updated */
    ARP_GET_NODE_STATUS () = RM_ACTIVE;
    ARP_RM_GET_NUM_STANDBY_NODES_UP ();

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedHandleIdleToActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedHandleIdleToStandby                       */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedHandleIdleToStandby (VOID)
{
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedHandleIdleToStandby\r \n");
#if defined (NPAPI_WANTED) && defined (MBSM_WANTED) && defined (CFA_WANTED)
    /* This filter will remove copytocpu and add the
     * RedirectPort action for Ucast and will disable
     * Bcast filter */
    if (ArpFsNpCfaModifyFilter (GO_STANDBY) == FNP_FAILURE)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedHandleIdleToStandby: Failed to Modify CFA_NP_ARP_UCAST "
                 "and CFA_NP_ARP_BCAST Filter action\r\n");
    }
#endif

    /* the node status is set to standby and no of standby nodes is set to 0 */
    ARP_GET_NODE_STATUS () = RM_STANDBY;
    ARP_NUM_STANDBY_NODES () = 0;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "ArpRedHandleIdleToStandby: Node Status Idle to Standby\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                  "ArpRedHandleIdleToStandby: Node Status Idle to Standby"));
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedHandleIdleToStandby\r \n");

    return;
}

/************************************************************************/
/* Function Name      : ArpStartTimers                                  */
/*                                                                      */
/* Description        : This is a recursive function which is registered*/
/*                      to RBTreeWalk. The RBTree is walked and the     */
/*                      elements in the RBtree is checked for either    */
/*                      static or dynamic entry. If static entry, then  */
/*                      the timer is not started. If the entry is       */
/*                      dynamic, then the cache timer is started.       */
/*                                                                      */
/* Input(s)           : pRBElem - RBTree to walk.                       */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RB_WALK_CONT/RB_WALK_BREAK                      */
/************************************************************************/

INT4
ArpStartTimers (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                void *pArg, void *pOut)
{
    tArpCxt            *pArpCxt = NULL;
    t_ARP_CACHE        *pArpCache = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4Duration = 0;
    UINT4               u4ContextId = 0;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);
    UNUSED_PARAM (pOut);

    if (visit == postorder || visit == leaf)
    {

        if (pRBElem != NULL)
        {
            pArpCache = (t_ARP_CACHE *) pRBElem;
            if ((pArpCache->i1State == ARP_STATIC) ||
                (pArpCache->i1State == ARP_STATIC_NOT_IN_SERVICE) ||
                (pArpCache->i1State == ARP_INVALID))
            {
                return RB_WALK_CONT;
            }

            if (NetIpv4GetIfInfo ((UINT4) pArpCache->u2Port, &NetIpIfInfo) ==
                NETIPV4_FAILURE)
            {

                ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                         CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                         "ArpRedHandleStandbyToActive:Get interface info failure "
                         " \r \n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                              "ArpRedHandleStandbyToActive:Get interface info "
                              "failure"));
                return RB_WALK_BREAK;
            }

            u4ContextId = NetIpIfInfo.u4ContextId;

            pArpCxt = gArpGblInfo.apArpCxt[u4ContextId];

            if (pArpCache->i1State == ARP_DYNAMIC)
            {
                pArpCache->Timer.u1Id = ARP_CACHE_TIMER_ID;
                u4Duration = pArpCxt->Arp_config.i4Cache_timeout;
            }
            else
            {
                pArpCache->Timer.u1Id = ARP_REQUEST_RETRY_TIMER_ID;
                u4Duration = ARP_REQUEST_RETRY_TIMEOUT;
                pArpCache->i4Retries = ARP_ONE;
                ArpCheckAndSendReq (pArpCache->i1State, pArpCache->u2Port,
                                    pArpCache->u4Ip_addr, pArpCache->i2Hardware,
                                    pArpCache->u1EncapType);
            }

            ArpCheckAndStartTimer (pArpCache, u4Duration);

        }
        else
        {
            return RB_WALK_BREAK;
        }
    }
    return RB_WALK_CONT;
}

/************************************************************************/
/* Function Name      : ArpRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedHandleStandbyToActive (VOID)
{
    UINT4               u4CurrentTime;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedHandleStandbyToActive\r \n");

    OsixGetSysTime ((tOsixSysTime *) & u4CurrentTime);
    gArpRedGlobalInfo.i4ArpRedEntryTime = (INT4) u4CurrentTime;
    /* Hardware audit is done and the hardware and software entries are 
     * checked for synchronization */
    ArpRedHwAudit ();

    ARP_GET_NODE_STATUS () = RM_ACTIVE;
    ARP_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Timers to be started, check the dynamic arp entry. 
     * If in pending/ageout state, then start the arp age out 
     * timer and send the arp request. If in dynamic state,
     * start the arp cache timer. */

    OsixEvtSend (ARP_TASK_ID, ARP_RED_START_TIMER_EVENT);

    OsixGetSysTime ((tOsixSysTime *) & u4CurrentTime);
    gArpRedGlobalInfo.i4ArpRedExitTime = (INT4) u4CurrentTime;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "ArpRedHandleStandbyToActive: Node Status Standby to Active\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                  "ArpRedHandleStandbyToActive: Node Status Standby to Active"));
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedHandleStandbyToActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedHandleActiveToStandby (VOID)
{
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedHandleActiveToStandby\r \n");

    /*update the statistics */
    ARP_GET_NODE_STATUS () = RM_STANDBY;
    ARP_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Stop the running timers in the active node. Check whether any timers are 
     * running, if running, stop those timers. If there are any messages in the 
     * queue, discard those messages. Delete all the dynamically learnt entries
     * and flush those memories. These entries will be learnt from the new
     * active node through bulk updated */

    ArpDisable ();
    ArpEnable ();

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "ArpRedHandleActiveToStandby: Node Status Active to Standby\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                  "ArpRedHandleActiveToStandby: Node Status Active to Standby"));
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedHandleActiveToStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessPeerMsgAtActive                    */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedProcessPeerMsgAtActive\r \n");

    ProtoEvt.u4AppId = RM_ARP_APP_ID;

    ARP_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);
    ARP_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

    if (u2OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u2Length != ARP_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u1MsgType == ARP_RED_BULK_REQ_MESSAGE)
    {
        gArpRedGlobalInfo.u1BulkUpdStatus = ARP_HA_UPD_NOT_STARTED;
        if (!ARP_IS_STANDBY_UP ())
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gArpRedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;

            ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                     CONTROL_PLANE_TRC, ARP_NAME,
                     "ArpRedProcessPeerMsgAtActive:Bulk request message "
                     "before RM_STANDBY_UP\r \n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                          "ArpRedProcessPeerMsgAtActive:Bulk request message "
                          "before RM_STANDBY_UP"));
            return;
        }
        ArpRedSendBulkUpdMsg ();
    }

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedProcessPeerMsgAtActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2RemMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedProcessPeerMsgAtStandby\r \n");

    ProtoEvt.u4AppId = RM_ARP_APP_ID;
    u2MinLen = ARP_RED_TYPE_FIELD_SIZE + ARP_RED_LEN_FIELD_SIZE;

    ARP_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

    ARP_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

    if (u2Length < u2MinLen)
    {
        /* If length is less that the minimum length, then ignore the message
         * and send error to RM */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);

        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedProcessPeerMsgAtStandby:RM_PROCESS Failure\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedProcessPeerMsgAtStandby:RM_PROCESS Failure"));
        return;
    }

    u2RemMsgLen = (UINT2) (u2Length - u2MinLen);

    if ((u2OffSet + u2RemMsgLen) != u2DataLen)
    {
        /* If there is a length mismatch between the message and the given 
         * length, ignore the message and send error to RM */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedProcessPeerMsgAtStandby:RM_PROCESS Failure\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedProcessPeerMsgAtStandby:RM_PROCESS Failure"));
        return;
    }
    switch (u1MsgType)
    {
        case ARP_RED_BULK_UPD_TAIL_MESSAGE:
            if (u2Length != ARP_RED_BULK_UPD_TAIL_MSG_SIZE)
            {
                ProtoEvt.u4Error = RM_PROCESS_FAIL;

                RmApiHandleProtocolEvent (&ProtoEvt);
                ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                         CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                         "ArpRedProcessPeerMsgAtStandby:RM_PROCESS Failure\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                              "ArpRedProcessPeerMsgAtStandby:RM_PROCESS"
                              " Failure"));
                return;
            }
            ArpRedProcessBulkTailMsg (pMsg, &u2OffSet);
            break;
        case ARP_RED_BULK_CACHE_INFO:
            ArpRedProcessBulkInfo (pMsg, &u2OffSet);
            break;
        case ARP_RED_DYN_CACHE_INFO:
            ArpRedProcessDynamicInfo (pMsg, &u2OffSet);
            break;
        case ARP_RED_DYN_CACHE_TIME_INFO:
            u4OffSet = (UINT4) u2OffSet;
            ArpRedProcessDynamicCacheTimeInfo (pMsg, &u4OffSet);
            break;
    }

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedProcessPeerMsgAtStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the ARP module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
ArpRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedRmReleaseMemoryForMsg\r \n");
    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {

        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "ArpRedRmReleaseMemoryForMsg:Failure in releasing allocated"
                 " memory\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedRmReleaseMemoryForMsg:Failure in releasing allocated"
                      " memory"));
        return OSIX_FAILURE;
    }
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedRmReleaseMemoryForMsg\r \n");

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : ArpRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
ArpRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = 0;
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Entering ArpRedSendMsgToRm\r \n");

    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, u2Length,
                                     RM_ARP_APP_ID, RM_ARP_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedSendMsgToRm:Freememory due to message send "
                 "failure\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendMsgToRm:Freememory due to message send"
                      " failure"));
        return OSIX_FAILURE;
    }

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Exiting ArpRedSendMsgToRm\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : ArpRedSendBulkReqMsg                            */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = 0;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Entering ArpRedSendBulkReqMsg\r \n");

    ProtoEvt.u4AppId = RM_ARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    ARP Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (ARP_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "ArpRedSendBulkReqMsg: RM Memory allocation failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendBulkReqMsg: RM Memory allocation failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, ARP_RED_BULK_REQ_MESSAGE);
    ARP_RM_PUT_2_BYTE (pMsg, &u2OffSet, ARP_RED_BULK_REQ_MSG_SIZE);

    if (ArpRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedSendBulkReqMsg: Send message to RM failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendBulkReqMsg: Send message to RM failed"));
        return;
    }

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Exiting ArpRedSendBulkReqMsg\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedSendBulkUpdMsg (VOID)
{
    UINT1               u1BulkUpdPendFlg = OSIX_FALSE;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Entering ArpRedSendBulkUpdMsg\r \n");

    if (ARP_IS_STANDBY_UP () == OSIX_FALSE)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedSendBulkUpMsg:ARP Standby up failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendBulkUpMsg:ARP stand by up failure"));
        return;
    }

    if (gArpRedGlobalInfo.u1BulkUpdStatus == ARP_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        gArpRedGlobalInfo.u1BulkUpdStatus = ARP_HA_UPD_IN_PROGRESS;
        MEMSET (&gArpRedGlobalInfo.ArpHACacheMarker, 0,
                sizeof (tArpHACacheMarker));
    }
    if (gArpRedGlobalInfo.u1BulkUpdStatus == ARP_HA_UPD_IN_PROGRESS)
    {
        /* If bulk update flag is in progress, sent the bulk cache */
        if (ArpRedSendBulkCacheInfo (&u1BulkUpdPendFlg) != ARP_SUCCESS)
        {
            /* If there is any error in sending bulk update, set the 
             * bulk update flag to aborted */
            gArpRedGlobalInfo.u1BulkUpdStatus = ARP_HA_UPD_ABORTED;
            ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                     CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                     "ArpRedSendBulkUpdMsg: failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                          "ArpRedSendBulkUpdMsg: failed"));
        }
        else
        {
            if (u1BulkUpdPendFlg == OSIX_TRUE)
            {
                /* Send an event to the ARP main task indicating that there
                 * are pending entries in the ARP database to be synced */

                OsixEvtSend (ARP_TASK_ID, ARP_HA_PEND_BLKUPD_EVENT);
            }
            else
            {
                RmSetBulkUpdatesStatus (RM_ARP_APP_ID);
                /* Send the tail msg to indicate the completion of Bulk
                 * update process.
                 */
                gArpRedGlobalInfo.u1BulkUpdStatus = ARP_HA_UPD_COMPLETED;
                ArpRedSendBulkUpdTailMsg ();
            }
        }
    }

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Exiting ArpRedSendBulkUpdMsg\r \n");
    return;

}

/***********************************************************************
 * Function Name      : ArpRedSendBulkCacheInfo                         
 *                                                                      
 * Description        : This function sends the Bulk update messages to 
 *                      the peer standby ARP.                           
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : pu1BulkUpdPendFlg                               
 *                                                                      
 * Returns            : ARP_SUCCESS/ARP_FAILURE                         
 ***********************************************************************/

PUBLIC INT4
ArpRedSendBulkCacheInfo (UINT1 *pu1BulkUpdPendFlg)
{
    tRmMsg             *pMsg = NULL;
    tArpHACacheMarker  *pArpCacheMarker = NULL;
    t_ARP_CACHE        *pArpCache = NULL;
    t_ARP_CACHE        *pPrevArpCache = NULL;
    t_ARP_CACHE         ArpCache;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2LenOffSet = 0;
    UINT2               u2MsgPacked = 0;
    UINT2               u2NoCacheOffset = 0;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedSendBulkCacheInfo\r \n");
    ProtoEvt.u4AppId = RM_ARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    ARP Bulk Update message - dynamic entry
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 4 B ->|<- 6 B ->|<-2B->|<-2B->|
     *    -------------------------------------------------------------------
     *    | Msg. Type | Length | No of   | IP      | HW      | Port | HW   |
     *    |           |        | Entries | Address | Address | No   | Type |
     *    -------------------------------------------------------------------
     *
     *    <- 1 B ->|<-1 B->|<- 1 B ->|
     *    ----------------------------
     *    | Row    | Entry | Hw     |
     *    | Status | State | Status |
     *    ----------------------------
     *
     *    ARP Bulk Update message - static entry
     *
     *    |<- 4 B ->|<-2B->|<-1 B->|
     *    ---------------------------
     *    | IP      | Port | Entry |
     *    | Address | No   | State |
     *    ---------------------------
     *
     */

    if ((pMsg = RM_ALLOC_TX_BUF (ARP_RED_MAX_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "ArpRedSendBulkCacheInfo:RM Memory allocation failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendBulkCacheInfo:RM Memory allocation failed"));
        return ARP_FAILURE;
    }

    pArpCacheMarker = &(gArpRedGlobalInfo.ArpHACacheMarker);
    MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
    ArpCache.u4Ip_addr = pArpCacheMarker->u4IpAddr;
    ArpCache.u4IfIndex = pArpCacheMarker->u4IfIndex;
    pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                               (tRBElem *) & ArpCache, NULL);

    ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, ARP_RED_BULK_CACHE_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u2LenOffSet = u2OffSet;
    ARP_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    u2NoCacheOffset = u2OffSet;
    ARP_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgPacked);

    while (pArpCache != NULL)
    {
        if ((pArpCache->i1State == ARP_STATIC_NOT_IN_SERVICE) ||
            (pArpCache->i1State == ARP_INVALID))
        {
            /* If the entry is configured static, then no need to send in
             * dynamic update. So skip to next entry */
            ArpCache.u4Ip_addr = pArpCache->u4Ip_addr;
            ArpCache.u4IfIndex = pArpCache->u4IfIndex;
            pPrevArpCache = pArpCache;
            pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                                       (tRBElem *) & ArpCache, NULL);
            continue;
        }

        if ((u2OffSet + ARP_RED_DYN_INFO_SIZE) < ARP_RED_MAX_MSG_SIZE)
        {
            ARP_RM_PUT_4_BYTE (pMsg, &u2OffSet, pArpCache->u4Ip_addr);
            ARP_RM_PUT_2_BYTE (pMsg, &u2OffSet, pArpCache->u2Port);
            ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, pArpCache->i1State);

            if (pArpCache->i1State == ARP_STATIC)
            {
                ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, pArpCache->u1HwStatus);
            }
            else
            {
                ARP_RM_PUT_N_BYTE (pMsg, &u2OffSet, pArpCache->i1Hw_addr,
                                   CFA_ENET_ADDR_LEN);
                ARP_RM_PUT_2_BYTE (pMsg, &u2OffSet, pArpCache->i2Hardware);
                ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, pArpCache->u1RowStatus);
                ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, pArpCache->u1HwStatus);
                ARP_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                                   pArpCache->u4LastUpdatedTime);
            }
            u2MsgPacked += 1;
        }
        else
        {
            /* If message size is greater than MAX_SIZE, set the pending flag
             * as true and break */
            *pu1BulkUpdPendFlg = OSIX_TRUE;
            break;
        }
        ArpCache.u4Ip_addr = pArpCache->u4Ip_addr;
        ArpCache.u4IfIndex = pArpCache->u4IfIndex;
        pPrevArpCache = pArpCache;
        pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                                   (tRBElem *) & ArpCache, NULL);
    }

    ARP_RM_PUT_2_BYTE (pMsg, &u2LenOffSet, u2OffSet);
    ARP_RM_PUT_2_BYTE (pMsg, &u2NoCacheOffset, u2MsgPacked);
    if (u2MsgPacked == 0)
    {
        /* If message packet is 0, then do not send the message */
        RM_FREE (pMsg);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC, ARP_NAME,
                 "ArpRedSendBulkCacheInfo: RM Freed\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendBulkCacheInfo: RM Freed"));
        return ARP_SUCCESS;
    }

    if (ArpRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        *pu1BulkUpdPendFlg = OSIX_FALSE;
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "ArpRedSendBulkCacheInfo: Sending message to RM failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendBulkCacheInfo: Sending message to RM failed"));
        return ARP_FAILURE;
    }

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedSendBulkCacheInfo\r \n");
    /* ArpCacheMarker is set to the last updated entry */
    pArpCacheMarker->u4IpAddr = pPrevArpCache->u4Ip_addr;
    pArpCacheMarker->u4IfIndex = pPrevArpCache->u4IfIndex;
    return ARP_SUCCESS;
}

/************************************************************************/
/* Function Name      : ArpRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the ARP offers an IP address         */
/*                      to the ARP client and dynamically update the    */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pArpBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
ArpRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedSendBulkUpdTailMsg\r \n");

    ProtoEvt.u4AppId = RM_ARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr * ARP_RED_BULK_UPD_TAIL_MSG * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (ARP_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "ArpRedSendBulkUpdTailMsg: RM Memory allocation"
                 " failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendBulkUpdTailMsg: RM Memory allocation "
                      "failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    u2OffSet = 0;

    ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, ARP_RED_BULK_UPD_TAIL_MESSAGE);
    ARP_RM_PUT_2_BYTE (pMsg, &u2OffSet, ARP_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (ArpRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedSendBulkUpdTailMsg:Send message to RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendBulkUpdTailMsg:Send message to RM failed"));
        return;
    }
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedSendBulkUpdTailMsg\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
ArpRedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_ARP_APP_ID;
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedProcessBulkTailMsg\r \n");

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "ArpRedProcessBulkTailMsg: Bulk Update Tail Message received"
             " at Standby node...\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4ArpSysLogId,
                  "ArpRedProcessBulkTailMsg: Bulk Update Tail Message"
                  " received at Standby node"));

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    RmApiHandleProtocolEvent (&ProtoEvt);

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedProcessBulkTailMsg\r \n");

    return;
}

/************************************************************************
 * Function Name      : ArpRedSendDynamicCacheInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, when the ARP offers an IP address 
 *                      to the ARP client and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : None
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/

PUBLIC VOID
ArpRedSendDynamicCacheInfo ()
{
    tRmMsg             *pMsg = NULL;
    tArpRedTable       *pArpRedInfo = NULL;
    tArpRedTable       *pBkpArpRedInfo = NULL;
    tArpRedTable        ArpRedCacheInfo;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4MsgAllocSize = 0;
    UINT4               u4Count = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2LenOffSet = 0;
    UINT2               u2MsgPacked = 0;
    UINT2               u2NoCacheOffset = 0;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedSendDynamicCacheInfo\r \n");
    ProtoEvt.u4AppId = RM_ARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    RBTreeCount (gArpGblInfo.ArpRedTable, &u4Count);
    u4MsgAllocSize = ARP_RED_MIM_MSG_SIZE;
    u4MsgAllocSize += (u4Count * (ARP_RED_DYN_INFO_SIZE + ARP_ONE_BYTE));
    u4MsgAllocSize = (u4MsgAllocSize < ARP_RED_MAX_MSG_SIZE) ?
        u4MsgAllocSize : ARP_RED_MAX_MSG_SIZE;

    /* Message is allcoated upto required size. The number of entries in the 
     * RBTree is got and then it is multiplied with the size of a single 
     * dynamic update to get the total count. If that length is greater than
     * MAX size, then the message is allocated upto max size */

    /*
     *    ARP Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|< 1 B ->|<- 4 B ->|<- 6 B ->|<-2B->|
     *    ---------------------------------------------------------------------
     *    | Msg. Type | Length | No of   | Action | IP      | HW      | Port |
     *    |           |        | Entries |        | Address | Address | No   |
     *    ---------------------------------------------------------------------
     *
     *    <-2B->|<- 1 B ->|<-1 B->|<- 1 B ->
     *    -----------------------------------
     *    | HW   | Row    | Entry | Hw     |
     *    | Type | Status | State | Status |
     *    -----------------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (u4MsgAllocSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);

        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "ArpRedSendBulkUpdTailMsg: RM Memory allocation"
                 " failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendBulkUpdTailMsg: RM Memory allocation"
                      " failed"));

        return;
    }

    pArpRedInfo = RBTreeGetFirst (gArpGblInfo.ArpRedTable);

    ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, ARP_RED_DYN_CACHE_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u2LenOffSet = u2OffSet;
    ARP_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    u2NoCacheOffset = u2OffSet;
    ARP_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgPacked);

    while (pArpRedInfo != NULL)
    {
        if ((UINT4) (u2OffSet + ARP_ONE_BYTE + ARP_RED_MSG_SIZE (pArpRedInfo))
            > u4MsgAllocSize)
        {
            /* If there message size is greater than the allocated size, then
             * the remaining entries are sent in another update. */
            break;
        }

        ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, pArpRedInfo->u1Action);
        ARP_RM_PUT_4_BYTE (pMsg, &u2OffSet, pArpRedInfo->u4IpAddr);

        ARP_RM_PUT_N_BYTE (pMsg, &u2OffSet, pArpRedInfo->i1HwAddr,
                           CFA_ENET_ADDR_LEN);
        ARP_RM_PUT_2_BYTE (pMsg, &u2OffSet, pArpRedInfo->u2Port);
        ARP_RM_PUT_2_BYTE (pMsg, &u2OffSet, pArpRedInfo->i2IntfType);
        ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, pArpRedInfo->u1RowStatus);
        ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, pArpRedInfo->i1State);
        ARP_RM_PUT_1_BYTE (pMsg, &u2OffSet, pArpRedInfo->u1HwStatus);
        ARP_RM_PUT_4_BYTE (pMsg, &u2OffSet, pArpRedInfo->u4LastUpdatedtime);

        u2MsgPacked += 1;

        ArpRedCacheInfo.u4IpAddr = pArpRedInfo->u4IpAddr;
        ArpRedCacheInfo.u4IfIndex = pArpRedInfo->u4IfIndex;
        pArpRedInfo = RBTreeGetNext (gArpGblInfo.ArpRedTable,
                                     (tRBElem *) & ArpRedCacheInfo, NULL);
    }

    if (u2MsgPacked == 0)
    {
        /* If there are no new items to sent, then it the message is not sent */
        RM_FREE (pMsg);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedSendBulkUpdTailMsg:Freememory due to message send"
                 " failure\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendBulkUpdTailMsg:Freememory due to message "
                      "send failure"));
        return;
    }
    ARP_RM_PUT_2_BYTE (pMsg, &u2LenOffSet, u2OffSet);
    ARP_RM_PUT_2_BYTE (pMsg, &u2NoCacheOffset, u2MsgPacked);
    if (ArpRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedSendBulkUpdTailMsg:Send message to RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedSendBulkUpdTailMsg:Send message to RM failed"));
        return;
    }

    pArpRedInfo =
        RBTreeGet (gArpGblInfo.ArpRedTable, (tRBElem *) & ArpRedCacheInfo);
    if (pArpRedInfo != NULL)
    {
        pArpRedInfo->u1DelFlag = ARP_RED_CACHE_DEL;
    }
    /* The last updated message's delete flag is set */
    pArpRedInfo = RBTreeGetFirst (gArpGblInfo.ArpRedTable);
    while (pArpRedInfo != NULL)
    {
        ArpRedCacheInfo.u4IpAddr = pArpRedInfo->u4IpAddr;
        ArpRedCacheInfo.u4IfIndex = pArpRedInfo->u4IfIndex;
        pBkpArpRedInfo = RBTreeGetNext (gArpGblInfo.ArpRedTable,
                                        (tRBElem *) & ArpRedCacheInfo, NULL);

        /* The temporary RBTree is scanned till the entry with the delete flag 
         * is reached. If the hardware status is NP_UPDATED, the corresponding
         * entry is deleted, else the entry is not deleted, and the
         * delete flag is reset. */
        if (pArpRedInfo->u1HwStatus == NP_UPDATED)
        {
            RBTreeRem (gArpGblInfo.ArpRedTable, pArpRedInfo);
            if (pArpRedInfo->u1DelFlag == ARP_RED_CACHE_DEL)
            {
                MemReleaseMemBlock (ARP_DYN_MSG_POOL_ID, (UINT1 *) pArpRedInfo);
                break;
            }
            MemReleaseMemBlock (ARP_DYN_MSG_POOL_ID, (UINT1 *) pArpRedInfo);
        }
        else
        {
            if (pArpRedInfo->u1DelFlag == ARP_RED_CACHE_DEL)
            {
                pArpRedInfo->u1DelFlag = ARP_RED_CACHE_DONT_DEL;
                break;
            }
        }
        pArpRedInfo = pBkpArpRedInfo;
    }

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedSendDynamicCacheInfo\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessBulkInfo                           */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
ArpRedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tARP_DATA           ArpInfo;
    t_ARP_CACHE         ArpCacheInfo;
    t_ARP_CACHE        *pArpCacheInfo = NULL;
    t_ARP_CACHE        *pArpEntry = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;
    tArpRedTable        ArpRedNode;
    tArpRedTable       *pArpRedNode = NULL;
    UINT2               u2NoOfEntries = 0;
    UINT1               u1HwStatus = 0;
    UINT4               u4LastUpdatedTime = 0;
    INT4                i4RetVal = 0;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedProcessBulkInfo\r \n");
    MEMSET (&ArpInfo, 0, sizeof (tARP_DATA));
    MEMSET (&ArpCacheInfo, 0, sizeof (t_ARP_CACHE));
    MEMSET (&ArpRedNode, 0, sizeof (tArpRedTable));

    ARP_RM_GET_2_BYTE (pMsg, pu2OffSet, u2NoOfEntries);

    while (u2NoOfEntries > 0)
    {
        /* The number of entries is got from the message and it is processed */
        u2NoOfEntries--;
        ARP_RM_GET_4_BYTE (pMsg, pu2OffSet, ArpInfo.u4IpAddr);
        ARP_RM_GET_2_BYTE (pMsg, pu2OffSet, ArpInfo.u2Port);
        ARP_RM_GET_1_BYTE (pMsg, pu2OffSet, ArpInfo.i1State);
        if (ArpInfo.i1State == ARP_STATIC)
        {
            ARP_RM_GET_1_BYTE (pMsg, pu2OffSet, u1HwStatus);
        }
        else
        {
            ARP_RM_GET_N_BYTE (pMsg, pu2OffSet, ArpInfo.i1Hw_addr,
                               CFA_ENET_ADDR_LEN);
            ARP_RM_GET_2_BYTE (pMsg, pu2OffSet, ArpInfo.i2Hardware);
            ARP_RM_GET_1_BYTE (pMsg, pu2OffSet, ArpInfo.u1RowStatus);
            ARP_RM_GET_1_BYTE (pMsg, pu2OffSet, u1HwStatus);
            ARP_RM_GET_4_BYTE (pMsg, pu2OffSet, u4LastUpdatedTime);
        }

        ArpRedNode.u4IpAddr = ArpInfo.u4IpAddr;
        NetIpv4GetCfaIfIndexFromPort ((UINT4) ArpInfo.u2Port,
                                      &(ArpRedNode.u4IfIndex));

        if (u1HwStatus == NP_UPDATED)
        {
            pArpRedNode = RBTreeGet (gArpGblInfo.ArpRedTable,
                                     (tRBElem *) & ArpRedNode);
            /* If the hardware status is NP_UPDATED, then the corresponding
             * entry is deleted from the temporary RBTree and added to the 
             * software. If the hardware status is NP_NOT_UPDATED, then the 
             * corresponding entry is added to the temporary data structure */
            if (pArpRedNode != NULL)
            {
                RBTreeRem (gArpGblInfo.ArpRedTable, pArpRedNode);
                MemReleaseMemBlock (ARP_DYN_MSG_POOL_ID, (UINT1 *) pArpRedNode);
            }
            if (ArpInfo.i1State == ARP_STATIC)
            {
                ArpCacheInfo.u4Ip_addr = ArpRedNode.u4IpAddr;
                ArpCacheInfo.u4IfIndex = ArpRedNode.u4IfIndex;
                pArpCacheInfo = RBTreeGet (gArpGblInfo.ArpTable,
                                           (tRBElem *) & ArpCacheInfo);
                if (pArpCacheInfo != NULL)
                {
                    pArpCacheInfo->u1HwStatus = u1HwStatus;
                }
            }
            else
            {
                NetIpv4GetIfInfo ((UINT4) ArpInfo.u2Port, &NetIpIfInfo);
                ArpInfo.u1EncapType = NetIpIfInfo.u1EncapType;
                ArpInfo.i1Hwalen = CFA_ENET_ADDR_LEN;
                arp_add (ArpInfo);
            }
        }
        else
        {
            /* If the entry is not present in NP (this is applicable only in
             * async NP case), then the entry is added to the temporary 
             * RBtree and not updated in the software */

            if (ArpInfo.i1State == ARP_STATIC)
            {
                ArpCacheInfo.u4Ip_addr = ArpRedNode.u4IpAddr;
                ArpCacheInfo.u4IfIndex = ArpRedNode.u4IfIndex;
                pArpCacheInfo = RBTreeGet (gArpGblInfo.ArpTable,
                                           (tRBElem *) & ArpCacheInfo);
                ArpRedAddDynamicInfo (pArpCacheInfo, ARP_RED_ADD_CACHE);
            }
            else
            {
                ArpCacheInfo.u4Ip_addr = ArpInfo.u4IpAddr;
                NetIpv4GetCfaIfIndexFromPort ((UINT4) ArpInfo.u2Port,
                                              &(ArpCacheInfo.u4IfIndex));
                ArpCacheInfo.u2Port = ArpInfo.u2Port;
                ArpCacheInfo.i2Hardware = ArpInfo.i2Hardware;
                ArpCacheInfo.u1RowStatus = ArpInfo.u1RowStatus;
                ArpCacheInfo.u1HwStatus = u1HwStatus;
                ArpCacheInfo.i1State = ArpInfo.i1State;
                MEMCPY (ArpCacheInfo.i1Hw_addr, ArpInfo.i1Hw_addr,
                        CFA_ENET_ADDR_LEN);

                ArpRedAddDynamicInfo (&ArpCacheInfo, ARP_RED_ADD_CACHE);
            }
        }

        i4RetVal =
            ArpGetCacheWithIndex (ArpRedNode.u4IfIndex, ArpInfo.u4IpAddr,
                                  &pArpEntry);
        if (i4RetVal != ARP_FAILURE)
        {
            OsixGetSysTime ((tOsixSysTime *) & (pArpEntry->u4LastUpdatedTime));
        }
    }

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Exiting ArpRedProcessBulkInfo\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessDynamicInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
ArpRedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    t_ARP_CACHE        *pArpCache = NULL;
    t_ARP_CACHE         ArpCache;
    tARP_DATA           ArpInfo;
    tNetIpv4IfInfo      NetIpIfInfo;
    tArpRedTable        ArpRedNode;
    tArpRedTable       *pArpRedNode = NULL;
    UINT2               u2NoOfEntries = 0;
    UINT4               u4LastUpdatedTime = 0;
    UINT4               u4ArpCacheIfIndex = 0;
    UINT1               u1HwStatus = 0;
    UINT1               u1Action;
#ifdef LNXIP4_WANTED
    INT4                i4RetVal = 0;
    UINT1               au1IfName[MAX_IFNAME_SIZE];
    MEMSET (&au1IfName, 0, sizeof (MAX_IFNAME_SIZE));
#endif
    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedProcessDynamicInfo\r \n");

    MEMSET (&ArpRedNode, 0, sizeof (tArpRedTable));
    MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
    ARP_RM_GET_2_BYTE (pMsg, pu2OffSet, u2NoOfEntries);

    while (u2NoOfEntries > 0)
    {
        /* The number of entries is got from the message and it is processed */
        ARP_RM_GET_1_BYTE (pMsg, pu2OffSet, u1Action);
        u2NoOfEntries--;
        MEMSET (&ArpInfo, 0, sizeof (tARP_DATA));
        ARP_RM_GET_4_BYTE (pMsg, pu2OffSet, ArpInfo.u4IpAddr);
        ARP_RM_GET_N_BYTE (pMsg, pu2OffSet, ArpInfo.i1Hw_addr,
                           CFA_ENET_ADDR_LEN);
        ARP_RM_GET_2_BYTE (pMsg, pu2OffSet, ArpInfo.u2Port);
        ARP_RM_GET_2_BYTE (pMsg, pu2OffSet, ArpInfo.i2Hardware);
        ARP_RM_GET_1_BYTE (pMsg, pu2OffSet, ArpInfo.u1RowStatus);
        ARP_RM_GET_1_BYTE (pMsg, pu2OffSet, ArpInfo.i1State);
        ARP_RM_GET_1_BYTE (pMsg, pu2OffSet, u1HwStatus);
        ARP_RM_GET_4_BYTE (pMsg, pu2OffSet, u4LastUpdatedTime);
        NetIpv4GetCfaIfIndexFromPort ((UINT4) ArpInfo.u2Port,
                                      &u4ArpCacheIfIndex);

        if (u1Action == ARP_RED_ADD_CACHE)
        {
            /* If the entry is present in NP, (this is always true in sync
             * NP calls) then the entry is checked in the temporary RBtree.
             * If present, it is deleted from the RBtree. The entry is added
             * to the Software */
            if (u1HwStatus == NP_UPDATED)
            {
                ArpRedNode.u4IpAddr = ArpInfo.u4IpAddr;
                NetIpv4GetCfaIfIndexFromPort ((UINT4) ArpInfo.u2Port,
                                              &(ArpRedNode.u4IfIndex));
                pArpRedNode = RBTreeGet (gArpGblInfo.ArpRedTable,
                                         (tRBElem *) & ArpRedNode);
                if (pArpRedNode != NULL)
                {
                    RBTreeRem (gArpGblInfo.ArpRedTable, pArpRedNode);
                    MemReleaseMemBlock (ARP_DYN_MSG_POOL_ID,
                                        (UINT1 *) pArpRedNode);
                }
                NetIpv4GetIfInfo ((UINT4) ArpInfo.u2Port, &NetIpIfInfo);
                ArpInfo.u1EncapType = NetIpIfInfo.u1EncapType;
                ArpInfo.i1Hwalen = CFA_ENET_ADDR_LEN;

                arp_add (ArpInfo);
            }
            /* If the entry is not present in NP (this is applicable only in 
             * async NP case), then the entry is added to the temporary 
             * RBtree and not updated in the software */
            else
            {
                ArpCache.u4Ip_addr = ArpInfo.u4IpAddr;
                NetIpv4GetCfaIfIndexFromPort ((UINT4) ArpInfo.u2Port,
                                              &(ArpCache.u4IfIndex));
                ArpCache.u2Port = ArpInfo.u2Port;
                ArpCache.i2Hardware = ArpInfo.i2Hardware;
                ArpCache.u1RowStatus = ArpInfo.u1RowStatus;
                ArpCache.u1HwStatus = u1HwStatus;
                ArpCache.i1State = ArpInfo.i1State;
                MEMCPY (ArpCache.i1Hw_addr, ArpInfo.i1Hw_addr,
                        CFA_ENET_ADDR_LEN);

                ArpRedAddDynamicInfo (&ArpCache, ARP_RED_ADD_CACHE);
            }
        }
        else
        {

            MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
            ArpCache.u4Ip_addr = ArpInfo.u4IpAddr;
            ArpCache.u2Port = ArpInfo.u2Port;
            ArpCache.i2Hardware = ArpInfo.i2Hardware;
            ArpCache.u1RowStatus = ArpInfo.u1RowStatus;
            ArpCache.u1HwStatus = u1HwStatus;
            ArpCache.i1State = ArpInfo.i1State;
            MEMCPY (ArpCache.i1Hw_addr, ArpInfo.i1Hw_addr, CFA_ENET_ADDR_LEN);

            /* flag is NP_UPDATED which indicates entry from hardware
             * is deleted.Hence delete the same entry from s/w. */

            if (u1HwStatus == NP_UPDATED)
            {
                NetIpv4GetCfaIfIndexFromPort ((UINT4) ArpInfo.u2Port,
                                              &(ArpCache.u4IfIndex));
                pArpCache =
                    RBTreeGet (gArpGblInfo.ArpTable, (tRBElem *) & ArpCache);
                if (pArpCache != NULL)
                {
                    if ((pArpCache->i1State == ARP_STATIC) ||
                        (pArpCache->i1State == ARP_STATIC_NOT_IN_SERVICE) ||
                        (pArpCache->i1State == ARP_INVALID))
                    {
                        continue;
                    }
                    RBTreeRem (gArpGblInfo.ArpTable, pArpCache);
#if defined (LNXIP4_WANTED)
                    /* Once the entry present in the software is removed,
                     *delete the corresponding entry in the kernel arp cache if LNXIP_WANTED*/
                    if (NetIpv4GetLinuxIfName
                        ((UINT4) pArpCache->u2Port,
                         au1IfName) == NETIPV4_SUCCESS)
                    {
                        i4RetVal =
                            ArpUpdateKernelEntry (ARP_INVALID,
                                                  (UINT1 *) pArpCache->
                                                  i1Hw_addr,
                                                  pArpCache->i1Hwalen,
                                                  pArpCache->u4Ip_addr,
                                                  au1IfName);
                        if (i4RetVal == ARP_FAILURE)
                        {
                            ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                                     CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                     ARP_NAME,
                                     "ArpRedProcessDynamicInfo:Removing the entry in \
                                    kernel failed \r\n");
                        }
                    }
                    else
                    {
                        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                                 "Getting Linux Port Name failed\r\n");
                    }
#endif /* LNXIP4_WANTED */

                    MemReleaseMemBlock (ARP_POOL_ID, (UINT1 *) pArpCache);
                }
                /* Delete same entry from RED table */
                MEMSET (&ArpRedNode, 0, sizeof (tArpRedTable));
                ArpRedNode.u4IpAddr = ArpInfo.u4IpAddr;
                NetIpv4GetCfaIfIndexFromPort ((UINT4) ArpInfo.u2Port,
                                              &(ArpRedNode.u4IfIndex));
                pArpRedNode = RBTreeGet (gArpGblInfo.ArpRedTable,
                                         (tRBElem *) & ArpRedNode);
                if (pArpRedNode != NULL)
                {
                    RBTreeRem (gArpGblInfo.ArpRedTable, pArpRedNode);
                    MemReleaseMemBlock (ARP_DYN_MSG_POOL_ID,
                                        (UINT1 *) pArpRedNode);
                }
            }
            /* If flag is NOT_UPDATED , entry from h/w is not yet deleted.
             * hence ignore the deletion and add the entry in temporary 
             * Red table to avoid the loss of data */
            else
            {
                ArpCache.u4Ip_addr = ArpInfo.u4IpAddr;
                NetIpv4GetCfaIfIndexFromPort ((UINT4) ArpInfo.u2Port,
                                              &(ArpCache.u4IfIndex));
                ArpCache.u2Port = ArpInfo.u2Port;
                ArpCache.i2Hardware = ArpInfo.i2Hardware;
                ArpCache.u1RowStatus = ArpInfo.u1RowStatus;
                ArpCache.u1HwStatus = u1HwStatus;
                ArpCache.i1State = ArpInfo.i1State;
                MEMCPY (ArpCache.i1Hw_addr, ArpInfo.i1Hw_addr,
                        CFA_ENET_ADDR_LEN);

                ArpRedAddDynamicInfo (&ArpCache, ARP_RED_ADD_CACHE);
            }
        }
    }

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Exiting ArpRedProcessDynamicInfo\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedAddDynamicInfo                            */
/*                                                                      */
/* Description        : This function adds the dynamic entries to the   */
/*                      global tree and this tree is scanned everytime  */
/*                      for sending the dynamic entry.                  */
/*                                                                      */
/* Input(s)           : pArpCache - ArpCacje message                    */
/*                      u1Operation - add or delete entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
ArpRedAddDynamicInfo (t_ARP_CACHE * pArpCache, UINT1 u1Operation)
{
    tArpRedTable       *pArpRedNode = NULL;
    tArpRedTable       *pTmpArpRedNode = NULL;
    tArpHACacheMarker  *pArpCacheMarker = NULL;
    tArpRedTable        ArpRedNode;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Entering ArpRedAddDynamicInfo\r \n");
    if (pArpCache == NULL)
    {
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedAddDynamicInfo:Arp Cache is NULL\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedAddDynamicInfo:Arp Cache is NULL"));
        return;
    }
    if ((pArpCache->i1State == ARP_STATIC) ||
        (pArpCache->i1State == ARP_STATIC_NOT_IN_SERVICE) ||
        (pArpCache->i1State == ARP_INVALID))
    {
        /* Adding entry in the RBtree, if the entry state is static, skip the 
         * entry */
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedAddDynamicInfo:Entry state is static\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedAddDynamicInfo:Entry state is static"));
        return;
    }

    if ((ARP_GET_NODE_STATUS () != RM_STANDBY) &&
        ((gArpRedGlobalInfo.u1BulkUpdStatus == ARP_HA_UPD_NOT_STARTED) ||
         (ARP_IS_STANDBY_UP () == OSIX_FALSE)))
    {
        /* If bulk update is not started, then do not add the entry in the
         * RBtree as this will be taken care through bulk update */
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedAddDynamicInfo:BulkUpdate not started\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedAddDynamicInfo:Bulk Update not started"));
        return;
    }

    pArpCacheMarker = &(gArpRedGlobalInfo.ArpHACacheMarker);
    if ((gArpRedGlobalInfo.u1BulkUpdStatus == ARP_HA_UPD_IN_PROGRESS) &&
        ((pArpCacheMarker->u4IfIndex < pArpCache->u4IfIndex) ||
         ((pArpCacheMarker->u4IfIndex == pArpCache->u4IfIndex) &&
          (pArpCacheMarker->u4IpAddr < pArpCache->u4Ip_addr))))
    {
        /* If the added/updated/deleted entry is after the marker, then it will
         * be taken care through dynamic update. No need to add that entry
         * in the RBtree. Skip the entry */
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedAddDynamicInfo:Bulk Update not complete, this will be"
                 " taken care in bulk\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                      "ArpRedAddDynamicInfo:Bulk Update not complete, this will "
                      "be taken care in bulk"));
        return;
    }

    MEMSET (&ArpRedNode, 0, sizeof (tArpRedTable));
    /* Check whether the entry is already present in the RBtree, if present
     * just update the fields. If not present, allocate memory and add the 
     * entry to the RBtree */
    ArpRedNode.u4IpAddr = pArpCache->u4Ip_addr;
    ArpRedNode.u4IfIndex = pArpCache->u4IfIndex;
    pTmpArpRedNode = RBTreeGet (gArpGblInfo.ArpRedTable,
                                (tRBElem *) & ArpRedNode);
    if (pTmpArpRedNode == NULL)
    {
        pArpRedNode =
            (tArpRedTable *) MemAllocMemBlk ((tMemPoolId) ARP_DYN_MSG_POOL_ID);
        if (pArpRedNode == NULL)
        {
            ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                     CONTROL_PLANE_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                     ARP_NAME,
                     "ArpRedAddDynamicInfo:Memory Allocation Failure\r \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4ArpSysLogId,
                          "ArpRedAddDynamicInfo:Memory Allocation Failure"));
            return;
        }
        MEMSET (pArpRedNode, 0, sizeof (tArpRedTable));
        pArpRedNode->u4IpAddr = pArpCache->u4Ip_addr;
        pArpRedNode->u4IfIndex = pArpCache->u4IfIndex;
        if (RBTreeAdd (gArpGblInfo.ArpRedTable, pArpRedNode) == RB_FAILURE)
        {
            return;
        }
    }
    else
    {
        pArpRedNode = pTmpArpRedNode;
    }

    pArpRedNode->u1Action = u1Operation;
    pArpRedNode->i2IntfType = pArpCache->i2Hardware;
    pArpRedNode->u2Port = pArpCache->u2Port;
    pArpRedNode->u1RowStatus = pArpCache->u1RowStatus;
    pArpRedNode->i1State = pArpCache->i1State;
    MEMCPY (pArpRedNode->i1HwAddr, pArpCache->i1Hw_addr, CFA_ENET_ADDR_LEN);
    pArpRedNode->u1HwStatus = pArpCache->u1HwStatus;
    pArpRedNode->u4LastUpdatedtime = pArpCache->u4LastUpdatedTime;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Exiting ArpRedAddDynamicInfo\r \n");
    return;
}

/************************************************************************/
/* Function Name      : ArpRedHwAudit                                   */
/*                                                                      */
/* Description        : This function does the hardware audit in two    */
/*                      approches.                                      */
/*                                                                      */
/*                      First:                                          */
/*                      When there is a transaction between standby and */
/*                      active node, the ArpRedTable is walked, if      */
/*                      there  are any entries in the table, they are   */
/*                      verified with the hardware, if the entry is     */
/*                      present in the hardware, then the entry is      */
/*                      added to the sofware. If not, the entry is      */
/*                      deleted.                                        */
/*                                                                      */
/*                      Second:                                         */
/*                      When there is a transaction between standby and */
/*                      active node, the entries in the hardware are    */
/*                      retrived and checked with the software. If      */
/*                      is a mismatch in the entry, the entry in the    */
/*                      hardware is updated to the software.            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
ArpRedHwAudit ()
{
#ifdef NPAPI_WANTED
    tArpRedTable       *pArpRedInfo = NULL;
    t_ARP_CACHE        *pArpCache = NULL;
    t_ARP_CACHE        *pPrevArpCache = NULL;
    t_ARP_CACHE         ArpCache;
    tARP_DATA           ArpData;
    tNetIpv4IfInfo      NetIpIfInfo;
    tArpNpInParams      ArpNpInParam;
    tArpNpOutParams     ArpNpOutParam;
    INT4                i4RetVal = 0;
#ifdef LNXIP4_WANTED
    UINT1               au1IfName[MAX_IFNAME_SIZE];
    MEMSET (&au1IfName, 0, sizeof (MAX_IFNAME_SIZE));
#endif

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Entering ArpRedHwAudit\r \n");

    MEMSET (&ArpNpInParam, 0, sizeof (tArpNpInParams));
    MEMSET (&ArpNpOutParam, 0, sizeof (tArpNpOutParams));
    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    /* If the audit level is optimized(applicable only in async NP calls) the
     * temporary RBtree is scanned and the entries in the RBtree are checked 
     * in the hardware. If present, then it is added in the software. If not
     * then the entry is ignored. In sync NP calls, there wont be any need to 
     * do the hardware audit in this manner as the entries are added to the
     * standby database only after hardware call is success */
#ifdef OPTIMIZED_HW_AUDIT_WANTED
    pArpRedInfo = RBTreeGetFirst (gArpGblInfo.ArpRedTable);
    while (pArpRedInfo != NULL)
    {
        if (NetIpv4GetIfInfo ((UINT4) pArpRedInfo->u2Port, &NetIpIfInfo) ==
            NETIPV4_FAILURE)
        {
            RBTreeRem (gArpGblInfo.ArpRedTable, pArpRedInfo);
            MemReleaseMemBlock (ARP_DYN_MSG_POOL_ID, (UINT1 *) pArpRedInfo);
            pArpRedInfo = RBTreeGetFirst (gArpGblInfo.ArpRedTable);

            continue;
        }

        ArpNpInParam.u4ContextId = NetIpIfInfo.u4ContextId;
        ArpNpInParam.u4IpAddr = pArpRedInfo->u4IpAddr;
        if (ArpNpGet (ArpNpInParam, &ArpNpOutParam) != ARP_FAILURE)
        {
            if (pArpRedInfo->i1State == ARP_DYNAMIC)
            {
                MEMSET (&ArpData, 0, sizeof (tARP_DATA));
                ArpData.u4IpAddr = pArpRedInfo->u4IpAddr;
                ArpData.u2Port = pArpRedInfo->u2Port;
                ArpData.i1State = pArpRedInfo->i1State;
                ArpData.u1EncapType = NetIpIfInfo.u1EncapType;
                ArpData.i1Hwalen = CFA_ENET_ADDR_LEN;
                ArpData.i2Hardware = pArpRedInfo->i2IntfType;
                MEMCPY (ArpData.i1Hw_addr, pArpRedInfo->i1HwAddr,
                        CFA_ENET_ADDR_LEN);
                arp_add (ArpData);
            }
            else if (pArpRedInfo->i1State == ARP_STATIC)
            {
                MEMSET (&ArpCache, 0, sizeof (t_ARP_CACHE));
                ArpCache.u4Ip_addr = pArpRedInfo->u4IpAddr;
                ArpCache.u4IfIndex = pArpRedInfo->u4IfIndex;
                pArpCache = RBTreeGet (gArpGblInfo.ArpTable,
                                       (tRBElem *) & ArpCache);
                if (pArpCache != NULL)
                {
                    pArpCache->u1HwStatus = NP_UPDATED;
                }
            }
        }
        RBTreeRem (gArpGblInfo.ArpRedTable, pArpRedInfo);
        MemReleaseMemBlock (ARP_DYN_MSG_POOL_ID, (UINT1 *) pArpRedInfo);
        pArpRedInfo = RBTreeGetFirst (gArpGblInfo.ArpRedTable);
    }
    UNUSED_PARAM (i4RetVal);
    UNUSED_PARAM (pPrevArpCache);
#else
    u4IpAddr = 0;
    u4ContextId = 0;
    u4IfIndex = 0;
    /* In normal audit, the software and the hardware entry is scanned */

    pArpCache = RBTreeGetFirst (gArpGblInfo.ArpTable);
    i4RetVal = ArpNpGetNext (ArpNpInParam, &ArpNpOutParam);
    while ((pArpCache != NULL) || (i4RetVal == ARP_SUCCESS))
    {
        if ((pArpCache == NULL) || ((i4RetVal == ARP_SUCCESS) &&
                                    (ARP_INDEX_COMPARE
                                     (pArpCache,
                                      ArpNpOutParam) == ARP_GREATER)))
        {
            /* The entry is not present in software, and present in 
             * hardware, add the entry to the software and get next entry
             * in the hardware */
            MEMSET (&ArpData, 0, sizeof (tARP_DATA));
            ArpData.u4IpAddr = ArpNpOutParam.u4IpAddr;
            NetIpv4GetPortFromIfIndex (ArpNpOutParam.u4IfIndex,
                                       (UINT4 *) &(ArpData.u2Port));
            ArpData.i1State = ARP_DYNAMIC;
            NetIpv4GetIfInfo ((UINT4) ArpData.u2Port, &NetIpIfInfo);
            ArpData.u1EncapType = NetIpIfInfo.u1EncapType;
            ArpData.i1Hwalen = CFA_ENET_ADDR_LEN;
            ArpData.i2Hardware = ARP_IFACE_TYPE ((INT2) NetIpIfInfo.u4IfType);
            MEMCPY (ArpData.i1Hw_addr, ArpNpOutParam.HwAddr, CFA_ENET_ADDR_LEN);
            arp_add (ArpData);
            i4RetVal = ArpNpGetNext (ArpNpInParam, &ArpNpOutParam);
        }
        else if ((i4RetVal != ARP_SUCCESS) || ((pArpCache != NULL) &&
                                               (ARP_INDEX_COMPARE
                                                (pArpCache,
                                                 ArpNpOutParam) == ARP_LESSER)))
        {
            /* The entry is present in the software and not present in the
             * hardware, so delete the entry from the software and get
             * next entry in the software */

            pPrevArpCache = pArpCache;
            pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                                       (tRBElem *) pPrevArpCache, NULL);
            if (pPrevArpCache->i1State == ARP_DYNAMIC)
            {
                /* Decrement the corresponding context ArpEntry Count */
                pPrevArpCache->pArpCxt->u4ArpEntryCount--;
                RBTreeRem (gArpGblInfo.ArpTable, pPrevArpCache);
#if defined (LNXIP4_WANTED)
                /* Once the entry present in the software is removed, 
                 *delete the corresponding entry in the kernel arp cache if LNXIP_WANTED*/
                if (NetIpv4GetLinuxIfName
                    ((UINT4) pPrevArpCache->u2Port,
                     au1IfName) == NETIPV4_SUCCESS)
                {
                    i4RetVal =
                        ArpUpdateKernelEntry (ARP_INVALID,
                                              (UINT1 *) pPrevArpCache->
                                              i1Hw_addr,
                                              pPrevArpCache->i1Hwalen,
                                              pPrevArpCache->u4Ip_addr,
                                              au1IfName);
                    if (i4RetVal == ARP_FAILURE)
                    {
                        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                                 "ArpRedHwAudit:Removing the entry in \
                                kernel failed \r \n");
                    }

                }
                else
                {
                    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                             CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                             "Getting Linux Port Name failed\r \n");
                }
#endif /* LNXIP4_WANTED */
                TmrStopTimer (ARP_TIMER_LIST_ID,
                              &(pPrevArpCache->Timer.Timer_node));
                MemReleaseMemBlock (ARP_POOL_ID, (UINT1 *) pPrevArpCache);
            }
        }
        else
        {
            /* If the entry is present in both the hardware and software,
             * scan for next entry in both */
            pPrevArpCache = pArpCache;
            pArpCache = RBTreeGetNext (gArpGblInfo.ArpTable,
                                       (tRBElem *) pPrevArpCache, NULL);
            i4RetVal = ArpNpGetNext (ArpNpInParam, &ArpNpOutParam);
            pPrevArpCache->u1HwStatus = NP_UPDATED;
        }
    }
    UNUSED_PARAM (ArpCache);
    UNUSED_PARAM (pArpRedInfo);
#endif
#endif

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME, "Exiting ArpRedHwAudit\r \n");
    return;
}

/*-------------------------------------------------------------------+
 * Function           : ArpRBTreeRedEntryCmp 
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : ARP_RB_LESSER  if pRBElem <  pRBElemIn
 *                      ARP_RB_GREATER if pRBElem >  pRBElemIn
 *                      ARP_RB_EQUAL   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy cache entries in lexicographic
 * order of the indices of the Redundancy cache entry.
+-------------------------------------------------------------------*/

INT4
ArpRBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    tArpRedTable       *pArpRed = pRBElem;
    tArpRedTable       *pArpRedIn = pRBElemIn;

    if (pArpRed->u4IfIndex < pArpRedIn->u4IfIndex)
    {
        return ARP_RB_LESSER;
    }
    else if (pArpRed->u4IfIndex > pArpRedIn->u4IfIndex)
    {
        return ARP_RB_GREATER;
    }

    if (pArpRed->u4IpAddr < pArpRedIn->u4IpAddr)
    {
        return ARP_RB_LESSER;
    }
    else if (pArpRed->u4IpAddr > pArpRedIn->u4IpAddr)
    {
        return ARP_RB_GREATER;
    }

    return ARP_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name      : ArpRedHandleDynSyncAudit                             */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ArpRedHandleDynSyncAudit (VOID)
{
/*On receiving this event, ARP should execute show cmd and calculate checksum*/
    ArpExecuteCmdAndCalculateChkSum ();
}

/*****************************************************************************/
/* Function Name      : ArpExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ArpExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_ARP_APP_ID;
    UINT2               u2ChkSum = 0;

    ARP_PROT_UNLOCK ();
    if (ArpGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == ARP_FAILURE)
    {
        ARP_PROT_LOCK ();
        return;
    }

#ifdef L2RED_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        ARP_PROT_LOCK ();
        return;
    }
#else
    UNUSED_PARAM (u2AppId);
#endif
    ARP_PROT_LOCK ();
    return;
}

/************************************************************************
 * Function Name      : ArpRedSendDynamicCacheTimeInfo
 *
 * Description        : This function sends the binding table dynamic
 *                      sync up message to Standby node from the Active
 *                      node, when the ARP offers an IP address
 *                      to the ARP client and dynamically update the
 *                      binding table entries.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/

PUBLIC VOID
ArpRedSendDynamicCacheTimeInfo (t_ARP_CACHE * pArpInfo)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4MsgAllocSize = 0;
    UINT2               u2MsgLen = 0;
    UINT4               u4OffSet = 0;
    UINT4               u4LenOffSet = 0;
    UINT4               u4MsgPacked = 1;
    UINT4               u4NoCacheOffset = 0;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedSendDynamicCacheTimeInfo\r \n");
    ProtoEvt.u4AppId = RM_ARP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u4MsgAllocSize = ARP_RED_MIM_MSG_SIZE;
    u4MsgAllocSize += ARP_RED_DYN_INFO_TIME_SIZE;

    /* Message is allcoated upto required size. The number of entries in the
     * RBTree is got and then it is multiplied with the size of a single
     * dynamic update to get the total count. If that length is greater than
     * MAX size, then the message is allocated upto max size */

    /*
     *    ARP Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 4 B ->|<-2B->|<---- 4B ---->
     *    ---------------------------------------------------------------
     *    | Msg. Type | Length | No of   | IP      | Port | LastUpdated |
     *    |           |        | Entries | Address | No   | Time        |
     *    ---------------------------------------------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (u4MsgAllocSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);

        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | OS_RESOURCE_TRC, ARP_NAME,
                 "ArpRedSendBulkUpdTailMsg: RM Memory allocation"
                 " failed\r\n");
        return;
    }

    ARP_RM_PUT_1_BYTE (pMsg, &u4OffSet, ARP_RED_DYN_CACHE_TIME_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u4LenOffSet = u4OffSet;
    ARP_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
    u4NoCacheOffset = u4OffSet;
    ARP_RM_PUT_2_BYTE (pMsg, &u4OffSet, u4MsgPacked);

    ARP_RM_PUT_4_BYTE (pMsg, &u4OffSet, pArpInfo->u4Ip_addr);
    ARP_RM_PUT_2_BYTE (pMsg, &u4OffSet, pArpInfo->u2Port);
    ARP_RM_PUT_4_BYTE (pMsg, &u4OffSet, pArpInfo->u4LastUpdatedTime);
    u4MsgPacked += 1;

    ARP_RM_PUT_2_BYTE (pMsg, &u4LenOffSet, u4OffSet);
    ARP_RM_PUT_2_BYTE (pMsg, &u4NoCacheOffset, u4MsgPacked);
    if (ArpRedSendMsgToRm (pMsg, (UINT2) u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, ARP_NAME,
                 "ArpRedSendBulkUpdTailMsg:Send message to RM failed \r\n");
        return;
    }
    return;
}

/************************************************************************/
/* Function Name      : ArpRedProcessDynamicCacheTimeInfo               */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
ArpRedProcessDynamicCacheTimeInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    t_ARP_CACHE        *pArpEntry = NULL;
    UINT4               u4LastUpdatedtime = 0;
    UINT4               u4ArpCacheIfIndex = 0;
    UINT4               u4IpAddr = 0;
    UINT2               u2NoOfEntries = 0;
    UINT2               u2Port = 0;
    INT4                i4RetVal = 0;

    ARP_TRC (ARP_DEFAULT_CONTEXT, ARP_MOD_TRC,
             CONTROL_PLANE_TRC, ARP_NAME,
             "Entering ArpRedProcessDynamicCacheTimeInfo\r \n");

    ARP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2NoOfEntries);

    ARP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IpAddr);
    ARP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2Port);
    ARP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4LastUpdatedtime);
    NetIpv4GetCfaIfIndexFromPort ((UINT4) u2Port, &u4ArpCacheIfIndex);
    i4RetVal = ArpGetCacheWithIndex (u4ArpCacheIfIndex, u4IpAddr, &pArpEntry);
    if (i4RetVal != ARP_FAILURE)
    {
        OsixGetSysTime ((tOsixSysTime *) & (pArpEntry->u4LastUpdatedTime));
        return;
    }
    return;
}
#endif
