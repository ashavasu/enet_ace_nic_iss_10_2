
/* $Id: fsmparnc.c,v 1.1 2015/12/29 12:04:45 siva Exp $
    ISS Wrapper module
    module ARICENT-MIARP-MIB

 */
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmparlw.h"
# include  "fsmparnc.h"


/********************************************************************
* FUNCTION NcFsMIArpCacheTimeoutSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIArpCacheTimeoutSet (
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpCacheTimeout )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetFsMIArpCacheTimeout(
                 i4FsMIStdIpContextIdfsMIStdIpContextId,
                i4FsMIArpCacheTimeout);

    return i1RetVal;


} /* NcFsMIArpCacheTimeoutSet */

/********************************************************************
* FUNCTION NcFsMIArpCacheTimeoutTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIArpCacheTimeoutTest (UINT4 *pu4Error,
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpCacheTimeout )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2FsMIArpCacheTimeout(pu4Error,
                 i4FsMIStdIpContextIdfsMIStdIpContextId,
                i4FsMIArpCacheTimeout);

    return i1RetVal;


} /* NcFsMIArpCacheTimeoutTest */

/********************************************************************
* FUNCTION NcFsMIArpCachePendTimeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIArpCachePendTimeSet (
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpCachePendTime )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetFsMIArpCachePendTime(
                 i4FsMIStdIpContextIdfsMIStdIpContextId,
                i4FsMIArpCachePendTime);

    return i1RetVal;


} /* NcFsMIArpCachePendTimeSet */

/********************************************************************
* FUNCTION NcFsMIArpCachePendTimeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIArpCachePendTimeTest (UINT4 *pu4Error,
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpCachePendTime )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2FsMIArpCachePendTime(pu4Error,
                 i4FsMIStdIpContextIdfsMIStdIpContextId,
                i4FsMIArpCachePendTime);

    return i1RetVal;


} /* NcFsMIArpCachePendTimeTest */

/********************************************************************
* FUNCTION NcFsMIArpMaxRetriesSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIArpMaxRetriesSet (
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpMaxRetries )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetFsMIArpMaxRetries(
                 i4FsMIStdIpContextIdfsMIStdIpContextId,
                i4FsMIArpMaxRetries);

    return i1RetVal;


} /* NcFsMIArpMaxRetriesSet */

/********************************************************************
* FUNCTION NcFsMIArpMaxRetriesTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIArpMaxRetriesTest (UINT4 *pu4Error,
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 i4FsMIArpMaxRetries )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2FsMIArpMaxRetries(pu4Error,
                 i4FsMIStdIpContextIdfsMIStdIpContextId,
                i4FsMIArpMaxRetries);

    return i1RetVal;


} /* NcFsMIArpMaxRetriesTest */

/********************************************************************
* FUNCTION NcFsMIArpPendingEntryCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIArpPendingEntryCountGet (
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 *pi4FsMIArpPendingEntryCount )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceFsMIArpTable(
                 i4FsMIStdIpContextIdfsMIStdIpContextId) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIArpPendingEntryCount(
                 i4FsMIStdIpContextIdfsMIStdIpContextId,
                 pi4FsMIArpPendingEntryCount );

    return i1RetVal;


} /* NcFsMIArpPendingEntryCountGet */

/********************************************************************
* FUNCTION NcFsMIArpCacheEntryCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsMIArpCacheEntryCountGet (
                INT4 i4FsMIStdIpContextIdfsMIStdIpContextId,
                INT4 *pi4FsMIArpCacheEntryCount )
{

    INT1 i1RetVal;

    if (nmhValidateIndexInstanceFsMIArpTable(
                 i4FsMIStdIpContextIdfsMIStdIpContextId) == SNMP_FAILURE)

    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsMIArpCacheEntryCount(
                 i4FsMIStdIpContextIdfsMIStdIpContextId,
                 pi4FsMIArpCacheEntryCount );

    return i1RetVal;


} /* NcFsMIArpCacheEntryCountGet */

/* END i_ARICENT_MIARP_MIB.c */
